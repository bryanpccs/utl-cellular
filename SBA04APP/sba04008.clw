

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04008.INC'),ONCE        !Local module procedure declarations
                     END


DespatchMultiple     PROCEDURE  (func:Courier,func:AccountNumber,func:BatchNumber,func:LabelType) ! Declare Procedure
csv_filename         STRING(255)
save_muld_id         USHORT,AUTO
save_mulj_id         USHORT,AUTO
tmp:PassedConsignNo  STRING(30)
tmp:DoDespatch       BYTE(0)
tmp:AccountNumber    STRING(30)
tmp:OldConsignNo     STRING(30)
sav:Path             STRING(255)
tmp:LabelError       STRING(255)
tmp:ConsignNo        STRING(30)
tmp:PrintSummaryNote BYTE(0)
tmp:Today            DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! csv file Declarations
CSVFILE               FILE,DRIVER('BASIC'),OEM,PRE(csv),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
JobNumber                   STRING(10)
AccountNumber               STRING(15)
Manufacturer                STRING(30)
Model                       STRING(30)
IMEI                        STRING(20)
WarrantyRepairType          STRING(30)
ChargeableRepairType        STRING(30)
                         END
                     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchMultiple')      !Add Procedure to Log
  end


    Access:COURIER.Clearkey(cou:Courier_Key)
    cou:Courier = func:Courier
    If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Found
        If cou:AutoConsignmentNo 
            cou:LastConsignmentNo += 1
            Access:COURIER.Update()
            tmp:PassedConsignNo   = cou:LastConsignmentNo

        Else !If cou:AutoConsignmentNo     
            tmp:PassedConsignNo = ''
        End!Else !If cou:AutoConsignmentNo  

        tmp:DoDespatch = 1
        Case cou:Courier_Type
            Of 'CITY LINK'
                glo:file_name   = 'C:\CITYOUT.TXT'
                Remove(expcity)
                access:expcity.open()
                access:expcity.usefile()
                multi# = 1

                DespatchCityLink(1,func:AccountNumber)

                access:expcity.insert()
                access:expcity.close()
                Copy(expcity,Clip(cou:export_path))
                If error()
                    Stop(error())
                    tmp:DoDespatch = 0
                End!If error()
            Of 'LABEL G'
                !Is the despatch after the "cut off" time.
                !Move to the next day
                tmp:Today   = 0
                If cou:Last_Despatch_Time <> 0
                    If Clock() > cou:Last_Despatch_Time
                        tmp:Today = Today()
                        SetToday(Today() + 1)
                    End !If Clock() > cou:Last_Despatch_Time
                End !If cou:Last_Despatch_Time <> 0
                tmp:accountnumber   = func:AccountNumber
                tmp:OldConsignNo    = ''

                x# =DespatchLabelG(1,'CHECKOLD',func:AccountNumber,'')

                ! Start Change ????7Digit Job No BE(22/04/03)
                !glo:file_name   = Clip(cou:export_path) & '\CB' & Format(dbt:batch_number,@n06) & '.TXT'
                glo:file_name   = Clip(cou:export_path) & '\CB' & Format(dbt:batch_number,@n08) & '.TXT'
                ! End Change ????7Digit Job No BE(22/04/03)
                
                Remove(glo:file_name)


                x# = DespatchLabelG(1,'EXPORT',func:AccountNumber,'')

                sav:path    = Path()
                Setpath(cou:export_path)

                ! Start Change ????7Digit Job No BE(22/04/03)
                !Run('LABELG.EXE ' & Clip(glo:file_name) & ' ' & Clip(cou:labgOptions),1)
                Run('LABELG32.EXE ' & 'CB' & Format(dbt:batch_number,@n08) & '.TXT ' & Clip(cou:labgOptions),1)
                ! End Change ????7Digit Job No BE(22/04/03)

                Setpath(sav:path)


                tmp:LabelError = DespatchLabelG('1','ERROR',func:AccountNumber,'')

                If tmp:labelerror = 0
                    tmp:DoDespatch = 1
                End!If tmp:labelerror = 0
                If tmp:Today <> 0
                    SetToday(tmp:Today)
                End !If tmp:Today <> 0
            Of 'SDS'
                !Fudge to make the label work. The Label needs the Global Job Number Queue, for it's job
                !number list, so I'll fill it in.
                Clear(glo:q_JobNumber)
                Free(glo:Q_JobNumber)
                Access:MULDESP.ClearKey(muld:RecordNumberKey)
                muld:RecordNumber = func:BatchNumber
                If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Found
                    Save_mulj_ID = Access:MULDESPJ.SaveFile()
                    Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                    mulj:RefNumber = muld:RecordNumber
                    Set(mulj:JobNumberKey,mulj:JobNumberKey)
                    Loop
                        If Access:MULDESPJ.NEXT()
                           Break
                        End !If
                        If mulj:RefNumber <> muld:RecordNumber      |
                            Then Break.  ! End If
                        glo:Job_Number_Pointer = mulj:JobNumber
                        Add(glo:q_JobNumber)
                    End !Loop
                    Access:MULDESPJ.RestoreFile(Save_mulj_ID)
                Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign

                If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDS_Thermal_Multiple(func:AccountNumber)
                Else !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDSLabel(func:AccountNumber)
                End !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                despatch# = 1
                Clear(glo:q_JobNumber)
                Free(glo:Q_JobNumber)

            Of 'PARCELINE LASERNET'
                tmp:ParcelLineName = Clip(cou:Export_Path) & '\DB' & Format(dbt:Batch_Number,@n06) &  '.TXA'
                Remove(tmp:ParcelLineName)
                Open(ParcelLineExport)
                If Error()
                    Create(ParcelLineExport)
                    Open(ParcelLineExport)
                End !If Error()
                Clear(Parcel:Record)
                multi# = 1

                DespatchParceline(1,func:AccountNumber)

                Add(ParcelLineExport)

                CLose(ParcelLineExport)
                RENAME(Clip(tmp:ParcelLineName),Clip(Clip(cou:Export_Path) & '\DB' & Format(dbt:Batch_Number,@n06) &  '.TXT'))
            Else
                If cou:PrintLabel
                    glo:Select1 = tra:Account_Number
                    glo:Select2 = sub:Account_Number
                    glo:Select3 = func:LabelType
                    Address_Label_Multiple(tmp:PassedConsignNo)
                    glo:Select1 = ''
                    glo:Select2 = ''
                    glo:Select3 = ''
                End !If cou:PrintLabel
        End !Case cou:Courier_Type

        If tmp:DoDespatch = 1

            setcursor()
            Insert_Consignment_Note_Number(func:LabelType,tra:account_number,|
                                            sub:account_number,tmp:ConsignNo,tmp:PassedConsignNo,1)

            If tmp:consignno <> ''
                recordspercycle     = 25
                recordsprocessed    = 0
                percentprogress     = 0
                setcursor(cursor:wait)
                open(progresswindow)
                progress:thermometer    = 0
                ?progress:pcttext{prop:text} = '0% Completed'

                recordstoprocess    = Records(MULDESP)

                !Only create the CSV if the main default is set - TrkBs: 4996 (DBH: 07-12-2004)
                If GETINI('DESPATCH','CreateMultipleDespatchCSV',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                    ! Start Change 4437 BE(27/07/2004)
                    csv_filename = 'Despatch.csv'
                    csv# = true
                    csvempty# = true
                    IF (NOT FILEDIALOG('Choose CSV File', csv_filename, '*.CSV', |
                                        FILE:Save+FILE:KeepDir+FILE:LongName)) THEN
                        csv# = false
                    ELSE
                        CSVFILE{PROP:NAME} = CLIP(csv_filename)

                        IF (EXISTS(csv_filename)) THEN
                            REMOVE(CSVFILE)
                            CREATE(CSVFILE)
                        ELSE
                            CREATE(CSVFILE)
                        END

                        OPEN(CSVFILE)
                        IF (ERRORCODE()) THEN
                            csv# = false
                            MESSAGE(ERROR())
                        END
                    END
                    ! End Change 4437 BE(27/07/2004)
                Else ! If GETINI('DESPATCH','CreateMultipleDespatchCSV',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                    csv# = False
                End ! If GETINI('DESPATCH','CreateMultipleDespatchCSV',,CLIP(PATH())&'\SB2KDEF.INI') = 1


                Access:MULDESP.ClearKey(muld:RecordNumberKey)
                muld:RecordNumber = func:BatchNumber
                If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Found

                    Clear(glo:Q_JobNumber)
                    Free(glo:Q_JobNumber)
                    Save_mulj_ID = Access:MULDESPJ.SaveFile()
                    Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                    mulj:RefNumber = func:BatchNumber
                    Set(mulj:JobNumberKey,mulj:JobNumberKey)
                    Loop
                        If Access:MULDESPJ.NEXT()
                           Break
                        End !If
                        If mulj:RefNumber <> func:BatchNumber      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        access:jobs.clearkey(job:ref_number_key)
                        job:ref_number  = mulj:JobNumber
                        If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                            Despatch(job:Despatch_Type,tmp:ConsignNo,1)

                            ! Start Change 4437 BE(27/07/2004)
                            IF (csv#) THEN
                                IF (csvempty#) THEN
                                    csvempty# = false
                                    CLEAR(CSVFILE)
                                    csv:JobNumber                   = 'Job Number'
                                    csv:AccountNumber               = 'Account Number'
                                    csv:Manufacturer                = 'Manufacturer'
                                    csv:Model                       = 'Model'
                                    csv:IMEI                        = 'IMEI Number'
                                    csv:WarrantyRepairType          = 'Warranty Repair Type'
                                    csv:ChargeableRepairType        = 'Chargeable Repair Type'
                                    ADD(CSVFILE)
                                END
                                CLEAR(CSVFILE)
                                csv:JobNumber                   = job:Ref_Number
                                csv:AccountNumber               = job:Account_Number
                                csv:Manufacturer                = job:Manufacturer
                                csv:Model                       = job:Model_Number
                                csv:IMEI                        = job:ESN
                                csv:WarrantyRepairType          = job:Repair_Type
                                csv:ChargeableRepairType        = job:Repair_Type_Warranty
                                ADD(CSVFILE)
                            END
                            ! End Change 4437 BE(27/07/2004)

                        End!If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                        glo:Job_Number_Pointer = mulj:JobNumber
                        Add(glo:q_JobNumber)

                    End !Loop
                    Access:MULDESPJ.RestoreFile(Save_mulj_ID)

                    ! Start Change 4437 BE(27/07/2004)
                    IF (csv#) THEN
                        CLOSE(CSVFILE)
                    END
                    ! End Change 4437 BE(27/07/2004)

                    setcursor()
                    close(progresswindow)

                    If InvoiceSubAccounts(func:AccountNumber)
                        If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                            tmp:PrintSummaryNote = 1
                        Else !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                            tmp:PrintSummaryNote = 0
                        End !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                    Else !If InvoiceSubAccounts(func:AccountNumber)
                        If tra:Print_Despatch_Despatch = 'YES' And tra:Summary_Despatch_Notes = 'YES'
                            tmp:PrintSummaryNote = 1
                        Else !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                            tmp:PrintSummaryNote = 0
                        End !If sub:Print_Despatch_Despatch = 'YES' And sub:Summary_Despatch_Notes = 'YES'
                    End !If InvoiceSubAccounts(func:AccountNumber)

                    IF tmp:PrintSummaryNote
                        glo:select1  = func:AccountNumber
                        glo:select2  = tmp:consignno
                        glo:select3  = dbt:batch_number
                        glo:select4  = func:Courier
                        Multiple_Despatch_Note
                        glo:select1  = ''
                        glo:select2  = ''
                        glo:select3  = ''
                        glo:select4  = ''
                    End!IF Print_summary# = 1
                    Clear(glo:Q_JobNumber)
                    Free(glo:Q_JobNumber)

                    Relate:MULDESP.Delete(0)

                Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign

            End!If tmp:consignno <> ''
        End !If tmp:Despatch = 1
    Else! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:.Tryfetch(cou:Courier_Key) = Level:Benign
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchMultiple',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('csv_filename',csv_filename,'DespatchMultiple',1)
    SolaceViewVars('save_muld_id',save_muld_id,'DespatchMultiple',1)
    SolaceViewVars('save_mulj_id',save_mulj_id,'DespatchMultiple',1)
    SolaceViewVars('tmp:PassedConsignNo',tmp:PassedConsignNo,'DespatchMultiple',1)
    SolaceViewVars('tmp:DoDespatch',tmp:DoDespatch,'DespatchMultiple',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'DespatchMultiple',1)
    SolaceViewVars('tmp:OldConsignNo',tmp:OldConsignNo,'DespatchMultiple',1)
    SolaceViewVars('sav:Path',sav:Path,'DespatchMultiple',1)
    SolaceViewVars('tmp:LabelError',tmp:LabelError,'DespatchMultiple',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'DespatchMultiple',1)
    SolaceViewVars('tmp:PrintSummaryNote',tmp:PrintSummaryNote,'DespatchMultiple',1)
    SolaceViewVars('tmp:Today',tmp:Today,'DespatchMultiple',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
