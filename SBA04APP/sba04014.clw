

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04014.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
DespatchToteParceLine PROCEDURE  (func:Multiple,func:AccountNumber,func:ConsignmentNo) ! Declare Procedure
tmp:WorkstationName  STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchToteParceLine')      !Add Procedure to Log
  end


     IF GetComputerName(JB:ComputerName,JB:ComputerLen).
     tmp:WorkstationName=JB:ComputerName
    TPL:Labels  = 1
    If func:Multiple
        TPL:OrderNo = 'DB' & dbt:Batch_Number
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = func:AccountNumber
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:Use_Sub_Accounts = 'YES'
                    TPL:CompanyName  = sub:Company_Name
                    TPL:AddressLine1 = sub:Address_Line1
                    TPL:AddressLine2 = sub:Address_Line2
                    TPL:Town         = sub:Address_Line3
                    TPL:Postcode     = sub:Postcode
                Else !If tra:Use_Sub_Accounts = 'YES'
                    TPL:CompanyName  = tra:Company_Name
                    TPL:AddressLine1 = tra:Address_Line1
                    TPL:AddressLine2 = tra:Address_Line2
                    TPL:Town         = tra:Address_Line3
                    TPL:Postcode     = tra:Postcode
                 
                End !If tra:Use_Sub_Accounts = 'YES'
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            
        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        TPL:AccountCode  = func:AccountNumber
    Else !If multi# = 1
        TPL:OrderNo = 'J' & job:Ref_Number & ' ' & job:Despatch_Type
        TPL:Instructions = 'DB' & dbt:Batch_Number
        TPL:CompanyName  = job:Company_Name_Delivery
        TPL:AddressLine1 = job:Address_Line1_Delivery
        TPL:AddressLine2 = job:Address_Line2_Delivery
        TPL:Town         = job:Address_Line3_Delivery
        TPL:Postcode     = job:Postcode_Delivery
        TPL:AccountCode  = job:Account_Number
    End !If multi# = 1
    TPL:Service       = cou:Service
    TPL:Type          = 1
    TPL:Workstation   = tmp:WorkstationName
    TPL:ConsignmentNo = func:ConsignmentNo


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchToteParceLine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:WorkstationName',tmp:WorkstationName,'DespatchToteParceLine',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
