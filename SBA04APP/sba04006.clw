

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04006.INC'),ONCE        !Local module procedure declarations
                     END


DespatchCityLink     PROCEDURE  (func:Multiple,func:AccountNumber) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchCityLink')      !Add Procedure to Log
  end


    epc:account_number    = Left(Sub(cou:account_number,1,8))
    dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
    If func:Multiple
        dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
        epc:ref_number        = '|DB' & Clip(Format(dbt:batch_number,@s9))
        epc:customer_name     = '|' & Left(Sub(func:AccountNumber,1,30))        
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = func:AccountNumber
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                epc:contact_name      = '|N/A' 
                if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(sub:contact_name)
                    epc:address_line1 = '|' & Left(sub:company_name)
                    epc:address_line2 = '|' & Left(sub:address_line1)
                    epc:town = '|' & Left(sub:address_line2)
                    epc:county = '|' & Left(sub:address_line3)
                    epc:postcode = '|' & Left(sub:postcode)
                else!if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(tra:contact_name)
                    epc:address_line1 = '|' & Left(tra:company_name)
                    epc:address_line2 = '|' & Left(tra:address_line1)
                    epc:town = '|' & Left(tra:address_line2)
                    epc:county = '|' & Left(tra:address_line3)
                    epc:postcode = '|' & Left(tra:postcode)
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If multiple_despatch_temp = 'YES'
        If job:surname = ''
            epc:contact_name      = '|N/A'            
        Else!If job:surname = ''
            epc:contact_name      = '|' & Left(Sub(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),1,30))
        End!
        epc:address_line1     = '|' & Left(Sub(job:company_name_delivery,1,30))
        epc:address_line2     = '|' & Left(Sub(job:address_line1_delivery,1,30))
        epc:town              = '|' & Left(Sub(job:address_line2_delivery,1,30))
        epc:county            = '|' & Left(Sub(job:address_line3_delivery,1,30))
        epc:postcode          = '|' & Left(Sub(job:postcode_delivery,1,8))
        epc:customer_name     = '|' & Left(Sub(job:account_number,1,30))        
    End!If multiple_despatch_temp = 'YES'
    epc:ref_number        = '|J' & Clip(Format(job:ref_number,@s9)) & '/DB' & Clip(Format(dbt:batch_number,@s9)) & |
                                    '/' & Clip(job:order_number)
    
    epc:nol               = '|' & '01'
    epc:city_service      = '|' & Left(Sub(cou:service,1,2))
    Case job:despatch_type    
        Of 'JOB'
            If job:jobservice <> ''
                epc:city_service      = '|' & Left(Sub(job:jobservice,1,2))                        
            End!            If job:jobservice <> ''
        Of 'EXC'
            If job:excservice <> ''
                epc:city_service      = '|' & Left(Sub(job:excservice,1,2))            
            End!If job:excservice <> ''
        Of 'LOA'
            If job:loaservice <> ''
                epc:city_service      = '|' & Left(Sub(job:loaservice,1,2))            
            End!If job:loaservice <> ''
    End!Case job:despatch_type
    access:jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:ref_number
    access:jobnotes.tryfetch(jbn:REfNumberKey)
    epc:city_instructions = '|' & Left(Sub(jbn:delivery_text,1,30))
    epc:pudamt            = '|' & Left(Sub('0.00',1,4))
    
    return#    = 1
    If job:despatch_type = 'EXC' Or job:despatch_type = 'LOA'
        If job:workshop = 'YES'
            return# = 0
        Else!If job:workshop = 'YES'
            If job:third_party_site <> ''
                return# = 0
            End!If job:third_party_site <> ''
        End!If job:workshop = 'YES'
    End!If job:despatch_type = 'EXC'
    If job:despatch_type = 'JOB'
        If job:loan_unit_number = ''
            return# = 0
        End!If job:loan_unit_number = ''
    End!If job:despatch_type = 'JOB'
    
    If return# = 0
        epc:Return_it    = '|N'
    Else!If return# = 0
        epc:Return_it    = '|Y'
    End!If return# = 0
    epc:saturday          = '|' & Left('N')
    epc:dog               = '|' & Left(Sub('Mobile Phone Goods',1,30))


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchCityLink',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
