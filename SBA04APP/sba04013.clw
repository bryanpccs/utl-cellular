

   MEMBER('sba04app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA04013.INC'),ONCE        !Local module procedure declarations
                     END


DespatchInvoice PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CourierCost      REAL
tmp:LabourCost       REAL
tmp:PartsCost        REAL
tmp:SubTotal         REAL
tmp:VAT              REAL
tmp:Total            REAL
tmp:LabourVATRate    REAL
tmp:PartsVATRate     REAL
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Chargeable Repair'),AT(,,371,223),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,364,188),USE(?Sheet1),SPREAD
                         TAB('Job Details'),USE(?Tab1)
                           PROMPT('Account Number'),AT(8,20),USE(?Prompt1:2),FONT(,7,,)
                           STRING(@s15),AT(8,28),USE(job:Account_Number),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Account Name'),AT(8,40),USE(?Prompt1),FONT(,7,,)
                           STRING(@s30),AT(8,48),USE(sub:Company_Name),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Despatch Courier'),AT(172,48),USE(?Prompt11)
                           COMBO(@s20),AT(240,48,124,10),USE(job:Courier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           STRING(@s30),AT(8,68),USE(job:Charge_Type),FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(8,88),USE(job:Repair_Type),FONT(,,,FONT:bold,CHARSET:ANSI)
                           GROUP('Billing Details'),AT(8,100,136,84),USE(?Group1),BOXED
                             PROMPT('Labour Cost'),AT(16,124),USE(?Prompt5)
                             PROMPT('Parts Cost'),AT(16,136),USE(?Prompt5:2)
                             STRING(@n14.2),AT(64,136),USE(tmp:PartsCost),FONT(,,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Sub Total'),AT(16,148),USE(?Prompt5:4)
                             STRING(@n14.2),AT(64,148),USE(tmp:SubTotal),FONT(,,,FONT:bold,CHARSET:ANSI)
                             PROMPT('V.A.T.'),AT(16,160),USE(?Prompt5:5)
                             STRING(@n14.2),AT(64,160),USE(tmp:VAT),FONT(,,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Total'),AT(16,172),USE(?Prompt5:6)
                             STRING(@n14.2),AT(64,172),USE(tmp:Total),FONT(,,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Courier Cost'),AT(16,112),USE(?Prompt5:3)
                             STRING(@n14.2),AT(64,112),USE(tmp:CourierCost),FONT(,,,FONT:bold,CHARSET:ANSI)
                             STRING(@n14.2),AT(64,124),USE(tmp:LabourCost),FONT(,,,FONT:bold,CHARSET:ANSI)
                           END
                           GROUP('Invoice Details'),AT(232,108,132,80),USE(?InvoiceDetails),BOXED,HIDE
                             PROMPT('Invoice Number:'),AT(239,132),USE(?InvoiceNumber)
                             STRING(@s8),AT(299,132),USE(job:Invoice_Number),FONT(,,,FONT:bold)
                             STRING(@d6b),AT(300,152),USE(job:Invoice_Date),FONT(,,,FONT:bold)
                             PROMPT('Invoice Date:'),AT(240,152),USE(?InvoiceNumber:2)
                           END
                           PROMPT('Charge Type'),AT(8,60),USE(?Prompt1:3),FONT(,7,,)
                           PROMPT('Repair Type'),AT(8,80),USE(?Prompt1:4),FONT(,7,,)
                           BUTTON('Print Invoice'),AT(264,84,80,19),USE(?PrintInvoice),LEFT,ICON('money.gif')
                         END
                       END
                       PANEL,AT(4,196,364,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(308,200,56,16),USE(?OK),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?job:Account_Number{prop:FontColor} = -1
    ?job:Account_Number{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?sub:Company_Name{prop:FontColor} = -1
    ?sub:Company_Name{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    If ?job:Courier{prop:ReadOnly} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 15066597
    Elsif ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 8454143
    Else ! If ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 16777215
    End ! If ?job:Courier{prop:Req} = True
    ?job:Courier{prop:Trn} = 0
    ?job:Courier{prop:FontStyle} = font:Bold
    ?job:Charge_Type{prop:FontColor} = -1
    ?job:Charge_Type{prop:Color} = 15066597
    ?job:Repair_Type{prop:FontColor} = -1
    ?job:Repair_Type{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    ?tmp:PartsCost{prop:FontColor} = -1
    ?tmp:PartsCost{prop:Color} = 15066597
    ?Prompt5:4{prop:FontColor} = -1
    ?Prompt5:4{prop:Color} = 15066597
    ?tmp:SubTotal{prop:FontColor} = -1
    ?tmp:SubTotal{prop:Color} = 15066597
    ?Prompt5:5{prop:FontColor} = -1
    ?Prompt5:5{prop:Color} = 15066597
    ?tmp:VAT{prop:FontColor} = -1
    ?tmp:VAT{prop:Color} = 15066597
    ?Prompt5:6{prop:FontColor} = -1
    ?Prompt5:6{prop:Color} = 15066597
    ?tmp:Total{prop:FontColor} = -1
    ?tmp:Total{prop:Color} = 15066597
    ?Prompt5:3{prop:FontColor} = -1
    ?Prompt5:3{prop:Color} = 15066597
    ?tmp:CourierCost{prop:FontColor} = -1
    ?tmp:CourierCost{prop:Color} = 15066597
    ?tmp:LabourCost{prop:FontColor} = -1
    ?tmp:LabourCost{prop:Color} = 15066597
    ?InvoiceDetails{prop:Font,3} = -1
    ?InvoiceDetails{prop:Color} = 15066597
    ?InvoiceDetails{prop:Trn} = 0
    ?InvoiceNumber{prop:FontColor} = -1
    ?InvoiceNumber{prop:Color} = 15066597
    ?job:Invoice_Number{prop:FontColor} = -1
    ?job:Invoice_Number{prop:Color} = 15066597
    ?job:Invoice_Date{prop:FontColor} = -1
    ?job:Invoice_Date{prop:Color} = 15066597
    ?InvoiceNumber:2{prop:FontColor} = -1
    ?InvoiceNumber:2{prop:Color} = 15066597
    ?Prompt1:3{prop:FontColor} = -1
    ?Prompt1:3{prop:Color} = 15066597
    ?Prompt1:4{prop:FontColor} = -1
    ?Prompt1:4{prop:Color} = 15066597
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchInvoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:CourierCost',tmp:CourierCost,'DespatchInvoice',1)
    SolaceViewVars('tmp:LabourCost',tmp:LabourCost,'DespatchInvoice',1)
    SolaceViewVars('tmp:PartsCost',tmp:PartsCost,'DespatchInvoice',1)
    SolaceViewVars('tmp:SubTotal',tmp:SubTotal,'DespatchInvoice',1)
    SolaceViewVars('tmp:VAT',tmp:VAT,'DespatchInvoice',1)
    SolaceViewVars('tmp:Total',tmp:Total,'DespatchInvoice',1)
    SolaceViewVars('tmp:LabourVATRate',tmp:LabourVATRate,'DespatchInvoice',1)
    SolaceViewVars('tmp:PartsVATRate',tmp:PartsVATRate,'DespatchInvoice',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Account_Number;  SolaceCtrlName = '?job:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sub:Company_Name;  SolaceCtrlName = '?sub:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier;  SolaceCtrlName = '?job:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type;  SolaceCtrlName = '?job:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type;  SolaceCtrlName = '?job:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PartsCost;  SolaceCtrlName = '?tmp:PartsCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:4;  SolaceCtrlName = '?Prompt5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SubTotal;  SolaceCtrlName = '?tmp:SubTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:5;  SolaceCtrlName = '?Prompt5:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:VAT;  SolaceCtrlName = '?tmp:VAT';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:6;  SolaceCtrlName = '?Prompt5:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Total;  SolaceCtrlName = '?tmp:Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:3;  SolaceCtrlName = '?Prompt5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CourierCost;  SolaceCtrlName = '?tmp:CourierCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LabourCost;  SolaceCtrlName = '?tmp:LabourCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceDetails;  SolaceCtrlName = '?InvoiceDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceNumber;  SolaceCtrlName = '?InvoiceNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Number;  SolaceCtrlName = '?job:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Date;  SolaceCtrlName = '?job:Invoice_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceNumber:2;  SolaceCtrlName = '?InvoiceNumber:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:3;  SolaceCtrlName = '?Prompt1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:4;  SolaceCtrlName = '?Prompt1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PrintInvoice;  SolaceCtrlName = '?PrintInvoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DespatchInvoice')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'DespatchInvoice')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:COURIER.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  Access:INVOICE.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  If job:Invoice_Number <> 0
      tmp:CourierCost = job:Invoice_Courier_Cost
      tmp:LabourCost  = job:Invoice_Labour_Cost
      tmp:PartsCost   = job:Invoice_Parts_Cost
      tmp:SubTotal    = job:Invoice_Sub_Total
      Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
      inv:Invoice_Number  = job:Invoice_Number
      If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
          !Found
          tmp:VAT         = (job:Invoice_Courier_Cost * inv:Vat_Rate_Labour/100) + |
                              (job:Invoice_Labour_Cost * inv:Vat_Rate_Labour/100) + |
                              (job:Invoice_Parts_Cost * inv:Vat_Rate_Parts/100)
      Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
  
      ?InvoiceDetails{prop:Hide} = 0
  
  Else !job:Invoice_Number <> 0
      tmp:CourierCost = job:Courier_Cost
      tmp:LabourCost  = job:Labour_Cost
      tmp:PartsCost   = job:Parts_Cost
      tmp:SubTotal    = job:Sub_Total
  
      If InvoiceSubAccounts(job:Account_Number)
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = sub:Labour_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:LabourVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
  
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = sub:Parts_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:PartsVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
      Else !If InvoiceSubAccounts(job:Account_Number)
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = tra:Labour_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:LabourVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
  
          Access:VATCODE.Clearkey(vat:VAT_Code_Key)
          vat:VAT_Code    = tra:Parts_Vat_Code
          If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Found
              tmp:PartsVATRate   = vat:VAT_Rate
          Else! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Return Level:Fatal
          End! If Access:VATCODE.Tryfetch(vat:VAT_Code_Key) = Level:Benign
  
      End !If InvoiceSubAccounts(job:Account_Number)
  
      tmp:VAT         = (job:Courier_Cost * tmp:LabourVATRate/100) + |
                              (job:Labour_Cost * tmp:LabourVATRate/100) + |
                              (job:Parts_Cost * tmp:PartsVATRate/100)
      ?InvoiceDetails{prop:Hide} = 1
      ?PrintInvoice{prop:Text} = 'Create Invoice'
  End !job:Invoice_Number <> 0
  tmp:Total       = tmp:SubTotal + tmp:VAT
  
  FDCB4.Init(job:Courier,?job:Courier,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(cou:Courier_Key)
  FDCB4.AddField(cou:Courier,FDCB4.Q.cou:Courier)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'DespatchInvoice',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PrintInvoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
    !Do a double check to see if the invoice can still be created
    Error# = 0
    If job:Invoice_Number = ''
        If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
            If CreateInvoice() = Level:Benign

            Else !If CreateInvoice() = Level:Benign
?   Message('Create Invoice? NO','Debug Message', Icon:Hand)
                Error# = 1
            End !If CreateInvoice() = Level:Benign
        Else !If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
?   Message('Can Invoice Be Printed? NO','Debug Message', Icon:Hand)
            Error# = 1
        End !If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
    End !job:Invoice_Number = ''
    If Error# = 0
        glo:Select1 = job:Invoice_Number
        Single_Invoice
        glo:Select1 = ''
        ThisWindow.Reset(1)
        ?InvoiceDetails{prop:Hide} = 0
    End !Error# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PrintInvoice, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'DespatchInvoice')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

