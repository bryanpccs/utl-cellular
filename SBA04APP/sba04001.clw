

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04001.INC'),ONCE        !Local module procedure declarations
                     END


DespatchANC          PROCEDURE  (f_Courier,func:type) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchANC')      !Add Procedure to Log
  end


    job:despatched = 'REA'
    job:Despatch_Type = func:type
    job:Current_Courier = f_courier
?   Message('f_courier: ' & f_courier & '|func:type: ' & func:type,'Debug Message', Icon:Hand)
    Set(defaults)
    Access:defaults.next()
    access:courier.clearkey(cou:Courier_Key)
    cou:courier = f_courier
    if access:courier.tryfetch(cou:Courier_Key) = Level:Benign
        If cou:Courier_Type = 'ANC' Or cou:Courier_Type = 'ROYAL MAIL' Or |
                cou:Courier_Type = 'UPS' Or cou:Courier_Type = 'PARCELINE'
            If func:Type <> 'JOB'
                If job:Workshop <> 'YES'                
                    Return Today()                    
                End!If job:Workshop <> 'YES'
            End!If func:Type <> 'JOB'
            Case Today() % 7
                Of 6 !Saturday
                    If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                        Return Today() + 2
                    End!If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                    If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                        Return Today() + 1
                    End!If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                    IF def:Include_Saturday = 'YES' and def:Include_Sunday <> 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 2
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!IF def:Include_Saturday = 'YES' and def:Include_Sunday <> 'YES'
                    If def:Include_Saturday = 'YES' and def:Include_Sunday = 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 1
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!If def:Include_Saturday = 'YES' and def:Include_Sunday = 'YES'
                Of 0 !Sunday
                    If def:Include_Sunday = 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 1
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    Else!If def:Include_Sunday = 'YES'
                        Return Today() + 1
                    End!If def:Include_Sunday = 'YES'
                Of 5 !Friday
                    If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 3
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!If def:Include_Saturday <> 'YES' And def:Include_Sunday <> 'YES'
                    If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 2
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!If def:Include_Saturday <> 'YES' and def:Include_Sunday = 'YES'
                    IF def:Include_Saturday = 'YES' 
                        If clock() > cou:Last_Despatch_Time
                            Return Today() + 1
                        Else!If clock() > cou:Last_Despatch_Time
                            Return Today()
                        End!If clock() > cou:Last_Despatch_Time
                    End!IF def:Include_Saturday = 'YES' and def:Include_Sunday <> 'YES'
                Else
                    If clock() > cou:Last_Despatch_Time
                        Return Today() + 1
                    Else!If clock() > cou:Last_Despatch_Time
                        Return Today()
                    End!If clock() > cou:Last_Despatch_Time
            End!Case Today() % 7
        End!If cou:CourierType = 'ANC'
    end!if access:courier.tryfetch(cou:CourierKey) = Level:Benign
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchANC',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
