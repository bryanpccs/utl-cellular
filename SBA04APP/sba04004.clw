

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04004.INC'),ONCE        !Local module procedure declarations
                     END


DespatchSingle       PROCEDURE                        ! Declare Procedure
tmp:PassedConsignNo  STRING(30)
tmp:ConsignNo        STRING(30)
tmp:AccountNumber    STRING(30)
tmp:OldConsignNo     STRING(30)
sav:Path             STRING(255)
tmp:LabelError       STRING(255)
Account_Number2_Temp STRING(30)
tmp:Today            DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchSingle')      !Add Procedure to Log
  end


    cont# = 1
    Error# = 0
    If InvoiceSubAccounts(job:Account_Number)
        If job:Chargeable_Job = 'YES' and sub:Stop_Account = 'YES'
            Case MessageEx('Cannot despatch job number ' & clip(job:Ref_Number) & '. It''s Trade Account is "on stop".','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            Error# = 1
        End !If job:Chargeable_Job = 'YES' and sub:Stop_Account = 'YES'
        !Does this account required auto/manual invoice created?
        If sub:InvoiceAtDespatch and job:Despatch_Type = 'JOB'
            !Is it manual or automatic?
            Case sub:InvoiceType
                Of 0 !Manual
                    If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                        DespatchInvoice()
                    End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                Of 1 !Automatic
                    If job:Invoice_Number = ''
                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                            PrintInvoice# = 0
                            If job:Invoice_Number = ''
                                If CreateInvoice() = Level:Benign
                                    PrintInvoice# = 1
                                End !If CreateInvoice() = Level:Benign
                            Else !If job:Invoice_Number = ''
                                PrintInvoice# = 1
                            End !If job:Invoice_Number = ''
                            If PrintInvoice#
                                glo:Select1 = job:Invoice_Number
                                Single_Invoice
                                glo:Select1 = ''
                            End !If PrintInvoice#
                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                    End !If job:Invoice_Number = ''
            End !Case sub:InvoiceType
        End !If sub:InvoiceAtDespatch

    Else !If InvoiceSubAccounts(job:Account_Number)
        If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
            Case MessageEx('Cannot despatch job number ' & clip(job:Ref_Number) & '. It''s Trade Account is "on stop".','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            Error# = 1
        End !If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
        !Does this account required auto/manual invoice created?
        If tra:InvoiceAtDespatch and job:Despatch_Type = 'JOB'
            !Is it manual or automatic?
            Case tra:InvoiceType
                Of 0 !Manual
                    If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                        DespatchInvoice()
                    End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                Of 1 !Automatic
                    If job:Invoice_Number = ''
                        If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                            PrintInvoice# = 0
                            If job:Invoice_Number = ''
                                If CreateInvoice() = Level:Benign
                                    PrintInvoice# = 1
                                End !If CreateInvoice() = Level:Benign
                            Else !If job:Invoice_Number = ''
                                PrintInvoice# = 1
                            End !If job:Invoice_Number = ''
                            If PrintInvoice#
                                glo:Select1 = job:Invoice_Number
                                Single_Invoice
                                glo:Select1 = ''
                            End !If PrintInvoice#
                        End !If CanInvoiceBePrinted(job:Ref_Number,0) = Level:Benign
                    End !If job:Invoice_Number = ''
            End !Case sub:InvoiceType
        End !If sub:InvoiceAtDespatch
    End !If InvoiceSubAccounts(job:Account_Number)

    Access:COURIER.Clearkey(cou:Courier_Key)
    Case job:Despatch_Type
        Of 'JOB'
            cou:Courier = job:Courier
        Of 'EXC'
            cou:Courier = job:Exchange_Courier
        Of 'LOA'
            cou:Courier = job:Loan_Courier
    End !Case job:Despatch_Type
    If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Found
    Else! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
    
    ! Start Change 3813 BE(19/01/04)
    IF ((job:Despatch_Type = 'JOB')  AND (cou:Last_Despatch_Time <> '') AND |
        ((cou:Courier_Type = 'TOTE') OR (cou:Courier_Type = 'TOTE PARCELINE'))) THEN
        IF CLOCK() > cou:Last_Despatch_Time
            thedate# = TODAY()+1
            IF (thedate# %7 = 6) THEN
                thedate# = thedate# + 2
            END
        ELSE
            thedate#    = Today()
        END
        job:date_despatched = thedate#
    END
    ! End Change 3813 BE(19/01/04)

    If Error# = 0
       If job:Chargeable_Job = 'YES' and job:Despatch_Type = 'JOB'
            If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                ! Start Change 2893 BE(21/07/03)
                !If CheckPaid() = Level:Benign
                Total_Price('C', paid_vat", paid_total", paid_balance")
                !p_total$ = paid_total"
                p_balance$ = paid_balance"
                IF (p_balance$ > 0) THEN
                ! End Change 2893 BE(21/07/03)
                    glo:Select1 = job:Ref_Number
                    Browse_Payments
                    glo:Select1 = ''
                End !If CheckPaid() = Level:Benign
            End !If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        End !If job:Chargeable_Job = 'YES'
        If cou:AutoConsignmentNo 
            cou:LastConsignmentNo += 1
            Access:COURIER.Update()
            tmp:PassedConsignNo   = cou:LastConsignmentNo
        Else !If cou:AutoConsignmentNo     
            tmp:PassedConsignNo = ''
        End!Else !If cou:AutoConsignmentNo

        Case cou:Courier_Type
            Of 'CITY LINK'
                glo:file_name   = 'C:\CITYOUT.TXT'
                Remove(expcity)
                access:expcity.open()
                access:expcity.usefile()
                epc:ref_number  = '|' & Left(Sub(Format(job:ref_number,@n_30),1,30))

                !Single Despatch Of City Link

                DespatchCityLink(0,job:Account_Number)

                access:expcity.insert()
                access:expcity.close()
                Copy(expcity,Clip(cou:export_path))
                tmp:ConsignNo   =   ''
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''

            Of 'LABEL G'
                !If the time is past the "Cut off" despatch time, advance the day by one
                tmp:Today = 0
                If cou:Last_Despatch_Time <> 0
                    If Clock() > cou:Last_Despatch_Time
                        tmp:Today = Today()
                        SetToday(Today() + 1)
                    End !If Clock() > cou:Last_Despatch_Time
                End !If cou:Last_Despatch_Time <> 0

                tmp:accountnumber   = job:account_number
                tmp:OldConsignNo    = ''
                tmp:OldConsignNo    = DespatchLabelG(0,'CHECKOLD',job:Account_Number,'')


                ! Start Change ????7Digit Job No BE(22/04/03)
                !glo:file_name   = Clip(cou:export_path) & '\CL' & Format(job:ref_number,@n06) & '.TXT'
                glo:file_name   = Clip(cou:export_path) & '\CL' & Format(job:ref_number,@n08) & '.TXT'
                ! End Change ????7Digit Job No BE(22/04/03)

                Remove(glo:file_name)

                !Nothing will be returned by this.
                Nothing# = DespatchLabelG(0,'EXPORT',job:Account_Number,tmp:OldConsignNo)

                sav:path    = Path()
                Setpath(cou:export_path)

                ! Start Change ????7Digit Job No BE(22/04/03)
                !Run('LABELG.EXE ' & Clip(glo:file_name) & ' ' & Clip(COU:LabGOptions),1)
                Run('LABELG32.EXE ' & 'CL' & Format(job:ref_number,@n08) & '.TXT ' & Clip(COU:LabGOptions),1)
                ! End Change ????7Digit Job No BE(22/04/03)

                Setpath(sav:path)

                tmp:LabelError = DespatchLabelG(0,'ERROR',job:Account_Number,'')
                
                If tmp:labelerror <> 1
                    tmp:ConsignNo   =   ''
                    Setcursor()
                    Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:labelerror,0)
                    Setcursor(cursor:wait)
                    If tmp:ConsignNo  = ''
                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:ref_number    = job:ref_number
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = job:despatch_type
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            aud:action        = 'DESPATCH ROUTINE CANCELLED'
                            access:audit.insert()
                        end!�if access:audit.primerecord() = level:benign
        
                    Else!If consignment_number"  = ''
                        Despatch(job:Despatch_Type,tmp:ConsignNo,0)                                      
                    End!If consignment_number"  = ''
                End!If label_error# = 0
                !If the date has been put forward, change it back.
                If tmp:Today <> 0
                    SetToday(tmp:Today)
                End !If tmp:Today <> 0
            Of 'UPS ALTERNATIVE'
                local:FileName   = 'C:\UPS.TXT'
                Remove(local:FileName)

                DespatchUPSAlt(0,job:Account_Number)
                Copy(local:FileName,Clip(cou:Export_Path))

                tmp:ConsignNo   =   ''
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''

            Of 'ANC'
                !Do Nothing
            Of 'SDS'
                If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDS_Thermal_Label(job:Ref_Number)
                Else !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                    SDSLabelInd(job:Ref_Number)        
                End !If GETINI('PRINTING','SDS',,CLIP(PATH())&'\SB2KDEF.INI') = 'YES'
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                setcursor(cursor:wait)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''
            Of 'PARCELINE LASERNET'
                !Parceline
                tmp:ParcelLineName = Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXA'
                Remove(tmp:ParcelLineName)
                Open(ParcelLineExport)
                If Error()
                    Create(ParcelLineExport)
                    Open(ParcelLineExport)
                End !If Error()
                Clear(Parcel:Record)

                DespatchParceline(0,job:Account_Number)

                Add(ParcelLineExport)

                CLose(ParcelLineExport)
                RENAME(Clip(tmp:ParcelLineName),Clip(Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXT'))
                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''
            Of 'TOTE'
                IF job:Date_Despatched = ''
                  tmp:PassedConsignNo = 'IN' & Clip(sub:Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2)
                ELSE
                  tmp:PassedConsignNo = 'IN' & Clip(sub:Account_Number) & Format(Day(job:Date_Despatched),@n02) & Format(Month(job:Date_Despatched),@n02) & Sub(Year(job:Date_Despatched),3,2)
                END

                !Start - Only print label when required - TrkBs: 5248 (DBH: 18-01-2005)
                If cou:PrintLabel
                    ToteLabel(job:Ref_Number,tmp:PassedConsignNo)
                End ! If cou:PrintLabel
                !End   - Only print label when required - TrkBs: 5248 (DBH: 18-01-2005)
                
                Insert_Consignment_Note_Number('DEL',job:Ref_Number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''
            ! Start Change 3813 BE(19/01/04)
            Of 'TOTE PARCELINE'
                IF job:Date_Despatched = ''
                  tmp:PassedConsignNo = 'IN' & Clip(sub:Account_Number) & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Year(Today()),3,2)
                ELSE
                  tmp:PassedConsignNo = 'IN' & Clip(sub:Account_Number) & Format(Day(job:Date_Despatched),@n02) & Format(Month(job:Date_Despatched),@n02) & Sub(Year(job:Date_Despatched),3,2)
                END

                tmp:ParcelLineName = Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXA'
                Remove(tmp:ParcelLineName)
                Open(ToteParcelLineExport)
                If Error()
                    Create(ToteParcelLineExport)
                    Open(ToteParcelLineExport)
                End !If Error()
                Clear(TPL:Record)

                DespatchToteParceline(0,job:Account_Number,tmp:PassedConsignNo)

                Add(ToteParcelLineExport)

                CLose(ToteParcelLineExport)
                RENAME(Clip(tmp:ParcelLineName),Clip(Clip(cou:Export_Path) & '\J' & Format(Job:Ref_Number,@n07) &  '.TXT'))

                !Start - Only print label when required - TrkBs: 5248 (DBH: 18-01-2005)
                If cou:PrintLabel
                    ToteLabel(job:Ref_Number,tmp:PassedConsignNo)
                End !If cou:PrintLabel
                !End   - Only print label when required - TrkBs: 5248 (DBH: 18-01-2005)

                Insert_Consignment_Note_Number('DEL',job:Ref_Number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''
            ! End Change 3813 BE(19/01/04)
            Else
                
                If cou:PrintLabel
                    glo:Select1 = job:Ref_Number
                    Address_Label('DEL',tmp:PassedConsignNo)
                    glo:Select1 = ''
                End !If cou:PrintLabel


                Insert_Consignment_Note_Number('DEL',job:ref_number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)

                If tmp:ConsignNo  = ''
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = job:despatch_type
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = 'DESPATCH ROUTINE CANCELLED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
        
                Else!If consignment_number"  = ''
                    Despatch(job:Despatch_Type,tmp:ConsignNo,0)
                End!If consignment_number"  = ''
        End!Case cou:courier_type
    End !If Error# = 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchSingle',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:PassedConsignNo',tmp:PassedConsignNo,'DespatchSingle',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'DespatchSingle',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'DespatchSingle',1)
    SolaceViewVars('tmp:OldConsignNo',tmp:OldConsignNo,'DespatchSingle',1)
    SolaceViewVars('sav:Path',sav:Path,'DespatchSingle',1)
    SolaceViewVars('tmp:LabelError',tmp:LabelError,'DespatchSingle',1)
    SolaceViewVars('Account_Number2_Temp',Account_Number2_Temp,'DespatchSingle',1)
    SolaceViewVars('tmp:Today',tmp:Today,'DespatchSingle',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
