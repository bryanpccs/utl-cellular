

   MEMBER('sba04app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA04003.INC'),ONCE        !Local module procedure declarations
                     END


DespatchAtClosing    PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DespatchAtClosing')      !Add Procedure to Log
  end


    If job:despatched = 'YES'
        !Job Already Ready To Despatch
        Return Level:Benign
    Else !job:despatched = 'YES'
        If ToBeLoaned() = 1
            restock# = 0
            If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                Case MessageEx('A Loan Unit has been issued to this job but the Initial Transit Type has been set-up so that an Exchange Unit is required.<13,10><13,10>Do you wish to continue and mark this uni for Despatch Back to the Customer, or do you want to Restock it?','ServiceBase 2000',|
                               'Styles\question.ico','|&Continue|&Restock Unit',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                    Of 1 ! &Continue Button
                    Of 2 ! &Restock Unit Button
                        ForceDespatch()
                        restock# = 1
                End!Case MessageEx
            End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
            If restock# = 0
                job:despatched  = 'LAT'
                job:despatch_Type = 'JOB'
            End!If restock# = 0
        Else!If ToBeLoaned() = 1
            If ToBeExchanged()
                ForceDespatch()
            Else!If ToBeExchanged()
                job:date_despatched = DespatchANC(job:courier,'JOB')
                access:courier.clearkey(cou:courier_key)
                cou:courier = job:courier
                if access:courier.tryfetch(cou:courier_key) = Level:benign
                    If cou:despatchclose = 'YES'
                        Return Level:Fatal
                    Else!If cou:despatchclose = 'YES'
                        Return Level:Benign
                    End!If cou:despatchclose = 'YES'
                End!if access:courier.tryfetch(cou:courier_key) = Level:benign
            End!If ToBeExchanged()
        End!If ToBeLoaned() = 1

    End !job:despatched = 'YES'


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchAtClosing',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
