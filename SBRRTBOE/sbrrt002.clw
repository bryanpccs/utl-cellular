

   MEMBER('sbrrtboe.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetSimp.inc'),ONCE

                     MAP
                       INCLUDE('SBRRT002.INC'),ONCE        !Local module procedure declarations
                     END


TestEmailSend PROCEDURE (func:Email,func:Summary,func:Generate_all) !Generated from procedure template - Window

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       This is just an example of some of the functionality
            !       of the NetEmailSend object.
            !
            !       We have tried to keep it simple (no frills) as it's purpose
            !       is to be an example.
            !
            !       You will notice that the code has been commented to help you
            !       understand what we have done and why.
            !
            !       We strongly encourage you to read through the NetTalk documention
            !       and to work through at least one scenario (found in the documentation)
            !
            !       We hope this example code helps you to use NetTalk and so you are allowed
            !       to copy or modify the code in this procedure as much as you like.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

Temp_Group           GROUP,PRE(tmp)
FileName             STRING(255)
TotalBackOrders      REAL
BackOrderCount       LONG
UseEuro              BYTE(0)
                     END
Message_Group        GROUP,PRE()
MessageText          STRING(64000)
MessageHtml          STRING(64000)
ToField              STRING(1024)
From                 STRING(80)
CC                   STRING(1024)
BCC                  STRING(1024)
Subject              STRING(255)
Server               STRING(80)
Port                 USHORT(25)
AttachmentList       STRING(1024)
EmbedList            STRING(1024)
ReplyTo              STRING(80)
Organization         STRING(80)
                     END
Misc_Group           GROUP,PRE()
FilesOpened          BYTE
tmp:recordcount      LONG
save_res_id          USHORT,AUTO
progress:thermometer BYTE
SendEmail            BYTE
                     END
Local_Group          GROUP,PRE(LOC)
OldestOrderDate      DATE
                     END
! our data
StartPos             long
EndPos               long
ParamPath            string (255)
Param1               string (255)
count                long
tempFileList         string (Net:StdEmailAttachmentListSize)
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Form
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Form
mo:SelectedField  Long
Window               WINDOW('Email Procedure'),AT(,,343,279),FONT('Tahoma',8,,FONT:regular),CENTER,TIMER(10),SYSTEM,GRAY,MAX,DOUBLE,IMM
                       SHEET,AT(4,4,336,244),USE(?Sheet1),SPREAD
                         TAB(' &General'),USE(?Tab1)
                           GROUP('Email Details'),AT(8,20,320,160),USE(?Group3),BOXED
                             STRING('Addresses format : Fred Smith <<fred@email.com>, Jane Smith <<jane@email.com>'),AT(16,28,304,8),USE(?String1),CENTER,FONT(,,COLOR:Green,)
                             PROMPT('From'),AT(12,44),USE(?ToField:Prompt:2)
                             ENTRY(@s80),AT(84,44,240,10),USE(From),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Subject'),AT(12,64),USE(?Subject:Prompt)
                             ENTRY(@s255),AT(84,64,240,10),USE(Subject),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Message'),AT(12,80),USE(?Subject:Prompt:2)
                             TEXT,AT(84,80,240,96),USE(MessageText),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           END
                           PROMPT('File Progress'),AT(8,188),USE(?Prompt1)
                           PROGRESS,USE(progress:thermometer),HIDE,AT(8,200,320,8),RANGE(0,100)
                           PROMPT('Email Progress'),AT(8,220),USE(?Prompt2)
                           PROGRESS,USE(?OurProgress),HIDE,AT(8,232,320,8),RANGE(0,100)
                         END
                       END
                       PANEL,AT(4,252,336,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Begin'),AT(224,256,56,16),USE(?Send),LEFT,ICON('MAIL.GIF')
                       BUTTON('Cancel'),AT(280,256,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       STRING(''),AT(8,260,128,8),USE(?Status),TRN,CENTER
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

!Local Data Classes
ThisEmailSend        CLASS(NetEmailSend)              !Generated by NetTalk Extension (Class Definition)
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED
Init                   PROCEDURE(uLong Mode=NET:SimpleClient),DERIVED
MessageSent            PROCEDURE(),DERIVED

                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte


EmailDefaults       File,Driver('BASIC'),Pre(EmailDef),NAME('RTBOMAIL.INI'),Create,Bindable,Thread
Record                  Record
ToField                 String(255)
From                    String(255)
Subject                 String(255)
MessageText             String(255)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
!        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
!    cancel# = 0
!    tmp:cancel = 0
!    accept
!        Case Event()
!            Of Event:Timer
!                Break
!            Of Event:CloseWindow
!                cancel# = 1
!                Break
!            Of Event:accepted
!                If Field() = ?ProgressCancel
!                    cancel# = 1
!                    Break
!                End!If Field() = ?Button1
!        End!Case Event()
!    End!accept
!    If cancel# = 1
!        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                tmp:cancel = 1
!            Of 2 ! &No Button
!        End!Case MessageEx
!    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
!    ?progress:pcttext{prop:text} = '100% Completed'
!    close(progresswindow)
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('TestEmailSend')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  Open(EmailDefaults)
  If Error()
      Create(EmailDefaults)
      Open(EmailDefaults)
  End !Error()
  
  Set(EmailDefaults)
  Next(EmailDefaults)
  If ~Error()
      From    = emaildef:From
      Subject = emaildef:Subject
      MessageText = emaildef:MessageText
  
  End !Error()
  Close(EmailDefaults)
  OPEN(Window)
  SELF.Opened=True
                                               ! Generated by NetTalk Extension (Start)
  ThisEmailSend.init()
  if ThisEmailSend.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! Load the users last settings
  !Server = getIni ('EmailSendDemo', 'Server', '', '.\netdemo.ini')
  !Port = getIni ('EmailSendDemo', 'Port', 25, '.\netdemo.ini')
  !From = getIni ('EmailSendDemo', 'From', '', '.\netdemo.ini')
  !ToField = getIni ('EmailSendDemo', 'ToField', '', '.\netdemo.ini')
  !CC = getIni ('EmailSendDemo', 'CC', '', '.\netdemo.ini')
  !BCC = getIni ('EmailSendDemo', 'BCC', '', '.\netdemo.ini')
  !Subject = getIni ('EmailSendDemo', 'Subject', '', '.\netdemo.ini')
  !ReplyTo = getIni ('EmailSendDemo', 'ReplyTo', '', '.\netdemo.ini')
  !Organization = getIni ('EmailSendDemo', 'Organization', '', '.\netdemo.ini')
  !MessageText = getIni ('EmailSendDemo', 'MessageText', '', '.\netdemo.ini')
  !MessageHTML = getIni ('EmailSendDemo', 'MessageHTML', '', '.\netdemo.ini')
  !AttachmentList = getIni ('EmailSendDemo', 'AttachmentList', '', '.\netdemo.ini')
  !EmbedList = getIni ('EmailSendDemo', 'EmbedList', '', '.\netdemo.ini')
  ThisEmailSend.ProgressControl = ?OurProgress
  
  ThisMakeover.SetWindow(Win:Form)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,?Sheet1)
  Window{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Store the users settings for next time
  Open(EmailDefaults)
  If Error()
      Create(EmailDefaults)
      Open(EmailDefaults)
  End !Error()
  
  Set(EmailDefaults)
  Next(EmailDefaults)
  If ~Error()
  
      emaildef:Subject    = Subject
      emaildef:MessageText    = MessageText
      emaildef:From           = From
      Put(EmailDefaults)
  Else !Error()
      emaildef:Subject    = Subject
      emaildef:MessageText    = MessageText
      emaildef:From           = From
      Add(EmailDefaults)
  
  End !Error()
  Close(EmailDefaults)
  setcursor
  
  ThisEmailSend.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Send
      ! set up and send csv file
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       You need to set up all the details of the server you want to
            !       connect to as well as the content of your email.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ?Send{Prop:Disable} = 1
      ?progress:thermometer{prop:hide} = 0
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      ThisEmailSend.Server = Clip(def:EmailServerAddress)
      ThisEmailSend.Port = def:EmailServerPort
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      
      
      recordstoprocess    = Records(glo:Queue)
      
      If func:Summary
          glo:File_Name   = Clip(glo:Path) & '\BKORDSUM.CSV'
          Remove(glo:File_Name)
      
          Access:EXPGEN.Open()
              Access:EXPGEN.Usefile()
      
              Clear(gen:Record)
                  gen:Line1   = 'Company Name,Account Number,Back Orders,Value'
                  !-------------------------------------------------------------
                  ! 16 Dec 2002 John LOG 153
                  ! We need to find the date of the oldest back order,
                  ! I have run an export to ASCII(Xport.csv) in 'Back Order Processing', however the date is not stated.
                  ! Please can you amend this so that the date of each part appears in a column.
                  gen:Line1   = CLIP(gen:Line1) &  ',Oldest Requested Date'
                  LOC:OldestOrderDate = ''
                  !-------------------------------------------------------------
              Access:EXPGEN.Insert()
          Access:EXPGEN.Close()
      End !func:Summary
      
      
      Loop x# = 1 To Records(glo:Queue)
          Get(glo:Queue,x#)
      !---Before Routine
      
      !---Insert Routine
          Do GetNextRecord2
          cancelcheck# += 1
          If cancelcheck# > (RecordsToProcess/100)
              Do cancelcheck
              If tmp:cancel = 1
                  Break
              End!If tmp:cancel = 1
              cancelcheck# = 0
          End!If cancelcheck# > 50
      
          SendEMail = FALSE
          tmp:UseEuro = 0
          Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
          sub:Account_Number = glo:Pointer
          If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
              !Found
              Access:TRADEACC.ClearKey(tra:Account_Number_Key)
              tra:Account_Number = sub:Main_Account_Number
              If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  !Found
                  If tra:Use_Sub_Accounts = 'YES'
                      If sub:EmailAddress <> ''
                          ThisEmailSend.ToList = sub:EmailAddress
                          SendEMail = TRUE
                      Else !If sub:EmailAddress <> ''
                          SendEMail = FALSE
                      End !If sub:EmailAddress <> ''
                  Else !If tra:Use_Sub_Accounts = 'YES'
                      If tra:EmailAddress <> ''
                          ThisEmailSend.ToList    = tra:EmailAddress
                          SendEMail = TRUE
                      Else !If tra:EmailAddress <> ''
                          SendEMail = FALSE
                      End !If tra:EmailAddress <> ''
                  End !If tra:Use_Sub_Accounts = 'YES'
                  If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                      If sub:EuroApplies
                          tmp:UseEuro = 1
                      End !If sub:EuroApplies.
                  Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                      If tra:EuroApplies
                          tmp:UseEuro = 1
                      End !If tra:EuroApplies
                  End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
              Else!If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:TRADEACC.TryFetch(tra:Account_Number_Key) = Level:Benign
          Else!If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:SUBTRACC.TryFetch(sub:Account_Number_Key) = Level:Benign
      
          glo:File_Name   = Clip(glo:Path) & '\' & Clip(StripNonAlpha(sub:Account_Number)) & '-' & Format(TODAY(),@d11) & '.CSV'
          tmp:FileName    = Clip(StripNonAlpha(sub:Account_Number)) & '-' & Format(TODAY(),@d11) & '.CSV'
      
          Remove(glo:File_Name)
          Access:EXPGEN.Open()
          Access:EXPGEN.Usefile()
      
          Clear(gen:Record)
          gen:line1   = CLIP(def:User_Name)&' - BACK ORDER SPARES REPORT'
          Access:EXPGEN.Insert()
      
          gen:line1   = ''
          Access:EXPGEN.Insert()
      
          gen:line1   = 'THE ITEMS LISTED BELOW ARE CURRENTLY ON BACKORDER'
          Access:EXPGEN.Insert()
      
          gen:line1   = ''
          Access:EXPGEN.Insert()
      
          !--------------------------------------------------------------------
          ! 11 Dec 2002 John
          ! CommunicAid Log 152 Date 11 Dec 2002
          ! When we run the back order export, please can we have an additional
          !   column stating the description of the part
          ! Also the date is not appearing. Ref problem Log139.
          !gen:Line1  = 'Company,Account Number,Order Number,Date Of Order,Part Number,            Quantity,Cost In Euros,Price,Total'
          gen:Line1   = 'Company,Account Number,Order Number,Date Requested,Part Number,Description,Quantity,Cost In Euros,Price,Total'
          Access:EXPGEN.Insert()
          !--------------------------------------------------------------------
      
          tmp:TotalBackOrders = 0
          tmp:BackOrderCount = 0
          tmp:recordcount = 0
          Save_res_ID = Access:RETSTOCK.SaveFile()
          Access:RETSTOCK.ClearKey(res:DespatchedKey)
          res:Despatched = 'PEN'
          Set(res:DespatchedKey,res:DespatchedKey)
          Loop
              If Access:RETSTOCK.NEXT()
                 Break
              End !If
              If res:Despatched <> 'PEN'      |
                  Then Break.  ! End If
              Access:RETSALES.ClearKey(ret:Ref_Number_Key)
              ret:Ref_Number = res:Ref_Number
              If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                  !Found
                  If ret:Account_Number <> glo:Pointer
                      Cycle
                  End !If ret:Account_Number <> glo:Pointer
      
              Else!If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
                  Cycle
              End!If Access:RETSALES.TryFetch(ret:Ref_Number_Key) = Level:Benign
      
              If ret:Invoice_Number <> ''
                  Access:INVOICE.Clearkey(inv:Invoice_Number_Key)
                  inv:Invoice_Number  = ret:Invoice_Number
                  If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                      !Found
                      If inv:UseAlternativeAddress
                          tmp:UseEuro = 2
                      End !If inv:UseAlternativeAddress
                  Else! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:INVOICE.Tryfetch(inv:Invoice_Number_Key) = Level:Benign
                  
              End !If ret:Invoice_Number <> ''
              tmp:recordcount+=1
              Clear(gen:Record)
                  !------------------------------------------------------------
                  gen:Line1   = Clip(sub:Company_Name)
                  gen:Line1   = Clip(gen:Line1) & ',' & Clip(sub:Account_Number)
                  ! Start Change 1764 BE(21/03/03)
                  !
                  ! the problem of the obscure 'VARIOUS' P/O code has been corrected in SBG01 but
                  ! this code change is to allow for incorrect coding in historical records
                  ! where back orders have been created from back orders prior to the
                  ! SBG01 fix.
                  !
                  !gen:Line1   = Clip(gen:Line1) & ',' & Clip(ret:Purchase_Order_Number)
                  IF (ret:Purchase_Order_Number <> 'VARIOUS') THEN
                      gen:Line1   = Clip(gen:Line1) & ',' & Clip(ret:Purchase_Order_Number)
                  ELSE
                      gen:Line1   = Clip(gen:Line1) & ',' & Clip(res:Purchase_Order_Number)
                  END
                  ! End Change 1764 BE(21/03/03)
                  gen:Line1   = Clip(gen:Line1) & ',' & Format(ret:Date_Booked,@d06)
                  !------------------------------------------------------------
                  gen:Line1   = Clip(gen:Line1) & ',' & Clip(res:Part_Number)
                  !------------------------------------------------------------
                  ! 11 Dec 2002 John
                  ! CommunicAid Log 152 Date 11 Dec 2002
                  ! When we run the back order export, please can we have an additional
                  !   column stating the description of the part
                  ! Also the date is not appearing. Ref problem Log139.
                  gen:Line1   = Clip(gen:Line1) & ',' & Clip(res:Description)
                  !------------------------------------------------------------
                  gen:Line1   = Clip(gen:Line1) & ',' & Clip(res:Quantity)
                  
                  Case tmp:UseEuro
                      Of 0
                          gen:Line1   = Clip(gen:Line1) & ',NO'
                          gen:Line1   = Clip(gen:Line1) & ',' & Clip(Round(res:Item_Cost,.01))
                          gen:Line1   = Clip(gen:Line1) & ',' & Clip(Round(res:Quantity * res:Item_Cost,.01))
                          tmp:TotalBackOrders += res:Quantity * res:Item_Cost
                      Of 1
                          gen:Line1   = Clip(gen:Line1) & ',YES'
                          gen:Line1   = Clip(gen:Line1) & ',' & Clip(Round(res:Item_Cost * def:EuroRate,.01))
                          gen:Line1   = Clip(gen:Line1) & ',' & Clip(Round((res:Quantity * res:Item_Cost) * def:EuroRate,.01))
                          tmp:TotalBackOrders += res:Quantity * res:Item_Cost * def:EuroRate
                      Of 2
                          gen:Line1   = Clip(gen:Line1) & ',YES'
                          gen:Line1   = Clip(gen:Line1) & ',' & Clip(Round(res:Item_Cost * inv:EuroExhangeRate,.01))
                          gen:Line1   = Clip(gen:Line1) & ',' & Clip(Round((res:Quantity * res:Item_Cost) * inv:EuroExhangeRate,.01))
                          tmp:TotalBackOrders += res:Quantity * res:Item_Cost * inv:EuroExhangeRate
                  End !Case tmp:UseEuro
                  !-------------------------------------------------------------
                  ! 16 Dec 2002 John LOG 153
                  ! We need to find the date of the oldest back order,
                  ! I have run an export to ASCII(Xport.csv) in 'Back Order Processing', however the date is not stated.
                  ! Please can you amend this so that the date of each part appears in a column.
                  !
                  ! Start Change 1764 BE(17/03/03)
                  !IF ret:Date_Booked > LOC:OldestOrderDate
                  IF ((tmp:recordcount = 1) OR (ret:Date_Booked < LOC:OldestOrderDate)) THEN
                  ! End Change 1764 BE(17/03/03)
                      LOC:OldestOrderDate = ret:Date_Booked
                  END !IF
                  !-------------------------------------------------------------
              Access:EXPGEN.Insert()
              
              tmp:BackOrderCount  += 1
          End !Loop
          Access:RETSTOCK.RestoreFile(Save_res_ID)
      
          Access:EXPGEN.Close()
          IF tmp:recordcount = 0 and func:generate_all = FALSE
            REMOVE(EXPGEN)
            SendEMail = FALSE
          END
      
          If SendEMail = TRUE and func:Email <> 0
              ThisEmailSend.ccList = ''
              ThisEmailSend.bccList = ''
              ThisEmailSend.From = From
              ThisEmailSend.Subject = Subject
              ThisEmailSend.ReplyTo = From
              ThisEmailSend.Organization = def:User_Name
              ThisEmailSend.References = ''     
              ThisEmailSend.AttachmentList = Clip(glo:File_Name)
              ThisEmailSend.EmbedList = ''
              ThisEmailSend.SetRequiredMessageSize (0, Len(Clip(MessageText)), len(clip(MessageHTML)))
              if ThisEmailSend.Error = 0
                ThisEmailSend.MessageText = Clip(MessageText)
                setcursor (CURSOR:WAIT)
                display()
                    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    !
                    ! NOTE TO PROGRAMMERS
                    !       The SendMail() method will queue your message and start
                    !       sending it.
                    !
                    !       This is an Asynchronous function. This means it returns to you
                    !       but your email has not finished being sent yet. You can send emails
                    !       one after another and they are just kept in the queue and sent out as
                    !       fast as the Mail Server will take them.
                    !
                    !       The MailSent() method is called each time an email is successfully sent
                    !       to the SMTP server.
                    !
                    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                ThisEmailSend.SendMail(NET:EMailMadeFromPartsMode)  ! Put email in queue and start sending it now
                if ThisEmailSend.error <> 0
                  ! Handle Send Error (This error is a connection error - not a sending error)
                  ?status{prop:text} = 'Could not connect to email server.'
                else
                  if records (ThisEmailSend.DataQueue) > 0
                    ?status{prop:text} = 'Sending ' & records (ThisEmailSend.DataQueue) & ' email(s)'
                  else
                    ?status{prop:text} = ''
                  end
                  REMOVE(glo:File_Name)
                end
                setcursor
                display()
              end
      
          End !If SendEMail
          !Print the summary export, if required.
          If func:Summary
              glo:File_Name   = Clip(glo:Path) & '\BKORDSUM.CSV'
              Access:EXPGEN.Open()
              Access:EXPGEN.Usefile()
              IF tmp:recordcount > 0
                Clear(gen:Record)
                gen:Line1   = Clip(sub:Company_Name)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(sub:Account_Number)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(tmp:BackOrderCount)
                gen:Line1   = Clip(gen:Line1) & ',' & Clip(Round(tmp:TotalBackOrders,.01))
                  !-------------------------------------------------------------
                  ! 16 Dec 2002 John LOG 153
                  ! We need to find the date of the oldest back order,
                  ! I have run an export to ASCII(Xport.csv) in 'Back Order Processing', however the date is not stated.
                  ! Please can you amend this so that the date of each part appears in a column.
                  !
                gen:Line1   = Clip(gen:Line1) & ',' & Format(LOC:OldestOrderDate, @d06)
                 !-------------------------------------------------------------
                Access:EXPGEN.Insert()
              END
              Access:EXPGEN.Close()
          End !If func:Summary
      End !x# = 1 To Records(glo:Queue)
      
      !---After Routine
      Do EndPrintRun
      
      IF func:Email = 0
          Case MessageEx('Export Files created.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          ?Send{prop:disable} = 0
          ?progress:thermometer{prop:hide} = 1
      End !func:Email = 0
      
      
      !ThisEmailSend.ToList = ToField
      !ThisEmailSend.ccList = CC
      !ThisEmailSend.bccList = BCC
      !ThisEmailSend.From = From
      !ThisEmailSend.Subject = Subject
      !ThisEmailSend.ReplyTo = ReplyTo
      !ThisEmailSend.Organization = Organization
      !ThisEmailSend.References = ''     ! Used for replies e.g. '<<00cd01c02dde$765a6880$0802a8c0@spiff> <<00dc01c02de0$35fbea00$0802a8c0@spiff>'
      !ThisEmailSend.AttachmentList = AttachmentList
      !ThisEmailSend.EmbedList = EmbedList     ! New 6/3/2001
      !      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !      !
      !      ! NOTE TO PROGRAMMERS
      !      !       You must call the SetRequiredMessageSize before
      !      !       populating <object>.MessageText, MessageHTML or WholeMessage
      !      !
      !      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !ThisEmailSend.SetRequiredMessageSize (0, len(clip(MessageText)), len(clip(MessageHTML))) 
      !if ThisEmailSend.Error = 0
      !  ThisEmailSend.MessageText = MessageText
      !  if len(clip(MessageHTML)) > 0
      !    ThisEmailSend.MessageHTML = MessageHTML
      !  end
      !  setcursor (CURSOR:WAIT)
      !  display()
      !      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !      !
      !      ! NOTE TO PROGRAMMERS
      !      !       The SendMail() method will queue your message and start
      !      !       sending it.
      !      !
      !      !       This is an Asynchronous function. This means it returns to you
      !      !       but your email has not finished being sent yet. You can send emails
      !      !       one after another and they are just kept in the queue and sent out as
      !      !       fast as the Mail Server will take them.
      !      !
      !      !       The MailSent() method is called each time an email is successfully sent
      !      !       to the SMTP server.
      !      !
      !      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !  ThisEmailSend.SendMail(NET:EMailMadeFromPartsMode)  ! Put email in queue and start sending it now
      !  if ThisEmailSend.error <> 0
      !    ! Handle Send Error (This error is a connection error - not a sending error)
      !    ?status{prop:text} = 'Could not connect to email server.'
      !  else
      !    if records (ThisEmailSend.DataQueue) > 0
      !      ?status{prop:text} = 'Sending ' & records (ThisEmailSend.DataQueue) & ' email(s)'
      !    else
      !      ?status{prop:text} = ''
      !    end
      !  end
      !  setcursor
      !  display()
      !end
      !
    OF ?Close
      post (event:closewindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisEmailSend.TakeEvent()                 ! Generated by NetTalk Extension
    ThisMakeover.TakeEvent(Win:Form,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Form,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       When the user tries to close the window, we test to
            !       see if there are still emails being sent.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if records (ThisEmailSend.DataQueue) > 0
        if Message ('The email is still being sent.|Are you sure you want to quit?','NetTalk Send Email ',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No) = Button:No
          cycle
        end
      end
    OF EVENT:Timer
      ThisEmailSend.CalcProgress()
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


ThisEmailSend.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
  PARENT.ErrorTrap(errorStr,functionName)
  ?status{prop:text} = 'An error occurred.'
  display


ThisEmailSend.Init PROCEDURE(uLong Mode=NET:SimpleClient)


  CODE
  PARENT.Init(Mode)
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       You can tell the object to use the different Mime Formats
            !       for encoding text or HTML.
            !       The options are:
            !         'quoted-printable'  - (Microsoft default) all characters are trasmitted and sent in a way that is humanly friendly
            !         '7bit'   - 7bit characters are sent
            !         '8bit'   - all 8bit characters are sent, some SMTP servers may choke on these characters though
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  self.OptionsMimeTextTransferEncoding = 'quoted-printable'           ! '7bit', '8bit' or 'quoted-printable'
  self.OptionsMimeHtmlTransferEncoding = 'quoted-printable'           ! '7bit', '8bit' or 'quoted-printable'
  
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       You can set other settings by uncommenting
            !       the following lines
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  !self.OptionsKeepConnectionOpen = true
  self.ReconnectAfterXMsgs = 20
  self.XMailer = 'NetDemo Application'
  !self.SuppressErrorMsg = true
  


ThisEmailSend.MessageSent PROCEDURE


  CODE
  PARENT.MessageSent
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       The number of records still to send
            !       = records (ThisEmailSend.DataQueue) - 1
            !
            !       The email just sent is still in the queue, so
            !       that you can access it's properties if you need.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    if records (ThisEmailSend.DataQueue) > 1
      ?status{prop:text} = 'Sending ' & (records (ThisEmailSend.DataQueue) - 1) & ' email(s)'
    else
      ?status{prop:text} = ''
      Case MessageEx('Export Files created and/or Emails sent.','ServiceBase 2000',|
                     'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
          Of 1 ! &OK Button
      End!Case MessageEx
      ?progress:thermometer{prop:hide} = 1
      ?send{prop:disable} = 0
    end


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

