

   MEMBER('modelpricingexport.clw')                   ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('MODEL001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

locImportFile        STRING(255)
window               WINDOW('Model Pricing Import/Export Routines'),AT(,,271,172),FONT('Tahoma',8,,),COLOR(COLOR:White),CENTER,GRAY,DOUBLE
                       BUTTON('Close'),AT(212,150,56,16),USE(?Close)
                       SHEET,AT(4,66,264,80),USE(?Sheet1:2),COLOR(0F0F0F0H),SPREAD
                         TAB('Import Model Tier/Prices'),USE(?Tab2)
                           PROMPT('Update Model''s tier/prices from selected CSV File:'),AT(57,84),USE(?Prompt3)
                           PROMPT('CSV Filename'),AT(12,102),USE(?locImportFile:Prompt)
                           TEXT,AT(76,102,172,10),USE(locImportFile),COLOR(COLOR:White),SINGLE
                           BUTTON('...'),AT(252,102,12,10),USE(?LookupFile)
                           BUTTON('Load CSV File && Update Models'),AT(72,120,132,16),USE(?btnImport)
                         END
                       END
                       SHEET,AT(4,4,264,60),USE(?Sheet1),COLOR(0F0F0F0H),SPREAD
                         TAB('Export Model Tier/Prices'),USE(?Tab1)
                           PROMPT('Create an CSV file containing all Models and their tiers/prices.'),AT(37,22),USE(?Prompt2)
                           BUTTON('Create Export File'),AT(68,40,132,16),USE(?btnExport)
                         END
                       END
                     END

ProgressBar     LONG

ProgressWindow WINDOW('Processing....'),AT(,,295,27),FONT('Tahoma',8,,),CENTER,TIMER(1)
       PROGRESS,USE(ProgressBar),AT(4,6,288,14),RANGE(0,200)
     END
Prog        Class
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(),Byte
ProgressText       Procedure(String func:String)
ProgressFinish     Procedure()
            End
! moving bar window
prog:RecordsToProcess   Long,Auto
prog:RecordsPerCycle    Long,Auto
prog:RecordsProcessed   Long,Auto
prog:RecordsThisCycle   Long,Auto
prog:PercentProgress    Byte
prog:Cancel             Byte
prog:ProgressThermometer    Byte


prog:thermometer byte
prog:SkipRecords        Long
omit('***',ClarionetUsed=0)
CNprog:thermometer byte
CNprog:Text         String(60)
CNprog:pctText      String(60)
***z
progwindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(prog:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?prog:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?prog:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('cancel.gif')
     END

omit('***',ClarionetUsed=0)

!static webjob window
CN:progwindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(cnprog:Text),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?cnprog:Prompt),FONT(,14,,FONT:bold)
     END
***
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup3          SelectFileClass
    MAP
ImportProcess   PROCEDURE()
ExportProcess PROCEDURE()
    END ! MAP

locSavePath CSTRING(255)
locExportFile CSTRING(255),STATIC
ExportFile File,DRIVER('BASIC'),PRE(expfil),Name(locExportFile),CREATE,BINDABLE,THREAD
RECORD RECORD
Manufacturer    STRING(30)
ModelNumber    STRING(30)
Tier    STRING(30)
PostalRepairCharge STRING(30)
ExchangeCharge    STRING(30)
BERCharge    STRING(30)
        END
        END

locExportFolder CSTRING(255)
locArchiveFolder CSTRING(255)
locCountRecords    LONG()

locLogFile CSTRING(255),STATIC
LogFile File,DRIVER('ASCII'),PRE(logfil),Name(locLogFile),CREATE,BINDABLE,THREAD
RECORD RECORD
Line    STRING(1000)
        END
        END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

Prog:nextrecord      routine
    Yield()
    prog:recordsprocessed += 1
    prog:recordsthiscycle += 1
    If prog:percentprogress < 100
        prog:percentprogress = (prog:recordsprocessed / prog:recordstoprocess)*100
        If prog:percentprogress > 100
            prog:percentprogress = 100
        End
        If prog:percentprogress <> prog:thermometer then
            prog:thermometer = prog:percentprogress

Omit('***',ClarionetUsed=0)
            If ClarionetServer:Active()
                cnprog:thermometer = prog:thermometer
            Else ! If ClarionetServer:Active()
***
                ?prog:pcttext{prop:text} = format(prog:percentprogress,@n3) & '% Completed'
Omit('***',ClarionetUsed=0)
            End ! If ClarionetServer:Active()
***
        End
    End
    Display()

prog:cancelcheck         routine
    cancel# = 0
    prog:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            prog:Cancel = 1
        Of Button:No
        End !CASE
    End!If cancel# = 1


prog:Finish         routine
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        CNprog:thermometer = 100
    Else ! If ClarionetServer:Active()
***
        prog:thermometer = 100
        ?prog:pcttext{prop:text} = '100% Completed'
        display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Close
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  Access:MODELNUM.UseFile
  Access:SIDRRCT.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  FileLookup3.Init
  FileLookup3.Flags=BOR(FileLookup3.Flags,FILE:LongName)
  FileLookup3.SetMask('ImportFile','ModelPricing*.csv')
  FileLookup3.WindowTitle='''Choose Import File'''
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFile
      ThisWindow.Update
      locImportFile = FileLookup3.Ask(1)
      DISPLAY
    OF ?btnImport
      ThisWindow.Update
      IF (locImportFile <> '')
          BEEP(BEEP:SystemQuestion)  ;  YIELD()
          CASE MESSAGE('Read CSV File.'&|
              '|'&|
              '|Are you sure?','ServiceBase',|
                         ICON:Question,'&Yes|&No',2) 
          OF 1 ! &Yes Button
              ImportProcess()
          OF 2 ! &No Button
          END!CASE MESSAGE
      ELSE
          SELECT(?locImportFile)
      END ! IF
    OF ?btnExport
      ThisWindow.Update
      BEEP(BEEP:SystemQuestion)  ;  YIELD()
      CASE MESSAGE('Create export file.'&|
          '|'&|
          '|Are you sure?','ServiceBase',|
                     ICON:Question,'&Yes|&No',2) 
      OF 1 ! &Yes Button
          ExportProcess()
      OF 2 ! &No Button
      END!CASE MESSAGE
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ImportProcess    PROCEDURE()
bytesSoFar    LONG
updateRecord LONG
qFailure    QUEUE(),pRE(qFailure)
Manufacturer STRING(30)
ModelNumber STRING(30)
Reason     STRING(100)
            END ! IF

i LONG
locRecordCount LONG
totalRecords    LONG
    CODE
        locExportFile = locImportFile

        OPEN(ExportFile)
        IF (ERRORCODE())
            BEEP(BEEP:SystemHand)  ;  YIELD()
            CASE MESSAGE('Unable to access the selected CSV File.','ServiceBase',|
                           ICON:Hand,'&OK',1) 
            OF 1 ! &OK Button
            END!CASE MESSAGE
            RETURN
        END ! IF

        OPEN(ProgressWindow)
        DISPLAY()

        totalRecords = 0
        SETCURSOR(CURSOR:Wait)
        SET(ExportFile)
        LOOP
            NEXT(ExportFile)
            IF (ERRORCODE())
                BREAK
            END ! IF
            totalRecords += 1
        END ! LOOP
        SETCURSOR()
        ?ProgressBar{Prop:RangeHigh} = totalRecords

        first# = 1

        SET(ExportFile)
        LOOP
            NEXT(ExportFile)
            IF (ERRORCODE())
                BREAK
            END ! IF

            IF (first# = 1)
                ! SKIP First Line
                first# = 0
                CYCLE
            END ! IF

            bytesSoFar += 1
            ?ProgressBar{prop:Progress} = bytesSoFar

            !Does the model number exist?
            Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
            mod:Manufacturer = expfil:Manufacturer
            mod:Model_Number = expfil:ModelNumber
            IF (Access:MODELNUM.TryFetch(mod:Manufacturer_Key))
                ! ERROR
                qFailure.Manufacturer = expfil:Manufacturer
                qFailure.ModelNumber = expfil:ModelNumber
                qFailure.Reason = '"Could not find Manufacturer/Model Number"'
                ADD(qFailure)
                CYCLE
            END !IF

            updateRecord = FALSE
            ! Does the Tier exist?
            IF (expfil:Tier = 0)
                ! IF the tier is zero, then assume they want to blank it. (A guess)
                IF (mod:RetailRepairTier <> expfil:Tier)
                    mod:RetailRepairTier = expfil:Tier
                    updateRecord = TRUE
                END ! IF
            ELSE ! IF
                Access:SIDRRCT.ClearKey(rrct:TierKey)
                rrct:Tier = expfil:Tier
                IF (Access:SIDRRCT.TryFetch(rrct:TierKey) = Level:Benign)
                    IF (mod:RetailRepairTier <> expfil:Tier)
                        mod:RetailRepairTier = rrct:Tier
                        updateRecord = TRUE
                    END ! IF
                ELSE ! IF
                    qFailure.Manufacturer = expfil:Manufacturer
                    qFailure.ModelNumber = expfil:ModelNumber
                    qFailure.Reason = '"Tier does not exist: ' & CLIP(expfil:Tier) & '"'
                    ADD(qFailure)
                    CYCLE
                END !IF
            END ! IF
            IF (mod:ExchangeCharge <> expfil:ExchangeCharge)
                mod:ExchangeCharge = expfil:ExchangeCharge
                updateRecord = TRUE
            END ! IF
            IF (mod:PostalRepairCharge <> expfil:PostalRepairCharge)
                mod:PostalRepairCharge = expfil:PostalRepairCharge
                updateRecord = TRUE
            END ! IF
            IF (mod:BERCharge <> expfil:BERCharge)
                mod:BERCharge = expfil:BERCharge
                updateRecord = TRUE
            END ! IF

            IF (updateRecord = TRUE)
                
                IF (Access:MODELNUM.TryUpdate())
                    qFailure.Manufacturer = expfil:Manufacturer
                    qFailure.ModelNumber = expfil:ModelNumber
                    qFailure.Reason = '"Could not save record"'
                    ADD(qFailure)
                    CYCLE
                ELSE ! IF
                    locRecordCount += 1
                END ! IF
            END ! IF


        END ! LOOP

        CLOSE(ProgressWindow)

        CLOSE(ExportFile)


        ! Create Folders
        locLogFile = PATH() & '\Logs'
        IF (NOT EXISTS(locLogFile))
            IF ~(MkDir(locLogFile))
            END ! IF
        END ! IF

        locLogFile = CLIP(locLogFile) & '\ModelPricing'

        IF (NOT EXISTS(locLogFile))
            IF ~(MkDir(locLogFile))
            END ! IF
        END ! IF


        locLogFile = CLIP(locLogFile) & '\ModelPricing' & FORMAT(DAY(TODAY()),@n02) & FORMAT(MONTH(TODAY()),@n02) & YEAR(TODAY()) & '.log'

        CREATE(LogFile)
        OPEN(LogFile)

        logfil:Line = 'Import File: ' & locImportFile
        ADD(LogFile)

        logfil:Line = ''
        ADD(LogFile)

        logfil:Line = 'Models Updated Successfully: ' & locRecordCount
        ADD(LogFile)

        IF (RECORDS(qFailure) = 0)
            logfil:Line = ''
            ADD(LogFile)
            logfil:Line = 'No Errors.'
            ADD(LogFile)
        ELSE ! IF
            logfil:Line = ''
            ADD(LogFile)
            logfil:Line = '======== Errors ======='
            ADD(LogFile)
            logfil:Line = ''
            ADD(LogFile)

            LOOP i = 1 TO RECORDS(qFailure)
                GET(qFailure,i)
                logfil:Line = CLIP(qFailure.Manufacturer) & ' ' & CLIP(qFailure.ModelNumber) & ': ' & CLIP(qFailure.Reason)
                ADD(LogFile)
            END ! IF
        END ! IF

        CLOSE(LogFile)


        BEEP(BEEP:SystemExclamation)  ;  YIELD()
        CASE MESSAGE('File Imported.'&|
            '|'&|
            '|Models Updated Successfully: ' & locRecordCount&|
            '|'&|
            '|Errors: ' & RECORDS(qFailure),'ServiceBase',|
                       ICON:Exclamation,'&Open Log File|&Close',2) 
        OF 1 ! &Open Log File Button
            RUN('EXPLORER.EXE ' & CLIP(locLogFile))
        OF 2 ! &Close Button
        END!CASE MESSAGE
ExportProcess    PROCEDURE()
    CODE
        ! Create Folders
        locExportFolder = PATH() & '\Export'
        IF (NOT EXISTS(locExportFolder))
            IF ~(MkDir(locExportFolder))
            END ! IF
        END ! IF

        locExportFolder = CLIP(locExportFolder) & '\ModelPricing'

        IF (NOT EXISTS(locExportFolder))
            IF ~(MkDir(locExportFolder))
            END ! IF
        END ! IF

        locArchiveFolder = PATH() & '\Archive'
        IF (NOT EXISTS(locArchiveFolder))
            IF ~(MkDir(locArchiveFolder))
            END ! IF
        END ! IF

        locArchiveFolder = CLIP(locArchiveFolder) & '\ModelPricingExport'

        IF (NOT EXISTS(locArchiveFolder))
            IF ~(MkDir(locArchiveFolder))
            END ! IF
        END ! IF

        locExportFile = CLIP(locExportFolder) & '\ModelPricing' & FORMAT(DAY(TODAY()),@n02) & FORMAT(MONTH(TODAY()),@n02) & YEAR(TODAY()) & '.csv'

        IF (EXISTS(locExportFile))
            BEEP(BEEP:SystemExclamation)  ;  YIELD()
            CASE MESSAGE('An export file for today already exists. Do you wish to overwite it?','ServiceBase',|
                           ICON:Exclamation,'&Yes|&No',2) 
            OF 1 ! &Yes Button
            OF 2 ! &No Button
                RETURN
            END!CASE MESSAGE
        END ! IF
        REMOVE(locExportFile)

        CREATE(ExportFile)
        IF (ERRORCODE())
            BEEP(BEEP:SystemHand)  ;  YIELD()
            CASE MESSAGE('Unable to create export file.||' & ERROR(),'ServiceBase',|
                           ICON:Hand,'&OK',1) 
            OF 1 ! &OK Button
            END!CASE MESSAGE
            RETURN
        END ! IF

        OPEN(ProgressWindow)
        DISPLAY()
        ?ProgressBar{Prop:RangeHigh} = RECORDS(MODELNUM)

        OPEN(ExportFile)

        expfil:Manufacturer = 'Manufacturer'
        expfil:ModelNumber = 'Model Number'
        expfil:Tier = 'Tier'
        expfil:PostalRepairCharge = 'Postal Repair Charge'
        expfil:ExchangeCharge = 'Exchange Charge'
        expfil:BERCharge = 'BER Charge'
        ADD(ExportFile)

        Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
        mod:Manufacturer = ''
        mod:Model_Number = ''
        SET(mod:Manufacturer_Key,mod:Manufacturer_Key)
        LOOP UNTIL Access:MODELNUM.Next() <> Level:Benign
            CLEAR(expfil:Record)
            expfil:Manufacturer = mod:Manufacturer
            expfil:ModelNumber = mod:Model_Number
            expfil:Tier = mod:RetailRepairTier
            expfil:PostalRepairCharge = LEFT(FORMAT(mod:PostalRepairCharge,@n_14.2))
            expfil:ExchangeCharge = LEFT(FORMAT(mod:ExchangeCharge,@n_14.2))
            expfil:BERCharge = LEFT(FORMAT(mod:BERCharge,@n_14.2))
            ADD(ExportFile)
            locCountRecords += 1

            ?ProgressBar{Prop:Progress} = locCountRecords

        END ! LOOP

        CLOSE(ExportFile)

        CLose(ProgressWindow)

        IF (locCountRecords = 0)
            REMOVE(locExportFile)
        ELSE
            ! Copy final file to archive folder
            locArchiveFolder = CLIP(locArchiveFolder) & '\ModelPricingExportBackup' & FORMAT(DAY(TODAY()),@n02) & FORMAT(MONTH(TODAY()),@n02) & YEAR(TODAY()) & '.csv'
            REMOVE(locArchiveFolder)
            COPY(locExportFile,locArchiveFolder)
        END ! IF


        BEEP(BEEP:SystemAsterisk)  ;  YIELD()
        CASE MESSAGE('Export Completed.','ServiceBase',|
                       ICON:Asterisk,'&OK',1) 
        OF 1 ! &OK Button
        END!CASE MESSAGE
Prog.ProgressSetup        Procedure(Long func:Records)
Code
    prog:SkipRecords = 0

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(CN:ProgWindow)
    Else ! If ClarionetServer:Active()
***
        Open(ProgWindow)
        Prog.ResetProgress(func:Records)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    prog:recordspercycle         = 25
    prog:recordsprocessed        = 0
    prog:percentprogress         = 0
    prog:thermometer    = 0
    prog:recordstoprocess    = func:Records
    prog:recordstoprocess   = INT(func:Records/100)

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:pcttext{prop:text} = '0% Completed'
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.InsideLoop         Procedure()
Code
    prog:SkipRecords += 1
    If prog:SkipRecords < 100
        Return 0
    Else
        prog:SkipRecords = 0
    End

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Do Prog:NextRecord
        Do Prog:CancelCheck
        If Prog:Cancel = 1
            Return 1
        End !If Prog:Cancel = 1
        Return 0
Omit('***',ClarionetUsed=0)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:UserString{prop:Text} = Clip(func:String)
        Display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Do Prog:Finish
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(CN:progwindow)
    Else ! If ClarionetServer:Active()
***
        close(progwindow)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

