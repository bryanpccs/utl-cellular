

   MEMBER('celrapmu.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('CELRA003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA005.INC'),ONCE        !Req'd for module callout resolution
                     END


Multiple_Job_Update_Wizard PROCEDURE                  !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Job_Number_Pointer            LIKE(GLO:Job_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::25:TAGFLAG         BYTE(0)
DASBRW::25:TAGMOUSE        BYTE(0)
DASBRW::25:TAGDISPSTATUS   BYTE(0)
DASBRW::25:QUEUE          QUEUE
Job_Number_Pointer            LIKE(GLO:Job_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
FilesOpened          BYTE
RepairErrorText      STRING(255)
tmpCheckChargeableParts BYTE(0)
TotalPartsCost       REAL
save_par_id          USHORT
save_wpr_id          USHORT
Audit_Notes          STRING(255)
Print_Despatch_Note_Temp STRING(30)
time_Completed_temp  STRING(20)
check_for_bouncers_temp BYTE
saved_ref_number_temp REAL
saved_esn_temp       STRING(16)
saved_msn_temp       STRING(16)
difference_temp      BYTE
job_count_temp       BYTE
Charge_Type_temp     STRING(30)
charge_type_tick_temp STRING('N')
warranty_charge_type_tick_temp STRING('N')
Warranty_Charge_Type_temp STRING(30)
QA_Passed_Temp       STRING('NO {1}')
QA_Rejected_Temp     STRING('NO {1}')
QA_Second_Passed_Temp STRING('NO {1}')
same_model_temp      BYTE(False)
use_fault_codes_temp BYTE(False)
manufacturer_temp    STRING(30)
TabNumber            BYTE(1)
auto_invoice_temp    STRING(1)
Invoice_Text_Tick_Temp STRING(1)
Engineers_Notes_Tick_Temp STRING(1)
Add_Spares_Tick_Temp STRING(1)
Labour_Cost_Tick_Temp STRING(1)
Auto_Despatch_Tick_Temp STRING(1)
engineer_tick_temp   STRING(1)
location_tick_temp   STRING(1)
status_tick_temp     STRING(1)
Fault_Codes_Tick_Temp STRING(1)
engineer_temp        STRING(3)
location_temp        STRING(30)
invoice_text_temp    STRING(10000)
engineers_notes_temp STRING(10000)
Labour_Cost_Temp     REAL
Labour_Cost_Warranty_temp REAL
labour_cost_warranty_tick_temp STRING(1)
status_temp          STRING(30)
batch_number_tick_temp STRING('N')
batch_number_temp    REAL
Fault_Codes1         GROUP,PRE()
Fault_Code1          STRING(30)
Fault_Code2          STRING(30)
Fault_Code3          STRING(30)
Fault_Code4          STRING(30)
Fault_Code5          STRING(30)
Fault_Code6          STRING(30)
                     END
Fault_Codes2         GROUP,PRE()
Fault_Code7          STRING(30)
Fault_Code8          STRING(30)
Fault_Code9          STRING(30)
Fault_Code10         STRING(30)
Fault_Code11         STRING(30)
Fault_Code12         STRING(30)
                     END
Part_Details_Group   QUEUE,PRE(qpar)
Part_Number          STRING(30)
Description          STRING(30)
mark                 BYTE
viewposition         STRING(1024)
                     END
Model_Number_temp    STRING(30)
model_number1_temp   STRING(30)
model_number2_temp   STRING(30)
status_location_temp STRING(30)
status_location1_temp STRING(30)
status_location2_temp STRING(30)
DisplayString        STRING(255)
tag_temp             STRING(1)
engineer_name_temp   STRING(30)
update_stock_module_temp STRING(1)
tag2_temp            STRING(1)
in_repair_tick_temp  STRING(1)
On_Test_Tick_Temp    STRING(1)
authority_number_tick_temp STRING(1)
authority_number_temp STRING(30)
auto_complete_tick_temp STRING(1)
date_completed_temp  DATE
advance_payment_tick_temp STRING(1)
advance_payment_temp REAL
repair_type_tick_temp STRING('N')
Repair_Type_Temp     STRING(30)
over_location_tick   STRING('N')
over_authority_number_tick STRING('N')
over_engineer_tick   STRING('N')
over_advance_payment_tick STRING('N')
over_engineers_notes_tick STRING('N')
over_invoice_Text_tick STRING('N')
over_chargeable_tick STRING('N')
over_warranty_tick   STRING('N')
over_repair_type_tick STRING('N')
over_in_repair_tick  STRING('N')
over_on_test_tick    STRING('N')
over_date_completed_tick STRING('N')
over_qa_passed_tick  STRING('N')
over_qa_rejected_tick STRING('N')
over_qa_second_check_tick STRING('N')
over_despatched_tick STRING('N')
over_fault_codes_tick STRING('N')
workshop_tick_temp   STRING('N')
over_workshop_tick_temp STRING('N')
repair_type_warranty_tick_temp STRING(1)
Repair_Type_Warranty_Temp STRING(30)
over_repair_type_warranty_temp STRING(1)
tmp:OverwriteFaultCode1 BYTE(0)
tmp:OverwriteFaultCode2 BYTE(0)
tmp:OverwriteFaultCode3 BYTE(0)
tmp:OverwriteFaultCode4 BYTE(0)
tmp:OverwriteFaultCode5 BYTE(0)
tmp:OverwriteFaultCode6 BYTE(0)
tmp:OverwriteFaultCode7 BYTE(0)
tmp:OverwriteFaultCode8 BYTE(0)
tmp:OverwriteFaultCode9 BYTE(0)
tmp:OverwriteFaultCode10 BYTE(0)
tmp:OverwriteFaultCode11 BYTE(0)
tmp:OverwriteFaultCode12 BYTE(0)
tmp:SelectJobsType   BYTE(0)
tmp:ImportFile       STRING(255),STATIC
MaxTabs              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?status_location1_temp
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?model_number2_temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?status_location2_temp
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?model_number1_temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB10::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
FDCB37::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
FDCB39::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
FDCB2::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
BRW4::View:Browse    VIEW(PARTSTMP)
                       PROJECT(partmp:Part_Number)
                       PROJECT(partmp:Description)
                       PROJECT(partmp:record_number)
                       PROJECT(partmp:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
partmp:Part_Number     LIKE(partmp:Part_Number)       !List box control field - type derived from field
partmp:Description     LIKE(partmp:Description)       !List box control field - type derived from field
partmp:record_number   LIKE(partmp:record_number)     !Primary key field - type derived from field
partmp:Ref_Number      LIKE(partmp:Ref_Number)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Current_Status)
                       PROJECT(job:Batch_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Current_Status     LIKE(job:Current_Status)       !List box control field - type derived from field
job:Batch_Number       LIKE(job:Batch_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW24::View:Browse   VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Current_Status)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tag2_temp              LIKE(tag2_temp)                !List box control field - type derived from local data
tag2_temp_Icon         LONG                           !Entry's icon ID
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Current_Status     LIKE(job:Current_Status)       !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW31::View:Browse   VIEW(WPARTTMP)
                       PROJECT(wartmp:Part_Number)
                       PROJECT(wartmp:Description)
                       PROJECT(wartmp:record_number)
                       PROJECT(wartmp:Ref_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
wartmp:Part_Number     LIKE(wartmp:Part_Number)       !List box control field - type derived from field
wartmp:Description     LIKE(wartmp:Description)       !List box control field - type derived from field
wartmp:record_number   LIKE(wartmp:record_number)     !Primary key field - type derived from field
wartmp:Ref_Number      LIKE(wartmp:Ref_Number)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Window               WINDOW('Multiple Job Update Wizard'),AT(,,515,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,MDI,IMM
                       PROMPT('Welcome To The Multiple Job Update Wizard'),AT(12,8),USE(?Prompt1),FONT(,14,COLOR:Navy,FONT:bold)
                       SHEET,AT(4,4,508,312),USE(?Sheet1),SPREAD
                         TAB('Page 1'),USE(?Tab1)
                           PROMPT('Tick The Fields To Update'),AT(104,36),USE(?Prompt2),FONT(,12,,FONT:bold)
                           PROMPT('Some jobs may contain data in the selected fields. If you wish to overwrite the ' &|
   'existing information, tick the ''Overwrite'' box.'),AT(104,52,272,20),USE(?Prompt27)
                           PROMPT('Tick To Update Fields'),AT(104,72,96,12),USE(?Prompt26),FONT(,,,FONT:bold+FONT:underline)
                           CHECK('Workshop'),AT(104,84),USE(workshop_tick_temp),VALUE('Y','N')
                           CHECK,AT(348,85),USE(over_workshop_tick_temp),HIDE,VALUE('Y','N')
                           PROMPT('Overwrite'),AT(332,72,40,8),USE(?Prompt25),FONT(,,,FONT:bold+FONT:underline)
                           CHECK('Internal Location'),AT(104,100),USE(location_tick_temp),VALUE('Y','N')
                           BUTTON,AT(192,100,10,10),USE(?Lookup_Internal_Location),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(208,100,124,10),USE(location_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK,AT(348,100),USE(over_location_tick),VALUE('Y','N')
                           CHECK('Authority Number'),AT(104,116),USE(authority_number_tick_temp),VALUE('Y','N')
                           ENTRY(@s30),AT(208,116,124,10),USE(authority_number_temp),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK,AT(348,116),USE(over_authority_number_tick),VALUE('Y','N')
                           CHECK('Engineer'),AT(104,132),USE(engineer_tick_temp),VALUE('Y','N')
                           BUTTON,AT(192,132,10,10),USE(?Lookup_Engineer),HIDE,ICON('list3.ico')
                           ENTRY(@s3),AT(208,132,24,10),USE(engineer_temp),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(236,132,96,10),USE(engineer_name_temp),SKIP,HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           CHECK,AT(348,132),USE(over_engineer_tick),VALUE('Y','N')
                           CHECK('Engineers Notes'),AT(104,148),USE(Engineers_Notes_Tick_Temp),VALUE('Y','N')
                           BUTTON,AT(192,148,10,10),USE(?lookup_engineers_notes),HIDE,LEFT,ICON('list3.ico')
                           TEXT,AT(208,148,124,32),USE(engineers_notes_temp),HIDE,VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK,AT(348,148),USE(over_engineers_notes_tick),VALUE('Y','N')
                           CHECK('Invoice Text'),AT(104,184),USE(Invoice_Text_Tick_Temp),VALUE('Y','N')
                           BUTTON,AT(192,184,10,10),USE(?Lookup_Invoice_Text),HIDE,LEFT,ICON('list3.ico')
                           TEXT,AT(208,184,124,32),USE(invoice_text_temp),HIDE,VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK,AT(348,184),USE(over_invoice_Text_tick),VALUE('Y','N')
                           CHECK('Charge. Charge Type'),AT(104,224,89,10),USE(charge_type_tick_temp),VALUE('Y','N')
                           BUTTON,AT(192,224,10,10),USE(?Lookup_Charge_Type),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(208,224,124,10),USE(Charge_Type_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK,AT(348,224),USE(over_chargeable_tick),VALUE('Y','N')
                           CHECK('Warranty Charge Type'),AT(104,240),USE(warranty_charge_type_tick_temp),VALUE('Y','N')
                           BUTTON,AT(192,240,10,10),USE(?Lookup_Warranty_Charge_Type),HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(208,240,124,10),USE(Warranty_Charge_Type_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK,AT(348,240),USE(over_warranty_tick),VALUE('Y','N')
                           CHECK('In Repair'),AT(104,256),USE(in_repair_tick_temp),VALUE('Y','N')
                           CHECK,AT(348,252),USE(over_in_repair_tick),VALUE('Y','N')
                           CHECK('On Test'),AT(104,272,40,12),USE(On_Test_Tick_Temp),VALUE('Y','N')
                           CHECK,AT(348,268),USE(over_on_test_tick),VALUE('Y','N')
                           CHECK('Completed Date'),AT(104,288),USE(auto_complete_tick_temp),VALUE('Y','N')
                           ENTRY(@D6b),AT(272,284,56,10),USE(date_completed_temp),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(332,284,10,10),USE(?PopCalendar:13),HIDE,LEFT,ICON('Calenda2.ico')
                           CHECK,AT(348,284,12,12),USE(over_date_completed_tick),HIDE,VALUE('Y','N')
                         END
                         TAB('Tab 14'),USE(?Tab14)
                           PROMPT('Select Jobs or Import'),AT(104,36),USE(?Prompt29),FONT(,12,,FONT:bold)
                           PROMPT('Do you wish to tag the jobs to update, or do you want to select the jobs to upda' &|
   'te from a text file?'),AT(106,58,210,26),USE(?Prompt30)
                           OPTION('Select Jobs Type'),AT(108,128,202,50),USE(tmp:SelectJobsType),BOXED
                             RADIO('Tag Jobs'),AT(126,150),USE(?tmp:SelectJobsType:Radio1),VALUE('0')
                             RADIO('Import Jobs'),AT(222,150),USE(?tmp:SelectJobsType:Radio2),VALUE('1')
                           END
                           GROUP,AT(100,188,292,28),USE(?ImportGroup),HIDE
                             PROMPT('Import File'),AT(112,196),USE(?tmp:ImportFile:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s255),AT(168,196,200,10),USE(tmp:ImportFile),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Import File'),TIP('Import File'),UPR
                             BUTTON,AT(372,196,10,10),USE(?LookupImportFile),SKIP,ICON('List3.ico')
                           END
                         END
                         TAB('Page 2'),USE(?Tab5)
                           PROMPT('Jobs On Batch Number: '),AT(104,36),USE(?Prompt18),FONT(,12,,FONT:bold)
                           STRING(@p<<<<<<<#p),AT(232,36),USE(GLO:Select24),LEFT,FONT(,12,,FONT:bold)
                           PROMPT('Tag the jobs you wish to update and select ''Update Tagged Jobs'' to amend them.'),AT(104,52),USE(?Prompt19),FONT(,10,,)
                           LIST,AT(108,108,396,180),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10L(2)I@s1@64L(2)|M~Job Number~@p<<<<<<<<<<<<#p@129L(2)|M~Model Number~@s30@65L(2)|M~A' &|
   'ccount Number~@s15@120L(2)|M~Current Status~@s30@'),FROM(Queue:Browse:1)
                           SHEET,AT(104,76,404,236),USE(?Sheet2),SPREAD
                             TAB('By Job Number'),USE(?Tab6)
                             END
                             TAB('By Current Status'),USE(?Tab7)
                               COMBO(@s30),AT(180,92,124,10),USE(status_location1_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                             END
                             TAB('By Model Number'),USE(?Tab11)
                               COMBO(@s30),AT(176,92,124,10),USE(model_number1_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                             END
                           END
                           BUTTON('Tag All '),AT(160,292,48,16),USE(?DASTAGAll),LEFT,ICON('TAGALL.GIF')
                           BUTTON('Untag All'),AT(212,292,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           BUTTON('&Tag'),AT(108,292,48,16),USE(?DASTAG),LEFT,ICON('TAG.GIF')
                           BUTTON('&Rev tags'),AT(264,292,32,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(296,292,32,13),USE(?DASSHOWTAG),HIDE
                           ENTRY(@p<<<<<<#pb),AT(108,92,64,10),USE(job:Ref_Number),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                         END
                         TAB('Page 3'),USE(?Tab10)
                           PROMPT('All Jobs'),AT(104,36),USE(?Prompt20),FONT(,12,,FONT:bold)
                           PROMPT('Tag the jobs you wish to update and select ''Update Tagged Jobs'' to amend them.'),AT(104,52),USE(?Prompt19:2),FONT(,10,,)
                           SHEET,AT(104,76,404,232),USE(?Sheet3),SPREAD
                             TAB('By Job Number'),USE(?Job_number_Tab)
                             END
                             TAB('By Current Status'),USE(?Tab12)
                               COMBO(@s30),AT(176,92,124,10),USE(status_location2_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:3)
                             END
                             TAB('By Model Number'),USE(?Tab13)
                               COMBO(@s30),AT(179,93,124,10),USE(model_number2_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:5)
                             END
                           END
                           LIST,AT(108,108,396,176),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10L(2)I@s1@63L(2)|M~Job Number~@p<<<<<<<<<<<<#p@124L(2)|M~Model Number~@s30@60L(2)|M~A' &|
   'ccount Number~@s15@120L(2)|M~Current Status~@s30@'),FROM(Queue:Browse:2)
                           ENTRY(@p<<<<<<#pb),AT(108,92,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:2),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Tag All'),AT(160,288,48,16),USE(?DASTAGAll:2),LEFT,ICON('TAGALL.GIF')
                           BUTTON('Untag All'),AT(212,288,48,16),USE(?DASUNTAGALL:2),LEFT,ICON('untag.gif')
                           BUTTON('&Tag'),AT(108,288,48,16),USE(?DASTAG:2),LEFT,ICON('TAG.GIF')
                           BUTTON('&Rev tags'),AT(388,292,32,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(420,292,32,13),USE(?DASSHOWTAG:2),HIDE
                         END
                         TAB('Page 4'),USE(?Tab3)
                           PROMPT('Manufacturer Fault Codes'),AT(132,36),USE(?Prompt6),FONT(,12,,FONT:bold)
                           PROMPT('Overwrite'),AT(388,60),USE(?Prompt28),FONT(,,,FONT:bold+FONT:underline)
                           PROMPT('Fault Code 1:'),AT(133,76),USE(?Fault_Code1:Prompt),TRN
                           ENTRY(@s30),AT(232,76,124,10),USE(Fault_Code1),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,76,10,10),USE(?Lookup1),ICON('list3.ico')
                           BUTTON,AT(373,76,10,10),USE(?PopCalendar),ICON('Calenda2.ico')
                           CHECK,AT(396,76),USE(tmp:OverwriteFaultCode1),HIDE,MSG('Over1'),TIP('Over1'),VALUE('1','0')
                           ENTRY(@d6b),AT(181,76,64,10),USE(Fault_Code1,,?Fault_Code1:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 2:'),AT(133,92),USE(?Fault_Code2:Prompt),TRN
                           ENTRY(@s30),AT(232,92,124,10),USE(Fault_Code2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(181,92,64,10),USE(Fault_Code2,,?Fault_Code2:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 3:'),AT(133,108),USE(?Fault_Code3:Prompt),TRN
                           ENTRY(@s30),AT(232,108,124,10),USE(Fault_Code3),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(181,108,64,10),USE(Fault_Code3,,?Fault_Code3:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 4:'),AT(133,124),USE(?Fault_Code4:Prompt),TRN
                           ENTRY(@s30),AT(232,124,124,10),USE(Fault_Code4),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,92,10,10),USE(?Lookup1:2),ICON('list3.ico')
                           BUTTON,AT(373,92,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                           CHECK,AT(396,92),USE(tmp:OverwriteFaultCode2),HIDE,MSG('Over2'),TIP('Over2'),VALUE('1','0')
                           CHECK,AT(396,108),USE(tmp:OverwriteFaultCode3),HIDE,MSG('Over3'),TIP('Over3'),VALUE('1','0')
                           PROMPT('Fault Code 5:'),AT(133,140),USE(?Fault_Code5:Prompt),TRN
                           ENTRY(@s30),AT(232,140,124,10),USE(Fault_Code5),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,108,10,10),USE(?Lookup1:3),ICON('list3.ico')
                           BUTTON,AT(373,108,10,10),USE(?PopCalendar:3),ICON('Calenda2.ico')
                           ENTRY(@d6b),AT(181,124,64,10),USE(Fault_Code4,,?Fault_Code4:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 6:'),AT(133,156),USE(?Fault_Code6:Prompt),TRN
                           ENTRY(@s30),AT(232,156,124,10),USE(Fault_Code6),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,124,10,10),USE(?Lookup1:4),ICON('list3.ico')
                           BUTTON,AT(373,124,10,10),USE(?PopCalendar:4),ICON('Calenda2.ico')
                           CHECK,AT(396,124),USE(tmp:OverwriteFaultCode4),HIDE,MSG('Over4'),TIP('Over4'),VALUE('1','0')
                           ENTRY(@d6b),AT(181,156,64,10),USE(Fault_Code6,,?Fault_Code6:2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(181,140,64,10),USE(Fault_Code5,,?Fault_Code5:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 7:'),AT(133,172),USE(?Fault_Code7:Prompt),TRN
                           ENTRY(@s255),AT(232,172,124,10),USE(Fault_Code7),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,140,10,10),USE(?Lookup1:5),ICON('list3.ico')
                           BUTTON,AT(373,140,10,10),USE(?PopCalendar:5),ICON('Calenda2.ico')
                           CHECK,AT(396,140),USE(tmp:OverwriteFaultCode5),HIDE,MSG('Over5'),TIP('Over5'),VALUE('1','0')
                           ENTRY(@d6b),AT(181,172,64,10),USE(Fault_Code7,,?Fault_Code7:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 8:'),AT(133,188),USE(?Fault_Code8:Prompt),TRN
                           ENTRY(@s255),AT(232,188,124,10),USE(Fault_Code8),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,156,10,10),USE(?Lookup1:6),ICON('list3.ico')
                           BUTTON,AT(373,156,10,10),USE(?PopCalendar:6),ICON('Calenda2.ico')
                           CHECK,AT(396,156),USE(tmp:OverwriteFaultCode6),HIDE,MSG('Over6'),TIP('Over6'),VALUE('1','0')
                           PROMPT('Fault Code 9:'),AT(133,204),USE(?Fault_Code9:Prompt),TRN
                           ENTRY(@s255),AT(232,204,124,10),USE(Fault_Code9),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,172,10,10),USE(?Lookup1:7),ICON('list3.ico')
                           BUTTON,AT(373,172,10,10),USE(?PopCalendar:7),ICON('Calenda2.ico')
                           CHECK,AT(396,172),USE(tmp:OverwriteFaultCode7),HIDE,MSG('Over7'),TIP('Over7'),VALUE('1','0')
                           ENTRY(@d6b),AT(181,188,64,10),USE(Fault_Code8,,?Fault_Code8:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 10:'),AT(133,220),USE(?Fault_Code10:Prompt),TRN
                           ENTRY(@s255),AT(232,220,124,10),USE(Fault_Code10),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,188,10,10),USE(?Lookup1:8),ICON('list3.ico')
                           BUTTON,AT(373,188,10,10),USE(?PopCalendar:8),ICON('Calenda2.ico')
                           CHECK,AT(396,188),USE(tmp:OverwriteFaultCode8),HIDE,MSG('Over9'),TIP('Over9'),VALUE('1','0')
                           ENTRY(@d6b),AT(181,204,64,10),USE(Fault_Code9,,?Fault_Code9:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 11:'),AT(133,236),USE(?Fault_Code11:Prompt),TRN
                           ENTRY(@s255),AT(232,236,124,10),USE(Fault_Code11),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(360,204,10,10),USE(?Lookup1:9),ICON('list3.ico')
                           BUTTON,AT(373,204,10,10),USE(?PopCalendar:9),ICON('Calenda2.ico')
                           CHECK,AT(396,204),USE(tmp:OverwriteFaultCode9),HIDE,MSG('Over9'),TIP('Over9'),VALUE('1','0')
                           BUTTON,AT(360,220,10,10),USE(?Lookup1:10),ICON('list3.ico')
                           BUTTON,AT(373,220,10,10),USE(?PopCalendar:10),ICON('Calenda2.ico')
                           CHECK,AT(396,220),USE(tmp:OverwriteFaultCode10),HIDE,MSG('Over10'),TIP('Over10'),VALUE('1','0')
                           BUTTON,AT(360,236,10,10),USE(?Lookup1:11),ICON('list3.ico')
                           BUTTON,AT(373,252,10,10),USE(?PopCalendar:12),ICON('Calenda2.ico')
                           CHECK,AT(396,252),USE(tmp:OverwriteFaultCode12),HIDE,MSG('Over12'),TIP('Over12'),VALUE('1','0')
                           BUTTON,AT(360,252,10,10),USE(?Lookup1:12),ICON('list3.ico')
                           BUTTON,AT(373,236,10,10),USE(?PopCalendar:11),ICON('Calenda2.ico')
                           CHECK,AT(396,236),USE(tmp:OverwriteFaultCode11),HIDE,MSG('Over11'),TIP('Over11'),VALUE('1','0')
                           ENTRY(@d6b),AT(181,220,64,10),USE(Fault_Code10,,?Fault_Code10:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 12:'),AT(133,252),USE(?Fault_Code12:Prompt),TRN
                           ENTRY(@s255),AT(232,252,124,10),USE(Fault_Code12),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(181,252,64,10),USE(Fault_Code12,,?Fault_Code12:2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(181,236,64,10),USE(Fault_Code11,,?Fault_Code11:2),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('Page 5'),USE(?Tab4)
                           PROMPT('Attach Parts And Allocate Repair Type'),AT(96,36),USE(?Prompt16),FONT(,12,,FONT:bold)
                           PROMPT('Tick "Do Not Take Parts from Stock" if you do not with to decrement stock.'),AT(96,52),USE(?Prompt17),FONT(,10,,)
                           CHECK('Do Not Take Parts From Stock'),AT(96,72),USE(update_stock_module_temp)
                           LIST,AT(208,92,236,56),USE(?List),IMM,MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@'),FROM(Queue:Browse)
                           PROMPT('Chargeable Parts'),AT(96,92),USE(?Prompt23),FONT(,12,,FONT:bold)
                           BUTTON('&Insert'),AT(448,92,56,16),USE(?Insert),LEFT,ICON('Insert.ico')
                           BUTTON('&Change'),AT(448,112,56,16),USE(?Change),LEFT,ICON('Edit.ico')
                           BUTTON('&Delete'),AT(448,132,56,16),USE(?Delete),LEFT,ICON('delete.ico')
                           LIST,AT(208,184,236,56),USE(?List:4),IMM,MSG('Browsing Records'),FORMAT('120L(2)|M~Part Number~@s30@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:3)
                           PROMPT('Warranty Parts'),AT(96,184),USE(?Prompt24),FONT(,12,,FONT:bold)
                           BUTTON('&Insert'),AT(448,184,56,16),USE(?Insert:2),LEFT,ICON('insert.ico')
                           BUTTON('&Change'),AT(448,204,56,16),USE(?Change:2),LEFT,ICON('edit.ico')
                           BUTTON('&Delete'),AT(448,224,56,16),USE(?Delete:2),LEFT,ICON('delete.ico')
                           CHECK('Warranty Repair Type'),AT(96,248),USE(repair_type_warranty_tick_temp),VALUE('Y','N')
                           ENTRY(@s30),AT(208,248,124,10),USE(Repair_Type_Warranty_Temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Overwrite'),AT(340,248),USE(over_repair_type_warranty_temp),VALUE('Y','N')
                           BUTTON,AT(192,248,10,10),USE(?Lookup_Repair_Type_Warranty),LEFT,ICON('list3.ico')
                           ENTRY(@s30),AT(208,156,124,10),USE(Repair_Type_Temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Chargeable Repair Type'),AT(96,156),USE(repair_type_tick_temp),VALUE('Y','N')
                           CHECK('Overwrite'),AT(340,156),USE(over_repair_type_tick),VALUE('Y','N')
                           BUTTON,AT(192,156,10,10),USE(?Lookup_Repair_Type),HIDE,ICON('list3.ico')
                         END
                       END
                       BUTTON('Close'),AT(452,324,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       BUTTON('&Back'),AT(8,324,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,324,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('&Finish'),AT(120,324,56,16),USE(?Finish_Button),LEFT,ICON('ok.gif')
                       PANEL,AT(4,320,508,24),USE(?Panel1),FILL(COLOR:Silver)
                       IMAGE('wiz.gif'),AT(12,120),USE(?Image1)
                     END

Wizard36         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB37               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB39               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW6::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 2
BRW6::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 3
BRW6::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW6::Sort1:StepClass StepLongClass                   !Conditional Step Manager - Choice(?Sheet2) = 2
BRW24                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW24::Sort0:Locator IncrementalLocatorClass          !Default Locator
BRW24::Sort1:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet3) = 2
BRW24::Sort2:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet3) = 3
BRW31                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW31::Sort0:Locator StepLocatorClass                 !Default Locator
FileLookup35         SelectFileClass
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
ImportFile    File,Driver('ASCII'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
JobNumber               String(10)
                        End
                    End
save_job_id   ushort,auto
save_maf_id   ushort,auto
save_partmp_id  ushort,auto
save_wartmp_id  ushort,auto

    MAP
ValidateRepairType      PROCEDURE(STRING,STRING,STRING,STRING,STRING,STRING), LONG
    END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    ?workshop_tick_temp{prop:Font,3} = -1
    ?workshop_tick_temp{prop:Color} = 15066597
    ?workshop_tick_temp{prop:Trn} = 0
    ?over_workshop_tick_temp{prop:Font,3} = -1
    ?over_workshop_tick_temp{prop:Color} = 15066597
    ?over_workshop_tick_temp{prop:Trn} = 0
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?location_tick_temp{prop:Font,3} = -1
    ?location_tick_temp{prop:Color} = 15066597
    ?location_tick_temp{prop:Trn} = 0
    If ?location_temp{prop:ReadOnly} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 15066597
    Elsif ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 8454143
    Else ! If ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 16777215
    End ! If ?location_temp{prop:Req} = True
    ?location_temp{prop:Trn} = 0
    ?location_temp{prop:FontStyle} = font:Bold
    ?over_location_tick{prop:Font,3} = -1
    ?over_location_tick{prop:Color} = 15066597
    ?over_location_tick{prop:Trn} = 0
    ?authority_number_tick_temp{prop:Font,3} = -1
    ?authority_number_tick_temp{prop:Color} = 15066597
    ?authority_number_tick_temp{prop:Trn} = 0
    If ?authority_number_temp{prop:ReadOnly} = True
        ?authority_number_temp{prop:FontColor} = 65793
        ?authority_number_temp{prop:Color} = 15066597
    Elsif ?authority_number_temp{prop:Req} = True
        ?authority_number_temp{prop:FontColor} = 65793
        ?authority_number_temp{prop:Color} = 8454143
    Else ! If ?authority_number_temp{prop:Req} = True
        ?authority_number_temp{prop:FontColor} = 65793
        ?authority_number_temp{prop:Color} = 16777215
    End ! If ?authority_number_temp{prop:Req} = True
    ?authority_number_temp{prop:Trn} = 0
    ?authority_number_temp{prop:FontStyle} = font:Bold
    ?over_authority_number_tick{prop:Font,3} = -1
    ?over_authority_number_tick{prop:Color} = 15066597
    ?over_authority_number_tick{prop:Trn} = 0
    ?engineer_tick_temp{prop:Font,3} = -1
    ?engineer_tick_temp{prop:Color} = 15066597
    ?engineer_tick_temp{prop:Trn} = 0
    If ?engineer_temp{prop:ReadOnly} = True
        ?engineer_temp{prop:FontColor} = 65793
        ?engineer_temp{prop:Color} = 15066597
    Elsif ?engineer_temp{prop:Req} = True
        ?engineer_temp{prop:FontColor} = 65793
        ?engineer_temp{prop:Color} = 8454143
    Else ! If ?engineer_temp{prop:Req} = True
        ?engineer_temp{prop:FontColor} = 65793
        ?engineer_temp{prop:Color} = 16777215
    End ! If ?engineer_temp{prop:Req} = True
    ?engineer_temp{prop:Trn} = 0
    ?engineer_temp{prop:FontStyle} = font:Bold
    If ?engineer_name_temp{prop:ReadOnly} = True
        ?engineer_name_temp{prop:FontColor} = 65793
        ?engineer_name_temp{prop:Color} = 15066597
    Elsif ?engineer_name_temp{prop:Req} = True
        ?engineer_name_temp{prop:FontColor} = 65793
        ?engineer_name_temp{prop:Color} = 8454143
    Else ! If ?engineer_name_temp{prop:Req} = True
        ?engineer_name_temp{prop:FontColor} = 65793
        ?engineer_name_temp{prop:Color} = 16777215
    End ! If ?engineer_name_temp{prop:Req} = True
    ?engineer_name_temp{prop:Trn} = 0
    ?engineer_name_temp{prop:FontStyle} = font:Bold
    ?over_engineer_tick{prop:Font,3} = -1
    ?over_engineer_tick{prop:Color} = 15066597
    ?over_engineer_tick{prop:Trn} = 0
    ?Engineers_Notes_Tick_Temp{prop:Font,3} = -1
    ?Engineers_Notes_Tick_Temp{prop:Color} = 15066597
    ?Engineers_Notes_Tick_Temp{prop:Trn} = 0
    If ?engineers_notes_temp{prop:ReadOnly} = True
        ?engineers_notes_temp{prop:FontColor} = 65793
        ?engineers_notes_temp{prop:Color} = 15066597
    Elsif ?engineers_notes_temp{prop:Req} = True
        ?engineers_notes_temp{prop:FontColor} = 65793
        ?engineers_notes_temp{prop:Color} = 8454143
    Else ! If ?engineers_notes_temp{prop:Req} = True
        ?engineers_notes_temp{prop:FontColor} = 65793
        ?engineers_notes_temp{prop:Color} = 16777215
    End ! If ?engineers_notes_temp{prop:Req} = True
    ?engineers_notes_temp{prop:Trn} = 0
    ?engineers_notes_temp{prop:FontStyle} = font:Bold
    ?over_engineers_notes_tick{prop:Font,3} = -1
    ?over_engineers_notes_tick{prop:Color} = 15066597
    ?over_engineers_notes_tick{prop:Trn} = 0
    ?Invoice_Text_Tick_Temp{prop:Font,3} = -1
    ?Invoice_Text_Tick_Temp{prop:Color} = 15066597
    ?Invoice_Text_Tick_Temp{prop:Trn} = 0
    If ?invoice_text_temp{prop:ReadOnly} = True
        ?invoice_text_temp{prop:FontColor} = 65793
        ?invoice_text_temp{prop:Color} = 15066597
    Elsif ?invoice_text_temp{prop:Req} = True
        ?invoice_text_temp{prop:FontColor} = 65793
        ?invoice_text_temp{prop:Color} = 8454143
    Else ! If ?invoice_text_temp{prop:Req} = True
        ?invoice_text_temp{prop:FontColor} = 65793
        ?invoice_text_temp{prop:Color} = 16777215
    End ! If ?invoice_text_temp{prop:Req} = True
    ?invoice_text_temp{prop:Trn} = 0
    ?invoice_text_temp{prop:FontStyle} = font:Bold
    ?over_invoice_Text_tick{prop:Font,3} = -1
    ?over_invoice_Text_tick{prop:Color} = 15066597
    ?over_invoice_Text_tick{prop:Trn} = 0
    ?charge_type_tick_temp{prop:Font,3} = -1
    ?charge_type_tick_temp{prop:Color} = 15066597
    ?charge_type_tick_temp{prop:Trn} = 0
    If ?Charge_Type_temp{prop:ReadOnly} = True
        ?Charge_Type_temp{prop:FontColor} = 65793
        ?Charge_Type_temp{prop:Color} = 15066597
    Elsif ?Charge_Type_temp{prop:Req} = True
        ?Charge_Type_temp{prop:FontColor} = 65793
        ?Charge_Type_temp{prop:Color} = 8454143
    Else ! If ?Charge_Type_temp{prop:Req} = True
        ?Charge_Type_temp{prop:FontColor} = 65793
        ?Charge_Type_temp{prop:Color} = 16777215
    End ! If ?Charge_Type_temp{prop:Req} = True
    ?Charge_Type_temp{prop:Trn} = 0
    ?Charge_Type_temp{prop:FontStyle} = font:Bold
    ?over_chargeable_tick{prop:Font,3} = -1
    ?over_chargeable_tick{prop:Color} = 15066597
    ?over_chargeable_tick{prop:Trn} = 0
    ?warranty_charge_type_tick_temp{prop:Font,3} = -1
    ?warranty_charge_type_tick_temp{prop:Color} = 15066597
    ?warranty_charge_type_tick_temp{prop:Trn} = 0
    If ?Warranty_Charge_Type_temp{prop:ReadOnly} = True
        ?Warranty_Charge_Type_temp{prop:FontColor} = 65793
        ?Warranty_Charge_Type_temp{prop:Color} = 15066597
    Elsif ?Warranty_Charge_Type_temp{prop:Req} = True
        ?Warranty_Charge_Type_temp{prop:FontColor} = 65793
        ?Warranty_Charge_Type_temp{prop:Color} = 8454143
    Else ! If ?Warranty_Charge_Type_temp{prop:Req} = True
        ?Warranty_Charge_Type_temp{prop:FontColor} = 65793
        ?Warranty_Charge_Type_temp{prop:Color} = 16777215
    End ! If ?Warranty_Charge_Type_temp{prop:Req} = True
    ?Warranty_Charge_Type_temp{prop:Trn} = 0
    ?Warranty_Charge_Type_temp{prop:FontStyle} = font:Bold
    ?over_warranty_tick{prop:Font,3} = -1
    ?over_warranty_tick{prop:Color} = 15066597
    ?over_warranty_tick{prop:Trn} = 0
    ?in_repair_tick_temp{prop:Font,3} = -1
    ?in_repair_tick_temp{prop:Color} = 15066597
    ?in_repair_tick_temp{prop:Trn} = 0
    ?over_in_repair_tick{prop:Font,3} = -1
    ?over_in_repair_tick{prop:Color} = 15066597
    ?over_in_repair_tick{prop:Trn} = 0
    ?On_Test_Tick_Temp{prop:Font,3} = -1
    ?On_Test_Tick_Temp{prop:Color} = 15066597
    ?On_Test_Tick_Temp{prop:Trn} = 0
    ?over_on_test_tick{prop:Font,3} = -1
    ?over_on_test_tick{prop:Color} = 15066597
    ?over_on_test_tick{prop:Trn} = 0
    ?auto_complete_tick_temp{prop:Font,3} = -1
    ?auto_complete_tick_temp{prop:Color} = 15066597
    ?auto_complete_tick_temp{prop:Trn} = 0
    If ?date_completed_temp{prop:ReadOnly} = True
        ?date_completed_temp{prop:FontColor} = 65793
        ?date_completed_temp{prop:Color} = 15066597
    Elsif ?date_completed_temp{prop:Req} = True
        ?date_completed_temp{prop:FontColor} = 65793
        ?date_completed_temp{prop:Color} = 8454143
    Else ! If ?date_completed_temp{prop:Req} = True
        ?date_completed_temp{prop:FontColor} = 65793
        ?date_completed_temp{prop:Color} = 16777215
    End ! If ?date_completed_temp{prop:Req} = True
    ?date_completed_temp{prop:Trn} = 0
    ?date_completed_temp{prop:FontStyle} = font:Bold
    ?over_date_completed_tick{prop:Font,3} = -1
    ?over_date_completed_tick{prop:Color} = 15066597
    ?over_date_completed_tick{prop:Trn} = 0
    ?Tab14{prop:Color} = 15066597
    ?Prompt29{prop:FontColor} = -1
    ?Prompt29{prop:Color} = 15066597
    ?Prompt30{prop:FontColor} = -1
    ?Prompt30{prop:Color} = 15066597
    ?tmp:SelectJobsType{prop:Font,3} = -1
    ?tmp:SelectJobsType{prop:Color} = 15066597
    ?tmp:SelectJobsType{prop:Trn} = 0
    ?tmp:SelectJobsType:Radio1{prop:Font,3} = -1
    ?tmp:SelectJobsType:Radio1{prop:Color} = 15066597
    ?tmp:SelectJobsType:Radio1{prop:Trn} = 0
    ?tmp:SelectJobsType:Radio2{prop:Font,3} = -1
    ?tmp:SelectJobsType:Radio2{prop:Color} = 15066597
    ?tmp:SelectJobsType:Radio2{prop:Trn} = 0
    ?ImportGroup{prop:Font,3} = -1
    ?ImportGroup{prop:Color} = 15066597
    ?ImportGroup{prop:Trn} = 0
    ?tmp:ImportFile:Prompt{prop:FontColor} = -1
    ?tmp:ImportFile:Prompt{prop:Color} = 15066597
    If ?tmp:ImportFile{prop:ReadOnly} = True
        ?tmp:ImportFile{prop:FontColor} = 65793
        ?tmp:ImportFile{prop:Color} = 15066597
    Elsif ?tmp:ImportFile{prop:Req} = True
        ?tmp:ImportFile{prop:FontColor} = 65793
        ?tmp:ImportFile{prop:Color} = 8454143
    Else ! If ?tmp:ImportFile{prop:Req} = True
        ?tmp:ImportFile{prop:FontColor} = 65793
        ?tmp:ImportFile{prop:Color} = 16777215
    End ! If ?tmp:ImportFile{prop:Req} = True
    ?tmp:ImportFile{prop:Trn} = 0
    ?tmp:ImportFile{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    ?Prompt18{prop:FontColor} = -1
    ?Prompt18{prop:Color} = 15066597
    ?GLO:Select24{prop:FontColor} = -1
    ?GLO:Select24{prop:Color} = 15066597
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab6{prop:Color} = 15066597
    ?Tab7{prop:Color} = 15066597
    If ?status_location1_temp{prop:ReadOnly} = True
        ?status_location1_temp{prop:FontColor} = 65793
        ?status_location1_temp{prop:Color} = 15066597
    Elsif ?status_location1_temp{prop:Req} = True
        ?status_location1_temp{prop:FontColor} = 65793
        ?status_location1_temp{prop:Color} = 8454143
    Else ! If ?status_location1_temp{prop:Req} = True
        ?status_location1_temp{prop:FontColor} = 65793
        ?status_location1_temp{prop:Color} = 16777215
    End ! If ?status_location1_temp{prop:Req} = True
    ?status_location1_temp{prop:Trn} = 0
    ?status_location1_temp{prop:FontStyle} = font:Bold
    ?Tab11{prop:Color} = 15066597
    If ?model_number1_temp{prop:ReadOnly} = True
        ?model_number1_temp{prop:FontColor} = 65793
        ?model_number1_temp{prop:Color} = 15066597
    Elsif ?model_number1_temp{prop:Req} = True
        ?model_number1_temp{prop:FontColor} = 65793
        ?model_number1_temp{prop:Color} = 8454143
    Else ! If ?model_number1_temp{prop:Req} = True
        ?model_number1_temp{prop:FontColor} = 65793
        ?model_number1_temp{prop:Color} = 16777215
    End ! If ?model_number1_temp{prop:Req} = True
    ?model_number1_temp{prop:Trn} = 0
    ?model_number1_temp{prop:FontStyle} = font:Bold
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab10{prop:Color} = 15066597
    ?Prompt20{prop:FontColor} = -1
    ?Prompt20{prop:Color} = 15066597
    ?Prompt19:2{prop:FontColor} = -1
    ?Prompt19:2{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Job_number_Tab{prop:Color} = 15066597
    ?Tab12{prop:Color} = 15066597
    If ?status_location2_temp{prop:ReadOnly} = True
        ?status_location2_temp{prop:FontColor} = 65793
        ?status_location2_temp{prop:Color} = 15066597
    Elsif ?status_location2_temp{prop:Req} = True
        ?status_location2_temp{prop:FontColor} = 65793
        ?status_location2_temp{prop:Color} = 8454143
    Else ! If ?status_location2_temp{prop:Req} = True
        ?status_location2_temp{prop:FontColor} = 65793
        ?status_location2_temp{prop:Color} = 16777215
    End ! If ?status_location2_temp{prop:Req} = True
    ?status_location2_temp{prop:Trn} = 0
    ?status_location2_temp{prop:FontStyle} = font:Bold
    ?Tab13{prop:Color} = 15066597
    If ?model_number2_temp{prop:ReadOnly} = True
        ?model_number2_temp{prop:FontColor} = 65793
        ?model_number2_temp{prop:Color} = 15066597
    Elsif ?model_number2_temp{prop:Req} = True
        ?model_number2_temp{prop:FontColor} = 65793
        ?model_number2_temp{prop:Color} = 8454143
    Else ! If ?model_number2_temp{prop:Req} = True
        ?model_number2_temp{prop:FontColor} = 65793
        ?model_number2_temp{prop:Color} = 16777215
    End ! If ?model_number2_temp{prop:Req} = True
    ?model_number2_temp{prop:Trn} = 0
    ?model_number2_temp{prop:FontStyle} = font:Bold
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    If ?JOB:Ref_Number:2{prop:ReadOnly} = True
        ?JOB:Ref_Number:2{prop:FontColor} = 65793
        ?JOB:Ref_Number:2{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:2{prop:Req} = True
        ?JOB:Ref_Number:2{prop:FontColor} = 65793
        ?JOB:Ref_Number:2{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:2{prop:Req} = True
        ?JOB:Ref_Number:2{prop:FontColor} = 65793
        ?JOB:Ref_Number:2{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:2{prop:Req} = True
    ?JOB:Ref_Number:2{prop:Trn} = 0
    ?JOB:Ref_Number:2{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?Fault_Code1:Prompt{prop:FontColor} = -1
    ?Fault_Code1:Prompt{prop:Color} = 15066597
    If ?Fault_Code1{prop:ReadOnly} = True
        ?Fault_Code1{prop:FontColor} = 65793
        ?Fault_Code1{prop:Color} = 15066597
    Elsif ?Fault_Code1{prop:Req} = True
        ?Fault_Code1{prop:FontColor} = 65793
        ?Fault_Code1{prop:Color} = 8454143
    Else ! If ?Fault_Code1{prop:Req} = True
        ?Fault_Code1{prop:FontColor} = 65793
        ?Fault_Code1{prop:Color} = 16777215
    End ! If ?Fault_Code1{prop:Req} = True
    ?Fault_Code1{prop:Trn} = 0
    ?Fault_Code1{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode1{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode1{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode1{prop:Trn} = 0
    If ?Fault_Code1:2{prop:ReadOnly} = True
        ?Fault_Code1:2{prop:FontColor} = 65793
        ?Fault_Code1:2{prop:Color} = 15066597
    Elsif ?Fault_Code1:2{prop:Req} = True
        ?Fault_Code1:2{prop:FontColor} = 65793
        ?Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?Fault_Code1:2{prop:Req} = True
        ?Fault_Code1:2{prop:FontColor} = 65793
        ?Fault_Code1:2{prop:Color} = 16777215
    End ! If ?Fault_Code1:2{prop:Req} = True
    ?Fault_Code1:2{prop:Trn} = 0
    ?Fault_Code1:2{prop:FontStyle} = font:Bold
    ?Fault_Code2:Prompt{prop:FontColor} = -1
    ?Fault_Code2:Prompt{prop:Color} = 15066597
    If ?Fault_Code2{prop:ReadOnly} = True
        ?Fault_Code2{prop:FontColor} = 65793
        ?Fault_Code2{prop:Color} = 15066597
    Elsif ?Fault_Code2{prop:Req} = True
        ?Fault_Code2{prop:FontColor} = 65793
        ?Fault_Code2{prop:Color} = 8454143
    Else ! If ?Fault_Code2{prop:Req} = True
        ?Fault_Code2{prop:FontColor} = 65793
        ?Fault_Code2{prop:Color} = 16777215
    End ! If ?Fault_Code2{prop:Req} = True
    ?Fault_Code2{prop:Trn} = 0
    ?Fault_Code2{prop:FontStyle} = font:Bold
    If ?Fault_Code2:2{prop:ReadOnly} = True
        ?Fault_Code2:2{prop:FontColor} = 65793
        ?Fault_Code2:2{prop:Color} = 15066597
    Elsif ?Fault_Code2:2{prop:Req} = True
        ?Fault_Code2:2{prop:FontColor} = 65793
        ?Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?Fault_Code2:2{prop:Req} = True
        ?Fault_Code2:2{prop:FontColor} = 65793
        ?Fault_Code2:2{prop:Color} = 16777215
    End ! If ?Fault_Code2:2{prop:Req} = True
    ?Fault_Code2:2{prop:Trn} = 0
    ?Fault_Code2:2{prop:FontStyle} = font:Bold
    ?Fault_Code3:Prompt{prop:FontColor} = -1
    ?Fault_Code3:Prompt{prop:Color} = 15066597
    If ?Fault_Code3{prop:ReadOnly} = True
        ?Fault_Code3{prop:FontColor} = 65793
        ?Fault_Code3{prop:Color} = 15066597
    Elsif ?Fault_Code3{prop:Req} = True
        ?Fault_Code3{prop:FontColor} = 65793
        ?Fault_Code3{prop:Color} = 8454143
    Else ! If ?Fault_Code3{prop:Req} = True
        ?Fault_Code3{prop:FontColor} = 65793
        ?Fault_Code3{prop:Color} = 16777215
    End ! If ?Fault_Code3{prop:Req} = True
    ?Fault_Code3{prop:Trn} = 0
    ?Fault_Code3{prop:FontStyle} = font:Bold
    If ?Fault_Code3:2{prop:ReadOnly} = True
        ?Fault_Code3:2{prop:FontColor} = 65793
        ?Fault_Code3:2{prop:Color} = 15066597
    Elsif ?Fault_Code3:2{prop:Req} = True
        ?Fault_Code3:2{prop:FontColor} = 65793
        ?Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?Fault_Code3:2{prop:Req} = True
        ?Fault_Code3:2{prop:FontColor} = 65793
        ?Fault_Code3:2{prop:Color} = 16777215
    End ! If ?Fault_Code3:2{prop:Req} = True
    ?Fault_Code3:2{prop:Trn} = 0
    ?Fault_Code3:2{prop:FontStyle} = font:Bold
    ?Fault_Code4:Prompt{prop:FontColor} = -1
    ?Fault_Code4:Prompt{prop:Color} = 15066597
    If ?Fault_Code4{prop:ReadOnly} = True
        ?Fault_Code4{prop:FontColor} = 65793
        ?Fault_Code4{prop:Color} = 15066597
    Elsif ?Fault_Code4{prop:Req} = True
        ?Fault_Code4{prop:FontColor} = 65793
        ?Fault_Code4{prop:Color} = 8454143
    Else ! If ?Fault_Code4{prop:Req} = True
        ?Fault_Code4{prop:FontColor} = 65793
        ?Fault_Code4{prop:Color} = 16777215
    End ! If ?Fault_Code4{prop:Req} = True
    ?Fault_Code4{prop:Trn} = 0
    ?Fault_Code4{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode2{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode2{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode2{prop:Trn} = 0
    ?tmp:OverwriteFaultCode3{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode3{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode3{prop:Trn} = 0
    ?Fault_Code5:Prompt{prop:FontColor} = -1
    ?Fault_Code5:Prompt{prop:Color} = 15066597
    If ?Fault_Code5{prop:ReadOnly} = True
        ?Fault_Code5{prop:FontColor} = 65793
        ?Fault_Code5{prop:Color} = 15066597
    Elsif ?Fault_Code5{prop:Req} = True
        ?Fault_Code5{prop:FontColor} = 65793
        ?Fault_Code5{prop:Color} = 8454143
    Else ! If ?Fault_Code5{prop:Req} = True
        ?Fault_Code5{prop:FontColor} = 65793
        ?Fault_Code5{prop:Color} = 16777215
    End ! If ?Fault_Code5{prop:Req} = True
    ?Fault_Code5{prop:Trn} = 0
    ?Fault_Code5{prop:FontStyle} = font:Bold
    If ?Fault_Code4:2{prop:ReadOnly} = True
        ?Fault_Code4:2{prop:FontColor} = 65793
        ?Fault_Code4:2{prop:Color} = 15066597
    Elsif ?Fault_Code4:2{prop:Req} = True
        ?Fault_Code4:2{prop:FontColor} = 65793
        ?Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?Fault_Code4:2{prop:Req} = True
        ?Fault_Code4:2{prop:FontColor} = 65793
        ?Fault_Code4:2{prop:Color} = 16777215
    End ! If ?Fault_Code4:2{prop:Req} = True
    ?Fault_Code4:2{prop:Trn} = 0
    ?Fault_Code4:2{prop:FontStyle} = font:Bold
    ?Fault_Code6:Prompt{prop:FontColor} = -1
    ?Fault_Code6:Prompt{prop:Color} = 15066597
    If ?Fault_Code6{prop:ReadOnly} = True
        ?Fault_Code6{prop:FontColor} = 65793
        ?Fault_Code6{prop:Color} = 15066597
    Elsif ?Fault_Code6{prop:Req} = True
        ?Fault_Code6{prop:FontColor} = 65793
        ?Fault_Code6{prop:Color} = 8454143
    Else ! If ?Fault_Code6{prop:Req} = True
        ?Fault_Code6{prop:FontColor} = 65793
        ?Fault_Code6{prop:Color} = 16777215
    End ! If ?Fault_Code6{prop:Req} = True
    ?Fault_Code6{prop:Trn} = 0
    ?Fault_Code6{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode4{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode4{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode4{prop:Trn} = 0
    If ?Fault_Code6:2{prop:ReadOnly} = True
        ?Fault_Code6:2{prop:FontColor} = 65793
        ?Fault_Code6:2{prop:Color} = 15066597
    Elsif ?Fault_Code6:2{prop:Req} = True
        ?Fault_Code6:2{prop:FontColor} = 65793
        ?Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?Fault_Code6:2{prop:Req} = True
        ?Fault_Code6:2{prop:FontColor} = 65793
        ?Fault_Code6:2{prop:Color} = 16777215
    End ! If ?Fault_Code6:2{prop:Req} = True
    ?Fault_Code6:2{prop:Trn} = 0
    ?Fault_Code6:2{prop:FontStyle} = font:Bold
    If ?Fault_Code5:2{prop:ReadOnly} = True
        ?Fault_Code5:2{prop:FontColor} = 65793
        ?Fault_Code5:2{prop:Color} = 15066597
    Elsif ?Fault_Code5:2{prop:Req} = True
        ?Fault_Code5:2{prop:FontColor} = 65793
        ?Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?Fault_Code5:2{prop:Req} = True
        ?Fault_Code5:2{prop:FontColor} = 65793
        ?Fault_Code5:2{prop:Color} = 16777215
    End ! If ?Fault_Code5:2{prop:Req} = True
    ?Fault_Code5:2{prop:Trn} = 0
    ?Fault_Code5:2{prop:FontStyle} = font:Bold
    ?Fault_Code7:Prompt{prop:FontColor} = -1
    ?Fault_Code7:Prompt{prop:Color} = 15066597
    If ?Fault_Code7{prop:ReadOnly} = True
        ?Fault_Code7{prop:FontColor} = 65793
        ?Fault_Code7{prop:Color} = 15066597
    Elsif ?Fault_Code7{prop:Req} = True
        ?Fault_Code7{prop:FontColor} = 65793
        ?Fault_Code7{prop:Color} = 8454143
    Else ! If ?Fault_Code7{prop:Req} = True
        ?Fault_Code7{prop:FontColor} = 65793
        ?Fault_Code7{prop:Color} = 16777215
    End ! If ?Fault_Code7{prop:Req} = True
    ?Fault_Code7{prop:Trn} = 0
    ?Fault_Code7{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode5{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode5{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode5{prop:Trn} = 0
    If ?Fault_Code7:2{prop:ReadOnly} = True
        ?Fault_Code7:2{prop:FontColor} = 65793
        ?Fault_Code7:2{prop:Color} = 15066597
    Elsif ?Fault_Code7:2{prop:Req} = True
        ?Fault_Code7:2{prop:FontColor} = 65793
        ?Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?Fault_Code7:2{prop:Req} = True
        ?Fault_Code7:2{prop:FontColor} = 65793
        ?Fault_Code7:2{prop:Color} = 16777215
    End ! If ?Fault_Code7:2{prop:Req} = True
    ?Fault_Code7:2{prop:Trn} = 0
    ?Fault_Code7:2{prop:FontStyle} = font:Bold
    ?Fault_Code8:Prompt{prop:FontColor} = -1
    ?Fault_Code8:Prompt{prop:Color} = 15066597
    If ?Fault_Code8{prop:ReadOnly} = True
        ?Fault_Code8{prop:FontColor} = 65793
        ?Fault_Code8{prop:Color} = 15066597
    Elsif ?Fault_Code8{prop:Req} = True
        ?Fault_Code8{prop:FontColor} = 65793
        ?Fault_Code8{prop:Color} = 8454143
    Else ! If ?Fault_Code8{prop:Req} = True
        ?Fault_Code8{prop:FontColor} = 65793
        ?Fault_Code8{prop:Color} = 16777215
    End ! If ?Fault_Code8{prop:Req} = True
    ?Fault_Code8{prop:Trn} = 0
    ?Fault_Code8{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode6{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode6{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode6{prop:Trn} = 0
    ?Fault_Code9:Prompt{prop:FontColor} = -1
    ?Fault_Code9:Prompt{prop:Color} = 15066597
    If ?Fault_Code9{prop:ReadOnly} = True
        ?Fault_Code9{prop:FontColor} = 65793
        ?Fault_Code9{prop:Color} = 15066597
    Elsif ?Fault_Code9{prop:Req} = True
        ?Fault_Code9{prop:FontColor} = 65793
        ?Fault_Code9{prop:Color} = 8454143
    Else ! If ?Fault_Code9{prop:Req} = True
        ?Fault_Code9{prop:FontColor} = 65793
        ?Fault_Code9{prop:Color} = 16777215
    End ! If ?Fault_Code9{prop:Req} = True
    ?Fault_Code9{prop:Trn} = 0
    ?Fault_Code9{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode7{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode7{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode7{prop:Trn} = 0
    If ?Fault_Code8:2{prop:ReadOnly} = True
        ?Fault_Code8:2{prop:FontColor} = 65793
        ?Fault_Code8:2{prop:Color} = 15066597
    Elsif ?Fault_Code8:2{prop:Req} = True
        ?Fault_Code8:2{prop:FontColor} = 65793
        ?Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?Fault_Code8:2{prop:Req} = True
        ?Fault_Code8:2{prop:FontColor} = 65793
        ?Fault_Code8:2{prop:Color} = 16777215
    End ! If ?Fault_Code8:2{prop:Req} = True
    ?Fault_Code8:2{prop:Trn} = 0
    ?Fault_Code8:2{prop:FontStyle} = font:Bold
    ?Fault_Code10:Prompt{prop:FontColor} = -1
    ?Fault_Code10:Prompt{prop:Color} = 15066597
    If ?Fault_Code10{prop:ReadOnly} = True
        ?Fault_Code10{prop:FontColor} = 65793
        ?Fault_Code10{prop:Color} = 15066597
    Elsif ?Fault_Code10{prop:Req} = True
        ?Fault_Code10{prop:FontColor} = 65793
        ?Fault_Code10{prop:Color} = 8454143
    Else ! If ?Fault_Code10{prop:Req} = True
        ?Fault_Code10{prop:FontColor} = 65793
        ?Fault_Code10{prop:Color} = 16777215
    End ! If ?Fault_Code10{prop:Req} = True
    ?Fault_Code10{prop:Trn} = 0
    ?Fault_Code10{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode8{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode8{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode8{prop:Trn} = 0
    If ?Fault_Code9:2{prop:ReadOnly} = True
        ?Fault_Code9:2{prop:FontColor} = 65793
        ?Fault_Code9:2{prop:Color} = 15066597
    Elsif ?Fault_Code9:2{prop:Req} = True
        ?Fault_Code9:2{prop:FontColor} = 65793
        ?Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?Fault_Code9:2{prop:Req} = True
        ?Fault_Code9:2{prop:FontColor} = 65793
        ?Fault_Code9:2{prop:Color} = 16777215
    End ! If ?Fault_Code9:2{prop:Req} = True
    ?Fault_Code9:2{prop:Trn} = 0
    ?Fault_Code9:2{prop:FontStyle} = font:Bold
    ?Fault_Code11:Prompt{prop:FontColor} = -1
    ?Fault_Code11:Prompt{prop:Color} = 15066597
    If ?Fault_Code11{prop:ReadOnly} = True
        ?Fault_Code11{prop:FontColor} = 65793
        ?Fault_Code11{prop:Color} = 15066597
    Elsif ?Fault_Code11{prop:Req} = True
        ?Fault_Code11{prop:FontColor} = 65793
        ?Fault_Code11{prop:Color} = 8454143
    Else ! If ?Fault_Code11{prop:Req} = True
        ?Fault_Code11{prop:FontColor} = 65793
        ?Fault_Code11{prop:Color} = 16777215
    End ! If ?Fault_Code11{prop:Req} = True
    ?Fault_Code11{prop:Trn} = 0
    ?Fault_Code11{prop:FontStyle} = font:Bold
    ?tmp:OverwriteFaultCode9{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode9{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode9{prop:Trn} = 0
    ?tmp:OverwriteFaultCode10{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode10{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode10{prop:Trn} = 0
    ?tmp:OverwriteFaultCode12{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode12{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode12{prop:Trn} = 0
    ?tmp:OverwriteFaultCode11{prop:Font,3} = -1
    ?tmp:OverwriteFaultCode11{prop:Color} = 15066597
    ?tmp:OverwriteFaultCode11{prop:Trn} = 0
    If ?Fault_Code10:2{prop:ReadOnly} = True
        ?Fault_Code10:2{prop:FontColor} = 65793
        ?Fault_Code10:2{prop:Color} = 15066597
    Elsif ?Fault_Code10:2{prop:Req} = True
        ?Fault_Code10:2{prop:FontColor} = 65793
        ?Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?Fault_Code10:2{prop:Req} = True
        ?Fault_Code10:2{prop:FontColor} = 65793
        ?Fault_Code10:2{prop:Color} = 16777215
    End ! If ?Fault_Code10:2{prop:Req} = True
    ?Fault_Code10:2{prop:Trn} = 0
    ?Fault_Code10:2{prop:FontStyle} = font:Bold
    ?Fault_Code12:Prompt{prop:FontColor} = -1
    ?Fault_Code12:Prompt{prop:Color} = 15066597
    If ?Fault_Code12{prop:ReadOnly} = True
        ?Fault_Code12{prop:FontColor} = 65793
        ?Fault_Code12{prop:Color} = 15066597
    Elsif ?Fault_Code12{prop:Req} = True
        ?Fault_Code12{prop:FontColor} = 65793
        ?Fault_Code12{prop:Color} = 8454143
    Else ! If ?Fault_Code12{prop:Req} = True
        ?Fault_Code12{prop:FontColor} = 65793
        ?Fault_Code12{prop:Color} = 16777215
    End ! If ?Fault_Code12{prop:Req} = True
    ?Fault_Code12{prop:Trn} = 0
    ?Fault_Code12{prop:FontStyle} = font:Bold
    If ?Fault_Code12:2{prop:ReadOnly} = True
        ?Fault_Code12:2{prop:FontColor} = 65793
        ?Fault_Code12:2{prop:Color} = 15066597
    Elsif ?Fault_Code12:2{prop:Req} = True
        ?Fault_Code12:2{prop:FontColor} = 65793
        ?Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?Fault_Code12:2{prop:Req} = True
        ?Fault_Code12:2{prop:FontColor} = 65793
        ?Fault_Code12:2{prop:Color} = 16777215
    End ! If ?Fault_Code12:2{prop:Req} = True
    ?Fault_Code12:2{prop:Trn} = 0
    ?Fault_Code12:2{prop:FontStyle} = font:Bold
    If ?Fault_Code11:2{prop:ReadOnly} = True
        ?Fault_Code11:2{prop:FontColor} = 65793
        ?Fault_Code11:2{prop:Color} = 15066597
    Elsif ?Fault_Code11:2{prop:Req} = True
        ?Fault_Code11:2{prop:FontColor} = 65793
        ?Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?Fault_Code11:2{prop:Req} = True
        ?Fault_Code11:2{prop:FontColor} = 65793
        ?Fault_Code11:2{prop:Color} = 16777215
    End ! If ?Fault_Code11:2{prop:Req} = True
    ?Fault_Code11:2{prop:Trn} = 0
    ?Fault_Code11:2{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?update_stock_module_temp{prop:Font,3} = -1
    ?update_stock_module_temp{prop:Color} = 15066597
    ?update_stock_module_temp{prop:Trn} = 0
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt23{prop:FontColor} = -1
    ?Prompt23{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    ?repair_type_warranty_tick_temp{prop:Font,3} = -1
    ?repair_type_warranty_tick_temp{prop:Color} = 15066597
    ?repair_type_warranty_tick_temp{prop:Trn} = 0
    If ?Repair_Type_Warranty_Temp{prop:ReadOnly} = True
        ?Repair_Type_Warranty_Temp{prop:FontColor} = 65793
        ?Repair_Type_Warranty_Temp{prop:Color} = 15066597
    Elsif ?Repair_Type_Warranty_Temp{prop:Req} = True
        ?Repair_Type_Warranty_Temp{prop:FontColor} = 65793
        ?Repair_Type_Warranty_Temp{prop:Color} = 8454143
    Else ! If ?Repair_Type_Warranty_Temp{prop:Req} = True
        ?Repair_Type_Warranty_Temp{prop:FontColor} = 65793
        ?Repair_Type_Warranty_Temp{prop:Color} = 16777215
    End ! If ?Repair_Type_Warranty_Temp{prop:Req} = True
    ?Repair_Type_Warranty_Temp{prop:Trn} = 0
    ?Repair_Type_Warranty_Temp{prop:FontStyle} = font:Bold
    ?over_repair_type_warranty_temp{prop:Font,3} = -1
    ?over_repair_type_warranty_temp{prop:Color} = 15066597
    ?over_repair_type_warranty_temp{prop:Trn} = 0
    If ?Repair_Type_Temp{prop:ReadOnly} = True
        ?Repair_Type_Temp{prop:FontColor} = 65793
        ?Repair_Type_Temp{prop:Color} = 15066597
    Elsif ?Repair_Type_Temp{prop:Req} = True
        ?Repair_Type_Temp{prop:FontColor} = 65793
        ?Repair_Type_Temp{prop:Color} = 8454143
    Else ! If ?Repair_Type_Temp{prop:Req} = True
        ?Repair_Type_Temp{prop:FontColor} = 65793
        ?Repair_Type_Temp{prop:Color} = 16777215
    End ! If ?Repair_Type_Temp{prop:Req} = True
    ?Repair_Type_Temp{prop:Trn} = 0
    ?Repair_Type_Temp{prop:FontStyle} = font:Bold
    ?repair_type_tick_temp{prop:Font,3} = -1
    ?repair_type_tick_temp{prop:Color} = 15066597
    ?repair_type_tick_temp{prop:Trn} = 0
    ?over_repair_type_tick{prop:Font,3} = -1
    ?over_repair_type_tick{prop:Color} = 15066597
    ?over_repair_type_tick{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW6.UpdateBuffer
   GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
   GET(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
  IF ERRORCODE()
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     ADD(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:q_JobNumber)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(GLO:q_JobNumber)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     ADD(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:q_JobNumber)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:q_JobNumber)
    GET(GLO:q_JobNumber,QR#)
    DASBRW::8:QUEUE = GLO:q_JobNumber
    ADD(DASBRW::8:QUEUE)
  END
  FREE(GLO:q_JobNumber)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Job_Number_Pointer = job:Ref_Number
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Job_Number_Pointer)
    IF ERRORCODE()
       GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
       ADD(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::25:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW24.UpdateBuffer
   GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
   GET(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
  IF ERRORCODE()
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     ADD(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    tag2_temp = '*'
  ELSE
    DELETE(GLO:q_JobNumber)
    tag2_temp = ''
  END
    Queue:Browse:2.tag2_temp = tag2_temp
  IF (tag2_temp = '*')
    Queue:Browse:2.tag2_temp_Icon = 2
  ELSE
    Queue:Browse:2.tag2_temp_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::25:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW24.Reset
  FREE(GLO:q_JobNumber)
  LOOP
    NEXT(BRW24::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     ADD(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
  END
  SETCURSOR
  BRW24.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::25:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:q_JobNumber)
  BRW24.Reset
  SETCURSOR
  BRW24.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::25:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::25:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:q_JobNumber)
    GET(GLO:q_JobNumber,QR#)
    DASBRW::25:QUEUE = GLO:q_JobNumber
    ADD(DASBRW::25:QUEUE)
  END
  FREE(GLO:q_JobNumber)
  BRW24.Reset
  LOOP
    NEXT(BRW24::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::25:QUEUE.Job_Number_Pointer = job:Ref_Number
     GET(DASBRW::25:QUEUE,DASBRW::25:QUEUE.Job_Number_Pointer)
    IF ERRORCODE()
       GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
       ADD(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    END
  END
  SETCURSOR
  BRW24.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::25:DASSHOWTAG Routine
   CASE DASBRW::25:TAGDISPSTATUS
   OF 0
      DASBRW::25:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::25:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::25:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW24.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
InitMaxParts    ROUTINE    !Start Change 1895 BE(19/05/03)
    tmpCheckChargeableParts = 0
    TotalPartsCost = 0.0
    access:SUBTRACC.clearkey(sub:account_number_key)
    sub:Account_Number  = job:Account_Number
    IF (access:SUBTRACC.tryfetch(sub:account_number_key) = Level:Benign) THEN
        access:TRADEACC.clearkey(tra:account_number_key)
        tra:Account_Number  = sub:Main_Account_Number
        IF (access:TRADEACC.tryfetch(tra:account_number_key) = Level:Benign) THEN
            TmpCheckChargeableParts = tra:CheckChargeablePartsCost
        END
    END
!End Change 1895 BE(19/05/03)
TotalChargeableParts    ROUTINE ! Start Change 1895 BE(19/05/03)
    TotalPartsCost = 0
    !Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
    Access:PARTS.ClearKey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    SET(par:Part_Number_Key, par:Part_Number_Key)
    LOOP
        IF ((Access:PARTS.NEXT() <> Level:Benign) OR |
            (par:Ref_Number  <> job:Ref_Number) ) THEN
            BREAK
        END
        IF (par:Adjustment <> 'YES') THEN
            TotalPartsCost += par:Purchase_Cost * par:Quantity
        END
    END
    !Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)
! End Change 1895 BE(19/05/03)
Chargeable2Warranty ROUTINE ! Change 2386 BE(19/03/03)
    save_par_id = access:parts.savefile()
    access:parts.clearkey(par:part_number_key)
    par:ref_number  = job:ref_number
    SET(par:part_number_key,par:part_number_key)
    LOOP
        IF (access:parts.next()) THEN
        BREAK
        END
        IF (par:ref_number  <> job:ref_number) THEN
            BREAK
        END
        IF (par:pending_ref_number <> '') THEN
            access:ordpend.clearkey(ope:ref_number_key)
            ope:ref_number = par:pending_ref_number
            IF (access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign) THEN
                ope:part_type = 'JOB'
                access:ordpend.update()
            END
        END
        IF (par:order_number <> '') THEN
            access:ordparts.clearkey(orp:record_number_key)
            orp:record_number   = par:order_number
            IF (access:ordparts.tryfetch(orp:record_number_key) = Level:Benign) THEN
                orp:part_type = 'JOB'
                access:ordparts.update()
            END
        END
        GET(warparts,0)
        IF (access:warparts.primerecord() = Level:Benign) THEN
            record_number$  = wpr:record_number
            wpr:record      :=: par:record
            wpr:record_number   = record_number$
            IF (access:warparts.insert()) THEN
                access:warparts.cancelautoinc()
            END
            if def:PickNoteNormal or def:PickNoteMainStore then TransferParts('chargeable'). !transfer from chargeable
        END
        DELETE(parts)
    END ! LOOP

    access:parts.restorefile(save_par_id)

    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number    = job:Model_Number
    IF (Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign)
        job:EDI = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
    END

! End Change 2386 BE(19/03/03)
Warranty2Chargeable  ROUTINE ! Change 2386 BE(19/03/03)
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    SET(wpr:part_number_key,wpr:part_number_key)
    LOOP
        IF (access:warparts.next()) THEN
            BREAK
        END
        IF (wpr:ref_number  <> job:ref_number) THEN
            BREAK
        END
        IF (wpr:pending_ref_number <> '') THEN
            access:ordpend.clearkey(ope:ref_number_key)
            ope:ref_number = wpr:pending_ref_number
            IF (access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign) THEN
                ope:part_type = 'JOB'
                access:ordpend.update()
            END
        END
        IF (wpr:order_number <> '') THEN
            access:ordparts.clearkey(orp:record_number_key)
            orp:record_number   = wpr:order_number
            IF (access:ordparts.tryfetch(orp:record_number_key) = Level:Benign) THEN
                orp:part_type = 'JOB'
                access:ordparts.update()
            END
        END
        GET(parts,0)
        IF (access:parts.primerecord() = Level:Benign) THEN
            record_number$  = par:record_number
            par:record      :=: wpr:record
            par:record_number   = record_number$
            IF (access:parts.insert()) THEN
                access:parts.cancelautoinc()
            END
            if def:PickNoteNormal or def:PickNoteMainStore then TransferParts('warranty').
        END
        DELETE(warparts)
    END ! LOOP

    access:warparts.restorefile(save_wpr_id)


    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
    mod:Model_Number = job:Model_NUmber
    IF (Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign) THEN
        job:EDI = PendingJob(mod:Manufacturer)
        job:Manufacturer    = mod:Manufacturer
    END

! End Change 2386 BE(19/03/03)
RepairParts  ROUTINE                ! Start Change 2720 BE(12/06/03)
        RepairErrorText = ''

        IF (repair_type_tick_temp = 'Y') THEN
            IF ((job:repair_type = '') OR ((job:repair_type <> '') AND (over_repair_type_tick = 'Y'))) THEN
                IF (ValidateRepairType(job:Model_Number,job:Account_Number,|
                                       job:Charge_Type,job:Unit_Type,'C',repair_type_temp) = Level:Benign) THEN
                    job:repair_type = repair_type_temp
                ELSE
                    RepairErrorText = 'The Repair Type ' & CLIP(repair_type_temp) & ' is not available for Job ' |
                                    & CLIP(job:Ref_Number) & ' and has not been applied.'
                END
           END
        END

        IF (repair_type_warranty_tick_temp = 'Y') THEN
            IF ((job:repair_type_warranty = '') OR ((job:repair_type_warranty <> '') AND (over_repair_type_warranty_temp = 'Y'))) THEN
                IF (ValidateRepairType(job:Model_Number,job:Account_Number,|
                                       job:Warranty_Charge_Type,job:Unit_Type,'W',repair_type_warranty_temp) = Level:Benign) THEN
                    job:repair_type_warranty = repair_type_warranty_temp
                ELSE
                    IF (Clip(RepairErrorText) <> '') THEN
                        RepairErrorText = CLIP(RepairErrorText) & '<13,10>'
                    END
                    RepairErrorText = CLIP(RepairErrorText) & 'The Repair Type ' & CLIP(repair_type_warranty_temp) & ' is not available for Job ' |
                                    & CLIP(job:Ref_Number) & ' and has not been applied.'
                END
            END
        END
! End Change 2720 BE(12/06/03)
Fault_Coding        Routine   ! Manufacturers Fault Codes

    use_fault_codes_temp = False
    Hide(?fault_code1)
    Hide(?fault_code1:2)
    Hide(?fault_code2)
    Hide(?fault_code2:2)
    Hide(?fault_code3)
    Hide(?fault_code3:2)
    Hide(?fault_code4)
    Hide(?fault_code4:2)
    Hide(?fault_code5)
    Hide(?fault_code5:2)
    Hide(?fault_code6)
    Hide(?fault_code6:2)
    Hide(?fault_code7)
    Hide(?fault_code7:2)
    Hide(?fault_code8)
    Hide(?fault_code8:2)
    Hide(?fault_code9)
    Hide(?fault_code9:2)
    Hide(?fault_code10)
    Hide(?fault_code10:2)
    Hide(?fault_code11)
    Hide(?fault_code11:2)
    Hide(?fault_code12)
    Hide(?fault_code12:2)
    Hide(?fault_code1:prompt)
    Hide(?fault_code2:prompt)
    Hide(?fault_code3:prompt)
    Hide(?fault_code4:prompt)
    Hide(?fault_code5:prompt)
    Hide(?fault_code6:prompt)
    Hide(?fault_code7:prompt)
    Hide(?fault_code8:prompt)
    Hide(?fault_code9:prompt)
    Hide(?fault_code10:prompt)
    Hide(?fault_code11:prompt)
    Hide(?fault_code12:prompt)
    Hide(?popcalendar)
    Hide(?popcalendar:2)
    Hide(?popcalendar:3)
    Hide(?popcalendar:4)
    Hide(?popcalendar:5)
    Hide(?popcalendar:6)
    Hide(?popcalendar:7)
    Hide(?popcalendar:8)
    Hide(?popcalendar:9)
    Hide(?popcalendar:10)
    Hide(?popcalendar:11)
    Hide(?popcalendar:12)
    Hide(?lookup1)
    Hide(?lookup1:2)
    Hide(?lookup1:3)
    Hide(?lookup1:4)
    Hide(?lookup1:5)
    Hide(?lookup1:6)
    Hide(?lookup1:7)
    Hide(?lookup1:8)
    Hide(?lookup1:9)
    Hide(?lookup1:10)
    Hide(?lookup1:11)
    Hide(?lookup1:12)
    ?tmp:OverwriteFaultCode1{prop:Hide} = 1
    ?tmp:OverwriteFaultCode2{prop:Hide} = 1
    ?tmp:OverwriteFaultCode3{prop:Hide} = 1
    ?tmp:OverwriteFaultCode4{prop:Hide} = 1
    ?tmp:OverwriteFaultCode5{prop:Hide} = 1
    ?tmp:OverwriteFaultCode6{prop:Hide} = 1
    ?tmp:OverwriteFaultCode7{prop:Hide} = 1
    ?tmp:OverwriteFaultCode8{prop:Hide} = 1
    ?tmp:OverwriteFaultCode9{prop:Hide} = 1
    ?tmp:OverwriteFaultCode10{prop:Hide} = 1
    ?tmp:OverwriteFaultCode11{prop:Hide} = 1
    ?tmp:OverwriteFaultCode12{prop:Hide} = 1

    required# = 0
    access:chartype.clearkey(cha:charge_type_key)
    cha:charge_type = job:charge_type
    If access:chartype.fetch(cha:charge_type_key) = Level:Benign
        If cha:force_warranty = 'YES'
            required# = 1
        End
    end !if


    setcursor(cursor:wait)
    save_maf_id = access:manfault.savefile()
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = manufacturer_temp
    set(maf:field_number_key,maf:field_number_key)
    loop
        if access:manfault.next()
           break
        end !if
        if maf:manufacturer <> manufacturer_temp      |
            then break.  ! end if
        use_fault_codes_temp = True
        Case maf:field_number
            Of 1
                Unhide(?fault_code1:prompt)
                ?tmp:OverwriteFaultCode1{prop:Hide} = 0
                ?fault_code1:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar)
                    ?popcalendar{prop:xpos} = 300
                    Unhide(?fault_code1:2)
                    ?fault_code1:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code1)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1)
                    End
                End
                If required# = 1
                    ?fault_code1{prop:req} = 1
                    ?fault_code1:2{prop:req} = 1
                Else
                    ?fault_code1{prop:req} = 0
                    ?fault_code1:2{prop:req} = 0
                End
            Of 2
                Unhide(?fault_code2:prompt)
                ?tmp:OverwriteFaultCode2{prop:Hide} = 0
                ?fault_code2:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:2)
                    ?popcalendar:2{prop:xpos} = 300
                    Unhide(?fault_code2:2)
                    ?fault_code2:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code2)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:2)
                    End
                End
                If required# = 1
                    ?fault_code2{prop:req} = 1
                    ?fault_code2:2{prop:req} = 1
                Else
                    ?fault_code2{prop:req} = 0
                    ?fault_code2:2{prop:req} = 0
                End

            Of 3
                Unhide(?fault_code3:prompt)
                ?tmp:OverwriteFaultCode3{prop:Hide} = 0
                ?fault_code3:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:3)
                    ?popcalendar:3{prop:xpos} = 300
                    Unhide(?fault_code3:2)
                    ?fault_code3:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code3)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:3)
                    End
                End
                If required# = 1
                    ?fault_code3{prop:req} = 1
                    ?fault_code3:2{prop:req} = 1
                Else
                    ?fault_code3{prop:req} = 0
                    ?fault_code3:2{prop:req} = 0
                End

            Of 4
                Unhide(?fault_code4:prompt)
                ?tmp:OverwriteFaultCode4{prop:Hide} = 0
                ?fault_code4:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:4)
                    ?popcalendar:4{prop:xpos} = 300
                    Unhide(?fault_code4:2)
                    ?fault_code4:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code4)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:4)
                    End
                End
                If required# = 1
                    ?fault_code4{prop:req} = 1
                    ?fault_code4:2{prop:req} = 1
                Else
                    ?fault_code4{prop:req} = 0
                    ?fault_code4:2{prop:req} = 0
                End

            Of 5
                Unhide(?fault_code5:prompt)
                ?tmp:OverwriteFaultCode5{prop:Hide} = 0
                ?fault_code5:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:5)
                    ?popcalendar:5{prop:xpos} = 300
                    Unhide(?fault_code5:2)
                    ?fault_code5:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code5)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:5)
                    End
                End
                If required# = 1
                    ?fault_code5{prop:req} = 1
                    ?fault_code5:2{prop:req} = 1
                Else
                    ?fault_code5{prop:req} = 0
                    ?fault_code5:2{prop:req} = 0
                End

            Of 6
                Unhide(?fault_code6:prompt)
                ?tmp:OverwriteFaultCode6{prop:Hide} = 0
                ?fault_code6:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:6)
                    ?popcalendar:6{prop:xpos} = 300
                    Unhide(?fault_code6:2)
                    ?fault_code6:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code6)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:6)
                    End
                End
                If required# = 1
                    ?fault_code6{prop:req} = 1
                    ?fault_code6:2{prop:req} = 1
                Else
                    ?fault_code6{prop:req} = 0
                    ?fault_code6:2{prop:req} = 0
                End

            Of 7
                Unhide(?fault_code7:prompt)
                ?tmp:OverwriteFaultCode7{prop:Hide} = 0
                ?fault_code7:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:7)
                    ?popcalendar:7{prop:xpos} = 300
                    Unhide(?fault_code7:2)
                    ?fault_code7:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code7)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:7)
                    End
                End
                If required# = 1
                    ?fault_code7{prop:req} = 1
                    ?fault_code7:2{prop:req} = 1
                Else
                    ?fault_code7{prop:req} = 0
                    ?fault_code7:2{prop:req} = 0
                End

            Of 8
                Unhide(?fault_code8:prompt)
                ?tmp:OverwriteFaultCode8{prop:Hide} = 0
                ?fault_code8:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:8)
                    ?popcalendar:8{prop:xpos} = 300
                    Unhide(?fault_code8:2)
                    ?fault_code8:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code8)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:8)
                    End
                End
                If required# = 1
                    ?fault_code8{prop:req} = 1
                    ?fault_code8:2{prop:req} = 1
                Else
                    ?fault_code8{prop:req} = 0
                    ?fault_code8:2{prop:req} = 0
                End

            Of 9
                Unhide(?fault_code9:prompt)
                ?tmp:OverwriteFaultCode9{prop:Hide} = 0
                ?fault_code9:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:9)
                    ?popcalendar:9{prop:xpos} = 300
                    Unhide(?fault_code9:2)
                    ?fault_code9:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code9)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:9)
                    End
                End
                If required# = 1
                    ?fault_code9{prop:req} = 1
                    ?fault_code9:2{prop:req} = 1
                Else
                    ?fault_code9{prop:req} = 0
                    ?fault_code9:2{prop:req} = 0
                End

            Of 10
                Unhide(?fault_code10:prompt)
                ?tmp:OverwriteFaultCode10{prop:Hide} = 0
                ?fault_code10:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:10)
                    ?popcalendar:10{prop:xpos} = 300
                    Unhide(?fault_code10:2)
                    ?fault_code10:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code10)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:10)
                    End
                End
                If required# = 1
                    ?fault_code10{prop:req} = 1
                    ?fault_code10:2{prop:req} = 1
                Else
                    ?fault_code10{prop:req} = 0
                    ?fault_code10:2{prop:req} = 0
                End

            Of 11
                Unhide(?fault_code11:prompt)
                ?tmp:OverwriteFaultCode11{prop:Hide} = 0
                ?fault_code11:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:11)
                    ?popcalendar:11{prop:xpos} = 300
                    Unhide(?fault_code11:2)
                    ?fault_code11:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code11)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:11)
                    End
                End
                If required# = 1
                    ?fault_code11{prop:req} = 1
                    ?fault_code11:2{prop:req} = 1
                Else
                    ?fault_code11{prop:req} = 0
                    ?fault_code11:2{prop:req} = 0
                End

            Of 12
                Unhide(?fault_code12:prompt)
                ?tmp:OverwriteFaultCode12{prop:Hide} = 0
                ?fault_code12:prompt{prop:text} = maf:field_name
                If maf:field_type = 'DATE'
                    Unhide(?popcalendar:12)
                    ?popcalendar:12{prop:xpos} = 300
                    Unhide(?fault_code12:2)
                    ?fault_code12:2{prop:xpos} = 232
                Else
                    Unhide(?fault_code12)
                    If maf:lookup = 'YES'
                        Unhide(?lookup1:12)
                    End
                End
                If required# = 1
                    ?fault_code12{prop:req} = 1
                    ?fault_code12:2{prop:req} = 1
                Else
                    ?fault_code12{prop:req} = 0
                    ?fault_code12:2{prop:req} = 0
                End

        End
    end !loop
    access:manfault.restorefile(save_maf_id)
    setcursor()
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
UpdateJobs      Routine

        ! Start Change 4482 BE(27/08/2004)
        ! Put Update under transaction control to allow
        ! ALL changes to be backed out in the event of validation failure.
        LOGOUT(60, JOBSE,           |
                   LOCINTER,        |
                   JOBNOTES,        |
                   STOCK,           |
                   JOBS,            |
                   PARTS,           |
                   STOHIST,         |
                   ORDPEND,         |
                   WARPARTS,        |
                   DESBATCH,        |
                   AUDIT,           |
                   PICKNOTE,        |
                   PICKDET)
        ! End Change 4482 BE(27/08/2004)

        ! Start Change 2386 BE(19/03/03)
        ! Initiliase Audit Notes String
        Audit_Notes = 'JOB UPDATED BY MULTIPLE JOB UPDATE'
        ! End Change 2386 BE(19/03/03)

        Pricing_Routine('C',labour",parts",pass",a")
        If pass" = True
            job:labour_cost = labour"
            job:parts_cost  = parts"
        Else
            job:labour_cost = 0
            job:parts_cost  = 0
        End!If pass" = True
        Pricing_Routine('W',labour",parts",pass",a")
        ! Start change 4483 BE(20/07/2004)
        !If pass_warranty" = True
        If pass" = True
        ! End change 4483 BE(20/07/2004)
            job:labour_cost_warranty    = labour"
            job:parts_cost_warranty     = parts"
        Else
            job:labour_cost_warranty = 0
            job:parts_cost_warranty = 0
        End!If pass_warranty" = True

        If workshop_tick_temp = 'Y'
            If job:workshop <> 'YES' Or (job:workshop = 'YES' And over_workshop_tick_temp = 'Y')
                job:workshop = 'YES'
                GetStatus(305,0,'JOB') !awaiting allocation
                Access:JOBSE.Clearkey(jobe:RefNumberKey)
                jobe:RefNumber = job:Ref_Number
                If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Found
                    jobe:InWorkshopDate = Today()
                    jobe:InWorkshopTime = Clock()
                    Access:JOBSE.Update()
                    ! Start Change 2386 BE(19/03/03)
                    Audit_Notes = CLIP(Audit_Notes) & '<13><10>IN WORKSHOP: ' & |
                                  FORMAT(jobe:InWorkshopDate, @D06) & ' ' |
                                  & FORMAT(jobe:InWorkshopTime, @T4)
                    ! End Change 2386 BE(19/03/03)
                Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                    !Error
                End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
            End!If job:workshop <> 'YES' Or (job:workshop = 'YES' And over_workshop_tick_temp = 'Y')
        End!If workshop_tick_temp = 'Y'
        
        if location_tick_temp = 'Y'
            If job:location = '' Or (job:location <> '' And over_location_tick = 'Y')
                If job:location <> ''
                    access:locinter.clearkey(loi:location_key)
                    loi:location = job:location
                    If access:locinter.fetch(loi:location_key) = Level:Benign
                        If loi:allocate_spaces = 'YES'
                            loi:current_spaces+= 1
                            access:locinter.update()
                        End
                    end !if

                End!If job:location = ''
                job:location = location_temp
                access:locinter.clearkey(loi:location_key)
                loi:location = job:location
                If access:locinter.fetch(loi:location_key) = Level:Benign
                    If loi:allocate_spaces = 'YES'
                        loi:current_spaces -= 1
                        If loi:current_spaces< 1
                            loi:location_available = 'NO'
                        End
                        access:locinter.update()
                    End
                End
            End!If job:location = '' Or (job:location <> '' And over_location_tick = 'Y')
        End

        If authority_number_tick_temp = 'Y'
            If job:authority_number = '' Or (job:authority_number <> '' And over_authority_number_tick = 'Y')
                job:authority_number = authority_number_temp
            End!If job:authority_number = '' Or (job:authority_number <> '' And over_authority_number_tick = 'Y')
        End!If authority_number_tick_temp = 'Y'

        If engineer_tick_temp = 'Y'
            If job:engineer = '' Or (job:engineer <> '' And over_engineer_tick = 'Y')
                job:engineer = engineer_temp
                GetStatus(310,0,'JOB') !allocated to engineer
                ! Start Change 2386 BE(19/03/03)
                Audit_Notes = CLIP(Audit_Notes) & '<13><10>ENGINEER: ' & job:engineer
                ! End Change 2386 BE(19/03/03)
            End
        End!If engineer_tick_temp = 'Y'

        If in_repair_tick_temp = 'Y'
            If job:in_repair <> 'YES' Or (job:in_repair = 'YES' And over_in_repair_tick = 'Y')
                job:in_repair = 'YES'
                job:date_in_repair = Today()
                job:time_in_repair = Clock()
                GetStatus(315,0,'JOB') !In Repair

            End
        End!If in_repair_tick_temp = 'Y'

        If on_test_tick_temp = 'Y'
            If job:on_test <> 'YES' Or (job:on_test = 'YES' And over_on_test_tick = 'Y')
                job:on_test = 'YES'
                job:date_on_test = Today()
                job:time_on_test = Clock()
                GetStatus(320,0,'JOB') !on test

            End
        End!If on_test_tick_temp = 'Y'

        If advance_payment_tick_Temp = 'Y'
            If job:advance_payment = '' Or (job:advance_payment <> '' And over_advance_payment_Tick = 'Y')
                job:advance_payment = Advance_Payment_Temp
            End
        End!If advance_payment_tick_Temp = 'Y'

        access:jobnotes.clearkey(jbn:RefNumberKey)
        jbn:RefNUmber = job:ref_number
        access:jobnotes.tryfetch(jbn:RefNumberKey)
        If invoice_text_tick_temp = 'Y'
            If over_invoice_text_tick = 'Y'
                jbn:invoice_text    = invoice_text_temp
            Else!If over_invoice_text_tick = 'Y'
                If jbn:invoice_Text = ''
                    jbn:invoice_text    = invoice_text_temp
                Else!If job:invoice_Text = ''
                    jbn:invoice_text    = Clip(jbn:invoice_text) & '<13,10>' & invoice_text_temp
                End!If job:invoice_Text = ''
            End!If over_invoice_text_tick = 'Y'
        End

        If engineers_notes_tick_temp = 'Y'
            If over_engineers_notes_tick = 'Y'
                jbn:engineers_notes = engineers_notes_temp
            Else!If over_engineers_notes_tick = 'Y'
                If jbn:engineers_notes = ''
                    jbn:engineers_notes = engineers_notes_temp
                Else!If job:engineers_notes = ''
                    jbn:engineers_notes = Clip(jbn:engineers_notes) & '<13,10>' & engineers_notes_temp
                End!If job:engineers_notes = ''
            End!If over_engineers_notes_tick = 'Y'
        End
        access:jobnotes.update()

        ! Start Change 2386 BE(19/03/03)
        FoundChargeableParts# = 0
        FoundWarrantyParts# = 0
        ! End Change 2386 BE(19/03/03)

        If charge_type_tick_temp = 'Y'
            IF Charge_Type_Temp <> ''
                If (job:Charge_Type <> '' And Over_Chargeable_Tick = 'Y') Or job:Charge_Type = ''
                    job:Charge_Type = Charge_Type_Temp
                    job:Chargeable_Job = 'YES'
                    ! Start Change 2386 BE(19/03/03)
                    Audit_Notes = CLIP(Audit_Notes) & '<13><10>CHARGEABLE REPAIR TYPE: ' & job:Charge_Type
                    ! End Change 2386 BE(19/03/03)
                End !If (job:Charge_Type <> '' And Over_Chargeable_Tick = 'Y') Or job:Charge_Type = ''
            Else !IF job:Charge_Type <> ''
                If job:Chargeable_Job = 'YES' And Over_Chargeable_Tick = 'Y'
                    !Before I make this warranty, are there any
                    !Chargeable parts?
                    ! Start Change 2386 BE(19/03/03)
                    !FoundParts# = 0
                    ! End Change 2386 BE(19/03/03)
                    Access:PARTS.ClearKey(par:Part_Number_Key)
                    par:Ref_Number  = job:Ref_Number
                    Set(par:Part_Number_Key,par:Part_Number_Key)
                    If Access:PARTS.Next() = Level:Benign
                        If par:Ref_Number = job:Ref_Number
                            ! Start Change 2386 BE(19/03/03)
                            !FoundParts# = 1
                            FoundChargeableParts# = 1
                            ! End Change 2386 BE(19/03/03)
                        End !If par:Ref_Number = job:Ref_Number
                    End !If Access:PARTS.Next() = Level:Benign

                    ! Start Change 2386 BE(19/03/03)
                    !If FoundParts# = 0
                    IF (FoundChargeableParts# = 0) THEN
                    ! End Change 2386 BE(19/03/03)
                        job:Charge_Type = ''
                        job:Chargeable_Job = 'NO'
                    ! Start Change 2386 BE(19/03/03)
                    !Else
                    !    Case MessageEx('Cannot transfer job ' & clip(job:Ref_Number) & ' from Warranty to Chargeable. It has parts attached.','ServiceBase 2000',|
                    !                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                    !        Of 1 ! &OK Button
                    !    End!Case MessageEx
                    ! End Change 2386 BE(19/03/03)
                    End !If FoundParts# = 0
                End !If job:Chargeable_Job = 'YES' And Over_Chargeable_Tick = 'Y'
            End !IF job:Charge_Type <> ''
        End!If charge_type_tick_temp = 'Y'

        If warranty_charge_type_tick_temp = 'Y'
            If Warranty_Charge_Type_Temp <> ''
                If (job:Warranty_Charge_Type <> '' And Over_Warranty_Tick = 'Y') Or job:Warranty_Charge_Type = ''
                    job:Warranty_Charge_Type = Warranty_Charge_Type_Temp
                    job:Warranty_Job = 'YES'
                    ! Start Change 2386 BE(19/03/03)
                    Audit_Notes = CLIP(Audit_Notes) & '<13><10>WARRANTY REPAIR TYPE: ' & job:Warranty_Charge_Type
                    ! End Change 2386 BE(19/03/03)
                End !If (job:Warranty_Charge_Type <> '' And Over_Warranty_Tick = 'Y') Or job:Warranty_Charge_Type = ''
            Else !If job:Warranty_Charge_Type <> ''
                If job:Warranty_Job = 'YES' And Over_Warranty_Tick = 'Y'
                    !Before I make this warranty, are there any
                    !Chargeable parts?
                    ! Start Change 2386 BE(19/03/03)
                    !FoundParts# = 0
                    ! End Change 2386 BE(19/03/03)
                    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                    wpr:Ref_Number  = job:Ref_Number
                    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    If Access:WARPARTS.Next() = Level:Benign
                        If wpr:Ref_Number = job:Ref_Number
                            ! Start Change 2386 BE(19/03/03)
                            !FoundParts# = 1
                            FoundWarrantyParts# = 1
                            ! End Change 2386 BE(19/03/03)
                        End !If par:Ref_Number = job:Ref_Number
                    End !If Access:PARTS.Next() = Level:Benign

                    ! Start Change 2386 BE(19/03/03)
                    !If FoundParts# = 0
                    If FoundWarrantyParts# = 0
                    ! End Change 2386 BE(19/03/03)
                        job:Warranty_Charge_Type = ''
                        job:Warranty_Job = 'NO'
                    ! Start Change 2386 BE(19/03/03)
                    !Else
                    !    Case MessageEx('Cannot transfer job ' & clip(job:Ref_Number) & ' from Chargeable to Warranty. It has parts attached.','ServiceBase 2000',|
                    !                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                    !        Of 1 ! &OK Button
                    !    End!Case MessageEx
                    ! End Change 2386 BE(19/03/03)
                    End !If FoundParts# = 0
                End !If job:Chargeable_Job = 'YES' And Over_Chargeable_Tick = 'Y'
            End !If job:Warranty_Charge_Type <> ''

        End!If warranty_charge_type_tick_temp = 'Y'

        ! Start Change 2386 BE(19/03/03)
        !IF (((job:Chargeable_Job = 'NO') AND (FoundWarrantyParts# <> 0)) OR |
        !     ((job:Warranty_Job = 'NO') AND (FoundChargeableParts# <> 0))) THEN
        !    MessageEx('Cannot transfer job ' & clip(job:Ref_Number) & '. It has parts attached.','ServiceBase 2000', |
        !        'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
        !ELSIF (FoundChargeableParts# <> 0) THEN
        !    DO Chargeable2Warranty
        !    job:Charge_Type = ''
        !    job:Chargeable_Job = 'NO'
        !ELSIF (FoundWarrantyParts# <> 0) THEN
        !    DO Warranty2Chargeable
        !    job:Warranty_Charge_Type = ''
        !    job:Warranty_Job = 'NO'
        !END
        ! End Change 2386 BE(19/03/03)

        ! Start Change 2720 BE(12/06/03)
        !If repair_type_tick_temp = 'Y'
        !    If job:repair_type = '' Or (job:repair_type <> '' And over_repair_type_tick = 'Y')
        !        job:repair_type = repair_type_temp
        !    End!If job:repair_type = '' Or (job:repair_type <> '' And over_repair_type_tick = 'Y')
        !End!If repair_type_tick_temp = 'Y'
        !
        !If repair_type_warranty_tick_temp = 'Y'
        !    If job:repair_type_warranty = '' Or (job:repair_type_warranty <> '' And over_repair_type_warranty_temp = 'Y')
        !        job:repair_type_warranty = repair_type_warranty_temp
        !    End!If job:repair_type_warranty '' Or (job:repair_type_warranty <> '' And over_repair_type_warranty_temp = 'Y')
        !End!If repair_type_warranty_tick_temp = 'Y'
        ! End Change 2720 BE(12/06/03)

! Start Change 4482 BE(26/07/2004)
!  Code moved to separate section so that parts list updates occur before attempting to
! transfer parts from warranty/chargeable or vice-versa.
!
!        despatch# = 0
!
!        If Auto_Complete_Tick_Temp <> 'Y'
!            ! Start Change 2386 BE(19/03/03)
!            glo:ErrorText = ''
!            !CompulsoryFieldCheck('B')
!            !If Clip(glo:ErrorText) <> ''
!            !    glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be updated due to the following error(s): <13,10>' & Clip(glo:errortext)
!            !    Error_Text
!            !    glo:errortext = ''
!            !    Exit
!            !End !If Clip(glo:ErrorText) <> ''
!            IF (((job:Chargeable_Job = 'NO') AND (FoundWarrantyParts# <> 0)) OR |
!                ((job:Warranty_Job = 'NO') AND (FoundChargeableParts# <> 0))) THEN
!                MessageEx('Cannot transfer job ' & clip(job:Ref_Number) & '. It has parts attached.','ServiceBase 2000', |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!            ELSIF (FoundChargeableParts# <> 0) THEN
!                DO Chargeable2Warranty
!                job:Charge_Type = ''
!                job:Chargeable_Job = 'NO'
!                ! start Change 2720 BE(12/06/03)
!                DO RepairParts
!                ! End Change 2720 BE(12/06/03)
!                CompulsoryFieldCheck('B')
!                ! start Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
!                END
!                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
!                ! End Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    DO Warranty2Chargeable
!                END
!            ELSIF (FoundWarrantyParts# <> 0) THEN
!                DO Warranty2Chargeable
!                job:Warranty_Charge_Type = ''
!                job:Warranty_Job = 'NO'
!                ! start Change 2720 BE(12/06/03)
!                DO RepairParts
!                ! End Change 2720 BE(12/06/03)
!                CompulsoryFieldCheck('B')
!                ! start Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
!                END
!                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
!                ! End Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    DO Chargeable2Warranty
!                END
!            ELSE
!                ! start Change 2720 BE(12/06/03)
!                DO RepairParts
!                ! End Change 2720 BE(12/06/03)
!                CompulsoryFieldCheck('B')
!                ! start Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
!                END
!                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
!                ! End Change 2720 BE(12/06/03)
!            END
!            IF (Clip(glo:ErrorText) <> '') THEN
!                glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be completed due to the following error(s): <13,10>' |
!                                              & Clip(glo:errortext)
!                Error_Text
!                glo:errortext = ''
!                Exit
!            END
!            ! End Change 2386 BE(19/03/03)
!        Else !If AutoComplete_Tick_Temp <> 'Y'
!            ! Start Change 2386 BE(19/03/03)
!            glo:ErrorText = ''
!            !CompulsoryFieldCheck('C')
!            !If Clip(glo:ErrorText) <> ''
!            !    glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be completed due to the following error(s): <13,10>' & Clip(glo:errortext)
!            !    Error_Text
!            !    glo:errortext = ''
!            !    Exit
!            !End !If Clip(glo:ErrorText) <> ''
!            IF (((job:Chargeable_Job = 'NO') AND (FoundWarrantyParts# <> 0)) OR |
!                ((job:Warranty_Job = 'NO') AND (FoundChargeableParts# <> 0))) THEN
!                MessageEx('Cannot transfer job ' & clip(job:Ref_Number) & '. It has parts attached.','ServiceBase 2000', |
!                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!            ELSIF (FoundChargeableParts# <> 0) THEN
!                DO Chargeable2Warranty
!                job:Charge_Type = ''
!                job:Chargeable_Job = 'NO'
!                ! start Change 2720 BE(12/06/03)
!                DO RepairParts
!                ! End Change 2720 BE(12/06/03)
!                CompulsoryFieldCheck('C')
!                ! start Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
!                END
!                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
!                ! End Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    DO Warranty2Chargeable
!                END
!            ELSIF (FoundWarrantyParts# <> 0) THEN
!                DO Warranty2Chargeable
!                job:Warranty_Charge_Type = ''
!                job:Warranty_Job = 'NO'
!                ! start Change 2720 BE(12/06/03)
!                DO RepairParts
!                ! End Change 2720 BE(12/06/03)
!                CompulsoryFieldCheck('C')
!                ! start Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
!                END
!                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
!                ! End Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    DO Chargeable2Warranty
!                END
!            ELSE
!                ! start Change 2720 BE(12/06/03)
!                DO RepairParts
!                ! End Change 2720 BE(12/06/03)
!                CompulsoryFieldCheck('C')
!                ! start Change 2720 BE(12/06/03)
!                IF (CLIP(glo:ErrorText) <> '') THEN
!                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
!                END
!                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
!                ! End Change 2720 BE(12/06/03)
!            END
!            IF (Clip(glo:ErrorText) <> '') THEN
!                glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be completed due to the following error(s): <13,10>' |
!                                              & Clip(glo:errortext)
!                Error_Text
!                glo:errortext = ''
!                Exit
!            END
!            ! End Change 2386 BE(19/03/03)
!        End !If AutoComplete_Tick_Temp <> 'Y'

!        If auto_complete_tick_temp = 'Y'
!            If job:date_completed = '' Or (job:date_completed <> '' And over_date_Completed_tick = 'Y')
!                !Fill in the totals with the right costs from the
!                !Price Structure.
!
!                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
!                man:Manufacturer = job:Manufacturer
!                If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!                    !Found
!                    If man:QAAtCompletion
!                        !If QA at completion is ticked, then don't actually
!                        !complete the job. That is done at QA
!                        GetStatus(605,1,'JOB')
!                        job:On_Test = 'YES'
!                        job:Date_On_Test = Today()
!                        job:Time_On_Test = Clock()
!                        ! Start Change 3879 BE(26/02/2004)
!                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
!                        jobe:RefNumber = job:Ref_number
!                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
!                            ! Start Change 4053 BE(19/03/04)
!                            !jobe:CompleteRepairType = 1
!                            !jobe:CompleteRepairDate = TODAY()
!                            !jobe:CompleteRepairTime = CLOCK()
!                            !access:jobse.update()
!                            ! Start Change 4200 BE(04/05/04)
!                            !IF (jobe:CompleteRepairDate = '') THEN
!                            !    jobe:CompleteRepairType = 1
!                            !    jobe:CompleteRepairDate = TODAY()
!                            !    jobe:CompleteRepairTime = CLOCK()
!                            !    access:jobse.update()
!                            !END
!                            access:JOBSENG.clearkey(joe:JobNumberKey)
!                            joe:JobNumber = job:Ref_Number
!                            SET(joe:JobNumberKey, joe:JobNumberKey)
!                            IF (access:JOBSENG.next() = Level:Benign) THEN
!                                IF ((jobe:CompleteRepairDate = '') OR |
!                                    (joe:DateAllocated > jobe:CompleteRepairDate) OR |
!                                    ((joe:DateAllocated = jobe:CompleteRepairDate) AND |
!                                     (joe:TimeAllocated > jobe:CompleteRepairTime))) THEN
!                                    jobe:CompleteRepairType = 1
!                                    jobe:CompleteRepairDate = TODAY()
!                                    jobe:CompleteRepairTime = CLOCK()
!                                    access:jobse.update()
!                                END
!                            END
!                            ! End Change 4200 BE(04/05/04)
!                            ! End Change 4053 BE(19/03/04)
!                        END
!                        ! End Change 3879 BE(26/02/2004)
!
!                    Else !If man:QAAtCompletion
!                        If ExchangeAccount(job:Account_Number)
!                            !Is the Trade Account used for Exchange Units
!                            !if so, then there's no need to despatch the
!                            !unit normal. Just auto fill the despatch fields
!                            !and mark the job as to return to exchange stock.
!                            ForceDespatch
!                            GetStatus(707,1,'JOB')
!                        Else !If ExchangeAccount(job:Account_Number)
!
!                            !Will the job be restocked, i.e. has, or will have, an
!                            !Exchange Unit attached, or is it a normal despatch.
!                            restock# = 0
!                            If ExchangeAccount(job:Account_Number)
!                                restock# = 1
!                            Else !If ExchangeAccount(job:Account_Number)
!                                glo:select1  = job:ref_number
!                                If ToBeExchanged()
!                                !Has this unit got an Exchange, or is it expecting an exchange
!                                    restock# = 1
!                                End!If job:exchange_unit_number <> ''
!                                !If there is an loan unit attached, assume that a despatch note is required.
!                                If job:loan_unit_number <> ''
!                                    restock# = 0
!                                End!If job:loan_unit_number <> ''
!                            End !If ExchangeAccount(job:Account_Number)
!
!                            If CompletePrintDespatch(job:Account_Number) = Level:Benign
!                                Print_Despatch_Note_Temp = 'YES'
!                            End !CompletePrintDespatch(job:Account_Number) = Level:Benign
!
!                            If restock# = 0
!
!                                !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
!                                !If either of those are ticked then don't despatch yet
!                                print_despatch_note_temp = ''
!
!                                If AccountAllowedToDespatch()
!                                    !Do we need to despatch at completion?
!                                    If DespatchAtClosing()
!                                        !Does the Trade Account have "Skip Despatch" set?
!                                        If AccountSkipDespatch(job:Account_Number) = Level:Benign
!                                            access:courier.clearkey(cou:courier_key)
!                                            cou:courier = job:courier
!                                            if access:courier.tryfetch(cou:courier_key)
!                                                Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
!                                                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                                    Of 1 ! &OK Button
!                                                End!Case MessageEx
!                                            Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
!                                                !Give the job a unique Despatch Number
!                                                !then call the despsing.inc.
!                                                !This calls the relevant exports etc, and different
!                                                !despatch procedures depending on the Courier.
!                                                !It will then print a Despatch Note and change
!                                                !The status as required.
!                                                If access:desbatch.primerecord() = Level:Benign
!                                                    If access:desbatch.tryinsert()
!                                                        access:desbatch.cancelautoinc()
!                                                    Else!If access:despatch.tryinsert()
!                                                        DespatchSingle()
!                                                        Print_Despatch_Note_Temp = ''
!                                                    End!If access:despatch.tryinsert()
!                                                End!If access:desbatch.primerecord() = Level:Benign
!                                            End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
!                                        End !If AccountSkipDespatch = Level:Benign
!                                    Else !If DespatchAtClosing()
!                                        If job:Loan_Unit_Number <> ''
!                                            GetStatus(811,1,'JOB')
!                                        End !If job:Loan_Unit_Number <> ''
!!                                        Else !job:Loan_Unit_Number <> ''
!!                                            If job:Exchange_Unit_Number <> ''    
!!                                                GetStatus(707,1,'JOB')
!!                                            Else !If job:Exchange_Unit_Number <> ''    
!!                                                GetStatus(705,1,'JOB')
!!                                            End !If job:Exchange_Unit_Number <> ''    
!!                                        End !job:Loan_Unit_Number <> ''
!                                    End !If DespatchAtClosing()
!                                End !If AccountAllowedToDespatch
!                            Else !If restock# = 0
!                                If job:Exchange_Unit_Number <> ''    
!                                    GetStatus(707,1,'JOB')
!                                Else !If job:Exchange_Unit_Number <> ''    
!                                    GetStatus(705,1,'JOB')
!                                End !If job:Exchange_Unit_Number <> ''    
!                            End !If restock# = 0
!
!                            !Does the Account care about Bouncers?
!                            Check_For_Bouncers_Temp = 1
!                            If CompleteCheckForBouncer(job:Account_Number)
!                                Check_For_Bouncers_Temp = 0
!                            End !CompleteCheckForBouncer(job:Account_Number) = Level:Benign
!
!                        End !If ExchangeAccount(job:Account_Number)
!
!                        !Fill in the relevant fields
!                        saved_ref_number_temp   = job:ref_number
!                        saved_esn_temp  = job:esn
!                        saved_msn_temp  = job:msn
!                        job:date_completed = Date_Completed_Temp
!                        job:time_completed = Clock()
!                        date_completed_temp = job:date_completed
!                        time_completed_temp = job:time_completed
!                        job:completed = 'YES'
!                        ! Start Change 3879 BE(26/02/2004)
!                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
!                        jobe:RefNumber = job:Ref_number
!                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
!                            IF (jobe:CompleteRepairDate = '') THEN
!                                jobe:CompleteRepairType = 2
!                                jobe:CompleteRepairDate = TODAY()
!                                jobe:CompleteRepairTime = CLOCK()
!                                access:jobse.update()
!                            END
!                        END
!                        ! End Change 3879 BE(26/02/2004)
!
!                        If Check_For_Bouncers_Temp
!                            !Check if the job is a Bouncer and add
!                            !it to the Bouncer List
!                            If CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
!                                !Bouncer
!                                AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
!                            Else !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
!                            End !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
!
!                        End !If Check_For_Bouncers_Temp
!
!                        !Check if this job is an EDI job
!                        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
!                        mod:Model_Number    = job:Model_Number
!                        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                            !Found
!                            job:edi = PendingJob(mod:Manufacturer)
!                            job:Manufacturer    = mod:Manufacturer
!
!                        Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!                            !Error
!                            !Assert(0,'<13,10>Fetch Error<13,10>')
!                        End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
!
!                        !Mark the job's exchange unit as being available
!                        CompleteDoExchange(job:Exchange_Unit_Number)
!
!                        ThisMakeover.SetWindow(win:form)
!                        Access:JOBS.TryUpdate()
!
!                        If ExchangeAccount(job:Account_Number)
!                        End !If ExchangeAccount(job:Account_Number)
!
!                        If print_despatch_note_temp = 'YES'
!                            If restock# = 1
!                                restocking_note
!                            Else!If restock# = 1
!                                despatch_note
!                            End!If restock# = 1
!                            glo:select1  = ''
!                        End!If print_despatch_note_temp = 'YES'
!                    End !If man:QAAtCompletion
!                Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!                    !Error
!                    !Assert(0,'<13,10>Fetch Error<13,10>')
!                End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!
!            End !Complete# = 1
!
!        End
!
!        If Fault_Code1 <> ''
!            If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!                job:Fault_Code1 = Fault_Code1
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code2 <> ''
!            If (job:Fault_Code2 <> '' And tmp:OverwriteFaultCode2) Or job:Fault_Code2 = ''
!                job:Fault_Code2 = Fault_Code2
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code3 <> ''
!            If (job:Fault_Code3 <> '' And tmp:OverwriteFaultCode3) Or job:Fault_Code3 = ''
!                job:Fault_Code3 = Fault_Code3
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code4 <> ''
!            If (job:Fault_Code4 <> '' And tmp:OverwriteFaultCode4) Or job:Fault_Code4 = ''
!                job:Fault_Code4 = Fault_Code4
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code5 <> ''
!            If (job:Fault_Code5 <> '' And tmp:OverwriteFaultCode5) Or job:Fault_Code5 = ''
!                job:Fault_Code5 = Fault_Code5
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code6 <> ''
!            If (job:Fault_Code6 <> '' And tmp:OverwriteFaultCode6) Or job:Fault_Code6 = ''
!                job:Fault_Code6 = Fault_Code6
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code7 <> ''
!            If (job:Fault_Code7 <> '' And tmp:OverwriteFaultCode7) Or job:Fault_Code7 = ''
!                job:Fault_Code7 = Fault_Code7
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code8 <> ''
!            If (job:Fault_Code8 <> '' And tmp:OverwriteFaultCode8) Or job:Fault_Code8 = ''
!                job:Fault_Code8 = Fault_Code8
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code9 <> ''
!            If (job:Fault_Code9 <> '' And tmp:OverwriteFaultCode9) Or job:Fault_Code9 = ''
!                job:Fault_Code9 = Fault_Code9
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code10 <> ''
!            If (job:Fault_Code10 <> '' And tmp:OverwriteFaultCode10) Or job:Fault_Code10 = ''
!                job:Fault_Code10 = Fault_Code10
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code11 <> ''
!            If (job:Fault_Code11 <> '' And tmp:OverwriteFaultCode11) Or job:Fault_Code11 = ''
!                job:Fault_Code11 = Fault_Code11
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!        If Fault_Code12 <> ''
!            If (job:Fault_Code12 <> '' And tmp:OverwriteFaultCode12) Or job:Fault_Code12 = ''
!                job:Fault_Code12 = Fault_Code12
!            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
!        End !If Fault_Code1 <> ''
!
!        Pricing_Routine('C',labour",parts",pass",a")
!        If pass" = True
!            job:labour_cost = labour"
!            job:parts_cost  = parts"
!        Else
!            job:labour_cost = 0
!            job:parts_cost  = 0
!        End!If pass" = True
!        Pricing_Routine('W',labour",parts",pass",a")
!        ! Start Change 4483 BE(20/07/2007)
!        !If pass_warranty" = True
!        If pass" = True
!        ! End Change 4483 BE(20/07/2007)
!            job:labour_cost_warranty    = labour"
!            job:parts_cost_warranty     = parts"
!        Else
!            job:labour_cost_warranty = 0
!            job:parts_cost_warranty = 0
!        End!If pass_warranty" = True
!
!        access:jobs.update()
!
!        ! Start Change 2386 BE(19/03/03)
!        GET(audit,0)
!        IF (access:audit.primerecord() = level:benign) THEN
!            aud:ref_number    = job:ref_number
!            aud:date          = today()
!            aud:time          = clock()
!            aud:type          = 'JOB'
!            access:users.clearkey(use:password_key)
!            use:password = glo:password
!            access:users.fetch(use:password_key)
!            aud:user = use:user_code
!            aud:action        = 'MULTIPLE JOB UPDATE'
!            aud:notes = Audit_Notes
!            access:audit.insert()
!        END
!        ! End Change 2386 BE(19/03/03)
! End Change 4482 BE(26/07/2004)
!UpdateJobs Continued...

      ! Start Change 1895 BE(19/05/03)
      DO InitMaxParts
      ! End Change 1895 BE(19/05/03)

      setcursor(cursor:wait)
      save_partmp_id = access:partstmp.savefile()
      set(partmp:part_number_key)
      loop
          if access:partstmp.next()
             break
          end !if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if

          ! Start Change 1895 BE(19/05/03)
          IF (tmpCheckChargeableParts) THEN
            DO TotalChargeableParts
            TotalPartsCost += partmp:Purchase_cost * partmp:Quantity
            IF (TotalPartsCost > tra:MaxChargeablePartsCost) THEN
                MESSAGE('Part No. ' & CLIP(partmp:part_number) & ' cannot be attached to job ' & CLIP(LEFT(FORMAT(job:ref_number, @n_8))) &'.<13,10>' & |
                        'This would exceed the Maximum Chargeable Parts Cost of the Trade Account.', |
                        'ServiceBase 2000', Icon:Question,  'OK', 1, 0)
                CYCLE
            END
          END
          ! End Change 1895 BE(19/05/03)

          get(parts,0)
          if access:parts.primerecord() = level:benign
              par:ref_number           = job:ref_number
              par:adjustment           = partmp:adjustment
              par:part_ref_number      = partmp:part_ref_number
              par:part_number     = partmp:part_number
              par:description     = partmp:description
              par:supplier        = partmp:supplier
              par:purchase_cost   = partmp:purchase_cost
              par:sale_cost       = partmp:sale_cost
              par:retail_cost     = partmp:retail_cost
              par:quantity             = partmp:quantity
              par:warranty_part        = 'NO'
              par:despatch_note_number = ''
              par:date_ordered         = ''
              par:pending_ref_number   = ''
              par:order_number         = ''
              par:order_part_number    = ''
              par:date_received        = ''
              par:status_date          = ''
              par:fault_code1    = partmp:fault_code1
              par:fault_code2    = partmp:fault_code2
              par:fault_code3    = partmp:fault_code3
              par:fault_code4    = partmp:fault_code4
              par:fault_code5    = partmp:fault_code5
              par:fault_code6    = partmp:fault_code6
              par:fault_code7    = partmp:fault_code7
              par:fault_code8    = partmp:fault_code8
              par:fault_code9    = partmp:fault_code9
              par:fault_code10   = partmp:fault_code10
              par:fault_code11   = partmp:fault_code11
              par:fault_code12   = partmp:fault_code12
              If update_stock_module_temp = 'Y'
                  par:exclude_from_order = 'YES'
              Else!If update_stock_module_temp = 'Y'
                  par:exclude_from_order  = partmp:exclude_from_order
              End!If update_stock_module_temp = 'Y'
              If par:exclude_from_order <> 'YES'                                  !If not excluded.
                  If par:part_ref_number <> ''                                    !If it's a stock part, check
                      access:stock.clearkey(sto:ref_number_key)                   !the stock levels.
                      sto:ref_number = par:part_ref_number
                      If access:stock.fetch(sto:ref_number_key) = Level:Benign
                          If sto:sundry_item <> 'YES'

                              !**** PICK NOTE CODE TH
                              if def:PickNoteNormal or def:PickNoteMainStore then
                                  CreatePickingNote (sto:Location)
                                  InsertPickingPart ('chargeable')
                              end

                             !Ignore if it's a sundry part.
                          
                              If par:quantity > sto:quantity_stock                !If there is insufficient in
                                  get(ordpend,0)                                  !stock. Create a pending order
                                  if access:ordpend.primerecord() = level:benign  !record.
                                      ope:part_ref_number = sto:ref_number
                                      ope:job_number      = job:ref_number
                                      ope:part_type       = 'JOB'
                                      ope:supplier        = par:supplier
                                      ope:part_number     = par:part_number
                                      ope:description     = par:description
                                      If sto:quantity_stock = 0
                                          ope:quantity    = partmp:quantity
                                          par:pending_ref_number = ope:ref_number
                                          sto:quantity_to_order += par:quantity
                                          access:stock.update()
                                          If access:parts.insert()
                                              access:parts.cancelautoinc()
                                          End
                                      Else!If sto:quantity_stock = 0                             
                                          !sto:quantity_to_order += par:quantity                 
                                          ope:quantity   = partmp:quantity - |   !If there are some left in stock
                                                              sto:quantity_stock  !then use these up to create the
                                          par:quantity    = sto:quantity_stock    !1st part.
                                          sto:quantity_stock = 0                                 
                                          access:stock.update()
                                          par:date_ordered = Today()
                                          If access:parts.insert()
                                              access:parts.cancelautoinc()
                                          End

                                          get(stohist,0)                                  !Add the decrement stock to
                                          if access:stohist.primerecord() = level:benign  !stock history.
                                              shi:ref_number           = sto:ref_number
                                              access:users.clearkey(use:password_key)
                                              use:password             =glo:password
                                              access:users.fetch(use:password_key)
                                              shi:user                 = use:user_code
                                              shi:date                 = today()
                                              shi:transaction_type     = 'DEC'
                                              shi:despatch_note_number = par:despatch_note_number
                                              shi:job_number           = job:ref_number
                                              shi:quantity             = par:quantity
                                              shi:purchase_cost        = sto:purchase_cost
                                              shi:sale_cost            = sto:sale_cost
                                              shi:notes                = 'STOCK DECREMENTED'
                                              if access:stohist.insert()
                                                 access:stohist.cancelautoinc()
                                              end
                                          end!if access:stohist.primerecord() = level:benign

                                          get(parts,0)
                                          if access:parts.primerecord() = level:benign        !Then create a 2nd
                                              par:ref_number           = job:ref_number       !part to link to the
                                              par:adjustment           = partmp:adjustment    !pending order record.
                                              par:part_ref_number      = partmp:part_ref_number
                                              par:part_number     = partmp:part_number
                                              par:description     = partmp:description
                                              par:supplier        = partmp:supplier
                                              par:purchase_cost   = partmp:purchase_cost
                                              par:sale_cost       = partmp:sale_cost
                                              par:retail_cost     = partmp:retail_cost
                                              par:quantity             = ope:quantity
                                              par:warranty_part        = 'NO'
                                              par:exclude_from_order   = 'NO'
                                              par:despatch_note_number = ''
                                              par:date_ordered         = ''
                                              par:pending_ref_number   = ope:ref_number
                                              par:order_number         = ''
                                              par:order_part_number    = ''
                                              par:date_received        = ''
                                              par:status_date          = ''
                                              par:fault_code1    = partmp:fault_code1
                                              par:fault_code2    = partmp:fault_code2
                                              par:fault_code3    = partmp:fault_code3
                                              par:fault_code4    = partmp:fault_code4
                                              par:fault_code5    = partmp:fault_code5
                                              par:fault_code6    = partmp:fault_code6
                                              par:fault_code7    = partmp:fault_code7
                                              par:fault_code8    = partmp:fault_code8
                                              par:fault_code9    = partmp:fault_code9
                                              par:fault_code10   = partmp:fault_code10
                                              par:fault_code11   = partmp:fault_code11
                                              par:fault_code12   = partmp:fault_code12
                                              if access:parts.insert()
                                                  access:parts.cancelautoinc()
                                              end
                                          end!if access:parts.primerecord() = level:benign
                                      End!If sto:quantity_stock = 0
                                      if access:ordpend.insert()
                                          access:ordpend.cancelautoinc()
                                      end

                                  end!if access:ordpend.primerecord() = level:benign
                              Else!If par:quantity > sto:quantity_stock
                                  par:date_ordered = Today()
                                  sto:quantity_stock -= par:quantity              !If there are sufficent in stock.
                                  If sto:quantity_stock < 0                       !Take them, and add the part.
                                      sto:quantity_stock = 0
                                  End
                                  access:stock.update()
                                  If access:parts.insert()
                                      access:parts.cancelautoinc()
                                  End
                                  get(stohist,0)                                  !Add the decrement stock to
                                  if access:stohist.primerecord() = level:benign  !stock history.
                                      shi:ref_number           = sto:ref_number
                                      access:users.clearkey(use:password_key)
                                      use:password             =glo:password
                                      access:users.fetch(use:password_key)
                                      shi:user                 = use:user_code
                                      shi:date                 = today()
                                      shi:transaction_type     = 'DEC'
                                      shi:despatch_note_number = par:despatch_note_number
                                      shi:job_number           = job:ref_number
                                      shi:quantity             = par:quantity
                                      shi:purchase_cost        = sto:purchase_cost
                                      shi:sale_cost            = sto:sale_cost
                                      shi:notes                = 'STOCK DECREMENTED'
                                      if access:stohist.insert()
                                         access:stohist.cancelautoinc()
                                      end
                                  end!if access:stohist.primerecord() = level:benign
                              End!If par:quantity > sto:quantity_stock
                          Else!If sto:sundry_item <> 'YES'
                              par:date_ordered = Today()                          !It is a sundry item. No stock
                              if access:parts.insert()                            !check.
                                  access:parts.cancelautoinc()
                              end
                          End!If sto:sundry_item <> 'YES'
                      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
                  Else!If par:part_ref_number <> ''
                      get(ordpend,0)
                      if access:ordpend.primerecord() = level:benign
                          ope:part_ref_number = ''
                          ope:job_number      = job:ref_number
                          ope:part_type       = 'JOB'
                          ope:supplier        = par:supplier
                          ope:part_number     = par:part_number
                          ope:description     = par:description
                          ope:quantity        = par:quantity
                          if access:ordpend.insert()
                              access:ordpend.cancelautoinc()
                          end
                          par:pending_ref_number  = ope:ref_number
                          If access:parts.insert()
                             access:parts.cancelautoinc()
                          End
                      end!if access:ordpend.primerecord() = level:benign
                  End!If par:part_ref_number <> ''
              Else!If par:exclude_from_order <> 'YES'
                  par:date_ordered    = Today()
                  If access:parts.insert()
                      access:parts.cancelautoinc()
                  End
              End!If par:exclude_from_order <> 'YES'
          end!if access:parts.primerecord() = level:benign
      end !loop
      access:partstmp.restorefile(save_partmp_id)
      setcursor()

      setcursor(cursor:wait)
      save_wartmp_id = access:wparttmp.savefile()
      set(wartmp:part_number_key)
      loop
          if access:wparttmp.next()
             break
          end !if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if

          get(warparts,0)
          if access:warparts.primerecord() = level:benign
              wpr:ref_number           = job:ref_number
              wpr:adjustment           = wartmp:adjustment
              wpr:part_ref_number      = wartmp:part_ref_number
              wpr:part_number     = wartmp:part_number
              wpr:description     = wartmp:description
              wpr:supplier        = wartmp:supplier
              wpr:purchase_cost   = wartmp:purchase_cost
              wpr:sale_cost       = wartmp:sale_cost
              wpr:retail_cost     = wartmp:retail_cost
              wpr:quantity             = wartmp:quantity
              wpr:warranty_part        = 'NO'
              wpr:despatch_note_number = ''
              wpr:date_ordered         = ''
              wpr:pending_ref_number   = ''
              wpr:order_number         = ''
              wpr:order_part_number    = ''
              wpr:date_received        = ''
              wpr:status_date          = ''
              wpr:fault_code1    = wartmp:fault_code1
              wpr:fault_code2    = wartmp:fault_code2
              wpr:fault_code3    = wartmp:fault_code3
              wpr:fault_code4    = wartmp:fault_code4
              wpr:fault_code5    = wartmp:fault_code5
              wpr:fault_code6    = wartmp:fault_code6
              wpr:fault_code7    = wartmp:fault_code7
              wpr:fault_code8    = wartmp:fault_code8
              wpr:fault_code9    = wartmp:fault_code9
              wpr:fault_code10   = wartmp:fault_code10
              wpr:fault_code11   = wartmp:fault_code11
              wpr:fault_code12   = wartmp:fault_code12
              If update_stock_module_temp = 'Y'
                  wpr:exclude_from_order = 'YES'
              Else!If update_stock_module_temp = 'Y'
                  wpr:exclude_from_order  = wartmp:exclude_from_order
              End!If update_stock_module_temp = 'Y'
              If wpr:exclude_from_order <> 'YES'                                  !If not excluded.
                  If wpr:part_ref_number <> ''                                    !If it's a stock part, check
                      access:stock.clearkey(sto:ref_number_key)                   !the stock levels.
                      sto:ref_number = wpr:part_ref_number
                      If access:stock.fetch(sto:ref_number_key) = Level:Benign
                          If sto:sundry_item <> 'YES'                             !Ignore if it's a sundry part.

                              !**** PICK NOTE CODE TH
                              if def:PickNoteNormal or def:PickNoteMainStore then
                                  CreatePickingNote (sto:Location)
                                  InsertPickingPart ('warranty')
                              end

                              If wpr:quantity > sto:quantity_stock                !If there is insufficient in
                                  get(ordpend,0)                                  !stock. Create a pending order
                                  if access:ordpend.primerecord() = level:benign  !record.
                                      ope:part_ref_number = sto:ref_number
                                      ope:job_number      = job:ref_number
                                      ope:part_type       = 'JOB'
                                      ope:supplier        = wpr:supplier
                                      ope:part_number     = wpr:part_number
                                      ope:description     = wpr:description
                                      If sto:quantity_stock = 0
                                          ope:quantity    = wartmp:quantity
                                          wpr:pending_ref_number = ope:ref_number
                                          sto:quantity_to_order += wpr:quantity
                                          access:stock.update()
                                          If access:warparts.insert()
                                              access:warparts.cancelautoinc()
                                          End
                                      Else!If sto:quantity_stock = 0                             
                                          !sto:quantity_to_order += wpr:quantity                 
                                          ope:quantity   = wartmp:quantity - |   !If there are some left in stock
                                                              sto:quantity_stock  !then use these up to create the
                                          wpr:quantity    = sto:quantity_stock    !1st part.
                                          sto:quantity_stock = 0                                 
                                          access:stock.update()
                                          wpr:date_ordered = Today()
                                          If access:warparts.insert()
                                              access:warparts.cancelautoinc()
                                          End

                                          get(stohist,0)                                  !Add the decrement stock to
                                          if access:stohist.primerecord() = level:benign  !stock history.
                                              shi:ref_number           = sto:ref_number
                                              access:users.clearkey(use:password_key)
                                              use:password             =glo:password
                                              access:users.fetch(use:password_key)
                                              shi:user                 = use:user_code
                                              shi:date                 = today()
                                              shi:transaction_type     = 'DEC'
                                              shi:despatch_note_number = wpr:despatch_note_number
                                              shi:job_number           = job:ref_number
                                              shi:quantity             = wpr:quantity
                                              shi:purchase_cost        = sto:purchase_cost
                                              shi:sale_cost            = sto:sale_cost
                                              shi:notes                = 'STOCK DECREMENTED'
                                              if access:stohist.insert()
                                                 access:stohist.cancelautoinc()
                                              end
                                          end!if access:stohist.primerecord() = level:benign

                                          get(warparts,0)
                                          if access:warparts.primerecord() = level:benign        !Then create a 2nd
                                              wpr:ref_number           = job:ref_number       !part to link to the
                                              wpr:adjustment           = wartmp:adjustment    !pending order record.
                                              wpr:part_ref_number      = wartmp:part_ref_number
                                              wpr:part_number     = wartmp:part_number
                                              wpr:description     = wartmp:description
                                              wpr:supplier        = wartmp:supplier
                                              wpr:purchase_cost   = wartmp:purchase_cost
                                              wpr:sale_cost       = wartmp:sale_cost
                                              wpr:retail_cost     = wartmp:retail_cost
                                              wpr:quantity             = ope:quantity
                                              wpr:warranty_part        = 'NO'
                                              wpr:exclude_from_order   = 'NO'
                                              wpr:despatch_note_number = ''
                                              wpr:date_ordered         = ''
                                              wpr:pending_ref_number   = ope:ref_number
                                              wpr:order_number         = ''
                                              wpr:order_part_number    = ''
                                              wpr:date_received        = ''
                                              wpr:status_date          = ''
                                              wpr:fault_code1    = wartmp:fault_code1
                                              wpr:fault_code2    = wartmp:fault_code2
                                              wpr:fault_code3    = wartmp:fault_code3
                                              wpr:fault_code4    = wartmp:fault_code4
                                              wpr:fault_code5    = wartmp:fault_code5
                                              wpr:fault_code6    = wartmp:fault_code6
                                              wpr:fault_code7    = wartmp:fault_code7
                                              wpr:fault_code8    = wartmp:fault_code8
                                              wpr:fault_code9    = wartmp:fault_code9
                                              wpr:fault_code10   = wartmp:fault_code10
                                              wpr:fault_code11   = wartmp:fault_code11
                                              wpr:fault_code12   = wartmp:fault_code12
                                              if access:warparts.insert()
                                                  access:warparts.cancelautoinc()
                                              end
                                          end!if access:warparts.primerecord() = level:benign
                                      End!If sto:quantity_stock = 0
                                      if access:ordpend.insert()
                                          access:ordpend.cancelautoinc()
                                      end

                                  end!if access:ordpend.primerecord() = level:benign
                              Else!If wpr:quantity > sto:quantity_stock
                                  wpr:date_ordered    = Today()
                                  sto:quantity_stock -= wpr:quantity              !If there are sufficent in stock.
                                  If sto:quantity_stock < 0                       !Take them, and add the part.
                                      sto:quantity_stock = 0
                                  End
                                  access:stock.update()
                                  If access:warparts.insert()
                                      access:warparts.cancelautoinc()
                                  End
                                  get(stohist,0)                                  !Add the decrement stock to
                                  if access:stohist.primerecord() = level:benign  !stock history.
                                      shi:ref_number           = sto:ref_number
                                      access:users.clearkey(use:password_key)
                                      use:password             =glo:password
                                      access:users.fetch(use:password_key)
                                      shi:user                 = use:user_code
                                      shi:date                 = today()
                                      shi:transaction_type     = 'DEC'
                                      shi:despatch_note_number = wpr:despatch_note_number
                                      shi:job_number           = job:ref_number
                                      shi:quantity             = wpr:quantity
                                      shi:purchase_cost        = sto:purchase_cost
                                      shi:sale_cost            = sto:sale_cost
                                      shi:notes                = 'STOCK DECREMENTED'
                                      if access:stohist.insert()
                                         access:stohist.cancelautoinc()
                                      end
                                  end!if access:stohist.primerecord() = level:benign
                              End!If wpr:quantity > sto:quantity_stock
                          Else!If sto:sundry_item <> 'YES'
                              wpr:date_ordered = Today()                          !It is a sundry item. No stock
                              if access:warparts.insert()                            !check.
                                  access:warparts.cancelautoinc()
                              end
                          End!If sto:sundry_item <> 'YES'
                      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
                  Else!If wpr:part_ref_number <> ''
                      get(ordpend,0)
                      if access:ordpend.primerecord() = level:benign
                          ope:part_ref_number = ''
                          ope:job_number      = job:ref_number
                          ope:part_type       = 'JOB'
                          ope:supplier        = wpr:supplier
                          ope:part_number     = wpr:part_number
                          ope:description     = wpr:description
                          ope:quantity        = wpr:quantity
                          if access:ordpend.insert()
                              access:ordpend.cancelautoinc()
                          end
                          wpr:pending_ref_number  = ope:ref_number
                          If access:warparts.insert()
                             access:warparts.cancelautoinc()
                          End
                      end!if access:ordpend.primerecord() = level:benign
                  End!If wpr:part_ref_number <> ''
              Else!If wpr:exclude_from_order <> 'YES'
                  wpr:date_ordered    = Today()
                  If access:warparts.insert()
                      access:warparts.cancelautoinc()
                  End
              End!If wpr:exclude_from_order <> 'YES'
          end!if access:warparts.primerecord() = level:benign
      end !loop
      access:wparttmp.restorefile(save_wartmp_id)
      setcursor()

! Update Jobs Continued 2...
! Start Change 4482 BE(26/07/2004)
! Code moved...
!Start - Makes more sense to have the fault codes filled in BEFORE the comp field check - TrkBs: 5494 (DBH: 16-03-2005)
       If Fault_Code1 <> ''
            If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
                job:Fault_Code1 = Fault_Code1
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code2 <> ''
            If (job:Fault_Code2 <> '' And tmp:OverwriteFaultCode2) Or job:Fault_Code2 = ''
                job:Fault_Code2 = Fault_Code2
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code3 <> ''
            If (job:Fault_Code3 <> '' And tmp:OverwriteFaultCode3) Or job:Fault_Code3 = ''
                job:Fault_Code3 = Fault_Code3
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code4 <> ''
            If (job:Fault_Code4 <> '' And tmp:OverwriteFaultCode4) Or job:Fault_Code4 = ''
                job:Fault_Code4 = Fault_Code4
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code5 <> ''
            If (job:Fault_Code5 <> '' And tmp:OverwriteFaultCode5) Or job:Fault_Code5 = ''
                job:Fault_Code5 = Fault_Code5
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code6 <> ''
            If (job:Fault_Code6 <> '' And tmp:OverwriteFaultCode6) Or job:Fault_Code6 = ''
                job:Fault_Code6 = Fault_Code6
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code7 <> ''
            If (job:Fault_Code7 <> '' And tmp:OverwriteFaultCode7) Or job:Fault_Code7 = ''
                job:Fault_Code7 = Fault_Code7
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code8 <> ''
            If (job:Fault_Code8 <> '' And tmp:OverwriteFaultCode8) Or job:Fault_Code8 = ''
                job:Fault_Code8 = Fault_Code8
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code9 <> ''
            If (job:Fault_Code9 <> '' And tmp:OverwriteFaultCode9) Or job:Fault_Code9 = ''
                job:Fault_Code9 = Fault_Code9
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code10 <> ''
            If (job:Fault_Code10 <> '' And tmp:OverwriteFaultCode10) Or job:Fault_Code10 = ''
                job:Fault_Code10 = Fault_Code10
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code11 <> ''
            If (job:Fault_Code11 <> '' And tmp:OverwriteFaultCode11) Or job:Fault_Code11 = ''
                job:Fault_Code11 = Fault_Code11
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        If Fault_Code12 <> ''
            If (job:Fault_Code12 <> '' And tmp:OverwriteFaultCode12) Or job:Fault_Code12 = ''
                job:Fault_Code12 = Fault_Code12
            End !If (job:Fault_Code1 <> '' And tmp:OverwriteFaultCode1) Or job:Fault_Code1 = ''
        End !If Fault_Code1 <> ''
        !End   - Makes more sense to have the fault codes filled in BEFORE the comp field check - TrkBs: 5494 (DBH: 16-03-2005)

        despatch# = 0

        If Auto_Complete_Tick_Temp <> 'Y'
            ! Start Change 2386 BE(19/03/03)
            glo:ErrorText = ''
            !CompulsoryFieldCheck('B')
            !If Clip(glo:ErrorText) <> ''
            !    glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be updated due to the following error(s): <13,10>' & Clip(glo:errortext)
            !    Error_Text
            !    glo:errortext = ''
            !    Exit
            !End !If Clip(glo:ErrorText) <> ''
            IF (((job:Chargeable_Job = 'NO') AND (FoundWarrantyParts# <> 0)) OR |
                ((job:Warranty_Job = 'NO') AND (FoundChargeableParts# <> 0))) THEN
                MessageEx('Cannot transfer job ' & clip(job:Ref_Number) & '. It has parts attached.','ServiceBase 2000', |
                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
            ELSIF (FoundChargeableParts# <> 0) THEN
                DO Chargeable2Warranty
                job:Charge_Type = ''
                job:Chargeable_Job = 'NO'
                ! start Change 2720 BE(12/06/03)
                DO RepairParts
                ! End Change 2720 BE(12/06/03)
                CompulsoryFieldCheck('B')
                ! start Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
                END
                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
                ! End Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    DO Warranty2Chargeable
                END
            ELSIF (FoundWarrantyParts# <> 0) THEN
                DO Warranty2Chargeable
                job:Warranty_Charge_Type = ''
                job:Warranty_Job = 'NO'
                ! start Change 2720 BE(12/06/03)
                DO RepairParts
                ! End Change 2720 BE(12/06/03)
                CompulsoryFieldCheck('B')
                ! start Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
                END
                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
                ! End Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    DO Chargeable2Warranty
                END
            ELSE
                ! start Change 2720 BE(12/06/03)
                DO RepairParts
                ! End Change 2720 BE(12/06/03)
                CompulsoryFieldCheck('B')
                ! start Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
                END
                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
                ! End Change 2720 BE(12/06/03)
            END
            IF (Clip(glo:ErrorText) <> '') THEN
                glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be completed due to the following error(s): <13,10>' |
                                              & Clip(glo:errortext)
                Error_Text
                glo:errortext = ''

                ! Start Change 4482 BE(27/08/2004)
                ROLLBACK
                ! End Change 4482 BE(27/08/2004)
                Exit
            END
            ! End Change 2386 BE(19/03/03)
        Else !If AutoComplete_Tick_Temp <> 'Y'
            ! Start Change 2386 BE(19/03/03)
            glo:ErrorText = ''
            !CompulsoryFieldCheck('C')
            !If Clip(glo:ErrorText) <> ''
            !    glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be completed due to the following error(s): <13,10>' & Clip(glo:errortext)
            !    Error_Text
            !    glo:errortext = ''
            !    Exit
            !End !If Clip(glo:ErrorText) <> ''
            IF (((job:Chargeable_Job = 'NO') AND (FoundWarrantyParts# <> 0)) OR |
                ((job:Warranty_Job = 'NO') AND (FoundChargeableParts# <> 0))) THEN
                MessageEx('Cannot transfer job ' & clip(job:Ref_Number) & '. It has parts attached.','ServiceBase 2000', |
                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
            ELSIF (FoundChargeableParts# <> 0) THEN
                DO Chargeable2Warranty
                job:Charge_Type = ''
                job:Chargeable_Job = 'NO'
                ! start Change 2720 BE(12/06/03)
                DO RepairParts
                ! End Change 2720 BE(12/06/03)
                CompulsoryFieldCheck('C')
                ! start Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
                END
                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
                ! End Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    DO Warranty2Chargeable
                END
            ELSIF (FoundWarrantyParts# <> 0) THEN
                DO Warranty2Chargeable
                job:Warranty_Charge_Type = ''
                job:Warranty_Job = 'NO'
                ! start Change 2720 BE(12/06/03)
                DO RepairParts
                ! End Change 2720 BE(12/06/03)
                CompulsoryFieldCheck('C')
                ! start Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
                END
                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
                ! End Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    DO Chargeable2Warranty
                END
            ELSE
                ! start Change 2720 BE(12/06/03)
                DO RepairParts
                ! End Change 2720 BE(12/06/03)
                CompulsoryFieldCheck('C')
                ! start Change 2720 BE(12/06/03)
                IF (CLIP(glo:ErrorText) <> '') THEN
                    glo:ErrorText = CLIP(glo:ErrorText) & '<13,10>'
                END
                glo:ErrorText = CLIP(glo:ErrorText) & CLIP(RepairErrorText)
                ! End Change 2720 BE(12/06/03)
            END
            IF (Clip(glo:ErrorText) <> '') THEN
                glo:errortext = 'Job Number ' & Clip(job:Ref_Number) & ' cannot be completed due to the following error(s): <13,10>' |
                                              & Clip(glo:errortext)
                Error_Text
                glo:errortext = ''
                Exit

                ! Start Change 4482 BE(27/08/2004)
                COMMIT
                ! End Change 4482 BE(27/08/2004)
            END
            ! End Change 2386 BE(19/03/03)
        End !If AutoComplete_Tick_Temp <> 'Y'

! End Change 4482 BE(26/07/2004)
! Update Job Continue 3...
! Start Change 4482 BE(26/07/2004)
! Code moved...

        If auto_complete_tick_temp = 'Y'
            If job:date_completed = '' Or (job:date_completed <> '' And over_date_Completed_tick = 'Y')
                !Fill in the totals with the right costs from the
                !Price Structure.

                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                man:Manufacturer = job:Manufacturer
                If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                    !Found
                    If man:QAAtCompletion
                        !If QA at completion is ticked, then don't actually
                        !complete the job. That is done at QA
                        GetStatus(605,1,'JOB')
                        job:On_Test = 'YES'
                        job:Date_On_Test = Today()
                        job:Time_On_Test = Clock()
                        ! Start Change 3879 BE(26/02/2004)
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                            ! Start Change 4053 BE(19/03/04)
                            !jobe:CompleteRepairType = 1
                            !jobe:CompleteRepairDate = TODAY()
                            !jobe:CompleteRepairTime = CLOCK()
                            !access:jobse.update()
                            ! Start Change 4200 BE(04/05/04)
                            !IF (jobe:CompleteRepairDate = '') THEN
                            !    jobe:CompleteRepairType = 1
                            !    jobe:CompleteRepairDate = TODAY()
                            !    jobe:CompleteRepairTime = CLOCK()
                            !    access:jobse.update()
                            !END
                            access:JOBSENG.clearkey(joe:JobNumberKey)
                            joe:JobNumber = job:Ref_Number
                            SET(joe:JobNumberKey, joe:JobNumberKey)
                            IF (access:JOBSENG.next() = Level:Benign) THEN
                                IF ((jobe:CompleteRepairDate = '') OR |
                                    (joe:DateAllocated > jobe:CompleteRepairDate) OR |
                                    ((joe:DateAllocated = jobe:CompleteRepairDate) AND |
                                     (joe:TimeAllocated > jobe:CompleteRepairTime))) THEN
                                    jobe:CompleteRepairType = 1
                                    jobe:CompleteRepairDate = TODAY()
                                    jobe:CompleteRepairTime = CLOCK()
                                    access:jobse.update()
                                END
                            Else !IF (access:JOBSENG.next() = Level:Benign) THEN
                                !Start - Oop!. Forget to allow for there being no engineer history (DBH: 21-03-2005)
                                If jobe:CompleteRepairDate = ''
                                    jobe:CompleteRepairType = 1
                                    jobe:CompleteRepairDate = TODAY()
                                    jobe:CompleteRepairTime = CLOCK()
                                    access:jobse.update()
                                End ! If jobe:CompleteRepairDate = ''
                                !End   - Oop!. Forget to allow for there being no engineer history (DBH: 21-03-2005)
                            END !IF (access:JOBSENG.next() = Level:Benign) THEN

                            ! End Change 4200 BE(04/05/04)
                            ! End Change 4053 BE(19/03/04)
                        END
                        ! End Change 3879 BE(26/02/2004)

                    Else !If man:QAAtCompletion
                        If ExchangeAccount(job:Account_Number)
                            !Is the Trade Account used for Exchange Units
                            !if so, then there's no need to despatch the
                            !unit normal. Just auto fill the despatch fields
                            !and mark the job as to return to exchange stock.
                            ForceDespatch
                            GetStatus(707,1,'JOB')
                        Else !If ExchangeAccount(job:Account_Number)

                            !Will the job be restocked, i.e. has, or will have, an
                            !Exchange Unit attached, or is it a normal despatch.
                            restock# = 0
                            If ExchangeAccount(job:Account_Number)
                                restock# = 1
                            Else !If ExchangeAccount(job:Account_Number)
                                glo:select1  = job:ref_number
                                If ToBeExchanged()
                                !Has this unit got an Exchange, or is it expecting an exchange
                                    restock# = 1
                                End!If job:exchange_unit_number <> ''
                                !If there is an loan unit attached, assume that a despatch note is required.
                                If job:loan_unit_number <> ''
                                    restock# = 0
                                End!If job:loan_unit_number <> ''
                            End !If ExchangeAccount(job:Account_Number)

                            If CompletePrintDespatch(job:Account_Number) = Level:Benign
                                Print_Despatch_Note_Temp = 'YES'
                            End !CompletePrintDespatch(job:Account_Number) = Level:Benign

                            If restock# = 0

                                !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
                                !If either of those are ticked then don't despatch yet
                                print_despatch_note_temp = ''

                                If AccountAllowedToDespatch()
                                    !Do we need to despatch at completion?
                                    If DespatchAtClosing()
                                        !Does the Trade Account have "Skip Despatch" set?
                                        If AccountSkipDespatch(job:Account_Number) = Level:Benign
                                            access:courier.clearkey(cou:courier_key)
                                            cou:courier = job:courier
                                            if access:courier.tryfetch(cou:courier_key)
                                                Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
                                                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                    Of 1 ! &OK Button
                                                End!Case MessageEx
                                            Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                                !Give the job a unique Despatch Number
                                                !then call the despsing.inc.
                                                !This calls the relevant exports etc, and different
                                                !despatch procedures depending on the Courier.
                                                !It will then print a Despatch Note and change
                                                !The status as required.
                                                If access:desbatch.primerecord() = Level:Benign
                                                    If access:desbatch.tryinsert()
                                                        access:desbatch.cancelautoinc()
                                                    Else!If access:despatch.tryinsert()
                                                        DespatchSingle()
                                                        Print_Despatch_Note_Temp = ''
                                                    End!If access:despatch.tryinsert()
                                                End!If access:desbatch.primerecord() = Level:Benign
                                            End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                        End !If AccountSkipDespatch = Level:Benign
                                    Else !If DespatchAtClosing()
                                        If job:Loan_Unit_Number <> ''
                                            GetStatus(811,1,'JOB')
                                        End !If job:Loan_Unit_Number <> ''
!                                        Else !job:Loan_Unit_Number <> ''
!                                            If job:Exchange_Unit_Number <> ''    
!                                                GetStatus(707,1,'JOB')
!                                            Else !If job:Exchange_Unit_Number <> ''    
!                                                GetStatus(705,1,'JOB')
!                                            End !If job:Exchange_Unit_Number <> ''    
!                                        End !job:Loan_Unit_Number <> ''
                                    End !If DespatchAtClosing()
                                End !If AccountAllowedToDespatch
                            Else !If restock# = 0
                                If job:Exchange_Unit_Number <> ''    
                                    GetStatus(707,1,'JOB')
                                Else !If job:Exchange_Unit_Number <> ''    
                                    GetStatus(705,1,'JOB')
                                End !If job:Exchange_Unit_Number <> ''    
                            End !If restock# = 0

                            !Does the Account care about Bouncers?
                            Check_For_Bouncers_Temp = 1
                            If CompleteCheckForBouncer(job:Account_Number)
                                Check_For_Bouncers_Temp = 0
                            End !CompleteCheckForBouncer(job:Account_Number) = Level:Benign

                        End !If ExchangeAccount(job:Account_Number)

                        !Fill in the relevant fields
                        saved_ref_number_temp   = job:ref_number
                        saved_esn_temp  = job:esn
                        saved_msn_temp  = job:msn
                        job:date_completed = Date_Completed_Temp
                        job:time_completed = Clock()
                        date_completed_temp = job:date_completed
                        time_completed_temp = job:time_completed
                        job:completed = 'YES'
                        ! Start Change 3879 BE(26/02/2004)
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                            IF (jobe:CompleteRepairDate = '') THEN
                                jobe:CompleteRepairType = 2
                                jobe:CompleteRepairDate = TODAY()
                                jobe:CompleteRepairTime = CLOCK()
                                access:jobse.update()
                            END
                        END
                        ! End Change 3879 BE(26/02/2004)

                        If Check_For_Bouncers_Temp
                            !Check if the job is a Bouncer and add
                            !it to the Bouncer List
                            If CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
                                !Bouncer
                                AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                            Else !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                            End !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

                        End !If Check_For_Bouncers_Temp

                        !Check if this job is an EDI job
                        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                        mod:Model_Number    = job:Model_Number
                        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                            !Found
                            job:edi = PendingJob(mod:Manufacturer)
                            job:Manufacturer    = mod:Manufacturer

                        Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

                        !Mark the job's exchange unit as being available
                        CompleteDoExchange(job:Exchange_Unit_Number)

                        !ThisMakeover.SetWindow(win:form)
                        Access:JOBS.TryUpdate()

                        If ExchangeAccount(job:Account_Number)
                        End !If ExchangeAccount(job:Account_Number)

                        If print_despatch_note_temp = 'YES'
                            If restock# = 1
                                restocking_note
                            Else!If restock# = 1
                                despatch_note
                            End!If restock# = 1
                            glo:select1  = ''
                        End!If print_despatch_note_temp = 'YES'
                    End !If man:QAAtCompletion
                Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

            End !Complete# = 1

        End

        Pricing_Routine('C',labour",parts",pass",a")
        If pass" = True
            job:labour_cost = labour"
            job:parts_cost  = parts"
        Else
            job:labour_cost = 0
            job:parts_cost  = 0
        End!If pass" = True
        Pricing_Routine('W',labour",parts",pass",a")
        ! Start Change 4483 BE(20/07/2007)
        !If pass_warranty" = True
        If pass" = True
        ! End Change 4483 BE(20/07/2007)
            job:labour_cost_warranty    = labour"
            job:parts_cost_warranty     = parts"
        Else
            job:labour_cost_warranty = 0
            job:parts_cost_warranty = 0
        End!If pass_warranty" = True

        !Start - Recheck the job's EDI position. It may of changed from chargeable to warranty and vice versa - TrkBs: 5039 (DBH: 07-12-2004)
        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
        mod:Model_Number    = job:Model_Number
        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Found
            job:edi = PendingJob(mod:Manufacturer)
            job:Manufacturer    = mod:Manufacturer

        Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
        !End   - Recheck the job's EDI position. It may of changed from chargeable to warranty and vice versa - TrkBs: 5039 (DBH: 07-12-2004)

        access:jobs.update()

        ! Start Change 2386 BE(19/03/03)
        GET(audit,0)
        IF (access:audit.primerecord() = level:benign) THEN
            aud:ref_number    = job:ref_number
            aud:date          = today()
            aud:time          = clock()
            aud:type          = 'JOB'
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'MULTIPLE JOB UPDATE'
            aud:notes = Audit_Notes
            access:audit.insert()
        END
        ! End Change 2386 BE(19/03/03)

        ! Start Change 4482 BE(27/08/2004)
        COMMIT
        ! End Change 4482 BE(27/08/2004)

! End Change 4482 BE(26/07/2004)
LookForDifferences       Routine
        recordspercycle     = 25
        recordsprocessed    = 0
        percentprogress     = 0
        setcursor(cursor:wait)
        open(progresswindow)
        progress:thermometer    = 0
        ?progress:pcttext{prop:text} = '0% Completed'
        recordstoprocess    = Records(glo:q_jobnumber)
        first_job# = 1
        difference_temp = 0
        job_count_temp = 0
        Loop x# = 1 to RecordsToProcess
            Get(glo:q_jobnumber,x#)
            Do GetNextRecord2
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = glo:job_number_pointer
            If access:jobs.fetch(job:ref_number_key) = Level:Benign
                job_count_temp = 1
                If first_job# = 1
                    model_number_temp    = job:model_number
                    manufacturer_temp    = job:manufacturer
                    first_job# = 0
                Else
                    If job:model_number <> model_number_temp
                        difference_temp = 1
                        Break
                    End
                End
            End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
        End!Loop x# = 1 to RecordsToProcess
        Close(Progresswindow)
        ! Start Change 2720 BE(11/06/2003)
        SETCURSOR
        ! End Change 2720 BE(11/06/2003)

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Multiple_Job_Update_Wizard')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:JOBS2_ALIAS.Open
  Relate:PARTSTMP.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:REPTYDEF.Open
  Relate:STATUS.Open
  Relate:VATCODE.Open
  Relate:WPARTTMP.Open
  Access:JOBS.UseFile
  Access:PARTS.UseFile
  Access:CHARTYPE.UseFile
  Access:MANFAULT.UseFile
  Access:ORDPEND.UseFile
  Access:STOCK.UseFile
  Access:WARPARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:STOHIST.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:JOBSE.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:JOBSENG.UseFile
  Access:COURIER.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:PARTSTMP,SELF)
  BRW6.Init(?List:2,Queue:Browse:1.ViewPosition,BRW6::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  BRW24.Init(?List:3,Queue:Browse:2.ViewPosition,BRW24::View:Browse,Queue:Browse:2,Relate:JOBS,SELF)
  BRW31.Init(?List:4,Queue:Browse:3.ViewPosition,BRW31::View:Browse,Queue:Browse:3,Relate:WPARTTMP,SELF)
  Case MessageEx('Do you want to update a batch?','ServiceBase 2000',|
                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,14084079,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      Of 1 ! &Yes Button
          Select_Batch_Number
          If glo:select23 = 'QUIT'
              Return(Level:Fatal)
          End
      Of 2 ! &No Button
          glo:Select23 = ''
          glo:Select24 = ''
  End!Case MessageEx
  
  
  OPEN(Window)
  SELF.Opened=True
  Set(defaults)
  access:defaults.next()
  Hide(?Finish_Button)
  Select(?Sheet1,TabNumber)
  Do Fault_Coding
  date_completed_temp = Today()
  Do RecolourWindow
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
    Wizard36.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish_Button, |                ! OK button
                     ?CancelButton, |                 ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ?date_completed_temp{Prop:Alrt,255} = MouseLeft2
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,partmp:Part_Number_Key)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,partmp:Ref_Number,1,BRW4)
  BRW4.AddField(partmp:Part_Number,BRW4.Q.partmp:Part_Number)
  BRW4.AddField(partmp:Description,BRW4.Q.partmp:Description)
  BRW4.AddField(partmp:record_number,BRW4.Q.partmp:record_number)
  BRW4.AddField(partmp:Ref_Number,BRW4.Q.partmp:Ref_Number)
  BRW6.Q &= Queue:Browse:1
  BRW6::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW6.AddSortOrder(BRW6::Sort1:StepClass,job:Batch_Status_Key)
  BRW6.AddRange(job:Current_Status,status_location1_temp)
  BRW6.AddLocator(BRW6::Sort1:Locator)
  BRW6::Sort1:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW6)
  BRW6.SetFilter('(Upper(job:batch_number) = Upper(glo:select24))')
  BRW6.AddSortOrder(,job:BatchModelNoKey)
  BRW6.AddRange(job:Model_Number,model_number1_temp)
  BRW6.AddLocator(BRW6::Sort2:Locator)
  BRW6::Sort2:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW6)
  BRW6.SetFilter('(Upper(job:batch_number) = Upper(glo:select24))')
  BRW6::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW6.AddSortOrder(BRW6::Sort0:StepClass,job:Batch_Number_Key)
  BRW6.AddRange(job:Batch_Number,GLO:Select24)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW6)
  BIND('tag_temp',tag_temp)
  BIND('batch_number_temp',batch_number_temp)
  BIND('status_location1_temp',status_location1_temp)
  BIND('GLO:Select24',GLO:Select24)
  BIND('model_number1_temp',model_number1_temp)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tag_temp,BRW6.Q.tag_temp)
  BRW6.AddField(job:Ref_Number,BRW6.Q.job:Ref_Number)
  BRW6.AddField(job:Model_Number,BRW6.Q.job:Model_Number)
  BRW6.AddField(job:Account_Number,BRW6.Q.job:Account_Number)
  BRW6.AddField(job:Current_Status,BRW6.Q.job:Current_Status)
  BRW6.AddField(job:Batch_Number,BRW6.Q.job:Batch_Number)
  BRW24.Q &= Queue:Browse:2
  BRW24.AddSortOrder(,job:By_Status)
  BRW24.AddRange(job:Current_Status,status_location2_temp)
  BRW24.AddLocator(BRW24::Sort1:Locator)
  BRW24::Sort1:Locator.Init(?JOB:Ref_Number:2,job:Ref_Number,1,BRW24)
  BRW24.AddSortOrder(,job:Model_Number_Key)
  BRW24.AddRange(job:Model_Number,model_number2_temp)
  BRW24.AddLocator(BRW24::Sort2:Locator)
  BRW24::Sort2:Locator.Init(?JOB:Ref_Number:2,job:Model_Number,1,BRW24)
  BRW24.AppendOrder('job:Ref_Number')
  BRW24.AddSortOrder(,job:Ref_Number_Key)
  BRW24.AddLocator(BRW24::Sort0:Locator)
  BRW24::Sort0:Locator.Init(?JOB:Ref_Number:2,job:Ref_Number,1,BRW24)
  BIND('tag2_temp',tag2_temp)
  BIND('status_location2_temp',status_location2_temp)
  BIND('model_number2_temp',model_number2_temp)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW24.AddField(tag2_temp,BRW24.Q.tag2_temp)
  BRW24.AddField(job:Ref_Number,BRW24.Q.job:Ref_Number)
  BRW24.AddField(job:Model_Number,BRW24.Q.job:Model_Number)
  BRW24.AddField(job:Account_Number,BRW24.Q.job:Account_Number)
  BRW24.AddField(job:Current_Status,BRW24.Q.job:Current_Status)
  BRW31.Q &= Queue:Browse:3
  BRW31.AddSortOrder(,wartmp:Part_Number_Key)
  BRW31.AddLocator(BRW31::Sort0:Locator)
  BRW31::Sort0:Locator.Init(,wartmp:Ref_Number,1,BRW31)
  BRW31.AddField(wartmp:Part_Number,BRW31.Q.wartmp:Part_Number)
  BRW31.AddField(wartmp:Description,BRW31.Q.wartmp:Description)
  BRW31.AddField(wartmp:record_number,BRW31.Q.wartmp:record_number)
  BRW31.AddField(wartmp:Ref_Number,BRW31.Q.wartmp:Ref_Number)
  IF ?workshop_tick_temp{Prop:Checked} = True
    UNHIDE(?over_workshop_tick_temp)
  END
  IF ?workshop_tick_temp{Prop:Checked} = False
    HIDE(?over_workshop_tick_temp)
  END
  IF ?location_tick_temp{Prop:Checked} = True
    UNHIDE(?location_temp)
    UNHIDE(?over_location_tick)
    UNHIDE(?Lookup_Internal_Location)
  END
  IF ?location_tick_temp{Prop:Checked} = False
    location_temp = ''
    HIDE(?location_temp)
    HIDE(?over_location_tick)
    HIDE(?Lookup_Internal_Location)
  END
  IF ?authority_number_tick_temp{Prop:Checked} = True
    UNHIDE(?authority_number_temp)
    UNHIDE(?over_authority_number_tick)
  END
  IF ?authority_number_tick_temp{Prop:Checked} = False
    authority_number_temp = ''
    HIDE(?authority_number_temp)
    HIDE(?over_authority_number_tick)
  END
  IF ?engineer_tick_temp{Prop:Checked} = True
    UNHIDE(?engineer_name_temp)
    UNHIDE(?engineer_temp)
    UNHIDE(?Lookup_Engineer)
    UNHIDE(?over_engineer_tick)
  END
  IF ?engineer_tick_temp{Prop:Checked} = False
    engineer_temp = ''
    HIDE(?engineer_name_temp)
    HIDE(?engineer_temp)
    HIDE(?Lookup_Engineer)
    HIDE(?over_engineer_tick)
  END
  IF ?Engineers_Notes_Tick_Temp{Prop:Checked} = True
    UNHIDE(?engineers_notes_temp)
    UNHIDE(?lookup_engineers_notes)
    UNHIDE(?over_engineers_notes_tick)
  END
  IF ?Engineers_Notes_Tick_Temp{Prop:Checked} = False
    engineers_notes_temp = ''
    HIDE(?engineers_notes_temp)
    HIDE(?lookup_engineers_notes)
    HIDE(?over_engineers_notes_tick)
  END
  IF ?Invoice_Text_Tick_Temp{Prop:Checked} = True
    UNHIDE(?invoice_text_temp)
    UNHIDE(?Lookup_Invoice_Text)
    UNHIDE(?over_invoice_Text_tick)
  END
  IF ?Invoice_Text_Tick_Temp{Prop:Checked} = False
    invoice_text_temp = ''
    HIDE(?invoice_text_temp)
    HIDE(?Lookup_Invoice_Text)
    HIDE(?over_invoice_Text_tick)
  END
  IF ?charge_type_tick_temp{Prop:Checked} = True
    UNHIDE(?Charge_Type_temp)
    UNHIDE(?over_chargeable_tick)
    UNHIDE(?Lookup_Charge_Type)
  END
  IF ?charge_type_tick_temp{Prop:Checked} = False
    Charge_Type_temp = ''
    HIDE(?Charge_Type_temp)
    HIDE(?over_chargeable_tick)
    HIDE(?Lookup_Charge_Type)
  END
  IF ?warranty_charge_type_tick_temp{Prop:Checked} = True
    UNHIDE(?Warranty_Charge_Type_temp)
    UNHIDE(?over_warranty_tick)
    UNHIDE(?Lookup_Warranty_Charge_Type)
  END
  IF ?warranty_charge_type_tick_temp{Prop:Checked} = False
    Warranty_Charge_Type_temp = ''
    HIDE(?Warranty_Charge_Type_temp)
    HIDE(?over_warranty_tick)
    HIDE(?Lookup_Warranty_Charge_Type)
  END
  IF ?in_repair_tick_temp{Prop:Checked} = True
    UNHIDE(?over_in_repair_tick)
  END
  IF ?in_repair_tick_temp{Prop:Checked} = False
    HIDE(?over_in_repair_tick)
  END
  IF ?On_Test_Tick_Temp{Prop:Checked} = True
    UNHIDE(?over_on_test_tick)
  END
  IF ?On_Test_Tick_Temp{Prop:Checked} = False
    HIDE(?over_on_test_tick)
  END
  IF ?auto_complete_tick_temp{Prop:Checked} = True
    UNHIDE(?PopCalendar:13)
    UNHIDE(?date_completed_temp)
    HIDE(?over_date_completed_tick)
  END
  IF ?auto_complete_tick_temp{Prop:Checked} = False
    HIDE(?PopCalendar:13)
    HIDE(?date_completed_temp)
    HIDE(?over_date_completed_tick)
  END
  IF ?repair_type_warranty_tick_temp{Prop:Checked} = True
    UNHIDE(?over_repair_type_warranty_temp)
    UNHIDE(?Lookup_Repair_Type_Warranty)
    UNHIDE(?Repair_Type_Warranty_Temp)
  END
  IF ?repair_type_warranty_tick_temp{Prop:Checked} = False
    HIDE(?Repair_Type_Warranty_Temp)
    HIDE(?Lookup_Repair_Type_Warranty)
    HIDE(?over_repair_type_warranty_temp)
  END
  IF ?repair_type_tick_temp{Prop:Checked} = True
    UNHIDE(?Repair_Type_Temp)
    UNHIDE(?over_repair_type_tick)
    UNHIDE(?Lookup_Repair_Type)
  END
  IF ?repair_type_tick_temp{Prop:Checked} = False
    Repair_Type_Temp = ''
    HIDE(?Repair_Type_Temp)
    HIDE(?over_repair_type_tick)
    HIDE(?Lookup_Repair_Type)
  END
  FDCB10.Init(status_location1_temp,?status_location1_temp,Queue:FileDropCombo:2.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:2,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:2
  FDCB10.AddSortOrder(sts:Status_Key)
  FDCB10.AddField(sts:Status,FDCB10.Q.sts:Status)
  FDCB10.AddField(sts:Ref_Number,FDCB10.Q.sts:Ref_Number)
  FDCB10.AddUpdateField(sts:Status,status_location1_temp)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB37.Init(model_number2_temp,?model_number2_temp,Queue:FileDropCombo:5.ViewPosition,FDCB37::View:FileDropCombo,Queue:FileDropCombo:5,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB37.Q &= Queue:FileDropCombo:5
  FDCB37.AddSortOrder(mod:Model_Number_Key)
  FDCB37.AddField(mod:Model_Number,FDCB37.Q.mod:Model_Number)
  FDCB37.AddUpdateField(mod:Model_Number,model_number2_temp)
  ThisWindow.AddItem(FDCB37.WindowComponent)
  FDCB39.Init(status_location2_temp,?status_location2_temp,Queue:FileDropCombo:3.ViewPosition,FDCB39::View:FileDropCombo,Queue:FileDropCombo:3,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB39.Q &= Queue:FileDropCombo:3
  FDCB39.AddSortOrder(sts:Status_Key)
  FDCB39.AddField(sts:Status,FDCB39.Q.sts:Status)
  FDCB39.AddField(sts:Ref_Number,FDCB39.Q.sts:Ref_Number)
  FDCB39.AddUpdateField(sts:Status,status_location2_temp)
  ThisWindow.AddItem(FDCB39.WindowComponent)
  FDCB2.Init(model_number1_temp,?model_number1_temp,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(mod:Model_Number_Key)
  FDCB2.AddField(mod:Model_Number,FDCB2.Q.mod:Model_Number)
  FDCB2.AddUpdateField(mod:Model_Number,model_number1_temp)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  BRW4.AskProcedure = 1
  BRW31.AskProcedure = 2
  FileLookup35.Init
  FileLookup35.Flags=BOR(FileLookup35.Flags,FILE:LongName)
  FileLookup35.SetMask('Text Files','*.txt')
  FileLookup35.WindowTitle='Import File'
  BRW4.AddToolbarTarget(Toolbar)
  BRW31.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:q_JobNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:q_JobNumber)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:q_JobNumber)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:q_JobNumber)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:JOBS2_ALIAS.Close
    Relate:PARTSTMP.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:REPTYDEF.Close
    Relate:STATUS.Close
    Relate:VATCODE.Close
    Relate:WPARTTMP.Close
  END
  Remove(partstmp)
  Remove(wparttmp)
  
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  glo:select12 = model_number_temp
  glo:select11 = manufacturer_temp
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_TempParts
      Update_TempWarParts
    END
    ReturnValue = GlobalResponse
  END
  glo:select12 = ''
  glo:select11 = ''
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard36.Validate()
        DISABLE(Wizard36.NextControl())
     ELSE
        ENABLE(Wizard36.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Lookup_Charge_Type
      glo:select1 = 'NO'
    OF ?Lookup_Warranty_Charge_Type
      glo:select1 = 'YES'
    OF ?Lookup1
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 1
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code1 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:2
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 2
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code2 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:3
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 3
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code3 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:4
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 4
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code4 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:5
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 5
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code5 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:6
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 6
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code6 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:7
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 7
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code7 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:8
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 8
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code8 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:9
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 9
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code9 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:10
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 10
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code10 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:11
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 11
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code11 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup1:12
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = manufacturer_temp
      glo:select2  = 12
      glo:select3  = ''
      browse_manufacturer_fault_lookup
      if globalresponse = requestcompleted
          fault_code12 = mfo:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Lookup_Repair_Type_Warranty
      glo:select1 = model_number_temp
    OF ?Lookup_Repair_Type
      glo:select1 = model_number_temp
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?workshop_tick_temp
      IF ?workshop_tick_temp{Prop:Checked} = True
        UNHIDE(?over_workshop_tick_temp)
      END
      IF ?workshop_tick_temp{Prop:Checked} = False
        HIDE(?over_workshop_tick_temp)
      END
      ThisWindow.Reset
    OF ?location_tick_temp
      IF ?location_tick_temp{Prop:Checked} = True
        UNHIDE(?location_temp)
        UNHIDE(?over_location_tick)
        UNHIDE(?Lookup_Internal_Location)
      END
      IF ?location_tick_temp{Prop:Checked} = False
        location_temp = ''
        HIDE(?location_temp)
        HIDE(?over_location_tick)
        HIDE(?Lookup_Internal_Location)
      END
      ThisWindow.Reset
    OF ?Lookup_Internal_Location
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Available_Locations
      ThisWindow.Reset
      case globalresponse
          of requestcompleted
              location_temp = loi:location  
              select(?+2)
          of requestcancelled
              location_temp = ''
              select(?-1)
      end!case globalreponse
      display(?location_temp)
    OF ?location_temp
      access:locinter.clearkey(loi:location_available_key)
      loi:location_available = location_temp
      loi:location           = 'YES'
      if access:locinter.fetch(loi:location_available_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          press(clip(location_temp))
          browse_available_locations
          if globalresponse = requestcompleted
              location_temp = loi:location
          else
              location_temp = ''
              select(?-1)
          end
          display(?location_temp)
          globalrequest     = saverequest#
      end!if access:locinter.fetch(loi:location_available_key)
    OF ?authority_number_tick_temp
      IF ?authority_number_tick_temp{Prop:Checked} = True
        UNHIDE(?authority_number_temp)
        UNHIDE(?over_authority_number_tick)
      END
      IF ?authority_number_tick_temp{Prop:Checked} = False
        authority_number_temp = ''
        HIDE(?authority_number_temp)
        HIDE(?over_authority_number_tick)
      END
      ThisWindow.Reset
    OF ?engineer_tick_temp
      IF ?engineer_tick_temp{Prop:Checked} = True
        UNHIDE(?engineer_name_temp)
        UNHIDE(?engineer_temp)
        UNHIDE(?Lookup_Engineer)
        UNHIDE(?over_engineer_tick)
      END
      IF ?engineer_tick_temp{Prop:Checked} = False
        engineer_temp = ''
        HIDE(?engineer_name_temp)
        HIDE(?engineer_temp)
        HIDE(?Lookup_Engineer)
        HIDE(?over_engineer_tick)
      END
      ThisWindow.Reset
    OF ?Lookup_Engineer
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browse_users_job_assignment
      if globalresponse = requestcompleted
          engineer_temp = use:user_code
          engineer_name_temp   = Clip(use:forename) & ' ' & Clip(use:surname)
          display()
      end
      globalrequest     = saverequest#
    OF ?Engineers_Notes_Tick_Temp
      IF ?Engineers_Notes_Tick_Temp{Prop:Checked} = True
        UNHIDE(?engineers_notes_temp)
        UNHIDE(?lookup_engineers_notes)
        UNHIDE(?over_engineers_notes_tick)
      END
      IF ?Engineers_Notes_Tick_Temp{Prop:Checked} = False
        engineers_notes_temp = ''
        HIDE(?engineers_notes_temp)
        HIDE(?lookup_engineers_notes)
        HIDE(?over_engineers_notes_tick)
      END
      ThisWindow.Reset
    OF ?lookup_engineers_notes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Engineers_Notes
      ThisWindow.Reset
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If engineers_notes_temp = ''
                  engineers_notes_temp    = Clip(glo:notes_pointer)
              Else !If job:invoice_text = ''
                  engineers_notes_temp = Clip(engineers_notes_temp) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:invoice_text = ''
          End
          Display()
      End
    OF ?Invoice_Text_Tick_Temp
      IF ?Invoice_Text_Tick_Temp{Prop:Checked} = True
        UNHIDE(?invoice_text_temp)
        UNHIDE(?Lookup_Invoice_Text)
        UNHIDE(?over_invoice_Text_tick)
      END
      IF ?Invoice_Text_Tick_Temp{Prop:Checked} = False
        invoice_text_temp = ''
        HIDE(?invoice_text_temp)
        HIDE(?Lookup_Invoice_Text)
        HIDE(?over_invoice_Text_tick)
      END
      ThisWindow.Reset
    OF ?Lookup_Invoice_Text
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Invoice_Text
      ThisWindow.Reset
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If invoice_text_temp = ''
                  invoice_text_temp    = Clip(glo:notes_pointer)
              Else !If job:invoice_text = ''
                  invoice_text_temp = Clip(invoice_text_temp) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:invoice_text = ''
          End
          Display()
      End
    OF ?charge_type_tick_temp
      IF ?charge_type_tick_temp{Prop:Checked} = True
        UNHIDE(?Charge_Type_temp)
        UNHIDE(?over_chargeable_tick)
        UNHIDE(?Lookup_Charge_Type)
      END
      IF ?charge_type_tick_temp{Prop:Checked} = False
        Charge_Type_temp = ''
        HIDE(?Charge_Type_temp)
        HIDE(?over_chargeable_tick)
        HIDE(?Lookup_Charge_Type)
      END
      ThisWindow.Reset
    OF ?Lookup_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              charge_type_temp = cha:charge_type  
              select(?+2)
          of requestcancelled
              charge_type_temp = ''
              select(?-1)
      end!case globalreponse
      display(?charge_type_temp)
    OF ?Charge_Type_temp
      access:chartype.clearkey(cha:warranty_key)
      cha:warranty    = 'NO'
      cha:charge_type = charge_type_temp
      if access:chartype.fetch(cha:warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = 'NO'
          press(clip(charge_type_temp))
          browse_warranty_charge_types
          glo:select1 = ''
          if globalresponse = requestcompleted
              charge_type_temp = cha:charge_type
          else
              charge_type_temp = ''
              select(?-1)
          end
          display(?charge_type_temp)
          globalrequest     = saverequest#
      end!if access:chartype.fetch(cha:warranty_key)
    OF ?warranty_charge_type_tick_temp
      IF ?warranty_charge_type_tick_temp{Prop:Checked} = True
        UNHIDE(?Warranty_Charge_Type_temp)
        UNHIDE(?over_warranty_tick)
        UNHIDE(?Lookup_Warranty_Charge_Type)
      END
      IF ?warranty_charge_type_tick_temp{Prop:Checked} = False
        Warranty_Charge_Type_temp = ''
        HIDE(?Warranty_Charge_Type_temp)
        HIDE(?over_warranty_tick)
        HIDE(?Lookup_Warranty_Charge_Type)
      END
      ThisWindow.Reset
    OF ?Lookup_Warranty_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              warranty_charge_type_temp = cha:charge_type  
              select(?+2)
          of requestcancelled
              warranty_charge_type_temp = ''
              select(?-1)
      end!case globalreponse
      display(?warranty_charge_type_temp)
    OF ?Warranty_Charge_Type_temp
      access:chartype.clearkey(cha:warranty_key)
      cha:warranty    = 'YES'
      cha:charge_type = warranty_charge_type_temp
      if access:chartype.fetch(cha:warranty_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = 'YES'
          press(clip(warranty_charge_type_temp))
          browse_warranty_charge_types
          glo:select1 = ''
          if globalresponse = requestcompleted
              warranty_charge_type_temp = cha:charge_type
          else
              warranty_charge_type_temp = ''
              select(?-1)
          end
          display(?warranty_charge_type_temp)
          globalrequest     = saverequest#
      end!if access:chartype.fetch(cha:warranty_key)
    OF ?in_repair_tick_temp
      IF ?in_repair_tick_temp{Prop:Checked} = True
        UNHIDE(?over_in_repair_tick)
      END
      IF ?in_repair_tick_temp{Prop:Checked} = False
        HIDE(?over_in_repair_tick)
      END
      ThisWindow.Reset
    OF ?On_Test_Tick_Temp
      IF ?On_Test_Tick_Temp{Prop:Checked} = True
        UNHIDE(?over_on_test_tick)
      END
      IF ?On_Test_Tick_Temp{Prop:Checked} = False
        HIDE(?over_on_test_tick)
      END
      ThisWindow.Reset
    OF ?auto_complete_tick_temp
      IF ?auto_complete_tick_temp{Prop:Checked} = True
        UNHIDE(?PopCalendar:13)
        UNHIDE(?date_completed_temp)
        HIDE(?over_date_completed_tick)
      END
      IF ?auto_complete_tick_temp{Prop:Checked} = False
        HIDE(?PopCalendar:13)
        HIDE(?date_completed_temp)
        HIDE(?over_date_completed_tick)
      END
      ThisWindow.Reset
    OF ?PopCalendar:13
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          date_completed_temp = TINCALENDARStyle1(date_completed_temp)
          Display(?date_completed_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?tmp:SelectJobsType
      Case tmp:SelectJobsType
          Of 0
              ?ImportGroup{prop:Hide} = 1
          Of 1
              ?ImportGroup{prop:Hide} = 0
      End !tmp:SelectJobsType
    OF ?LookupImportFile
      ThisWindow.Update
      tmp:ImportFile = Upper(FileLookup35.Ask(1)  )
      DISPLAY
    OF ?status_location1_temp
      BRW6.ResetQueue(1)
    OF ?model_number1_temp
      BRW6.ResetQueue(1)
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?status_location2_temp
      BRW24.ResetQueue(1)
    OF ?model_number2_temp
      BRW24.ResetSort(1)
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Fault_Code1
      If fault_code1 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 1
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 1
                  mfo:field        = fault_code1
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 1
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code1 = mfo:field
                      else
                          fault_code1 = ''
                          select(?-1)
                      end
                      display(?fault_code1)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code1 = TINCALENDARStyle1(Fault_Code1)
          Display(?Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code2
      If fault_code2 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 2
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 2
                  mfo:field        = fault_code2
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 2
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code2 = mfo:field
                      else
                          fault_code2 = ''
                          select(?-1)
                      end
                      display(?fault_code2)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?Fault_Code3
      If fault_code3 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 3
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 3
                  mfo:field        = fault_code3
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 3
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code3 = mfo:field
                      else
                          fault_code3 = ''
                          select(?-1)
                      end
                      display(?fault_code3)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?Fault_Code4
      If fault_code4 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 4
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 4
                  mfo:field        = fault_code4
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 4
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code4 = mfo:field
                      else
                          fault_code4 = ''
                          select(?-1)
                      end
                      display(?fault_code4)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code2 = TINCALENDARStyle1(Fault_Code2)
          Display(?Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code5
      If fault_code5 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 5
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 5
                  mfo:field        = fault_code5
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 5
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code5 = mfo:field
                      else
                          fault_code5 = ''
                          select(?-1)
                      end
                      display(?fault_code5)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code3 = TINCALENDARStyle1(Fault_Code3)
          Display(?Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code6
      If fault_code6 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 6
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 6
                  mfo:field        = fault_code6
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 6
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code6 = mfo:field
                      else
                          fault_code6 = ''
                          select(?-1)
                      end
                      display(?fault_code6)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code4 = TINCALENDARStyle1(Fault_Code4)
          Display(?Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code7
      If fault_code7 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 7
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 7
                  mfo:field        = fault_code7
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 7
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code7 = mfo:field
                      else
                          fault_code7 = ''
                          select(?-1)
                      end
                      display(?fault_code7)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code5 = TINCALENDARStyle1(Fault_Code5)
          Display(?Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code8
      If fault_code8 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 8
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 8
                  mfo:field        = fault_code8
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 8
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code8 = mfo:field
                      else
                          fault_code8 = ''
                          select(?-1)
                      end
                      display(?fault_code8)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code6 = TINCALENDARStyle1(Fault_Code6)
          Display(?Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code9
      If fault_code9 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 9
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 9
                  mfo:field        = fault_code9
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 9
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code9 = mfo:field
                      else
                          fault_code9 = ''
                          select(?-1)
                      end
                      display(?fault_code9)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code7 = TINCALENDARStyle1(Fault_Code7)
          Display(?Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code10
      If fault_code10 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 10
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 10
                  mfo:field        = fault_code10
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 10
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code10 = mfo:field
                      else
                          fault_code10 = ''
                          select(?-1)
                      end
                      display(?fault_code10)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code8 = TINCALENDARStyle1(Fault_Code8)
          Display(?Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code11
      If fault_code11 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 11
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 11
                  mfo:field        = fault_code11
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 11
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code11 = mfo:field
                      else
                          fault_code11 = ''
                          select(?-1)
                      end
                      display(?fault_code11)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code9 = TINCALENDARStyle1(Fault_Code9)
          Display(?Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code10 = TINCALENDARStyle1(Fault_Code10)
          Display(?Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code12 = TINCALENDARStyle1(Fault_Code12)
          Display(?Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          Fault_Code11 = TINCALENDARStyle1(Fault_Code11)
          Display(?Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Fault_Code12
      If fault_code12 <> ''
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = manufacturer_temp
          maf:field_number = 12
          if access:manfault.fetch(maf:field_number_key) = Level:Benign
              If maf:force_lookup = 'YES'
                  access:manfaulo.clearkey(mfo:field_key)
                  mfo:manufacturer = manufacturer_temp
                  mfo:field_number = 12
                  mfo:field        = fault_code12
                  if access:manfaulo.fetch(mfo:field_key)
      
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      glo:select1  = manufacturer_temp
                      glo:select2  = 12
                      glo:select3  = ''
      
                      browse_manufacturer_fault_lookup
                      if globalresponse = requestcompleted
                          fault_code12 = mfo:field
                      else
                          fault_code12 = ''
                          select(?-1)
                      end
                      display(?fault_code12)
                      glo:select1  = ''
                      glo:select2  = ''
                      glo:select3  = ''
      
                  end!if access:manfaulo.fetch(mfo:field_key)
              End!If maf:force_lookup = 'YES'
          end!if access:manfault.fetch(maf:field_number_key) = Level:Benign
      End!If fault_code1 <> ''
    OF ?repair_type_warranty_tick_temp
      IF ?repair_type_warranty_tick_temp{Prop:Checked} = True
        UNHIDE(?over_repair_type_warranty_temp)
        UNHIDE(?Lookup_Repair_Type_Warranty)
        UNHIDE(?Repair_Type_Warranty_Temp)
      END
      IF ?repair_type_warranty_tick_temp{Prop:Checked} = False
        HIDE(?Repair_Type_Warranty_Temp)
        HIDE(?Lookup_Repair_Type_Warranty)
        HIDE(?over_repair_type_warranty_temp)
      END
      ThisWindow.Reset
    OF ?Repair_Type_Warranty_Temp
      access:repairty.clearkey(rep:model_number_key)
      rep:model_number = model_number_temp
      rep:repair_type  = repair_type_warranty_temp
      if access:repairty.fetch(rep:model_number_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = model_number_temp
          ! Start Change 2386 BE(14/04/03)
          !browserepairty
          browserepairty_warranty
          ! End Change 2386 BE(14/04/03)
          glo:select1 = ''
          if globalresponse = requestcompleted
              repair_type_warranty_temp = rep:repair_type
          else
              repair_type_warranty_temp = ''
              select(?-1)
          end
          display(?repair_type_warranty_temp)
          globalrequest     = saverequest#
      end!if access:repairty.fetch(rep:model_number_key)
    OF ?Lookup_Repair_Type_Warranty
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseREPAIRTY_Warranty
      ThisWindow.Reset
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              repair_type_warranty_temp = rep:repair_type  
              select(?+2)
          of requestcancelled
              repair_type_warranty_temp = ''
              select(?-1)
      end!case globalreponse
      display(?repair_type_warranty_temp)
    OF ?Repair_Type_Temp
      access:repairty.clearkey(rep:model_number_key)
      rep:model_number = model_number_temp
      rep:repair_type  = repair_type_temp
      if access:repairty.fetch(rep:model_number_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          glo:select1 = model_number_temp
          press(clip(repair_type_temp))
          browserepairty
          glo:select1 = ''
          if globalresponse = requestcompleted
              repair_type_temp = rep:repair_type
          else
              repair_type_temp = ''
              select(?-1)
          end
          display(?repair_type_temp)
          globalrequest     = saverequest#
      end!if access:repairty.fetch(rep:model_number_key)
    OF ?repair_type_tick_temp
      IF ?repair_type_tick_temp{Prop:Checked} = True
        UNHIDE(?Repair_Type_Temp)
        UNHIDE(?over_repair_type_tick)
        UNHIDE(?Lookup_Repair_Type)
      END
      IF ?repair_type_tick_temp{Prop:Checked} = False
        Repair_Type_Temp = ''
        HIDE(?Repair_Type_Temp)
        HIDE(?over_repair_type_tick)
        HIDE(?Lookup_Repair_Type)
      END
      ThisWindow.Reset
    OF ?Lookup_Repair_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseREPAIRTY
      ThisWindow.Reset
      glo:select1 = ''
      case globalresponse
          of requestcompleted
              repair_type_temp = rep:repair_type  
              select(?+2)
          of requestcancelled
              repair_type_temp = ''
              select(?-1)
      end!case globalreponse
      display(?repair_type_temp)
    OF ?VSBackButton
      ThisWindow.Update
         Wizard36.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
      DoNext# = 1
      Case Choice(?Sheet1)
          Of 2
              If tmp:SelectJobsType = 0
                  If Clip(glo:Select24) <> ''
                      ?Tab5{prop:Hide} = 0
                      ?Tab10{prop:Hide} = 1
                  Else !If glo:Select23 <> 0
                      ?Tab5{prop:Hide} = 1
                      ?Tab10{prop:Hide} = 0
                  End !If glo:Select23 <> 0
              Else !If tmp:SelectJobsType = 0
                  ?Tab5{prop:Hide} = 1
                  ?Tab10{prop:Hide} = 1
      
                  !Fill the queue with the job numbers from the import file.
                  !I can then use the existing routines to check the manufacturer.
                  Open(ImportFile)
                  If Error()
                      Case MessageEx('Cannot open the import file.'&|
                        '<13,10>'&|
                        '<13,10>' & Error() & '.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else !If Error()
                      Set(ImportFile,0)
                      Loop
                          Next(ImportFile)
                          If Error()
                              Break
                          End !If Error()
                          !Check to see if the actual job number exits
                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                          job:Ref_Number  = impfil:JobNumber
                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Found
                              glo:Job_Number_Pointer = job:Ref_Number
                              Add(glo:Q_JobNumber)
                          Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          
                      End !Loop
                  End !If Error()
                  Close(ImportFile)
      
                  If Records(glo:Q_JobNumber)
                      Do LookForDifferences
                      If job_count_temp <> 0
                          If difference_temp = 1
                              ?Tab3{prop:Hide} = 1
                              ! Start Change 2720 BE(12/06/03)
                              ?update_stock_module_temp{prop:disable} = 1
                              ?List{prop:disable} = 1
                              ?Insert{prop:disable} = 1
                              ?repair_type_tick_temp{prop:disable} = 1
                              ?repair_type_warranty_tick_temp{prop:disable} = 1
                              ?List:4{prop:disable} = 1
                              ?Insert:2{prop:disable} = 1
                              ! End Change 2720 BE(12/06/03)
                              Same_Model_Temp = False
                              Case MessageEx('The selected jobs do not all contain the same Model Number.'&|
                                '<13,10>You cannot therefore attach parts/fault codes.'&|
                                '<13,10>'&|
                                '<13,10>Do you wish to continue and update the selected jobs.','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      DoNext# = 0
                              end !case
                          Else!If found_difference# = 1
                              Same_Model_temp = True
                              Use_Fault_Codes_temp = False
                              Do Fault_Coding
                              If Use_Fault_codes_temp = True
                                  ?Tab3{prop:Hide} = 0
                              Else!If Use_Fault_codes_temp = True
                                  ?Tab3{prop:Hide} = 1
                              End!If Use_Fault_codes_temp = True
                              ! Start Change 2720 BE(12/06/03)
                              ?update_stock_module_temp{prop:disable} = 0
                              ?List{prop:disable} = 0
                              ?Insert{prop:disable} = 0
                              ?repair_type_tick_temp{prop:disable} = 0
                              ?repair_type_warranty_tick_temp{prop:disable} = 0
                              ?List:4{prop:disable} = 0
                              ?Insert:2{prop:disable} = 0
                              ! End Change 2720 BE(12/06/03)
                          End!If found_difference# = 1
                      End!If some_jobs# <> 0
      
                  End !If Records(glo:Q_JobNumber)
      
              End !If tmp:SelectJobsType = 1
      
      
              
          Of 3 Orof 4
              If ~Records(glo:Q_JobNumber)
                  DoNext# = 0
              Else !If ~Records(glo:Q_JobNumber)
      
                  Do LookForDifferences
      
                  If job_count_temp <> 0
                      If difference_temp = 1
                          ?Tab3{prop:Hide} = 1
                          ! Start Change 2720 BE(12/06/03)
                          ?update_stock_module_temp{prop:disable} = 1
                          ?List{prop:disable} = 1
                          ?Insert{prop:disable} = 1
                          ?repair_type_tick_temp{prop:disable} = 1
                          ?repair_type_warranty_tick_temp{prop:disable} = 1
                          ?List:4{prop:disable} = 1
                          ?Insert:2{prop:disable} = 1
                          ! End Change 2720 BE(12/06/03)
                          Same_Model_Temp = False
                          Case MessageEx('The selected jobs do not all contain the same Model Number.'&|
                            '<13,10>You cannot therefore attach parts/fault codes.'&|
                            '<13,10>'&|
                            '<13,10>Do you wish to continue and update the selected jobs.','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                              Of 2 ! &No Button
                                  DoNext# = 0
                          end !case
                      Else!If found_difference# = 1
                          Same_Model_temp = True
                          Use_Fault_Codes_temp = False
                          Do Fault_Coding
                          If Use_Fault_codes_temp = True
                              ?Tab3{prop:Hide} = 0
                          Else!If Use_Fault_codes_temp = True
                              ?Tab3{prop:Hide} = 1
                          End!If Use_Fault_codes_temp = True
                          ! Start Change 2720 BE(12/06/03)
                          ?update_stock_module_temp{prop:disable} = 0
                          ?List{prop:disable} = 0
                          ?Insert{prop:disable} = 0
                          ?repair_type_tick_temp{prop:disable} = 0
                          ?repair_type_warranty_tick_temp{prop:disable} = 0
                          ?List:4{prop:disable} = 0
                          ?Insert:2{prop:disable} = 0
                          ! End Change 2720 BE(12/06/03)
                      End!If found_difference# = 1
                  End!If some_jobs# <> 0
              End !If ~Records(glo:Q_JobNumber)
      End !Choice(?Sheet1)
      
      
      If DoNext#
         Wizard36.TakeAccepted()
      End!If DoNext#
    OF ?Finish_Button
      ThisWindow.Update
      Case MessageEx('The selected jobs will now be updated.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
          error# = 0
      
          If Records(glo:q_jobnumber) And error# = 0
              recordspercycle     = 25
              recordsprocessed    = 0
              percentprogress     = 0
              open(progresswindow)
              progress:thermometer    = 0
              ?progress:pcttext{prop:text} = '0% Completed'
      
              recordstoprocess    = Records(glo:q_jobnumber)
      
              Loop x$ = 1 To Records(glo:q_jobnumber)
                  Get(glo:q_jobnumber,x$)
                  Do GetNextRecord2
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number  = glo:job_number_pointer
                  If access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Do UpdateJobs
                  End !If access:jobs.fetch(job:ref_number_key) = Level:Benign
              End
              close(progresswindow)
      
              Case MessageEx('Jobs Updated.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                 Of 1 ! &OK Button
              End!Case MessageEx
              DO DASBRW::8:DASUNTAGALL
              DO DASBRW::25:DASUNTAGALL
              Select(?Sheet1,TabNumber)
          End !If Records(glo:q_jobnumber)
      
      of button:no
      
      end !case
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?date_completed_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:13)
      CYCLE
    END
  OF ?Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  OF ?Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?DASTAGAll
    Select(?List:2)
  OF ?DASUNTAGALL
    Select(?List:2)
  OF ?DASTAG
    Select(?List:2)
  OF ?DASTAGAll:2
    Select(?List:3)
  OF ?DASUNTAGALL:2
    Select(?List:3)
  OF ?DASTAG:2
    Select(?List:3)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::25:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Ref_Number
      Select(?List:2)
    OF ?JOB:Ref_Number:2
      Select(?List:3)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      DO DASBRW::8:DASUNTAGALL
      DO DASBRW::25:DASUNTAGALL
      Select(?Sheet1,1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

ValidateRepairType      PROCEDURE(IN:ModelNumber,IN:AccountNumber,IN:ChargeType,IN:UnitType,IN:Type,IN:RepairType) !RETURN LONG
!FirstChar     LONG
    CODE
    IF (IN:RepairType = '') THEN
        RETURN Level:benign
    END

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = IN:AccountNumber
    IF (Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign) THEN
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        IF (Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign) THEN
            ! Start Change 3542 BE(11/11/03)
            !  Check on tra:ShowRepairTypes removed because it is inconsistent with equivalent validation
            !        routine in Job Progress (SBJ01)
            !IF (tra:ShowRepairTypes) THEN
            ! End Change 3542 BE(11/11/03)
                IF ((tra:Invoice_Sub_Accounts = 'YES') AND (tra:Use_Sub_Accounts = 'YES')) THEN
                    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                    suc:Account_Number = IN:AccountNumber
                    suc:Model_Number   = IN:ModelNumber
                    suc:Charge_Type    = IN:ChargeType
                    suc:Unit_Type      = IN:UnitType
                    suc:Repair_Type    = IN:RepairType
                    IF (Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign) THEN
                        RETURN Level:Benign
                    END
                END
                Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
                trc:Account_Number = tra:Account_Number
                trc:Model_Number   = IN:ModelNumber
                trc:Charge_Type    = IN:ChargeType
                trc:Unit_Type      = IN:UnitType
                trc:Repair_Type    = IN:RepairType
                IF (Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign) THEN
                    RETURN Level:Benign
                END
            ! Start Change 3542 BE(11/11/03)
            !END
            ! End Change 3542 BE(11/11/03)
        END
    END

    Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
    sta:Model_Number = IN:ModelNumber
    sta:Charge_Type  = IN:ChargeType
    sta:Unit_Type    = IN:UnitType
    sta:Repair_Type  = IN:RepairType
    IF (Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign) THEN
        RETURN Level:Benign
    END
    RETURN Level:Fatal

BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet2) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     GET(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     GET(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


BRW24.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet3) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet3) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW24.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     GET(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    IF ERRORCODE()
      tag2_temp = ''
    ELSE
      tag2_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag2_temp = '*')
    SELF.Q.tag2_temp_Icon = 2
  ELSE
    SELF.Q.tag2_temp_Icon = 1
  END


BRW24.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW24.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW24::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW24::RecordStatus=ReturnValue
  IF BRW24::RecordStatus NOT=Record:OK THEN RETURN BRW24::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     GET(GLO:q_JobNumber,GLO:q_JobNumber.Job_Number_Pointer)
    EXECUTE DASBRW::25:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW24::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW24::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW24::RecordStatus
  RETURN ReturnValue


BRW31.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW31.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

Wizard36.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW6.Q &= NULL) ! Has Browse Object been initialized?
       BRW6.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW24.Q &= NULL) ! Has Browse Object been initialized?
       BRW24.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW31.Q &= NULL) ! Has Browse Object been initialized?
       BRW31.ResetQueue(Reset:Queue)
    END

Wizard36.TakeBackEmbed PROCEDURE
   CODE

Wizard36.TakeNextEmbed PROCEDURE
   CODE

Wizard36.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
