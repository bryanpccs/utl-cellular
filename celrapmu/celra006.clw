

   MEMBER('celrapmu.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA006.INC'),ONCE        !Local module procedure declarations
                     END


Select_Batch_Number PROCEDURE                         !Generated from procedure template - Window

FilesOpened          BYTE
batch_temp           STRING('NO {1}')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
FDB4::View:FileDrop  VIEW(JOBBATCH)
                       PROJECT(jbt:Batch_Number)
                     END
Queue:FileDrop       QUEUE                            !Queue declaration for browse/combo box using ?GLO:Select24
jbt:Batch_Number       LIKE(jbt:Batch_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Multiple Job Update Wizard'),AT(,,184,93),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,176,58),USE(?Sheet1),SPREAD
                         TAB('Select Batch Number'),USE(?Tab1)
                           PROMPT('Batch Number'),AT(22,30),USE(?Prompt2)
                           LIST,AT(96,30,64,10),USE(GLO:Select24),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('24L(2)@p<<<<<<<<<<#pB@'),DROP(10),FROM(Queue:FileDrop)
                         END
                       END
                       BUTTON('&OK'),AT(64,70,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(120,70,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,66,176,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDB4                 CLASS(FileDropClass)             !File drop manager
Q                      &Queue:FileDrop                !Reference to display queue
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?GLO:Select24{prop:FontColor} = 65793
    ?GLO:Select24{prop:Color}= 16777215
    ?GLO:Select24{prop:Color,2} = 16777215
    ?GLO:Select24{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Batch_Number')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBBATCH.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  Clear(glo:G_Select1)
  Do RecolourWindow
  ?GLO:Select24{prop:vcr} = TRUE
  FDB4.Init(?GLO:Select24,Queue:FileDrop.ViewPosition,FDB4::View:FileDrop,Queue:FileDrop,Relate:JOBBATCH,ThisWindow)
  FDB4.Q &= Queue:FileDrop
  FDB4.AddSortOrder(jbt:Batch_Number_Key)
  FDB4.AddField(jbt:Batch_Number,FDB4.Q.jbt:Batch_Number)
  ThisWindow.AddItem(FDB4.WindowComponent)
  FDB4.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBBATCH.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      close# = 1
      found# = 0
      access:jobs.clearkey(job:batch_number_key)
      job:batch_number = glo:select24
      set(job:batch_number_key,job:batch_number_key)
      loop
          if access:jobs.next()
             break
          end !if
          if job:batch_number <> glo:select24      |
              then break.  ! end if
          found# = 1
          Break
      end !loop
      If found# = 0
          close# = 0
          beep(beep:systemhand)  ;  yield()
          message('There are no jobs on the selected Batch Number.', |
                  'ServiceBase 2000', icon:hand)
          Select(?glo:select24)
      End!If found# = 0
      If close# = 1
          glo:select23 = ''
          Post(Event:CloseWindow)
      End!If close# = 1
    OF ?CancelButton
      ThisWindow.Update
      glo:select23 = 'QUIT'
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

