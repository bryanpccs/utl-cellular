

   MEMBER('celrapmu.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA005.INC'),ONCE        !Local module procedure declarations
                     END


Update_TempWarParts PROCEDURE                         !Generated from procedure template - Window

CurrentTab           STRING(80)
main_part_temp       STRING(3)
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?wartmp:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::wartmp:Record LIKE(wartmp:RECORD),STATIC
QuickWindow          WINDOW('Temporary Chargeable Part'),AT(,,289,247),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Warranty_Part'),TILED,SYSTEM,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,284,212),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Warranty Part'),AT(8,20),USE(?Prompt8),FONT('Tahoma',12,COLOR:Navy,FONT:bold)
                           PROMPT('Part Number'),AT(8,36),USE(?wartmp:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(wartmp:Part_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('Browse Stock'),AT(212,36,68,10),USE(?browse_stock_button),SKIP,LEFT,ICON('book.ico')
                           PROMPT('Description'),AT(8,48),USE(?wartmp:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,48,124,10),USE(wartmp:Description),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Purchase Cost'),AT(8,64),USE(?wartmp:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,64,64,10),USE(wartmp:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Sale Cost'),AT(8,76),USE(?wartmp:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,76,64,10),USE(wartmp:Sale_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Supplier'),AT(8,92),USE(?Prompt9)
                           COMBO(@s30),AT(84,92,124,10),USE(wartmp:Supplier),VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Quantity'),AT(8,108),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(84,108,64,10),USE(wartmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,RANGE(1,99999999),STEP(1)
                           CHECK('Exclude From Order'),AT(84,124,84,8),USE(wartmp:Exclude_From_Order),VALUE('YES','NO')
                           CHECK('Main Part'),AT(84,136),USE(wartmp:Main_Part),HIDE,VALUE('YES','NO')
                         END
                         TAB('Fault Coding'),USE(?Fault_Code_Tab),HIDE
                           BUTTON,AT(228,24,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 1:'),AT(8,24),USE(?wartmp:Fault_Code1:Prompt),HIDE
                           ENTRY(@s30),AT(84,24,124,10),USE(wartmp:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,24,124,10),USE(wartmp:Fault_Code1,,?wartmp:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 2:'),AT(8,40),USE(?wartmp:Fault_Code2:Prompt),HIDE
                           ENTRY(@s30),AT(84,40,124,10),USE(wartmp:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,40,10,10),USE(?Button4:2),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,40,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,24,10,10),USE(?Button4),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,40,124,10),USE(wartmp:Fault_Code2,,?wartmp:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 3:'),AT(8,56),USE(?wartmp:Fault_Code3:Prompt),HIDE
                           ENTRY(@s30),AT(84,56,124,10),USE(wartmp:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,56,10,10),USE(?Button4:3),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,56,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,72,10,10),USE(?Button4:4),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,56,124,10),USE(wartmp:Fault_Code3,,?wartmp:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 4:'),AT(8,72),USE(?wartmp:Fault_Code4:Prompt),HIDE
                           ENTRY(@s30),AT(84,72,124,10),USE(wartmp:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,88,10,10),USE(?Button4:5),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,72,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,104,10,10),USE(?Button4:6),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,88,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,120,10,10),USE(?Button4:7),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7:'),AT(8,120),USE(?wartmp:Fault_Code7:Prompt),HIDE
                           ENTRY(@s30),AT(84,120,124,10),USE(wartmp:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,120,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,136,10,10),USE(?Button4:8),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,120,124,10),USE(wartmp:Fault_Code7,,?wartmp:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 8:'),AT(8,136),USE(?wartmp:Fault_Code8:Prompt),HIDE
                           ENTRY(@s30),AT(84,136,124,10),USE(wartmp:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,136,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(228,104,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,152,10,10),USE(?Button4:9),HIDE,ICON('list3.ico')
                           BUTTON,AT(212,168,10,10),USE(?Button4:10),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,136,124,10),USE(wartmp:Fault_Code8,,?wartmp:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 9:'),AT(8,152),USE(?wartmp:Fault_Code9:Prompt),HIDE
                           ENTRY(@s30),AT(84,152,124,10),USE(wartmp:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,152,124,10),USE(wartmp:Fault_Code9,,?wartmp:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 10:'),AT(8,168),USE(?wartmp:Fault_Code10:Prompt),HIDE
                           ENTRY(@s30),AT(84,168,124,10),USE(wartmp:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,152,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 11:'),AT(8,184),USE(?wartmp:Fault_Code11:Prompt),HIDE
                           ENTRY(@s30),AT(84,184,124,10),USE(wartmp:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,168,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           ENTRY(@d6b),AT(52,168,124,10),USE(wartmp:Fault_Code10,,?wartmp:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 12:'),AT(8,200),USE(?wartmp:Fault_Code12:Prompt),HIDE
                           ENTRY(@s30),AT(84,200,124,10),USE(wartmp:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,184,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,184,10,10),USE(?Button4:11),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,200,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,200,10,10),USE(?Button4:12),HIDE,ICON('list3.ico')
                           ENTRY(@d6b),AT(52,184,124,10),USE(wartmp:Fault_Code11,,?wartmp:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,200,124,10),USE(wartmp:Fault_Code12,,?wartmp:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,72,124,10),USE(wartmp:Fault_Code4,,?wartmp:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 5:'),AT(8,88),USE(?wartmp:Fault_Code5:Prompt),HIDE
                           ENTRY(@s30),AT(84,88,124,10),USE(wartmp:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,88,124,10),USE(wartmp:Fault_Code5,,?wartmp:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 6:'),AT(8,104),USE(?wartmp:Fault_Code6:Prompt),HIDE
                           ENTRY(@s30),AT(84,104,124,10),USE(wartmp:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,104,124,10),USE(wartmp:Fault_Code6,,?wartmp:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,220,284,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(172,224,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(228,224,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

save_map_id   ushort,auto
save_war_ali_id     ushort,auto
save_wartmp_ali_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?wartmp:Part_Number:Prompt{prop:FontColor} = -1
    ?wartmp:Part_Number:Prompt{prop:Color} = 15066597
    If ?wartmp:Part_Number{prop:ReadOnly} = True
        ?wartmp:Part_Number{prop:FontColor} = 65793
        ?wartmp:Part_Number{prop:Color} = 15066597
    Elsif ?wartmp:Part_Number{prop:Req} = True
        ?wartmp:Part_Number{prop:FontColor} = 65793
        ?wartmp:Part_Number{prop:Color} = 8454143
    Else ! If ?wartmp:Part_Number{prop:Req} = True
        ?wartmp:Part_Number{prop:FontColor} = 65793
        ?wartmp:Part_Number{prop:Color} = 16777215
    End ! If ?wartmp:Part_Number{prop:Req} = True
    ?wartmp:Part_Number{prop:Trn} = 0
    ?wartmp:Part_Number{prop:FontStyle} = font:Bold
    ?wartmp:Description:Prompt{prop:FontColor} = -1
    ?wartmp:Description:Prompt{prop:Color} = 15066597
    If ?wartmp:Description{prop:ReadOnly} = True
        ?wartmp:Description{prop:FontColor} = 65793
        ?wartmp:Description{prop:Color} = 15066597
    Elsif ?wartmp:Description{prop:Req} = True
        ?wartmp:Description{prop:FontColor} = 65793
        ?wartmp:Description{prop:Color} = 8454143
    Else ! If ?wartmp:Description{prop:Req} = True
        ?wartmp:Description{prop:FontColor} = 65793
        ?wartmp:Description{prop:Color} = 16777215
    End ! If ?wartmp:Description{prop:Req} = True
    ?wartmp:Description{prop:Trn} = 0
    ?wartmp:Description{prop:FontStyle} = font:Bold
    ?wartmp:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?wartmp:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?wartmp:Purchase_Cost{prop:ReadOnly} = True
        ?wartmp:Purchase_Cost{prop:FontColor} = 65793
        ?wartmp:Purchase_Cost{prop:Color} = 15066597
    Elsif ?wartmp:Purchase_Cost{prop:Req} = True
        ?wartmp:Purchase_Cost{prop:FontColor} = 65793
        ?wartmp:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?wartmp:Purchase_Cost{prop:Req} = True
        ?wartmp:Purchase_Cost{prop:FontColor} = 65793
        ?wartmp:Purchase_Cost{prop:Color} = 16777215
    End ! If ?wartmp:Purchase_Cost{prop:Req} = True
    ?wartmp:Purchase_Cost{prop:Trn} = 0
    ?wartmp:Purchase_Cost{prop:FontStyle} = font:Bold
    ?wartmp:Sale_Cost:Prompt{prop:FontColor} = -1
    ?wartmp:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?wartmp:Sale_Cost{prop:ReadOnly} = True
        ?wartmp:Sale_Cost{prop:FontColor} = 65793
        ?wartmp:Sale_Cost{prop:Color} = 15066597
    Elsif ?wartmp:Sale_Cost{prop:Req} = True
        ?wartmp:Sale_Cost{prop:FontColor} = 65793
        ?wartmp:Sale_Cost{prop:Color} = 8454143
    Else ! If ?wartmp:Sale_Cost{prop:Req} = True
        ?wartmp:Sale_Cost{prop:FontColor} = 65793
        ?wartmp:Sale_Cost{prop:Color} = 16777215
    End ! If ?wartmp:Sale_Cost{prop:Req} = True
    ?wartmp:Sale_Cost{prop:Trn} = 0
    ?wartmp:Sale_Cost{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?wartmp:Supplier{prop:ReadOnly} = True
        ?wartmp:Supplier{prop:FontColor} = 65793
        ?wartmp:Supplier{prop:Color} = 15066597
    Elsif ?wartmp:Supplier{prop:Req} = True
        ?wartmp:Supplier{prop:FontColor} = 65793
        ?wartmp:Supplier{prop:Color} = 8454143
    Else ! If ?wartmp:Supplier{prop:Req} = True
        ?wartmp:Supplier{prop:FontColor} = 65793
        ?wartmp:Supplier{prop:Color} = 16777215
    End ! If ?wartmp:Supplier{prop:Req} = True
    ?wartmp:Supplier{prop:Trn} = 0
    ?wartmp:Supplier{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?wartmp:Quantity{prop:ReadOnly} = True
        ?wartmp:Quantity{prop:FontColor} = 65793
        ?wartmp:Quantity{prop:Color} = 15066597
    Elsif ?wartmp:Quantity{prop:Req} = True
        ?wartmp:Quantity{prop:FontColor} = 65793
        ?wartmp:Quantity{prop:Color} = 8454143
    Else ! If ?wartmp:Quantity{prop:Req} = True
        ?wartmp:Quantity{prop:FontColor} = 65793
        ?wartmp:Quantity{prop:Color} = 16777215
    End ! If ?wartmp:Quantity{prop:Req} = True
    ?wartmp:Quantity{prop:Trn} = 0
    ?wartmp:Quantity{prop:FontStyle} = font:Bold
    ?wartmp:Exclude_From_Order{prop:Font,3} = -1
    ?wartmp:Exclude_From_Order{prop:Color} = 15066597
    ?wartmp:Exclude_From_Order{prop:Trn} = 0
    ?wartmp:Main_Part{prop:Font,3} = -1
    ?wartmp:Main_Part{prop:Color} = 15066597
    ?wartmp:Main_Part{prop:Trn} = 0
    ?Fault_Code_Tab{prop:Color} = 15066597
    ?wartmp:Fault_Code1:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code1{prop:ReadOnly} = True
        ?wartmp:Fault_Code1{prop:FontColor} = 65793
        ?wartmp:Fault_Code1{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code1{prop:Req} = True
        ?wartmp:Fault_Code1{prop:FontColor} = 65793
        ?wartmp:Fault_Code1{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code1{prop:Req} = True
        ?wartmp:Fault_Code1{prop:FontColor} = 65793
        ?wartmp:Fault_Code1{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code1{prop:Req} = True
    ?wartmp:Fault_Code1{prop:Trn} = 0
    ?wartmp:Fault_Code1{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code1:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code1:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code1:2{prop:Req} = True
        ?wartmp:Fault_Code1:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code1:2{prop:Req} = True
        ?wartmp:Fault_Code1:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code1:2{prop:Req} = True
    ?wartmp:Fault_Code1:2{prop:Trn} = 0
    ?wartmp:Fault_Code1:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code2:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code2{prop:ReadOnly} = True
        ?wartmp:Fault_Code2{prop:FontColor} = 65793
        ?wartmp:Fault_Code2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code2{prop:Req} = True
        ?wartmp:Fault_Code2{prop:FontColor} = 65793
        ?wartmp:Fault_Code2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code2{prop:Req} = True
        ?wartmp:Fault_Code2{prop:FontColor} = 65793
        ?wartmp:Fault_Code2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code2{prop:Req} = True
    ?wartmp:Fault_Code2{prop:Trn} = 0
    ?wartmp:Fault_Code2{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code2:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code2:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code2:2{prop:Req} = True
        ?wartmp:Fault_Code2:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code2:2{prop:Req} = True
        ?wartmp:Fault_Code2:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code2:2{prop:Req} = True
    ?wartmp:Fault_Code2:2{prop:Trn} = 0
    ?wartmp:Fault_Code2:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code3:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code3{prop:ReadOnly} = True
        ?wartmp:Fault_Code3{prop:FontColor} = 65793
        ?wartmp:Fault_Code3{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code3{prop:Req} = True
        ?wartmp:Fault_Code3{prop:FontColor} = 65793
        ?wartmp:Fault_Code3{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code3{prop:Req} = True
        ?wartmp:Fault_Code3{prop:FontColor} = 65793
        ?wartmp:Fault_Code3{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code3{prop:Req} = True
    ?wartmp:Fault_Code3{prop:Trn} = 0
    ?wartmp:Fault_Code3{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code3:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code3:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code3:2{prop:Req} = True
        ?wartmp:Fault_Code3:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code3:2{prop:Req} = True
        ?wartmp:Fault_Code3:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code3:2{prop:Req} = True
    ?wartmp:Fault_Code3:2{prop:Trn} = 0
    ?wartmp:Fault_Code3:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code4:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code4{prop:ReadOnly} = True
        ?wartmp:Fault_Code4{prop:FontColor} = 65793
        ?wartmp:Fault_Code4{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code4{prop:Req} = True
        ?wartmp:Fault_Code4{prop:FontColor} = 65793
        ?wartmp:Fault_Code4{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code4{prop:Req} = True
        ?wartmp:Fault_Code4{prop:FontColor} = 65793
        ?wartmp:Fault_Code4{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code4{prop:Req} = True
    ?wartmp:Fault_Code4{prop:Trn} = 0
    ?wartmp:Fault_Code4{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code7:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code7{prop:ReadOnly} = True
        ?wartmp:Fault_Code7{prop:FontColor} = 65793
        ?wartmp:Fault_Code7{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code7{prop:Req} = True
        ?wartmp:Fault_Code7{prop:FontColor} = 65793
        ?wartmp:Fault_Code7{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code7{prop:Req} = True
        ?wartmp:Fault_Code7{prop:FontColor} = 65793
        ?wartmp:Fault_Code7{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code7{prop:Req} = True
    ?wartmp:Fault_Code7{prop:Trn} = 0
    ?wartmp:Fault_Code7{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code7:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code7:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code7:2{prop:Req} = True
        ?wartmp:Fault_Code7:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code7:2{prop:Req} = True
        ?wartmp:Fault_Code7:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code7:2{prop:Req} = True
    ?wartmp:Fault_Code7:2{prop:Trn} = 0
    ?wartmp:Fault_Code7:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code8:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code8{prop:ReadOnly} = True
        ?wartmp:Fault_Code8{prop:FontColor} = 65793
        ?wartmp:Fault_Code8{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code8{prop:Req} = True
        ?wartmp:Fault_Code8{prop:FontColor} = 65793
        ?wartmp:Fault_Code8{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code8{prop:Req} = True
        ?wartmp:Fault_Code8{prop:FontColor} = 65793
        ?wartmp:Fault_Code8{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code8{prop:Req} = True
    ?wartmp:Fault_Code8{prop:Trn} = 0
    ?wartmp:Fault_Code8{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code8:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code8:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code8:2{prop:Req} = True
        ?wartmp:Fault_Code8:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code8:2{prop:Req} = True
        ?wartmp:Fault_Code8:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code8:2{prop:Req} = True
    ?wartmp:Fault_Code8:2{prop:Trn} = 0
    ?wartmp:Fault_Code8:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code9:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code9{prop:ReadOnly} = True
        ?wartmp:Fault_Code9{prop:FontColor} = 65793
        ?wartmp:Fault_Code9{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code9{prop:Req} = True
        ?wartmp:Fault_Code9{prop:FontColor} = 65793
        ?wartmp:Fault_Code9{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code9{prop:Req} = True
        ?wartmp:Fault_Code9{prop:FontColor} = 65793
        ?wartmp:Fault_Code9{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code9{prop:Req} = True
    ?wartmp:Fault_Code9{prop:Trn} = 0
    ?wartmp:Fault_Code9{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code9:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code9:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code9:2{prop:Req} = True
        ?wartmp:Fault_Code9:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code9:2{prop:Req} = True
        ?wartmp:Fault_Code9:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code9:2{prop:Req} = True
    ?wartmp:Fault_Code9:2{prop:Trn} = 0
    ?wartmp:Fault_Code9:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code10:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code10{prop:ReadOnly} = True
        ?wartmp:Fault_Code10{prop:FontColor} = 65793
        ?wartmp:Fault_Code10{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code10{prop:Req} = True
        ?wartmp:Fault_Code10{prop:FontColor} = 65793
        ?wartmp:Fault_Code10{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code10{prop:Req} = True
        ?wartmp:Fault_Code10{prop:FontColor} = 65793
        ?wartmp:Fault_Code10{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code10{prop:Req} = True
    ?wartmp:Fault_Code10{prop:Trn} = 0
    ?wartmp:Fault_Code10{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code11:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code11{prop:ReadOnly} = True
        ?wartmp:Fault_Code11{prop:FontColor} = 65793
        ?wartmp:Fault_Code11{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code11{prop:Req} = True
        ?wartmp:Fault_Code11{prop:FontColor} = 65793
        ?wartmp:Fault_Code11{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code11{prop:Req} = True
        ?wartmp:Fault_Code11{prop:FontColor} = 65793
        ?wartmp:Fault_Code11{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code11{prop:Req} = True
    ?wartmp:Fault_Code11{prop:Trn} = 0
    ?wartmp:Fault_Code11{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code10:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code10:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code10:2{prop:Req} = True
        ?wartmp:Fault_Code10:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code10:2{prop:Req} = True
        ?wartmp:Fault_Code10:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code10:2{prop:Req} = True
    ?wartmp:Fault_Code10:2{prop:Trn} = 0
    ?wartmp:Fault_Code10:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code12:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code12{prop:ReadOnly} = True
        ?wartmp:Fault_Code12{prop:FontColor} = 65793
        ?wartmp:Fault_Code12{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code12{prop:Req} = True
        ?wartmp:Fault_Code12{prop:FontColor} = 65793
        ?wartmp:Fault_Code12{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code12{prop:Req} = True
        ?wartmp:Fault_Code12{prop:FontColor} = 65793
        ?wartmp:Fault_Code12{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code12{prop:Req} = True
    ?wartmp:Fault_Code12{prop:Trn} = 0
    ?wartmp:Fault_Code12{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code11:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code11:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code11:2{prop:Req} = True
        ?wartmp:Fault_Code11:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code11:2{prop:Req} = True
        ?wartmp:Fault_Code11:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code11:2{prop:Req} = True
    ?wartmp:Fault_Code11:2{prop:Trn} = 0
    ?wartmp:Fault_Code11:2{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code12:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code12:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code12:2{prop:Req} = True
        ?wartmp:Fault_Code12:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code12:2{prop:Req} = True
        ?wartmp:Fault_Code12:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code12:2{prop:Req} = True
    ?wartmp:Fault_Code12:2{prop:Trn} = 0
    ?wartmp:Fault_Code12:2{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code4:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code4:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code4:2{prop:Req} = True
        ?wartmp:Fault_Code4:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code4:2{prop:Req} = True
        ?wartmp:Fault_Code4:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code4:2{prop:Req} = True
    ?wartmp:Fault_Code4:2{prop:Trn} = 0
    ?wartmp:Fault_Code4:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code5:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code5{prop:ReadOnly} = True
        ?wartmp:Fault_Code5{prop:FontColor} = 65793
        ?wartmp:Fault_Code5{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code5{prop:Req} = True
        ?wartmp:Fault_Code5{prop:FontColor} = 65793
        ?wartmp:Fault_Code5{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code5{prop:Req} = True
        ?wartmp:Fault_Code5{prop:FontColor} = 65793
        ?wartmp:Fault_Code5{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code5{prop:Req} = True
    ?wartmp:Fault_Code5{prop:Trn} = 0
    ?wartmp:Fault_Code5{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code5:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code5:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code5:2{prop:Req} = True
        ?wartmp:Fault_Code5:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code5:2{prop:Req} = True
        ?wartmp:Fault_Code5:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code5:2{prop:Req} = True
    ?wartmp:Fault_Code5:2{prop:Trn} = 0
    ?wartmp:Fault_Code5:2{prop:FontStyle} = font:Bold
    ?wartmp:Fault_Code6:Prompt{prop:FontColor} = -1
    ?wartmp:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?wartmp:Fault_Code6{prop:ReadOnly} = True
        ?wartmp:Fault_Code6{prop:FontColor} = 65793
        ?wartmp:Fault_Code6{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code6{prop:Req} = True
        ?wartmp:Fault_Code6{prop:FontColor} = 65793
        ?wartmp:Fault_Code6{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code6{prop:Req} = True
        ?wartmp:Fault_Code6{prop:FontColor} = 65793
        ?wartmp:Fault_Code6{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code6{prop:Req} = True
    ?wartmp:Fault_Code6{prop:Trn} = 0
    ?wartmp:Fault_Code6{prop:FontStyle} = font:Bold
    If ?wartmp:Fault_Code6:2{prop:ReadOnly} = True
        ?wartmp:Fault_Code6:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?wartmp:Fault_Code6:2{prop:Req} = True
        ?wartmp:Fault_Code6:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?wartmp:Fault_Code6:2{prop:Req} = True
        ?wartmp:Fault_Code6:2{prop:FontColor} = 65793
        ?wartmp:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?wartmp:Fault_Code6:2{prop:Req} = True
    ?wartmp:Fault_Code6:2{prop:Trn} = 0
    ?wartmp:Fault_Code6:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Adjustment          Routine
    If wartmp:adjustment = 'YES'
        wartmp:Part_Number = 'ADJUSTMENT'
        wartmp:Description = 'ADJUSTMENT'
        wartmp:Purchase_Cost = ''
        wartmp:Sale_Cost     = ''
        wartmp:quantity = 1

       ?wartmp:Part_Number{prop:fontcolor} = 0
       ?wartmp:Part_Number{prop:color} = 12632256
       ?wartmp:Part_Number{prop:readonly} = 0
       ?wartmp:Part_Number{prop:skip} = 0
       ?wartmp:Part_Number{prop:disable} = 1
   
       ?wartmp:Description{prop:fontcolor} = 0
       ?wartmp:Description{prop:color} = 12632256
       ?wartmp:Description{prop:readonly} = 0
       ?wartmp:Description{prop:skip} = 0
       ?wartmp:Description{prop:disable} = 1
   
       ?wartmp:Quantity{prop:fontcolor} = 0
       ?wartmp:Quantity{prop:color} = 12632256
       ?wartmp:Quantity{prop:readonly} = 0
       ?wartmp:Quantity{prop:skip} = 0
       ?wartmp:Quantity{prop:disable} = 1
   
       ?wartmp:Purchase_Cost{prop:fontcolor} = 0
       ?wartmp:Purchase_Cost{prop:color} = 12632256
       ?wartmp:Purchase_Cost{prop:readonly} = 0
       ?wartmp:Purchase_Cost{prop:skip} = 0
       ?wartmp:Purchase_Cost{prop:disable} = 1
   
       ?wartmp:Sale_Cost{prop:fontcolor} = 0
       ?wartmp:Sale_Cost{prop:color} = 12632256
       ?wartmp:Sale_Cost{prop:readonly} = 0
       ?wartmp:Sale_Cost{prop:skip} = 0
       ?wartmp:Sale_Cost{prop:disable} = 1
   
       ?wartmp:Supplier{prop:fontcolor} = 0
       ?wartmp:Supplier{prop:color} = 12632256
       ?wartmp:Supplier{prop:readonly} = 0
       ?wartmp:Supplier{prop:skip} = 0
       ?wartmp:Supplier{prop:disable} = 1
   
       ?wartmp:Exclude_From_Order{prop:fontcolor} = 0
       ?wartmp:Exclude_From_Order{prop:color} = 12632256
       ?wartmp:Exclude_From_Order{prop:readonly} = 0
       ?wartmp:Exclude_From_Order{prop:skip} = 0
       ?wartmp:Exclude_From_Order{prop:disable} = 1
   
    Else
   
       ?wartmp:Part_Number{prop:fontcolor} = 0
       ?wartmp:Part_Number{prop:color} = 16777215
       ?wartmp:Part_Number{prop:readonly} = 0
       ?wartmp:Part_Number{prop:skip} = 0
       ?wartmp:Part_Number{prop:disable} = 0
   
       ?wartmp:Description{prop:fontcolor} = 0
       ?wartmp:Description{prop:color} = 16777215
       ?wartmp:Description{prop:readonly} = 0
       ?wartmp:Description{prop:skip} = 0
       ?wartmp:Description{prop:disable} = 0
   
       ?wartmp:Quantity{prop:fontcolor} = 0
       ?wartmp:Quantity{prop:color} = 16777215
       ?wartmp:Quantity{prop:readonly} = 0
       ?wartmp:Quantity{prop:skip} = 0
       ?wartmp:Quantity{prop:disable} = 0
   
       ?wartmp:Purchase_Cost{prop:fontcolor} = 0
       ?wartmp:Purchase_Cost{prop:color} = 16777215
       ?wartmp:Purchase_Cost{prop:readonly} = 0
       ?wartmp:Purchase_Cost{prop:skip} = 0
       ?wartmp:Purchase_Cost{prop:disable} = 0
   
       ?wartmp:Sale_Cost{prop:fontcolor} = 0
       ?wartmp:Sale_Cost{prop:color} = 16777215
       ?wartmp:Sale_Cost{prop:readonly} = 0
       ?wartmp:Sale_Cost{prop:skip} = 0
       ?wartmp:Sale_Cost{prop:disable} = 0
   
       ?wartmp:Supplier{prop:fontcolor} = 0
       ?wartmp:Supplier{prop:color} = 16777215
       ?wartmp:Supplier{prop:readonly} = 0
       ?wartmp:Supplier{prop:skip} = 0
       ?wartmp:Supplier{prop:disable} = 0
   
       ?wartmp:Exclude_From_Order{prop:fontcolor} = 0
       ?wartmp:Exclude_From_Order{prop:color} = 12632256
       ?wartmp:Exclude_From_Order{prop:readonly} = 0
       ?wartmp:Exclude_From_Order{prop:skip} = 0
       ?wartmp:Exclude_From_Order{prop:disable} = 0
   
    End
    Display()

Hide_Fields     Routine
    If partmp:part_ref_number = ''
        Do Show_parts
    Else
        Do Hide_parts
    End
Show_Parts      Routine
   
       ?wartmp:Part_Number{prop:fontcolor} = 0
       ?wartmp:Part_Number{prop:color} = 16777215
       ?wartmp:Part_Number{prop:readonly} = 0
       ?wartmp:Part_Number{prop:skip} = 0
       ?wartmp:Part_Number{prop:disable} = 0
   
       ?wartmp:Description{prop:fontcolor} = 0
       ?wartmp:Description{prop:color} = 16777215
       ?wartmp:Description{prop:readonly} = 0
       ?wartmp:Description{prop:skip} = 0
       ?wartmp:Description{prop:disable} = 0
   
       ?wartmp:Purchase_Cost{prop:fontcolor} = 0
       ?wartmp:Purchase_Cost{prop:color} = 16777215
       ?wartmp:Purchase_Cost{prop:readonly} = 0
       ?wartmp:Purchase_Cost{prop:skip} = 0
       ?wartmp:Purchase_Cost{prop:disable} = 0
   
       ?wartmp:Sale_Cost{prop:fontcolor} = 0
       ?wartmp:Sale_Cost{prop:color} = 16777215
       ?wartmp:Sale_Cost{prop:readonly} = 0
       ?wartmp:Sale_Cost{prop:skip} = 0
       ?wartmp:Sale_Cost{prop:disable} = 0
   
       ?wartmp:Supplier{prop:fontcolor} = 0
       ?wartmp:Supplier{prop:color} = 16777215
       ?wartmp:Supplier{prop:readonly} = 0
       ?wartmp:Supplier{prop:skip} = 0
       ?wartmp:Supplier{prop:disable} = 0
   
       Display()
   
   
Hide_Parts      Routine
   
       ?wartmp:Part_Number{prop:fontcolor} = 0
       ?wartmp:Part_Number{prop:color} = 12632256
       ?wartmp:Part_Number{prop:readonly} = 1
       ?wartmp:Part_Number{prop:skip} = 1
       ?wartmp:Part_Number{prop:disable} = 0
   
       ?wartmp:Description{prop:fontcolor} = 0
       ?wartmp:Description{prop:color} = 12632256
       ?wartmp:Description{prop:readonly} = 1
       ?wartmp:Description{prop:skip} = 1
       ?wartmp:Description{prop:disable} = 0
   
       ?wartmp:Purchase_Cost{prop:fontcolor} = 0
       ?wartmp:Purchase_Cost{prop:color} = 12632256
       ?wartmp:Purchase_Cost{prop:readonly} = 1
       ?wartmp:Purchase_Cost{prop:skip} = 1
       ?wartmp:Purchase_Cost{prop:disable} = 0
   
       ?wartmp:Sale_Cost{prop:fontcolor} = 0
       ?wartmp:Sale_Cost{prop:color} = 12632256
       ?wartmp:Sale_Cost{prop:readonly} = 1
       ?wartmp:Sale_Cost{prop:skip} = 1
       ?wartmp:Sale_Cost{prop:disable} = 0
   
       ?wartmp:Supplier{prop:fontcolor} = 0
       ?wartmp:Supplier{prop:color} = 12632256
       ?wartmp:Supplier{prop:readonly} = 1
       ?wartmp:Supplier{prop:skip} = 1
       ?wartmp:Supplier{prop:disable} = 0
   
       Display()


ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Warranty Part'
  OF ChangeRecord
    ActionMessage = 'Changing A Warranty Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_TempWarParts')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt8
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(wartmp:Record,History::wartmp:Record)
  SELF.AddHistoryField(?wartmp:Part_Number,6)
  SELF.AddHistoryField(?wartmp:Description,7)
  SELF.AddHistoryField(?wartmp:Purchase_Cost,9)
  SELF.AddHistoryField(?wartmp:Sale_Cost,10)
  SELF.AddHistoryField(?wartmp:Supplier,8)
  SELF.AddHistoryField(?wartmp:Quantity,12)
  SELF.AddHistoryField(?wartmp:Exclude_From_Order,14)
  SELF.AddHistoryField(?wartmp:Main_Part,19)
  SELF.AddHistoryField(?wartmp:Fault_Code1,21)
  SELF.AddHistoryField(?wartmp:Fault_Code1:2,21)
  SELF.AddHistoryField(?wartmp:Fault_Code2,22)
  SELF.AddHistoryField(?wartmp:Fault_Code2:2,22)
  SELF.AddHistoryField(?wartmp:Fault_Code3,23)
  SELF.AddHistoryField(?wartmp:Fault_Code3:2,23)
  SELF.AddHistoryField(?wartmp:Fault_Code4,24)
  SELF.AddHistoryField(?wartmp:Fault_Code7,28)
  SELF.AddHistoryField(?wartmp:Fault_Code7:2,28)
  SELF.AddHistoryField(?wartmp:Fault_Code8,29)
  SELF.AddHistoryField(?wartmp:Fault_Code8:2,29)
  SELF.AddHistoryField(?wartmp:Fault_Code9,30)
  SELF.AddHistoryField(?wartmp:Fault_Code9:2,30)
  SELF.AddHistoryField(?wartmp:Fault_Code10,31)
  SELF.AddHistoryField(?wartmp:Fault_Code11,32)
  SELF.AddHistoryField(?wartmp:Fault_Code10:2,31)
  SELF.AddHistoryField(?wartmp:Fault_Code12,33)
  SELF.AddHistoryField(?wartmp:Fault_Code11:2,32)
  SELF.AddHistoryField(?wartmp:Fault_Code12:2,33)
  SELF.AddHistoryField(?wartmp:Fault_Code4:2,24)
  SELF.AddHistoryField(?wartmp:Fault_Code5,25)
  SELF.AddHistoryField(?wartmp:Fault_Code5:2,25)
  SELF.AddHistoryField(?wartmp:Fault_Code6,26)
  SELF.AddHistoryField(?wartmp:Fault_Code6:2,26)
  SELF.AddUpdateFile(Access:WPARTTMP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:PARTSTMP.Open
  Relate:WPARTTMP.Open
  Relate:WPARTTMP_ALIAS.Open
  Access:SUPPLIER.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:WPARTTMP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?wartmp:Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?wartmp:Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(wartmp:Supplier,?wartmp:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  FDCB6.AddUpdateField(sup:Company_Name,wartmp:Supplier)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:PARTSTMP.Close
    Relate:WPARTTMP.Close
    Relate:WPARTTMP_ALIAS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    wartmp:Adjustment = 'NO'
    wartmp:Warranty_Part = 'NO'
    wartmp:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?wartmp:Main_Part
      If wartmp:main_part <> main_part_temp
          If wartmp:main_part = 'YES'
              found# = 0
              setcursor(cursor:wait)
              save_wartmp_ali_id = access:wparttmp_alias.savefile()
              set(wparttmp_alias)
              loop
                  if access:wparttmp_alias.next()
                     break
                  end !if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  If wartmp_ali:record_number <> wartmp:record_number
                      If wartmp_ali:main_part = 'YES'
                          found# = 1
                          beep(beep:systemhand)  ;  yield()
                          message('One of the other parts attached to this job has already been marked '&|
                                  'as the ''Main Part''.', |
                                  'ServiceBase 2000', icon:hand)
                          Break
                      End!If wpr_ali:main_part = 'YES'
                  End!If wpr_ali:record_number <> wartmp:record_number
              end !loop
              access:wparttmp_alias.restorefile(save_wartmp_ali_id)
              setcursor()
              If found# = 0
                  main_part_temp = wartmp:main_part
              Else!If found# = 0
                  wartmp:main_part = main_part_temp
              End!If found# = 0
          Else!If wartmp:main_part = 'YES'
              beep(beep:systemexclamation)  ;  yield()
              message('Please make sure ONE of the other parts attached to this Job is '&|
                      'marked as the ''Main Part''.', |
                      'ServiceBase 2000', icon:exclamation)
              main_part_temp  = wartmp:main_part
          End!If wartmp:main_part = 'YES'
      End!If wartmp:main_part <> main_part_temp
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?wartmp:Part_Number
      If ~0{prop:acceptall}
      access:stock.clearkey(sto:manufacturer_key)
      sto:manufacturer = glo:select11
      sto:part_number  = wartmp:part_number
      if access:stock.fetch(sto:manufacturer_key)
          beep(beep:systemexclamation)  ;  yield()
          case message('This Part Number does not exist in the Stock. '&|
                  '||Do you want to select another Model Number or continue.', |
                  'ServiceBase 2000', icon:exclamation, |
                   'Select|Continue', 1, 0)
          of 1  ! name: select  (default)
              Post(event:accepted,?browse_stock_button)
          of 2  ! name: continue
              Select(?wartmp:description)
          end !case
      
      Else!if access:stock.fetch(sto:manufacturer_key)
          wartmp:Part_Number    = sto:Part_Number
          wartmp:Description    = sto:Description
          wartmp:Supplier       = sto:Supplier
          wartmp:Purchase_Cost  = sto:Purchase_Cost
          wartmp:Sale_Cost      = sto:Sale_Cost
          wartmp:Retail_Cost    = sto:Retail_Cost
          wartmp:part_ref_number = sto:ref_number
          Select(?wartmp:quantity)
          display()
      end
      End!If ~0{prop:acceptall}
    OF ?browse_stock_button
      ThisWindow.Update
      access:users.clearkey(use:password_key)
      use:password    =glo:password
      access:users.fetch(use:password_key)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browse_model_stock(glo:select12,use:user_code)
      if globalresponse = requestcompleted
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = stm:ref_number
          if access:stock.fetch(sto:ref_number_key)
              beep(beep:systemhand)  ;  yield()
              message('Error! Cannot access the Stock File.', |
                      'ServiceBase 2000', icon:hand)
          Else!if access:stock.fetch(sto:ref_number_key)
              wartmp:Part_Number    = sto:Part_Number
              wartmp:Description    = sto:Description
              wartmp:Supplier       = sto:Supplier
              wartmp:Purchase_Cost  = sto:Purchase_Cost
              wartmp:Sale_Cost      = sto:Sale_Cost
              wartmp:Retail_Cost    = sto:Retail_Cost
              wartmp:part_ref_number = sto:ref_number
              If sto:Assign_Fault_Codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = glo:Select12
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      wartmp:fault_code1    = stm:Faultcode1
                      wartmp:fault_code2    = stm:Faultcode2
                      wartmp:fault_code3    = stm:Faultcode3
                      wartmp:fault_code4    = stm:Faultcode4
                      wartmp:fault_code5    = stm:Faultcode5
                      wartmp:fault_code6    = stm:Faultcode6
                      wartmp:fault_code7    = stm:Faultcode7
                      wartmp:fault_code8    = stm:Faultcode8
                      wartmp:fault_code9    = stm:Faultcode9
                      wartmp:fault_code10   = stm:Faultcode10
                      wartmp:fault_code11   = stm:Faultcode11
                      wartmp:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
      
              End !If sto:Assign_Fault_Codes = 'YES'
              Select(?wartmp:quantity)
              !TH 09/02/04
              Access:LOCATION.ClearKey(loc:ActiveLocationKey)
              loc:Active   = 1
              loc:Location = sto:Location
              If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
      
              if loc:PickNoteEnable then ?wartmp:Exclude_From_Order{prop:disable} = true.
              display()
          end!if access:stock.fetch(sto:ref_number_key)
      end
      globalrequest     = saverequest#
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code1 = TINCALENDARStyle1(wartmp:Fault_Code1)
          Display(?wartmp:Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wartmp:Fault_Code1
      If wartmp:fault_code1 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 1
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 1
                  mfp:field        = wartmp:fault_code1
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code1 = mfp:field
                      else
                          wartmp:fault_code1 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code1)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?wartmp:Fault_Code2
      If wartmp:fault_code2 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 2
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 2
                  mfp:field        = wartmp:fault_code2
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code2 = mfp:field
                      else
                          wartmp:fault_code2 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code2)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?Button4:2
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 2
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code2 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code2 = TINCALENDARStyle1(wartmp:Fault_Code2)
          Display(?wartmp:Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 1
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code1 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?wartmp:Fault_Code3
      If wartmp:fault_code3 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 3
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 3
                  mfp:field        = wartmp:fault_code3
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code3 = mfp:field
                      else
                          wartmp:fault_code3 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code3)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?Button4:3
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 3
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code3 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code3 = TINCALENDARStyle1(wartmp:Fault_Code3)
          Display(?wartmp:Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 4
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code4 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?wartmp:Fault_Code4
      If wartmp:fault_code4 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 4
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 4
                  mfp:field        = wartmp:fault_code4
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code4 = mfp:field
                      else
                          wartmp:fault_code4 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code4)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?Button4:5
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 5
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code5 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code4 = TINCALENDARStyle1(wartmp:Fault_Code4)
          Display(?wartmp:Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:6
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 6
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code6 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code5 = TINCALENDARStyle1(wartmp:Fault_Code5)
          Display(?wartmp:Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:7
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 7
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code7 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?wartmp:Fault_Code7
      If wartmp:fault_code7 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 7
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 7
                  mfp:field        = wartmp:fault_code7
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code7 = mfp:field
                      else
                          wartmp:fault_code7 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code7)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code7 = TINCALENDARStyle1(wartmp:Fault_Code7)
          Display(?wartmp:Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:8
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 8
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code8 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?wartmp:Fault_Code8
      If wartmp:fault_code8 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 8
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 8
                  mfp:field        = wartmp:fault_code8
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code8 = mfp:field
                      else
                          wartmp:fault_code8 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code8)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code8 = TINCALENDARStyle1(wartmp:Fault_Code8)
          Display(?wartmp:Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code6 = TINCALENDARStyle1(wartmp:Fault_Code6)
          Display(?wartmp:Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:9
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 9
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code9 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?Button4:10
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 10
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code10 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?wartmp:Fault_Code9
      If wartmp:fault_code9 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 9
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 9
                  mfp:field        = wartmp:fault_code9
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code9 = mfp:field
                      else
                          wartmp:fault_code9 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code9)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?wartmp:Fault_Code10
      If wartmp:fault_code10 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 10
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 10
                  mfp:field        = wartmp:fault_code10
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code10 = mfp:field
                      else
                          wartmp:fault_code10 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code10)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code9 = TINCALENDARStyle1(wartmp:Fault_Code9)
          Display(?wartmp:Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wartmp:Fault_Code11
      If wartmp:fault_code11 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 11
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 11
                  mfp:field        = wartmp:fault_code11
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code11 = mfp:field
                      else
                          wartmp:fault_code11 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code11)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code10 = TINCALENDARStyle1(wartmp:Fault_Code10)
          Display(?wartmp:Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wartmp:Fault_Code12
      If wartmp:fault_code12 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 12
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 12
                  mfp:field        = wartmp:fault_code12
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code12 = mfp:field
                      else
                          wartmp:fault_code12 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code12)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code11 = TINCALENDARStyle1(wartmp:Fault_Code11)
          Display(?wartmp:Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:11
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 11
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code11 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wartmp:Fault_Code12 = TINCALENDARStyle1(wartmp:Fault_Code12)
          Display(?wartmp:Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:12
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = glo:select12
      glo:select2  = 12
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wartmp:fault_code12 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?wartmp:Fault_Code5
      If wartmp:fault_code5 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 5
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 5
                  mfp:field        = wartmp:fault_code5
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code5 = mfp:field
                      else
                          wartmp:fault_code5 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code5)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?wartmp:Fault_Code6
      If wartmp:fault_code6 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = glo:select12
          map:field_number = 6
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = glo:select12
                  mfp:field_number = 6
                  mfp:field        = wartmp:fault_code6
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          wartmp:fault_code6 = mfp:field
                      else
                          wartmp:fault_code6 = ''
                          select(?-1)
                      end
                      display(?wartmp:fault_code6)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wartmp:fault_code1 <> ''
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  setcursor(cursor:wait)
  save_wartmp_ali_id = access:wparttmp_alias.savefile()
  access:wparttmp_alias.clearkey(wartmp_ali:part_ref_number2_key)
  wartmp_ali:part_ref_number = wartmp:part_ref_number
  set(wartmp_ali:part_ref_number2_key,wartmp_ali:part_ref_number2_key)
  loop
      if access:wparttmp_alias.next()
         break
      end !if
      if wartmp_ali:part_ref_number <> wartmp:part_ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      If wartmp_ali:record_number <> wartmp:record_number
          found# = 1
          Break
      End!    If wartmp_ali:record_number <> wartmp:record_number
  end !loop
  access:wparttmp_alias.restorefile(save_wartmp_ali_id)
  setcursor()
  If found# = 1
      beep(beep:systemhand)  ;  yield()
      message('This part has already been selected.', |
             'ServiceBase 2000', icon:hand)
      Select(?wartmp:part_number)
      Cycle
  End !If found# = 1
  
  !Start - Check if the part has been suspended - TrkBs: 5372 (DBH: 18-02-2005)
  Access:STOCK.Clearkey(sto:Manufacturer_Key)
  sto:Manufacturer    = glo:Select11
  sto:Part_Number     = wartmp:Part_Number
  If Access:STOCK.Tryfetch(sto:Manufacturer_Key) = Level:Benign
      !Found
      If sto:Suspend = 1
          Case MessageEx('The selected part has been suspended. '&|
            '<13,10>'&|
            '<13,10>Please select another.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,14084079,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?wartmp:Part_Number)
          Cycle
      End ! If sto:Suspend = 1
  Else ! If Access:STOCK.Tryfetch(sto:Manufacturer_Key) = Level:Benign
      !Error
  End ! If Access:STOCK.Tryfetch(sto:Manufacturer_Key) = Level:Benign
  !End   - Check if the part has been suspended - TrkBs: 5372 (DBH: 18-02-2005)
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?wartmp:Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?wartmp:Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?wartmp:Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?wartmp:Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?wartmp:Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?wartmp:Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?wartmp:Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?wartmp:Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?wartmp:Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  OF ?wartmp:Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?wartmp:Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?wartmp:Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:OpenWindow
              adjustment_temp = partmp:Adjustment
      Do Adjustment
      If view_only# <> 1
          Do Hide_Fields
      End
      If ThisWindow.Request = Insertrecord
          wartmp:quantity = 1
          Do show_parts
      Else
          Disable(?browse_stock_button)
      End
      !Save Fields
      quantity_temp = wartmp:quantity
      main_part_temp  = wartmp:main_part
      !Security Check
      If SecurityCheck('JOB PART COSTS - EDIT')
          ?wartmp:purchase_cost{prop:readonly} = 1
          ?wartmp:sale_cost{prop:readonly} = 1
          ?wartmp:purchase_cost{prop:color} = color:silver
          ?wartmp:sale_cost{prop:color} = color:silver
      End!"If x" = False
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          0{prop:buffer} = 1
      ! Fault Coding (Hopefully)
      If glo:select11 = 'NEC'
          Unhide(?wartmp:main_part)
      Else
          Hide(?wartmp:main_part)
      End
      required# = 0
      
      found# = 0
      setcursor(cursor:wait)
      save_map_id = access:manfaupa.savefile()
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = glo:select11
      set(map:field_number_key,map:field_number_key)
      loop
          if access:manfaupa.next()
             break
          end !if
          if map:manufacturer <> glo:select11  |
              then break.  ! end if
      
          Case map:field_number
              Of 1
                  found# = 1
                  Unhide(?wartmp:fault_code1:prompt)
                  ?wartmp:fault_code1:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar)
                      ?popcalendar{prop:xpos} = 212
                      Unhide(?wartmp:fault_code1:2)
                      ?wartmp:fault_code1:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code1)
                      If map:lookup = 'YES'
                          Unhide(?button4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code1{prop:req} = 1
                      ?wartmp:fault_code1:2{prop:req} = 1
                  else
                      ?wartmp:fault_code1{prop:req} = 0
                      ?wartmp:fault_code1:2{prop:req} = 0
                  End
              Of 2
                  found# = 1
                  Unhide(?wartmp:fault_code2:prompt)
                  ?wartmp:fault_code2:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:2)
                      ?popcalendar:2{prop:xpos} = 212
                      Unhide(?wartmp:fault_code2:2)
                      ?wartmp:fault_code2:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code2)
                      If map:lookup = 'YES'
                          Unhide(?button4:2)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code2{prop:req} = 1
                      ?wartmp:fault_code2:2{prop:req} = 1
                  else
                      ?wartmp:fault_code2{prop:req} = 0
                      ?wartmp:fault_code2:2{prop:req} = 0
                  End
              Of 3
                  found# = 1
                  Unhide(?wartmp:fault_code3:prompt)
                  ?wartmp:fault_code3:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:3)
                      ?popcalendar:3{prop:xpos} = 212
                      Unhide(?wartmp:fault_code3:2)
                      ?wartmp:fault_code3:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code3)
                      If map:lookup = 'YES'
                          Unhide(?button4:3)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code3{prop:req} = 1
                      ?wartmp:fault_code3:2{prop:req} = 1
                  else
                      ?wartmp:fault_code3{prop:req} = 0
                      ?wartmp:fault_code3:2{prop:req} = 0
                  End
              Of 4
                  found# = 1
                  Unhide(?wartmp:fault_code4:prompt)
                  ?wartmp:fault_code4:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:4)
                      ?popcalendar:4{prop:xpos} = 212
                      Unhide(?wartmp:fault_code4:2)
                      ?wartmp:fault_code4:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code4)
                      If map:lookup = 'YES'
                          Unhide(?button4:4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code4{prop:req} = 1
                      ?wartmp:fault_code4:2{prop:req} = 1
                  else
                      ?wartmp:fault_code4{prop:req} = 0
                      ?wartmp:fault_code4:2{prop:req} = 0
                  End
              Of 5
                  found# = 1
                  Unhide(?wartmp:fault_code5:prompt)
                  ?wartmp:fault_code5:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:5)
                      ?popcalendar:5{prop:xpos} = 212
                      Unhide(?wartmp:fault_code5:2)
                      ?wartmp:fault_code5:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code5)
                      If map:lookup = 'YES'
                          Unhide(?button4:5)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code5{prop:req} = 1
                      ?wartmp:fault_code5:2{prop:req} = 1
                  else
                      ?wartmp:fault_code5{prop:req} = 0
                      ?wartmp:fault_code5:2{prop:req} = 0
                  End
              Of 6
                  found# = 1
                  Unhide(?wartmp:fault_code6:prompt)
                  ?wartmp:fault_code6:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:6)
                      ?popcalendar:6{prop:xpos} = 212
                      Unhide(?wartmp:fault_code6:2)
                      ?wartmp:fault_code6:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code6)
                      If map:lookup = 'YES'
                          Unhide(?button4:6)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code6{prop:req} = 1
                      ?wartmp:fault_code6:2{prop:req} = 1
                  else
                      ?wartmp:fault_code6{prop:req} = 0
                      ?wartmp:fault_code6:2{prop:req} = 0
                  End
              Of 7
                  found# = 1
                  Unhide(?wartmp:fault_code7:prompt)
                  ?wartmp:fault_code7:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:7)
                      ?popcalendar:7{prop:xpos} = 212
                      Unhide(?wartmp:fault_code7:2)
                      ?wartmp:fault_code7:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code7)
                      If map:lookup = 'YES'
                          Unhide(?button4:7)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code7{prop:req} = 1
                      ?wartmp:fault_code7:2{prop:req} = 1
                  else
                      ?wartmp:fault_code7{prop:req} = 0
                      ?wartmp:fault_code7:2{prop:req} = 0
                  End
      
              Of 8
                  found# = 1
                  Unhide(?wartmp:fault_code8:prompt)
                  ?wartmp:fault_code8:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:8)
                      ?popcalendar:8{prop:xpos} = 212
                      Unhide(?wartmp:fault_code8:2)
                      ?wartmp:fault_code8:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code8)
                      If map:lookup = 'YES'
                          Unhide(?button4:8)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code8{prop:req} = 1
                      ?wartmp:fault_code8:2{prop:req} = 1
                  else
                      ?wartmp:fault_code8{prop:req} = 0
                      ?wartmp:fault_code8:2{prop:req} = 0
                  End
      
              Of 9
                  found# = 1
                  Unhide(?wartmp:fault_code9:prompt)
                  ?wartmp:fault_code9:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:9)
                      ?popcalendar:9{prop:xpos} = 212
                      Unhide(?wartmp:fault_code9:2)
                      ?wartmp:fault_code9:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code9)
                      If map:lookup = 'YES'
                          Unhide(?button4:9)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code9{prop:req} = 1
                      ?wartmp:fault_code9:2{prop:req} = 1
                  else
                      ?wartmp:fault_code9{prop:req} = 0
                      ?wartmp:fault_code9:2{prop:req} = 0
                  End
      
              Of 10
                  found# = 1
                  Unhide(?wartmp:fault_code10:prompt)
                  ?wartmp:fault_code10:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:10)
                      ?popcalendar:10{prop:xpos} = 212
                      Unhide(?wartmp:fault_code10:2)
                      ?wartmp:fault_code10:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code10)
                      If map:lookup = 'YES'
                          Unhide(?button4:10)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code10{prop:req} = 1
                      ?wartmp:fault_code10:2{prop:req} = 1
                  else
                      ?wartmp:fault_code10{prop:req} = 0
                      ?wartmp:fault_code10:2{prop:req} = 0
                  End
      
              Of 11
                  found# = 1
                  Unhide(?wartmp:fault_code11:prompt)
                  ?wartmp:fault_code11:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:11)
                      ?popcalendar:11{prop:xpos} = 212
                      Unhide(?wartmp:fault_code11:2)
                      ?wartmp:fault_code11:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code11)
                      If map:lookup = 'YES'
                          Unhide(?button4:11)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code11{prop:req} = 1
                      ?wartmp:fault_code11:2{prop:req} = 1
                  else
                      ?wartmp:fault_code11{prop:req} = 0
                      ?wartmp:fault_code11:2{prop:req} = 0
                  End
      
              Of 12
                  found# = 1
                  Unhide(?wartmp:fault_code12:prompt)
                  ?wartmp:fault_code12:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:12)
                      ?popcalendar:12{prop:xpos} = 212
                      Unhide(?wartmp:fault_code12:2)
                      ?wartmp:fault_code12:2{prop:xpos} = 84
                  Else
                      Unhide(?wartmp:fault_code12)
                      If map:lookup = 'YES'
                          Unhide(?button4:12)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      ?wartmp:fault_code12{prop:req} = 1
                      ?wartmp:fault_code12:2{prop:req} = 1
                  else
                      ?wartmp:fault_code12{prop:req} = 0
                      ?wartmp:fault_code12:2{prop:req} = 0
                  End
      
          End !Case map:field_number
      end !loop
      access:manfaupa.restorefile(save_map_id)
      setcursor()
      
      If found# = 1
          Unhide(?fault_Code_tab)
      End!If found# = 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

