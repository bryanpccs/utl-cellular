

   MEMBER('loa01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('LOA01002.INC'),ONCE        !Local module procedure declarations
                     END


StockMovementExport PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
tmp:startdate        DATE
tmp:ModelNumber      STRING(30)
tmp:SMPFNumber       STRING(30)
tmp:DeliveryNote     STRING(30)
savepath             STRING(255)
save_mod_id          USHORT,AUTO
save_logsth_id       USHORT,AUTO
tmp:EndDate          DATE
tmp:StockLocation    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:StockLocation
logstl:Location        LIKE(logstl:Location)          !List box control field - type derived from field
logstl:RefNumber       LIKE(logstl:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(LOGSTOLC)
                       PROJECT(logstl:Location)
                       PROJECT(logstl:RefNumber)
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Window
mo:SelectedField  Long
window               WINDOW('Stock Movement Date Range'),AT(,,220,99),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,64),USE(?Sheet1),SPREAD
                         TAB('Stock Movement Date Range'),USE(?Tab1)
                           COMBO(@s30),AT(84,20,124,10),USE(tmp:StockLocation),IMM,VSCROLL,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Start Date'),AT(8,36),USE(?tmp:startdate:Prompt)
                           PROMPT('Stock Location'),AT(8,20),USE(?tmp:startdate:Prompt:2)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:startdate),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,36,10,10),USE(?PopCalendar),SKIP,ICON('calenda2.ico')
                           PROMPT('End Date'),AT(8,52),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(84,52,64,10),USE(tmp:EndDate),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(152,52,10,10),USE(?PopCalendar:2),SKIP,ICON('calenda2.ico')
                         END
                       END
                       PANEL,AT(4,72,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,76,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,76,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        display()
      end
    end

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        case messageex('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,charset:ansi,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            of 1 ! &yes button
                tmp:cancel = 1
            of 2 ! &no button
        end!case messageex
    end!if cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StockMovementExport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:startdate',tmp:startdate,'StockMovementExport',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'StockMovementExport',1)
    SolaceViewVars('tmp:SMPFNumber',tmp:SMPFNumber,'StockMovementExport',1)
    SolaceViewVars('tmp:DeliveryNote',tmp:DeliveryNote,'StockMovementExport',1)
    SolaceViewVars('savepath',savepath,'StockMovementExport',1)
    SolaceViewVars('save_mod_id',save_mod_id,'StockMovementExport',1)
    SolaceViewVars('save_logsth_id',save_logsth_id,'StockMovementExport',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'StockMovementExport',1)
    SolaceViewVars('tmp:StockLocation',tmp:StockLocation,'StockMovementExport',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StockLocation;  SolaceCtrlName = '?tmp:StockLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:startdate:Prompt;  SolaceCtrlName = '?tmp:startdate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:startdate:Prompt:2;  SolaceCtrlName = '?tmp:startdate:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:startdate;  SolaceCtrlName = '?tmp:startdate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('StockMovementExport')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'StockMovementExport')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StockLocation
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOGSTHII.Open
  Relate:LOGSTOLC.Open
  Relate:MODELNUM.Open
  Access:LOGSTHIS.UseFile
  Access:LOGSTOCK.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  tmp:startdate   = Deformat('1/1/1990',@d6)
  tmp:enddate     = Today()
  ?tmp:startdate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,?Sheet1)
  window{prop:buffer} = 1
  FDCB6.Init(tmp:StockLocation,?tmp:StockLocation,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOGSTOLC,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(logstl:LocationKey)
  FDCB6.AddField(logstl:Location,FDCB6.Q.logstl:Location)
  FDCB6.AddField(logstl:RefNumber,FDCB6.Q.logstl:RefNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGSTHII.Close
    Relate:LOGSTOLC.Close
    Relate:MODELNUM.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'StockMovementExport',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:startdate = TINCALENDARStyle1(tmp:startdate)
          Display(?tmp:startdate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      savepath = path()
      glo:file_name = 'SM' & Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Sub(Format(Year(Today()),@n04),3,2) & '.CSV'
      if not filedialog ('Choose File',glo:file_name,'CSV Files|*.*|All Files|*.*', |
                  file:keepdir + file:noerror + file:longname)
          !failed
          setpath(savepath)
      else!if not filedialog
          !found
      
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          open(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
      
          recordstoprocess    = Abs(tmp:enddate - tmp:startdate)
      
      !---before routine
      
          Remove(glo:file_name)
      
          setpath(savepath)
          access:expgendm.open()
          access:expgendm.usefile()
      !Fill in the top line with all the Model Numbers
      
          Clear(exp:record)
          x# = 4
          save_mod_id = access:modelnum.savefile()
          access:modelnum.clearkey(mod:manufacturer_key)
          mod:manufacturer = 'NOKIA'
          set(mod:manufacturer_key,mod:manufacturer_key)
          loop
              if access:modelnum.next()
                 break
              end !if
              if mod:manufacturer <> 'NOKIA'      |
                  then break.  ! end if
              x# += 1
              exp:line1[x#]    = mod:model_Number
          end !loop
          access:modelnum.restorefile(save_mod_id)
          access:expgendm.insert()
      
      !Loop through all the days in the date range
          Clear(exp:record)
          Loop x$ = tmp:startdate To tmp:enddate
              do getnextrecord2
              cancelcheck# += 1
              if cancelcheck# > (recordstoprocess/100)
                  do cancelcheck
                  if tmp:cancel = 1
                      break
                  end!if tmp:cancel = 1
                  cancelcheck# = 0
              end!if cancelcheck# > 50
      
      !Loop through all the models again, and then find a stock history entry for the model
              x# = 4
              count# = 0
              tmp:SMPFNumber      = ''
              tmp:DeliveryNote    = ''
      
              save_mod_id = access:modelnum.savefile()
              access:modelnum.clearkey(mod:manufacturer_key)
              mod:manufacturer = 'NOKIA'
              set(mod:manufacturer_key,mod:manufacturer_key)
              loop
                  if access:modelnum.next()
                     break
                  end !if
                  if mod:manufacturer <> 'NOKIA'      |
                      then break.  ! end if
                  x# += 1
                  exp:line1[x#] = 0
      
                  first# = 0
                  last# = 0
      !Save the model number, so that if there is a history entry on the same day, but for a
      !different model number it will create a new entry
      
                  save_logsth_id = access:logsthis.savefile()
                  access:logsthis.clearkey(logsth:datestaclubkey)
                  logsth:date      = x$
                  logsth:status    = 'AVAILABLE'
                  logsth:modelnumber  = mod:model_number
                  logsth:Location     = tmp:StockLocation
                  set(logsth:datestaclubkey,logsth:datestaclubkey)
                  loop
                      if access:logsthis.next()
                         break
                      end !if
                      if logsth:date      <> x$      |
                      or logsth:status    <> 'AVAILABLE'      |
                      or logsth:modelnumber <> mod:model_number |
                      or logsth:location  <> tmp:stocklocation|
                          then break.  ! end if
                      access:logstock.clearkey(logsto:refnumberkey)
                      logsto:refnumber = logsth:RefNumber
                      if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                          Clear(exp:record)
                          exp:line1[1] = Format(x$,@d6)
                          exp:line1[4] = 'IN'
                          exp:line1[2]    = logsth:SMPFNumber
                          exp:line1[3]    = logsth:DespatchNoteNo
                          exp:line1[x#]   = logsth:stockin
                          access:expgendm.insert()
                      End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
      
                  end !loop
                  access:logsthis.restorefile(save_logsth_id)
              end !loop
              access:modelnum.restorefile(save_mod_id)
      
      !---insert routine
              x# = 4
              count# = 0
              save_mod_id = access:modelnum.savefile()
              access:modelnum.clearkey(mod:manufacturer_key)
              mod:manufacturer = 'NOKIA'
              set(mod:manufacturer_key,mod:manufacturer_key)
              loop
                  if access:modelnum.next()
                     break
                  end !if
                  if mod:manufacturer <> 'NOKIA'      |
                      then break.  ! end if
                  x# += 1
                  exp:line1[x#] = 0
      
                  save_logsth_id = access:logsthis.savefile()
                  access:logsthis.clearkey(logsth:datestaclubkey)
                  logsth:date      = x$
                  logsth:status    = 'DESPATCHED'
                  logsth:modelnumber  = mod:model_number
                  logsth:location     = tmp:StockLocation
                  set(logsth:datestaclubkey,logsth:datestaclubkey)
                  loop
                      if access:logsthis.next()
                         break
                      end !if
                      if logsth:date      <> x$      |
                      or logsth:status    <> 'DESPATCHED'      |
                      or logsth:modelnumber <> mod:model_number |
                      or logsth:location  <> tmp:stocklocation |
                          then break.  ! end if
                      access:logstock.clearkey(logsto:refnumberkey)
                      logsto:refnumber = logsth:RefNumber
                      if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                          Clear(exp:record)
                          exp:line1[1] = Format(x$,@d6)
                          exp:line1[4] = 'OUT'
                          exp:line1[2]    = logsth:SMPFNumber
                          exp:line1[3]    = logsth:DespatchNoteNo
                          exp:line1[x#] += logsth:stockout
                          access:expgendm.insert()
                      End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
                  end !loop
                  access:logsthis.restorefile(save_logsth_id)
              end !loop
              access:modelnum.restorefile(save_mod_id)
      
          End!Loop x$ = tmp:startdate To tmp:enddate
          
          access:expgendm.close()
      !---after routine
      
          do endprintrun
          close(progresswindow)
      
          Case MessageEx('Export Completed.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      end!if not filedialog
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'StockMovementExport')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
    ThisMakeover.TakeEvent(Win:Window,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:startdate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

