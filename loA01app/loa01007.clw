

   MEMBER('loa01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('LOA01007.INC'),ONCE        !Local module procedure declarations
                     END


StockLevelCriteria PROCEDURE                          !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::6:TAGFLAG          BYTE(0)
DASBRW::6:TAGMOUSE         BYTE(0)
DASBRW::6:TAGDISPSTATUS    BYTE(0)
DASBRW::6:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:tag              STRING(1)
tmp:tag2             STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW2::View:Browse    VIEW(LOGSTOLC)
                       PROJECT(logstl:Location)
                       PROJECT(logstl:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
logstl:Location        LIKE(logstl:Location)          !List box control field - type derived from field
logstl:RefNumber       LIKE(logstl:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(LOGCLSTE)
                       PROJECT(logclu:ClubNokia)
                       PROJECT(logclu:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:tag2               LIKE(tmp:tag2)                 !List box control field - type derived from local data
tmp:tag2_Icon          LONG                           !Entry's icon ID
logclu:ClubNokia       LIKE(logclu:ClubNokia)         !List box control field - type derived from field
logclu:RecordNumber    LIKE(logclu:RecordNumber)      !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
mo:SelectedTab    Long  ! Makeover Template      LocalTreat = Wizard
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Wizard
mo:SelectedField  Long
window               WINDOW('Stock Level Report Criteria'),AT(,,248,259),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       PROMPT('Stock Level Report Criteria'),AT(8,8),USE(?StockLevelReport),FONT(,12,,FONT:bold)
                       BUTTON('&Back'),AT(8,236,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,236,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('&Finish'),AT(128,236,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       BUTTON('Close'),AT(184,236,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,232,240,24),USE(?Panel1),FILL(COLOR:Silver)
                       SHEET,AT(4,4,240,224),USE(?Sheet1),SPREAD
                         TAB('Stock Level Report Criteria'),USE(?Tab1)
                           PROMPT('Stock Locations'),AT(93,24),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold+FONT:underline)
                           PROMPT('Select the Stock Locations to include on the report.'),AT(92,36,144,16),USE(?Prompt3)
                           LIST,AT(92,56,146,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Stock Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(141,100,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(149,124,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(92,204,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(140,204,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(189,204,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Club Nokia Sites'),AT(92,24),USE(?Prompt4),FONT(,,COLOR:Navy,FONT:bold+FONT:underline)
                           PROMPT('Select the Club Nokia Sites to include on the report.'),AT(92,36,144,16),USE(?Prompt5)
                           LIST,AT(92,56,146,144),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Club Nokia Site~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(133,124,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(141,148,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('&Tag'),AT(92,204,48,16),USE(?DASTAG:2),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(140,204,48,16),USE(?DASTAGAll:2),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(188,204,48,16),USE(?DASUNTAGALL:2),LEFT,ICON('untag.gif')
                         END
                       END
                       IMAGE('wizard.gif'),AT(8,76),USE(?Image1)
                     END

Wizard4         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::6:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW2.UpdateBuffer
   GLO:Queue.Pointer = logstl:Location
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = logstl:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(GLO:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW2.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = logstl:Location
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW2.Reset
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::6:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::6:QUEUE = GLO:Queue
    ADD(DASBRW::6:QUEUE)
  END
  FREE(GLO:Queue)
  BRW2.Reset
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::6:QUEUE.Pointer = logstl:Location
     GET(DASBRW::6:QUEUE,DASBRW::6:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = logstl:Location
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::6:DASSHOWTAG Routine
   CASE DASBRW::6:TAGDISPSTATUS
   OF 0
      DASBRW::6:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::6:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::6:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW2.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW7.UpdateBuffer
   GLO:Queue2.Pointer2 = logclu:ClubNokia
   GET(GLO:Queue2,GLO:Queue2.Pointer2)
  IF ERRORCODE()
     GLO:Queue2.Pointer2 = logclu:ClubNokia
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    tmp:tag2 = '*'
  ELSE
    DELETE(GLO:Queue2)
    tmp:tag2 = ''
  END
    Queue:Browse:1.tmp:tag2 = tmp:tag2
  IF (tmp:tag2 = '*')
    Queue:Browse:1.tmp:tag2_Icon = 2
  ELSE
    Queue:Browse:1.tmp:tag2_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW7.Reset
  FREE(GLO:Queue2)
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue2.Pointer2 = logclu:ClubNokia
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue2)
  BRW7.Reset
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue2)
    GET(GLO:Queue2,QR#)
    DASBRW::8:QUEUE = GLO:Queue2
    ADD(DASBRW::8:QUEUE)
  END
  FREE(GLO:Queue2)
  BRW7.Reset
  LOOP
    NEXT(BRW7::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer2 = logclu:ClubNokia
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer2)
    IF ERRORCODE()
       GLO:Queue2.Pointer2 = logclu:ClubNokia
       ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW7.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW7.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StockLevelCriteria',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:tag',tmp:tag,'StockLevelCriteria',1)
    SolaceViewVars('tmp:tag2',tmp:tag2,'StockLevelCriteria',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?StockLevelReport;  SolaceCtrlName = '?StockLevelReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StockLevelCriteria')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'StockLevelCriteria')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?StockLevelReport
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOGCLSTE.Open
  Relate:LOGSTOLC.Open
  SELF.FilesOpened = True
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:LOGSTOLC,SELF)
  BRW7.Init(?List:2,Queue:Browse:1.ViewPosition,BRW7::View:Browse,Queue:Browse:1,Relate:LOGCLSTE,SELF)
  OPEN(window)
  SELF.Opened=True
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
    Wizard4.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,logstl:LocationKey)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,logstl:Location,1,BRW2)
  BIND('tmp:tag',tmp:tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW2.AddField(tmp:tag,BRW2.Q.tmp:tag)
  BRW2.AddField(logstl:Location,BRW2.Q.logstl:Location)
  BRW2.AddField(logstl:RefNumber,BRW2.Q.logstl:RefNumber)
  BRW7.Q &= Queue:Browse:1
  BRW7.AddSortOrder(,logclu:ClubNokiaKey)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,logclu:ClubNokia,1,BRW7)
  BIND('tmp:tag2',tmp:tag2)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW7.AddField(tmp:tag2,BRW7.Q.tmp:tag2)
  BRW7.AddField(logclu:ClubNokia,BRW7.Q.logclu:ClubNokia)
  BRW7.AddField(logclu:RecordNumber,BRW7.Q.logclu:RecordNumber)
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,?Sheet1)
  window{prop:buffer} = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW7.AskProcedure = 0
      CLEAR(BRW7.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:LOGCLSTE.Close
    Relate:LOGSTOLC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'StockLevelCriteria',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard4.Validate()
        DISABLE(Wizard4.NextControl())
     ELSE
        ENABLE(Wizard4.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?VSBackButton
      ThisWindow.Update
         Wizard4.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard4.TakeAccepted()
    OF ?Finish
      ThisWindow.Update
      StockLevelReport
      ThisWindow.Reset
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::6:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'StockLevelCriteria')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
    ThisMakeover.TakeEvent(Win:Wizard,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab,Field())
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::6:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


BRW2.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = logstl:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW2::RecordStatus=ReturnValue
  IF BRW2::RecordStatus NOT=Record:OK THEN RETURN BRW2::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = logstl:Location
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::6:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW2::RecordStatus
  RETURN ReturnValue


BRW7.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = logclu:ClubNokia
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:tag2 = ''
    ELSE
      tmp:tag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:tag2 = '*')
    SELF.Q.tmp:tag2_Icon = 2
  ELSE
    SELF.Q.tmp:tag2_Icon = 1
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW7.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW7::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW7::RecordStatus=ReturnValue
  IF BRW7::RecordStatus NOT=Record:OK THEN RETURN BRW7::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = logclu:ClubNokia
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW7::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW7::RecordStatus
  RETURN ReturnValue

Wizard4.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW2.Q &= NULL) ! Has Browse Object been initialized?
       BRW2.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW7.Q &= NULL) ! Has Browse Object been initialized?
       BRW7.ResetQueue(Reset:Queue)
    END

Wizard4.TakeBackEmbed PROCEDURE
   CODE

Wizard4.TakeNextEmbed PROCEDURE
   CODE

Wizard4.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
