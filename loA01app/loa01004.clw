

   MEMBER('loa01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('LOA01004.INC'),ONCE        !Local module procedure declarations
                     END








LogDespatchNote PROCEDURE(f_RefNumber)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_logsti_id       USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:count            LONG
tmp:printedby        STRING(60)
tmp:defaulttelephone STRING(20)
tmp:defaultfax       STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!-----------------------------------------------------------------------------
Process:View         VIEW(LOGSTOCK)
                       PROJECT(logsto:Description)
                       PROJECT(logsto:ModelNumber)
                       PROJECT(logsto:SalesCode)
                     END
report               REPORT,AT(396,3865,7521,7438),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(375,958,7521,2469),USE(?unnamed)
                         STRING('Printed By:'),AT(5000,365),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,365),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Despatch To:'),AT(5000,156),USE(?string27:4),TRN,FONT(,8,,)
                         STRING(@s30),AT(5885,156),USE(GLO:Select1),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,573),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,573),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,573),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,781),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,781),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,781),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,781,375,208),USE(?CPCSPgOfPgStr),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(260,1250),USE(logclu:ClubNokia),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING(@s30),AT(260,1406),USE(logclu:AddressLine1),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING(@s30),AT(260,1563),USE(logclu:AddressLine2),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING(@s30),AT(260,1719),USE(logclu:AddressLine3),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING(@s15),AT(260,1875),USE(logclu:Postcode),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING('Tel:'),AT(260,2031),USE(?String40),TRN,FONT('Arial',8,,)
                         STRING(@s15),AT(521,2031),USE(logclu:TelephoneNumber),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                         STRING('Fax:'),AT(260,2188),USE(?String40:2),TRN,FONT('Arial',8,,)
                         STRING(@s30),AT(521,2188),USE(logclu:FaxNumber),TRN,LEFT,FONT('Arial',8,,956,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,229),USE(?detailband)
                           STRING(@s30),AT(208,0),USE(logsto:SalesCode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(2188,0),USE(logsto:Description),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(4167,0),USE(logsto:ModelNumber),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s16),AT(6094,0),USE(logsti:IMEI),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                           LINE,AT(156,52,7188,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING('Total Lines:'),AT(208,104),USE(?String29),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(938,104),USE(tmp:count),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE,AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('DESPATCH NOTE'),AT(4063,0,3385,260),USE(?Title),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:defaulttelephone),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:defaultfax),TRN,FONT(,9,,)
                         STRING('DESPATCH ADDRESS'),AT(156,1458),USE(?String32),TRN,FONT(,9,,FONT:bold)
                         STRING('Sales Code'),AT(208,3125),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(2188,3125),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Model Number'),AT(4167,3125),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('I.M.E.I. Number'),AT(6094,3125),USE(?string25),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5504
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('LogDespatchNote')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:LOGSTOCK.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:LOGCLSTE.Open
  Relate:STANTEXT.Open
  Access:LOGSTHIS.UseFile
  Access:LOGSTHII.UseFile
  
  
  RecordsToProcess = RECORDS(LOGSTOCK)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(LOGSTOCK,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        ! Before Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
        access:logsthis.clearkey(logsth:despatchnokey)
        logsth:despatchno = f_RefNumber
        if access:logsthis.tryfetch(logsth:despatchnokey) = Level:Benign
        Compile('***',Debug=1)
            Message('1','Debug Message',icon:exclamation)
        ***
            access:logstock.clearkey(logsto:refnumberkey)
            logsto:refnumber = logsth:refnumber
            if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
        Compile('***',Debug=1)
            Message('2','Debug Message',icon:exclamation)
        ***
                save_logsti_id = access:logsthii.savefile()
                access:logsthii.clearkey(logsti:refnumberkey)
                logsti:refnumber = logsth:recordnumber
                set(logsti:refnumberkey,logsti:refnumberkey)
                loop
                    if access:logsthii.next()
                       break
                    end !if
                    if logsti:refnumber <> logsth:recordnumber      |
                        then break.  ! end if
        Compile('***',Debug=1)
            Message('Inside Loop','Debug Message',icon:exclamation)
        ***
                    tmp:count += 1
                    Print(rpt:detail)
                end !loop
                access:logsthii.restorefile(save_logsti_id)
            End!if access:logstock.tryfetch(logsto:refnumberkey) = Level:Benign
        End!if access:logsthis.tryfetch(logsth:despatchnokey) = Level:Benign
        
        
        LocalResponse = RequestCompleted
        BREAK
        ! After Embed Point: %HandCodeEventTimer) DESC(*HandCode* Top of EVENT:Timer (Process Files here)) ARG()
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(LOGSTOCK,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:LOGCLSTE.Close
    Relate:LOGSTHII.Close
    Relate:STANTEXT.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','DESPATCH NOTE - LOGITSTIC')
  Else
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'DESPATCH NOTE - LOGITSTIC'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      printer{propprint:copies} = dep:Copies
      previewreq = false
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      previewreq = true
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  Settarget(Report)
  ?title{prop:text} = 'DESPATCH NOTE - ' & F_RefNumber
  Settarget()
  set(defaults)
  access:defaults.next()
  Settarget(report)
  ?image1{prop:text} = 'Styles\rlistdet.gif'
  If def:remove_backgrounds = 'YES'
      ?image1{prop:text} = ''
  End!If def:remove_backgrounds = 'YES'
  Settarget()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  !Standard Text
  access:stantext.clearkey(stt:description_key)
  stt:description = 'DESPATCH NOTE - LOGITSTIC'
  access:stantext.fetch(stt:description_key)
  If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = stt:TelephoneNumber
  Else!If stt:TelephoneNumber <> ''
      tmp:DefaultTelephone    = def:Telephone_Number
  End!If stt:TelephoneNumber <> ''
  IF stt:FaxNumber <> ''
      tmp:DefaultFax  = stt:FaxNumber
  Else!IF stt:FaxNumber <> ''
      tmp:DefaultFax  = def:Fax_Number
  End!IF stt:FaxNumber <> ''
  !Default Printed
  access:defprint.clearkey(dep:Printer_Name_Key)
  dep:Printer_Name = 'DESPATCH NOTE - LOGITSTIC'
  If access:defprint.tryfetch(dep:Printer_Name_Key) = Level:Benign
      IF dep:background = 'YES'   
          Settarget(report)
          ?image1{prop:text} = ''
          Settarget()
      End!IF dep:background <> 'YES'
      
  End!If access:defprint.tryfetch(dep:PrinterNameKey) = Level:Benign
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='LogDespatchNote'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'LogDespatchNote',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'LogDespatchNote',1)
    SolaceViewVars('save_logsti_id',save_logsti_id,'LogDespatchNote',1)
    SolaceViewVars('LocalRequest',LocalRequest,'LogDespatchNote',1)
    SolaceViewVars('LocalResponse',LocalResponse,'LogDespatchNote',1)
    SolaceViewVars('FilesOpened',FilesOpened,'LogDespatchNote',1)
    SolaceViewVars('WindowOpened',WindowOpened,'LogDespatchNote',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'LogDespatchNote',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'LogDespatchNote',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'LogDespatchNote',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'LogDespatchNote',1)
    SolaceViewVars('PercentProgress',PercentProgress,'LogDespatchNote',1)
    SolaceViewVars('RecordStatus',RecordStatus,'LogDespatchNote',1)
    SolaceViewVars('EndOfReport',EndOfReport,'LogDespatchNote',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'LogDespatchNote',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'LogDespatchNote',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'LogDespatchNote',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'LogDespatchNote',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'LogDespatchNote',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'LogDespatchNote',1)
    SolaceViewVars('InitialPath',InitialPath,'LogDespatchNote',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'LogDespatchNote',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'LogDespatchNote',1)
    SolaceViewVars('tmp:count',tmp:count,'LogDespatchNote',1)
    SolaceViewVars('tmp:printedby',tmp:printedby,'LogDespatchNote',1)
    SolaceViewVars('tmp:defaulttelephone',tmp:defaulttelephone,'LogDespatchNote',1)
    SolaceViewVars('tmp:defaultfax',tmp:defaultfax,'LogDespatchNote',1)


BuildCtrlQueue      Routine







