

   MEMBER('celrapde.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:StatusBox        STRING(255)
save_sto_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sup_id          USHORT,AUTO
save_res_id          USHORT,AUTO
tmp:OldestBackOrder  DATE
tmp:OrderValue       REAL
window               WINDOW('Days End Procedure'),AT(,,220,129),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       TEXT,AT(4,4,212,100),USE(tmp:StatusBox)
                       BUTTON('Close'),AT(82,110,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StatusBox
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:LOCATION.Open
  Relate:RETSTOCK.Open
  Access:LOCVALUE.UseFile
  Access:STOCK.UseFile
  Access:ORDPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:SUPVALA.UseFile
  Access:SUPVALB.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:RETSTOCK.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      tmp:StatusBox = 'Begining Procedure, ' & |
                      '<13,10,13,10>(' & Format(Today(),@d6) & '  ' & Format(Clock(),@t1) &') Calculating Stock Values ......'
      
      
      Display()
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      thiswindow.reset(1)
      open(progresswindow)
      
      ?progress:userstring{prop:text} = 'Running...'
      ?progress:pcttext{prop:text} = '0% Completed'
      
      
      recordstoprocess    = Records(LOCATION)
      
      
      Save_loc_ID = Access:LOCATION.SaveFile()
      SET(LOCATION)
      Loop
          If Access:LOCATION.NEXT()
             Break
          End !If
          ?progress:userstring{prop:Text} = Clip(loc:Location)
          Do GetNextRecord2
          cancelcheck# += 1
          If cancelcheck# > (RecordsToProcess/100)
              Do cancelcheck
              If tmp:cancel = 1
                  Break
              End!If tmp:cancel = 1
              cancelcheck# = 0
          End!If cancelcheck# > 50
      
          If Access:LOCVALUE.PrimeRecord() = Level:Benign
      
              Save_sto_ID = Access:STOCK.SaveFile()
              Access:STOCK.ClearKey(sto:Location_Key)
              sto:Location    = loc:Location
              Set(sto:Location_Key,sto:Location_Key)
              Loop
                  If Access:STOCK.NEXT()
                     Break
                  End !If
                  If sto:Location    <> loc:Location      |
                      Then Break.  ! End If
                  lov:PurchaseTotal   += Round(sto:Purchase_Cost,.01)
                  lov:SaleCostTotal   += Round(sto:Sale_Cost,.01)
                  lov:RetailCostTotal += Round(sto:Retail_Cost,.01)
                  lov:QuantityTotal   += Round(sto:Quantity_Stock,.01)
              End !Loop
              Access:STOCK.RestoreFile(Save_sto_ID)
      
              lov:Location    = loc:Location
              lov:TheDate     = Today()
              If Access:LOCVALUE.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:LOCVALUE.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:LOCVALUE.TryInsert() = Level:Benign
          End !If Access:LOCVALUE.PrimeRecord() = Level:Benign
      
      End !Loop
      Access:LOCATION.RestoreFile(Save_loc_ID)
      
      Do EndPrintRun
      close(progresswindow)
      
      tmp:StatusBox = Clip(tmp:StatusBox) & '<13,10>(' & Format(Today(),@d6) & '  ' & Format(Clock(),@t1) &') Finished Stock Values.'
      tmp:StatusBox = Clip(tmp:StatusBox) & '<13,10,13,10>(' & Format(Today(),@d6) & '  ' & Format(Clock(),@t1) &') Calculating Back Order Dates....'
      Display()
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      thiswindow.reset(1)
      open(progresswindow)
      
      ?progress:userstring{prop:text} = 'Running...'
      ?progress:pcttext{prop:text} = '0% Completed'
      
      
      recordstoprocess    = Records(SUPPLIER)
      
      Save_sup_ID = Access:SUPPLIER.SaveFile()
      Set(sup:Company_Name_Key)
      Loop
          If Access:SUPPLIER.NEXT()
             Break
          End !If
          ?progress:userstring{prop:Text} = Clip(sup:Company_Name)
          Do GetNextRecord2
          cancelcheck# += 1
          If cancelcheck# > (RecordsToProcess/100)
              Do cancelcheck
              If tmp:cancel = 1
                  Break
              End!If tmp:cancel = 1
              cancelcheck# = 0
          End!If cancelcheck# > 50
      
      
          tmp:OldestBackOrder = ''
          Save_res_ID = Access:RETSTOCK.SaveFile()
          Access:RETSTOCK.ClearKey(res:DespatchedKey)
          res:Despatched = 'ORD'
          Set(res:DespatchedKey,res:DespatchedKey)
          Loop
              If Access:RETSTOCK.NEXT()
                 Break
              End !If
              If res:Despatched <> 'ORD'      |
                  Then Break.  ! End If
      
              If res:Supplier <> sup:Company_Name
                  Cycle
              End !If res:Supplier <> sup:Company_Name
      
              If tmp:OldestBackOrder = ''
                  tmp:OldestBackOrder = res:Despatch_Date
              End !If tmp:OlderestBackOrder = ''
              If res:Despatch_Date < tmp:OldestBackOrder
                  tmp:OldestBackOrder = res:Despatch_Date
              End !If res:Despatch_Date < tmp:OldestBackOrder
      
          End !Loop
          Access:RETSTOCK.RestoreFile(Save_res_ID)
          If tmp:OldestBackOrder <> ''
              If Access:SUPVALA.PrimeRecord() = Level:Benign
                  suva:Supplier   = sup:Company_Name
                  suva:RunDate    = Today()
                  suva:OldBackOrder   = tmp:OldestBackOrder
                  If Access:SUPVALA.TryInsert() = Level:Benign
                      !Insert Successful
                  Else !If Access:SUPVALA.TryInsert() = Level:Benign
                      !Insert Failed
                  End !If Access:SUPVALA.TryInsert() = Level:Benign
              End !If Access:SUPVALA.PrimeRecord() = Level:Benign
          End !If tmp:OldestBackOrder <> ''
      
          Save_loc_ID = Access:LOCATION.SaveFile()
          Set(loc:Location_Key)
          Loop
              If Access:LOCATION.NEXT()
                 Break
              End !If
      
              tmp:OrderValue = 0
              Save_res_ID = Access:RETSTOCK.SaveFile()
              Access:RETSTOCK.ClearKey(res:DespatchedKey)
              res:Despatched = 'ORD'
              Set(res:DespatchedKey,res:DespatchedKey)
              Loop
                  If Access:RETSTOCK.NEXT()
                     Break
                  End !If
                  If res:Despatched <> 'ORD'      |
                      Then Break.  ! End If
                  If res:Supplier <> sup:Company_Name
                      Cycle
                  End !If res:Supplier <> sup:Company_Name
                  Access:STOCK.ClearKey(sto:Ref_Number_Key)
                  sto:Ref_Number = res:Part_Ref_Number
                  If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
                      If sto:Location <> loc:Location
                          Cycle
                      End !If sto:Location <> loc:Location
                  Else!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      Cycle
                  End!If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                  tmp:OrderValue += res:Item_Cost * res:Quantity
              End !Loop
              Access:RETSTOCK.RestoreFile(Save_res_ID)
      
              If Access:SUPVALB.PrimeRecord() = Level:Benign
                  suvb:Supplier   = sup:Company_Name
                  suvb:RunDate    = Today()
                  suvb:Location   = loc:Location
                  suvb:BackORderValue = Round(tmp:OrderValue,.01)
                  If Access:SUPVALB.TryInsert() = Level:Benign
                      !Insert Successful
                  Else !If Access:.TryInsert() = Level:Benign
                      !Insert Failed
                  End !If Access:SUPVALB.TryInsert() = Level:Benign
              End !If Access:SUPVALB.PrimeRecord() = Level:Benign
      
          End !Loop
          Access:LOCATION.RestoreFile(Save_loc_ID)
      
      End !Loop
      Access:SUPPLIER.RestoreFile(Save_sup_ID)
      
      Do EndPrintRun
      close(progresswindow)
      
      tmp:StatusBox = Clip(tmp:StatusBox) & '<13,10>(' & Format(Today(),@d6) & '  ' & Format(Clock(),@t1) &') All Processes Finished.'
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

