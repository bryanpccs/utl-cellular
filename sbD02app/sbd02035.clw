

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02035.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Request_Label  PROCEDURE  (f_date,f_time,f_quantity,f_job_number,f_engineer,f_sale_cost) ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Stock_Request_Label')      !Add Procedure to Log
  end


    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    If tmp:High = 0
        Stock_Request_Label_NT(f_date,f_time,f_quantity,f_job_number,f_engineer,f_sale_cost)
    Else!If tmp:High = 0
        Stock_Request_Label_9X(f_date,f_time,f_quantity,f_job_number,f_engineer,f_sale_cost)
    End!If tmp:High = 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Request_Label',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:OS',tmp:OS,'Stock_Request_Label',1)
    SolaceViewVars('tmp:High',tmp:High,'Stock_Request_Label',1)
    SolaceViewVars('tmp:Low',tmp:Low,'Stock_Request_Label',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
