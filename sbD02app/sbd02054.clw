

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02054.INC'),ONCE        !Local module procedure declarations
                     END


Address_Label_NT PROCEDURE (func:Type,func:ConsignmentNo) !Generated from procedure template - Report

RejectRecord         LONG,AUTO
tmp:ConsignmentNumber STRING(30)
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CompanyName      STRING(30)
tmp:AddressLine1     STRING(30)
tmp:AddressLine2     STRING(30)
tmp:AddressLine3     STRING(30)
tmp:Postcode         STRING(30)
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(0,0,5000,2000),PAPER(PAPER:USER,5000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,5000,2000),USE(?Label)
                           STRING('ConsignmentNo:'),AT(625,1219),USE(?ConsignmentNo),TRN,FONT(,8,,)
                           STRING(@s30),AT(625,1375,1458,208),USE(tmp:ConsignmentNumber),TRN,FONT(,8,,)
                           STRING(@s60),AT(1292,177),USE(customer_name_temp),TRN
                           STRING(@s30),AT(1292,333),USE(tmp:CompanyName)
                           STRING(@s30),AT(1292,490),USE(tmp:AddressLine1)
                           STRING(@s30),AT(1292,656),USE(tmp:AddressLine2)
                           STRING(@s30),AT(1292,813),USE(tmp:AddressLine3)
                           STRING(@s30),AT(1292,969),USE(tmp:Postcode),TRN,LEFT
                           STRING(@s10),AT(2385,1375),USE(job_number_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(625,1510,2917,156),USE(Bar_Code2_Temp),TRN,LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@s10),AT(2385,1219,1156,156),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Address_Label_NT',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Address_Label_NT',1)
    SolaceViewVars('tmp:ConsignmentNumber',tmp:ConsignmentNumber,'Address_Label_NT',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Address_Label_NT',1)
    SolaceViewVars('code_temp',code_temp,'Address_Label_NT',1)
    SolaceViewVars('option_temp',option_temp,'Address_Label_NT',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Address_Label_NT',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Address_Label_NT',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Address_Label_NT',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Address_Label_NT',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Address_Label_NT',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Address_Label_NT',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Address_Label_NT',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Address_Label_NT',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Address_Label_NT',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Address_Label_NT',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Address_Label_NT',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Address_Label_NT',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Address_Label_NT',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Address_Label_NT',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Address_Label_NT',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Address_Label_NT',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Address_Label_NT',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Address_Label_NT',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Address_Label_NT',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Address_Label_NT',1)
    SolaceViewVars('InitialPath',InitialPath,'Address_Label_NT',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Address_Label_NT',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Address_Label_NT',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Address_Label_NT',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Address_Label_NT',1)
    SolaceViewVars('esn_temp',esn_temp,'Address_Label_NT',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Address_Label_NT',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Address_Label_NT',1)
    SolaceViewVars('job_type_temp',job_type_temp,'Address_Label_NT',1)
    SolaceViewVars('tmp:CompanyName',tmp:CompanyName,'Address_Label_NT',1)
    SolaceViewVars('tmp:AddressLine1',tmp:AddressLine1,'Address_Label_NT',1)
    SolaceViewVars('tmp:AddressLine2',tmp:AddressLine2,'Address_Label_NT',1)
    SolaceViewVars('tmp:AddressLine3',tmp:AddressLine3,'Address_Label_NT',1)
    SolaceViewVars('tmp:Postcode',tmp:Postcode,'Address_Label_NT',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Address_Label_NT')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Address_Label_NT')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Address_Label_NT',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Address Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Address_Label_NT')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'J' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE'
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY'
  End
  
  If job_ali:title = '' And job_ali:initial = ''
      customer_name_temp = Clip(job_ali:surname)
  Elsif job_ali:title = '' And job_ali:initial <> ''
      customer_name_temp = Clip(job_ali:initial) & ' ' & Clip(job_ali:surname)
  Elsif job_ali:title <> '' And job_ali:initial = ''
      customer_name_temp = Clip(job_ali:title) & ' ' & Clip(job_ali:surname)
  Elsif job_ali:title <> '' And job_ali:initial <> ''
      customer_name_Temp = Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & ' ' & CLip(job_ali:surname)
  Else
      customer_name_Temp = ''
  End
  
  Case func:Type
      Of 'COL'
          tmp:CompanyName    = job_ali:Company_Name_Collection
          tmp:AddressLine1    = job_ali:Address_Line1_Collection
          tmp:AddressLine2    = job_ali:Address_Line2_Collection
          tmp:AddressLIne3    = job_ali:Address_Line3_Collection
          tmp:Postcode        = job_ali:Postcode_Collection
      Of 'DEL'
          tmp:CompanyName    = job_ali:Company_Name_Delivery
          tmp:AddressLine1    = job_ali:Address_Line1_Delivery
          tmp:AddressLine2    = job_ali:Address_Line2_Delivery
          tmp:AddressLIne3    = job_ali:Address_Line3_Delivery
          tmp:Postcode        = job_ali:Postcode_Delivery
  
  End !func:Type
  
  SetTarget(Report)
  If func:ConsignmentNo <> ''
      ?tmp:ConsignmentNumber{prop:Hide} = 0
      ?ConsignmentNo{prop:Hide} = 0
      tmp:ConsignmentNumber = func:ConsignmentNo
      ! Show the consignment number barcode (if passed) - TrkBs: 6236 (DBH: 28-09-2005)
      bar_code_string_temp = Clip(tmp:ConsignmentNumber)
      SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
      ?Bar_Code2_Temp{Prop:Hide} = 0
  Else !func:ConsignmentNo <> ''
      ?tmp:ConsignmentNumber{prop:Hide} = 1
      ?ConsignmentNo{prop:Hide} = 1
      ?Bar_Code2_Temp{Prop:Hide} = 1
  End !func:ConsignmentNo <> ''
  SetTarget()
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

