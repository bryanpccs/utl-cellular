

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02021.INC'),ONCE        !Local module procedure declarations
                     END


QA_Failed_Label_Exchange_9X PROCEDURE                 !Generated from procedure template - Report

RejectRecord         LONG,AUTO
qa_reason_temp       STRING(255)
save_aud_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
engineer_temp        STRING(60)
tmp:PrintedBy        STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(EXCHANGE_ALIAS)
                       PROJECT(xch_ali:ESN)
                       PROJECT(xch_ali:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(125,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,3000,2000),USE(?Label)
                           STRING(@s30),AT(365,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('FAILED QA (Exchange)'),AT(750,208),USE(?String8),TRN,FONT(,10,,FONT:bold)
                           STRING('Date:'),AT(104,417),USE(?String3),TRN,FONT(,7,,)
                           STRING('xx/xx/xxxx'),AT(885,417),USE(?ReportDateStamp),TRN,FONT(,7,,FONT:bold)
                           STRING('Unit Details:'),AT(104,573),USE(?String3:2),TRN,FONT(,7,,)
                           STRING(@s255),AT(885,573,2083,156),USE(unit_details_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('hh:mm'),AT(1458,417),USE(?ReportTimeStamp),TRN,FONT(,7,,FONT:bold)
                           STRING('I.M.E.I. / E.S.N.'),AT(104,781),USE(?String5),TRN,FONT(,7,,)
                           STRING(@s30),AT(885,781),USE(xch_ali:ESN),FONT(,7,,FONT:bold)
                           STRING('Inspector:'),AT(104,1146),USE(?String5:4),TRN,FONT(,7,,)
                           STRING(@s60),AT(885,1146),USE(tmp:PrintedBy),FONT(,7,,FONT:bold)
                           STRING('Failure Reason:'),AT(104,1406),USE(?String5:5),TRN,FONT(,7,,)
                           TEXT,AT(885,1406,2031,365),USE(glo:Notes_Global),FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'QA_Failed_Label_Exchange_9X',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('qa_reason_temp',qa_reason_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('save_aud_id',save_aud_id,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('tmp:printer',tmp:printer,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('code_temp',code_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('option_temp',option_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('LocalRequest',LocalRequest,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('LocalResponse',LocalResponse,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('FilesOpened',FilesOpened,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('WindowOpened',WindowOpened,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('PercentProgress',PercentProgress,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('RecordStatus',RecordStatus,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('EndOfReport',EndOfReport,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('InitialPath',InitialPath,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('model_number_temp',model_number_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('esn_temp',esn_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('job_number_temp',job_number_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('job_type_temp',job_type_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('engineer_temp',engineer_temp,'QA_Failed_Label_Exchange_9X',1)
    SolaceViewVars('tmp:PrintedBy',tmp:PrintedBy,'QA_Failed_Label_Exchange_9X',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('QA_Failed_Label_Exchange_9X')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'QA_Failed_Label_Exchange_9X')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE_ALIAS.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:EXCHANGE_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, xch_ali:Ref_Number)
  ThisReport.AddSortOrder(xch_ali:Ref_Number_Key)
  ThisReport.AddRange(xch_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:EXCHANGE_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE_ALIAS.Close
    Relate:USERS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'QA_Failed_Label_Exchange_9X',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL - QA'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  setcursor(cursor:wait)
  save_aud_id = access:audit.savefile()
  access:audit.clearkey(aud:action_key)
  aud:ref_number = glo:select1
  aud:action     = 'QA REJECTION'
  set(aud:action_key,aud:action_key)
  loop
      !omemo(audit)
      if access:audit.next()
         break
      end !if
      if aud:ref_number <> glo:select1      |
      or aud:action     <> 'QA REJECTION'      |
          then break.  ! end if
      qa_reason_temp = aud:notes
      Break
  end !loop
  access:audit.restorefile(save_aud_id)
  setcursor()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  IF ~ReturnValue
    SELF.Report $ ?ReportDateStamp{PROP:Text}=FORMAT(TODAY(),@D17)
  END
  IF ~ReturnValue
    SELF.Report $ ?ReportTimeStamp{PROP:Text}=FORMAT(CLOCK(),@T7)
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'QA_Failed_Label_Exchange_9X')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  
  access:users.clearkey(use:password_key)
  use:password =glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  
  unit_details_temp = Clip(xch_ali:model_number) & ' ' & Clip(xch_ali:manufacturer)
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

