

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02016.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Request_Label_B452 PROCEDURE (f_date,f_time,f_quantity,f_job_number,f_engineer,f_sale_cost) !Generated from procedure template - Report

RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(STOCK_ALIAS)
                       PROJECT(sto_ali:Description)
                       PROJECT(sto_ali:Location)
                       PROJECT(sto_ali:Part_Number)
                       PROJECT(sto_ali:Ref_Number)
                       PROJECT(sto_ali:Shelf_Location)
                       PROJECT(sto_ali:Supplier)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(20,32,75,50),PAPER(PAPER:USER,105,80),PRE(RPT),FONT('Arial',10,,),MM
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,75,50),USE(?Label)
                           STRING(@s30),AT(9,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('Part Number:'),AT(3,14),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s30),AT(22,15,40,4),USE(sto_ali:Part_Number),FONT('Arial',7,,FONT:bold)
                           STRING('Description:'),AT(3,18),USE(?String20),TRN,FONT(,7,,)
                           STRING(@s30),AT(22,18),USE(sto_ali:Description),FONT('Arial',7,,FONT:bold)
                           STRING('Supplier:'),AT(3,22),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s30),AT(22,22),USE(sto_ali:Supplier),FONT('Arial',7,,FONT:bold)
                           STRING('Location:'),AT(3,26),USE(?String21),TRN,FONT(,7,,)
                           STRING(@s30),AT(22,26),USE(sto_ali:Location),FONT('Arial',7,,FONT:bold)
                           STRING('Job Number:'),AT(3,35),USE(?String19),TRN,FONT(,7,,)
                           STRING('Qty Requested.:'),AT(40,7),USE(?String14),TRN,FONT(,7,,)
                           STRING(@s8),AT(59,7),USE(f_quantity),TRN,LEFT,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                           STRING('Time Requested.:'),AT(3,9),USE(?String8:2),TRN,FONT(,7,,)
                           STRING(@t1),AT(22,9),USE(f_time),TRN,FONT('Arial',7,,FONT:bold)
                           STRING(@d6b),AT(22,7),USE(f_date),TRN,FONT('Arial',7,,FONT:bold)
                           STRING('Date Requested.:'),AT(3,7),USE(?String8),TRN,FONT(,7,,)
                           STRING('Engineer:'),AT(3,39),USE(?String15),TRN,FONT(,7,,)
                           STRING(@s30),AT(22,30,17,4),USE(sto_ali:Shelf_Location),FONT('Arial',7,,FONT:bold)
                           STRING('Shelf Location:'),AT(3,30),USE(?String18),TRN,FONT(,7,,)
                           STRING(@s30),AT(22,39),USE(f_engineer),TRN,LEFT,FONT('Arial',7,,FONT:bold)
                           STRING(@s30),AT(22,35),USE(f_job_number),TRN,LEFT,FONT('Arial',7,,FONT:bold)
                           STRING('Sale Cost:'),AT(3,43),USE(?String22),TRN,FONT(,7,,)
                           STRING(@n14.2),AT(22,43,17,4),USE(f_sale_cost),TRN,LEFT,FONT('Arial',7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Request_Label_B452',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Stock_Request_Label_B452',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Stock_Request_Label_B452',1)
    SolaceViewVars('code_temp',code_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('option_temp',option_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Stock_Request_Label_B452',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Stock_Request_Label_B452',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Request_Label_B452',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Stock_Request_Label_B452',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Stock_Request_Label_B452',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Stock_Request_Label_B452',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Stock_Request_Label_B452',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Stock_Request_Label_B452',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Stock_Request_Label_B452',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Stock_Request_Label_B452',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Stock_Request_Label_B452',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Stock_Request_Label_B452',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Stock_Request_Label_B452',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Stock_Request_Label_B452',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Stock_Request_Label_B452',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Stock_Request_Label_B452',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Stock_Request_Label_B452',1)
    SolaceViewVars('InitialPath',InitialPath,'Stock_Request_Label_B452',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Request_Label_B452',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Stock_Request_Label_B452',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('esn_temp',esn_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Stock_Request_Label_B452',1)
    SolaceViewVars('job_type_temp',job_type_temp,'Stock_Request_Label_B452',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Request_Label_B452')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Request_Label_B452')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBS_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:STOCK_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, sto_ali:Ref_Number)
  ThisReport.AddSortOrder(sto_ali:Ref_Number_Key)
  ThisReport.AddRange(sto_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:STOCK_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS_ALIAS.Close
    Relate:STOCK_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Request_Label_B452',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'STOCK REQUEST LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Request_Label_B452')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'J' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N. / I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE'
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY'
  End
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number) & ' (' & Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname) & ')'
  Else!If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number)
  End!
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

