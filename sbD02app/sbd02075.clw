

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02075.INC'),ONCE        !Local module procedure declarations
                     END


ToteLabel9X PROCEDURE (func:JobNumber,func:ConsignmentNumber) !Generated from procedure template - Report

RejectRecord         LONG,AUTO
save_job2_id         USHORT,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
tmp:accessories      STRING(255)
tmp:type             STRING(4)
tmp:UserName         STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Company          STRING(40)
tmp:JobNumber        LONG
tmp:CompanyName      STRING(30)
tmp:AddressLine1     STRING(30)
tmp:AddressLine2     STRING(30)
tmp:AddressLine3     STRING(30)
tmp:PostCode         STRING(30)
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Ref_Number)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                       END
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(100,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(10,10,3000,2000),USE(?Label),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(73,1271),USE(job_number_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(917,854),USE(tmp:AddressLine1),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s8),AT(73,1479,1302,260),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                           STRING(@s30),AT(917,1323),USE(tmp:PostCode),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(917,1010),USE(tmp:AddressLine2),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(917,1167),USE(tmp:AddressLine3),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s30),AT(73,490,2292,156),USE(func:ConsignmentNumber),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Tote Consignment'),AT(73,73),USE(?String5),TRN,LEFT,FONT(,8,,)
                           STRING('Job Number'),AT(73,1115),USE(?String5:2),TRN,FONT(,8,,)
                           STRING('Address'),AT(917,594),USE(?String5:3),TRN,FONT(,7,,)
                           STRING(@s30),AT(927,698),USE(tmp:CompanyName),TRN,LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           STRING(@s16),AT(73,229,2396,260),USE(Bar_Code2_Temp),LEFT,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ToteLabel9X',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'ToteLabel9X',1)
    SolaceViewVars('save_job2_id',save_job2_id,'ToteLabel9X',1)
    SolaceViewVars('save_jac_id',save_jac_id,'ToteLabel9X',1)
    SolaceViewVars('tmp:printer',tmp:printer,'ToteLabel9X',1)
    SolaceViewVars('code_temp',code_temp,'ToteLabel9X',1)
    SolaceViewVars('option_temp',option_temp,'ToteLabel9X',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'ToteLabel9X',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'ToteLabel9X',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'ToteLabel9X',1)
    SolaceViewVars('LocalRequest',LocalRequest,'ToteLabel9X',1)
    SolaceViewVars('LocalResponse',LocalResponse,'ToteLabel9X',1)
    SolaceViewVars('FilesOpened',FilesOpened,'ToteLabel9X',1)
    SolaceViewVars('WindowOpened',WindowOpened,'ToteLabel9X',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'ToteLabel9X',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'ToteLabel9X',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'ToteLabel9X',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'ToteLabel9X',1)
    SolaceViewVars('PercentProgress',PercentProgress,'ToteLabel9X',1)
    SolaceViewVars('RecordStatus',RecordStatus,'ToteLabel9X',1)
    SolaceViewVars('EndOfReport',EndOfReport,'ToteLabel9X',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'ToteLabel9X',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'ToteLabel9X',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'ToteLabel9X',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'ToteLabel9X',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'ToteLabel9X',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'ToteLabel9X',1)
    SolaceViewVars('InitialPath',InitialPath,'ToteLabel9X',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'ToteLabel9X',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'ToteLabel9X',1)
    SolaceViewVars('model_number_temp',model_number_temp,'ToteLabel9X',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'ToteLabel9X',1)
    SolaceViewVars('esn_temp',esn_temp,'ToteLabel9X',1)
    SolaceViewVars('job_number_temp',job_number_temp,'ToteLabel9X',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'ToteLabel9X',1)
    SolaceViewVars('job_type_temp',job_type_temp,'ToteLabel9X',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'ToteLabel9X',1)
    SolaceViewVars('tmp:type',tmp:type,'ToteLabel9X',1)
    SolaceViewVars('tmp:UserName',tmp:UserName,'ToteLabel9X',1)
    SolaceViewVars('tmp:Company',tmp:Company,'ToteLabel9X',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'ToteLabel9X',1)
    SolaceViewVars('tmp:CompanyName',tmp:CompanyName,'ToteLabel9X',1)
    SolaceViewVars('tmp:AddressLine1',tmp:AddressLine1,'ToteLabel9X',1)
    SolaceViewVars('tmp:AddressLine2',tmp:AddressLine2,'ToteLabel9X',1)
    SolaceViewVars('tmp:AddressLine3',tmp:AddressLine3,'ToteLabel9X',1)
    SolaceViewVars('tmp:PostCode',tmp:PostCode,'ToteLabel9X',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ToteLabel9X')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ToteLabel9X')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:JobNumber   = func:JobNumber
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBACC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:TRANTYPE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ToteLabel9X',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  
  
  ReturnValue = PARENT.OpenReport()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ProgressWindow{Prop:AcceptAll} THEN RETURN.
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)
  PARENT.Reset(Force)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ToteLabel9X')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.Update PROCEDURE

  CODE
  PARENT.Update
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job_ali:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(func:ConsignmentNumber)
  esn_temp = Clip(func:ConsignmentNumber)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  
  !I'm keep this in local variable, because I'm sure it will change
  !in future.
  tmp:CompanyName    = job_ali:Company_Name_Delivery
  tmp:AddressLine1    = job_Ali:Address_Line1_Delivery
  tmp:AddressLine2    = job_ali:Address_Line2_Delivery
  tmp:AddressLine3    = job_ali:Address_Line3_Delivery
  tmp:Postcode        = job_ali:Postcode
  
  
  
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

