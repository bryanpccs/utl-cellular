

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02091.INC'),ONCE        !Local module procedure declarations
                     END








GenericJobLabel PROCEDURE(f_Type)
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(40)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
tmp:accessories      STRING(255)
tmp:type             STRING(7)
tmp:UserName         STRING(30)
tmp:Company          STRING(40)
tmp:BookedDate       DATE
tmp:BookedTime       TIME
tmp:InWorkshopDate   BYTE(0)
tmp:BookedLabel      STRING(12)
tmp:HideIMEIBarcode  BYTE
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
save_job2_id         USHORT,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:PrintPreview     BYTE(0)
BreaksStarted        BYTE(0)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Location)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                       END
                     END
REPORT               REPORT,AT(2,2,77,52),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),MM
DETAIL                 DETAIL,AT(,,69,44),USE(?Label),TOGETHER
                         STRING(@s30),AT(2,0),USE(def:User_Name),TRN,FONT(,8,,FONT:bold)
                         STRING(@s8),AT(56,0),USE(job_number_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                         STRING(@t1b),AT(20,6),USE(tmp:BookedTime),TRN,FONT(,6,,)
                         STRING(@s2),AT(30,6),USE(jobe:SkillLevel),TRN,FONT(,6,,)
                         STRING('Contact:'),AT(2,9),USE(?Contact),TRN,FONT(,6,,)
                         STRING(@s20),AT(14,9,30,3),USE(customer_name_temp),TRN,LEFT,FONT(,7,,)
                         STRING('Company:'),AT(2,11),USE(?String20),TRN,FONT(,6,,)
                         STRING('Skill Lvl'),AT(28,4),USE(?String8:2),TRN,FONT(,6,,)
                         STRING(@s40),AT(14,11),USE(tmp:Company),TRN,LEFT,FONT(,7,,)
                         STRING('Job Type:'),AT(2,13),USE(?String11),TRN,FONT(,6,,)
                         STRING(@s40),AT(14,13),USE(job_type_temp),TRN,FONT(,7,,)
                         STRING(@s7),AT(52,10),USE(tmp:type),TRN,HIDE,FONT(,10,,)
                         STRING('Location:'),AT(2,30),USE(?Location),TRN,FONT(,6,,)
                         STRING(@s20),AT(14,30),USE(job_ali:Location),TRN,LEFT,FONT(,7,,)
                         STRING(@s30),AT(14,27),USE(tmp:UserName),TRN,LEFT,FONT(,7,,)
                         STRING('Access.:'),AT(2,22),USE(?String21:2),TRN,HIDE,FONT(,6,,)
                         TEXT,AT(14,22,52,5),USE(tmp:accessories),HIDE,FONT(,6,,)
                         STRING('Retained'),AT(2,24),USE(?Retained),TRN,HIDE,FONT(,6,,)
                         STRING('Cust Unit:'),AT(2,33),USE(?String19),TRN,FONT(,6,,)
                         STRING('Fault:'),AT(2,16),USE(?String14),TRN,FONT(,6,,)
                         TEXT,AT(15,17,52,4),USE(jbn_ali:Fault_Description),FONT(,6,,)
                         STRING(@d6b),AT(17,4),USE(tmp:BookedDate),TRN,FONT(,6,,)
                         STRING(@s12),AT(2,5),USE(tmp:BookedLabel),TRN,FONT(,6,,)
                         STRING(@s8),AT(33,6,33,4),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                         STRING(@s30),AT(14,33,17,4),USE(job_ali:Model_Number),TRN,LEFT,FONT(,7,,)
                         STRING('IMEI:'),AT(41,30),USE(?String18),TRN,FONT(,6,,)
                         STRING(@s16),AT(7,40,23,4),USE(job_ali:ESN),TRN,LEFT,FONT(,7,,)
                         STRING(@s16),AT(8,37,60,4),USE(Bar_Code2_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                         STRING(@s30),AT(32,33,17,4),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,7,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('GenericJobLabel')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = True
  BIND('GLO:Select1',GLO:Select1)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBS2_ALIAS.Open
  Relate:TRANTYPE.Open
  Access:JOBNOTES_ALIAS.UseFile
  Access:JOBACC.UseFile
  Access:JOBSE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  ! Before Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      SetTarget(Report)
      ?job_ali:Location{prop:Hide} = 1
      ?Location{prop:Hide} = 1
      SetTarget()
  End !def:HideLocation
  
  ! Start Change 2648 BE(22/05/2003)
  tmp:InWorkshopDate = GETINI('PRINTING','InWorkshopDate',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2648 BE(22/05/2003)
  
  ! Start Change 3701 BE(15/12/2003)
  tmp:HideIMEIBarcode = GETINI('PRINTING','HideIMEIBarcode',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 3701 BE(15/12/2003)
  
  
  !choose printer
  PreviewReq = True
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      PreviewReq = False
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          PreviewReq = False
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ! After Embed Point: %AfterFileOpen) DESC(Beginning of Procedure, After Opening Files) ARG()
  
  
  RecordsToProcess = RECORDS(JOBS_ALIAS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(JOBS_ALIAS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(job_ali:Ref_Number_Key)
      Process:View{Prop:Filter} = |
      'job_ali:Ref_Number = GLO:Select1'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        ! Before Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        set(defaults)
        access:defaults.next()
        
        Settarget(Report)
        If def:PrintBarcode = 1
            ! Start Change 3701 BE(15/12/03)
            !?Bar_Code2_Temp{prop:hide} = 0
            IF (tmp:HideIMEIBarcode) THEN
                ?Bar_Code2_Temp{prop:hide} = 1
            ELSE
                ?Bar_Code2_Temp{prop:hide} = 0
            END
            ! End Change 3701 BE(15/12/03)
        Else
            ?job_ali:ESN{prop:Hide} = 1
            ?String18{prop:Hide} = 1
            ?Bar_Code2_Temp{prop:Hide} = 1
        End!If def:PrintBarcode = 1
        Settarget()
        
        IF def:Job_Label_Accessories = 'YES'
          Settarget(Report)
          ?String21:2{Prop:Hide} = FALSE
          ?retained{Prop:Hide} = FALSE
          ?tmp:accessories{Prop:Hide} = FALSE
          Settarget()
        END
        
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job_ali:Ref_Number
        If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Found
        
        Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
        
        ! Start Change 2648 BE(22/05/2003)
        IF (tmp:InWorkshopDate) THEN
            tmp:BookedDate = jobe:InWorkshopDate
            tmp:BookedTime = jobe:InWorkshopTime
            IF (tmp:BookedDate = '') THEN
                tmp:BookedLabel = ''
            ELSE
                tmp:BookedLabel = 'In Workshop:'
            END
        ELSE
            tmp:BookedDate = job_ali:date_booked
            tmp:BookedTime = job_ali:time_booked
            tmp:BookedLabel = 'Booked:'
        END
        ! End Change 2648 BE(22/05/2003)
        
        code_temp = 3
        option_temp = 0
        bar_code_string_temp = Clip(job_ali:ref_number)
        job_number_temp = 'J' & Clip(job_ali:ref_number)
        SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
        
        bar_code_string_temp = Clip(job_ali:esn)
        esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
        SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
        
        If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
            job_Type_temp = 'SPLIT-' & Clip(Format(job_ali:Charge_Type,@s20)) & '/' & Clip(Format(job_ali:Warranty_Charge_Type,@s20))
        End
        If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
            job_type_temp = 'CHARGEABLE-'&CLIP(job_ali:Charge_Type)
        End
        If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
            job_Type_temp = 'WARRANTY-'&CLIP(job_ali:Warranty_Charge_Type)
        End
        If job_ali:surname <> ''
            customer_name_temp = Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                                ' ' & Clip(job_ali:surname)
        Else!If job_ali:surname <> ''
            customer_name_temp = ''
            SetTarget(Report)
            ?Contact{Prop:Hide} = 1
            SetTarget()
        End!
        tmp:Company = Clip(job_ali:Account_Number) & ' - ' & Clip(job_ali:Company_Name)
        
        acc_count# = 0
        save_jac_id = access:jobacc.savefile()
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job_ali:ref_number
        set(jac:ref_number_key,jac:ref_number_key)
        loop
            if access:jobacc.next()
               break
            end !if
            if jac:ref_number <> job_ali:ref_number  |
                then break.  ! end if
            acc_count# += 1
            If acc_count# = 1
                tmp:accessories = Clip(jac:accessory)
            Else!If acc_count# = 1
                tmp:accessories = Clip(tmp:accessories) & ', ' & Clip(jac:accessory)
            End!If acc_count# = 1
        end !loop
        access:jobacc.restorefile(save_jac_id)
        
        refurb# = 0
        
        access:jobnotes_alias.clearkey(JBN_ALI:RefNumberKey)
        JBN_ALI:RefNumber   = job_ali:ref_number
        access:jobnotes_alias.tryfetch(JBN_ALI:RefNumberKey)
        
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job_ali:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                If tra:refurbcharge = 'YES'
                    If job_ali:exchange_unit_number <> ''
                        refurb# = 1
                    End!If job_ali:exchange_unit_number <> ''
                    If refurb# = 0
                        access:trantype.clearkey(trt:transit_type_key)
                        trt:transit_type = job_ali:transit_type
                        if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                            If trt:exchange_unit = 'YES'
                                refurb# = 1
                            End!If trt:exchange = 'YES'
                        end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                    End!If refurb# = 0
                    If refurb# = 0
                        If Sub(job_ali:exchange_status,1,3) = '108'
                            refurb# = 1
                        End!If Sub(job_ali:current_status,1,3) = '108'
                    End!If refurb# = 0
                End!If tra:refurbcharge = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
        
        
        If refurb# = 1
            tmp:username    = 'REFURBISHMENT REQUIRED'
        Else!If refurb# = 1
            tmp:username    = ''
            ! Start Change 2760 BE(20/06/03)
            !If f_type <> ''
            IF (f_type = 'ER') THEN
            ! End Change 2760 BE(20/06/03)
                Settarget(report)
                Unhide(?tmp:type)
                tmp:type = f_type
                Settarget()
            Else
                ! Start Change 2821 BE(20/06/03)
                Bounce_String" = ''
                ! End Change 2821 BE(20/06/03)
                If job_ali:esn <> 'N/A'
                    ! Start Change 2760 BE(20/06/03)
                    !If CountBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:esn)
                    !    Settarget(report)
                    !    Unhide(?tmp:type)
                    !    tmp:type = 'SE'
                    !    Settarget()
                    !End!If found# = 1
                    ! Start Change 2821 BE(20/06/03
                    !bc# = CountBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:esn)
                    !IF (bc# > 0) THEN
                    !    Settarget(report)
                    !    Unhide(?tmp:type)
                    !    IF (bc# = 1) THEN
                    !        tmp:type = '2E'
                    !    ELSIF (bc# = 2) THEN
                    !        tmp:type = '3E'
                    !    ELSE
                    !        tmp:type = '3E+'
                    !    END
                    !    Settarget()
                    !END
                    bc# = CountBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:esn)
                    IF (bc# > 0) THEN
                        IF (bc# = 1) THEN
                            Bounce_String" = '2E'
                        ELSIF (bc# = 2) THEN
                            Bounce_String" = '3E'
                        ELSE
                            Bounce_String" = '3E+'
                        END
                    END
                    ! Start Change 2821 BE(20/06/03
                    ! End Change 2760 BE(20/06/03)
                End!If job2:esn <> 'N/A'
                ! Start Change 2821 BE(24/06/03)
                IF (job:mobile_number <> '') THEN
                    bc# = CountMobileBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:mobile_number)
                    IF (bc# > 0) THEN
                        IF (bc# = 1) THEN
                            IF (Bounce_String" = '') THEN
                                Bounce_String" = '2M'
                            ELSE
                                Bounce_String" = CLIP(Bounce_String") & ' 2M'
                            END
                        ELSIF (bc# = 2) THEN
                            IF (Bounce_String" = '') THEN
                                Bounce_String" = '3M'
                            ELSE
                                Bounce_String" = CLIP(Bounce_String") & ' 3M'
                            END
                        ELSE
                            IF (Bounce_String" = '') THEN
                                Bounce_String" = '3M+'
                            ELSE
                                Bounce_String" = CLIP(Bounce_String") & ' 3M+'
                            END
                        END
                    END
                END
                IF (Bounce_String" <> '') THEN
                    Bounce_String_Length# = LEN(CLIP(Bounce_String"))
                    IF (Bounce_String_Length# > 7) THEN
                        Bounce_String_Length# = 7
                    END
                    Bounce_String_Right" = '       '
                    Bounce_String_Right"[(8-Bounce_String_Length#) : 7] = Bounce_String"[1 : Bounce_String_Length#]
                    Settarget(report)
                    Unhide(?tmp:type)
                    tmp:type = Bounce_String_Right"[1 : 7]
                    Settarget()
                END
                ! End Change 2821 BE(24/06/03)
            End! If f_type <> ''
        End!If refurb# = 1
        
        
        ! After Embed Point: %BeforePrint) DESC(Before Printing Detail Section) ARG()
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(RPT:DETAIL)
        END
        
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS_ALIAS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(REPORT)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        REPORT{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),REPORT,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = REPORT{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,REPORT{PROPPRINT:Copies})
          REPORT{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND REPORT{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR REPORT{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(REPORT)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = REPORT{PROPPRINT:Copies}
        IF REPORT{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,REPORT{PROPPRINT:Copies})
        END
        REPORT{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(REPORT)
  ! Before Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  ! After Embed Point: %AfterClosingReport) DESC(After Closing Report) ARG()
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:TRANTYPE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'JOBS_ALIAS')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(REPORT)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> REPORT{PROPPRINT:Copies}
    REPORT{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = REPORT{PROPPRINT:Copies}
  IF REPORT{PROPPRINT:COLLATE}=True
    REPORT{PROPPRINT:Copies} = 1
  END
  ! Before Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  !SYSTEM{PROP:AutoPaper} = ''
  !!REPORT$?Label{PROP:MinHeight} = REPORT$?Label{PROP:Height}
  !Report$?Label{prop:xpos} = GETINI('REPORT','x',,Clip(Path()) & '\REPORTTEST.INI')
  !Report$?Label{prop:ypos} = GETINI('REPORT','y',,Clip(Path()) & '\REPORTTEST.INI')
  !Report$?Label{prop:height} = GETINI('REPORT','height',,Clip(Path()) & '\REPORTTEST.INI')
  !Report$?Label{prop:width} = GETINI('REPORT','width',,Clip(Path()) & '\REPORTTEST.INI')
  !Report$?Label{prop:Minheight} = Report$?Label{prop:Height}
  !Report$?Label{prop:MinWidth} = Report$?Label{prop:Width}
  ! After Embed Point: %AfterOpeningReport) DESC(After Opening Report) ARG()
  IF REPORT{PROP:TEXT}=''
    REPORT{PROP:TEXT}='GenericJobLabel'
  END
  IF PreviewReq = True OR (REPORT{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR REPORT{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    REPORT{Prop:Preview} = PrintPreviewImage
  END




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'GenericJobLabel',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('ReportRunDate',ReportRunDate,'GenericJobLabel',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'GenericJobLabel',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'GenericJobLabel',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'GenericJobLabel',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'GenericJobLabel',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'GenericJobLabel',1)
    SolaceViewVars('InitialPath',InitialPath,'GenericJobLabel',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'GenericJobLabel',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'GenericJobLabel',1)
    SolaceViewVars('model_number_temp',model_number_temp,'GenericJobLabel',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'GenericJobLabel',1)
    SolaceViewVars('esn_temp',esn_temp,'GenericJobLabel',1)
    SolaceViewVars('job_number_temp',job_number_temp,'GenericJobLabel',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'GenericJobLabel',1)
    SolaceViewVars('job_type_temp',job_type_temp,'GenericJobLabel',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'GenericJobLabel',1)
    SolaceViewVars('tmp:type',tmp:type,'GenericJobLabel',1)
    SolaceViewVars('tmp:UserName',tmp:UserName,'GenericJobLabel',1)
    SolaceViewVars('tmp:Company',tmp:Company,'GenericJobLabel',1)
    SolaceViewVars('tmp:BookedDate',tmp:BookedDate,'GenericJobLabel',1)
    SolaceViewVars('tmp:BookedTime',tmp:BookedTime,'GenericJobLabel',1)
    SolaceViewVars('tmp:InWorkshopDate',tmp:InWorkshopDate,'GenericJobLabel',1)
    SolaceViewVars('tmp:BookedLabel',tmp:BookedLabel,'GenericJobLabel',1)
    SolaceViewVars('tmp:HideIMEIBarcode',tmp:HideIMEIBarcode,'GenericJobLabel',1)
    SolaceViewVars('RejectRecord',RejectRecord,'GenericJobLabel',1)
    SolaceViewVars('LocalRequest',LocalRequest,'GenericJobLabel',1)
    SolaceViewVars('LocalResponse',LocalResponse,'GenericJobLabel',1)
    SolaceViewVars('FilesOpened',FilesOpened,'GenericJobLabel',1)
    SolaceViewVars('WindowOpened',WindowOpened,'GenericJobLabel',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'GenericJobLabel',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'GenericJobLabel',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'GenericJobLabel',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'GenericJobLabel',1)
    SolaceViewVars('PercentProgress',PercentProgress,'GenericJobLabel',1)
    SolaceViewVars('RecordStatus',RecordStatus,'GenericJobLabel',1)
    SolaceViewVars('EndOfReport',EndOfReport,'GenericJobLabel',1)
    SolaceViewVars('save_job2_id',save_job2_id,'GenericJobLabel',1)
    SolaceViewVars('save_jac_id',save_jac_id,'GenericJobLabel',1)
    SolaceViewVars('tmp:printer',tmp:printer,'GenericJobLabel',1)
    SolaceViewVars('code_temp',code_temp,'GenericJobLabel',1)
    SolaceViewVars('option_temp',option_temp,'GenericJobLabel',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'GenericJobLabel',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'GenericJobLabel',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'GenericJobLabel',1)
    SolaceViewVars('tmp:PrintPreview',tmp:PrintPreview,'GenericJobLabel',1)
    SolaceViewVars('BreaksStarted',BreaksStarted,'GenericJobLabel',1)


BuildCtrlQueue      Routine







