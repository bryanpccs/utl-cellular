

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02084.INC'),ONCE        !Local module procedure declarations
                     END


Virgin_Labels        PROCEDURE                        ! Declare Procedure
osver                ULONG
nt                   ULONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Virgin_Labels')      !Add Procedure to Log
  end


! For all platforms, the low order word contains the version number of the operating system.
! The low-order byte of this word specifies the major version number, in hexadecimal notation.
! The high-order byte specifies the minor version (revision) number, in hexadecimal notation.
! For Windows NT 3.5 and above the high order bit (of the high order Byte) is zero.

    osver = GetVersion()
    nt = BAND(osver, 080000000h)

    Access:Defaults.open()
    Access:Defaults.UseFile()

    SET(Defaults)
    IF (Access:Defaults.next() = Level:Benign) THEN
        CASE def:label_printer_type
            Of 'TEC B-440 / B-442'
                IF (def:UseSmallLabel = 1) THEN
                    IF (nt = 0) THEN
                        Virgin_Labels_Small_NT()
                    ELSE
                        Virgin_Labels_Small_9X()
                    END
                ELSE
                    IF (nt = 0) THEN
                        Virgin_Labels_NT()
                    ELSE
                        Virgin_Labels_9X()
                    END
                END
            Of 'TEC B-452'
                Virgin_Labels_B452()
        END
    END

    Access:Defaults.close()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Virgin_Labels',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('osver',osver,'Virgin_Labels',1)
    SolaceViewVars('nt',nt,'Virgin_Labels',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
