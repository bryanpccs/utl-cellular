

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02081.INC'),ONCE        !Local module procedure declarations
                     END


Complete_JobLabel    PROCEDURE  (f_jobref)            ! Declare Procedure
osver                ULONG
nt                   ULONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Complete_JobLabel')      !Add Procedure to Log
  end


    ! For all platforms, the low order word contains the version number of the operating system.
    ! The low-order byte of this word specifies the major version number, in hexadecimal notation.
    ! The high-order byte specifies the minor version (revision) number, in hexadecimal notation.
    ! For Windows NT 3.5 and above the high order bit (of the high order Byte) is zero.

    osver = GetVersion()
    nt = BAND(osver, 080000000h)

    IF (nt = 0) 
        Complete_JobLabel_NT(f_jobref)
    ELSE
        Complete_JobLabel_9x(f_jobref)
    END



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Complete_JobLabel',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('osver',osver,'Complete_JobLabel',1)
    SolaceViewVars('nt',nt,'Complete_JobLabel',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
