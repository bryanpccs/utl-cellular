

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02069.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Barcode_Labels PROCEDURE  (stock_ref)           ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Stock_Barcode_Labels')      !Add Procedure to Log
  end


    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    Access:Defaults.Open()
    Access:Defaults.UseFile()

    SET(Defaults,0)
    IF Access:Defaults.Next()
      ! Error!
    ELSE
      If tmp:High = 0
          Stock_Labels_Small_NT(stock_ref)
      Else!If tmp:High = 0
          Stock_Labels_Small_9X(stock_ref)
      End!If tmp:High = 0
    END

    Access:Defaults.Close()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Barcode_Labels',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:OS',tmp:OS,'Stock_Barcode_Labels',1)
    SolaceViewVars('tmp:High',tmp:High,'Stock_Barcode_Labels',1)
    SolaceViewVars('tmp:Low',tmp:Low,'Stock_Barcode_Labels',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
