

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02090.INC'),ONCE        !Local module procedure declarations
                     END


Thermal_Labels_9X_OLD PROCEDURE (f_type)              !Generated from procedure template - Report

RejectRecord         LONG,AUTO
save_job2_id         USHORT,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(40)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
tmp:accessories      STRING(255)
tmp:type             STRING(7)
tmp:UserName         STRING(30)
tmp:Company          STRING(40)
tmp:BookedDate       DATE
tmp:BookedTime       TIME
tmp:InWorkshopDate   BYTE(0)
tmp:BookedLabel      STRING(12)
tmp:HideIMEIBarcode  BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Location)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                       END
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(10,1,75,46),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),MM
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(1,,72,46),USE(?Label)
                           STRING(@s30),AT(1,0),USE(def:User_Name),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(56,0),USE(job_number_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@t1b),AT(20,6),USE(tmp:BookedTime),TRN,FONT(,6,,)
                           STRING(@s2),AT(33,6),USE(jobe:SkillLevel),TRN,FONT(,6,,)
                           STRING('Contact:'),AT(2,9),USE(?Contact),TRN,FONT(,6,,)
                           STRING(@s20),AT(14,9,30,3),USE(customer_name_temp),TRN,LEFT,FONT(,6,,)
                           STRING('Company:'),AT(2,12),USE(?String20),TRN,FONT(,6,,)
                           STRING('Skill Lvl'),AT(31,4),USE(?String8:2),TRN,FONT(,6,,)
                           STRING(@s40),AT(14,12),USE(tmp:Company),TRN,LEFT,FONT(,6,,)
                           STRING('Job Type:'),AT(2,14),USE(?String11),TRN,FONT(,6,,)
                           STRING(@s40),AT(14,14),USE(job_type_temp),TRN,FONT(,6,,)
                           STRING(@s7),AT(56,12),USE(tmp:type),TRN,HIDE,FONT(,10,,)
                           STRING('Location:'),AT(2,34),USE(?Location),TRN,FONT(,6,,)
                           STRING(@s20),AT(14,34),USE(job_ali:Location),TRN,LEFT,FONT(,6,,)
                           STRING(@s30),AT(14,30),USE(tmp:UserName),TRN,LEFT,FONT(,6,,)
                           STRING('Access.:'),AT(2,24),USE(?String21:2),TRN,HIDE,FONT(,6,,)
                           TEXT,AT(14,24,56,7),USE(tmp:accessories),HIDE,FONT(,6,,)
                           STRING('Retained'),AT(2,27),USE(?Retained),TRN,HIDE,FONT(,6,,)
                           STRING('Cust Unit:'),AT(2,36),USE(?String19),TRN,FONT(,6,,)
                           STRING('Fault:'),AT(2,17),USE(?String14),TRN,FONT(,6,,)
                           TEXT,AT(14,17,56,7),USE(jbn_ali:Fault_Description),FONT(,6,,)
                           STRING(@d6b),AT(17,4),USE(tmp:BookedDate),TRN,FONT(,6,,)
                           STRING(@s12),AT(2,5),USE(tmp:BookedLabel),TRN,FONT(,6,,)
                           STRING(@s8),AT(40,4),USE(Bar_Code_Temp),CENTER,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                           STRING(@s30),AT(14,36,17,4),USE(job_ali:Model_Number),TRN,LEFT,FONT(,6,,)
                           STRING('IMEI:'),AT(38,34),USE(?String18),TRN,FONT(,6,,)
                           STRING(@s16),AT(44,34,23,4),USE(job_ali:ESN),TRN,LEFT,FONT(,6,,)
                           STRING(@s16),AT(8,40,60,4),USE(Bar_Code2_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                           STRING(@s30),AT(32,36,17,4),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,6,,)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Thermal_Labels_9X_OLD',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('save_job2_id',save_job2_id,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('code_temp',code_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('option_temp',option_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('InitialPath',InitialPath,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('esn_temp',esn_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('job_type_temp',job_type_temp,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:type',tmp:type,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:UserName',tmp:UserName,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:Company',tmp:Company,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:BookedDate',tmp:BookedDate,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:BookedTime',tmp:BookedTime,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:InWorkshopDate',tmp:InWorkshopDate,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:BookedLabel',tmp:BookedLabel,'Thermal_Labels_9X_OLD',1)
    SolaceViewVars('tmp:HideIMEIBarcode',tmp:HideIMEIBarcode,'Thermal_Labels_9X_OLD',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Thermal_Labels_9X_OLD')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Thermal_Labels_9X_OLD')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBACC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:TRANTYPE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Thermal_Labels_9X_OLD',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  
  
  ReturnValue = PARENT.OpenReport()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      SetTarget(Report)
      ?job_ali:Location{prop:Hide} = 1
      ?Location{prop:Hide} = 1
      SetTarget()
  End !def:HideLocation
  
  ! Start Change 2648 BE(22/05/2003)
  tmp:InWorkshopDate = GETINI('PRINTING','InWorkshopDate',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2648 BE(22/05/2003)
  
  ! Start Change 3701 BE(15/12/2003)
  tmp:HideIMEIBarcode = GETINI('PRINTING','HideIMEIBarcode',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 3701 BE(15/12/2003)
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ProgressWindow{Prop:AcceptAll} THEN RETURN.
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)
  PARENT.Reset(Force)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Thermal_Labels_9X_OLD')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.Update PROCEDURE

  CODE
  PARENT.Update
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  
  Settarget(Report)
  If def:PrintBarcode = 1
      ! Start Change 3701 BE(15/12/03)
      !?Bar_Code2_Temp{prop:hide} = 0
      IF (tmp:HideIMEIBarcode) THEN
          ?Bar_Code2_Temp{prop:hide} = 1
      ELSE
          ?Bar_Code2_Temp{prop:hide} = 0
      END
      ! End Change 3701 BE(15/12/03)
  Else
      ?job_ali:ESN{prop:Hide} = 1
      ?String18{prop:Hide} = 1
      ?Bar_Code2_Temp{prop:Hide} = 1
  End!If def:PrintBarcode = 1
  Settarget()
  
  IF def:Job_Label_Accessories = 'YES'
    Settarget(Report)
    ?String21:2{Prop:Hide} = FALSE
    ?retained{Prop:Hide} = FALSE
    ?tmp:accessories{Prop:Hide} = FALSE
    Settarget()
  END
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job_ali:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  ! Start Change 2648 BE(22/05/2003)
  IF (tmp:InWorkshopDate) THEN
      tmp:BookedDate = jobe:InWorkshopDate
      tmp:BookedTime = jobe:InWorkshopTime
      IF (tmp:BookedDate = '') THEN
          tmp:BookedLabel = ''
      ELSE
          tmp:BookedLabel = 'In Workshop:'
      END
  ELSE
      tmp:BookedDate = job_ali:date_booked
      tmp:BookedTime = job_ali:time_booked
      tmp:BookedLabel = 'Booked:'
  END
  ! End Change 2648 BE(22/05/2003)
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'J' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'SPLIT-' & Clip(Format(job_ali:Charge_Type,@s20)) & '/' & Clip(Format(job_ali:Warranty_Charge_Type,@s20))
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE-'&CLIP(job_ali:Charge_Type)
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY-'&CLIP(job_ali:Warranty_Charge_Type)
  End
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname)
  Else!If job_ali:surname <> ''
      customer_name_temp = ''
      SetTarget(Report)
      ?Contact{Prop:Hide} = 1
      SetTarget()
  End!
  tmp:Company = Clip(job_ali:Account_Number) & ' - ' & Clip(job_ali:Company_Name)
  
  acc_count# = 0
  save_jac_id = access:jobacc.savefile()
  access:jobacc.clearkey(jac:ref_number_key)
  jac:ref_number = job_ali:ref_number
  set(jac:ref_number_key,jac:ref_number_key)
  loop
      if access:jobacc.next()
         break
      end !if
      if jac:ref_number <> job_ali:ref_number  |
          then break.  ! end if
      acc_count# += 1
      If acc_count# = 1
          tmp:accessories = Clip(jac:accessory)
      Else!If acc_count# = 1
          tmp:accessories = Clip(tmp:accessories) & ', ' & Clip(jac:accessory)
      End!If acc_count# = 1
  end !loop
  access:jobacc.restorefile(save_jac_id)
  
  refurb# = 0
  
  access:jobnotes_alias.clearkey(JBN_ALI:RefNumberKey)
  JBN_ALI:RefNumber   = job_ali:ref_number
  access:jobnotes_alias.tryfetch(JBN_ALI:RefNumberKey)
  
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job_ali:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key)
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          If tra:refurbcharge = 'YES'
              If job_ali:exchange_unit_number <> ''
                  refurb# = 1
              End!If job_ali:exchange_unit_number <> ''
              If refurb# = 0
                  access:trantype.clearkey(trt:transit_type_key)
                  trt:transit_type = job_ali:transit_type
                  if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                      If trt:exchange_unit = 'YES'
                          refurb# = 1
                      End!If trt:exchange = 'YES'
                  end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              End!If refurb# = 0
              If refurb# = 0
                  If Sub(job_ali:exchange_status,1,3) = '108'
                      refurb# = 1
                  End!If Sub(job_ali:current_status,1,3) = '108'
              End!If refurb# = 0
          End!If tra:refurbcharge = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  
  
  If refurb# = 1
      tmp:username    = 'REFURBISHMENT REQUIRED'
  Else!If refurb# = 1
      tmp:username    = ''
      ! Start Change 2760 BE(20/06/03)
      !If f_type <> ''
      IF (f_type = 'ER') THEN
      ! End Change 2760 BE(20/06/03)
          Settarget(report)
          Unhide(?tmp:type)
          tmp:type = f_type
          Settarget()
      Else
          ! Start Change 2821 BE(20/06/03)
          Bounce_String" = ''
          ! End Change 2821 BE(20/06/03)
          If job_ali:esn <> 'N/A'
              ! Start Change 2760 BE(20/06/03)
              !If CountBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:esn)
              !    Settarget(report)
              !    Unhide(?tmp:type)
              !    tmp:type = 'SE'
              !    Settarget()
              !End!If found# = 1
              ! Start Change 2821 BE(20/06/03
              !bc# = CountBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:esn)
              !IF (bc# > 0) THEN
              !    Settarget(report)
              !    Unhide(?tmp:type)
              !    IF (bc# = 1) THEN
              !        tmp:type = '2E'
              !    ELSIF (bc# = 2) THEN
              !        tmp:type = '3E'
              !    ELSE
              !        tmp:type = '3E+'
              !    END
              !    Settarget()
              !END
              bc# = CountBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:esn)
              IF (bc# > 0) THEN
                  IF (bc# = 1) THEN
                      Bounce_String" = '2E'
                  ELSIF (bc# = 2) THEN
                      Bounce_String" = '3E'
                  ELSE
                      Bounce_String" = '3E+'
                  END
              END
              ! Start Change 2821 BE(20/06/03
              ! End Change 2760 BE(20/06/03)
          End!If job2:esn <> 'N/A'
          ! Start Change 2821 BE(24/06/03)
          IF (job:mobile_number <> '') THEN
              bc# = CountMobileBouncer(job_ali:ref_number,job_ali:date_booked,job_ali:mobile_number)
              IF (bc# > 0) THEN
                  IF (bc# = 1) THEN
                      IF (Bounce_String" = '') THEN
                          Bounce_String" = '2M'
                      ELSE
                          Bounce_String" = CLIP(Bounce_String") & ' 2M'
                      END
                  ELSIF (bc# = 2) THEN
                      IF (Bounce_String" = '') THEN
                          Bounce_String" = '3M'
                      ELSE
                          Bounce_String" = CLIP(Bounce_String") & ' 3M'
                      END
                  ELSE
                      IF (Bounce_String" = '') THEN
                          Bounce_String" = '3M+'
                      ELSE
                          Bounce_String" = CLIP(Bounce_String") & ' 3M+'
                      END
                  END
              END
          END
          IF (Bounce_String" <> '') THEN
              Bounce_String_Length# = LEN(CLIP(Bounce_String"))
              IF (Bounce_String_Length# > 7) THEN
                  Bounce_String_Length# = 7
              END
              Bounce_String_Right" = '       '
              Bounce_String_Right"[(8-Bounce_String_Length#) : 7] = Bounce_String"[1 : Bounce_String_Length#]
              Settarget(report)
              Unhide(?tmp:type)
              tmp:type = Bounce_String_Right"[1 : 7]
              Settarget()
          END
          ! End Change 2821 BE(24/06/03)
      End! If f_type <> ''
  End!If refurb# = 1
  
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

