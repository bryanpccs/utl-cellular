

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02045.INC'),ONCE        !Local module procedure declarations
                     END


Loan_Unit_Label      PROCEDURE                        ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Loan_Unit_Label')      !Add Procedure to Log
  end


    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    If tmp:High = 0
        Loan_Unit_Label_NT
    Else!If tmp:High = 0
        Loan_Unit_Label_9X
    End!If tmp:High = 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Loan_Unit_Label',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:OS',tmp:OS,'Loan_Unit_Label',1)
    SolaceViewVars('tmp:High',tmp:High,'Loan_Unit_Label',1)
    SolaceViewVars('tmp:Low',tmp:Low,'Loan_Unit_Label',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
