

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02002.INC'),ONCE        !Local module procedure declarations
                     END


Thermal_Labels_Exchange_B452 PROCEDURE                !Generated from procedure template - Report

RejectRecord         LONG,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
Bar_Code3_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Company_Name)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(20,32,75,50),PAPER(PAPER:USER,105,80),PRE(RPT),FONT('Arial',10,,),MM
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,75,50),USE(?Label)
                           STRING(@s30),AT(9,0),USE(def:User_Name),FONT(,,,FONT:bold)
                           STRING('Job No:'),AT(35,6),USE(?String7),TRN,FONT(,8,,)
                           STRING(@s10),AT(46,10),USE(job_number_temp),TRN,LEFT,FONT(,8,,FONT:bold)
                           STRING('Customer:'),AT(2,15),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s60),AT(15,15,58,4),USE(customer_name_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Company:'),AT(2,19),USE(?String25),TRN,FONT(,7,,)
                           STRING(@s30),AT(15,19),USE(job_ali:Company_Name),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Job Type:'),AT(2,23),USE(?String11),TRN,FONT(,7,,)
                           STRING(@s40),AT(15,23),USE(job_type_temp),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(33,31,42,6),USE(Bar_Code3_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Customer'),AT(2,38),USE(?String23),TRN,FONT(,7,,)
                           STRING(@d6b),AT(15,6),USE(job_ali:date_booked),FONT(,7,,FONT:bold)
                           STRING('Booked:'),AT(2,6),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s10),AT(46,6,29,4),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Exchange '),AT(2,27),USE(?String15),TRN,FONT(,7,,)
                           STRING(@s30),AT(15,27,17,4),USE(xch:Model_Number),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(15,31,17,4),USE(xch:Manufacturer),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(15,38,17,4),USE(job_ali:Model_Number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('E.S.N.:'),AT(33,38),USE(?String18),TRN,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(44,38),USE(job_ali:ESN),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('Unit:'),AT(2,30),USE(?String19:2),TRN,FONT(,7,,)
                           STRING('Unit:'),AT(2,41),USE(?String19),TRN,FONT(,7,,)
                           STRING(@s16),AT(33,42,42,6),USE(Bar_Code2_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('E.S.N.:'),AT(33,27),USE(?String18:2),TRN,FONT(,7,,FONT:bold)
                           STRING(@s30),AT(44,27),USE(xch:ESN,,?XCH:ESN:2),FONT(,7,,FONT:bold)
                           STRING(@s30),AT(15,42,17,4),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Thermal_Labels_Exchange_B452',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('code_temp',code_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('option_temp',option_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('Bar_Code3_Temp',Bar_Code3_Temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('InitialPath',InitialPath,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('esn_temp',esn_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Thermal_Labels_Exchange_B452',1)
    SolaceViewVars('job_type_temp',job_type_temp,'Thermal_Labels_Exchange_B452',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Thermal_Labels_Exchange_B452')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Thermal_Labels_Exchange_B452')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:EXCHANGE.Open
  Relate:JOBS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:EXCHANGE.Close
    Relate:JOBS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Thermal_Labels_Exchange_B452',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Unit Exchange Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Thermal_Labels_Exchange_B452')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'E' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'E.S.N. / I.M.E.I.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  access:exchange.clearkey(xch:ref_number_key)
  xch:ref_number = glo:select2
  access:exchange.fetch(xch:ref_number_key)
  bar_code_string_temp = Clip(xch:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code3_Temp)
  
  
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'CHARGEABLE / WARRANTY'
  End
  If job_ali:chargeable_job = 'YES' And job_ali:warranty_job <> 'YES'
      job_type_temp = 'CHARGEABLE-'&CLIP(job_ali:Charge_Type)
  End
  If job_ali:chargeable_job <> 'YES' And job_ali:warranty_job = 'YES'
      job_Type_temp = 'WARRANTY-'&CLIP(job_ali:Warranty_Charge_Type)
  End
  
  If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number) & ' (' & Clip(job_ali:title) & ' ' & CLip(job_ali:initial) & |
                          ' ' & Clip(job_ali:surname) & ')'
  Else!If job_ali:surname <> ''
      customer_name_temp = Clip(job_ali:account_number)
  End!
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

