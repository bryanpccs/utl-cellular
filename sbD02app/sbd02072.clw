

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02072.INC'),ONCE        !Local module procedure declarations
                     END


ExchangeRepairLabel  PROCEDURE  (func:JobNumber)      ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ExchangeRepairLabel')      !Add Procedure to Log
  end


    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS

    If tmp:High = 0
        ExchangeRepairLabelNT(func:JobNumber)
    Else!If tmp:High = 0
        ExchangeRepairLabel9X(func:JobNumber)
    End!If tmp:High = 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ExchangeRepairLabel',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:OS',tmp:OS,'ExchangeRepairLabel',1)
    SolaceViewVars('tmp:High',tmp:High,'ExchangeRepairLabel',1)
    SolaceViewVars('tmp:Low',tmp:Low,'ExchangeRepairLabel',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
