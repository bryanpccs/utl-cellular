

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02068.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Labels_Small_NT PROCEDURE (stock_ref)           !Generated from procedure template - Report

RejectRecord         LONG,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
save_job2_id         USHORT,AUTO
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(21)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
tmp:accessories      STRING(255)
tmp:type             STRING(4)
tmp:UserName         STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Company          STRING(40)
Bar_code3_temp       CSTRING(21)
Stock_ref_temp       LONG
Process:View         VIEW(STOCK)
                       PROJECT(sto:Description)
                       PROJECT(sto:Location)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Shelf_Location)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(0,0,5000,2000),PAPER(PAPER:USER,5000,1000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(10,,5000,1000),USE(?Label)
                           STRING(@s21),AT(1260,292,,126),USE(Bar_Code2_Temp),TRN,LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@s30),AT(1260,448),USE(sto:Description),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(646,521,573,208),USE(sto:Location),TRN,FONT('Arial',7,,)
                           STRING('Description'),AT(646,292),USE(?String9),TRN,FONT(,7,,)
                           STRING(@s21),AT(1260,573,,126),USE(Bar_code3_temp),TRN,LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@s30),AT(1260,729),USE(sto:Shelf_Location),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING('Location'),AT(646,656),USE(?String20),TRN,FONT(,7,,)
                           STRING('Part Number'),AT(646,10),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s21),AT(1260,21,,126),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@s30),AT(1260,177),USE(sto:Part_Number),TRN,FONT('Arial',8,,,CHARSET:ANSI)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Labels_Small_NT',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Stock_Labels_Small_NT',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Stock_Labels_Small_NT',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Stock_Labels_Small_NT',1)
    SolaceViewVars('save_job2_id',save_job2_id,'Stock_Labels_Small_NT',1)
    SolaceViewVars('code_temp',code_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('option_temp',option_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Stock_Labels_Small_NT',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Stock_Labels_Small_NT',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Labels_Small_NT',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Stock_Labels_Small_NT',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Stock_Labels_Small_NT',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Stock_Labels_Small_NT',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Stock_Labels_Small_NT',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Stock_Labels_Small_NT',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Stock_Labels_Small_NT',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Stock_Labels_Small_NT',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Stock_Labels_Small_NT',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Stock_Labels_Small_NT',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Stock_Labels_Small_NT',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Stock_Labels_Small_NT',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Stock_Labels_Small_NT',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Stock_Labels_Small_NT',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Stock_Labels_Small_NT',1)
    SolaceViewVars('InitialPath',InitialPath,'Stock_Labels_Small_NT',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Stock_Labels_Small_NT',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Stock_Labels_Small_NT',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('esn_temp',esn_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('job_type_temp',job_type_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Stock_Labels_Small_NT',1)
    SolaceViewVars('tmp:type',tmp:type,'Stock_Labels_Small_NT',1)
    SolaceViewVars('tmp:UserName',tmp:UserName,'Stock_Labels_Small_NT',1)
    SolaceViewVars('tmp:Company',tmp:Company,'Stock_Labels_Small_NT',1)
    SolaceViewVars('Bar_code3_temp',Bar_code3_temp,'Stock_Labels_Small_NT',1)
    SolaceViewVars('Stock_ref_temp',Stock_ref_temp,'Stock_Labels_Small_NT',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Stock_Labels_Small_NT')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Labels_Small_NT')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  Stock_Ref_Temp = Stock_Ref
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:DEFSTOCK.Open
  Relate:STOCK.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:STOCK, ?Progress:PctText, Progress:Thermometer, ProgressMgr, sto:Ref_Number)
  ThisReport.AddSortOrder(sto:Ref_Number_Key)
  ThisReport.AddRange(sto:Ref_Number,Stock_ref_temp)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:STOCK.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:DEFSTOCK.Close
    Relate:STOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Labels_Small_NT',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  
  
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Barcode Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Labels_Small_NT')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  
  !Get Stock!
  Access:Stock.ClearKey(sto:Ref_Number_Key)
  sto:Ref_Number = Stock_ref
  IF Access:Stock.Fetch(sto:Ref_Number_Key)
    !Would be unlikely, as the only to this routine is FROM the stock item!
  END
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(sto:Part_Number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(SUB(sto:Description,1,15))
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  bar_code_string_temp = Clip(sto:Shelf_Location)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code3_Temp)
  
  
  !bar_code_string_temp = Clip(job_ali:esn)
  !esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
  !SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

