

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02071.INC'),ONCE        !Local module procedure declarations
                     END


ExchangeRepairLabelNT PROCEDURE (func:JobNumber)      !Generated from procedure template - Report

RejectRecord         LONG,AUTO
save_job2_id         USHORT,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(30)
tmp:accessories      STRING(255)
tmp:type             STRING(4)
tmp:UserName         STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:Company          STRING(40)
tmp:JobNumber        LONG
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:date_booked)
                       PROJECT(job_ali:time_booked)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                         PROJECT(jbn_ali:Fault_Description)
                       END
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(0,0,5000,2000),PAPER(PAPER:USER,5000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,5000,2000),USE(?Label)
                           STRING(@s30),AT(760,10),USE(def:User_Name),TRN,FONT(,8,,FONT:bold)
                           STRING('EXCHANGE REPAIR'),AT(760,156),USE(?String16),TRN,FONT(,8,,FONT:bold)
                           STRING(@s8),AT(2948,354),USE(job_number_temp),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@t1),AT(1323,594),USE(job_ali:time_booked),FONT(,7,,FONT:bold)
                           STRING(@s2),AT(2104,625),USE(jobe:SkillLevel),TRN,FONT(,7,,)
                           STRING('Skill Level'),AT(1938,500),USE(?String8:2),TRN,FONT(,7,,)
                           STRING('Cust Unit:'),AT(760,1333),USE(?String19),TRN,FONT(,7,,)
                           STRING('Fault:'),AT(760,896),USE(?String14),TRN,FONT(,7,,)
                           TEXT,AT(1333,896,2240,260),USE(jbn_ali:Fault_Description),FONT(,7,,FONT:bold)
                           STRING(@d6b),AT(1333,500),USE(job_ali:date_booked),TRN,FONT(,7,,FONT:bold)
                           STRING('Booked:'),AT(760,552),USE(?String8),TRN,FONT(,7,,)
                           STRING(@s8),AT(2458,510,1302,260),USE(Bar_Code_Temp),LEFT,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                           STRING(@s30),AT(1333,1344,677,156),USE(job_ali:Model_Number),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING('IMEI:'),AT(1323,1458),USE(?String18),TRN,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(1552,1458,938,156),USE(job_ali:ESN),TRN,LEFT,FONT(,7,,FONT:bold)
                           STRING(@s16),AT(1333,1594,2396,260),USE(Bar_Code2_Temp),HIDE,LEFT,FONT('C128 High 12pt LJ3',16,,,CHARSET:ANSI)
                           STRING(@s30),AT(2063,1344,677,156),USE(job_ali:Manufacturer),TRN,LEFT,FONT(,7,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
Update                 PROCEDURE(),DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ExchangeRepairLabelNT',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'ExchangeRepairLabelNT',1)
    SolaceViewVars('save_job2_id',save_job2_id,'ExchangeRepairLabelNT',1)
    SolaceViewVars('save_jac_id',save_jac_id,'ExchangeRepairLabelNT',1)
    SolaceViewVars('tmp:printer',tmp:printer,'ExchangeRepairLabelNT',1)
    SolaceViewVars('code_temp',code_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('option_temp',option_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('LocalRequest',LocalRequest,'ExchangeRepairLabelNT',1)
    SolaceViewVars('LocalResponse',LocalResponse,'ExchangeRepairLabelNT',1)
    SolaceViewVars('FilesOpened',FilesOpened,'ExchangeRepairLabelNT',1)
    SolaceViewVars('WindowOpened',WindowOpened,'ExchangeRepairLabelNT',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'ExchangeRepairLabelNT',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'ExchangeRepairLabelNT',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'ExchangeRepairLabelNT',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'ExchangeRepairLabelNT',1)
    SolaceViewVars('PercentProgress',PercentProgress,'ExchangeRepairLabelNT',1)
    SolaceViewVars('RecordStatus',RecordStatus,'ExchangeRepairLabelNT',1)
    SolaceViewVars('EndOfReport',EndOfReport,'ExchangeRepairLabelNT',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'ExchangeRepairLabelNT',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'ExchangeRepairLabelNT',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'ExchangeRepairLabelNT',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'ExchangeRepairLabelNT',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'ExchangeRepairLabelNT',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'ExchangeRepairLabelNT',1)
    SolaceViewVars('InitialPath',InitialPath,'ExchangeRepairLabelNT',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'ExchangeRepairLabelNT',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'ExchangeRepairLabelNT',1)
    SolaceViewVars('model_number_temp',model_number_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('esn_temp',esn_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('job_number_temp',job_number_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('job_type_temp',job_type_temp,'ExchangeRepairLabelNT',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'ExchangeRepairLabelNT',1)
    SolaceViewVars('tmp:type',tmp:type,'ExchangeRepairLabelNT',1)
    SolaceViewVars('tmp:UserName',tmp:UserName,'ExchangeRepairLabelNT',1)
    SolaceViewVars('tmp:Company',tmp:Company,'ExchangeRepairLabelNT',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'ExchangeRepairLabelNT',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ExchangeRepairLabelNT')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ExchangeRepairLabelNT')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  tmp:JobNumber   = func:JobNumber
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBACC.Open
  Relate:JOBS2_ALIAS.Open
  Relate:TRANTYPE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,tmp:JobNumber)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBACC.Close
    Relate:JOBS2_ALIAS.Close
    Relate:TRANTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ExchangeRepairLabelNT',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  
  
  ReturnValue = PARENT.OpenReport()
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF ProgressWindow{Prop:AcceptAll} THEN RETURN.
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)
  PARENT.Reset(Force)


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ExchangeRepairLabelNT')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.Update PROCEDURE

  CODE
  PARENT.Update
  jbn_ali:RefNumber = job_ali:Ref_Number              ! Assign linking field value
  Access:JOBNOTES_ALIAS.Fetch(jbn_ali:RefNumberKey)


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  set(defaults)
  access:defaults.next()
  
  If def:PrintBarcode = 1
      Settarget(Report)
      ?Bar_Code2_Temp{prop:hide} = 0
      Settarget()
  End!If def:PrintBarcode = 1
  
  
  Access:JOBSE.ClearKey(jobe:RefNumberKey)
  jobe:RefNumber = job_ali:Ref_Number
  If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  job_number_temp = 'J' & Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  bar_code_string_temp = Clip(job_ali:esn)
  esn_temp = 'I.M.E.I. No.: ' & Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  
  refurb# = 0
  
  access:jobnotes_alias.clearkey(JBN_ALI:RefNumberKey)
  JBN_ALI:RefNumber   = job_ali:ref_number
  access:jobnotes_alias.tryfetch(JBN_ALI:RefNumberKey)
  
  
  
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

