

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBD02053.INC'),ONCE        !Local module procedure declarations
                     END


Address_Label_Multiple PROCEDURE  (func:ConsignmentNo) ! Declare Procedure
tmp:OS               STRING(20)
tmp:High             BYTE
tmp:Low              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Address_Label_Multiple')      !Add Procedure to Log
  end


    tmp:OS  = GetVersion()
    tmp:High    = BShift(tmp:OS,-8)
    tmp:Low     = tmp:OS
!    Address_Label_Multiple_NT(func:ConsignmentNo)
    If tmp:High = 0
        Address_Label_Multiple_NT(func:ConsignmentNo)
    Else!If tmp:High = 0
        Address_Label_Multiple_9X(func:ConsignmentNo)
    End!If tmp:High = 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Address_Label_Multiple',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:OS',tmp:OS,'Address_Label_Multiple',1)
    SolaceViewVars('tmp:High',tmp:High,'Address_Label_Multiple',1)
    SolaceViewVars('tmp:Low',tmp:Low,'Address_Label_Multiple',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
