

   MEMBER('sbd02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBD02088.INC'),ONCE        !Local module procedure declarations
                     END


Virgin_Labels_Small_9X PROCEDURE                      !Generated from procedure template - Report

RejectRecord         LONG,AUTO
save_job2_id         USHORT,AUTO
save_jac_id          USHORT,AUTO
tmp:printer          STRING(255)
code_temp            BYTE
option_temp          BYTE
Bar_code_string_temp CSTRING(21)
Bar_Code_Temp        CSTRING(21)
Bar_Code2_Temp       CSTRING(31)
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          BYTE
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
model_number_temp    STRING(30)
unit_details_temp    STRING(255)
esn_temp             STRING(40)
job_number_temp      STRING(20)
customer_name_temp   STRING(60)
job_type_temp        STRING(40)
tmp:accessories      STRING(255)
tmp:type             STRING(7)
tmp:UserName         STRING(30)
tmp:Company          STRING(40)
tmp:BookedDate       DATE
tmp:BookedTime       TIME
tmp:InWorkshopDate   BYTE(0)
tmp:BookedLabel      STRING(12)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Process:View         VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:Location)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Ref_Number)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,59),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(0,3,141,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(0,30,141,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(45,42,50,15),USE(?Progress:Cancel)
                     END

REPORT               REPORT,AT(100,0,3000,2000),PAPER(PAPER:USER,3000,2000),PRE(RPT),FONT('Arial',10,,),THOUS
EndOfReportBreak       BREAK(EndOfReport)
DETAIL                   DETAIL,AT(,,3000,1000),USE(?Label)
                           STRING(@s10),AT(313,52,2552,260),USE(Bar_Code_Temp),TRN,RIGHT,FONT('C128 High 12pt LJ3',12,,)
                           STRING('Model:'),AT(104,260),USE(?String2),TRN,FONT(,8,,)
                           STRING(@s30),AT(573,260,2240,208),USE(job_ali:Model_Number),TRN,FONT(,8,,FONT:bold)
                           STRING('IMEI:'),AT(104,406),USE(?String3),TRN,FONT(,8,,)
                           STRING(@s20),AT(573,406,2240,208),USE(job_ali:ESN),TRN,FONT(,8,,FONT:bold)
                           STRING(@s30),AT(573,573,2292,260),USE(Bar_Code2_Temp),TRN,FONT('C128 High 12pt LJ3',12,,)
                           STRING(@s30),AT(573,781,2292,208),USE(job_ali:Location),TRN,LEFT,FONT(,12,,FONT:bold)
                         END
                       END
                     END
ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
OpenReport             PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisReport           CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepLongClass                    !Progress Manager
Previewer            PrintPreviewClass                !Print Previewer
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Virgin_Labels_Small_9X',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('RejectRecord',RejectRecord,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('save_job2_id',save_job2_id,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:printer',tmp:printer,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('code_temp',code_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('option_temp',option_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('Bar_code_string_temp',Bar_code_string_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('Bar_Code_Temp',Bar_Code_Temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('Bar_Code2_Temp',Bar_Code2_Temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('RecordsToProcess',RecordsToProcess,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('RecordsProcessed',RecordsProcessed,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('RecordsPerCycle',RecordsPerCycle,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('RecordsThisCycle',RecordsThisCycle,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('PercentProgress',PercentProgress,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('RecordStatus',RecordStatus,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('EndOfReport',EndOfReport,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('ReportRunDate',ReportRunDate,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('ReportRunTime',ReportRunTime,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('ReportPageNo',ReportPageNo,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('FileOpensReached',FileOpensReached,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('PartialPreviewReq',PartialPreviewReq,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('DisplayProgress',DisplayProgress,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('InitialPath',InitialPath,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('IniFileToUse',IniFileToUse,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('unit_details_temp',unit_details_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('esn_temp',esn_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('customer_name_temp',customer_name_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('job_type_temp',job_type_temp,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:accessories',tmp:accessories,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:type',tmp:type,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:UserName',tmp:UserName,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:Company',tmp:Company,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:BookedDate',tmp:BookedDate,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:BookedTime',tmp:BookedTime,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:InWorkshopDate',tmp:InWorkshopDate,'Virgin_Labels_Small_9X',1)
    SolaceViewVars('tmp:BookedLabel',tmp:BookedLabel,'Virgin_Labels_Small_9X',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Virgin_Labels_Small_9X')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Virgin_Labels_Small_9X')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:DEFPRINT.Open
  Relate:JOBS_ALIAS.Open
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisReport.Init(Process:View, Relate:JOBS_ALIAS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job_ali:Ref_Number)
  ThisReport.AddSortOrder(job_ali:Ref_Number_Key)
  ThisReport.AddRange(job_ali:Ref_Number,GLO:Select1)
  SELF.AddItem(?Progress:Cancel,RequestCancelled)
  SELF.Init(ThisReport,REPORT,Previewer)
  ?Progress:UserString{Prop:Text}=''
  Relate:JOBS_ALIAS.SetQuickScan(1,Propagate:OneMany)
  Previewer.AllowUserZoom=True
  Previewer.Maximize=True
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:printer
  end!if tmp:printer <> ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:DEFPRINT.Close
    Relate:JOBS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Virgin_Labels_Small_9X',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.OpenReport PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  !choose printer
  access:defprint.clearkey(dep:printer_name_key)
  dep:printer_name = 'LABEL'
  if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      printer{propprint:device} = dep:printer_path
      ! Start Change 2558 BE(28/04/03)
      printer{propprint:copies} = dep:copies
      ! End Change 2558 BE(28/04/03)
      self.skippreview = true
  else!if access:defprint.fetch(dep:printer_name_key) = level:benign
      tmp:printer = printer{propprint:device}
      choose_printer(new_printer",preview",copies")
      printer{propprint:device} = new_printer"
      printer{propprint:copies} = copies"
      if preview" <> 'YES'
          self.skippreview = true
      end!if preview" <> 'YES'
  end!if access:defprint.fetch(dep:printer_name_key) = level:benign
  
  ReturnValue = PARENT.OpenReport()
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  REPORT{Prop:Text} = 'Job Label'
  !--------------------------------------------------------------------------
  ! Tinman Report Job Name
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(OpenReport, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Virgin_Labels_Small_9X')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisReport.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

SkipDetails BYTE
  CODE
  ! Before Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:ref_number)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code_Temp)
  
  code_temp = 3
  option_temp = 0
  bar_code_string_temp = Clip(job_ali:esn)
  SEQUENCE2(code_temp,option_temp,Bar_code_string_temp,Bar_Code2_Temp)
  
  
  ReturnValue = PARENT.TakeRecord()
  PRINT(RPT:DETAIL)
  ! After Embed Point: %ProcessManagerMethodCodeSection) DESC(Process Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue

