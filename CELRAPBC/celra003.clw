

   MEMBER('celrapbc.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA003.INC'),ONCE        !Local module procedure declarations
                     END




Browse_Stock PROCEDURE                                !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::25:TAGFLAG         BYTE(0)
DASBRW::25:TAGMOUSE        BYTE(0)
DASBRW::25:TAGDISPSTATUS   BYTE(0)
DASBRW::25:QUEUE          QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
average_temp         LONG
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
average_text_temp    STRING(30)
save_shi_id          USHORT,AUTO
tmp:LastOrdered      DATE
save_orp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
tmp:partnumber       STRING(30)
tmp:description      STRING(30)
tmp:location         STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
Location_Temp        STRING(30)
Manufacturer_Temp    STRING(30)
CPCSExecutePopUp     BYTE
accessory_temp       STRING('ALL')
shelf_location_temp  STRING(60)
no_temp              STRING('NO')
yes_temp             STRING('YES')
BrFilter1            STRING(300)
BrLocator1           STRING(30)
BrFilter2            STRING(300)
BrLocator2           STRING(30)
tmp:QtyOnOrder       LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Tag                  STRING(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Location_Temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Manufacturer)
                       PROJECT(sto:Supplier)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Purchase_Cost)
                       PROJECT(sto:Sale_Cost)
                       PROJECT(sto:Retail_Cost)
                       PROJECT(sto:Location)
                       PROJECT(sto:Accessory)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
Tag                    LIKE(Tag)                      !List box control field - type derived from local data
Tag_Icon               LONG                           !Entry's icon ID
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Manufacturer       LIKE(sto:Manufacturer)         !List box control field - type derived from field
shelf_location_temp    LIKE(shelf_location_temp)      !List box control field - type derived from local data
sto:Supplier           LIKE(sto:Supplier)             !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Retail_Cost        LIKE(sto:Retail_Cost)          !List box control field - type derived from field
no_temp                LIKE(no_temp)                  !Browse hot field - type derived from local data
yes_temp               LIKE(yes_temp)                 !Browse hot field - type derived from local data
days_7_temp            LIKE(days_7_temp)              !Browse hot field - type derived from local data
days_30_temp           LIKE(days_30_temp)             !Browse hot field - type derived from local data
days_60_temp           LIKE(days_60_temp)             !Browse hot field - type derived from local data
days_90_temp           LIKE(days_90_temp)             !Browse hot field - type derived from local data
average_text_temp      LIKE(average_text_temp)        !Browse hot field - type derived from local data
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
sto:Accessory          LIKE(sto:Accessory)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK3::sto:Location         LIKE(sto:Location)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
mo:SelectedTab  Long  ! makeover template ! LocalTreat = Default ; PT = Window  WT = Browse
QuickWindow          WINDOW('Browse the Stock File'),AT(,,512,336),FONT('Arial',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Stock'),SYSTEM,GRAY,RESIZE,MDI
                       PROMPT('Site Location'),AT(8,12),USE(?Prompt1),FONT(,,COLOR:White,)
                       COMBO(@s30),AT(84,12,124,10),USE(Location_Temp),VSCROLL,FONT('Arial',8,,FONT:bold),UPR,FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       PROMPT('Press ''Home'' To Clear Locator'),AT(136,48),USE(?Prompt3),FONT(,,COLOR:Navy,FONT:bold)
                       BUTTON('Tag &All'),AT(64,312,56,16),USE(?DASTAGAll),LEFT,ICON('tagall.ico')
                       LIST,AT(8,64,412,244),USE(?Browse:1),IMM,MSG('Browsing Records'),ALRT(HomeKey),FORMAT('11L(2)I@s1@86L(2)|M~Part Number~@s30@86L(2)|M~Description~@s30@86L(2)|M~Manufact' &|
   'urer~@s30@132L(2)|M~Shelf Location~@s60@0L(2)|M~Supplier~@s30@/0R(2)|M~Qty In St' &|
   'ock~L@N8@0L(2)|M~Reference Number~@n012@/0L(2)|M~Purchase Cost~@n14.2@/0L(2)|M~S' &|
   'ale Cost~@n14.2@/0L(2)|M~Retail Cost~@n14.2@/'),FROM(Queue:Browse:1)
                       BUTTON('&Untag All'),AT(120,312,56,16),USE(?DASUNTAGALL),LEFT,ICON('untagall.ico')
                       BUTTON('Print Labels'),AT(432,44,76,20),USE(?Button7),LEFT,ICON('scanner.gif')
                       BUTTON('&Rev tags'),AT(452,292,16,15),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(432,292,16,15),USE(?DASSHOWTAG),HIDE
                       SHEET,AT(4,32,420,300),USE(?CurrentTab),SPREAD
                         TAB('By &Part Number'),USE(?Tab:3)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Part_Number),FONT('Arial',8,,FONT:bold),UPR
                         END
                         TAB('By Desc&ription'),USE(?Tab:4)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Description),FONT('Arial',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&Tag'),AT(8,312,56,16),USE(?DASTAG),LEFT,ICON('tag.ico')
                       BUTTON('Close'),AT(432,312,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                       PANEL,AT(4,4,504,24),USE(?Panel1),FILL(COLOR:Gray)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
UpdateWindow           PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort5:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
Resize                 PROCEDURE(SIGNED Control),BYTE,PROC,DERIVED
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

save_stm_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::25:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:Queue.Pointer = sto:Ref_Number
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = sto:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
    Tag = '*'
  ELSE
    DELETE(glo:Queue)
    Tag = ''
  END
    Queue:Browse:1.Tag = Tag
  IF (Tag = '*')
    Queue:Browse:1.Tag_Icon = 2
  ELSE
    Queue:Browse:1.Tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::25:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = sto:Ref_Number
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::25:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::25:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::25:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::25:QUEUE = glo:Queue
    ADD(DASBRW::25:QUEUE)
  END
  FREE(glo:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::25:QUEUE.Pointer = sto:Ref_Number
     GET(DASBRW::25:QUEUE,DASBRW::25:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = sto:Ref_Number
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::25:DASSHOWTAG Routine
   CASE DASBRW::25:TAGDISPSTATUS
   OF 0
      DASBRW::25:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::25:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::25:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Stock',1)
    SolaceViewVars('average_temp',average_temp,'Browse_Stock',1)
    SolaceViewVars('days_7_temp',days_7_temp,'Browse_Stock',1)
    SolaceViewVars('days_30_temp',days_30_temp,'Browse_Stock',1)
    SolaceViewVars('days_60_temp',days_60_temp,'Browse_Stock',1)
    SolaceViewVars('days_90_temp',days_90_temp,'Browse_Stock',1)
    SolaceViewVars('average_text_temp',average_text_temp,'Browse_Stock',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Browse_Stock',1)
    SolaceViewVars('tmp:LastOrdered',tmp:LastOrdered,'Browse_Stock',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Browse_Stock',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Browse_Stock',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Browse_Stock',1)
    SolaceViewVars('tmp:partnumber',tmp:partnumber,'Browse_Stock',1)
    SolaceViewVars('tmp:description',tmp:description,'Browse_Stock',1)
    SolaceViewVars('tmp:location',tmp:location,'Browse_Stock',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Stock',1)
    SolaceViewVars('Location_Temp',Location_Temp,'Browse_Stock',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Browse_Stock',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Browse_Stock',1)
    SolaceViewVars('accessory_temp',accessory_temp,'Browse_Stock',1)
    SolaceViewVars('shelf_location_temp',shelf_location_temp,'Browse_Stock',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Stock',1)
    SolaceViewVars('yes_temp',yes_temp,'Browse_Stock',1)
    SolaceViewVars('BrFilter1',BrFilter1,'Browse_Stock',1)
    SolaceViewVars('BrLocator1',BrLocator1,'Browse_Stock',1)
    SolaceViewVars('BrFilter2',BrFilter2,'Browse_Stock',1)
    SolaceViewVars('BrLocator2',BrLocator2,'Browse_Stock',1)
    SolaceViewVars('tmp:QtyOnOrder',tmp:QtyOnOrder,'Browse_Stock',1)
    SolaceViewVars('Tag',Tag,'Browse_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp;  SolaceCtrlName = '?Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button7;  SolaceCtrlName = '?Button7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:USERS_ALIAS.Open
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?STO:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?STO:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,sto:Location,1,BRW1)
  BIND('Tag',Tag)
  BIND('shelf_location_temp',shelf_location_temp)
  BIND('no_temp',no_temp)
  BIND('yes_temp',yes_temp)
  BIND('days_7_temp',days_7_temp)
  BIND('days_30_temp',days_30_temp)
  BIND('days_60_temp',days_60_temp)
  BIND('days_90_temp',days_90_temp)
  BIND('average_text_temp',average_text_temp)
  BIND('Location_Temp',Location_Temp)
  BIND('Manufacturer_Temp',Manufacturer_Temp)
  BIND('accessory_temp',accessory_temp)
  BIND('tmp:QtyOnOrder',tmp:QtyOnOrder)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(Tag,BRW1.Q.Tag)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Manufacturer,BRW1.Q.sto:Manufacturer)
  BRW1.AddField(shelf_location_temp,BRW1.Q.shelf_location_temp)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Retail_Cost,BRW1.Q.sto:Retail_Cost)
  BRW1.AddField(no_temp,BRW1.Q.no_temp)
  BRW1.AddField(yes_temp,BRW1.Q.yes_temp)
  BRW1.AddField(days_7_temp,BRW1.Q.days_7_temp)
  BRW1.AddField(days_30_temp,BRW1.Q.days_30_temp)
  BRW1.AddField(days_60_temp,BRW1.Q.days_60_temp)
  BRW1.AddField(days_90_temp,BRW1.Q.days_90_temp)
  BRW1.AddField(average_text_temp,BRW1.Q.average_text_temp)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Accessory,BRW1.Q.sto:Accessory)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ThisMakeover.SetWindow(Win:Browse)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,?CurrentTab)
      
  
  FDCB6.Init(Location_Temp,?Location_Temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:Location_Key)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,Location_Temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?Browse:1{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:3{PROP:TEXT} = 'By &Part Number'
    ?Tab:4{PROP:TEXT} = 'By Desc&ription'
    ?Browse:1{PROP:FORMAT} ='10L(2)|MJ@s1@#1#86L(2)|M~Part Number~@s30@#3#86L(2)|M~Description~@s30@#4#86L(2)|M~Manufacturer~@s30@#5#132L(2)|M~Shelf Location~@s60@#6#0L(2)|M~Supplier~@s30@/#7#0R(2)|M~Qty In Stock~L@N8@#8#0L(2)|M~Reference Number~@n012@/#9#0L(2)|M~Purchase Cost~@n14.2@/#10#0L(2)|M~Sale Cost~@n14.2@/#11#0L(2)|M~Retail Cost~@n14.2@/#12#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Location_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?sto:Description)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button7
      ThisWindow.Update
      LOOP x# = 1 TO RECORDS(glo:Queue)
        GET(glo:Queue,x#)
        Stock_Barcode_Labels(glo:Queue.GLO:Pointer)
      END
      FREE(glo:Queue)
      CLEAR(glo:Queue)
      BRW1.ResetSort(1)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::25:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      If keycode() = HomeKey
          Presskey(CtrlHome)
      End!If keycode() = HomeKey
      IF KEYCODE() = MouseLeft2
        POST(EVENT:Accepted,?DASTAG)
      END !IF
      
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Location_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?sto:Description)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::25:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='10L(2)|MJ@s1@#1#86L(2)|M~Part Number~@s30@#3#86L(2)|M~Description~@s30@#4#86L(2)|M~Manufacturer~@s30@#5#132L(2)|M~Shelf Location~@s60@#6#0L(2)|M~Supplier~@s30@/#7#0R(2)|M~Qty In Stock~L@N8@#8#0L(2)|M~Reference Number~@n012@/#9#0L(2)|M~Purchase Cost~@n14.2@/#10#0L(2)|M~Sale Cost~@n14.2@/#11#0L(2)|M~Retail Cost~@n14.2@/#12#'
          ?Tab:3{PROP:TEXT} = 'By &Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='10L(2)|MJ@s1@#1#86L(2)|M~Description~@s30@#4#86L(2)|M~Part Number~@s30@#3#86L(2)|M~Manufacturer~@s30@#5#132L(2)|M~Shelf Location~@s60@#6#0L(2)|M~Supplier~@s30@/#7#0R(2)|M~Qty In Stock~L@N8@#8#0L(2)|M~Reference Number~@n012@/#9#0L(2)|M~Purchase Cost~@n14.2@/#10#0L(2)|M~Sale Cost~@n14.2@/#11#0L(2)|M~Retail Cost~@n14.2@/#12#'
          ?Tab:4{PROP:TEXT} = 'By Desc&ription'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Browse,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sto:Part_Number
      Select(?Browse:1)
    OF ?sto:Description
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Site Location Defaults
      Set(defstock)
      access:defstock.next()
      If dst:use_site_location = 'YES'
          location_temp = dst:site_location
      Else
          location_temp = ''
      End
      brw1.resetsort(1)
      Display()
      Select(?browse:1)
      
      BRW1.ResetSort(1)
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSIF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Location_Temp
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sto:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      Tag = ''
    ELSE
      Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  shelf_location_temp = CLIP(sto:Shelf_Location) & ' / ' & CLIP(sto:Second_Location)
  PARENT.SetQueueRecord
  IF (Tag = '*')
    SELF.Q.Tag_Icon = 2
  ELSE
    SELF.Q.Tag_Icon = 1
  END
  SELF.Q.shelf_location_temp = shelf_location_temp    !Assign formula result to display queue


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.UpdateWindow PROCEDURE

  CODE
  PARENT.UpdateWindow
    if Self.SelectControl{prop:hide} <>  (Self.SelectControl+1000){prop:hide}
       (Self.SelectControl+1000){prop:hide} = Self.SelectControl{prop:hide}
    end


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = sto:Ref_Number
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::25:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue


Resizer.Resize PROCEDURE(SIGNED Control)

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize(Control)
    ThisMakeover.Refresh()
  RETURN ReturnValue


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

