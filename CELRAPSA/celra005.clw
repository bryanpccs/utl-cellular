

   MEMBER('celrapsa.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA005.INC'),ONCE        !Local module procedure declarations
                     END


Deplete_Stock PROCEDURE (Passed_Quantity)             !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Decrement Stock Level'),AT(,,215,115),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),TILED,GRAY,DOUBLE,MDI,IMM
                       SHEET,AT(4,4,208,80),USE(?Sheet1),SPREAD
                         TAB('Decrement Stock Level'),USE(?Tab1)
                           PROMPT('Quantity'),AT(8,20),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(84,20,64,10),USE(quantity_temp),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY,RANGE(1,99999999)
                           PROMPT('Date'),AT(8,36),USE(?glo:select2:Prompt)
                           ENTRY(@d6b),AT(84,36,64,10),USE(date_received_temp),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Additional Notes'),AT(8,52),USE(?Prompt3)
                           TEXT,AT(84,52,124,24),USE(additional_notes_temp),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,88,208,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(152,92,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(8,92,56,16),USE(?CancelButton),HIDE,LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?quantity_temp{prop:ReadOnly} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 15066597
    Elsif ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 8454143
    Else ! If ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 16777215
    End ! If ?quantity_temp{prop:Req} = True
    ?quantity_temp{prop:Trn} = 0
    ?quantity_temp{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?date_received_temp{prop:ReadOnly} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 15066597
    Elsif ?date_received_temp{prop:Req} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 8454143
    Else ! If ?date_received_temp{prop:Req} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 16777215
    End ! If ?date_received_temp{prop:Req} = True
    ?date_received_temp{prop:Trn} = 0
    ?date_received_temp{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?additional_notes_temp{prop:ReadOnly} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 15066597
    Elsif ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 8454143
    Else ! If ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 16777215
    End ! If ?additional_notes_temp{prop:Req} = True
    ?additional_notes_temp{prop:Trn} = 0
    ?additional_notes_temp{prop:FontStyle} = font:Bold
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Deplete_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Deplete_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Deplete_Stock',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Deplete_Stock',1)
    SolaceViewVars('purchase_cost_Temp',purchase_cost_Temp,'Deplete_Stock',1)
    SolaceViewVars('sale_cost_temp',sale_cost_temp,'Deplete_Stock',1)
    SolaceViewVars('date_received_temp',date_received_temp,'Deplete_Stock',1)
    SolaceViewVars('additional_notes_temp',additional_notes_temp,'Deplete_Stock',1)
    SolaceViewVars('despatch_note_temp',despatch_note_temp,'Deplete_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_temp;  SolaceCtrlName = '?quantity_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?date_received_temp;  SolaceCtrlName = '?date_received_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?additional_notes_temp;  SolaceCtrlName = '?additional_notes_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Deplete_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Deplete_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt7
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:STOAUDIT.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOAUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Deplete_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      If date_received_temp = ''
          Select(?date_received_temp)
      Else
          if quantity_temp > sto:quantity_stock
      		Case MessageEx('You cannot decrement the stock level below zero.','ServiceBase 2000',|
      		               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      			Of 1 ! &OK Button
      		End!Case MessageEx
              quantity_temp = ''
              Select(?quantity_temp)
          Else
              sto:quantity_stock -= quantity_temp
              if sto:quantity_stock < 0
                  sto:quantity_stock = 0
              End
              access:stock.update()
      
              get(stohist,0)
              shi:information         = additional_notes_temp
              shi:ref_number          = sto:ref_number
              shi:transaction_type    = 'DEC'
              shi:despatch_note_number= ''
              shi:quantity            = quantity_temp
              shi:date                = date_received_temp
              shi:purchase_cost       = sto:purchase_cost
              shi:sale_cost           = sto:sale_cost
              shi:retail_cost         = sto:retail_cost
              shi:job_number          = ''
              access:users.clearkey(use:password_key)
              use:password            =glo:password
              access:users.fetch(use:password_key)
              shi:user                = use:user_code
              shi:notes               = 'STOCK DECREMENTED'
              access:stohist.insert()
              Post(Event:CloseWindow)
          End
      End
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Deplete_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      IF Keycode() = ESCKey
        CYCLE
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Quantity_Temp = Passed_Quantity
      !access:stock.clearkey(sto:ref_number_key)
      !sto:ref_number = glo:select1
      !If access:stock.fetch(sto:ref_number_key)
      !    Return Level:Fatal
      !end
      date_received_temp = Today()
      DISPLAY()
      UPDATE()
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

