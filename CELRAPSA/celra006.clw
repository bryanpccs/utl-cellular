

   MEMBER('celrapsa.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA002.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowseAudit PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STMASAUD)
                       PROJECT(stom:Audit_No)
                       PROJECT(stom:Audit_Date)
                       PROJECT(stom:Audit_Time)
                       PROJECT(stom:Audit_User)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stom:Audit_No          LIKE(stom:Audit_No)            !List box control field - type derived from field
stom:Audit_No_NormalFG LONG                           !Normal forground color
stom:Audit_No_NormalBG LONG                           !Normal background color
stom:Audit_No_SelectedFG LONG                         !Selected forground color
stom:Audit_No_SelectedBG LONG                         !Selected background color
stom:Audit_Date        LIKE(stom:Audit_Date)          !List box control field - type derived from field
stom:Audit_Date_NormalFG LONG                         !Normal forground color
stom:Audit_Date_NormalBG LONG                         !Normal background color
stom:Audit_Date_SelectedFG LONG                       !Selected forground color
stom:Audit_Date_SelectedBG LONG                       !Selected background color
stom:Audit_Time        LIKE(stom:Audit_Time)          !List box control field - type derived from field
stom:Audit_Time_NormalFG LONG                         !Normal forground color
stom:Audit_Time_NormalBG LONG                         !Normal background color
stom:Audit_Time_SelectedFG LONG                       !Selected forground color
stom:Audit_Time_SelectedBG LONG                       !Selected background color
stom:Audit_User        LIKE(stom:Audit_User)          !List box control field - type derived from field
stom:Audit_User_NormalFG LONG                         !Normal forground color
stom:Audit_User_NormalBG LONG                         !Normal background color
stom:Audit_User_SelectedFG LONG                       !Selected forground color
stom:Audit_User_SelectedBG LONG                       !Selected background color
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Stock Audits'),AT(,,348,172),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,HLP('BrowseAudit'),SYSTEM,GRAY,RESIZE,MDI
                       LIST,AT(8,32,252,132),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('64R(2)|M*~Audit Number~L@n-14@64L(2)|M*~Audit Date~@d6@64L(2)|M*~Audit Time~@t1@' &|
   '40L(2)|M*~User Code~@S3@'),FROM(Queue:Browse:1)
                       BUTTON('Create Audit'),AT(268,32,76,20),USE(?Button2),LEFT,ICON('Cabinet.gif')
                       BUTTON('Continue Audit'),AT(268,56,76,20),USE(?Button3),LEFT,ICON('transfer.gif')
                       BUTTON('Complete Audit'),AT(268,80,76,20),USE(?Button4),LEFT,ICON('Wizok.ico')
                       BUTTON('Reprint Audit'),AT(268,104,76,20),USE(?Button5),LEFT,ICON(ICON:Print1)
                       SHEET,AT(4,4,260,164),USE(?CurrentTab),SPREAD
                         TAB('Stock Audit Table'),USE(?Tab:2)
                           ENTRY(@n-14),AT(8,16,64,12),USE(stom:Audit_No),RIGHT(1),FONT(,,,FONT:bold)
                         END
                       END
                       BUTTON('Close'),AT(268,148,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?stom:Audit_No{prop:ReadOnly} = True
        ?stom:Audit_No{prop:FontColor} = 65793
        ?stom:Audit_No{prop:Color} = 15066597
    Elsif ?stom:Audit_No{prop:Req} = True
        ?stom:Audit_No{prop:FontColor} = 65793
        ?stom:Audit_No{prop:Color} = 8454143
    Else ! If ?stom:Audit_No{prop:Req} = True
        ?stom:Audit_No{prop:FontColor} = 65793
        ?stom:Audit_No{prop:Color} = 16777215
    End ! If ?stom:Audit_No{prop:Req} = True
    ?stom:Audit_No{prop:Trn} = 0
    ?stom:Audit_No{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BrowseAudit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'BrowseAudit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button5;  SolaceCtrlName = '?Button5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stom:Audit_No;  SolaceCtrlName = '?stom:Audit_No';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowseAudit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BrowseAudit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:STMASAUD.Open
  Access:STOAUDIT.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STMASAUD,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stom:AutoIncrement_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STOM:Audit_No,stom:Audit_No,1,BRW1)
  BRW1.AddField(stom:Audit_No,BRW1.Q.stom:Audit_No)
  BRW1.AddField(stom:Audit_Date,BRW1.Q.stom:Audit_Date)
  BRW1.AddField(stom:Audit_Time,BRW1.Q.stom:Audit_Time)
  BRW1.AddField(stom:Audit_User,BRW1.Q.stom:Audit_User)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STMASAUD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BrowseAudit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Button2
      !Check if there are no INCOMPLETE audits!
      Access:StMasAud.ClearKey(STOM:Compeleted_Key)
      STOM:Complete = 'N'
      IF Access:StMasAud.Fetch(STOM:Compeleted_Key)
        !Fine, there are no unfinished audits!
        Access:StMasAud.PrimeRecord()
        STOM:Audit_Date = TODAY()
        STOM:Audit_Time = CLOCK()
        Access:USERS.Clearkey(use:Password_Key)
        use:Password    = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Found
            STOM:Audit_User = use:User_Code
        Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        STOM:Complete   = 'N'
        Access:StMasAud.Update()
      
      
      
      Stock_Check_Procedure()
      ELSE
        Case MessageEx('There are incomplete Stock Audits currently outstanding. You cannot start another Stock Audit.','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,3000) 
          Of 1 ! &OK Button
        End!Case MessageEx
      END
      BRW1.ResetSort(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button3
      ThisWindow.Update
      ThisWindow.Update()
      IF STOM:Complete = 'N'
      Stock_Check_Procedure()
      ELSE
        Case MessageEx('This Stock Audit is complete. Please start a new Audit or select an incomplete Audit.','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,3000) 
          Of 1 ! &OK Button
        End!Case MessageEx
      END
      BRW1.ResetSort(1)
    OF ?Button4
      ThisWindow.Update
      ThisWindow.Update()
      IF STOM:Complete = 'N'
        Case MessageEx('Complete the current Stock Audit?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,3000) 
          Of 1 ! &OK Button
            !Lets check if we can!
            None# = 0 !temp to check
            Access:StoAudit.ClearKey(STOA:Audit_Ref_No_Key)
            STOA:Audit_Ref_No = STOM:Audit_No
            SET(STOA:Audit_Ref_No_Key,STOA:Audit_Ref_No_Key)
            LOOP
              IF Access:StoAudit.Next()
                BREAK
              END
              IF STOA:Audit_Ref_No <> STOM:Audit_No
                BREAK
              END
              IF STOA:Preliminary = 'Y'
                None# = 1
                BREAK
              END
            END
            IF None# = 0
              STOM:Complete = 'Y'
              Access:StMasAud.Update()
      
       Stock_Audit_Report (STOM:Audit_No)
              BRW1.ResetSort(1)
            ELSE
              Case MessageEx('Unable to complete Stock Audit. There are items still marked as preliminary.','ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,3000) 
                Of 1 ! &OK Button
              End!Case MessageEx
            END
          Of 2 ! &Cancel Button
        End!Case MessagheEx
      ELSE
        Case MessageEx('Stock Audit already marked as complete.','ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,3000) 
          Of 1 ! &OK Button
        End!Case MessageEx
      END
    OF ?Button5
      ThisWindow.Update
      ThisWindow.Update()
       Stock_Audit_Report (STOM:Audit_No)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BrowseAudit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.stom:Audit_No_NormalFG = -1
  SELF.Q.stom:Audit_No_NormalBG = -1
  SELF.Q.stom:Audit_No_SelectedFG = -1
  SELF.Q.stom:Audit_No_SelectedBG = -1
  SELF.Q.stom:Audit_Date_NormalFG = -1
  SELF.Q.stom:Audit_Date_NormalBG = -1
  SELF.Q.stom:Audit_Date_SelectedFG = -1
  SELF.Q.stom:Audit_Date_SelectedBG = -1
  SELF.Q.stom:Audit_Time_NormalFG = -1
  SELF.Q.stom:Audit_Time_NormalBG = -1
  SELF.Q.stom:Audit_Time_SelectedFG = -1
  SELF.Q.stom:Audit_Time_SelectedBG = -1
  SELF.Q.stom:Audit_User_NormalFG = -1
  SELF.Q.stom:Audit_User_NormalBG = -1
  SELF.Q.stom:Audit_User_SelectedFG = -1
  SELF.Q.stom:Audit_User_SelectedBG = -1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

