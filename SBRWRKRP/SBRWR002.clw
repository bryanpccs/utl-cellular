

   MEMBER('SBRWRKRP.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBRWR002.INC'),ONCE        !Local module procedure declarations
                     END








WorkshopReport PROCEDURE(func:StartDate,func:EndDate,func:Status)
tmp:RecordsCount    LONG
tmp:Printer    STRING(255)
RejectRecord         LONG,AUTO
save_job_id          USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:In               LONG
tmp:Out              LONG
tmp:WIP              LONG
tmp:Spares           LONG
tmp:Exchange         LONG
tmp:IMEI             LONG
tmp:OldestJob        DATE
tmp:printedby        STRING(60)
tmp:TelephoneNumber  STRING(20)
tmp:FaxNumber        STRING(20)
!-----------------------------------------------------------------------------
Process:View         VIEW(JOBS)
                     END
report               REPORT,AT(396,2792,7521,8510),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(396,927,7521,1375),USE(?unnamed)
                         STRING('Start Date:'),AT(5000,156),USE(?string22),TRN,FONT(,8,,)
                         STRING(@d6),AT(5938,156),USE(func:StartDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('End Date:'),AT(5000,365),USE(?string22:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,365),USE(func:EndDate),TRN,LEFT,FONT(,8,,FONT:bold)
                         STRING('Printed By:'),AT(5000,573),USE(?string27),TRN,FONT(,8,,)
                         STRING(@s60),AT(5885,573),USE(tmp:printedby),TRN,LEFT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Printed:'),AT(5000,781),USE(?string27:2),TRN,FONT(,8,,)
                         STRING(@d6),AT(5885,781),USE(ReportRunDate),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@t1),AT(6510,781),USE(ReportRunTime),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Page Number:'),AT(5000,990),USE(?string27:3),TRN,FONT(,8,,)
                         STRING(@s3),AT(5885,990),PAGENO,USE(?reportpageno),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                         STRING('Of'),AT(6146,990),USE(?string26),TRN,FONT(,8,,FONT:bold)
                         STRING('?PP?'),AT(6354,990,375,208),USE(?CPCSPgOfPgStr),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
endofreportbreak       BREAK(endofreport)
detail                   DETAIL,AT(,,,3250),USE(?detailband)
                           STRING('In:'),AT(3156,260),USE(?String24),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@s8),AT(3542,260),USE(tmp:In),TRN,LEFT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('Out:'),AT(3021,625),USE(?String24:2),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@s8),AT(3542,625),USE(tmp:Out),TRN,LEFT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('W.I.P.'),AT(2927,990),USE(?String24:3),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@s8),AT(3542,990),USE(tmp:WIP),TRN,LEFT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('Spares:'),AT(2740,1406),USE(?String24:4),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@s8),AT(3542,1406),USE(tmp:Spares),TRN,LEFT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('Awaiting Exchange:'),AT(1771,1823),USE(?String24:5),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@s8),AT(3542,1823),USE(tmp:Exchange),TRN,LEFT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('I.M.E.I.'),AT(2865,2240),USE(?String24:6),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@s8),AT(3542,2240),USE(tmp:IMEI),TRN,LEFT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING('Oldest Job:'),AT(2448,2656),USE(?String24:7),TRN,RIGHT,FONT(,12,,FONT:bold)
                           STRING(@d6),AT(3542,2656),USE(tmp:OldestJob),TRN,LEFT,FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0,,438),USE(?unnamed:2)
                         END
                       END
                       FOOTER,AT(396,10156,7521,333),USE(?unnamed:4)
                       END
                       FORM,AT(396,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RLISTSIM.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING(@s30),AT(156,0,3844,240),USE(def:User_Name),TRN,LEFT,FONT(,16,,FONT:bold)
                         STRING('Workshop Report'),AT(4063,0,3385,260),USE(?string20),TRN,RIGHT,FONT(,14,,FONT:bold)
                         STRING(@s30),AT(156,260,2240,156),USE(def:Address_Line1),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,417,2240,156),USE(def:Address_Line2),TRN,FONT(,9,,)
                         STRING(@s30),AT(156,573,2240,156),USE(def:Address_Line3),TRN,FONT(,9,,)
                         STRING(@s15),AT(156,729,1156,156),USE(def:Postcode),TRN,FONT(,9,,)
                         STRING('Tel:'),AT(156,1042),USE(?string16),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1042),USE(tmp:TelephoneNumber),TRN,FONT(,9,,)
                         STRING('Fax: '),AT(156,1198),USE(?string19),TRN,FONT(,9,,)
                         STRING(@s20),AT(521,1198),USE(tmp:FaxNumber),TRN,FONT(,9,,)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('WorkshopReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  AsciiOutputReq = True
  AsciiLineOption = 76
  SETCURSOR
  IF ClarioNETServer:Active()                         !---ClarioNET 82
    PreviewReq = True
  ELSE
  PreviewReq = True
  END                                                 !---ClarioNET 83
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:JOBS.Open
  Relate:DEFAULTS.Open
  Access:USERS.UseFile
  
  
  RecordsToProcess = RECORDS(JOBS)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  IF ClarioNETServer:Active() THEN SYSTEM{PROP:PrintMode} = 2 END !---ClarioNET 68
  SEND(JOBS,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      RecordsToProcess = Records(JOBS)
      DO OpenReportRoutine
    OF Event:Timer
        tmp:In          =0
        tmp:Out         =0
        tmp:WIP         =0
        tmp:Spares      =0
        tmp:Exchange    =0
        tmp:IMEI        =0
        tmp:OldestJob   =0
        
        Save_job_ID = Access:JOBS.SaveFile()
        Access:JOBS.ClearKey(job:Date_Booked_Key)
        job:date_booked = func:StartDate
        Set(job:Date_Booked_Key,job:Date_Booked_Key)
        Loop
            If Access:JOBS.NEXT()
               Break
            End !If
            If job:date_booked > func:EndDate      |
                Then Break.  ! End If
            RecordsProcessed += 1
            Do DisplayProgress
            !Ignore "Process"
            If job:Warranty_Job = 'YES'
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = job:Warranty_Charge_Type
                Get(glo:Queue,glo:Pointer)
                If ~Error()
                    Cycle
                End !If Error()
            End !If job:Warranty_Job = 'YES'
        
            If job:Date_Completed = job:Date_Booked
                tmp:In += 1
            End !If job:Date_Completed = job:Date_Booked
        
            If job:Date_Despatched = job:Date_Booked
                tmp:Out += 1
            End !If job:Date_Despatched = job:Date_Booked
            If job:Exchange_Despatched = job:Date_Booked
                tmp:Out += 1
            End !If job:Exchange_Despatched = job:Date_Booked
        
        End !Loop
        Access:JOBS.RestoreFile(Save_job_ID)
        
        Save_job_ID = Access:JOBS.SaveFile()
        Access:JOBS.ClearKey(job:DateCompletedKey)
        job:Date_Completed = ''
        Set(job:DateCompletedKey,job:DateCompletedKey)
        Loop
            If Access:JOBS.NEXT()
               Break
            End !If
            If job:Date_Completed <> ''      |
                Then Break.  ! End If
        
            RecordsProcessed += 1
            Do DisplayProgress
            !Ignore "Process"
            If job:Warranty_Job = 'YES'
                Sort(glo:Queue,glo:Pointer)
                glo:Pointer = job:Warranty_Charge_Type
                Get(glo:Queue,glo:Pointer)
                If ~Error()
                    Cycle
                End !If Error()
            End !If job:Warranty_Job = 'YES'
        
            !WIP
            tmp:WIP += 1
        
            !Spares
            Sort(glo:Queue2,glo:Pointer2)
            glo:Pointer2    = job:Current_Status
            Get(glo:Queue2,glo:Pointer2)
            If ~Error()
                tmp:Spares += 1
            End !If ~Error()
            
        
            !Exchange
            Sort(glo:Queue3,glo:Pointer3)
            glo:Pointer3    = job:Current_Status
            Get(glo:Queue3,glo:Pointer3)
            If ~Error()
                tmp:Exchange += 1
            End !If ~Error()
        
            !IMEI
            Sort(glo:Queue4,glo:Pointer4)
            glo:Pointer4    = job:Current_Status
            Get(glo:Queue4,glo:Pointer4)
            If ~Error()
                tmp:IMEI += 1
            End !If ~Error()
        
            !Oldest Job
            Sort(glo:Queue5,glo:Pointer5)
            glo:Pointer5    = job:Transit_Type
            Get(glo:Queue5,glo:Pointer5)
            If ~Error()
                Cycle
            End !If ~Error()
        
            If job:Current_Status   = func:Status
                If tmp:OldestJob = ''
                    tmp:OldestJob = job:Date_Booked
                End !If tmp:OldestJob = ''
                If job:Date_Booked < tmp:OldestJob
                    tmp:OldestJob = job:Date_Booked
                End !If job:Date_Booked < tmp:OldestJob
            End !If job:Current_Status   = func:Status
        
        End !Loop
        Access:JOBS.RestoreFile(Save_job_ID)
        Print(rpt:Detail)
        
        
        LocalResponse = RequestCompleted
        BREAK
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(JOBS,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          DO AsciiOutputRoutine
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        DO AsciiOutputRoutine
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  !reset printer
  if tmp:printer <> ''
      printer{propprint:device} = tmp:Printer
  end!if tmp:printer <> ''
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN

AsciiOutputRoutine    ROUTINE
IF RECORDS(PrintPreviewQueue)
  Wmf2AsciiName = 'c:\REPORT.TXT'
  IF FileDialog(CPCS:AsciiOutTitle,Wmf2AsciiName,CPCS:AsciiOutMasks,1)
    Wmf2Ascii(PrintPreviewQueue, Wmf2AsciiName, AsciiLineOption)
  END
END

GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  If clarionetserver:active()
  previewreq = True
  ClarioNET:CallClientProcedure('SetClientPrinter','')
  Else
  previewreq = true
  End !If clarionetserver:active()
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  set(defaults)
  access:defaults.next()
  !Printed By
  access:users.clearkey(use:password_key)
  use:password = glo:password
  access:users.fetch(use:password_key)
  tmp:PrintedBy = clip(use:forename) & ' ' & clip(use:surname)
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='WorkshopReport'
  END
  report{Prop:Preview} = PrintPreviewImage


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674





