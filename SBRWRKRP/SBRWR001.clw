

   MEMBER('SBRWRKRP.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBRWR001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::3:TAGFLAG          BYTE(0)
DASBRW::3:TAGMOUSE         BYTE(0)
DASBRW::3:TAGDISPSTATUS    BYTE(0)
DASBRW::3:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer3                      LIKE(GLO:Pointer3)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer4                      LIKE(GLO:Pointer4)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::15:TAGFLAG         BYTE(0)
DASBRW::15:TAGMOUSE        BYTE(0)
DASBRW::15:TAGDISPSTATUS   BYTE(0)
DASBRW::15:QUEUE          QUEUE
Pointer5                      LIKE(GLO:Pointer5)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:RepairTag        STRING('0')
tmp:StatusTag        STRING(1)
tmp:Yes              STRING('YES')
tmp:StatusTag2       STRING(1)
tmp:StatusTag3       STRING(1)
tmp:Status           STRING(30)
tmp:TransitTag       STRING(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:Status
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW2::View:Browse    VIEW(REPTYDEF)
                       PROJECT(rtd:Repair_Type)
                       PROJECT(rtd:Warranty)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:RepairTag          LIKE(tmp:RepairTag)            !List box control field - type derived from local data
tmp:RepairTag_Icon     LONG                           !Entry's icon ID
rtd:Repair_Type        LIKE(rtd:Repair_Type)          !List box control field - type derived from field
rtd:Warranty           LIKE(rtd:Warranty)             !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW4::View:Browse    VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Job)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:StatusTag          LIKE(tmp:StatusTag)            !List box control field - type derived from local data
tmp:StatusTag_Icon     LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
sts:Job                LIKE(sts:Job)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Job)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:StatusTag2         LIKE(tmp:StatusTag2)           !List box control field - type derived from local data
tmp:StatusTag2_Icon    LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
sts:Job                LIKE(sts:Job)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Job)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
tmp:StatusTag3         LIKE(tmp:StatusTag3)           !List box control field - type derived from local data
tmp:StatusTag3_Icon    LONG                           !Entry's icon ID
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
sts:Job                LIKE(sts:Job)                  !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:5
tmp:TransitTag         LIKE(tmp:TransitTag)           !List box control field - type derived from local data
tmp:TransitTag_Icon    LONG                           !Entry's icon ID
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB13::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
window               WINDOW('Workshop Report Criteria'),AT(,,303,326),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       PROMPT('Workshop Report'),AT(8,8),USE(?Prompt1),FONT(,16,,FONT:bold)
                       SHEET,AT(4,4,296,292),USE(?Sheet1),BELOW,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Date Range'),AT(96,36),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI)
                           PROMPT('Select the Booking Date range.'),AT(96,52,208,12),USE(?Prompt3)
                           PROMPT('Start Date'),AT(96,120),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(168,120,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(236,120,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('End Date'),AT(96,144),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(168,144,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(236,144,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                         END
                         TAB('Tab 2'),USE(?Tab2)
                           PROMPT('Process'),AT(96,36),USE(?Prompt6),FONT(,8,COLOR:Navy,FONT:underline)
                           PROMPT('Select the Repair Type which signifies a "Process" job.'),AT(96,52,196,20),USE(?Prompt7)
                           LIST,AT(108,80,168,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)J@s1@120L|M~Repair Type~L(2)@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(151,116,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(163,144,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(108,228,56,16),USE(?DASTAG),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(164,228,56,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(220,228,56,16),USE(?DASUNTAGALL),LEFT,ICON('UnTag.gif')
                         END
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Spares'),AT(96,36),USE(?Prompt8),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Statuses to include in the "Spares" calculation'),AT(96,52,196,16),USE(?Prompt9)
                           LIST,AT(108,80,169,144),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@120L|M~Status~L(2)@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(167,108,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(171,132,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('&Tag'),AT(108,228,56,16),USE(?DASTAG:2),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(164,228,56,16),USE(?DASTAGAll:2),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(220,228,56,16),USE(?DASUNTAGALL:2),LEFT,ICON('UnTag.gif')
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Awaiting Exchange'),AT(96,36),USE(?Prompt10),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select which Statuses to include in the "Awaiting Exchange" calculation.'),AT(96,52,200,24),USE(?Prompt11)
                           LIST,AT(108,80,168,144),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Status~@s30@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(199,124,50,13),USE(?DASREVTAG:3),HIDE
                           BUTTON('sho&W tags'),AT(191,148,70,13),USE(?DASSHOWTAG:3),HIDE
                           BUTTON('&Tag'),AT(108,228,56,16),USE(?DASTAG:3),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(164,228,56,16),USE(?DASTAGAll:3),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(220,228,56,16),USE(?DASUNTAGALL:3),LEFT,ICON('UnTag.gif')
                         END
                         TAB('Tab 5'),USE(?Tab5)
                           PROMPT('IMEI'),AT(96,36),USE(?Prompt12),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select the Statuses to include in the "IMEI" calculation.'),AT(96,52),USE(?Prompt13)
                           LIST,AT(108,80,168,144),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L|M~Status~L(2)@s30@'),FROM(Queue:Browse:3)
                           BUTTON('&Rev tags'),AT(203,88,50,13),USE(?DASREVTAG:4),HIDE
                           BUTTON('sho&W tags'),AT(195,120,70,13),USE(?DASSHOWTAG:4),HIDE
                           BUTTON('&Tag'),AT(108,228,56,16),USE(?DASTAG:4),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(164,228,56,16),USE(?DASTAGAll:4),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(220,228,56,16),USE(?DASUNTAGALL:4),LEFT,ICON('UnTag.gif')
                         END
                         TAB('Tab 6'),USE(?Tab6)
                           PROMPT('Oldest Job'),AT(96,36),USE(?Prompt14),FONT(,,COLOR:Navy,FONT:underline)
                           PROMPT('Select the Status to find the "Oldest Job".'),AT(96,52),USE(?Prompt15)
                           PROMPT('Status'),AT(96,68),USE(?Prompt16)
                           COMBO(@s20),AT(168,68,124,10),USE(tmp:Status),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Select the Transit Type(s) to EXCLUDE from the "Oldest Job" calculations.'),AT(96,88,196,20),USE(?Prompt17)
                           LIST,AT(108,112,168,144),USE(?List:5),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@120L(2)|M~Transit Type~@s30@'),FROM(Queue:Browse:4)
                           BUTTON('&Rev tags'),AT(187,148,50,13),USE(?DASREVTAG:5),HIDE
                           BUTTON('sho&W tags'),AT(203,172,70,13),USE(?DASSHOWTAG:5),HIDE
                           BUTTON('&Tag'),AT(108,260,56,16),USE(?DASTAG:5),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All'),AT(164,260,56,16),USE(?DASTAGAll:5),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(220,260,56,16),USE(?DASUNTAGALL:5),LEFT,ICON('UnTag.gif')
                         END
                       END
                       IMAGE('WIZARD.GIF'),AT(8,104),USE(?Image1)
                       PANEL,AT(4,300,296,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,304,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,304,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       BUTTON('&Finish'),AT(184,304,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       BUTTON('Cancel'),AT(240,304,56,16),USE(?Close),LEFT,ICON('CANCEL.GIF')
                     END

Wizard10         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW4                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW11                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW11::Sort0:Locator StepLocatorClass                 !Default Locator
BRW14                CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:4                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
FDCB13               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Tab3{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Tab4{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Tab5{prop:Color} = 15066597
    ?Prompt12{prop:FontColor} = -1
    ?Prompt12{prop:Color} = 15066597
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Tab6{prop:Color} = 15066597
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    ?Prompt15{prop:FontColor} = -1
    ?Prompt15{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    If ?tmp:Status{prop:ReadOnly} = True
        ?tmp:Status{prop:FontColor} = 65793
        ?tmp:Status{prop:Color} = 15066597
    Elsif ?tmp:Status{prop:Req} = True
        ?tmp:Status{prop:FontColor} = 65793
        ?tmp:Status{prop:Color} = 8454143
    Else ! If ?tmp:Status{prop:Req} = True
        ?tmp:Status{prop:FontColor} = 65793
        ?tmp:Status{prop:Color} = 16777215
    End ! If ?tmp:Status{prop:Req} = True
    ?tmp:Status{prop:Trn} = 0
    ?tmp:Status{prop:FontStyle} = font:Bold
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?List:5{prop:FontColor} = 65793
    ?List:5{prop:Color}= 16777215
    ?List:5{prop:Color,2} = 16777215
    ?List:5{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::3:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW2.UpdateBuffer
   glo:Queue.Pointer = rtd:Repair_Type
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = rtd:Repair_Type
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:RepairTag = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:RepairTag = ''
  END
    Queue:Browse.tmp:RepairTag = tmp:RepairTag
  IF (tmp:Repairtag = '*')
    Queue:Browse.tmp:RepairTag_Icon = 2
  ELSE
    Queue:Browse.tmp:RepairTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW2.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = rtd:Repair_Type
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW2.Reset
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::3:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::3:QUEUE = glo:Queue
    ADD(DASBRW::3:QUEUE)
  END
  FREE(glo:Queue)
  BRW2.Reset
  LOOP
    NEXT(BRW2::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::3:QUEUE.Pointer = rtd:Repair_Type
     GET(DASBRW::3:QUEUE,DASBRW::3:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = rtd:Repair_Type
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW2.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::3:DASSHOWTAG Routine
   CASE DASBRW::3:TAGDISPSTATUS
   OF 0
      DASBRW::3:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::3:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::3:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW2.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW4.UpdateBuffer
   glo:Queue2.Pointer2 = sts:Status
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = sts:Status
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:StatusTag = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:StatusTag = ''
  END
    Queue:Browse:1.tmp:StatusTag = tmp:StatusTag
  IF (tmp:Statustag = '*')
    Queue:Browse:1.tmp:StatusTag_Icon = 2
  ELSE
    Queue:Browse:1.tmp:StatusTag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW4.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = sts:Status
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW4.Reset
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::5:QUEUE = glo:Queue2
    ADD(DASBRW::5:QUEUE)
  END
  FREE(glo:Queue2)
  BRW4.Reset
  LOOP
    NEXT(BRW4::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer2 = sts:Status
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = sts:Status
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW4.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW4.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW6.UpdateBuffer
   glo:Queue3.Pointer3 = sts:Status
   GET(glo:Queue3,glo:Queue3.Pointer3)
  IF ERRORCODE()
     glo:Queue3.Pointer3 = sts:Status
     ADD(glo:Queue3,glo:Queue3.Pointer3)
    tmp:StatusTag2 = '*'
  ELSE
    DELETE(glo:Queue3)
    tmp:StatusTag2 = ''
  END
    Queue:Browse:2.tmp:StatusTag2 = tmp:StatusTag2
  IF (tmp:Statustag2 = '*')
    Queue:Browse:2.tmp:StatusTag2_Icon = 2
  ELSE
    Queue:Browse:2.tmp:StatusTag2_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue3)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue3.Pointer3 = sts:Status
     ADD(glo:Queue3,glo:Queue3.Pointer3)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue3)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue3)
    GET(glo:Queue3,QR#)
    DASBRW::7:QUEUE = glo:Queue3
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Queue3)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer3 = sts:Status
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer3)
    IF ERRORCODE()
       glo:Queue3.Pointer3 = sts:Status
       ADD(glo:Queue3,glo:Queue3.Pointer3)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:3{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:3{PROP:Text} = 'Show All'
      ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:3{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:3,CHOICE(?List:4))
  BRW11.UpdateBuffer
   glo:Queue4.Pointer4 = sts:Status
   GET(glo:Queue4,glo:Queue4.Pointer4)
  IF ERRORCODE()
     glo:Queue4.Pointer4 = sts:Status
     ADD(glo:Queue4,glo:Queue4.Pointer4)
    tmp:StatusTag3 = '*'
  ELSE
    DELETE(glo:Queue4)
    tmp:StatusTag3 = ''
  END
    Queue:Browse:3.tmp:StatusTag3 = tmp:StatusTag3
  IF (tmp:Statustag3 = '*')
    Queue:Browse:3.tmp:StatusTag3_Icon = 2
  ELSE
    Queue:Browse:3.tmp:StatusTag3_Icon = 1
  END
  PUT(Queue:Browse:3)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::12:DASTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW11.Reset
  FREE(glo:Queue4)
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue4.Pointer4 = sts:Status
     ADD(glo:Queue4,glo:Queue4.Pointer4)
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::12:DASUNTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue4)
  BRW11.Reset
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::12:DASREVTAGALL Routine
  ?List:4{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue4)
    GET(glo:Queue4,QR#)
    DASBRW::12:QUEUE = glo:Queue4
    ADD(DASBRW::12:QUEUE)
  END
  FREE(glo:Queue4)
  BRW11.Reset
  LOOP
    NEXT(BRW11::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer4 = sts:Status
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer4)
    IF ERRORCODE()
       glo:Queue4.Pointer4 = sts:Status
       ADD(glo:Queue4,glo:Queue4.Pointer4)
    END
  END
  SETCURSOR
  BRW11.ResetSort(1)
  SELECT(?List:4,CHOICE(?List:4))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:4{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:4{PROP:Text} = 'Show All'
      ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:4{PROP:Text})
   BRW11.ResetSort(1)
   SELECT(?List:4,CHOICE(?List:4))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::15:DASTAGONOFF Routine
  GET(Queue:Browse:4,CHOICE(?List:5))
  BRW14.UpdateBuffer
   glo:Queue5.Pointer5 = trt:Transit_Type
   GET(glo:Queue5,glo:Queue5.Pointer5)
  IF ERRORCODE()
     glo:Queue5.Pointer5 = trt:Transit_Type
     ADD(glo:Queue5,glo:Queue5.Pointer5)
    tmp:TransitTag = '*'
  ELSE
    DELETE(glo:Queue5)
    tmp:TransitTag = ''
  END
    Queue:Browse:4.tmp:TransitTag = tmp:TransitTag
  IF (tmp:Transittag = '*')
    Queue:Browse:4.tmp:TransitTag_Icon = 2
  ELSE
    Queue:Browse:4.tmp:TransitTag_Icon = 1
  END
  PUT(Queue:Browse:4)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW14.Reset
  FREE(glo:Queue5)
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue5.Pointer5 = trt:Transit_Type
     ADD(glo:Queue5,glo:Queue5.Pointer5)
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASUNTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue5)
  BRW14.Reset
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASREVTAGALL Routine
  ?List:5{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::15:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue5)
    GET(glo:Queue5,QR#)
    DASBRW::15:QUEUE = glo:Queue5
    ADD(DASBRW::15:QUEUE)
  END
  FREE(glo:Queue5)
  BRW14.Reset
  LOOP
    NEXT(BRW14::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::15:QUEUE.Pointer5 = trt:Transit_Type
     GET(DASBRW::15:QUEUE,DASBRW::15:QUEUE.Pointer5)
    IF ERRORCODE()
       glo:Queue5.Pointer5 = trt:Transit_Type
       ADD(glo:Queue5,glo:Queue5.Pointer5)
    END
  END
  SETCURSOR
  BRW14.ResetSort(1)
  SELECT(?List:5,CHOICE(?List:5))
DASBRW::15:DASSHOWTAG Routine
   CASE DASBRW::15:TAGDISPSTATUS
   OF 0
      DASBRW::15:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::15:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:5{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::15:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:5{PROP:Text} = 'Show All'
      ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:5{PROP:Text})
   BRW14.ResetSort(1)
   SELECT(?List:5,CHOICE(?List:5))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:REPTYDEF.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  SELF.FilesOpened = True
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  BRW2.Init(?List,Queue:Browse.ViewPosition,BRW2::View:Browse,Queue:Browse,Relate:REPTYDEF,SELF)
  BRW4.Init(?List:2,Queue:Browse:1.ViewPosition,BRW4::View:Browse,Queue:Browse:1,Relate:STATUS,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:STATUS,SELF)
  BRW11.Init(?List:4,Queue:Browse:3.ViewPosition,BRW11::View:Browse,Queue:Browse:3,Relate:STATUS,SELF)
  BRW14.Init(?List:5,Queue:Browse:4.ViewPosition,BRW14::View:Browse,Queue:Browse:4,Relate:TRANTYPE,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
    Wizard10.Init(?Sheet1, |                          ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?Finish, |                       ! OK button
                     ?Close, |                        ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  BRW2.Q &= Queue:Browse
  BRW2.AddSortOrder(,rtd:Warranty_Key)
  BRW2.AddRange(rtd:Warranty,tmp:Yes)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,rtd:Repair_Type,1,BRW2)
  BIND('tmp:RepairTag',tmp:RepairTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW2.AddField(tmp:RepairTag,BRW2.Q.tmp:RepairTag)
  BRW2.AddField(rtd:Repair_Type,BRW2.Q.rtd:Repair_Type)
  BRW2.AddField(rtd:Warranty,BRW2.Q.rtd:Warranty)
  BRW4.Q &= Queue:Browse:1
  BRW4.AddSortOrder(,sts:JobKey)
  BRW4.AddRange(sts:Job,tmp:Yes)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,sts:Status,1,BRW4)
  BIND('tmp:StatusTag',tmp:StatusTag)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW4.AddField(tmp:StatusTag,BRW4.Q.tmp:StatusTag)
  BRW4.AddField(sts:Status,BRW4.Q.sts:Status)
  BRW4.AddField(sts:Ref_Number,BRW4.Q.sts:Ref_Number)
  BRW4.AddField(sts:Job,BRW4.Q.sts:Job)
  BRW6.Q &= Queue:Browse:2
  BRW6.AddSortOrder(,sts:JobKey)
  BRW6.AddRange(sts:Job,tmp:Yes)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,sts:Status,1,BRW6)
  BIND('tmp:StatusTag2',tmp:StatusTag2)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:StatusTag2,BRW6.Q.tmp:StatusTag2)
  BRW6.AddField(sts:Status,BRW6.Q.sts:Status)
  BRW6.AddField(sts:Ref_Number,BRW6.Q.sts:Ref_Number)
  BRW6.AddField(sts:Job,BRW6.Q.sts:Job)
  BRW11.Q &= Queue:Browse:3
  BRW11.AddSortOrder(,sts:JobKey)
  BRW11.AddRange(sts:Job,tmp:Yes)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(,sts:Status,1,BRW11)
  BIND('tmp:StatusTag3',tmp:StatusTag3)
  ?List:4{PROP:IconList,1} = '~notick1.ico'
  ?List:4{PROP:IconList,2} = '~tick1.ico'
  BRW11.AddField(tmp:StatusTag3,BRW11.Q.tmp:StatusTag3)
  BRW11.AddField(sts:Status,BRW11.Q.sts:Status)
  BRW11.AddField(sts:Ref_Number,BRW11.Q.sts:Ref_Number)
  BRW11.AddField(sts:Job,BRW11.Q.sts:Job)
  BRW14.Q &= Queue:Browse:4
  BRW14.AddSortOrder(,trt:Transit_Type_Key)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,trt:Transit_Type,1,BRW14)
  BIND('tmp:TransitTag',tmp:TransitTag)
  ?List:5{PROP:IconList,1} = '~notick1.ico'
  ?List:5{PROP:IconList,2} = '~tick1.ico'
  BRW14.AddField(tmp:TransitTag,BRW14.Q.tmp:TransitTag)
  BRW14.AddField(trt:Transit_Type,BRW14.Q.trt:Transit_Type)
  FDCB13.Init(tmp:Status,?tmp:Status,Queue:FileDropCombo.ViewPosition,FDCB13::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB13.Q &= Queue:FileDropCombo
  FDCB13.AddSortOrder(sts:JobKey)
  FDCB13.AddRange(sts:Job,tmp:Yes)
  FDCB13.AddField(sts:Status,FDCB13.Q.sts:Status)
  FDCB13.AddField(sts:Ref_Number,FDCB13.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB13.WindowComponent)
  FDCB13.DefaultFill = 0
  BRW2.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW2.AskProcedure = 0
      CLEAR(BRW2.AskProcedure, 1)
    END
  END
  BRW4.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW4.AskProcedure = 0
      CLEAR(BRW4.AskProcedure, 1)
    END
  END
  BRW6.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW6.AskProcedure = 0
      CLEAR(BRW6.AskProcedure, 1)
    END
  END
  BRW11.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW11.AskProcedure = 0
      CLEAR(BRW11.AskProcedure, 1)
    END
  END
  BRW14.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW14.AskProcedure = 0
      CLEAR(BRW14.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  ?DASSHOWTAG:3{PROP:Text} = 'Show All'
  ?DASSHOWTAG:3{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:3{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  ?DASSHOWTAG:4{PROP:Text} = 'Show All'
  ?DASSHOWTAG:4{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:4{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  ?DASSHOWTAG:5{PROP:Text} = 'Show All'
  ?DASSHOWTAG:5{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:5{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue3)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue4)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue5)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:REPTYDEF.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard10.Validate()
        DISABLE(Wizard10.NextControl())
     ELSE
        ENABLE(Wizard10.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::3:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::15:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?VSBackButton
      ThisWindow.Update
         Wizard10.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard10.TakeAccepted()
    OF ?Finish
      ThisWindow.Update
      WorkshopReport(tmp:StartDate,tmp:EndDate,tmp:Status)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::3:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:3)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:4
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:4{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:4{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:4)
               ?List:4{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:5
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:5{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:5{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::15:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:5)
               ?List:5{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      DO DASBRW::15:DASUNTAGALL
      DO DASBRW::12:DASUNTAGALL
      DO DASBRW::7:DASUNTAGALL
      DO DASBRW::5:DASUNTAGALL
      DO DASBRW::3:DASUNTAGALL
      Select(?tmp:StartDate)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtd:Repair_Type
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:RepairTag = ''
    ELSE
      tmp:RepairTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Repairtag = '*')
    SELF.Q.tmp:RepairTag_Icon = 2
  ELSE
    SELF.Q.tmp:RepairTag_Icon = 1
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW2.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW2::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW2::RecordStatus=ReturnValue
  IF BRW2::RecordStatus NOT=Record:OK THEN RETURN BRW2::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = rtd:Repair_Type
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::3:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW2::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW2::RecordStatus
  RETURN ReturnValue


BRW4.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sts:Status
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:StatusTag = ''
    ELSE
      tmp:StatusTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Statustag = '*')
    SELF.Q.tmp:StatusTag_Icon = 2
  ELSE
    SELF.Q.tmp:StatusTag_Icon = 1
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW4.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW4::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW4::RecordStatus=ReturnValue
  IF BRW4::RecordStatus NOT=Record:OK THEN RETURN BRW4::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = sts:Status
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW4::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW4::RecordStatus
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = sts:Status
     GET(glo:Queue3,glo:Queue3.Pointer3)
    IF ERRORCODE()
      tmp:StatusTag2 = ''
    ELSE
      tmp:StatusTag2 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Statustag2 = '*')
    SELF.Q.tmp:StatusTag2_Icon = 2
  ELSE
    SELF.Q.tmp:StatusTag2_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue3.Pointer3 = sts:Status
     GET(glo:Queue3,glo:Queue3.Pointer3)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue


BRW11.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = sts:Status
     GET(glo:Queue4,glo:Queue4.Pointer4)
    IF ERRORCODE()
      tmp:StatusTag3 = ''
    ELSE
      tmp:StatusTag3 = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Statustag3 = '*')
    SELF.Q.tmp:StatusTag3_Icon = 2
  ELSE
    SELF.Q.tmp:StatusTag3_Icon = 1
  END


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW11.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW11::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW11::RecordStatus=ReturnValue
  IF BRW11::RecordStatus NOT=Record:OK THEN RETURN BRW11::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue4.Pointer4 = sts:Status
     GET(glo:Queue4,glo:Queue4.Pointer4)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW11::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW11::RecordStatus
  RETURN ReturnValue


BRW14.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = trt:Transit_Type
     GET(glo:Queue5,glo:Queue5.Pointer5)
    IF ERRORCODE()
      tmp:TransitTag = ''
    ELSE
      tmp:TransitTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:Transittag = '*')
    SELF.Q.tmp:TransitTag_Icon = 2
  ELSE
    SELF.Q.tmp:TransitTag_Icon = 1
  END


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW14::RecordStatus  BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW14::RecordStatus=ReturnValue
  IF BRW14::RecordStatus NOT=Record:OK THEN RETURN BRW14::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue5.Pointer5 = trt:Transit_Type
     GET(glo:Queue5,glo:Queue5.Pointer5)
    EXECUTE DASBRW::15:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW14::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW14::RecordStatus
  RETURN ReturnValue

Wizard10.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW2.Q &= NULL) ! Has Browse Object been initialized?
       BRW2.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW4.Q &= NULL) ! Has Browse Object been initialized?
       BRW4.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW6.Q &= NULL) ! Has Browse Object been initialized?
       BRW6.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW11.Q &= NULL) ! Has Browse Object been initialized?
       BRW11.ResetQueue(Reset:Queue)
    END
    IF NOT(BRW14.Q &= NULL) ! Has Browse Object been initialized?
       BRW14.ResetQueue(Reset:Queue)
    END

Wizard10.TakeBackEmbed PROCEDURE
   CODE

Wizard10.TakeNextEmbed PROCEDURE
   CODE

Wizard10.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
