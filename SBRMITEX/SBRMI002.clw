

   MEMBER('SBRMITEX.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBRMI002.INC'),ONCE        !Local module procedure declarations
                     END


MitsubishiExport PROCEDURE (func:StartDate,func:EndDate) !Generated from procedure template - Process

Progress:Thermometer BYTE
save_wpr_id          USHORT,AUTO
tmp:CustomerFault    STRING(255)
save_par_id          USHORT,AUTO
tmp:Fault            STRING(255)
tmp:DepartureDate    DATE
tmp:StartDate        DATE
tmp:EndDate          DATE
SheetDesc            CSTRING(41)
tmp:ExchangeIMEI     STRING(30)
tmp:ESN              STRING(30)
PartsGroup           GROUP,PRE()
tmp:PartNumber1      STRING(30)
tmp:Description1     STRING(30)
tmp:PurchaseCost1    REAL
tmp:PartNumber2      STRING(30)
tmp:Description2     STRING(30)
tmp:PurchaseCost2    REAL
tmp:PartNumber3      STRING(30)
tmp:Description3     STRING(30)
tmp:PurchaseCost3    REAL
tmp:PartNumber4      STRING(30)
tmp:Description4     STRING(30)
tmp:PurchaseCost4    REAL
tmp:PartNumber5      STRING(30)
tmp:Description5     STRING(30)
tmp:PurchaseCost5    REAL
tmp:PartNumber6      STRING(30)
tmp:Description6     STRING(30)
tmp:PurchaseCost6    REAL
tmp:PartNumber7      STRING(30)
tmp:Description7     STRING(30)
tmp:PurchaseCost7    REAL
tmp:PartNumber8      STRING(30)
tmp:Description8     STRING(30)
tmp:PurchaseCost8    REAL
tmp:PartNumber9      STRING(30)
tmp:Description9     STRING(30)
tmp:PurchaseCost9    REAL
tmp:PartNumber10     STRING(30)
tmp:Description10    STRING(30)
tmp:PurchaseCost10   REAL
                     END
Process:View         VIEW(JOBS)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,148,60),FONT('Arial',8,,),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,15,111,12),RANGE(0,100)
                       STRING(''),AT(4,4,140,10),USE(?Progress:UserString),CENTER
                       STRING(''),AT(4,30,140,10),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(44,40,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          CLASS(ProcessClass)              !Process Manager
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
                     END

ProgressMgr          StepRealClass                    !Progress Manager
?F1SS                EQUATE(1000)
!# SW2 Extension            STRING(3)
f1Action             BYTE
f1FileName           CSTRING(256)
RowNumber            USHORT
StartRow             LONG
StartCol             LONG
Template             CSTRING(129)
f1Disabled           BYTE(0)
ssFieldQ              QUEUE(tqField).
CQ                    QUEUE(tqField).
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!---------------------
ssInit         ROUTINE
!---------------------
  DO ssBuildQueue                                     ! Initialize f1 field queue
  IF NOT UsBrowse(SsFieldQ,'MitsubishiExport',,,,CQ,SheetDesc,StartRow,StartCol,Template)
    ThisWindow.Kill
    RETURN
  END

!---------------------
ssBuildQueue   ROUTINE
!---------------------
    ssFieldQ.Name = 'job:Ref_Number'
    ssFieldQ.Desc = 'Job Number'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Chr(39) & tmp:esn'
    ssFieldQ.Desc = 'Original IMEI'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Model_Number'
    ssFieldQ.Desc = 'Model'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Account_Number'
    ssFieldQ.Desc = 'Customer'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code1'
    ssFieldQ.Desc = 'Date Code'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code3'
    ssFieldQ.Desc = 'Assembly Version'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code8'
    ssFieldQ.Desc = 'CA Version'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:CustomerFault'
    ssFieldQ.Desc = 'Customer Fault'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Fault'
    ssFieldQ.Desc = 'Fault'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code4'
    ssFieldQ.Desc = 'Defect Code'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code5'
    ssFieldQ.Desc = 'Repair Code'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code6'
    ssFieldQ.Desc = 'TOPO'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code2'
    ssFieldQ.Desc = 'Repair Type'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(job:date_booked,@d17)'
    ssFieldQ.Desc = 'Arrival Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Format(tmp:DepartureDate,@d17)'
    ssFieldQ.Desc = 'Departure Date'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'job:Fault_Code7'
    ssFieldQ.Desc = 'Repair Level'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'Chr(39) & tmp:ExchangeIMEI'
    ssFieldQ.Desc = 'Exchange IMEI'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber1'
    ssFieldQ.Desc = 'Part Number 1'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description1'
    ssFieldQ.Desc = 'Description 1'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost1'
    ssFieldQ.Desc = 'Purchase Cost 1'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber2'
    ssFieldQ.Desc = 'Part Number 2'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description2'
    ssFieldQ.Desc = 'Description 2'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost2'
    ssFieldQ.Desc = 'Purchase Cost 2'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber3'
    ssFieldQ.Desc = 'Part Number 3'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description3'
    ssFieldQ.Desc = 'Description 3'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost3'
    ssFieldQ.Desc = 'Purchase Cost 3'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber4'
    ssFieldQ.Desc = 'Part Number 4'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description4'
    ssFieldQ.Desc = 'Description 4'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost4'
    ssFieldQ.Desc = 'Purchase Cost 4'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber5'
    ssFieldQ.Desc = 'Part Number 5'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description5'
    ssFieldQ.Desc = 'Description 5'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost5'
    ssFieldQ.Desc = 'Purchase Cost 5'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber6'
    ssFieldQ.Desc = 'Part Number 6'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description6'
    ssFieldQ.Desc = 'Description 6'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost6'
    ssFieldQ.Desc = 'Purchase Cost 6'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber7'
    ssFieldQ.Desc = 'Part Number 7'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description7'
    ssFieldQ.Desc = 'Description 7'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost7'
    ssFieldQ.Desc = 'Purchase Cost 7'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber8'
    ssFieldQ.Desc = 'Part Number 8'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description8'
    ssFieldQ.Desc = 'Description 8'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost8'
    ssFieldQ.Desc = 'Purchase Cost 8'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber9'
    ssFieldQ.Desc = 'Part Number 9'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description9'
    ssFieldQ.Desc = 'Description 9'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost9'
    ssFieldQ.Desc = 'Purchase Cost 9'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PartNumber10'
    ssFieldQ.Desc = 'Part Number 10'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:Description10'
    ssFieldQ.Desc = 'Description 10'
    ssFieldQ.IsString = TRUE
    ADD(ssFieldQ)
    ssFieldQ.Name = 'tmp:PurchaseCost10'
    ssFieldQ.Desc = 'Purchase Cost 10'
    ssFieldQ.IsString = FALSE
    ADD(ssFieldQ)
  SORT(ssFieldQ,+ssFieldQ.Desc)


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MitsubishiExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  tmp:StartDate   = func:StartDate
  tmp:EndDate     = func:EndDate
    BIND('tmp:CustomerFault',tmp:CustomerFault)
    BIND('tmp:DepartureDate',tmp:DepartureDate)
    BIND('tmp:Description1',tmp:Description1)
    BIND('tmp:Description10',tmp:Description10)
    BIND('tmp:Description2',tmp:Description2)
    BIND('tmp:Description3',tmp:Description3)
    BIND('tmp:Description4',tmp:Description4)
    BIND('tmp:Description5',tmp:Description5)
    BIND('tmp:Description6',tmp:Description6)
    BIND('tmp:Description7',tmp:Description7)
    BIND('tmp:Description8',tmp:Description8)
    BIND('tmp:Description9',tmp:Description9)
    BIND('tmp:ESN',tmp:ESN)
    BIND('tmp:ExchangeIMEI',tmp:ExchangeIMEI)
    BIND('tmp:Fault',tmp:Fault)
    BIND('tmp:PartNumber1',tmp:PartNumber1)
    BIND('tmp:PartNumber10',tmp:PartNumber10)
    BIND('tmp:PartNumber2',tmp:PartNumber2)
    BIND('tmp:PartNumber3',tmp:PartNumber3)
    BIND('tmp:PartNumber4',tmp:PartNumber4)
    BIND('tmp:PartNumber5',tmp:PartNumber5)
    BIND('tmp:PartNumber6',tmp:PartNumber6)
    BIND('tmp:PartNumber7',tmp:PartNumber7)
    BIND('tmp:PartNumber8',tmp:PartNumber8)
    BIND('tmp:PartNumber9',tmp:PartNumber9)
    BIND('tmp:PurchaseCost1',tmp:PurchaseCost1)
    BIND('tmp:PurchaseCost10',tmp:PurchaseCost10)
    BIND('tmp:PurchaseCost2',tmp:PurchaseCost2)
    BIND('tmp:PurchaseCost3',tmp:PurchaseCost3)
    BIND('tmp:PurchaseCost4',tmp:PurchaseCost4)
    BIND('tmp:PurchaseCost5',tmp:PurchaseCost5)
    BIND('tmp:PurchaseCost6',tmp:PurchaseCost6)
    BIND('tmp:PurchaseCost7',tmp:PurchaseCost7)
    BIND('tmp:PurchaseCost8',tmp:PurchaseCost8)
    BIND('tmp:PurchaseCost9',tmp:PurchaseCost9)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  DO ssInit
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Access:MANFAULO.UseFile
  Access:JOBTHIRD.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  IF ThisWindow.Response <> RequestCancelled
    UsInitDoc(CQ,?F1ss,StartRow,StartCol,Template)
    ?F1SS{'Sheets("Sheet1").Name'} = SheetDesc
    IF Template = ''
      RowNumber = StartRow + 1
    ELSE
      RowNumber = StartRow
    END
  END
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:date_booked)
  ThisProcess.AddSortOrder(job:Date_Booked_Key)
  ThisProcess.AddRange(job:date_booked,tmp:StartDate,tmp:EndDate)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}=''
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Progress:Cancel
                                                      ! SW2 This section changed
      IF Message('Would you like to view the partially created spreadsheet?','Process Cancelled',ICON:Question,Button:Yes+Button:No,Button:No)|
      = Button:Yes
        UsDeInit(f1FileName, ?F1SS, 1)
      END
      ReturnValue = Level:Fatal
      RETURN ReturnValue
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ThisWindow.Response = RequestCompleted
    f1FileName = 'Mitsubishi'
    f1Action = 2
  
    IF UsGetFileName( f1FileName, 1, f1Action )
      UsDeInit( f1FileName, ?F1SS, f1Action )
    END
    ?F1SS{'quit()'}
  ELSE
    IF RowNumber THEN ?F1SS{'quit()'}.
  END
  ReturnValue = PARENT.TakeCloseEvent()
  RETURN ReturnValue


ThisProcess.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !include Warranty Jobs Only
  If job:warranty_job <> 'YES'
      Return Level:User
  End!If job:warranty_job <> 'YES'
  If job:Manufacturer <> 'MITSUBISHI'
      Return Level:User
  End!If job:Manufacturer <> 'MITSUBISHI'
  
  !Get Notes
  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
  jbn:RefNumber = job:Ref_Number
  Access:JOBNOTES.TryFetch(jbn:RefNumberKey)
  
  tmp:ExchangeIMEI = ''
  IMEIError# = 0
  If job:Third_Party_Site <> ''
      Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
      jot:RefNumber = job:Ref_Number
      Set(jot:RefNumberKey,jot:RefNumberKey)
      If Access:JOBTHIRD.NEXT()
          IMEIError# = 1
      Else !If Access:JOBTHIRD.NEXT()
          If jot:RefNumber <> job:Ref_Number            
              IMEIError# = 1
          Else !If jot:RefNumber <> job:Ref_Number            
              IMEIError# = 0                
          End !If jot:RefNumber <> job:Ref_Number            
      End !If Access:JOBTHIRD.NEXT()
  Else !job:Third_Party_Site <> ''
      IMEIError# = 1    
  End !job:Third_Party_Site <> ''
  
  If IMEIError# = 1
      tmp:Esn = job:esn
  Else !IMEIError# = 1
      tmp:Esn = jot:OriginalIMEI
      tmp:ExchangeIMEI    = job:ESN
  End !IMEIError# = 1
  
  !Has it got an Exchange Unit?
  tmp:DepartureDate = job:Date_Completed
  If job:Exchange_unit_Number <> 0
      Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
      xch:Ref_Number = job:Exchange_Unit_Number
      If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
          tmp:ExchangeIMEI    = xch:ESN
          If job:Exchange_Despatched <> ''
              tmp:DepartureDate   = job:Exchange_Despatched
          End!If job:Exchange_Despatched <> ''
      End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
  End!If job:Exchange_unit_Number <> 0
  
  !Get Faults
  access:manfaulo.clearkey(mfo:field_key)
  mfo:manufacturer = 'MITSUBISHI'
  mfo:field_number = 9
  mfo:field        = job:fault_code9
  if access:manfaulo.tryfetch(mfo:field_key) = Level:Benign
      tmp:CustomerFault = mfo:description
  End!if access:manfpalo.tryfetch(mfp:field_key) = Level:Benign
  
  access:manfaulo.clearkey(mfo:field_key)
  mfo:manufacturer = 'MITSUBISHI'
  mfo:field_number = 10
  mfo:field        = job:fault_code10
  if access:manfaulo.tryfetch(mfo:field_key) = Level:Benign
      tmp:Fault = mfo:description
  End!if access:manfpalo.tryfetch(mfp:field_key) = Level:Benign
  
  Clear(PartsGroup)
  x# = 0
  Save_par_ID = Access:PARTS.SaveFile()
  Access:PARTS.ClearKey(par:Part_Number_Key)
  par:Ref_Number  = job:Ref_Number
  Set(par:Part_Number_Key,par:Part_Number_Key)
  Loop
      If Access:PARTS.NEXT()
         Break
      End !If
      If par:Ref_Number  <> job:Ref_Number      |
          Then Break.  ! End If
  
      x# += 1
      Case x#
           Of 1
              tmp:PartNumber1 = par:Part_Number
              tmp:Description1 = par:Description
              tmp:PurchaseCost1 = par:Purchase_Cost
  
           Of 2
              tmp:PartNumber2 = par:Part_Number
              tmp:Description2= par:Description
              tmp:PurchaseCost2 = par:Purchase_Cost
  
           Of 3
              tmp:PartNumber3 = par:Part_Number
              tmp:Description3 = par:Description
              tmp:PurchaseCost3 = par:Purchase_Cost
  
           Of 4
              tmp:PartNumber4 = par:Part_Number
              tmp:Description4 = par:Description
              tmp:PurchaseCost4 = par:Purchase_Cost
  
           Of 5
              tmp:PartNumber5 = par:Part_Number
              tmp:Description5 = par:Description
              tmp:PurchaseCost5 = par:Purchase_Cost
  
           Of 6
              tmp:PartNumber6 = par:Part_Number
              tmp:Description6 = par:Description
              tmp:PurchaseCost6 = par:Purchase_Cost
  
           Of 7
              tmp:PartNumber7 = par:Part_Number
              tmp:Description7 = par:Description
              tmp:PurchaseCost7 = par:Purchase_Cost
  
           Of 8
              tmp:PartNumber8 = par:Part_Number
              tmp:Description8 = par:Description
              tmp:PurchaseCost8 = par:Purchase_Cost
  
           Of 9
              tmp:PartNumber9 = par:Part_Number
              tmp:Description9 = par:Description
              tmp:PurchaseCost9 = par:Purchase_Cost
  
           Of 10
              tmp:PartNumber10 = par:Part_Number
              tmp:Description10 = par:Description
              tmp:PurchaseCost10 = par:Purchase_Cost
  
      End !Case x#
      If x# > 10
          Break
      End !If x# > 20.
  End !Loop
  Access:PARTS.RestoreFile(Save_par_ID)
  
  Save_wpr_ID = Access:WARPARTS.SaveFile()
  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
  wpr:Ref_Number  = job:Ref_Number
  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
  Loop
      If Access:WARPARTS.NEXT()
         Break
      End !If
      If wpr:Ref_Number  <> job:Ref_Number |
          Then Break.  ! End If
      x# += 1
      Case x#
           Of 1
              tmp:PartNumber1 = wpr:Part_Number
              tmp:Description1 = wpr:Description
              tmp:PurchaseCost1 = wpr:Purchase_Cost
  
           Of 2
              tmp:PartNumber2 = wpr:Part_Number
              tmp:Description2= wpr:Description
              tmp:PurchaseCost2 = wpr:Purchase_Cost
  
           Of 3
              tmp:PartNumber3 = wpr:Part_Number
              tmp:Description3 = wpr:Description
              tmp:PurchaseCost3 = wpr:Purchase_Cost
  
           Of 4
              tmp:PartNumber4 = wpr:Part_Number
              tmp:Description4 = wpr:Description
              tmp:PurchaseCost4 = wpr:Purchase_Cost
  
           Of 5
              tmp:PartNumber5 = wpr:Part_Number
              tmp:Description5 = wpr:Description
              tmp:PurchaseCost5 = wpr:Purchase_Cost
  
           Of 6
              tmp:PartNumber6 = wpr:Part_Number
              tmp:Description6 = wpr:Description
              tmp:PurchaseCost6 = wpr:Purchase_Cost
  
           Of 7
              tmp:PartNumber7 = wpr:Part_Number
              tmp:Description7 = wpr:Description
              tmp:PurchaseCost7 = wpr:Purchase_Cost
  
           Of 8
              tmp:PartNumber8 = wpr:Part_Number
              tmp:Description8 = wpr:Description
              tmp:PurchaseCost8 = wpr:Purchase_Cost
  
           Of 9
              tmp:PartNumber9 = wpr:Part_Number
              tmp:Description9 = wpr:Description
              tmp:PurchaseCost9 = wpr:Purchase_Cost
  
           Of 10
              tmp:PartNumber10 = wpr:Part_Number
              tmp:Description10 = wpr:Description
              tmp:PurchaseCost10 = wpr:Purchase_Cost
  
      End !Case x#
  End !Loop
  Access:WARPARTS.RestoreFile(Save_wpr_ID)
  ReturnValue = PARENT.TakeRecord()
  UsFillRow(CQ,?F1SS,RowNumber,StartCol)
  RETURN ReturnValue

