

   MEMBER('tswebimp.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('TSWEB003.INC'),ONCE        !Local module procedure declarations
                     END


Turnaround_Routine   PROCEDURE  (f_days,f_hours,f_end_days,f_end_hours) ! Declare Procedure
clock_temp           TIME
start_time_temp      TIME
  CODE
    Relate:Defaults.Open()

    Set(Defaults)
    access:defaults.next()
    f_end_days = Today()
    f_end_hours = Clock()
    x# = 0
    count# = 0
    If f_days <> 0
        Loop
            count# += 1
            day_number# = (Today() + count#) % 7
            If DEF:Include_Saturday <> 'YES'
                If day_number# = 6
                    f_end_days += 1
                    Cycle
                End
            End
            If DEF:Include_Sunday <> 'YES'
                If day_number# = 0
                    f_end_days += 1
                    Cycle
                End
            End
            f_end_days += 1
            x# += 1
            If x# >= f_days
                Break
            End!If x# >= sts:turnaround_days
        End!Loop
    End!If f_days <> ''
    If f_hours <> 0
        start_time_temp = Clock()
        new_day# = 0
        Loop x# = 1 To f_hours
            clock_temp = start_time_temp + (x# * 360000)
            If DEF:Start_Work_Hours <> '' And DEF:End_Work_Hours <> ''
                If clock_temp < DEF:Start_Work_Hours Or clock_temp > DEF:End_Work_Hours
                    f_end_hours = DEF:Start_Work_Hours
                    f_end_days += 1
                End!If clock_temp > def:start_work_hours And clock_temp < def:end_work_hours
            End!If def:start_work_hours <> '' And def:end_work_hours <> ''
            f_end_hours += 360000
        End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
    End!If f_hours <> ''

    Relate:Defaults.Close()
