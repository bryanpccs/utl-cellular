

   MEMBER('tswebimp.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('TSWEB006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('TSWEB005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB007.INC'),ONCE        !Req'd for module callout resolution
                     END


TransitToStoreDate   PROCEDURE  (strDirection, strAccountTo, scAccountID, strDate, strPath) ! Declare Procedure
connectionStr       string(260), static
iniFilePath         string(260)

dayOfWeek           string(10)
repairLimit         long

startDate   date
result      long
transitDaysToStore long
! SQL tables

SQLFile             file, driver('Scalable'), oem, owner(connectionStr), pre(sql), name('WebJob'), bindable, thread
Record                  record
RecordCount                 long, name('Ref_Number')
                        end
                    end
    map
FindNextFreeDate        procedure(*cstring strAccount, date currentDate), date
FindPreviousFreeDate    procedure(*cstring strAccount, date currentDate), date
    end
  CODE
    !
    ! Description : Get the number of transit days from the service centre to the store
    !

    if strDate <> '' and exists(clip(strPath) & '\ACCREG.DAT')
        ! Change the paths this way
        ACCREG{Prop:Name} = clip(strPath) & '\ACCREG.DAT'
        SIDACREG{Prop:Name} = clip(strPath) & '\SIDACREG.DAT'
        SIDREG{Prop:Name} = clip(strPath) & '\SIDREG.DAT'
        SIDDEF{Prop:Name} = clip(strPath) & '\SIDDEF.DAT'
        SIDSRVCN{Prop:Name} = clip(strPath) & '\SIDSRVCN.DAT'

        Access:AccReg.Open()
        Access:AccReg.UseFile()

        Access:SIDACREG.Open()
        Access:SIDACREG.UseFile()

        Access:SIDREG.Open()
        Access:SIDREG.UseFile()

        Access:SIDDEF.Open()
        Access:SIDDEF.UseFile()

        Access:SIDSRVCN.Open()
        Access:SIDSRVCN.UseFile()

        startDate = deformat(strDate, @d06b)

        iniFilePath = clip(strPath) & '\Webimp.ini'
        connectionStr = getini('Defaults', 'Connection', '', clip(iniFilePath))

        ! Days to store from service centre
        Access:AccReg.ClearKey(acg:AccountNoKey)
        acg:AccountNo = strAccountTo
        if Access:AccReg.Fetch(acg:AccountNoKey) = Level:Benign

            Access:SIDACREG.ClearKey(sar:SCAccRegIDKey)
            sar:SCAccountID = scAccountID
            sar:AccRegID = acg:AccRegID
            if Access:SIDACREG.Fetch(sar:SCAccRegIDKey) = Level:Benign

                if sar:TransitDaysException = true
                    transitDaysToStore = sar:TransitDaysToStore
                else
                    ! Lookup region by Service Centre
                    Access:SIDREG.ClearKey(srg:SCRegionNameKey)
                    srg:SCAccountID = scAccountID
                    srg:RegionName = acg:RegionName
                    if Access:SIDREG.Fetch(srg:SCRegionNameKey) = Level:Benign
                        transitDaysToStore = srg:TransitDaysToStore
                    end
                end
            end

        end

        if transitDaysToStore = 0
            if scAccountID = 0 ! Record might not exist, but we have a valid Service Centre
                ! Fetch unallocated model default
                set(SIDDEF, 0)
                if Access:SIDDEF.Next() = Level:Benign
                    transitDaysToStore = SID:UnallocTransitStore
                end
            end
            ! Cannot be zero
            if transitDaysToStore = 0 then transitDaysToStore = 1. ! Default
        end

        ! Calculate number of days in transit to store
        loop transitDaysToStore times
            if strDirection = '-'
                ! Previous date
                startDate -= 1
                ! Check date is working day
                startDate = FindPreviousFreeDate(strAccountTo, startDate)
            else
                ! Next date
                startDate += 1
                ! Check date is working day
                startDate = FindNextFreeDate(strAccountTo, startDate)
            end
        end ! loop

        ! Retail Repair Capacity Limit (TrackerBase 8305)
        open(SQLFile)

        loop
            SQLFile{prop:SQL} = 'SELECT COUNT(*) FROM WebJob Where CustCollectionDate = ''' & format(startDate, @d10-) & ''''

            next(SQLFile)
            if error() then break.

            dayOfWeek = GetDayOfWeek(startDate)

            repairLimit = 0

            Access:SIDSRVCN.ClearKey(srv:SCAccountIDKey)
            srv:SCAccountID = scAccountID
            if Access:SIDSRVCN.Fetch(srv:SCAccountIDKey) = Level:Benign
                case dayOfWeek
                    of 'SUN' ! Sunday is not applicable
                    of 'MON'
                        repairLimit = srv:MonRepairCapacity
                    of 'TUE'
                        repairLimit = srv:TueRepairCapacity
                    of 'WED'
                        repairLimit = srv:WedRepairCapacity
                    of 'THU'
                        repairLimit = srv:ThuRepairCapacity
                    of 'FRI'
                        repairLimit = srv:FriRepairCapacity
                    of 'SAT' ! Saturday should use Fridays repair limit
                        repairLimit = srv:FriRepairCapacity
                end ! case
            end

            if repairLimit = 0 then break.

            if sql:RecordCount >= repairLimit ! Also equal to because the current job has not yet been created
                startDate = FindNextFreeDate(strAccountTo, startDate + 1)
            else
                break
            end
        end

        close(SQLFile)


        strDate = format(startDate, @d06b)

        Access:AccReg.Close()
        Access:SIDACREG.Close()
        Access:SIDREG.Close()
        Access:SIDDEF.Close()
        Access:SIDSRVCN.Close()

        result = true
    end

    return result
FindNextFreeDate procedure(*cstring strAccount, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDate)
    !

strNext          cstring('+')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strNext, strAccount, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
FindPreviousFreeDate procedure(*cstring strAccount, date currentDate)
    !
    ! Description : Find the previous free date (Just a wrapper for FindFreeDate)
    !

strPrevious      cstring('-')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strPrevious, strAccount, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
