

   MEMBER('tswebimp.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('TSWEB002.INC'),ONCE        !Local module procedure declarations
                     END


RemoveSpaces         PROCEDURE  (strIn)               ! Declare Procedure
i long
j long
strOut string(255)
  CODE
    strOut = ''

    if clip(strIn) <> ''

        j = 1

        loop i = 1 to len(clip(strIn))
            if strIn[i] = ' ' then cycle.
            strOut[j] = strIn[i]
            j += 1
        end ! loop

    end

    return clip(strOut)
