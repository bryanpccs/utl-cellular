

   MEMBER('tswebimp.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TSWEB001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('TSWEB002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB009.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

JobCount             LONG
ConnectionStr        STRING(255),STATIC
window               WINDOW('ServiceBase - Web Jobs Import'),AT(,,174,26),FONT('Arial',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WEBJOB               FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(WEB),BINDABLE,THREAD
UK_Ref_Number            KEY(WEB:Ref_Number),NAME('UK_Ref_Number'),PRIMARY
EXPORTEDKEY              KEY(WEB:Exported), DUP
SBJOBNOKEY               KEY(WEB:SBJobNo), DUP
PROCESSIDKEY             KEY(WEB:ProcessID), DUP
RETSTOREBOOKKEY          KEY(WEB:RetStoreCode, WEB:BookDate), DUP
ACCOUNTNOSTATUSKEY       KEY(WEB:AccountNo, WEB:StatusChange), DUP
BOOKTYPEDATEKEY          KEY(WEB:BookingType, WEB:BookDate), DUP
TYPERETSTOREBOOKKEY      KEY(WEB:BookingType, WEB:RetStoreCode, WEB:BookDate), DUP
TYPEACCNOSTATUSKEY       KEY(WEB:BookingType, WEB:AccountNo, WEB:StatusChange), DUP
TYPERETURNBOOKKEY        KEY(WEB:BookingType, WEB:UnitReturned, WEB:BookDate), DUP
TYPERETURNDATEKEY        KEY(WEB:BookingType, WEB:UnitReturned, WEB:DateUnitReturned), DUP
ESNKEY                   KEY(WEB:ESN), DUP
CPIMEIKEY                KEY(WEB:CPIMEI, WEB:BookDate), DUP
SMSALERTNOKEY            KEY(WEB:SMSAlertNo), DUP
CUSTCOLLECTDATEKEY       KEY(WEB:CustCollectionDate), DUP
SENDSMSKEY               KEY(WEB:SMSType, WEB:SMSReq, WEB:SMSSent, WEB:Ref_Number), DUP
SENDEMAILKEY             KEY(WEB:SMSType, WEB:EmailReq, WEB:EmailSent, WEB:Ref_Number), DUP
MOBILENOKEY              KEY(WEB:Mobile_Number), DUP
BOOKDATEKEY              KEY(WEB:BookDate), DUP
CPALLOCBOOKKEY           KEY(WEB:CPAllocated, WEB:BookDate), DUP
CPONLYALLOCBOOKKEY       KEY(WEB:CPOnly, WEB:CPAllocated, WEB:BookDate), DUP
COMPCUSTCOLLDATEKEY      KEY(WEB:Completed, WEB:CustCollectionDate), DUP
Record                   RECORD,PRE()
Ref_Number                  LONG
BookTime                    TIME
BookDate                    DATE
ESN                         STRING(20)
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Telephone_Number            STRING(15)
PostCode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Mobile_Number               STRING(15)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Colour                      STRING(30)
Phone_Lock                  STRING(30)
PrimaryFault                STRING(255)
FreeTextFault               STRING(255)
Intermittent_Fault          BYTE
Battery                     BYTE
Charger                     BYTE
BatteryCover                BYTE
None                        BYTE
LoanESN                     STRING(20)
LoanMake                    STRING(30)
LoanModel                   STRING(30)
LoanBattery                 BYTE
LoanCharger                 BYTE
ExportTime                  TIME
ExportDate                  DATE
Exported                    BYTE
RepairType                  STRING(2)
DateOfPurchase              DATE
InsuranceRefNo              STRING(30)
AccountNo                   STRING(15)
BookedBy                    STRING(30)
SBJobNo                     LONG
CompanyNameCollect          STRING(30)
AddressL1Collect            STRING(30)
AddressL2Collect            STRING(30)
AddressL3Collect            STRING(30)
PostcodeCollect             STRING(10)
TelNoCollect                STRING(15)
CompanyNameDelivery         STRING(30)
AddressL1Delivery           STRING(30)
AddressL2Delivery           STRING(30)
AddressL3Delivery           STRING(30)
PostcodeDelivery            STRING(10)
TelNoDelivery               STRING(15)
UnitCondition               STRING(255)
StoreNotes                  STRING(255)
StatusChange                STRING(30)
ProcessID                   LONG
ContractPeriod              LONG
Reason                      STRING(255)
FaxNo                       STRING(15)
TitleCollect                STRING(4)
InitialCollect              STRING(1)
SurnameCollect              STRING(30)
TitleDelivery               STRING(4)
InitialDelivery             STRING(1)
SurnameDelivery             STRING(30)
EmailAddress                STRING(255)
RetStoreCode                STRING(15)
SMSReq                      LONG
SMSSent                     LONG
LoanBoxNumber               STRING(20)
BookingType                 STRING(1)
CollectionMadeToday         BYTE
UseCustCollectDate          BYTE
SysCollectionDate           DATE
CustCollectionDate          DATE
SMSType                     STRING(1)
CPOnly                      BYTE
CPIssueReason               STRING(30)
CPExplanation               STRING(255)
CPAllocated                 STRING(1)
CPIMEI                      STRING(20)
CPAccBattery                STRING(1)
CPAccCharger                STRING(1)
CPAccHeadset                STRING(1)
CPAccGuide                  STRING(1)
CPAccPackaging              STRING(1)
MemoryCard                  BYTE
LoanManual                  BYTE
UnitReturned                BYTE
DateUnitReturned            DATE
DeviceCondition             STRING(40)
CPReturnDate                DATE
CPDepositValue              PDECIMAL(8, 2)
CPDepositSKUCode            STRING(30)
CPDeposit                   STRING(1)
CPReturnDateReason          STRING(255)
EmailReq                    BYTE
ValueSegment                STRING(20)
Contract                    BYTE
Country                     STRING(20)
CountryCollect              STRING(20)
CountryDelivery             STRING(20)
StatusAgentName             STRING(30)
SMSAlertNo                  STRING(15)
EmailSent                   BYTE
Completed                   BYTE
                         END
                     END  

! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD, PRE(), NAME('Ref_Number')
UniqueID                    LONG, NAME('Ref_Number')
                        END
                    END

WebJobEx        file, driver('Scalable'), oem, owner(ConnectionStr), name('WEBJOBEX'), pre(webex), bindable, thread
Record              record
SCAccountID             long, name('SCAccountID')
SIDVersion              string(1), name('SIDVersion')
                    end
                end

WebAudStat      file, driver('Scalable'), oem, owner(ConnectionStr), name('WebAudStat'), pre(webas), bindable, thread
Record              record
EntryDate             date, name('EntryDate')
EntryTime             time, name('EntryTime')
StatusName            string(30), name('StatusName')
UserName              string(30), name('UserName')
                    end
                end
iniFilePath         string(255)
errorOccurred       byte

dName               string(30)
dAdd1               string(30)
dAdd2               string(30)
dAdd3               string(30)
dPostCode           string(10)
dTel                string(20)
inCourier           string(20)
outCourier          string(20)

tmpChargeType       string(30)
tmpRepairType       string(30)
strBookingType      string(40)
strToteCollected    string(40)

strTempDate         cstring(20)
strAccount          cstring(16)

strDirection        cstring(2)

strPath             cstring(260)

scAccountID         long


! Procedure Prototypes

    map
ImportJobs          procedure(), long, proc
    end
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFWEB.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:AUDSTATS.UseFile
  Access:CONTHIST.UseFile
  Access:AUDSTAEX.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  0{Prop:Hide} = true ! Hide the window
  
  iniFilePath =  clip(path()) & '\Webimp.ini'
  ConnectionStr = getini('Defaults', 'Connection', '', clip(iniFilePath))
  
  open(WEBJOB)
  !Message(WEBJOB{Prop:ConnectString})
  
  if (errorcode())
      if errorcode() = 90
          message('Error Opening WEBJOB : ' & fileerrorcode() & ' - ' & fileerror())
      else
          message('Error Opening WEBJOB : ' & error())
      end
      errorOccurred = true
      post(Event:CloseWindow)
  else
      ImportJobs()
  end
  
  post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFWEB.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

ImportJobs  procedure()

duplicateFound byte
oldStatus      like(aus:OldStatus)
sidVersion     string(1)
foundSBStatus  byte
    code

    ! Jobs to be exported
    open(SQLFile)
    if error() then return false.

    open(WebJobEx)
    open(WebAudStat)

    ! Exclude pending jobs, cancelled jobs
    SQLFile{Prop:SQL} = 'SELECT Ref_Number FROM WebJob WHERE Exported = 0 AND BookingType <> ''P'''

    loop
        next(SQLFile)
        if error() then break.

        !if web:exported <> 0 then break.
        web:Ref_Number = SQL:UniqueID
        get(WEBJOB, web:UK_Ref_Number)
        if error() then cycle.

        ! Exclude courtesy phone only, expired pending records
        if web:BookingType = 'C' or web:BookingType = 'A'
            web:ExportDate = today()
            web:ExportTime = clock()
            web:Exported = 1
            put(WEBJOB)
            cycle
        end

        ! Duplicate check
        duplicateFound = false

        Access:Jobs.ClearKey(job:AccOrdNoKey)
        job:account_number = clip(web:AccountNo)
        job:Order_Number   = clip(web:Ref_Number)
        set(job:AccOrdNoKey, job:AccOrdNoKey)
        loop until Access:Jobs.Next()
            if clip(job:account_number) <> clip(web:AccountNo) then break.
            if clip(job:Order_Number) <> clip(web:Ref_Number) then break.
            if job:date_booked <> web:BookDate then cycle.
            if job:time_booked <> web:BookTime then cycle.
            if clip(job:esn) <> clip(RemoveSpaces(web:ESN)) then cycle.
            duplicateFound = true
            break
        end ! loop

        if duplicateFound = true
            web:ExportDate = today()
            web:ExportTime = clock()
            web:Exported = 1
            web:SBJobNo  = job:ref_number
            put(WEBJOB)
            cycle
        end

        sidVersion = ''

        if web:BookingType = 'V' ! Resolved job
            WebJobEx{Prop:SQL} = 'SELECT SCAccountID, SIDVersion FROM WebJobEx WHERE WebJobNo = ' & web:Ref_Number
            next(WebJobEx)
            if not error()
                sidVersion = webex:SIDVersion ! Get whether the job was booked in contact centre or retail
            end
        end

        ! Create new job in ServiceBase
        if not Access:JOBS.PrimeRecord()
            job:EDI                         = 'XXX'
            job:account_number              = web:AccountNo
            job:who_booked                  = 'WEB'
            job:date_booked                 = web:BookDate
            job:time_booked                 = web:BookTime
            job:model_number                = web:Model_Number
            job:manufacturer                = web:Manufacturer
            job:esn                         = RemoveSpaces(web:ESN)
            job:colour                      = web:Colour
            job:phone_lock                  = web:Phone_Lock
            job:intermittent_fault          = web:Intermittent_Fault
            job:title                       = web:Title
            job:initial                     = web:Initial
            job:surname                     = web:Surname

            job:dop                         = web:DateOfPurchase
            job:Insurance_Reference_Number  = web:InsuranceRefNo

            if web:Intermittent_Fault = true
                job:Intermittent_Fault = 'YES'
            else
                job:Intermittent_Fault = 'NO'
            end

            job:company_name       = web:Company_Name
            job:address_line1      = web:Address_Line1
            job:address_line2      = web:Address_Line2
            job:address_line3      = web:Address_Line3
            job:postcode           = web:Postcode
            job:telephone_number   = web:Telephone_Number
            job:Fax_Number         = web:FaxNo

            job:mobile_number      = RemoveSpaces(web:Mobile_Number)

            dName           = ''
            dAdd1           = ''
            dAdd2           = ''
            dAdd3           = ''
            dPostCode       = ''
            dTel            = ''
            inCourier       = ''
            outCourier      = ''

            tmpChargeType   = ''
            tmpRepairType   = ''

            Access:SUBTRACC.ClearKey(sub:account_number_key)
            sub:account_number = job:account_number
            if not Access:SUBTRACC.Fetch(sub:account_number_key)
                Access:TRADEACC.ClearKey(tra:account_number_key)
                tra:account_number = SUB:Main_Account_Number
                if not Access:TRADEACC.Fetch(tra:account_number_key)
                    if tra:use_sub_accounts = 'YES'
                        dName           = SUB:Company_Name
                        dAdd1           = SUB:Address_Line1
                        dAdd2           = SUB:Address_Line2
                        dAdd3           = SUB:Address_Line3
                        dPostCode       = SUB:Postcode
                        dTel            = SUB:Telephone_Number
                        inCourier       = SUB:Courier_Incoming
                        outCourier      = SUB:Courier_Outgoing
                    else
                        dName           = TRA:Company_Name
                        dAdd1           = TRA:Address_Line1
                        dAdd2           = TRA:Address_Line2
                        dAdd3           = TRA:Address_Line3
                        dPostCode       = TRA:Postcode
                        dTel            = TRA:Telephone_Number
                        inCourier       = TRA:Courier_Incoming
                        outCourier      = TRA:Courier_Outgoing
                    end
                end
            end

            job:Incoming_Courier   = inCourier
            job:Incoming_Date      = web:BookDate
            job:Loan_Courier       = outCourier
            job:Exchange_Courier   = outCourier
            job:Courier            = outCourier
            job:Order_Number       = web:Ref_Number

            if sidVersion <> '' ! Resolved jobs have a processID of 50, override so the next part works
                case sidVersion
                    of 'R' ! Retail
                      web:ProcessID = 0
                    of 'C' ! Contact Centre
                      web:ProcessID = 3
                    of 'L' ! Contact Centre Lite
                      web:ProcessID = 3
                end
            end

            case web:ProcessID
                of 0 ! Retail
                orof 98 ! Retail - It is possible that the user may cancel/despatch the unit before it has been imported into ServiceBase
                orof 99 
                    ! Collection address
                    job:Company_Name_Collection   = dName
                    job:Address_Line1_Collection  = dAdd1
                    job:Address_Line2_Collection  = dAdd2
                    job:Address_Line3_Collection  = dAdd3
                    job:Postcode_Collection       = dPostCode
                    job:Telephone_Collection      = dTel

                    ! Delivery address (return address)
                    if (clip(web:CompanyNameDelivery) = '' and clip(web:PostcodeDelivery) = '' and clip(web:AddressL1Delivery) = '' and |
                       clip(web:AddressL2Delivery) = '' and clip(web:AddressL3Delivery) = '' and clip(web:TelNoDelivery) = '')
                        job:Company_Name_Delivery   =  dName
                        job:Postcode_Delivery       =  dPostCode
                        job:Address_Line1_Delivery  =  dAdd1
                        job:Address_Line2_Delivery  =  dAdd2
                        job:Address_Line3_Delivery  =  dAdd3
                        job:Telephone_Delivery      =  dTel
                    else
                        job:Company_Name_Delivery   =  web:CompanyNameDelivery
                        job:Postcode_Delivery       =  web:PostcodeDelivery
                        job:Address_Line1_Delivery  =  web:AddressL1Delivery
                        job:Address_Line2_Delivery  =  web:AddressL2Delivery
                        job:Address_Line3_Delivery  =  web:AddressL3Delivery
                        job:Telephone_Delivery      =  web:TelNoDelivery
                    end

                of 1 ! Call Centre - currently unused
                    ! Collection address
                    job:Company_Name_Collection   = web:CompanyNameCollect
                    job:Address_Line1_Collection  = web:AddressL1Collect
                    job:Address_Line2_Collection  = web:AddressL2Collect
                    job:Address_Line3_Collection  = web:AddressL3Collect
                    job:Postcode_Collection       = web:PostcodeCollect
                    job:Telephone_Collection      = web:TelNoCollect

                    ! Delivery address
                    job:Company_Name_Delivery   =  web:CompanyNameDelivery
                    job:Postcode_Delivery       =  web:PostcodeDelivery
                    job:Address_Line1_Delivery  =  web:AddressL1Delivery
                    job:Address_Line2_Delivery  =  web:AddressL2Delivery
                    job:Address_Line3_Delivery  =  web:AddressL3Delivery
                    job:Telephone_Delivery      =  web:TelNoDelivery

                    ! This section modifies the data if trader details say to use traders address.
                    Access:SUBTRACC.ClearKey(sub:account_number_key)
                    sub:account_number = job:account_number
                    if Access:SUBTRACC.Fetch(sub:account_number_key) = level:benign
                        Access:TRADEACC.ClearKey(tra:account_number_key)
                        tra:account_number = sub:Main_Account_Number
                        if access:tradeacc.fetch(tra:account_number_key) = level:benign
                            job:Estimate         = tra:Force_Estimate
                            job:Estimate_If_Over = tra:Estimate_If_Over
                            if TRA:Invoice_Sub_Accounts = 'YES'
                                if clip(upper(sub:Use_Customer_Address)) <> 'YES'
                                    job:company_name        = sub:company_name
                                    job:address_line1       = sub:address_line1
                                    job:address_line2       = sub:address_line2
                                    job:address_line3       = sub:address_line3
                                    job:Postcode            = sub:postcode
                                    job:telephone_number    = sub:telephone_number
                                    job:fax_number          = sub:fax_number
                                end

                                if sub:Use_Collection_Address = 'YES'
                                    job:Company_Name_Collection   = sub:company_name
                                    job:Address_Line1_Collection  = sub:address_line1
                                    job:Address_Line2_Collection  = sub:address_line2
                                    job:Address_Line3_Collection  = sub:address_line3
                                    job:Postcode_Collection       = sub:postcode
                                    job:Telephone_Collection      = sub:telephone_number
                                end !if sub use collection address

                                If sub:Use_Delivery_Address = 'YES'
                                    job:Company_Name_Delivery   = sub:company_name
                                    job:address_line1_Delivery  = sub:address_line1
                                    job:address_line2_Delivery  = sub:address_line2
                                    job:address_line3_Delivery  = sub:address_line3
                                    job:Postcode_Delivery       = sub:postcode
                                    job:Telephone_Delivery      = sub:telephone_number
                                end !If sub:use_delivery_address = 'YES'
                            else!if tra:use_sub_accounts = 'YES'
                                if clip(upper(tra:Use_Customer_Address)) <> 'YES'
                                    job:company_name        = tra:company_name
                                    job:Address_Line1       = tra:address_line1
                                    job:address_line2       = tra:address_line2
                                    job:address_line3       = tra:address_line3
                                    job:Postcode            = tra:postcode
                                    job:telephone_number    = tra:telephone_number
                                    job:fax_number          = tra:fax_number
                                end

                                if upper(sub(TRA:Use_Delivery_Address,1,1)) = 'Y'
                                    job:Company_Name_Delivery   = tra:company_name
                                    job:Address_Line1_Delivery  = tra:address_line1
                                    job:address_line2_Delivery  = tra:address_line2
                                    job:address_line3_Delivery  = tra:address_line3
                                    job:Postcode_Delivery       = tra:postcode
                                    job:Telephone_Delivery      = tra:Telephone_Number
                                end !If tra:use_delivery_address
                            end!if tra:use_sub_accounts = 'YES'
                        end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                    end!if access:subtracc.fetch(sub:account_number_key) = level:benign

                of 2 ! Contact Centre - Exchange Only (Remove this!, left here for compatibility with old version)

                    ! Collection address
                    job:Company_Name_Collection   = job:company_name
                    job:Address_Line1_Collection  = job:address_line1
                    job:Address_Line2_Collection  = job:address_line2
                    job:Address_Line3_Collection  = job:address_line3
                    job:Postcode_Collection       = job:postcode
                    job:Telephone_Collection      = job:telephone_number

                    ! Delivery address (Return address)
                    job:Company_Name_Delivery   =  job:company_name
                    job:Address_Line1_Delivery  =  job:address_line1
                    job:Address_Line2_Delivery  =  job:address_line2
                    job:Address_Line3_Delivery  =  job:address_line3
                    job:Postcode_Delivery       =  job:postcode
                    job:Telephone_Delivery      =  job:telephone_number

                of 3 ! Contact Centre - Exchange/Back To Base Repair Jobs

                    ! Collection address
                    job:Company_Name_Collection   = web:CompanyNameCollect
                    job:Address_Line1_Collection  = web:AddressL1Collect
                    job:Address_Line2_Collection  = web:AddressL2Collect
                    job:Address_Line3_Collection  = web:AddressL3Collect
                    job:Postcode_Collection       = web:PostcodeCollect
                    job:Telephone_Collection      = web:TelNoCollect

                    ! Delivery address (Return address)
                    job:Company_Name_Delivery   =  web:CompanyNameDelivery
                    job:Postcode_Delivery       =  web:PostcodeDelivery
                    job:Address_Line1_Delivery  =  web:AddressL1Delivery
                    job:Address_Line2_Delivery  =  web:AddressL2Delivery
                    job:Address_Line3_Delivery  =  web:AddressL3Delivery
                    job:Telephone_Delivery      =  web:TelNoDelivery

            end ! case

            ! Charge type
            Access:DEFWEB.ClearKey(dew:account_number_key)
            dew:account_number = job:account_number
            if not Access:DEFWEB.Fetch(dew:account_number_key)
                case web:RepairType
                    of 'IW' ! In Warranty
                        job:warranty_job = 'YES'
                        job:Warranty_Charge_Type = 'WARRANTY'
                        job:web_type    = 'WAR'
                        tmpChargeType = job:Warranty_Charge_Type
                        tmpRepairType = 'WARRANTY'
                    of 'IV' ! In Vodafone Warranty
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = 'IN VODAFONE WARRANTY'
                        job:web_type    = 'CHA'
                        tmpChargeType = JOB:Charge_Type
                        tmpRepairType = 'IN VODAFONE WARRANTY'
                    of 'ON' ! Out of warranty (Not chargeable)
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = 'OOW BUT NOT CHARGEABLE'
                        job:web_type    = 'CHA'
                        tmpChargeType = JOB:Charge_Type
                        tmpRepairType = 'OOW BUT NOT CHARGEABLE'
                    of 'IR' ! Insurance
                        job:Charge_Type = DEW:Insurance_Charge_Type
                        job:web_type    = 'INS'
                        job:chargeable_job = 'YES'
                        tmpChargeType = job:Charge_Type
                        tmpRepairType = 'INSURANCE'
                    of 'OC' ! Out Of Warranty (Chargeable)
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = DEW:Chargeable_Charge_Type
                        job:web_type    = 'CHA'
                        tmpChargeType = job:Charge_Type
                        tmpRepairType = 'CHARGEABLE'
                end ! case

                Access:TRANTYPE.ClearKey(TRT:Transit_Type_Key)

                ! Delivery Transit Type - dew:Transit_Type
                ! Collection Transit Type - dew:Job_Priority
                ! Exchange Transit Type - dew:Status_Type

                case web:BookingType
                    of 'B' ! Back To Base Repair
                        TRT:Transit_Type = DEW:Job_Priority
                    of 'E' ! Exchange
                    orof 'U' ! 7 Day Exchange
                        TRT:Transit_Type = DEW:Status_Type
                    of 'V' ! Resolved
                        TRT:Transit_Type = ''
                    else ! Retail
                        TRT:Transit_Type = DEW:Transit_Type
                end ! case

                Access:TRANTYPE.Fetch(TRT:Transit_Type_Key)

                job:unit_Type       = DEW:HandSet
                ! JOB:Current_Status  = TRT:Initial_Status
                case web:BookingType
                    of 'B'
                    orof 'G'
                        JOB:Current_Status = '136 AWAITING ENVELOPE DESPATCH'
                    of 'E'
                    orof 'F'
                        JOB:Current_Status = '111 SID JOB AWAITING PICK'
                    of 'Y'
                        JOB:Current_Status = '112 IN-STORE EXCHANGE REQUEST'
                    of 'Z'
                        JOB:Current_Status = '113 EXTERNAL EXCHANGE REQUEST'
                    of 'V'
                        JOB:Current_Status = clip(upper(web:StatusChange))
                    of 'W'
                        JOB:Current_Status = '120 - 7 DAY REPLACEMENT'
                    of 'Q'
                    orof 'X'
                    orof 'S'
                    orof 'T'
                    orof 'H'
                    orof 'J'
                    orof 'D'
                    orof 'I'
                    orof 'O'
                        JOB:Current_Status = '121 - RETURN JOB BOOKED'
                    of 'U'
                        JOB:Current_Status = '119 7 DAY EXCHANGE'
                    else
                        JOB:Current_Status = '114 AWAITING DESPATCH'
                end ! case

                JOB:Exchange_Status = TRT:ExchangeStatus
                JOB:Loan_Status     = TRT:LoanStatus

                job:location        = DEW:location
                job:transit_Type    = TRT:Transit_Type
                job:turnaround_time = TRT:Initial_Priority

                Access:TURNARND.ClearKey(TUR:Turnaround_Time_Key)
                tur:turnaround_time = job:turnaround_time
                if not Access:TURNARND.Fetch(TUR:Turnaround_Time_Key)
                    Turnaround_Routine(tur:days, tur:hours, end_date", end_time")
                    JOB:Turnaround_End_Date = end_date"
                    JOB:Turnaround_End_Time = end_time"
                end
                    
                JOB:Status_End_Date = 0 
                JOB:Status_End_Time = 0
            end

            if Access:JOBS.TryUpdate()
                Access:JOBS.CancelAutoInc()
            else
                if web:Battery <> 0
                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'BATTERY'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end
                if web:Charger <> 0
                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'CHARGER'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end
                if web:BatteryCover <> 0
                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'BATTERY COVER'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end

                if web:MemoryCard <> 0
                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'MEMORY CARD'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end

                if web:Battery = 0 and web:Charger = 0 and web:BatteryCover = 0 and web:MemoryCard = 0

                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'NONE'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end

                if not Access:JOBNOTES.PrimeRecord()
                    jbn:refnumber = job:ref_number

                    jbn:ColContatName   = clip(web:TitleCollect)
                    if clip(web:InitialCollect) <> ''
                        if clip(jbn:ColContatName) <> ''
                            jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:InitialCollect
                        else
                            jbn:ColContatName = web:InitialCollect
                        end
                    end
                    if clip(web:SurnameCollect) <> ''
                        if clip(jbn:ColContatName) <> ''
                            jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:SurnameCollect
                        else
                            jbn:ColContatName = web:SurnameCollect
                        end
                    end

                    jbn:DelContactName  = clip(web:TitleDelivery)
                    if clip(web:InitialDelivery) <> ''
                        if clip(jbn:DelContactName) <> ''
                            jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:InitialDelivery
                        else
                            jbn:DelContactName = web:InitialDelivery
                        end
                    end
                    if clip(web:SurnameDelivery) <> ''
                        if clip(jbn:DelContactName) <> ''
                            jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:SurnameDelivery
                        else
                            jbn:DelContactName = web:SurnameDelivery
                        end
                    end

                    !IF CLIP(UPPER(web:PrimaryFault)) <> '25 OTHER - SEE COMMENTS'
                    jbn:Fault_Description = web:PrimaryFault
                    !END

                    if job:Intermittent_Fault = 'YES'
                        if jbn:Fault_Description = ''
                            jbn:Fault_Description = '(INTERMITTENT FAULT)'
                        else
                            jbn:Fault_Description = clip(jbn:Fault_Description) & ' (INTERMITTENT FAULT)'
                        end
                    end

                    !jbn:Engineers_Notes = web:FreeTextFault
                    ! Change o' spec 29 July 2004
                    if clip(web:FreeTextFault) <> ''
                        if jbn:Fault_Description = ''
                            jbn:Fault_Description = clip(web:FreeTextFault)
                        else
                            jbn:Fault_Description = clip(jbn:Fault_Description) & ' ' & clip(web:FreeTextFault)
                        end
                    end

                    if Access:JOBNOTES.TryInsert()
                        Access:JOBNOTES.CancelAutoInc()
                    end
                end

                case web:BookingType
                    of 'B'
                        strBookingType = 'CONTACT CENTRE POSTAL REPAIR'
                    of 'E'
                        strBookingType = 'CONTACT CENTRE EXCHANGE'
                    of 'G'
                        strBookingType = 'CONTACT CENTRE EBU POSTAL REPAIR'
                    of 'F'
                        strBookingType = 'CONTACT CENTRE EBU EXCHANGE'
                    of 'Y'
                        strBookingType = 'SID RETAIL IN STORE EXCHANGE'
                    of 'Z'
                        strBookingType = 'SID RETAIL EXTERNAL EXCHANGE'
                    of 'V'
                        if sidVersion = 'R'
                            strBookingType = 'SID RETAIL RESOLVED'
                        else
                            strBookingType = 'CONTACT CENTRE RESOLVED'
                        end
                    of 'W'
                        strBookingType = 'SID RETAIL 7 DAY REPLACEMENT'
                    of 'Q'
                        strBookingType = 'SID RETAIL CUSTOMER RETURN'
                    of 'S'
                        strBookingType = 'SID RETAIL STORE RETURN'
                    of 'T'
                        strBookingType = 'SID RETAIL STORE RECALL'
                    of 'X'
                        strBookingType = 'SID RETAIL FEAST RETURN'
                    of 'O'
                        strBookingType = 'SID RETAIL OTHER RETURN'
                    of 'H'
                        strBookingType = 'CONTACT CENTRE HARDWARE RETURN'
                    of 'J'
                        strBookingType = 'CONTACT CENTRE EMPLOYEE RETURN'
                    of 'D'
                        strBookingType = 'CONTACT CENTRE PACKAGE'
                    of 'I'
                        strBookingType = 'CONTACT CENTRE INELIGIBLE'
                    of 'U'
                        strBookingType = 'CONTACT CENTRE 7 DAY EXCHANGE'
                    else
                        strBookingType = 'SID RETAIL REPAIR' ! Default to 'R'
                end ! case

                if web:CollectionMadeToday = 0
                    strToteCollected = 'TOTE COLLECTED: NO'
                else
                    if web:CollectionMadeToday = 1
                        strToteCollected = 'TOTE COLLECTED: YES'
                    else
                        strToteCollected = 'TOTE COLLECTED: NO - NOT AVAILABLE'
                    end
                end

                if not Access:AUDIT.PrimeRecord()
                    aud:notes         = ('JOB CREATED BY WEBMASTER' & '<13,10>'|
                                        & 'FOR: ' & clip(job:Account_Number)  & '<13,10>' |
                                        & clip(strBookingType) & '<13,10>' |
                                        & 'CHARGE TYPE: ' & clip(tmpChargeType) & '<13,10>'|
                                        & 'BOOKED BY: ' & clip(web:BookedBy) & '<13,10>'||
                                        & 'UNIT DETAILS: ' & clip(job:Manufacturer) & ' ' & clip(job:Model_Number) & ' ' & clip(job:Unit_Type) & '<13,10>'|
                                        & 'E.S.N./I.M.E.I: ' & clip(job:ESN)& '<13,10>'|
                                        & 'CONDITION OF DEVICE: ' & clip(web:DeviceCondition) & ', ' & clip(web:UnitCondition)& '<13,10>'|
                                        & clip(strToteCollected))

                                        !& 'AS: ' & clip(tmpRepairType) & '<13,10>'|

                                        !& 'M.S.N.: '&CLIP(job:MSN)& '<13,10>'| Was after IMEI
                                        !& 'CHARGEABLE JOB: ' & CLIP(job:Chargeable_Job)&'<13,10>'|
                                        !& 'ESTIMATE: ' & CLIP(job:Estimate) & '  VALUE: '& CLIP(job:Estimate_If_Over)&'<13,10>'|
                                        !& 'END USER: ' & clip(job:Title) & clip(job:Surname) & ' - ' &clip(job:telephone_number))

                    aud:User          = 'WEB'
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    aud:action        = 'NEW JOB INITIAL BOOKING'
                    if Access:AUDIT.TryInsert()
                        Access:AUDIT.CancelAutoInc()
                    end
                end

                ! GK 21/11/2007 - Add an inital status audit trail entry so it gets processed by alert notifications
                if Access:AUDIT.PrimeRecord() = Level:Benign
                    aud:notes   = 'INITIAL STATUS: ' & clip(job:current_status)
                    aud:ref_number  = job:ref_number
                    aud:date        = today()
                    aud:time        = clock() + 100
                    aud:user        = 'WEB'
                    aud:action      = 'STATUS CHANGED TO: ' & clip(job:current_status)
                    if Access:AUDIT.TryInsert()
                        Access:AUDIT.CancelAutoInc()
                    end
                end

                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:ref_number
                if Access:JOBSE.Fetch(jobe:RefNumberKey)
                    if not Access:JOBSE.PrimeRecord()
                        jobe:RefNumber = job:Ref_Number
                        jobe:EndUserEmailAddress = web:EmailAddress
                        jobe:TraFaultCode4 = web:BookedBy
                        jobe:TraFaultCode5 = web:SMSAlertNo

                        if web:UseCustCollectDate = false
                            jobe:CustomerCollectionDate = web:SysCollectionDate
                        else
                            jobe:CustomerCollectionDate = web:CustCollectionDate
                        end

                        ! Calculate the estimated despatch date from the service centre
                        strAccount = job:Account_Number
                        ! Get the returning store account number
                        if job:Company_Name_Delivery <> ''
                            Access:SUBTRACC.ClearKey(sub:Company_Name_Key)
                            sub:Company_Name = job:Company_Name_Delivery
                            if not Access:SUBTRACC.Fetch(sub:Company_Name_Key)
                                strAccount = sub:Account_Number
                            end
                        end

                        ! Work backwards
                        if jobe:CustomerCollectionDate <> '' ! Empty date hangs the exe!!
                            ! Get the Service Centre
                            scAccountID = 0
                            WebJobEx{Prop:SQL} = 'SELECT SCAccountID, SIDVersion FROM WebJobEx WHERE WebJobNo = ' & web:Ref_Number
                            next(WebJobEx)
                            if not error()
                                scAccountID = webex:SCAccountID
                            end
                            strPath = clip(path()) & '\'
                            strDirection = '-'
                            strTempDate = format(jobe:CustomerCollectionDate, @d06b)
                            TransitToStoreDate(strDirection, strAccount, scAccountID, strTempDate, strPath)
                            FindFreeDateATSC(strDirection, scAccountID, strTempDate, strPath) ! Check for bank holiday (changed regions)
                            jobe:EstimatedDespatchDate = deformat(strTempDate, @d06b)
                        end

                        if Access:JOBSE.TryInsert()
                            Access:JOBSE.CancelAutoInc()
                        end
                    end
                end

                ! Contact history
                if clip(web:StoreNotes) <> ''
                    if not Access:ContHist.PrimeRecord()
                        cht:Ref_Number = job:ref_number
                        cht:Date = today()
                        cht:Time = clock()
                        cht:User = 'SID'
                        cht:Action = 'SID CUSTOMER NOTES'
                        cht:Notes = 'NOTE ENTERED BY: ' & web:BookedBy & '<13,10>' & web:StoreNotes
                        if Access:ContHist.TryInsert()
                            Access:ContHist.CancelAutoInc()
                        end
                    end
                end

                web:ExportDate = today()
                web:ExportTime = clock()
                web:Exported = 1
                web:SBJobNo  = job:ref_number
                put(WEBJOB)

                !if web:BookingType = 'E' ! Exchange Contact Centre Version Only
                !    ! Print job card as wm2vjob.exe , see log 2557 in TrackerBase
                !    if exists(clip(path()) & '\wm2vprnt.exe') ! wm2vprnt.exe renamed
                !        run(clip(path()) & '\wm2vprnt.exe '& job:ref_number)
                !    end
                !end

                clear(webas:Record)

                foundSBStatus = false
                oldStatus = '101 SID BOOKING INITIATED' ! GK 30/05/2008 - All jobs should have this initial entry
                WebAudStat{Prop:SQL} = 'SELECT EntryDate, EntryTime, StatusName, UserName FROM WebAudStat WHERE SIDJobNo = ' & web:Ref_Number & ' ORDER BY EntryDate ASC, EntryTime ASC'
                loop
                    next(WebAudStat)
                    if error() then break.

                    if webas:StatusName = '135 DESPATCHED FROM STORE' or |
                       webas:StatusName = '137 ORIGINAL DEVICE DESPATCHED'  or |
                       webas:StatusName = '798 JOB CANCELLED BY VODAFONE'
                       foundSBStatus = true
                       cycle
                    end
                    !if oldStatus = ''
                        ! 24/01/2008 GK - We need the initial pending status date/time recorded
                        ! A new system status was added to allow this (101)
                        ! oldStatus = '101 SID BOOKING INITIATED'
                        ! oldStatus = clip(webas:StatusName)
                        ! cycle
                    !end

                    if Access:AUDSTATS.PrimeRecord() = Level:Benign
                        aus:RefNumber    = job:Ref_Number
                        aus:Type         = 'JOB'
                        aus:DateChanged  = webas:EntryDate
                        aus:TimeChanged  = webas:EntryTime
                        aus:OldStatus    = oldStatus
                        aus:NewStatus    = webas:StatusName
                        aus:UserCode     = 'WEB'
                        if Access:AUDSTATS.TryInsert() <> Level:Benign
                            Access:AUDSTATS.CancelAutoInc()
                        else
                            oldStatus = webas:StatusName
                            if Access:AUDSTAEX.PrimeRecord() = Level:Benign
                                aux:RefNumber = aus:RecordNumber
                                aux:AgentName = webas:UserName
                                if Access:AUDSTAEX.TryInsert() <> Level:Benign
                                    Access:AUDSTAEX.CancelAutoInc()
                                end
                            end
                        end
                    end
                end ! loop

                if (oldStatus <> '') and (clip(oldStatus) <> clip(job:Current_Status))
                    if Access:AUDSTATS.PrimeRecord() = Level:Benign
                        aus:RefNumber    = job:Ref_Number
                        aus:Type         = 'JOB'
                        aus:DateChanged  = job:date_booked
                        aus:TimeChanged  = job:time_booked
                        aus:OldStatus    = oldStatus
                        aus:NewStatus    = job:Current_Status
                        aus:UserCode     = 'WEB'
                        if Access:AUDSTATS.TryInsert() <> Level:Benign
                            Access:AUDSTATS.CancelAutoInc()
                        else
                            oldStatus = webas:StatusName
                            if Access:AUDSTAEX.PrimeRecord() = Level:Benign
                                aux:RefNumber = aus:RecordNumber
                                if clip(webas:UserName) = ''
                                    aux:AgentName = clip(web:StatusAgentName)
                                    web:StatusAgentName = ''
                                    put(WEBJOB)
                                else
                                    aux:AgentName = webas:UserName
                                end
                                if Access:AUDSTAEX.TryInsert() <> Level:Benign
                                    Access:AUDSTAEX.CancelAutoInc()
                                end
                            end
                        end
                    end
                end

                if foundSBStatus
                    WebAudStat{Prop:SQL} = 'SELECT EntryDate, EntryTime, StatusName, UserName FROM WebAudStat WHERE SIDJobNo = ' & web:Ref_Number & ' ORDER BY EntryDate ASC, EntryTime ASC'
                    loop
                        next(WebAudStat)
                        if error() then break.

                        ! Once a SID job has been booked it appears in the despatch list straight away. It is possible if the scheduled task has been disabled or
                        ! is running slow that the user attempts to despatch or cancel the job from the despatch list. This action causes a status change in
                        ! ServiceBase which will fail as this scheduled task has not yet created the ServiceBase job. In this instance, if no ServiceBase job
                        ! exists the system will write the status change to the WebAudStat table. Therefore we change the status here now that the job exists.
                        if webas:StatusName = '135 DESPATCHED FROM STORE' or |
                           webas:StatusName = '137 ORIGINAL DEVICE DESPATCHED'  or |
                           webas:StatusName = '798 JOB CANCELLED BY VODAFONE'
                            GetStatus(webas:StatusName[1:3], 0, 'JOB', webas:UserName, webas:EntryDate, webas:EntryTime)
                            Access:JOBS.TryUpdate()
                        end
                    end
                end

            end
        end

    end ! loop

    close(WebJobEx)
    close(WebAudStat)
    close(SQLFile)

    return true
