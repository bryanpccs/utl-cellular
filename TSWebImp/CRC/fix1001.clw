

   MEMBER('fix1.clw')                                 ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FIX1001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

foundAudit           BYTE
window               WINDOW('ServiceBase - Web Jobs Import'),AT(,,174,26),FONT('Arial',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  ! Add missing audit trail entries
  
  Access:JOBS.ClearKey(job:Date_Booked_Key)
  job:date_booked = today() - 10
  set(job:Date_Booked_Key, job:Date_Booked_Key)
  loop
      if Access:JOBS.Next() <> Level:Benign then break.
      if job:date_booked >= today() then break.
  
      foundAudit = false
  
      Access:AUDIT.ClearKey(aud:Ref_Number_Key)
      aud:Ref_Number = job:Ref_Number
      set(aud:Ref_Number_Key, aud:Ref_Number_Key)
      loop
          if Access:AUDIT.Next() <> Level:Benign then break.
          if aud:Ref_Number <> job:Ref_Number then break.
          foundAudit = true
          break
      end
  
      if foundAudit then cycle.
  
      if Access:AUDIT.PrimeRecord() = Level:Benign
          aud:notes         = ('JOB CREATED BY WEBMASTER' & '<13,10>'|
                            & 'FOR: ' & clip(job:Account_Number)  & '<13,10>' |
                            & 'SID ' & '<13,10>' |
                            & 'CHARGE TYPE: ' & '<13,10>'|
                            & 'BOOKED BY: ' & '<13,10>'||
                            & 'UNIT DETAILS: ' & clip(job:Manufacturer) & ' ' & clip(job:Model_Number) & ' ' & clip(job:Unit_Type) & '<13,10>'|
                            & 'E.S.N./I.M.E.I: ' & clip(job:ESN) & '<13,10>'|
                            & 'CONDITION OF DEVICE: ' & ' ' & ', ' & '<13,10>'|
                            & 'TOTE COLLECTED: NO')
  
          aud:User          = 'WEB'
          aud:ref_number    = job:Ref_Number
          aud:date          = today()
          aud:time          = clock()
          aud:action        = 'NEW JOB INITIAL BOOKING'
          if Access:AUDIT.TryInsert() <> Level:Benign
              Access:AUDIT.CancelAutoInc()
          else
              LinePrint('Added audit record for job number: ' & job:Ref_Number, 'C:\SbAudit.log')
          end
      end
  
  end
  
  message('Complete')
  
  post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

