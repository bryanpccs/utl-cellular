

   MEMBER('fix3.clw')                                 ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FIX3001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
window               WINDOW('Job Notes Fix'),AT(,,185,92),FONT('Arial',8,,FONT:regular,CHARSET:ANSI),GRAY,DOUBLE
                       BUTTON('Fix'),AT(100,72,35,14),USE(?OkButton),DEFAULT
                       BUTTON('Cancel'),AT(140,72,36,14),USE(?CancelButton),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WebJobFile File,Driver('Scalable'),OEM,Owner(ConnectionStr),Pre(web),Name('WebJob'),Bindable,Thread
Record          Record
TitleCollect          string(4), name('TitleCollect')
InitialCollect        string(1), name('InitialCollect')
SurnameCollect        string(30), name('SurnameCollect')
TitleDelivery         string(4), name('TitleDelivery')
InitialDelivery       string(1), name('InitialDelivery')
SurnameDelivery       string(30), name('SurnameDelivery')
PrimaryFault          string(255), name('PrimaryFault')
FreeTextFault         string(255), name('FreeTextFault')
                End
             End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

ProcessJobs routine

data

jobNo long

code

    IniFilepath = CLIP(PATH()) & '\Webimp.ini'
    ConnectionStr = GETINI('Defaults', 'Connection', '', CLIP(IniFilepath))

    open(WebJobFile)

    jobNo = 5093134

    loop
        if jobNo > 5093444 then break.

        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = jobNo
        if Access:JOBS.Fetch(job:Ref_Number_Key) <> Level:Benign
            message('Jobs - SB JOB NO:' & clip(jobNo) & ' ' & error())
            break
        end

        WebJobFile{Prop:SQL} = 'SELECT TitleCollect, InitialCollect, SurnameCollect, TitleDelivery, InitialDelivery, SurnameDelivery, PrimaryFault, FreeTextFault FROM WebJob WHERE Ref_Number = ' & clip(job:Order_Number)
        next(WebJobFile)
        if error()
            message('WebJob - SB JOB NO:' & clip(jobNo) & ' ' & error())
            message(job:Order_Number)
            message(fileerror())
            break
        end

        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = jobNo
        if Access:JOBNOTES.Fetch(jbn:RefNumberKey) <> Level:Benign

            if not Access:JOBNOTES.PrimeRecord()
                jbn:refnumber = jobNo

                jbn:ColContatName   = clip(web:TitleCollect)
                if clip(web:InitialCollect) <> ''
                    if clip(jbn:ColContatName) <> ''
                        jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:InitialCollect
                    else
                        jbn:ColContatName = web:InitialCollect
                    end
                end
                if clip(web:SurnameCollect) <> ''
                    if clip(jbn:ColContatName) <> ''
                        jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:SurnameCollect
                    else
                        jbn:ColContatName = web:SurnameCollect
                    end
                end

                jbn:DelContactName  = clip(web:TitleDelivery)
                if clip(web:InitialDelivery) <> ''
                    if clip(jbn:DelContactName) <> ''
                        jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:InitialDelivery
                    else
                        jbn:DelContactName = web:InitialDelivery
                    end
                end
                if clip(web:SurnameDelivery) <> ''
                    if clip(jbn:DelContactName) <> ''
                        jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:SurnameDelivery
                    else
                        jbn:DelContactName = web:SurnameDelivery
                    end
                end

                !IF CLIP(UPPER(web:PrimaryFault)) <> '25 OTHER - SEE COMMENTS'
                jbn:Fault_Description = web:PrimaryFault
                !END

                if job:Intermittent_Fault = 'YES'
                    if jbn:Fault_Description = ''
                        jbn:Fault_Description = '(INTERMITTENT FAULT)'
                    else
                        jbn:Fault_Description = clip(jbn:Fault_Description) & ' (INTERMITTENT FAULT)'
                    end
                end

                !jbn:Engineers_Notes = web:FreeTextFault
                ! Change o' spec 29 July 2004
                if clip(web:FreeTextFault) <> ''
                    if jbn:Fault_Description = ''
                        jbn:Fault_Description = clip(web:FreeTextFault)
                    else
                        jbn:Fault_Description = clip(jbn:Fault_Description) & ' ' & clip(web:FreeTextFault)
                    end
                end

                if Access:JOBNOTES.TryInsert()
                    Access:JOBNOTES.CancelAutoInc()
                end
            end

        end

        jobNo += 1
    end ! loop

    close(WebJobFile)

    message('Finished')

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?OkButton
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
      do ProcessJobs
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

