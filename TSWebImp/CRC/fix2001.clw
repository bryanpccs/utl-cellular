

   MEMBER('fix2.clw')                                 ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('FIX2001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

JobCount             LONG
ConnectionStr        STRING(255),STATIC
window               WINDOW('ServiceBase - Fix 2'),AT(,,174,26),FONT('Arial',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WEBJOB               FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(WEB),BINDABLE,THREAD
UK_Ref_Number            KEY(WEB:Ref_Number),NAME('UK_Ref_Number'),PRIMARY
EXPORTEDKEY              KEY(WEB:Exported), DUP
SBJOBNOKEY               KEY(WEB:SBJobNo), DUP
PROCESSIDKEY             KEY(WEB:ProcessID), DUP
RETSTOREBOOKKEY          KEY(WEB:RetStoreCode, WEB:BookDate), DUP
ACCOUNTNOSTATUSKEY       KEY(WEB:AccountNo, WEB:StatusChange), DUP
BOOKTYPEDATEKEY          KEY(WEB:BookingType, WEB:BookDate), DUP
TYPERETSTOREBOOKKEY      KEY(WEB:BookingType, WEB:RetStoreCode, WEB:BookDate), DUP
TYPEACCNOSTATUSKEY       KEY(WEB:BookingType, WEB:AccountNo, WEB:StatusChange), DUP
TYPERETURNBOOKKEY        KEY(WEB:BookingType, WEB:UnitReturned, WEB:BookDate), DUP
TYPERETURNDATEKEY        KEY(WEB:BookingType, WEB:UnitReturned, WEB:DateUnitReturned), DUP
ESNKEY                   KEY(WEB:ESN), DUP
CPIMEIKEY                KEY(WEB:CPIMEI, WEB:BookDate), DUP
SMSALERTNOKEY            KEY(WEB:SMSAlertNo), DUP

Record                   RECORD,PRE()
Ref_Number                  LONG
BookTime                    TIME
BookDate                    DATE
ESN                         STRING(20)
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Telephone_Number            STRING(15)
PostCode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Mobile_Number               STRING(15)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Colour                      STRING(30)
Phone_Lock                  STRING(30)
PrimaryFault                STRING(255)
FreeTextFault               STRING(255)
Intermittent_Fault          BYTE
Battery                     BYTE
Charger                     BYTE
BatteryCover                BYTE
None                        BYTE
LoanESN                     STRING(20)
LoanMake                    STRING(30)
LoanModel                   STRING(30)
LoanBattery                 BYTE
LoanCharger                 BYTE
ExportTime                  TIME
ExportDate                  DATE
Exported                    BYTE
RepairType                  STRING(2)
DateOfPurchase              DATE
InsuranceRefNo              STRING(30)
AccountNo                   STRING(15)
BookedBy                    STRING(30)
SBJobNo                     LONG
CompanyNameCollect          STRING(30)
AddressL1Collect            STRING(30)
AddressL2Collect            STRING(30)
AddressL3Collect            STRING(30)
PostcodeCollect             STRING(10)
TelNoCollect                STRING(15)
CompanyNameDelivery         STRING(30)
AddressL1Delivery           STRING(30)
AddressL2Delivery           STRING(30)
AddressL3Delivery           STRING(30)
PostcodeDelivery            STRING(10)
TelNoDelivery               STRING(15)
UnitCondition               STRING(255)
StoreNotes                  STRING(255)
StatusChange                STRING(30)
ProcessID                   LONG
ContractPeriod              LONG
Reason                      STRING(255)
FaxNo                       STRING(15)
TitleCollect                STRING(4)
InitialCollect              STRING(1)
SurnameCollect              STRING(30)
TitleDelivery               STRING(4)
InitialDelivery             STRING(1)
SurnameDelivery             STRING(30)
EmailAddress                STRING(255)
RetStoreCode                STRING(15)
SMSReq                      LONG
SMSSent                     LONG
LoanBoxNumber               STRING(20)
BookingType                 STRING(1)
CollectionMadeToday         BYTE
UseCustCollectDate          BYTE
SysCollectionDate           DATE
CustCollectionDate          DATE
SMSType                     STRING(1)
CPOnly                      BYTE
CPIssueReason               STRING(30)
CPExplanation               STRING(255)
CPAllocated                 STRING(1)
CPIMEI                      STRING(20)
CPAccBattery                STRING(1)
CPAccCharger                STRING(1)
CPAccHeadset                STRING(1)
CPAccGuide                  STRING(1)
CPAccPackaging              STRING(1)
MemoryCard                  BYTE
LoanManual                  BYTE
UnitReturned                BYTE
DateUnitReturned            DATE
DeviceCondition             STRING(40)
CPReturnDate                DATE
CPDepositValue              PDECIMAL(8, 2)
CPDepositSKUCode            STRING(30)
CPDeposit                   STRING(1)
CPReturnDateReason          STRING(255)
EmailReq                    BYTE
ValueSegment                STRING(20)
Contract                    BYTE
Country                     STRING(20)
CountryCollect              STRING(20)
CountryDelivery             STRING(20)
StatusAgentName             STRING(30)
SMSAlertNo                  STRING(15)
                         END
                     END  

! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD, PRE(), NAME('Ref_Number')
UniqueID                    LONG, NAME('Ref_Number')
                        END
                    END
iniFilePath         string(255)
errorOccurred       byte

dName               string(30)
dAdd1               string(30)
dAdd2               string(30)
dAdd3               string(30)
dPostCode           string(10)
dTel                string(20)
inCourier           string(20)
outCourier          string(20)

tmpChargeType       string(30)
tmpRepairType       string(30)
strBookingType      string(40)
strToteCollected    string(20)

strTempDate         cstring(20)
strAccount          cstring(16)

strServiceCentre    cstring('*SC*')
strDirection        cstring(2)

strPath             cstring(260)


! Procedure Prototypes

    map
Fix2          procedure(), long, proc
    end
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFWEB.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:AUDSTATS.UseFile
  Access:CONTHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  0{Prop:Hide} = true ! Hide the window
  
  iniFilePath =  clip(path()) & '\Webimp.ini'
  ConnectionStr = getini('Defaults', 'Connection', '', clip(iniFilePath))
  
  open(WEBJOB)
  if (errorcode())
      if errorcode() = 90
          message('Error Opening WEBJOB : ' & fileerrorcode() & ' - ' & fileerror())
      else
          message('Error Opening WEBJOB : ' & error())
      end
      errorOccurred = true
      post(Event:CloseWindow)
  else
      Fix2()
  end
  
  post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFWEB.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

Fix2  procedure()

duplicateFound byte

    code

    ! Jobs to be exported
    open(SQLFile)
    if error() then return false.

    SQLFile{Prop:SQL} = 'SELECT Ref_Number FROM WebJob WHERE AccountNo = ''VODA CARDIFF'''

    loop
        next(SQLFile)
        if error() then break.

        !if web:exported <> 0 then break.
        web:Ref_Number = SQL:UniqueID
        get(WEBJOB, web:UK_Ref_Number)
        if error() then cycle.

        Access:Jobs.ClearKey(job:AccOrdNoKey)
        job:account_number = 'VODA CARDIFF'
        job:Order_Number   = clip(web:Ref_Number)
        if not Access:Jobs.Fetch(job:AccOrdNoKey)

            ! Charge type
            Access:DEFWEB.ClearKey(dew:account_number_key)
            dew:account_number = job:account_number
            if not Access:DEFWEB.Fetch(dew:account_number_key)
                case web:RepairType
                    of 'IW' ! In Warranty
                        job:warranty_job = 'YES'
                        job:Warranty_Charge_Type = 'WARRANTY'
                        job:web_type    = 'WAR'
                        tmpChargeType = job:Warranty_Charge_Type
                        tmpRepairType = 'WARRANTY'
                    of 'IV' ! In Vodafone Warranty
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = 'IN VODAFONE WARRANTY'
                        job:web_type    = 'CHA'
                        tmpChargeType = JOB:Charge_Type
                        tmpRepairType = 'IN VODAFONE WARRANTY'
                    of 'ON' ! Out of warranty (Not chargeable)
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = 'OOW BUT NOT CHARGEABLE'
                        job:web_type    = 'CHA'
                        tmpChargeType = JOB:Charge_Type
                        tmpRepairType = 'OOW BUT NOT CHARGEABLE'
                    of 'IR' ! Insurance
                        job:Charge_Type = DEW:Insurance_Charge_Type
                        job:web_type    = 'INS'
                        job:chargeable_job = 'YES'
                        tmpChargeType = job:Charge_Type
                        tmpRepairType = 'INSURANCE'
                    of 'OC' ! Out Of Warranty (Chargeable)
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = DEW:Chargeable_Charge_Type
                        job:web_type    = 'CHA'
                        tmpChargeType = job:Charge_Type
                        tmpRepairType = 'CHARGEABLE'
                end ! case

                Access:TRANTYPE.ClearKey(TRT:Transit_Type_Key)

                ! Delivery Transit Type - dew:Transit_Type
                ! Collection Transit Type - dew:Job_Priority
                ! Exchange Transit Type - dew:Status_Type

                case web:BookingType
                    of 'B' ! Back To Base Repair
                        TRT:Transit_Type = DEW:Job_Priority
                    of 'E' ! Exchange
                        TRT:Transit_Type = DEW:Status_Type
                    else ! Retail
                        TRT:Transit_Type = DEW:Transit_Type
                end ! case

                Access:TRANTYPE.Fetch(TRT:Transit_Type_Key)

                job:unit_Type       = DEW:HandSet
                !JOB:Current_Status  = TRT:Initial_Status
                !JOB:Exchange_Status = TRT:ExchangeStatus
                !JOB:Loan_Status     = TRT:LoanStatus

                job:location        = DEW:location
                job:transit_Type    = TRT:Transit_Type
                job:turnaround_time = TRT:Initial_Priority

                Access:JOBS.TryUpdate()
            end
        end
    end ! loop

    close(SQLFile)

    return true
