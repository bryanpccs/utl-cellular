

   MEMBER('tswebimp.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('TSWEB005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('TSWEB007.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB008.INC'),ONCE        !Req'd for module callout resolution
                     END


FindFreeDate         PROCEDURE  (strDirection, strAccount, strDate, strPath) ! Declare Procedure
result      long
strIniFile  like(reg:BankHolidaysFileName)
availableDate date
    map
FreeDate        procedure(date sourceDate), date
    end
  CODE
    !
    ! Description : Find the next free date (check weekends / bank holidays)
    !

    if exists(clip(strPath) & '\ACCREG.DAT')
        ! Change the paths this way
        ACCREG{Prop:Name} = clip(strPath) & '\ACCREG.DAT'
        REGIONS{Prop:Name} = clip(strPath) & '\REGIONS.DAT'

        Access:AccReg.Open()
        Access:AccReg.UseFile()

        Access:Regions.Open()
        Access:Regions.UseFile()

        availableDate = deformat(strDate, @d06b)

        Access:AccReg.ClearKey(acg:AccountNoKey)
        acg:AccountNo = strAccount
        if not Access:AccReg.Fetch(acg:AccountNoKey)

            Access:Regions.ClearKey(reg:RegionNameKey)
            reg:RegionName = acg:RegionName
            if not Access:Regions.Fetch(reg:RegionNameKey)
                strIniFile = reg:BankHolidaysFileName
            end

        end

        if strIniFile = ''
            strIniFile = 'BHSC.DAT' ! Default, service centre will not exist as account
        end

        availableDate = FreeDate(availableDate)

        strDate = format(availableDate, @d06b)

        Access:Regions.Close()
        Access:AccReg.Close()

        result =  true
    end

    return result
FreeDate procedure(date sourceDate)

x   long(1)
bh  date, dim(15)

    code

    LoadBankHolidays(bh, strIniFile, strPath)

    loop x = 1 to 15
        if (GetDayOfWeek(sourceDate) = 'SAT')
            if strDirection = '-'
                sourceDate = sourceDate - 1
            else
                sourceDate = sourceDate + 2
            end
            sourceDate = FreeDate(sourceDate)
        end
        if (GetDayOfWeek(sourceDate) = 'SUN')
            if strDirection = '-'
                sourceDate = sourceDate - 2
            else
                sourceDate = sourceDate + 1
            end
            sourceDate = FreeDate(sourceDate)
        end
        if (bh[x] <> '')
            if (sourceDate = bh[x])
                if strDirection = '-'
                    sourceDate = sourceDate - 1
                else
                    sourceDate = sourceDate + 1
                end
                sourceDate = FreeDate(sourceDate)
            end
        end
    end ! loop

    return sourceDate
