

   MEMBER('tswebimp.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TSWEB001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('TSWEB002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('TSWEB004.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

JobCount             LONG
ConnectionStr        STRING(255),STATIC
window               WINDOW('ServiceBase - Web Jobs Import'),AT(,,174,26),FONT('Arial',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
WEBJOB               FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(WEB),BINDABLE,THREAD
UK_Ref_Number            KEY(WEB:Ref_Number),NAME('UK_Ref_Number'),PRIMARY
EXPORTEDKEY              KEY(WEB:Exported), DUP
SBJOBNOKEY               KEY(WEB:SBJobNo), DUP
PROCESSIDKEY             KEY(WEB:ProcessID), DUP
!RETSTOREBOOKKEY          KEY(WEB:RetStoreCode, WEB:BookDate), DUP
!ACCOUNTNOSTATUSKEY       KEY(WEB:AccountNo, WEB:StatusChange), DUP
Record                   RECORD,PRE()
Ref_Number                  LONG
BookTime                    TIME
BookDate                    DATE
ESN                         STRING(20)
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Telephone_Number            STRING(15)
PostCode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Mobile_Number               STRING(15)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Colour                      STRING(30)
Phone_Lock                  STRING(30)
PrimaryFault                STRING(255)
FreeTextFault               STRING(255)
Intermittent_Fault          BYTE
Battery                     BYTE
Charger                     BYTE
BatteryCover                BYTE
None                        BYTE
LoanESN                     STRING(20)
LoanMake                    STRING(30)
LoanModel                   STRING(30)
LoanBattery                 BYTE
LoanCharger                 BYTE
ExportTime                  TIME
ExportDate                  DATE
Exported                    BYTE
RepairType                  STRING(2)
DateOfPurchase              DATE
InsuranceRefNo              STRING(30)
AccountNo                   STRING(15)
BookedBy                    STRING(30)
SBJobNo                     LONG
CompanyNameCollect          STRING(30)
AddressL1Collect            STRING(30)
AddressL2Collect            STRING(30)
AddressL3Collect            STRING(30)
PostcodeCollect             STRING(10)
TelNoCollect                STRING(15)
CompanyNameDelivery         STRING(30)
AddressL1Delivery           STRING(30)
AddressL2Delivery           STRING(30)
AddressL3Delivery           STRING(30)
PostcodeDelivery            STRING(10)
TelNoDelivery               STRING(15)
UnitCondition               STRING(255)
StoreNotes                  STRING(255)
StatusChange                STRING(30)
ProcessID                   LONG
ContractPeriod              LONG
Reason                      STRING(255)
FaxNo                       STRING(15)
TitleCollect                STRING(4)
InitialCollect              STRING(1)
SurnameCollect              STRING(30)
TitleDelivery               STRING(4)
InitialDelivery             STRING(1)
SurnameDelivery             STRING(30)
EmailAddress                STRING(255)
RetStoreCode                STRING(15)
SMSReq                      LONG
SMSSent                     LONG
!LoanBoxNumber               STRING(20)
!BookingType                 STRING(1)
!CollectionMadeToday         BYTE
!UseCustCollectDate          BYTE
!SysCollectionDate           DATE
!CustCollectionDate          DATE
                         END
                     END  

! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD, PRE(), NAME('Ref_Number')
UniqueID                    LONG, NAME('Ref_Number')
                        END
                    END
IniFilepath          STRING(255)
errorOccurred        BYTE

dName                STRING(30)
dAdd1                STRING(30)
dAdd2                STRING(30)
dAdd3                STRING(30)
dPostCode            STRING(10)
dTel                 STRING(20)
inCourier            STRING(20)
outCourier           STRING(20)

tmpChargeType        STRING(30)
tmpRepairType        STRING(30)
strBookingType       STRING(30)

    MAP
ImportJobs          PROCEDURE(), long, proc
    END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DEFWEB.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:AUDSTATS.UseFile
  Access:CONTHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  0{Prop:Hide} = true ! Hide the window
  
  IniFilepath =  CLIP(PATH()) & '\Webimp.ini'
  ConnectionStr = GETINI('Defaults', 'Connection', '', CLIP(IniFilepath))
  
  OPEN(WEBJOB)
  IF (ERRORCODE()) THEN
      if errorcode() = 90
          MESSAGE('Error Opening WEBJOB : ' & fileerrorcode() & ' - ' & fileerror())
      else
          MESSAGE('Error Opening WEBJOB : ' & ERROR())
      end
      errorOccurred = true
      POST(EVENT:CloseWindow)
  ELSE
      ImportJobs()
  END
  
  POST(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DEFWEB.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

ImportJobs          PROCEDURE()

duplicateFound byte

    CODE

    ! Jobs to be exported
    open(SQLFile)
    if error() then return false.

    SQLFile{Prop:SQL} = 'SELECT Ref_Number FROM WebJob WHERE Exported = 0'

    !web:exported = 0
    !set(web:ExportedKey, web:ExportedKey)
    loop
        next(SQLFile)
        if error() then break.

        !if web:exported <> 0 then break.
        web:Ref_Number = SQL:UniqueID
        get(WEBJOB, web:UK_Ref_Number)
        if error() then cycle.

        ! Duplicate check
        duplicateFound = false

        Access:Jobs.ClearKey(job:AccOrdNoKey)
        job:account_number = clip(web:AccountNo)
        job:Order_Number   = clip(web:Ref_Number)
        set(job:AccOrdNoKey, job:AccOrdNoKey)
        loop until Access:Jobs.Next()
            if clip(job:account_number) <> clip(web:AccountNo) then break.
            if clip(job:Order_Number) <> clip(web:Ref_Number) then break.
            if job:date_booked <> web:BookDate then cycle.
            if job:time_booked <> web:BookTime then cycle.
            if clip(job:esn) <> clip(RemoveSpaces(web:ESN)) then cycle.
            duplicateFound = true
            break
        end ! loop

        if duplicateFound = true
            web:ExportDate = today()
            web:ExportTime = clock()
            web:Exported = 1
            web:SBJobNo  = job:ref_number
            put(WEBJOB)
            cycle
        end

        if not Access:JOBS.PrimeRecord()
            job:EDI                         = 'XXX'
            job:account_number              = web:AccountNo
            job:who_booked                  = 'WEB'
            job:date_booked                 = web:BookDate
            job:time_booked                 = web:BookTime
            job:model_number                = web:Model_Number
            job:manufacturer                = web:Manufacturer
            job:esn                         = RemoveSpaces(web:ESN)
            job:colour                      = web:Colour
            job:phone_lock                  = web:Phone_Lock
            job:intermittent_fault          = web:Intermittent_Fault
            job:title                       = web:Title
            job:initial                     = web:Initial
            job:surname                     = web:Surname

            job:dop                         = web:DateOfPurchase
            job:Insurance_Reference_Number  = web:InsuranceRefNo

            if web:Intermittent_Fault = true
                job:Intermittent_Fault = 'YES'
            else
                job:Intermittent_Fault = 'NO'
            end

            job:company_name       = web:Company_Name
            job:address_line1      = web:Address_Line1
            job:address_line2      = web:Address_Line2
            job:address_line3      = web:Address_Line3
            job:postcode           = web:Postcode
            job:telephone_number   = web:Telephone_Number
            job:Fax_Number         = web:FaxNo

            job:mobile_number      = RemoveSpaces(web:Mobile_Number)

            dName           = ''
            dAdd1           = ''
            dAdd2           = ''
            dAdd3           = ''
            dPostCode       = ''
            dTel            = ''
            inCourier       = ''
            outCourier      = ''

            tmpChargeType   = ''
            tmpRepairType   = ''

            Access:SUBTRACC.ClearKey(sub:account_number_key)
            sub:account_number = job:account_number
            IF NOT Access:SUBTRACC.Fetch(sub:account_number_key)
                Access:TRADEACC.ClearKey(tra:account_number_key)
                tra:account_number = SUB:Main_Account_Number
                IF NOT Access:TRADEACC.Fetch(tra:account_number_key)
                    IF tra:use_sub_accounts = 'YES'
                        dName           = SUB:Company_Name
                        dAdd1           = SUB:Address_Line1
                        dAdd2           = SUB:Address_Line2
                        dAdd3           = SUB:Address_Line3
                        dPostCode       = SUB:Postcode
                        dTel            = SUB:Telephone_Number
                        inCourier       = SUB:Courier_Incoming
                        outCourier      = SUB:Courier_Outgoing
                    ELSE
                        dName           = TRA:Company_Name
                        dAdd1           = TRA:Address_Line1
                        dAdd2           = TRA:Address_Line2
                        dAdd3           = TRA:Address_Line3
                        dPostCode       = TRA:Postcode
                        dTel            = TRA:Telephone_Number
                        inCourier       = TRA:Courier_Incoming
                        outCourier      = TRA:Courier_Outgoing
                    END
                END
            END

            job:Incoming_Courier   = inCourier
            job:Incoming_Date      = web:BookDate
            job:Loan_Courier       = outCourier
            job:Exchange_Courier   = outCourier
            job:Courier            = outCourier
            job:Order_Number       = web:Ref_Number

            case web:ProcessID
                of 0 ! Retail
                    ! Collection address
                    job:Company_Name_Collection   = dName
                    job:Address_Line1_Collection  = dAdd1
                    job:Address_Line2_Collection  = dAdd2
                    job:Address_Line3_Collection  = dAdd3
                    job:Postcode_Collection       = dPostCode
                    job:Telephone_Collection      = dTel

                    ! Delivery address (return address)
                    if (web:CompanyNameDelivery = '' and web:PostcodeDelivery = '' and web:AddressL1Delivery = '' and |
                       web:AddressL2Delivery = '' and web:AddressL3Delivery = '' and web:TelNoDelivery = '')
                        job:Company_Name_Delivery   =  dName
                        job:Postcode_Delivery       =  dPostCode
                        job:Address_Line1_Delivery  =  dAdd1
                        job:Address_Line2_Delivery  =  dAdd2
                        job:Address_Line3_Delivery  =  dAdd3
                        job:Telephone_Delivery      =  dTel
                    else
                        job:Company_Name_Delivery   =  web:CompanyNameDelivery
                        job:Postcode_Delivery       =  web:PostcodeDelivery
                        job:Address_Line1_Delivery  =  web:AddressL1Delivery
                        job:Address_Line2_Delivery  =  web:AddressL2Delivery
                        job:Address_Line3_Delivery  =  web:AddressL3Delivery
                        job:Telephone_Delivery      =  web:TelNoDelivery
                    end

                of 1 ! Call Centre
                    ! Delivery address
                    job:Company_Name_Delivery   =  web:CompanyNameDelivery
                    job:Postcode_Delivery       =  web:PostcodeDelivery
                    job:Address_Line1_Delivery  =  web:AddressL1Delivery
                    job:Address_Line2_Delivery  =  web:AddressL2Delivery
                    job:Address_Line3_Delivery  =  web:AddressL3Delivery
                    job:Telephone_Delivery      =  web:TelNoDelivery

                    ! Collection address
                    job:Company_Name_Collection   = web:CompanyNameCollect
                    job:Address_Line1_Collection  = web:AddressL1Collect
                    job:Address_Line2_Collection  = web:AddressL2Collect
                    job:Address_Line3_Collection  = web:AddressL3Collect
                    job:Postcode_Collection       = web:PostcodeCollect
                    job:Telephone_Collection      = web:TelNoCollect

                    ! This section modifies the data if trader details say to use traders address.
                    Access:SUBTRACC.ClearKey(sub:account_number_key)
                    sub:account_number = job:account_number
                    if Access:SUBTRACC.Fetch(sub:account_number_key) = level:benign
                        Access:TRADEACC.ClearKey(tra:account_number_key)
                        tra:account_number = sub:Main_Account_Number
                        if access:tradeacc.fetch(tra:account_number_key) = level:benign
                            job:Estimate         = tra:Force_Estimate
                            job:Estimate_If_Over = tra:Estimate_If_Over
                            if TRA:Invoice_Sub_Accounts = 'YES'
                                if clip(upper(sub:Use_Customer_Address)) <> 'YES'
                                    job:company_name        = sub:company_name
                                    job:address_line1       = sub:address_line1
                                    job:address_line2       = sub:address_line2
                                    job:address_line3       = sub:address_line3
                                    job:Postcode            = sub:postcode
                                    job:telephone_number    = sub:telephone_number
                                    job:fax_number          = sub:fax_number
                                end

                                if sub:Use_Collection_Address = 'YES'
                                    job:Company_Name_Collection   = sub:company_name
                                    job:Address_Line1_Collection  = sub:address_line1
                                    job:Address_Line2_Collection  = sub:address_line2
                                    job:Address_Line3_Collection  = sub:address_line3
                                    job:Postcode_Collection       = sub:postcode
                                    job:Telephone_Collection      = sub:telephone_number
                                end !if sub use collection address

                                If sub:Use_Delivery_Address = 'YES'
                                    job:Company_Name_Delivery   = sub:company_name
                                    job:address_line1_Delivery  = sub:address_line1
                                    job:address_line2_Delivery  = sub:address_line2
                                    job:address_line3_Delivery  = sub:address_line3
                                    job:Postcode_Delivery       = sub:postcode
                                    job:Telephone_Delivery      = sub:telephone_number
                                end !If sub:use_delivery_address = 'YES'
                            else!if tra:use_sub_accounts = 'YES'
                                if clip(upper(tra:Use_Customer_Address)) <> 'YES'
                                    job:company_name        = tra:company_name
                                    job:Address_Line1       = tra:address_line1
                                    job:address_line2       = tra:address_line2
                                    job:address_line3       = tra:address_line3
                                    job:Postcode            = tra:postcode
                                    job:telephone_number    = tra:telephone_number
                                    job:fax_number          = tra:fax_number
                                end

                                if upper(sub(TRA:Use_Delivery_Address,1,1)) = 'Y'
                                    job:Company_Name_Delivery   = tra:company_name
                                    job:Address_Line1_Delivery  = tra:address_line1
                                    job:address_line2_Delivery  = tra:address_line2
                                    job:address_line3_Delivery  = tra:address_line3
                                    job:Postcode_Delivery       = tra:postcode
                                    job:Telephone_Delivery      = tra:Telephone_Number
                                end !If tra:use_delivery_address
                            end!if tra:use_sub_accounts = 'YES'
                        end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                    end!if access:subtracc.fetch(sub:account_number_key) = level:benign

            end ! case

            ! Charge type
            Access:DEFWEB.ClearKey(dew:account_number_key)
            dew:account_number = job:account_number
            IF NOT Access:DEFWEB.Fetch(dew:account_number_key)
                CASE web:RepairType
                    OF 'IW' ! In Warranty
                    OROF 'IV' ! In Vodafone Warranty
                        job:warranty_job = 'YES'
                        job:Warranty_Charge_Type = 'WARRANTY'
                        job:web_type    = 'WAR'
                        tmpChargeType = job:Warranty_Charge_Type
                        tmpRepairType = 'WARRANTY'
                    OF 'ON'  ! Out of warranty (Not chargeable)
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = 'OOW BUT NOT CHARGEABLE'
                        job:web_type    = 'CHA'
                        tmpChargeType = JOB:Charge_Type
                        tmpRepairType = 'OOW BUT NOT CHARGEABLE'
                    OF 'IR' ! Insurance
                        job:Charge_Type = DEW:Insurance_Charge_Type
                        job:web_type    = 'INS'
                        job:chargeable_job = 'YES'
                        tmpChargeType = job:Charge_Type
                        tmpRepairType = 'INSURANCE'
                    OF 'OC' ! Out Of Warranty (Chargeable)
                        job:chargeable_job = 'YES'
                        JOB:Charge_Type = DEW:Chargeable_Charge_Type
                        job:web_type    = 'CHA'
                        tmpChargeType = job:Charge_Type
                        tmpRepairType = 'CHARGEABLE'
                END ! CASE

                Access:TRANTYPE.ClearKey(TRT:Transit_Type_Key)
                TRT:Transit_Type = DEW:Transit_Type
                Access:TRANTYPE.Fetch(TRT:Transit_Type_Key)

                job:unit_Type       = DEW:HandSet
                JOB:Current_Status  = TRT:Initial_Status
                JOB:Exchange_Status = TRT:ExchangeStatus
                JOB:Loan_Status     = TRT:LoanStatus

                job:location        = DEW:location
                job:transit_Type    = TRT:Transit_Type
                job:turnaround_time = TRT:Initial_Priority

                Access:TURNARND.ClearKey(TUR:Turnaround_Time_Key)
                tur:turnaround_time = job:turnaround_time
                IF NOT Access:TURNARND.Fetch(TUR:Turnaround_Time_Key)
                    Turnaround_Routine(tur:days, tur:hours, end_date", end_time")
                    JOB:Turnaround_End_Date = end_date"
                    JOB:Turnaround_End_Time = end_time"
                END
                    
                JOB:Status_End_Date = 0 
                JOB:Status_End_Time = 0
            END

            IF Access:JOBS.TryUpdate()
                Access:JOBS.CancelAutoInc()
            ELSE
                if web:Battery <> 0
                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'BATTERY'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end
                if web:Charger <> 0
                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'CHARGER'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end
                if web:BatteryCover <> 0
                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'BATTERY COVER'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end

                if web:Battery = 0 and web:Charger = 0 and web:BatteryCover = 0

                    if not Access:JOBACC.PrimeRecord()
                        jac:ref_number = job:ref_number
                        jac:accessory = 'NONE'
                        if Access:JOBACC.TryInsert()
                            Access:JOBACC.CancelAutoInc()
                        end
                    end
                end

                if not Access:JOBNOTES.PrimeRecord()
                    jbn:refnumber = job:ref_number

                    jbn:ColContatName   = clip(web:TitleCollect)
                    if clip(web:InitialCollect) <> ''
                        if clip(jbn:ColContatName) <> ''
                            jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:InitialCollect
                        else
                            jbn:ColContatName = web:InitialCollect
                        end
                    end
                    if clip(web:SurnameCollect) <> ''
                        if clip(jbn:ColContatName) <> ''
                            jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:SurnameCollect
                        else
                            jbn:ColContatName = web:SurnameCollect
                        end
                    end

                    jbn:DelContactName  = clip(web:TitleDelivery)
                    if clip(web:InitialDelivery) <> ''
                        if clip(jbn:DelContactName) <> ''
                            jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:InitialDelivery
                        else
                            jbn:DelContactName = web:InitialDelivery
                        end
                    end
                    if clip(web:SurnameDelivery) <> ''
                        if clip(jbn:DelContactName) <> ''
                            jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:SurnameDelivery
                        else
                            jbn:DelContactName = web:SurnameDelivery
                        end
                    end

                    !IF CLIP(UPPER(web:PrimaryFault)) <> '25 OTHER - SEE COMMENTS'
                    jbn:Fault_Description = web:PrimaryFault
                    !END

                    if job:Intermittent_Fault = 'YES'
                        if jbn:Fault_Description = ''
                            jbn:Fault_Description = '(INTERMITTENT FAULT)'
                        else
                            jbn:Fault_Description = clip(jbn:Fault_Description) & ' (INTERMITTENT FAULT)'
                        end
                    end

                    !jbn:Engineers_Notes = web:FreeTextFault
                    ! Change o' spec 29 July 2004
                    if web:FreeTextFault <> ''
                        if jbn:Fault_Description = ''
                            jbn:Fault_Description = clip(web:FreeTextFault)
                        else
                            jbn:Fault_Description = clip(jbn:Fault_Description) & ' ' & clip(web:FreeTextFault)
                        end
                    end

                    if Access:JOBNOTES.TryInsert()
                        Access:JOBNOTES.CancelAutoInc()
                    end
                end

                !case web:BookingType
                !    of 'P'
                !        strBookingType = 'CONTACT CENTRE PAYT REPAIR'
                !    of 'E'
                !        strBookingType = 'CONTACT CENTRE EXCHANGE'
                !    else
                        strBookingType = 'SID RETAIL REPAIR' ! Default to 'R'
                !end ! case

                if not Access:AUDIT.PrimeRecord()
                    aud:notes         = ('JOB CREATED BY WEBMASTER' & '<13,10>'|
                                        & 'FOR: ' & clip(job:Account_Number)  & '<13,10>' |
                                        & clip(strBookingType) & '<13,10>' |
                                        & 'CHARGE TYPE: ' & clip(tmpChargeType) & '<13,10>'|
                                        & 'BOOKED BY: ' & clip(web:BookedBy) & '<13,10>'||
                                        & 'UNIT DETAILS: ' & clip(job:Manufacturer) & ' ' & clip(job:Model_Number) & ' ' & clip(job:Unit_Type) & '<13,10>'|
                                        & 'E.S.N./I.M.E.I: ' & clip(job:ESN)& '<13,10>'|
                                        & 'CONDITION OF DEVICE: ' & clip(web:UnitCondition))

                                        !& 'AS: ' & clip(tmpRepairType) & '<13,10>'|

                                        !& 'M.S.N.: '&CLIP(job:MSN)& '<13,10>'| Was after IMEI
                                        !& 'CHARGEABLE JOB: ' & CLIP(job:Chargeable_Job)&'<13,10>'|
                                        !& 'ESTIMATE: ' & CLIP(job:Estimate) & '  VALUE: '& CLIP(job:Estimate_If_Over)&'<13,10>'|
                                        !& 'END USER: ' & clip(job:Title) & clip(job:Surname) & ' - ' &clip(job:telephone_number))

                    aud:User          = 'WEB'
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    aud:action        = 'NEW JOB INITIAL BOOKING'
                    if Access:AUDIT.TryInsert()
                        Access:AUDIT.CancelAutoInc()
                    end
                end

                Access:JOBSE.ClearKey(jobe:RefNumberKey)
                jobe:RefNumber = job:ref_number
                if Access:JOBSE.Fetch(jobe:RefNumberKey)
                    if not Access:JOBSE.PrimeRecord()
                        jobe:RefNumber = job:Ref_Number
                        jobe:EndUserEmailAddress = web:EmailAddress
                        jobe:TraFaultCode4 = web:BookedBy
                        if Access:JOBSE.TryInsert()
                            Access:JOBSE.CancelAutoInc()
                        end
                    end
                end

                ! Contact history
                if clip(web:StoreNotes) <> ''
                    if not Access:ContHist.PrimeRecord()
                        cht:Ref_Number = job:ref_number
                        cht:Date = today()
                        cht:Time = clock()
                        cht:User = 'SID'
                        cht:Action = 'SID CUSTOMER NOTES'
                        cht:Notes = 'NOTE ENTERED BY: ' & web:BookedBy & '<13,10>' & web:StoreNotes
                        if Access:ContHist.TryInsert()
                            Access:ContHist.CancelAutoInc()
                        end
                    end
                end

                web:ExportDate = today()
                web:ExportTime = clock()
                web:Exported = 1
                web:SBJobNo  = job:ref_number
                put(WEBJOB)

                ! Reset the key
                !web:exported = 0
                !set(web:ExportedKey, web:ExportedKey)
            END
        end

    END ! loop


    ! Update IMEI status
    SQLFile{Prop:SQL} = 'SELECT Ref_Number FROM WebJob WHERE ProcessID = 99'

    !web:ProcessID = 99
    !SET(web:ProcessIDKey, web:ProcessIDKey)
    loop
        next(SQLFile)
        if error() then break.
        !IF (web:ProcessID <> 99) THEN BREAK.

        web:Ref_Number = SQL:UniqueID
        get(WEBJOB, web:UK_Ref_Number)
        if error() then cycle.

        Access:Jobs.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = web:SBJobNo
        if Access:JOBS.Fetch(job:Ref_Number_Key) then cycle.

        GetStatus(web:StatusChange[1:3], 1, 'JOB')

        if not Access:JOBS.TryUpdate()
            web:ProcessID = 0
            web:StatusChange = ''
            put(WEBJOB)

            ! Reset the key
            !web:ProcessID = 99
            !set(web:ProcessIDKey, web:ProcessIDKey)
        end

    end ! loop

    ! Update cancelled jobs
    SQLFile{Prop:SQL} = 'SELECT Ref_Number FROM WebJob WHERE ProcessID = 98'

    !web:ProcessID = 98
    !SET(web:ProcessIDKey, web:ProcessIDKey)
    loop
        next(SQLFile)
        if error() then break.
        !IF (web:ProcessID <> 98) THEN BREAK.

        web:Ref_Number = SQL:UniqueID
        get(WEBJOB, web:UK_Ref_Number)
        if error() then cycle.

        Access:JOBS.ClearKey(job:Ref_Number_Key)
        job:Ref_Number = web:SBJobNo
        if Access:JOBS.Fetch(job:Ref_Number_Key) then cycle.

        ! 798 JOB CANCELLED BY VODAFONE
        GetStatus(798, 1, 'JOB')

        if not Access:JOBS.TryUpdate()

            ! Contact history
            if clip(web:StoreNotes) <> ''
                if not Access:ContHist.PrimeRecord()
                    cht:Ref_Number = job:ref_number
                    cht:Date = today()
                    cht:Time = clock()
                    cht:User = 'SID'
                    cht:Action = 'SID JOB CANCELLED'
                    cht:Notes = web:StoreNotes
                    if Access:ContHist.TryInsert()
                        Access:ContHist.CancelAutoInc()
                    end
                end
            end

            web:ProcessID = 0
            web:StatusChange = ''
            put(WEBJOB)

            ! Reset the key
            !web:ProcessID = 98
            !set(web:ProcessIDKey, web:ProcessIDKey)
        end

    END ! LOOP

    close(SQLFile)

    return true
