

   MEMBER('tswebimp.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('TSWEB006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('TSWEB005.INC'),ONCE        !Req'd for module callout resolution
                     END


TransitToStoreDate   PROCEDURE  (strDirection, strAccountTo, strDate, strPath) ! Declare Procedure
startDate   date
result      long
transitDaysToStore long
strServiceCentre cstring('*SC*')
    map
FindNextFreeDate        procedure(*cstring strAccount, date currentDate), date
FindPreviousFreeDate    procedure(*cstring strAccount, date currentDate), date
    end
  CODE
    !
    ! Description : Get the number of transit days from the service centre to the store
    !

    if exists(clip(strPath) & '\ACCREG.DAT')
        ! Change the paths this way
        ACCREG{Prop:Name} = clip(strPath) & '\ACCREG.DAT'
        REGIONS{Prop:Name} = clip(strPath) & '\REGIONS.DAT'

        Access:AccReg.Open()
        Access:AccReg.UseFile()

        Access:Regions.Open()
        Access:Regions.UseFile()

        startDate = deformat(strDate, @d06b)

        ! Days to store from service centre
        Access:AccReg.ClearKey(acg:AccountNoKey)
        acg:AccountNo = strAccountTo
        if not Access:AccReg.Fetch(acg:AccountNoKey)
            if acg:TransitDaysException = true
                transitDaysToStore = acg:TransitDaysToStore
            else
                ! Lookup region
                Access:Regions.ClearKey(reg:RegionNameKey)
                reg:RegionName = acg:RegionName
                if not Access:Regions.Fetch(reg:RegionNameKey)
                    transitDaysToStore = reg:TransitDaysToStore
                end
            end
        end

        if transitDaysToStore = 0
            transitDaysToStore = 1 ! Default
        end

        ! Calculate number of days in transit to store

        loop transitDaysToStore times
            if strDirection = '-'
                ! Previous date
                startDate -= 1
                ! Check date is working day
                startDate = FindPreviousFreeDate(strAccountTo, startDate)
            else
                ! Next date
                startDate += 1
                ! Check date is working day
                startDate = FindNextFreeDate(strAccountTo, startDate)
            end
        end ! loop

        strDate = format(startDate, @d06b)

        Access:AccReg.Close()
        Access:Regions.Close()

        result = true
    end

    return result
FindNextFreeDate procedure(*cstring strAccount, date currentDate)
    !
    ! Description : Find the next free date (Just a wrapper for FindFreeDate)
    !

strNext          cstring('+')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strNext, strAccount, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
FindPreviousFreeDate procedure(*cstring strAccount, date currentDate)
    !
    ! Description : Find the previous free date (Just a wrapper for FindFreeDate)
    !

strPrevious      cstring('-')
strDateToProcess cstring(30)
    code

    strDateToProcess = format(currentDate, @d06b)

    FindFreeDate(strPrevious, strAccount, strDateToProcess, strPath)

    return deformat(strDateToProcess, @d06b)
