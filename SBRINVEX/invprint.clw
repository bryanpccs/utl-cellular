   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE
   INCLUDE('PrnProp.clw')

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('INVPRBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('INVPR001.CLW')
Main                   PROCEDURE   !
     END
     MODULE('CPC55L32.LIB')
       Wmf2Ascii(QUEUE, STRING, LONG),NAME('Wmf2Ascii')
       PaperSupport(SHORT,<QUEUE>),STRING,NAME('PaperSupport')
       PrintPreview(QUEUE,SHORT,<STRING>,REPORT,BYTE,<STRING>,<LONG>,<STRING>,<STRING>),LONG,NAME('PrintPreview')
       Supported(LONG),SHORT,NAME('Supported')
       HandleCopies(QUEUE,LONG),NAME('HandleCopies')
       ShowPrinterStatus(STRING,SHORT,<STRING>),NAME('ShowPrinterStatus')
       AssgnPgOfPg(Queue,<STRING>,<BYTE>,<STRING>,<LONG>),NAME('AssgnPgOfPg')
       SaveClipToFile(<STRING>),STRING,NAME('SaveClipToFile')
       SetPrinterDraftMode(),LONG,NAME('SetPrinterDraftMode')
     END
          FillCpcsIniStrings(STRING,STRING,STRING,STRING)
          MODULE('invpr_SF.CLW')
            CheckOpen(FILE File,<BYTE OverrideCreate>,<BYTE OverrideOpenMode>)
            StandardWarning(LONG WarningID),LONG,PROC
            StandardWarning(LONG WarningID,STRING WarningText1),LONG,PROC
            StandardWarning(LONG WarningID,STRING WarningText1,STRING WarningText2),LONG,PROC
            RISaveError
          END
   END

glo:FileName       STRING(255)
SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

CPCSStartUpPrintDevice      CSTRING(64)

CPCS:ProgWinTitlePrvw   CSTRING(64)  
CPCS:ProgWinTitlePrnt   CSTRING(64)  
CPCS:ProgWinPctText     CSTRING(64)  
CPCS:ProgWinRecText     CSTRING(64)  
CPCS:ProgWinUsrText     CSTRING(64)  
CPCS:PrtrDlgTitle       CSTRING(64)  
CPCS:AskPrvwDlgTitle    CSTRING(64)  
CPCS:AskPrvwDlgText     CSTRING(64)  
CPCS:AreYouSureTitle    CSTRING(64)  
CPCS:AreYouSureText     CSTRING(64)  
CPCS:PrvwPartialTitle   CSTRING(64)  
CPCS:PrvwPartialText    CSTRING(128) 
CPCS:NoDfltPrtrTitle    CSTRING(64)  
CPCS:NoDfltPrtrText     CSTRING(255) 
CPCS:NthgToPrvwTitle    CSTRING(64)  
CPCS:NthgToPrvwText     CSTRING(64)  
CPCS:NthgToPrntTitle    CSTRING(64)  
CPCS:NthgToPrntText     CSTRING(64)  
CPCS:AsciiOutTitle      CSTRING(64)  
CPCS:AsciiOutmasks      CSTRING(64)  
CPCS:DynLblErrTitle     CSTRING(64)
CPCS:DynLblErrText1     CSTRING(64)  
CPCS:DynLblErrText2     CSTRING(64)  

SaveErrorCode               LONG
SaveError                   CSTRING(255)
SaveFileErrorCode           LONG
SaveFileError               CSTRING(255)

Warn:InvalidFile         EQUATE (1)
Warn:InvalidKey          EQUATE (2)
Warn:RebuildError        EQUATE (3)
Warn:CreateError         EQUATE (4)
Warn:CreateOpenError     EQUATE (5)
Warn:ProcedureToDo       EQUATE (6)
Warn:BadKeyedRec         EQUATE (7)
Warn:OutOfRangeHigh      EQUATE (8)
Warn:OutOfRangeLow       EQUATE (9)
Warn:OutOfRange          EQUATE (10)
Warn:NotInFile           EQUATE (11)
Warn:RestrictUpdate      EQUATE (12)
Warn:RestrictDelete      EQUATE (13)
Warn:InsertError         EQUATE (14)
Warn:RIUpdateError       EQUATE (15)
Warn:UpdateError         EQUATE (16)
Warn:RIDeleteError       EQUATE (17)
Warn:DeleteError         EQUATE (18)
Warn:InsertDisabled      EQUATE (19)
Warn:UpdateDisabled      EQUATE (20)
Warn:DeleteDisabled      EQUATE (21)
Warn:NoCreate            EQUATE (22)
Warn:ConfirmCancel       EQUATE (23)
Warn:DuplicateKey        EQUATE (24)
Warn:AutoIncError        EQUATE (25)
Warn:FileLoadError       EQUATE (26)
Warn:ConfirmCancelLoad   EQUATE (27)
Warn:FileZeroLength      EQUATE (28)
Warn:EndOfAsciiQueue     EQUATE (29)
Warn:DiskError           EQUATE (30)
Warn:ProcessActionError  EQUATE (31)
Warn:StandardDelete      EQUATE (32)
Warn:SaveOnCancel        EQUATE (33)
Warn:LogoutError         EQUATE (34)
Warn:RecordFetchError    EQUATE (35)
Warn:ViewOpenError       EQUATE (36)
Warn:NewRecordAdded      EQUATE (37)
Warn:RIFormUpdateError   EQUATE (38)


INVIMP               FILE,DRIVER('BASIC'),OEM,NAME(glo:FileName),PRE(INV),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Field1                      STRING(30)
Field2                      STRING(30)
Field3                      STRING(30)
Field4                      STRING(30)
Field5                      STRING(30)
Field6                      STRING(30)
Field7                      STRING(30)
Field8                      STRING(30)
Field9                      STRING(30)
Field10                     STRING(30)
Field11                     STRING(30)
Field12                     STRING(30)
Field13                     STRING(30)
Field14                     STRING(30)
Field15                     STRING(30)
Field16                     STRING(30)
Field17                     STRING(30)
Field18                     STRING(30)
Field19                     STRING(30)
Field20                     STRING(30)
Field21                     STRING(30)
Field22                     STRING(30)
Field23                     STRING(30)
Field24                     STRING(30)
Field25                     STRING(30)
Field26                     STRING(30)
Field27                     STRING(30)
Field28                     STRING(30)
Field29                     STRING(30)
Field30                     STRING(30)
Field31                     STRING(30)
Field32                     STRING(30)
Field33                     STRING(30)
Field34                     STRING(30)
Field35                     STRING(30)
Field36                     STRING(255)
Field37                     STRING(255)
Field38                     STRING(30)
Field39                     STRING(30)
                         END
                     END                       

Invoice              FILE,DRIVER('TOPSPEED'),OEM,NAME('invoice.tps'),PRE(INV1),CREATE,BINDABLE,THREAD
InvoiceNumberKey         KEY(INV1:InvoiceNumber),DUP,NOCASE
Record                   RECORD,PRE()
InvoiceNumber               LONG
DefName                     STRING(30)
DefAdd1                     STRING(30)
DefAdd2                     STRING(30)
DefAdd3                     STRING(30)
DefPostcode                 STRING(30)
DefTelephone                STRING(30)
DefFax                      STRING(30)
DefEmail                    STRING(255)
VatNumber                   STRING(30)
DateCreated                 DATE
SaleNumber                  LONG
InvCompanyName              STRING(30)
InvAdd1                     STRING(30)
InvAdd2                     STRING(30)
InvAdd3                     STRING(30)
InvPostcode                 STRING(30)
InvTelephone                STRING(30)
InvFax                      STRING(30)
DelCompanyName              STRING(30)
DelAdd1                     STRING(30)
DelAdd2                     STRING(30)
DelAdd3                     STRING(30)
DelPostcode                 STRING(30)
DelTelephone                STRING(30)
DelFax                      STRING(30)
AccountNumber               STRING(30)
PurchaseOrderNumber         STRING(30)
ContactName                 STRING(30)
CourierCost                 REAL
TotalLineCost               REAL
Vat                         REAL
Total                       REAL
InvoiceText                 STRING(255)
StandardText                STRING(255)
PaymentMethod               STRING(30)
UseEuro                     BYTE
EuroRate                    REAL
                         END
                     END                       

Parts                FILE,DRIVER('TOPSPEED'),OEM,NAME('parts.tps'),PRE(INV2),CREATE,BINDABLE,THREAD
InvoiceNumberKey         KEY(INV2:InvoiceNumber,INV2:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
InvoiceNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
ExchangeValue               REAL
ItemCost                    REAL
Quantity                    LONG
LineCost                    REAL
                         END
                     END                       

InvoiceAlias         FILE,DRIVER('TOPSPEED'),OEM,NAME('invoice.tps'),PRE(invali),CREATE,BINDABLE,THREAD
InvoiceNumberKey         KEY(invali:InvoiceNumber),DUP,NOCASE
Record                   RECORD,PRE()
InvoiceNumber               LONG
DefName                     STRING(30)
DefAdd1                     STRING(30)
DefAdd2                     STRING(30)
DefAdd3                     STRING(30)
DefPostcode                 STRING(30)
DefTelephone                STRING(30)
DefFax                      STRING(30)
DefEmail                    STRING(255)
VatNumber                   STRING(30)
DateCreated                 DATE
SaleNumber                  LONG
InvCompanyName              STRING(30)
InvAdd1                     STRING(30)
InvAdd2                     STRING(30)
InvAdd3                     STRING(30)
InvPostcode                 STRING(30)
InvTelephone                STRING(30)
InvFax                      STRING(30)
DelCompanyName              STRING(30)
DelAdd1                     STRING(30)
DelAdd2                     STRING(30)
DelAdd3                     STRING(30)
DelPostcode                 STRING(30)
DelTelephone                STRING(30)
DelFax                      STRING(30)
AccountNumber               STRING(30)
PurchaseOrderNumber         STRING(30)
ContactName                 STRING(30)
CourierCost                 REAL
TotalLineCost               REAL
Vat                         REAL
Total                       REAL
InvoiceText                 STRING(255)
StandardText                STRING(255)
PaymentMethod               STRING(30)
UseEuro                     BYTE
EuroRate                    REAL
                         END
                     END                       



Access:INVIMP        &FileManager
Relate:INVIMP        &RelationManager
Access:Invoice       &FileManager
Relate:Invoice       &RelationManager
Access:Parts         &FileManager
Relate:Parts         &RelationManager
Access:InvoiceAlias  &FileManager
Relate:InvoiceAlias  &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('invprint.INI')
  CPCSStartUpPrintDevice = PRINTER{PROPPRINT:DEVICE}
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill
    
FillCpcsIniStrings      PROCEDURE(CpcsIniFile,WindowMessage,PreviewDialogTitle,PreviewDialogText)
IniToUse      CSTRING(63)
  CODE
  IF CLIP(CpcsIniFile) <> ''
    IniToUse = CLIP(CpcsIniFile)
  ELSE
    IniToUse = 'WIN.INI'
  END
  CPCS:ProgWinTitlePrvw =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinTitlePrvw',       'Generating Report', IniToUse)
  CPCS:ProgWinTitlePrnt =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinTitlePrnt',       'Printing Report', IniToUse)
  CPCS:ProgWinPctText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinPctText',         '% Completed', IniToUse)
  CPCS:ProgWinRecText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinRecText',         'Records Read', IniToUse)
  CPCS:ProgWinUsrText   =  GETINI('DISPLAY STRINGS 2.0', 'ProgWinUsrText',         WindowMessage, IniToUse)
  CPCS:PrtrDlgTitle     =  GETINI('DISPLAY STRINGS 2.0', 'PrinterDlgTitle',        'Report Destination', IniToUse)
  CPCS:AskPrvwDlgTitle  =  GETINI('DISPLAY STRINGS 2.0', 'AskPrvwDialogTitle',     PreviewDialogTitle, IniToUse)
  CPCS:AskPrvwDlgText   =  GETINI('DISPLAY STRINGS 2.0', 'AskPrvwDialogText',      PreviewDialogText, IniToUse)
  CPCS:AreYouSureTitle  =  GETINI('DISPLAY STRINGS 2.0', 'CancelAreYouSureTitle',  'Question', IniToUse)
  CPCS:AreYouSureText   =  GETINI('DISPLAY STRINGS 2.0', 'CancelAreYouSureText',   'Are you sure you want to CANCEL this Report?', IniToUse)
  CPCS:PrvwPartialTitle =  GETINI('DISPLAY STRINGS 2.0', 'CancelPrvwPartialTitle', 'Question', IniToUse)
  CPCS:PrvwPartialText  =  GETINI('DISPLAY STRINGS 2.0', 'CancelPrvwPartialText',  'Report Cancellation Requested!||Preview Report generated so far?', IniToUse)
  CPCS:NoDfltPrtrTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NoDfltPrtrTitle',        'WARNING!', IniToUse)
  CPCS:NoDfltPrtrText   =  GETINI('DISPLAY STRINGS 2.0', 'NoDfltPrtrText',         'Warning:  No DEFAULT Printer Driver Found!||Printing may not occur correctly (or at all)|without a Default printer assigned.||Use CONTROL PANEL (PRINTERS) to set a|printer as your Default.||Continue with report anyway?', IniToUse)
  CPCS:NthgToPrvwTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrvwTitle',     'NOTE...', IniToUse)
  CPCS:NthgToPrvwText   =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrvwText',      'Nothing to Preview.', IniToUse)
  CPCS:NthgToPrntTitle  =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrntTitle',     'NOTE...', IniToUse)
  CPCS:NthgToPrntText   =  GETINI('DISPLAY STRINGS 2.0', 'NothingToPrntText',      'Nothing to Print.', IniToUse)
  CPCS:AsciiOutTitle    =  GETINI('DISPLAY STRINGS 2.0', 'AsciiOutTitle',          'Save to what Filename?', IniToUse)
  CPCS:AsciiOutMasks    =  GETINI('DISPLAY STRINGS 2.0', 'AsciiOutMasks',          'Text|*.TXT|All Files|*.*', IniToUse)
  CPCS:DynLblErrTitle   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrTitle',         'Problem!', IniToUse)
  CPCS:DynLblErrText1   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrText1',         'Sorry!||', IniToUse)
  CPCS:DynLblErrText2   =  GETINI('DISPLAY STRINGS 2.0', 'DynLblErrText2',         '||Does not support paper type:', IniToUse)




