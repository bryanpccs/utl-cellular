

   MEMBER('invprint.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('INVPR001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('INVPR002.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:InvoiceFile      STRING(255)
save_inv1_id         USHORT,AUTO
window               WINDOW('Multiple Retail Invoice Print Routine'),AT(,,311,71),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       MENUBAR
                         ITEM('Print Setup'),USE(?PrintSetup),STD(STD:PrintSetup)
                       END
                       SHEET,AT(4,4,304,36),USE(?Sheet1),SPREAD
                         TAB('Select Invoice File'),USE(?Tab1),COLOR(COLOR:Silver)
                           PROMPT('Invoice File'),AT(12,23),USE(?tmp:InvoiceFile:Prompt),TRN,FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(64,23,224,10),USE(glo:FileName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Invoice File'),TIP('Invoice File'),UPR
                           BUTTON,AT(292,24,10,10),USE(?LookupInvoiceFile),SKIP,FLAT,ICON('List3.ico')
                         END
                       END
                       PANEL,AT(4,44,304,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Close'),AT(248,48,56,16),USE(?Close),FLAT,LEFT,ICON('cancel.gif')
                       BUTTON('&Print'),AT(192,48,56,16),USE(?Print),FLAT,LEFT,ICON('Print.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup1          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:InvoiceFile:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  FileLookup1.Init
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:LongName)
  FileLookup1.SetMask('CSV File','*.CSV')
  FileLookup1.WindowTitle='Invoice File'
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupInvoiceFile
      ThisWindow.Update
      glo:FileName = Upper(FileLookup1.Ask(1)  )
      DISPLAY
    OF ?Print
      ThisWindow.Update
      Access:INVIMP.Open()
      If Access:INVIMP.UseFile() = Level:Benign
          Remove(invoice)
          Remove(parts)
      
          Relate:INVOICE.Open()
          Set(INVIMP,0)
          Loop
              Next(INVIMP)
              If Error()
                  Break
              End !If Error()
              If inv:Field1 = 'I'
                  If Access:Invoice.PrimeRecord() = Level:Benign
                      INV1:EuroRate            = inv:Field39
                      INV1:UseEuro             = inv:Field38
                      INV1:InvoiceNumber       = inv:Field2
                      INV1:DefName             = inv:Field3
                      INV1:DefAdd1             = inv:Field4
                      INV1:DefAdd2             = inv:Field5
                      INV1:DefAdd3             = inv:Field6
                      INV1:DefPostcode         = inv:Field7
                      INV1:DefTelephone        = inv:Field8
                      INV1:DefFax              = inv:Field9
                      INV1:DefEmail            = inv:Field10
                      INV1:VatNumber           = inv:Field11
                      INV1:DateCreated         = Deformat(inv:Field12,@d6)
                      INV1:SaleNumber          = inv:Field13
                      INV1:InvCompanyName      = inv:Field14
                      INV1:InvAdd1             = inv:Field15
                      INV1:InvAdd2             = inv:Field16
                      INV1:InvAdd3             = inv:Field17
                      INV1:InvPostcode         = inv:Field18
                      INV1:InvTelephone        = inv:Field19
                      INV1:InvFax              = inv:Field20
                      INV1:DelCompanyName      = inv:Field21
                      INV1:DelAdd1             = inv:Field22
                      INV1:DelAdd2             = inv:Field23
                      INV1:DelAdd3             = inv:Field24
                      INV1:DelPostcode         = inv:Field25
                      INV1:DelTelephone        = inv:Field26
                      INV1:DelFax              = inv:Field27
                      INV1:AccountNumber       = inv:Field28
                      INV1:PurchaseOrderNumber = inv:Field29
                      INV1:ContactName         = inv:Field30
      
                      If INV1:EuroRate
                          INV1:CourierCost         = Round(inv:Field31 * inv1:EuroRate,.01)
                          INV1:TotalLineCost       = Round(inv:Field32 * inv1:EuroRate,.01)
                          INV1:Vat                 = Round(inv:Field33 * inv1:EuroRate,.01)
                          INV1:Total               = Round(inv:Field34 * inv1:EuroRate,.01)
                      Else
                          INV1:CourierCost         = inv:Field31
                          INV1:TotalLineCost       = inv:Field32
                          INV1:Vat                 = inv:Field33
                          INV1:Total               = inv:Field34
                      End !If INV1:EuroRate
      
                      INV1:InvoiceText         = inv:Field35
                      INV1:StandardText        = inv:Field36
                      INV1:PaymentMethod       = inv:Field37
                      
                      
                      If Access:INVOICE.Insert()
                          Access:INVOICE.CancelAutoInc()
                      End !If Access:INVOICE.Insert()
                  End !If Access:Invoice.PrimeRecord() = Level:Benign
      
              End !If inv:Field1 = 'I'
      
              If inv:Field1 = 'P'
                  If Access:Parts.PrimeRecord() = Level:Benign
                      INV2:InvoiceNumber = inv:Field2
                      INV2:PartNumber    = inv:Field3
                      INV2:Description   = inv:Field4
                      INV2:Quantity      = inv:Field7
                      If inv1:UseEuro
                          INV2:ExchangeValue = Round(inv:Field5 * inv1:EuroRate,.01)
                          INV2:ItemCost      = Round(inv:Field6 * inv1:EuroRate,.01)
                          INV2:LineCost      = Round(inv:Field8 * inv1:EuroRate,.01)
                      Else !If inv1:UseEuro
                          INV2:ExchangeValue = inv:Field5
                          INV2:ItemCost      = inv:Field6
                          INV2:LineCost      = inv:Field8
                      End !If inv1:UseEuro
                      If Access:Parts.Insert()
                          Access:Parts.CancelAutoInc()
                      End
                  End !If Access:Parts.PrimeRecord() = Level:Benign
              End !If inv:Field1 = 'P'
          End !Loop
          Access:INVIMP.Close()
      
          BEEP(BEEP:SystemExclamation)  ;  YIELD()
          CASE MESSAGE('File Read.', |
                 'Invoice Import', ICON:Exclamation, |
                  BUTTON:OK, BUTTON:OK, 0)
          Of BUTTON:OK
          End !CASE
      
          Access:INVOICEALIAS.Open()
          Access:INVOICEALIAS.UseFile()
          Clear(invali:Record)
          Set(INVOICEALIAS,0)
          Loop
              If Access:InvoiceAlias.NEXT()
                 Break
              End !If
      !        stop(inv1:invoiceNumber)
              RetailInvoice(invali:InvoiceNumber)
          End !Loop
          Access:INVOICEALIAS.Close()
          Relate:INVOICE.Close()
      
          
      End !Access:INVIMP.UseFile() = Level:Benign
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

