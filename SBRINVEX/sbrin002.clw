

   MEMBER('sbrinvex.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('NetEmail.inc'),ONCE

                     MAP
                       INCLUDE('SBRIN002.INC'),ONCE        !Local module procedure declarations
                     END


MultipleInvoiceExport PROCEDURE                       !Generated from procedure template - Window

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       This is just an example of some of the functionality
            !       of the NetEmailSend object.
            !
            !       We have tried to keep it simple (no frills) as it's purpose
            !       is to be an example.
            !
            !       You will notice that the code has been commented to help you
            !       understand what we have done and why.
            !
            !       We strongly encourage you to read through the NetTalk documention
            !       and to work through at least one scenario (found in the documentation)
            !
            !       We hope this example code helps you to use NetTalk and so you are allowed
            !       to copy or modify the code in this procedure as much as you like.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

FilesOpened          BYTE
save_res_id          USHORT,AUTO
save_inv_id          USHORT,AUTO
MessageText          STRING(64000)
MessageHtml          STRING(64000)
ToField              STRING(1024)
From                 STRING(80)
CC                   STRING(1024)
BCC                  STRING(1024)
Subject              STRING(255)
Server               STRING(80)
Port                 USHORT(25)
AttachmentList       STRING(1024)
EmbedList            STRING(1024)
ReplyTo              STRING(80)
Organization         STRING(80)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:InvoiceType      BYTE(0)
! our data
StartPos             long
EndPos               long
ParamPath            string (255)
Param1               string (255)
count                long
tempFileList         string (Net:StdEmailAttachmentListSize)
Window               WINDOW('Multiple Retail Invoice Account Export'),AT(,,333,242),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),TIMER(10),SYSTEM,GRAY,MAX,DOUBLE,IMM
                       SHEET,AT(4,4,333,236),USE(?Sheet1),SPREAD
                         TAB('Multiple Invoice Account Export'),USE(?Tab1)
                           PROMPT('Invoice Start Date'),AT(8,20),USE(?tmp:StartDate:Prompt)
                           ENTRY(@d6),AT(84,20,64,10),USE(tmp:StartDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Invoice Start Date'),TIP('Invoice Start Date'),UPR
                           BUTTON,AT(152,20,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('Invoice End Date'),AT(8,36),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(84,36,64,10),USE(tmp:EndDate),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Invoice End Date'),TIP('Invoice End Date'),UPR
                           BUTTON,AT(152,36,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                           GROUP('Email Details'),AT(8,48,320,160),USE(?Group3),BOXED
                             STRING('Addresses format : Fred Smith <<fred@email.com>, Jane Smith <<jane@email.com>'),AT(16,56,304,8),USE(?String1),CENTER,FONT(,,COLOR:Green,)
                             PROMPT('From'),AT(12,68),USE(?ToField:Prompt:2)
                             ENTRY(@s80),AT(84,68,240,10),USE(From),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             PROMPT('To'),AT(12,80),USE(?ToField:Prompt)
                             ENTRY(@s255),AT(84,80,240,10),USE(ToField),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             PROMPT('Subject'),AT(12,96),USE(?Subject:Prompt)
                             ENTRY(@s255),AT(84,96,240,10),USE(Subject),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                             TEXT,AT(84,108,240,96),USE(MessageText),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           END
                           PROGRESS,USE(?OurProgress),HIDE,AT(5,231,324,6),RANGE(0,100)
                           STRING(''),AT(5,217,143,11),USE(?Status),TRN,CENTER
                           BUTTON('&Send'),AT(152,214,56,16),USE(?Send),LEFT,ICON('MAIL.GIF')
                           BUTTON('Close'),AT(272,214,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                           BUTTON('Abort'),AT(212,214,56,16),USE(?Abort),LEFT,ICON('FailClam.gif')
                         END
                       END
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Local Data Classes
ThisEmailSend        CLASS(NetEmailSend)              !Generated by NetTalk Extension (Class Definition)
ErrorTrap              PROCEDURE(string errorStr,string functionName),DERIVED
Init                   PROCEDURE(uLong Mode=NET:SimpleClient),DERIVED
MessageSent            PROCEDURE(),DERIVED

                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

EmailDefaults       File,Driver('BASIC'),Pre(EmailDef),NAME('INVEMAIL.INI'),Create,Bindable,Thread
Record                  Record
ToField                 String(255)
From                    String(255)
Subject                 String(255)
MessageText             String(255)
                        End
                    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:StartDate:Prompt{prop:FontColor} = -1
    ?tmp:StartDate:Prompt{prop:Color} = 15066597
    If ?tmp:StartDate{prop:ReadOnly} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 15066597
    Elsif ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 8454143
    Else ! If ?tmp:StartDate{prop:Req} = True
        ?tmp:StartDate{prop:FontColor} = 65793
        ?tmp:StartDate{prop:Color} = 16777215
    End ! If ?tmp:StartDate{prop:Req} = True
    ?tmp:StartDate{prop:Trn} = 0
    ?tmp:StartDate{prop:FontStyle} = font:Bold
    ?tmp:EndDate:Prompt{prop:FontColor} = -1
    ?tmp:EndDate:Prompt{prop:Color} = 15066597
    If ?tmp:EndDate{prop:ReadOnly} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 15066597
    Elsif ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 8454143
    Else ! If ?tmp:EndDate{prop:Req} = True
        ?tmp:EndDate{prop:FontColor} = 65793
        ?tmp:EndDate{prop:Color} = 16777215
    End ! If ?tmp:EndDate{prop:Req} = True
    ?tmp:EndDate{prop:Trn} = 0
    ?tmp:EndDate{prop:FontStyle} = font:Bold
    ?Group3{prop:Font,3} = -1
    ?Group3{prop:Color} = 15066597
    ?Group3{prop:Trn} = 0
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?ToField:Prompt:2{prop:FontColor} = -1
    ?ToField:Prompt:2{prop:Color} = 15066597
    If ?From{prop:ReadOnly} = True
        ?From{prop:FontColor} = 65793
        ?From{prop:Color} = 15066597
    Elsif ?From{prop:Req} = True
        ?From{prop:FontColor} = 65793
        ?From{prop:Color} = 8454143
    Else ! If ?From{prop:Req} = True
        ?From{prop:FontColor} = 65793
        ?From{prop:Color} = 16777215
    End ! If ?From{prop:Req} = True
    ?From{prop:Trn} = 0
    ?From{prop:FontStyle} = font:Bold
    ?ToField:Prompt{prop:FontColor} = -1
    ?ToField:Prompt{prop:Color} = 15066597
    If ?ToField{prop:ReadOnly} = True
        ?ToField{prop:FontColor} = 65793
        ?ToField{prop:Color} = 15066597
    Elsif ?ToField{prop:Req} = True
        ?ToField{prop:FontColor} = 65793
        ?ToField{prop:Color} = 8454143
    Else ! If ?ToField{prop:Req} = True
        ?ToField{prop:FontColor} = 65793
        ?ToField{prop:Color} = 16777215
    End ! If ?ToField{prop:Req} = True
    ?ToField{prop:Trn} = 0
    ?ToField{prop:FontStyle} = font:Bold
    ?Subject:Prompt{prop:FontColor} = -1
    ?Subject:Prompt{prop:Color} = 15066597
    If ?Subject{prop:ReadOnly} = True
        ?Subject{prop:FontColor} = 65793
        ?Subject{prop:Color} = 15066597
    Elsif ?Subject{prop:Req} = True
        ?Subject{prop:FontColor} = 65793
        ?Subject{prop:Color} = 8454143
    Else ! If ?Subject{prop:Req} = True
        ?Subject{prop:FontColor} = 65793
        ?Subject{prop:Color} = 16777215
    End ! If ?Subject{prop:Req} = True
    ?Subject{prop:Trn} = 0
    ?Subject{prop:FontStyle} = font:Bold
    If ?MessageText{prop:ReadOnly} = True
        ?MessageText{prop:FontColor} = 65793
        ?MessageText{prop:Color} = 15066597
    Elsif ?MessageText{prop:Req} = True
        ?MessageText{prop:FontColor} = 65793
        ?MessageText{prop:Color} = 8454143
    Else ! If ?MessageText{prop:Req} = True
        ?MessageText{prop:FontColor} = 65793
        ?MessageText{prop:Color} = 16777215
    End ! If ?MessageText{prop:Req} = True
    ?MessageText{prop:Trn} = 0
    ?MessageText{prop:FontStyle} = font:Bold
    ?Status{prop:FontColor} = -1
    ?Status{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('MultipleInvoiceExport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:StartDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Relate:STANTEXT.Open
  Access:RETSTOCK.UseFile
  SELF.FilesOpened = True
  Open(EmailDefaults)
  If Error()
      Create(EmailDefaults)
      Open(EmailDefaults)
  End !Error()
  
  Set(EmailDefaults)
  Next(EmailDefaults)
  If ~Error()
      ToField  = emaildef:ToField
      From    = emaildef:From
      Subject = emaildef:Subject
      MessageText = emaildef:MessageText
  
  End !Error()
  Close(EmailDefaults)
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Today()
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
                                               ! Generated by NetTalk Extension (Start)
  ThisEmailSend.init()
  if ThisEmailSend.error <> 0
    ! Put code in here to handle if the object does not initialise properly
  end
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  ! Load the users last settings
  ThisEmailSend.ProgressControl = ?OurProgress
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Store the users settings for next time
  Open(EmailDefaults)
  If Error()
      Create(EmailDefaults)
      Open(EmailDefaults)
  End !Error()
  
  Set(EmailDefaults)
  Next(EmailDefaults)
  If ~Error()
      emaildef:ToField    = ToField
      emaildef:Subject    = Subject
      emaildef:MessageText    = MessageText
      emaildef:From           = From
      Put(EmailDefaults)
  End !Error()
  Close(EmailDefaults)
  setcursor
  
  ThisEmailSend.Kill()                              ! Generated by NetTalk Extension
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
    Relate:STANTEXT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?Send
      !!Export File Layout
      !Type
      !Invoice Number
      !Default Name
      !Default Add 1
      !Default Add 2
      !Default Add 3
      !Default Postcode
      !Default Tel
      !Default Fax
      !Default Email
      !Vat Number
      !Date
      !Sale Number
      !Invoice Name
      !Invoice Add 1
      !Invoice Add 2
      !Invoice Add 3
      !Invoice Postcode
      !Invoice Tel
      !Invoice Fax
      !Delivery Name
      !Delivery Add 1
      !Delivery Add 2
      !Delivery Add 3
      !Delivery Postcode
      !Delivery Tel
      !Delivery Fax
      !Account Number
      !Purchase Order Number
      !Contact Name
      !Courier Cost
      !Total Line Cost
      !Vat
      !Total
      !Invoice Text
      !Payment Method
      !Euro
      !Euro Rate
      !
      !=====Parts====
      !Type
      !Invoice Number
      !Part Number
      !Description
      !Exchange Value
      !Item Cost
      !Quantity
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       You need to set up all the details of the server you want to
            !       connect to as well as the content of your email.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      Set(DEFAULT2)
      Access:DEFAULT2.Next()
      
      glo:File_Name    = 'INV' & Format(Today(),@d7) & '.CSV'
      Remove(glo:File_Name)
      Access:EXPGEN.Open()
      Access:EXPGEN.UseFile()
      
      Setcursor(Cursor:Wait)
      RecordCount# = 0
      Save_inv_ID = Access:INVOICE.SaveFile()
      Access:INVOICE.ClearKey(inv:Date_Created_Key)
      inv:Date_Created = tmp:StartDate
      Set(inv:Date_Created_Key,inv:Date_Created_Key)
      Loop
          If Access:INVOICE.NEXT()
             Break
          End !If
          If inv:Date_Created > tmp:EndDate      |
              Then Break.  ! End If
          If inv:Invoice_Type <> 'RET'
              Cycle
          End !If inv:Invoice_Type <> 'RET'
      
          RecordCount# += 1
          If RecordCount# > 2000
              RecordCount# = Records(INVOICE)
              Break
          End !If RecordCount# > 2000
      End !Loop
      Access:INVOICE.RestoreFile(Save_inv_ID)
      
      SetCursor()
      If RecordCount# = 0
          Case MessageEx('There are no records that match the criteria.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !RecordCount# = 0
      
      
      
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
          thiswindow.reset(1)
          open(progresswindow)
      
          ?progress:userstring{prop:text} = 'Running...'
          ?progress:pcttext{prop:text} = '0% Completed'
      
      
          recordstoprocess    = RecordCount#
      
          Save_inv_ID = Access:INVOICE.SaveFile()
          Access:INVOICE.ClearKey(inv:Date_Created_Key)
          inv:Date_Created = tmp:StartDate
          Set(inv:Date_Created_Key,inv:Date_Created_Key)
          Loop
              If Access:INVOICE.NEXT()
                 Break
              End !If
              If inv:Date_Created > tmp:EndDate      |
                  Then Break.  ! End If
              Do GetNextRecord2
              cancelcheck# += 1
              If cancelcheck# > (RecordsToProcess/100)
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
                  cancelcheck# = 0
              End!If cancelcheck# > 50
      
              If inv:Invoice_Type <> 'RET'
                  Cycle
              End !If inv:Invoice_Type <> 'RET'
      
              Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
              ret:Invoice_Number = inv:Invoice_Number
              If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                  !Found
                  Clear(gen:Record)
                  gen:Line1   = '"I' !Type
                  gen:Line1   = Clip(gen:Line1) & '","' & inv:Invoice_Number
      
                  If inv:UseAlternativeAddress
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2CompanyName
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2AddressLine1
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2AddressLine2
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2AddressLine3
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2Postcode
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2TelephoneNumber
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2FaxNumber
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2EmailAddress
                      gen:Line1   = Clip(gen:Line1) & '","' & de2:Inv2VATNumber
      
                  Else !If inv:UseAlternativeAddress
                      If def:use_invoice_address = 'YES'
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_Company_Name
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_Address_Line1
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_Address_Line2
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_Address_Line3
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_Postcode
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_Telephone_Number
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_Fax_Number
                          gen:Line1   = Clip(gen:Line1) & '","' & def:InvoiceEmailAddress
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Invoice_VAT_Number
                      Else!If def:use_invoice_address = 'YES'
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:User_Name
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Address_Line1
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Address_Line2
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Address_Line3
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Postcode
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Telephone_Number
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:Fax_Number
                          gen:Line1   = Clip(gen:Line1) & '","' & def:EmailAddress
                          gen:Line1   = Clip(gen:Line1) & '","' & DEF:VAT_Number
                      End!If def:use_invoice_address = 'YES'
      
                  End !If inv:UseAlternativeAddress
      
                  gen:Line1   = Clip(gen:Line1) & '","' & Format(inv:Date_Created,@d6)
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Ref_Number
      
                  If ret:payment_method = 'CAS'
                      gen:Line1   = Clip(gen:Line1) & '","' & ret:contact_name
                  Else!If ret:payment_method = 'CAS'
                      gen:Line1   = Clip(gen:Line1) & '","' & ret:company_name
                  End!If ret:payment_method = 'CAS'
      
                  If ret:building_name    = ''
                      gen:Line1   = Clip(gen:Line1) & '","' & RET:Address_Line1
                  Else!    If ret:building_name    = ''
                      gen:Line1   = Clip(gen:Line1) & '","' & Clip(RET:Building_Name) & ' ' & Clip(RET:Address_Line1)
                  End!
      
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:address_Line2
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:address_Line3
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:postcode
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Telephone_Number
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Fax_Number
      
      
                  If ret:payment_method = 'CAS'
                      gen:Line1   = Clip(gen:Line1) & '","' & ret:contact_name
                  Else!If ret:payment_method = 'CAS'
                      gen:Line1   = Clip(gen:Line1) & '","' & RET:Company_Name_Delivery
                  End!If ret:payment_method = 'CAS'
      
                  If ret:building_name_delivery   = ''
                      gen:Line1   = Clip(gen:Line1) & '","' & RET:Address_Line1_Delivery
                  Else!    If ret:building_name_delivery   = ''
                      gen:Line1   = Clip(gen:Line1) & '","' & Clip(RET:Building_Name_Delivery) & ' ' & Clip(RET:Address_Line1_Delivery)
                  End!
      
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:address_Line2_Delivery
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:address_Line3_Delivery
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:postcode_Delivery
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Telephone_Delivery
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Fax_Number_Delivery
      
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Account_Number
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Purchase_Order_Number
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Contact_Name
                  gen:Line1   = Clip(gen:Line1) & '","' & inv:Courier_Paid
                  gen:Line1   = Clip(gen:Line1) & '","' & inv:Parts_Paid
                  gen:Line1   = Clip(gen:Line1) & '","' & Round((inv:courier_paid + inv:parts_paid) * inv:Vat_Rate_Retail/100,.01)
                  gen:Line1   = Clip(gen:Line1) & '","' & inv:courier_paid + Round(Round((inv:courier_paid + inv:parts_paid) * inv:Vat_Rate_Retail/100,.01),.01) + inv:parts_paid
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Invoice_Text
                  Access:STANTEXT.ClearKey(stt:Description_Key)
                  stt:Description = 'INVOICE - RETAIL'
                  If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
                      !Found
      
                  Else!If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STANTEXT.TryFetch(stt:Description_Key) = Level:Benign
                  gen:Line1   = Clip(gen:Line1) & '","' & stt:Text
                  gen:Line1   = Clip(gen:Line1) & '","' & ret:Payment_Method
                  gen:Line1   = Clip(gen:Line1) & '","' & inv:UseAlternativeAddress
                  gen:Line1   = Clip(gen:Line1) & '","' & inv:EuroExhangeRate & '"'
      
      
                  Access:EXPGEN.Insert()
      
                  Save_res_ID = Access:RETSTOCK.SaveFile()
                  Access:RETSTOCK.ClearKey(res:Part_Number_Key)
                  res:Ref_Number  = ret:Ref_Number
                  Set(res:Part_Number_Key,res:Part_Number_Key)
                  Loop
                      If Access:RETSTOCK.NEXT()
                         Break
                      End !If
                      If res:Ref_Number  <> ret:Ref_Number      |
                          Then Break.  ! End If
                      If res:Despatched <> 'YES'
                          Cycle
                      End !If res:Despatched <> 'YES'
      
                      Clear(gen:Record)
                      gen:Line1   = '"P'
                      gen:Line1   = Clip(gen:Line1) & '","' & inv:Invoice_Number
                      gen:Line1   = Clip(gen:Line1) & '","' & res:Part_Number
                      gen:Line1   = Clip(gen:Line1) & '","' & res:Description
                      gen:Line1   = Clip(gen:Line1) & '","' & res:AccessoryCost * res:Quantity
                      gen:Line1   = Clip(gen:Line1) & '","' & res:Item_Cost
                      gen:Line1   = Clip(gen:Line1) & '","' & res:Quantity
                      gen:Line1   = Clip(gen:Line1) & '","' & Round(res:Item_Cost * res:Quantity,.01) & '"'
                      
                      Access:EXPGEN.Insert()
                  End !Loop
                  Access:RETSTOCK.RestoreFile(Save_res_ID)
      
              Else!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:RETSALES.TryFetch(ret:Invoice_Number_Key) = Level:Benign
      
      
          End !Loop
          Access:INVOICE.RestoreFile(Save_inv_ID)
      
          Do EndPrintRun
          close(progresswindow)
      
          ACcess:EXPGEN.Close()
      
      
          ThisEmailSend.Server = Clip(def:EmailServerAddress)
          ThisEmailSend.Port = Clip(def:EmailServerPort)
          ThisEmailSend.ToList = ToField
          ThisEmailSend.ccList = ''
          ThisEmailSend.bccList = ''
          ThisEmailSend.From = From
          ThisEmailSend.Subject = Subject
          ThisEmailSend.ReplyTo = ReplyTo
          ThisEmailSend.Organization = Organization
          ThisEmailSend.References = ''     ! Used for replies e.g. '<<00cd01c02dde$765a6880$0802a8c0@spiff> <<00dc01c02de0$35fbea00$0802a8c0@spiff>'
          ThisEmailSend.AttachmentList = Clip(glo:File_Name)!AttachmentList
          ThisEmailSend.EmbedList = EmbedList     ! New 6/3/2001
                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                !
                ! NOTE TO PROGRAMMERS
                !       You must call the SetRequiredMessageSize before
                !       populating <object>.MessageText, MessageHTML or WholeMessage
                !
                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          ThisEmailSend.SetRequiredMessageSize (0, len(clip(MessageText)), len(clip(MessageHTML))) 
          if ThisEmailSend.Error = 0
            ThisEmailSend.MessageText = MessageText
            if len(clip(MessageHTML)) > 0
              ThisEmailSend.MessageHTML = MessageHTML
            end
            setcursor (CURSOR:WAIT)
            display()
                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                !
                ! NOTE TO PROGRAMMERS
                !       The SendMail() method will queue your message and start
                !       sending it.
                !
                !       This is an Asynchronous function. This means it returns to you
                !       but your email has not finished being sent yet. You can send emails
                !       one after another and they are just kept in the queue and sent out as
                !       fast as the Mail Server will take them.
                !
                !       The MailSent() method is called each time an email is successfully sent
                !       to the SMTP server.
                !
                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            ThisEmailSend.SendMail(NET:EMailMadeFromPartsMode)  ! Put email in queue and start sending it now
            if ThisEmailSend.error <> 0
              ! Handle Send Error (This error is a connection error - not a sending error)
              ?status{prop:text} = 'Could not connect to email server.'
            else
              if records (ThisEmailSend.DataQueue) > 0
                ?status{prop:text} = 'Sending ' & records (ThisEmailSend.DataQueue) & ' email(s)'
              else
                ?status{prop:text} = ''
              end
            end
            setcursor
            display()
          end
      
      End !RecordCount# = 0
    OF ?Close
      PUTINI('Email','EmailTo',Clip(ToField),Clip(Path()) & '\INVEMAIL.INI')
      PUTINI('Email','Subject',Clip(Subject),Clip(Path()) & '\INVEMAIL.INI')
      PUTINI('Email','MessageText',Clip(MessageText),Clip(Path()) & '\INVEMAIL.INI')
      post (event:closewindow)
    OF ?Abort
      ThisEmailSend.Abort    ! Kill the connection now. This is different from Close, which does a graceful close. Abort stops the lot right now.
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisEmailSend.TakeEvent()                 ! Generated by NetTalk Extension
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       When the user tries to close the window, we test to
            !       see if there are still emails being sent.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if records (ThisEmailSend.DataQueue) > 0
        if Message ('The email is still being sent.|Are you sure you want to quit?','NetTalk Send Email ',ICON:Question,BUTTON:Yes+BUTTON:No,BUTTON:No) = Button:No
          cycle
        end
      end
    OF EVENT:Timer
      ThisEmailSend.CalcProgress()
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisEmailSend.ErrorTrap PROCEDURE(string errorStr,string functionName)


  CODE
  PARENT.ErrorTrap(errorStr,functionName)
  ?status{prop:text} = 'An error occurred.'
  display


ThisEmailSend.Init PROCEDURE(uLong Mode=NET:SimpleClient)


  CODE
  PARENT.Init(Mode)
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       You can tell the object to use the different Mime Formats
            !       for encoding text or HTML.
            !       The options are:
            !         'quoted-printable'  - (Microsoft default) all characters are trasmitted and sent in a way that is humanly friendly
            !         '7bit'   - 7bit characters are sent
            !         '8bit'   - all 8bit characters are sent, some SMTP servers may choke on these characters though
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  self.OptionsMimeTextTransferEncoding = 'quoted-printable'           ! '7bit', '8bit' or 'quoted-printable'
  self.OptionsMimeHtmlTransferEncoding = 'quoted-printable'           ! '7bit', '8bit' or 'quoted-printable'
  
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       You can set other settings by uncommenting
            !       the following lines
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
  !self.OptionsKeepConnectionOpen = true
  self.ReconnectAfterXMsgs = 20
  self.XMailer = 'NetDemo Application'
  !self.SuppressErrorMsg = true
  


ThisEmailSend.MessageSent PROCEDURE


  CODE
  PARENT.MessageSent
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            !
            ! NOTE TO PROGRAMMERS
            !       The number of records still to send
            !       = records (ThisEmailSend.DataQueue) - 1
            !
            !       The email just sent is still in the queue, so
            !       that you can access it's properties if you need.
            !
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    if records (ThisEmailSend.DataQueue) > 1
      ?status{prop:text} = 'Sending ' & (records (ThisEmailSend.DataQueue) - 1) & ' email(s)'
    else
      ?status{prop:text} = ''
      Message ('Email(s) Sent Successfully', 'NetTalk Email Send Demo', ICON:ASTERISK)
    end


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

