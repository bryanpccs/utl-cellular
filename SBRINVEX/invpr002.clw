

   MEMBER('invprint.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('INVPR002.INC'),ONCE        !Local module procedure declarations
                     END








RetailInvoice PROCEDURE(func:InvoiceNumber)
RejectRecord         LONG,AUTO
save_inv1_id         USHORT,AUTO
save_inv2_id         USHORT,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
tmp:InvoiceNumber    LONG
tmp:TotalExchangeValue STRING(20)
tmp:Today            DATE
!-----------------------------------------------------------------------------
Process:View         VIEW(Invoice)
                       PROJECT(INV1:AccountNumber)
                       PROJECT(INV1:ContactName)
                       PROJECT(INV1:CourierCost)
                       PROJECT(INV1:DateCreated)
                       PROJECT(INV1:DefAdd1)
                       PROJECT(INV1:DefAdd2)
                       PROJECT(INV1:DefAdd3)
                       PROJECT(INV1:DefEmail)
                       PROJECT(INV1:DefFax)
                       PROJECT(INV1:DefName)
                       PROJECT(INV1:DefPostcode)
                       PROJECT(INV1:DefTelephone)
                       PROJECT(INV1:DelAdd1)
                       PROJECT(INV1:DelAdd2)
                       PROJECT(INV1:DelAdd3)
                       PROJECT(INV1:DelCompanyName)
                       PROJECT(INV1:DelFax)
                       PROJECT(INV1:DelPostcode)
                       PROJECT(INV1:DelTelephone)
                       PROJECT(INV1:InvAdd1)
                       PROJECT(INV1:InvAdd2)
                       PROJECT(INV1:InvAdd3)
                       PROJECT(INV1:InvCompanyName)
                       PROJECT(INV1:InvFax)
                       PROJECT(INV1:InvPostcode)
                       PROJECT(INV1:InvTelephone)
                       PROJECT(INV1:InvoiceNumber)
                       PROJECT(INV1:InvoiceText)
                       PROJECT(INV1:PurchaseOrderNumber)
                       PROJECT(INV1:SaleNumber)
                       PROJECT(INV1:StandardText)
                       PROJECT(INV1:Total)
                       PROJECT(INV1:TotalLineCost)
                       PROJECT(INV1:Vat)
                       PROJECT(INV1:VatNumber)
                     END
report               REPORT,AT(396,4573,7521,4292),PAPER(PAPER:A4),PRE(rpt),FONT('Arial',10,,FONT:regular),THOUS
                       HEADER,AT(354,490,7521,10896),USE(?unnamed)
                         STRING(@s30),AT(156,83,3844,240),USE(INV1:DefName),LEFT,FONT(,16,,FONT:bold)
                         STRING(@s30),AT(156,323,3844,156),USE(INV1:DefAdd1),TRN
                         STRING(@s30),AT(156,479,3844,156),USE(INV1:DefAdd2),TRN
                         STRING(@s30),AT(156,635,3844,156),USE(INV1:DefAdd3),TRN
                         STRING(@s30),AT(156,802,1156,156),USE(INV1:DefPostcode),TRN
                         STRING('Tel:'),AT(156,958),USE(?String16),TRN
                         STRING(@s30),AT(677,958),USE(INV1:DefTelephone)
                         STRING(@d6),AT(5990,938),USE(tmp:Today),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax: '),AT(156,1094),USE(?String19),TRN
                         STRING(@s30),AT(677,1094),USE(INV1:DefFax)
                         STRING(@s255),AT(677,1250,3844,208),USE(INV1:DefEmail),TRN
                         STRING('Email:'),AT(156,1250),USE(?String19:2),TRN
                         STRING('Date Printed:'),AT(5000,938),USE(?string22),TRN,FONT(,8,,)
                         STRING('Tel:'),AT(4063,2500),USE(?string22:4),TRN,FONT(,8,,)
                         STRING('INVOICE:'),AT(4896,52),USE(?string22:3),TRN,FONT(,16,,FONT:bold)
                         STRING(@s8),AT(5990,52),USE(INV1:InvoiceNumber),TRN,LEFT,FONT('Arial',16,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(5990,469),USE(INV1:VatNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('V.A.T. Number:'),AT(5000,469),USE(?string22:12),TRN,FONT(,8,,)
                         STRING(@s8),AT(5990,1094),USE(INV1:SaleNumber),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Date Of Invoice:'),AT(5000,625),USE(?String65),TRN,FONT(,8,,)
                         STRING(@d6),AT(5990,625),USE(INV1:DateCreated),TRN,FONT(,8,,FONT:bold)
                         STRING('Sale Number:'),AT(5000,1094),USE(?string22:10),TRN,FONT(,8,,)
                         STRING('Account No'),AT(208,3177),USE(?string22:6),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(208,3385,1354,156),USE(INV1:AccountNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Purch Order No'),AT(1615,3177),USE(?string22:7),TRN,FONT(,8,,FONT:bold)
                         STRING('Contact Name'),AT(3125,3177),USE(?string22:11),TRN,FONT(,8,,FONT:bold)
                         STRING(@s30),AT(1615,3385,1458,208),USE(INV1:PurchaseOrderNumber),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(3125,3385),USE(INV1:ContactName),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(2917,8750,1719,781),USE(INV1:InvoiceText),FONT(,8,,FONT:bold)
                         STRING(@s30),AT(4063,1719),USE(INV1:DelCompanyName),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1719),USE(INV1:InvCompanyName),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,1875,3281,208),USE(INV1:DelAdd1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,1875,3646,208),USE(INV1:InvAdd1),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2031),USE(INV1:DelAdd2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2031),USE(INV1:InvAdd2),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4271,2500),USE(INV1:DelTelephone),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Tel:'),AT(208,2500),USE(?string22:9),TRN,FONT(,8,,)
                         STRING(@s30),AT(469,2500),USE(INV1:InvTelephone),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(4063,2656),USE(?string22:5),TRN,FONT(,8,,)
                         STRING(@s30),AT(4063,2344),USE(INV1:DelPostcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2344),USE(INV1:InvPostcode),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4063,2188),USE(INV1:DelAdd3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(208,2188),USE(INV1:InvAdd3),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING(@s30),AT(4271,2656),USE(INV1:DelFax),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Fax:'),AT(208,2656),USE(?string22:8),TRN,FONT(,8,,)
                         STRING(@s30),AT(469,2656),USE(INV1:InvFax),TRN,LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                       END
break2                 BREAK(endofreport),USE(?unnamed:2)
detail2                  DETAIL,AT(,,,208),USE(?unnamed:5)
                           STRING(@s8),AT(5573,0),USE(INV2:Quantity),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(6510,0),USE(INV2:LineCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(3542,0),USE(INV2:ExchangeValue),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@n14.2),AT(4427,0),USE(INV2:ItemCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(156,0,1510,208),USE(INV2:PartNumber),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                           STRING(@s30),AT(1771,0,1719,208),USE(INV2:Description),TRN,LEFT,FONT('Arial',8,,,CHARSET:ANSI)
                         END
                         FOOTER,AT(0,0),USE(?unnamed:6)
                           STRING('Total Exchage Value:'),AT(2240,104),USE(?TotalExchangeValue),TRN,HIDE,FONT(,8,,)
                           STRING(@n14.2),AT(4427,104),USE(tmp:TotalExchangeValue),TRN,HIDE,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(2052,313,3427,0),USE(?TotalExchangeLine),HIDE,COLOR(COLOR:Black)
                           STRING('Warranty Note: You may raise a warranty claim to a value equal to the total exch' &|
   'ange value detailed above'),AT(156,365,7240,208),USE(?WarrantyNote),TRN,HIDE,CENTER
                           LINE,AT(156,52,7188,0),USE(?TotalExchangeLine2),HIDE,COLOR(COLOR:Black)
                         END
                       END
TotalBand              DETAIL,AT(300,9200),USE(?TotalBand),ABSOLUTE
                         STRING('Courier Cost:'),AT(4823,52),USE(?CourierCost),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6510,52),USE(INV1:CourierCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING(@n14.2),AT(6510,219),USE(INV1:TotalLineCost),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('Total Line Cost (Ex VAT)'),AT(4823,260),USE(?TotalLineCost),TRN,FONT(,8,,)
                         STRING(@n14.2),AT(6510,427),USE(INV1:Vat),TRN,RIGHT,FONT('Arial',8,,,CHARSET:ANSI)
                         STRING('V.A.T.:'),AT(4823,469),USE(?VAT),TRN,FONT(,8,,)
                         LINE,AT(6302,625,1042,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Total (Inc VAT):'),AT(4823,688),USE(?TotalVat),TRN,FONT(,8,,FONT:bold)
                         STRING(@n14.2),AT(6510,677),USE(INV1:Total),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('<128>'),AT(6469,677,52,208),USE(?Euro),TRN,HIDE,FONT(,8,,FONT:bold)
                         TEXT,AT(104,1146,7292,781),USE(INV1:StandardText),TRN,FONT(,8,,)
                       END
                       FOOTER,AT(365,9219,7521,198),USE(?unnamed:4)
                       END
                       FORM,AT(354,479,7521,11198),USE(?unnamed:3)
                         IMAGE('RINVDET.GIF'),AT(0,0,7521,11156),USE(?image1)
                         STRING('Quantity'),AT(5698,3854),USE(?string44),TRN,FONT(,8,,FONT:bold)
                         STRING('Part Number'),AT(156,3854),USE(?string45),TRN,FONT(,8,,FONT:bold)
                         STRING('Description'),AT(1792,3854),USE(?string24),TRN,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('Line Cost'),AT(6917,3823),USE(?LineCost1),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Item Cost'),AT(4740,3854),USE(?ItemCost),TRN,RIGHT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('(Ex.V.A.T.)'),AT(6896,3917),USE(?LineCost2),TRN,FONT('Arial',7,,FONT:bold,CHARSET:ANSI)
                         STRING('Exchange Value'),AT(3646,3854),USE(?ExchangeValue),TRN,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI)
                         STRING('INVOICE ADDRESS'),AT(208,1469),USE(?string44:2),TRN,FONT(,9,,FONT:bold)
                         STRING('DELIVERY ADDRESS'),AT(4063,1469),USE(?string44:3),TRN,FONT(,9,,FONT:bold)
                         STRING('PAYMENT DETAILS'),AT(104,8490),USE(?string44:4),TRN,HIDE,FONT(,9,,FONT:bold)
                         STRING('CHARGE DETAILS'),AT(4740,8490),USE(?string44:5),TRN,FONT(,9,,FONT:bold)
                         STRING('INVOICE TEXT'),AT(2906,8490),USE(?string44:6),TRN,FONT(,9,,FONT:bold)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CPCSDummyDetail         SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('RetailInvoice')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  PreviewReq = False
  BIND('tmp:InvoiceNumber',tmp:InvoiceNumber)
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:Invoice.Open
  Access:Parts.UseFile
  tmp:InvoiceNUmber   = func:InvoiceNumber
  
  
  RecordsToProcess = RECORDS(Invoice)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(Invoice,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      SET(INV1:InvoiceNumberKey)
      Process:View{Prop:Filter} = |
      'INV1:InvoiceNumber = tmp:InvoiceNumber'
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      OPEN(Process:View)
      IF ERRORCODE()
        StandardWarning(Warn:ViewOpenError)
      END
      LOOP
        DO GetNextRecord
        DO ValidateRecord
        CASE RecordStatus
          OF Record:Ok
            BREAK
          OF Record:OutOfRange
            LocalResponse = RequestCancelled
            BREAK
        END
      END
      IF LocalResponse = RequestCancelled
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        CYCLE
      END
      
      DO OpenReportRoutine
    OF Event:Timer
      LOOP RecordsPerCycle TIMES
        Save_INV2_ID = Access:Parts.SaveFile()
        Access:Parts.ClearKey(INV2:InvoiceNumberKey)
        INV2:InvoiceNumber = inv1:InvoiceNumber
        Set(INV2:InvoiceNumberKey,INV2:InvoiceNumberKey)
        Loop
            If Access:Parts.NEXT()
               Break
            End !If
            If INV2:InvoiceNumber <> inv1:InvoiceNumber      |
                Then Break.  ! End If
            Print(Rpt:Detail2)
            tmp:TotalExchangeValue += (INV2:ExchangeValue * INV2:Quantity)
            Count# = 1
        End !Loop
        Access:Parts.RestoreFile(Save_INV2_ID)
        PrintSkipDetails = FALSE
        
        
        
        IF ~PrintSkipDetails THEN
          PRINT(rpt:TotalBand)
        END
        
        Print(Rpt:TotalBand)
        LOOP
          DO GetNextRecord
          DO ValidateRecord
          CASE RecordStatus
            OF Record:OutOfRange
              LocalResponse = RequestCancelled
              BREAK
            OF Record:OK
              BREAK
          END
        END
        IF LocalResponse = RequestCancelled
          LocalResponse = RequestCompleted
          BREAK
        END
        LocalResponse = RequestCancelled
      END
      IF LocalResponse = RequestCompleted
        ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
      END
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(Invoice,'QUICKSCAN=off').
  IF ~ReportWasOpened
    DO OpenReportRoutine
  END
  IF ~CPCSDummyDetail
    SETTARGET(report)
    CPCSDummyDetail = LASTFIELD()+1
    CREATE(CPCSDummyDetail,CREATE:DETAIL)
    UNHIDE(CPCSDummyDetail)
    SETPOSITION(CPCSDummyDetail,,,,10)
    CREATE((CPCSDummyDetail+1),CREATE:STRING,CPCSDummyDetail)
    UNHIDE((CPCSDummyDetail+1))
    SETPOSITION((CPCSDummyDetail+1),0,0,0,0)
    (CPCSDummyDetail+1){PROP:TEXT}='X'
    SETTARGET
    PRINT(report,CPCSDummyDetail)
    LocalResponse=RequestCompleted
  END
  IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
     (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
    ENDPAGE(report)
    IF ~RECORDS(PrintPreviewQueue)
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    ELSE
      IF CPCSPgOfPgOption = True
        AssgnPgOfPg(PrintPreviewQueue)
      END
      PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
      HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
      report{PROP:FlushPreview} = True
    END
  ELSIF ~ReportWasOpened
    MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
  END
  CLOSE(report)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:Invoice.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')


ValidateRecord       ROUTINE
!|
!| This routine is used to provide for complex record filtering and range limiting. This
!| routine is only generated if you've included your own code in the EMBED points provided in
!| this routine.
!|
  RecordStatus = Record:OutOfRange
  IF LocalResponse = RequestCancelled THEN EXIT.
  RecordStatus = Record:OK
  EXIT

GetNextRecord ROUTINE
  NEXT(Process:View)
  IF ERRORCODE()
    IF ERRORCODE() <> BadRecErr
      StandardWarning(Warn:RecordFetchError,'Invoice')
    END
    LocalResponse = RequestCancelled
    EXIT
  ELSE
    LocalResponse = RequestCompleted
  END
  DO DisplayProgress


DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  CPCSPgOfPgOption = True
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  SetTarget(Report)
  tmp:Today   = Today()
  If inv1:PaymentMethod = 'EXC'
      ?couriercost{prop:Hide} = 1
      ?totallinecost{prop:Hide} = 1
      ?vat{prop:Hide} = 1
      ?totalvat{prop:Hide} = 1
  
      ?INV1:CourierCost{prop:Hide} = 1
      ?INV1:TotalLineCost{prop:Hide} = 1
      ?INV1:Vat{prop:Hide} = 1
      ?INV1:Total{prop:Hide} = 1
  
      ?INV2:ExchangeValue{prop:Hide} = 0
      ?INV2:LineCost{prop:Hide} = 1
  
      ?ExchangeValue{prop:Hide} = 0
      ?ItemCost{prop:Text} = 'Total'
      ?LineCost1{prop:Hide} = 1
      ?LineCost2{prop:Hide} = 1
      ?tmp:TotalExchangeValue{prop:Hide} = 0
      ?TotalExchangeValue{prop:Hide} = 0
      ?TotalExchangeLine{prop:Hide} = 0
      ?TotalExchangeLine2{prop:Hide} = 0
      ?WarrantyNote{prop:Hide} = 0
  Else !ret:Payment_Method = 'EXC'
      ?couriercost{prop:Hide} = 0
      ?totallinecost{prop:Hide} = 0
      ?vat{prop:Hide} = 0
      ?totalvat{prop:Hide} = 0
  
      ?INV1:CourierCost{prop:Hide} = 0
      ?INV1:TotalLineCost{prop:Hide} = 0
      ?INV1:Vat{prop:Hide} = 0
      ?INV1:Total{prop:Hide} = 0
  
      ?INV2:ExchangeValue{prop:Hide} = 1
      ?INV2:LineCost{prop:Hide} = 0
  
      ?ExchangeValue{prop:Hide} = 1
      ?LineCost1{prop:Hide} = 0
      ?LineCost2{prop:Hide} = 0
      ?tmp:TotalExchangeValue{prop:Hide} = 1
      ?TotalExchangeValue{prop:Hide} = 1
      ?TotalExchangeLine{prop:Hide} = 1
      ?TotalExchangeLine2{prop:Hide} = 1
      ?WarrantyNote{prop:Hide} = 1
  End !ret:Payment_Method = 'EXC'
  If inv1:UseEuro
      ?Euro{prop:Hide} = 0
  Else !inv1:UseEuro
      ?Euro{prop:Hide} = 1
  End !inv1:UseEuro
  SetTarget()
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='RetailInvoice'
  END
  IF (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END







