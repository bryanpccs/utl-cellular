

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01001.INC'),ONCE        !Local module procedure declarations
                     END


eps_defaults PROCEDURE                                !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?dee:Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?dee:Warranty_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?dee:Transit_Type
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?dee:Job_Priority
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?dee:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB2::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB4::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB5::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
FDCB8::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
Window               WINDOW('Siemens Import Defaults'),AT(,,231,204),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,168),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number'),AT(8,20),USE(?Prompt1)
                           COMBO(@s15),AT(84,20,124,10),USE(dee:Account_Number),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('65L(2)|M@s15@120L(2)@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Charge Type'),AT(8,36),USE(?Prompt2)
                           COMBO(@s30),AT(84,36,124,10),USE(dee:Warranty_Charge_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Transit Type'),AT(8,52),USE(?Prompt4)
                           COMBO(@s30),AT(84,52,124,10),USE(dee:Transit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           PROMPT('Job Turnaround Time'),AT(8,68),USE(?Prompt5)
                           COMBO(@s30),AT(84,68,124,10),USE(dee:Job_Priority),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           PROMPT('Unit Type'),AT(8,84),USE(?dee:UnitType:prompt)
                           COMBO(@s30),AT(84,84,124,10),USE(dee:Unit_Type),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5)
                           PROMPT('Status'),AT(8,100),USE(?dee:Status_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,100,124,10),USE(dee:Status_Type),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,100,10,10),USE(?LookupStatus),SKIP,ICON('List3.ico')
                           PROMPT('Delivery Text'),AT(8,116),USE(?dee:DeliveryText:Prompt)
                           TEXT,AT(84,116,124,48),USE(dee:Delivery_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,176,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,180,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(168,180,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?dee:Account_Number{prop:ReadOnly} = True
        ?dee:Account_Number{prop:FontColor} = 65793
        ?dee:Account_Number{prop:Color} = 15066597
    Elsif ?dee:Account_Number{prop:Req} = True
        ?dee:Account_Number{prop:FontColor} = 65793
        ?dee:Account_Number{prop:Color} = 8454143
    Else ! If ?dee:Account_Number{prop:Req} = True
        ?dee:Account_Number{prop:FontColor} = 65793
        ?dee:Account_Number{prop:Color} = 16777215
    End ! If ?dee:Account_Number{prop:Req} = True
    ?dee:Account_Number{prop:Trn} = 0
    ?dee:Account_Number{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?dee:Warranty_Charge_Type{prop:ReadOnly} = True
        ?dee:Warranty_Charge_Type{prop:FontColor} = 65793
        ?dee:Warranty_Charge_Type{prop:Color} = 15066597
    Elsif ?dee:Warranty_Charge_Type{prop:Req} = True
        ?dee:Warranty_Charge_Type{prop:FontColor} = 65793
        ?dee:Warranty_Charge_Type{prop:Color} = 8454143
    Else ! If ?dee:Warranty_Charge_Type{prop:Req} = True
        ?dee:Warranty_Charge_Type{prop:FontColor} = 65793
        ?dee:Warranty_Charge_Type{prop:Color} = 16777215
    End ! If ?dee:Warranty_Charge_Type{prop:Req} = True
    ?dee:Warranty_Charge_Type{prop:Trn} = 0
    ?dee:Warranty_Charge_Type{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?dee:Transit_Type{prop:ReadOnly} = True
        ?dee:Transit_Type{prop:FontColor} = 65793
        ?dee:Transit_Type{prop:Color} = 15066597
    Elsif ?dee:Transit_Type{prop:Req} = True
        ?dee:Transit_Type{prop:FontColor} = 65793
        ?dee:Transit_Type{prop:Color} = 8454143
    Else ! If ?dee:Transit_Type{prop:Req} = True
        ?dee:Transit_Type{prop:FontColor} = 65793
        ?dee:Transit_Type{prop:Color} = 16777215
    End ! If ?dee:Transit_Type{prop:Req} = True
    ?dee:Transit_Type{prop:Trn} = 0
    ?dee:Transit_Type{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?dee:Job_Priority{prop:ReadOnly} = True
        ?dee:Job_Priority{prop:FontColor} = 65793
        ?dee:Job_Priority{prop:Color} = 15066597
    Elsif ?dee:Job_Priority{prop:Req} = True
        ?dee:Job_Priority{prop:FontColor} = 65793
        ?dee:Job_Priority{prop:Color} = 8454143
    Else ! If ?dee:Job_Priority{prop:Req} = True
        ?dee:Job_Priority{prop:FontColor} = 65793
        ?dee:Job_Priority{prop:Color} = 16777215
    End ! If ?dee:Job_Priority{prop:Req} = True
    ?dee:Job_Priority{prop:Trn} = 0
    ?dee:Job_Priority{prop:FontStyle} = font:Bold
    ?dee:UnitType:prompt{prop:FontColor} = -1
    ?dee:UnitType:prompt{prop:Color} = 15066597
    If ?dee:Unit_Type{prop:ReadOnly} = True
        ?dee:Unit_Type{prop:FontColor} = 65793
        ?dee:Unit_Type{prop:Color} = 15066597
    Elsif ?dee:Unit_Type{prop:Req} = True
        ?dee:Unit_Type{prop:FontColor} = 65793
        ?dee:Unit_Type{prop:Color} = 8454143
    Else ! If ?dee:Unit_Type{prop:Req} = True
        ?dee:Unit_Type{prop:FontColor} = 65793
        ?dee:Unit_Type{prop:Color} = 16777215
    End ! If ?dee:Unit_Type{prop:Req} = True
    ?dee:Unit_Type{prop:Trn} = 0
    ?dee:Unit_Type{prop:FontStyle} = font:Bold
    ?dee:Status_Type:Prompt{prop:FontColor} = -1
    ?dee:Status_Type:Prompt{prop:Color} = 15066597
    If ?dee:Status_Type{prop:ReadOnly} = True
        ?dee:Status_Type{prop:FontColor} = 65793
        ?dee:Status_Type{prop:Color} = 15066597
    Elsif ?dee:Status_Type{prop:Req} = True
        ?dee:Status_Type{prop:FontColor} = 65793
        ?dee:Status_Type{prop:Color} = 8454143
    Else ! If ?dee:Status_Type{prop:Req} = True
        ?dee:Status_Type{prop:FontColor} = 65793
        ?dee:Status_Type{prop:Color} = 16777215
    End ! If ?dee:Status_Type{prop:Req} = True
    ?dee:Status_Type{prop:Trn} = 0
    ?dee:Status_Type{prop:FontStyle} = font:Bold
    ?dee:DeliveryText:Prompt{prop:FontColor} = -1
    ?dee:DeliveryText:Prompt{prop:Color} = 15066597
    If ?dee:Delivery_Text{prop:ReadOnly} = True
        ?dee:Delivery_Text{prop:FontColor} = 65793
        ?dee:Delivery_Text{prop:Color} = 15066597
    Elsif ?dee:Delivery_Text{prop:Req} = True
        ?dee:Delivery_Text{prop:FontColor} = 65793
        ?dee:Delivery_Text{prop:Color} = 8454143
    Else ! If ?dee:Delivery_Text{prop:Req} = True
        ?dee:Delivery_Text{prop:FontColor} = 65793
        ?dee:Delivery_Text{prop:Color} = 16777215
    End ! If ?dee:Delivery_Text{prop:Req} = True
    ?dee:Delivery_Text{prop:Trn} = 0
    ?dee:Delivery_Text{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'eps_defaults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'eps_defaults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Account_Number;  SolaceCtrlName = '?dee:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Warranty_Charge_Type;  SolaceCtrlName = '?dee:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Transit_Type;  SolaceCtrlName = '?dee:Transit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Job_Priority;  SolaceCtrlName = '?dee:Job_Priority';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:UnitType:prompt;  SolaceCtrlName = '?dee:UnitType:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Unit_Type;  SolaceCtrlName = '?dee:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Status_Type:Prompt;  SolaceCtrlName = '?dee:Status_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Status_Type;  SolaceCtrlName = '?dee:Status_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStatus;  SolaceCtrlName = '?LookupStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:DeliveryText:Prompt;  SolaceCtrlName = '?dee:DeliveryText:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?dee:Delivery_Text;  SolaceCtrlName = '?dee:Delivery_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('eps_defaults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'eps_defaults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:DEFEPS.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  SELF.FilesOpened = True
  Set(defeps)
  If access:defeps.next()
      get(defeps,0)
      if access:defeps.primerecord() = level:benign
          if access:defeps.insert()
              access:defeps.cancelautoinc()
          end
      end!if access:defeps.primerecord() = level:benign
  End!If access.defeps.next()
  
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?dee:Status_Type{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?dee:Status_Type{Prop:Tip}
  END
  IF ?dee:Status_Type{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?dee:Status_Type{Prop:Msg}
  END
  FDCB1.Init(dee:Account_Number,?dee:Account_Number,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(sub:Account_Number_Key)
  FDCB1.AddField(sub:Account_Number,FDCB1.Q.sub:Account_Number)
  FDCB1.AddField(sub:Company_Name,FDCB1.Q.sub:Company_Name)
  FDCB1.AddField(sub:RecordNumber,FDCB1.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  FDCB2.Init(dee:Warranty_Charge_Type,?dee:Warranty_Charge_Type,Queue:FileDropCombo:1.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo:1,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo:1
  FDCB2.AddSortOrder(cha:Warranty_Key)
  FDCB2.AddField(cha:Charge_Type,FDCB2.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB4.Init(dee:Transit_Type,?dee:Transit_Type,Queue:FileDropCombo:3.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:3,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:3
  FDCB4.AddSortOrder(trt:Transit_Type_Key)
  FDCB4.AddField(trt:Transit_Type,FDCB4.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(dee:Job_Priority,?dee:Job_Priority,Queue:FileDropCombo:4.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:4,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:4
  FDCB5.AddSortOrder()
  FDCB5.AddField(tur:Turnaround_Time,FDCB5.Q.tur:Turnaround_Time)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB8.Init(dee:Unit_Type,?dee:Unit_Type,Queue:FileDropCombo:5.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:5,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:5
  FDCB8.AddSortOrder(uni:Unit_Type_Key)
  FDCB8.AddField(uni:Unit_Type,FDCB8.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFEPS.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'eps_defaults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      access:defeps.update()
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStatus
      ThisWindow.Update
      ! Lookup button pressed. Call select procedure if required
      sts:Status = dee:Status_Type                    ! Move value for lookup
      IF Access:STATUS.TryFetch(sts:Status_Key)       ! IF record not found
         sts:Status = dee:Status_Type                 ! Move value for lookup
         ! Based on ?dee:Status_Type
         SET(sts:Status_Key,sts:Status_Key)           ! Prime order for browse
         Access:STATUS.TryNext()
      END
      GlobalRequest = SelectRecord
      Browse_Status                                   ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?LookupStatus)                        ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?dee:Status_Type,sts:Status)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?LookupStatus+1)                         ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'eps_defaults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB1.ResetQueue(1)
      FDCB2.ResetQueue(1)
      FDCB4.ResetQueue(1)
      FDCB5.ResetQueue(1)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

