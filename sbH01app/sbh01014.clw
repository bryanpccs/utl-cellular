

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01014.INC'),ONCE        !Local module procedure declarations
                     END


UpdateLetters PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?mrg:Status
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB26::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
History::mrg:Record  LIKE(mrg:RECORD),STATIC
QuickWindow          WINDOW('Letter Editor'),AT(,,533,347),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('UpdateLetters'),SYSTEM,GRAY,MAX,RESIZE,IMM
                       MENUBAR
                         MENU('&File'),USE(?FileMenu)
                           ITEM('&Print'),USE(?FilePrint),FIRST
                           ITEM('P&age Setup'),USE(?FilePageSetup),FIRST
                         END
                         MENU('&Edit'),USE(?Edit)
                           ITEM('Undo<9>Ctrl-Z'),USE(?EditUndo),KEY(CtrlZ)
                           ITEM('ReDo<9>Ctrl-Y'),USE(?EditReDo),KEY(CtrlY)
                           ITEM,SEPARATOR
                           ITEM('Copy<9>Ctrl-C'),USE(?EditCopy),KEY(CtrlC)
                           ITEM('Cut<9>Ctrl-X'),USE(?EditCut),KEY(CtrlX)
                           ITEM('Paste<9>Ctrl-V'),USE(?EditPaste),KEY(CtrlV)
                           ITEM,SEPARATOR
                           ITEM('&Find<9>Ctrl-F'),USE(?EditFind),KEY(CtrlF)
                           ITEM('&Replace'),USE(?EditReplace)
                         END
                         MENU('F&ormat'),USE(?Format)
                           ITEM('&Font...'),USE(?FormatFont)
                           MENU('&Alignment'),USE(?FormatAlignment)
                             ITEM('&Left'),USE(?FormatAlignmentLeft)
                             ITEM('&Center'),USE(?FormatAlignmentCenter)
                             ITEM('&Right'),USE(?FormatAlignmentRight)
                           END
                           MENU('&Bullet style'),USE(?FormatBulletstyle)
                             ITEM('Off'),USE(?FormatBulletstyleItem14)
                             ITEM('Normal'),USE(?FormatBulletstyleNormal)
                             ITEM('Arabic'),USE(?FormatBulletstyleItem20)
                             ITEM('Lower case letters'),USE(?FormatBulletstyleLowercaseletters)
                             ITEM('Upper case letter'),USE(?FormatBulletstyleUppercaseletter)
                             ITEM('Lower roman numerals'),USE(?FormatBulletstyleLowerromannumerals)
                             ITEM('Upper roman numerals'),USE(?FormatBulletstyleUpperromannumerals)
                           END
                         END
                         MENU('&View'),USE(?View)
                           ITEM('Display Toolbar'),USE(?ViewDisplayToolbar)
                           ITEM('Hide Toolbar'),USE(?ViewHideToolbar)
                           ITEM,SEPARATOR
                           ITEM('Display Format bar'),USE(?ViewDisplayFormatbar)
                           ITEM('Hide Format bar'),USE(?ViewHideFormatbar)
                           ITEM,SEPARATOR
                           ITEM('Display Ruler'),USE(?ViewDisplayRuler)
                           ITEM('Hide Ruler'),USE(?ViewHideRuler)
                         END
                         MENU('Standard Fields'),USE(?StandardFields)
                           ITEM('Today''s Date'),USE(?StandardFieldsTodaysDate)
                           ITEM('Current Time'),USE(?StandardFieldsCurrentTime)
                           ITEM('Logged In User Name'),USE(?StandardFieldsLoggedInUserName)
                         END
                       END
                       TEXT,AT(0,1,532,288),USE(?RTFTextBox),VSCROLL
                       SHEET,AT(4,293,524,24),USE(?CurrentTab),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Letter Name'),AT(8,301),USE(?MRG:LetterName:Prompt)
                           ENTRY(@s30),AT(64,301,124,10),USE(mrg:LetterName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           CHECK('New Status'),AT(220,301),USE(mrg:UseStatus),MSG('Is a Status Change Required?'),TIP('Is a Status Change Required?'),VALUE('YES','NO')
                           COMBO(@s30),AT(284,301,124,10),USE(mrg:Status),IMM,HIDE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           BUTTON('Insert Merge Field'),AT(436,297,88,16),USE(?Insert_Field),LEFT,ICON('Mail.gif')
                         END
                       END
                       BUTTON('&OK'),AT(412,325,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(468,325,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,321,524,24),USE(?Panel1),FILL(COLOR:Silver)
                     END
oRTF_RTFTextBox         &cwRTF,THREAD

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB26               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    If ?RTFTextBox{prop:ReadOnly} = True
        ?RTFTextBox{prop:FontColor} = 65793
        ?RTFTextBox{prop:Color} = 15066597
    Elsif ?RTFTextBox{prop:Req} = True
        ?RTFTextBox{prop:FontColor} = 65793
        ?RTFTextBox{prop:Color} = 8454143
    Else ! If ?RTFTextBox{prop:Req} = True
        ?RTFTextBox{prop:FontColor} = 65793
        ?RTFTextBox{prop:Color} = 16777215
    End ! If ?RTFTextBox{prop:Req} = True
    ?RTFTextBox{prop:Trn} = 0
    ?RTFTextBox{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?MRG:LetterName:Prompt{prop:FontColor} = -1
    ?MRG:LetterName:Prompt{prop:Color} = 15066597
    If ?mrg:LetterName{prop:ReadOnly} = True
        ?mrg:LetterName{prop:FontColor} = 65793
        ?mrg:LetterName{prop:Color} = 15066597
    Elsif ?mrg:LetterName{prop:Req} = True
        ?mrg:LetterName{prop:FontColor} = 65793
        ?mrg:LetterName{prop:Color} = 8454143
    Else ! If ?mrg:LetterName{prop:Req} = True
        ?mrg:LetterName{prop:FontColor} = 65793
        ?mrg:LetterName{prop:Color} = 16777215
    End ! If ?mrg:LetterName{prop:Req} = True
    ?mrg:LetterName{prop:Trn} = 0
    ?mrg:LetterName{prop:FontStyle} = font:Bold
    ?mrg:UseStatus{prop:Font,3} = -1
    ?mrg:UseStatus{prop:Color} = 15066597
    ?mrg:UseStatus{prop:Trn} = 0
    If ?mrg:Status{prop:ReadOnly} = True
        ?mrg:Status{prop:FontColor} = 65793
        ?mrg:Status{prop:Color} = 15066597
    Elsif ?mrg:Status{prop:Req} = True
        ?mrg:Status{prop:FontColor} = 65793
        ?mrg:Status{prop:Color} = 8454143
    Else ! If ?mrg:Status{prop:Req} = True
        ?mrg:Status{prop:FontColor} = 65793
        ?mrg:Status{prop:Color} = 16777215
    End ! If ?mrg:Status{prop:Req} = True
    ?mrg:Status{prop:Trn} = 0
    ?mrg:Status{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLetters',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLetters',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLetters',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?FileMenu;  SolaceCtrlName = '?FileMenu';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FilePrint;  SolaceCtrlName = '?FilePrint';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FilePageSetup;  SolaceCtrlName = '?FilePageSetup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Edit;  SolaceCtrlName = '?Edit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EditUndo;  SolaceCtrlName = '?EditUndo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EditReDo;  SolaceCtrlName = '?EditReDo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EditCopy;  SolaceCtrlName = '?EditCopy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EditCut;  SolaceCtrlName = '?EditCut';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EditPaste;  SolaceCtrlName = '?EditPaste';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EditFind;  SolaceCtrlName = '?EditFind';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EditReplace;  SolaceCtrlName = '?EditReplace';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Format;  SolaceCtrlName = '?Format';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatFont;  SolaceCtrlName = '?FormatFont';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatAlignment;  SolaceCtrlName = '?FormatAlignment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatAlignmentLeft;  SolaceCtrlName = '?FormatAlignmentLeft';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatAlignmentCenter;  SolaceCtrlName = '?FormatAlignmentCenter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatAlignmentRight;  SolaceCtrlName = '?FormatAlignmentRight';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyle;  SolaceCtrlName = '?FormatBulletstyle';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyleItem14;  SolaceCtrlName = '?FormatBulletstyleItem14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyleNormal;  SolaceCtrlName = '?FormatBulletstyleNormal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyleItem20;  SolaceCtrlName = '?FormatBulletstyleItem20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyleLowercaseletters;  SolaceCtrlName = '?FormatBulletstyleLowercaseletters';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyleUppercaseletter;  SolaceCtrlName = '?FormatBulletstyleUppercaseletter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyleLowerromannumerals;  SolaceCtrlName = '?FormatBulletstyleLowerromannumerals';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FormatBulletstyleUpperromannumerals;  SolaceCtrlName = '?FormatBulletstyleUpperromannumerals';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?View;  SolaceCtrlName = '?View';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewDisplayToolbar;  SolaceCtrlName = '?ViewDisplayToolbar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewHideToolbar;  SolaceCtrlName = '?ViewHideToolbar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewDisplayFormatbar;  SolaceCtrlName = '?ViewDisplayFormatbar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewHideFormatbar;  SolaceCtrlName = '?ViewHideFormatbar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewDisplayRuler;  SolaceCtrlName = '?ViewDisplayRuler';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewHideRuler;  SolaceCtrlName = '?ViewHideRuler';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StandardFields;  SolaceCtrlName = '?StandardFields';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StandardFieldsTodaysDate;  SolaceCtrlName = '?StandardFieldsTodaysDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StandardFieldsCurrentTime;  SolaceCtrlName = '?StandardFieldsCurrentTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StandardFieldsLoggedInUserName;  SolaceCtrlName = '?StandardFieldsLoggedInUserName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RTFTextBox;  SolaceCtrlName = '?RTFTextBox';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MRG:LetterName:Prompt;  SolaceCtrlName = '?MRG:LetterName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mrg:LetterName;  SolaceCtrlName = '?mrg:LetterName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mrg:UseStatus;  SolaceCtrlName = '?mrg:UseStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mrg:Status;  SolaceCtrlName = '?mrg:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert_Field;  SolaceCtrlName = '?Insert_Field';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a Letters Record'
  OF ChangeRecord
    ActionMessage = 'Changing a Letters Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateLetters')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLetters')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RTFTextBox
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mrg:Record,History::mrg:Record)
  SELF.AddHistoryField(?mrg:LetterName,2)
  SELF.AddHistoryField(?mrg:UseStatus,3)
  SELF.AddHistoryField(?mrg:Status,4)
  SELF.AddUpdateFile(Access:MERGELET)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MERGE.Open
  Relate:MERGELET.Open
  Relate:STATUS.Open
  Access:MERGETXT.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MERGELET
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If thiswindow.request   = insertrecord
      If access:mergetxt.primerecord() = Level:Benign
          mrt:refnumber   = mrg:refnumber
          access:mergetxt.insert()
      End!If access:mergetxt.primerecord() = Level:Benign
  End!If thiswindow.request   = insertrecord
  access:mergetxt.clearkey(mrt:refnumberkey)
  mrt:refnumber   = mrg:refnumber
  access:mergetxt.tryfetch(mrt:refnumberkey)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  oRTF_RTFTextBox &= NEW cwRTF
  oRTF_RTFTextBox.Init( QuickWindow, ?RTFTextBox, 1, 1, 1 )
  IF ERRORCODE() = 95
    RETURN Level:Fatal
  END
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?mrg:UseStatus{Prop:Checked} = True
    UNHIDE(?MRG:Status)
  END
  IF ?mrg:UseStatus{Prop:Checked} = False
    HIDE(?MRG:Status)
  END
  FDCB26.Init(mrg:Status,?mrg:Status,Queue:FileDropCombo.ViewPosition,FDCB26::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB26.Q &= Queue:FileDropCombo
  FDCB26.AddSortOrder(sts:JobKey)
  FDCB26.AddField(sts:Status,FDCB26.Q.sts:Status)
  FDCB26.AddField(sts:Ref_Number,FDCB26.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB26.WindowComponent)
  FDCB26.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MERGE.Close
    Relate:MERGELET.Close
    Relate:STATUS.Close
  END
  DISPOSE( oRTF_RTFTextBox )
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLetters',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?FilePageSetup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FilePageSetup, Accepted)
      oRTF_RTFTextBox.PageSetup()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FilePageSetup, Accepted)
    OF ?EditUndo
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditUndo, Accepted)
      IF oRTF_RTFTextBox.CanUndo()
        oRTF_RTFTextBox.Undo()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditUndo, Accepted)
    OF ?EditReDo
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditReDo, Accepted)
      IF oRTF_RTFTextBox.CanRedo()
        oRTF_RTFTextBox.Redo()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditReDo, Accepted)
    OF ?EditCopy
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditCopy, Accepted)
      oRTF_RTFTextBox.Copy()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditCopy, Accepted)
    OF ?EditCut
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditCut, Accepted)
      oRTF_RTFTextBox.Cut()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditCut, Accepted)
    OF ?EditPaste
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditPaste, Accepted)
      oRTF_RTFTextBox.Paste()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditPaste, Accepted)
    OF ?EditFind
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditFind, Accepted)
      oRTF_RTFTextBox.Find(  )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditFind, Accepted)
    OF ?EditReplace
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditReplace, Accepted)
      oRTF_RTFTextBox.Replace( , '' )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EditReplace, Accepted)
    OF ?FormatFont
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatFont, Accepted)
      oRTF_RTFTextBox.Font( , , ,  )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatFont, Accepted)
    OF ?FormatAlignmentLeft
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatAlignmentLeft, Accepted)
      oRTF_RTFTextBox.AlignParaLeft()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatAlignmentLeft, Accepted)
    OF ?FormatAlignmentCenter
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatAlignmentCenter, Accepted)
      oRTF_RTFTextBox.AlignParaCenter()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatAlignmentCenter, Accepted)
    OF ?FormatAlignmentRight
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatAlignmentRight, Accepted)
      oRTF_RTFTextBox.AlignParaRight()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatAlignmentRight, Accepted)
    OF ?FormatBulletstyleItem14
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleItem14, Accepted)
      oRTF_RTFTextBox.ParaBulletsOff()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleItem14, Accepted)
    OF ?FormatBulletstyleNormal
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleNormal, Accepted)
      oRTF_RTFTextBox.ParaBulletsOn( Bullets:On )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleNormal, Accepted)
    OF ?FormatBulletstyleItem20
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleItem20, Accepted)
      oRTF_RTFTextBox.ParaBulletsOn( Bullets:On )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleItem20, Accepted)
    OF ?FormatBulletstyleLowercaseletters
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleLowercaseletters, Accepted)
      oRTF_RTFTextBox.ParaBulletsOn( Bullets:LowerLetters )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleLowercaseletters, Accepted)
    OF ?FormatBulletstyleUppercaseletter
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleUppercaseletter, Accepted)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleUppercaseletter, Accepted)
    OF ?FormatBulletstyleLowerromannumerals
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleLowerromannumerals, Accepted)
      oRTF_RTFTextBox.ParaBulletsOn( Bullets:LowerRoman )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleLowerromannumerals, Accepted)
    OF ?FormatBulletstyleUpperromannumerals
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleUpperromannumerals, Accepted)
      oRTF_RTFTextBox.ParaBulletsOn( Bullets:UpperRoman )
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FormatBulletstyleUpperromannumerals, Accepted)
    OF ?StandardFieldsTodaysDate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StandardFieldsTodaysDate, Accepted)
      SetClipBoard('[*TODAY*]')
      oRTF_RTFTextBox.Paste()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StandardFieldsTodaysDate, Accepted)
    OF ?StandardFieldsCurrentTime
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StandardFieldsCurrentTime, Accepted)
      SetClipBoard('[*TIME*]')
      oRTF_RTFTextBox.Paste()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StandardFieldsCurrentTime, Accepted)
    OF ?StandardFieldsLoggedInUserName
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StandardFieldsLoggedInUserName, Accepted)
      SetClipBoard('[*USER*]')
      oRTF_RTFTextBox.Paste()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?StandardFieldsLoggedInUserName, Accepted)
    OF ?mrg:UseStatus
      IF ?mrg:UseStatus{Prop:Checked} = True
        UNHIDE(?MRG:Status)
      END
      IF ?mrg:UseStatus{Prop:Checked} = False
        HIDE(?MRG:Status)
      END
      ThisWindow.Reset
    OF ?Insert_Field
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert_Field, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      browsemerge('LETTER')
      if globalresponse = requestcompleted
          SetClipBoard('[' & Clip(mer:fieldname) & ']')
          
          oRTF_RTFTextBox.Paste()
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert_Field, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      oRTF_RTFTextBox.SaveField( mrt:Letter )
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  access:mergetxt.update()
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLetters')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?RTFTextBox
    CASE EVENT()
    ELSE
      IF EVENT() = EVENT:RTFReady
        oRTF_RTFTextBox.LoadField( mrt:Letter )
      END
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      oRTF_RTFTextBox.Kill(FALSE)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(event:accepted,?mrg:UseStatus)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Tab:1, Resize:FixLeft+Resize:FixBottom, Resize:LockHeight)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixBottom, Resize:LockHeight)
  SELF.SetStrategy(?OK, Resize:FixRight+Resize:FixBottom, Resize:LockSize)
  SELF.SetStrategy(?Cancel, Resize:FixRight+Resize:FixBottom, Resize:LockSize)
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixBottom, Resize:LockHeight)

