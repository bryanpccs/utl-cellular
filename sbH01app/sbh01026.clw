

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBH01026.INC'),ONCE        !Local module procedure declarations
                     END


GetDirectory         PROCEDURE  ()                    ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
cTitle         CSTRING(30)
cPath          CSTRING(251)

BROWSEINFO     GROUP
hWndOwner        LONG
pIDLRoot         LONG
lpszDisplayName  LONG
lpszTitle        LONG
ulFlags          LONG
lpfnCallBack     LONG
iImage           LONG
               END

theDirectory   STRING(255)
savePath       STRING(255)
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'GetDirectory')      !Add Procedure to Log
  end


    savePath = PATH()
    theDirectory = ''
    cTitle = 'Choose a Directory'
    cPath = '<0>{250}'
    BrowseInfo.hWndOwner = 0{prop:Handle}
    BrowseInfo.lpszTitle = ADDRESS(cTitle)
    BrowseInfo.ulFlags = 65 ! 0x0001 = only show directories!
                            ! 0x0040 = BIF_NEWSTYLEDIALOG
    lpIDList# = SHBrowseForFolder(BrowseInfo)
    IF (lpIDList#) THEN
        Result# = SHGetPathFromIDList(lpIDList#, cPath)
        COTaskMemFree(lpIDList#)
        IF (Result#) THEN
            theDirectory = cPath
        END
    END

    SETPATH(CLIP(savePath))
    RETURN CLIP(theDirectory)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'GetDirectory',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
