

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01011.INC'),ONCE        !Local module procedure declarations
                     END


Update_Letter PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::let:Record  LIKE(let:RECORD),STATIC
QuickWindow          WINDOW('Update the LETTERS File'),AT(,,358,186),FONT('MS Sans Serif',8,,),IMM,HLP('Update_Letter'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,350,160),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number:'),AT(8,20),USE(?LET:RecordNumber:Prompt)
                           ENTRY(@n-14),AT(76,20,64,10),USE(let:RecordNumber),RIGHT(1)
                           PROMPT('Description'),AT(8,34),USE(?LET:Description:Prompt)
                           ENTRY(@s255),AT(76,34,274,10),USE(let:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter''s Description'),TIP('Letter''s Description'),UPR
                           PROMPT('Subject'),AT(8,48),USE(?LET:Subject:Prompt)
                           ENTRY(@s255),AT(76,48,274,10),USE(let:Subject),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter''s Subject (i.e. RE:)'),TIP('Letter''s Subject (i.e. RE:)'),UPR
                           PROMPT('Telephone Number'),AT(8,62),USE(?LET:TelephoneNumber:Prompt)
                           ENTRY(@s20),AT(76,62,84,10),USE(let:TelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Telephone Number on top of letter'),TIP('Telephone Number on top of letter'),UPR
                           PROMPT('Fax Number'),AT(8,76),USE(?LET:FaxNumber:Prompt)
                           ENTRY(@s20),AT(76,76,84,10),USE(let:FaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fax Number on top of letter'),TIP('Fax Number on top of letter'),UPR
                           PROMPT('Letter Text'),AT(8,90),USE(?LET:LetterText:Prompt)
                           TEXT,AT(76,90,274,30),USE(let:LetterText),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           CHECK('New Status'),AT(76,124,70,8),USE(let:UseStatus),MSG('Is a Status Change Required?'),TIP('Is a Status Change Required?'),VALUE('YES','NO')
                           PROMPT('Status'),AT(8,136),USE(?LET:Status:Prompt)
                           ENTRY(@s30),AT(76,136,124,10),USE(let:Status),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Dummy Field:'),AT(8,150),USE(?LET:DummyField:Prompt)
                           ENTRY(@s1),AT(76,150,40,10),USE(let:DummyField),MSG('For File Manager'),TIP('For File Manager')
                         END
                       END
                       BUTTON('OK'),AT(211,168,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(260,168,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(309,168,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?LET:RecordNumber:Prompt{prop:FontColor} = -1
    ?LET:RecordNumber:Prompt{prop:Color} = 15066597
    If ?let:RecordNumber{prop:ReadOnly} = True
        ?let:RecordNumber{prop:FontColor} = 65793
        ?let:RecordNumber{prop:Color} = 15066597
    Elsif ?let:RecordNumber{prop:Req} = True
        ?let:RecordNumber{prop:FontColor} = 65793
        ?let:RecordNumber{prop:Color} = 8454143
    Else ! If ?let:RecordNumber{prop:Req} = True
        ?let:RecordNumber{prop:FontColor} = 65793
        ?let:RecordNumber{prop:Color} = 16777215
    End ! If ?let:RecordNumber{prop:Req} = True
    ?let:RecordNumber{prop:Trn} = 0
    ?let:RecordNumber{prop:FontStyle} = font:Bold
    ?LET:Description:Prompt{prop:FontColor} = -1
    ?LET:Description:Prompt{prop:Color} = 15066597
    If ?let:Description{prop:ReadOnly} = True
        ?let:Description{prop:FontColor} = 65793
        ?let:Description{prop:Color} = 15066597
    Elsif ?let:Description{prop:Req} = True
        ?let:Description{prop:FontColor} = 65793
        ?let:Description{prop:Color} = 8454143
    Else ! If ?let:Description{prop:Req} = True
        ?let:Description{prop:FontColor} = 65793
        ?let:Description{prop:Color} = 16777215
    End ! If ?let:Description{prop:Req} = True
    ?let:Description{prop:Trn} = 0
    ?let:Description{prop:FontStyle} = font:Bold
    ?LET:Subject:Prompt{prop:FontColor} = -1
    ?LET:Subject:Prompt{prop:Color} = 15066597
    If ?let:Subject{prop:ReadOnly} = True
        ?let:Subject{prop:FontColor} = 65793
        ?let:Subject{prop:Color} = 15066597
    Elsif ?let:Subject{prop:Req} = True
        ?let:Subject{prop:FontColor} = 65793
        ?let:Subject{prop:Color} = 8454143
    Else ! If ?let:Subject{prop:Req} = True
        ?let:Subject{prop:FontColor} = 65793
        ?let:Subject{prop:Color} = 16777215
    End ! If ?let:Subject{prop:Req} = True
    ?let:Subject{prop:Trn} = 0
    ?let:Subject{prop:FontStyle} = font:Bold
    ?LET:TelephoneNumber:Prompt{prop:FontColor} = -1
    ?LET:TelephoneNumber:Prompt{prop:Color} = 15066597
    If ?let:TelephoneNumber{prop:ReadOnly} = True
        ?let:TelephoneNumber{prop:FontColor} = 65793
        ?let:TelephoneNumber{prop:Color} = 15066597
    Elsif ?let:TelephoneNumber{prop:Req} = True
        ?let:TelephoneNumber{prop:FontColor} = 65793
        ?let:TelephoneNumber{prop:Color} = 8454143
    Else ! If ?let:TelephoneNumber{prop:Req} = True
        ?let:TelephoneNumber{prop:FontColor} = 65793
        ?let:TelephoneNumber{prop:Color} = 16777215
    End ! If ?let:TelephoneNumber{prop:Req} = True
    ?let:TelephoneNumber{prop:Trn} = 0
    ?let:TelephoneNumber{prop:FontStyle} = font:Bold
    ?LET:FaxNumber:Prompt{prop:FontColor} = -1
    ?LET:FaxNumber:Prompt{prop:Color} = 15066597
    If ?let:FaxNumber{prop:ReadOnly} = True
        ?let:FaxNumber{prop:FontColor} = 65793
        ?let:FaxNumber{prop:Color} = 15066597
    Elsif ?let:FaxNumber{prop:Req} = True
        ?let:FaxNumber{prop:FontColor} = 65793
        ?let:FaxNumber{prop:Color} = 8454143
    Else ! If ?let:FaxNumber{prop:Req} = True
        ?let:FaxNumber{prop:FontColor} = 65793
        ?let:FaxNumber{prop:Color} = 16777215
    End ! If ?let:FaxNumber{prop:Req} = True
    ?let:FaxNumber{prop:Trn} = 0
    ?let:FaxNumber{prop:FontStyle} = font:Bold
    ?LET:LetterText:Prompt{prop:FontColor} = -1
    ?LET:LetterText:Prompt{prop:Color} = 15066597
    If ?let:LetterText{prop:ReadOnly} = True
        ?let:LetterText{prop:FontColor} = 65793
        ?let:LetterText{prop:Color} = 15066597
    Elsif ?let:LetterText{prop:Req} = True
        ?let:LetterText{prop:FontColor} = 65793
        ?let:LetterText{prop:Color} = 8454143
    Else ! If ?let:LetterText{prop:Req} = True
        ?let:LetterText{prop:FontColor} = 65793
        ?let:LetterText{prop:Color} = 16777215
    End ! If ?let:LetterText{prop:Req} = True
    ?let:LetterText{prop:Trn} = 0
    ?let:LetterText{prop:FontStyle} = font:Bold
    ?let:UseStatus{prop:Font,3} = -1
    ?let:UseStatus{prop:Color} = 15066597
    ?let:UseStatus{prop:Trn} = 0
    ?LET:Status:Prompt{prop:FontColor} = -1
    ?LET:Status:Prompt{prop:Color} = 15066597
    If ?let:Status{prop:ReadOnly} = True
        ?let:Status{prop:FontColor} = 65793
        ?let:Status{prop:Color} = 15066597
    Elsif ?let:Status{prop:Req} = True
        ?let:Status{prop:FontColor} = 65793
        ?let:Status{prop:Color} = 8454143
    Else ! If ?let:Status{prop:Req} = True
        ?let:Status{prop:FontColor} = 65793
        ?let:Status{prop:Color} = 16777215
    End ! If ?let:Status{prop:Req} = True
    ?let:Status{prop:Trn} = 0
    ?let:Status{prop:FontStyle} = font:Bold
    ?LET:DummyField:Prompt{prop:FontColor} = -1
    ?LET:DummyField:Prompt{prop:Color} = 15066597
    If ?let:DummyField{prop:ReadOnly} = True
        ?let:DummyField{prop:FontColor} = 65793
        ?let:DummyField{prop:Color} = 15066597
    Elsif ?let:DummyField{prop:Req} = True
        ?let:DummyField{prop:FontColor} = 65793
        ?let:DummyField{prop:Color} = 8454143
    Else ! If ?let:DummyField{prop:Req} = True
        ?let:DummyField{prop:FontColor} = 65793
        ?let:DummyField{prop:Color} = 16777215
    End ! If ?let:DummyField{prop:Req} = True
    ?let:DummyField{prop:Trn} = 0
    ?let:DummyField{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Letter',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Letter',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Letter',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:RecordNumber:Prompt;  SolaceCtrlName = '?LET:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:RecordNumber;  SolaceCtrlName = '?let:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:Description:Prompt;  SolaceCtrlName = '?LET:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:Description;  SolaceCtrlName = '?let:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:Subject:Prompt;  SolaceCtrlName = '?LET:Subject:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:Subject;  SolaceCtrlName = '?let:Subject';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:TelephoneNumber:Prompt;  SolaceCtrlName = '?LET:TelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:TelephoneNumber;  SolaceCtrlName = '?let:TelephoneNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:FaxNumber:Prompt;  SolaceCtrlName = '?LET:FaxNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:FaxNumber;  SolaceCtrlName = '?let:FaxNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:LetterText:Prompt;  SolaceCtrlName = '?LET:LetterText:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:LetterText;  SolaceCtrlName = '?let:LetterText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:UseStatus;  SolaceCtrlName = '?let:UseStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:Status:Prompt;  SolaceCtrlName = '?LET:Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:Status;  SolaceCtrlName = '?let:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:DummyField:Prompt;  SolaceCtrlName = '?LET:DummyField:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:DummyField;  SolaceCtrlName = '?let:DummyField';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Letter')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Letter')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LET:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(let:Record,History::let:Record)
  SELF.AddHistoryField(?let:RecordNumber,1)
  SELF.AddHistoryField(?let:Description,2)
  SELF.AddHistoryField(?let:Subject,3)
  SELF.AddHistoryField(?let:TelephoneNumber,4)
  SELF.AddHistoryField(?let:FaxNumber,5)
  SELF.AddHistoryField(?let:LetterText,6)
  SELF.AddHistoryField(?let:UseStatus,7)
  SELF.AddHistoryField(?let:Status,8)
  SELF.AddHistoryField(?let:DummyField,9)
  SELF.AddUpdateFile(Access:LETTERS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LETTERS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LETTERS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LETTERS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Letter',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Letter')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

