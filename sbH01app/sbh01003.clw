

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBH01002.INC'),ONCE        !Req'd for module callout resolution
                     END


Browse_Manufacturer_Fault_Coding PROCEDURE            !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MANFAULT)
                       PROJECT(maf:Field_Number)
                       PROJECT(maf:Field_Name)
                       PROJECT(maf:Field_Type)
                       PROJECT(maf:Compulsory_At_Booking)
                       PROJECT(maf:Compulsory)
                       PROJECT(maf:Lookup)
                       PROJECT(maf:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
maf:Field_Number       LIKE(maf:Field_Number)         !List box control field - type derived from field
maf:Field_Name         LIKE(maf:Field_Name)           !List box control field - type derived from field
maf:Field_Type         LIKE(maf:Field_Type)           !List box control field - type derived from field
maf:Compulsory_At_Booking LIKE(maf:Compulsory_At_Booking) !List box control field - type derived from field
maf:Compulsory         LIKE(maf:Compulsory)           !List box control field - type derived from field
maf:Lookup             LIKE(maf:Lookup)               !List box control field - type derived from field
GLO:Select1            STRING(1)                      !Browse hot field - unable to determine correct data type
maf:Manufacturer       LIKE(maf:Manufacturer)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Fault Code File'),AT(,,529,186),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Manufacturer_Fault_Coding'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,36,436,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('49R(2)|M~Field Number~L@n2@125L(2)|M~Field Name~@s30@95L(2)|M~Field Type~@s30@[4' &|
   '5L(2)|M~Booking~@s3@40L(2)|M~Completion~@s3@]|M~Compulsory~30L(2)|M~Lookup~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(452,20,76,20),USE(?Select:2),LEFT,ICON('SELECT.ICO')
                       BUTTON('&Insert'),AT(452,88,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(452,112,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO'),DEFAULT
                       BUTTON('&Delete'),AT(452,136,76,20),USE(?Delete:3),LEFT,ICON('DELETE.ICO')
                       SHEET,AT(4,4,444,180),USE(?CurrentTab),SPREAD
                         TAB('By Field Number'),USE(?Tab:2)
                           ENTRY(@n2),AT(8,20,59,12),USE(maf:Field_Number),FONT('Tahoma',8,,FONT:bold),MSG('Fault Code Field Number')
                         END
                       END
                       BUTTON('Close'),AT(452,164,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?maf:Field_Number{prop:ReadOnly} = True
        ?maf:Field_Number{prop:FontColor} = 65793
        ?maf:Field_Number{prop:Color} = 15066597
    Elsif ?maf:Field_Number{prop:Req} = True
        ?maf:Field_Number{prop:FontColor} = 65793
        ?maf:Field_Number{prop:Color} = 8454143
    Else ! If ?maf:Field_Number{prop:Req} = True
        ?maf:Field_Number{prop:FontColor} = 65793
        ?maf:Field_Number{prop:Color} = 16777215
    End ! If ?maf:Field_Number{prop:Req} = True
    ?maf:Field_Number{prop:Trn} = 0
    ?maf:Field_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Manufacturer_Fault_Coding',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Manufacturer_Fault_Coding',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Manufacturer_Fault_Coding',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Manufacturer_Fault_Coding',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Field_Number;  SolaceCtrlName = '?maf:Field_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manufacturer_Fault_Coding')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Manufacturer_Fault_Coding')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANFAULT.Open
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAULT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,maf:Field_Number_Key)
  BRW1.AddRange(maf:Manufacturer,Relate:MANFAULT,Relate:MANUFACT)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?maf:Field_Number,maf:Field_Number,1,BRW1)
  BIND('GLO:Select1',GLO:Select1)
  BRW1.AddField(maf:Field_Number,BRW1.Q.maf:Field_Number)
  BRW1.AddField(maf:Field_Name,BRW1.Q.maf:Field_Name)
  BRW1.AddField(maf:Field_Type,BRW1.Q.maf:Field_Type)
  BRW1.AddField(maf:Compulsory_At_Booking,BRW1.Q.maf:Compulsory_At_Booking)
  BRW1.AddField(maf:Compulsory,BRW1.Q.maf:Compulsory)
  BRW1.AddField(maf:Lookup,BRW1.Q.maf:Lookup)
  BRW1.AddField(GLO:Select1,BRW1.Q.GLO:Select1)
  BRW1.AddField(maf:Manufacturer,BRW1.Q.maf:Manufacturer)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAULT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Manufacturer_Fault_Coding',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('MANUFACT FAULT CODES - INSERT',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            check_access('MANUFACT FAULT CODES - CHANGE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            check_access('MANUFACT FAULT CODES - DELETE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAULT
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Manufacturer_Fault_Coding')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?MAF:Field_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

