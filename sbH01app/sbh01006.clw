

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBH01022.INC'),ONCE        !Req'd for module callout resolution
                     END


Browse_Manufacturer_Parts_Fault_Coding PROCEDURE      !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(MANFAUPA)
                       PROJECT(map:Field_Number)
                       PROJECT(map:Field_Name)
                       PROJECT(map:Field_Type)
                       PROJECT(map:Compulsory)
                       PROJECT(map:Lookup)
                       PROJECT(map:Manufacturer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
map:Field_Number       LIKE(map:Field_Number)         !List box control field - type derived from field
map:Field_Name         LIKE(map:Field_Name)           !List box control field - type derived from field
map:Field_Type         LIKE(map:Field_Type)           !List box control field - type derived from field
map:Compulsory         LIKE(map:Compulsory)           !List box control field - type derived from field
map:Lookup             LIKE(map:Lookup)               !List box control field - type derived from field
map:Manufacturer       LIKE(map:Manufacturer)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Fault Code File'),AT(,,440,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Manufacturer_Parts_Fault_Coding'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,36,340,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('52R(2)|M~Field Number~L@n2@80L(2)|M~Field Name~@s30@103L(2)|M~Field Type~@s30@43' &|
   'L(2)|M~Compulsory~@s3@12L(2)|M~Lookup~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(360,20,76,20),USE(?Select:2),LEFT,ICON('SELECT.ICO')
                       BUTTON('&Insert'),AT(360,88,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(360,112,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO'),DEFAULT
                       BUTTON('&Delete'),AT(360,136,76,20),USE(?Delete:3),LEFT,ICON('DELETE.ICO')
                       SHEET,AT(4,4,348,180),USE(?CurrentTab),FONT('Tahoma',8,,FONT:regular),SPREAD
                         TAB('By Field Number'),USE(?Tab:2)
                           ENTRY(@n2),AT(8,20,64,10),USE(map:Field_Number),FONT(,,,FONT:bold),MSG('Fault Code Field Number')
                         END
                       END
                       BUTTON('Close'),AT(360,164,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                       BUTTON('DELETE THIS'),AT(252,200,76,20),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?map:Field_Number{prop:ReadOnly} = True
        ?map:Field_Number{prop:FontColor} = 65793
        ?map:Field_Number{prop:Color} = 15066597
    Elsif ?map:Field_Number{prop:Req} = True
        ?map:Field_Number{prop:FontColor} = 65793
        ?map:Field_Number{prop:Color} = 8454143
    Else ! If ?map:Field_Number{prop:Req} = True
        ?map:Field_Number{prop:FontColor} = 65793
        ?map:Field_Number{prop:Color} = 16777215
    End ! If ?map:Field_Number{prop:Req} = True
    ?map:Field_Number{prop:Trn} = 0
    ?map:Field_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Manufacturer_Parts_Fault_Coding',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Manufacturer_Parts_Fault_Coding',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Manufacturer_Parts_Fault_Coding',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Manufacturer_Parts_Fault_Coding',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Number;  SolaceCtrlName = '?map:Field_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Manufacturer_Parts_Fault_Coding')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Manufacturer_Parts_Fault_Coding')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANFAUPA.Open
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:MANFAUPA,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,map:Field_Number_Key)
  BRW1.AddRange(map:Manufacturer,Relate:MANFAUPA,Relate:MANUFACT)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?map:Field_Number,map:Field_Number,1,BRW1)
  BRW1.AddField(map:Field_Number,BRW1.Q.map:Field_Number)
  BRW1.AddField(map:Field_Name,BRW1.Q.map:Field_Name)
  BRW1.AddField(map:Field_Type,BRW1.Q.map:Field_Type)
  BRW1.AddField(map:Compulsory,BRW1.Q.map:Compulsory)
  BRW1.AddField(map:Lookup,BRW1.Q.map:Lookup)
  BRW1.AddField(map:Manufacturer,BRW1.Q.map:Manufacturer)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAUPA.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Manufacturer_Parts_Fault_Coding',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('MANUFACT FAULT CODES - INSERT',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            check_access('MANUFACT FAULT CODES - CHANGE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            check_access('MANUFACT FAULT CODES - DELETE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAUPA
    ReturnValue = GlobalResponse
  END
  End!do_udpate# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Manufacturer_Parts_Fault_Coding')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?MAP:Field_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

