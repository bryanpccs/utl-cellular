

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01022.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBH01005.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateMANFAUPA PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
tmp:DatePicture      STRING(30)
Filepath             STRING(255)
SaveMFP              USHORT
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW8::View:Browse    VIEW(MANFPALO)
                       PROJECT(mfp:Field)
                       PROJECT(mfp:Description)
                       PROJECT(mfp:Manufacturer)
                       PROJECT(mfp:Field_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mfp:Field              LIKE(mfp:Field)                !List box control field - type derived from field
mfp:Description        LIKE(mfp:Description)          !List box control field - type derived from field
mfp:Manufacturer       LIKE(mfp:Manufacturer)         !Primary key field - type derived from field
mfp:Field_Number       LIKE(mfp:Field_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK11::mfp:Field_Number    LIKE(mfp:Field_Number)
HK11::mfp:Manufacturer    LIKE(mfp:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
History::map:Record  LIKE(map:RECORD),STATIC
QuickWindow          WINDOW('Update Fault Codes'),AT(,,594,323),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateMANFAULT'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,280,212),USE(?General_Sheet),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?map:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(map:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Field Number'),AT(8,36),USE(?map:Field_Number:Prompt)
                           SPIN(@n2),AT(84,36,64,10),USE(map:Field_Number),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,UPR,RANGE(1,12),STEP(1),MSG('Fault Code Field Number')
                           PROMPT('Field Name'),AT(8,52),USE(?map:Field_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(map:Field_Name),FONT('Tahoma',8,,FONT:bold),REQ,CAP
                           LIST,AT(192,84,64,10),USE(tmp:DatePicture),HIDE,LEFT(2),FONT(,8,,FONT:bold),COLOR(0BDFFFFH),DROP(20),FROM('DD/MM/YYYY|DD/MM/YY|MM/DD/YYYY|MM/DD/YY|MM/YYYY|MM/YY|YYYY/MM|YY/MM|YYYY/MM/DD|YY/MM/DD|YYYYMMDD')
                           OPTION('Field Type'),AT(84,64,104,44),USE(map:Field_Type),BOXED
                             RADIO('Generic String'),AT(92,72),USE(?map:Field_Type:Radio1),VALUE('STRING')
                             RADIO('Date'),AT(92,84),USE(?map:Field_Type:Radio2),VALUE('DATE')
                             RADIO('Number Only'),AT(92,96),USE(?map:Field_Type:Radio3),VALUE('NUMBER')
                           END
                           PROMPT('Date Picture'),AT(192,76),USE(?DatePicture),HIDE
                           CHECK('Lookup'),AT(84,112,52,12),USE(map:Lookup),FONT('Tahoma',8,,FONT:regular),VALUE('YES','NO')
                           CHECK('Force Lookup'),AT(192,112),USE(map:Force_Lookup),VALUE('YES','NO')
                           CHECK('Compulsory At Completion'),AT(84,128),USE(map:Compulsory),VALUE('YES','NO')
                           CHECK('Restrict Length'),AT(84,168),USE(map:RestrictLength),MSG('Restrict Length'),TIP('Restrict Length'),VALUE('1','0')
                           GROUP,AT(4,180,196,32),USE(?RestrictGroup),HIDE
                             PROMPT('Length From'),AT(8,184),USE(?map:LengthFrom:Prompt),TRN
                             ENTRY(@s8),AT(84,184,64,10),USE(map:LengthFrom),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Length From'),TIP('Length From'),REQ,UPR
                             PROMPT('Length To'),AT(8,200),USE(?map:LengthTo:Prompt),TRN
                             ENTRY(@s8),AT(84,200,64,10),USE(map:LengthTo),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Length To'),TIP('Length To'),REQ,UPR
                           END
                         END
                       END
                       SHEET,AT(288,4,304,212),USE(?Sheet2),SPREAD
                         TAB('Lookup Field'),USE(?Tab2)
                           ENTRY(@s30),AT(292,20,127,10),USE(mfo:Field),FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(292,36,296,156),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('86L(2)|M~Field~@s30@240L(2)|M~Description~S(240)@s60@'),FROM(Queue:Browse)
                           BUTTON('Import'),AT(292,196,56,16),USE(?ImportButton),LEFT,ICON(ICON:Open)
                           BUTTON('&Insert'),AT(412,196,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(472,196,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(532,196,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       SHEET,AT(4,220,588,72),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           CHECK('Force Format'),AT(84,224),USE(map:ForceFormat),MSG('Force Format'),TIP('Force Format'),VALUE('1','0')
                           GROUP('Field Format Types'),AT(288,223,265,66),USE(?FieldFormatTypes),BOXED
                             PROMPT('MM - Month'),AT(292,232),USE(?Prompt8)
                             PROMPT('YY - Two Digit Year'),AT(292,244),USE(?Prompt8:2)
                             PROMPT('YYYY - Four Digit Year'),AT(292,256),USE(?Prompt8:3)
                             PROMPT('A - Alpha Only Character'),AT(292,268),USE(?Prompt8:4)
                             PROMPT('"" - Put any fixed text in Quotes, e.g. "TEST"'),AT(396,244),USE(?Prompt8:7)
                             PROMPT('0 - Numeric Only Character'),AT(292,280),USE(?Prompt8:5)
                             PROMPT('X - Alphanumeric Character'),AT(396,232),USE(?Prompt8:6)
                           END
                           PROMPT('Field Format'),AT(8,240),USE(?map:FieldFormat:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,240,124,10),USE(map:FieldFormat),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Field Format'),TIP('Field Format'),REQ,UPR
                         END
                       END
                       BUTTON('&OK'),AT(476,300,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(532,300,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,296,588,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
! Start Change BE021 BE(2/12/03)
IMPORT_FILE          FILE,DRIVER('BASIC'),OEM,PRE(IMPF),CREATE,THREAD
Record                   RECORD,PRE(IMP)
Code                        STRING(30)
Description                 STRING(60)
                         END
                     END
! End Change BE021 BE(2/12/03)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?General_Sheet{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?map:Manufacturer:Prompt{prop:FontColor} = -1
    ?map:Manufacturer:Prompt{prop:Color} = 15066597
    If ?map:Manufacturer{prop:ReadOnly} = True
        ?map:Manufacturer{prop:FontColor} = 65793
        ?map:Manufacturer{prop:Color} = 15066597
    Elsif ?map:Manufacturer{prop:Req} = True
        ?map:Manufacturer{prop:FontColor} = 65793
        ?map:Manufacturer{prop:Color} = 8454143
    Else ! If ?map:Manufacturer{prop:Req} = True
        ?map:Manufacturer{prop:FontColor} = 65793
        ?map:Manufacturer{prop:Color} = 16777215
    End ! If ?map:Manufacturer{prop:Req} = True
    ?map:Manufacturer{prop:Trn} = 0
    ?map:Manufacturer{prop:FontStyle} = font:Bold
    ?map:Field_Number:Prompt{prop:FontColor} = -1
    ?map:Field_Number:Prompt{prop:Color} = 15066597
    If ?map:Field_Number{prop:ReadOnly} = True
        ?map:Field_Number{prop:FontColor} = 65793
        ?map:Field_Number{prop:Color} = 15066597
    Elsif ?map:Field_Number{prop:Req} = True
        ?map:Field_Number{prop:FontColor} = 65793
        ?map:Field_Number{prop:Color} = 8454143
    Else ! If ?map:Field_Number{prop:Req} = True
        ?map:Field_Number{prop:FontColor} = 65793
        ?map:Field_Number{prop:Color} = 16777215
    End ! If ?map:Field_Number{prop:Req} = True
    ?map:Field_Number{prop:Trn} = 0
    ?map:Field_Number{prop:FontStyle} = font:Bold
    ?map:Field_Name:Prompt{prop:FontColor} = -1
    ?map:Field_Name:Prompt{prop:Color} = 15066597
    If ?map:Field_Name{prop:ReadOnly} = True
        ?map:Field_Name{prop:FontColor} = 65793
        ?map:Field_Name{prop:Color} = 15066597
    Elsif ?map:Field_Name{prop:Req} = True
        ?map:Field_Name{prop:FontColor} = 65793
        ?map:Field_Name{prop:Color} = 8454143
    Else ! If ?map:Field_Name{prop:Req} = True
        ?map:Field_Name{prop:FontColor} = 65793
        ?map:Field_Name{prop:Color} = 16777215
    End ! If ?map:Field_Name{prop:Req} = True
    ?map:Field_Name{prop:Trn} = 0
    ?map:Field_Name{prop:FontStyle} = font:Bold
    ?tmp:DatePicture{prop:FontColor} = 65793
    ?tmp:DatePicture{prop:Color}= 16777215
    ?tmp:DatePicture{prop:Color,2} = 16777215
    ?tmp:DatePicture{prop:Color,3} = 12937777
    ?map:Field_Type{prop:Font,3} = -1
    ?map:Field_Type{prop:Color} = 15066597
    ?map:Field_Type{prop:Trn} = 0
    ?map:Field_Type:Radio1{prop:Font,3} = -1
    ?map:Field_Type:Radio1{prop:Color} = 15066597
    ?map:Field_Type:Radio1{prop:Trn} = 0
    ?map:Field_Type:Radio2{prop:Font,3} = -1
    ?map:Field_Type:Radio2{prop:Color} = 15066597
    ?map:Field_Type:Radio2{prop:Trn} = 0
    ?map:Field_Type:Radio3{prop:Font,3} = -1
    ?map:Field_Type:Radio3{prop:Color} = 15066597
    ?map:Field_Type:Radio3{prop:Trn} = 0
    ?DatePicture{prop:FontColor} = -1
    ?DatePicture{prop:Color} = 15066597
    ?map:Lookup{prop:Font,3} = -1
    ?map:Lookup{prop:Color} = 15066597
    ?map:Lookup{prop:Trn} = 0
    ?map:Force_Lookup{prop:Font,3} = -1
    ?map:Force_Lookup{prop:Color} = 15066597
    ?map:Force_Lookup{prop:Trn} = 0
    ?map:Compulsory{prop:Font,3} = -1
    ?map:Compulsory{prop:Color} = 15066597
    ?map:Compulsory{prop:Trn} = 0
    ?map:RestrictLength{prop:Font,3} = -1
    ?map:RestrictLength{prop:Color} = 15066597
    ?map:RestrictLength{prop:Trn} = 0
    ?RestrictGroup{prop:Font,3} = -1
    ?RestrictGroup{prop:Color} = 15066597
    ?RestrictGroup{prop:Trn} = 0
    ?map:LengthFrom:Prompt{prop:FontColor} = -1
    ?map:LengthFrom:Prompt{prop:Color} = 15066597
    If ?map:LengthFrom{prop:ReadOnly} = True
        ?map:LengthFrom{prop:FontColor} = 65793
        ?map:LengthFrom{prop:Color} = 15066597
    Elsif ?map:LengthFrom{prop:Req} = True
        ?map:LengthFrom{prop:FontColor} = 65793
        ?map:LengthFrom{prop:Color} = 8454143
    Else ! If ?map:LengthFrom{prop:Req} = True
        ?map:LengthFrom{prop:FontColor} = 65793
        ?map:LengthFrom{prop:Color} = 16777215
    End ! If ?map:LengthFrom{prop:Req} = True
    ?map:LengthFrom{prop:Trn} = 0
    ?map:LengthFrom{prop:FontStyle} = font:Bold
    ?map:LengthTo:Prompt{prop:FontColor} = -1
    ?map:LengthTo:Prompt{prop:Color} = 15066597
    If ?map:LengthTo{prop:ReadOnly} = True
        ?map:LengthTo{prop:FontColor} = 65793
        ?map:LengthTo{prop:Color} = 15066597
    Elsif ?map:LengthTo{prop:Req} = True
        ?map:LengthTo{prop:FontColor} = 65793
        ?map:LengthTo{prop:Color} = 8454143
    Else ! If ?map:LengthTo{prop:Req} = True
        ?map:LengthTo{prop:FontColor} = 65793
        ?map:LengthTo{prop:Color} = 16777215
    End ! If ?map:LengthTo{prop:Req} = True
    ?map:LengthTo{prop:Trn} = 0
    ?map:LengthTo{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?mfo:Field{prop:ReadOnly} = True
        ?mfo:Field{prop:FontColor} = 65793
        ?mfo:Field{prop:Color} = 15066597
    Elsif ?mfo:Field{prop:Req} = True
        ?mfo:Field{prop:FontColor} = 65793
        ?mfo:Field{prop:Color} = 8454143
    Else ! If ?mfo:Field{prop:Req} = True
        ?mfo:Field{prop:FontColor} = 65793
        ?mfo:Field{prop:Color} = 16777215
    End ! If ?mfo:Field{prop:Req} = True
    ?mfo:Field{prop:Trn} = 0
    ?mfo:Field{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?map:ForceFormat{prop:Font,3} = -1
    ?map:ForceFormat{prop:Color} = 15066597
    ?map:ForceFormat{prop:Trn} = 0
    ?FieldFormatTypes{prop:Font,3} = -1
    ?FieldFormatTypes{prop:Color} = 15066597
    ?FieldFormatTypes{prop:Trn} = 0
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Prompt8:2{prop:FontColor} = -1
    ?Prompt8:2{prop:Color} = 15066597
    ?Prompt8:3{prop:FontColor} = -1
    ?Prompt8:3{prop:Color} = 15066597
    ?Prompt8:4{prop:FontColor} = -1
    ?Prompt8:4{prop:Color} = 15066597
    ?Prompt8:7{prop:FontColor} = -1
    ?Prompt8:7{prop:Color} = 15066597
    ?Prompt8:5{prop:FontColor} = -1
    ?Prompt8:5{prop:Color} = 15066597
    ?Prompt8:6{prop:FontColor} = -1
    ?Prompt8:6{prop:Color} = 15066597
    ?map:FieldFormat:Prompt{prop:FontColor} = -1
    ?map:FieldFormat:Prompt{prop:Color} = 15066597
    If ?map:FieldFormat{prop:ReadOnly} = True
        ?map:FieldFormat{prop:FontColor} = 65793
        ?map:FieldFormat{prop:Color} = 15066597
    Elsif ?map:FieldFormat{prop:Req} = True
        ?map:FieldFormat{prop:FontColor} = 65793
        ?map:FieldFormat{prop:Color} = 8454143
    Else ! If ?map:FieldFormat{prop:Req} = True
        ?map:FieldFormat{prop:FontColor} = 65793
        ?map:FieldFormat{prop:Color} = 16777215
    End ! If ?map:FieldFormat{prop:Req} = True
    ?map:FieldFormat{prop:Trn} = 0
    ?map:FieldFormat{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMANFAUPA',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMANFAUPA',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateMANFAUPA',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateMANFAUPA',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMANFAUPA',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateMANFAUPA',1)
    SolaceViewVars('tmp:DatePicture',tmp:DatePicture,'UpdateMANFAUPA',1)
    SolaceViewVars('Filepath',Filepath,'UpdateMANFAUPA',1)
    SolaceViewVars('SaveMFP',SaveMFP,'UpdateMANFAUPA',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?General_Sheet;  SolaceCtrlName = '?General_Sheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Manufacturer:Prompt;  SolaceCtrlName = '?map:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Manufacturer;  SolaceCtrlName = '?map:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Number:Prompt;  SolaceCtrlName = '?map:Field_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Number;  SolaceCtrlName = '?map:Field_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Name:Prompt;  SolaceCtrlName = '?map:Field_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Name;  SolaceCtrlName = '?map:Field_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DatePicture;  SolaceCtrlName = '?tmp:DatePicture';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Type;  SolaceCtrlName = '?map:Field_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Type:Radio1;  SolaceCtrlName = '?map:Field_Type:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Type:Radio2;  SolaceCtrlName = '?map:Field_Type:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Field_Type:Radio3;  SolaceCtrlName = '?map:Field_Type:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DatePicture;  SolaceCtrlName = '?DatePicture';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Lookup;  SolaceCtrlName = '?map:Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Force_Lookup;  SolaceCtrlName = '?map:Force_Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:Compulsory;  SolaceCtrlName = '?map:Compulsory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:RestrictLength;  SolaceCtrlName = '?map:RestrictLength';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RestrictGroup;  SolaceCtrlName = '?RestrictGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:LengthFrom:Prompt;  SolaceCtrlName = '?map:LengthFrom:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:LengthFrom;  SolaceCtrlName = '?map:LengthFrom';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:LengthTo:Prompt;  SolaceCtrlName = '?map:LengthTo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:LengthTo;  SolaceCtrlName = '?map:LengthTo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mfo:Field;  SolaceCtrlName = '?mfo:Field';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportButton;  SolaceCtrlName = '?ImportButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:ForceFormat;  SolaceCtrlName = '?map:ForceFormat';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FieldFormatTypes;  SolaceCtrlName = '?FieldFormatTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:2;  SolaceCtrlName = '?Prompt8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:3;  SolaceCtrlName = '?Prompt8:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:4;  SolaceCtrlName = '?Prompt8:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:7;  SolaceCtrlName = '?Prompt8:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:5;  SolaceCtrlName = '?Prompt8:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:6;  SolaceCtrlName = '?Prompt8:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:FieldFormat:Prompt;  SolaceCtrlName = '?map:FieldFormat:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?map:FieldFormat;  SolaceCtrlName = '?map:FieldFormat';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Part Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Part Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMANFAUPA')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMANFAUPA')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?map:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(map:Record,History::map:Record)
  SELF.AddHistoryField(?map:Manufacturer,1)
  SELF.AddHistoryField(?map:Field_Number,2)
  SELF.AddHistoryField(?map:Field_Name,3)
  SELF.AddHistoryField(?map:Field_Type,4)
  SELF.AddHistoryField(?map:Lookup,5)
  SELF.AddHistoryField(?map:Force_Lookup,6)
  SELF.AddHistoryField(?map:Compulsory,7)
  SELF.AddHistoryField(?map:RestrictLength,8)
  SELF.AddHistoryField(?map:LengthFrom,9)
  SELF.AddHistoryField(?map:LengthTo,10)
  SELF.AddHistoryField(?map:ForceFormat,11)
  SELF.AddHistoryField(?map:FieldFormat,12)
  SELF.AddUpdateFile(Access:MANFAUPA)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANFAULT.Open
  Relate:USELEVEL.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFAUPA
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:MANFPALO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! What is the picture?
  Case map:DateType
      Of '@d5'
          tmp:DatePicture = 'DD/MM/YY'
      Of '@d6'
          tmp:DatePicture = 'DD/MM/YYYY'
      Of '@d1'
          tmp:DatePicture = 'MM/DD/YY'
      Of '@d2'
          tmp:DatePicture = 'MM/DD/YYYY'
      Of '@d13'
          tmp:DatePicture = 'MM/YY'
      Of '@d14'
          tmp:DatePicture = 'MM/YYYY'
      Of '@d15'
          tmp:DatePicture = 'YY/MM'
      Of '@d16'
          tmp:DatePicture = 'YYYY/MM'
      Of '@d9'
          tmp:DatePicture = 'YY/MM/DD'
      Of '@d10'
          tmp:DatePicture = 'YYYY/MM/DD'
      Of '@d11'
          tmp:DatePicture = 'YYMMDD'
      Of '@d12'
          tmp:DatePicture = 'YYYYMMDD'
  End !tmp:DatePicture
  Post(Event:Accepted,?map:Field_Type)
  Do RecolourWindow
  ?tmp:DatePicture{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,mfp:Field_Key)
  BRW8.AddRange(mfp:Field_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,mfp:Field,1,BRW8)
  BRW8.AddField(mfp:Field,BRW8.Q.mfp:Field)
  BRW8.AddField(mfp:Description,BRW8.Q.mfp:Description)
  BRW8.AddField(mfp:Manufacturer,BRW8.Q.mfp:Manufacturer)
  BRW8.AddField(mfp:Field_Number,BRW8.Q.mfp:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?map:Lookup{Prop:Checked} = True
    UNHIDE(?map:Force_Lookup)
    UNHIDE(?Insert)
    UNHIDE(?Change)
    UNHIDE(?Delete)
    UNHIDE(?ImportButton)
  END
  IF ?map:Lookup{Prop:Checked} = False
    HIDE(?map:Force_Lookup)
    HIDE(?Insert)
    HIDE(?Change)
    HIDE(?Delete)
    HIDE(?ImportButton)
  END
  IF ?map:RestrictLength{Prop:Checked} = True
    UNHIDE(?RestrictGroup)
  END
  IF ?map:RestrictLength{Prop:Checked} = False
    HIDE(?RestrictGroup)
  END
  IF ?map:ForceFormat{Prop:Checked} = True
    UNHIDE(?map:FieldFormat:Prompt)
    UNHIDE(?map:FieldFormat)
    UNHIDE(?FieldFormatTypes)
  END
  IF ?map:ForceFormat{Prop:Checked} = False
    HIDE(?map:FieldFormat:Prompt)
    HIDE(?map:FieldFormat)
    HIDE(?FieldFormatTypes)
  END
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAULT.Close
    Relate:USELEVEL.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMANFAUPA',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request  = Insertrecord
      If map:field_number = ''
          Case MessageEx('You have not entered a Field Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_update# = 0
      End
  End
  If do_update# = 1 And map:lookup = 'YES'
      glo:select1  = map:manufacturer
      glo:select2  = map:field_number
      glo:select3  = map:field_name
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFPALO
    ReturnValue = GlobalResponse
  END
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
  End !If do_update# = 1 And maf:lookup = 'YES'
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?map:Field_Number
      IF Access:MANFAUPA.TryValidateField(2)
        SELECT(?map:Field_Number)
        QuickWindow{Prop:AcceptAll} = False
        CYCLE
      ELSE
        FieldColorQueue.Feq = ?map:Field_Number
        GET(FieldColorQueue, FieldColorQueue.Feq)
        IF ~ERRORCODE()
          ?map:Field_Number{Prop:FontColor} = FieldColorQueue.OldColor
          DELETE(FieldColorQueue)
        END
      END
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW8.ApplyRange
      BRW8.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?map:Field_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?map:Field_Type, Accepted)
      Case map:Field_Type
          Of 'DATE'
              ?tmp:DatePicture{prop:Hide} = 0
              ?DatePicture{prop:Hide} = 0
              map:ForceFormat = 0
              ?map:ForceFormat{prop:Disable} = 1
              map:Lookup = 'NO'
              map:RestrictLength = 0
          Of 'STRING'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?map:ForceFormat{prop:Disable} = 0
          Of 'NUMBER'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?map:ForceFormat{prop:Disable} = 1
              map:ForceFormat = 0
      End !map:Field_Type
      Post(Event:Accepted,?map:ForceFormat)
      Post(Event:Accepted,?map:Lookup)
      Post(Event:Accepted,?map:RestrictLength)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?map:Field_Type, Accepted)
    OF ?map:Lookup
      IF ?map:Lookup{Prop:Checked} = True
        UNHIDE(?map:Force_Lookup)
        UNHIDE(?Insert)
        UNHIDE(?Change)
        UNHIDE(?Delete)
        UNHIDE(?ImportButton)
      END
      IF ?map:Lookup{Prop:Checked} = False
        HIDE(?map:Force_Lookup)
        HIDE(?Insert)
        HIDE(?Change)
        HIDE(?Delete)
        HIDE(?ImportButton)
      END
      ThisWindow.Reset
    OF ?map:RestrictLength
      IF ?map:RestrictLength{Prop:Checked} = True
        UNHIDE(?RestrictGroup)
      END
      IF ?map:RestrictLength{Prop:Checked} = False
        HIDE(?RestrictGroup)
      END
      ThisWindow.Reset
    OF ?ImportButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
      ! Start Change BE021 BE(2/12/03)
      IF (FileDialog('Import File', Filepath, 'CSV Files|*.CSV|Text Files|*.TXT|All Files|*.*', |
                      file:keepdir + file:longname)) THEN
          IMPORT_FILE{PROP:NAME} = CLIP(Filepath)
          OPEN(IMPORT_FILE, 20h)
          IF (ERRORCODE()) THEN
              MESSAGE('FILE OPEN ERROR: ' & ERROR())
          ELSE
              SET(IMPORT_FILE)
              SaveMFP = access:MANFPALO.SaveFile()
              SETCURSOR(Cursor:wait)
              LOOP
                  NEXT(IMPORT_FILE)
                  IF (ERRORCODE()) THEN
                      BREAK
                  END
                  access:MANFPALO.ClearKey(mfp:Field_Key)
                  mfp:Manufacturer = map:Manufacturer
                  mfp:Field_Number = map:Field_Number
                  mfp:Field = IMP:Code
                  IF (access:MANFPALO.TryFetch(mfp:Field_Key) = Level:Benign) THEN
                          mfp:Description = IMP:Description
                          access:MANFPALO.Update()
                  ELSE
                      IF (access:MANFPALO.Primerecord() = Level:benign) THEN
                          mfp:Manufacturer = map:Manufacturer
                          mfp:Field_Number = map:Field_Number
                          mfp:Field = UPPER(IMP:Code)
                          mfp:Description = UPPER(IMP:Description)
                          access:MANFPALO.Insert()
                      END
                  END
              END
              CLOSE(IMPORT_FILE)
              access:MANFPALO.restorefile(SaveMFP)
              BRW8.ResetSort(1)
              SETCURSOR()
          END
      END
      ! End Change BE021 BE(2/12/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
    OF ?map:ForceFormat
      IF ?map:ForceFormat{Prop:Checked} = True
        UNHIDE(?map:FieldFormat:Prompt)
        UNHIDE(?map:FieldFormat)
        UNHIDE(?FieldFormatTypes)
      END
      IF ?map:ForceFormat{Prop:Checked} = False
        HIDE(?map:FieldFormat:Prompt)
        HIDE(?map:FieldFormat)
        HIDE(?FieldFormatTypes)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case tmp:DatePicture
      Of 'DD/MM/YY'
          map:DateType = '@d5'
      Of 'DD/MM/YYYY'
          map:DateType = '@d6'
      Of 'MM/DD/YY'
          map:DateType = '@d1'
      Of 'MM/DD/YYYY'
          map:DateType = '@d2'
      Of 'MM/YY'
          map:DateType = '@d13'
      Of 'MM/YYYY'
          map:DateType = '@d14'
      Of 'YY/MM'
          map:DateType = '@d15'
      Of 'YYYY/MM'
          map:DateType = '@d16'
      Of 'YY/MM/DD'
          map:DateType = '@d9'
      Of 'YYYY/MM/DD'
          map:DateType = '@d10'
      Of 'YYMMDD'
          map:DateType = '@d11'
      Of 'YYYYMMDD'
          map:DateType = '@d12'
  End !tmp:DatePicture
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMANFAUPA')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?map:Field_Number
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse)
      BRW8.ApplyRange
      BRW8.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(event:accepted,?map:compulsory)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW8.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = map:Manufacturer
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = map:Field_Number
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

