

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01020.INC'),ONCE        !Local module procedure declarations
                     END


RapidLetterPrint PROCEDURE                            !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
tmp:counter          LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:lettername       STRING(30)
tmp:jobnumber        LONG
JobNumberQueue       QUEUE,PRE(QUE)
jobnumber            LONG
number               LONG
                     END
tmp:PrintType        BYTE(0)
window               WINDOW('Rapid Letter/Label Printing'),AT(,,331,299),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,324,264),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Rapid Letter/Label Printing'),AT(8,8,224,20),USE(?Prompt1),FONT('Tahoma',14,,FONT:bold,CHARSET:ANSI)
                           OPTION('Print Type'),AT(176,28,120,24),USE(tmp:PrintType),BOXED
                             RADIO('Letter'),AT(184,40,36,8),USE(?tmp:PrintType:Radio1),VALUE('0')
                             RADIO('Job Label'),AT(244,40),USE(?tmp:PrintType:Radio2),VALUE('1')
                           END
                           PROMPT('Select the letter you wish to print'),AT(100,56),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:bold+FONT:underline)
                           PROMPT('Select the jobs to print '),AT(100,88),USE(?Prompt2:2),FONT(,,COLOR:Navy,FONT:bold+FONT:underline)
                           PROMPT('Letter Name'),AT(100,68),USE(?tmp:lettername:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(176,68,124,10),USE(tmp:lettername),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter Name'),TIP('Letter Name'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(304,68,10,10),USE(?LookupLetterName),SKIP,ICON('List3.ico')
                           IMAGE('WIZARD.GIF'),AT(12,96),USE(?Image1)
                           PROMPT('Job Number'),AT(100,100),USE(?tmp:jobnumber:Prompt),TRN
                           ENTRY(@s8),AT(176,100,64,10),USE(tmp:jobnumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                           LIST,AT(176,116,148,144),USE(?List1),VSCROLL,FORMAT('32L(2)|M~Job Number~@s8@'),FROM(JobNumberQueue)
                         END
                       END
                       PANEL,AT(4,272,324,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Print'),AT(208,276,56,16),USE(?Button3),LEFT,ICON('MAIL.GIF')
                       BUTTON('Cancel'),AT(268,276,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:PrintType{prop:Font,3} = -1
    ?tmp:PrintType{prop:Color} = 15066597
    ?tmp:PrintType{prop:Trn} = 0
    ?tmp:PrintType:Radio1{prop:Font,3} = -1
    ?tmp:PrintType:Radio1{prop:Color} = 15066597
    ?tmp:PrintType:Radio1{prop:Trn} = 0
    ?tmp:PrintType:Radio2{prop:Font,3} = -1
    ?tmp:PrintType:Radio2{prop:Color} = 15066597
    ?tmp:PrintType:Radio2{prop:Trn} = 0
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    ?tmp:lettername:Prompt{prop:FontColor} = -1
    ?tmp:lettername:Prompt{prop:Color} = 15066597
    If ?tmp:lettername{prop:ReadOnly} = True
        ?tmp:lettername{prop:FontColor} = 65793
        ?tmp:lettername{prop:Color} = 15066597
    Elsif ?tmp:lettername{prop:Req} = True
        ?tmp:lettername{prop:FontColor} = 65793
        ?tmp:lettername{prop:Color} = 8454143
    Else ! If ?tmp:lettername{prop:Req} = True
        ?tmp:lettername{prop:FontColor} = 65793
        ?tmp:lettername{prop:Color} = 16777215
    End ! If ?tmp:lettername{prop:Req} = True
    ?tmp:lettername{prop:Trn} = 0
    ?tmp:lettername{prop:FontStyle} = font:Bold
    ?tmp:jobnumber:Prompt{prop:FontColor} = -1
    ?tmp:jobnumber:Prompt{prop:Color} = 15066597
    If ?tmp:jobnumber{prop:ReadOnly} = True
        ?tmp:jobnumber{prop:FontColor} = 65793
        ?tmp:jobnumber{prop:Color} = 15066597
    Elsif ?tmp:jobnumber{prop:Req} = True
        ?tmp:jobnumber{prop:FontColor} = 65793
        ?tmp:jobnumber{prop:Color} = 8454143
    Else ! If ?tmp:jobnumber{prop:Req} = True
        ?tmp:jobnumber{prop:FontColor} = 65793
        ?tmp:jobnumber{prop:Color} = 16777215
    End ! If ?tmp:jobnumber{prop:Req} = True
    ?tmp:jobnumber{prop:Trn} = 0
    ?tmp:jobnumber{prop:FontStyle} = font:Bold
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        case messageex('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,charset:ansi,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            of 1 ! &yes button
                tmp:cancel = 1
            of 2 ! &no button
        end!case messageex
    end!if cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()

Audit_Trail         Routine
    get(audit,0)
    if access:audit.primerecord() = level:benign
        aud:notes         = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                            '<13,10>E.S.N. / I.M.E.I.: ' & Clip(job:esn) & |
                            '<13,10>M.S.N.: ' & Clip(job:msn)
        count# = 0
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        set(jac:ref_number_key,jac:ref_number_key)
        loop
            if access:jobacc.next()
               break
            end !if
            if jac:ref_number <> job:ref_number      |
                then break.  ! end if
            count# += 1
            aud:notes = Clip(aud:notes) & '<13,10>Accessory ' & Clip(count#) & ': ' & Clip(jac:accessory)
        end !loop
        aud:ref_number    = job:ref_number
        aud:date          = Today()
        aud:time          = Clock()
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        aud:user = use:user_code
        aud:action        = 'RAPID LETTER PRINT - '&CLIP(tmp:lettername)
        if access:audit.insert()
            access:audit.cancelautoinc()
        end
    end!if access:audit.primerecord() = level:benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'RapidLetterPrint',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:counter',tmp:counter,'RapidLetterPrint',1)
    SolaceViewVars('tmp:lettername',tmp:lettername,'RapidLetterPrint',1)
    SolaceViewVars('tmp:jobnumber',tmp:jobnumber,'RapidLetterPrint',1)
    SolaceViewVars('JobNumberQueue:jobnumber',JobNumberQueue:jobnumber,'RapidLetterPrint',1)
    SolaceViewVars('JobNumberQueue:number',JobNumberQueue:number,'RapidLetterPrint',1)
    SolaceViewVars('tmp:PrintType',tmp:PrintType,'RapidLetterPrint',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PrintType;  SolaceCtrlName = '?tmp:PrintType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PrintType:Radio1;  SolaceCtrlName = '?tmp:PrintType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PrintType:Radio2;  SolaceCtrlName = '?tmp:PrintType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:lettername:Prompt;  SolaceCtrlName = '?tmp:lettername:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:lettername;  SolaceCtrlName = '?tmp:lettername';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLetterName;  SolaceCtrlName = '?LookupLetterName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:jobnumber:Prompt;  SolaceCtrlName = '?tmp:jobnumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:jobnumber;  SolaceCtrlName = '?tmp:jobnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('RapidLetterPrint')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'RapidLetterPrint')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Access:JOBS.UseFile
  Access:JOBACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'RapidLetterPrint',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:PrintType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PrintType, Accepted)
      Case tmp:PrintType
          Of 0
              ?tmp:LetterName{prop:Disable} = 0
              ?tmp:LetterName:Prompt{prop:Disable} = 0
              ?LookupLetterName{prop:Disable} = 0
          Of 1
              ?tmp:LetterName{prop:Disable} = 1
              ?tmp:LetterName:Prompt{prop:Disable} = 1
              ?LookupLetterName{prop:Disable} = 1
      End !tmp:PrintType
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PrintType, Accepted)
    OF ?LookupLetterName
      ThisWindow.Update
      GlobalRequest = SelectRecord
      tmp:lettername = BrowseAllLetters()
      ThisWindow.Reset
    OF ?tmp:jobnumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:jobnumber, Accepted)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          que:jobnumber = tmp:jobnumber
          Get(jobnumberqueue,que:jobnumber)
          If Error()
              que:jobnumber = tmp:jobnumber
              que:number  += tmp:counter
              Add(jobnumberqueue)
              Sort(jobnumberqueue,que:number)
          End!If Error()
      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          Case MessageEx('Cannot find the selected job number.','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
          Of 1 ! &OK Button
          End!Case MessageEx
          
      End !If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      Select(?tmp:JobNumber)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:jobnumber, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      error# = 0
      If tmp:lettername = '' and tmp:PrintType = 0
          Case MessageEx('You have not selected a letter.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?tmp:lettername)
          error# = 1
      End!If tmp:lettername = ''
      If error# = 0 And ~Records(jobnumberqueue)
          Case MessageEx('You have not selected any jobs.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          error# = 1
      End!If error# = 0 And ~Records(jobnumberqueue)
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      
      thiswindow.reset(1)
      open(progresswindow)
      ?progress:userstring{prop:text} = 'Running...'
      ?progress:pcttext{prop:text} = '0% Completed'
      
      recordstoprocess    = Records(jobnumberqueue)
      
      !---before routine
      
      Loop x# = 1 To Records(jobnumberqueue)
          Get(jobnumberqueue,x#)
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number = que:jobnumber
          If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
              !Found
      
              Case tmp:PrintType
                  Of 0 !Letter
                      PrintLetter(job:Ref_Number,tmp:LetterName)
                      DO Audit_Trail
                  Of 1 !Label
                      glo:Select1 = job:Ref_Number
                      Set(defaults)
                      access:defaults.next()
                      Case def:label_printer_type
                          Of 'TEC B-440 / B-442'
                              If job:bouncer = 'B'
                                  Thermal_Labels('SE')
                              Else!If job:bouncer = 'B'
                                  Thermal_Labels('')
                              End!If job:bouncer = 'B'
                              
                          Of 'TEC B-452'
                              Thermal_Labels_B452
                      End!Case def:label_printer_type
                      glo:select1 = ''
              End !Case tmp:PrintType
          !---insert routine
              do getnextrecord2
              cancelcheck# += 1
              if cancelcheck# > (recordstoprocess/100)
                  do cancelcheck
                  if tmp:cancel = 1
                      break
                  end!if tmp:cancel = 1
                  cancelcheck# = 0
              end!if cancelcheck# > 50
          !---after routine
      
          Else! If access:.tryfetch(job:ref_number_key) = Level:Benign
              !Error
          End! If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      
      
      End!Loop x# = 1 To Records(jobnumberqueue)
      do endprintrun
      close(progresswindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'RapidLetterPrint')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:lettername
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:lettername, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Letter Name')
             Post(Event:Accepted,?LookupLetterName)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupLetterName)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:lettername, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

