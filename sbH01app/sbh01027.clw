

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01027.INC'),ONCE        !Local module procedure declarations
                     END



Samsung_Defaults PROCEDURE                            !Generated from procedure template - Window

FilesOpened          BYTE
IniFilename          STRING(100)
SamDefaults          GROUP,PRE(sam)
Account_Number       STRING(15)
Warranty_Charge_Type STRING(30)
Chargeable_Charge_Type STRING(30)
Transit_Type         STRING(30)
Job_Priority         STRING(30)
Unit_Type            STRING(30)
Status_Type          STRING(30)
Delivery_Text        STRING(1000)
                     END
Yes_temp             STRING('YES')
No_temp              STRING('NO {1}')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?sam:Account_Number
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?sam:Warranty_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?sam:Transit_Type
trt:Transit_Type       LIKE(trt:Transit_Type)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?sam:Job_Priority
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?sam:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?sam:Chargeable_Charge_Type
cha:Charge_Type        LIKE(cha:Charge_Type)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB2::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
FDCB4::View:FileDropCombo VIEW(TRANTYPE)
                       PROJECT(trt:Transit_Type)
                     END
FDCB5::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                     END
FDCB8::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB6::View:FileDropCombo VIEW(CHARTYPE)
                       PROJECT(cha:Charge_Type)
                     END
mo:SelectedTab  Long  ! makeover template ! LocalTreat = Default ; PT = Window  WT = Window
Window               WINDOW('Samsung Import Defaults'),AT(,,231,214),FONT('Arial',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,180),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Account Number'),AT(8,20),USE(?Prompt1)
                           COMBO(@s15),AT(84,20,124,10),USE(sam:Account_Number),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('65L(2)|M@s15@120L(2)@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           PROMPT('Warranty Type'),AT(8,36),USE(?Prompt2)
                           COMBO(@s30),AT(84,36,124,10),USE(sam:Warranty_Charge_Type),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Charge Type'),AT(8,52),USE(?Prompt9)
                           COMBO(@s30),AT(84,52,124,10),USE(sam:Chargeable_Charge_Type),IMM,VSCROLL,FONT('Arial',8,,FONT:bold),FORMAT('120L|M~Charge Type~L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           PROMPT('Transit Type'),AT(8,68),USE(?Prompt4)
                           COMBO(@s30),AT(84,68,124,10),USE(sam:Transit_Type),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           PROMPT('Job Turnaround Time'),AT(8,84),USE(?Prompt5)
                           COMBO(@s30),AT(84,84,124,10),USE(sam:Job_Priority),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4)
                           PROMPT('Unit Type'),AT(8,100),USE(?Prompt6)
                           COMBO(@s30),AT(84,100,124,10),USE(sam:Unit_Type),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5)
                           PROMPT('Status'),AT(8,116),USE(?Prompt7),TRN
                           ENTRY(@s30),AT(84,116,124,10),USE(sam:Status_Type),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON,AT(212,116,10,10),USE(?LookupStatus),SKIP,ICON('List3.ico')
                           PROMPT('Delivery Text'),AT(8,132),USE(?Prompt8)
                           TEXT,AT(84,132,124,48),USE(sam:Delivery_Text),VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,188,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,192,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(168,192,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB1                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Samsung_Defaults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Samsung_Defaults',1)
    SolaceViewVars('IniFilename',IniFilename,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Account_Number',SamDefaults:Account_Number,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Warranty_Charge_Type',SamDefaults:Warranty_Charge_Type,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Chargeable_Charge_Type',SamDefaults:Chargeable_Charge_Type,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Transit_Type',SamDefaults:Transit_Type,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Job_Priority',SamDefaults:Job_Priority,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Unit_Type',SamDefaults:Unit_Type,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Status_Type',SamDefaults:Status_Type,'Samsung_Defaults',1)
    SolaceViewVars('SamDefaults:Delivery_Text',SamDefaults:Delivery_Text,'Samsung_Defaults',1)
    SolaceViewVars('Yes_temp',Yes_temp,'Samsung_Defaults',1)
    SolaceViewVars('No_temp',No_temp,'Samsung_Defaults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Account_Number;  SolaceCtrlName = '?sam:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Warranty_Charge_Type;  SolaceCtrlName = '?sam:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Chargeable_Charge_Type;  SolaceCtrlName = '?sam:Chargeable_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Transit_Type;  SolaceCtrlName = '?sam:Transit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Job_Priority;  SolaceCtrlName = '?sam:Job_Priority';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Unit_Type;  SolaceCtrlName = '?sam:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Status_Type;  SolaceCtrlName = '?sam:Status_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStatus;  SolaceCtrlName = '?LookupStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sam:Delivery_Text;  SolaceCtrlName = '?sam:Delivery_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, Window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Samsung_Defaults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Samsung_Defaults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  SELF.FilesOpened = True
  IniFilename = PATH() & '\SAMSUNGIMP.INI'
  sam:Account_Number = GETINI('Defaults', 'Account_Number','',IniFilename)
  sam:Warranty_Charge_Type = GETINI('Defaults', 'Warranty_Charge_Type','',IniFilename)
  sam:Chargeable_Charge_Type = GETINI('Defaults', 'Chargeable_Charge_Type','',IniFilename)
  sam:Transit_Type = GETINI('Defaults', 'Transit_Type','',IniFilename)
  sam:Job_Priority = GETINI('Defaults', 'Job_Priority','',IniFilename)
  sam:Unit_Type = GETINI('Defaults', 'Unit_Type','',IniFilename)
  sam:Status_Type = GETINI('Defaults', 'Status_Type','',IniFilename)
  sam:Delivery_text = GETINI('Defaults', 'Delivery_text','',IniFilename)
  
  OPEN(Window)
  SELF.Opened=True
    Loop DBHControl# = Firstfield() To LastField()
        If DBHControl#{prop:type} = Create:OPTION Or |
            DBHControl#{prop:type} = Create:RADIO Or |
            DBhControl#{prop:type} = Create:GROUP
            DBHControl#{prop:trn} = 1      
        End!DBhControl#{prop:type} = 'GROUP'
    End!Loop DBHControl# = Firstfield() To LastField()
  IF ?sam:Status_Type{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?sam:Status_Type{Prop:Tip}
  END
  IF ?sam:Status_Type{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?sam:Status_Type{Prop:Msg}
  END
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,?Sheet1)
      
  
  FDCB1.Init(sam:Account_Number,?sam:Account_Number,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB1.Q &= Queue:FileDropCombo
  FDCB1.AddSortOrder(sub:Account_Number_Key)
  FDCB1.AddField(sub:Account_Number,FDCB1.Q.sub:Account_Number)
  FDCB1.AddField(sub:Company_Name,FDCB1.Q.sub:Company_Name)
  FDCB1.AddField(sub:RecordNumber,FDCB1.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB1.WindowComponent)
  FDCB1.DefaultFill = 0
  FDCB2.Init(sam:Warranty_Charge_Type,?sam:Warranty_Charge_Type,Queue:FileDropCombo:1.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo:1,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo:1
  FDCB2.AddSortOrder(cha:Warranty_Key)
  FDCB2.AddRange(cha:Warranty,Yes_temp)
  FDCB2.AddField(cha:Charge_Type,FDCB2.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB4.Init(sam:Transit_Type,?sam:Transit_Type,Queue:FileDropCombo:3.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo:3,Relate:TRANTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo:3
  FDCB4.AddSortOrder(trt:Transit_Type_Key)
  FDCB4.AddField(trt:Transit_Type,FDCB4.Q.trt:Transit_Type)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  FDCB5.Init(sam:Job_Priority,?sam:Job_Priority,Queue:FileDropCombo:4.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo:4,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo:4
  FDCB5.AddSortOrder()
  FDCB5.AddField(tur:Turnaround_Time,FDCB5.Q.tur:Turnaround_Time)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB8.Init(sam:Unit_Type,?sam:Unit_Type,Queue:FileDropCombo:5.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:5,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:5
  FDCB8.AddSortOrder(uni:Unit_Type_Key)
  FDCB8.AddField(uni:Unit_Type,FDCB8.Q.uni:Unit_Type)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB6.Init(sam:Chargeable_Charge_Type,?sam:Chargeable_Charge_Type,Queue:FileDropCombo:2.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:2,Relate:CHARTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:2
  FDCB6.AddSortOrder(cha:Warranty_Key)
  FDCB6.AddRange(cha:Warranty,No_temp)
  FDCB6.AddField(cha:Charge_Type,FDCB6.Q.cha:Charge_Type)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Samsung_Defaults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      PUTINI('Defaults', 'Account_Number', sam:Account_Number, IniFilename)
      PUTINI('Defaults', 'Warranty_Charge_Type', sam:Warranty_Charge_Type, IniFilename)
      PUTINI('Defaults', 'Chargeable_Charge_Type', sam:Chargeable_Charge_Type, IniFilename)
      PUTINI('Defaults', 'Transit_Type', sam:Transit_Type, IniFilename)
      PUTINI('Defaults', 'Job_Priority', sam:Job_Priority, IniFilename)
      PUTINI('Defaults', 'Unit_Type', sam:Unit_Type, IniFilename)
      PUTINI('Defaults', 'Status_Type', sam:Status_Type, IniFilename)
      PUTINI('Defaults', 'Delivery_text', sam:Delivery_text, IniFilename)
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupStatus
      ThisWindow.Update
      ! Lookup button pressed. Call select procedure if required
      sts:Status = sam:Status_Type                    ! Move value for lookup
      IF Access:STATUS.TryFetch(sts:Status_Key)       ! IF record not found
         sts:Status = sam:Status_Type                 ! Move value for lookup
         ! Based on ?sam:Status_Type
         SET(sts:Status_Key,sts:Status_Key)           ! Prime order for browse
         Access:STATUS.TryNext()
      END
      GlobalRequest = SelectRecord
      Browse_Status                                   ! Allow user to select
      IF GlobalResponse <> RequestCompleted           ! User cancelled
         SELECT(?LookupStatus)                        ! Reselect control
         CYCLE                                        ! Resume accept loop
      .
      CHANGE(?sam:Status_Type,sts:Status)
      GlobalResponse = RequestCancelled               ! Reset global response
      SELECT(?LookupStatus+1)                         ! Select next control in sequence
      SELF.Request = SELF.OriginalRequest             ! Reset local request
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Samsung_Defaults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB1.ResetQueue(1)
      FDCB2.ResetQueue(1)
      FDCB4.ResetQueue(1)
      FDCB5.ResetQueue(1)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

