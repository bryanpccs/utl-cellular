

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01012.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMERGE PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::mer:Record  LIKE(mer:RECORD),STATIC
QuickWindow          WINDOW('Update The Merge Fields'),AT(,,303,228),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('UpdateMERGE'),SYSTEM,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,296,196),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Field Name'),AT(8,20),USE(?MER:FieldName:Prompt)
                           TEXT,AT(84,20,124,10),USE(mer:FieldName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,SINGLE
                           PROMPT('File Name'),AT(8,34),USE(?MER:FileName:Prompt)
                           TEXT,AT(84,34,124,10),USE(mer:FileName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,SINGLE
                           PROMPT('Use the proper dictionary name ("prefix:field"), e.g. JOB:ORDER_NUMBER, JOBE:REF' &|
   'NUMBER'),AT(84,46,140,16),USE(?Prompt5),FONT(,7,,)
                           PROMPT('For SQL files use the file name followed by field name ("file.field"), e.g. WEBJ' &|
   'OB.SBJOBNO, CCEXCHANGE.EXCHANGEDATE'),AT(84,64,188,18),USE(?Prompt6),FONT(,7,,)
                           PROMPT('Description'),AT(8,88),USE(?MER:FileName:Prompt:2)
                           TEXT,AT(84,88,124,40),USE(mer:Description),VSCROLL,UPR
                           PROMPT('Total Field Length'),AT(8,134),USE(?mer:TotalFieldLength:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@s8),AT(84,134,64,10),USE(mer:TotalFieldLength),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Total Field Length'),TIP('Total Field Length'),UPR
                           OPTION('Field Type'),AT(84,146,188,24),USE(mer:Type),BOXED,MSG('String, Decimal, Date')
                             RADIO('AlphaNumeric'),AT(88,157),USE(?MER:Type:Radio1),VALUE('STR')
                             RADIO('Decimal'),AT(150,157),USE(?MER:Type:Radio2),VALUE('DEC')
                             RADIO('Date'),AT(193,157),USE(?MER:Type:Radio3),VALUE('DAT')
                             RADIO('Time'),AT(227,157),USE(?mer:Type:Radio7),VALUE('TIM')
                           END
                           OPTION('Field Type'),AT(84,172,188,24),USE(mer:Capitals),BOXED,MSG('Upper / Caps / Lower')
                             RADIO('Upper'),AT(88,183),USE(?MER:Type:Radio1:2),VALUE('1')
                             RADIO('Caps'),AT(159,183),USE(?MER:Type:Radio2:2),VALUE('2')
                             RADIO('Lower'),AT(227,183),USE(?MER:Type:Radio3:2),VALUE('3')
                           END
                         END
                       END
                       PANEL,AT(4,202,296,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(180,206,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(240,206,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?MER:FieldName:Prompt{prop:FontColor} = -1
    ?MER:FieldName:Prompt{prop:Color} = 15066597
    If ?mer:FieldName{prop:ReadOnly} = True
        ?mer:FieldName{prop:FontColor} = 65793
        ?mer:FieldName{prop:Color} = 15066597
    Elsif ?mer:FieldName{prop:Req} = True
        ?mer:FieldName{prop:FontColor} = 65793
        ?mer:FieldName{prop:Color} = 8454143
    Else ! If ?mer:FieldName{prop:Req} = True
        ?mer:FieldName{prop:FontColor} = 65793
        ?mer:FieldName{prop:Color} = 16777215
    End ! If ?mer:FieldName{prop:Req} = True
    ?mer:FieldName{prop:Trn} = 0
    ?mer:FieldName{prop:FontStyle} = font:Bold
    ?MER:FileName:Prompt{prop:FontColor} = -1
    ?MER:FileName:Prompt{prop:Color} = 15066597
    If ?mer:FileName{prop:ReadOnly} = True
        ?mer:FileName{prop:FontColor} = 65793
        ?mer:FileName{prop:Color} = 15066597
    Elsif ?mer:FileName{prop:Req} = True
        ?mer:FileName{prop:FontColor} = 65793
        ?mer:FileName{prop:Color} = 8454143
    Else ! If ?mer:FileName{prop:Req} = True
        ?mer:FileName{prop:FontColor} = 65793
        ?mer:FileName{prop:Color} = 16777215
    End ! If ?mer:FileName{prop:Req} = True
    ?mer:FileName{prop:Trn} = 0
    ?mer:FileName{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?MER:FileName:Prompt:2{prop:FontColor} = -1
    ?MER:FileName:Prompt:2{prop:Color} = 15066597
    If ?mer:Description{prop:ReadOnly} = True
        ?mer:Description{prop:FontColor} = 65793
        ?mer:Description{prop:Color} = 15066597
    Elsif ?mer:Description{prop:Req} = True
        ?mer:Description{prop:FontColor} = 65793
        ?mer:Description{prop:Color} = 8454143
    Else ! If ?mer:Description{prop:Req} = True
        ?mer:Description{prop:FontColor} = 65793
        ?mer:Description{prop:Color} = 16777215
    End ! If ?mer:Description{prop:Req} = True
    ?mer:Description{prop:Trn} = 0
    ?mer:Description{prop:FontStyle} = font:Bold
    ?mer:TotalFieldLength:Prompt{prop:FontColor} = -1
    ?mer:TotalFieldLength:Prompt{prop:Color} = 15066597
    If ?mer:TotalFieldLength{prop:ReadOnly} = True
        ?mer:TotalFieldLength{prop:FontColor} = 65793
        ?mer:TotalFieldLength{prop:Color} = 15066597
    Elsif ?mer:TotalFieldLength{prop:Req} = True
        ?mer:TotalFieldLength{prop:FontColor} = 65793
        ?mer:TotalFieldLength{prop:Color} = 8454143
    Else ! If ?mer:TotalFieldLength{prop:Req} = True
        ?mer:TotalFieldLength{prop:FontColor} = 65793
        ?mer:TotalFieldLength{prop:Color} = 16777215
    End ! If ?mer:TotalFieldLength{prop:Req} = True
    ?mer:TotalFieldLength{prop:Trn} = 0
    ?mer:TotalFieldLength{prop:FontStyle} = font:Bold
    ?mer:Type{prop:Font,3} = -1
    ?mer:Type{prop:Color} = 15066597
    ?mer:Type{prop:Trn} = 0
    ?MER:Type:Radio1{prop:Font,3} = -1
    ?MER:Type:Radio1{prop:Color} = 15066597
    ?MER:Type:Radio1{prop:Trn} = 0
    ?MER:Type:Radio2{prop:Font,3} = -1
    ?MER:Type:Radio2{prop:Color} = 15066597
    ?MER:Type:Radio2{prop:Trn} = 0
    ?MER:Type:Radio3{prop:Font,3} = -1
    ?MER:Type:Radio3{prop:Color} = 15066597
    ?MER:Type:Radio3{prop:Trn} = 0
    ?mer:Type:Radio7{prop:Font,3} = -1
    ?mer:Type:Radio7{prop:Color} = 15066597
    ?mer:Type:Radio7{prop:Trn} = 0
    ?mer:Capitals{prop:Font,3} = -1
    ?mer:Capitals{prop:Color} = 15066597
    ?mer:Capitals{prop:Trn} = 0
    ?MER:Type:Radio1:2{prop:Font,3} = -1
    ?MER:Type:Radio1:2{prop:Color} = 15066597
    ?MER:Type:Radio1:2{prop:Trn} = 0
    ?MER:Type:Radio2:2{prop:Font,3} = -1
    ?MER:Type:Radio2:2{prop:Color} = 15066597
    ?MER:Type:Radio2:2{prop:Trn} = 0
    ?MER:Type:Radio3:2{prop:Font,3} = -1
    ?MER:Type:Radio3:2{prop:Color} = 15066597
    ?MER:Type:Radio3:2{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMERGE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMERGE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMERGE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:FieldName:Prompt;  SolaceCtrlName = '?MER:FieldName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:FieldName;  SolaceCtrlName = '?mer:FieldName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:FileName:Prompt;  SolaceCtrlName = '?MER:FileName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:FileName;  SolaceCtrlName = '?mer:FileName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:FileName:Prompt:2;  SolaceCtrlName = '?MER:FileName:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:Description;  SolaceCtrlName = '?mer:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:TotalFieldLength:Prompt;  SolaceCtrlName = '?mer:TotalFieldLength:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:TotalFieldLength;  SolaceCtrlName = '?mer:TotalFieldLength';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:Type;  SolaceCtrlName = '?mer:Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:Type:Radio1;  SolaceCtrlName = '?MER:Type:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:Type:Radio2;  SolaceCtrlName = '?MER:Type:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:Type:Radio3;  SolaceCtrlName = '?MER:Type:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:Type:Radio7;  SolaceCtrlName = '?mer:Type:Radio7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mer:Capitals;  SolaceCtrlName = '?mer:Capitals';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:Type:Radio1:2;  SolaceCtrlName = '?MER:Type:Radio1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:Type:Radio2:2;  SolaceCtrlName = '?MER:Type:Radio2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MER:Type:Radio3:2;  SolaceCtrlName = '?MER:Type:Radio3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Mail Merge Field'
  OF ChangeRecord
    ActionMessage = 'Changing A Mail Merge Field'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMERGE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMERGE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MER:FieldName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mer:Record,History::mer:Record)
  SELF.AddHistoryField(?mer:FieldName,2)
  SELF.AddHistoryField(?mer:FileName,3)
  SELF.AddHistoryField(?mer:Description,5)
  SELF.AddHistoryField(?mer:TotalFieldLength,7)
  SELF.AddHistoryField(?mer:Type,4)
  SELF.AddHistoryField(?mer:Capitals,6)
  SELF.AddUpdateFile(Access:MERGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MERGE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MERGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MERGE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMERGE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMERGE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

