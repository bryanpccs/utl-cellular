

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01002.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMANFAULT PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
tmp:DatePicture      STRING(30)
Filepath             STRING(255)
SaveMFO              USHORT
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW8::View:Browse    VIEW(MANFAULO)
                       PROJECT(mfo:Field)
                       PROJECT(mfo:Description)
                       PROJECT(mfo:Manufacturer)
                       PROJECT(mfo:Field_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mfo:Field              LIKE(mfo:Field)                !List box control field - type derived from field
mfo:Description        LIKE(mfo:Description)          !List box control field - type derived from field
mfo:Manufacturer       LIKE(mfo:Manufacturer)         !Primary key field - type derived from field
mfo:Field_Number       LIKE(mfo:Field_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::maf:Record  LIKE(maf:RECORD),STATIC
QuickWindow          WINDOW('Update Fault Codes'),AT(,,594,323),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateMANFAULT'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,280,212),USE(?General_Sheet),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?MAF:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(maf:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Field Number'),AT(8,36),USE(?MAF:Field_Number:Prompt)
                           SPIN(@n2),AT(84,36,64,10),USE(maf:Field_Number),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,UPR,RANGE(1,20),STEP(1),MSG('Fault Code Field Number')
                           PROMPT('Field Name'),AT(8,52),USE(?MAF:Field_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(maf:Field_Name),FONT('Tahoma',8,,FONT:bold),REQ,CAP
                           LIST,AT(192,84,64,10),USE(tmp:DatePicture),HIDE,LEFT(2),FONT(,8,,FONT:bold),COLOR(0BDFFFFH),DROP(20),FROM('DD/MM/YYYY|DD/MM/YY|MM/DD/YYYY|MM/DD/YY|MM/YYYY|MM/YY|YYYY/MM|YY/MM|YYYY/MM/DD|YY/MM/DD|YYYYMMDD')
                           OPTION('Field Type'),AT(84,64,104,44),USE(maf:Field_Type),BOXED
                             RADIO('Generic String'),AT(92,72),USE(?maf:Field_Type:Radio1),VALUE('STRING')
                             RADIO('Date'),AT(92,84),USE(?maf:Field_Type:Radio2),VALUE('DATE')
                             RADIO('Number Only'),AT(92,96),USE(?maf:Field_Type:Radio3),VALUE('NUMBER')
                           END
                           PROMPT('Date Picture'),AT(192,76),USE(?DatePicture),HIDE
                           CHECK('Lookup'),AT(84,112,52,12),USE(maf:Lookup),FONT('Tahoma',8,,FONT:regular),VALUE('YES','NO')
                           CHECK('Force Lookup'),AT(192,112),USE(maf:Force_Lookup),VALUE('YES','NO')
                           CHECK('Compulsory At Completion'),AT(84,128),USE(maf:Compulsory),VALUE('YES','NO')
                           CHECK('Compulsory At Booking'),AT(192,128),USE(maf:Compulsory_At_Booking),HIDE,VALUE('YES','NO')
                           CHECK('Replicate To Fault Description'),AT(84,144),USE(maf:ReplicateFault),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Replicate To Invoice Text'),AT(84,156),USE(maf:ReplicateInvoice),MSG('Replicate To Fault Description Field'),TIP('Replicate To Fault Description Field'),VALUE('YES','NO')
                           CHECK('Restrict Length'),AT(84,168),USE(maf:RestrictLength),MSG('Restrict Length'),TIP('Restrict Length'),VALUE('1','0')
                           GROUP,AT(4,180,196,32),USE(?RestrictGroup),HIDE
                             PROMPT('Length From'),AT(8,184),USE(?MAF:LengthFrom:Prompt),TRN
                             ENTRY(@s8),AT(84,184,64,10),USE(maf:LengthFrom),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Length From'),TIP('Length From'),REQ,UPR
                             PROMPT('Length To'),AT(8,200),USE(?MAF:LengthTo:Prompt),TRN
                             ENTRY(@s8),AT(84,200,64,10),USE(maf:LengthTo),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Length To'),TIP('Length To'),REQ,UPR
                           END
                         END
                       END
                       SHEET,AT(288,4,304,212),USE(?Sheet2),SPREAD
                         TAB('Lookup Field'),USE(?Tab2)
                           ENTRY(@s30),AT(292,20,127,10),USE(mfo:Field),FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(292,36,296,156),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('86L(2)|M~Field~@s30@240L(2)|M~Description~S(240)@s60@'),FROM(Queue:Browse)
                           BUTTON('Import'),AT(292,196,56,16),USE(?ImportButton),LEFT,ICON(ICON:Open)
                           BUTTON('&Insert'),AT(412,196,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(472,196,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(532,196,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       SHEET,AT(4,220,588,72),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           CHECK('Force Format'),AT(84,224),USE(maf:ForceFormat),MSG('Force Format'),TIP('Force Format'),VALUE('1','0')
                           GROUP('Field Format Types'),AT(288,223,265,66),USE(?FieldFormatTypes),BOXED
                             PROMPT('MM - Month'),AT(292,232),USE(?Prompt8)
                             PROMPT('YY - Two Digit Year'),AT(292,244),USE(?Prompt8:2)
                             PROMPT('YYYY - Four Digit Year'),AT(292,256),USE(?Prompt8:3)
                             PROMPT('A - Alpha Only Character'),AT(292,268),USE(?Prompt8:4)
                             PROMPT('"" - Put any fixed text in Quotes, e.g. "TEST"'),AT(396,244),USE(?Prompt8:7)
                             PROMPT('0 - Numeric Only Character'),AT(292,280),USE(?Prompt8:5)
                             PROMPT('X - Alphanumeric Character'),AT(396,232),USE(?Prompt8:6)
                           END
                           PROMPT('Field Format'),AT(8,240),USE(?maf:FieldFormat:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,240,124,10),USE(maf:FieldFormat),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Field Format'),TIP('Field Format'),REQ,UPR
                         END
                       END
                       BUTTON('&OK'),AT(476,300,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(532,300,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,296,588,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
! Start Change BE021 BE(2/12/03)
IMPORT_FILE          FILE,DRIVER('BASIC'),OEM,PRE(IMPF),CREATE,THREAD
Record                   RECORD,PRE(IMP)
Code                        STRING(30)
Description                 STRING(60)
                         END
                     END
! End Change BE021 BE(2/12/03)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?General_Sheet{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?MAF:Manufacturer:Prompt{prop:FontColor} = -1
    ?MAF:Manufacturer:Prompt{prop:Color} = 15066597
    If ?maf:Manufacturer{prop:ReadOnly} = True
        ?maf:Manufacturer{prop:FontColor} = 65793
        ?maf:Manufacturer{prop:Color} = 15066597
    Elsif ?maf:Manufacturer{prop:Req} = True
        ?maf:Manufacturer{prop:FontColor} = 65793
        ?maf:Manufacturer{prop:Color} = 8454143
    Else ! If ?maf:Manufacturer{prop:Req} = True
        ?maf:Manufacturer{prop:FontColor} = 65793
        ?maf:Manufacturer{prop:Color} = 16777215
    End ! If ?maf:Manufacturer{prop:Req} = True
    ?maf:Manufacturer{prop:Trn} = 0
    ?maf:Manufacturer{prop:FontStyle} = font:Bold
    ?MAF:Field_Number:Prompt{prop:FontColor} = -1
    ?MAF:Field_Number:Prompt{prop:Color} = 15066597
    If ?maf:Field_Number{prop:ReadOnly} = True
        ?maf:Field_Number{prop:FontColor} = 65793
        ?maf:Field_Number{prop:Color} = 15066597
    Elsif ?maf:Field_Number{prop:Req} = True
        ?maf:Field_Number{prop:FontColor} = 65793
        ?maf:Field_Number{prop:Color} = 8454143
    Else ! If ?maf:Field_Number{prop:Req} = True
        ?maf:Field_Number{prop:FontColor} = 65793
        ?maf:Field_Number{prop:Color} = 16777215
    End ! If ?maf:Field_Number{prop:Req} = True
    ?maf:Field_Number{prop:Trn} = 0
    ?maf:Field_Number{prop:FontStyle} = font:Bold
    ?MAF:Field_Name:Prompt{prop:FontColor} = -1
    ?MAF:Field_Name:Prompt{prop:Color} = 15066597
    If ?maf:Field_Name{prop:ReadOnly} = True
        ?maf:Field_Name{prop:FontColor} = 65793
        ?maf:Field_Name{prop:Color} = 15066597
    Elsif ?maf:Field_Name{prop:Req} = True
        ?maf:Field_Name{prop:FontColor} = 65793
        ?maf:Field_Name{prop:Color} = 8454143
    Else ! If ?maf:Field_Name{prop:Req} = True
        ?maf:Field_Name{prop:FontColor} = 65793
        ?maf:Field_Name{prop:Color} = 16777215
    End ! If ?maf:Field_Name{prop:Req} = True
    ?maf:Field_Name{prop:Trn} = 0
    ?maf:Field_Name{prop:FontStyle} = font:Bold
    ?tmp:DatePicture{prop:FontColor} = 65793
    ?tmp:DatePicture{prop:Color}= 16777215
    ?tmp:DatePicture{prop:Color,2} = 16777215
    ?tmp:DatePicture{prop:Color,3} = 12937777
    ?maf:Field_Type{prop:Font,3} = -1
    ?maf:Field_Type{prop:Color} = 15066597
    ?maf:Field_Type{prop:Trn} = 0
    ?maf:Field_Type:Radio1{prop:Font,3} = -1
    ?maf:Field_Type:Radio1{prop:Color} = 15066597
    ?maf:Field_Type:Radio1{prop:Trn} = 0
    ?maf:Field_Type:Radio2{prop:Font,3} = -1
    ?maf:Field_Type:Radio2{prop:Color} = 15066597
    ?maf:Field_Type:Radio2{prop:Trn} = 0
    ?maf:Field_Type:Radio3{prop:Font,3} = -1
    ?maf:Field_Type:Radio3{prop:Color} = 15066597
    ?maf:Field_Type:Radio3{prop:Trn} = 0
    ?DatePicture{prop:FontColor} = -1
    ?DatePicture{prop:Color} = 15066597
    ?maf:Lookup{prop:Font,3} = -1
    ?maf:Lookup{prop:Color} = 15066597
    ?maf:Lookup{prop:Trn} = 0
    ?maf:Force_Lookup{prop:Font,3} = -1
    ?maf:Force_Lookup{prop:Color} = 15066597
    ?maf:Force_Lookup{prop:Trn} = 0
    ?maf:Compulsory{prop:Font,3} = -1
    ?maf:Compulsory{prop:Color} = 15066597
    ?maf:Compulsory{prop:Trn} = 0
    ?maf:Compulsory_At_Booking{prop:Font,3} = -1
    ?maf:Compulsory_At_Booking{prop:Color} = 15066597
    ?maf:Compulsory_At_Booking{prop:Trn} = 0
    ?maf:ReplicateFault{prop:Font,3} = -1
    ?maf:ReplicateFault{prop:Color} = 15066597
    ?maf:ReplicateFault{prop:Trn} = 0
    ?maf:ReplicateInvoice{prop:Font,3} = -1
    ?maf:ReplicateInvoice{prop:Color} = 15066597
    ?maf:ReplicateInvoice{prop:Trn} = 0
    ?maf:RestrictLength{prop:Font,3} = -1
    ?maf:RestrictLength{prop:Color} = 15066597
    ?maf:RestrictLength{prop:Trn} = 0
    ?RestrictGroup{prop:Font,3} = -1
    ?RestrictGroup{prop:Color} = 15066597
    ?RestrictGroup{prop:Trn} = 0
    ?MAF:LengthFrom:Prompt{prop:FontColor} = -1
    ?MAF:LengthFrom:Prompt{prop:Color} = 15066597
    If ?maf:LengthFrom{prop:ReadOnly} = True
        ?maf:LengthFrom{prop:FontColor} = 65793
        ?maf:LengthFrom{prop:Color} = 15066597
    Elsif ?maf:LengthFrom{prop:Req} = True
        ?maf:LengthFrom{prop:FontColor} = 65793
        ?maf:LengthFrom{prop:Color} = 8454143
    Else ! If ?maf:LengthFrom{prop:Req} = True
        ?maf:LengthFrom{prop:FontColor} = 65793
        ?maf:LengthFrom{prop:Color} = 16777215
    End ! If ?maf:LengthFrom{prop:Req} = True
    ?maf:LengthFrom{prop:Trn} = 0
    ?maf:LengthFrom{prop:FontStyle} = font:Bold
    ?MAF:LengthTo:Prompt{prop:FontColor} = -1
    ?MAF:LengthTo:Prompt{prop:Color} = 15066597
    If ?maf:LengthTo{prop:ReadOnly} = True
        ?maf:LengthTo{prop:FontColor} = 65793
        ?maf:LengthTo{prop:Color} = 15066597
    Elsif ?maf:LengthTo{prop:Req} = True
        ?maf:LengthTo{prop:FontColor} = 65793
        ?maf:LengthTo{prop:Color} = 8454143
    Else ! If ?maf:LengthTo{prop:Req} = True
        ?maf:LengthTo{prop:FontColor} = 65793
        ?maf:LengthTo{prop:Color} = 16777215
    End ! If ?maf:LengthTo{prop:Req} = True
    ?maf:LengthTo{prop:Trn} = 0
    ?maf:LengthTo{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?mfo:Field{prop:ReadOnly} = True
        ?mfo:Field{prop:FontColor} = 65793
        ?mfo:Field{prop:Color} = 15066597
    Elsif ?mfo:Field{prop:Req} = True
        ?mfo:Field{prop:FontColor} = 65793
        ?mfo:Field{prop:Color} = 8454143
    Else ! If ?mfo:Field{prop:Req} = True
        ?mfo:Field{prop:FontColor} = 65793
        ?mfo:Field{prop:Color} = 16777215
    End ! If ?mfo:Field{prop:Req} = True
    ?mfo:Field{prop:Trn} = 0
    ?mfo:Field{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?maf:ForceFormat{prop:Font,3} = -1
    ?maf:ForceFormat{prop:Color} = 15066597
    ?maf:ForceFormat{prop:Trn} = 0
    ?FieldFormatTypes{prop:Font,3} = -1
    ?FieldFormatTypes{prop:Color} = 15066597
    ?FieldFormatTypes{prop:Trn} = 0
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?Prompt8:2{prop:FontColor} = -1
    ?Prompt8:2{prop:Color} = 15066597
    ?Prompt8:3{prop:FontColor} = -1
    ?Prompt8:3{prop:Color} = 15066597
    ?Prompt8:4{prop:FontColor} = -1
    ?Prompt8:4{prop:Color} = 15066597
    ?Prompt8:7{prop:FontColor} = -1
    ?Prompt8:7{prop:Color} = 15066597
    ?Prompt8:5{prop:FontColor} = -1
    ?Prompt8:5{prop:Color} = 15066597
    ?Prompt8:6{prop:FontColor} = -1
    ?Prompt8:6{prop:Color} = 15066597
    ?maf:FieldFormat:Prompt{prop:FontColor} = -1
    ?maf:FieldFormat:Prompt{prop:Color} = 15066597
    If ?maf:FieldFormat{prop:ReadOnly} = True
        ?maf:FieldFormat{prop:FontColor} = 65793
        ?maf:FieldFormat{prop:Color} = 15066597
    Elsif ?maf:FieldFormat{prop:Req} = True
        ?maf:FieldFormat{prop:FontColor} = 65793
        ?maf:FieldFormat{prop:Color} = 8454143
    Else ! If ?maf:FieldFormat{prop:Req} = True
        ?maf:FieldFormat{prop:FontColor} = 65793
        ?maf:FieldFormat{prop:Color} = 16777215
    End ! If ?maf:FieldFormat{prop:Req} = True
    ?maf:FieldFormat{prop:Trn} = 0
    ?maf:FieldFormat{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMANFAULT',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMANFAULT',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateMANFAULT',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateMANFAULT',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMANFAULT',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateMANFAULT',1)
    SolaceViewVars('tmp:DatePicture',tmp:DatePicture,'UpdateMANFAULT',1)
    SolaceViewVars('Filepath',Filepath,'UpdateMANFAULT',1)
    SolaceViewVars('SaveMFO',SaveMFO,'UpdateMANFAULT',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?General_Sheet;  SolaceCtrlName = '?General_Sheet';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAF:Manufacturer:Prompt;  SolaceCtrlName = '?MAF:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Manufacturer;  SolaceCtrlName = '?maf:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAF:Field_Number:Prompt;  SolaceCtrlName = '?MAF:Field_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Field_Number;  SolaceCtrlName = '?maf:Field_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAF:Field_Name:Prompt;  SolaceCtrlName = '?MAF:Field_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Field_Name;  SolaceCtrlName = '?maf:Field_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DatePicture;  SolaceCtrlName = '?tmp:DatePicture';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Field_Type;  SolaceCtrlName = '?maf:Field_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Field_Type:Radio1;  SolaceCtrlName = '?maf:Field_Type:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Field_Type:Radio2;  SolaceCtrlName = '?maf:Field_Type:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Field_Type:Radio3;  SolaceCtrlName = '?maf:Field_Type:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DatePicture;  SolaceCtrlName = '?DatePicture';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Lookup;  SolaceCtrlName = '?maf:Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Force_Lookup;  SolaceCtrlName = '?maf:Force_Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Compulsory;  SolaceCtrlName = '?maf:Compulsory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:Compulsory_At_Booking;  SolaceCtrlName = '?maf:Compulsory_At_Booking';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:ReplicateFault;  SolaceCtrlName = '?maf:ReplicateFault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:ReplicateInvoice;  SolaceCtrlName = '?maf:ReplicateInvoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:RestrictLength;  SolaceCtrlName = '?maf:RestrictLength';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RestrictGroup;  SolaceCtrlName = '?RestrictGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAF:LengthFrom:Prompt;  SolaceCtrlName = '?MAF:LengthFrom:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:LengthFrom;  SolaceCtrlName = '?maf:LengthFrom';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAF:LengthTo:Prompt;  SolaceCtrlName = '?MAF:LengthTo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:LengthTo;  SolaceCtrlName = '?maf:LengthTo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mfo:Field;  SolaceCtrlName = '?mfo:Field';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ImportButton;  SolaceCtrlName = '?ImportButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:ForceFormat;  SolaceCtrlName = '?maf:ForceFormat';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FieldFormatTypes;  SolaceCtrlName = '?FieldFormatTypes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:2;  SolaceCtrlName = '?Prompt8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:3;  SolaceCtrlName = '?Prompt8:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:4;  SolaceCtrlName = '?Prompt8:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:7;  SolaceCtrlName = '?Prompt8:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:5;  SolaceCtrlName = '?Prompt8:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8:6;  SolaceCtrlName = '?Prompt8:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:FieldFormat:Prompt;  SolaceCtrlName = '?maf:FieldFormat:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?maf:FieldFormat;  SolaceCtrlName = '?maf:FieldFormat';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Job Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMANFAULT')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMANFAULT')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MAF:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(maf:Record,History::maf:Record)
  SELF.AddHistoryField(?maf:Manufacturer,1)
  SELF.AddHistoryField(?maf:Field_Number,2)
  SELF.AddHistoryField(?maf:Field_Name,3)
  SELF.AddHistoryField(?maf:Field_Type,4)
  SELF.AddHistoryField(?maf:Lookup,5)
  SELF.AddHistoryField(?maf:Force_Lookup,6)
  SELF.AddHistoryField(?maf:Compulsory,7)
  SELF.AddHistoryField(?maf:Compulsory_At_Booking,8)
  SELF.AddHistoryField(?maf:ReplicateFault,9)
  SELF.AddHistoryField(?maf:ReplicateInvoice,10)
  SELF.AddHistoryField(?maf:RestrictLength,11)
  SELF.AddHistoryField(?maf:LengthFrom,12)
  SELF.AddHistoryField(?maf:LengthTo,13)
  SELF.AddHistoryField(?maf:ForceFormat,14)
  SELF.AddHistoryField(?maf:FieldFormat,15)
  SELF.AddUpdateFile(Access:MANFAULT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANFAULO.Open
  Relate:USELEVEL.Open
  Access:MANFAULT.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFAULT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:MANFAULO,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! What is the picture?
  Case maf:DateType
      Of '@d5'
          tmp:DatePicture = 'DD/MM/YY'
      Of '@d6'
          tmp:DatePicture = 'DD/MM/YYYY'
      Of '@d1'
          tmp:DatePicture = 'MM/DD/YY'
      Of '@d2'
          tmp:DatePicture = 'MM/DD/YYYY'
      Of '@d13'
          tmp:DatePicture = 'MM/YY'
      Of '@d14'
          tmp:DatePicture = 'MM/YYYY'
      Of '@d15'
          tmp:DatePicture = 'YY/MM'
      Of '@d16'
          tmp:DatePicture = 'YYYY/MM'
      Of '@d9'
          tmp:DatePicture = 'YY/MM/DD'
      Of '@d10'
          tmp:DatePicture = 'YYYY/MM/DD'
      Of '@d11'
          tmp:DatePicture = 'YYMMDD'
      Of '@d12'
          tmp:DatePicture = 'YYYYMMDD'
  End !tmp:DatePicture
  Post(Event:Accepted,?maf:Field_Type)
  Do RecolourWindow
  ?tmp:DatePicture{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,mfo:Field_Key)
  BRW8.AddRange(mfo:Manufacturer,Relate:MANFAULO,Relate:MANFAULT)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,mfo:Field_Number,1,BRW8)
  BRW8.SetFilter('(Upper(mfo:manufacturer) = Upper(maf:manufacturer) And Upper(mfo:field_number) = Upper(maf:field_number))')
  BRW8.AddField(mfo:Field,BRW8.Q.mfo:Field)
  BRW8.AddField(mfo:Description,BRW8.Q.mfo:Description)
  BRW8.AddField(mfo:Manufacturer,BRW8.Q.mfo:Manufacturer)
  BRW8.AddField(mfo:Field_Number,BRW8.Q.mfo:Field_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?maf:Lookup{Prop:Checked} = True
    UNHIDE(?Insert)
    UNHIDE(?Change)
    UNHIDE(?Delete)
    UNHIDE(?MAF:Force_Lookup)
    UNHIDE(?ImportButton)
  END
  IF ?maf:Lookup{Prop:Checked} = False
    HIDE(?Insert)
    HIDE(?Change)
    HIDE(?Delete)
    HIDE(?MAF:Force_Lookup)
    HIDE(?ImportButton)
  END
  IF ?maf:Compulsory{Prop:Checked} = True
    UNHIDE(?MAF:Compulsory_At_Booking)
  END
  IF ?maf:Compulsory{Prop:Checked} = False
    maf:Compulsory_At_Booking = 'NO'
    HIDE(?MAF:Compulsory_At_Booking)
  END
  IF ?maf:RestrictLength{Prop:Checked} = True
    UNHIDE(?RestrictGroup)
  END
  IF ?maf:RestrictLength{Prop:Checked} = False
    HIDE(?RestrictGroup)
  END
  IF ?maf:ForceFormat{Prop:Checked} = True
    UNHIDE(?maf:FieldFormat)
    UNHIDE(?FieldFormatTypes)
    UNHIDE(?maf:FieldFormat:Prompt)
  END
  IF ?maf:ForceFormat{Prop:Checked} = False
    HIDE(?maf:FieldFormat)
    HIDE(?FieldFormatTypes)
    HIDE(?maf:FieldFormat:Prompt)
  END
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW8.AskProcedure = 0
      CLEAR(BRW8.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFAULO.Close
    Relate:USELEVEL.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMANFAULT',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request  = Insertrecord
      If maf:field_number = ''
          Case MessageEx('You have not entered a Field Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_update# = 0
      End
  End
  If do_update# = 1 And maf:lookup = 'YES'
      glo:select1  = maf:manufacturer
      glo:select2  = maf:field_number
      glo:select3  = maf:field_name
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMANFAULO
    ReturnValue = GlobalResponse
  END
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
  End !If do_update# = 1 And maf:lookup = 'YES'
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?maf:Field_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:Field_Type, Accepted)
      Case maf:Field_Type
          Of 'DATE'
              ?tmp:DatePicture{prop:Hide} = 0
              ?DatePicture{prop:Hide} = 0
              maf:ForceFormat = 0
              ?maf:ForceFormat{prop:Disable} = 1
              maf:Lookup = 'NO'
              maf:RestrictLength = 0
          Of 'STRING'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?maf:ForceFormat{prop:Disable} = 0
          Of 'NUMBER'
              ?tmp:DatePicture{prop:Hide} = 1
              ?DatePicture{prop:Hide} = 1
              ?maf:ForceFormat{prop:Disable} = 1
              maf:ForceFormat = 0
      End !maf:Field_Type
      Post(Event:Accepted,?maf:ForceFormat)
      Post(Event:Accepted,?maf:Lookup)
      Post(Event:Accepted,?maf:RestrictLength)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?maf:Field_Type, Accepted)
    OF ?maf:Lookup
      IF ?maf:Lookup{Prop:Checked} = True
        UNHIDE(?Insert)
        UNHIDE(?Change)
        UNHIDE(?Delete)
        UNHIDE(?MAF:Force_Lookup)
        UNHIDE(?ImportButton)
      END
      IF ?maf:Lookup{Prop:Checked} = False
        HIDE(?Insert)
        HIDE(?Change)
        HIDE(?Delete)
        HIDE(?MAF:Force_Lookup)
        HIDE(?ImportButton)
      END
      ThisWindow.Reset
    OF ?maf:Compulsory
      IF ?maf:Compulsory{Prop:Checked} = True
        UNHIDE(?MAF:Compulsory_At_Booking)
      END
      IF ?maf:Compulsory{Prop:Checked} = False
        maf:Compulsory_At_Booking = 'NO'
        HIDE(?MAF:Compulsory_At_Booking)
      END
      ThisWindow.Reset
    OF ?maf:RestrictLength
      IF ?maf:RestrictLength{Prop:Checked} = True
        UNHIDE(?RestrictGroup)
      END
      IF ?maf:RestrictLength{Prop:Checked} = False
        HIDE(?RestrictGroup)
      END
      ThisWindow.Reset
    OF ?ImportButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
      ! Start Change BE021 BE(2/12/03)
      IF (FileDialog('Import File', Filepath, 'CSV Files|*.CSV|Text Files|*.TXT|All Files|*.*', |
                      file:keepdir + file:longname)) THEN
          IMPORT_FILE{PROP:NAME} = CLIP(Filepath)
          OPEN(IMPORT_FILE, 20h)
          IF (ERRORCODE()) THEN
              MESSAGE('FILE OPEN ERROR: ' & ERROR())
          ELSE
              SET(IMPORT_FILE)
              SaveMFO = access:MANFAULO.SaveFile()
              SETCURSOR(Cursor:wait)
              LOOP
                  NEXT(IMPORT_FILE)
                  IF (ERRORCODE()) THEN
                      BREAK
                  END
                  access:MANFAULO.ClearKey(mfo:Field_Key)
                  mfo:Manufacturer = maf:Manufacturer
                  mfo:Field_Number = maf:Field_Number
                  mfo:Field = IMP:Code
                  IF (access:MANFAULO.TryFetch(mfo:Field_Key) = Level:Benign) THEN
                          mfo:Description = IMP:Description
                          access:MANFAULO.Update()
                  ELSE
                      IF (access:MANFAULO.Primerecord() = Level:benign) THEN
                          mfo:Manufacturer = maf:Manufacturer
                          mfo:Field_Number = maf:Field_Number
                          mfo:Field = UPPER(IMP:Code)
                          mfo:Description = UPPER(IMP:Description)
                          access:MANFAULO.Insert()
                      END
                  END
              END
              CLOSE(IMPORT_FILE)
              access:MANFAULO.restorefile(SaveMFO)
              BRW8.ResetSort(1)
              SETCURSOR()
          END
      END
      ! End Change BE021 BE(2/12/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ImportButton, Accepted)
    OF ?maf:ForceFormat
      IF ?maf:ForceFormat{Prop:Checked} = True
        UNHIDE(?maf:FieldFormat)
        UNHIDE(?FieldFormatTypes)
        UNHIDE(?maf:FieldFormat:Prompt)
      END
      IF ?maf:ForceFormat{Prop:Checked} = False
        HIDE(?maf:FieldFormat)
        HIDE(?FieldFormatTypes)
        HIDE(?maf:FieldFormat:Prompt)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case tmp:DatePicture
      Of 'DD/MM/YY'
          maf:DateType = '@d5'
      Of 'DD/MM/YYYY'
          maf:DateType = '@d6'
      Of 'MM/DD/YY'
          maf:DateType = '@d1'
      Of 'MM/DD/YYYY'
          maf:DateType = '@d2'
      Of 'MM/YY'
          maf:DateType = '@d13'
      Of 'MM/YYYY'
          maf:DateType = '@d14'
      Of 'YY/MM'
          maf:DateType = '@d15'
      Of 'YYYY/MM'
          maf:DateType = '@d16'
      Of 'YY/MM/DD'
          maf:DateType = '@d9'
      Of 'YYYY/MM/DD'
          maf:DateType = '@d10'
      Of 'YYMMDD'
          maf:DateType = '@d11'
      Of 'YYYYMMDD'
          maf:DateType = '@d12'
  End !tmp:DatePicture
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMANFAULT')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(event:accepted,?maf:compulsory)
      !thismakeover.setwindow(win:form)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

