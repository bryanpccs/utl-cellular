

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01016.INC'),ONCE        !Local module procedure declarations
                     END


ViewLetter PROCEDURE                                  !Generated from procedure template - Window

tmp:saved            STRING(20)
tmp:status           BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('View Letter'),AT(,,530,315),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,RESIZE
                       MENUBAR
                         ITEM('Page Setup'),USE(?PageSetup)
                       END
                       TEXT,AT(0,1,528,284),USE(?RTFTextBox),VSCROLL,READONLY
                       BUTTON('&Print'),AT(8,292,56,16),USE(?Button2),LEFT,ICON(ICON:Print1)
                       BUTTON('Cancel'),AT(468,293,56,16),USE(?Button3),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       BUTTON('&Print'),AT(68,293,56,16),USE(?AutoPrint),LEFT,ICON(ICON:Print1)
                       PANEL,AT(4,288,524,25),USE(?Panel1),FILL(COLOR:Silver)
                     END
oRTF_RTFTextBox         &cwRTF,THREAD

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    If ?RTFTextBox{prop:ReadOnly} = True
        ?RTFTextBox{prop:FontColor} = 65793
        ?RTFTextBox{prop:Color} = 15066597
    Elsif ?RTFTextBox{prop:Req} = True
        ?RTFTextBox{prop:FontColor} = 65793
        ?RTFTextBox{prop:Color} = 8454143
    Else ! If ?RTFTextBox{prop:Req} = True
        ?RTFTextBox{prop:FontColor} = 65793
        ?RTFTextBox{prop:Color} = 16777215
    End ! If ?RTFTextBox{prop:Req} = True
    ?RTFTextBox{prop:Trn} = 0
    ?RTFTextBox{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ViewLetter',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:saved',tmp:saved,'ViewLetter',1)
    SolaceViewVars('tmp:status',tmp:status,'ViewLetter',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?PageSetup;  SolaceCtrlName = '?PageSetup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RTFTextBox;  SolaceCtrlName = '?RTFTextBox';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AutoPrint;  SolaceCtrlName = '?AutoPrint';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ViewLetter')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ViewLetter')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RTFTextBox
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  Relate:MERGE.Open
  Relate:MERGELET.Open
  Access:MERGETXT.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  access:jobs.clearkey(job:Ref_Number_Key)
  job:Ref_Number  = glo:select1
  access:jobs.fetch(job:Ref_Number_Key)
  access:mergelet.clearkey(MRG:LetterNameKey)
  MRG:LetterName  = glo:select2
  access:mergelet.fetch(MRG:LetterNameKey)
  access:mergetxt.clearkey(mrt:refnumberkey)
  mrt:refnumber   = mrg:refnumber
  access:mergetxt.fetch(mrt:refnumberkey)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  oRTF_RTFTextBox &= NEW cwRTF
  oRTF_RTFTextBox.Init( window, ?RTFTextBox, 0, 0, 0 )
  IF ERRORCODE() = 95
    RETURN Level:Fatal
  END
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:MERGE.Close
    Relate:MERGELET.Close
  END
  DISPOSE( oRTF_RTFTextBox )
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ViewLetter',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PageSetup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PageSetup, Accepted)
      oRTF_RTFTextBox.PageSetup()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PageSetup, Accepted)
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      oRTF_RTFTextBox.SaveField( mrt:Letter )
      tmp:status = oRTF_RTFTextBox._Print( 1 )
      If tmp:status <> 0
          Post(event:closewindow)
      End!If tmp:status <> 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    OF ?AutoPrint
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoPrint, Accepted)
      oRTF_RTFTextBox.SaveField( mrt:Letter )
      tmp:status = oRTF_RTFTextBox._Print( 0 )
      Post(event:closewindow)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AutoPrint, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ViewLetter')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?RTFTextBox
    CASE EVENT()
    ELSE
      IF EVENT() = EVENT:RTFReady
        oRTF_RTFTextBox.LimitTextSize( SIZE( mrt:Letter ) )
        oRTF_RTFTextBox.LoadField( mrt:Letter )
      END
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:CloseWindow
      oRTF_RTFTextBox.Kill(FALSE)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      oRTF_RTFTextBox.LoadField( mrt:Letter )
      If glo:select1 <> 'TEST'
          oRTF_RTFTextBox.Replace( '[*TODAY*]', Format(Today(),@d6) )
          oRTF_RTFTextBox.Replace( '[*TIME*]', Format(Clock(),@t1) )
          access:users.clearkey(use:Password_key)
          use:password    = glo:password
          access:users.tryfetch(use:Password_key)
          oRTF_RTFTextBox.Replace( '[*USER*]', Capitalize(Clip(use:forename)) & ' ' & Capitalize(Clip(use:surname)) )
      
          access:merge.clearkey(mer:RefNumberKey)
          Set(mer:RefNumberKey,mer:Refnumberkey)
          Loop
              If access:merge.next()
                  Break
              End!If access:merge.next()
              Case mer:type
                  Of 'STR'
                      Case mer:capitals
                          Of 1
                              oRTF_RTFTextBox.Replace( '[' & Clip(mer:fieldname) & ']', Upper(EVALUATE(mer:filename)) )
                          Of 2
                              oRTF_RTFTextBox.Replace( '[' & Clip(mer:fieldname) & ']', Capitalize(EVALUATE(mer:filename)) )
                          Of 3
                              oRTF_RTFTextBox.Replace( '[' & Clip(mer:fieldname) & ']', Lower(EVALUATE(mer:filename)) )
                          Else
                              oRTF_RTFTextBox.Replace( '[' & Clip(mer:fieldname) & ']', Upper(EVALUATE(mer:filename)) )
                      End!Case mer:capitals
                  Of 'DAT'
                      oRTF_RTFTextBox.Replace( '[' & Clip(mer:fieldname) & ']', Format(EVALUATE(mer:filename),@d6) )
                  Of 'DEC'
                      oRTF_RTFTextBox.Replace( '[' & Clip(mer:fieldname) & ']', Format(EVALUATE(mer:filename),@n14.2) )
              End!Case mer:type
      
          End!Loop
      End!If glo:select1 = 'TEST'
      oRTF_RTFTextBox.SaveField( mrt:Letter )
      !Auto print.. hopefully
      If glo:Preview = False
          Post(Event:Accepted,?AutoPrint)
      End !glo:Preview = Flase
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

