

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBH01003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SBH01006.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateMANUFACT PROCEDURE                              !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
AccessoryExport      BYTE(false)
AccessoryExportPath  STRING(255)
EDIDirPath           STRING(255)
tmp:ModelToDelete    STRING(30)
save_stm_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
save_sta_id          USHORT,AUTO
save_suc_id          USHORT,AUTO
save_trc_id          USHORT,AUTO
pos                  STRING(255)
tmp:manufacturer     STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
Charge_Type_Temp     STRING(30)
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
EDIFileTypeQueue     QUEUE,PRE()
EDIType              STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?man:Trade_Account
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?man:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW19::View:Browse   VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                       PROJECT(mod:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
mod:Manufacturer       LIKE(mod:Manufacturer)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW21::View:Browse   VIEW(REPAIRTY)
                       PROJECT(rep:Repair_Type)
                       PROJECT(rep:Manufacturer)
                       PROJECT(rep:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
rep:Repair_Type        LIKE(rep:Repair_Type)          !List box control field - type derived from field
rep:Manufacturer       LIKE(rep:Manufacturer)         !Primary key field - type derived from field
rep:Model_Number       LIKE(rep:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB13::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
FDCB11::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::man:Record  LIKE(man:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
QuickWindow          WINDOW('Update the MANUFACT File'),AT(,,667,370),FONT('Tahoma',8,,),CENTER,IMM,ICON('Pc.ico'),HLP('UpdateMANUFACT'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,460,334),USE(?Sheet3),SPREAD
                         TAB('General'),USE(?Tab3)
                           PROMPT('Manufacturer'),AT(8,20),USE(?MAN:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(80,20,124,10),USE(man:Manufacturer),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Account Number'),AT(8,36),USE(?man:AccountNumber:Prompt),TRN
                           ENTRY(@s15),AT(80,36,64,10),USE(man:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('EDI Type'),AT(244,46),USE(?Prompt23),TRN
                           LIST,AT(320,46,124,10),USE(man:EDIFileType),VSCROLL,LEFT(2),FONT(,,,FONT:bold),COLOR(080FFFFH),FORMAT('120L(2)|M@s30@'),DROP(25),FROM(EDIFileTypeQueue),MSG('EDI File Type')
                           CHECK('D.O.P. Compulsory for Warranty Jobs'),AT(320,32),USE(man:DOPCompulsory),MSG('D.O.P. Compulsory for Warranty'),TIP('D.O.P. Compulsory for Warranty'),VALUE('1','0')
                           PROMPT('Postcode'),AT(8,52),USE(?MAN:Postcode:Prompt),TRN
                           ENTRY(@s15),AT(80,52,64,10),USE(man:Postcode),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Clear'),AT(148,52,44,10),USE(?Clear_Address),SKIP,LEFT,ICON(ICON:Cut)
                           PROMPT('Address'),AT(8,64),USE(?man:AddressLine1:Prompt),TRN
                           ENTRY(@s30),AT(80,64,124,10),USE(man:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(80,76,124,10),USE(man:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK('Accessory Export    '),AT(244,90),USE(AccessoryExport),TRN,HIDE,LEFT,VALUE('1','0')
                           PROMPT('Accessory Export Path'),AT(244,102,67,17),USE(?Prompt22),TRN,HIDE
                           ENTRY(@s30),AT(80,88,124,10),USE(man:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Contact Numbers'),AT(8,108,60,13),USE(?man:TelephoneNumber:Prompt),TRN
                           ENTRY(@s15),AT(80,108,60,10),USE(man:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s15),AT(144,108,60,10),USE(man:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Contact Name 1'),AT(8,140),USE(?man:ContactName1:Prompt),TRN
                           ENTRY(@s60),AT(80,140,124,10),USE(man:Contact_Name1),FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Telephone Number'),AT(80,100),USE(?String2),TRN,FONT(,7,,)
                           PROMPT('Contact Name 2'),AT(8,152),USE(?man:ContactName2:Prompt),TRN
                           ENTRY(@s60),AT(80,152,124,10),USE(man:Contact_Name2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Warranty Claims Processed By'),AT(244,176,72,20),USE(?man:nokiatype:prompt),TRN,HIDE
                           LIST,AT(320,178,124,10),USE(man:NokiaType),HIDE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('80L(2)|M@s20@'),DROP(10),FROM('COMMUNICAID|NSH')
                           STRING('Fax Number'),AT(144,100),USE(?String3),TRN,FONT(,7,,)
                           ENTRY(@s255),AT(320,102,124,10),USE(AccessoryExportPath),HIDE,FONT(,,,FONT:bold)
                           BUTTON,AT(448,102,10,10),USE(?AccessoryExportPathLookup),HIDE,LEFT,ICON('list3.ico')
                           CHECK('Remove Accessory Claims Costs'),AT(320,148),USE(man:RemAccCosts),MSG('Remove Warranty Claim Costs'),TIP('Remove costs from Accessory Claims Report'),VALUE('1','0')
                           CHECK('Use M.S.N.'),AT(80,168),USE(man:Use_MSN),VALUE('YES','NO')
                           CHECK('Use Product Code'),AT(136,168),USE(man:UseProductCode),MSG('Use Product Code'),TIP('Use Product Code'),VALUE('1','0')
                           PROMPT('Use EDI Version'),AT(244,194),USE(?EDIVersionPrompt),TRN,HIDE
                           SPIN(@n3),AT(320,194,40,10),USE(man:SiemensNewEDI),HIDE,RIGHT,RANGE(0,255),STEP(1),MSG('Use New EDI Format')
                           PROMPT('Head Office'),AT(8,188),USE(?man:HeadOfficeTel:Prompt),TRN
                           STRING('Telephone Number'),AT(80,180),USE(?String2:2),TRN,FONT(,7,,)
                           ENTRY(@s15),AT(80,188,60,10),USE(man:Head_Office_Telephone),FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Fax Number'),AT(144,180),USE(?String3:2),TRN,FONT(,7,,)
                           CHECK('Disallow N/A for MSN'),AT(320,206),USE(man:DisallowNA),TRN,MSG('Disallow N/A for MSN')
                           ENTRY(@s15),AT(144,188,60,10),USE(man:Head_Office_Fax),FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Telephone Number'),AT(80,200),USE(?man:tech_support:prompt),TRN,FONT(,7,,)
                           STRING('Fax Number'),AT(144,200),USE(?man:FaxNumber:prompt),TRN,FONT(,7,,)
                           PROMPT('Technical Support'),AT(8,208),USE(?man:TechsuppTel:Prompt),TRN
                           ENTRY(@s15),AT(80,208,60,10),USE(man:Technical_Support_Telephone),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s15),AT(144,208,60,10),USE(man:Technical_Support_Fax),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Tech. Support Hours'),AT(8,224,72,16),USE(?man:TechSuppHours:Prompt),TRN
                           ENTRY(@s30),AT(80,224,124,10),USE(man:Technical_Support_Hours),FONT('Tahoma',8,,FONT:bold),UPR
                           GROUP('QA Defaults'),AT(244,274,216,46),USE(?QADefaults),BOXED
                             CHECK('Use QA'),AT(248,284),USE(man:UseQA),MSG('Use QA'),TIP('Use QA'),VALUE('1','0')
                             CHECK('Use Electronic QA'),AT(248,296),USE(man:UseElectronicQA),MSG('Use Electronic QA'),TIP('Use Electronic QA'),VALUE('1','0')
                             CHECK('QA Loan/Exchange Units'),AT(248,308),USE(man:QALoanExchange),MSG('QA Loan/Exchange Units'),TIP('QA Loan/Exchange Units'),VALUE('1','0')
                             CHECK('Completion At QA Pass'),AT(356,284),USE(man:QAAtCompletion),MSG('QA At Completion'),TIP('QA At Completion'),VALUE('1','0')
                             CHECK('Validate Parts At QA'),AT(356,296),USE(man:QAParts),MSG('Validate Parts At QA'),TIP('Validate Parts At QA'),VALUE('1','0')
                             CHECK('Validate Network At QA'),AT(356,308),USE(man:QANetwork),MSG('Validate Network At QA'),TIP('Validate Network At QA'),VALUE('1','0')
                           END
                           PROMPT('Notes'),AT(8,240),USE(?MAN:Notes:Prompt),TRN
                           TEXT,AT(80,240,124,34),USE(man:Notes),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK('EDI Warranty Adjustments'),AT(80,280),USE(man:IncludeAdjustment),MSG('Include Warranty Adjustments'),TIP('Include Warranty Adjustments'),VALUE('YES','NO')
                           STRING('MSN Format Mask'),AT(244,322),USE(?String7)
                           ENTRY(@s30),AT(320,322,100,10),USE(man:MSNFieldMask),UPR
                           CHECK('Vodafone Retail Store Specific'),AT(320,218),USE(man:IsVodafoneSpecific),RIGHT,VALUE('1','0')
                           CHECK('Vodafone CBU Centre Specific'),AT(320,230),USE(man:IsCCentreSpecific),RIGHT,VALUE('1','0'),MSG('Vodafone Contact Centre Specific')
                           CHECK('Vodafone ISP Account Specific'),AT(320,242),USE(man:VodafoneISPAccount),MSG('Vodafone ISP Account Specific'),TIP('Vodafone ISP Account Specific'),VALUE('1','0')
                           CHECK('Vodafone EBU Account Specific'),AT(320,254),USE(man:VodafoneEBUAccount),MSG('Vodafone EBU Account Specific'),TIP('Vodafone EBU Account Specific'),VALUE('1','0')
                           CHECK('Vodafone Known Issue Display'),AT(320,266),USE(man:KnownIssueDisplay),MSG('Vodafone Known Issue Display'),TIP('Vodafone Known Issue Display'),VALUE('1','0')
                           CHECK('Assign Part Number To Warranty Adjustment'),AT(80,292),USE(man:AdjustPart),MSG('Force Part Number'),TIP('Force Part Number'),VALUE('1','0')
                           BUTTON('Key'),AT(424,322,36,12),USE(?Button16),LEFT,ICON('key.ico')
                           CHECK('Force Warranty Adjustment If No Parts Used'),AT(80,304),USE(man:ForceParts),MSG('Force Warranty Adjustment If No Parts Used'),TIP('Force Warranty Adjustment If No Parts Used'),VALUE('1','0')
                           PROMPT('Warranty Period From DOP'),AT(244,20,72,16),USE(?man:WarrantyPeriod:Prompt),TRN
                           PROMPT('Months'),AT(388,20),USE(?man:WarrantyPeriod:Prompt:2),TRN
                           ENTRY(@s8),AT(320,20,64,10),USE(man:Warranty_Period),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('EDI Account Number'),AT(244,62),USE(?man:EDIAccountNumber:Prompt),TRN
                           ENTRY(@s30),AT(320,62,124,10),USE(man:EDI_Account_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('EDI Path'),AT(244,76),USE(?man:EDIPath:Prompt),TRN
                           ENTRY(@s255),AT(320,76,124,10),USE(man:EDI_Path),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(448,76,10,10),USE(?EDI_Path_lookup),ICON('list3.ico')
                           PROMPT('Warranty Trade Acc'),AT(244,124),USE(?Prompt14),TRN
                           COMBO(@s30),AT(320,120,124,10),USE(man:Trade_Account),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('64L(2)|M@s15@120L(2)@s30@'),DROP(10,200),FROM(Queue:FileDropCombo)
                           BUTTON,AT(448,120,10,10),USE(?Lookup_Trade_Account),SKIP,ICON('list3.ico')
                           PROMPT('Supplier'),AT(244,138),USE(?man:supplier:prompt)
                           COMBO(@s30),AT(320,134,124,10),USE(man:Supplier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           BUTTON,AT(448,134,10,10),USE(?Lookup_Supplier),SKIP,ICON('list3.ico')
                           PROMPT('EDI Claim Number'),AT(244,162),USE(?MAN:SamsungCount:Prompt),HIDE
                           ENTRY(@n-14),AT(320,162,64,10),USE(man:SamsungCount),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Count Of Samsung Claims'),TIP('Count Of Samsung Claims'),UPR
                           PROMPT('Email Address'),AT(8,124),USE(?man:EmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(80,123,124,10),USE(man:EmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                         END
                       END
                       SHEET,AT(468,4,196,156),USE(?Sheet1),SPREAD
                         TAB('Model Numbers'),USE(?Tab1)
                           ENTRY(@s30),AT(472,20,124,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(472,36,124,120),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)~Model Number~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(604,36,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(604,88,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(604,140,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       SHEET,AT(468,164,196,174),USE(?Sheet2),SPREAD
                         TAB('Repair Types'),USE(?Tab2)
                           ENTRY(@s30),AT(472,180,124,10),USE(rep:Repair_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('&Insert'),AT(604,196,56,16),USE(?Insert:2),LEFT,ICON('insert.gif')
                           LIST,AT(472,196,124,138),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)~Repair Type~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Change'),AT(604,262,56,16),USE(?Change:2),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(604,318,56,16),USE(?Delete:2),LEFT,ICON('delete.gif')
                         END
                       END
                       PANEL,AT(4,342,660,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('Job Fault Coding'),AT(8,346,60,16),USE(?JobFaultCoding),LEFT,ICON('FauCode.gif')
                       BUTTON('Part Fault Coding'),AT(72,346,60,16),USE(?PartFaultCoding),LEFT,ICON('FauCode.gif')
                       BUTTON('EDI Defaults'),AT(136,346,60,16),USE(?EDI_Defaults),HIDE,LEFT,ICON('menatwork.ico')
                       BUTTON('&OK'),AT(548,346,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(604,346,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW19                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW19::Sort0:Locator IncrementalLocatorClass          !Default Locator
BRW21                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW21::Sort0:Locator IncrementalLocatorClass          !Default Locator
FDCB13               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
save_mod_id   ushort,auto
save_rep_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
        Map
RemovePriceStructures       Procedure(String    func:ModelNumber,String     func:RepairType)
        End
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?MAN:Manufacturer:Prompt{prop:FontColor} = -1
    ?MAN:Manufacturer:Prompt{prop:Color} = 15066597
    If ?man:Manufacturer{prop:ReadOnly} = True
        ?man:Manufacturer{prop:FontColor} = 65793
        ?man:Manufacturer{prop:Color} = 15066597
    Elsif ?man:Manufacturer{prop:Req} = True
        ?man:Manufacturer{prop:FontColor} = 65793
        ?man:Manufacturer{prop:Color} = 8454143
    Else ! If ?man:Manufacturer{prop:Req} = True
        ?man:Manufacturer{prop:FontColor} = 65793
        ?man:Manufacturer{prop:Color} = 16777215
    End ! If ?man:Manufacturer{prop:Req} = True
    ?man:Manufacturer{prop:Trn} = 0
    ?man:Manufacturer{prop:FontStyle} = font:Bold
    ?man:AccountNumber:Prompt{prop:FontColor} = -1
    ?man:AccountNumber:Prompt{prop:Color} = 15066597
    If ?man:Account_Number{prop:ReadOnly} = True
        ?man:Account_Number{prop:FontColor} = 65793
        ?man:Account_Number{prop:Color} = 15066597
    Elsif ?man:Account_Number{prop:Req} = True
        ?man:Account_Number{prop:FontColor} = 65793
        ?man:Account_Number{prop:Color} = 8454143
    Else ! If ?man:Account_Number{prop:Req} = True
        ?man:Account_Number{prop:FontColor} = 65793
        ?man:Account_Number{prop:Color} = 16777215
    End ! If ?man:Account_Number{prop:Req} = True
    ?man:Account_Number{prop:Trn} = 0
    ?man:Account_Number{prop:FontStyle} = font:Bold
    ?Prompt23{prop:FontColor} = -1
    ?Prompt23{prop:Color} = 15066597
    ?man:EDIFileType{prop:FontColor} = 65793
    ?man:EDIFileType{prop:Color}= 16777215
    ?man:EDIFileType{prop:Color,2} = 16777215
    ?man:EDIFileType{prop:Color,3} = 12937777
    ?man:DOPCompulsory{prop:Font,3} = -1
    ?man:DOPCompulsory{prop:Color} = 15066597
    ?man:DOPCompulsory{prop:Trn} = 0
    ?MAN:Postcode:Prompt{prop:FontColor} = -1
    ?MAN:Postcode:Prompt{prop:Color} = 15066597
    If ?man:Postcode{prop:ReadOnly} = True
        ?man:Postcode{prop:FontColor} = 65793
        ?man:Postcode{prop:Color} = 15066597
    Elsif ?man:Postcode{prop:Req} = True
        ?man:Postcode{prop:FontColor} = 65793
        ?man:Postcode{prop:Color} = 8454143
    Else ! If ?man:Postcode{prop:Req} = True
        ?man:Postcode{prop:FontColor} = 65793
        ?man:Postcode{prop:Color} = 16777215
    End ! If ?man:Postcode{prop:Req} = True
    ?man:Postcode{prop:Trn} = 0
    ?man:Postcode{prop:FontStyle} = font:Bold
    ?man:AddressLine1:Prompt{prop:FontColor} = -1
    ?man:AddressLine1:Prompt{prop:Color} = 15066597
    If ?man:Address_Line1{prop:ReadOnly} = True
        ?man:Address_Line1{prop:FontColor} = 65793
        ?man:Address_Line1{prop:Color} = 15066597
    Elsif ?man:Address_Line1{prop:Req} = True
        ?man:Address_Line1{prop:FontColor} = 65793
        ?man:Address_Line1{prop:Color} = 8454143
    Else ! If ?man:Address_Line1{prop:Req} = True
        ?man:Address_Line1{prop:FontColor} = 65793
        ?man:Address_Line1{prop:Color} = 16777215
    End ! If ?man:Address_Line1{prop:Req} = True
    ?man:Address_Line1{prop:Trn} = 0
    ?man:Address_Line1{prop:FontStyle} = font:Bold
    If ?man:Address_Line2{prop:ReadOnly} = True
        ?man:Address_Line2{prop:FontColor} = 65793
        ?man:Address_Line2{prop:Color} = 15066597
    Elsif ?man:Address_Line2{prop:Req} = True
        ?man:Address_Line2{prop:FontColor} = 65793
        ?man:Address_Line2{prop:Color} = 8454143
    Else ! If ?man:Address_Line2{prop:Req} = True
        ?man:Address_Line2{prop:FontColor} = 65793
        ?man:Address_Line2{prop:Color} = 16777215
    End ! If ?man:Address_Line2{prop:Req} = True
    ?man:Address_Line2{prop:Trn} = 0
    ?man:Address_Line2{prop:FontStyle} = font:Bold
    ?AccessoryExport{prop:Font,3} = -1
    ?AccessoryExport{prop:Color} = 15066597
    ?AccessoryExport{prop:Trn} = 0
    ?Prompt22{prop:FontColor} = -1
    ?Prompt22{prop:Color} = 15066597
    If ?man:Address_Line3{prop:ReadOnly} = True
        ?man:Address_Line3{prop:FontColor} = 65793
        ?man:Address_Line3{prop:Color} = 15066597
    Elsif ?man:Address_Line3{prop:Req} = True
        ?man:Address_Line3{prop:FontColor} = 65793
        ?man:Address_Line3{prop:Color} = 8454143
    Else ! If ?man:Address_Line3{prop:Req} = True
        ?man:Address_Line3{prop:FontColor} = 65793
        ?man:Address_Line3{prop:Color} = 16777215
    End ! If ?man:Address_Line3{prop:Req} = True
    ?man:Address_Line3{prop:Trn} = 0
    ?man:Address_Line3{prop:FontStyle} = font:Bold
    ?man:TelephoneNumber:Prompt{prop:FontColor} = -1
    ?man:TelephoneNumber:Prompt{prop:Color} = 15066597
    If ?man:Telephone_Number{prop:ReadOnly} = True
        ?man:Telephone_Number{prop:FontColor} = 65793
        ?man:Telephone_Number{prop:Color} = 15066597
    Elsif ?man:Telephone_Number{prop:Req} = True
        ?man:Telephone_Number{prop:FontColor} = 65793
        ?man:Telephone_Number{prop:Color} = 8454143
    Else ! If ?man:Telephone_Number{prop:Req} = True
        ?man:Telephone_Number{prop:FontColor} = 65793
        ?man:Telephone_Number{prop:Color} = 16777215
    End ! If ?man:Telephone_Number{prop:Req} = True
    ?man:Telephone_Number{prop:Trn} = 0
    ?man:Telephone_Number{prop:FontStyle} = font:Bold
    If ?man:Fax_Number{prop:ReadOnly} = True
        ?man:Fax_Number{prop:FontColor} = 65793
        ?man:Fax_Number{prop:Color} = 15066597
    Elsif ?man:Fax_Number{prop:Req} = True
        ?man:Fax_Number{prop:FontColor} = 65793
        ?man:Fax_Number{prop:Color} = 8454143
    Else ! If ?man:Fax_Number{prop:Req} = True
        ?man:Fax_Number{prop:FontColor} = 65793
        ?man:Fax_Number{prop:Color} = 16777215
    End ! If ?man:Fax_Number{prop:Req} = True
    ?man:Fax_Number{prop:Trn} = 0
    ?man:Fax_Number{prop:FontStyle} = font:Bold
    ?man:ContactName1:Prompt{prop:FontColor} = -1
    ?man:ContactName1:Prompt{prop:Color} = 15066597
    If ?man:Contact_Name1{prop:ReadOnly} = True
        ?man:Contact_Name1{prop:FontColor} = 65793
        ?man:Contact_Name1{prop:Color} = 15066597
    Elsif ?man:Contact_Name1{prop:Req} = True
        ?man:Contact_Name1{prop:FontColor} = 65793
        ?man:Contact_Name1{prop:Color} = 8454143
    Else ! If ?man:Contact_Name1{prop:Req} = True
        ?man:Contact_Name1{prop:FontColor} = 65793
        ?man:Contact_Name1{prop:Color} = 16777215
    End ! If ?man:Contact_Name1{prop:Req} = True
    ?man:Contact_Name1{prop:Trn} = 0
    ?man:Contact_Name1{prop:FontStyle} = font:Bold
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    ?man:ContactName2:Prompt{prop:FontColor} = -1
    ?man:ContactName2:Prompt{prop:Color} = 15066597
    If ?man:Contact_Name2{prop:ReadOnly} = True
        ?man:Contact_Name2{prop:FontColor} = 65793
        ?man:Contact_Name2{prop:Color} = 15066597
    Elsif ?man:Contact_Name2{prop:Req} = True
        ?man:Contact_Name2{prop:FontColor} = 65793
        ?man:Contact_Name2{prop:Color} = 8454143
    Else ! If ?man:Contact_Name2{prop:Req} = True
        ?man:Contact_Name2{prop:FontColor} = 65793
        ?man:Contact_Name2{prop:Color} = 16777215
    End ! If ?man:Contact_Name2{prop:Req} = True
    ?man:Contact_Name2{prop:Trn} = 0
    ?man:Contact_Name2{prop:FontStyle} = font:Bold
    ?man:nokiatype:prompt{prop:FontColor} = -1
    ?man:nokiatype:prompt{prop:Color} = 15066597
    ?man:NokiaType{prop:FontColor} = 65793
    ?man:NokiaType{prop:Color}= 16777215
    ?man:NokiaType{prop:Color,2} = 16777215
    ?man:NokiaType{prop:Color,3} = 12937777
    ?String3{prop:FontColor} = -1
    ?String3{prop:Color} = 15066597
    If ?AccessoryExportPath{prop:ReadOnly} = True
        ?AccessoryExportPath{prop:FontColor} = 65793
        ?AccessoryExportPath{prop:Color} = 15066597
    Elsif ?AccessoryExportPath{prop:Req} = True
        ?AccessoryExportPath{prop:FontColor} = 65793
        ?AccessoryExportPath{prop:Color} = 8454143
    Else ! If ?AccessoryExportPath{prop:Req} = True
        ?AccessoryExportPath{prop:FontColor} = 65793
        ?AccessoryExportPath{prop:Color} = 16777215
    End ! If ?AccessoryExportPath{prop:Req} = True
    ?AccessoryExportPath{prop:Trn} = 0
    ?AccessoryExportPath{prop:FontStyle} = font:Bold
    ?man:RemAccCosts{prop:Font,3} = -1
    ?man:RemAccCosts{prop:Color} = 15066597
    ?man:RemAccCosts{prop:Trn} = 0
    ?man:Use_MSN{prop:Font,3} = -1
    ?man:Use_MSN{prop:Color} = 15066597
    ?man:Use_MSN{prop:Trn} = 0
    ?man:UseProductCode{prop:Font,3} = -1
    ?man:UseProductCode{prop:Color} = 15066597
    ?man:UseProductCode{prop:Trn} = 0
    ?EDIVersionPrompt{prop:FontColor} = -1
    ?EDIVersionPrompt{prop:Color} = 15066597
    If ?man:SiemensNewEDI{prop:ReadOnly} = True
        ?man:SiemensNewEDI{prop:FontColor} = 65793
        ?man:SiemensNewEDI{prop:Color} = 15066597
    Elsif ?man:SiemensNewEDI{prop:Req} = True
        ?man:SiemensNewEDI{prop:FontColor} = 65793
        ?man:SiemensNewEDI{prop:Color} = 8454143
    Else ! If ?man:SiemensNewEDI{prop:Req} = True
        ?man:SiemensNewEDI{prop:FontColor} = 65793
        ?man:SiemensNewEDI{prop:Color} = 16777215
    End ! If ?man:SiemensNewEDI{prop:Req} = True
    ?man:SiemensNewEDI{prop:Trn} = 0
    ?man:SiemensNewEDI{prop:FontStyle} = font:Bold
    ?man:HeadOfficeTel:Prompt{prop:FontColor} = -1
    ?man:HeadOfficeTel:Prompt{prop:Color} = 15066597
    ?String2:2{prop:FontColor} = -1
    ?String2:2{prop:Color} = 15066597
    If ?man:Head_Office_Telephone{prop:ReadOnly} = True
        ?man:Head_Office_Telephone{prop:FontColor} = 65793
        ?man:Head_Office_Telephone{prop:Color} = 15066597
    Elsif ?man:Head_Office_Telephone{prop:Req} = True
        ?man:Head_Office_Telephone{prop:FontColor} = 65793
        ?man:Head_Office_Telephone{prop:Color} = 8454143
    Else ! If ?man:Head_Office_Telephone{prop:Req} = True
        ?man:Head_Office_Telephone{prop:FontColor} = 65793
        ?man:Head_Office_Telephone{prop:Color} = 16777215
    End ! If ?man:Head_Office_Telephone{prop:Req} = True
    ?man:Head_Office_Telephone{prop:Trn} = 0
    ?man:Head_Office_Telephone{prop:FontStyle} = font:Bold
    ?String3:2{prop:FontColor} = -1
    ?String3:2{prop:Color} = 15066597
    ?man:DisallowNA{prop:Font,3} = -1
    ?man:DisallowNA{prop:Color} = 15066597
    ?man:DisallowNA{prop:Trn} = 0
    If ?man:Head_Office_Fax{prop:ReadOnly} = True
        ?man:Head_Office_Fax{prop:FontColor} = 65793
        ?man:Head_Office_Fax{prop:Color} = 15066597
    Elsif ?man:Head_Office_Fax{prop:Req} = True
        ?man:Head_Office_Fax{prop:FontColor} = 65793
        ?man:Head_Office_Fax{prop:Color} = 8454143
    Else ! If ?man:Head_Office_Fax{prop:Req} = True
        ?man:Head_Office_Fax{prop:FontColor} = 65793
        ?man:Head_Office_Fax{prop:Color} = 16777215
    End ! If ?man:Head_Office_Fax{prop:Req} = True
    ?man:Head_Office_Fax{prop:Trn} = 0
    ?man:Head_Office_Fax{prop:FontStyle} = font:Bold
    ?man:tech_support:prompt{prop:FontColor} = -1
    ?man:tech_support:prompt{prop:Color} = 15066597
    ?man:FaxNumber:prompt{prop:FontColor} = -1
    ?man:FaxNumber:prompt{prop:Color} = 15066597
    ?man:TechsuppTel:Prompt{prop:FontColor} = -1
    ?man:TechsuppTel:Prompt{prop:Color} = 15066597
    If ?man:Technical_Support_Telephone{prop:ReadOnly} = True
        ?man:Technical_Support_Telephone{prop:FontColor} = 65793
        ?man:Technical_Support_Telephone{prop:Color} = 15066597
    Elsif ?man:Technical_Support_Telephone{prop:Req} = True
        ?man:Technical_Support_Telephone{prop:FontColor} = 65793
        ?man:Technical_Support_Telephone{prop:Color} = 8454143
    Else ! If ?man:Technical_Support_Telephone{prop:Req} = True
        ?man:Technical_Support_Telephone{prop:FontColor} = 65793
        ?man:Technical_Support_Telephone{prop:Color} = 16777215
    End ! If ?man:Technical_Support_Telephone{prop:Req} = True
    ?man:Technical_Support_Telephone{prop:Trn} = 0
    ?man:Technical_Support_Telephone{prop:FontStyle} = font:Bold
    If ?man:Technical_Support_Fax{prop:ReadOnly} = True
        ?man:Technical_Support_Fax{prop:FontColor} = 65793
        ?man:Technical_Support_Fax{prop:Color} = 15066597
    Elsif ?man:Technical_Support_Fax{prop:Req} = True
        ?man:Technical_Support_Fax{prop:FontColor} = 65793
        ?man:Technical_Support_Fax{prop:Color} = 8454143
    Else ! If ?man:Technical_Support_Fax{prop:Req} = True
        ?man:Technical_Support_Fax{prop:FontColor} = 65793
        ?man:Technical_Support_Fax{prop:Color} = 16777215
    End ! If ?man:Technical_Support_Fax{prop:Req} = True
    ?man:Technical_Support_Fax{prop:Trn} = 0
    ?man:Technical_Support_Fax{prop:FontStyle} = font:Bold
    ?man:TechSuppHours:Prompt{prop:FontColor} = -1
    ?man:TechSuppHours:Prompt{prop:Color} = 15066597
    If ?man:Technical_Support_Hours{prop:ReadOnly} = True
        ?man:Technical_Support_Hours{prop:FontColor} = 65793
        ?man:Technical_Support_Hours{prop:Color} = 15066597
    Elsif ?man:Technical_Support_Hours{prop:Req} = True
        ?man:Technical_Support_Hours{prop:FontColor} = 65793
        ?man:Technical_Support_Hours{prop:Color} = 8454143
    Else ! If ?man:Technical_Support_Hours{prop:Req} = True
        ?man:Technical_Support_Hours{prop:FontColor} = 65793
        ?man:Technical_Support_Hours{prop:Color} = 16777215
    End ! If ?man:Technical_Support_Hours{prop:Req} = True
    ?man:Technical_Support_Hours{prop:Trn} = 0
    ?man:Technical_Support_Hours{prop:FontStyle} = font:Bold
    ?QADefaults{prop:Font,3} = -1
    ?QADefaults{prop:Color} = 15066597
    ?QADefaults{prop:Trn} = 0
    ?man:UseQA{prop:Font,3} = -1
    ?man:UseQA{prop:Color} = 15066597
    ?man:UseQA{prop:Trn} = 0
    ?man:UseElectronicQA{prop:Font,3} = -1
    ?man:UseElectronicQA{prop:Color} = 15066597
    ?man:UseElectronicQA{prop:Trn} = 0
    ?man:QALoanExchange{prop:Font,3} = -1
    ?man:QALoanExchange{prop:Color} = 15066597
    ?man:QALoanExchange{prop:Trn} = 0
    ?man:QAAtCompletion{prop:Font,3} = -1
    ?man:QAAtCompletion{prop:Color} = 15066597
    ?man:QAAtCompletion{prop:Trn} = 0
    ?man:QAParts{prop:Font,3} = -1
    ?man:QAParts{prop:Color} = 15066597
    ?man:QAParts{prop:Trn} = 0
    ?man:QANetwork{prop:Font,3} = -1
    ?man:QANetwork{prop:Color} = 15066597
    ?man:QANetwork{prop:Trn} = 0
    ?MAN:Notes:Prompt{prop:FontColor} = -1
    ?MAN:Notes:Prompt{prop:Color} = 15066597
    If ?man:Notes{prop:ReadOnly} = True
        ?man:Notes{prop:FontColor} = 65793
        ?man:Notes{prop:Color} = 15066597
    Elsif ?man:Notes{prop:Req} = True
        ?man:Notes{prop:FontColor} = 65793
        ?man:Notes{prop:Color} = 8454143
    Else ! If ?man:Notes{prop:Req} = True
        ?man:Notes{prop:FontColor} = 65793
        ?man:Notes{prop:Color} = 16777215
    End ! If ?man:Notes{prop:Req} = True
    ?man:Notes{prop:Trn} = 0
    ?man:Notes{prop:FontStyle} = font:Bold
    ?man:IncludeAdjustment{prop:Font,3} = -1
    ?man:IncludeAdjustment{prop:Color} = 15066597
    ?man:IncludeAdjustment{prop:Trn} = 0
    ?String7{prop:FontColor} = -1
    ?String7{prop:Color} = 15066597
    If ?man:MSNFieldMask{prop:ReadOnly} = True
        ?man:MSNFieldMask{prop:FontColor} = 65793
        ?man:MSNFieldMask{prop:Color} = 15066597
    Elsif ?man:MSNFieldMask{prop:Req} = True
        ?man:MSNFieldMask{prop:FontColor} = 65793
        ?man:MSNFieldMask{prop:Color} = 8454143
    Else ! If ?man:MSNFieldMask{prop:Req} = True
        ?man:MSNFieldMask{prop:FontColor} = 65793
        ?man:MSNFieldMask{prop:Color} = 16777215
    End ! If ?man:MSNFieldMask{prop:Req} = True
    ?man:MSNFieldMask{prop:Trn} = 0
    ?man:MSNFieldMask{prop:FontStyle} = font:Bold
    ?man:IsVodafoneSpecific{prop:Font,3} = -1
    ?man:IsVodafoneSpecific{prop:Color} = 15066597
    ?man:IsVodafoneSpecific{prop:Trn} = 0
    ?man:IsCCentreSpecific{prop:Font,3} = -1
    ?man:IsCCentreSpecific{prop:Color} = 15066597
    ?man:IsCCentreSpecific{prop:Trn} = 0
    ?man:VodafoneISPAccount{prop:Font,3} = -1
    ?man:VodafoneISPAccount{prop:Color} = 15066597
    ?man:VodafoneISPAccount{prop:Trn} = 0
    ?man:VodafoneEBUAccount{prop:Font,3} = -1
    ?man:VodafoneEBUAccount{prop:Color} = 15066597
    ?man:VodafoneEBUAccount{prop:Trn} = 0
    ?man:KnownIssueDisplay{prop:Font,3} = -1
    ?man:KnownIssueDisplay{prop:Color} = 15066597
    ?man:KnownIssueDisplay{prop:Trn} = 0
    ?man:AdjustPart{prop:Font,3} = -1
    ?man:AdjustPart{prop:Color} = 15066597
    ?man:AdjustPart{prop:Trn} = 0
    ?man:ForceParts{prop:Font,3} = -1
    ?man:ForceParts{prop:Color} = 15066597
    ?man:ForceParts{prop:Trn} = 0
    ?man:WarrantyPeriod:Prompt{prop:FontColor} = -1
    ?man:WarrantyPeriod:Prompt{prop:Color} = 15066597
    ?man:WarrantyPeriod:Prompt:2{prop:FontColor} = -1
    ?man:WarrantyPeriod:Prompt:2{prop:Color} = 15066597
    If ?man:Warranty_Period{prop:ReadOnly} = True
        ?man:Warranty_Period{prop:FontColor} = 65793
        ?man:Warranty_Period{prop:Color} = 15066597
    Elsif ?man:Warranty_Period{prop:Req} = True
        ?man:Warranty_Period{prop:FontColor} = 65793
        ?man:Warranty_Period{prop:Color} = 8454143
    Else ! If ?man:Warranty_Period{prop:Req} = True
        ?man:Warranty_Period{prop:FontColor} = 65793
        ?man:Warranty_Period{prop:Color} = 16777215
    End ! If ?man:Warranty_Period{prop:Req} = True
    ?man:Warranty_Period{prop:Trn} = 0
    ?man:Warranty_Period{prop:FontStyle} = font:Bold
    ?man:EDIAccountNumber:Prompt{prop:FontColor} = -1
    ?man:EDIAccountNumber:Prompt{prop:Color} = 15066597
    If ?man:EDI_Account_Number{prop:ReadOnly} = True
        ?man:EDI_Account_Number{prop:FontColor} = 65793
        ?man:EDI_Account_Number{prop:Color} = 15066597
    Elsif ?man:EDI_Account_Number{prop:Req} = True
        ?man:EDI_Account_Number{prop:FontColor} = 65793
        ?man:EDI_Account_Number{prop:Color} = 8454143
    Else ! If ?man:EDI_Account_Number{prop:Req} = True
        ?man:EDI_Account_Number{prop:FontColor} = 65793
        ?man:EDI_Account_Number{prop:Color} = 16777215
    End ! If ?man:EDI_Account_Number{prop:Req} = True
    ?man:EDI_Account_Number{prop:Trn} = 0
    ?man:EDI_Account_Number{prop:FontStyle} = font:Bold
    ?man:EDIPath:Prompt{prop:FontColor} = -1
    ?man:EDIPath:Prompt{prop:Color} = 15066597
    If ?man:EDI_Path{prop:ReadOnly} = True
        ?man:EDI_Path{prop:FontColor} = 65793
        ?man:EDI_Path{prop:Color} = 15066597
    Elsif ?man:EDI_Path{prop:Req} = True
        ?man:EDI_Path{prop:FontColor} = 65793
        ?man:EDI_Path{prop:Color} = 8454143
    Else ! If ?man:EDI_Path{prop:Req} = True
        ?man:EDI_Path{prop:FontColor} = 65793
        ?man:EDI_Path{prop:Color} = 16777215
    End ! If ?man:EDI_Path{prop:Req} = True
    ?man:EDI_Path{prop:Trn} = 0
    ?man:EDI_Path{prop:FontStyle} = font:Bold
    ?Prompt14{prop:FontColor} = -1
    ?Prompt14{prop:Color} = 15066597
    If ?man:Trade_Account{prop:ReadOnly} = True
        ?man:Trade_Account{prop:FontColor} = 65793
        ?man:Trade_Account{prop:Color} = 15066597
    Elsif ?man:Trade_Account{prop:Req} = True
        ?man:Trade_Account{prop:FontColor} = 65793
        ?man:Trade_Account{prop:Color} = 8454143
    Else ! If ?man:Trade_Account{prop:Req} = True
        ?man:Trade_Account{prop:FontColor} = 65793
        ?man:Trade_Account{prop:Color} = 16777215
    End ! If ?man:Trade_Account{prop:Req} = True
    ?man:Trade_Account{prop:Trn} = 0
    ?man:Trade_Account{prop:FontStyle} = font:Bold
    ?man:supplier:prompt{prop:FontColor} = -1
    ?man:supplier:prompt{prop:Color} = 15066597
    If ?man:Supplier{prop:ReadOnly} = True
        ?man:Supplier{prop:FontColor} = 65793
        ?man:Supplier{prop:Color} = 15066597
    Elsif ?man:Supplier{prop:Req} = True
        ?man:Supplier{prop:FontColor} = 65793
        ?man:Supplier{prop:Color} = 8454143
    Else ! If ?man:Supplier{prop:Req} = True
        ?man:Supplier{prop:FontColor} = 65793
        ?man:Supplier{prop:Color} = 16777215
    End ! If ?man:Supplier{prop:Req} = True
    ?man:Supplier{prop:Trn} = 0
    ?man:Supplier{prop:FontStyle} = font:Bold
    ?MAN:SamsungCount:Prompt{prop:FontColor} = -1
    ?MAN:SamsungCount:Prompt{prop:Color} = 15066597
    If ?man:SamsungCount{prop:ReadOnly} = True
        ?man:SamsungCount{prop:FontColor} = 65793
        ?man:SamsungCount{prop:Color} = 15066597
    Elsif ?man:SamsungCount{prop:Req} = True
        ?man:SamsungCount{prop:FontColor} = 65793
        ?man:SamsungCount{prop:Color} = 8454143
    Else ! If ?man:SamsungCount{prop:Req} = True
        ?man:SamsungCount{prop:FontColor} = 65793
        ?man:SamsungCount{prop:Color} = 16777215
    End ! If ?man:SamsungCount{prop:Req} = True
    ?man:SamsungCount{prop:Trn} = 0
    ?man:SamsungCount{prop:FontStyle} = font:Bold
    ?man:EmailAddress:Prompt{prop:FontColor} = -1
    ?man:EmailAddress:Prompt{prop:Color} = 15066597
    If ?man:EmailAddress{prop:ReadOnly} = True
        ?man:EmailAddress{prop:FontColor} = 65793
        ?man:EmailAddress{prop:Color} = 15066597
    Elsif ?man:EmailAddress{prop:Req} = True
        ?man:EmailAddress{prop:FontColor} = 65793
        ?man:EmailAddress{prop:Color} = 8454143
    Else ! If ?man:EmailAddress{prop:Req} = True
        ?man:EmailAddress{prop:FontColor} = 65793
        ?man:EmailAddress{prop:Color} = 16777215
    End ! If ?man:EmailAddress{prop:Req} = True
    ?man:EmailAddress{prop:Trn} = 0
    ?man:EmailAddress{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?mod:Model_Number{prop:ReadOnly} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 15066597
    Elsif ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 8454143
    Else ! If ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 16777215
    End ! If ?mod:Model_Number{prop:Req} = True
    ?mod:Model_Number{prop:Trn} = 0
    ?mod:Model_Number{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?rep:Repair_Type{prop:ReadOnly} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 15066597
    Elsif ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 8454143
    Else ! If ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 16777215
    End ! If ?rep:Repair_Type{prop:Req} = True
    ?rep:Repair_Type{prop:Trn} = 0
    ?rep:Repair_Type{prop:FontStyle} = font:Bold
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMANUFACT',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMANUFACT',1)
    SolaceViewVars('AccessoryExport',AccessoryExport,'UpdateMANUFACT',1)
    SolaceViewVars('AccessoryExportPath',AccessoryExportPath,'UpdateMANUFACT',1)
    SolaceViewVars('EDIDirPath',EDIDirPath,'UpdateMANUFACT',1)
    SolaceViewVars('tmp:ModelToDelete',tmp:ModelToDelete,'UpdateMANUFACT',1)
    SolaceViewVars('save_stm_id',save_stm_id,'UpdateMANUFACT',1)
    SolaceViewVars('save_sto_id',save_sto_id,'UpdateMANUFACT',1)
    SolaceViewVars('save_sta_id',save_sta_id,'UpdateMANUFACT',1)
    SolaceViewVars('save_suc_id',save_suc_id,'UpdateMANUFACT',1)
    SolaceViewVars('save_trc_id',save_trc_id,'UpdateMANUFACT',1)
    SolaceViewVars('pos',pos,'UpdateMANUFACT',1)
    SolaceViewVars('tmp:manufacturer',tmp:manufacturer,'UpdateMANUFACT',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateMANUFACT',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateMANUFACT',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMANUFACT',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateMANUFACT',1)
    SolaceViewVars('Charge_Type_Temp',Charge_Type_Temp,'UpdateMANUFACT',1)
    SolaceViewVars('DirRetCode',DirRetCode,'UpdateMANUFACT',1)
    SolaceViewVars('DirDialogHeader',DirDialogHeader,'UpdateMANUFACT',1)
    SolaceViewVars('DirTargetVariable',DirTargetVariable,'UpdateMANUFACT',1)
    SolaceViewVars('EDIFileTypeQueue:EDIType',EDIFileTypeQueue:EDIType,'UpdateMANUFACT',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAN:Manufacturer:Prompt;  SolaceCtrlName = '?MAN:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Manufacturer;  SolaceCtrlName = '?man:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:AccountNumber:Prompt;  SolaceCtrlName = '?man:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Account_Number;  SolaceCtrlName = '?man:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt23;  SolaceCtrlName = '?Prompt23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:EDIFileType;  SolaceCtrlName = '?man:EDIFileType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:DOPCompulsory;  SolaceCtrlName = '?man:DOPCompulsory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAN:Postcode:Prompt;  SolaceCtrlName = '?MAN:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Postcode;  SolaceCtrlName = '?man:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Address;  SolaceCtrlName = '?Clear_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:AddressLine1:Prompt;  SolaceCtrlName = '?man:AddressLine1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Address_Line1;  SolaceCtrlName = '?man:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Address_Line2;  SolaceCtrlName = '?man:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AccessoryExport;  SolaceCtrlName = '?AccessoryExport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt22;  SolaceCtrlName = '?Prompt22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Address_Line3;  SolaceCtrlName = '?man:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:TelephoneNumber:Prompt;  SolaceCtrlName = '?man:TelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Telephone_Number;  SolaceCtrlName = '?man:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Fax_Number;  SolaceCtrlName = '?man:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:ContactName1:Prompt;  SolaceCtrlName = '?man:ContactName1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Contact_Name1;  SolaceCtrlName = '?man:Contact_Name1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:ContactName2:Prompt;  SolaceCtrlName = '?man:ContactName2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Contact_Name2;  SolaceCtrlName = '?man:Contact_Name2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:nokiatype:prompt;  SolaceCtrlName = '?man:nokiatype:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:NokiaType;  SolaceCtrlName = '?man:NokiaType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3;  SolaceCtrlName = '?String3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AccessoryExportPath;  SolaceCtrlName = '?AccessoryExportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AccessoryExportPathLookup;  SolaceCtrlName = '?AccessoryExportPathLookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:RemAccCosts;  SolaceCtrlName = '?man:RemAccCosts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Use_MSN;  SolaceCtrlName = '?man:Use_MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:UseProductCode;  SolaceCtrlName = '?man:UseProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EDIVersionPrompt;  SolaceCtrlName = '?EDIVersionPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:SiemensNewEDI;  SolaceCtrlName = '?man:SiemensNewEDI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:HeadOfficeTel:Prompt;  SolaceCtrlName = '?man:HeadOfficeTel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2:2;  SolaceCtrlName = '?String2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Head_Office_Telephone;  SolaceCtrlName = '?man:Head_Office_Telephone';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3:2;  SolaceCtrlName = '?String3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:DisallowNA;  SolaceCtrlName = '?man:DisallowNA';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Head_Office_Fax;  SolaceCtrlName = '?man:Head_Office_Fax';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:tech_support:prompt;  SolaceCtrlName = '?man:tech_support:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:FaxNumber:prompt;  SolaceCtrlName = '?man:FaxNumber:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:TechsuppTel:Prompt;  SolaceCtrlName = '?man:TechsuppTel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Technical_Support_Telephone;  SolaceCtrlName = '?man:Technical_Support_Telephone';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Technical_Support_Fax;  SolaceCtrlName = '?man:Technical_Support_Fax';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:TechSuppHours:Prompt;  SolaceCtrlName = '?man:TechSuppHours:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Technical_Support_Hours;  SolaceCtrlName = '?man:Technical_Support_Hours';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QADefaults;  SolaceCtrlName = '?QADefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:UseQA;  SolaceCtrlName = '?man:UseQA';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:UseElectronicQA;  SolaceCtrlName = '?man:UseElectronicQA';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:QALoanExchange;  SolaceCtrlName = '?man:QALoanExchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:QAAtCompletion;  SolaceCtrlName = '?man:QAAtCompletion';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:QAParts;  SolaceCtrlName = '?man:QAParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:QANetwork;  SolaceCtrlName = '?man:QANetwork';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAN:Notes:Prompt;  SolaceCtrlName = '?MAN:Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Notes;  SolaceCtrlName = '?man:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:IncludeAdjustment;  SolaceCtrlName = '?man:IncludeAdjustment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String7;  SolaceCtrlName = '?String7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:MSNFieldMask;  SolaceCtrlName = '?man:MSNFieldMask';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:IsVodafoneSpecific;  SolaceCtrlName = '?man:IsVodafoneSpecific';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:IsCCentreSpecific;  SolaceCtrlName = '?man:IsCCentreSpecific';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:VodafoneISPAccount;  SolaceCtrlName = '?man:VodafoneISPAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:VodafoneEBUAccount;  SolaceCtrlName = '?man:VodafoneEBUAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:KnownIssueDisplay;  SolaceCtrlName = '?man:KnownIssueDisplay';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:AdjustPart;  SolaceCtrlName = '?man:AdjustPart';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button16;  SolaceCtrlName = '?Button16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:ForceParts;  SolaceCtrlName = '?man:ForceParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:WarrantyPeriod:Prompt;  SolaceCtrlName = '?man:WarrantyPeriod:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:WarrantyPeriod:Prompt:2;  SolaceCtrlName = '?man:WarrantyPeriod:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Warranty_Period;  SolaceCtrlName = '?man:Warranty_Period';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:EDIAccountNumber:Prompt;  SolaceCtrlName = '?man:EDIAccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:EDI_Account_Number;  SolaceCtrlName = '?man:EDI_Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:EDIPath:Prompt;  SolaceCtrlName = '?man:EDIPath:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:EDI_Path;  SolaceCtrlName = '?man:EDI_Path';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EDI_Path_lookup;  SolaceCtrlName = '?EDI_Path_lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt14;  SolaceCtrlName = '?Prompt14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Trade_Account;  SolaceCtrlName = '?man:Trade_Account';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Trade_Account;  SolaceCtrlName = '?Lookup_Trade_Account';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:supplier:prompt;  SolaceCtrlName = '?man:supplier:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:Supplier;  SolaceCtrlName = '?man:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Supplier;  SolaceCtrlName = '?Lookup_Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MAN:SamsungCount:Prompt;  SolaceCtrlName = '?MAN:SamsungCount:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:SamsungCount;  SolaceCtrlName = '?man:SamsungCount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:EmailAddress:Prompt;  SolaceCtrlName = '?man:EmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?man:EmailAddress;  SolaceCtrlName = '?man:EmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number;  SolaceCtrlName = '?mod:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rep:Repair_Type;  SolaceCtrlName = '?rep:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobFaultCoding;  SolaceCtrlName = '?JobFaultCoding';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartFaultCoding;  SolaceCtrlName = '?PartFaultCoding';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EDI_Defaults;  SolaceCtrlName = '?EDI_Defaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Manufacturer'
  OF ChangeRecord
    ActionMessage = 'Changing A Manufacturer'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMANUFACT')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMANUFACT')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MAN:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(man:Record,History::man:Record)
  SELF.AddHistoryField(?man:Manufacturer,2)
  SELF.AddHistoryField(?man:Account_Number,3)
  SELF.AddHistoryField(?man:EDIFileType,47)
  SELF.AddHistoryField(?man:DOPCompulsory,32)
  SELF.AddHistoryField(?man:Postcode,4)
  SELF.AddHistoryField(?man:Address_Line1,5)
  SELF.AddHistoryField(?man:Address_Line2,6)
  SELF.AddHistoryField(?man:Address_Line3,7)
  SELF.AddHistoryField(?man:Telephone_Number,8)
  SELF.AddHistoryField(?man:Fax_Number,9)
  SELF.AddHistoryField(?man:Contact_Name1,12)
  SELF.AddHistoryField(?man:Contact_Name2,13)
  SELF.AddHistoryField(?man:NokiaType,29)
  SELF.AddHistoryField(?man:RemAccCosts,30)
  SELF.AddHistoryField(?man:Use_MSN,11)
  SELF.AddHistoryField(?man:UseProductCode,40)
  SELF.AddHistoryField(?man:SiemensNewEDI,31)
  SELF.AddHistoryField(?man:Head_Office_Telephone,14)
  SELF.AddHistoryField(?man:DisallowNA,43)
  SELF.AddHistoryField(?man:Head_Office_Fax,15)
  SELF.AddHistoryField(?man:Technical_Support_Telephone,16)
  SELF.AddHistoryField(?man:Technical_Support_Fax,17)
  SELF.AddHistoryField(?man:Technical_Support_Hours,18)
  SELF.AddHistoryField(?man:UseQA,34)
  SELF.AddHistoryField(?man:UseElectronicQA,35)
  SELF.AddHistoryField(?man:QALoanExchange,36)
  SELF.AddHistoryField(?man:QAAtCompletion,37)
  SELF.AddHistoryField(?man:QAParts,41)
  SELF.AddHistoryField(?man:QANetwork,42)
  SELF.AddHistoryField(?man:Notes,33)
  SELF.AddHistoryField(?man:IncludeAdjustment,26)
  SELF.AddHistoryField(?man:MSNFieldMask,45)
  SELF.AddHistoryField(?man:IsVodafoneSpecific,44)
  SELF.AddHistoryField(?man:IsCCentreSpecific,46)
  SELF.AddHistoryField(?man:VodafoneISPAccount,48)
  SELF.AddHistoryField(?man:VodafoneEBUAccount,50)
  SELF.AddHistoryField(?man:KnownIssueDisplay,49)
  SELF.AddHistoryField(?man:AdjustPart,27)
  SELF.AddHistoryField(?man:ForceParts,28)
  SELF.AddHistoryField(?man:Warranty_Period,24)
  SELF.AddHistoryField(?man:EDI_Account_Number,20)
  SELF.AddHistoryField(?man:EDI_Path,21)
  SELF.AddHistoryField(?man:Trade_Account,22)
  SELF.AddHistoryField(?man:Supplier,23)
  SELF.AddHistoryField(?man:SamsungCount,25)
  SELF.AddHistoryField(?man:EmailAddress,10)
  SELF.AddUpdateFile(Access:MANUFACT)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:MANUFACT.Open
  Relate:REPTYDEF.Open
  Access:STOCK.UseFile
  Access:STOMODEL.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANUFACT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Free(EDIFileTypeQueue)
  EDIType = 'N/A'
  Add(EDIFileTypeQueue)
  EDIType = 'ALCATEL'
  Add(EDIFileTypeQueue)
  EDIType = 'BOSCH'
  Add(EDIFileTypeQueue)
  EDIType = 'ERICSSON'
  Add(EDIFileTypeQueue)
  EDIType = 'LG'
  Add(EDIFileTypeQueue)
  EDIType = 'MAXON'
  Add(EDIFileTypeQueue)
  EDIType = 'MITSUBISHI'
  Add(EDIFileTypeQueue)
  EDIType = 'MOTOROLA'
  Add(EDIFileTypeQueue)
  EDIType = 'NOKIA'
  Add(EDIFileTypeQueue)
  EDIType = 'NEC'
  Add(EDIFileTypeQueue)
  EDIType = 'PANASONIC'
  Add(EDIFileTypeQueue)
  EDIType = 'SAGEM'
  Add(EDIFileTypeQueue)
  EDIType = 'SAMSUNG'
  Add(EDIFileTypeQueue)
  EDIType = 'SHARP'
  Add(EDIFileTypeQueue)
  EDIType = 'SIEMENS'
  Add(EDIFileTypeQueue)
  EDIType = 'SIEMENS DECT'
  Add(EDIFileTypeQueue)
  EDIType = 'SENDO'
  Add(EDIFileTypeQueue)
  EDIType = 'SONY'
  Add(EDIFileTypeQueue)
  EDIType = 'TELITAL'
  Add(EDIFileTypeQueue)
  EDIType = 'VK'
  Add(EDIFileTypeQueue)
  If Instring('ACR',def:User_Name,1,1)
      EDIType = 'STANDARD FORMAT'
      Add(EDIFileTypeQueue)
  End ! If Instring('ACR',def:User_Name,1,1)
  BRW19.Init(?List,Queue:Browse.ViewPosition,BRW19::View:Browse,Queue:Browse,Relate:MODELNUM,SELF)
  BRW21.Init(?List:2,Queue:Browse:1.ViewPosition,BRW21::View:Browse,Queue:Browse:1,Relate:REPAIRTY,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If def:QAPreliminary = 'YES'
      ?man:UseElectronicQA{prop:Hide} = 1
  End !def:QAPreliminary = 'YES'
  If man:EDIFileType = ''
      man:EDIFileType = 'N/A'
  End ! If man:EDIFileType = ''
  
  Do RecolourWindow
  ?man:EDIFileType{prop:vcr} = TRUE
  ?man:NokiaType{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?man:EDIFileType{prop:vcr} = False
  ?man:NokiaType{prop:vcr} = False
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW19.Q &= Queue:Browse
  BRW19.AddSortOrder(,mod:Manufacturer_Key)
  BRW19.AddRange(mod:Manufacturer,Relate:MODELNUM,Relate:MANUFACT)
  BRW19.AddLocator(BRW19::Sort0:Locator)
  BRW19::Sort0:Locator.Init(?mod:Model_Number,mod:Model_Number,1,BRW19)
  BRW19.AddField(mod:Model_Number,BRW19.Q.mod:Model_Number)
  BRW19.AddField(mod:Manufacturer,BRW19.Q.mod:Manufacturer)
  BRW21.Q &= Queue:Browse:1
  BRW21.AddSortOrder(,rep:Model_Number_Key)
  BRW21.AddRange(rep:Model_Number,mod:Model_Number)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(?rep:Repair_Type,rep:Repair_Type,1,BRW21)
  BRW21.AddField(rep:Repair_Type,BRW21.Q.rep:Repair_Type)
  BRW21.AddField(rep:Manufacturer,BRW21.Q.rep:Manufacturer)
  BRW21.AddField(rep:Model_Number,BRW21.Q.rep:Model_Number)
  IF ?man:UseQA{Prop:Checked} = True
    ENABLE(?man:UseElectronicQA)
    ENABLE(?man:QALoanExchange)
    ENABLE(?man:QAAtCompletion)
  END
  IF ?man:UseQA{Prop:Checked} = False
    man:UseElectronicQA = 0
    man:QALoanExchange = 0
    man:QAAtCompletion = 0
    DISABLE(?man:UseElectronicQA)
    DISABLE(?man:QALoanExchange)
    DISABLE(?man:QAAtCompletion)
  END
  IF ?man:IncludeAdjustment{Prop:Checked} = True
    UNHIDE(?MAN:AdjustPart)
  END
  IF ?man:IncludeAdjustment{Prop:Checked} = False
    HIDE(?MAN:AdjustPart)
  END
  BRW19.AskProcedure = 1
  BRW21.AskProcedure = 2
  FDCB13.Init(man:Trade_Account,?man:Trade_Account,Queue:FileDropCombo.ViewPosition,FDCB13::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB13.Q &= Queue:FileDropCombo
  FDCB13.AddSortOrder(sub:Account_Number_Key)
  FDCB13.AddField(sub:Account_Number,FDCB13.Q.sub:Account_Number)
  FDCB13.AddField(sub:Company_Name,FDCB13.Q.sub:Company_Name)
  FDCB13.AddField(sub:RecordNumber,FDCB13.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB13.WindowComponent)
  FDCB13.DefaultFill = 0
  FDCB11.Init(man:Supplier,?man:Supplier,Queue:FileDropCombo:1.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo:1,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo:1
  FDCB11.AddSortOrder(sup:Company_Name_Key)
  FDCB11.AddField(sup:Company_Name,FDCB11.Q.sup:Company_Name)
  FDCB11.AddField(sup:RecordNumber,FDCB11.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW19.AskProcedure = 0
      CLEAR(BRW19.AskProcedure, 1)
    END
  END
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW21.AskProcedure = 0
      CLEAR(BRW21.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:MANUFACT.Close
    Relate:REPTYDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMANUFACT',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    man:Use_MSN = 'NO'
    man:Batch_Number = 1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

! Before Embed Point: %WindowManagerMethodDataSection) DESC(WindowManager Method Data Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
saveModelNumber LIKE(mod:Model_Number)
! After Embed Point: %WindowManagerMethodDataSection) DESC(WindowManager Method Data Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_insert# = 1
  
    !Security Check
    Case number
        Of 1 !Model Number
            Case request
                Of Insertrecord
                    check_access('MODEL NUMBERS - INSERT',x")
                    If x" = False
                        beep(beep:systemhand)  ;  yield()
                        message('You do not have access to this option.', |
                                'ServiceBase 2000', icon:hand)
                        do_insert# = 0
                    End
                Of Changerecord
                    check_access('MODEL NUMBERS - CHANGE',x")
                    If x" = False
                        beep(beep:systemhand)  ;  yield()
                        message('You do not have access to this option.', |
                                'ServiceBase 2000', icon:hand)
                        do_insert# = 0
                    End
                Of Deleterecord
                    check_access('MODEL NUMBERS - DELETE',x")
                    If x" = False
                        beep(beep:systemhand)  ;  yield()
                        message('You do not have access to this option.', |
                                'ServiceBase 2000', icon:hand)
                        do_insert# = 0
                    End
                    saveModelNumber = mod:Model_Number  ! #4491 Delete Carisma table manually as keys don't match up to do it automatically (DBH: 07/01/2015)
            End !Case request
        Of 2 !repair Type
            Case request
                Of Insertrecord
                    check_access('REPAIR TYPES - INSERT',x")
                    If x" = False
                        beep(beep:systemhand)  ;  yield()
                        message('You do not have access to this option.', |
                                'ServiceBase 2000', icon:hand)
                        do_insert# = 0
                    End
                Of Changerecord
                    check_access('REPAIR TYPES - CHANGE',x")
                    If x" = False
                        beep(beep:systemhand)  ;  yield()
                        message('You do not have access to this option.', |
                                'ServiceBase 2000', icon:hand)
                        do_insert# = 0
                    End
                Of Deleterecord
                    check_access('REPAIR TYPES - DELETE',x")
                    If x" = False
                        beep(beep:systemhand)  ;  yield()
                        message('You do not have access to this option.', |
                                'ServiceBase 2000', icon:hand)
                        do_insert# = 0
                    End
            End !Case request
  
    End !Case number
  
  
  If request = Insertrecord
      If man:manufacturer = ''
          beep(beep:systemhand)  ;  yield()
          message('You have not entered the Manufacturer''s Name.', |
                  'ServiceBase 2000', icon:hand)
          do_insert# = 0
      End
      If number = 2 And do_insert# = 1
          brw19.resetsort(1)
          If mod:Model_Number = ''
              beep(beep:systemhand)  ;  yield()
              message('You must select a Model Number before you can insert a Repair '&|
                      'Type.', |
                      'ServiceBase 2000', icon:hand)
              do_insert# = 0
          End !If brw19.q.mod:ModelNumber = ''
      End !If number = 2
  End !If request = Insertrecord
  
  If number = 2 And Request = DeleteRecord And Do_Insert# = 1
      Do_Insert# = 0
      Case MessageEx('Do you wish to delete the selected Repair Type for the Selected Model Number, or for ALL the Model Numbers attached this Manufacturer?','ServiceBase 2000',|
                     'Styles\trash.ico','|&Delete Selected|&Delete ALL|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,48,msgex:samewidths,84,26,0) 
          Of 1 ! &Delete Selected Button
              RemovePriceStructures(brw19.q.mod:Model_Number,brw21.q.rep:Repair_Type)
              Relate:REPAIRTY.Delete(0)
          Of 2 ! &Delete ALL Button
              Case MessageEx('You have selected to delete the selected repair type for ALL Model Numbers attached to this Manufactuer.'&|
                '<13,10>'&|
                '<13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,48,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      !Count Records
                      Setcursor(Cursor:Wait)
                      RecordsToProcess = 0
                      Save_mod_ID = Access:MODELNUM.SaveFile()
                      Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                      mod:Manufacturer = man:Manufacturer
                      Set(mod:Manufacturer_Key,mod:Manufacturer_Key)
                      Loop
                          If Access:MODELNUM.NEXT()
                             Break
                          End !If
                          If mod:Manufacturer <> man:Manufacturer      |
                              Then Break.  ! End If
                          RecordsToProcess += 1
                      End !Loop
                      Access:MODELNUM.RestoreFile(Save_mod_ID)
                      Setcursor()
  
                      recordspercycle         = 25
                      recordsprocessed        = 0
                      percentprogress         = 0
                      progress:thermometer    = 0
                      thiswindow.reset(1)
                      open(progresswindow)
  
                      ?progress:userstring{prop:text} = 'Deleting...'
                      ?progress:pcttext{prop:text} = '0% Completed'
  
                      tmp:ModelToDelete = brw21.q.rep:Repair_Type
                      Save_mod_ID = Access:MODELNUM.SaveFile()
                      Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                      mod:Manufacturer = man:Manufacturer
                      Set(mod:Manufacturer_Key,mod:Manufacturer_Key)
                      Loop
                          If Access:MODELNUM.NEXT()
                             Break
                          End !If
                          If mod:Manufacturer <> mod:Manufacturer      |
                              Then Break.  ! End If
  
                          Do GetNextRecord2
                          cancelcheck# += 1
                          If cancelcheck# > (RecordsToProcess/100)
                              Do cancelcheck
                              If tmp:cancel = 1
                                  Break
                              End!If tmp:cancel = 1
                              cancelcheck# = 0
                          End!If cancelcheck# > 50
  
                          RemovePriceStructures(mod:Model_Number,tmp:ModelToDelete)
  
                          Access:REPAIRTY.ClearKey(rep:Model_Number_Key)
                          rep:Model_Number = mod:Model_Number
                          rep:Repair_Type  = tmp:ModelToDelete
                          If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Found
                              Relate:REPAIRTY.Delete(0)
                          Else!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                      End !Loop
                      Access:MODELNUM.RestoreFile(Save_mod_ID)
                      tmp:ModelToDelete = ''
                      Do EndPrintRun
                      close(progresswindow)
  
                  Of 2 ! &No Button
  
              End!Case MessageEx
          Of 3 ! &Cancel Button
      End!Case MessageEx
  End !number = 2 And Request = DeleteRecord And Do_Insert# = 1
  
  If number = 2 And request = Insertrecord And do_insert# = 1
      do_insert# = 0
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      pick_default_repair_type
      if globalresponse = requestcompleted
          If Records(glo:Q_RepairType)
              Loop x# = 1 To Records(glo:Q_RepairType)
                  get(glo:Q_RepairType,x#)
                  get(repairty,0)
                  if access:repairty.primerecord() = level:benign
                      rep:manufacturer = man:manufacturer
                      rep:Model_Number = mod:Model_Number
                      rep:Repair_Type  = glo:repair_type_pointer
                      access:reptydef.clearkey(rtd:repair_type_key)
                      rtd:repair_type = glo:repair_type_pointer
                      if access:reptydef.tryfetch(rtd:repair_type_key) = Level:Benign
                          rep:chargeable  = rtd:chargeable
                          rep:warranty    = rtd:warranty
                          if access:repairty.tryinsert()
                              access:repairty.cancelautoinc()
                          end
                      Else!if access:reptydef.tryfetch(rtd:repair_type_key)
                          access:repairty.cancelautoinc()
                      End!if access:reptydef.tryfetch(rtd:repair_type_key)
                  end!if access:repairty.primerecord() = level:benign
              End!Loop x# = 1 To Records(glo:Q_RepairType)
          End!If Records(glo:Q_RepairType)
      end
      globalrequest     = saverequest#
  End
  
  If do_insert# = 1
      glo:select1 = man:manufacturer
      glo:select2 = man:Use_MSN
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateMODELNUM
      UpdateREPAIRTY
    END
    ReturnValue = GlobalResponse
  END
        IF (number = 1 AND Request = DeleteRecord AND ReturnValue = RequestCompleted)
            ! #4491 Delete Carisma table manually as keys don't match up to do it automatically (DBH: 07/01/2015)
            SETCURSOR(CURSOR:WAIT)
            Relate:CARISMA.Open()
            Access:CARISMA.ClearKey(cma:ManufactModColourKey)
            cma:Manufacturer = man:Manufacturer
            cma:ModelNo = saveModelNumber
            SET(cma:ManufactModColourKey,cma:ManufactModColourKey)
            LOOP UNTIL Access:CARISMA.Next() <> Level:Benign
                IF (cma:Manufacturer <> man:Manufacturer OR |
                    cma:ModelNo <> saveModelNumber)
                    BREAK
                END ! IF
                Access:CARISMA.DeleteRecord(0)
            END ! LOOP
            Relate:CARISMA.Close()
            SETCURSOR()
        END ! IF
    End !If do_insert# = 0
  
  
      !! ** Bryan Harrison (c)1998 **
  
      glo:G_Select1 = ''
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      !Remove Invalid Entries
      delete# = 0
      setcursor(cursor:wait)
      save_mod_id = access:modelnum.savefile()
      access:modelnum.clearkey(mod:Manufacturer_Key)
      mod:Manufacturer = man:manufacturer
      set(mod:Manufacturer_Key,mod:Manufacturer_Key)
      loop
          if access:modelnum.next()
             break
          end !if
          if mod:Manufacturer <> man:manufacturer      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
      
          failed# = 1
          save_rep_id = access:repairty.savefile()
          access:repairty.clearkey(rep:Repair_Type_Key)
          rep:manufacturer = man:manufacturer
          rep:Model_Number = mod:Model_Number
          set(rep:Repair_Type_Key,rep:Repair_Type_Key)
          loop
              if access:repairty.next()
                 break
              end !if
              if rep:manufacturer <> man:manufacturer      |
              or rep:Model_Number <> mod:Model_Number      |
                  then break.  ! end if
              failed# = 0
              Break
          end !loop
          access:repairty.restorefile(save_rep_id)
          If failed# = 1
              If delete# = 0
                  Case MessageEx('One or more of the Model Numbers attached to this Manufacturer do not have any Repair Types associated.<13,10><13,10>Do you wish to remove these EXTRA Model Numbers?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          delete# = 1
                      Of 2 ! &No Button
                  End!Case MessageEx
              End!If delete# = 0
              If delete# = 1
                  Delete(modelnum)
              End!IF delete# = 1
          End
      end !loop
      access:modelnum.restorefile(save_mod_id)
      setcursor()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?man:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          Postcode_Routine (man:postcode,man:Address_Line1,man:Address_Line2,man:Address_Line3)
          Select(?man:Address_Line1,1)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Postcode, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      BEEP(BEEP:SystemQuestion)  ;  YIELD()
      CASE MESSAGE('Are you sure you want to clear the address?', |
           'ServiceBase 2000', ICON:Question, |
            BUTTON:Yes+BUTTON:No, BUTTON:Yes, 0)
      OF BUTTON:Yes
          man:address_line1    = ''
          man:address_line2    = ''
          man:address_line3    = ''
          man:postcode        = ''
          Select(?man:postcode)
      OF BUTTON:No
      END !CASE
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?AccessoryExport
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AccessoryExport, Accepted)
      IF (AccessoryExport) THEN
          UNHIDE(?Prompt22)
          UNHIDE(?AccessoryExportPath)
          UNHIDE(?AccessoryExportPathLookup)
      ELSE
          HIDE(?Prompt22)
          HIDE(?AccessoryExportPath)
          HIDE(?AccessoryExportPathLookup)
          AccessoryExportPath = ''
          DISPLAY(?AccessoryExportPath)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AccessoryExport, Accepted)
    OF ?man:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Telephone_Number, Accepted)
      
          temp_string = Clip(left(man:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          man:Telephone_Number    = temp_string
          Display(?man:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Telephone_Number, Accepted)
    OF ?man:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Fax_Number, Accepted)
      
          temp_string = Clip(left(man:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          man:Fax_Number    = temp_string
          Display(?man:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Fax_Number, Accepted)
    OF ?AccessoryExportPathLookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AccessoryExportPathLookup, Accepted)
      ! Start Change 4591 BE(24/09/2004)
      EdiDirPath = GetDirectory()
      IF (EdiDirPath <> '') THEN
          AccessoryExportPath = UPPER(EdiDirPath)
          DISPLAY(?AccessoryExportPath)
      END
      ! Start Change 4591 BE(24/09/2004)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AccessoryExportPathLookup, Accepted)
    OF ?man:Head_Office_Telephone
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Head_Office_Telephone, Accepted)
      
          temp_string = Clip(left(man:Head_Office_Telephone))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          man:Head_Office_Telephone    = temp_string
          Display(?man:Head_Office_Telephone)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Head_Office_Telephone, Accepted)
    OF ?man:Head_Office_Fax
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Head_Office_Fax, Accepted)
      
          temp_string = Clip(left(man:Head_Office_Fax))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          man:Head_Office_Fax    = temp_string
          Display(?man:Head_Office_Fax)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Head_Office_Fax, Accepted)
    OF ?man:Technical_Support_Telephone
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Technical_Support_Telephone, Accepted)
      
          temp_string = Clip(left(man:Technical_Support_Telephone))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          man:Technical_Support_Telephone    = temp_string
          Display(?man:Technical_Support_Telephone)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Technical_Support_Telephone, Accepted)
    OF ?man:Technical_Support_Fax
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Technical_Support_Fax, Accepted)
      
          temp_string = Clip(left(man:Technical_Support_Fax))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          man:Technical_Support_Fax    = temp_string
          Display(?man:Technical_Support_Fax)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?man:Technical_Support_Fax, Accepted)
    OF ?man:UseQA
      IF ?man:UseQA{Prop:Checked} = True
        ENABLE(?man:UseElectronicQA)
        ENABLE(?man:QALoanExchange)
        ENABLE(?man:QAAtCompletion)
      END
      IF ?man:UseQA{Prop:Checked} = False
        man:UseElectronicQA = 0
        man:QALoanExchange = 0
        man:QAAtCompletion = 0
        DISABLE(?man:UseElectronicQA)
        DISABLE(?man:QALoanExchange)
        DISABLE(?man:QAAtCompletion)
      END
      ThisWindow.Reset
    OF ?man:IncludeAdjustment
      IF ?man:IncludeAdjustment{Prop:Checked} = True
        UNHIDE(?MAN:AdjustPart)
      END
      IF ?man:IncludeAdjustment{Prop:Checked} = False
        HIDE(?MAN:AdjustPart)
      END
      ThisWindow.Reset
    OF ?Button16
      ThisWindow.Update
      MSN_Mask_Key
      ThisWindow.Reset
    OF ?EDI_Path_lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Path_lookup, Accepted)
      ! Start Change 4511 BE(21/07/2004)
      !filedialog ('Choose Directory',man:EDI_Path,'All Directories|*.*', |
      !            file:save+file:keepdir + file:noerror + file:longname + file:directory)
      !man:EDI_Path = upper(man:EDI_Path)
      !display(?man:EDI_Path)
      !
      ! FILEDIALOG select directory 32 bit has a severe memory leak
      ! so call GetDirectory function and handle API calls directly
      !
      EdiDirPath = GetDirectory()
      IF (EdiDirPath <> '') THEN
          man:EDI_Path = UPPER(EdiDirPath)
          DISPLAY(?man:EDI_Path)
      END
      ! Start Change 4511 BE(21/07/2004)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Path_lookup, Accepted)
    OF ?Lookup_Trade_Account
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Sub_Accounts
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Trade_Account, Accepted)
      case globalresponse
          of requestcompleted
              man:Trade_Account = sub:Account_Number
      !        select(?+2)
          of requestcancelled
      !        man:AccountNumber = ''
              select(?-1)
      end!case globalreponse
      display(?man:Trade_Account)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Trade_Account, Accepted)
    OF ?Lookup_Supplier
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Suppliers
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Supplier, Accepted)
      case globalresponse
          of requestcompleted
              man:supplier = sup:Company_Name
              select(?+2)
          of requestcancelled
      !        man:supplier = ''
              select(?-1)
      end!case globalreponse
      display(?man:supplier)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Supplier, Accepted)
    OF ?JobFaultCoding
      ThisWindow.Update
      Browse_Manufacturer_Fault_Coding
      ThisWindow.Reset
    OF ?PartFaultCoding
      ThisWindow.Update
      Browse_Manufacturer_Parts_Fault_Coding
      ThisWindow.Reset
    OF ?EDI_Defaults
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Defaults, Accepted)
      Case man:manufacturer
          Of 'MOTOROLA'
              Edi_Defaults
          Of 'SIEMENS'
              Eps_Defaults
          Of 'ERICSSON'
              Edi_Defaults_Ericsson
      End!Case man:manufacturer
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?EDI_Defaults, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ! Close Window Check
  glo:errortext   = ''
  setcursor(cursor:wait)
  save_mod_id = access:modelnum.savefile()
  access:modelnum.clearkey(mod:Manufacturer_Key)
  mod:Manufacturer = man:manufacturer
  set(mod:Manufacturer_Key,mod:Manufacturer_Key)
  loop
      if access:modelnum.next()
         break
      end !if
      if mod:Manufacturer <> man:manufacturer      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
  
      failed# = 1
      save_rep_id = access:repairty.savefile()
      access:repairty.clearkey(rep:Repair_Type_Key)
      rep:manufacturer = man:manufacturer
      rep:Model_Number = mod:Model_Number
      set(rep:Repair_Type_Key,rep:Repair_Type_Key)
      loop
          if access:repairty.next()
             break
          end !if
          if rep:manufacturer <> man:manufacturer      |
          or rep:Model_Number <> mod:Model_Number      |
              then break.  ! end if
          failed# = 0
          Break
      end !loop
      access:repairty.restorefile(save_rep_id)
      If failed# = 1
          If glo:errortext = ''
              glo:errortext = Clip(mod:model_number)
          Else!If glo:errortext = ''
              glo:errortext = Clip(glo:errortext) & ', ' & CLip(mod:model_number)
          End!If glo:errortext = ''
      End
  end !loop
  access:modelnum.restorefile(save_mod_id)
  setcursor()
  
  If glo:errortext <> ''
      glo:errortext = 'The following Model Numbers do NOT have a Repair Type attached:<13,10,13,10>' & clip(glo:errortext)
      Error_Text
      glo:errortext = ''
      Cycle
  End!If glo:errortext <> ''
  
  If failed# = 1
      beep(beep:systemhand)  ;  yield()
      message('You must specify a repair type for each of the model numbers for '&|
              'this manufacturer.', |
              'ServiceBase 2000', icon:hand)
      Select(?man:manufacturer)
  End
  
  If man:manufacturer <> tmp:manufacturer And tmp:Manufacturer <> ''
      Case MessageEx('You have changed the name of this Manufacturer.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              setcursor(cursor:wait)
              save_sto_id = access:stock.savefile()
              access:stock.clearkey(sto:Manufacturer_Key)
              sto:manufacturer = tmp:manufacturer
              set(sto:Manufacturer_Key,sto:Manufacturer_Key)
              loop
                  if access:stock.next()
                     break
                  end !if
                  if sto:manufacturer <> tmp:manufacturer      |
                      then break.  ! end if
                  pos = Position(sto:Manufacturer_Key)
                  sto:manufacturer = man:manufacturer
                  access:stock.tryupdate()
                  Reset(sto:Manufacturer_Key,pos)
  
                  save_stm_id = access:stomodel.savefile()
                  access:stomodel.clearkey(stm:mode_number_only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  set(stm:mode_number_only_Key,stm:mode_number_only_Key)
                  loop
                      if access:stomodel.next()
                         break
                      end !if
                      if stm:Ref_Number   <> sto:Ref_Number      |
                          then break.  ! end if
                      stm:manufacturer = man:manufacturer
                      access:stomodel.tryupdate()
                  end !loop
                  access:stomodel.restorefile(save_stm_id)
              end !loop
              access:stock.restorefile(save_sto_id)
              setcursor()
          Of 2 ! &No Button
              man:manufacturer = tmp:manufacturer
              Cycle
      End!Case MessageEx
      Display()
  End!If sto:manufacturer <> tmp:manufacturer
  
  ! Start Change 4591 BE(24/09/2004)
  IF ((man:manufacturer = 'NOKIA') OR (man:manufacturer = 'MOTOROLA')) THEN
      PUTINI(CLIP(man:manufacturer), 'AccessoryExport', AccessoryExport, CLIP(PATH()) & '\SB2KDEF.INI')
      PUTINI(CLIP(man:manufacturer), 'AccessoryExportPath', AccessoryExportPath, CLIP(PATH()) & '\SB2KDEF.INI')
  END
  ! End Change 4591 BE(24/09/2004)
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMANUFACT')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?mod:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Model_Number, Selected)
      Select(?list)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Model_Number, Selected)
    OF ?rep:Repair_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rep:Repair_Type, Selected)
      Select(?list:2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rep:Repair_Type, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case man:manufacturer
          ! Start Nokia EDI Bug BE(08/05/03)
          !Of 'MOTOROLA' Orof 'ERICSSON'
          Of 'MOTOROLA' Orof 'ERICSSON' Orof 'NOKIA'
          ! End Nokia EDI Bug BE(08/05/03)
              ! Start Change BE030 BE(16/06/2004)
              !Unhide(?edi_defaults)
              ! End Change BE030 BE(16/06/2004)
      
              If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
                  ?man:SiemensNewEDI{prop:Hide} = 0
                  ! Start Change 2894 BE(09/07/03)
                  ?EDIVersionPrompt{prop:Hide} = 0
                  ! End Change 2894 BE(09/07/03)
              End !If FullAccess() = Level:Benign
              ! Start Nokia EDI Bug BE(08/05/03)
              IF (man:Manufacturer = 'NOKIA') THEN
                  Unhide(?man:nokiatype)
                  Unhide(?man:nokiatype:prompt)
              ! Start Change BE030 BE(16/06/2004)
              ELSE
                   Unhide(?edi_defaults)
              ! End Change BE030 BE(16/06/2004)
              END
              ! End Nokia EDI Bug BE(08/05/03)
          Of 'SAMSUNG'
              check_access('MANUFACT FAULT CODES - CHANGE',x")
              if x" = true
                  Unhide(?man:samsungcount)
                  Unhide(?man:samsungcount:prompt)
              end
          ! Start Nokia EDI Bug BE(08/05/03)
          !Of 'NOKIA'
          !    Unhide(?man:nokiatype)
          !    Unhide(?man:nokiatype:prompt)
          ! End Nokia EDI Bug BE(08/05/03)
          Of 'SIEMENS'
              ! Start Change 4017 BE(10/03/04)
              !Unhide(?man:SiemensNewEDI)
              !! Start Change 1673
              !Unhide(?EDIVersionPrompt)
              !! End Change 1673
              IF (FullAccess(glo:PassAccount,glo:Password) = Level:Benign) THEN
                  UNHIDE(?man:SiemensNewEDI)
                  UNHIDE(?EDIVersionPrompt)
              END
              ! End Change 4017 BE(10/03/04)
          ! Start Change 1673
          !Of 'SONY'
          Of 'SONY' OROF 'PANASONIC'
          ! End Change 1673
              If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
                  ?man:SiemensNewEDI{prop:Hide} = 0
                  ! Start Change 2894 BE(09/07/03)
                  !?man:SiemensNewEDI{prop:Text} = 'Use Sony Ericsson EDI Format'
                  ?EDIVersionPrompt{prop:Hide} = 0
                  ! End Change 2984 BE(09/07/03)
              End !If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
          ! Start Change 3808 BE(2/2/04)
          OF 'SHARP'
              If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
                  ?man:SiemensNewEDI{prop:Hide} = 0
                  ?EDIVersionPrompt{prop:Hide} = 0
              End !If FullAccess() = Level:Benign
          ! End Change 3808 BE(2/2/04)
          ! Start Change 3883 BE(16/2/04)
          OF 'SAGEM'
              IF (FullAccess(glo:PassAccount,glo:Password) = Level:Benign) THEN
                  ?man:SiemensNewEDI{prop:Hide} = 0
                  ?EDIVersionPrompt{prop:Hide} = 0
              END
          ! End Change 3883 BE(16/2/04)
          ! Start Change 4010 BE(12/3/04)
          OF 'SENDO'
              IF (FullAccess(glo:PassAccount,glo:Password) = Level:Benign) THEN
                  ?man:SiemensNewEDI{prop:Hide} = 0
                  ?EDIVersionPrompt{prop:Hide} = 0
              END
          ! End Change 4010 BE(12/3/04)
      End!Case man:manufacturer
      
      ! Start Change 4591 BE(24/09/2004)
      IF ((man:manufacturer = 'NOKIA') OR (man:manufacturer = 'MOTOROLA')) THEN
          AccessoryExport     = GETINI(CLIP(man:manufacturer), 'AccessoryExport',, CLIP(PATH()) & '\SB2KDEF.INI')
          AccessoryExportPath = GETINI(CLIP(man:manufacturer), 'AccessoryExportPath',, CLIP(PATH()) & '\SB2KDEF.INI')
          UNHIDE(?AccessoryExport)
          IF (AccessoryExport) THEN
              UNHIDE(?Prompt22)
              UNHIDE(?AccessoryExportPath)
              UNHIDE(?AccessoryExportPathLookup)
          END
      END
      ! End Change 4591 BE(24/09/2004)
      
      !Save Name
      tmp:manufacturer = man:manufacturer
      Display()
      
      If man:EDIFileType = ''
          man:EDIFileType = 'N/A'
      End ! If man:EDIFileType = ''
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW19.ResetSort(1)
      BRW21.ResetSort(1)
      FDCB11.ResetQueue(1)
      FDCB13.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
RemovePriceStructures       Procedure(String    func:ModelNumber,String     func:RepairType)
    Code

    Save_trc_ID = Access:TRACHRGE.SaveFile()
    Access:TRACHRGE.ClearKey(trc:Model_Repair_Key)
    trc:Model_Number = func:ModelNumber
    trc:Repair_Type  = func:RepairType
    Set(trc:Model_Repair_Key,trc:Model_Repair_Key)
    Loop
        If Access:TRACHRGE.NEXT()
           Break
        End !If
        If trc:Model_Number <> func:ModelNumber |
        Or trc:Repair_Type  <> func:RepairType      |
            Then Break.  ! End If
        Delete(TRACHRGE)
    End !Loop
    Access:TRACHRGE.RestoreFile(Save_trc_ID)

    Save_suc_ID = Access:SUBCHRGE.SaveFile()
    Access:SUBCHRGE.ClearKey(suc:Model_Repair_Key)
    suc:Model_Number = func:ModelNumber
    suc:Repair_Type  = func:RepairType
    Set(suc:Model_Repair_Key,suc:Model_Repair_Key)
    Loop
        If Access:SUBCHRGE.NEXT()
           Break
        End !If
        If suc:Model_Number <> func:ModelNumber      |
        Or suc:Repair_Type  <> func:RepairType      |
            Then Break.  ! End If
        Delete(SUBCHRGE)
    End !Loop
    Access:SUBCHRGE.RestoreFile(Save_suc_ID)

    Save_sta_ID = Access:STDCHRGE.SaveFile()
    Access:STDCHRGE.ClearKey(sta:Repair_Type_Key)
    sta:Model_Number = func:ModelNumber
    sta:Repair_Type  = func:RepairType
    Set(sta:Repair_Type_Key,sta:Repair_Type_Key)
    Loop
        If Access:STDCHRGE.NEXT()
           Break
        End !If
        If sta:Model_Number <> func:ModelNumber      |
        Or sta:Repair_Type  <> func:RepairType      |
            Then Break.  ! End If
        Delete(STDCHRGE)
    End !Loop
    Access:STDCHRGE.RestoreFile(Save_sta_ID)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW19.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW19.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW21.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
