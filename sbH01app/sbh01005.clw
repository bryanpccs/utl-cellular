

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01005.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMANFPALO PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::mfp:Record  LIKE(mfp:RECORD),STATIC
QuickWindow          WINDOW('Update the MANFPALO File'),AT(,,220,114),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateMANFPALO'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,80),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                         END
                       END
                       PROMPT('Manufacturer'),AT(8,20),USE(?MFP:Manufacturer:Prompt),TRN
                       ENTRY(@s30),AT(84,20,124,10),USE(mfp:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       PROMPT('Field Number'),AT(8,36),USE(?MFP:Field_Number:Prompt),TRN
                       ENTRY(@n2),AT(84,36,64,10),USE(mfp:Field_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       PROMPT('Field'),AT(8,52),USE(?MFP:Field:Prompt),TRN
                       ENTRY(@s30),AT(84,52,124,10),USE(mfp:Field),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                       PROMPT('Description'),AT(8,68),USE(?MFP:Description:Prompt)
                       ENTRY(@s60),AT(84,68,124,10),USE(mfp:Description),FONT(,,,FONT:bold),UPR
                       PANEL,AT(4,88,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,92,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,92,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?MFP:Manufacturer:Prompt{prop:FontColor} = -1
    ?MFP:Manufacturer:Prompt{prop:Color} = 15066597
    If ?mfp:Manufacturer{prop:ReadOnly} = True
        ?mfp:Manufacturer{prop:FontColor} = 65793
        ?mfp:Manufacturer{prop:Color} = 15066597
    Elsif ?mfp:Manufacturer{prop:Req} = True
        ?mfp:Manufacturer{prop:FontColor} = 65793
        ?mfp:Manufacturer{prop:Color} = 8454143
    Else ! If ?mfp:Manufacturer{prop:Req} = True
        ?mfp:Manufacturer{prop:FontColor} = 65793
        ?mfp:Manufacturer{prop:Color} = 16777215
    End ! If ?mfp:Manufacturer{prop:Req} = True
    ?mfp:Manufacturer{prop:Trn} = 0
    ?mfp:Manufacturer{prop:FontStyle} = font:Bold
    ?MFP:Field_Number:Prompt{prop:FontColor} = -1
    ?MFP:Field_Number:Prompt{prop:Color} = 15066597
    If ?mfp:Field_Number{prop:ReadOnly} = True
        ?mfp:Field_Number{prop:FontColor} = 65793
        ?mfp:Field_Number{prop:Color} = 15066597
    Elsif ?mfp:Field_Number{prop:Req} = True
        ?mfp:Field_Number{prop:FontColor} = 65793
        ?mfp:Field_Number{prop:Color} = 8454143
    Else ! If ?mfp:Field_Number{prop:Req} = True
        ?mfp:Field_Number{prop:FontColor} = 65793
        ?mfp:Field_Number{prop:Color} = 16777215
    End ! If ?mfp:Field_Number{prop:Req} = True
    ?mfp:Field_Number{prop:Trn} = 0
    ?mfp:Field_Number{prop:FontStyle} = font:Bold
    ?MFP:Field:Prompt{prop:FontColor} = -1
    ?MFP:Field:Prompt{prop:Color} = 15066597
    If ?mfp:Field{prop:ReadOnly} = True
        ?mfp:Field{prop:FontColor} = 65793
        ?mfp:Field{prop:Color} = 15066597
    Elsif ?mfp:Field{prop:Req} = True
        ?mfp:Field{prop:FontColor} = 65793
        ?mfp:Field{prop:Color} = 8454143
    Else ! If ?mfp:Field{prop:Req} = True
        ?mfp:Field{prop:FontColor} = 65793
        ?mfp:Field{prop:Color} = 16777215
    End ! If ?mfp:Field{prop:Req} = True
    ?mfp:Field{prop:Trn} = 0
    ?mfp:Field{prop:FontStyle} = font:Bold
    ?MFP:Description:Prompt{prop:FontColor} = -1
    ?MFP:Description:Prompt{prop:Color} = 15066597
    If ?mfp:Description{prop:ReadOnly} = True
        ?mfp:Description{prop:FontColor} = 65793
        ?mfp:Description{prop:Color} = 15066597
    Elsif ?mfp:Description{prop:Req} = True
        ?mfp:Description{prop:FontColor} = 65793
        ?mfp:Description{prop:Color} = 8454143
    Else ! If ?mfp:Description{prop:Req} = True
        ?mfp:Description{prop:FontColor} = 65793
        ?mfp:Description{prop:Color} = 16777215
    End ! If ?mfp:Description{prop:Req} = True
    ?mfp:Description{prop:Trn} = 0
    ?mfp:Description{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMANFPALO',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMANFPALO',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateMANFPALO',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateMANFPALO',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMANFPALO',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateMANFPALO',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MFP:Manufacturer:Prompt;  SolaceCtrlName = '?MFP:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mfp:Manufacturer;  SolaceCtrlName = '?mfp:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MFP:Field_Number:Prompt;  SolaceCtrlName = '?MFP:Field_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mfp:Field_Number;  SolaceCtrlName = '?mfp:Field_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MFP:Field:Prompt;  SolaceCtrlName = '?MFP:Field:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mfp:Field;  SolaceCtrlName = '?mfp:Field';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MFP:Description:Prompt;  SolaceCtrlName = '?MFP:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mfp:Description;  SolaceCtrlName = '?mfp:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMANFPALO')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMANFPALO')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?MFP:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mfp:Record,History::mfp:Record)
  SELF.AddHistoryField(?mfp:Manufacturer,1)
  SELF.AddHistoryField(?mfp:Field_Number,2)
  SELF.AddHistoryField(?mfp:Field,3)
  SELF.AddHistoryField(?mfp:Description,4)
  SELF.AddUpdateFile(Access:MANFPALO)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MANFPALO.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MANFPALO
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANFPALO.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMANFPALO',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    mfp:Manufacturer = glo:select1
    mfp:Field_Number = glo:select2
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMANFPALO')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

