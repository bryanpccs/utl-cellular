

   MEMBER('sbh01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBH01008.INC'),ONCE        !Local module procedure declarations
                     END


Update_Letters PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?let:Status
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Ref_Number         LIKE(sts:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(STATUS)
                       PROJECT(sts:Status)
                       PROJECT(sts:Ref_Number)
                     END
History::let:Record  LIKE(let:RECORD),STATIC
QuickWindow          WINDOW('Update The Letters File'),AT(,,439,339),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('Update_Letters'),SYSTEM,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,432,304),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Description'),AT(8,20),USE(?LET:Description:Prompt)
                           ENTRY(@s255),AT(84,20,240,10),USE(let:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter''s Description'),TIP('Letter''s Description'),UPR
                           PROMPT('Subject'),AT(8,36),USE(?LET:Subject:Prompt)
                           ENTRY(@s255),AT(84,36,240,10),USE(let:Subject),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Letter''s Subject (i.e. RE:)'),TIP('Letter''s Subject (i.e. RE:)'),UPR
                           CHECK('New Status'),AT(84,52),USE(let:UseStatus),MSG('Is a Status Change Required?'),TIP('Is a Status Change Required?'),VALUE('YES','NO')
                           GROUP,AT(144,48,140,16),USE(?Status_Group),HIDE
                             PROMPT('Status Type'),AT(148,52),USE(?Prompt6)
                             COMBO(@s30),AT(216,52,124,10),USE(let:Status),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           END
                           PROMPT('Telephone Number'),AT(8,68),USE(?LET:TelephoneNumber:Prompt)
                           ENTRY(@s20),AT(84,68,84,10),USE(let:TelephoneNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Telephone Number on top of letter'),TIP('Telephone Number on top of letter'),UPR
                           PROMPT('Fax Number'),AT(8,84),USE(?LET:FaxNumber:Prompt)
                           ENTRY(@s20),AT(84,84,84,10),USE(let:FaxNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fax Number on top of letter'),TIP('Fax Number on top of letter'),UPR
                           PROMPT('Letter Text'),AT(8,100),USE(?LET:LetterText:Prompt)
                           TEXT,AT(84,100,348,200),USE(let:LetterText),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON('&OK'),AT(320,316,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(376,316,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,312,432,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCloseEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?LET:Description:Prompt{prop:FontColor} = -1
    ?LET:Description:Prompt{prop:Color} = 15066597
    If ?let:Description{prop:ReadOnly} = True
        ?let:Description{prop:FontColor} = 65793
        ?let:Description{prop:Color} = 15066597
    Elsif ?let:Description{prop:Req} = True
        ?let:Description{prop:FontColor} = 65793
        ?let:Description{prop:Color} = 8454143
    Else ! If ?let:Description{prop:Req} = True
        ?let:Description{prop:FontColor} = 65793
        ?let:Description{prop:Color} = 16777215
    End ! If ?let:Description{prop:Req} = True
    ?let:Description{prop:Trn} = 0
    ?let:Description{prop:FontStyle} = font:Bold
    ?LET:Subject:Prompt{prop:FontColor} = -1
    ?LET:Subject:Prompt{prop:Color} = 15066597
    If ?let:Subject{prop:ReadOnly} = True
        ?let:Subject{prop:FontColor} = 65793
        ?let:Subject{prop:Color} = 15066597
    Elsif ?let:Subject{prop:Req} = True
        ?let:Subject{prop:FontColor} = 65793
        ?let:Subject{prop:Color} = 8454143
    Else ! If ?let:Subject{prop:Req} = True
        ?let:Subject{prop:FontColor} = 65793
        ?let:Subject{prop:Color} = 16777215
    End ! If ?let:Subject{prop:Req} = True
    ?let:Subject{prop:Trn} = 0
    ?let:Subject{prop:FontStyle} = font:Bold
    ?let:UseStatus{prop:Font,3} = -1
    ?let:UseStatus{prop:Color} = 15066597
    ?let:UseStatus{prop:Trn} = 0
    ?Status_Group{prop:Font,3} = -1
    ?Status_Group{prop:Color} = 15066597
    ?Status_Group{prop:Trn} = 0
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?let:Status{prop:ReadOnly} = True
        ?let:Status{prop:FontColor} = 65793
        ?let:Status{prop:Color} = 15066597
    Elsif ?let:Status{prop:Req} = True
        ?let:Status{prop:FontColor} = 65793
        ?let:Status{prop:Color} = 8454143
    Else ! If ?let:Status{prop:Req} = True
        ?let:Status{prop:FontColor} = 65793
        ?let:Status{prop:Color} = 16777215
    End ! If ?let:Status{prop:Req} = True
    ?let:Status{prop:Trn} = 0
    ?let:Status{prop:FontStyle} = font:Bold
    ?LET:TelephoneNumber:Prompt{prop:FontColor} = -1
    ?LET:TelephoneNumber:Prompt{prop:Color} = 15066597
    If ?let:TelephoneNumber{prop:ReadOnly} = True
        ?let:TelephoneNumber{prop:FontColor} = 65793
        ?let:TelephoneNumber{prop:Color} = 15066597
    Elsif ?let:TelephoneNumber{prop:Req} = True
        ?let:TelephoneNumber{prop:FontColor} = 65793
        ?let:TelephoneNumber{prop:Color} = 8454143
    Else ! If ?let:TelephoneNumber{prop:Req} = True
        ?let:TelephoneNumber{prop:FontColor} = 65793
        ?let:TelephoneNumber{prop:Color} = 16777215
    End ! If ?let:TelephoneNumber{prop:Req} = True
    ?let:TelephoneNumber{prop:Trn} = 0
    ?let:TelephoneNumber{prop:FontStyle} = font:Bold
    ?LET:FaxNumber:Prompt{prop:FontColor} = -1
    ?LET:FaxNumber:Prompt{prop:Color} = 15066597
    If ?let:FaxNumber{prop:ReadOnly} = True
        ?let:FaxNumber{prop:FontColor} = 65793
        ?let:FaxNumber{prop:Color} = 15066597
    Elsif ?let:FaxNumber{prop:Req} = True
        ?let:FaxNumber{prop:FontColor} = 65793
        ?let:FaxNumber{prop:Color} = 8454143
    Else ! If ?let:FaxNumber{prop:Req} = True
        ?let:FaxNumber{prop:FontColor} = 65793
        ?let:FaxNumber{prop:Color} = 16777215
    End ! If ?let:FaxNumber{prop:Req} = True
    ?let:FaxNumber{prop:Trn} = 0
    ?let:FaxNumber{prop:FontStyle} = font:Bold
    ?LET:LetterText:Prompt{prop:FontColor} = -1
    ?LET:LetterText:Prompt{prop:Color} = 15066597
    If ?let:LetterText{prop:ReadOnly} = True
        ?let:LetterText{prop:FontColor} = 65793
        ?let:LetterText{prop:Color} = 15066597
    Elsif ?let:LetterText{prop:Req} = True
        ?let:LetterText{prop:FontColor} = 65793
        ?let:LetterText{prop:Color} = 8454143
    Else ! If ?let:LetterText{prop:Req} = True
        ?let:LetterText{prop:FontColor} = 65793
        ?let:LetterText{prop:Color} = 16777215
    End ! If ?let:LetterText{prop:Req} = True
    ?let:LetterText{prop:Trn} = 0
    ?let:LetterText{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Letters',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Letters',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Letters',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:Description:Prompt;  SolaceCtrlName = '?LET:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:Description;  SolaceCtrlName = '?let:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:Subject:Prompt;  SolaceCtrlName = '?LET:Subject:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:Subject;  SolaceCtrlName = '?let:Subject';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:UseStatus;  SolaceCtrlName = '?let:UseStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Status_Group;  SolaceCtrlName = '?Status_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:Status;  SolaceCtrlName = '?let:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:TelephoneNumber:Prompt;  SolaceCtrlName = '?LET:TelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:TelephoneNumber;  SolaceCtrlName = '?let:TelephoneNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:FaxNumber:Prompt;  SolaceCtrlName = '?LET:FaxNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:FaxNumber;  SolaceCtrlName = '?let:FaxNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LET:LetterText:Prompt;  SolaceCtrlName = '?LET:LetterText:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?let:LetterText;  SolaceCtrlName = '?let:LetterText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Letter'
  OF ChangeRecord
    ActionMessage = 'Changing A Letter'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Letters')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Letters')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LET:Description:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(let:Record,History::let:Record)
  SELF.AddHistoryField(?let:Description,2)
  SELF.AddHistoryField(?let:Subject,3)
  SELF.AddHistoryField(?let:UseStatus,7)
  SELF.AddHistoryField(?let:Status,8)
  SELF.AddHistoryField(?let:TelephoneNumber,4)
  SELF.AddHistoryField(?let:FaxNumber,5)
  SELF.AddHistoryField(?let:LetterText,6)
  SELF.AddUpdateFile(Access:LETTERS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LETTERS.Open
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LETTERS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?let:UseStatus{Prop:Checked} = True
    UNHIDE(?Status_Group)
  END
  IF ?let:UseStatus{Prop:Checked} = False
    HIDE(?Status_Group)
  END
  FDCB6.Init(let:Status,?let:Status,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:STATUS,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sts:Status_Key)
  FDCB6.AddField(sts:Status,FDCB6.Q.sts:Status)
  FDCB6.AddField(sts:Ref_Number,FDCB6.Q.sts:Ref_Number)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LETTERS.Close
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Letters',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?let:UseStatus
      IF ?let:UseStatus{Prop:Checked} = True
        UNHIDE(?Status_Group)
      END
      IF ?let:UseStatus{Prop:Checked} = False
        HIDE(?Status_Group)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCloseEvent PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF ClarioNETServer:Active()
    IF SELF.Response <> RequestCompleted AND ~SELF.Primary &= NULL
      IF SELF.CancelAction <> Cancel:Cancel AND ( SELF.Request = InsertRecord OR SELF.Request = ChangeRecord )
        IF ~SELF.Primary.Me.EqualBuffer(SELF.Saved)
          IF BAND(SELF.CancelAction,Cancel:Save)
            IF BAND(SELF.CancelAction,Cancel:Query)
              CASE SELF.Errors.Message(Msg:SaveRecord,Button:Yes+Button:No,Button:No)
              OF Button:Yes
                POST(Event:Accepted,SELF.OKControl)
                RETURN Level:Notify
              !OF BUTTON:Cancel
              !  SELECT(SELF.FirstField)
              !  RETURN Level:Notify
              END
            ELSE
              POST(Event:Accepted,SELF.OKControl)
              RETURN Level:Notify
            END
          ELSE
            IF SELF.Errors.Throw(Msg:ConfirmCancel) = Level:Cancel
              SELECT(SELF.FirstField)
              RETURN Level:Notify
            END
          END
        END
      END
      IF SELF.OriginalRequest = InsertRecord AND SELF.Response = RequestCancelled
        IF SELF.Primary.CancelAutoInc() THEN
          SELECT(SELF.FirstField)
          RETURN Level:Notify
        END
      END
      !IF SELF.LastInsertedPosition
      !  SELF.Response = RequestCompleted
      !  SELF.Primary.Me.TryReget(SELF.LastInsertedPosition)
      !END
    END
  
    RETURNValue = Level:Benign                                                                        !---ClarioNET ??
  ELSE                                                                                                !---ClarioNET ??
  ReturnValue = PARENT.TakeCloseEvent()
  END                                                                                                 !---ClarioNET ??
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Letters')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Post(event:accepted,?let:usestatus)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

