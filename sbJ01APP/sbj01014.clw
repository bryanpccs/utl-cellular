

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01014.INC'),ONCE        !Local module procedure declarations
                     END


UpdateJOBTHIRD PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::jot:Record  LIKE(jot:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBTHIRD File'),AT(,,240,178),FONT('Tahoma',8,,),IMM,HLP('UpdateJOBTHIRD'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,232,152),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?JOT:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,20,64,10),USE(jot:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Ref Number'),AT(8,34),USE(?JOT:RefNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,34,64,10),USE(jot:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link To Job''s File'),TIP('Link To Job''s File'),UPR
                           PROMPT('Original I.M.E.I. No'),AT(8,48),USE(?JOT:OriginalIMEI:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,48,124,10),USE(jot:OriginalIMEI),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Original IMEI Number'),TIP('Original IMEI Number'),UPR
                           PROMPT('Outgoing I.M.E.I. Number'),AT(8,62),USE(?JOT:OutIMEI:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,62,124,10),USE(jot:OutIMEI),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Outgoing I.M.E.I. Number'),TIP('Outgoing I.M.E.I. Number'),UPR
                           PROMPT('Incoming I.M.E.I. No'),AT(8,76),USE(?JOT:InIMEI:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(108,76,124,10),USE(jot:InIMEI),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Incoming I.M.E.I. Number'),TIP('Incoming I.M.E.I. Number'),UPR
                           PROMPT('Outgoing Date'),AT(8,90),USE(?JOT:DateOut:Prompt)
                           ENTRY(@d6),AT(108,90,64,10),USE(jot:DateOut),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Incoming Date'),AT(8,104),USE(?JOT:DateIn:Prompt)
                           ENTRY(@d6),AT(108,104,64,10),USE(jot:DateIn),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Dummy Field:'),AT(8,118),USE(?JOT:DummyField:Prompt)
                           PROMPT('Link To Third Party Entry'),AT(8,132),USE(?jot:ThirdPartyNumber:Prompt),TRN
                           ENTRY(@s8),AT(108,132,64,10),USE(jot:ThirdPartyNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Link To Third Party Entry'),TIP('Link To Third Party Entry'),UPR
                         END
                       END
                       BUTTON('OK'),AT(136,160,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(188,160,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(191,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?JOT:RecordNumber:Prompt{prop:FontColor} = -1
    ?JOT:RecordNumber:Prompt{prop:Color} = 15066597
    If ?jot:RecordNumber{prop:ReadOnly} = True
        ?jot:RecordNumber{prop:FontColor} = 65793
        ?jot:RecordNumber{prop:Color} = 15066597
    Elsif ?jot:RecordNumber{prop:Req} = True
        ?jot:RecordNumber{prop:FontColor} = 65793
        ?jot:RecordNumber{prop:Color} = 8454143
    Else ! If ?jot:RecordNumber{prop:Req} = True
        ?jot:RecordNumber{prop:FontColor} = 65793
        ?jot:RecordNumber{prop:Color} = 16777215
    End ! If ?jot:RecordNumber{prop:Req} = True
    ?jot:RecordNumber{prop:Trn} = 0
    ?jot:RecordNumber{prop:FontStyle} = font:Bold
    ?JOT:RefNumber:Prompt{prop:FontColor} = -1
    ?JOT:RefNumber:Prompt{prop:Color} = 15066597
    If ?jot:RefNumber{prop:ReadOnly} = True
        ?jot:RefNumber{prop:FontColor} = 65793
        ?jot:RefNumber{prop:Color} = 15066597
    Elsif ?jot:RefNumber{prop:Req} = True
        ?jot:RefNumber{prop:FontColor} = 65793
        ?jot:RefNumber{prop:Color} = 8454143
    Else ! If ?jot:RefNumber{prop:Req} = True
        ?jot:RefNumber{prop:FontColor} = 65793
        ?jot:RefNumber{prop:Color} = 16777215
    End ! If ?jot:RefNumber{prop:Req} = True
    ?jot:RefNumber{prop:Trn} = 0
    ?jot:RefNumber{prop:FontStyle} = font:Bold
    ?JOT:OriginalIMEI:Prompt{prop:FontColor} = -1
    ?JOT:OriginalIMEI:Prompt{prop:Color} = 15066597
    If ?jot:OriginalIMEI{prop:ReadOnly} = True
        ?jot:OriginalIMEI{prop:FontColor} = 65793
        ?jot:OriginalIMEI{prop:Color} = 15066597
    Elsif ?jot:OriginalIMEI{prop:Req} = True
        ?jot:OriginalIMEI{prop:FontColor} = 65793
        ?jot:OriginalIMEI{prop:Color} = 8454143
    Else ! If ?jot:OriginalIMEI{prop:Req} = True
        ?jot:OriginalIMEI{prop:FontColor} = 65793
        ?jot:OriginalIMEI{prop:Color} = 16777215
    End ! If ?jot:OriginalIMEI{prop:Req} = True
    ?jot:OriginalIMEI{prop:Trn} = 0
    ?jot:OriginalIMEI{prop:FontStyle} = font:Bold
    ?JOT:OutIMEI:Prompt{prop:FontColor} = -1
    ?JOT:OutIMEI:Prompt{prop:Color} = 15066597
    If ?jot:OutIMEI{prop:ReadOnly} = True
        ?jot:OutIMEI{prop:FontColor} = 65793
        ?jot:OutIMEI{prop:Color} = 15066597
    Elsif ?jot:OutIMEI{prop:Req} = True
        ?jot:OutIMEI{prop:FontColor} = 65793
        ?jot:OutIMEI{prop:Color} = 8454143
    Else ! If ?jot:OutIMEI{prop:Req} = True
        ?jot:OutIMEI{prop:FontColor} = 65793
        ?jot:OutIMEI{prop:Color} = 16777215
    End ! If ?jot:OutIMEI{prop:Req} = True
    ?jot:OutIMEI{prop:Trn} = 0
    ?jot:OutIMEI{prop:FontStyle} = font:Bold
    ?JOT:InIMEI:Prompt{prop:FontColor} = -1
    ?JOT:InIMEI:Prompt{prop:Color} = 15066597
    If ?jot:InIMEI{prop:ReadOnly} = True
        ?jot:InIMEI{prop:FontColor} = 65793
        ?jot:InIMEI{prop:Color} = 15066597
    Elsif ?jot:InIMEI{prop:Req} = True
        ?jot:InIMEI{prop:FontColor} = 65793
        ?jot:InIMEI{prop:Color} = 8454143
    Else ! If ?jot:InIMEI{prop:Req} = True
        ?jot:InIMEI{prop:FontColor} = 65793
        ?jot:InIMEI{prop:Color} = 16777215
    End ! If ?jot:InIMEI{prop:Req} = True
    ?jot:InIMEI{prop:Trn} = 0
    ?jot:InIMEI{prop:FontStyle} = font:Bold
    ?JOT:DateOut:Prompt{prop:FontColor} = -1
    ?JOT:DateOut:Prompt{prop:Color} = 15066597
    If ?jot:DateOut{prop:ReadOnly} = True
        ?jot:DateOut{prop:FontColor} = 65793
        ?jot:DateOut{prop:Color} = 15066597
    Elsif ?jot:DateOut{prop:Req} = True
        ?jot:DateOut{prop:FontColor} = 65793
        ?jot:DateOut{prop:Color} = 8454143
    Else ! If ?jot:DateOut{prop:Req} = True
        ?jot:DateOut{prop:FontColor} = 65793
        ?jot:DateOut{prop:Color} = 16777215
    End ! If ?jot:DateOut{prop:Req} = True
    ?jot:DateOut{prop:Trn} = 0
    ?jot:DateOut{prop:FontStyle} = font:Bold
    ?JOT:DateIn:Prompt{prop:FontColor} = -1
    ?JOT:DateIn:Prompt{prop:Color} = 15066597
    If ?jot:DateIn{prop:ReadOnly} = True
        ?jot:DateIn{prop:FontColor} = 65793
        ?jot:DateIn{prop:Color} = 15066597
    Elsif ?jot:DateIn{prop:Req} = True
        ?jot:DateIn{prop:FontColor} = 65793
        ?jot:DateIn{prop:Color} = 8454143
    Else ! If ?jot:DateIn{prop:Req} = True
        ?jot:DateIn{prop:FontColor} = 65793
        ?jot:DateIn{prop:Color} = 16777215
    End ! If ?jot:DateIn{prop:Req} = True
    ?jot:DateIn{prop:Trn} = 0
    ?jot:DateIn{prop:FontStyle} = font:Bold
    ?JOT:DummyField:Prompt{prop:FontColor} = -1
    ?JOT:DummyField:Prompt{prop:Color} = 15066597
    ?jot:ThirdPartyNumber:Prompt{prop:FontColor} = -1
    ?jot:ThirdPartyNumber:Prompt{prop:Color} = 15066597
    If ?jot:ThirdPartyNumber{prop:ReadOnly} = True
        ?jot:ThirdPartyNumber{prop:FontColor} = 65793
        ?jot:ThirdPartyNumber{prop:Color} = 15066597
    Elsif ?jot:ThirdPartyNumber{prop:Req} = True
        ?jot:ThirdPartyNumber{prop:FontColor} = 65793
        ?jot:ThirdPartyNumber{prop:Color} = 8454143
    Else ! If ?jot:ThirdPartyNumber{prop:Req} = True
        ?jot:ThirdPartyNumber{prop:FontColor} = 65793
        ?jot:ThirdPartyNumber{prop:Color} = 16777215
    End ! If ?jot:ThirdPartyNumber{prop:Req} = True
    ?jot:ThirdPartyNumber{prop:Trn} = 0
    ?jot:ThirdPartyNumber{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateJOBTHIRD',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateJOBTHIRD',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateJOBTHIRD',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:RecordNumber:Prompt;  SolaceCtrlName = '?JOT:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:RecordNumber;  SolaceCtrlName = '?jot:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:RefNumber:Prompt;  SolaceCtrlName = '?JOT:RefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:RefNumber;  SolaceCtrlName = '?jot:RefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:OriginalIMEI:Prompt;  SolaceCtrlName = '?JOT:OriginalIMEI:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:OriginalIMEI;  SolaceCtrlName = '?jot:OriginalIMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:OutIMEI:Prompt;  SolaceCtrlName = '?JOT:OutIMEI:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:OutIMEI;  SolaceCtrlName = '?jot:OutIMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:InIMEI:Prompt;  SolaceCtrlName = '?JOT:InIMEI:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:InIMEI;  SolaceCtrlName = '?jot:InIMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:DateOut:Prompt;  SolaceCtrlName = '?JOT:DateOut:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:DateOut;  SolaceCtrlName = '?jot:DateOut';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:DateIn:Prompt;  SolaceCtrlName = '?JOT:DateIn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:DateIn;  SolaceCtrlName = '?jot:DateIn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOT:DummyField:Prompt;  SolaceCtrlName = '?JOT:DummyField:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:ThirdPartyNumber:Prompt;  SolaceCtrlName = '?jot:ThirdPartyNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jot:ThirdPartyNumber;  SolaceCtrlName = '?jot:ThirdPartyNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJOBTHIRD')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateJOBTHIRD')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JOT:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(jot:Record,History::jot:Record)
  SELF.AddHistoryField(?jot:RecordNumber,1)
  SELF.AddHistoryField(?jot:RefNumber,2)
  SELF.AddHistoryField(?jot:OriginalIMEI,3)
  SELF.AddHistoryField(?jot:OutIMEI,4)
  SELF.AddHistoryField(?jot:InIMEI,5)
  SELF.AddHistoryField(?jot:DateOut,6)
  SELF.AddHistoryField(?jot:DateIn,8)
  SELF.AddHistoryField(?jot:ThirdPartyNumber,9)
  SELF.AddUpdateFile(Access:JOBTHIRD)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBTHIRD.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBTHIRD
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBTHIRD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateJOBTHIRD',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateJOBTHIRD')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

