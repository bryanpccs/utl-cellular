

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01040.INC'),ONCE        !Local module procedure declarations
                     END


Update_Invoice PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
sav:couriercost      REAL
sav:labourcost       REAL
sav:partscost        REAL
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::inv:Record  LIKE(inv:RECORD),STATIC
QuickWindow          WINDOW('Update the INVOICE File'),AT(,,219,212),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('Update_Invoice'),TIMER(50),SYSTEM,GRAY,DOUBLE,IMM
                       PROMPT('Invoice Number'),AT(8,20),USE(?INV:Invoice_Number:Prompt),TRN
                       ENTRY(@s8),AT(84,20,56,10),USE(inv:Invoice_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       PROMPT('Account Number'),AT(8,36),USE(?INV:Account_Number:Prompt),TRN
                       ENTRY(@s15),AT(84,36,124,10),USE(inv:Account_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                       SHEET,AT(4,4,212,176),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Batch Number'),AT(8,52),USE(?INV:Batch_Number:Prompt)
                           ENTRY(@s6),AT(84,52,60,10),USE(inv:Batch_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Courier Value'),AT(8,68),USE(?INV:Courier_Paid:Prompt)
                           ENTRY(@n14.2b),AT(84,68,60,10),USE(inv:Courier_Paid),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Labour Value'),AT(8,84),USE(?INV:Labour_Paid:Prompt)
                           ENTRY(@n14.2b),AT(84,84,60,10),USE(inv:Labour_Paid),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Parts Value'),AT(8,100),USE(?INV:Parts_Paid:Prompt)
                           ENTRY(@n14.2b),AT(84,100,60,10),USE(inv:Parts_Paid),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Total (Ex V.A.T.)'),AT(8,116),USE(?INV:Total:Prompt),TRN
                           ENTRY(@n14.2b),AT(84,116,60,10),USE(inv:Total),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour V.A.T. Rate'),AT(8,132),USE(?INV:Vat_Rate_Labour:Prompt),TRN
                           ENTRY(@N14.2B),AT(84,132,60,10),USE(inv:Vat_Rate_Labour),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Parts V.A.T. Rate'),AT(8,148),USE(?INV:Vat_Rate_Parts:Prompt),TRN
                           ENTRY(@N14.2B),AT(84,148,60,10),USE(inv:Vat_Rate_Parts),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Retail V.A.T. Rate'),AT(8,164),USE(?INV:Vat_Rate_Retail:Prompt),TRN
                           ENTRY(@n14.2B),AT(84,164,60,10),USE(inv:Vat_Rate_Retail),RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('Chargeable Invoice'),USE(?Tab2),HIDE
                           PROMPT('Courier Cost'),AT(8,52),USE(?JOB:Invoice_Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,52,64,10),USE(job:Invoice_Courier_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour Cost'),AT(8,68),USE(?JOB:Invoice_Labour_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,68,64,10),USE(job:Invoice_Labour_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Parts Cost'),AT(8,84),USE(?JOB:Invoice_Parts_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(job:Invoice_Parts_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Total (Ex Vat)'),AT(8,100),USE(?JOB:Invoice_Sub_Total:Prompt),TRN
                           ENTRY(@n14.2),AT(84,100,64,10),USE(job:Invoice_Sub_Total),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                         TAB('Warranty Invoice'),USE(?Tab3)
                           PROMPT('EDI Batch Number'),AT(8,52),USE(?INV:Batch_Number:Prompt:2)
                           ENTRY(@n6),AT(84,52,60,10),USE(inv:Batch_Number,,?INV:Batch_Number:2),DECIMAL(14),FONT(,8,,FONT:bold),UPR
                           PROMPT('Claim Reference'),AT(8,68),USE(?INV:Claim_Reference:Prompt),TRN
                           ENTRY(@s30),AT(84,68,60,10),USE(inv:Claim_Reference),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Courier Cost'),AT(8,100),USE(?INV:Courier_Paid:Prompt:2)
                           ENTRY(@n14.2),AT(84,100,60,10),USE(inv:Courier_Paid,,?INV:Courier_Paid:2),DECIMAL(14),FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Labour Cost'),AT(8,116),USE(?INV:Labour_Paid:Prompt:2)
                           ENTRY(@n14.2),AT(84,116,60,10),USE(inv:Labour_Paid,,?INV:Labour_Paid:2),DECIMAL(14),FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Parts Cost'),AT(8,132),USE(?INV:Parts_Paid:Prompt:2)
                           ENTRY(@n14.2),AT(84,132,60,10),USE(inv:Parts_Paid,,?INV:Parts_Paid:2),DECIMAL(14),FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Total Claimed'),AT(8,84),USE(?INV:Total_Claimed:Prompt)
                           ENTRY(@n14.2),AT(84,84,60,10),USE(inv:Total_Claimed),DECIMAL(14),FONT(,,,FONT:bold),MSG('r'),TIP('r')
                         END
                       END
                       BUTTON('&OK'),AT(100,188,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,188,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,184,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?INV:Invoice_Number:Prompt{prop:FontColor} = -1
    ?INV:Invoice_Number:Prompt{prop:Color} = 15066597
    If ?inv:Invoice_Number{prop:ReadOnly} = True
        ?inv:Invoice_Number{prop:FontColor} = 65793
        ?inv:Invoice_Number{prop:Color} = 15066597
    Elsif ?inv:Invoice_Number{prop:Req} = True
        ?inv:Invoice_Number{prop:FontColor} = 65793
        ?inv:Invoice_Number{prop:Color} = 8454143
    Else ! If ?inv:Invoice_Number{prop:Req} = True
        ?inv:Invoice_Number{prop:FontColor} = 65793
        ?inv:Invoice_Number{prop:Color} = 16777215
    End ! If ?inv:Invoice_Number{prop:Req} = True
    ?inv:Invoice_Number{prop:Trn} = 0
    ?inv:Invoice_Number{prop:FontStyle} = font:Bold
    ?INV:Account_Number:Prompt{prop:FontColor} = -1
    ?INV:Account_Number:Prompt{prop:Color} = 15066597
    If ?inv:Account_Number{prop:ReadOnly} = True
        ?inv:Account_Number{prop:FontColor} = 65793
        ?inv:Account_Number{prop:Color} = 15066597
    Elsif ?inv:Account_Number{prop:Req} = True
        ?inv:Account_Number{prop:FontColor} = 65793
        ?inv:Account_Number{prop:Color} = 8454143
    Else ! If ?inv:Account_Number{prop:Req} = True
        ?inv:Account_Number{prop:FontColor} = 65793
        ?inv:Account_Number{prop:Color} = 16777215
    End ! If ?inv:Account_Number{prop:Req} = True
    ?inv:Account_Number{prop:Trn} = 0
    ?inv:Account_Number{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?INV:Batch_Number:Prompt{prop:FontColor} = -1
    ?INV:Batch_Number:Prompt{prop:Color} = 15066597
    If ?inv:Batch_Number{prop:ReadOnly} = True
        ?inv:Batch_Number{prop:FontColor} = 65793
        ?inv:Batch_Number{prop:Color} = 15066597
    Elsif ?inv:Batch_Number{prop:Req} = True
        ?inv:Batch_Number{prop:FontColor} = 65793
        ?inv:Batch_Number{prop:Color} = 8454143
    Else ! If ?inv:Batch_Number{prop:Req} = True
        ?inv:Batch_Number{prop:FontColor} = 65793
        ?inv:Batch_Number{prop:Color} = 16777215
    End ! If ?inv:Batch_Number{prop:Req} = True
    ?inv:Batch_Number{prop:Trn} = 0
    ?inv:Batch_Number{prop:FontStyle} = font:Bold
    ?INV:Courier_Paid:Prompt{prop:FontColor} = -1
    ?INV:Courier_Paid:Prompt{prop:Color} = 15066597
    If ?inv:Courier_Paid{prop:ReadOnly} = True
        ?inv:Courier_Paid{prop:FontColor} = 65793
        ?inv:Courier_Paid{prop:Color} = 15066597
    Elsif ?inv:Courier_Paid{prop:Req} = True
        ?inv:Courier_Paid{prop:FontColor} = 65793
        ?inv:Courier_Paid{prop:Color} = 8454143
    Else ! If ?inv:Courier_Paid{prop:Req} = True
        ?inv:Courier_Paid{prop:FontColor} = 65793
        ?inv:Courier_Paid{prop:Color} = 16777215
    End ! If ?inv:Courier_Paid{prop:Req} = True
    ?inv:Courier_Paid{prop:Trn} = 0
    ?inv:Courier_Paid{prop:FontStyle} = font:Bold
    ?INV:Labour_Paid:Prompt{prop:FontColor} = -1
    ?INV:Labour_Paid:Prompt{prop:Color} = 15066597
    If ?inv:Labour_Paid{prop:ReadOnly} = True
        ?inv:Labour_Paid{prop:FontColor} = 65793
        ?inv:Labour_Paid{prop:Color} = 15066597
    Elsif ?inv:Labour_Paid{prop:Req} = True
        ?inv:Labour_Paid{prop:FontColor} = 65793
        ?inv:Labour_Paid{prop:Color} = 8454143
    Else ! If ?inv:Labour_Paid{prop:Req} = True
        ?inv:Labour_Paid{prop:FontColor} = 65793
        ?inv:Labour_Paid{prop:Color} = 16777215
    End ! If ?inv:Labour_Paid{prop:Req} = True
    ?inv:Labour_Paid{prop:Trn} = 0
    ?inv:Labour_Paid{prop:FontStyle} = font:Bold
    ?INV:Parts_Paid:Prompt{prop:FontColor} = -1
    ?INV:Parts_Paid:Prompt{prop:Color} = 15066597
    If ?inv:Parts_Paid{prop:ReadOnly} = True
        ?inv:Parts_Paid{prop:FontColor} = 65793
        ?inv:Parts_Paid{prop:Color} = 15066597
    Elsif ?inv:Parts_Paid{prop:Req} = True
        ?inv:Parts_Paid{prop:FontColor} = 65793
        ?inv:Parts_Paid{prop:Color} = 8454143
    Else ! If ?inv:Parts_Paid{prop:Req} = True
        ?inv:Parts_Paid{prop:FontColor} = 65793
        ?inv:Parts_Paid{prop:Color} = 16777215
    End ! If ?inv:Parts_Paid{prop:Req} = True
    ?inv:Parts_Paid{prop:Trn} = 0
    ?inv:Parts_Paid{prop:FontStyle} = font:Bold
    ?INV:Total:Prompt{prop:FontColor} = -1
    ?INV:Total:Prompt{prop:Color} = 15066597
    If ?inv:Total{prop:ReadOnly} = True
        ?inv:Total{prop:FontColor} = 65793
        ?inv:Total{prop:Color} = 15066597
    Elsif ?inv:Total{prop:Req} = True
        ?inv:Total{prop:FontColor} = 65793
        ?inv:Total{prop:Color} = 8454143
    Else ! If ?inv:Total{prop:Req} = True
        ?inv:Total{prop:FontColor} = 65793
        ?inv:Total{prop:Color} = 16777215
    End ! If ?inv:Total{prop:Req} = True
    ?inv:Total{prop:Trn} = 0
    ?inv:Total{prop:FontStyle} = font:Bold
    ?INV:Vat_Rate_Labour:Prompt{prop:FontColor} = -1
    ?INV:Vat_Rate_Labour:Prompt{prop:Color} = 15066597
    If ?inv:Vat_Rate_Labour{prop:ReadOnly} = True
        ?inv:Vat_Rate_Labour{prop:FontColor} = 65793
        ?inv:Vat_Rate_Labour{prop:Color} = 15066597
    Elsif ?inv:Vat_Rate_Labour{prop:Req} = True
        ?inv:Vat_Rate_Labour{prop:FontColor} = 65793
        ?inv:Vat_Rate_Labour{prop:Color} = 8454143
    Else ! If ?inv:Vat_Rate_Labour{prop:Req} = True
        ?inv:Vat_Rate_Labour{prop:FontColor} = 65793
        ?inv:Vat_Rate_Labour{prop:Color} = 16777215
    End ! If ?inv:Vat_Rate_Labour{prop:Req} = True
    ?inv:Vat_Rate_Labour{prop:Trn} = 0
    ?inv:Vat_Rate_Labour{prop:FontStyle} = font:Bold
    ?INV:Vat_Rate_Parts:Prompt{prop:FontColor} = -1
    ?INV:Vat_Rate_Parts:Prompt{prop:Color} = 15066597
    If ?inv:Vat_Rate_Parts{prop:ReadOnly} = True
        ?inv:Vat_Rate_Parts{prop:FontColor} = 65793
        ?inv:Vat_Rate_Parts{prop:Color} = 15066597
    Elsif ?inv:Vat_Rate_Parts{prop:Req} = True
        ?inv:Vat_Rate_Parts{prop:FontColor} = 65793
        ?inv:Vat_Rate_Parts{prop:Color} = 8454143
    Else ! If ?inv:Vat_Rate_Parts{prop:Req} = True
        ?inv:Vat_Rate_Parts{prop:FontColor} = 65793
        ?inv:Vat_Rate_Parts{prop:Color} = 16777215
    End ! If ?inv:Vat_Rate_Parts{prop:Req} = True
    ?inv:Vat_Rate_Parts{prop:Trn} = 0
    ?inv:Vat_Rate_Parts{prop:FontStyle} = font:Bold
    ?INV:Vat_Rate_Retail:Prompt{prop:FontColor} = -1
    ?INV:Vat_Rate_Retail:Prompt{prop:Color} = 15066597
    If ?inv:Vat_Rate_Retail{prop:ReadOnly} = True
        ?inv:Vat_Rate_Retail{prop:FontColor} = 65793
        ?inv:Vat_Rate_Retail{prop:Color} = 15066597
    Elsif ?inv:Vat_Rate_Retail{prop:Req} = True
        ?inv:Vat_Rate_Retail{prop:FontColor} = 65793
        ?inv:Vat_Rate_Retail{prop:Color} = 8454143
    Else ! If ?inv:Vat_Rate_Retail{prop:Req} = True
        ?inv:Vat_Rate_Retail{prop:FontColor} = 65793
        ?inv:Vat_Rate_Retail{prop:Color} = 16777215
    End ! If ?inv:Vat_Rate_Retail{prop:Req} = True
    ?inv:Vat_Rate_Retail{prop:Trn} = 0
    ?inv:Vat_Rate_Retail{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?JOB:Invoice_Courier_Cost:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Courier_Cost:Prompt{prop:Color} = 15066597
    If ?job:Invoice_Courier_Cost{prop:ReadOnly} = True
        ?job:Invoice_Courier_Cost{prop:FontColor} = 65793
        ?job:Invoice_Courier_Cost{prop:Color} = 15066597
    Elsif ?job:Invoice_Courier_Cost{prop:Req} = True
        ?job:Invoice_Courier_Cost{prop:FontColor} = 65793
        ?job:Invoice_Courier_Cost{prop:Color} = 8454143
    Else ! If ?job:Invoice_Courier_Cost{prop:Req} = True
        ?job:Invoice_Courier_Cost{prop:FontColor} = 65793
        ?job:Invoice_Courier_Cost{prop:Color} = 16777215
    End ! If ?job:Invoice_Courier_Cost{prop:Req} = True
    ?job:Invoice_Courier_Cost{prop:Trn} = 0
    ?job:Invoice_Courier_Cost{prop:FontStyle} = font:Bold
    ?JOB:Invoice_Labour_Cost:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Labour_Cost:Prompt{prop:Color} = 15066597
    If ?job:Invoice_Labour_Cost{prop:ReadOnly} = True
        ?job:Invoice_Labour_Cost{prop:FontColor} = 65793
        ?job:Invoice_Labour_Cost{prop:Color} = 15066597
    Elsif ?job:Invoice_Labour_Cost{prop:Req} = True
        ?job:Invoice_Labour_Cost{prop:FontColor} = 65793
        ?job:Invoice_Labour_Cost{prop:Color} = 8454143
    Else ! If ?job:Invoice_Labour_Cost{prop:Req} = True
        ?job:Invoice_Labour_Cost{prop:FontColor} = 65793
        ?job:Invoice_Labour_Cost{prop:Color} = 16777215
    End ! If ?job:Invoice_Labour_Cost{prop:Req} = True
    ?job:Invoice_Labour_Cost{prop:Trn} = 0
    ?job:Invoice_Labour_Cost{prop:FontStyle} = font:Bold
    ?JOB:Invoice_Parts_Cost:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Parts_Cost:Prompt{prop:Color} = 15066597
    If ?job:Invoice_Parts_Cost{prop:ReadOnly} = True
        ?job:Invoice_Parts_Cost{prop:FontColor} = 65793
        ?job:Invoice_Parts_Cost{prop:Color} = 15066597
    Elsif ?job:Invoice_Parts_Cost{prop:Req} = True
        ?job:Invoice_Parts_Cost{prop:FontColor} = 65793
        ?job:Invoice_Parts_Cost{prop:Color} = 8454143
    Else ! If ?job:Invoice_Parts_Cost{prop:Req} = True
        ?job:Invoice_Parts_Cost{prop:FontColor} = 65793
        ?job:Invoice_Parts_Cost{prop:Color} = 16777215
    End ! If ?job:Invoice_Parts_Cost{prop:Req} = True
    ?job:Invoice_Parts_Cost{prop:Trn} = 0
    ?job:Invoice_Parts_Cost{prop:FontStyle} = font:Bold
    ?JOB:Invoice_Sub_Total:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Sub_Total:Prompt{prop:Color} = 15066597
    If ?job:Invoice_Sub_Total{prop:ReadOnly} = True
        ?job:Invoice_Sub_Total{prop:FontColor} = 65793
        ?job:Invoice_Sub_Total{prop:Color} = 15066597
    Elsif ?job:Invoice_Sub_Total{prop:Req} = True
        ?job:Invoice_Sub_Total{prop:FontColor} = 65793
        ?job:Invoice_Sub_Total{prop:Color} = 8454143
    Else ! If ?job:Invoice_Sub_Total{prop:Req} = True
        ?job:Invoice_Sub_Total{prop:FontColor} = 65793
        ?job:Invoice_Sub_Total{prop:Color} = 16777215
    End ! If ?job:Invoice_Sub_Total{prop:Req} = True
    ?job:Invoice_Sub_Total{prop:Trn} = 0
    ?job:Invoice_Sub_Total{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    ?INV:Batch_Number:Prompt:2{prop:FontColor} = -1
    ?INV:Batch_Number:Prompt:2{prop:Color} = 15066597
    If ?INV:Batch_Number:2{prop:ReadOnly} = True
        ?INV:Batch_Number:2{prop:FontColor} = 65793
        ?INV:Batch_Number:2{prop:Color} = 15066597
    Elsif ?INV:Batch_Number:2{prop:Req} = True
        ?INV:Batch_Number:2{prop:FontColor} = 65793
        ?INV:Batch_Number:2{prop:Color} = 8454143
    Else ! If ?INV:Batch_Number:2{prop:Req} = True
        ?INV:Batch_Number:2{prop:FontColor} = 65793
        ?INV:Batch_Number:2{prop:Color} = 16777215
    End ! If ?INV:Batch_Number:2{prop:Req} = True
    ?INV:Batch_Number:2{prop:Trn} = 0
    ?INV:Batch_Number:2{prop:FontStyle} = font:Bold
    ?INV:Claim_Reference:Prompt{prop:FontColor} = -1
    ?INV:Claim_Reference:Prompt{prop:Color} = 15066597
    If ?inv:Claim_Reference{prop:ReadOnly} = True
        ?inv:Claim_Reference{prop:FontColor} = 65793
        ?inv:Claim_Reference{prop:Color} = 15066597
    Elsif ?inv:Claim_Reference{prop:Req} = True
        ?inv:Claim_Reference{prop:FontColor} = 65793
        ?inv:Claim_Reference{prop:Color} = 8454143
    Else ! If ?inv:Claim_Reference{prop:Req} = True
        ?inv:Claim_Reference{prop:FontColor} = 65793
        ?inv:Claim_Reference{prop:Color} = 16777215
    End ! If ?inv:Claim_Reference{prop:Req} = True
    ?inv:Claim_Reference{prop:Trn} = 0
    ?inv:Claim_Reference{prop:FontStyle} = font:Bold
    ?INV:Courier_Paid:Prompt:2{prop:FontColor} = -1
    ?INV:Courier_Paid:Prompt:2{prop:Color} = 15066597
    If ?INV:Courier_Paid:2{prop:ReadOnly} = True
        ?INV:Courier_Paid:2{prop:FontColor} = 65793
        ?INV:Courier_Paid:2{prop:Color} = 15066597
    Elsif ?INV:Courier_Paid:2{prop:Req} = True
        ?INV:Courier_Paid:2{prop:FontColor} = 65793
        ?INV:Courier_Paid:2{prop:Color} = 8454143
    Else ! If ?INV:Courier_Paid:2{prop:Req} = True
        ?INV:Courier_Paid:2{prop:FontColor} = 65793
        ?INV:Courier_Paid:2{prop:Color} = 16777215
    End ! If ?INV:Courier_Paid:2{prop:Req} = True
    ?INV:Courier_Paid:2{prop:Trn} = 0
    ?INV:Courier_Paid:2{prop:FontStyle} = font:Bold
    ?INV:Labour_Paid:Prompt:2{prop:FontColor} = -1
    ?INV:Labour_Paid:Prompt:2{prop:Color} = 15066597
    If ?INV:Labour_Paid:2{prop:ReadOnly} = True
        ?INV:Labour_Paid:2{prop:FontColor} = 65793
        ?INV:Labour_Paid:2{prop:Color} = 15066597
    Elsif ?INV:Labour_Paid:2{prop:Req} = True
        ?INV:Labour_Paid:2{prop:FontColor} = 65793
        ?INV:Labour_Paid:2{prop:Color} = 8454143
    Else ! If ?INV:Labour_Paid:2{prop:Req} = True
        ?INV:Labour_Paid:2{prop:FontColor} = 65793
        ?INV:Labour_Paid:2{prop:Color} = 16777215
    End ! If ?INV:Labour_Paid:2{prop:Req} = True
    ?INV:Labour_Paid:2{prop:Trn} = 0
    ?INV:Labour_Paid:2{prop:FontStyle} = font:Bold
    ?INV:Parts_Paid:Prompt:2{prop:FontColor} = -1
    ?INV:Parts_Paid:Prompt:2{prop:Color} = 15066597
    If ?INV:Parts_Paid:2{prop:ReadOnly} = True
        ?INV:Parts_Paid:2{prop:FontColor} = 65793
        ?INV:Parts_Paid:2{prop:Color} = 15066597
    Elsif ?INV:Parts_Paid:2{prop:Req} = True
        ?INV:Parts_Paid:2{prop:FontColor} = 65793
        ?INV:Parts_Paid:2{prop:Color} = 8454143
    Else ! If ?INV:Parts_Paid:2{prop:Req} = True
        ?INV:Parts_Paid:2{prop:FontColor} = 65793
        ?INV:Parts_Paid:2{prop:Color} = 16777215
    End ! If ?INV:Parts_Paid:2{prop:Req} = True
    ?INV:Parts_Paid:2{prop:Trn} = 0
    ?INV:Parts_Paid:2{prop:FontStyle} = font:Bold
    ?INV:Total_Claimed:Prompt{prop:FontColor} = -1
    ?INV:Total_Claimed:Prompt{prop:Color} = 15066597
    If ?inv:Total_Claimed{prop:ReadOnly} = True
        ?inv:Total_Claimed{prop:FontColor} = 65793
        ?inv:Total_Claimed{prop:Color} = 15066597
    Elsif ?inv:Total_Claimed{prop:Req} = True
        ?inv:Total_Claimed{prop:FontColor} = 65793
        ?inv:Total_Claimed{prop:Color} = 8454143
    Else ! If ?inv:Total_Claimed{prop:Req} = True
        ?inv:Total_Claimed{prop:FontColor} = 65793
        ?inv:Total_Claimed{prop:Color} = 16777215
    End ! If ?inv:Total_Claimed{prop:Req} = True
    ?inv:Total_Claimed{prop:Trn} = 0
    ?inv:Total_Claimed{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Invoice',1)
    SolaceViewVars('sav:couriercost',sav:couriercost,'Update_Invoice',1)
    SolaceViewVars('sav:labourcost',sav:labourcost,'Update_Invoice',1)
    SolaceViewVars('sav:partscost',sav:partscost,'Update_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Invoice',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Invoice',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?INV:Invoice_Number:Prompt;  SolaceCtrlName = '?INV:Invoice_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Invoice_Number;  SolaceCtrlName = '?inv:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Account_Number:Prompt;  SolaceCtrlName = '?INV:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Account_Number;  SolaceCtrlName = '?inv:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Batch_Number:Prompt;  SolaceCtrlName = '?INV:Batch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Batch_Number;  SolaceCtrlName = '?inv:Batch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Courier_Paid:Prompt;  SolaceCtrlName = '?INV:Courier_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Courier_Paid;  SolaceCtrlName = '?inv:Courier_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Labour_Paid:Prompt;  SolaceCtrlName = '?INV:Labour_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Labour_Paid;  SolaceCtrlName = '?inv:Labour_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Parts_Paid:Prompt;  SolaceCtrlName = '?INV:Parts_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Parts_Paid;  SolaceCtrlName = '?inv:Parts_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Total:Prompt;  SolaceCtrlName = '?INV:Total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Total;  SolaceCtrlName = '?inv:Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Vat_Rate_Labour:Prompt;  SolaceCtrlName = '?INV:Vat_Rate_Labour:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Vat_Rate_Labour;  SolaceCtrlName = '?inv:Vat_Rate_Labour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Vat_Rate_Parts:Prompt;  SolaceCtrlName = '?INV:Vat_Rate_Parts:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Vat_Rate_Parts;  SolaceCtrlName = '?inv:Vat_Rate_Parts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Vat_Rate_Retail:Prompt;  SolaceCtrlName = '?INV:Vat_Rate_Retail:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Vat_Rate_Retail;  SolaceCtrlName = '?inv:Vat_Rate_Retail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Courier_Cost:Prompt;  SolaceCtrlName = '?JOB:Invoice_Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Courier_Cost;  SolaceCtrlName = '?job:Invoice_Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Labour_Cost:Prompt;  SolaceCtrlName = '?JOB:Invoice_Labour_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Labour_Cost;  SolaceCtrlName = '?job:Invoice_Labour_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Parts_Cost:Prompt;  SolaceCtrlName = '?JOB:Invoice_Parts_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Parts_Cost;  SolaceCtrlName = '?job:Invoice_Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Sub_Total:Prompt;  SolaceCtrlName = '?JOB:Invoice_Sub_Total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Sub_Total;  SolaceCtrlName = '?job:Invoice_Sub_Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Batch_Number:Prompt:2;  SolaceCtrlName = '?INV:Batch_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Batch_Number:2;  SolaceCtrlName = '?INV:Batch_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Claim_Reference:Prompt;  SolaceCtrlName = '?INV:Claim_Reference:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Claim_Reference;  SolaceCtrlName = '?inv:Claim_Reference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Courier_Paid:Prompt:2;  SolaceCtrlName = '?INV:Courier_Paid:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Courier_Paid:2;  SolaceCtrlName = '?INV:Courier_Paid:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Labour_Paid:Prompt:2;  SolaceCtrlName = '?INV:Labour_Paid:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Labour_Paid:2;  SolaceCtrlName = '?INV:Labour_Paid:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Parts_Paid:Prompt:2;  SolaceCtrlName = '?INV:Parts_Paid:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Parts_Paid:2;  SolaceCtrlName = '?INV:Parts_Paid:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Total_Claimed:Prompt;  SolaceCtrlName = '?INV:Total_Claimed:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Total_Claimed;  SolaceCtrlName = '?inv:Total_Claimed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Amending An Invoice'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Invoice')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Invoice')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INV:Invoice_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(inv:Record,History::inv:Record)
  SELF.AddHistoryField(?inv:Invoice_Number,1)
  SELF.AddHistoryField(?inv:Account_Number,5)
  SELF.AddHistoryField(?inv:Batch_Number,14)
  SELF.AddHistoryField(?inv:Courier_Paid,18)
  SELF.AddHistoryField(?inv:Labour_Paid,19)
  SELF.AddHistoryField(?inv:Parts_Paid,20)
  SELF.AddHistoryField(?inv:Total,7)
  SELF.AddHistoryField(?inv:Vat_Rate_Labour,8)
  SELF.AddHistoryField(?inv:Vat_Rate_Parts,9)
  SELF.AddHistoryField(?inv:Vat_Rate_Retail,10)
  SELF.AddHistoryField(?INV:Batch_Number:2,14)
  SELF.AddHistoryField(?inv:Claim_Reference,16)
  SELF.AddHistoryField(?INV:Courier_Paid:2,18)
  SELF.AddHistoryField(?INV:Labour_Paid:2,19)
  SELF.AddHistoryField(?INV:Parts_Paid:2,20)
  SELF.AddHistoryField(?inv:Total_Claimed,17)
  SELF.AddUpdateFile(Access:INVOICE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVOICE.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVOICE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Case inv:invoice_Type
      Of 'SIN'
          Hide(?tab1)
          Hide(?tab3)
          Unhide(?tab2)
          access:jobs.clearkey(job:invoicenumberkey)
          job:invoice_number  = inv:invoice_number
          If access:jobs.tryfetch(job:invoicenumberkey) = Level:Benign
              !Found
              sav:labourcost  = job:invoice_courier_cost
              sav:partscost   = job:invoice_parts_cost
              sav:couriercost = job:invoice_courier_cost
          Else! If access:jobs.tryfetch(job:invoice_number_key) = Level:Benign
              !Error
          End! If access:.tryfetch(job:invoice_number_key) = Level:Benign
      Of 'WAR'
          hide(?tab1)
          hide(?tab2)
          Unhide(?tab3)
  End!Case inv:invoice_Type
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Invoice',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case inv:invoice_Type
      Of 'SIN'
          If job:invoice_courier_cost <> sav:couriercost Or |
              job:invoice_parts_cost <> sav:partscost Or |
              job:invoice_labour_cost <> sav:labourcost
              Case MessageEx('You have amended the invoice costs. The job will be updated with these values.<13,10><13,10>Are you sure you want to amend these values?','ServiceBase 2000',|
                             'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      access:jobs.update()
                  Of 2 ! &No Button
              End!Case MessageEx
          End!job:invoice_labour_cost <> sav:labourcost
  End!Case inv:invoice_Type
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Invoice')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Invoice type and saved values
      !
      !Single Invoice (Chargeable)
      !
      !inv:job_number    = Job Number
      !inv:account_number    = Job Trade Account Number
      !inv:total    = Job Sub Total
      !inv:vat_rate....    = Trade VAT rates
      !
      !
      !Warranty Invoice
      !
      !inv:account_number    = Manufacturer Trade Account Number
      !inv:vat_rate...        = Trade VAT rates
      !inv:batch_number    = Batch Number
      !inv:claim_reference    = Claim Reference Number
      !inv:total_claimed    = Total Claimed
      !inv:courier_paid    = Courier Charge Paid
      !inv:labour_paid        = Labour Charge Paid
      !inv:parts_paid        = Parts Charge Paid
      !inv:jobs_count        = Number Of Jobs Invoiced
      !
      !
      !Multiple Invoice (Chargeable)
      !
      !inv:account_number    = Job Trade Account Number
      !inv:total            = Total of all invoices
      !inv:vat_rate ....    = Trade VAT Rates
      !inv:labour_paid        = Total Labour Paid
      !inv:parts_paid        = Total Parts Paid
      !inv:courier_paid    = Total Courier Paid
      !Invoice Update Type
      Case inv:invoice_type
          Of 'SIN'
              Disable(?inv:batch_number)
          Of 'CHA'
              Disable(?inv:batch_number)
          Of 'WAR'
              ?inv:total:prompt{prop:text} = 'Total Claimed'
      End!Case inv:invoice_type
    OF EVENT:Timer
      job:invoice_sub_total = job:invoice_courier_cost + job:invoice_parts_cost + job:invoice_labour_cost
      Display(?job:invoice_Sub_total)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

