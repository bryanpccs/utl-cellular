

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01001.INC'),ONCE        !Local module procedure declarations
                     END


UpdateJOBS PROCEDURE                                  !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp_RF_Board_IMEI    STRING(20)
ValidateMobileNumber BYTE(0)
MSN_Field_Mask       STRING(30)
temp_255string       STRING(255)
save_joe_id          USHORT,AUTO
save_taf_id          USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
save_cou_id          USHORT,AUTO
tmp:ConsignNo        STRING(30)
tmp:accountnumber    STRING(30)
tmp:OldConsignNo     STRING(30)
tmp:labelerror       LONG
account_number2_temp STRING(30)
tmp:DespatchClose    BYTE(0)
sav:ref_number       LONG
tmp:printjobcard     BYTE(0)
tmp:printjobreceipt  BYTE(0)
tmp:SuspendPrintJobCard BYTE(0)
tmp:PrintJobcardNow  BYTE(0)
save_trb_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
save_cha_id          USHORT,AUTO
sav:path             STRING(255)
print_despatch_note_temp STRING(3)
error_type_temp      STRING(1),DIM(50)
error_queue_temp     QUEUE,PRE(err)
field                STRING(30)
                     END
error_message_temp   STRING(255)
Cancelling_Group     GROUP,PRE()
old_exchange_unit_number_temp REAL
new_exchange_unit_number_temp REAL
old_loan_unit_number_temp REAL
save_moc_id          USHORT,AUTO
new_loan_unit_number_temp REAL
saved_ref_number_temp REAL
                     END
parts_queue_temp     QUEUE,PRE(partmp)
Part_Ref_Number      REAL
Quantity             REAL
Exclude_From_Order   STRING(3)
Pending_Ref_Number   REAL
                     END
check_for_bouncers_temp STRING('0')
save_aud_id          USHORT,AUTO
save_job_id          USHORT,AUTO
saved_esn_temp       STRING(16)
saved_msn_temp       STRING(16)
engineer_sundry_temp STRING(3)
main_store_sundry_temp STRING(3)
engineer_ref_number_temp REAL
main_store_ref_number_temp REAL
engineer_quantity_temp REAL
main_store_quantity_temp REAL
save_ccp_id          USHORT
save_cwp_id          USHORT,AUTO
label_type_temp      BYTE
exchange_accessory_count_temp REAL
accessory_count_temp REAL
Ignore_Chargeable_Charges_Temp STRING('''NO''')
Ignore_Warranty_Charges_Temp STRING('''NO''')
Ignore_Estimate_Charges_Temp STRING('''NO''')
Force_Fault_Codes_Temp STRING('NO {1}')
balance_due_warranty_temp REAL
paid_warranty_temp   REAL
print_label_temp     STRING(3)
Courier_temp         STRING(30)
loan_Courier_temp    STRING(30)
exchange_Courier_temp STRING(30)
engineer_name_temp   STRING(60)
msn_fail_temp        STRING('NO {1}')
esn_fail_temp        STRING('NO {1}')
day_count_temp       REAL
day_number_temp      REAL
date_error_temp      STRING(3)
estimate_ready_temp  STRING(3)
in_repair_temp       STRING(3)
on_test_temp         STRING(3)
qa_passed_temp       STRING(3)
qa_rejected_temp     STRING(3)
qa_second_passed_temp STRING(3)
estimate_accepted_temp STRING(3)
estimate_rejected_temp STRING(3)
Third_Party_Site_temp STRING(30)
Chargeable_Job_temp  STRING(3)
Warranty_Job_Temp    STRING(3)
warranty_charge_type_temp STRING(30)
Partemp_group        GROUP,PRE()
Partemp:part_ref_number REAL
Partemp:pending_ref_number REAL
Partemp:despatch_note_number STRING(30)
Partemp:quantity     LONG
Partemp:purchase_cost REAL
Partemp:sale_cost    REAL
                     END
estimate_temp        STRING(3)
workshop_temp        STRING(3)
location_temp        STRING(30)
repair_type_temp     STRING(30)
warranty_repair_type_temp STRING(30)
model_number_temp    STRING(30)
History_Run_Temp     SHORT
transit_type_temp    STRING(30)
account_number_temp  STRING(30)
ESN_temp             STRING(30)
MSN_temp             STRING(30)
Unit_Type_temp       STRING(30)
Current_Status_Temp  STRING(30)
Order_Number_temp    STRING(30)
Surname_temp         STRING(30)
Date_Completed_temp  DATE
Time_Completed_Temp  TIME
LocalRequest         LONG
LocalResponse        LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
charge_type_Temp     STRING(30)
title_3_temp         STRING(46)
details_temp         STRING(31)
date_booked_temp     STRING(29)
name_temp            STRING(60)
Vat_Total_Temp       REAL
Total_Temp           REAL
engineer_temp        STRING(30)
loan_model_make_temp STRING(60)
loan_esn_temp        STRING(15)
loan_msn_temp        STRING(10)
Loan_Accessories_Temp STRING(30)
Exchange_Model_Make_temp STRING(60)
Exchange_Esn_temp    STRING(15)
Exchange_Msn_temp    STRING(10)
Exchange_Accessories_Temp STRING(30)
balance_due_temp     REAL
Accessories_Temp     STRING(30)
location_type_temp   STRING(30)
vat_total_estimate_temp REAL
total_estimate_temp  REAL
vat_total_warranty_temp REAL
Total_warranty_temp  REAL
chargeable_part_total_temp REAL
warranty_part_total_temp REAL
job_time_remaining_temp STRING(20)
status_time_remaining_temp STRING(20)
ordered_temp         STRING(18)
CPCSExecutePopUp     BYTE
ProcedureRunning     BYTE
warranty_ordered_temp STRING(18)
vat_estimate_temp    REAL
vat_chargeable_temp  REAL
vat_warranty_temp    REAL
paid_chargeable_temp REAL
estimate_parts_cost_temp REAL
show_booking_temp    STRING('NO {1}')
trade_account_string_temp STRING(60)
customer_name_string_temp STRING(60)
Exchange_Consignment_Number_temp STRING(30)
Exchange_Despatched_temp DATE
Exchange_Despatched_User_temp STRING(3)
Exchange_Despatch_Number_temp REAL
Loan_Consignment_Number_temp STRING(30)
Loan_Despatched_temp DATE
Loan_Despatched_User_temp STRING(3)
Loan_Despatch_Number_temp REAL
Date_Despatched_temp DATE
Despatch_Number_temp REAL
Despatch_User_temp   STRING(3)
Consignment_Number_temp STRING(30)
third_party_returned_temp DATE
tmp:FaultDescription STRING(255)
tmp:EngineerNotes    STRING(255)
tmp:InvoiceText      STRING(255)
SaveGroup            GROUP,PRE(sav)
ChargeType           STRING(30)
WarrantyChargeType   STRING(20)
RepairType           STRING(30)
WarrantyRepairType   STRING(30)
ChargeableJob        STRING(3)
WarrantyJob          STRING(3)
                     END
ChangedGroup         GROUP,PRE(sav1)
cct                  BYTE(0)
wct                  BYTE(0)
crt                  BYTE(0)
wrt                  BYTE(0)
chj                  BYTE(0)
wrj                  BYTE(0)
                     END
tmp:PartQueue        QUEUE,PRE(tmp)
PartNumber           STRING(30)
                     END
tmp:ParcelLineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
tmp:IncomingIMEI     STRING(30)
tmp:IncomingMSN      STRING(30)
tmp:ExchangeIMEI     STRING(30)
tmp:ExchangeMSN      STRING(30)
tmp:EstimateDetails  STRING(255)
tmp:SkillLevel       LONG
tmp:one              BYTE(1)
tmp:DateAllocated    DATE
tmp:FaultyUnit       BYTE(0)
tmp:BERConsignmentNumber STRING(30)
tmp:Network          STRING(30)
tmp:zero             BYTE(0)
tmp:InWorkshopDate   DATE
tmp:InWorkshopTime   TIME
tmp:SaveAccessory    STRING(255),STATIC
SaveAccessoryQueue   QUEUE,PRE(savaccq)
Accessory            STRING(30)
Type                 BYTE(0)
                     END
partpntstore         USHORT
warpartpntstore      USHORT
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?job:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:8 QUEUE                           !Queue declaration for browse/combo box using ?job:Loan_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:9 QUEUE                           !Queue declaration for browse/combo box using ?job:Exchange_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:11 QUEUE                          !Queue declaration for browse/combo box using ?job:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:12 QUEUE                          !Queue declaration for browse/combo box using ?job:Turnaround_Time
tur:Turnaround_Time    LIKE(tur:Turnaround_Time)      !List box control field - type derived from field
tur:Days               LIKE(tur:Days)                 !List box control field - type derived from field
tur:Hours              LIKE(tur:Hours)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?job:Incoming_Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?job:Colour
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
moc:Record_Number      LIKE(moc:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?job:LoaService
cit:Service            LIKE(cit:Service)              !List box control field - type derived from field
cit:Description        LIKE(cit:Description)          !List box control field - type derived from field
cit:RefNumber          LIKE(cit:RefNumber)            !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:5 QUEUE                           !Queue declaration for browse/combo box using ?job:ExcService
cit:Service            LIKE(cit:Service)              !List box control field - type derived from field
cit:Description        LIKE(cit:Description)          !List box control field - type derived from field
cit:RefNumber          LIKE(cit:RefNumber)            !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:6 QUEUE                           !Queue declaration for browse/combo box using ?job:JobService
cit:Service            LIKE(cit:Service)              !List box control field - type derived from field
cit:Description        LIKE(cit:Description)          !List box control field - type derived from field
cit:RefNumber          LIKE(cit:RefNumber)            !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:ProductCode
prd:ProductCode        LIKE(prd:ProductCode)          !List box control field - type derived from field
prd:RecordNumber       LIKE(prd:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB40::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB41::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB8::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB17::View:FileDropCombo VIEW(TURNARND)
                       PROJECT(tur:Turnaround_Time)
                       PROJECT(tur:Days)
                       PROJECT(tur:Hours)
                     END
FDCB10::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB25::View:FileDropCombo VIEW(MODELCOL)
                       PROJECT(moc:Colour)
                       PROJECT(moc:Record_Number)
                     END
FDCB16::View:FileDropCombo VIEW(CITYSERV)
                       PROJECT(cit:Service)
                       PROJECT(cit:Description)
                       PROJECT(cit:RefNumber)
                     END
FDCB24::View:FileDropCombo VIEW(CITYSERV)
                       PROJECT(cit:Service)
                       PROJECT(cit:Description)
                       PROJECT(cit:RefNumber)
                     END
FDCB30::View:FileDropCombo VIEW(CITYSERV)
                       PROJECT(cit:Service)
                       PROJECT(cit:Description)
                       PROJECT(cit:RefNumber)
                     END
FDCB46::View:FileDropCombo VIEW(PRODCODE)
                       PROJECT(prd:ProductCode)
                       PROJECT(prd:RecordNumber)
                     END
BRW12::View:Browse   VIEW(PARTS)
                       PROJECT(par:Part_Number)
                       PROJECT(par:Description)
                       PROJECT(par:Order_Number)
                       PROJECT(par:Quantity)
                       PROJECT(par:Part_Ref_Number)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
par:Part_Number        LIKE(par:Part_Number)          !List box control field - type derived from field
par:Part_Number_NormalFG LONG                         !Normal forground color
par:Part_Number_NormalBG LONG                         !Normal background color
par:Part_Number_SelectedFG LONG                       !Selected forground color
par:Part_Number_SelectedBG LONG                       !Selected background color
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Description_NormalFG LONG                         !Normal forground color
par:Description_NormalBG LONG                         !Normal background color
par:Description_SelectedFG LONG                       !Selected forground color
par:Description_SelectedBG LONG                       !Selected background color
par:Order_Number       LIKE(par:Order_Number)         !List box control field - type derived from field
par:Order_Number_NormalFG LONG                        !Normal forground color
par:Order_Number_NormalBG LONG                        !Normal background color
par:Order_Number_SelectedFG LONG                      !Selected forground color
par:Order_Number_SelectedBG LONG                      !Selected background color
ordered_temp           LIKE(ordered_temp)             !List box control field - type derived from local data
ordered_temp_NormalFG  LONG                           !Normal forground color
ordered_temp_NormalBG  LONG                           !Normal background color
ordered_temp_SelectedFG LONG                          !Selected forground color
ordered_temp_SelectedBG LONG                          !Selected background color
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Quantity_NormalFG  LONG                           !Normal forground color
par:Quantity_NormalBG  LONG                           !Normal background color
par:Quantity_SelectedFG LONG                          !Selected forground color
par:Quantity_SelectedBG LONG                          !Selected background color
chargeable_part_total_temp LIKE(chargeable_part_total_temp) !List box control field - type derived from local data
chargeable_part_total_temp_NormalFG LONG              !Normal forground color
chargeable_part_total_temp_NormalBG LONG              !Normal background color
chargeable_part_total_temp_SelectedFG LONG            !Selected forground color
chargeable_part_total_temp_SelectedBG LONG            !Selected background color
par:Part_Ref_Number    LIKE(par:Part_Ref_Number)      !List box control field - type derived from field
par:Part_Ref_Number_NormalFG LONG                     !Normal forground color
par:Part_Ref_Number_NormalBG LONG                     !Normal background color
par:Part_Ref_Number_SelectedFG LONG                   !Selected forground color
par:Part_Ref_Number_SelectedBG LONG                   !Selected background color
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW21::View:Browse   VIEW(JOBSTAGE)
                       PROJECT(jst:Job_Stage)
                       PROJECT(jst:Date)
                       PROJECT(jst:Time)
                       PROJECT(jst:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
jst:Job_Stage          LIKE(jst:Job_Stage)            !List box control field - type derived from field
jst:Date               LIKE(jst:Date)                 !List box control field - type derived from field
jst:Time               LIKE(jst:Time)                 !List box control field - type derived from field
jst:Ref_Number         LIKE(jst:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW77::View:Browse   VIEW(ESTPARTS)
                       PROJECT(epr:Part_Number)
                       PROJECT(epr:Description)
                       PROJECT(epr:Quantity)
                       PROJECT(epr:record_number)
                       PROJECT(epr:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
epr:Part_Number        LIKE(epr:Part_Number)          !List box control field - type derived from field
epr:Description        LIKE(epr:Description)          !List box control field - type derived from field
epr:Quantity           LIKE(epr:Quantity)             !List box control field - type derived from field
estimate_parts_cost_temp LIKE(estimate_parts_cost_temp) !List box control field - type derived from local data
epr:record_number      LIKE(epr:record_number)        !Primary key field - type derived from field
epr:Ref_Number         LIKE(epr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW79::View:Browse   VIEW(WARPARTS)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Order_Number)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:Part_Ref_Number)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:4
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Part_Number_NormalFG LONG                         !Normal forground color
wpr:Part_Number_NormalBG LONG                         !Normal background color
wpr:Part_Number_SelectedFG LONG                       !Selected forground color
wpr:Part_Number_SelectedBG LONG                       !Selected background color
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Description_NormalFG LONG                         !Normal forground color
wpr:Description_NormalBG LONG                         !Normal background color
wpr:Description_SelectedFG LONG                       !Selected forground color
wpr:Description_SelectedBG LONG                       !Selected background color
wpr:Order_Number       LIKE(wpr:Order_Number)         !List box control field - type derived from field
wpr:Order_Number_NormalFG LONG                        !Normal forground color
wpr:Order_Number_NormalBG LONG                        !Normal background color
wpr:Order_Number_SelectedFG LONG                      !Selected forground color
wpr:Order_Number_SelectedBG LONG                      !Selected background color
warranty_ordered_temp  LIKE(warranty_ordered_temp)    !List box control field - type derived from local data
warranty_ordered_temp_NormalFG LONG                   !Normal forground color
warranty_ordered_temp_NormalBG LONG                   !Normal background color
warranty_ordered_temp_SelectedFG LONG                 !Selected forground color
warranty_ordered_temp_SelectedBG LONG                 !Selected background color
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
wpr:Quantity_NormalFG  LONG                           !Normal forground color
wpr:Quantity_NormalBG  LONG                           !Normal background color
wpr:Quantity_SelectedFG LONG                          !Selected forground color
wpr:Quantity_SelectedBG LONG                          !Selected background color
warranty_part_total_temp LIKE(warranty_part_total_temp) !List box control field - type derived from local data
warranty_part_total_temp_NormalFG LONG                !Normal forground color
warranty_part_total_temp_NormalBG LONG                !Normal background color
warranty_part_total_temp_SelectedFG LONG              !Selected forground color
warranty_part_total_temp_SelectedBG LONG              !Selected background color
wpr:Part_Ref_Number    LIKE(wpr:Part_Ref_Number)      !List box control field - type derived from field
wpr:Part_Ref_Number_NormalFG LONG                     !Normal forground color
wpr:Part_Ref_Number_NormalBG LONG                     !Normal background color
wpr:Part_Ref_Number_SelectedFG LONG                   !Selected forground color
wpr:Part_Ref_Number_SelectedBG LONG                   !Selected background color
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::job:Record  LIKE(job:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Main Frame'),AT(,,668,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdateJOBS'),ALRT(F10Key),TILED,TIMER(100),GRAY,DOUBLE
                       PROMPT(''),AT(344,8,164,12),USE(?Batch_Number_Text),RIGHT,FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                       PROMPT(''),AT(8,8,224,12),USE(?Title_Text),FONT(,10,COLOR:Navy,FONT:bold)
                       SHEET,AT(4,4,508,188),USE(?Sheet4),BELOW,SPREAD
                         TAB,USE(?Booking1_Tab)
                           PROMPT('Auto Search'),AT(8,20),USE(?JOB:Auto_Search:Prompt),TRN
                           ENTRY(@s30),AT(84,20,70,10),USE(job:Auto_Search),FONT('Tahoma',8,,FONT:bold),ALRT(AltF12),UPR
                           BUTTON('History'),AT(156,20,48,10),USE(?History_Button),SKIP,LEFT
                           ENTRY(@n6),AT(204,20,16,10),USE(job:Last_Repair_Days),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR
                           PROMPT('Days From Last Repair'),AT(224,20,47,17),USE(?JOB:Last_Repair_Days:Prompt),TRN
                           PROMPT('Trade Account Number'),AT(8,36,56,20),USE(?Job:Account_Number:Prompt)
                           PROMPT('Trade Account Status'),AT(84,48,136,8),USE(?Trade_Account_Status),HIDE,FONT(,,COLOR:Red,FONT:bold)
                           ENTRY(@s15),AT(84,36,136,10),USE(job:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),REQ,UPR
                           BUTTON,AT(224,36,10,10),USE(?LookupTradeAccount),SKIP,ICON('list3.ico')
                           PROMPT('Order Number'),AT(8,60),USE(?job:order_number:prompt),TRN
                           ENTRY(@s30),AT(84,60,136,10),USE(job:Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(AltF12),UPR
                           GROUP,AT(8,72,220,20),USE(?NameGroup),HIDE
                             PROMPT('Title'),AT(84,72),USE(?Title:prompt),FONT(,7,,)
                             PROMPT('Initial'),AT(112,72),USE(?Initial:prompt),FONT(,7,,)
                             PROMPT('Surname'),AT(128,72),USE(?surname:prompt),TRN,FONT(,7,,)
                             PROMPT('Customer Name'),AT(8,80,60,12),USE(?job:title:prompt),TRN
                             ENTRY(@s4),AT(84,80,24,10),USE(job:Title),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s1),AT(112,80,12,10),USE(job:Initial),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(128,80,92,10),USE(job:Surname),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           END
                           PROMPT('Initial Transit Type'),AT(8,96),USE(?Prompt49),TRN
                           ENTRY(@s30),AT(84,96,136,10),USE(job:Transit_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(224,96,10,10),USE(?LookupTransitType),SKIP,ICON('list3.ico')
                           PROMPT('Job Site'),AT(8,128),USE(?job:location_type:prompt),TRN,HIDE
                           BUTTON('No Label'),AT(236,96,44,12),USE(?no_Label_button),HIDE,LEFT
                           CHECK('Workshop'),AT(84,128),USE(job:Workshop),HIDE,VALUE('YES','NO')
                           GROUP,AT(4,140,236,20),USE(?Location_Group),HIDE
                             PROMPT('Internal Location'),AT(8,144),USE(?job:location:prompt),TRN
                             ENTRY(@s30),AT(84,144,136,10),USE(job:Location),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey)
                             BUTTON,AT(224,144,10,10),USE(?LookupLocation),SKIP,ICON('list3.ico')
                           END
                           PROMPT('At 3rd Party Site'),AT(176,128),USE(?Third_party_message),HIDE,FONT(,,COLOR:Green,FONT:bold)
                           ENTRY(@s30),AT(88,180,136,10),USE(job:Third_Party_Site),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           TEXT,AT(372,20,136,10),USE(job:ESN),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('MSN'),AT(284,32),USE(?JOB:MSN:Prompt),TRN,HIDE
                           ENTRY(@s16),AT(372,32,136,10),USE(job:MSN),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Product Code'),AT(284,44),USE(?job:ProductCode:Prompt),TRN
                           COMBO(@s30),AT(372,44,136,10),USE(job:ProductCode),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Model Number'),AT(284,56),USE(?JOB:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(372,56,72,10),USE(job:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(448,56,10,10),USE(?LookupModelNumber),SKIP,ICON('list3.ico')
                           ENTRY(@s30),AT(460,56,48,10),USE(job:Manufacturer),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Unit Type'),AT(284,68,76,12),USE(?Prompt:Job:Unit_Type),TRN
                           COMBO(@s30),AT(372,68,84,10),USE(job:Unit_Type),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           COMBO(@s30),AT(460,68,48,10),USE(job:Colour),IMM,HIDE,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                           BUTTON,AT(353,84,12,10),USE(?accessory_button),ICON('list3.ico')
                           PROMPT('Accessories'),AT(284,84),USE(?Accessories_Temp:Prompt),TRN
                           ENTRY(@s30),AT(372,84,70,10),USE(Accessories_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('DOP'),AT(444,84),USE(?JOB:DOP:Prompt),TRN
                           ENTRY(@d6),AT(460,84,48,10),USE(job:DOP),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Description'),AT(284,96),USE(?Prompt47),TRN
                           BUTTON('Fault Description Text'),AT(372,96,136,12),USE(?Fault_Description_Text_Button),LEFT,ICON('list3.ico')
                           PROMPT('Network'),AT(9,112),USE(?tmp:Network:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,112,136,10),USE(tmp:Network,,?tmp:Network:2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('N'),TIP('N'),UPR
                           BUTTON,AT(224,112,10,10),USE(?LookupNetwork:2),SKIP,ICON('List3.ico')
                           PROMPT('I.M.E.I. Number'),AT(284,20),USE(?job:esn:prompt),TRN
                           PROMPT('Mobile Number'),AT(284,112),USE(?JOB:Mobile_Number:Prompt),TRN,HIDE
                           ENTRY(@s15),AT(372,112,136,10),USE(job:Mobile_Number),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           STRING(@d6),AT(136,124),USE(tmp:InWorkshopDate),TRN,HIDE,FONT(,,,FONT:bold)
                           STRING(@t1),AT(136,132),USE(tmp:InWorkshopTime),TRN,HIDE,FONT(,,,FONT:bold)
                           PROMPT('Job Turnaround Time'),AT(284,128),USE(?Prompt101)
                           COMBO(@s30),AT(372,128,136,10),USE(job:Turnaround_Time),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('100L(2)@s30@31L(2)@p<<<<# Dysp@8L(2)@p<<# Hrsp@'),DROP(10,164),FROM(Queue:FileDropCombo:12)
                           PROMPT('3rd Party Service Cen.'),AT(12,180),USE(?JOB:Third_Party_Site:Prompt),TRN,HIDE
                           PROMPT('Charge Type'),AT(372,140),USE(?job:charge_type:prompt),TRN,FONT(,7,,FONT:underline)
                           CHECK('Chargeable Job'),AT(284,148,68,12),USE(job:Chargeable_Job),LEFT,VALUE('YES','NO')
                           ENTRY(@s30),AT(372,148,124,10),USE(job:Charge_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),ALRT(EnterKey),REQ,UPR
                           BUTTON,AT(500,148,10,10),USE(?LookupChargeType),SKIP,ICON('List3.ico')
                           PROMPT('Despatched'),AT(8,160),USE(?JOB:Third_Party_Despatch_Date:Prompt),HIDE
                           ENTRY(@d6b),AT(84,160,64,10),USE(job:ThirdPartyDateDesp),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Warranty Job'),AT(284,160,68,12),USE(job:Warranty_Job),LEFT,VALUE('YES','NO')
                           ENTRY(@s30),AT(372,160,124,10),USE(job:Warranty_Charge_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(500,160,10,10),USE(?LookupWarrantyChargeType),SKIP,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('List3.ico')
                           ENTRY(@d6b),AT(84,172,64,10),USE(job:Date_Paid),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Returned'),AT(8,172),USE(?job:date_paid:prompt),HIDE
                           CHECK('Estimate Required'),AT(284,176,88,12),USE(job:Estimate),HIDE,LEFT,VALUE('YES','NO')
                           ENTRY(@n14.2),AT(416,176,32,12),USE(job:Estimate_If_Over),HIDE,FONT(,,,FONT:bold)
                           CHECK('Accepted'),AT(460,172),USE(job:Estimate_Accepted),HIDE,VALUE('YES','NO')
                           CHECK('Rejected'),AT(460,180),USE(job:Estimate_Rejected),HIDE,VALUE('YES','NO')
                           STRING('If Over'),AT(388,176),USE(?job:Estimate_If_Over:prompt),HIDE
                         END
                         TAB,USE(?Booking2_Tab)
                           GROUP,AT(76,16,160,156),USE(?Group9),BOXED
                             STRING(@s60),AT(80,24),USE(trade_account_string_temp),FONT(,,,FONT:bold)
                             STRING(@s30),AT(80,36),USE(job:Order_Number,,?JOB:Order_Number:2),FONT(,,,FONT:bold)
                             STRING(@s60),AT(80,48),USE(customer_name_string_temp),HIDE,FONT(,,,FONT:bold)
                             STRING(@s30),AT(80,68),USE(job:Transit_Type,,?JOB:Transit_Type:2),FONT(,,,FONT:bold)
                             STRING(@s30),AT(80,100),USE(tmp:Network),FONT(,,,FONT:bold)
                             BUTTON,AT(204,100,10,10),USE(?LookupNetwork),SKIP,ICON('List3.ico')
                             STRING(@s3),AT(80,88),USE(job:Workshop,,?JOB:Workshop:2),FONT(,,,FONT:bold)
                             STRING(@s30),AT(80,112),USE(job:Location,,?JOB:Location:2),HIDE,FONT(,,,FONT:bold)
                             STRING(@s30),AT(80,124),USE(job:Third_Party_Site,,?JOB:Third_Party_Site:2),HIDE,FONT(,,,FONT:bold)
                             STRING(@s30),AT(80,136),USE(job:Special_Instructions,,?JOB:Special_Instructions:2),HIDE,FONT(,,,FONT:bold)
                             PROMPT('${30}'),AT(80,156),USE(?StatusWarning),TRN,HIDE,FONT(,14,COLOR:Red,)
                           END
                           GROUP,AT(348,16,160,156),USE(?Group9:2),BOXED
                             TEXT,AT(352,48,80,8),USE(job:ESN,,?job:ESN:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(440,48,64,8),USE(job:MSN,,?job:MSN:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                             TEXT,AT(352,60,80,8),USE(job:Model_Number,,?job:Model_Number:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(440,60,64,8),USE(job:ProductCode,,?job:ProductCode:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                             TEXT,AT(440,84,64,8),USE(job:Colour,,?job:Colour:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                             TEXT,AT(352,96,80,8),USE(job:Mobile_Number,,?job:Mobile_Number:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             STRING(@d6),AT(440,96),USE(job:DOP,,?job:DOP:2),FONT(,,,FONT:bold)
                             TEXT,AT(352,108,152,8),USE(job:Turnaround_Time,,?job:Turnaround_Time:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(352,120,152,8),USE(job:Charge_Type,,?job:Charge_Type:2),SKIP,TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(352,132,152,8),USE(job:Warranty_Charge_Type,,?job:Warranty_Charge_Type:2),SKIP,TRN,HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(352,144,152,8),USE(tmp:EstimateDetails),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(352,72,80,8),USE(job:Manufacturer,,?job:Manufacturer:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             PROMPT('SIM Number:'),AT(440,72),USE(?SIMNumber),HIDE
                             TEXT,AT(440,72,80,8),USE(jobe:SIMNumber),SKIP,TRN,HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY,MSG('SIM Number')
                             TEXT,AT(352,24,80,8),USE(tmp:IncomingIMEI),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(440,24,64,8),USE(tmp:IncomingMSN),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                             TEXT,AT(352,36,80,8),USE(tmp:ExchangeIMEI),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             TEXT,AT(440,36,64,8),USE(tmp:ExchangeMSN),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                             BUTTON,AT(452,72,10,10),USE(?LookupSIMNumber),SKIP,HIDE,ICON('List3.ico')
                             TEXT,AT(352,84,80,8),USE(job:Unit_Type,,?job:Unit_Type:2),SKIP,TRN,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver),READONLY
                             BUTTON('Accessories'),AT(352,156,76,12),USE(?accessory_button:2),LEFT,ICON('list3.ico')
                             BUTTON('Fault Description'),AT(428,156,76,12),USE(?Fault_Description_Text_Button:2),LEFT,ICON('list3.ico')
                           END
                           PROMPT('Trade Account:'),AT(8,24),USE(?Prompt107)
                           PROMPT('Final'),AT(264,48),USE(?Final)
                           PROMPT('Incoming'),AT(264,24),USE(?Incoming:3)
                           PROMPT('IMEI / MSN:'),AT(308,24),USE(?IMEIMSN)
                           PROMPT('Exchanged'),AT(264,36),USE(?Exchanged)
                           PROMPT('IMEI / MSN:'),AT(308,36),USE(?IMEIMSN:2)
                           PROMPT('Order Number:'),AT(8,36),USE(?Prompt108)
                           PROMPT('Model No / Product Code:'),AT(264,60),USE(?ModelNumberProductCode)
                           PROMPT('Manufacturer:'),AT(264,72),USE(?Manufacturer)
                           PROMPT('IMEI / MSN:'),AT(308,48),USE(?IMEIMSN:3)
                           PROMPT('Customer Name:'),AT(8,48),USE(?Customer_Name_String_temp:Prompt),HIDE
                           PROMPT('Initial Transit Type:'),AT(8,68),USE(?Prompt110)
                           PROMPT('Job Turnaround Time'),AT(264,108),USE(?JobTurnaroundTime)
                           PROMPT('Unit Type / Colour:'),AT(264,84),USE(?UnitTypeColour)
                           PROMPT('Mobile No / D.O.P.'),AT(264,96),USE(?MobileNumberDOP)
                           PROMPT('In Workshop:'),AT(8,88),USE(?Prompt111)
                           PROMPT('Network'),AT(8,100),USE(?tmp:Network:Prompt)
                           PROMPT('Internal Location:'),AT(8,112),USE(?location_string),HIDE
                           PROMPT('3rd Party Site:'),AT(8,124),USE(?third_party_string),HIDE
                           PROMPT('Chargeable Charge Type:'),AT(264,120),USE(?chargeable_Type_string),HIDE
                           PROMPT('Special Instructions:'),AT(8,136),USE(?special_instructions_string),HIDE
                           PROMPT('Warranty Charge Type:'),AT(264,132),USE(?warranty_charge_Type_String),HIDE
                           PROMPT('Estimate:'),AT(264,144),USE(?EstimatePrompt),HIDE
                           PROMPT(''),AT(8,176,208,12),USE(?BouncerText),HIDE,FONT(,10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           CHECK('Amend Details'),AT(8,152,68,20),USE(show_booking_temp),VALUE('YES','NO')
                           PROMPT('JOB COMPLETED'),AT(224,176),USE(?jobcompleted),HIDE,FONT(,14,COLOR:Green,FONT:bold)
                           PROMPT(''),AT(348,176,160,12),USE(?Exchanged_Title),RIGHT,FONT(,10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(4,196,508,120),USE(?Sheet2),SPREAD
                         TAB('A&ddress Details'),USE(?Tab4)
                           STRING('Customer Address'),AT(84,212),USE(?String6),FONT(,8,COLOR:Navy,FONT:bold)
                           PROMPT('Company Name'),AT(8,224),USE(?Prompt128)
                           STRING(@s30),AT(84,224),USE(job:Company_Name),FONT(,,,FONT:bold)
                           STRING(@s10),AT(84,260),USE(job:Postcode,,?job:Postcode:2),FONT(,,,FONT:bold)
                           STRING(@s15),AT(152,272),USE(job:Telephone_Number),FONT(,,,FONT:bold)
                           STRING(@s15),AT(152,280),USE(job:Fax_Number),FONT(,,,FONT:bold)
                           STRING('Email Address'),AT(84,288),USE(?String21:4)
                           STRING(@s255),AT(84,296),USE(jobe:EndUserEmailAddress),FONT(,,,FONT:bold)
                           STRING('Fax Number'),AT(84,280),USE(?String21:3)
                           STRING(@s30),AT(84,236),USE(job:Address_Line1),FONT(,,,FONT:bold)
                           STRING(@s30),AT(84,244),USE(job:Address_Line2),FONT(,,,FONT:bold)
                           PROMPT('Address'),AT(8,236),USE(?JOB:Address:Prompt),TRN
                           STRING(@s30),AT(84,252),USE(job:Address_Line3),FONT(,,,FONT:bold)
                           BUTTON('Amend Addresses'),AT(8,324,64,16),USE(?AmendAddresses),LEFT,ICON('auditsm.gif')
                           STRING('Contact Numbers'),AT(8,272),USE(?String21)
                           STRING('Telephone Number'),AT(84,272),USE(?String21:2)
                           GROUP,AT(224,216,141,102),USE(?Collection_Address_Group),HIDE
                             STRING('Collection Address'),AT(228,212),USE(?String7),FONT(,8,COLOR:Navy,FONT:bold)
                             STRING(@s30),AT(228,224),USE(job:Company_Name_Collection),FONT(,,,FONT:bold)
                             STRING(@s30),AT(228,236),USE(job:Address_Line1_Collection),FONT(,,,FONT:bold)
                             STRING(@s30),AT(228,244),USE(job:Address_Line2_Collection),FONT(,,,FONT:bold)
                             STRING(@s30),AT(228,252),USE(job:Address_Line3_Collection),FONT(,,,FONT:bold)
                             STRING(@s10),AT(228,260),USE(job:Postcode_Collection),FONT(,,,FONT:bold)
                             STRING(@s15),AT(228,272),USE(job:Telephone_Collection),FONT(,,,FONT:bold)
                             TEXT,AT(228,288,132,24),USE(jbn:Collection_Text),SKIP,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                             BUTTON('Collection Text'),AT(292,276,72,12),USE(?Collection_Text_Button),LEFT,ICON('text.ico')
                           END
                           GROUP,AT(368,216,141,102),USE(?delivery_Address_Group)
                             STRING('Delivery Address'),AT(372,212),USE(?String8),FONT(,8,COLOR:Navy,FONT:bold)
                             STRING(@s30),AT(372,224),USE(job:Company_Name_Delivery),FONT(,,,FONT:bold)
                             STRING(@s30),AT(372,236),USE(job:Address_Line1_Delivery),FONT(,,,FONT:bold)
                             STRING(@s30),AT(372,244),USE(job:Address_Line2_Delivery),FONT(,,,FONT:bold)
                             STRING(@s30),AT(372,252),USE(job:Address_Line3_Delivery),FONT(,,,FONT:bold)
                             STRING(@s10),AT(372,260),USE(job:Postcode_Delivery),FONT(,,,FONT:bold)
                             STRING(@s15),AT(372,272),USE(job:Telephone_Delivery),FONT(,,,FONT:bold)
                             TEXT,AT(372,288,132,24),USE(jbn:Delivery_Text),SKIP,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                             BUTTON('Delivery Text'),AT(436,276,72,12),USE(?Delivery_Text_Button),LEFT,ICON('text.ico')
                           END
                         END
                         TAB('En&gineering Details'),USE(?Tab5)
                           BUTTON,AT(68,212,10,10),USE(?Lookup_Engineer),ICON('list3.ico')
                           PROMPT('Engineer'),AT(8,212),USE(?Job:Engineer:Prompt)
                           GROUP,AT(0,232,236,20),USE(?Repair_Type_Chargeable_Group),HIDE
                             PROMPT('Chargeable Type'),AT(8,239),USE(?Prompt105)
                             ENTRY(@s30),AT(84,240,136,10),USE(job:Repair_Type),FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                             BUTTON,AT(224,240,10,10),USE(?LookupChargeableChargeType),SKIP,ICON('List3.ico')
                           END
                           PROMPT('Repair Type'),AT(84,232),USE(?Job:Repair_Type:Prompt),TRN,FONT(,7,,FONT:underline)
                           PROMPT('Engineers Notes'),AT(8,272),USE(?JOB:Engineers_Notes:Prompt)
                           BUTTON,AT(68,272,10,10),USE(?Engineers_Notes_Button),LEFT,ICON('list3.ico')
                           TEXT,AT(84,272,136,40),USE(jbn:Engineers_Notes),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Repair Completed'),AT(336,292),USE(?Prompt131),TRN
                           BUTTON('Repair History'),AT(8,324,72,16),USE(?Button62),HIDE,LEFT,ICON(ICON:Print1)
                           PROMPT('Remove Parts && Invoice Text'),AT(460,268,52,16),USE(?Prompt125:2)
                           PROMPT('Allocate Common Fault'),AT(360,268,52,16),USE(?Prompt125)
                           BUTTON('Remove Parts && Invoice Text'),AT(428,268,20,16),USE(?RemoveParts),LEFT,ICON('Bin.gif')
                           BUTTON('Allocate Engineer'),AT(80,324,72,16),USE(?AllocateEngineer),LEFT,ICON('MenWork2.gif')
                           BUTTON('Use Common Fault'),AT(336,268,20,16),USE(?Common_Fault),LEFT,ICON('fault.gif')
                           PROMPT('Repair Status'),AT(284,212),USE(?Prompt85)
                           CHECK('Estimate Ready'),AT(336,212,60,12),USE(job:Estimate_Ready),HIDE,VALUE('YES','NO')
                           BUTTON('Eng History'),AT(10,224,68,12),USE(?Button59),LEFT,ICON('History2.gif')
                           STRING(@s2),AT(120,224),USE(tmp:SkillLevel),TRN,FONT(,,,FONT:bold)
                           PROMPT('Job Level:'),AT(132,224),USE(?JobLevel),TRN
                           STRING(@s2),AT(168,224),USE(jobe:SkillLevel),TRN,FONT(,,,FONT:bold)
                           PROMPT('Allocated:'),AT(180,224),USE(?Prompt126:4),TRN
                           STRING(@s30),AT(84,212),USE(engineer_name_temp),TRN,FONT(,,,FONT:bold)
                           STRING(@d6b),AT(216,224),USE(tmp:DateAllocated),FONT(,,,FONT:bold)
                           PROMPT('Eng Level:'),AT(84,224),USE(?Prompt126:2),TRN
                           GROUP,AT(4,252,236,20),USE(?Repair_Type_Warranty_Group),HIDE
                             PROMPT('Warranty Type'),AT(8,256),USE(?JOB:Repair_Type_Warranty:Prompt),TRN
                             ENTRY(@s30),AT(84,256,136,10),USE(job:Repair_Type_Warranty),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                             BUTTON,AT(224,256,10,10),USE(?LookupWarrantyRepairType),SKIP,ICON('List3.ico')
                           END
                           CHECK('In Repair'),AT(336,228),USE(job:In_Repair),VALUE('YES','NO')
                           CHECK('On Test'),AT(428,228),USE(job:On_Test),VALUE('YES','NO')
                           STRING(@d6b),AT(336,240),USE(job:Date_In_Repair,,?job:Date_In_Repair:2)
                           STRING(@t1b),AT(388,240),USE(job:Time_In_Repair,,?job:Time_In_Repair:2)
                           STRING(@d6b),AT(424,240),USE(job:Date_On_Test)
                           STRING(@t1b),AT(476,240),USE(job:Time_On_Test)
                           PROMPT('Date Completed'),AT(428,292),USE(?Prompt100),TRN
                           PANEL,AT(244,296,72,16),USE(?Panel8),FILL(COLOR:Lime)
                           BUTTON('Complete Job [F10]'),AT(244,296,72,16),USE(?Today_Button),TRN,LEFT,ICON('fixer.gif')
                           ENTRY(@D6b),AT(424,300,52,10),USE(job:Date_Completed),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@t1),AT(476,300,32,10),USE(job:Time_Completed),SKIP,FONT(,,,FONT:bold),READONLY
                           ENTRY(@d6B),AT(336,300,52,10),USE(jobe:CompleteRepairDate),SKIP,FONT(,,,FONT:bold),READONLY
                           ENTRY(@t1),AT(388,300,32,10),USE(jobe:CompleteRepairTime),SKIP,FONT(,,,FONT:bold),READONLY
                           GROUP,AT(272,252,240,12),USE(?QA_Group),HIDE
                             PROMPT('QA Status'),AT(284,252),USE(?Prompt99)
                             CHECK('QA Passed'),AT(336,252),USE(job:QA_Passed),VALUE('YES','NO')
                             ENTRY(@d6b),AT(424,252,52,10),USE(job:Date_QA_Passed),SKIP,FONT(,,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),READONLY
                             ENTRY(@t1b),AT(476,252,32,10),USE(job:Time_QA_Passed),SKIP,FONT(,,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),READONLY
                           END
                         END
                         TAB('E&stimate Parts'),USE(?Estimate_Details_Tab),HIDE
                           LIST,AT(84,212,424,76),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('107L(2)|M~Part Number~@s30@237L(2)|M~Description~@s30@20R(2)|M~Qty~@n4@35R(2)|M~' &|
   'Total~@n14.2@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(332,296,56,16),USE(?Insert:2),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(392,296,56,16),USE(?Change:2),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(452,296,56,16),USE(?Delete:2),LEFT,ICON('delete.gif')
                         END
                         TAB('&Chargeable Parts'),USE(?Chargeable_Details_Tab)
                           BUTTON('Parts Transfer'),AT(8,212,68,16),USE(?WarrantyPartsTransfer:2),LEFT,ICON('transfer.gif')
                           LIST,AT(84,212,424,76),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('104L(2)|M*~Part Number~@s30@113L(2)|M*~Description~@s30@61L(2)|M*~Order Number~@' &|
   'n012b@74L(2)|M*~Order Status~@s18@20L(2)|M*~Qty~@n4@35R(2)|M*~Total~@n8.2@500L(5' &|
   '0)*~Part Ref Number~@n012@/'),FROM(Queue:Browse)
                           BUTTON('Print Pick Note'),AT(8,232,68,16),USE(?CreatePickNote),LEFT,ICON('pick.gif')
                           BUTTON('Insert'),AT(332,292,56,16),USE(?Insert),LEFT,TIP('Insert'),ICON('insert.gif')
                           BUTTON('Change'),AT(392,292,56,16),USE(?Change),LEFT,TIP('Change'),ICON('edit.gif')
                           BUTTON('Add Adjustment'),AT(84,292,64,16),USE(?ChargeableAdjustment),LEFT,ICON('insert.gif')
                           BUTTON('Delete'),AT(452,292,56,16),USE(?Delete),LEFT,TIP('Delete'),ICON('delete.gif')
                         END
                         TAB('&Warranty Parts'),USE(?Warranty_Details_Tab)
                           LIST,AT(84,212,424,76),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('104L(2)|M*~Part Number~@s30@113L(2)|M*~Description~@s30@61D(2)|M*~Order Number~L' &|
   '@n012b@74L(2)|M*~Order Status~@s18@20L(2)|M*~Qty~@n4@35R(2)|M*~Total~@n14.2@500L' &|
   '(50)|M*~Part Ref Number~@n012@/'),FROM(Queue:Browse:3)
                           BUTTON('Print Pick Note'),AT(8,232,68,16),USE(?CreatePickNote:2),LEFT,ICON('pick.gif')
                           BUTTON('Parts Transfer'),AT(8,212,68,16),USE(?WarrantyPartsTransfer),LEFT,ICON('transfer.gif')
                           BUTTON('Insert'),AT(332,292,56,16),USE(?Insert:3),LEFT,ICON('insert.gif')
                           BUTTON('Change'),AT(392,292,56,16),USE(?Change:3),LEFT,ICON('edit.gif')
                           BUTTON('Add Adjustment'),AT(84,292,68,16),USE(?WarrantyAdjustment),LEFT,ICON('insert.gif')
                           BUTTON('Delete'),AT(452,292,56,16),USE(?Delete:3),LEFT,ICON('delete.gif')
                         END
                         TAB('In&voice Details'),USE(?Tab11)
                           PROMPT('Advance Payment'),AT(8,212),USE(?job:advance_payment:Prompt),HIDE
                           ENTRY(@n14.2b),AT(88,212,64,10),USE(job:Advance_Payment,,?JOB:Advance_Payment:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Warranty Invoice No'),AT(296,224),USE(?JOB:Invoice_Number_Warranty:Prompt),TRN
                           PROMPT('Authority Number'),AT(8,228),USE(?JOB:Authority_Number:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(88,228,64,10),USE(job:Authority_Number),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Outgoing'),AT(444,236),USE(?Prompt103),TRN,FONT(,7,,)
                           PROMPT('Incoming'),AT(372,236),USE(?Prompt104),TRN,FONT(,7,,)
                           PROMPT('Courier'),AT(296,244),USE(?job:courier:prompt)
                           COMBO(@s30),AT(444,244,64,10),USE(job:Courier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:11)
                           COMBO(@s30),AT(372,244,64,10),USE(job:Incoming_Courier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:3)
                           ENTRY(@d6b),AT(444,212,64,10),USE(job:Invoice_Date),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@p<<<<<<<#pb),AT(372,224,64,10),USE(job:Invoice_Number_Warranty),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Chargeable Invoice No'),AT(296,212),USE(?JOB:Invoice_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<#pb),AT(372,212,64,10),USE(job:Invoice_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Invoice Text:'),AT(8,260),USE(?JOB:Invoice_Text:Prompt)
                           BUTTON,AT(76,260,10,10),USE(?Invoice_Text_Buton),LEFT,ICON('list3.ico')
                           TEXT,AT(88,260,136,36),USE(jbn:Invoice_Text),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           BUTTON('Vodafone Dates'),AT(88,324,64,16),USE(?CRCDespatchDateButton),HIDE,LEFT,ICON('truck.ico')
                           PROMPT('Consignment Number'),AT(296,256),USE(?JOB:Consignment_Number:Prompt),TRN
                           ENTRY(@s30),AT(444,256,64,10),USE(job:Consignment_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON,AT(428,280,10,10),USE(?despatched_Calendar:3),HIDE,ICON('calenda2.ico')
                           ENTRY(@s30),AT(372,256,64,10),USE(job:Incoming_Consignment_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Date'),AT(296,268),USE(?JOB:Date_Despatched:Prompt),TRN
                           ENTRY(@d6b),AT(444,268,64,10),USE(job:Date_Despatched),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@d6b),AT(372,268,64,10),USE(job:Incoming_Date),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR
                           ENTRY(@d6b),AT(444,224,64,10),USE(job:Invoice_Date_Warranty),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@n6b),AT(88,300,64,10),USE(job:EDI_Batch_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Despatch User'),AT(296,280),USE(?JOB:Despatch_User:Prompt),TRN
                           ENTRY(@s3),AT(444,280,64,10),USE(job:Despatch_User),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Batch Despatch No'),AT(296,292),USE(?JOB:Despatch_Number:Prompt),TRN
                           ENTRY(@s8),AT(444,292,64,10),USE(job:Despatch_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Insurance Ref No'),AT(8,244),USE(?JOB:Insurance_Reference_Number:Prompt),TRN
                           ENTRY(@s30),AT(88,244,124,10),USE(job:Insurance_Reference_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Service'),AT(296,304),USE(?JOB:Despatch_Number:Prompt:2),TRN
                           COMBO(@s1),AT(372,304,64,10),USE(jobe:UPSFlagCode),HIDE,FONT(,,,FONT:bold),FORMAT('4L|M~UPS Flag Code~L(2)@s1@'),DROP(10),FROM('1|2|3'),MSG('UPS Flag Code')
                           COMBO(@s1),AT(444,304,64,10),USE(job:JobService),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('10L(2)|M@s1@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:6),MSG('Job Service For City Link / Label G')
                           BUTTON('Amend'),AT(372,280,52,10),USE(?Amend_Despatch:3),HIDE,LEFT,ICON('truck.ico')
                           BUTTON('Skip Despatch Validation'),AT(8,324,76,16),USE(?Skip_Despatch_Validation),HIDE,LEFT,ICON('Truck2.ico')
                           PROMPT('EDI Batch Number'),AT(8,300),USE(?JOB:EDI_Batch_Number:Prompt)
                         END
                         TAB('Loan / Exchange Details'),USE(?Loan_Exchange_Tab),HIDE
                           PROMPT('Loan Unit Number'),AT(8,212),USE(?JOB:Loan_Unit_Number:Prompt),TRN
                           ENTRY(@p<<<<<<#pb),AT(84,212,40,8),USE(job:Loan_Unit_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON('Pick Unit'),AT(124,212,60,8),USE(?Lookup_Loan_Unit),LEFT,ICON('list3.ico')
                           PROMPT('User'),AT(184,212),USE(?JOB:Loan_User:Prompt),TRN
                           ENTRY(@s3),AT(224,212,20,8),USE(job:Loan_User),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Exchange Unit Number'),AT(276,212),USE(?JOB:Exchange_Unit_Number:Prompt),TRN,HIDE
                           ENTRY(@s6b),AT(328,212,40,10),USE(job:Exchange_Unit_Number),SKIP,HIDE,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON('Pick Unit'),AT(372,212,56,8),USE(?Lookup_Exchange_Unit),LEFT,ICON('list3.ico')
                           PROMPT('User'),AT(464,252),USE(?JOB:Exchange_User:Prompt),TRN,FONT(,8,,)
                           ENTRY(@s3),AT(480,252,28,8),USE(job:Exchange_User),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Exchange Reason'),AT(276,264),USE(?jobe:ExchangeReason:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s80),AT(348,264,124,8),USE(jobe:ExchangeReason),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('5'),TIP('5'),UPR
                           BUTTON,AT(500,264,10,8),USE(?LookupExchangeReason),SKIP,ICON('List3.ico')
                           PROMPT('Loan Reason'),AT(8,264),USE(?jobe:LoanReason:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s80),AT(84,264,124,8),USE(jobe:LoanReason),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('5'),TIP('5'),UPR
                           BUTTON,AT(212,264,10,8),USE(?LookupLoanReason),SKIP,ICON('List3.ico')
                           PROMPT('Loan Courier'),AT(8,276),USE(?job:loan_courier:prompt)
                           COMBO(@s30),AT(84,276,76,8),USE(job:Loan_Courier),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:8)
                           PROMPT('Exchange Courier'),AT(276,276),USE(?job:exchange_courier:prompt)
                           COMBO(@s30),AT(348,276,76,8),USE(job:Exchange_Courier),VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:9)
                           PROMPT('Unit Details'),AT(8,228),USE(?loan_model_make_temp:Prompt),TRN
                           ENTRY(@s60),AT(84,228,136,8),USE(loan_model_make_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Exchange Unit'),AT(276,228),USE(?Exchange_Model_Make_temp:Prompt),TRN
                           ENTRY(@s60),AT(348,228,136,8),USE(Exchange_Model_Make_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(8,240),USE(?loan_esn_temp:Prompt),TRN
                           ENTRY(@s15),AT(84,240,88,8),USE(loan_esn_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('M.S.N.'),AT(176,240),USE(?loan_msn_temp:Prompt),TRN
                           ENTRY(@s10),AT(200,240,44,8),USE(loan_msn_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(276,240),USE(?Exchange_Esn_temp:Prompt),TRN
                           ENTRY(@s15),AT(348,240,88,8),USE(Exchange_Esn_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON,AT(176,252,12,8),USE(?Show_List_Button),LEFT,ICON('list3.ico')
                           PROMPT('M.S.N.'),AT(439,240),USE(?Exchange_Msn_temp:Prompt),TRN
                           ENTRY(@s10),AT(464,240,44,8),USE(Exchange_Msn_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Accessories'),AT(276,252),USE(?Exchange_Accessories_Temp:Prompt),TRN
                           ENTRY(@s60),AT(348,252,89,8),USE(Exchange_Accessories_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           BUTTON('Pick Accessory'),AT(432,212,76,8),USE(?Exchange_Accessory_Button),LEFT,ICON('list3.ico')
                           GROUP,AT(4,272,220,40),USE(?loan_group)
                             BUTTON('Amend Loan'),AT(8,324,64,16),USE(?Amend_Despatch:2),HIDE,LEFT,ICON('truck.ico')
                             PROMPT('Consignment Number'),AT(8,288),USE(?JOB:Loan_Consignment_Number:Prompt),TRN
                             ENTRY(@s30),AT(84,288,76,8),USE(job:Loan_Consignment_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Despatched'),AT(8,300),USE(?JOB:Loan_Despatched:Prompt),TRN
                             ENTRY(@d6b),AT(84,300,44,8),USE(job:Loan_Despatched),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Despatch No.'),AT(156,300),USE(?JOB:Loan_Despatch_Number:Prompt),TRN
                             ENTRY(@p<<<<<<#pb),AT(204,300,40,8),USE(job:Loan_Despatch_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('User'),AT(188,288),USE(?JOB:Loan_Despatched_User:Prompt),TRN
                             ENTRY(@s2),AT(216,288,28,8),USE(job:Loan_Despatched_User),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Service'),AT(188,276),USE(?JOB:Loan_Despatched_User:Prompt:2),TRN
                             COMBO(@s1),AT(216,276,28,8),USE(job:LoaService),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('10L(2)|M@s1@120L|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:4),MSG('Loan Service For City Link / LabelG')
                           END
                           GROUP,AT(272,276,236,40),USE(?Exchange_Group),HIDE
                             PROMPT('Service'),AT(448,280),USE(?JOB:Loan_Despatched_User:Prompt:3),TRN
                             COMBO(@s1),AT(476,280,28,8),USE(job:ExcService),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('10L(2)|M@s1@120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:5),MSG('Exchange Service For City Link / Label G')
                             BUTTON('Amend Exchange'),AT(56,328,64,16),USE(?Amend_Despatch),HIDE,LEFT,ICON('truck.ico')
                             PROMPT('Consignment Number'),AT(276,292),USE(?JOB:Exchange_Consignment_Number:Prompt),TRN
                             ENTRY(@s30),AT(348,292,76,8),USE(job:Exchange_Consignment_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Despatched'),AT(276,304),USE(?JOB:Exchange_Despatched:Prompt),TRN
                             BUTTON,AT(348,304,10,10),USE(?Despatched_Calendar),HIDE,ICON('calenda2.ico')
                             ENTRY(@d6b),AT(360,304,,8),USE(job:Exchange_Despatched),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Desp No.'),AT(432,304),USE(?JOB:Exchange_Despatch_Number:Prompt),TRN
                             ENTRY(@s8),AT(464,304,40,8),USE(job:Exchange_Despatch_Number),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('User'),AT(448,292),USE(?JOB:Exchange_Despatched_User:Prompt),TRN
                             ENTRY(@s3),AT(476,292,28,8),USE(job:Exchange_Despatched_User),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           END
                           PROMPT('Accessories'),AT(8,252),USE(?Loan_Accessories_Temp:Prompt),TRN
                           ENTRY(@s30),AT(84,252,89,8),USE(Loan_Accessories_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                         END
                       END
                       PANEL,AT(4,320,660,25),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Validate &Serial No'),AT(264,324,56,16),USE(?Validate_Serial_Number),LEFT,ICON('Spy.gif')
                       BUTTON('Print &Label'),AT(320,324,56,16),USE(?Print_Label),LEFT,ICON('Print.gif')
                       BUTTON('Contact &History'),AT(432,324,56,16),USE(?Contact_History),LEFT,ICON('Audit.gif')
                       BUTTON('&Audit Trail'),AT(488,324,56,16),USE(?Audit_Trail_Button),LEFT,ICON('Audit.gif')
                       SHEET,AT(516,196,148,120),USE(?Sheet3),SPREAD
                         TAB('Estimate'),USE(?estimate_tab),HIDE
                           CHECK('Ignore Default Charge'),AT(520,208,80,12),USE(job:Ignore_Estimate_Charges),LEFT,VALUE('YES','NO')
                           PROMPT('Courier Cost'),AT(520,220),USE(?JOB:Courier_Cost_Estimate:Prompt),TRN
                           ENTRY(@n14.2),AT(592,220,64,10),USE(job:Courier_Cost_Estimate),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour Cost'),AT(520,232),USE(?JOB:Labour_Cost_Estimate:Prompt),TRN
                           ENTRY(@n14.2),AT(592,232,64,10),USE(job:Labour_Cost_Estimate),RIGHT,FONT('Tahoma',8,,FONT:bold)
                           STRING('Parts Cost'),AT(520,244),TRN
                           STRING(@n14.2),AT(603,244,53,8),USE(job:Parts_Cost_Estimate),RIGHT
                           STRING('Sub Total'),AT(520,252),USE(?String18:2),TRN
                           STRING(@n14.2),AT(586,252),USE(job:Sub_Total_Estimate),RIGHT
                           PANEL,AT(592,264,64,2),USE(?Panel4:4),FILL(COLOR:Black)
                           STRING('V.A.T.'),AT(520,268),USE(?String19:2),TRN,RIGHT
                           STRING(@n14.2),AT(586,268),USE(vat_estimate_temp),RIGHT
                           STRING('Total'),AT(520,276),USE(?String20:2),RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n-14.2),AT(583,276),USE(total_estimate_temp),RIGHT,FONT(,,,FONT:bold)
                           PANEL,AT(592,288,64,2),USE(?Panel4:3),FILL(COLOR:Black)
                         END
                         TAB('Chargeable'),USE(?Chargeable_Tab),HIDE
                           CHECK('Ignore Default Charge'),AT(519,208,81,12),USE(job:Ignore_Chargeable_Charges),LEFT,VALUE('YES','NO')
                           PROMPT('Courier Cost'),AT(520,220),USE(?JOB:Courier_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(592,220,64,10),USE(job:Courier_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour Cost'),AT(520,232),USE(?JOB:Labour_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(592,232,64,10),USE(job:Labour_Cost),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Parts Cost'),AT(520,244),USE(?job:parts_cost:prompt),TRN
                           STRING(@n14.2),AT(603,244,53,8),USE(job:Parts_Cost),RIGHT
                           STRING('Sub Total'),AT(520,252),USE(?String18),TRN
                           STRING(@n14.2),AT(586,252),USE(job:Sub_Total),RIGHT
                           PANEL,AT(592,264,64,2),USE(?Panel4),FILL(COLOR:Black)
                           STRING('V.A.T.'),AT(520,268),USE(?CharVat),TRN,RIGHT
                           PROMPT('Invoice Rejected'),AT(540,264),USE(?CharRejected),HIDE,FONT(,,COLOR:Red,FONT:bold)
                           STRING(@n14.2),AT(586,268),USE(vat_chargeable_temp),RIGHT
                           STRING('Total'),AT(520,276),USE(?CharTotal),RIGHT,FONT(,8,,FONT:bold)
                           BUTTON('Unreject'),AT(544,276,48,12),USE(?UnRejectButton),HIDE,LEFT,ICON('arrow.ico')
                           STRING(@n-14.2),AT(583,276),USE(Total_Temp),RIGHT,FONT(,8,,FONT:bold)
                           PANEL,AT(592,288,64,2),USE(?Panel4:2),FILL(COLOR:Black)
                           PROMPT('Paid To Date'),AT(520,292),USE(?Prompt106)
                           STRING(@n-14.2),AT(589,292),USE(paid_chargeable_temp),RIGHT
                           STRING('Balance Due'),AT(520,304),USE(?String23),FONT(,10,,FONT:bold)
                           STRING(@n-14.2),AT(570,304),USE(balance_due_temp),RIGHT,FONT('Tahoma',10,,FONT:bold)
                         END
                         TAB('Warranty'),USE(?Warranty_Tab),HIDE
                           CHECK('Ignore Default Charge'),AT(520,208,80,12),USE(job:Ignore_Warranty_Charges),LEFT,VALUE('YES','NO')
                           PROMPT('Courier Cost'),AT(520,220),USE(?JOB:Courier_Cost_Warranty:Prompt),TRN
                           ENTRY(@n14.2),AT(592,220,64,10),USE(job:Courier_Cost_Warranty),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Labour Cost'),AT(520,232),USE(?JOB:Labour_Cost_Warranty:Prompt),TRN
                           ENTRY(@n14.2),AT(592,232,64,10),USE(job:Labour_Cost_Warranty),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR
                           STRING('Parts Cost'),AT(520,244),USE(?job:parts_cost_warranty:prompt),TRN
                           STRING(@n14.2),AT(603,244,53,8),USE(job:Parts_Cost_Warranty),RIGHT
                           PANEL,AT(592,264,64,2),USE(?Panel4:6),FILL(COLOR:Black)
                           STRING('Sub Total'),AT(520,252),USE(?String18:3),TRN
                           STRING(@n14.2),AT(586,252),USE(job:Sub_Total_Warranty),RIGHT
                           STRING('V.A.T. '),AT(520,268),USE(?String19:3),TRN,RIGHT
                           STRING(@n14.2),AT(586,268),USE(vat_warranty_temp),RIGHT
                           PANEL,AT(592,288,64,2),USE(?Panel4:5),FILL(COLOR:Black)
                           PROMPT('Claim Rejected'),AT(520,296,80,12),USE(?failed_warranty_claim),HIDE,FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           BUTTON('Resubmit'),AT(604,296,52,12),USE(?resubmit_button),HIDE,LEFT,ICON('arrow.ico')
                           STRING('Total'),AT(520,276),USE(?String20:3),RIGHT,FONT(,8,,FONT:bold)
                           STRING(@n-14.2),AT(583,276),USE(Total_warranty_temp),RIGHT,FONT(,,,FONT:bold)
                         END
                       END
                       BUTTON('Button 64'),AT(714,269,10,8),USE(?LookupExchangeReason:2),SKIP,ICON('List3.ico')
                       SHEET,AT(516,4,148,188),USE(?Sheet5),WIZARD,SPREAD
                         TAB('Tab 13'),USE(?Tab13)
                           STRING('Work In Progress Details'),AT(520,8),USE(?String9),TRN,FONT(,,COLOR:Navy,FONT:bold)
                           LIST,AT(520,20,140,68),USE(?List:2),IMM,VSCROLL,FONT('Tahoma',7,,FONT:regular,CHARSET:ANSI),MSG('Browsing Records'),FORMAT('69L(2)|M~Job Stage~@s20@43R(2)~Date & Time~@d6@/23L@t1@'),FROM(Queue:Browse:1)
                           PROMPT('Status'),AT(520,92),USE(?Prompt102),TRN,FONT(,,,FONT:underline)
                           STRING(@d6),AT(552,92),USE(job:Status_End_Date),HIDE
                           STRING(@t1),AT(592,92),USE(job:Status_End_Time),HIDE
                           PROMPT('Job'),AT(520,104),USE(?Prompt:Job:Current_Status),TRN,FONT(,8,,)
                           ENTRY(@s30),AT(544,104,116,10),USE(job:Current_Status),SKIP,FONT(,,,FONT:bold),READONLY
                           PROMPT('Exch.'),AT(520,120),USE(?JOB:Exchange_Status:Prompt),TRN,FONT(,8,,)
                           ENTRY(@s30),AT(544,120,116,10),USE(job:Exchange_Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Loan'),AT(520,132,,10),USE(?JOB:Loan_Status:Prompt),TRN,FONT(,8,,)
                           ENTRY(@s30),AT(544,132,116,10),USE(job:Loan_Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Turnaround Time'),AT(520,148,56,8),USE(?Prompt79),FONT(,,,FONT:underline)
                           PROMPT('Job Time Remain'),AT(520,160,68,8),USE(?Prompt80)
                           STRING(@s20),AT(592,160,67,10),USE(job_time_remaining_temp),FONT(,,COLOR:Navy,FONT:bold)
                           PROMPT('Status Time Remain'),AT(520,172),USE(?Prompt81)
                           STRING(@s20),AT(592,172,67,10),USE(status_time_remaining_temp),FONT(,,COLOR:Navy,FONT:bold)
                         END
                       END
                       BUTTON('&OK'),AT(548,324,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(604,324,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       BUTTON('Status Changes'),AT(376,324,56,16),USE(?Contact_History:2),LEFT,ICON('Audit.gif')
                       BUTTON('Third Part&y'),AT(208,324,56,16),USE(?ThirdPartyButton),LEFT,ICON('despatch.gif')
                       BUTTON('&Fault Codes'),AT(152,324,56,16),USE(?Fault_Codes_Lookup),LEFT,ICON('fault.gif')
                       STRING('JOB CANCELLED'),AT(0,0,516,196),USE(?CancelText),TRN,HIDE,FONT('Tahoma',36,COLOR:Red,FONT:bold,CHARSET:ANSI),ANGLE(250)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB40               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:8         !Reference to browse queue type
                     END

FDCB41               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:9         !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:11        !Reference to browse queue type
                     END

FDCB17               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:12        !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB25               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB16               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

FDCB24               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:5         !Reference to browse queue type
                     END

FDCB30               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:6         !Reference to browse queue type
                     END

FDCB46               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW12                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW12::Sort0:Locator StepLocatorClass                 !Default Locator
BRW21                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW21::Sort0:Locator StepLocatorClass                 !Default Locator
BRW77                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW77::Sort0:Locator StepLocatorClass                 !Default Locator
BRW79                CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:3                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW79::Sort0:Locator StepLocatorClass                 !Default Locator
save_lac_id   ushort,auto
save_xca_id   ushort,auto
save_jac_id   ushort,auto
save_job_ali_id   ushort,auto
save_trr_id   ushort,auto
save_orp_id   ushort,auto
save_maf_id   ushort,auto
save_par_id   ushort,auto
save_wpr_id   ushort,auto
save_map_id   ushort,auto
save_par_ali_id   ushort,auto
save_epr_id   ushort,auto
save_jpt_id   ushort,auto
save_esn_id   ushort,auto
save_jea_id   ushort,auto

! Start Change 2618 BE(06/08/03)
!
! include definition here rather than in CLARION Data section
! in order to exclude from Solace View which appears unable
! to cope with a GROUP ARRAY data item.
!
JobStatusColours     GROUP,PRE(JSC),DIM(8)
Colour               LONG
Action               BYTE
Status               STRING(30)
                     END
! Start Change 2618 BE(06/08/03)
    Map
LocalValidateAccessories    Procedure(),Byte
UpdateRFBoardIMEI PROCEDURE (LONG,STRING)
    End
SaveAccessory    File,Driver('TOPSPEED'),Pre(savacc),Name(tmp:SaveAccessory),Create,Bindable,Thread
AccessoryKey            Key(savacc:Accessory),NOCASE,DUP
Record                  Record
Accessory               String(30)
                        End
                    End
TempFilePath         CSTRING(255)

!Save Entry Fields Incase Of Lookup
look:job:Account_Number                Like(job:Account_Number)
look:job:Transit_Type                Like(job:Transit_Type)
look:job:Location                Like(job:Location)
look:job:Model_Number                Like(job:Model_Number)
look:tmp:Network                Like(tmp:Network)
look:job:Charge_Type                Like(job:Charge_Type)
look:job:Warranty_Charge_Type                Like(job:Warranty_Charge_Type)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
DBH34:PopupText String(1000)
DBH45:PopupText String(1000)

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Batch_Number_Text{prop:FontColor} = -1
    ?Batch_Number_Text{prop:Color} = 15066597
    ?Title_Text{prop:FontColor} = -1
    ?Title_Text{prop:Color} = 15066597
    ?Sheet4{prop:Color} = 15066597
    ?Booking1_Tab{prop:Color} = 15066597
    ?JOB:Auto_Search:Prompt{prop:FontColor} = -1
    ?JOB:Auto_Search:Prompt{prop:Color} = 15066597
    If ?job:Auto_Search{prop:ReadOnly} = True
        ?job:Auto_Search{prop:FontColor} = 65793
        ?job:Auto_Search{prop:Color} = 15066597
    Elsif ?job:Auto_Search{prop:Req} = True
        ?job:Auto_Search{prop:FontColor} = 65793
        ?job:Auto_Search{prop:Color} = 8454143
    Else ! If ?job:Auto_Search{prop:Req} = True
        ?job:Auto_Search{prop:FontColor} = 65793
        ?job:Auto_Search{prop:Color} = 16777215
    End ! If ?job:Auto_Search{prop:Req} = True
    ?job:Auto_Search{prop:Trn} = 0
    ?job:Auto_Search{prop:FontStyle} = font:Bold
    If ?job:Last_Repair_Days{prop:ReadOnly} = True
        ?job:Last_Repair_Days{prop:FontColor} = 65793
        ?job:Last_Repair_Days{prop:Color} = 15066597
    Elsif ?job:Last_Repair_Days{prop:Req} = True
        ?job:Last_Repair_Days{prop:FontColor} = 65793
        ?job:Last_Repair_Days{prop:Color} = 8454143
    Else ! If ?job:Last_Repair_Days{prop:Req} = True
        ?job:Last_Repair_Days{prop:FontColor} = 65793
        ?job:Last_Repair_Days{prop:Color} = 16777215
    End ! If ?job:Last_Repair_Days{prop:Req} = True
    ?job:Last_Repair_Days{prop:Trn} = 0
    ?job:Last_Repair_Days{prop:FontStyle} = font:Bold
    ?JOB:Last_Repair_Days:Prompt{prop:FontColor} = -1
    ?JOB:Last_Repair_Days:Prompt{prop:Color} = 15066597
    ?Job:Account_Number:Prompt{prop:FontColor} = -1
    ?Job:Account_Number:Prompt{prop:Color} = 15066597
    ?Trade_Account_Status{prop:FontColor} = -1
    ?Trade_Account_Status{prop:Color} = 15066597
    If ?job:Account_Number{prop:ReadOnly} = True
        ?job:Account_Number{prop:FontColor} = 65793
        ?job:Account_Number{prop:Color} = 15066597
    Elsif ?job:Account_Number{prop:Req} = True
        ?job:Account_Number{prop:FontColor} = 65793
        ?job:Account_Number{prop:Color} = 8454143
    Else ! If ?job:Account_Number{prop:Req} = True
        ?job:Account_Number{prop:FontColor} = 65793
        ?job:Account_Number{prop:Color} = 16777215
    End ! If ?job:Account_Number{prop:Req} = True
    ?job:Account_Number{prop:Trn} = 0
    ?job:Account_Number{prop:FontStyle} = font:Bold
    ?job:order_number:prompt{prop:FontColor} = -1
    ?job:order_number:prompt{prop:Color} = 15066597
    If ?job:Order_Number{prop:ReadOnly} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 15066597
    Elsif ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 8454143
    Else ! If ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 16777215
    End ! If ?job:Order_Number{prop:Req} = True
    ?job:Order_Number{prop:Trn} = 0
    ?job:Order_Number{prop:FontStyle} = font:Bold
    ?NameGroup{prop:Font,3} = -1
    ?NameGroup{prop:Color} = 15066597
    ?NameGroup{prop:Trn} = 0
    ?Title:prompt{prop:FontColor} = -1
    ?Title:prompt{prop:Color} = 15066597
    ?Initial:prompt{prop:FontColor} = -1
    ?Initial:prompt{prop:Color} = 15066597
    ?surname:prompt{prop:FontColor} = -1
    ?surname:prompt{prop:Color} = 15066597
    ?job:title:prompt{prop:FontColor} = -1
    ?job:title:prompt{prop:Color} = 15066597
    If ?job:Title{prop:ReadOnly} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 15066597
    Elsif ?job:Title{prop:Req} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 8454143
    Else ! If ?job:Title{prop:Req} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 16777215
    End ! If ?job:Title{prop:Req} = True
    ?job:Title{prop:Trn} = 0
    ?job:Title{prop:FontStyle} = font:Bold
    If ?job:Initial{prop:ReadOnly} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 15066597
    Elsif ?job:Initial{prop:Req} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 8454143
    Else ! If ?job:Initial{prop:Req} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 16777215
    End ! If ?job:Initial{prop:Req} = True
    ?job:Initial{prop:Trn} = 0
    ?job:Initial{prop:FontStyle} = font:Bold
    If ?job:Surname{prop:ReadOnly} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 15066597
    Elsif ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 8454143
    Else ! If ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 16777215
    End ! If ?job:Surname{prop:Req} = True
    ?job:Surname{prop:Trn} = 0
    ?job:Surname{prop:FontStyle} = font:Bold
    ?Prompt49{prop:FontColor} = -1
    ?Prompt49{prop:Color} = 15066597
    If ?job:Transit_Type{prop:ReadOnly} = True
        ?job:Transit_Type{prop:FontColor} = 65793
        ?job:Transit_Type{prop:Color} = 15066597
    Elsif ?job:Transit_Type{prop:Req} = True
        ?job:Transit_Type{prop:FontColor} = 65793
        ?job:Transit_Type{prop:Color} = 8454143
    Else ! If ?job:Transit_Type{prop:Req} = True
        ?job:Transit_Type{prop:FontColor} = 65793
        ?job:Transit_Type{prop:Color} = 16777215
    End ! If ?job:Transit_Type{prop:Req} = True
    ?job:Transit_Type{prop:Trn} = 0
    ?job:Transit_Type{prop:FontStyle} = font:Bold
    ?job:location_type:prompt{prop:FontColor} = -1
    ?job:location_type:prompt{prop:Color} = 15066597
    ?job:Workshop{prop:Font,3} = -1
    ?job:Workshop{prop:Color} = 15066597
    ?job:Workshop{prop:Trn} = 0
    ?Location_Group{prop:Font,3} = -1
    ?Location_Group{prop:Color} = 15066597
    ?Location_Group{prop:Trn} = 0
    ?job:location:prompt{prop:FontColor} = -1
    ?job:location:prompt{prop:Color} = 15066597
    If ?job:Location{prop:ReadOnly} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 15066597
    Elsif ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 8454143
    Else ! If ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 16777215
    End ! If ?job:Location{prop:Req} = True
    ?job:Location{prop:Trn} = 0
    ?job:Location{prop:FontStyle} = font:Bold
    ?Third_party_message{prop:FontColor} = -1
    ?Third_party_message{prop:Color} = 15066597
    If ?job:Third_Party_Site{prop:ReadOnly} = True
        ?job:Third_Party_Site{prop:FontColor} = 65793
        ?job:Third_Party_Site{prop:Color} = 15066597
    Elsif ?job:Third_Party_Site{prop:Req} = True
        ?job:Third_Party_Site{prop:FontColor} = 65793
        ?job:Third_Party_Site{prop:Color} = 8454143
    Else ! If ?job:Third_Party_Site{prop:Req} = True
        ?job:Third_Party_Site{prop:FontColor} = 65793
        ?job:Third_Party_Site{prop:Color} = 16777215
    End ! If ?job:Third_Party_Site{prop:Req} = True
    ?job:Third_Party_Site{prop:Trn} = 0
    ?job:Third_Party_Site{prop:FontStyle} = font:Bold
    If ?job:ESN{prop:ReadOnly} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 15066597
    Elsif ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 8454143
    Else ! If ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 16777215
    End ! If ?job:ESN{prop:Req} = True
    ?job:ESN{prop:Trn} = 0
    ?job:ESN{prop:FontStyle} = font:Bold
    ?JOB:MSN:Prompt{prop:FontColor} = -1
    ?JOB:MSN:Prompt{prop:Color} = 15066597
    If ?job:MSN{prop:ReadOnly} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 15066597
    Elsif ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 8454143
    Else ! If ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 16777215
    End ! If ?job:MSN{prop:Req} = True
    ?job:MSN{prop:Trn} = 0
    ?job:MSN{prop:FontStyle} = font:Bold
    ?job:ProductCode:Prompt{prop:FontColor} = -1
    ?job:ProductCode:Prompt{prop:Color} = 15066597
    If ?job:ProductCode{prop:ReadOnly} = True
        ?job:ProductCode{prop:FontColor} = 65793
        ?job:ProductCode{prop:Color} = 15066597
    Elsif ?job:ProductCode{prop:Req} = True
        ?job:ProductCode{prop:FontColor} = 65793
        ?job:ProductCode{prop:Color} = 8454143
    Else ! If ?job:ProductCode{prop:Req} = True
        ?job:ProductCode{prop:FontColor} = 65793
        ?job:ProductCode{prop:Color} = 16777215
    End ! If ?job:ProductCode{prop:Req} = True
    ?job:ProductCode{prop:Trn} = 0
    ?job:ProductCode{prop:FontStyle} = font:Bold
    ?JOB:Model_Number:Prompt{prop:FontColor} = -1
    ?JOB:Model_Number:Prompt{prop:Color} = 15066597
    If ?job:Model_Number{prop:ReadOnly} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 15066597
    Elsif ?job:Model_Number{prop:Req} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 8454143
    Else ! If ?job:Model_Number{prop:Req} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 16777215
    End ! If ?job:Model_Number{prop:Req} = True
    ?job:Model_Number{prop:Trn} = 0
    ?job:Model_Number{prop:FontStyle} = font:Bold
    If ?job:Manufacturer{prop:ReadOnly} = True
        ?job:Manufacturer{prop:FontColor} = 65793
        ?job:Manufacturer{prop:Color} = 15066597
    Elsif ?job:Manufacturer{prop:Req} = True
        ?job:Manufacturer{prop:FontColor} = 65793
        ?job:Manufacturer{prop:Color} = 8454143
    Else ! If ?job:Manufacturer{prop:Req} = True
        ?job:Manufacturer{prop:FontColor} = 65793
        ?job:Manufacturer{prop:Color} = 16777215
    End ! If ?job:Manufacturer{prop:Req} = True
    ?job:Manufacturer{prop:Trn} = 0
    ?job:Manufacturer{prop:FontStyle} = font:Bold
    ?Prompt:Job:Unit_Type{prop:FontColor} = -1
    ?Prompt:Job:Unit_Type{prop:Color} = 15066597
    If ?job:Unit_Type{prop:ReadOnly} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 15066597
    Elsif ?job:Unit_Type{prop:Req} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 8454143
    Else ! If ?job:Unit_Type{prop:Req} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 16777215
    End ! If ?job:Unit_Type{prop:Req} = True
    ?job:Unit_Type{prop:Trn} = 0
    ?job:Unit_Type{prop:FontStyle} = font:Bold
    If ?job:Colour{prop:ReadOnly} = True
        ?job:Colour{prop:FontColor} = 65793
        ?job:Colour{prop:Color} = 15066597
    Elsif ?job:Colour{prop:Req} = True
        ?job:Colour{prop:FontColor} = 65793
        ?job:Colour{prop:Color} = 8454143
    Else ! If ?job:Colour{prop:Req} = True
        ?job:Colour{prop:FontColor} = 65793
        ?job:Colour{prop:Color} = 16777215
    End ! If ?job:Colour{prop:Req} = True
    ?job:Colour{prop:Trn} = 0
    ?job:Colour{prop:FontStyle} = font:Bold
    ?Accessories_Temp:Prompt{prop:FontColor} = -1
    ?Accessories_Temp:Prompt{prop:Color} = 15066597
    If ?Accessories_Temp{prop:ReadOnly} = True
        ?Accessories_Temp{prop:FontColor} = 65793
        ?Accessories_Temp{prop:Color} = 15066597
    Elsif ?Accessories_Temp{prop:Req} = True
        ?Accessories_Temp{prop:FontColor} = 65793
        ?Accessories_Temp{prop:Color} = 8454143
    Else ! If ?Accessories_Temp{prop:Req} = True
        ?Accessories_Temp{prop:FontColor} = 65793
        ?Accessories_Temp{prop:Color} = 16777215
    End ! If ?Accessories_Temp{prop:Req} = True
    ?Accessories_Temp{prop:Trn} = 0
    ?Accessories_Temp{prop:FontStyle} = font:Bold
    ?JOB:DOP:Prompt{prop:FontColor} = -1
    ?JOB:DOP:Prompt{prop:Color} = 15066597
    If ?job:DOP{prop:ReadOnly} = True
        ?job:DOP{prop:FontColor} = 65793
        ?job:DOP{prop:Color} = 15066597
    Elsif ?job:DOP{prop:Req} = True
        ?job:DOP{prop:FontColor} = 65793
        ?job:DOP{prop:Color} = 8454143
    Else ! If ?job:DOP{prop:Req} = True
        ?job:DOP{prop:FontColor} = 65793
        ?job:DOP{prop:Color} = 16777215
    End ! If ?job:DOP{prop:Req} = True
    ?job:DOP{prop:Trn} = 0
    ?job:DOP{prop:FontStyle} = font:Bold
    ?Prompt47{prop:FontColor} = -1
    ?Prompt47{prop:Color} = 15066597
    ?tmp:Network:Prompt:2{prop:FontColor} = -1
    ?tmp:Network:Prompt:2{prop:Color} = 15066597
    If ?tmp:Network:2{prop:ReadOnly} = True
        ?tmp:Network:2{prop:FontColor} = 65793
        ?tmp:Network:2{prop:Color} = 15066597
    Elsif ?tmp:Network:2{prop:Req} = True
        ?tmp:Network:2{prop:FontColor} = 65793
        ?tmp:Network:2{prop:Color} = 8454143
    Else ! If ?tmp:Network:2{prop:Req} = True
        ?tmp:Network:2{prop:FontColor} = 65793
        ?tmp:Network:2{prop:Color} = 16777215
    End ! If ?tmp:Network:2{prop:Req} = True
    ?tmp:Network:2{prop:Trn} = 0
    ?tmp:Network:2{prop:FontStyle} = font:Bold
    ?job:esn:prompt{prop:FontColor} = -1
    ?job:esn:prompt{prop:Color} = 15066597
    ?JOB:Mobile_Number:Prompt{prop:FontColor} = -1
    ?JOB:Mobile_Number:Prompt{prop:Color} = 15066597
    If ?job:Mobile_Number{prop:ReadOnly} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 15066597
    Elsif ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 8454143
    Else ! If ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 16777215
    End ! If ?job:Mobile_Number{prop:Req} = True
    ?job:Mobile_Number{prop:Trn} = 0
    ?job:Mobile_Number{prop:FontStyle} = font:Bold
    ?tmp:InWorkshopDate{prop:FontColor} = -1
    ?tmp:InWorkshopDate{prop:Color} = 15066597
    ?tmp:InWorkshopTime{prop:FontColor} = -1
    ?tmp:InWorkshopTime{prop:Color} = 15066597
    ?Prompt101{prop:FontColor} = -1
    ?Prompt101{prop:Color} = 15066597
    If ?job:Turnaround_Time{prop:ReadOnly} = True
        ?job:Turnaround_Time{prop:FontColor} = 65793
        ?job:Turnaround_Time{prop:Color} = 15066597
    Elsif ?job:Turnaround_Time{prop:Req} = True
        ?job:Turnaround_Time{prop:FontColor} = 65793
        ?job:Turnaround_Time{prop:Color} = 8454143
    Else ! If ?job:Turnaround_Time{prop:Req} = True
        ?job:Turnaround_Time{prop:FontColor} = 65793
        ?job:Turnaround_Time{prop:Color} = 16777215
    End ! If ?job:Turnaround_Time{prop:Req} = True
    ?job:Turnaround_Time{prop:Trn} = 0
    ?job:Turnaround_Time{prop:FontStyle} = font:Bold
    ?JOB:Third_Party_Site:Prompt{prop:FontColor} = -1
    ?JOB:Third_Party_Site:Prompt{prop:Color} = 15066597
    ?job:charge_type:prompt{prop:FontColor} = -1
    ?job:charge_type:prompt{prop:Color} = 15066597
    ?job:Chargeable_Job{prop:Font,3} = -1
    ?job:Chargeable_Job{prop:Color} = 15066597
    ?job:Chargeable_Job{prop:Trn} = 0
    If ?job:Charge_Type{prop:ReadOnly} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 15066597
    Elsif ?job:Charge_Type{prop:Req} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 8454143
    Else ! If ?job:Charge_Type{prop:Req} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 16777215
    End ! If ?job:Charge_Type{prop:Req} = True
    ?job:Charge_Type{prop:Trn} = 0
    ?job:Charge_Type{prop:FontStyle} = font:Bold
    ?JOB:Third_Party_Despatch_Date:Prompt{prop:FontColor} = -1
    ?JOB:Third_Party_Despatch_Date:Prompt{prop:Color} = 15066597
    If ?job:ThirdPartyDateDesp{prop:ReadOnly} = True
        ?job:ThirdPartyDateDesp{prop:FontColor} = 65793
        ?job:ThirdPartyDateDesp{prop:Color} = 15066597
    Elsif ?job:ThirdPartyDateDesp{prop:Req} = True
        ?job:ThirdPartyDateDesp{prop:FontColor} = 65793
        ?job:ThirdPartyDateDesp{prop:Color} = 8454143
    Else ! If ?job:ThirdPartyDateDesp{prop:Req} = True
        ?job:ThirdPartyDateDesp{prop:FontColor} = 65793
        ?job:ThirdPartyDateDesp{prop:Color} = 16777215
    End ! If ?job:ThirdPartyDateDesp{prop:Req} = True
    ?job:ThirdPartyDateDesp{prop:Trn} = 0
    ?job:ThirdPartyDateDesp{prop:FontStyle} = font:Bold
    ?job:Warranty_Job{prop:Font,3} = -1
    ?job:Warranty_Job{prop:Color} = 15066597
    ?job:Warranty_Job{prop:Trn} = 0
    If ?job:Warranty_Charge_Type{prop:ReadOnly} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 15066597
    Elsif ?job:Warranty_Charge_Type{prop:Req} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 8454143
    Else ! If ?job:Warranty_Charge_Type{prop:Req} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 16777215
    End ! If ?job:Warranty_Charge_Type{prop:Req} = True
    ?job:Warranty_Charge_Type{prop:Trn} = 0
    ?job:Warranty_Charge_Type{prop:FontStyle} = font:Bold
    If ?job:Date_Paid{prop:ReadOnly} = True
        ?job:Date_Paid{prop:FontColor} = 65793
        ?job:Date_Paid{prop:Color} = 15066597
    Elsif ?job:Date_Paid{prop:Req} = True
        ?job:Date_Paid{prop:FontColor} = 65793
        ?job:Date_Paid{prop:Color} = 8454143
    Else ! If ?job:Date_Paid{prop:Req} = True
        ?job:Date_Paid{prop:FontColor} = 65793
        ?job:Date_Paid{prop:Color} = 16777215
    End ! If ?job:Date_Paid{prop:Req} = True
    ?job:Date_Paid{prop:Trn} = 0
    ?job:Date_Paid{prop:FontStyle} = font:Bold
    ?job:date_paid:prompt{prop:FontColor} = -1
    ?job:date_paid:prompt{prop:Color} = 15066597
    ?job:Estimate{prop:Font,3} = -1
    ?job:Estimate{prop:Color} = 15066597
    ?job:Estimate{prop:Trn} = 0
    If ?job:Estimate_If_Over{prop:ReadOnly} = True
        ?job:Estimate_If_Over{prop:FontColor} = 65793
        ?job:Estimate_If_Over{prop:Color} = 15066597
    Elsif ?job:Estimate_If_Over{prop:Req} = True
        ?job:Estimate_If_Over{prop:FontColor} = 65793
        ?job:Estimate_If_Over{prop:Color} = 8454143
    Else ! If ?job:Estimate_If_Over{prop:Req} = True
        ?job:Estimate_If_Over{prop:FontColor} = 65793
        ?job:Estimate_If_Over{prop:Color} = 16777215
    End ! If ?job:Estimate_If_Over{prop:Req} = True
    ?job:Estimate_If_Over{prop:Trn} = 0
    ?job:Estimate_If_Over{prop:FontStyle} = font:Bold
    ?job:Estimate_Accepted{prop:Font,3} = -1
    ?job:Estimate_Accepted{prop:Color} = 15066597
    ?job:Estimate_Accepted{prop:Trn} = 0
    ?job:Estimate_Rejected{prop:Font,3} = -1
    ?job:Estimate_Rejected{prop:Color} = 15066597
    ?job:Estimate_Rejected{prop:Trn} = 0
    ?job:Estimate_If_Over:prompt{prop:FontColor} = -1
    ?job:Estimate_If_Over:prompt{prop:Color} = 15066597
    ?Booking2_Tab{prop:Color} = 15066597
    ?Group9{prop:Font,3} = -1
    ?Group9{prop:Color} = 15066597
    ?Group9{prop:Trn} = 0
    ?trade_account_string_temp{prop:FontColor} = -1
    ?trade_account_string_temp{prop:Color} = 15066597
    ?JOB:Order_Number:2{prop:FontColor} = -1
    ?JOB:Order_Number:2{prop:Color} = 15066597
    ?customer_name_string_temp{prop:FontColor} = -1
    ?customer_name_string_temp{prop:Color} = 15066597
    ?JOB:Transit_Type:2{prop:FontColor} = -1
    ?JOB:Transit_Type:2{prop:Color} = 15066597
    ?tmp:Network{prop:FontColor} = -1
    ?tmp:Network{prop:Color} = 15066597
    ?JOB:Workshop:2{prop:FontColor} = -1
    ?JOB:Workshop:2{prop:Color} = 15066597
    ?JOB:Location:2{prop:FontColor} = -1
    ?JOB:Location:2{prop:Color} = 15066597
    ?JOB:Third_Party_Site:2{prop:FontColor} = -1
    ?JOB:Third_Party_Site:2{prop:Color} = 15066597
    ?JOB:Special_Instructions:2{prop:FontColor} = -1
    ?JOB:Special_Instructions:2{prop:Color} = 15066597
    ?StatusWarning{prop:FontColor} = -1
    ?StatusWarning{prop:Color} = 15066597
    ?Group9:2{prop:Font,3} = -1
    ?Group9:2{prop:Color} = 15066597
    ?Group9:2{prop:Trn} = 0
    If ?job:ESN:2{prop:ReadOnly} = True
        ?job:ESN:2{prop:FontColor} = 65793
        ?job:ESN:2{prop:Color} = 15066597
    Elsif ?job:ESN:2{prop:Req} = True
        ?job:ESN:2{prop:FontColor} = 65793
        ?job:ESN:2{prop:Color} = 8454143
    Else ! If ?job:ESN:2{prop:Req} = True
        ?job:ESN:2{prop:FontColor} = 65793
        ?job:ESN:2{prop:Color} = 16777215
    End ! If ?job:ESN:2{prop:Req} = True
    ?job:ESN:2{prop:Trn} = 0
    ?job:ESN:2{prop:FontStyle} = font:Bold
    If ?job:MSN:2{prop:ReadOnly} = True
        ?job:MSN:2{prop:FontColor} = 65793
        ?job:MSN:2{prop:Color} = 15066597
    Elsif ?job:MSN:2{prop:Req} = True
        ?job:MSN:2{prop:FontColor} = 65793
        ?job:MSN:2{prop:Color} = 8454143
    Else ! If ?job:MSN:2{prop:Req} = True
        ?job:MSN:2{prop:FontColor} = 65793
        ?job:MSN:2{prop:Color} = 16777215
    End ! If ?job:MSN:2{prop:Req} = True
    ?job:MSN:2{prop:Trn} = 0
    ?job:MSN:2{prop:FontStyle} = font:Bold
    If ?job:Model_Number:2{prop:ReadOnly} = True
        ?job:Model_Number:2{prop:FontColor} = 65793
        ?job:Model_Number:2{prop:Color} = 15066597
    Elsif ?job:Model_Number:2{prop:Req} = True
        ?job:Model_Number:2{prop:FontColor} = 65793
        ?job:Model_Number:2{prop:Color} = 8454143
    Else ! If ?job:Model_Number:2{prop:Req} = True
        ?job:Model_Number:2{prop:FontColor} = 65793
        ?job:Model_Number:2{prop:Color} = 16777215
    End ! If ?job:Model_Number:2{prop:Req} = True
    ?job:Model_Number:2{prop:Trn} = 0
    ?job:Model_Number:2{prop:FontStyle} = font:Bold
    If ?job:ProductCode:2{prop:ReadOnly} = True
        ?job:ProductCode:2{prop:FontColor} = 65793
        ?job:ProductCode:2{prop:Color} = 15066597
    Elsif ?job:ProductCode:2{prop:Req} = True
        ?job:ProductCode:2{prop:FontColor} = 65793
        ?job:ProductCode:2{prop:Color} = 8454143
    Else ! If ?job:ProductCode:2{prop:Req} = True
        ?job:ProductCode:2{prop:FontColor} = 65793
        ?job:ProductCode:2{prop:Color} = 16777215
    End ! If ?job:ProductCode:2{prop:Req} = True
    ?job:ProductCode:2{prop:Trn} = 0
    ?job:ProductCode:2{prop:FontStyle} = font:Bold
    If ?job:Colour:2{prop:ReadOnly} = True
        ?job:Colour:2{prop:FontColor} = 65793
        ?job:Colour:2{prop:Color} = 15066597
    Elsif ?job:Colour:2{prop:Req} = True
        ?job:Colour:2{prop:FontColor} = 65793
        ?job:Colour:2{prop:Color} = 8454143
    Else ! If ?job:Colour:2{prop:Req} = True
        ?job:Colour:2{prop:FontColor} = 65793
        ?job:Colour:2{prop:Color} = 16777215
    End ! If ?job:Colour:2{prop:Req} = True
    ?job:Colour:2{prop:Trn} = 0
    ?job:Colour:2{prop:FontStyle} = font:Bold
    If ?job:Mobile_Number:2{prop:ReadOnly} = True
        ?job:Mobile_Number:2{prop:FontColor} = 65793
        ?job:Mobile_Number:2{prop:Color} = 15066597
    Elsif ?job:Mobile_Number:2{prop:Req} = True
        ?job:Mobile_Number:2{prop:FontColor} = 65793
        ?job:Mobile_Number:2{prop:Color} = 8454143
    Else ! If ?job:Mobile_Number:2{prop:Req} = True
        ?job:Mobile_Number:2{prop:FontColor} = 65793
        ?job:Mobile_Number:2{prop:Color} = 16777215
    End ! If ?job:Mobile_Number:2{prop:Req} = True
    ?job:Mobile_Number:2{prop:Trn} = 0
    ?job:Mobile_Number:2{prop:FontStyle} = font:Bold
    ?job:DOP:2{prop:FontColor} = -1
    ?job:DOP:2{prop:Color} = 15066597
    If ?job:Turnaround_Time:2{prop:ReadOnly} = True
        ?job:Turnaround_Time:2{prop:FontColor} = 65793
        ?job:Turnaround_Time:2{prop:Color} = 15066597
    Elsif ?job:Turnaround_Time:2{prop:Req} = True
        ?job:Turnaround_Time:2{prop:FontColor} = 65793
        ?job:Turnaround_Time:2{prop:Color} = 8454143
    Else ! If ?job:Turnaround_Time:2{prop:Req} = True
        ?job:Turnaround_Time:2{prop:FontColor} = 65793
        ?job:Turnaround_Time:2{prop:Color} = 16777215
    End ! If ?job:Turnaround_Time:2{prop:Req} = True
    ?job:Turnaround_Time:2{prop:Trn} = 0
    ?job:Turnaround_Time:2{prop:FontStyle} = font:Bold
    If ?job:Charge_Type:2{prop:ReadOnly} = True
        ?job:Charge_Type:2{prop:FontColor} = 65793
        ?job:Charge_Type:2{prop:Color} = 15066597
    Elsif ?job:Charge_Type:2{prop:Req} = True
        ?job:Charge_Type:2{prop:FontColor} = 65793
        ?job:Charge_Type:2{prop:Color} = 8454143
    Else ! If ?job:Charge_Type:2{prop:Req} = True
        ?job:Charge_Type:2{prop:FontColor} = 65793
        ?job:Charge_Type:2{prop:Color} = 16777215
    End ! If ?job:Charge_Type:2{prop:Req} = True
    ?job:Charge_Type:2{prop:Trn} = 0
    ?job:Charge_Type:2{prop:FontStyle} = font:Bold
    If ?job:Warranty_Charge_Type:2{prop:ReadOnly} = True
        ?job:Warranty_Charge_Type:2{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type:2{prop:Color} = 15066597
    Elsif ?job:Warranty_Charge_Type:2{prop:Req} = True
        ?job:Warranty_Charge_Type:2{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type:2{prop:Color} = 8454143
    Else ! If ?job:Warranty_Charge_Type:2{prop:Req} = True
        ?job:Warranty_Charge_Type:2{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type:2{prop:Color} = 16777215
    End ! If ?job:Warranty_Charge_Type:2{prop:Req} = True
    ?job:Warranty_Charge_Type:2{prop:Trn} = 0
    ?job:Warranty_Charge_Type:2{prop:FontStyle} = font:Bold
    If ?tmp:EstimateDetails{prop:ReadOnly} = True
        ?tmp:EstimateDetails{prop:FontColor} = 65793
        ?tmp:EstimateDetails{prop:Color} = 15066597
    Elsif ?tmp:EstimateDetails{prop:Req} = True
        ?tmp:EstimateDetails{prop:FontColor} = 65793
        ?tmp:EstimateDetails{prop:Color} = 8454143
    Else ! If ?tmp:EstimateDetails{prop:Req} = True
        ?tmp:EstimateDetails{prop:FontColor} = 65793
        ?tmp:EstimateDetails{prop:Color} = 16777215
    End ! If ?tmp:EstimateDetails{prop:Req} = True
    ?tmp:EstimateDetails{prop:Trn} = 0
    ?tmp:EstimateDetails{prop:FontStyle} = font:Bold
    If ?job:Manufacturer:2{prop:ReadOnly} = True
        ?job:Manufacturer:2{prop:FontColor} = 65793
        ?job:Manufacturer:2{prop:Color} = 15066597
    Elsif ?job:Manufacturer:2{prop:Req} = True
        ?job:Manufacturer:2{prop:FontColor} = 65793
        ?job:Manufacturer:2{prop:Color} = 8454143
    Else ! If ?job:Manufacturer:2{prop:Req} = True
        ?job:Manufacturer:2{prop:FontColor} = 65793
        ?job:Manufacturer:2{prop:Color} = 16777215
    End ! If ?job:Manufacturer:2{prop:Req} = True
    ?job:Manufacturer:2{prop:Trn} = 0
    ?job:Manufacturer:2{prop:FontStyle} = font:Bold
    ?SIMNumber{prop:FontColor} = -1
    ?SIMNumber{prop:Color} = 15066597
    If ?jobe:SIMNumber{prop:ReadOnly} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 15066597
    Elsif ?jobe:SIMNumber{prop:Req} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 8454143
    Else ! If ?jobe:SIMNumber{prop:Req} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 16777215
    End ! If ?jobe:SIMNumber{prop:Req} = True
    ?jobe:SIMNumber{prop:Trn} = 0
    ?jobe:SIMNumber{prop:FontStyle} = font:Bold
    If ?tmp:IncomingIMEI{prop:ReadOnly} = True
        ?tmp:IncomingIMEI{prop:FontColor} = 65793
        ?tmp:IncomingIMEI{prop:Color} = 15066597
    Elsif ?tmp:IncomingIMEI{prop:Req} = True
        ?tmp:IncomingIMEI{prop:FontColor} = 65793
        ?tmp:IncomingIMEI{prop:Color} = 8454143
    Else ! If ?tmp:IncomingIMEI{prop:Req} = True
        ?tmp:IncomingIMEI{prop:FontColor} = 65793
        ?tmp:IncomingIMEI{prop:Color} = 16777215
    End ! If ?tmp:IncomingIMEI{prop:Req} = True
    ?tmp:IncomingIMEI{prop:Trn} = 0
    ?tmp:IncomingIMEI{prop:FontStyle} = font:Bold
    If ?tmp:IncomingMSN{prop:ReadOnly} = True
        ?tmp:IncomingMSN{prop:FontColor} = 65793
        ?tmp:IncomingMSN{prop:Color} = 15066597
    Elsif ?tmp:IncomingMSN{prop:Req} = True
        ?tmp:IncomingMSN{prop:FontColor} = 65793
        ?tmp:IncomingMSN{prop:Color} = 8454143
    Else ! If ?tmp:IncomingMSN{prop:Req} = True
        ?tmp:IncomingMSN{prop:FontColor} = 65793
        ?tmp:IncomingMSN{prop:Color} = 16777215
    End ! If ?tmp:IncomingMSN{prop:Req} = True
    ?tmp:IncomingMSN{prop:Trn} = 0
    ?tmp:IncomingMSN{prop:FontStyle} = font:Bold
    If ?tmp:ExchangeIMEI{prop:ReadOnly} = True
        ?tmp:ExchangeIMEI{prop:FontColor} = 65793
        ?tmp:ExchangeIMEI{prop:Color} = 15066597
    Elsif ?tmp:ExchangeIMEI{prop:Req} = True
        ?tmp:ExchangeIMEI{prop:FontColor} = 65793
        ?tmp:ExchangeIMEI{prop:Color} = 8454143
    Else ! If ?tmp:ExchangeIMEI{prop:Req} = True
        ?tmp:ExchangeIMEI{prop:FontColor} = 65793
        ?tmp:ExchangeIMEI{prop:Color} = 16777215
    End ! If ?tmp:ExchangeIMEI{prop:Req} = True
    ?tmp:ExchangeIMEI{prop:Trn} = 0
    ?tmp:ExchangeIMEI{prop:FontStyle} = font:Bold
    If ?tmp:ExchangeMSN{prop:ReadOnly} = True
        ?tmp:ExchangeMSN{prop:FontColor} = 65793
        ?tmp:ExchangeMSN{prop:Color} = 15066597
    Elsif ?tmp:ExchangeMSN{prop:Req} = True
        ?tmp:ExchangeMSN{prop:FontColor} = 65793
        ?tmp:ExchangeMSN{prop:Color} = 8454143
    Else ! If ?tmp:ExchangeMSN{prop:Req} = True
        ?tmp:ExchangeMSN{prop:FontColor} = 65793
        ?tmp:ExchangeMSN{prop:Color} = 16777215
    End ! If ?tmp:ExchangeMSN{prop:Req} = True
    ?tmp:ExchangeMSN{prop:Trn} = 0
    ?tmp:ExchangeMSN{prop:FontStyle} = font:Bold
    If ?job:Unit_Type:2{prop:ReadOnly} = True
        ?job:Unit_Type:2{prop:FontColor} = 65793
        ?job:Unit_Type:2{prop:Color} = 15066597
    Elsif ?job:Unit_Type:2{prop:Req} = True
        ?job:Unit_Type:2{prop:FontColor} = 65793
        ?job:Unit_Type:2{prop:Color} = 8454143
    Else ! If ?job:Unit_Type:2{prop:Req} = True
        ?job:Unit_Type:2{prop:FontColor} = 65793
        ?job:Unit_Type:2{prop:Color} = 16777215
    End ! If ?job:Unit_Type:2{prop:Req} = True
    ?job:Unit_Type:2{prop:Trn} = 0
    ?job:Unit_Type:2{prop:FontStyle} = font:Bold
    ?Prompt107{prop:FontColor} = -1
    ?Prompt107{prop:Color} = 15066597
    ?Final{prop:FontColor} = -1
    ?Final{prop:Color} = 15066597
    ?Incoming:3{prop:FontColor} = -1
    ?Incoming:3{prop:Color} = 15066597
    ?IMEIMSN{prop:FontColor} = -1
    ?IMEIMSN{prop:Color} = 15066597
    ?Exchanged{prop:FontColor} = -1
    ?Exchanged{prop:Color} = 15066597
    ?IMEIMSN:2{prop:FontColor} = -1
    ?IMEIMSN:2{prop:Color} = 15066597
    ?Prompt108{prop:FontColor} = -1
    ?Prompt108{prop:Color} = 15066597
    ?ModelNumberProductCode{prop:FontColor} = -1
    ?ModelNumberProductCode{prop:Color} = 15066597
    ?Manufacturer{prop:FontColor} = -1
    ?Manufacturer{prop:Color} = 15066597
    ?IMEIMSN:3{prop:FontColor} = -1
    ?IMEIMSN:3{prop:Color} = 15066597
    ?Customer_Name_String_temp:Prompt{prop:FontColor} = -1
    ?Customer_Name_String_temp:Prompt{prop:Color} = 15066597
    ?Prompt110{prop:FontColor} = -1
    ?Prompt110{prop:Color} = 15066597
    ?JobTurnaroundTime{prop:FontColor} = -1
    ?JobTurnaroundTime{prop:Color} = 15066597
    ?UnitTypeColour{prop:FontColor} = -1
    ?UnitTypeColour{prop:Color} = 15066597
    ?MobileNumberDOP{prop:FontColor} = -1
    ?MobileNumberDOP{prop:Color} = 15066597
    ?Prompt111{prop:FontColor} = -1
    ?Prompt111{prop:Color} = 15066597
    ?tmp:Network:Prompt{prop:FontColor} = -1
    ?tmp:Network:Prompt{prop:Color} = 15066597
    ?location_string{prop:FontColor} = -1
    ?location_string{prop:Color} = 15066597
    ?third_party_string{prop:FontColor} = -1
    ?third_party_string{prop:Color} = 15066597
    ?chargeable_Type_string{prop:FontColor} = -1
    ?chargeable_Type_string{prop:Color} = 15066597
    ?special_instructions_string{prop:FontColor} = -1
    ?special_instructions_string{prop:Color} = 15066597
    ?warranty_charge_Type_String{prop:FontColor} = -1
    ?warranty_charge_Type_String{prop:Color} = 15066597
    ?EstimatePrompt{prop:FontColor} = -1
    ?EstimatePrompt{prop:Color} = 15066597
    ?BouncerText{prop:FontColor} = -1
    ?BouncerText{prop:Color} = 15066597
    ?show_booking_temp{prop:Font,3} = -1
    ?show_booking_temp{prop:Color} = 15066597
    ?show_booking_temp{prop:Trn} = 0
    ?jobcompleted{prop:FontColor} = -1
    ?jobcompleted{prop:Color} = 15066597
    ?Exchanged_Title{prop:FontColor} = -1
    ?Exchanged_Title{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?String6{prop:FontColor} = -1
    ?String6{prop:Color} = 15066597
    ?Prompt128{prop:FontColor} = -1
    ?Prompt128{prop:Color} = 15066597
    ?job:Company_Name{prop:FontColor} = -1
    ?job:Company_Name{prop:Color} = 15066597
    ?job:Postcode:2{prop:FontColor} = -1
    ?job:Postcode:2{prop:Color} = 15066597
    ?job:Telephone_Number{prop:FontColor} = -1
    ?job:Telephone_Number{prop:Color} = 15066597
    ?job:Fax_Number{prop:FontColor} = -1
    ?job:Fax_Number{prop:Color} = 15066597
    ?String21:4{prop:FontColor} = -1
    ?String21:4{prop:Color} = 15066597
    ?jobe:EndUserEmailAddress{prop:FontColor} = -1
    ?jobe:EndUserEmailAddress{prop:Color} = 15066597
    ?String21:3{prop:FontColor} = -1
    ?String21:3{prop:Color} = 15066597
    ?job:Address_Line1{prop:FontColor} = -1
    ?job:Address_Line1{prop:Color} = 15066597
    ?job:Address_Line2{prop:FontColor} = -1
    ?job:Address_Line2{prop:Color} = 15066597
    ?JOB:Address:Prompt{prop:FontColor} = -1
    ?JOB:Address:Prompt{prop:Color} = 15066597
    ?job:Address_Line3{prop:FontColor} = -1
    ?job:Address_Line3{prop:Color} = 15066597
    ?String21{prop:FontColor} = -1
    ?String21{prop:Color} = 15066597
    ?String21:2{prop:FontColor} = -1
    ?String21:2{prop:Color} = 15066597
    ?Collection_Address_Group{prop:Font,3} = -1
    ?Collection_Address_Group{prop:Color} = 15066597
    ?Collection_Address_Group{prop:Trn} = 0
    ?String7{prop:FontColor} = -1
    ?String7{prop:Color} = 15066597
    ?job:Company_Name_Collection{prop:FontColor} = -1
    ?job:Company_Name_Collection{prop:Color} = 15066597
    ?job:Address_Line1_Collection{prop:FontColor} = -1
    ?job:Address_Line1_Collection{prop:Color} = 15066597
    ?job:Address_Line2_Collection{prop:FontColor} = -1
    ?job:Address_Line2_Collection{prop:Color} = 15066597
    ?job:Address_Line3_Collection{prop:FontColor} = -1
    ?job:Address_Line3_Collection{prop:Color} = 15066597
    ?job:Postcode_Collection{prop:FontColor} = -1
    ?job:Postcode_Collection{prop:Color} = 15066597
    ?job:Telephone_Collection{prop:FontColor} = -1
    ?job:Telephone_Collection{prop:Color} = 15066597
    If ?jbn:Collection_Text{prop:ReadOnly} = True
        ?jbn:Collection_Text{prop:FontColor} = 65793
        ?jbn:Collection_Text{prop:Color} = 15066597
    Elsif ?jbn:Collection_Text{prop:Req} = True
        ?jbn:Collection_Text{prop:FontColor} = 65793
        ?jbn:Collection_Text{prop:Color} = 8454143
    Else ! If ?jbn:Collection_Text{prop:Req} = True
        ?jbn:Collection_Text{prop:FontColor} = 65793
        ?jbn:Collection_Text{prop:Color} = 16777215
    End ! If ?jbn:Collection_Text{prop:Req} = True
    ?jbn:Collection_Text{prop:Trn} = 0
    ?jbn:Collection_Text{prop:FontStyle} = font:Bold
    ?delivery_Address_Group{prop:Font,3} = -1
    ?delivery_Address_Group{prop:Color} = 15066597
    ?delivery_Address_Group{prop:Trn} = 0
    ?String8{prop:FontColor} = -1
    ?String8{prop:Color} = 15066597
    ?job:Company_Name_Delivery{prop:FontColor} = -1
    ?job:Company_Name_Delivery{prop:Color} = 15066597
    ?job:Address_Line1_Delivery{prop:FontColor} = -1
    ?job:Address_Line1_Delivery{prop:Color} = 15066597
    ?job:Address_Line2_Delivery{prop:FontColor} = -1
    ?job:Address_Line2_Delivery{prop:Color} = 15066597
    ?job:Address_Line3_Delivery{prop:FontColor} = -1
    ?job:Address_Line3_Delivery{prop:Color} = 15066597
    ?job:Postcode_Delivery{prop:FontColor} = -1
    ?job:Postcode_Delivery{prop:Color} = 15066597
    ?job:Telephone_Delivery{prop:FontColor} = -1
    ?job:Telephone_Delivery{prop:Color} = 15066597
    If ?jbn:Delivery_Text{prop:ReadOnly} = True
        ?jbn:Delivery_Text{prop:FontColor} = 65793
        ?jbn:Delivery_Text{prop:Color} = 15066597
    Elsif ?jbn:Delivery_Text{prop:Req} = True
        ?jbn:Delivery_Text{prop:FontColor} = 65793
        ?jbn:Delivery_Text{prop:Color} = 8454143
    Else ! If ?jbn:Delivery_Text{prop:Req} = True
        ?jbn:Delivery_Text{prop:FontColor} = 65793
        ?jbn:Delivery_Text{prop:Color} = 16777215
    End ! If ?jbn:Delivery_Text{prop:Req} = True
    ?jbn:Delivery_Text{prop:Trn} = 0
    ?jbn:Delivery_Text{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    ?Job:Engineer:Prompt{prop:FontColor} = -1
    ?Job:Engineer:Prompt{prop:Color} = 15066597
    ?Repair_Type_Chargeable_Group{prop:Font,3} = -1
    ?Repair_Type_Chargeable_Group{prop:Color} = 15066597
    ?Repair_Type_Chargeable_Group{prop:Trn} = 0
    ?Prompt105{prop:FontColor} = -1
    ?Prompt105{prop:Color} = 15066597
    If ?job:Repair_Type{prop:ReadOnly} = True
        ?job:Repair_Type{prop:FontColor} = 65793
        ?job:Repair_Type{prop:Color} = 15066597
    Elsif ?job:Repair_Type{prop:Req} = True
        ?job:Repair_Type{prop:FontColor} = 65793
        ?job:Repair_Type{prop:Color} = 8454143
    Else ! If ?job:Repair_Type{prop:Req} = True
        ?job:Repair_Type{prop:FontColor} = 65793
        ?job:Repair_Type{prop:Color} = 16777215
    End ! If ?job:Repair_Type{prop:Req} = True
    ?job:Repair_Type{prop:Trn} = 0
    ?job:Repair_Type{prop:FontStyle} = font:Bold
    ?Job:Repair_Type:Prompt{prop:FontColor} = -1
    ?Job:Repair_Type:Prompt{prop:Color} = 15066597
    ?JOB:Engineers_Notes:Prompt{prop:FontColor} = -1
    ?JOB:Engineers_Notes:Prompt{prop:Color} = 15066597
    If ?jbn:Engineers_Notes{prop:ReadOnly} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 15066597
    Elsif ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 8454143
    Else ! If ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 16777215
    End ! If ?jbn:Engineers_Notes{prop:Req} = True
    ?jbn:Engineers_Notes{prop:Trn} = 0
    ?jbn:Engineers_Notes{prop:FontStyle} = font:Bold
    ?Prompt131{prop:FontColor} = -1
    ?Prompt131{prop:Color} = 15066597
    ?Prompt125:2{prop:FontColor} = -1
    ?Prompt125:2{prop:Color} = 15066597
    ?Prompt125{prop:FontColor} = -1
    ?Prompt125{prop:Color} = 15066597
    ?Prompt85{prop:FontColor} = -1
    ?Prompt85{prop:Color} = 15066597
    ?job:Estimate_Ready{prop:Font,3} = -1
    ?job:Estimate_Ready{prop:Color} = 15066597
    ?job:Estimate_Ready{prop:Trn} = 0
    ?tmp:SkillLevel{prop:FontColor} = -1
    ?tmp:SkillLevel{prop:Color} = 15066597
    ?JobLevel{prop:FontColor} = -1
    ?JobLevel{prop:Color} = 15066597
    ?jobe:SkillLevel{prop:FontColor} = -1
    ?jobe:SkillLevel{prop:Color} = 15066597
    ?Prompt126:4{prop:FontColor} = -1
    ?Prompt126:4{prop:Color} = 15066597
    ?engineer_name_temp{prop:FontColor} = -1
    ?engineer_name_temp{prop:Color} = 15066597
    ?tmp:DateAllocated{prop:FontColor} = -1
    ?tmp:DateAllocated{prop:Color} = 15066597
    ?Prompt126:2{prop:FontColor} = -1
    ?Prompt126:2{prop:Color} = 15066597
    ?Repair_Type_Warranty_Group{prop:Font,3} = -1
    ?Repair_Type_Warranty_Group{prop:Color} = 15066597
    ?Repair_Type_Warranty_Group{prop:Trn} = 0
    ?JOB:Repair_Type_Warranty:Prompt{prop:FontColor} = -1
    ?JOB:Repair_Type_Warranty:Prompt{prop:Color} = 15066597
    If ?job:Repair_Type_Warranty{prop:ReadOnly} = True
        ?job:Repair_Type_Warranty{prop:FontColor} = 65793
        ?job:Repair_Type_Warranty{prop:Color} = 15066597
    Elsif ?job:Repair_Type_Warranty{prop:Req} = True
        ?job:Repair_Type_Warranty{prop:FontColor} = 65793
        ?job:Repair_Type_Warranty{prop:Color} = 8454143
    Else ! If ?job:Repair_Type_Warranty{prop:Req} = True
        ?job:Repair_Type_Warranty{prop:FontColor} = 65793
        ?job:Repair_Type_Warranty{prop:Color} = 16777215
    End ! If ?job:Repair_Type_Warranty{prop:Req} = True
    ?job:Repair_Type_Warranty{prop:Trn} = 0
    ?job:Repair_Type_Warranty{prop:FontStyle} = font:Bold
    ?job:In_Repair{prop:Font,3} = -1
    ?job:In_Repair{prop:Color} = 15066597
    ?job:In_Repair{prop:Trn} = 0
    ?job:On_Test{prop:Font,3} = -1
    ?job:On_Test{prop:Color} = 15066597
    ?job:On_Test{prop:Trn} = 0
    ?job:Date_In_Repair:2{prop:FontColor} = -1
    ?job:Date_In_Repair:2{prop:Color} = 15066597
    ?job:Time_In_Repair:2{prop:FontColor} = -1
    ?job:Time_In_Repair:2{prop:Color} = 15066597
    ?job:Date_On_Test{prop:FontColor} = -1
    ?job:Date_On_Test{prop:Color} = 15066597
    ?job:Time_On_Test{prop:FontColor} = -1
    ?job:Time_On_Test{prop:Color} = 15066597
    ?Prompt100{prop:FontColor} = -1
    ?Prompt100{prop:Color} = 15066597
    ?Panel8{prop:Fill} = 15066597

    If ?job:Date_Completed{prop:ReadOnly} = True
        ?job:Date_Completed{prop:FontColor} = 65793
        ?job:Date_Completed{prop:Color} = 15066597
    Elsif ?job:Date_Completed{prop:Req} = True
        ?job:Date_Completed{prop:FontColor} = 65793
        ?job:Date_Completed{prop:Color} = 8454143
    Else ! If ?job:Date_Completed{prop:Req} = True
        ?job:Date_Completed{prop:FontColor} = 65793
        ?job:Date_Completed{prop:Color} = 16777215
    End ! If ?job:Date_Completed{prop:Req} = True
    ?job:Date_Completed{prop:Trn} = 0
    ?job:Date_Completed{prop:FontStyle} = font:Bold
    If ?job:Time_Completed{prop:ReadOnly} = True
        ?job:Time_Completed{prop:FontColor} = 65793
        ?job:Time_Completed{prop:Color} = 15066597
    Elsif ?job:Time_Completed{prop:Req} = True
        ?job:Time_Completed{prop:FontColor} = 65793
        ?job:Time_Completed{prop:Color} = 8454143
    Else ! If ?job:Time_Completed{prop:Req} = True
        ?job:Time_Completed{prop:FontColor} = 65793
        ?job:Time_Completed{prop:Color} = 16777215
    End ! If ?job:Time_Completed{prop:Req} = True
    ?job:Time_Completed{prop:Trn} = 0
    ?job:Time_Completed{prop:FontStyle} = font:Bold
    If ?jobe:CompleteRepairDate{prop:ReadOnly} = True
        ?jobe:CompleteRepairDate{prop:FontColor} = 65793
        ?jobe:CompleteRepairDate{prop:Color} = 15066597
    Elsif ?jobe:CompleteRepairDate{prop:Req} = True
        ?jobe:CompleteRepairDate{prop:FontColor} = 65793
        ?jobe:CompleteRepairDate{prop:Color} = 8454143
    Else ! If ?jobe:CompleteRepairDate{prop:Req} = True
        ?jobe:CompleteRepairDate{prop:FontColor} = 65793
        ?jobe:CompleteRepairDate{prop:Color} = 16777215
    End ! If ?jobe:CompleteRepairDate{prop:Req} = True
    ?jobe:CompleteRepairDate{prop:Trn} = 0
    ?jobe:CompleteRepairDate{prop:FontStyle} = font:Bold
    If ?jobe:CompleteRepairTime{prop:ReadOnly} = True
        ?jobe:CompleteRepairTime{prop:FontColor} = 65793
        ?jobe:CompleteRepairTime{prop:Color} = 15066597
    Elsif ?jobe:CompleteRepairTime{prop:Req} = True
        ?jobe:CompleteRepairTime{prop:FontColor} = 65793
        ?jobe:CompleteRepairTime{prop:Color} = 8454143
    Else ! If ?jobe:CompleteRepairTime{prop:Req} = True
        ?jobe:CompleteRepairTime{prop:FontColor} = 65793
        ?jobe:CompleteRepairTime{prop:Color} = 16777215
    End ! If ?jobe:CompleteRepairTime{prop:Req} = True
    ?jobe:CompleteRepairTime{prop:Trn} = 0
    ?jobe:CompleteRepairTime{prop:FontStyle} = font:Bold
    ?QA_Group{prop:Font,3} = -1
    ?QA_Group{prop:Color} = 15066597
    ?QA_Group{prop:Trn} = 0
    ?Prompt99{prop:FontColor} = -1
    ?Prompt99{prop:Color} = 15066597
    ?job:QA_Passed{prop:Font,3} = -1
    ?job:QA_Passed{prop:Color} = 15066597
    ?job:QA_Passed{prop:Trn} = 0
    If ?job:Date_QA_Passed{prop:ReadOnly} = True
        ?job:Date_QA_Passed{prop:FontColor} = 65793
        ?job:Date_QA_Passed{prop:Color} = 15066597
    Elsif ?job:Date_QA_Passed{prop:Req} = True
        ?job:Date_QA_Passed{prop:FontColor} = 65793
        ?job:Date_QA_Passed{prop:Color} = 8454143
    Else ! If ?job:Date_QA_Passed{prop:Req} = True
        ?job:Date_QA_Passed{prop:FontColor} = 65793
        ?job:Date_QA_Passed{prop:Color} = 16777215
    End ! If ?job:Date_QA_Passed{prop:Req} = True
    ?job:Date_QA_Passed{prop:Trn} = 0
    ?job:Date_QA_Passed{prop:FontStyle} = font:Bold
    If ?job:Time_QA_Passed{prop:ReadOnly} = True
        ?job:Time_QA_Passed{prop:FontColor} = 65793
        ?job:Time_QA_Passed{prop:Color} = 15066597
    Elsif ?job:Time_QA_Passed{prop:Req} = True
        ?job:Time_QA_Passed{prop:FontColor} = 65793
        ?job:Time_QA_Passed{prop:Color} = 8454143
    Else ! If ?job:Time_QA_Passed{prop:Req} = True
        ?job:Time_QA_Passed{prop:FontColor} = 65793
        ?job:Time_QA_Passed{prop:Color} = 16777215
    End ! If ?job:Time_QA_Passed{prop:Req} = True
    ?job:Time_QA_Passed{prop:Trn} = 0
    ?job:Time_QA_Passed{prop:FontStyle} = font:Bold
    ?Estimate_Details_Tab{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Chargeable_Details_Tab{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Warranty_Details_Tab{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Tab11{prop:Color} = 15066597
    ?job:advance_payment:Prompt{prop:FontColor} = -1
    ?job:advance_payment:Prompt{prop:Color} = 15066597
    If ?JOB:Advance_Payment:2{prop:ReadOnly} = True
        ?JOB:Advance_Payment:2{prop:FontColor} = 65793
        ?JOB:Advance_Payment:2{prop:Color} = 15066597
    Elsif ?JOB:Advance_Payment:2{prop:Req} = True
        ?JOB:Advance_Payment:2{prop:FontColor} = 65793
        ?JOB:Advance_Payment:2{prop:Color} = 8454143
    Else ! If ?JOB:Advance_Payment:2{prop:Req} = True
        ?JOB:Advance_Payment:2{prop:FontColor} = 65793
        ?JOB:Advance_Payment:2{prop:Color} = 16777215
    End ! If ?JOB:Advance_Payment:2{prop:Req} = True
    ?JOB:Advance_Payment:2{prop:Trn} = 0
    ?JOB:Advance_Payment:2{prop:FontStyle} = font:Bold
    ?JOB:Invoice_Number_Warranty:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Number_Warranty:Prompt{prop:Color} = 15066597
    ?JOB:Authority_Number:Prompt{prop:FontColor} = -1
    ?JOB:Authority_Number:Prompt{prop:Color} = 15066597
    If ?job:Authority_Number{prop:ReadOnly} = True
        ?job:Authority_Number{prop:FontColor} = 65793
        ?job:Authority_Number{prop:Color} = 15066597
    Elsif ?job:Authority_Number{prop:Req} = True
        ?job:Authority_Number{prop:FontColor} = 65793
        ?job:Authority_Number{prop:Color} = 8454143
    Else ! If ?job:Authority_Number{prop:Req} = True
        ?job:Authority_Number{prop:FontColor} = 65793
        ?job:Authority_Number{prop:Color} = 16777215
    End ! If ?job:Authority_Number{prop:Req} = True
    ?job:Authority_Number{prop:Trn} = 0
    ?job:Authority_Number{prop:FontStyle} = font:Bold
    ?Prompt103{prop:FontColor} = -1
    ?Prompt103{prop:Color} = 15066597
    ?Prompt104{prop:FontColor} = -1
    ?Prompt104{prop:Color} = 15066597
    ?job:courier:prompt{prop:FontColor} = -1
    ?job:courier:prompt{prop:Color} = 15066597
    If ?job:Courier{prop:ReadOnly} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 15066597
    Elsif ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 8454143
    Else ! If ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 16777215
    End ! If ?job:Courier{prop:Req} = True
    ?job:Courier{prop:Trn} = 0
    ?job:Courier{prop:FontStyle} = font:Bold
    If ?job:Incoming_Courier{prop:ReadOnly} = True
        ?job:Incoming_Courier{prop:FontColor} = 65793
        ?job:Incoming_Courier{prop:Color} = 15066597
    Elsif ?job:Incoming_Courier{prop:Req} = True
        ?job:Incoming_Courier{prop:FontColor} = 65793
        ?job:Incoming_Courier{prop:Color} = 8454143
    Else ! If ?job:Incoming_Courier{prop:Req} = True
        ?job:Incoming_Courier{prop:FontColor} = 65793
        ?job:Incoming_Courier{prop:Color} = 16777215
    End ! If ?job:Incoming_Courier{prop:Req} = True
    ?job:Incoming_Courier{prop:Trn} = 0
    ?job:Incoming_Courier{prop:FontStyle} = font:Bold
    If ?job:Invoice_Date{prop:ReadOnly} = True
        ?job:Invoice_Date{prop:FontColor} = 65793
        ?job:Invoice_Date{prop:Color} = 15066597
    Elsif ?job:Invoice_Date{prop:Req} = True
        ?job:Invoice_Date{prop:FontColor} = 65793
        ?job:Invoice_Date{prop:Color} = 8454143
    Else ! If ?job:Invoice_Date{prop:Req} = True
        ?job:Invoice_Date{prop:FontColor} = 65793
        ?job:Invoice_Date{prop:Color} = 16777215
    End ! If ?job:Invoice_Date{prop:Req} = True
    ?job:Invoice_Date{prop:Trn} = 0
    ?job:Invoice_Date{prop:FontStyle} = font:Bold
    If ?job:Invoice_Number_Warranty{prop:ReadOnly} = True
        ?job:Invoice_Number_Warranty{prop:FontColor} = 65793
        ?job:Invoice_Number_Warranty{prop:Color} = 15066597
    Elsif ?job:Invoice_Number_Warranty{prop:Req} = True
        ?job:Invoice_Number_Warranty{prop:FontColor} = 65793
        ?job:Invoice_Number_Warranty{prop:Color} = 8454143
    Else ! If ?job:Invoice_Number_Warranty{prop:Req} = True
        ?job:Invoice_Number_Warranty{prop:FontColor} = 65793
        ?job:Invoice_Number_Warranty{prop:Color} = 16777215
    End ! If ?job:Invoice_Number_Warranty{prop:Req} = True
    ?job:Invoice_Number_Warranty{prop:Trn} = 0
    ?job:Invoice_Number_Warranty{prop:FontStyle} = font:Bold
    ?JOB:Invoice_Number:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Number:Prompt{prop:Color} = 15066597
    If ?job:Invoice_Number{prop:ReadOnly} = True
        ?job:Invoice_Number{prop:FontColor} = 65793
        ?job:Invoice_Number{prop:Color} = 15066597
    Elsif ?job:Invoice_Number{prop:Req} = True
        ?job:Invoice_Number{prop:FontColor} = 65793
        ?job:Invoice_Number{prop:Color} = 8454143
    Else ! If ?job:Invoice_Number{prop:Req} = True
        ?job:Invoice_Number{prop:FontColor} = 65793
        ?job:Invoice_Number{prop:Color} = 16777215
    End ! If ?job:Invoice_Number{prop:Req} = True
    ?job:Invoice_Number{prop:Trn} = 0
    ?job:Invoice_Number{prop:FontStyle} = font:Bold
    ?JOB:Invoice_Text:Prompt{prop:FontColor} = -1
    ?JOB:Invoice_Text:Prompt{prop:Color} = 15066597
    If ?jbn:Invoice_Text{prop:ReadOnly} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 15066597
    Elsif ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 8454143
    Else ! If ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 16777215
    End ! If ?jbn:Invoice_Text{prop:Req} = True
    ?jbn:Invoice_Text{prop:Trn} = 0
    ?jbn:Invoice_Text{prop:FontStyle} = font:Bold
    ?JOB:Consignment_Number:Prompt{prop:FontColor} = -1
    ?JOB:Consignment_Number:Prompt{prop:Color} = 15066597
    If ?job:Consignment_Number{prop:ReadOnly} = True
        ?job:Consignment_Number{prop:FontColor} = 65793
        ?job:Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Consignment_Number{prop:Req} = True
        ?job:Consignment_Number{prop:FontColor} = 65793
        ?job:Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Consignment_Number{prop:Req} = True
        ?job:Consignment_Number{prop:FontColor} = 65793
        ?job:Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Consignment_Number{prop:Req} = True
    ?job:Consignment_Number{prop:Trn} = 0
    ?job:Consignment_Number{prop:FontStyle} = font:Bold
    If ?job:Incoming_Consignment_Number{prop:ReadOnly} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Incoming_Consignment_Number{prop:Req} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Incoming_Consignment_Number{prop:Req} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Incoming_Consignment_Number{prop:Req} = True
    ?job:Incoming_Consignment_Number{prop:Trn} = 0
    ?job:Incoming_Consignment_Number{prop:FontStyle} = font:Bold
    ?JOB:Date_Despatched:Prompt{prop:FontColor} = -1
    ?JOB:Date_Despatched:Prompt{prop:Color} = 15066597
    If ?job:Date_Despatched{prop:ReadOnly} = True
        ?job:Date_Despatched{prop:FontColor} = 65793
        ?job:Date_Despatched{prop:Color} = 15066597
    Elsif ?job:Date_Despatched{prop:Req} = True
        ?job:Date_Despatched{prop:FontColor} = 65793
        ?job:Date_Despatched{prop:Color} = 8454143
    Else ! If ?job:Date_Despatched{prop:Req} = True
        ?job:Date_Despatched{prop:FontColor} = 65793
        ?job:Date_Despatched{prop:Color} = 16777215
    End ! If ?job:Date_Despatched{prop:Req} = True
    ?job:Date_Despatched{prop:Trn} = 0
    ?job:Date_Despatched{prop:FontStyle} = font:Bold
    If ?job:Incoming_Date{prop:ReadOnly} = True
        ?job:Incoming_Date{prop:FontColor} = 65793
        ?job:Incoming_Date{prop:Color} = 15066597
    Elsif ?job:Incoming_Date{prop:Req} = True
        ?job:Incoming_Date{prop:FontColor} = 65793
        ?job:Incoming_Date{prop:Color} = 8454143
    Else ! If ?job:Incoming_Date{prop:Req} = True
        ?job:Incoming_Date{prop:FontColor} = 65793
        ?job:Incoming_Date{prop:Color} = 16777215
    End ! If ?job:Incoming_Date{prop:Req} = True
    ?job:Incoming_Date{prop:Trn} = 0
    ?job:Incoming_Date{prop:FontStyle} = font:Bold
    If ?job:Invoice_Date_Warranty{prop:ReadOnly} = True
        ?job:Invoice_Date_Warranty{prop:FontColor} = 65793
        ?job:Invoice_Date_Warranty{prop:Color} = 15066597
    Elsif ?job:Invoice_Date_Warranty{prop:Req} = True
        ?job:Invoice_Date_Warranty{prop:FontColor} = 65793
        ?job:Invoice_Date_Warranty{prop:Color} = 8454143
    Else ! If ?job:Invoice_Date_Warranty{prop:Req} = True
        ?job:Invoice_Date_Warranty{prop:FontColor} = 65793
        ?job:Invoice_Date_Warranty{prop:Color} = 16777215
    End ! If ?job:Invoice_Date_Warranty{prop:Req} = True
    ?job:Invoice_Date_Warranty{prop:Trn} = 0
    ?job:Invoice_Date_Warranty{prop:FontStyle} = font:Bold
    If ?job:EDI_Batch_Number{prop:ReadOnly} = True
        ?job:EDI_Batch_Number{prop:FontColor} = 65793
        ?job:EDI_Batch_Number{prop:Color} = 15066597
    Elsif ?job:EDI_Batch_Number{prop:Req} = True
        ?job:EDI_Batch_Number{prop:FontColor} = 65793
        ?job:EDI_Batch_Number{prop:Color} = 8454143
    Else ! If ?job:EDI_Batch_Number{prop:Req} = True
        ?job:EDI_Batch_Number{prop:FontColor} = 65793
        ?job:EDI_Batch_Number{prop:Color} = 16777215
    End ! If ?job:EDI_Batch_Number{prop:Req} = True
    ?job:EDI_Batch_Number{prop:Trn} = 0
    ?job:EDI_Batch_Number{prop:FontStyle} = font:Bold
    ?JOB:Despatch_User:Prompt{prop:FontColor} = -1
    ?JOB:Despatch_User:Prompt{prop:Color} = 15066597
    If ?job:Despatch_User{prop:ReadOnly} = True
        ?job:Despatch_User{prop:FontColor} = 65793
        ?job:Despatch_User{prop:Color} = 15066597
    Elsif ?job:Despatch_User{prop:Req} = True
        ?job:Despatch_User{prop:FontColor} = 65793
        ?job:Despatch_User{prop:Color} = 8454143
    Else ! If ?job:Despatch_User{prop:Req} = True
        ?job:Despatch_User{prop:FontColor} = 65793
        ?job:Despatch_User{prop:Color} = 16777215
    End ! If ?job:Despatch_User{prop:Req} = True
    ?job:Despatch_User{prop:Trn} = 0
    ?job:Despatch_User{prop:FontStyle} = font:Bold
    ?JOB:Despatch_Number:Prompt{prop:FontColor} = -1
    ?JOB:Despatch_Number:Prompt{prop:Color} = 15066597
    If ?job:Despatch_Number{prop:ReadOnly} = True
        ?job:Despatch_Number{prop:FontColor} = 65793
        ?job:Despatch_Number{prop:Color} = 15066597
    Elsif ?job:Despatch_Number{prop:Req} = True
        ?job:Despatch_Number{prop:FontColor} = 65793
        ?job:Despatch_Number{prop:Color} = 8454143
    Else ! If ?job:Despatch_Number{prop:Req} = True
        ?job:Despatch_Number{prop:FontColor} = 65793
        ?job:Despatch_Number{prop:Color} = 16777215
    End ! If ?job:Despatch_Number{prop:Req} = True
    ?job:Despatch_Number{prop:Trn} = 0
    ?job:Despatch_Number{prop:FontStyle} = font:Bold
    ?JOB:Insurance_Reference_Number:Prompt{prop:FontColor} = -1
    ?JOB:Insurance_Reference_Number:Prompt{prop:Color} = 15066597
    If ?job:Insurance_Reference_Number{prop:ReadOnly} = True
        ?job:Insurance_Reference_Number{prop:FontColor} = 65793
        ?job:Insurance_Reference_Number{prop:Color} = 15066597
    Elsif ?job:Insurance_Reference_Number{prop:Req} = True
        ?job:Insurance_Reference_Number{prop:FontColor} = 65793
        ?job:Insurance_Reference_Number{prop:Color} = 8454143
    Else ! If ?job:Insurance_Reference_Number{prop:Req} = True
        ?job:Insurance_Reference_Number{prop:FontColor} = 65793
        ?job:Insurance_Reference_Number{prop:Color} = 16777215
    End ! If ?job:Insurance_Reference_Number{prop:Req} = True
    ?job:Insurance_Reference_Number{prop:Trn} = 0
    ?job:Insurance_Reference_Number{prop:FontStyle} = font:Bold
    ?JOB:Despatch_Number:Prompt:2{prop:FontColor} = -1
    ?JOB:Despatch_Number:Prompt:2{prop:Color} = 15066597
    If ?jobe:UPSFlagCode{prop:ReadOnly} = True
        ?jobe:UPSFlagCode{prop:FontColor} = 65793
        ?jobe:UPSFlagCode{prop:Color} = 15066597
    Elsif ?jobe:UPSFlagCode{prop:Req} = True
        ?jobe:UPSFlagCode{prop:FontColor} = 65793
        ?jobe:UPSFlagCode{prop:Color} = 8454143
    Else ! If ?jobe:UPSFlagCode{prop:Req} = True
        ?jobe:UPSFlagCode{prop:FontColor} = 65793
        ?jobe:UPSFlagCode{prop:Color} = 16777215
    End ! If ?jobe:UPSFlagCode{prop:Req} = True
    ?jobe:UPSFlagCode{prop:Trn} = 0
    ?jobe:UPSFlagCode{prop:FontStyle} = font:Bold
    If ?job:JobService{prop:ReadOnly} = True
        ?job:JobService{prop:FontColor} = 65793
        ?job:JobService{prop:Color} = 15066597
    Elsif ?job:JobService{prop:Req} = True
        ?job:JobService{prop:FontColor} = 65793
        ?job:JobService{prop:Color} = 8454143
    Else ! If ?job:JobService{prop:Req} = True
        ?job:JobService{prop:FontColor} = 65793
        ?job:JobService{prop:Color} = 16777215
    End ! If ?job:JobService{prop:Req} = True
    ?job:JobService{prop:Trn} = 0
    ?job:JobService{prop:FontStyle} = font:Bold
    ?JOB:EDI_Batch_Number:Prompt{prop:FontColor} = -1
    ?JOB:EDI_Batch_Number:Prompt{prop:Color} = 15066597
    ?Loan_Exchange_Tab{prop:Color} = 15066597
    ?JOB:Loan_Unit_Number:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Unit_Number:Prompt{prop:Color} = 15066597
    If ?job:Loan_Unit_Number{prop:ReadOnly} = True
        ?job:Loan_Unit_Number{prop:FontColor} = 65793
        ?job:Loan_Unit_Number{prop:Color} = 15066597
    Elsif ?job:Loan_Unit_Number{prop:Req} = True
        ?job:Loan_Unit_Number{prop:FontColor} = 65793
        ?job:Loan_Unit_Number{prop:Color} = 8454143
    Else ! If ?job:Loan_Unit_Number{prop:Req} = True
        ?job:Loan_Unit_Number{prop:FontColor} = 65793
        ?job:Loan_Unit_Number{prop:Color} = 16777215
    End ! If ?job:Loan_Unit_Number{prop:Req} = True
    ?job:Loan_Unit_Number{prop:Trn} = 0
    ?job:Loan_Unit_Number{prop:FontStyle} = font:Bold
    ?JOB:Loan_User:Prompt{prop:FontColor} = -1
    ?JOB:Loan_User:Prompt{prop:Color} = 15066597
    If ?job:Loan_User{prop:ReadOnly} = True
        ?job:Loan_User{prop:FontColor} = 65793
        ?job:Loan_User{prop:Color} = 15066597
    Elsif ?job:Loan_User{prop:Req} = True
        ?job:Loan_User{prop:FontColor} = 65793
        ?job:Loan_User{prop:Color} = 8454143
    Else ! If ?job:Loan_User{prop:Req} = True
        ?job:Loan_User{prop:FontColor} = 65793
        ?job:Loan_User{prop:Color} = 16777215
    End ! If ?job:Loan_User{prop:Req} = True
    ?job:Loan_User{prop:Trn} = 0
    ?job:Loan_User{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Unit_Number:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Unit_Number:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Unit_Number{prop:ReadOnly} = True
        ?job:Exchange_Unit_Number{prop:FontColor} = 65793
        ?job:Exchange_Unit_Number{prop:Color} = 15066597
    Elsif ?job:Exchange_Unit_Number{prop:Req} = True
        ?job:Exchange_Unit_Number{prop:FontColor} = 65793
        ?job:Exchange_Unit_Number{prop:Color} = 8454143
    Else ! If ?job:Exchange_Unit_Number{prop:Req} = True
        ?job:Exchange_Unit_Number{prop:FontColor} = 65793
        ?job:Exchange_Unit_Number{prop:Color} = 16777215
    End ! If ?job:Exchange_Unit_Number{prop:Req} = True
    ?job:Exchange_Unit_Number{prop:Trn} = 0
    ?job:Exchange_Unit_Number{prop:FontStyle} = font:Bold
    ?JOB:Exchange_User:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_User:Prompt{prop:Color} = 15066597
    If ?job:Exchange_User{prop:ReadOnly} = True
        ?job:Exchange_User{prop:FontColor} = 65793
        ?job:Exchange_User{prop:Color} = 15066597
    Elsif ?job:Exchange_User{prop:Req} = True
        ?job:Exchange_User{prop:FontColor} = 65793
        ?job:Exchange_User{prop:Color} = 8454143
    Else ! If ?job:Exchange_User{prop:Req} = True
        ?job:Exchange_User{prop:FontColor} = 65793
        ?job:Exchange_User{prop:Color} = 16777215
    End ! If ?job:Exchange_User{prop:Req} = True
    ?job:Exchange_User{prop:Trn} = 0
    ?job:Exchange_User{prop:FontStyle} = font:Bold
    ?jobe:ExchangeReason:Prompt{prop:FontColor} = -1
    ?jobe:ExchangeReason:Prompt{prop:Color} = 15066597
    If ?jobe:ExchangeReason{prop:ReadOnly} = True
        ?jobe:ExchangeReason{prop:FontColor} = 65793
        ?jobe:ExchangeReason{prop:Color} = 15066597
    Elsif ?jobe:ExchangeReason{prop:Req} = True
        ?jobe:ExchangeReason{prop:FontColor} = 65793
        ?jobe:ExchangeReason{prop:Color} = 8454143
    Else ! If ?jobe:ExchangeReason{prop:Req} = True
        ?jobe:ExchangeReason{prop:FontColor} = 65793
        ?jobe:ExchangeReason{prop:Color} = 16777215
    End ! If ?jobe:ExchangeReason{prop:Req} = True
    ?jobe:ExchangeReason{prop:Trn} = 0
    ?jobe:ExchangeReason{prop:FontStyle} = font:Bold
    ?jobe:LoanReason:Prompt{prop:FontColor} = -1
    ?jobe:LoanReason:Prompt{prop:Color} = 15066597
    If ?jobe:LoanReason{prop:ReadOnly} = True
        ?jobe:LoanReason{prop:FontColor} = 65793
        ?jobe:LoanReason{prop:Color} = 15066597
    Elsif ?jobe:LoanReason{prop:Req} = True
        ?jobe:LoanReason{prop:FontColor} = 65793
        ?jobe:LoanReason{prop:Color} = 8454143
    Else ! If ?jobe:LoanReason{prop:Req} = True
        ?jobe:LoanReason{prop:FontColor} = 65793
        ?jobe:LoanReason{prop:Color} = 16777215
    End ! If ?jobe:LoanReason{prop:Req} = True
    ?jobe:LoanReason{prop:Trn} = 0
    ?jobe:LoanReason{prop:FontStyle} = font:Bold
    ?job:loan_courier:prompt{prop:FontColor} = -1
    ?job:loan_courier:prompt{prop:Color} = 15066597
    If ?job:Loan_Courier{prop:ReadOnly} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 15066597
    Elsif ?job:Loan_Courier{prop:Req} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 8454143
    Else ! If ?job:Loan_Courier{prop:Req} = True
        ?job:Loan_Courier{prop:FontColor} = 65793
        ?job:Loan_Courier{prop:Color} = 16777215
    End ! If ?job:Loan_Courier{prop:Req} = True
    ?job:Loan_Courier{prop:Trn} = 0
    ?job:Loan_Courier{prop:FontStyle} = font:Bold
    ?job:exchange_courier:prompt{prop:FontColor} = -1
    ?job:exchange_courier:prompt{prop:Color} = 15066597
    If ?job:Exchange_Courier{prop:ReadOnly} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 15066597
    Elsif ?job:Exchange_Courier{prop:Req} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 8454143
    Else ! If ?job:Exchange_Courier{prop:Req} = True
        ?job:Exchange_Courier{prop:FontColor} = 65793
        ?job:Exchange_Courier{prop:Color} = 16777215
    End ! If ?job:Exchange_Courier{prop:Req} = True
    ?job:Exchange_Courier{prop:Trn} = 0
    ?job:Exchange_Courier{prop:FontStyle} = font:Bold
    ?loan_model_make_temp:Prompt{prop:FontColor} = -1
    ?loan_model_make_temp:Prompt{prop:Color} = 15066597
    If ?loan_model_make_temp{prop:ReadOnly} = True
        ?loan_model_make_temp{prop:FontColor} = 65793
        ?loan_model_make_temp{prop:Color} = 15066597
    Elsif ?loan_model_make_temp{prop:Req} = True
        ?loan_model_make_temp{prop:FontColor} = 65793
        ?loan_model_make_temp{prop:Color} = 8454143
    Else ! If ?loan_model_make_temp{prop:Req} = True
        ?loan_model_make_temp{prop:FontColor} = 65793
        ?loan_model_make_temp{prop:Color} = 16777215
    End ! If ?loan_model_make_temp{prop:Req} = True
    ?loan_model_make_temp{prop:Trn} = 0
    ?loan_model_make_temp{prop:FontStyle} = font:Bold
    ?Exchange_Model_Make_temp:Prompt{prop:FontColor} = -1
    ?Exchange_Model_Make_temp:Prompt{prop:Color} = 15066597
    If ?Exchange_Model_Make_temp{prop:ReadOnly} = True
        ?Exchange_Model_Make_temp{prop:FontColor} = 65793
        ?Exchange_Model_Make_temp{prop:Color} = 15066597
    Elsif ?Exchange_Model_Make_temp{prop:Req} = True
        ?Exchange_Model_Make_temp{prop:FontColor} = 65793
        ?Exchange_Model_Make_temp{prop:Color} = 8454143
    Else ! If ?Exchange_Model_Make_temp{prop:Req} = True
        ?Exchange_Model_Make_temp{prop:FontColor} = 65793
        ?Exchange_Model_Make_temp{prop:Color} = 16777215
    End ! If ?Exchange_Model_Make_temp{prop:Req} = True
    ?Exchange_Model_Make_temp{prop:Trn} = 0
    ?Exchange_Model_Make_temp{prop:FontStyle} = font:Bold
    ?loan_esn_temp:Prompt{prop:FontColor} = -1
    ?loan_esn_temp:Prompt{prop:Color} = 15066597
    If ?loan_esn_temp{prop:ReadOnly} = True
        ?loan_esn_temp{prop:FontColor} = 65793
        ?loan_esn_temp{prop:Color} = 15066597
    Elsif ?loan_esn_temp{prop:Req} = True
        ?loan_esn_temp{prop:FontColor} = 65793
        ?loan_esn_temp{prop:Color} = 8454143
    Else ! If ?loan_esn_temp{prop:Req} = True
        ?loan_esn_temp{prop:FontColor} = 65793
        ?loan_esn_temp{prop:Color} = 16777215
    End ! If ?loan_esn_temp{prop:Req} = True
    ?loan_esn_temp{prop:Trn} = 0
    ?loan_esn_temp{prop:FontStyle} = font:Bold
    ?loan_msn_temp:Prompt{prop:FontColor} = -1
    ?loan_msn_temp:Prompt{prop:Color} = 15066597
    If ?loan_msn_temp{prop:ReadOnly} = True
        ?loan_msn_temp{prop:FontColor} = 65793
        ?loan_msn_temp{prop:Color} = 15066597
    Elsif ?loan_msn_temp{prop:Req} = True
        ?loan_msn_temp{prop:FontColor} = 65793
        ?loan_msn_temp{prop:Color} = 8454143
    Else ! If ?loan_msn_temp{prop:Req} = True
        ?loan_msn_temp{prop:FontColor} = 65793
        ?loan_msn_temp{prop:Color} = 16777215
    End ! If ?loan_msn_temp{prop:Req} = True
    ?loan_msn_temp{prop:Trn} = 0
    ?loan_msn_temp{prop:FontStyle} = font:Bold
    ?Exchange_Esn_temp:Prompt{prop:FontColor} = -1
    ?Exchange_Esn_temp:Prompt{prop:Color} = 15066597
    If ?Exchange_Esn_temp{prop:ReadOnly} = True
        ?Exchange_Esn_temp{prop:FontColor} = 65793
        ?Exchange_Esn_temp{prop:Color} = 15066597
    Elsif ?Exchange_Esn_temp{prop:Req} = True
        ?Exchange_Esn_temp{prop:FontColor} = 65793
        ?Exchange_Esn_temp{prop:Color} = 8454143
    Else ! If ?Exchange_Esn_temp{prop:Req} = True
        ?Exchange_Esn_temp{prop:FontColor} = 65793
        ?Exchange_Esn_temp{prop:Color} = 16777215
    End ! If ?Exchange_Esn_temp{prop:Req} = True
    ?Exchange_Esn_temp{prop:Trn} = 0
    ?Exchange_Esn_temp{prop:FontStyle} = font:Bold
    ?Exchange_Msn_temp:Prompt{prop:FontColor} = -1
    ?Exchange_Msn_temp:Prompt{prop:Color} = 15066597
    If ?Exchange_Msn_temp{prop:ReadOnly} = True
        ?Exchange_Msn_temp{prop:FontColor} = 65793
        ?Exchange_Msn_temp{prop:Color} = 15066597
    Elsif ?Exchange_Msn_temp{prop:Req} = True
        ?Exchange_Msn_temp{prop:FontColor} = 65793
        ?Exchange_Msn_temp{prop:Color} = 8454143
    Else ! If ?Exchange_Msn_temp{prop:Req} = True
        ?Exchange_Msn_temp{prop:FontColor} = 65793
        ?Exchange_Msn_temp{prop:Color} = 16777215
    End ! If ?Exchange_Msn_temp{prop:Req} = True
    ?Exchange_Msn_temp{prop:Trn} = 0
    ?Exchange_Msn_temp{prop:FontStyle} = font:Bold
    ?Exchange_Accessories_Temp:Prompt{prop:FontColor} = -1
    ?Exchange_Accessories_Temp:Prompt{prop:Color} = 15066597
    If ?Exchange_Accessories_Temp{prop:ReadOnly} = True
        ?Exchange_Accessories_Temp{prop:FontColor} = 65793
        ?Exchange_Accessories_Temp{prop:Color} = 15066597
    Elsif ?Exchange_Accessories_Temp{prop:Req} = True
        ?Exchange_Accessories_Temp{prop:FontColor} = 65793
        ?Exchange_Accessories_Temp{prop:Color} = 8454143
    Else ! If ?Exchange_Accessories_Temp{prop:Req} = True
        ?Exchange_Accessories_Temp{prop:FontColor} = 65793
        ?Exchange_Accessories_Temp{prop:Color} = 16777215
    End ! If ?Exchange_Accessories_Temp{prop:Req} = True
    ?Exchange_Accessories_Temp{prop:Trn} = 0
    ?Exchange_Accessories_Temp{prop:FontStyle} = font:Bold
    ?loan_group{prop:Font,3} = -1
    ?loan_group{prop:Color} = 15066597
    ?loan_group{prop:Trn} = 0
    ?JOB:Loan_Consignment_Number:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Consignment_Number:Prompt{prop:Color} = 15066597
    If ?job:Loan_Consignment_Number{prop:ReadOnly} = True
        ?job:Loan_Consignment_Number{prop:FontColor} = 65793
        ?job:Loan_Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Loan_Consignment_Number{prop:Req} = True
        ?job:Loan_Consignment_Number{prop:FontColor} = 65793
        ?job:Loan_Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Loan_Consignment_Number{prop:Req} = True
        ?job:Loan_Consignment_Number{prop:FontColor} = 65793
        ?job:Loan_Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Loan_Consignment_Number{prop:Req} = True
    ?job:Loan_Consignment_Number{prop:Trn} = 0
    ?job:Loan_Consignment_Number{prop:FontStyle} = font:Bold
    ?JOB:Loan_Despatched:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Despatched:Prompt{prop:Color} = 15066597
    If ?job:Loan_Despatched{prop:ReadOnly} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 15066597
    Elsif ?job:Loan_Despatched{prop:Req} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 8454143
    Else ! If ?job:Loan_Despatched{prop:Req} = True
        ?job:Loan_Despatched{prop:FontColor} = 65793
        ?job:Loan_Despatched{prop:Color} = 16777215
    End ! If ?job:Loan_Despatched{prop:Req} = True
    ?job:Loan_Despatched{prop:Trn} = 0
    ?job:Loan_Despatched{prop:FontStyle} = font:Bold
    ?JOB:Loan_Despatch_Number:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Despatch_Number:Prompt{prop:Color} = 15066597
    If ?job:Loan_Despatch_Number{prop:ReadOnly} = True
        ?job:Loan_Despatch_Number{prop:FontColor} = 65793
        ?job:Loan_Despatch_Number{prop:Color} = 15066597
    Elsif ?job:Loan_Despatch_Number{prop:Req} = True
        ?job:Loan_Despatch_Number{prop:FontColor} = 65793
        ?job:Loan_Despatch_Number{prop:Color} = 8454143
    Else ! If ?job:Loan_Despatch_Number{prop:Req} = True
        ?job:Loan_Despatch_Number{prop:FontColor} = 65793
        ?job:Loan_Despatch_Number{prop:Color} = 16777215
    End ! If ?job:Loan_Despatch_Number{prop:Req} = True
    ?job:Loan_Despatch_Number{prop:Trn} = 0
    ?job:Loan_Despatch_Number{prop:FontStyle} = font:Bold
    ?JOB:Loan_Despatched_User:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Despatched_User:Prompt{prop:Color} = 15066597
    If ?job:Loan_Despatched_User{prop:ReadOnly} = True
        ?job:Loan_Despatched_User{prop:FontColor} = 65793
        ?job:Loan_Despatched_User{prop:Color} = 15066597
    Elsif ?job:Loan_Despatched_User{prop:Req} = True
        ?job:Loan_Despatched_User{prop:FontColor} = 65793
        ?job:Loan_Despatched_User{prop:Color} = 8454143
    Else ! If ?job:Loan_Despatched_User{prop:Req} = True
        ?job:Loan_Despatched_User{prop:FontColor} = 65793
        ?job:Loan_Despatched_User{prop:Color} = 16777215
    End ! If ?job:Loan_Despatched_User{prop:Req} = True
    ?job:Loan_Despatched_User{prop:Trn} = 0
    ?job:Loan_Despatched_User{prop:FontStyle} = font:Bold
    ?JOB:Loan_Despatched_User:Prompt:2{prop:FontColor} = -1
    ?JOB:Loan_Despatched_User:Prompt:2{prop:Color} = 15066597
    If ?job:LoaService{prop:ReadOnly} = True
        ?job:LoaService{prop:FontColor} = 65793
        ?job:LoaService{prop:Color} = 15066597
    Elsif ?job:LoaService{prop:Req} = True
        ?job:LoaService{prop:FontColor} = 65793
        ?job:LoaService{prop:Color} = 8454143
    Else ! If ?job:LoaService{prop:Req} = True
        ?job:LoaService{prop:FontColor} = 65793
        ?job:LoaService{prop:Color} = 16777215
    End ! If ?job:LoaService{prop:Req} = True
    ?job:LoaService{prop:Trn} = 0
    ?job:LoaService{prop:FontStyle} = font:Bold
    ?Exchange_Group{prop:Font,3} = -1
    ?Exchange_Group{prop:Color} = 15066597
    ?Exchange_Group{prop:Trn} = 0
    ?JOB:Loan_Despatched_User:Prompt:3{prop:FontColor} = -1
    ?JOB:Loan_Despatched_User:Prompt:3{prop:Color} = 15066597
    If ?job:ExcService{prop:ReadOnly} = True
        ?job:ExcService{prop:FontColor} = 65793
        ?job:ExcService{prop:Color} = 15066597
    Elsif ?job:ExcService{prop:Req} = True
        ?job:ExcService{prop:FontColor} = 65793
        ?job:ExcService{prop:Color} = 8454143
    Else ! If ?job:ExcService{prop:Req} = True
        ?job:ExcService{prop:FontColor} = 65793
        ?job:ExcService{prop:Color} = 16777215
    End ! If ?job:ExcService{prop:Req} = True
    ?job:ExcService{prop:Trn} = 0
    ?job:ExcService{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Consignment_Number:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Consignment_Number:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Consignment_Number{prop:ReadOnly} = True
        ?job:Exchange_Consignment_Number{prop:FontColor} = 65793
        ?job:Exchange_Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Exchange_Consignment_Number{prop:Req} = True
        ?job:Exchange_Consignment_Number{prop:FontColor} = 65793
        ?job:Exchange_Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Exchange_Consignment_Number{prop:Req} = True
        ?job:Exchange_Consignment_Number{prop:FontColor} = 65793
        ?job:Exchange_Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Exchange_Consignment_Number{prop:Req} = True
    ?job:Exchange_Consignment_Number{prop:Trn} = 0
    ?job:Exchange_Consignment_Number{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Despatched:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Despatched:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Despatched{prop:ReadOnly} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 15066597
    Elsif ?job:Exchange_Despatched{prop:Req} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 8454143
    Else ! If ?job:Exchange_Despatched{prop:Req} = True
        ?job:Exchange_Despatched{prop:FontColor} = 65793
        ?job:Exchange_Despatched{prop:Color} = 16777215
    End ! If ?job:Exchange_Despatched{prop:Req} = True
    ?job:Exchange_Despatched{prop:Trn} = 0
    ?job:Exchange_Despatched{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Despatch_Number:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Despatch_Number:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Despatch_Number{prop:ReadOnly} = True
        ?job:Exchange_Despatch_Number{prop:FontColor} = 65793
        ?job:Exchange_Despatch_Number{prop:Color} = 15066597
    Elsif ?job:Exchange_Despatch_Number{prop:Req} = True
        ?job:Exchange_Despatch_Number{prop:FontColor} = 65793
        ?job:Exchange_Despatch_Number{prop:Color} = 8454143
    Else ! If ?job:Exchange_Despatch_Number{prop:Req} = True
        ?job:Exchange_Despatch_Number{prop:FontColor} = 65793
        ?job:Exchange_Despatch_Number{prop:Color} = 16777215
    End ! If ?job:Exchange_Despatch_Number{prop:Req} = True
    ?job:Exchange_Despatch_Number{prop:Trn} = 0
    ?job:Exchange_Despatch_Number{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Despatched_User:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Despatched_User:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Despatched_User{prop:ReadOnly} = True
        ?job:Exchange_Despatched_User{prop:FontColor} = 65793
        ?job:Exchange_Despatched_User{prop:Color} = 15066597
    Elsif ?job:Exchange_Despatched_User{prop:Req} = True
        ?job:Exchange_Despatched_User{prop:FontColor} = 65793
        ?job:Exchange_Despatched_User{prop:Color} = 8454143
    Else ! If ?job:Exchange_Despatched_User{prop:Req} = True
        ?job:Exchange_Despatched_User{prop:FontColor} = 65793
        ?job:Exchange_Despatched_User{prop:Color} = 16777215
    End ! If ?job:Exchange_Despatched_User{prop:Req} = True
    ?job:Exchange_Despatched_User{prop:Trn} = 0
    ?job:Exchange_Despatched_User{prop:FontStyle} = font:Bold
    ?Loan_Accessories_Temp:Prompt{prop:FontColor} = -1
    ?Loan_Accessories_Temp:Prompt{prop:Color} = 15066597
    If ?Loan_Accessories_Temp{prop:ReadOnly} = True
        ?Loan_Accessories_Temp{prop:FontColor} = 65793
        ?Loan_Accessories_Temp{prop:Color} = 15066597
    Elsif ?Loan_Accessories_Temp{prop:Req} = True
        ?Loan_Accessories_Temp{prop:FontColor} = 65793
        ?Loan_Accessories_Temp{prop:Color} = 8454143
    Else ! If ?Loan_Accessories_Temp{prop:Req} = True
        ?Loan_Accessories_Temp{prop:FontColor} = 65793
        ?Loan_Accessories_Temp{prop:Color} = 16777215
    End ! If ?Loan_Accessories_Temp{prop:Req} = True
    ?Loan_Accessories_Temp{prop:Trn} = 0
    ?Loan_Accessories_Temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597

    ?Sheet3{prop:Color} = 15066597
    ?estimate_tab{prop:Color} = 15066597
    ?job:Ignore_Estimate_Charges{prop:Font,3} = -1
    ?job:Ignore_Estimate_Charges{prop:Color} = 15066597
    ?job:Ignore_Estimate_Charges{prop:Trn} = 0
    ?JOB:Courier_Cost_Estimate:Prompt{prop:FontColor} = -1
    ?JOB:Courier_Cost_Estimate:Prompt{prop:Color} = 15066597
    If ?job:Courier_Cost_Estimate{prop:ReadOnly} = True
        ?job:Courier_Cost_Estimate{prop:FontColor} = 65793
        ?job:Courier_Cost_Estimate{prop:Color} = 15066597
    Elsif ?job:Courier_Cost_Estimate{prop:Req} = True
        ?job:Courier_Cost_Estimate{prop:FontColor} = 65793
        ?job:Courier_Cost_Estimate{prop:Color} = 8454143
    Else ! If ?job:Courier_Cost_Estimate{prop:Req} = True
        ?job:Courier_Cost_Estimate{prop:FontColor} = 65793
        ?job:Courier_Cost_Estimate{prop:Color} = 16777215
    End ! If ?job:Courier_Cost_Estimate{prop:Req} = True
    ?job:Courier_Cost_Estimate{prop:Trn} = 0
    ?job:Courier_Cost_Estimate{prop:FontStyle} = font:Bold
    ?JOB:Labour_Cost_Estimate:Prompt{prop:FontColor} = -1
    ?JOB:Labour_Cost_Estimate:Prompt{prop:Color} = 15066597
    If ?job:Labour_Cost_Estimate{prop:ReadOnly} = True
        ?job:Labour_Cost_Estimate{prop:FontColor} = 65793
        ?job:Labour_Cost_Estimate{prop:Color} = 15066597
    Elsif ?job:Labour_Cost_Estimate{prop:Req} = True
        ?job:Labour_Cost_Estimate{prop:FontColor} = 65793
        ?job:Labour_Cost_Estimate{prop:Color} = 8454143
    Else ! If ?job:Labour_Cost_Estimate{prop:Req} = True
        ?job:Labour_Cost_Estimate{prop:FontColor} = 65793
        ?job:Labour_Cost_Estimate{prop:Color} = 16777215
    End ! If ?job:Labour_Cost_Estimate{prop:Req} = True
    ?job:Labour_Cost_Estimate{prop:Trn} = 0
    ?job:Labour_Cost_Estimate{prop:FontStyle} = font:Bold
    ?job:Parts_Cost_Estimate{prop:FontColor} = -1
    ?job:Parts_Cost_Estimate{prop:Color} = 15066597
    ?String18:2{prop:FontColor} = -1
    ?String18:2{prop:Color} = 15066597
    ?job:Sub_Total_Estimate{prop:FontColor} = -1
    ?job:Sub_Total_Estimate{prop:Color} = 15066597
    ?Panel4:4{prop:Fill} = 15066597

    ?String19:2{prop:FontColor} = -1
    ?String19:2{prop:Color} = 15066597
    ?vat_estimate_temp{prop:FontColor} = -1
    ?vat_estimate_temp{prop:Color} = 15066597
    ?String20:2{prop:FontColor} = -1
    ?String20:2{prop:Color} = 15066597
    ?total_estimate_temp{prop:FontColor} = -1
    ?total_estimate_temp{prop:Color} = 15066597
    ?Panel4:3{prop:Fill} = 15066597

    ?Chargeable_Tab{prop:Color} = 15066597
    ?job:Ignore_Chargeable_Charges{prop:Font,3} = -1
    ?job:Ignore_Chargeable_Charges{prop:Color} = 15066597
    ?job:Ignore_Chargeable_Charges{prop:Trn} = 0
    ?JOB:Courier_Cost:Prompt{prop:FontColor} = -1
    ?JOB:Courier_Cost:Prompt{prop:Color} = 15066597
    If ?job:Courier_Cost{prop:ReadOnly} = True
        ?job:Courier_Cost{prop:FontColor} = 65793
        ?job:Courier_Cost{prop:Color} = 15066597
    Elsif ?job:Courier_Cost{prop:Req} = True
        ?job:Courier_Cost{prop:FontColor} = 65793
        ?job:Courier_Cost{prop:Color} = 8454143
    Else ! If ?job:Courier_Cost{prop:Req} = True
        ?job:Courier_Cost{prop:FontColor} = 65793
        ?job:Courier_Cost{prop:Color} = 16777215
    End ! If ?job:Courier_Cost{prop:Req} = True
    ?job:Courier_Cost{prop:Trn} = 0
    ?job:Courier_Cost{prop:FontStyle} = font:Bold
    ?JOB:Labour_Cost:Prompt{prop:FontColor} = -1
    ?JOB:Labour_Cost:Prompt{prop:Color} = 15066597
    If ?job:Labour_Cost{prop:ReadOnly} = True
        ?job:Labour_Cost{prop:FontColor} = 65793
        ?job:Labour_Cost{prop:Color} = 15066597
    Elsif ?job:Labour_Cost{prop:Req} = True
        ?job:Labour_Cost{prop:FontColor} = 65793
        ?job:Labour_Cost{prop:Color} = 8454143
    Else ! If ?job:Labour_Cost{prop:Req} = True
        ?job:Labour_Cost{prop:FontColor} = 65793
        ?job:Labour_Cost{prop:Color} = 16777215
    End ! If ?job:Labour_Cost{prop:Req} = True
    ?job:Labour_Cost{prop:Trn} = 0
    ?job:Labour_Cost{prop:FontStyle} = font:Bold
    ?job:parts_cost:prompt{prop:FontColor} = -1
    ?job:parts_cost:prompt{prop:Color} = 15066597
    ?job:Parts_Cost{prop:FontColor} = -1
    ?job:Parts_Cost{prop:Color} = 15066597
    ?String18{prop:FontColor} = -1
    ?String18{prop:Color} = 15066597
    ?job:Sub_Total{prop:FontColor} = -1
    ?job:Sub_Total{prop:Color} = 15066597
    ?Panel4{prop:Fill} = 15066597

    ?CharVat{prop:FontColor} = -1
    ?CharVat{prop:Color} = 15066597
    ?CharRejected{prop:FontColor} = -1
    ?CharRejected{prop:Color} = 15066597
    ?vat_chargeable_temp{prop:FontColor} = -1
    ?vat_chargeable_temp{prop:Color} = 15066597
    ?CharTotal{prop:FontColor} = -1
    ?CharTotal{prop:Color} = 15066597
    ?Total_Temp{prop:FontColor} = -1
    ?Total_Temp{prop:Color} = 15066597
    ?Panel4:2{prop:Fill} = 15066597

    ?Prompt106{prop:FontColor} = -1
    ?Prompt106{prop:Color} = 15066597
    ?paid_chargeable_temp{prop:FontColor} = -1
    ?paid_chargeable_temp{prop:Color} = 15066597
    ?String23{prop:FontColor} = -1
    ?String23{prop:Color} = 15066597
    ?balance_due_temp{prop:FontColor} = -1
    ?balance_due_temp{prop:Color} = 15066597
    ?Warranty_Tab{prop:Color} = 15066597
    ?job:Ignore_Warranty_Charges{prop:Font,3} = -1
    ?job:Ignore_Warranty_Charges{prop:Color} = 15066597
    ?job:Ignore_Warranty_Charges{prop:Trn} = 0
    ?JOB:Courier_Cost_Warranty:Prompt{prop:FontColor} = -1
    ?JOB:Courier_Cost_Warranty:Prompt{prop:Color} = 15066597
    If ?job:Courier_Cost_Warranty{prop:ReadOnly} = True
        ?job:Courier_Cost_Warranty{prop:FontColor} = 65793
        ?job:Courier_Cost_Warranty{prop:Color} = 15066597
    Elsif ?job:Courier_Cost_Warranty{prop:Req} = True
        ?job:Courier_Cost_Warranty{prop:FontColor} = 65793
        ?job:Courier_Cost_Warranty{prop:Color} = 8454143
    Else ! If ?job:Courier_Cost_Warranty{prop:Req} = True
        ?job:Courier_Cost_Warranty{prop:FontColor} = 65793
        ?job:Courier_Cost_Warranty{prop:Color} = 16777215
    End ! If ?job:Courier_Cost_Warranty{prop:Req} = True
    ?job:Courier_Cost_Warranty{prop:Trn} = 0
    ?job:Courier_Cost_Warranty{prop:FontStyle} = font:Bold
    ?JOB:Labour_Cost_Warranty:Prompt{prop:FontColor} = -1
    ?JOB:Labour_Cost_Warranty:Prompt{prop:Color} = 15066597
    If ?job:Labour_Cost_Warranty{prop:ReadOnly} = True
        ?job:Labour_Cost_Warranty{prop:FontColor} = 65793
        ?job:Labour_Cost_Warranty{prop:Color} = 15066597
    Elsif ?job:Labour_Cost_Warranty{prop:Req} = True
        ?job:Labour_Cost_Warranty{prop:FontColor} = 65793
        ?job:Labour_Cost_Warranty{prop:Color} = 8454143
    Else ! If ?job:Labour_Cost_Warranty{prop:Req} = True
        ?job:Labour_Cost_Warranty{prop:FontColor} = 65793
        ?job:Labour_Cost_Warranty{prop:Color} = 16777215
    End ! If ?job:Labour_Cost_Warranty{prop:Req} = True
    ?job:Labour_Cost_Warranty{prop:Trn} = 0
    ?job:Labour_Cost_Warranty{prop:FontStyle} = font:Bold
    ?job:parts_cost_warranty:prompt{prop:FontColor} = -1
    ?job:parts_cost_warranty:prompt{prop:Color} = 15066597
    ?job:Parts_Cost_Warranty{prop:FontColor} = -1
    ?job:Parts_Cost_Warranty{prop:Color} = 15066597
    ?Panel4:6{prop:Fill} = 15066597

    ?String18:3{prop:FontColor} = -1
    ?String18:3{prop:Color} = 15066597
    ?job:Sub_Total_Warranty{prop:FontColor} = -1
    ?job:Sub_Total_Warranty{prop:Color} = 15066597
    ?String19:3{prop:FontColor} = -1
    ?String19:3{prop:Color} = 15066597
    ?vat_warranty_temp{prop:FontColor} = -1
    ?vat_warranty_temp{prop:Color} = 15066597
    ?Panel4:5{prop:Fill} = 15066597

    ?failed_warranty_claim{prop:FontColor} = -1
    ?failed_warranty_claim{prop:Color} = 15066597
    ?String20:3{prop:FontColor} = -1
    ?String20:3{prop:Color} = 15066597
    ?Total_warranty_temp{prop:FontColor} = -1
    ?Total_warranty_temp{prop:Color} = 15066597
    ?Sheet5{prop:Color} = 15066597
    ?Tab13{prop:Color} = 15066597
    ?String9{prop:FontColor} = -1
    ?String9{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Prompt102{prop:FontColor} = -1
    ?Prompt102{prop:Color} = 15066597
    ?job:Status_End_Date{prop:FontColor} = -1
    ?job:Status_End_Date{prop:Color} = 15066597
    ?job:Status_End_Time{prop:FontColor} = -1
    ?job:Status_End_Time{prop:Color} = 15066597
    ?Prompt:Job:Current_Status{prop:FontColor} = -1
    ?Prompt:Job:Current_Status{prop:Color} = 15066597
    If ?job:Current_Status{prop:ReadOnly} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 15066597
    Elsif ?job:Current_Status{prop:Req} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 8454143
    Else ! If ?job:Current_Status{prop:Req} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 16777215
    End ! If ?job:Current_Status{prop:Req} = True
    ?job:Current_Status{prop:Trn} = 0
    ?job:Current_Status{prop:FontStyle} = font:Bold
    ?JOB:Exchange_Status:Prompt{prop:FontColor} = -1
    ?JOB:Exchange_Status:Prompt{prop:Color} = 15066597
    If ?job:Exchange_Status{prop:ReadOnly} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 15066597
    Elsif ?job:Exchange_Status{prop:Req} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 8454143
    Else ! If ?job:Exchange_Status{prop:Req} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 16777215
    End ! If ?job:Exchange_Status{prop:Req} = True
    ?job:Exchange_Status{prop:Trn} = 0
    ?job:Exchange_Status{prop:FontStyle} = font:Bold
    ?JOB:Loan_Status:Prompt{prop:FontColor} = -1
    ?JOB:Loan_Status:Prompt{prop:Color} = 15066597
    If ?job:Loan_Status{prop:ReadOnly} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 15066597
    Elsif ?job:Loan_Status{prop:Req} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 8454143
    Else ! If ?job:Loan_Status{prop:Req} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 16777215
    End ! If ?job:Loan_Status{prop:Req} = True
    ?job:Loan_Status{prop:Trn} = 0
    ?job:Loan_Status{prop:FontStyle} = font:Bold
    ?Prompt79{prop:FontColor} = -1
    ?Prompt79{prop:Color} = 15066597
    ?Prompt80{prop:FontColor} = -1
    ?Prompt80{prop:Color} = 15066597
    ?job_time_remaining_temp{prop:FontColor} = -1
    ?job_time_remaining_temp{prop:Color} = 15066597
    ?Prompt81{prop:FontColor} = -1
    ?Prompt81{prop:Color} = 15066597
    ?status_time_remaining_temp{prop:FontColor} = -1
    ?status_time_remaining_temp{prop:Color} = 15066597
    ?CancelText{prop:FontColor} = -1
    ?CancelText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
CheckRequiredFields     Routine
    If ForceTransitType('B')
        ?job:Transit_Type{prop:color} = 0BDFFFFH
        ?job:Transit_Type{prop:Req} = 1
    Else !If ForceTransitType('B')
        ?job:Transit_Type{prop:color} = color:white
        ?job:Transit_Type{prop:Req} = 0
    End !If ForceTransitType('B')

    If ForceIMEI('B')
        ?job:ESN{prop:color} = 0BDFFFFH
        ?job:ESN{prop:Req} = 1
    Else !If ForceIMEI('B')
        ?job:ESN{prop:color} = color:white
        ?job:ESN{prop:Req} = 0
    End !If ForceIMEI('B')

    ! Start Change 2941 BE(06/11/03)
    !If ForceMSN(job:Manufacturer,'B')
    If ForceMSN(job:Manufacturer,job:Workshop,'B')
    ! End Change 2941 BE(06/11/03)
        ?job:MSN{prop:color} = 0BDFFFFH
        ?job:MSN{prop:Req} = 1
    Else !If ForceMSN('B')
        ?job:MSN{prop:color} = color:white
        ?job:MSN{prop:Req} = 0
    End !If ForceMSN('B')

    If ForceModelNumber('B')
        ?job:Model_Number{prop:color} = 0BDFFFFH
        ?job:Model_Number{prop:Req} = 1
    Else !If ForceModelNumber('B')
        ?job:Model_Number{prop:color} = color:white
        ?job:Model_Number{prop:Req} = 0
    End !If ForceModelNumber('B')

    If ForceUnitType('B')
        ?job:Unit_Type{prop:color} = 0BDFFFFH
        ?job:Unit_Type{prop:Req} = 1
    Else !If ForceUnitType{'B')
        ?job:Unit_Type{prop:color} = color:white
        ?job:Unit_Type{prop:Req} = 0
    End !If ForceUnitType{'B')

    If ForceColour('B')
        ?job:Colour{prop:color} = 0BDFFFFH
        ?job:Colour{prop:Req} = 1
    Else !If ForceColour('B')
        ?job:Colour{prop:color} = color:white
        ?job:Colour{prop:Req} = 0
    End !If ForceColour('B')

    If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
        ?job:DOP{prop:color} = 0BDFFFFH
        ?job:DOP{prop:Req} = 1
    Else !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
        ?job:DOP{prop:color} = color:white
        ?job:DOP{prop:Req} = 0
    End !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')

    If ForceMobileNumber('B')
        ?job:Mobile_Number{prop:color} = 0BDFFFFH
        ?job:Mobile_Number{prop:Req} = 1
    Else !If ForceMobileNumber('B')
        ?job:Mobile_Number{prop:color} = color:white
        ?job:Mobile_Number{prop:Req} = 0
    End !If ForceMobileNumber('B')

    If ForceLocation(job:Transit_Type,job:Workshop,'B')
        ?job:Location{prop:color} = 0BDFFFFH
        ?job:Location{prop:Req} = 1
    Else !If ForceLocation(job:Transit_Type,job:Workshop,'B')
        ?job:Location{prop:color} = color:white
        ?job:Location{prop:Req} = 0
    End !If ForceLocation(job:Transit_Type,job:Workshop,'B')

    If ForceAuthorityNumber('B')
        ?job:Authority_Number{prop:color} = 0BDFFFFH
        ?job:Authority_Number{prop:Req} = 1
    Else !If ForceAuthorityNumber('B')
        ?job:Authority_Number{prop:color} = color:white
        ?job:Authority_Number{prop:Req} = 0
    End !If ForceAuthorityNumber('B')

    If ForceOrderNumber(job:Account_Number,'B')
        ?job:Order_Number{prop:color} = 0BDFFFFH
        ?job:Order_Number{prop:Req} = 1
    Else !If ForceOrderNumber(job:Account_Number,'B')
        ?job:Order_Number{prop:color} = color:white
        ?job:Order_Number{prop:Req} = 0
    End !If ForceOrderNumber(job:Account_Number,'B')

    If ForceCustomerName(job:Account_Number,'B')
        ?job:Initial{prop:color} = 0BDFFFFH
        ?job:Surname{prop:color} = 0BDFFFFH
        ?job:Title{prop:color} = 0BDFFFFH
        ?job:Surname{prop:Req} = 1
    Else !If ForceCustomerName(job:Account_Number,'B')
        ?job:Initial{prop:color} = color:white
        ?job:Surname{prop:color} = color:white
        ?job:Title{prop:color} = color:white
        ?job:Surname{prop:Req} = 0
    End !If ForceCustomerName(job:Account_Number,'B')

    If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = 0BDFFFFH
        ?job:incoming_consignment_number{prop:color} = 0BDFFFFH
        ?job:incoming_date{prop:color} = 0BDFFFFH
        ?job:Incoming_Courier{prop:Req} = 1
        ?job:Incoming_Consignment_Number{prop:Req} = 1
        ?job:Incoming_Date{prop:Req} = 1
    Else !If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = color:white
        ?job:incoming_consignment_number{prop:color} = color:white
        ?job:incoming_date{prop:color} = color:white
        ?job:Incoming_Courier{prop:Req} = 0
        ?job:Incoming_Consignment_Number{prop:Req} = 0
        ?job:Incoming_Date{prop:Req} = 0
    End !If ForceIncomingCourier('B')

    If ForceCourier('B')
        ?job:Courier{prop:color} = 0BDFFFFH
        ?job:Courier{prop:Req} = 1
    Else !If ForceCourier('B')
        ?job:Courier{prop:color} = color:White
        ?job:Courier{prop:Req} = 0
    End !If ForceCourier('B')

    If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = 0BDFFFFH
        ?job:Incoming_Courier{prop:Req} = 1
    Else !If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = color:White
        ?job:Incoming_Courier{prop:Req} = 0
    End !If ForceIncomingCourier('B')

    If ForceRepairType('B')
!        If ?job:Repair_Type{prop:Hide} = 0
            ?job:Repair_Type{prop:color} = 0BDFFFFH
            ?job:Repair_Type{prop:Req} = 1
!        Else !If ?job:Repair_Type{prop:Hide} = 0
!            ?job:Repair_Type{prop:color} = color:White
!            ?job:Repair_Type{prop:Req} = 0
!        End !If ?job:Repair_Type{prop:Hide} = 0
!        If ?job:Repair_Type_Warranty{prop:Hide} = 0
            ?job:Repair_Type_Warranty{prop:color} = 0BDFFFFH
            ?job:Repair_Type_Warranty{prop:Req} = 1
!        Else !If ?job:Repair_Type_Warranty{prop:Hide} = 0
!            ?job:Repair_Type_Warranty{prop:color} = color:White
!            ?job:Repair_Type_Warranty{prop:Req} = 0
!        End !If ?job:Repair_Type_Warranty{prop:Hide} = 0
    Else !If ForceRepairType('B')
        ?job:Repair_Type{prop:color} = color:White
        ?job:Repair_Type{prop:Req} = 0
        ?job:Repair_Type_Warranty{prop:color} = color:White
        ?job:Repair_Type_Warranty{prop:Req} = 0
    End !If ForceRepairType('B')

    If ForceNetwork('B')
        ?tmp:Network:2{prop:Req} = 1
        ?tmp:Network:2{prop:color} = 0BDFFFFH
    Else !If ForceNetwork('B')
        ?tmp:Network:2{prop:Req} = 0
        ?tmp:Network:2{prop:color} = color:White
    End !If ForceNetwork('B')
AddEngineer     Routine
    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
        tmp:SkillLevel   = use:SkillLevel
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign

    Do_Status# = 0
    
    If job:Engineer <> Engineer_Temp    
        IF job:Date_Completed <> ''        
            Error# = 1            
            Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                           'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            End!Case MessageEx
        End !IF job:Date_Completed <> ''        
        If job:Invoice_Number <> ''
            Error# = 1            
            Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                           'Styles\stop.ico','',0,0,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            End!Case MessageEx

        End !If job:Invoice_Number <> ''
        If Error# = 0
            If Engineer_Temp = ''
                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:notes         = 'SKILL LEVEL: ' & Clip(use:SkillLevel)
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    aud:type          = 'JOB'
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'ENGINEER ALLOCATED: ' & CLip(job:engineer)
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign

                If job:In_Repair <> 'YES'
                    job:In_Repair = 'YES'
                    job:Date_In_Repair = Today()
                    job:Time_In_Repair = Clock()
                End !If job:In_Repair <> 'YES'

                do_status# = 1
                engineer_temp = job:engineer

                If Access:JOBSENG.PrimeRecord() = Level:Benign
                    joe:JobNumber     = job:Ref_Number
                    joe:UserCode      = job:Engineer
                    joe:DateAllocated = Today()
                    joe:TimeAllocated = Clock()
                    joe:AllocatedBy   = use:User_Code
                    access:users.clearkey(use:User_Code_Key)
                    use:User_Code   = job:Engineer
                    access:users.fetch(use:User_Code_Key)

                    joe:EngSkillLevel = use:SkillLevel
                    joe:JobSkillLevel = use:SkillLevel

                    If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:JOBSENG.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:JOBSENG.TryInsert() = Level:Benign
                End !If Access:JOBSENG.PrimeRecord() = Level:Benign

            Else !If Engineer_Temp = ''
                Case MessageEx('Are you sure you want to change the Engineer?','ServiceBase 2000',|
                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                    Of 1 ! &Yes Button

                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:notes         = 'NEW ENGINEER: ' & CLip(job:Engineer) & |
                                                '<13,10>SKILL LEVEL: ' & use:SkillLevel & |
                                                '<13,10,13,10>PREVIOUS ENGINEER: ' & CLip(engineer_temp)
                            aud:ref_number    = job:ref_number
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = 'JOB'
                            access:users.clearkey(use:password_key)
                            use:password =glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            aud:action        = 'ENGINEER CHANGED TO ' & CLip(job:engineer)
                            access:audit.insert()
                        end!�if access:audit.primerecord() = level:benign
                        Engineer_Temp   = job:Engineer

                        If Access:JOBSENG.PrimeRecord() = Level:Benign
                            joe:JobNumber     = job:Ref_Number
                            joe:UserCode      = job:Engineer
                            joe:DateAllocated = Today()
                            joe:TimeAllocated = Clock()
                            joe:AllocatedBy   = use:User_Code
                            access:users.clearkey(use:User_Code_Key)
                            use:User_Code   = job:Engineer
                            access:users.fetch(use:User_Code_Key)

                            joe:EngSkillLevel = use:SkillLevel
                            joe:JobSkillLevel = jobe:SkillLevel

                            If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:JOBSENG.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:JOBSENG.TryInsert() = Level:Benign
                        End !If Access:JOBSENG.PrimeRecord() = Level:Benign

                    Of 2 ! &No Button
                        job:Engineer    = Engineer_Temp
                End!Case MessageEx
            End !If Engineer_Temp = ''
        Else !If Error# = 0
            job:Engineer    = engineer_temp
        End !If Error# = 0
        
    End !If job:Engineer <> Engineer_Temp    

    Access:USERS.Clearkey(use:User_Code_Key)
    use:User_Code   = job:Engineer
    If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Found
        Engineer_Name_temp  = Clip(Use:Forename) & ' ' & Clip(use:Surname)
        tmp:SkillLevel   = use:SkillLevel
    Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
    
    If do_status# = 1
        GetStatus(310,thiswindow.request,'JOB') !allocated to engineer
        brw21.resetsort(1)
        Do time_remaining
    End!If do_status# = 1

    Do Update_Engineer
AreThereNotes       Routine
    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found
        If Clip(jbn:Fault_Description) <> ''
            ?Fault_Description_Text_Button:2{prop:fontstyle} = font:bold
            ?Fault_Description_Text_Button{prop:text} = 'Fault Description Text - Filled'
            ?Fault_Description_Text_Button:2{prop:text} = 'Fault Desc.'
        Else!If jbn:FaultDescription <> ''
            ?Fault_Description_Text_Button{prop:text} = 'Fault Description Text - Empty'
            ?Fault_Description_Text_Button:2{prop:text} = 'Fault Description'
            ?Fault_Description_Text_Button:2{prop:fontstyle} = font:regular
        End!If jbn:FaultDescription <> ''
    Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
CountParts      Routine
    Clear(tmp:PartQueue)
    Free(tmp:PartQueue)

    Save_par_Id = Access:PARTS.Savefile()
    Access:PARTS.Clearkey(par:Part_Number_Key)
    par:Ref_Number  = job:Ref_Number
    Set(par:Part_Number_Key,par:Part_Number_Key)
    Loop
        If Access:PARTS.Next()
           Break
        End !If Access:PARTS.Next()
        If par:Ref_Number  <> job:Ref_Number
            Break
        End!If par:Ref_Number  <> job:Ref_Number

        tmp:PartNumber  = par:Part_Number
        Get(tmp:PartQueue,tmp:PartNumber)
        If Error()
            tmp:PartNumber  = par:Part_Number
            Add(tmp:PartQueue)
        End!If Error()
    End !loop
    Access:PARTS.Restorefile(Save_par_Id)

    ?chargeable_details_tab{prop:text} = '&Chargeable Parts (' & CLip(Records(tmp:Partqueue)) & ')'

    Clear(tmp:PartQueue)
    Free(tmp:PartQueue)

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        tmp:PartNumber  = wpr:part_number
        Get(tmp:PartQueue,tmp:PartNumber)
        If Error()
            tmp:Partnumber  = wpr:Part_number
            Add(tmp:PartQueue)
        End!If Error()
    end !loop
    access:warparts.restorefile(save_wpr_id)

    ?warranty_details_tab{prop:text} = '&Warranty Parts (' & CLip(Records(tmp:Partqueue)) & ')'
    Clear(tmp:PartQueue)
    Free(tmp:PartQueue)

    save_epr_id = access:estparts.savefile()
    access:estparts.clearkey(epr:part_number_key)
    epr:ref_number  = job:ref_number
    set(epr:part_number_key,epr:part_number_key)
    loop
        if access:estparts.next()
           break
        end !if
        if epr:ref_number  <> job:ref_number      |
            then break.  ! end if
        tmp:PartNumber  = epr:part_number
        Get(tmp:PartQueue,tmp:PartNumber)
        If Error()
            tmp:Partnumber  = epr:Part_number
            Add(tmp:PartQueue)
        End!If Error()
    end !loop
    access:estparts.restorefile(save_epr_id)

    ?estimate_details_tab{prop:text} = '&Estimate Parts (' & CLip(Records(tmp:Partqueue)) & ')'
    Clear(tmp:PartQueue)
    Free(tmp:PartQueue)

    
CityLink_Service        Routine
    Disable(?job:jobservice)
    Disable(?job:excservice)
    Disable(?job:loaservice)
    access:courier.clearkey(cou:courier_key)
    cou:courier = job:courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
            check_access('AMEND CITY LINK SERVICE',x")
            if x" = true
                Enable(?job:jobService)
            End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign

    access:courier.clearkey(cou:courier_key)
    cou:courier = job:exchange_courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
            check_access('AMEND CITY LINK SERVICE',x")
            if x" = true
                Enable(?job:excservice)
            End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign

    access:courier.clearkey(cou:courier_key)
    cou:courier = job:loan_courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
            check_access('AMEND CITY LINK SERVICE',x")
            if x" = true
                Enable(?job:loaservice)
            End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign

UPS_Service        Routine
    HIDE(?jobe:UPSFlagCode)
    access:courier.clearkey(cou:courier_key)
    cou:courier = job:incoming_courier
    If access:courier.tryfetch(cou:courier_key) = Level:Benign
        If cou:courier_type = 'UPS'
            !check_access('AMEND CITY LINK SERVICE',x")
            !if x" = true
                UNHIDE(?jobe:UPSFlagCode)
            !End!if x" = true
        End!If cou:courier_type = 'LABEL G' Or cou:courier_type = 'CITY LINK'
    End!If access:courier.clearkey(cou:courier_key) = Level:Benign
stock_history       Routine        !Do The Prime, and Set The Quantity First
    shi:ref_number      = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password        =glo:password
    access:users.fetch(use:password_key)
    shi:user            = use:user_code
    shi:date            = Today()
    shi:transaction_type    = 'DEC'
    shi:despatch_note_number    = par:despatch_note_number
    shi:job_number      = job:ref_number
    shi:purchase_cost   = par:purchase_cost
    shi:sale_cost       = par:sale_cost
    shi:retail_cost     = par:retail_cost
    shi:notes           = 'STOCK DECREMENTED'
    access:stohist.insert()
Skip_Despatch       Routine

    Hide(?Skip_Despatch_Validation)

    Check_Access('SKIP DESPATCH VALIDATION',x")
    If x" = True
        If job:date_Completed <> ''
            If job:despatched <> 'YES'
                Unhide(?skip_despatch_validation)
            End!If job:despatched <> 'YES'
        End!If job:date_Completed <> ''
    End!If x" = True
Check_For_Despatch        Routine
    Set(defaults)
    access:defaults.next()

    despatch# = 0

    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        despatch# = 1
    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                Else!If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                End!If tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'

    If despatch# = 1
        tmp:DespatchClose = 0
        If job:despatched <> 'YES'
            If ToBeLoaned() = 1
                restock# = 0
                If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                    Case MessageEx('A Loan Unit has been issued to this job but the Initial Transit Type has been set-up so that an Exchange Unit is required.<13,10><13,10>Do you wish to continue and mark this uni for Despatch Back to the Customer, or do you want to Restock it?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Continue|&Restock Unit',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Continue Button
                        Of 2 ! &Restock Unit Button
                            ForceDespatch()
                            restock# = 1
                    End!Case MessageEx
                End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                If restock# = 0
                    job:despatched  = 'LAT'
                    job:despatch_Type = 'JOB'
                End!If restock# = 0
            Else!If ToBeLoaned() = 1
                If ToBeExchanged()
                    ForceDespatch()
                Else!If ToBeExchanged()
                    job:date_despatched = DespatchANC(job:courier,'JOB')
                    access:courier.clearkey(cou:courier_key)
                    cou:courier = job:courier
                    if access:courier.tryfetch(cou:courier_key) = Level:benign
                        If cou:despatchclose = 'YES'
                            tmp:despatchclose = 1
                        Else!If cou:despatchclose = 'YES'
                            tmp:despatchclose = 0
                        End!If cou:despatchclose = 'YES'
                    End!if access:courier.tryfetch(cou:courier_key) = Level:benign
                End!If ToBeExchanged()
            End!If ToBeLoaned() = 1
        End!If job:despatched <> 'YES'
    End
Transit_Type_Bit        Routine
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        error# = 0
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                    error# = 1
                    Case MessageEx('Loan Units are not authorised for this Trade Account.<13,10><13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    job:transit_type    = transit_type_temp
                End!If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                    Case MessageEx('Exchange Units are not authorised for this Trade Account.<13,10><13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    job:transit_Type    = transit_type_temp
                    error# = 1
                End!If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES'
            end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
        If error# = 0
        
            access:trantype.clearkey(trt:transit_type_key)
            trt:transit_type = job:transit_type
            If access:trantype.fetch(trt:transit_type_key) = Level:Benign
                hide# = 0
                If trt:collection_address <> 'YES'
                    If job:address_line1 <> ''
                        Case MessageEx('The selected Transit Type will remove the Collection Address.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                            Of 1 ! &Yes Button

                                transit_type_temp   = job:transit_type
                                hide# = 1
                            Of 2 ! &No Button
                        
                                job:transit_type = transit_type_temp
                        End!Case MessageEx
                    Else
                        hide# = 1
                    End
                    If hide# = 1
                        job:postcode         = ''
                        job:company_name     = ''
                        job:address_line1    = ''
                        job:address_line2    = ''
                        job:address_line3    = ''
                        job:telephone_number = ''
                        job:fax_number       = ''
                        Hide(?collection_address_group)
                    End
                Else
                    Unhide(?collection_address_group)
                End
                hide# = 0
                If trt:delivery_address <> 'YES'
                    If job:address_line1_delivery <> ''
                        Case MessageEx('The selected Transit Type will remove the Delivery Address.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                            Of 1 ! &Yes Button

                                transit_type_temp   = job:transit_type
                                hide# = 1
                            Of 2 ! &No Button
                        
                                job:transit_type    = transit_type_temp
                        End!Case MessageExEnd!Case MessageEx
                    Else
                        hide# = 1
                    End
                    If hide# = 1
                        job:postcode_delivery         = ''
                        job:company_name_delivery     = ''
                        job:address_line1_delivery    = ''
                        job:address_line2_delivery    = ''
                        job:address_line3_delivery    = ''
                        job:telephone_delivery = ''
                        Hide(?delivery_address_group)
                    End
                Else
                    Unhide(?delivery_address_group)
                End
            end !if
            If trt:InWorkshop = 'YES'
                job:workshop = 'YES'
            Else!If trt:InWorkshop = 'YES'
                job:workshop = 'NO'
            End!If trt:InWorkshop = 'YES'
        
            If trt:location <> 'YES'
                Hide(?location_group)
                ?job:location{prop:req} = 0
            Else
                If ~def:HideLocation
                    If job:workshop = 'YES'
                        ?Location_Group{Prop:Hide} = 0
                    End
                    If trt:force_location = 'YES' And job:workshop = 'YES'
                        ?job:location{prop:req} = 1
                    Else!If trt:force_location = 'YES'
                        ?job:location{prop:req} = 0
                    End!If trt:force_location = 'YES'
                End !If ~def:HideLocation
            End
            If ThisWindow.Request = Insertrecord
                GetStatus(Sub(trt:initial_status,1,3),1,'JOB')

                Brw21.resetsort(1)
                Do time_remaining

                GetStatus(Sub(trt:Exchangestatus,1,3),1,'EXC')

                GetStatus(Sub(trt:loanstatus,1,3),1,'LOA')

                If trt:skip_workshop <> 'YES'
                    Unhide(?job:workshop)
                    ?tmp:InWorkshopDate{prop:Hide} = 0
                    ?tmp:InWorkshopTime{prop:Hide} = 0
                    Select(?job:workshop)
                End
                If trt:workshop_label = 'YES' and job:workshop = 'YES'
                    Unhide(?no_label_button)
                    print_label_temp = 'YES'
                Else
                    Hide(?no_label_button)
                    print_label_temp = 'NO'
                End

                access:subtracc.clearkey(sub:account_number_key)
                sub:account_number  = job:account_number
                access:subtracc.fetch(sub:account_number_key)
                access:tradeacc.clearkey(tra:account_number_key)
                tra:account_number  = sub:main_account_number
                access:tradeacc.fetch(tra:account_number_key)

                IF tra:refurbcharge = 'YES' and TRT:Exchange_Unit = 'YES'
                    job:chargeable_job = 'YES'
                    job:warranty_job    = 'YES'
                    job:charge_type = tra:ChargeType
                    job:Warranty_Charge_Type    = tra:WarChargeType
                    Unhide(?job:charge_type)
                    Unhide(?job:warranty_charge_type)
                    Unhide(?lookupwarrantychargetype)
                    Unhide(?lookupChargeType)
                End!IF tra:refurbcharge = 'YES'

                If trt:job_card = 'YES'
                    tmp:printjobcard    = 1
                Else!If trt:job_card = 'YES'
                    tmp:printjobcard    = 0
                End!If trt:job_card = 'YES'
                If trt:jobReceipt = 'YES'
                    tmp:printjobreceipt = 1
                Else!If trt:job_Receipt = 'YES'
                    tmp:printjobreceipt = 0
                End!If trt:job_Receipt = 'YES'

                error# = 0
                If job:turnaround_time <> '' and job:turnaround_time <> trt:initial_priority
                    Case MessageEx('The selected Transit Type requires a Turnaround Time of '&Clip(trt:initial_priority)&'.<13,10><13,10>A Turnaround Time has already been assigned to this job. Do you wish to change this?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                        Of 2 ! &No Button
                            error# = 1
                    End!Case MessageEx
                End!If job:turnaround_time <> ''
                If error# = 0
                    job:turnaround_time = trt:initial_priority
                    access:turnarnd.clearkey(tur:turnaround_time_key)
                    tur:turnaround_time = job:turnaround_time
                    if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
                        Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
                        job:turnaround_end_date = Clip(end_date")
                        job:turnaround_end_time = Clip(end_time")
                        Do Time_Remaining
                    end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
                End!If error# = 0
            End
        End!If error# = 0
    end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
Location_Bit     Routine
        job:location = loi:location
        If location_temp <> ''
    !Add To Old Location
            access:locinter.clearkey(loi:location_key)
            loi:location = location_temp
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    loi:current_spaces+= 1
                    loi:location_available = 'YES'
                    access:locinter.update()
                End
            end !if
        End!If location_temp <> ''
    !Take From New Location
        access:locinter.clearkey(loi:location_key)
        loi:location = job:location
        If access:locinter.fetch(loi:location_key) = Level:Benign
            If loi:allocate_spaces = 'YES'
                loi:current_spaces -= 1
                If loi:current_spaces< 1
                    loi:current_spaces = 0
                    loi:location_available = 'NO'
                End
                access:locinter.update()
            End
        end !if
        location_temp  = job:location

        If job:date_completed = ''
            If job:engineer <> ''
                GetStatus(310,1,'JOB') !allocated to engineer

                Brw21.resetsort(1)
                Do time_remaining
            Else!If job:engineer <> ''
                GetStatus(305,1,'JOB') !awaiting allocation

                Brw21.resetsort(1)
                Do time_remaining
            End!If job:engineer <> ''
        End!If job:date_completed = ''

        brw21.resetsort(1)
        


Show_Hide_Tabs      Routine
    check_access('JOBS - SHOW COSTS',x")
    If x" = False
        Hide(?chargeable_tab)
        Hide(?warranty_tab)
        Hide(?estimate_tab)
    Else!If x" = False
        If job:chargeable_job = 'YES'
            UnHide(?chargeable_tab)
            Unhide(?chargeable_details_tab)
            Unhide(?repair_type_chargeable_group)
        Else
            Hide(?chargeable_tab)
            Hide(?chargeable_details_tab)
            Hide(?repair_type_chargeable_group)
        End!If job:chargeable_job = 'YES'
        If job:warranty_job = 'YES'
            UnHide(?warranty_tab)
            Unhide(?warranty_details_tab)
            Unhide(?repair_type_warranty_group)
        Else
            Hide(?warranty_tab)
            Hide(?warranty_details_tab)
            Hide(?repair_type_warranty_group)
        End!If job:waranty_job = 'YES'
        If job:estimate = 'YES' and ?job:Estimate{prop:Hide} = 0
            If job:Chargeable_job = 'YES'
                Unhide(?EstimatePrompt)
                
                UnHide(?estimate_tab)
                Unhide(?estimate_details_tab)
                IF job:estimate_accepted <> 'YES' AND job:estimate_rejected <> 'YES'
                    Hide(?chargeable_tab)
                    Hide(?chargeable_details_tab)
                    Unhide(?repair_type_chargeable_group)
                End
            Else
                Hide(?Estimate_Tab)
                Hide(?Estimate_Details_Tab)
                hide(?EstimatePrompt)
                
            End!If job:Chargeable_job = 'YES'
        Else
            Hide(?estimate_tab)
            Hide(?estimate_details_tab)
            hide(?EstimatePrompt)
            
        End!If job:estimate = 'YES'
    End!If x" = False
    Thiswindow.reset(1)
Repair_Type_Bit     Routine
    price_error# = 0
    no_error# = 0
    If job:chargeable_job = 'YES'
        Pricing_Routine('C',labour",parts",pass",a")
        if pass" = False
            price_error# = 1
            Case MessageEx('A pricing structure has not been setup for this Repair Type.<13,10><13,10>If you choose to continue you must inform your System Supervisor.<13,10>Are you sure?','ServiceBase 2000',|
                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                Of 1 ! &Yes Button
                    no_error# = 2
                Of 2 ! &No Button
                    no_error# = 0
            End!Case MessageEx
        End!if pass" = False

    
        If price_error# = 0
            If repair_type_temp = ''
                no_error# = 1
            Else!If repair_type_temp = ''
                If job:labour_cost <> labour" Or job:parts_cost <> parts"
                    Case MessageEx('The selected Repair Type has a different pricing structure. If you choose to continue this job will be re-valued accordingly.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                            no_error# = 3 !pricing Change
                        Of 2 ! &No Button
                            no_error# = 0
                    End!Case MessageEx
                Else!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
                    Case MessageEx('Are you sure you want to change the Repair Type?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                             no_error# = 2 !Repair Type Change Only
                        Of 2 ! &No Button
                             no_error# = 0
                    End!Case MessageEx
                End!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
            End!If repair_type_temp = ''
        End!price_error# = 0
    
        If no_error# = 0
            job:repair_type = repair_type_temp
        Else!If no_error# = 0
            Case no_error#
                Of 2 orof 3
                    get(audit,0)
                    aud:ref_number = job:ref_number
                    aud:date       = Today()
                    aud:time       = Clock()
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
            End!Case no_error#
            Case no_error#
                Of 2
                    aud:notes      = 'NEW CHARGEABLE REPAIR TYPE: ' & Clip(job:repair_type) & '<13,10>OLD CHARGEABLE REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
                    aud:action     = 'CHARGEABLE REPAIR TYPE CHANGED: ' & Clip(job:repair_type)
                    access:audit.insert()
                Of 3
                    aud:notes      = 'NEW CHARGEABLE REPAIR TYPE: ' & Clip(job:repair_type) & '<13,10>OLD CHARGEABLE REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
                    aud:action     = 'REPRICE JOB - CHARGEABLE REPAIR TYPE CHANGED: ' & Clip(job:repair_type)
                    access:audit.insert()
            End
            If no_error# = 1 Or no_error# = 3
                Do pricing
            End!If no_error# = 1 Or no_error# = 3
            repair_type_temp    = job:repair_type
    
            Do third_party_repairer
            Post(Event:accepted,?job:charge_type)
        End!If no_error# = 0
    End!If job:chargeable_job = 'YES'
Repair_Type_Warranty_Bit     Routine
    price_error# = 0
    no_error# = 0
    If job:warranty_job = 'YES'
        Pricing_Routine('W',labour",parts",pass",a")
        if pass" = False
            price_error# = 1
            Case MessageEx('A pricing structure has not been setup for this Repair Type.<13,10><13,10>If you choose to continue you must inform your System Supervisor immediately.<13,10>Are you sure?','ServiceBase 2000',|
                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                Of 1 ! &Yes Button
                    no_error# = 2
                Of 2 ! &No Button
                    no_error# = 0
            End!Case MessageEx
        End!if pass" = False

    
        If price_error# = 0
            If repair_type_temp = ''
                no_error# = 1
            Else!If repair_type_temp = ''
                If job:labour_cost_warranty <> labour" Or job:parts_cost_warranty <> parts"
                    Case MessageEx('The selected Repair Type has a different pricing structure.<13,10><13,10>If you choose to continue this job will be re-valued accordingly.<13,10>Are you sure?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                            no_error# = 3 !pricing Change
                        Of 2 ! &No Button
                            no_error# = 0
                    End!Case MessageEx
                Else!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
                    Case MessageEx('Are you sure you want to change the Repair Type?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Yes Button
                             no_error# = 2 !Repair Type Change Only
                        Of 2 ! &No Button
                             no_error# = 0
                    End!Case MessageEx
                End!If job:labour_cost <> labour" Or job:parts_cost <> parts" Or |
            End!If repair_type_temp = ''
        End!price_error# = 0
    
        If no_error# = 0
            job:repair_type_warranty = repair_type_temp
        Else!If no_error# = 0
            Case no_error#
                Of 2 orof 3
                    get(audit,0)
                    aud:ref_number = job:ref_number
                    aud:date       = Today()
                    aud:time       = Clock()
                    access:users.clearkey(use:password_key)
                    use:password =glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
            End!Case no_error#
            Case no_error#
                Of 2
                    aud:notes      = 'NEW WARRANTY REPAIR TYPE: ' & Clip(job:repair_type_warranty) & '<13,10>OLD WARRANTY REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
                    aud:action     = 'WARRANTY REPAIR TYPE CHANGED: ' & Clip(job:repair_type_warranty)
                    access:audit.insert()
                Of 3
                    aud:notes      = 'NEW WARRANTY REPAIR TYPE: ' & Clip(job:repair_type_warranty) & '<13,10>OLD WARRANTY REPAIR TYPE NUMBER: ' & Clip(repair_type_temp)
                    aud:action     = 'REPRICE JOB - WARRANTY REPAIR TYPE CHANGED: ' & Clip(job:repair_type_warranty)
                    access:audit.insert()
            End
            If no_error# = 1 Or no_error# = 3
                Do pricing
            End!If no_error# = 1 Or no_error# = 3
            repair_type_temp    = job:repair_type_warranty
    
            Do third_party_repairer
            Post(Event:accepted,?job:charge_type)
        End!If no_error# = 0
    End!If job:chargeable_job = 'YES'
Trade_Account_Lookup_Bit        Routine
Check_Parts     Routine
    If job:Date_Completed = ''
        !Only change Status if job is NOT completed.

        found_requested# = 0
        found_ordered# = 0
        found_received# = 0
        setcursor(cursor:wait)
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            If wpr:pending_ref_number <> ''
                found_requested# = 1
                Break
            End                                     
            If wpr:order_number <> ''
                If wpr:date_received = ''
                    found_ordered# = 1
                    !Break  !Check all the parts. Don't break - TrkBs: 5057 (DBH: 07-12-2004)
                Else!If wpr:date_received = ''
                    found_received# = 1
                    !Break  !Check all the parts. Don't break - TrkBs: 5057 (DBH: 07-12-2004)
                End!If wpr:date_received = ''
            End
        end !loop
        access:warparts.restorefile(save_wpr_id)
        setcursor()

        If found_requested# = 0
            setcursor(cursor:wait)
            save_par_id = access:parts.savefile()
            access:parts.clearkey(par:part_number_key)
            par:ref_number  = job:ref_number
            set(par:part_number_key,par:part_number_key)
            loop
                if access:parts.next()
                   break
                end !if
                if par:ref_number  <> job:ref_number      |
                    then break.  ! end if
                If par:pending_ref_number <> ''
                    found_requested# = 1
                    Break
                End
                If par:order_number <> ''
                    If par:date_received = ''
                        found_ordered# = 1
                        !Break  !Check all the parts. Don't break - TrkBs: 5057 (DBH: 07-12-2004)
                    Else!If par:date_received = ''
                        found_received# = 1
                        !Break  !Check all the parts. Don't break - TrkBs: 5057 (DBH: 07-12-2004)
                    End!If par:date_received = ''
                End
            end !loop
            access:parts.restorefile(save_par_id)
            setcursor()
        End!If found_requested# = 0

        If found_requested# = 1
            GetStatus(330,thiswindow.request,'JOB') !spares requested
            ! Start Change 4145 BE(21/04/04)
            brw21.resetsort(1)
            ! End Change 4145 BE(21/04/04)
            Do time_remaining
            
        Else!If found_requested# = 1
            If found_ordered# = 1
                GetStatus(335,thiswindow.request,'JOB') !spares ordered
                brw21.resetsort(1)
                Do time_remaining
            Else!If found_ordered# = 1
                If found_received# = 1
                    GetStatus(345,thiswindow.request,'JOB') !spares received

                    brw21.resetsort(1)
                    Do time_remaining
                End!If found_received# = 1
            End!If found_ordered# = 1
        End!If found_requested# = 1

    End !If job:Date_Completed = ''

    Display()

Trade_Account_Status        Routine  ! Trade Account Status Display
!    access:subtracc.clearkey(sub:account_number_key)
!    sub:account_number = job:account_number
!    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!        access:tradeacc.clearkey(tra:account_number_key)
!        tra:account_number = sub:main_account_Number
!        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!            If tra:invoice_sub_accounts = 'YES'
!                If sub:stop_account = 'YES'
!                    If sub:allow_cash_sales = 'YES'
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP - ALLOW CASH TRANSACTIONS ONLY'
!                    Else
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP'
!                    End
!                End
!
!            Else !If tra:invoice_sub_accounts = 'YES'
!                If tra:stop_account = 'YES'
!                    If tra:allow_cash_sales = 'YES'
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP - ALLOW CASH TRANSACTIONS ONLY'
!                    Else
!                        Unhide(?trade_account_status)
!                        ?trade_account_status{prop:text} = 'ACCOUNT ON STOP'
!                    End
!                End
!            End !If tra:invoice_sub_accounts = 'YES'
!
!            If tra:use_contact_name = 'YES'
!            End
!        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!    end !if access:subtracc.fetch(sub:account_number_key) = Level:Benign
Show_Tabs       Routine   !Loan Exchange Tabs

    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = job:account_number
    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
        access:tradeacc.clearkey(tra:account_number_key)
        tra:account_number = sub:main_account_number
        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
            If tra:allow_loan <> 'YES' And tra:allow_exchange <> 'YES'
                hide(?loan_exchange_tab)
            Else!If tra:allow_loan <> 'YES' And tra:allow_exchange <> 'YES'
                Unhide(?loan_exchange_tab)
            End!If tra:allow_loan <> 'YES' And tra:allow_exchange <> 'YES'
        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
    end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign



Estimate_Check      Routine     !Estiamte Status Display
Check_Msn       Routine   !Does Manufacturer Use MSN Number
    esn_fail_temp = 'NO'
    msn_fail_temp = 'NO'
    Access:manufact.Clearkey(man:manufacturer_key)
    man:manufacturer    = job:manufacturer
    If Access:manufact.Fetch(man:manufacturer_key) = Level:Benign

        ! Start Change 4239 BE(20/05/04)
        MSN_Field_Mask = man:MSNFieldMask
        ! End Change 4239 BE(20/05/04)

        If man:use_msn = 'YES'
            Unhide(?job:msn)
            Unhide(?job:msn:prompt)
            If thiswindow.request   = Insertrecord
                Set(defaults)
                access:defaults.next()
                If def:force_msn    = 'B'
                    ?job:msn{prop:req}  = 1
                Else!If def:force_msn    = 'B'
                    ?job:msn{prop:req}  = 0
                End!If def:force_msn    = 'B'
            End!If thiswindow.request   = Insertrecord
        Else
            Hide(?job:msn)
            Hide(?job:msn:prompt)
        End

        If CheckLength('IMEI',job:Model_Number,job:ESN)
            ESN_Fail_Temp = 'YES'
            job:ESN = ''
        End !If CheckLength('IMEI',job:Model_Number,job:ESN)

        If ?job:MSN{prop:Hide} = 0
            ! Start Change 4239 BE(20/05/04)
            !IF (MSN_Field_Mask <> '') THEN
            IF (job:warranty_job = 'YES' AND MSN_Field_Mask <> '') THEN
                IF ((job:MSN <> '') AND (CheckFaultFormat(job:MSN, MSN_Field_Mask))) THEN
                    MSN_Fail_Temp = 'YES'
                    job:MSN = ''
                END
            END
            ! End Change 4239 BE(20/05/04)
            If CheckLength('MSN',job:Model_Number,job:MSN)
                MSN_Fail_Temp = 'YES'
                job:MSN = ''
            End !If CheckLength('MSN',job:Model_Number,job:MSN)
        End !If ?job:MSN{prop:Hide} = 0
    End !If Access:manufact.Fetch(man:manufacturer_key) = Level:Benign

Titles      Routine     !Show Date Booked etc.
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    If job:batch_number <> ''
        ?title_text{prop:text} ='Repair Details - Job No: ' & Clip(Format(job:ref_number,@p<<<<<<#p)) & |
                                '  Batch No: ' & Clip(Format(job:batch_number,@p<<<<<<#p))
    Else!If job:batch_number <> ''
        ?title_text{prop:text} ='Repair Details - Job No: ' & Clip(Format(job:ref_number,@p<<<<<<#p))
    End!If job:batch_number <> ''

    ?batch_number_text{prop:text} = 'Date Booked: ' &  Clip(Format(job:date_booked,@d6b)) & ' ' & |
                                    Clip(Format(job:time_booked,@t1b)) & ' - ' & Clip(job:who_booked)
    ?exchanged_title{prop:text} = ''
    If job:exchange_unit_number <> ''
        ?exchanged_title{prop:text} = 'Customer''s Unit Exchanged'
    End!If job:exchange_unit_number <> ''
    If job:loan_unit_number <> ''
        ?exchanged_title{prop:text} = 'Loan Unit Issued'
    End!If job:loan_unit_number <> ''

    Display()

    If job:date_completed <> ''
        job:completed = 'YES'
    Else
        job:completed = 'NO'
    End

    trade_account_string_temp = Clip(job:account_number) & ' - ' & Clip(job:company_name)
    If job:title <> '' and job:initial <> ''
        customer_name_String_temp = Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname)
    End!If job:title <> '' and job:initial <> ''
    If job:title = '' And job:initial <> ''
        customer_name_string_temp = Clip(job:initial) & ' ' & Clip(job:Surname)
    End!If job:title = '' And job:initial <> ''
    If job:title <> '' And job:initial = ''
        customer_name_String_temp = Clip(job:title) & ' ' & Clip(job:surname)
    End!If job:title <> '' And job:initial = ''
    If job:title = '' And job:initial = ''
        customer_name_string_temp = Clip(job:surname)
    End!If job:title = '' And job initial = ''

    If job:chargeable_job = 'YES'
        Unhide(?chargeable_type_string)
        Unhide(?job:charge_type:2)
    End
    If job:warranty_job = 'YES'
        Unhide(?Warranty_charge_Type_string)
        Unhide(?job:warranty_charge_type:2)
    End
    If job:location <> '' And job:workshop = 'YES'
        If ~def:HideLocation
            Unhide(?location_string)
            UnHide(?job:location:2)
        End !If ~def:HideLocation
    End
    If job:workshop <> '' And job:third_party_site <> ''
        Unhide(?third_party_string)
        Unhide(?job:third_party_site:2)
    End!If job:workshop = '' And job:third_party_site <> ''

    If job:special_instructions <> ''
        Unhide(?special_instructions_string)
        Unhide(?job:special_instructions:2)
    End

    ! Start Change  2821 BE(24/06/03)
    !bouncers# = 0
    !bouncers# = CountBouncer(job:ref_number,job:date_booked,job:esn)
    !If bouncers#
    !    Unhide(?BouncerText)
    !    Unhide(?Button62)
    !    ?bouncertext{prop:text} = Clip(bouncers#) & ' BOUNCER(S)'
    !Else!If bouncers#
    !    Hide(?BouncerText)
    !    Hide(?Button62)
    !End!If bouncers#
    Temp_255String = ''
    bouncers# = CountBouncer(job:ref_number,job:date_booked,job:esn)
    IF (bouncers# = 1) THEN
        Temp_255String = '1 IMEI BOUNCER'
    ELSIF (bouncers# > 1) THEN
        Temp_255String = Clip(bouncers#) & ' IMEI BOUNCERS'
    END
    bouncers# = CountMobileBouncer(job:ref_number,job:date_booked,job:Mobile_Number)
    IF (bouncers# = 1) THEN
        IF (Temp_255String = '') THEN
            Temp_255String = CLIP(temp_255String) & '1 MOBILE BOUNCER'
        ELSE
            Temp_255String = CLIP(temp_255String) & ' 1 MOBILE BOUNCER'
        END
    ELSIF (bouncers# > 1) THEN
        IF (Temp_255String = '') THEN
            Temp_255String = CLIP(temp_255String) & Clip(bouncers#) & ' MOBILE BOUNCERS'
        ELSE
            Temp_255String = CLIP(temp_255String) & ' ' & Clip(bouncers#) & ' MOBILE BOUNCERS'
        END
    END
    IF (Temp_255String <> '') THEN
        Unhide(?BouncerText)
        Unhide(?Button62)
        ?bouncertext{prop:text} = CLIP(Temp_255String)
    ELSE
        Hide(?BouncerText)
        Hide(?Button62)
    END
    ! End Change  2821 BE(24/06/03)

    !IMEIs
    !Exchange Unit
    tmp:ExchangeIMEI   = 'N/A'
    tmp:ExchangeMSN    = 'N/A'
    If job:Exchange_Unit_Number <> 0
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Found
            tmp:ExchangeIMEI   = xch:ESN
            tmp:ExchangeMSN    = xch:MSN
        Else!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
    End !If job:Exchange_Unit_Number <> 0

    !Third Party
    ! Start Change 2116 BE(25/06/03)
    !tmp:IncomingIMEI    = job:Esn
    access:JOBSE.clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
      IF (access:JOBSE.primerecord() = Level:Benign) THEN
          jobe:RefNumber  = job:Ref_Number
          IF (access:JOBSE.tryinsert()) THEN
              access:JOBSE.cancelautoinc()
          END
      END
    END
    IF (jobe:Pre_RF_Board_IMEI <> '') THEN
        tmp:IncomingIMEI = jobe:Pre_RF_Board_IMEI
    ELSE
        tmp:IncomingIMEI = job:Esn
    END
    ! End Change 2116 BE(25/06/03)
    tmp:IncomingMSN     = job:Msn
    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
    jot:RefNumber = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefNumberKey)
    If Access:JOBTHIRD.NEXT() = Level:Benign
        If job:Ref_Number = jot:RefNumber
            tmp:IncomingIMEI    = jot:OriginalIMEI
            tmp:IncomingMSN     = jot:OriginalMSN
            !Not implimented yet
        End !If job:Ref_Number = jot:RefNumber
    End !If Access:JOBTHIRD.NEXT() = Level:Benign
    If tmp:IncomingIMEI = ''
        tmp:IncomingIMEI = 'N/A'
    End !If tmp:IncomingIMEI = ''
    If tmp:IncomingMSN  = ''
        tmp:IncomingMSN = 'N/A'
    End !If tmp:IncomingMSN  = ''


    !Estimate Details
    If job:Estimate = 'YES' and ?job:Estimate{prop:Hide} = 0
        tmp:EstimateDetails = 'YES     If Over: ' & Clip(job:Estimate_If_Over) & '   Status: '
        If job:Estimate_Accepted = 'YES'
            tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' Accepted'
        Else !If job:EstimateAccepted = 'YES'
            If job:Estimate_Rejected = 'YES'
                ! Start Change 3874 BE(6/2/04)
                !tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' Accepted'
                tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' Rejected'
                ! End Change 3874 BE(6/2/04)
            Else !If job:Estimate_Rejected = 'YES'
                tmp:EstimateDetails = Clip(tmp:EstimateDetails) & ' In Progress'
            End !If job:Estimate_Rejected = 'YES'
        End !If job:EstimateAccepted = 'YES'
    Else
        tmp:EstimateDetails = ''
    End !If job:Estimate = 'YES'




Totals      Routine   !Work Out The Financial Stuff

    Total_Price('C',vat",total",balance")
    job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
    total_temp = total"
    vat_chargeable_temp = vat"
    balance_due_temp = balance"

    Total_Price('E',vat",total",balance")
    job:sub_total_estimate = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate
    total_estimate_temp = total"
    vat_estimate_temp = vat"

    Total_Price('W',vat",total",balance")
    job:sub_total_warranty  = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
    vat_warranty_temp = vat"
    total_warranty_temp = total"
    balance_due_warranty_temp = balance"
    Display()

Update_Engineer    Routine  !Engineers Full Name
    !Engineer
    ! Start Change ????SBJ01 Eng Date Allocation BE(30/04/03)
    IF (CLIP(job:Engineer) <> '') THEN
    ! End Change ????SBJ01 Eng Date Allocation BE(30/04/03)
        Save_joe_ID = Access:JOBSENG.SaveFile()

        ! Start Change ????SBJ01 Eng Date Allocation BE(30/04/03)
        !Access:JOBSENG.ClearKey(joe:UserCodeOnlyKey)
        !joe:UserCode      = job:Engineer
        !joe:DateAllocated = Today()
        !Set(joe:UserCodeOnlyKey,joe:UserCodeOnlyKey)
        !
        !  Goodness knows what this deleted code was supposed to do!!!
        !  Result was a completely arbitrary slection of an Engineer record with the same name but
        !  not necessarily the same job!!!!
        !
        Access:JOBSENG.ClearKey(joe:UserCodeKey)
        joe:JobNumber      = job:Ref_Number
        joe:UserCode      = job:Engineer
        joe:DateAllocated = Today()
        Set(joe:UserCodeKey, joe:UserCodeKey)
        ! End Change ????SBJ01 Eng Date Allocation BE(30/04/03)

        Loop

            ! Start Change ????SBJ01 Eng Date Allocation BE(30/04/03)
            !If Access:JOBSENG.PREVIOUS()
            !    Break
            !End !If
            !If joe:UserCode      <> job:Engineer      |
            !Or joe:DateAllocated > Today()      |
            !    Then Break.  ! End If
            !
            ! Note: joe:JobNumberKey is in Date/Time Order DESCENDING!!
            IF (Access:JOBSENG.Previous() <> Level:Benign) THEN
                BREAK
            END
            IF ((joe:JobNumber <> job:Ref_Number) OR |
                 (joe:UserCode <> job:Engineer) OR |
                 (joe:DateAllocated > Today())) THEN
                BREAK
            END
            ! End Change ????SBJ01 Eng Date Allocation BE(30/04/03)

            tmp:SkillLevel      = joe:EngSkillLevel
            tmp:DateAllocated   = joe:DateAllocated
            Break
        End !Loop
        Access:JOBSENG.RestoreFile(Save_joe_ID)

        Access:users.Clearkey(use:user_code_key)
        use:user_code    = job:engineer
        If Access:users.Fetch(use:user_Code_Key) = Level:Benign
            engineer_name_temp   = Clip(use:forename) & ' ' & Clip(use:surname)
!           tmp:SkillLevel  = use:SkillLevel
        End
    ! Start Change ????SBJ01 Eng Date Allocation BE(30/04/03)
    ELSE
        tmp:SkillLevel      = 0
        tmp:DateAllocated   = ''
        engineer_name_temp  = ''
    END
    ! End Change ????SBJ01 Eng Date Allocation BE(30/04/03)
    Display()

Update_Loan     Routine     !Sort Out Loan And Exchange Information
    If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?jobe:LoanReason{prop:Hide} = 0
        ?jobe:LoanReason:Prompt{prop:Hide} = 0
        ?LookupLoanReason{prop:Hide} = 0
    Else !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?jobe:LoanReason{prop:Hide} = 1
        ?jobe:LoanReason:Prompt{prop:Hide} = 1
        ?LookupLoanReason{prop:Hide} = 1
    End !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    If job:loan_unit_number = ''
        job:loan_user = ''
        loan_model_make_temp = ''
        loan_esn_temp = ''
        loan_msn_temp = ''
        Loan_Accessories_Temp = ''
    Else
        count# = 0
        setcursor(cursor:wait)
        save_lac_id = access:loanacc.savefile()
        access:loanacc.clearkey(lac:ref_number_key)
        lac:ref_number = loa:ref_number
        set(lac:ref_number_key,lac:ref_number_key)
        loop
            if access:loanacc.next()
               break
            end !if
            if lac:ref_number <> loa:ref_number      |
                then break.  ! end if
            count# += 1
        end !loop
        access:loanacc.restorefile(save_lac_id)
        setcursor()

        If count# = 0
            Loan_Accessories_Temp = ''
        End!If count# = 0
        If count# > 1
            loan_accessories_temp = count#
        End!If count# > 1

        access:loan.clearkey(loa:ref_number_key)
        loa:ref_number = job:loan_unit_number
        if access:loan.fetch(loa:ref_number_key) = Level:Benign
            loan_model_make_temp    = Clip(loa:ref_number) & ': ' & Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
            loan_esn_temp           = loa:esn
            loan_msn_temp           = loa:msn
        end !if access:loan.fetch(loa:ref_number_key) = Level:Benign
    End

Update_Exchange     Routine
    If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?jobe:ExchangeReason{prop:Hide} = 0
        ?jobe:ExchangeReason:Prompt{prop:Hide} = 0
        ?LookupExchangeReason{prop:Hide} = 0
    Else !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
        ?jobe:ExchangeReason{prop:Hide} = 1
        ?jobe:ExchangeReason:Prompt{prop:Hide} = 1
        ?LookupExchangeReason{prop:Hide} = 1
    End !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1

    If job:exchange_unit_number = ''
        job:exchange_user = ''
        exchange_model_make_temp = ''
        exchange_esn_temp = ''
        exchange_msn_temp = ''
        exchange_Accessories_Temp = ''
    End
    count# = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
        Exchange_Accessories_Temp = Clip(jea:part_number) & '-' & Clip(jea:description)
    end !loop
    access:jobexacc.restorefile(save_jea_id)

    If count# = 0
        Exchange_Accessories_Temp = ''
    End!If count# = 0
    If count# > 1
        Exchange_Accessories_temp = count#
    End!If count# > 1
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = job:exchange_unit_number
    if access:exchange.fetch(xch:ref_number_key) = Level:Benign
        exchange_model_make_temp    = Clip(job:exchange_unit_number) & ': ' & Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
        exchange_esn_temp           = xch:esn
        exchange_msn_temp           = xch:msn
    end !if access:exchange.fetch(xch:ref_number_key) = Level:Benign

Count_accessories       Routine
    exchange_accessory_count_temp = 0
    save_jea_id = access:jobexacc.savefile()
    access:jobexacc.clearkey(jea:part_number_key)
    jea:job_ref_number = job:Ref_number
    set(jea:part_number_key,jea:part_number_key)
    loop
        if access:jobexacc.next()
           break
        end !if
        if jea:job_ref_number <> job:ref_number      |
            then break.  ! end if
        exchange_accessory_count_temp += 1
    end !loop
    access:jobexacc.restorefile(save_jea_id)
Third_Party_Repairer        Routine    !Don't Think This Is Used Anymore
    clear(trm:record)
    trm:model_number = job:model_number
    get(trdmodel,trm:model_number_only_key)
    if errorcode()
       clear(trm:record)
!       Hide(?3rd_Party_Option)
    Else
!        Unhide(?3rd_Party_Option)
    end !if
Repair_Type_Check       Routine      !Third Party Repair Type
Repair_type_Lookup      Routine      !Lookup Repair Type

    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    glo:select1    = job:model_number
    browserepairty
    if globalresponse = requestcompleted
        job:repair_type = rep:repair_type
        glo:select1 = ''
        display()
    Else
        glo:select1 = 'END'
    end
    globalrequest     = saverequest#
Repair_type_Warranty_Lookup      Routine      !Lookup Repair Type

    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    glo:select1    = job:model_number
    browserepairty
    if globalresponse = requestcompleted
        job:repair_type_warranty = rep:repair_type
        glo:select1 = ''
        display()
    Else
        glo:select1 = 'END'
    end
    globalrequest     = saverequest#
Transit_type        Routine     !Lookup Transit Type
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    If Access:trantype.Fetch(trt:transit_type_key) = Level:Benign
        If trt:collection_address <> 'YES'
            Hide(?collection_address_group)
        Else
            Unhide(?collection_address_group)
        End
        If trt:delivery_address <> 'YES'
            Hide(?delivery_address_group)
        Else
            Unhide(?delivery_address_group)
        End
        If trt:location <> 'YES'
            Hide(?location_group)
        Else
            If ~def:HideLocation
                Unhide(?location_group)
            End !If ~def:HideLocation
        End
    end !if
    Display()
Location_Type       Routine     !Hide/Show If Workshop
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Case job:workshop
        Of 'YES'
            access:trantype.clearkey(trt:transit_type_key)
            trt:transit_type = job:transit_type
            if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                If trt:location <> 'YES'
                    Hide(?location_group)
                Else
                    If ~def:HideLocation
                        Unhide(?location_group)
                    End !If ~def:HideLocation
                End
            Else
                If ~def:HideLocation
                    Unhide(?location_group)
                End !If ~def:HideLocation
            end
            Hide(?job:third_party_site)
            hide(?job:third_party_site:prompt)
            Hide(?third_party_message)
            Hide(?job:ThirdPartyDateDesp)
            Hide(?job:third_party_despatch_date:prompt)
            Hide(?job:date_paid)
            Hide(?job:date_paid:prompt)
            If job:third_party_site <> ''
                Unhide(?thirdpartybutton)
            Else!If job:third_party_site <> ''
                Hide(?thirdpartybutton)
            End!If job:third_party_site <> ''
        Of 'NO'
            Hide(?location_group)
            If job:third_party_site <> ''
                Unhide(?thirdpartybutton)
                Unhide(?job:third_party_site)
                Unhide(?job:third_party_site:prompt)
                ?job:third_party_site{prop:xpos} = 84
                ?job:third_party_site{prop:ypos} = 144
                ?job:third_party_site:prompt{prop:xpos} = 8
                ?job:third_party_site:prompt{prop:ypos} = 144
                Unhide(?third_party_message)
                Unhide(?job:ThirdPartyDateDesp)
                Unhide(?job:third_party_despatch_date:prompt)
                Unhide(?job:date_paid)
                Unhide(?job:date_paid:prompt)
            End
    ENd
Charge_Type     Routine      !Is this an estimate charge type
    If job:chargeable_job   = 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:allow_estimate = 'YES'
                Unhide(?job:estimate)
                If job:estimate <> 'YES' and cha:force_estimate = 'YES'
                    Case MessageEx('The selected Charge Type means that this job must be an Estimate.','ServiceBase 2000',|
                                   'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    job:estimate = 'YES'
                    Post(Event:accepted,?job:estimate)
                End!If job:estimate <> 'YES'
            Else
                Hide(?job:estimate)
            End
        end !If access:chartype.fetch(cha:charge_type_key) = Level:Benign
    End!If job:chargeable_job   = 'YES'
Estimate        Routine       !Hide/Show If Estimate
    If job:Chargeable_Job = 'YES'
        If job:estimate = 'YES' And ?job:Estimate{prop:Hide} = 0
            Unhide(?job:Estimate)
            Unhide(?job:estimate_if_over:prompt)
            Unhide(?job:estimate_if_over)
            Unhide(?job:estimate_accepted)
            Unhide(?job:estimate_rejected)
            Unhide(?job:estimate_ready)
            set(defaults)
            access:defaults.next()
            If job:estimate_if_over = ''
                job:estimate_if_over    = def:estimate_if_over
            End
        Else
            Hide(?job:estimate_if_over:prompt)
            Hide(?job:estimate_if_over)
            Hide(?job:estimate_accepted)
            Hide(?job:estimate_rejected)
            Hide(?job:estimate_ready)
        End
    End!If job:Chargeable_Job = 'YES'
Update_accessories      Routine
    count# = 0
    setcursor(cursor:wait)
    save_jac_id = access:jobacc.savefile()
    access:jobacc.clearkey(jac:ref_number_key)
    jac:ref_number = job:ref_number
    set(jac:ref_number_key,jac:ref_number_key)
    loop
        if access:jobacc.next()
           break
        end !if
        if jac:ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
    end !loop
    access:jobacc.restorefile(save_jac_id)
    setcursor()

    If count# = 1
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        Set(jac:ref_number_key,jac:ref_number_key)
        access:jobacc.next()
        Accessories_Temp = jac:accessory
    ! Start Change 2885 BE(07/07/2003)
    ! Start Change AccessoriesDisplayBug BE(26/06/03)
    !Else
    !    accessories_temp = Clip(count#)
    ! End Change AccessoriesDisplayBug BE(26/06/03)
    Else
        accessories_temp = Clip(count#)
    ! End Change 2885 BE(07/07/2003)
    End

   ! Start Change AccessoriesDisplayBug BE(26/06/03)
   !If Accessories_Temp > 0
   IF (count# > 0) THEN
   ! End Change AccessoriesDisplayBug BE(26/06/03)
        ?Accessory_Button:2{prop:fontstyle} = font:bold
    Else
        ?Accessory_Button:2{prop:fontstyle} = font:regular
    End !If Accessories_Temp > 0
Loan_Box        Routine      !Show/Hide Loan/Exchange Boxes
    If job:loan_unit_number = ''
        Hide(?loan_group)
    Else
        Unhide(?loan_group)
    End
    Display()
Exchange_Box    Routine
    If job:exchange_unit_number = '' And Exchange_Accessories_Temp = ''
        Hide(?exchange_Group)
    ELse
        unhide(?exchange_group)
    End
    Display()
Get_Audit       Routine
    Clear(aud:record)
    aud:ref_number  = job:ref_number
    aud:date        = Today()
    aud:time        = Clock()
    clear(use:record)
    use:password =glo:password
    get(users,use:password_key)
    aud:user        = use:user_code
Time_Remaining          Routine      !Turnaround Time Nightmare
    Set(defaults)
    access:defaults.next()
    Compile('***',Debug=1)
        Unhide(?job:status_end_date)
        Unhide(?job:status_end_time)
    ***
    If job:date_completed <> ''
        ?status_time_remaining_temp{prop:fontcolor} = color:green
        status_time_remaining_temp = 'Job Completed'
        ?job_time_remaining_temp{prop:fontcolor} = color:green
        job_time_remaining_temp = 'Job Completed'

    Else!If job:date_completed <> ''
!Status Time
        If job:status_end_date = Deformat('1/1/2050',@d6) Or job:status_end_date = ''
            ?status_time_remaining_temp{prop:fontcolor} = color:green
            status_time_remaining_temp = 'Never Expire'
        Else !If job:status_end_date = ''
            If job:status_end_date > Today()
                count# = 0
                day_count_temp = 0
                Loop 
                    count# += 1
                    day_number_temp = (Today() + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'
                    day_count_temp += 1
                    If day_number_temp >= job:status_end_date
                        Break
                    End!If day_number_temp = job:status_end_date
                End!Loop
                ?status_time_remaining_temp{prop:fontcolor} = color:navy
                If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Day'
                Else!If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Days'
                End!If day_count_temp = 1            
            End!If job:status_end_date > Today()

            If job:status_end_date < Today()
                count# = 0
                day_count_temp = 0
                Loop 
                    count# += 1
                    day_number_temp = (job:status_end_date + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'    
                    day_count_temp += 1
                    If day_number_temp >= Today()
                        Break
                    End!If day_number_temp = job:status_end_date
                End!Loop    
                ?status_time_remaining_temp{prop:fontcolor} = color:red
                If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Day Over'
                Else!If day_count_temp = 1
                    status_time_remaining_temp = Clip(day_count_temp) & ' Days Over'
                End!If day_count_temp = 1            
            End!If job:status_end_date < Today()

            If job:status_end_date = Today()
                If job:status_end_time > Clock()
                    ?status_time_remaining_temp{prop:fontcolor} = color:green
                    status_time_remaining_temp = Format(job:status_end_time - Clock(),@t1) & ' Hrs'
                End!If job:status_end_time > Clock()
                If job:status_end_time < Clock()
                    ?status_time_remaining_temp{prop:fontcolor} = color:red
                    status_time_remaining_temp = Format(Clock() - job:status_end_time,@t1) & ' Hrs Over'
                End!If job:status_end_time > Clock()    
                If job:status_end_time = Clock()
                    ?status_time_remaining_temp{prop:fontcolor} = color:red
                    status_time_remaining_temp = 'Due Now'
                End            
            End!If job:status_end_date = Today()
        End !If job:status_end_date = ''

!Turnaround Time
        If job:turnaround_end_date = Deformat('1/1/2050',@d6) Or job:turnaround_end_date = ''
            ?job_time_remaining_temp{prop:fontcolor} = color:green
            job_time_remaining_temp = 'Never Expire'
        Else !If job:turnaround_end_date = ''
            If job:turnaround_end_date > Today()
                count# = 0
                day_count_temp = 0
                Loop 
                    count# += 1
                    day_number_temp = (Today() + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'
                    day_count_temp += 1
                    If day_number_temp >= job:turnaround_end_date
                        Break
                    End!If day_number_temp = job:turnaround_end_date
                End!Loop
                ?job_time_remaining_temp{prop:fontcolor} = color:navy
                If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Day'
                Else!If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Days'
                End!If day_count_temp = 1            
            End!If job:turnaround_end_date > Today()

            If job:turnaround_end_date < Today()
                count# = 0
                day_count_temp = 0
                Loop 
                    count# += 1
                    day_number_temp = (job:turnaround_end_date + count#)
                    If def:include_saturday <> 'YES'
                        If day_number_temp % 7 = 6
                            Cycle
                        End!If day_number_temp % 7 = 6
                    End!If def:include_saturday <> 'YES'
                    If def:include_sunday <> 'YES'
                        If day_number_temp % 7 = 0
                            Cycle
                        End!If day_number_temp % 7 = 0
                    End!If def:include_sunday <> 'YES'    
                    day_count_temp += 1
                    If day_number_temp >= Today()
                        Break
                    End!If day_number_temp = job:turnaround_end_date
                End!Loop    
                ?job_time_remaining_temp{prop:fontcolor} = color:red
                If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Day Over'
                Else!If day_count_temp = 1
                    job_time_remaining_temp = Clip(day_count_temp) & ' Days Over'
                End!If day_count_temp = 1            
            End!If job:turnaround_end_date < Today()
            If job:turnaround_end_date = Today()
                If job:turnaround_end_time > Clock()
                    ?job_time_remaining_temp{prop:fontcolor} = color:green
                    job_time_remaining_temp = Format(job:turnaround_end_time - Clock(),@t1) & ' Hrs'
                End!If job:turnaround_end_time > Clock()
                If job:turnaround_end_time < Clock()
                    ?job_time_remaining_temp{prop:fontcolor} = color:red
                    job_time_remaining_temp = Format(Clock() - job:turnaround_end_time,@t1) & ' Hrs Over'
                End!If job:turnaround_end_time > Clock()    
                If job:turnaround_end_time = Clock()
                    ?job_time_remaining_temp{prop:fontcolor} = color:red
                    job_time_remaining_temp = 'Due Now'
                End            
            End!If job:turnaround_end_date = Today()
        End !If job:turnaround_end_date = ''
    End!If job:date_completed <> ''
History_Search          Routine !AutoSearching
    Set(defaults)
    access:defaults.next()
    glo:select1 = ''
    glo:select2 = ''
    Do History_Search2
    If glo:select1 <> ''
        glo:select2 = job:auto_search
        glo:select12 = ''
        If ThisWindow.Request <> insertrecord
            glo:select12 = 'VIEW ONLY'
            glo:select11 = job:ref_number
        End
        
        Auto_Search_Window
        glo:select11 = ''
        glo:select12 = ''
        If ThisWindow.Request = Insertrecord
            If glo:select3 <> ''
                access:jobs_alias.clearkey(job_ali:ref_number_key)
                job_ali:ref_number = glo:select3
                if access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
                    access:jobnotes_alias.clearkey(jbn_ali:RefNumberKey)
                    jbn_ali:RefNUmber   = jbn:RefNumber
                    access:jobnotes_alias.tryfetch(jbn_ali:RefNumberKey)
                    If glo:select5 = 'Y'
                        job:account_number  = job_ali:account_number
                        job:trade_account_name  = job_ali:trade_account_name
                        job:postcode         = job_ali:postcode
                        job:company_name     = job_ali:company_name
                        job:address_line1    = job_ali:address_Line1
                        job:address_line2    = job_ali:address_Line2
                        job:address_line3    = job_ali:address_line3
                        job:telephone_number = job_ali:telephone_number
                        job:fax_number       = job_ali:fax_number
                    End
                    If glo:select6 = 'Y'
                        job:postcode_collection         = job_ali:postcode_collection
                        job:company_name_collection     = job_ali:company_name_collection
                        job:address_line1_collection    = job_ali:address_Line1_collection
                        job:address_line2_collection    = job_ali:address_Line2_collection
                        job:address_line3_collection    = job_ali:address_line3_collection
                        job:telephone_collection = job_ali:telephone_collection
                        job:postcode_delivery         = job_ali:postcode_delivery
                        job:company_name_delivery     = job_ali:company_name_delivery
                        job:address_line1_delivery    = job_ali:address_Line1_delivery
                        job:address_line2_delivery    = job_ali:address_Line2_delivery
                        job:address_line3_delivery    = job_ali:address_line3_delivery
                        job:telephone_delivery = job_ali:telephone_delivery
                    End
                    If glo:select7 = 'Y'
                        job:model_number = job_ali:model_number
                        job:mobile_number = job_ali:mobile_number
                        job:manufacturer    = job_ali:manufacturer
                        job:unit_type       = job_ali:unit_type
                        job:esn             = job_ali:esn
                        job:msn             = job_ali:msn
                        job:dop             = job_ali:dop
                    End
                    If glo:select8 = 'Y'
                        job:title   = job_ali:title
                        job:initial = job_ali:initial
                        job:surname = job_ali:surname
                    End
                    access:jobnotes.clearkey(jbn:RefNumberKey)
                    jbn:RefNumber   = job:Ref_Number
                    access:jobnotes.tryfetch(Jbn:RefNumberKey)
                    If glo:select9 = 'Y'
                        jbn:engineers_notes = jbn_ali:engineers_notes
                    End
                    If glo:select10 = 'Y'
                        jbn:invoice_text    = jbn_ali:invoice_text
                    End
                    access:jobnotes.update()
                    IF glo:select11 = 'Y'
                    End
                    job:last_repair_days    = Today() - job_ali:date_booked
                    Do last_repair_days
                    If job:last_repair_days < def:warranty_period And glo:select7 = 'Y'
                        Case MessageEx('This unit was previously repaired within the Internal Warranty Period specified by the System Supervisor.<13,10><13,10>Please check with your Supervisor to ascertain whether this  unit should be repaired FREE OF CHARGE.','ServiceBase 2000',|
                                       'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx

! Fill In Engineer's Notes With Previous Jobs

                        setcursor(cursor:wait)
                        save_job_ali_id = access:jobs_alias.savefile()
                        access:jobs_alias.clearkey(job_ali:esn_key)
                        job_ali:esn = job:esn
                        set(job_ali:esn_key,job_ali:esn_key)
                        loop
                            if access:jobs_alias.next()
                               break
                            end !if
                            if job_ali:esn <> job:esn      |
                                then break.  ! end if
                            If job_ali:ref_number = job:ref_number
                                Cycle
                            End
                            Jbn:engineers_notes = Clip(jbn:engineers_notes) & '<13,10>Previous Repair: <13,10>Job No: ' & Clip(Format(job_ali:ref_number,@n012))|
                                                     & ' Date: ' & Clip(Format(job_ali:date_booked,@d6b)) & ' Eng: ' & Clip(job_ali:engineer)
                            access:jobnotes.update()
                        end !loop
                        access:jobs_alias.restorefile(save_job_ali_id)
                        setcursor()
                    End

                    Clear(glo:G_Select1)
                    Clear(glo:G_Select1)

                end !if access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
            End!If glo:select3 <> ''
        End
    Else
        If ThisWindow.Request = insertrecord
            glo:select1 = 'NO MATCH'
            glo:select2 = job:auto_search
            Hide(?job:last_repair_days)
            Hide(?job:last_repair_days:prompt)
            ?history_button{prop:text} = 'No History'
            ?history_button{prop:fontcolor} = color:red
            If def:automatic_replicate <> 'YES'
                New_Job_Screen
                Case glo:select2
                    Of 'SURNAME'
                        job:surname = job:auto_search
                    Of 'MOBILE NUMBER'
                        job:mobile_number = job:auto_search
                    Of 'ESN'
                        job:esn = job:auto_search
                        Post(Event:Accepted,?job:ESN)
                    Of 'MSN'
                        job:msn = job:auto_search
                    Of 'POSTCODE'
                        job:postcode = job:auto_search
                        Postcode_Routine(job:postcode,job:address_line1,job:address_line2,job:address_line3)
                        Display()
                    Of 'SEARCH AGAIN'
                        Clear(job:auto_search)
                        Select(?job:auto_search)
                        Unhide(?job:last_repair_days)
                        ?history_button{prop:text} = 'History'
                        ?history_button{prop:text} = color:black
                        UnHide(?job:last_repair_days:prompt)
                End
            Else
                Case def:automatic_replicate_field
                    Of 'SURNAME'
                        job:surname = job:auto_search
                    Of 'MOBILE NUMBER'
                        job:mobile_number = job:auto_search
                    Of 'E.S.N./I.M.E.I.'
                        job:esn = job:auto_search
                        Post(Event:Accepted,?job:ESN)
                    Of 'M.S.N.'
                        job:msn = job:auto_search
                    Of 'POSTCODE'
                        job:postcode = job:auto_search
                        Postcode_Routine(job:postcode,job:address_line1,job:address_line2,job:address_line3)
                        Display()
                End
            End
        End
    End
    history# = 0
    Display()
    Clear(glo:select1)
History_search2     Routine
    glo:select1 = ''
    ! Surname
        clear(job_ali:record, -1)
        job_ali:surname       = job:auto_search
        set(job_ali:surname_key,job_ali:surname_key)
        loop
            next(jobs_alias)
            if errorcode()                       |
               or job_ali:surname       <> job:auto_search      |
               then break.  ! end if
            glo:select1 = 'SURNAME'
            Break
        end !loop
    !Mobile_Number
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:mobile_number = job:auto_search
            get(jobs_alias,job_ali:mobilenumberkey)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'MOBILE'
            end !if
        End
    !ESN
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:esn = job:auto_search
            get(jobs_alias,job_ali:esn_key)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'ESN'
            end !if
        End
    !MSN
        If glo:select1 = ''
            clear(job_ali:record)
            job_ali:msn = job:auto_search
            get(jobs_alias,job_ali:msn_key)
            if errorcode()
               clear(job_ali:record)
            Else
                glo:select1 = 'MSN'
            end !if
        End
    !Postcode
Last_Repair_Days        Routine   !Show/Hide Last Repair Days field
    If job:last_repair_days = 0
        Hide(?job:last_repair_days)
        Hide(?job:last_repair_days:prompt)
    Else
        UnHide(?job:last_repair_days)
        UnHide(?job:last_repair_days:prompt)
    End
    If job:last_repair_days < def:warranty_Period
        ?job:last_repair_days{prop:color} = color:red
        ?job:last_repair_days{prop:fontcolor} = color:yellow
    End
check_force_fault_codes    Routine
    force_fault_codes_temp = 'NO'
    If job:chargeable_job = 'YES'
        required# = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:warranty_job = 'YES'
Date_Completed      Routine     !When Date Completed Filled in
!    access:jobnotes.clearkey(jbn:RefNumberKey)
!    jbn:RefNUmber   = job:ref_number
!    If access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign
!        jbn:Fault_Description    = tmp:FaultDescription
!        jbn:Invoice_Text        = tmp:InvoiceText
!        jbn:Engineers_notes     = tmp:EngineerNotes
!        access:jobnotes.update()
!    End!If access:jobnotes.tryfetch(jbn:RefNumberKey) = Level:Benign

    Include('completed.inc')
    Do Time_Remaining
    Do skip_Despatch
    If error# = 1
        glo:errortext = 'This job cannot be completed due to the following error(s): <13,10>' & Clip(glo:errortext)
        Error_Text
        glo:errortext = ''
    End!If error# = 1

    brw21.resetsort(1)
!    loop error_count# = 1 to 50
!        If error_type_temp[x#] = 1
!            Case error_count#
!                OF 1
!                    ?job:transit_type{prop:req} = 1
!                OF 2
!                    ?job:mobile_number{prop:req} = 1
!                OF 3
!                    ?job:model_number{prop:req} = 1
!                OF 4
!                    ?job:unit_type{prop:req} = 1
!                OF 5
!                    ?tmp:FaultDescription{prop:req} = 1
!                OF 6
!                    ?job:esn{prop:req} = 1
!                OF 7
!                    ?job:incoming_courier{prop:req} = 1
!                OF 8
!                    ?job:msn{prop:req} = 1
!                OF 31
!                    ?job:courier{prop:req} = 1
!                OF 10
!                    ?job:engineer{prop:req} = 1
!                of 11
!                    ?tmp:InvoiceText{prop:req} = 1
!                OF 12
!                    ?job:repair_type{prop:req} = 1
!                OF 13
!                    ?job:repair_Type_warranty{prop:req} = 1
!                OF 14
!                    ?job:authority_number{prop:req} = 1
!                OF 27
!                OF 28
!                Of 29
!                        
!                Of 30
!                    
!            End!Case error#
!        End
!    End!loop error_count# = 1 to 30
!    If exchange_update# = 1
!        If job:exchange_unit_number <> ''
!            access:exchange.clearkey(xch:ref_number_key)
!            xch:ref_number = job:exchange_unit_number
!            if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!                If xch:audit_number <> ''
!                    access:excaudit.clearkey(exa:audit_number_key)
!                    exa:stock_type   = xch:stock_type
!                    exa:audit_number = xch:audit_number
!                    if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
!!Remove Stock Unit
!                        access:exchange.clearkey(xch:ref_number_key)
!                        xch:ref_number = exa:stock_unit_number
!                        if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!                            get(exchhist,0)
!                            if access:exchhist.primerecord() = level:benign
!                                exh:ref_number   = xch:ref_number
!                                exh:date          = today()
!                                exh:time          = clock()
!                                access:users.clearkey(use:password_key)
!                                use:password =glo:password
!                                access:users.fetch(use:password_key)
!                                exh:user = use:user_code
!                                exh:status        = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
!                                access:exchhist.insert()
!                            end!if access:exchhist.primerecord() = level:benign
!                            xch:audit_number = ''
!                            access:exchange.update()
!                        end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!                        access:exchange.clearkey(xch:ref_number_key)
!                        xch:ref_number = exa:replacement_unit_number
!                        if access:exchange.fetch(xch:ref_number_key) = Level:benign     !Make new unit available
!                            xch:available = 'RTS'
!                            xch:job_number = ''
!                            access:exchange.update()
!                            get(exchhist,0)
!                            if access:exchhist.primerecord() = level:benign
!                                exh:ref_number   = xch:ref_number
!                                exh:date          = today()
!                                exh:time          = clock()
!                                access:users.clearkey(use:password_key)
!                                use:password =glo:password
!                                access:users.fetch(use:password_key)
!                                exh:user = use:user_code
!                                exh:status        = 'UNIT AVAILABLE'
!                                access:exchhist.insert()
!                            end!if access:exchhist.primerecord() = level:benign
!                        end!if access:exchange.fetch(xch:ref_number_key) = Level:benign
!                        exa:stock_unit_number = exa:replacement_unit_number             !Make new unit the stock item
!                        exa:replacement_unit_number = ''
!                        access:excaudit.update()
!                    end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
!                Else!
!                End!If xch:audit_number <> ''
!            end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!        End!If job:exchange_unit_number <> ''
!
!    End!If exchange_update# = 1
!END Removing Exchange Unit From Stock, and putting replacement unit as stock item.
    Display()
QA_Group        Routine
    Hide(?qa_group)
    Set(defaults)
    access:defaults.next()
    If def:qa_required = 'YES' And job:date_completed <> ''
        Unhide(?qa_group)
    End!If def:qa_required = 'YES' And job:date_completed <> ''
    If def:qa_before_complete = 'YES'
        If job:date_completed = ''
            Disable(?qa_group)
            Unhide(?qa_group)
        Else!If job:date_completed = ''
            Unhide(?qa_group)
            Disable(?qa_group)
        End!If job:date_completed = ''
    End!If def:qa_before_complete = 'YES'
Pricing     Routine

    Set(defaults)
    access:defaults.next()
    If job:invoice_number <> ''
        ?job:labour_cost:prompt{prop:text} = 'Labour Cost (INV)'
        ?job:parts_cost:prompt{prop:text} = 'Parts Cost (INV)'
        ?job:courier_cost:prompt{prop:text} = 'Courier Cost (INV)'
    Else!End!If job:invoice_number <> ''
        If job:ignore_chargeable_charges <> 'YES'
            Disable(?job:labour_cost)
            Pricing_Routine('C',labour",parts",pass",discount")
            If pass" = True
                job:labour_cost = labour"
                job:parts_cost  = parts"
                If job:invoice_Number = ''
                    Case discount"
                        Of 'LA'
                            ?job:labour_cost:prompt{prop:text} = 'Labour Cost (TD)'
                        Of 'PA'
                            ?job:parts_cost:prompt{prop:text} = 'Parts Cost (TD)'
                        Of 'BO'
                            ?job:labour_cost:prompt{prop:text} = 'Labour Cost (TD)'
                            ?job:parts_cost:prompt{prop:text} = 'Parts Cost (TD)'
                    End!Case discount"
                End!If job:invoice_Number = ''
            Else
                job:labour_cost = 0
                job:parts_cost  = 0
            End!If pass" = True
        Else!If job:ignore_chargeable_charges <> 'YES'
            Enable(?job:labour_cost)
            parts" = 0
            access:chartype.clearkey(cha:charge_type_key)
            cha:charge_type = job:charge_type
            if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                If cha:no_charge <> 'YES'
                    setcursor(cursor:wait)
                    save_par_id = access:parts.savefile()
                    access:parts.clearkey(par:part_number_key)
                    par:ref_number  = job:ref_number
                    set(par:part_number_key,par:part_number_key)
                    loop
                        if access:parts.next()
                           break
                        end !if
                        if par:ref_number  <> job:ref_number      |
                            then break.  ! end if
                        yldcnt# += 1
                        if yldcnt# > 25
                           yield() ; yldcnt# = 0
                        end !if
                        parts" += (par:quantity * par:sale_cost)
                    end !loop
                    access:parts.restorefile(save_par_id)
                    setcursor()
                End!If cha:no_charge <> 'YES'
            end!if access:chartype.fetch(cha:charge_type_key) = Level:Benign
            job:parts_cost = parts"
            If job:invoice_number = ''
                ?job:labour_cost:prompt{prop:text} = 'Labour Cost'
                ?job:parts_cost:prompt{prop:text} = 'Parts Cost'
            End!If job:invoice_number = ''

        End!If job:ignore_chargeable_charges <> 'YES'
    End!End!If job:invoice_number <> ''

    IF job:invoice_number_warranty <> ''
        ?job:labour_cost_warranty:prompt{prop:text} = 'Labour Cost (INV)'
        ?job:parts_cost_warranty:prompt{prop:text} = 'Parts Cost (INV)'
        ?job:courier_cost_warranty:prompt{prop:text} = 'Courier Cost (INV)'
    Else!IF job:warranty_invoice_number
    
        If job:ignore_warranty_charges <> 'YES'
            Disable(?job:labour_cost_warranty)

            Pricing_Routine('W',labour",parts",pass",discount")
            If pass" = True
                job:labour_cost_warranty    = labour"
                job:parts_cost_warranty     = parts"
                If job:invoice_number_warranty = ''
                    Case discount"
                        Of 'LA'
                            ?job:labour_cost_warranty:prompt{prop:text} = 'Labour Cost (TD)'
                        Of 'PA'
                            ?job:parts_cost_warranty:prompt{prop:text} = 'Parts Cost (TD)'
                        Of 'BO'
                            ?job:labour_cost_warranty:prompt{prop:text} = 'Labour Cost (TD)'
                            ?job:parts_cost_warranty:prompt{prop:text} = 'Parts Cost (TD)'
                    End!Case discount_warranty"
                End!If job:invoice_number_warranty = ''
            Else
                job:labour_cost_warranty = 0
                job:parts_cost_warranty = 0
            End!If pass_warranty" = True
        Else!If job:ignore_chargeable_charges <> 'YES'
            setcursor(cursor:wait)
            parts" = 0
            access:chartype.clearkey(cha:charge_type_key)
            cha:charge_type = job:warranty_charge_type
            if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                If cha:no_charge <> 'YES'

                    save_wpr_id = access:warparts.savefile()
                    access:warparts.clearkey(wpr:part_number_key)
                    wpr:ref_number  = job:ref_number
                    set(wpr:part_number_key,wpr:part_number_key)
                    loop
                        if access:warparts.next()
                           break
                        end !if
                        if wpr:ref_number  <> job:ref_number      |
                            then break.  ! end if
                        yldcnt# += 1
                        if yldcnt# > 25
                           yield() ; yldcnt# = 0
                        end !if
                        parts" += (wpr:quantity * wpr:purchase_cost)
                    end !loop
                    access:warparts.restorefile(save_wpr_id)
                    setcursor()
                End!If cha:no_charge <> 'YES'
            end!if access:chartype.fetch(cha:charge_type_key) = Level:Benign

            job:parts_cost_warranty = parts"
            Enable(?job:labour_cost_warranty)
            If job:invoice_number_warranty = ''
                ?job:labour_cost_warranty:prompt{prop:text} = 'Labour Cost'
                ?job:parts_cost_warranty:prompt{prop:text} = 'Parts Cost'
            End!If job:invoice_number_warranty = ''
        End!If job:ignore_warranty_charges <> 'YES'
    End!IF job:warranty_invoice_number
    If job:estimate = 'YES' And job:estimate_accepted <> 'YES'
        If job:ignore_estimate_charges <> 'YES'
            Disable(?job:labour_cost_estimate)

            Pricing_Routine('E',labour",parts",pass",discount")
            If pass" = True
                job:labour_cost_estimate = labour"
                job:parts_cost_estimate = parts"
            Else!If pass" = True
                job:labour_cost_estimate = 0
                job:parts_cost_estimate = 0
            End!If pass" = True
        Else
            parts" = 0
            setcursor(cursor:wait)
            save_epr_id = access:estparts.savefile()
            access:estparts.clearkey(epr:part_number_key)
            epr:ref_number  = job:ref_number
            set(epr:part_number_key,epr:part_number_key)
            loop
                if access:estparts.next()
                   break
                end !if
                if epr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                parts" += (epr:quantity * epr:sale_cost)
            end !loop
            access:estparts.restorefile(save_epr_id)
            setcursor()
            job:parts_cost_estimate = parts"
            Enable(?job:labour_cost_estimate)
        End!If job:ignore_estimate_charges <> 'YES'
    End!If job:estimate = 'YES'
    Display()
Fill_Lists      Routine    !Fill the Drop Down Lists

   BRW12.ResetSort(1)
   BRW21.ResetSort(1)
   BRW77.ResetSort(1)
   BRW79.ResetSort(1)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateJOBS',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateJOBS',1)
    SolaceViewVars('tmp_RF_Board_IMEI',tmp_RF_Board_IMEI,'UpdateJOBS',1)
    SolaceViewVars('ValidateMobileNumber',ValidateMobileNumber,'UpdateJOBS',1)
    SolaceViewVars('MSN_Field_Mask',MSN_Field_Mask,'UpdateJOBS',1)
    SolaceViewVars('temp_255string',temp_255string,'UpdateJOBS',1)
    SolaceViewVars('save_joe_id',save_joe_id,'UpdateJOBS',1)
    SolaceViewVars('save_taf_id',save_taf_id,'UpdateJOBS',1)
    SolaceViewVars('save_cou_ali_id',save_cou_ali_id,'UpdateJOBS',1)
    SolaceViewVars('save_cou_id',save_cou_id,'UpdateJOBS',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'UpdateJOBS',1)
    SolaceViewVars('tmp:accountnumber',tmp:accountnumber,'UpdateJOBS',1)
    SolaceViewVars('tmp:OldConsignNo',tmp:OldConsignNo,'UpdateJOBS',1)
    SolaceViewVars('tmp:labelerror',tmp:labelerror,'UpdateJOBS',1)
    SolaceViewVars('account_number2_temp',account_number2_temp,'UpdateJOBS',1)
    SolaceViewVars('tmp:DespatchClose',tmp:DespatchClose,'UpdateJOBS',1)
    SolaceViewVars('sav:ref_number',sav:ref_number,'UpdateJOBS',1)
    SolaceViewVars('tmp:printjobcard',tmp:printjobcard,'UpdateJOBS',1)
    SolaceViewVars('tmp:printjobreceipt',tmp:printjobreceipt,'UpdateJOBS',1)
    SolaceViewVars('tmp:SuspendPrintJobCard',tmp:SuspendPrintJobCard,'UpdateJOBS',1)
    SolaceViewVars('tmp:PrintJobcardNow',tmp:PrintJobcardNow,'UpdateJOBS',1)
    SolaceViewVars('save_trb_id',save_trb_id,'UpdateJOBS',1)
    SolaceViewVars('save_xch_id',save_xch_id,'UpdateJOBS',1)
    SolaceViewVars('save_cha_id',save_cha_id,'UpdateJOBS',1)
    SolaceViewVars('sav:path',sav:path,'UpdateJOBS',1)
    SolaceViewVars('print_despatch_note_temp',print_despatch_note_temp,'UpdateJOBS',1)
    
      Loop SolaceDim1# = 1 to 50
        SolaceFieldName" = 'error_type_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",error_type_temp[SolaceDim1#],'UpdateJOBS',1)
      End
    
    
    SolaceViewVars('error_queue_temp:field',error_queue_temp:field,'UpdateJOBS',1)
    SolaceViewVars('error_message_temp',error_message_temp,'UpdateJOBS',1)
    SolaceViewVars('Cancelling_Group:old_exchange_unit_number_temp',Cancelling_Group:old_exchange_unit_number_temp,'UpdateJOBS',1)
    SolaceViewVars('Cancelling_Group:new_exchange_unit_number_temp',Cancelling_Group:new_exchange_unit_number_temp,'UpdateJOBS',1)
    SolaceViewVars('Cancelling_Group:old_loan_unit_number_temp',Cancelling_Group:old_loan_unit_number_temp,'UpdateJOBS',1)
    SolaceViewVars('Cancelling_Group:save_moc_id',Cancelling_Group:save_moc_id,'UpdateJOBS',1)
    SolaceViewVars('Cancelling_Group:new_loan_unit_number_temp',Cancelling_Group:new_loan_unit_number_temp,'UpdateJOBS',1)
    SolaceViewVars('Cancelling_Group:saved_ref_number_temp',Cancelling_Group:saved_ref_number_temp,'UpdateJOBS',1)
    SolaceViewVars('parts_queue_temp:Part_Ref_Number',parts_queue_temp:Part_Ref_Number,'UpdateJOBS',1)
    SolaceViewVars('parts_queue_temp:Quantity',parts_queue_temp:Quantity,'UpdateJOBS',1)
    SolaceViewVars('parts_queue_temp:Exclude_From_Order',parts_queue_temp:Exclude_From_Order,'UpdateJOBS',1)
    SolaceViewVars('parts_queue_temp:Pending_Ref_Number',parts_queue_temp:Pending_Ref_Number,'UpdateJOBS',1)
    SolaceViewVars('check_for_bouncers_temp',check_for_bouncers_temp,'UpdateJOBS',1)
    SolaceViewVars('save_aud_id',save_aud_id,'UpdateJOBS',1)
    SolaceViewVars('save_job_id',save_job_id,'UpdateJOBS',1)
    SolaceViewVars('saved_esn_temp',saved_esn_temp,'UpdateJOBS',1)
    SolaceViewVars('saved_msn_temp',saved_msn_temp,'UpdateJOBS',1)
    SolaceViewVars('engineer_sundry_temp',engineer_sundry_temp,'UpdateJOBS',1)
    SolaceViewVars('main_store_sundry_temp',main_store_sundry_temp,'UpdateJOBS',1)
    SolaceViewVars('engineer_ref_number_temp',engineer_ref_number_temp,'UpdateJOBS',1)
    SolaceViewVars('main_store_ref_number_temp',main_store_ref_number_temp,'UpdateJOBS',1)
    SolaceViewVars('engineer_quantity_temp',engineer_quantity_temp,'UpdateJOBS',1)
    SolaceViewVars('main_store_quantity_temp',main_store_quantity_temp,'UpdateJOBS',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'UpdateJOBS',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'UpdateJOBS',1)
    SolaceViewVars('label_type_temp',label_type_temp,'UpdateJOBS',1)
    SolaceViewVars('exchange_accessory_count_temp',exchange_accessory_count_temp,'UpdateJOBS',1)
    SolaceViewVars('accessory_count_temp',accessory_count_temp,'UpdateJOBS',1)
    SolaceViewVars('Ignore_Chargeable_Charges_Temp',Ignore_Chargeable_Charges_Temp,'UpdateJOBS',1)
    SolaceViewVars('Ignore_Warranty_Charges_Temp',Ignore_Warranty_Charges_Temp,'UpdateJOBS',1)
    SolaceViewVars('Ignore_Estimate_Charges_Temp',Ignore_Estimate_Charges_Temp,'UpdateJOBS',1)
    SolaceViewVars('Force_Fault_Codes_Temp',Force_Fault_Codes_Temp,'UpdateJOBS',1)
    SolaceViewVars('balance_due_warranty_temp',balance_due_warranty_temp,'UpdateJOBS',1)
    SolaceViewVars('paid_warranty_temp',paid_warranty_temp,'UpdateJOBS',1)
    SolaceViewVars('print_label_temp',print_label_temp,'UpdateJOBS',1)
    SolaceViewVars('Courier_temp',Courier_temp,'UpdateJOBS',1)
    SolaceViewVars('loan_Courier_temp',loan_Courier_temp,'UpdateJOBS',1)
    SolaceViewVars('exchange_Courier_temp',exchange_Courier_temp,'UpdateJOBS',1)
    SolaceViewVars('engineer_name_temp',engineer_name_temp,'UpdateJOBS',1)
    SolaceViewVars('msn_fail_temp',msn_fail_temp,'UpdateJOBS',1)
    SolaceViewVars('esn_fail_temp',esn_fail_temp,'UpdateJOBS',1)
    SolaceViewVars('day_count_temp',day_count_temp,'UpdateJOBS',1)
    SolaceViewVars('day_number_temp',day_number_temp,'UpdateJOBS',1)
    SolaceViewVars('date_error_temp',date_error_temp,'UpdateJOBS',1)
    SolaceViewVars('estimate_ready_temp',estimate_ready_temp,'UpdateJOBS',1)
    SolaceViewVars('in_repair_temp',in_repair_temp,'UpdateJOBS',1)
    SolaceViewVars('on_test_temp',on_test_temp,'UpdateJOBS',1)
    SolaceViewVars('qa_passed_temp',qa_passed_temp,'UpdateJOBS',1)
    SolaceViewVars('qa_rejected_temp',qa_rejected_temp,'UpdateJOBS',1)
    SolaceViewVars('qa_second_passed_temp',qa_second_passed_temp,'UpdateJOBS',1)
    SolaceViewVars('estimate_accepted_temp',estimate_accepted_temp,'UpdateJOBS',1)
    SolaceViewVars('estimate_rejected_temp',estimate_rejected_temp,'UpdateJOBS',1)
    SolaceViewVars('Third_Party_Site_temp',Third_Party_Site_temp,'UpdateJOBS',1)
    SolaceViewVars('Chargeable_Job_temp',Chargeable_Job_temp,'UpdateJOBS',1)
    SolaceViewVars('Warranty_Job_Temp',Warranty_Job_Temp,'UpdateJOBS',1)
    SolaceViewVars('warranty_charge_type_temp',warranty_charge_type_temp,'UpdateJOBS',1)
    SolaceViewVars('Partemp_group:Partemp:part_ref_number',Partemp_group:Partemp:part_ref_number,'UpdateJOBS',1)
    SolaceViewVars('Partemp_group:Partemp:pending_ref_number',Partemp_group:Partemp:pending_ref_number,'UpdateJOBS',1)
    SolaceViewVars('Partemp_group:Partemp:despatch_note_number',Partemp_group:Partemp:despatch_note_number,'UpdateJOBS',1)
    SolaceViewVars('Partemp_group:Partemp:quantity',Partemp_group:Partemp:quantity,'UpdateJOBS',1)
    SolaceViewVars('Partemp_group:Partemp:purchase_cost',Partemp_group:Partemp:purchase_cost,'UpdateJOBS',1)
    SolaceViewVars('Partemp_group:Partemp:sale_cost',Partemp_group:Partemp:sale_cost,'UpdateJOBS',1)
    SolaceViewVars('estimate_temp',estimate_temp,'UpdateJOBS',1)
    SolaceViewVars('workshop_temp',workshop_temp,'UpdateJOBS',1)
    SolaceViewVars('location_temp',location_temp,'UpdateJOBS',1)
    SolaceViewVars('repair_type_temp',repair_type_temp,'UpdateJOBS',1)
    SolaceViewVars('warranty_repair_type_temp',warranty_repair_type_temp,'UpdateJOBS',1)
    SolaceViewVars('model_number_temp',model_number_temp,'UpdateJOBS',1)
    SolaceViewVars('History_Run_Temp',History_Run_Temp,'UpdateJOBS',1)
    SolaceViewVars('transit_type_temp',transit_type_temp,'UpdateJOBS',1)
    SolaceViewVars('account_number_temp',account_number_temp,'UpdateJOBS',1)
    SolaceViewVars('ESN_temp',ESN_temp,'UpdateJOBS',1)
    SolaceViewVars('MSN_temp',MSN_temp,'UpdateJOBS',1)
    SolaceViewVars('Unit_Type_temp',Unit_Type_temp,'UpdateJOBS',1)
    SolaceViewVars('Current_Status_Temp',Current_Status_Temp,'UpdateJOBS',1)
    SolaceViewVars('Order_Number_temp',Order_Number_temp,'UpdateJOBS',1)
    SolaceViewVars('Surname_temp',Surname_temp,'UpdateJOBS',1)
    SolaceViewVars('Date_Completed_temp',Date_Completed_temp,'UpdateJOBS',1)
    SolaceViewVars('Time_Completed_Temp',Time_Completed_Temp,'UpdateJOBS',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateJOBS',1)
    SolaceViewVars('LocalResponse',LocalResponse,'UpdateJOBS',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateJOBS',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateJOBS',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateJOBS',1)
    SolaceViewVars('charge_type_Temp',charge_type_Temp,'UpdateJOBS',1)
    SolaceViewVars('title_3_temp',title_3_temp,'UpdateJOBS',1)
    SolaceViewVars('details_temp',details_temp,'UpdateJOBS',1)
    SolaceViewVars('date_booked_temp',date_booked_temp,'UpdateJOBS',1)
    SolaceViewVars('name_temp',name_temp,'UpdateJOBS',1)
    SolaceViewVars('Vat_Total_Temp',Vat_Total_Temp,'UpdateJOBS',1)
    SolaceViewVars('Total_Temp',Total_Temp,'UpdateJOBS',1)
    SolaceViewVars('engineer_temp',engineer_temp,'UpdateJOBS',1)
    SolaceViewVars('loan_model_make_temp',loan_model_make_temp,'UpdateJOBS',1)
    SolaceViewVars('loan_esn_temp',loan_esn_temp,'UpdateJOBS',1)
    SolaceViewVars('loan_msn_temp',loan_msn_temp,'UpdateJOBS',1)
    SolaceViewVars('Loan_Accessories_Temp',Loan_Accessories_Temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Model_Make_temp',Exchange_Model_Make_temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Esn_temp',Exchange_Esn_temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Msn_temp',Exchange_Msn_temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Accessories_Temp',Exchange_Accessories_Temp,'UpdateJOBS',1)
    SolaceViewVars('balance_due_temp',balance_due_temp,'UpdateJOBS',1)
    SolaceViewVars('Accessories_Temp',Accessories_Temp,'UpdateJOBS',1)
    SolaceViewVars('location_type_temp',location_type_temp,'UpdateJOBS',1)
    SolaceViewVars('vat_total_estimate_temp',vat_total_estimate_temp,'UpdateJOBS',1)
    SolaceViewVars('total_estimate_temp',total_estimate_temp,'UpdateJOBS',1)
    SolaceViewVars('vat_total_warranty_temp',vat_total_warranty_temp,'UpdateJOBS',1)
    SolaceViewVars('Total_warranty_temp',Total_warranty_temp,'UpdateJOBS',1)
    SolaceViewVars('chargeable_part_total_temp',chargeable_part_total_temp,'UpdateJOBS',1)
    SolaceViewVars('warranty_part_total_temp',warranty_part_total_temp,'UpdateJOBS',1)
    SolaceViewVars('job_time_remaining_temp',job_time_remaining_temp,'UpdateJOBS',1)
    SolaceViewVars('status_time_remaining_temp',status_time_remaining_temp,'UpdateJOBS',1)
    SolaceViewVars('ordered_temp',ordered_temp,'UpdateJOBS',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'UpdateJOBS',1)
    SolaceViewVars('ProcedureRunning',ProcedureRunning,'UpdateJOBS',1)
    SolaceViewVars('warranty_ordered_temp',warranty_ordered_temp,'UpdateJOBS',1)
    SolaceViewVars('vat_estimate_temp',vat_estimate_temp,'UpdateJOBS',1)
    SolaceViewVars('vat_chargeable_temp',vat_chargeable_temp,'UpdateJOBS',1)
    SolaceViewVars('vat_warranty_temp',vat_warranty_temp,'UpdateJOBS',1)
    SolaceViewVars('paid_chargeable_temp',paid_chargeable_temp,'UpdateJOBS',1)
    SolaceViewVars('estimate_parts_cost_temp',estimate_parts_cost_temp,'UpdateJOBS',1)
    SolaceViewVars('show_booking_temp',show_booking_temp,'UpdateJOBS',1)
    SolaceViewVars('trade_account_string_temp',trade_account_string_temp,'UpdateJOBS',1)
    SolaceViewVars('customer_name_string_temp',customer_name_string_temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Consignment_Number_temp',Exchange_Consignment_Number_temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Despatched_temp',Exchange_Despatched_temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Despatched_User_temp',Exchange_Despatched_User_temp,'UpdateJOBS',1)
    SolaceViewVars('Exchange_Despatch_Number_temp',Exchange_Despatch_Number_temp,'UpdateJOBS',1)
    SolaceViewVars('Loan_Consignment_Number_temp',Loan_Consignment_Number_temp,'UpdateJOBS',1)
    SolaceViewVars('Loan_Despatched_temp',Loan_Despatched_temp,'UpdateJOBS',1)
    SolaceViewVars('Loan_Despatched_User_temp',Loan_Despatched_User_temp,'UpdateJOBS',1)
    SolaceViewVars('Loan_Despatch_Number_temp',Loan_Despatch_Number_temp,'UpdateJOBS',1)
    SolaceViewVars('Date_Despatched_temp',Date_Despatched_temp,'UpdateJOBS',1)
    SolaceViewVars('Despatch_Number_temp',Despatch_Number_temp,'UpdateJOBS',1)
    SolaceViewVars('Despatch_User_temp',Despatch_User_temp,'UpdateJOBS',1)
    SolaceViewVars('Consignment_Number_temp',Consignment_Number_temp,'UpdateJOBS',1)
    SolaceViewVars('third_party_returned_temp',third_party_returned_temp,'UpdateJOBS',1)
    SolaceViewVars('tmp:FaultDescription',tmp:FaultDescription,'UpdateJOBS',1)
    SolaceViewVars('tmp:EngineerNotes',tmp:EngineerNotes,'UpdateJOBS',1)
    SolaceViewVars('tmp:InvoiceText',tmp:InvoiceText,'UpdateJOBS',1)
    SolaceViewVars('SaveGroup:ChargeType',SaveGroup:ChargeType,'UpdateJOBS',1)
    SolaceViewVars('SaveGroup:WarrantyChargeType',SaveGroup:WarrantyChargeType,'UpdateJOBS',1)
    SolaceViewVars('SaveGroup:RepairType',SaveGroup:RepairType,'UpdateJOBS',1)
    SolaceViewVars('SaveGroup:WarrantyRepairType',SaveGroup:WarrantyRepairType,'UpdateJOBS',1)
    SolaceViewVars('SaveGroup:ChargeableJob',SaveGroup:ChargeableJob,'UpdateJOBS',1)
    SolaceViewVars('SaveGroup:WarrantyJob',SaveGroup:WarrantyJob,'UpdateJOBS',1)
    SolaceViewVars('ChangedGroup:cct',ChangedGroup:cct,'UpdateJOBS',1)
    SolaceViewVars('ChangedGroup:wct',ChangedGroup:wct,'UpdateJOBS',1)
    SolaceViewVars('ChangedGroup:crt',ChangedGroup:crt,'UpdateJOBS',1)
    SolaceViewVars('ChangedGroup:wrt',ChangedGroup:wrt,'UpdateJOBS',1)
    SolaceViewVars('ChangedGroup:chj',ChangedGroup:chj,'UpdateJOBS',1)
    SolaceViewVars('ChangedGroup:wrj',ChangedGroup:wrj,'UpdateJOBS',1)
    SolaceViewVars('tmp:PartQueue:PartNumber',tmp:PartQueue:PartNumber,'UpdateJOBS',1)
    SolaceViewVars('tmp:ParcelLineName',tmp:ParcelLineName,'UpdateJOBS',1)
    SolaceViewVars('tmp:WorkstationName',tmp:WorkstationName,'UpdateJOBS',1)
    SolaceViewVars('tmp:IncomingIMEI',tmp:IncomingIMEI,'UpdateJOBS',1)
    SolaceViewVars('tmp:IncomingMSN',tmp:IncomingMSN,'UpdateJOBS',1)
    SolaceViewVars('tmp:ExchangeIMEI',tmp:ExchangeIMEI,'UpdateJOBS',1)
    SolaceViewVars('tmp:ExchangeMSN',tmp:ExchangeMSN,'UpdateJOBS',1)
    SolaceViewVars('tmp:EstimateDetails',tmp:EstimateDetails,'UpdateJOBS',1)
    SolaceViewVars('tmp:SkillLevel',tmp:SkillLevel,'UpdateJOBS',1)
    SolaceViewVars('tmp:one',tmp:one,'UpdateJOBS',1)
    SolaceViewVars('tmp:DateAllocated',tmp:DateAllocated,'UpdateJOBS',1)
    SolaceViewVars('tmp:FaultyUnit',tmp:FaultyUnit,'UpdateJOBS',1)
    SolaceViewVars('tmp:BERConsignmentNumber',tmp:BERConsignmentNumber,'UpdateJOBS',1)
    SolaceViewVars('tmp:Network',tmp:Network,'UpdateJOBS',1)
    SolaceViewVars('tmp:zero',tmp:zero,'UpdateJOBS',1)
    SolaceViewVars('tmp:InWorkshopDate',tmp:InWorkshopDate,'UpdateJOBS',1)
    SolaceViewVars('tmp:InWorkshopTime',tmp:InWorkshopTime,'UpdateJOBS',1)
    SolaceViewVars('tmp:SaveAccessory',tmp:SaveAccessory,'UpdateJOBS',1)
    SolaceViewVars('SaveAccessoryQueue:Accessory',SaveAccessoryQueue:Accessory,'UpdateJOBS',1)
    SolaceViewVars('SaveAccessoryQueue:Type',SaveAccessoryQueue:Type,'UpdateJOBS',1)
    SolaceViewVars('partpntstore',partpntstore,'UpdateJOBS',1)
    SolaceViewVars('warpartpntstore',warpartpntstore,'UpdateJOBS',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Batch_Number_Text;  SolaceCtrlName = '?Batch_Number_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title_Text;  SolaceCtrlName = '?Title_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Booking1_Tab;  SolaceCtrlName = '?Booking1_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Auto_Search:Prompt;  SolaceCtrlName = '?JOB:Auto_Search:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Auto_Search;  SolaceCtrlName = '?job:Auto_Search';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?History_Button;  SolaceCtrlName = '?History_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Last_Repair_Days;  SolaceCtrlName = '?job:Last_Repair_Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Last_Repair_Days:Prompt;  SolaceCtrlName = '?JOB:Last_Repair_Days:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job:Account_Number:Prompt;  SolaceCtrlName = '?Job:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Trade_Account_Status;  SolaceCtrlName = '?Trade_Account_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Account_Number;  SolaceCtrlName = '?job:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupTradeAccount;  SolaceCtrlName = '?LookupTradeAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:order_number:prompt;  SolaceCtrlName = '?job:order_number:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Order_Number;  SolaceCtrlName = '?job:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NameGroup;  SolaceCtrlName = '?NameGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title:prompt;  SolaceCtrlName = '?Title:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Initial:prompt;  SolaceCtrlName = '?Initial:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?surname:prompt;  SolaceCtrlName = '?surname:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:title:prompt;  SolaceCtrlName = '?job:title:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Title;  SolaceCtrlName = '?job:Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Initial;  SolaceCtrlName = '?job:Initial';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Surname;  SolaceCtrlName = '?job:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt49;  SolaceCtrlName = '?Prompt49';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Transit_Type;  SolaceCtrlName = '?job:Transit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupTransitType;  SolaceCtrlName = '?LookupTransitType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:location_type:prompt;  SolaceCtrlName = '?job:location_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?no_Label_button;  SolaceCtrlName = '?no_Label_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Workshop;  SolaceCtrlName = '?job:Workshop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Group;  SolaceCtrlName = '?Location_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:location:prompt;  SolaceCtrlName = '?job:location:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Location;  SolaceCtrlName = '?job:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Third_party_message;  SolaceCtrlName = '?Third_party_message';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Third_Party_Site;  SolaceCtrlName = '?job:Third_Party_Site';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:MSN:Prompt;  SolaceCtrlName = '?JOB:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:MSN;  SolaceCtrlName = '?job:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ProductCode:Prompt;  SolaceCtrlName = '?job:ProductCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ProductCode;  SolaceCtrlName = '?job:ProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Model_Number:Prompt;  SolaceCtrlName = '?JOB:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Model_Number;  SolaceCtrlName = '?job:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupModelNumber;  SolaceCtrlName = '?LookupModelNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Manufacturer;  SolaceCtrlName = '?job:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt:Job:Unit_Type;  SolaceCtrlName = '?Prompt:Job:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Unit_Type;  SolaceCtrlName = '?job:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Colour;  SolaceCtrlName = '?job:Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_button;  SolaceCtrlName = '?accessory_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Accessories_Temp:Prompt;  SolaceCtrlName = '?Accessories_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Accessories_Temp;  SolaceCtrlName = '?Accessories_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:DOP:Prompt;  SolaceCtrlName = '?JOB:DOP:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:DOP;  SolaceCtrlName = '?job:DOP';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt47;  SolaceCtrlName = '?Prompt47';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Description_Text_Button;  SolaceCtrlName = '?Fault_Description_Text_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network:Prompt:2;  SolaceCtrlName = '?tmp:Network:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network:2;  SolaceCtrlName = '?tmp:Network:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupNetwork:2;  SolaceCtrlName = '?LookupNetwork:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:esn:prompt;  SolaceCtrlName = '?job:esn:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Mobile_Number:Prompt;  SolaceCtrlName = '?JOB:Mobile_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Mobile_Number;  SolaceCtrlName = '?job:Mobile_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InWorkshopDate;  SolaceCtrlName = '?tmp:InWorkshopDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InWorkshopTime;  SolaceCtrlName = '?tmp:InWorkshopTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt101;  SolaceCtrlName = '?Prompt101';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Turnaround_Time;  SolaceCtrlName = '?job:Turnaround_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Third_Party_Site:Prompt;  SolaceCtrlName = '?JOB:Third_Party_Site:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:charge_type:prompt;  SolaceCtrlName = '?job:charge_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Chargeable_Job;  SolaceCtrlName = '?job:Chargeable_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type;  SolaceCtrlName = '?job:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupChargeType;  SolaceCtrlName = '?LookupChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Third_Party_Despatch_Date:Prompt;  SolaceCtrlName = '?JOB:Third_Party_Despatch_Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ThirdPartyDateDesp;  SolaceCtrlName = '?job:ThirdPartyDateDesp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Job;  SolaceCtrlName = '?job:Warranty_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Charge_Type;  SolaceCtrlName = '?job:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupWarrantyChargeType;  SolaceCtrlName = '?LookupWarrantyChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_Paid;  SolaceCtrlName = '?job:Date_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:date_paid:prompt;  SolaceCtrlName = '?job:date_paid:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Estimate;  SolaceCtrlName = '?job:Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Estimate_If_Over;  SolaceCtrlName = '?job:Estimate_If_Over';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Estimate_Accepted;  SolaceCtrlName = '?job:Estimate_Accepted';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Estimate_Rejected;  SolaceCtrlName = '?job:Estimate_Rejected';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Estimate_If_Over:prompt;  SolaceCtrlName = '?job:Estimate_If_Over:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Booking2_Tab;  SolaceCtrlName = '?Booking2_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group9;  SolaceCtrlName = '?Group9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trade_account_string_temp;  SolaceCtrlName = '?trade_account_string_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Order_Number:2;  SolaceCtrlName = '?JOB:Order_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?customer_name_string_temp;  SolaceCtrlName = '?customer_name_string_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Transit_Type:2;  SolaceCtrlName = '?JOB:Transit_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network;  SolaceCtrlName = '?tmp:Network';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupNetwork;  SolaceCtrlName = '?LookupNetwork';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Workshop:2;  SolaceCtrlName = '?JOB:Workshop:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Location:2;  SolaceCtrlName = '?JOB:Location:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Third_Party_Site:2;  SolaceCtrlName = '?JOB:Third_Party_Site:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Special_Instructions:2;  SolaceCtrlName = '?JOB:Special_Instructions:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StatusWarning;  SolaceCtrlName = '?StatusWarning';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group9:2;  SolaceCtrlName = '?Group9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN:2;  SolaceCtrlName = '?job:ESN:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:MSN:2;  SolaceCtrlName = '?job:MSN:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Model_Number:2;  SolaceCtrlName = '?job:Model_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ProductCode:2;  SolaceCtrlName = '?job:ProductCode:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Colour:2;  SolaceCtrlName = '?job:Colour:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Mobile_Number:2;  SolaceCtrlName = '?job:Mobile_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:DOP:2;  SolaceCtrlName = '?job:DOP:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Turnaround_Time:2;  SolaceCtrlName = '?job:Turnaround_Time:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type:2;  SolaceCtrlName = '?job:Charge_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Charge_Type:2;  SolaceCtrlName = '?job:Warranty_Charge_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EstimateDetails;  SolaceCtrlName = '?tmp:EstimateDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Manufacturer:2;  SolaceCtrlName = '?job:Manufacturer:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SIMNumber;  SolaceCtrlName = '?SIMNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:SIMNumber;  SolaceCtrlName = '?jobe:SIMNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IncomingIMEI;  SolaceCtrlName = '?tmp:IncomingIMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IncomingMSN;  SolaceCtrlName = '?tmp:IncomingMSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeIMEI;  SolaceCtrlName = '?tmp:ExchangeIMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeMSN;  SolaceCtrlName = '?tmp:ExchangeMSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSIMNumber;  SolaceCtrlName = '?LookupSIMNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Unit_Type:2;  SolaceCtrlName = '?job:Unit_Type:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_button:2;  SolaceCtrlName = '?accessory_button:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Description_Text_Button:2;  SolaceCtrlName = '?Fault_Description_Text_Button:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt107;  SolaceCtrlName = '?Prompt107';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Final;  SolaceCtrlName = '?Final';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Incoming:3;  SolaceCtrlName = '?Incoming:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IMEIMSN;  SolaceCtrlName = '?IMEIMSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchanged;  SolaceCtrlName = '?Exchanged';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IMEIMSN:2;  SolaceCtrlName = '?IMEIMSN:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt108;  SolaceCtrlName = '?Prompt108';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ModelNumberProductCode;  SolaceCtrlName = '?ModelNumberProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer;  SolaceCtrlName = '?Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?IMEIMSN:3;  SolaceCtrlName = '?IMEIMSN:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Customer_Name_String_temp:Prompt;  SolaceCtrlName = '?Customer_Name_String_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt110;  SolaceCtrlName = '?Prompt110';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobTurnaroundTime;  SolaceCtrlName = '?JobTurnaroundTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UnitTypeColour;  SolaceCtrlName = '?UnitTypeColour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MobileNumberDOP;  SolaceCtrlName = '?MobileNumberDOP';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt111;  SolaceCtrlName = '?Prompt111';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network:Prompt;  SolaceCtrlName = '?tmp:Network:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?location_string;  SolaceCtrlName = '?location_string';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?third_party_string;  SolaceCtrlName = '?third_party_string';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?chargeable_Type_string;  SolaceCtrlName = '?chargeable_Type_string';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?special_instructions_string;  SolaceCtrlName = '?special_instructions_string';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?warranty_charge_Type_String;  SolaceCtrlName = '?warranty_charge_Type_String';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EstimatePrompt;  SolaceCtrlName = '?EstimatePrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BouncerText;  SolaceCtrlName = '?BouncerText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?show_booking_temp;  SolaceCtrlName = '?show_booking_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobcompleted;  SolaceCtrlName = '?jobcompleted';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchanged_Title;  SolaceCtrlName = '?Exchanged_Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String6;  SolaceCtrlName = '?String6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt128;  SolaceCtrlName = '?Prompt128';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name;  SolaceCtrlName = '?job:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode:2;  SolaceCtrlName = '?job:Postcode:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Number;  SolaceCtrlName = '?job:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fax_Number;  SolaceCtrlName = '?job:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String21:4;  SolaceCtrlName = '?String21:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:EndUserEmailAddress;  SolaceCtrlName = '?jobe:EndUserEmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String21:3;  SolaceCtrlName = '?String21:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1;  SolaceCtrlName = '?job:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2;  SolaceCtrlName = '?job:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Address:Prompt;  SolaceCtrlName = '?JOB:Address:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3;  SolaceCtrlName = '?job:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AmendAddresses;  SolaceCtrlName = '?AmendAddresses';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String21;  SolaceCtrlName = '?String21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String21:2;  SolaceCtrlName = '?String21:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Collection_Address_Group;  SolaceCtrlName = '?Collection_Address_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String7;  SolaceCtrlName = '?String7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Collection;  SolaceCtrlName = '?job:Company_Name_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Collection;  SolaceCtrlName = '?job:Address_Line1_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Collection;  SolaceCtrlName = '?job:Address_Line2_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Collection;  SolaceCtrlName = '?job:Address_Line3_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Collection;  SolaceCtrlName = '?job:Postcode_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Collection;  SolaceCtrlName = '?job:Telephone_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Collection_Text;  SolaceCtrlName = '?jbn:Collection_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Collection_Text_Button;  SolaceCtrlName = '?Collection_Text_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?delivery_Address_Group;  SolaceCtrlName = '?delivery_Address_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String8;  SolaceCtrlName = '?String8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Delivery;  SolaceCtrlName = '?job:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Delivery;  SolaceCtrlName = '?job:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Delivery;  SolaceCtrlName = '?job:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Delivery;  SolaceCtrlName = '?job:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Delivery;  SolaceCtrlName = '?job:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Delivery;  SolaceCtrlName = '?job:Telephone_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Delivery_Text;  SolaceCtrlName = '?jbn:Delivery_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delivery_Text_Button;  SolaceCtrlName = '?Delivery_Text_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Engineer;  SolaceCtrlName = '?Lookup_Engineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job:Engineer:Prompt;  SolaceCtrlName = '?Job:Engineer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Repair_Type_Chargeable_Group;  SolaceCtrlName = '?Repair_Type_Chargeable_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt105;  SolaceCtrlName = '?Prompt105';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type;  SolaceCtrlName = '?job:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupChargeableChargeType;  SolaceCtrlName = '?LookupChargeableChargeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job:Repair_Type:Prompt;  SolaceCtrlName = '?Job:Repair_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Engineers_Notes:Prompt;  SolaceCtrlName = '?JOB:Engineers_Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Engineers_Notes_Button;  SolaceCtrlName = '?Engineers_Notes_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Engineers_Notes;  SolaceCtrlName = '?jbn:Engineers_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt131;  SolaceCtrlName = '?Prompt131';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button62;  SolaceCtrlName = '?Button62';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt125:2;  SolaceCtrlName = '?Prompt125:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt125;  SolaceCtrlName = '?Prompt125';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RemoveParts;  SolaceCtrlName = '?RemoveParts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AllocateEngineer;  SolaceCtrlName = '?AllocateEngineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Common_Fault;  SolaceCtrlName = '?Common_Fault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt85;  SolaceCtrlName = '?Prompt85';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Estimate_Ready;  SolaceCtrlName = '?job:Estimate_Ready';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button59;  SolaceCtrlName = '?Button59';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SkillLevel;  SolaceCtrlName = '?tmp:SkillLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobLevel;  SolaceCtrlName = '?JobLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:SkillLevel;  SolaceCtrlName = '?jobe:SkillLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt126:4;  SolaceCtrlName = '?Prompt126:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?engineer_name_temp;  SolaceCtrlName = '?engineer_name_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DateAllocated;  SolaceCtrlName = '?tmp:DateAllocated';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt126:2;  SolaceCtrlName = '?Prompt126:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Repair_Type_Warranty_Group;  SolaceCtrlName = '?Repair_Type_Warranty_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Repair_Type_Warranty:Prompt;  SolaceCtrlName = '?JOB:Repair_Type_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type_Warranty;  SolaceCtrlName = '?job:Repair_Type_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupWarrantyRepairType;  SolaceCtrlName = '?LookupWarrantyRepairType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:In_Repair;  SolaceCtrlName = '?job:In_Repair';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:On_Test;  SolaceCtrlName = '?job:On_Test';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_In_Repair:2;  SolaceCtrlName = '?job:Date_In_Repair:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Time_In_Repair:2;  SolaceCtrlName = '?job:Time_In_Repair:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_On_Test;  SolaceCtrlName = '?job:Date_On_Test';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Time_On_Test;  SolaceCtrlName = '?job:Time_On_Test';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt100;  SolaceCtrlName = '?Prompt100';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel8;  SolaceCtrlName = '?Panel8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Today_Button;  SolaceCtrlName = '?Today_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_Completed;  SolaceCtrlName = '?job:Date_Completed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Time_Completed;  SolaceCtrlName = '?job:Time_Completed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:CompleteRepairDate;  SolaceCtrlName = '?jobe:CompleteRepairDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:CompleteRepairTime;  SolaceCtrlName = '?jobe:CompleteRepairTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QA_Group;  SolaceCtrlName = '?QA_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt99;  SolaceCtrlName = '?Prompt99';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:QA_Passed;  SolaceCtrlName = '?job:QA_Passed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_QA_Passed;  SolaceCtrlName = '?job:Date_QA_Passed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Time_QA_Passed;  SolaceCtrlName = '?job:Time_QA_Passed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Estimate_Details_Tab;  SolaceCtrlName = '?Estimate_Details_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Chargeable_Details_Tab;  SolaceCtrlName = '?Chargeable_Details_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarrantyPartsTransfer:2;  SolaceCtrlName = '?WarrantyPartsTransfer:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CreatePickNote;  SolaceCtrlName = '?CreatePickNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChargeableAdjustment;  SolaceCtrlName = '?ChargeableAdjustment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Warranty_Details_Tab;  SolaceCtrlName = '?Warranty_Details_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CreatePickNote:2;  SolaceCtrlName = '?CreatePickNote:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarrantyPartsTransfer;  SolaceCtrlName = '?WarrantyPartsTransfer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarrantyAdjustment;  SolaceCtrlName = '?WarrantyAdjustment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab11;  SolaceCtrlName = '?Tab11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:advance_payment:Prompt;  SolaceCtrlName = '?job:advance_payment:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Advance_Payment:2;  SolaceCtrlName = '?JOB:Advance_Payment:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Number_Warranty:Prompt;  SolaceCtrlName = '?JOB:Invoice_Number_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Authority_Number:Prompt;  SolaceCtrlName = '?JOB:Authority_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Authority_Number;  SolaceCtrlName = '?job:Authority_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt103;  SolaceCtrlName = '?Prompt103';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt104;  SolaceCtrlName = '?Prompt104';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:courier:prompt;  SolaceCtrlName = '?job:courier:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier;  SolaceCtrlName = '?job:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Incoming_Courier;  SolaceCtrlName = '?job:Incoming_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Date;  SolaceCtrlName = '?job:Invoice_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Number_Warranty;  SolaceCtrlName = '?job:Invoice_Number_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Number:Prompt;  SolaceCtrlName = '?JOB:Invoice_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Number;  SolaceCtrlName = '?job:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Invoice_Text:Prompt;  SolaceCtrlName = '?JOB:Invoice_Text:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoice_Text_Buton;  SolaceCtrlName = '?Invoice_Text_Buton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Invoice_Text;  SolaceCtrlName = '?jbn:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CRCDespatchDateButton;  SolaceCtrlName = '?CRCDespatchDateButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Consignment_Number:Prompt;  SolaceCtrlName = '?JOB:Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Consignment_Number;  SolaceCtrlName = '?job:Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatched_Calendar:3;  SolaceCtrlName = '?despatched_Calendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Incoming_Consignment_Number;  SolaceCtrlName = '?job:Incoming_Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Date_Despatched:Prompt;  SolaceCtrlName = '?JOB:Date_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Date_Despatched;  SolaceCtrlName = '?job:Date_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Incoming_Date;  SolaceCtrlName = '?job:Incoming_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Invoice_Date_Warranty;  SolaceCtrlName = '?job:Invoice_Date_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:EDI_Batch_Number;  SolaceCtrlName = '?job:EDI_Batch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Despatch_User:Prompt;  SolaceCtrlName = '?JOB:Despatch_User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Despatch_User;  SolaceCtrlName = '?job:Despatch_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Despatch_Number:Prompt;  SolaceCtrlName = '?JOB:Despatch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Despatch_Number;  SolaceCtrlName = '?job:Despatch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Insurance_Reference_Number:Prompt;  SolaceCtrlName = '?JOB:Insurance_Reference_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Insurance_Reference_Number;  SolaceCtrlName = '?job:Insurance_Reference_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Despatch_Number:Prompt:2;  SolaceCtrlName = '?JOB:Despatch_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:UPSFlagCode;  SolaceCtrlName = '?jobe:UPSFlagCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:JobService;  SolaceCtrlName = '?job:JobService';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend_Despatch:3;  SolaceCtrlName = '?Amend_Despatch:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Skip_Despatch_Validation;  SolaceCtrlName = '?Skip_Despatch_Validation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:EDI_Batch_Number:Prompt;  SolaceCtrlName = '?JOB:EDI_Batch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Loan_Exchange_Tab;  SolaceCtrlName = '?Loan_Exchange_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Unit_Number:Prompt;  SolaceCtrlName = '?JOB:Loan_Unit_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Unit_Number;  SolaceCtrlName = '?job:Loan_Unit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Loan_Unit;  SolaceCtrlName = '?Lookup_Loan_Unit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_User:Prompt;  SolaceCtrlName = '?JOB:Loan_User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_User;  SolaceCtrlName = '?job:Loan_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Unit_Number:Prompt;  SolaceCtrlName = '?JOB:Exchange_Unit_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Unit_Number;  SolaceCtrlName = '?job:Exchange_Unit_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Exchange_Unit;  SolaceCtrlName = '?Lookup_Exchange_Unit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_User:Prompt;  SolaceCtrlName = '?JOB:Exchange_User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_User;  SolaceCtrlName = '?job:Exchange_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:ExchangeReason:Prompt;  SolaceCtrlName = '?jobe:ExchangeReason:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:ExchangeReason;  SolaceCtrlName = '?jobe:ExchangeReason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupExchangeReason;  SolaceCtrlName = '?LookupExchangeReason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:LoanReason:Prompt;  SolaceCtrlName = '?jobe:LoanReason:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:LoanReason;  SolaceCtrlName = '?jobe:LoanReason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLoanReason;  SolaceCtrlName = '?LookupLoanReason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:loan_courier:prompt;  SolaceCtrlName = '?job:loan_courier:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Courier;  SolaceCtrlName = '?job:Loan_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:exchange_courier:prompt;  SolaceCtrlName = '?job:exchange_courier:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Courier;  SolaceCtrlName = '?job:Exchange_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loan_model_make_temp:Prompt;  SolaceCtrlName = '?loan_model_make_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loan_model_make_temp;  SolaceCtrlName = '?loan_model_make_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Model_Make_temp:Prompt;  SolaceCtrlName = '?Exchange_Model_Make_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Model_Make_temp;  SolaceCtrlName = '?Exchange_Model_Make_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loan_esn_temp:Prompt;  SolaceCtrlName = '?loan_esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loan_esn_temp;  SolaceCtrlName = '?loan_esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loan_msn_temp:Prompt;  SolaceCtrlName = '?loan_msn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loan_msn_temp;  SolaceCtrlName = '?loan_msn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Esn_temp:Prompt;  SolaceCtrlName = '?Exchange_Esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Esn_temp;  SolaceCtrlName = '?Exchange_Esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Show_List_Button;  SolaceCtrlName = '?Show_List_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Msn_temp:Prompt;  SolaceCtrlName = '?Exchange_Msn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Msn_temp;  SolaceCtrlName = '?Exchange_Msn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Accessories_Temp:Prompt;  SolaceCtrlName = '?Exchange_Accessories_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Accessories_Temp;  SolaceCtrlName = '?Exchange_Accessories_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Accessory_Button;  SolaceCtrlName = '?Exchange_Accessory_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loan_group;  SolaceCtrlName = '?loan_group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend_Despatch:2;  SolaceCtrlName = '?Amend_Despatch:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Consignment_Number:Prompt;  SolaceCtrlName = '?JOB:Loan_Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Consignment_Number;  SolaceCtrlName = '?job:Loan_Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatched:Prompt;  SolaceCtrlName = '?JOB:Loan_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Despatched;  SolaceCtrlName = '?job:Loan_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatch_Number:Prompt;  SolaceCtrlName = '?JOB:Loan_Despatch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Despatch_Number;  SolaceCtrlName = '?job:Loan_Despatch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatched_User:Prompt;  SolaceCtrlName = '?JOB:Loan_Despatched_User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Despatched_User;  SolaceCtrlName = '?job:Loan_Despatched_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatched_User:Prompt:2;  SolaceCtrlName = '?JOB:Loan_Despatched_User:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:LoaService;  SolaceCtrlName = '?job:LoaService';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Group;  SolaceCtrlName = '?Exchange_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Despatched_User:Prompt:3;  SolaceCtrlName = '?JOB:Loan_Despatched_User:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ExcService;  SolaceCtrlName = '?job:ExcService';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend_Despatch;  SolaceCtrlName = '?Amend_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Consignment_Number:Prompt;  SolaceCtrlName = '?JOB:Exchange_Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Consignment_Number;  SolaceCtrlName = '?job:Exchange_Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Despatched:Prompt;  SolaceCtrlName = '?JOB:Exchange_Despatched:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Despatched_Calendar;  SolaceCtrlName = '?Despatched_Calendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Despatched;  SolaceCtrlName = '?job:Exchange_Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Despatch_Number:Prompt;  SolaceCtrlName = '?JOB:Exchange_Despatch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Despatch_Number;  SolaceCtrlName = '?job:Exchange_Despatch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Despatched_User:Prompt;  SolaceCtrlName = '?JOB:Exchange_Despatched_User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Despatched_User;  SolaceCtrlName = '?job:Exchange_Despatched_User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Loan_Accessories_Temp:Prompt;  SolaceCtrlName = '?Loan_Accessories_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Loan_Accessories_Temp;  SolaceCtrlName = '?Loan_Accessories_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Validate_Serial_Number;  SolaceCtrlName = '?Validate_Serial_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Label;  SolaceCtrlName = '?Print_Label';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Contact_History;  SolaceCtrlName = '?Contact_History';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Audit_Trail_Button;  SolaceCtrlName = '?Audit_Trail_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?estimate_tab;  SolaceCtrlName = '?estimate_tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ignore_Estimate_Charges;  SolaceCtrlName = '?job:Ignore_Estimate_Charges';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Courier_Cost_Estimate:Prompt;  SolaceCtrlName = '?JOB:Courier_Cost_Estimate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier_Cost_Estimate;  SolaceCtrlName = '?job:Courier_Cost_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Labour_Cost_Estimate:Prompt;  SolaceCtrlName = '?JOB:Labour_Cost_Estimate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Labour_Cost_Estimate;  SolaceCtrlName = '?job:Labour_Cost_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Parts_Cost_Estimate;  SolaceCtrlName = '?job:Parts_Cost_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String18:2;  SolaceCtrlName = '?String18:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Sub_Total_Estimate;  SolaceCtrlName = '?job:Sub_Total_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel4:4;  SolaceCtrlName = '?Panel4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String19:2;  SolaceCtrlName = '?String19:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_estimate_temp;  SolaceCtrlName = '?vat_estimate_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String20:2;  SolaceCtrlName = '?String20:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_estimate_temp;  SolaceCtrlName = '?total_estimate_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel4:3;  SolaceCtrlName = '?Panel4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Chargeable_Tab;  SolaceCtrlName = '?Chargeable_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ignore_Chargeable_Charges;  SolaceCtrlName = '?job:Ignore_Chargeable_Charges';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Courier_Cost:Prompt;  SolaceCtrlName = '?JOB:Courier_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier_Cost;  SolaceCtrlName = '?job:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Labour_Cost:Prompt;  SolaceCtrlName = '?JOB:Labour_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Labour_Cost;  SolaceCtrlName = '?job:Labour_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:parts_cost:prompt;  SolaceCtrlName = '?job:parts_cost:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Parts_Cost;  SolaceCtrlName = '?job:Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String18;  SolaceCtrlName = '?String18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Sub_Total;  SolaceCtrlName = '?job:Sub_Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel4;  SolaceCtrlName = '?Panel4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CharVat;  SolaceCtrlName = '?CharVat';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CharRejected;  SolaceCtrlName = '?CharRejected';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_chargeable_temp;  SolaceCtrlName = '?vat_chargeable_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CharTotal;  SolaceCtrlName = '?CharTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UnRejectButton;  SolaceCtrlName = '?UnRejectButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Total_Temp;  SolaceCtrlName = '?Total_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel4:2;  SolaceCtrlName = '?Panel4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt106;  SolaceCtrlName = '?Prompt106';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?paid_chargeable_temp;  SolaceCtrlName = '?paid_chargeable_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String23;  SolaceCtrlName = '?String23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?balance_due_temp;  SolaceCtrlName = '?balance_due_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Warranty_Tab;  SolaceCtrlName = '?Warranty_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ignore_Warranty_Charges;  SolaceCtrlName = '?job:Ignore_Warranty_Charges';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Courier_Cost_Warranty:Prompt;  SolaceCtrlName = '?JOB:Courier_Cost_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier_Cost_Warranty;  SolaceCtrlName = '?job:Courier_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Labour_Cost_Warranty:Prompt;  SolaceCtrlName = '?JOB:Labour_Cost_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Labour_Cost_Warranty;  SolaceCtrlName = '?job:Labour_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:parts_cost_warranty:prompt;  SolaceCtrlName = '?job:parts_cost_warranty:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Parts_Cost_Warranty;  SolaceCtrlName = '?job:Parts_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel4:6;  SolaceCtrlName = '?Panel4:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String18:3;  SolaceCtrlName = '?String18:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Sub_Total_Warranty;  SolaceCtrlName = '?job:Sub_Total_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String19:3;  SolaceCtrlName = '?String19:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_warranty_temp;  SolaceCtrlName = '?vat_warranty_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel4:5;  SolaceCtrlName = '?Panel4:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?failed_warranty_claim;  SolaceCtrlName = '?failed_warranty_claim';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?resubmit_button;  SolaceCtrlName = '?resubmit_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String20:3;  SolaceCtrlName = '?String20:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Total_warranty_temp;  SolaceCtrlName = '?Total_warranty_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupExchangeReason:2;  SolaceCtrlName = '?LookupExchangeReason:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet5;  SolaceCtrlName = '?Sheet5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab13;  SolaceCtrlName = '?Tab13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String9;  SolaceCtrlName = '?String9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt102;  SolaceCtrlName = '?Prompt102';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Status_End_Date;  SolaceCtrlName = '?job:Status_End_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Status_End_Time;  SolaceCtrlName = '?job:Status_End_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt:Job:Current_Status;  SolaceCtrlName = '?Prompt:Job:Current_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Current_Status;  SolaceCtrlName = '?job:Current_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Exchange_Status:Prompt;  SolaceCtrlName = '?JOB:Exchange_Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Status;  SolaceCtrlName = '?job:Exchange_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Loan_Status:Prompt;  SolaceCtrlName = '?JOB:Loan_Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Status;  SolaceCtrlName = '?job:Loan_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt79;  SolaceCtrlName = '?Prompt79';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt80;  SolaceCtrlName = '?Prompt80';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_time_remaining_temp;  SolaceCtrlName = '?job_time_remaining_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt81;  SolaceCtrlName = '?Prompt81';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_time_remaining_temp;  SolaceCtrlName = '?status_time_remaining_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Contact_History:2;  SolaceCtrlName = '?Contact_History:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ThirdPartyButton;  SolaceCtrlName = '?ThirdPartyButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Codes_Lookup;  SolaceCtrlName = '?Fault_Codes_Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelText;  SolaceCtrlName = '?CancelText';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job'
  OF ChangeRecord
    ActionMessage = 'Changing A Job'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateJOBS')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateJOBS')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Batch_Number_Text
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(job:Record,History::job:Record)
  SELF.AddHistoryField(?job:Auto_Search,4)
  SELF.AddHistoryField(?job:Last_Repair_Days,142)
  SELF.AddHistoryField(?job:Account_Number,39)
  SELF.AddHistoryField(?job:Order_Number,42)
  SELF.AddHistoryField(?job:Title,60)
  SELF.AddHistoryField(?job:Initial,61)
  SELF.AddHistoryField(?job:Surname,62)
  SELF.AddHistoryField(?job:Transit_Type,30)
  SELF.AddHistoryField(?job:Workshop,23)
  SELF.AddHistoryField(?job:Location,24)
  SELF.AddHistoryField(?job:Third_Party_Site,143)
  SELF.AddHistoryField(?job:ESN,16)
  SELF.AddHistoryField(?job:MSN,17)
  SELF.AddHistoryField(?job:ProductCode,18)
  SELF.AddHistoryField(?job:Model_Number,14)
  SELF.AddHistoryField(?job:Manufacturer,15)
  SELF.AddHistoryField(?job:Unit_Type,19)
  SELF.AddHistoryField(?job:Colour,20)
  SELF.AddHistoryField(?job:DOP,27)
  SELF.AddHistoryField(?job:Mobile_Number,70)
  SELF.AddHistoryField(?job:Turnaround_Time,168)
  SELF.AddHistoryField(?job:Chargeable_Job,13)
  SELF.AddHistoryField(?job:Charge_Type,36)
  SELF.AddHistoryField(?job:ThirdPartyDateDesp,149)
  SELF.AddHistoryField(?job:Warranty_Job,12)
  SELF.AddHistoryField(?job:Warranty_Charge_Type,37)
  SELF.AddHistoryField(?job:Date_Paid,88)
  SELF.AddHistoryField(?job:Estimate,144)
  SELF.AddHistoryField(?job:Estimate_If_Over,145)
  SELF.AddHistoryField(?job:Estimate_Accepted,146)
  SELF.AddHistoryField(?job:Estimate_Rejected,147)
  SELF.AddHistoryField(?JOB:Order_Number:2,42)
  SELF.AddHistoryField(?JOB:Transit_Type:2,30)
  SELF.AddHistoryField(?JOB:Workshop:2,23)
  SELF.AddHistoryField(?JOB:Location:2,24)
  SELF.AddHistoryField(?JOB:Third_Party_Site:2,143)
  SELF.AddHistoryField(?JOB:Special_Instructions:2,169)
  SELF.AddHistoryField(?job:ESN:2,16)
  SELF.AddHistoryField(?job:MSN:2,17)
  SELF.AddHistoryField(?job:Model_Number:2,14)
  SELF.AddHistoryField(?job:ProductCode:2,18)
  SELF.AddHistoryField(?job:Colour:2,20)
  SELF.AddHistoryField(?job:Mobile_Number:2,70)
  SELF.AddHistoryField(?job:DOP:2,27)
  SELF.AddHistoryField(?job:Turnaround_Time:2,168)
  SELF.AddHistoryField(?job:Charge_Type:2,36)
  SELF.AddHistoryField(?job:Warranty_Charge_Type:2,37)
  SELF.AddHistoryField(?job:Manufacturer:2,15)
  SELF.AddHistoryField(?job:Unit_Type:2,19)
  SELF.AddHistoryField(?job:Company_Name,64)
  SELF.AddHistoryField(?job:Postcode:2,63)
  SELF.AddHistoryField(?job:Telephone_Number,68)
  SELF.AddHistoryField(?job:Fax_Number,69)
  SELF.AddHistoryField(?job:Address_Line1,65)
  SELF.AddHistoryField(?job:Address_Line2,66)
  SELF.AddHistoryField(?job:Address_Line3,67)
  SELF.AddHistoryField(?job:Company_Name_Collection,72)
  SELF.AddHistoryField(?job:Address_Line1_Collection,73)
  SELF.AddHistoryField(?job:Address_Line2_Collection,74)
  SELF.AddHistoryField(?job:Address_Line3_Collection,75)
  SELF.AddHistoryField(?job:Postcode_Collection,71)
  SELF.AddHistoryField(?job:Telephone_Collection,76)
  SELF.AddHistoryField(?job:Company_Name_Delivery,79)
  SELF.AddHistoryField(?job:Address_Line1_Delivery,78)
  SELF.AddHistoryField(?job:Address_Line2_Delivery,80)
  SELF.AddHistoryField(?job:Address_Line3_Delivery,81)
  SELF.AddHistoryField(?job:Postcode_Delivery,77)
  SELF.AddHistoryField(?job:Telephone_Delivery,82)
  SELF.AddHistoryField(?job:Repair_Type,89)
  SELF.AddHistoryField(?job:Estimate_Ready,50)
  SELF.AddHistoryField(?job:Repair_Type_Warranty,90)
  SELF.AddHistoryField(?job:In_Repair,44)
  SELF.AddHistoryField(?job:On_Test,47)
  SELF.AddHistoryField(?job:Date_In_Repair:2,45)
  SELF.AddHistoryField(?job:Time_In_Repair:2,46)
  SELF.AddHistoryField(?job:Date_On_Test,48)
  SELF.AddHistoryField(?job:Time_On_Test,49)
  SELF.AddHistoryField(?job:Date_Completed,83)
  SELF.AddHistoryField(?job:Time_Completed,84)
  SELF.AddHistoryField(?job:QA_Passed,51)
  SELF.AddHistoryField(?job:Date_QA_Passed,52)
  SELF.AddHistoryField(?job:Time_QA_Passed,53)
  SELF.AddHistoryField(?JOB:Advance_Payment:2,96)
  SELF.AddHistoryField(?job:Authority_Number,25)
  SELF.AddHistoryField(?job:Courier,133)
  SELF.AddHistoryField(?job:Incoming_Courier,135)
  SELF.AddHistoryField(?job:Invoice_Date,179)
  SELF.AddHistoryField(?job:Invoice_Number_Warranty,185)
  SELF.AddHistoryField(?job:Invoice_Number,178)
  SELF.AddHistoryField(?job:Consignment_Number,134)
  SELF.AddHistoryField(?job:Incoming_Consignment_Number,136)
  SELF.AddHistoryField(?job:Date_Despatched,130)
  SELF.AddHistoryField(?job:Incoming_Date,137)
  SELF.AddHistoryField(?job:Invoice_Date_Warranty,180)
  SELF.AddHistoryField(?job:EDI_Batch_Number,175)
  SELF.AddHistoryField(?job:Despatch_User,132)
  SELF.AddHistoryField(?job:Despatch_Number,131)
  SELF.AddHistoryField(?job:Insurance_Reference_Number,26)
  SELF.AddHistoryField(?job:JobService,141)
  SELF.AddHistoryField(?job:Loan_Unit_Number,109)
  SELF.AddHistoryField(?job:Loan_User,111)
  SELF.AddHistoryField(?job:Exchange_Unit_Number,118)
  SELF.AddHistoryField(?job:Exchange_User,123)
  SELF.AddHistoryField(?job:Loan_Courier,112)
  SELF.AddHistoryField(?job:Exchange_Courier,124)
  SELF.AddHistoryField(?job:Loan_Consignment_Number,113)
  SELF.AddHistoryField(?job:Loan_Despatched,114)
  SELF.AddHistoryField(?job:Loan_Despatch_Number,116)
  SELF.AddHistoryField(?job:Loan_Despatched_User,115)
  SELF.AddHistoryField(?job:LoaService,117)
  SELF.AddHistoryField(?job:ExcService,129)
  SELF.AddHistoryField(?job:Exchange_Consignment_Number,125)
  SELF.AddHistoryField(?job:Exchange_Despatched,126)
  SELF.AddHistoryField(?job:Exchange_Despatch_Number,128)
  SELF.AddHistoryField(?job:Exchange_Despatched_User,127)
  SELF.AddHistoryField(?job:Ignore_Estimate_Charges,94)
  SELF.AddHistoryField(?job:Courier_Cost_Estimate,100)
  SELF.AddHistoryField(?job:Labour_Cost_Estimate,101)
  SELF.AddHistoryField(?job:Parts_Cost_Estimate,102)
  SELF.AddHistoryField(?job:Sub_Total_Estimate,103)
  SELF.AddHistoryField(?job:Ignore_Chargeable_Charges,92)
  SELF.AddHistoryField(?job:Courier_Cost,95)
  SELF.AddHistoryField(?job:Labour_Cost,97)
  SELF.AddHistoryField(?job:Parts_Cost,98)
  SELF.AddHistoryField(?job:Sub_Total,99)
  SELF.AddHistoryField(?job:Ignore_Warranty_Charges,93)
  SELF.AddHistoryField(?job:Courier_Cost_Warranty,104)
  SELF.AddHistoryField(?job:Labour_Cost_Warranty,105)
  SELF.AddHistoryField(?job:Parts_Cost_Warranty,106)
  SELF.AddHistoryField(?job:Sub_Total_Warranty,107)
  SELF.AddHistoryField(?job:Status_End_Date,164)
  SELF.AddHistoryField(?job:Status_End_Time,165)
  SELF.AddHistoryField(?job:Current_Status,38)
  SELF.AddHistoryField(?job:Exchange_Status,34)
  SELF.AddHistoryField(?job:Loan_Status,33)
  SELF.AddUpdateFile(Access:JOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:CITYSERV.Open
  Relate:COMMONCP.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:DISCOUNT.Open
  Relate:ESNMODEL.Open
  Relate:EXCAUDIT.Open
  Relate:EXCCHRGE.Open
  Relate:EXREASON.Open
  Relate:JOBEXACC.Open
  Relate:JOBNOTES_ALIAS.Open
  Relate:JOBPAYMT.Open
  Relate:JOBS2_ALIAS.Open
  Relate:NETWORKS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:REPTYDEF.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Access:USERS.UseFile
  Access:DEFCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MANUFACT.UseFile
  Access:LOAN.UseFile
  Access:LOANACC.UseFile
  Access:JOBLOHIS.UseFile
  Access:EXCHANGE.UseFile
  Access:JOBEXHIS.UseFile
  Access:EXCHACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBACC.UseFile
  Access:TRDMODEL.UseFile
  Access:TRDPARTY.UseFile
  Access:MANFAULT.UseFile
  Access:MANFAULO.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:WARPARTS.UseFile
  Access:STATUS.UseFile
  Access:LOANHIST.UseFile
  Access:EXCHHIST.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:COMMONFA.UseFile
  Access:COMMONWP.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:BOUNCER.UseFile
  Access:MODELCOL.UseFile
  Access:TRDBATCH.UseFile
  Access:REPAIRTY.UseFile
  Access:JOBNOTES.UseFile
  Access:MODELNUM.UseFile
  Access:STOCKTYP.UseFile
  Access:JOBSE.UseFile
  Access:TRAFAULT.UseFile
  Access:TRAFAULO.UseFile
  Access:CHARTYPE.UseFile
  Access:SUBCHRGE.UseFile
  Access:JOBSENG.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If thiswindow.request = Insertrecord
      get(jobnotes,0)
      if access:jobnotes.primerecord() = Level:Benign
          jbn:refnumber         = job:ref_number
          if access:jobnotes.tryinsert()
              access:jobnotes.cancelautoinc()
          end
      end!if access:jobnotes.primerecord() = Level:Benign
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber    = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Found
  
      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
          If access:JOBSE.primerecord() = Level:Benign
              jobe:RefNumber  = job:Ref_Number
              If access:JOBSE.tryinsert()
                  access:JOBSE.cancelautoinc()
              End!If access:JOBSE.tryinsert()
          End!If access:JOBSE.primerecord() = Level:Benign
      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  End
  access:jobnotes.clearkey(jbn:refnumberkey)
  jbn:refnumber = job:Ref_number
  if access:jobnotes.tryfetch(jbn:refnumberkey)
      If access:jobnotes.primerecord() = Level:Benign
          jbn:refnumber   = job:ref_number
          If access:jobnotes.tryinsert()
              access:jobnotes.cancelautoinc()
          End!If access:jobnotes.tryinsert()
      End!If access:jobnotes.primerecord() = Level:Benign
  Else
      tmp:FaultDescription    = jbn:Fault_Description
      tmp:EngineerNOtes       = jbn:Engineers_Notes
      tmp:InvoiceText         = jbn:Invoice_Text
  end!if access:jobnotes.tryfetch(jbn:refnumberkey) = Level:Benign
  
  access:JOBSE.clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      If access:JOBSE.primerecord() = Level:Benign
          jobe:RefNumber  = job:Ref_Number
          If access:JOBSE.tryinsert()
              access:JOBSE.cancelautoinc()
          End!If access:JOBSE.tryinsert()
      End!If access:JOBSE.primerecord() = Level:Benign
      !Assert(0,'Fetch Error')
  End! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
  tmp:Network = jobe:Network
  tmp:InWorkshopDate  = jobe:InWorkshopDate
  tmp:InWorkshopTime  = jobe:InWorkshopTime
  BRW12.Init(?List,Queue:Browse.ViewPosition,BRW12::View:Browse,Queue:Browse,Relate:PARTS,SELF)
  BRW21.Init(?List:2,Queue:Browse:1.ViewPosition,BRW21::View:Browse,Queue:Browse:1,Relate:JOBSTAGE,SELF)
  BRW77.Init(?List:3,Queue:Browse:2.ViewPosition,BRW77::View:Browse,Queue:Browse:2,Relate:ESTPARTS,SELF)
  BRW79.Init(?List:4,Queue:Browse:3.ViewPosition,BRW79::View:Browse,Queue:Browse:3,Relate:WARPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  !Set Defaults
  ?sheet4{prop:wizard} = 1
  Set(Defaults)
  access:defaults.next()
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  If de2:AllocateEngPassword
      ?Lookup_Engineer{prop:Hide} = 1
  End !de2:AllocateEngPassword
  
  access:users.clearkey(use:password_key)
  use:password    =glo:password
  access:users.fetch(use:password_key)
  
  If thiswindow.request = Insertrecord
      if def:force_initial_transit_type = 'B'
          ?job:transit_type{prop:req} = 1
      end !if def:force_initial transit type = 'B'
  
      if def:force_mobile_number = 'B'  and def:show_mobile_number <> 'YES'
          ?job:mobile_number{prop:req} = 1
      end !if def:force_mobile_number = 'B'
  
      if def:force_model_number = 'B' 
          ?job:model_number{prop:req} = 1
      end !if def:force_model_number = 'B'
  
      if def:force_unit_type = 'B' 
          ?job:unit_type{prop:req} = 1
      end !if def:force_unit_type = 'B'
  
  !    if def:force_fault_description = 'B'
  !        ?tmp:FaultDescription{prop:req} = 1
  !    end !if def:force_fault_description = 'B'
  
      if def:force_esn = 'B' 
          ?job:esn{prop:req} = 1
      end !if def:force_esn = 'B'
  
      If def:force_incoming_courier = 'B' 
          ?job:incoming_courier{prop:req} = 1
      End!If def:force_incoming_courier = 'B' 
  
      If def:force_outoing_courier = 'B' 
          ?job:courier{prop:req} = 1
      End!If def:force_incoming_courier = 'B' 
  
  
      if def:force_invoice_text = 'B' 
          !?tmp:InvoiceText{prop:req} = 1
      end !if def:force_invoice_text = 'B'
  
      if def:force_authority_number = 'B'  And def:hide_authority_number <> 'YES'
          ?job:authority_number{prop:req} = 1
      end !if def:force_authority_number = 'B'
  
  
      If DEF:Order_Number = 'B'
          ?job:order_number{prop:req} = 1
      End!If DEF:Order_Number = 'B'
  End!If thiswindow.request = Insertrecord
  
  Do CityLink_service
  Do UPS_Service
  
  If GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?JobLevel{prop:Hide} = 1
      ?jobe:SkillLevel{prop:Hide} = 1
  End !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  If Thiswindow.Request = ChangeRecord
      !Once a Loan/Exchange reason has been picked, you should not be allowed to change it
      !no matter what.
      If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          If jobe:LoanReason <> '' or job:Loan_Unit_Number <> ''
              ?jobe:LoanReason{prop:Disable} = 1
              ?LookupLoanReason{prop:Disable} = 1
          End !If jobe:LoanReason <> ''
      End !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          If jobe:ExchangeReason <> '' or job:Exchange_Unit_Number <> ''
              ?jobe:ExchangeReason{prop:Disable} = 1
              ?LookupExchangeReason{prop:Disable} = 1
          End !If jobe:LoanReason <> ''
      End !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
      ! Start Change 2186 BE(09/12/03)
      IF ((job:Despatched = 'YES') AND (SecurityCheck('AMEND DESPATCHED ADDRESS') <> Level:Benign)) THEN
          HIDE(?AmendAddresses)
      END
      ! End Change 2186 BE(09/12/03)
  
      ! Start Change 4554 BE(27/07/2004)
      IF ((job:who_booked = 'WEB') AND (job:order_number <> '')) THEN
          access:AUDIT.clearkey(aud:ref_number_key)
          aud:Ref_Number = job:ref_number
          SET(aud:ref_number_key, aud:ref_number_key)
          LOOP WHILE(access:AUDIT.next() = Level:Benign)
              IF (aud:Ref_Number <> job:ref_number) THEN
                  BREAK
              END
              IF (aud:Notes[1 : 24] = 'JOB CREATED BY WEBMASTER') THEN
                  ?job:order_number{prop:Disable} = 1
              END
          END
      END
      ! End Change 4554 BE(27/07/2004)
  
  End !Thiswindow.Request = ChangeRecord
  
  ! Start Change 2821 BE(24/06/03)
  ValidateMobileNumber = GETINI('VALIDATE','MobileNumber',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2821 BE(24/06/03)
  
  ! Start Change 2851 BE(02/07/2003)
  tmp:SuspendPrintJobcard = GETINI('PRINTING','SuspendPrintingJobCard',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2851 BE(02/07/2003)
  
  ! Start Change 2618 BE(05/08/03)
  JSC:Colour[1] = COLOR:Maroon
  JSC:Action[1] = GETINI('JobStatusColours','MaroonAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[1] = GETINI('JobStatusColours','Maroon',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[2] = COLOR:Green
  JSC:Action[2] = GETINI('JobStatusColours','GreenAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[2] = GETINI('JobStatusColours','Green',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[3] = COLOR:Olive
  JSC:Action[3] = GETINI('JobStatusColours','OliveAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[3] = GETINI('JobStatusColours','Olive',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[4] = COLOR:Navy
  JSC:Action[4] = GETINI('JobStatusColours','NavyAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[4] = GETINI('JobStatusColours','Navy',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[5] = COLOR:Purple
  JSC:Action[5] = GETINI('JobStatusColours','PurpleAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[5] = GETINI('JobStatusColours','Purple',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[6] = COLOR:Teal
  JSC:Action[6] = GETINI('JobStatusColours','TealAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[6] = GETINI('JobStatusColours','Teal',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[7] = COLOR:Gray
  JSC:Action[7] = GETINI('JobStatusColours','GrayAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[7] = GETINI('JobStatusColours','Gray',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[8] = COLOR:Blue
  JSC:Action[8] = GETINI('JobStatusColours','BlueAction',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Status[8] = GETINI('JobStatusColours','Blue',,CLIP(PATH())&'\SB2KDEF.INI')
  
  HIDE(?StatusWarning)
  LOOP ix# = 1 TO 8
      IF ((JSC:Status[ix#] <> '') AND (job:current_status = JSC:Status[ix#])) THEN
          ?StatusWarning{PROP:Text} = job:current_status[5 : LEN(CLIP(job:current_status))]
          UNHIDE(?StatusWarning)
          !IF (JSC:Action[ix#]) THEN
          !    HIDE(?OK)
          !END
          BREAK
      END
  END
  ! End Change 2618 BE(05/08/03)
  
  ! Start - Show the CRC despatch dates button - TrkBs: 5646 (DBH: 14-04-2005)
  If Instring('C.R.C.',def:User_Name,1,1)
      ?CRCDespatchDateButton{prop:Hide} = False
  End ! Instring('C.R.C.',def:User_Name,1,1)
  ! End   - Show the CRC despatch dates button - TrkBs: 5646 (DBH: 14-04-2005)
   
  Do AreThereNotes
  !Complete at QA?
  Access:MANUFACT.ClearKey(man:Manufacturer_Key)
  man:Manufacturer = job:Manufacturer
  If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Found
      IF man:QAAtCompletion
          ?Today_Button{prop:Text} = 'Complete Repair [F10]'
      End !IF man:QAAtCompletion
  
      ! Start Change 4239 BE(20/05/04)
      MSN_Field_Mask = man:MSNFieldMask
      ! End Change 4239 BE(20/05/04)
  
  Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!Complete AT QA?!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
  !Save Accesories incase they change
  If GetTempPathA(255,TempFilePath)
      If Sub(TempFilePath,-1,1) = '\'
          tmp:SaveAccessory = Clip(TempFilePath) & 'SAVACC' & Clock() & '.TMP'
      Else !If Sub(TempFilePath,-1,1) = '\'
          tmp:SaveAccessory = Clip(TempFilePath) & '\SAVACC' & Clock() & '.TMP'
      End !If Sub(TempFilePath,-1,1) = '\'
      Remove(tmp:SaveAccessory)
  
      Create(SaveAccessory)
      Open(SaveAccessory)
      If ~Error()
          Save_jac_ID = Access:JOBACC.SaveFile()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
          Loop
              If Access:JOBACC.NEXT()
                 Break
              End !If
              If jac:Ref_Number <> job:Ref_Number      |
                  Then Break.  ! End If
              savacc:Accessory = jac:Accessory
              Add(SaveAccessory)
          End !Loop
          Access:JOBACC.RestoreFile(Save_jac_ID)
      End !If ~Error()
      Close(SaveAccessory)
  End
  
  
  !Record in Use?
  If ThisWindow.Request <> Insertrecord
      ! For UTL: Make job view only, and remove record locking (DBH: 02-11-2005)
      If GETINI('DEFAULTS','View',,Clip(Path()) & '\SB2KDEF.INI') = 1
          ThisWindow.Request = ViewRecord
          ?OK{Prop:Hide} = True
      Else ! If GETINI('DEFAULTS','View',,Clip(Path()) & '\SB2KDEF.INI') = 1
          Pointer# = Pointer(JOBS)
          Hold(JOBS,1)
          Get(JOBS,Pointer#)
          If Errorcode() = 43
              Case MessageEx('This job is currently in use by another station.<13><10>The screen will be VIEW ONLY','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
              End!Case MessageEx
              ThisWindow.Request = ViewRecord
              ?OK{prop:Hide} = 1
          End !If Errorcode() = 43
      End ! If GETINI('DEFAULTS','View',,Clip(Path()) & '\SB2KDEF.INI') = 1
  End !ThisWindow.Request <> Insertrecord
  Do RecolourWindow
  ?List:3{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?job:ESN:2{prop:boxed} = False
  ?job:MSN:2{prop:boxed} = False
  ?tmp:IncomingIMEI{prop:boxed} = False
  ?tmp:IncomingMSN{prop:boxed} = False
  ?tmp:ExchangeIMEI{prop:boxed} = False
  ?tmp:ExchangeMSN{prop:boxed} = False
  ?job:Model_Number:2{prop:boxed} = False
  ?job:ProductCode:2{prop:boxed} = False
  ?job:Colour:2{prop:boxed} = False
  ?job:Mobile_Number:2{prop:boxed} = False
  ?job:Charge_Type:2{prop:boxed} = False
  ?job:Warranty_Charge_Type:2{prop:boxed} = False
  ?tmp:EstimateDetails{prop:boxed} = False
  ?jobe:SIMNumber{prop:boxed} = False
  ?Batch_Number_Text{prop:boxed} = False
  ?job:Manufacturer:2{prop:boxed} = False
  ?job:Unit_Type:2{prop:boxed} = False
  ?job:Turnaround_Time:2{prop:boxed} = False
  ?JOB:Exchange_Despatched{Prop:Alrt,255} = MouseLeft2
  ?JOB:Date_Despatched{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?job:Account_Number{Prop:Tip} AND ~?LookupTradeAccount{Prop:Tip}
     ?LookupTradeAccount{Prop:Tip} = 'Select ' & ?job:Account_Number{Prop:Tip}
  END
  IF ?job:Account_Number{Prop:Msg} AND ~?LookupTradeAccount{Prop:Msg}
     ?LookupTradeAccount{Prop:Msg} = 'Select ' & ?job:Account_Number{Prop:Msg}
  END
  IF ?job:Transit_Type{Prop:Tip} AND ~?LookupTransitType{Prop:Tip}
     ?LookupTransitType{Prop:Tip} = 'Select ' & ?job:Transit_Type{Prop:Tip}
  END
  IF ?job:Transit_Type{Prop:Msg} AND ~?LookupTransitType{Prop:Msg}
     ?LookupTransitType{Prop:Msg} = 'Select ' & ?job:Transit_Type{Prop:Msg}
  END
  IF ?job:Location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?job:Location{Prop:Tip}
  END
  IF ?job:Location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?job:Location{Prop:Msg}
  END
  IF ?job:Model_Number{Prop:Tip} AND ~?LookupModelNumber{Prop:Tip}
     ?LookupModelNumber{Prop:Tip} = 'Select ' & ?job:Model_Number{Prop:Tip}
  END
  IF ?job:Model_Number{Prop:Msg} AND ~?LookupModelNumber{Prop:Msg}
     ?LookupModelNumber{Prop:Msg} = 'Select ' & ?job:Model_Number{Prop:Msg}
  END
  IF ?job:Charge_Type{Prop:Tip} AND ~?LookupChargeType{Prop:Tip}
     ?LookupChargeType{Prop:Tip} = 'Select ' & ?job:Charge_Type{Prop:Tip}
  END
  IF ?job:Charge_Type{Prop:Msg} AND ~?LookupChargeType{Prop:Msg}
     ?LookupChargeType{Prop:Msg} = 'Select ' & ?job:Charge_Type{Prop:Msg}
  END
  IF ?job:Warranty_Charge_Type{Prop:Tip} AND ~?LookupWarrantyChargeType{Prop:Tip}
     ?LookupWarrantyChargeType{Prop:Tip} = 'Select ' & ?job:Warranty_Charge_Type{Prop:Tip}
  END
  IF ?job:Warranty_Charge_Type{Prop:Msg} AND ~?LookupWarrantyChargeType{Prop:Msg}
     ?LookupWarrantyChargeType{Prop:Msg} = 'Select ' & ?job:Warranty_Charge_Type{Prop:Msg}
  END
  IF ?tmp:Network:2{Prop:Tip} AND ~?LookupNetwork:2{Prop:Tip}
     ?LookupNetwork:2{Prop:Tip} = 'Select ' & ?tmp:Network:2{Prop:Tip}
  END
  IF ?tmp:Network:2{Prop:Msg} AND ~?LookupNetwork:2{Prop:Msg}
     ?LookupNetwork:2{Prop:Msg} = 'Select ' & ?tmp:Network:2{Prop:Msg}
  END
  IF SELF.Request = ViewRecord
    DISABLE(?LookupTransitType)
    DISABLE(?Despatched_Calendar)
    ?job:Courier_Cost{PROP:ReadOnly} = True
    ?job:Labour_Cost{PROP:ReadOnly} = True
    DISABLE(?resubmit_button)
    DISABLE(?LookupExchangeReason:2)
    ?job:Current_Status{PROP:ReadOnly} = True
    ?job:Exchange_Status{PROP:ReadOnly} = True
    ?job:Loan_Status{PROP:ReadOnly} = True
    DISABLE(?OK)
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW12.Q &= Queue:Browse
  BRW12.RetainRow = 0
  BRW12.AddSortOrder(,par:Part_Number_Key)
  BRW12.AddRange(par:Ref_Number,job:Ref_Number)
  BRW12.AddLocator(BRW12::Sort0:Locator)
  BRW12::Sort0:Locator.Init(,par:Part_Number,1,BRW12)
  BIND('ordered_temp',ordered_temp)
  BIND('chargeable_part_total_temp',chargeable_part_total_temp)
  BRW12.AddField(par:Part_Number,BRW12.Q.par:Part_Number)
  BRW12.AddField(par:Description,BRW12.Q.par:Description)
  BRW12.AddField(par:Order_Number,BRW12.Q.par:Order_Number)
  BRW12.AddField(ordered_temp,BRW12.Q.ordered_temp)
  BRW12.AddField(par:Quantity,BRW12.Q.par:Quantity)
  BRW12.AddField(chargeable_part_total_temp,BRW12.Q.chargeable_part_total_temp)
  BRW12.AddField(par:Part_Ref_Number,BRW12.Q.par:Part_Ref_Number)
  BRW12.AddField(par:Record_Number,BRW12.Q.par:Record_Number)
  BRW12.AddField(par:Ref_Number,BRW12.Q.par:Ref_Number)
  BRW21.Q &= Queue:Browse:1
  BRW21.RetainRow = 0
  BRW21.AddSortOrder(,jst:Ref_Number_Key)
  BRW21.AddRange(jst:Ref_Number,Relate:JOBSTAGE,Relate:JOBS)
  BRW21.AddLocator(BRW21::Sort0:Locator)
  BRW21::Sort0:Locator.Init(,jst:Date,1,BRW21)
  BRW21.AddField(jst:Job_Stage,BRW21.Q.jst:Job_Stage)
  BRW21.AddField(jst:Date,BRW21.Q.jst:Date)
  BRW21.AddField(jst:Time,BRW21.Q.jst:Time)
  BRW21.AddField(jst:Ref_Number,BRW21.Q.jst:Ref_Number)
  BRW77.Q &= Queue:Browse:2
  BRW77.RetainRow = 0
  BRW77.AddSortOrder(,epr:Part_Number_Key)
  BRW77.AddRange(epr:Ref_Number,job:Ref_Number)
  BRW77.AddLocator(BRW77::Sort0:Locator)
  BRW77::Sort0:Locator.Init(,epr:Part_Number,1,BRW77)
  BIND('estimate_parts_cost_temp',estimate_parts_cost_temp)
  BRW77.AddField(epr:Part_Number,BRW77.Q.epr:Part_Number)
  BRW77.AddField(epr:Description,BRW77.Q.epr:Description)
  BRW77.AddField(epr:Quantity,BRW77.Q.epr:Quantity)
  BRW77.AddField(estimate_parts_cost_temp,BRW77.Q.estimate_parts_cost_temp)
  BRW77.AddField(epr:record_number,BRW77.Q.epr:record_number)
  BRW77.AddField(epr:Ref_Number,BRW77.Q.epr:Ref_Number)
  BRW79.Q &= Queue:Browse:3
  BRW79.RetainRow = 0
  BRW79.AddSortOrder(,wpr:Part_Number_Key)
  BRW79.AddRange(wpr:Ref_Number,Relate:WARPARTS,Relate:JOBS)
  BRW79.AddLocator(BRW79::Sort0:Locator)
  BRW79::Sort0:Locator.Init(,wpr:Part_Number,1,BRW79)
  BIND('warranty_ordered_temp',warranty_ordered_temp)
  BIND('warranty_part_total_temp',warranty_part_total_temp)
  BRW79.AddField(wpr:Part_Number,BRW79.Q.wpr:Part_Number)
  BRW79.AddField(wpr:Description,BRW79.Q.wpr:Description)
  BRW79.AddField(wpr:Order_Number,BRW79.Q.wpr:Order_Number)
  BRW79.AddField(warranty_ordered_temp,BRW79.Q.warranty_ordered_temp)
  BRW79.AddField(wpr:Quantity,BRW79.Q.wpr:Quantity)
  BRW79.AddField(warranty_part_total_temp,BRW79.Q.warranty_part_total_temp)
  BRW79.AddField(wpr:Part_Ref_Number,BRW79.Q.wpr:Part_Ref_Number)
  BRW79.AddField(wpr:Record_Number,BRW79.Q.wpr:Record_Number)
  BRW79.AddField(wpr:Ref_Number,BRW79.Q.wpr:Ref_Number)
  ! Start Change 4145 BE(21/04/04)
  BIND('jst:date', jst:date)
  BIND('jst:time', jst:time)
  BRW21.SetOrder('jst:date,jst:time')
  ! End Change 4145 BE(21/04/04)
  IF ?job:Workshop{Prop:Checked} = True
    ENABLE(?Lookup_Engineer)
  END
  IF ?job:Workshop{Prop:Checked} = False
    DISABLE(?Lookup_Engineer)
  END
  IF ?job:Chargeable_Job{Prop:Checked} = True
    UNHIDE(?JOB:Charge_Type)
    UNHIDE(?LookupChargeType)
  END
  IF ?job:Chargeable_Job{Prop:Checked} = False
    HIDE(?JOB:Charge_Type)
    HIDE(?LookupChargeType)
  END
  IF ?job:Warranty_Job{Prop:Checked} = True
    UNHIDE(?job:charge_type:prompt)
    UNHIDE(?LookupWarrantyChargeType)
    UNHIDE(?job:Warranty_Charge_Type)
  END
  IF ?job:Warranty_Job{Prop:Checked} = False
    HIDE(?JOB:Warranty_Charge_Type)
    HIDE(?LookupWarrantyChargeType)
  END
  FDCB6.Init(job:Unit_Type,?job:Unit_Type,Queue:FileDropCombo:1.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo:1
  FDCB6.AddSortOrder(uni:ActiveKey)
  FDCB6.AddRange(uni:Active,tmp:one)
  FDCB6.AddField(uni:Unit_Type,FDCB6.Q.uni:Unit_Type)
  FDCB6.AddUpdateField(uni:Unit_Type,job:Unit_Type)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  FDCB40.Init(job:Loan_Courier,?job:Loan_Courier,Queue:FileDropCombo:8.ViewPosition,FDCB40::View:FileDropCombo,Queue:FileDropCombo:8,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB40.Q &= Queue:FileDropCombo:8
  FDCB40.AddSortOrder(cou:Courier_Key)
  FDCB40.AddField(cou:Courier,FDCB40.Q.cou:Courier)
  FDCB40.AddUpdateField(cou:Courier,job:Loan_Courier)
  ThisWindow.AddItem(FDCB40.WindowComponent)
  FDCB40.DefaultFill = 0
  FDCB41.Init(job:Exchange_Courier,?job:Exchange_Courier,Queue:FileDropCombo:9.ViewPosition,FDCB41::View:FileDropCombo,Queue:FileDropCombo:9,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB41.Q &= Queue:FileDropCombo:9
  FDCB41.AddSortOrder(cou:Courier_Key)
  FDCB41.AddField(cou:Courier,FDCB41.Q.cou:Courier)
  FDCB41.AddUpdateField(cou:Courier,job:Exchange_Courier)
  ThisWindow.AddItem(FDCB41.WindowComponent)
  FDCB41.DefaultFill = 0
  FDCB8.Init(job:Courier,?job:Courier,Queue:FileDropCombo:11.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:11,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:11
  FDCB8.AddSortOrder(cou:Courier_Key)
  FDCB8.AddField(cou:Courier,FDCB8.Q.cou:Courier)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB17.Init(job:Turnaround_Time,?job:Turnaround_Time,Queue:FileDropCombo:12.ViewPosition,FDCB17::View:FileDropCombo,Queue:FileDropCombo:12,Relate:TURNARND,ThisWindow,GlobalErrors,0,1,0)
  FDCB17.Q &= Queue:FileDropCombo:12
  FDCB17.AddSortOrder(tur:Turnaround_Time_Key)
  FDCB17.AddField(tur:Turnaround_Time,FDCB17.Q.tur:Turnaround_Time)
  FDCB17.AddField(tur:Days,FDCB17.Q.tur:Days)
  FDCB17.AddField(tur:Hours,FDCB17.Q.tur:Hours)
  ThisWindow.AddItem(FDCB17.WindowComponent)
  FDCB17.DefaultFill = 0
  FDCB10.Init(job:Incoming_Courier,?job:Incoming_Courier,Queue:FileDropCombo:3.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:3,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:3
  FDCB10.AddSortOrder(cou:Courier_Key)
  FDCB10.AddField(cou:Courier,FDCB10.Q.cou:Courier)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  FDCB25.Init(job:Colour,?job:Colour,Queue:FileDropCombo:2.ViewPosition,FDCB25::View:FileDropCombo,Queue:FileDropCombo:2,Relate:MODELCOL,ThisWindow,GlobalErrors,0,1,0)
  FDCB25.Q &= Queue:FileDropCombo:2
  FDCB25.AddSortOrder(moc:Colour_Key)
  FDCB25.AddRange(moc:Model_Number,job:Model_Number)
  FDCB25.AddField(moc:Colour,FDCB25.Q.moc:Colour)
  FDCB25.AddField(moc:Record_Number,FDCB25.Q.moc:Record_Number)
  ThisWindow.AddItem(FDCB25.WindowComponent)
  FDCB25.DefaultFill = 0
  FDCB16.Init(job:LoaService,?job:LoaService,Queue:FileDropCombo:4.ViewPosition,FDCB16::View:FileDropCombo,Queue:FileDropCombo:4,Relate:CITYSERV,ThisWindow,GlobalErrors,0,1,0)
  FDCB16.Q &= Queue:FileDropCombo:4
  FDCB16.AddSortOrder(cit:ServiceKey)
  FDCB16.AddField(cit:Service,FDCB16.Q.cit:Service)
  FDCB16.AddField(cit:Description,FDCB16.Q.cit:Description)
  FDCB16.AddField(cit:RefNumber,FDCB16.Q.cit:RefNumber)
  ThisWindow.AddItem(FDCB16.WindowComponent)
  FDCB16.DefaultFill = 0
  FDCB24.Init(job:ExcService,?job:ExcService,Queue:FileDropCombo:5.ViewPosition,FDCB24::View:FileDropCombo,Queue:FileDropCombo:5,Relate:CITYSERV,ThisWindow,GlobalErrors,0,1,0)
  FDCB24.Q &= Queue:FileDropCombo:5
  FDCB24.AddSortOrder(cit:ServiceKey)
  FDCB24.AddField(cit:Service,FDCB24.Q.cit:Service)
  FDCB24.AddField(cit:Description,FDCB24.Q.cit:Description)
  FDCB24.AddField(cit:RefNumber,FDCB24.Q.cit:RefNumber)
  ThisWindow.AddItem(FDCB24.WindowComponent)
  FDCB24.DefaultFill = 0
  FDCB30.Init(job:JobService,?job:JobService,Queue:FileDropCombo:6.ViewPosition,FDCB30::View:FileDropCombo,Queue:FileDropCombo:6,Relate:CITYSERV,ThisWindow,GlobalErrors,0,1,0)
  FDCB30.Q &= Queue:FileDropCombo:6
  FDCB30.AddSortOrder(cit:ServiceKey)
  FDCB30.AddField(cit:Service,FDCB30.Q.cit:Service)
  FDCB30.AddField(cit:Description,FDCB30.Q.cit:Description)
  FDCB30.AddField(cit:RefNumber,FDCB30.Q.cit:RefNumber)
  ThisWindow.AddItem(FDCB30.WindowComponent)
  FDCB30.DefaultFill = 0
  FDCB46.Init(job:ProductCode,?job:ProductCode,Queue:FileDropCombo.ViewPosition,FDCB46::View:FileDropCombo,Queue:FileDropCombo,Relate:PRODCODE,ThisWindow,GlobalErrors,0,1,0)
  FDCB46.Q &= Queue:FileDropCombo
  FDCB46.AddSortOrder(prd:ProductCodeKey)
  FDCB46.AddField(prd:ProductCode,FDCB46.Q.prd:ProductCode)
  FDCB46.AddField(prd:RecordNumber,FDCB46.Q.prd:RecordNumber)
  ThisWindow.AddItem(FDCB46.WindowComponent)
  FDCB46.DefaultFill = 0
  BRW12.AskProcedure = 8
  BRW77.AskProcedure = 9
  BRW79.AskProcedure = 10
  BRW12.popup.additem('Add Adjustment','Popup2','',1)
  BRW12.popup.additemevent('Popup2',event:accepted,?ChargeableAdjustment)
  BRW12.popup.seticon('Popup2','insert.gif')
  BRW12.popup.additem('-','EPopup20','',1)
  BRW79.popup.additem('Add Adjustment','Popup2','',1)
  BRW79.popup.additemevent('Popup2',event:accepted,?WarrantyAdjustment)
  BRW79.popup.seticon('Popup2','insert.gif')
  BRW79.popup.additem('-','EPopup20','',1)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  If GETINI('DEFAULTS','View',,Clip(Path()) & '\SB2KDEF.INI') <> 1
      ! Remove Record Locking for UTL (DBH: 02-11-2005)
      Release(JOBS)
  End  !If GETINI('DEFAULTS','View',,Clip(Path()) & '\SB2KDEF.INI') <> 1    !
  !If thiswindow.request = Insertrecord
      set(Defaults)
      access:defaults.next()
      IF def:use_job_label <> 'YES'
          print_label_temp = 'NO'
      End!IF def:use_job_label <> 'YES'
  !End!If thiswindow.request = Insertrecord
  
  
  !If print_label_temp = 'YES'
      Set(defaults)
      access:defaults.next()
      Case def:label_printer_type
          Of 'TEC B-440 / B-442'
              Label_Type_temp = 1
          Of 'TEC B-452'
              label_type_temp = 2
      End!Case def:themal_printer_type
  !End!If print_label_temp = 'YES'
  
  saved_ref_number_Temp   = job:ref_number
  saved_esn_temp          = job:esn
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:CITYSERV.Close
    Relate:COMMONCP.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:DISCOUNT.Close
    Relate:ESNMODEL.Close
    Relate:EXCAUDIT.Close
    Relate:EXCCHRGE.Close
    Relate:EXREASON.Close
    Relate:JOBEXACC.Close
    Relate:JOBNOTES_ALIAS.Close
    Relate:JOBPAYMT.Close
    Relate:JOBS2_ALIAS.Close
    Relate:NETWORKS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:REPTYDEF.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
  END
  If print_label_temp = 'YES' And thiswindow.response <> 2
      glo:select1 = job:Ref_number
      case label_type_temp
          Of 1
              If job:bouncer = 'B'
                  Thermal_Labels('SE')
              Else!If job:bouncer = 'B'
                  If ExchangeAccount(job:Account_Number)
                      Thermal_Labels('ER')
                  Else !If ExchangeAccount(job:Account_Number)
                      Thermal_Labels('')
                  End !If ExchangeAccount(job:Account_Number)
                  
              End!If job:bouncer = 'B'
          Of 2
              Thermal_Labels_B452
      End!case label_type_temp
      glo:select1 = ''
  End!If print_label_temp = 'YES'
  
  If thiswindow.response = 2
      If thiswindow.request = Insertrecord
          access:stock.open()
          access:stock.usefile()
          access:stohist.open()
          access:stohist.usefile()
          access:ordpend.open()
          access:ordpend.usefile()
          Clear(parts_queue_temp)
          Loop x# = 1 To Records(parts_queue_temp)
              Get(parts_queue_temp,x#)
              If partmp:exclude_from_order <> 'YES'
                  If partmp:pending_ref_number <> ''
                      access:ordpend.clearkey(ope:ref_number_key)
                      ope:ref_number = partmp:pending_ref_number
                      if access:ordpend.fetch(ope:ref_number_key) = Level:benign
                          Delete(ordpend)
                      End!if access:ordpend.fetch(ope:ref_number_key) = Level:benign
                  Else!If partmp:pending_ref_number <> ''
                      If partmp:part_ref_number <> ''
                          access:stock.clearkey(sto:ref_number_key)
                          sto:ref_number  = partmp:part_ref_number
                          If access:stock.fetch(sto:ref_number_key) = Level:Benign
                              sto:quantity_stock += partmp:quantity
                              access:stock.update()
                          End!If access:stock.clearkey(sto:ref_number_key) = Level:Benign
                          If access:stohist.primerecord() = Level:Benign
                              shi:information          = 'JOB CANCELLED'
                              shi:job_number           = job:ref_number
                              shi:ref_number           = sto:ref_number
                              shi:transaction_type     = 'REC'
                              shi:despatch_note_number = ''
                              shi:quantity             = partmp:quantity
                              shi:date                 = Today()
                              shi:purchase_cost        = sto:purchase_cost
                              shi:sale_cost            = sto:sale_cost
                              shi:retail_cost          = sto:retail_cost
                              Access:users.clearkey(use:password_key)
                              use:password =glo:password
                              Access:users.Fetch(use:password_key)
                              shi:user                 = use:user_code
                              shi:notes                = 'STOCK RECREDITED'
                              Access:stohist.Insert()
                          End!If access:stohist.primerecord() = Level:Benign
                      End!If partmp:part_ref_number <> ''
                  End!If partmp:pending_ref_number <> ''
              End!If par:exclude_from_order <> 'YES'
          End!Loop x# = 1 To Records(parts_queue_temp)
          access:stock.close()
          access:stohist.close()
          access:ordpend.close()
      End!If thiswindow.request = Insertrecord
      If old_exchange_unit_number_temp <> new_exchange_unit_number_temp
          access:jobs.open()
          access:jobs.usefile()
          access:jobs.clearkey(job:ref_number_key)
          access:exchange.open()
          access:Exchange.usefile()
          access:exchhist.open()
          access:exchhist.usefile()
          access:jobexhis.open()
          access:jobexhis.usefile()
          job:ref_number  = saved_ref_number_temp
          If access:jobs.fetch(job:ref_number_key) = Level:Benign
              If thiswindow.request <> Insertrecord
                  Case MessageEx('The Exchange Unit you attached has now been Re-Stocked.','ServiceBase 2000',|
                                 'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If thiswindow.request <> Insertrecord
              access:exchange.clearkey(xch:ref_number_key)
              xch:ref_number = new_exchange_unit_number_temp
              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                  xch:available = 'AVL'
                  xch:job_number = ''
                  access:exchange.update()
                  get(exchhist,0)
                  if access:exchhist.primerecord() = level:benign
                      exh:ref_number    = xch:ref_number
                      exh:date          = Today()
                      exh:time          = Clock()
                      access:users.clearkey(use:password_key)
                      use:password    =glo:password
                      access:users.fetch(use:password_key)
                      exh:user          = use:user_code
                      exh:status        = 'UNIT RE-STOCKED, CANCELLED JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                      if access:exchhist.insert()
                          access:exchhist.cancelautoinc()
                      end
                  end!if access:exchhist.primerecord() = level:benign
              end!if access:exchange.fetch(xch:ref_number_key)
              job:exchange_unit_number = ''
              get(jobexhis,0)
              if access:jobexhis.primerecord() = level:benign
                  jxh:ref_number       = job:ref_number
                  jxh:loan_unit_number = new_exchange_unit_number_temp
                  jxh:date             = Today()
                  jxh:time             = Clock()
                  jxh:user             = use:user_code
                  jxh:status           = 'UNIT RETURNED - JOB CANCELLED'
                  access:jobexhis.insert()
                  access:jobs.update()
              End!if access:jobexhis.primerecord() = level:benign
          End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
          access:jobs.close()
          access:exchange.close()
          access:exchhist.close()
          access:jobexhis.close()
      End!If old_exchange_unit_number_temp <> new_exchange_unit_number_temp
  Else!If ok_pressed_temp = 0
  End!If ok_pressed_temp = 0
  If thiswindow.response <> 2
      !Include('Bouncer.inc')
  Else!If ok not pressed
  End!If ok_pressed_temp = 1
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateJOBS',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    job:date_booked = Today()
    job:Physical_Damage = 'NO'
    job:Insurance = 'NO'
    job:Intermittent_Fault = 'NO'
    job:Internal_Status = 'NEWJOB'
    job:time_booked = Clock()
    job:Loan_Status = '101 NOT ISSUED'
    job:Exchange_Status = '101 NOT ISSUED'
    job:Workshop = 'NO'
    job:Estimate = 'NO'
    job:Third_Party_Printed = 'NO'
    job:EDI = 'XXX'
    job:Completed = 'NO'
    job:Despatched = 'NO'
    job:Paid = 'NO'
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  jbn:RefNumber = job:Ref_Number                      ! Assign linking field value
  Access:JOBNOTES.Fetch(jbn:RefNumberKey)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  !What should be happening when deleting a part from a job
  !
  !If this an external part
  !    Delete Part
  !Else!If this an external part
  !    If part has been ordered (part order number exists)
  !        If the part has been received (date received exists)    
  !            If it is a stock part (stock ref number exists)
  !                If stock part is a sundry
  !                    Delete Part                
  !                Else!            If stock part is a sundry
  !                    stock quantity += part quantity
  !                    Create Stock History (Add)
  !                    Delete Part
  !                End!            If stock part is a sundry
  !            Else!If it is a stock part (stock ref number exists)
  !                Create Stock Part
  !                Create Stock History (Add)
  !                Ask to pick locations
  !                Delete Part
  !            End!If it is a stock part (stock ref number exists)
  !        Else!If the part has been received (date received exists)
  !            Mark the Part on the order as received with quantity = 0
  !            Delete Part    
  !        End!If the part has been received (date received exists)
  !    Else!If part has been ordered (part order number exists)
  !        If is a pending order (pending order number exists)    
  !            Delete pending order
  !            Delete Part        
  !        Else!If is a pending order (pending order number exists)
  !            Is it a stock part (stock ref number exists)    
  !                If stock part is a sundry
  !                    Delete Part                
  !                Else!            If stock part is a sundry
  !                    stock quantity += part quantity
  !                    Create Stock History (Re-credit)
  !                    Delete Part            
  !                End!If stock part is a sundry
  !            Else!Is it a stock part (stock ref number exists)
  !                Delete Part                
  !            End!Is it a stock part (stock ref number exists)
  !        End!If is a pending order (pending order number exists)
  !    End!If part has been ordered (part order number exists)
  !End!If this an external part
      continue# = 1
  
      ! Start Change 2224  BE(01/04/03)
      ! Value of Number argument has changed to 8, 9, 10
      !If number = 7 or number = 8 or number = 9
      If number = 8 or number = 9 or number = 10
      ! End Change 2224  BE(01/04/03)
  
          If job:model_number = ''
              Case MessageEx('You must select a Model Number first.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              continue# = 0
          Else !If job:model_number = ''
              If job:charge_type = '' And number = 1
                  Case MessageEx('You must select a Charge Type first.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  continue# = 0
              End
              If job:warranty_charge_type = '' And number = 3 And continue# = 1
                  Case MessageEx('You must select a Warranty Charge Type first.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  continue# = 0
              End
  
          End !If job:model_number = ''
      End!If number = 5 or number = 6 or number = 7
  
      If job:date_completed <> ''
          Check_Access('AMEND COMPLETED JOBS',x")
          If x" = False
              Case MessageEx('Cannot Insert/Delete parts.<13,10><13,10>This job has been completed.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              continue# = 0
          End
      End
      If job:invoice_Number <> ''
          Check_Access('AMEND INVOICED JOBS',x")
          If x" = False
              If request = InsertRecord Or request = DeleteRecord
                  continue# = 0
                  If number = 7
                      Case MessageEx('Cannot Insert/Delete parts.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!If number = 1
                  If number = 9
                      Case MessageEx('Cannot Insert/Delete parts.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!If number = 1
  
              End!If request = InsertRecord Or request = DeleteRecord
  
          End
      End
  
      If continue# = 1
  
  !        Case number
   !           Of 7    !Insert Chargeable Part
                  If Field() = ?Insert
                      glo:select11 = job:manufacturer
                      glo:select12 = job:model_number
                  End !If Field() = ?Insert
                  If Field() = ?Delete
                      If DeleteChargeablePart()
                          !Begin deletion of pick detail entry TH
                          if def:PickNoteNormal or def:PickNoteMainStore then
                              tempquant# = par:Quantity
                              par:Quantity = 0 !reset since it is about to be deleted - can utilise
                              ChangePickingPart('chargeable')
                              par:Quantity = tempquant#
                              DeletePickingParts('chargeable',0)
                          end
                          ! Start Change 2116 BE(26/06/03)
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = par:Part_Ref_Number
                          IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
                              access:JOBSE.clearkey(jobe:RefNumberKey)
                              jobe:RefNumber  = job:Ref_Number
                              IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                                  IF (access:JOBSE.primerecord() = Level:Benign) THEN
                                      jobe:RefNumber  = job:Ref_Number
                                      IF (access:JOBSE.tryinsert()) THEN
                                          access:JOBSE.cancelautoinc()
                                      END
                                  END
                              END
                              IF ((sto:RF_BOARD) AND (jobe:Pre_RF_Board_IMEI <> '')) THEN
                                  GET(audit,0)
                                  IF (access:audit.primerecord() = level:benign) THEN
                                      aud:notes         = 'RF Board deleted from job.<13,10>New IMEI ' & Clip(jobe:Pre_RF_Board_IMEI) & |
                                                          '<13,10>Old IMEI ' & Clip(job:esn)
                                      aud:ref_number    = job:ref_number
                                      aud:date          = Today()
                                      aud:time          = Clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      aud:user = use:user_code
                                      aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(jobe:Pre_RF_Board_IMEI)
                                      if access:audit.insert()
                                          access:audit.cancelautoinc()
                                      end
                                  END
                                  job:ESN = jobe:Pre_RF_Board_IMEI
                                  jobe:Pre_RF_Board_IMEI = ''
                                  access:jobs.update()
                                  access:jobse.update()
                              END
                          END
                          ! End Change 2116 BE(26/06/03)
                          Delete(Parts)
                      End !If DeleteChargeablePart()
  !                    Include('delpartc.inc')
  !                    If do_delete# = 1
  !                        Delete(Parts)
  !                    End !If do_delete# = 1
                  End!If Field() = ?Delete
  
  !            Of 8    !Insert Estiamte Part
                  If Field() = ?Insert:2
                      glo:select11 = job:manufacturer
                      glo:select12 = job:model_number
                  End !If Field() = ?Insert:3
                  If Field() = ?Delete:2
                      ThisWindow.Update
                      !Stock Order
                      Case MessageEx('Are you sure you want to delete this part?','ServiceBase 2000',|
                                     'Styles\Trash.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              do_delete# = 1
                          Of 2 ! &No Button
                              do_delete# = 0
                      End !Case
  
                      If do_delete# = 1
                          Delete(EstParts)
                      End !If do_delete# = 1
                  End!If Field() = ?Delete
  
  !            Of 9    !Insert Warranty Part
                  If Field() = ?Insert:3
                      glo:select11 = job:manufacturer
                      glo:select12 = job:model_number
                  End !If Field() = ?Insert:3
                  If Field() = ?Delete:3
                      If DeleteWarrantyPart()
                          !Begin deletion of pick detail entry TH
                          if def:PickNoteNormal or def:PickNoteMainStore then
                              tempquant# = wpr:Quantity
                              wpr:Quantity = 0 !reset since it is about to be deleted - can utilise
                              ChangePickingPart('warranty')
                              wpr:Quantity = tempquant#
                              DeletePickingParts('warranty',0)
                          end
                          ! Start Change 2116 BE(26/06/03)
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = wpr:Part_Ref_Number
                          IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
                              access:JOBSE.clearkey(jobe:RefNumberKey)
                              jobe:RefNumber  = job:Ref_Number
                              IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                                  IF (access:JOBSE.primerecord() = Level:Benign) THEN
                                      jobe:RefNumber  = job:Ref_Number
                                      IF (access:JOBSE.tryinsert()) THEN
                                          access:JOBSE.cancelautoinc()
                                      END
                                  END
                              END
                              IF ((sto:RF_BOARD) AND (jobe:Pre_RF_Board_IMEI <> '')) THEN
                                  GET(audit,0)
                                  IF (access:audit.primerecord() = level:benign) THEN
                                      aud:notes         = 'RF Board deleted from job.<13,10>New IMEI ' & Clip(jobe:Pre_RF_Board_IMEI) & |
                                                          '<13,10>Old IMEI ' & Clip(job:esn)
                                      aud:ref_number    = job:ref_number
                                      aud:date          = Today()
                                      aud:time          = Clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      aud:user = use:user_code
                                      aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(jobe:Pre_RF_Board_IMEI)
                                      if access:audit.insert()
                                          access:audit.cancelautoinc()
                                      end
                                  END
                                  job:ESN = jobe:Pre_RF_Board_IMEI
                                  jobe:Pre_RF_Board_IMEI = ''
                                  access:jobs.update()
                                  access:jobse.update()
                              END
                          END
                          ! End Change 2116 BE(26/06/03)
                          Delete(WarParts)
                      End !If DeleteWarrantyPart()
                  End !If Field() = Delete:3
  
  
  !        End !Case number
          If request <> 3   ! DeleteRecord
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSubAccounts
      PickTransitTypes
      PickAvailableLocations
      Browse_Model_Numbers
      PickNetworks
      PickChargeableChargeTypes
      PickWarrantyChargeTypes
      Update_Parts
      Update_Estimate_Part
      Update_Warranty_Part
    END
    ReturnValue = GlobalResponse
  END
      End !If request <> 3
  
      Case number
          ! Start Change 2224  BE(01/04/03)
          ! Value of Number argument has changed to 8
          !Of 7 !Chargeable
          Of 8 !Chargeable
          ! End Change 2224  BE(01/04/03)

              if def:PickNoteNormal or def:PickNoteMainStore then
                  if request = insertrecord and returnvalue = requestcompleted then
                      CreatePickingNote (sto:Location)
                      InsertPickingPart ('chargeable')
                  end
                  if request = changerecord and returnvalue = requestcompleted then
                      ChangePickingPart ('chargeable')
                  end
              end

              If glo:select1 = 'NEW PENDING'
                  access:stock.clearkey(sto:ref_number_key)
                  sto:ref_number = par:part_ref_number
                  If access:stock.fetch(sto:ref_number_key)
                      Case MessageEx('Error! Cannot retrieve the details of this Stock Part','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If access:stock.fetch(sto:ref_number_key)
                      If Access:STOHIST.Primerecord() = Level:Benign
                          shi:Ref_Number              = sto:Ref_Number
                          shi:Transaction_Type        = 'DEC'
                          shi:Despatch_Note_Number    = par:Despatch_Note_Number
                          shi:Quantity                = sto:Quantity_stock
                          shi:Date                    = Today()
                          shi:Purchase_Cost           = par:Purchase_Cost
                          shi:Sale_Cost               = par:Sale_Cost
                          shi:Job_Number              = job:Ref_Number
                          Access:USERS.Clearkey(use:Password_Key)
                          use:Password                = glo:Password
                          Access:USERS.Tryfetch(use:Password_Key)
                          shi:User                    = use:User_Code
                          shi:Notes                   = 'STOCK DECREMENTED'
                          shi:Information             = ''
                          If Access:STOHIST.Tryinsert()
                              Access:STOHIST.Cancelautoinc()
                          End!If Access:STOHIST.Tryinsert()
                      End!If Access:STOHIST.Primerecord() = Level:Benign2
                      sto:quantity_stock     = 0
                      access:stock.update()
  
                      access:parts_alias.clearkey(par_ali:refpartrefnokey)
                      par_ali:ref_number      = job:ref_number
                      par_ali:part_ref_number = glo:select2
                      If access:parts_alias.fetch(par_ali:RefPartRefNoKey) = Level:Benign
                          par:ref_number           = par_ali:ref_number
                          par:adjustment           = par_ali:adjustment
                          par:part_ref_number      = par_ali:part_ref_number
  
                          par:part_number     = par_ali:part_number
                          par:description     = par_ali:description
                          par:supplier        = par_ali:supplier
                          par:purchase_cost   = par_ali:purchase_cost
                          par:sale_cost       = par_ali:sale_cost
                          par:retail_cost     = par_ali:retail_cost
                          par:quantity             = glo:select3
                          par:warranty_part        = 'NO'
                          par:exclude_from_order   = 'NO'
                          par:despatch_note_number = ''
                          par:date_ordered         = ''
                          par:date_received        = ''
                          par:pending_ref_number   = glo:select4
                          par:order_number         = ''
                          par:fault_code1    = par_ali:fault_code1
                          par:fault_code2    = par_ali:fault_code2
                          par:fault_code3    = par_ali:fault_code3
                          par:fault_code4    = par_ali:fault_code4
                          par:fault_code5    = par_ali:fault_code5
                          par:fault_code6    = par_ali:fault_code6
                          par:fault_code7    = par_ali:fault_code7
                          par:fault_code8    = par_ali:fault_code8
                          par:fault_code9    = par_ali:fault_code9
                          par:fault_code10   = par_ali:fault_code10
                          par:fault_code11   = par_ali:fault_code11
                          par:fault_code12   = par_ali:fault_code12
                          par:Requested      = True
                          access:parts.insert()
                      end !If access:parts_alias.clearkey(par_ali:RefPartRefNoKey) = Level:Benign
                  end !if access:stock.fetch(sto:ref_number_key = Level:Benign
                  if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('chargeable').
              End!If glo:select1 = 'NEW PENDING'
  
              !IF request = InsertRecord AND globalresponse = RequestCompleted THEN
              !   message('At this code')
              !END !IF
!              if request = insertrecord and returnvalue = requestcompleted then
!                  CreatePickingNote (sto:Location)
!                  InsertPickingPart ('chargeable')
!              end
!              if request = changerecord and returnvalue = requestcompleted then
!                  ChangePickingPart ('chargeable')
!              end
              DO Check_Parts !Check parts to see if status change required?
              ! Start Change BE025 BE(26/01/04)
              DO Pricing
              ! End Change BE025 BE(26/01/04)
          ! Start Change xxx BE(7/6/04)
          OF 9 ! Estimate
              DO Pricing
          ! Start Change xxx BE(7/6/04)
          ! Start Change 2224  BE(01/04/03)
          ! Value of Number argument has changed to 10
          !Of 9 !Warranty
          Of 10 !Warranty
          ! End Change 2224  BE(01/04/03)

              if def:PickNoteNormal or def:PickNoteMainStore then
                  if request = insertrecord and returnvalue = requestcompleted then
                      CreatePickingNote (sto:Location)
                      InsertPickingPart ('warranty')
                  end
                  if request = changerecord and returnvalue = requestcompleted then
                      ChangePickingPart ('warranty')
                  end
              end

              If glo:select1 = 'NEW PENDING'
                  access:stock.clearkey(sto:ref_number_key)
                  sto:ref_number = wpr:part_ref_number
                  If access:stock.fetch(sto:ref_number_key)
                      Case MessageEx('Error! Cannot retrieve the details of this Stock Part','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If access:stock.fetch(sto:ref_number_key)
                      If Access:STOHIST.Primerecord() = Level:Benign
                          shi:Ref_Number              = sto:Ref_Number
                          shi:Transaction_Type        = 'DEC'
                          shi:Despatch_Note_Number    = wpr:Despatch_Note_Number
                          shi:Quantity                = sto:Quantity_Stock
                          shi:Date                    = Today()
                          shi:Purchase_Cost           = wpr:Purchase_Cost
                          shi:Sale_Cost               = wpr:Sale_cost
                          shi:Job_Number              = wpr:Ref_Number
                          Access:USERS.Clearkey(use:Password_Key)
                          use:Password                = glo:Password
                          Access:USERS.Tryfetch(use:Password_Key)
                          shi:User                    = use:User_Code
                          shi:Notes                   = 'STOCK DECREMENTED'
                          shi:Information             = ''
                          If Access:STOHIST.Tryinsert()
                              Access:STOHIST.Cancelautoinc()
                          End!If Access:STOHIST.Tryinsert()
                      End!If Access:STOHIST.Primerecord() = Level:Benign
  
                      sto:quantity_stock     = 0
                      access:stock.update()
  
                      access:warparts_alias.clearkey(war_ali:RefPartRefNoKey)
                      war_ali:ref_number      = job:ref_number
                      war_ali:part_ref_number = glo:select2
                      If access:warparts_alias.fetch(war_ali:RefPartRefNoKey) = Level:Benign
                          wpr:ref_number           = war_ali:ref_number
                          wpr:adjustment           = war_ali:adjustment
                          wpr:part_ref_number      = war_ali:part_ref_number
                          wpr:part_number     = war_ali:part_number
                          wpr:description     = war_ali:description
                          wpr:supplier        = war_ali:supplier
                          wpr:purchase_cost   = war_ali:purchase_cost
                          wpr:sale_cost       = war_ali:sale_cost
                          wpr:retail_cost     = war_ali:retail_cost
                          wpr:quantity             = glo:select3
                          wpr:warranty_part        = 'YES'
                          wpr:exclude_from_order   = 'NO'
                          wpr:despatch_note_number = ''
                          wpr:date_ordered         = ''
                          wpr:date_received        = ''
                          wpr:pending_ref_number   = glo:select4
                          wpr:order_number         = ''
                          wpr:fault_code1    = war_ali:fault_code1
                          wpr:fault_code2    = war_ali:fault_code2
                          wpr:fault_code3    = war_ali:fault_code3
                          wpr:fault_code4    = war_ali:fault_code4
                          wpr:fault_code5    = war_ali:fault_code5
                          wpr:fault_code6    = war_ali:fault_code6
                          wpr:fault_code7    = war_ali:fault_code7
                          wpr:fault_code8    = war_ali:fault_code8
                          wpr:fault_code9    = war_ali:fault_code9
                          wpr:fault_code10   = war_ali:fault_code10
                          wpr:fault_code11   = war_ali:fault_code11
                          wpr:fault_code12   = war_ali:fault_code12
                          wpr:Requested      = True
                          access:warparts.insert()
                      end !If access:parts_alias.clearkey(par_ali:RefPartRefNoKey) = Level:Benign
                  end !if access:stock.fetch(sto:ref_number_key = Level:Benign
                  if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart ('warranty').
              End!If glo:select1 = 'NEW PENDING'
!              if request = insertrecord and returnvalue = requestcompleted then
!                  CreatePickingNote (sto:Location)
!                  InsertPickingPart ('warranty')
!              end
!              if request = changerecord and returnvalue = requestcompleted then
!                  ChangePickingPart ('warranty')
!              end
              DO Check_Parts !Check parts to see if status change required?
              ! Start Change BE025 BE(26/01/04)
              DO Pricing
              ! End Change BE025 BE(26/01/04)
  
      End !Case number
      ! Start Change BE025 BE(26/01/04)
      !DO Pricing
      ! End Change BE025 BE(26/01/04)
      Else!!If continue# = 1
          Case number
              ! Start Change 2224  BE(01/04/03)
              ! Value of Number argument has changed to 8
              !Of 7
              Of 8
              ! Start Change 2224  BE(01/04/03)
  
                  access:parts.cancelautoinc()
                  Do check_parts
                  Do Pricing
  
              ! Start Change 2224  BE(01/04/03)
              ! Value of Number argument has changed to 9
              !Of 8
              Of 9
              ! Start Change 2224  BE(01/04/03)
                  access:estparts.cancelautoinc()
                  Do check_parts
                  Do Pricing
  
              ! Start Change 2224  BE(01/04/03)
              ! Value of Number argument has changed to 10
              !Of 9
              Of 10
              ! Start Change 2224  BE(01/04/03)
                  access:warparts.cancelautoinc()
                  Do check_parts
                  Do Pricing
  
          End!Case number
  
      End !If continue# = 1
      Do CountParts
      Clear(glo:G_Select1)
      Clear(glo:G_Select1)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Fault_Description_Text_Button
      access:jobnotes.clearkey(JBN:RefNumberKey)
      jbn:refnumber   = job:ref_number
      If access:jobnotes.tryfetch(JBN:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:refnumber   = job:ref_number
              If access:jobnotes.tryinsert()
                  access:jobnotes.cancelautoinc()
              End!If access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End! If access:jobnotes.tryfetch(JBN:RefNumberKey) = Level:Benign
      
      glo:select1 = job:manufacturer
    OF ?job:Chargeable_Job
      Include('genjobs.inc','ChargeableJobAccepted')
    OF ?job:Warranty_Job
      Include('genjobs.inc','WarrantyJobAccepted')
    OF ?job:Estimate_Ready
      If ~0{prop:acceptall}
      If job:estimate_ready <> estimate_ready_temp
          If job:estimate_ready = 'NO'
              check_access('RELEASE - ESTIMATE READY',x")
              If x" = False
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:estimate_ready = estimate_temp
              Else
                  estimate_ready_temp = job:estimate_ready
              End !If x" = False
          Else!If job:estimate_ready = 'YES'
              error# = 0
              If total_estimate_temp < job:estimate_if_over
                  Case MessageEx('The total value of this Estimate is less than the specified ''Estimate If Over'' value.<13,10><13,10>Are you sure you want to mark this Estimate as ''Ready''?','ServiceBase 2000',|
                                 'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          error# = 0
                      Of 2 ! &No Button
                          error# = 1
                  End!Case MessageEx
              End!If total_estimate_temp < job:estimate_if_over
              If error# = 0
                  estimate_ready_temp   = job:estimate_ready
                GetStatus(510,thiswindow.request,'JOB') !estimate ready
      
                  brw21.resetsort(1)
                  Do time_remaining
              Else!If error# = 0
                  job:estimate_ready = estimate_ready_temp
              End!If error# = 0
      
          End !If job:estimate_ready = 'YES'
      End !If job:estimate_ready <> estimate_temp
      
      End !If ~0{prop:acceptall}
    OF ?job:In_Repair
      If ~0{prop:acceptall}
      
      If job:in_repair <> in_repair_temp
          If job:in_repair = 'NO'
              check_access('RELEASE - IN REPAIR',x")
              If x" = False
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:in_repair   = in_repair_temp
              Else !If x" = False
                  in_repair_temp  = job:in_repair
              End !If x" = False
          Else !If job:in_repair = 'NO'
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If job:invoice_Number <> ''
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
      
              If error# = 0
                  If job:workshop <> 'YES'
                      Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job is not in the Workshop.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End !If job:workshop <> 'YES'
              End !If error# = 0
              If error# = 0
                  job:date_in_repair = Today()
                  job:time_in_repair = Clock()
                    GetStatus(315,thiswindow.request,'JOB') !in repair
      
                  Brw21.resetsort(1)
                  Do time_remaining
              End !If error# = 0
              If error# = 1
                  job:date_in_repair = ''
                  job:time_in_repair = ''
                  job:in_repair = in_repair_temp
              Else
                  in_repair_temp  = job:in_repair
              End
              Display()
          End !If job:in_repair = 'NO'
      End !If job:in_repair <> in_repair_temp
      
      
      End !If ~0{prop:acceptall}
    OF ?job:On_Test
      If ~0{prop:acceptall}
      If job:on_test <> on_test_temp
          If job:on_test = 'NO'
              check_access('RELEASE - ON TEST',x")
              If x" = False
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:on_test = on_test_temp
              Else !If x" = False
                  on_test_temp    = job:on_test
              End !If x" = False
          Else !If job:in_repair = 'NO'
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If job:invoice_Number <> ''
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If error# = 0
                  If job:workshop <> 'YES'
                      Case MessageEx('Cannot mark as In Repair.<13,10><13,10>This job is not in the Workshop.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End !If job:workshop <> 'YES'
              End !If error# = 0
              If error# = 0
                  If job:in_repair <> 'YES'
                      Case MessageEx('Cannot mark as On Test.<13,10><13,10>This job has not been marked as In Repair.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End !If job:in_repair <> 'YES'
              End!If error# = 0
              If error# = 0
                  job:date_on_test = Today()
                  job:time_on_test = Clock()
                    GetStatus(320,thiswindow.request,'JOB') !on test
      
                  Brw21.resetsort(1)
                  Do time_remaining
              End !If error# = 0
      
              If error# = 1
                  job:date_on_test = ''
                  job:time_on_test = ''
                  job:on_test = on_test_temp
              Else
                  on_test_temp    = job:on_test
              End !If error# = 1
      
          End !If job:in_repair = 'NO'
      End !If job:on_test <> on_test_temp
      
      
      
      
      End !If ~0{prop:acceptall}
    OF ?job:QA_Passed
      If ~0{prop:acceptall}
      
      set(defaults)
      access:defaults.next()
      If job:qa_passed <> qa_passed_temp
          If job:qa_passed = 'NO'
              check_access('RELEASE - QA PASSED',x")
              If x" = False
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:qa_passed   = qa_passed_temp
              Else !If x" = False
                  qa_passed_temp  = job:qa_passed
              End !If x" = False
          Else !If job:in_repair = 'NO'
              If def:qa_required = 'YES'
                  Do Check_For_Despatch
              End!If def:qa_required = 'YES'
              pass# = 0
              Include('accjchk.inc')
              If pass# = 1
                  qa_passed_temp  = job:qa_passed
                  job:date_qa_passed = Today()
                  job:time_qa_passed = Clock()
                    GetStatus(610,thiswindow.request,'JOB') !manual qa passed
      
              Else!
                  job:qa_Passed  = qa_passed_temp
              End!If pass# = 1
              Brw21.resetsort(1)
             Do time_remaining
      
          End !If job:in_repair = 'NO'
      End !If job:in_repair <> in_repair_temp
      
      End !If ~0{prop:acceptall}
    OF ?Insert:2
      If job:model_number = ''
          Case MessageEx('You must enter a Model Number first.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Cycle
      End
    OF ?Show_List_Button
      GLO:Select1 = job:Loan_Unit_Number
    OF ?Exchange_Accessory_Button
      glo:select12 = job:model_Number
      Do Count_accessories
      accessory_count_temp = exchange_accessory_count_temp
    OF ?Contact_History
      glo:select12 = job:ref_number
    OF ?Audit_Trail_Button
      glo:select12 = job:ref_number
    OF ?job:Ignore_Estimate_Charges
      If ~0{prop:acceptall}
      If job:Ignore_Estimate_Charges <> Ignore_Estimate_Charges_Temp
          error# = 0
          Check_access('IGNORE DEFAULT CHARGE',x")
          If x" = False
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!If x" = False
      
      
          If job:date_completed <> '' and error# = 0
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End!If job:invoice_number <> ''
      
          If job:invoice_number <> '' and error# = 0
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End!If job:invoice_number <> ''
      
          If error# = 0
              ignore_estimate_charges_temp = job:ignore_estimate_charges
              Do pricing
          Else!If error# = 0
              job:ignore_estimate_charges = ignore_estimate_charges_temp
          End!If error# = 0
      
      End!If job:Ignore_Chargeable_Charges <> Ignore_Chargeable_Charges_Temp
      End!If ~0{prop:acceptall}
    OF ?job:Ignore_Chargeable_Charges
      If ~0{prop:acceptall}
      If job:Ignore_Chargeable_Charges <> Ignore_Chargeable_Charges_Temp
          error# = 0
      
          Check_access('IGNORE DEFAULT CHARGE',x")
          If x" = False
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!If x" = False
      
          If job:date_completed <> '' and error# = 0
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End!If job:invoice_number <> ''
      
          If job:invoice_number <> '' and error# = 0
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End!If job:invoice_number <> ''
      
      
          If error# = 0
              ignore_chargeable_charges_temp = job:ignore_chargeable_charges
              Do pricing
          Else!If error# = 0
              job:ignore_chargeable_charges = ignore_chargeable_charges_temp
          End!If error# = 0
      
      End!If job:Ignore_Chargeable_Charges <> Ignore_Chargeable_Charges_Temp
      
      End!If ~0{prop:acceptall}
    OF ?job:Ignore_Warranty_Charges
      If ~0{prop:acceptall}
      If job:Ignore_Warranty_Charges <> Ignore_Warranty_Charges_Temp
          error# = 0
      
          Check_access('IGNORE DEFAULT CHARGE',x")
          If x" = False
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!If x" = False
      
          If job:date_completed <> '' And error# = 0
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End!If job:invoice_number <> ''
      
          If job:invoice_number_warranty <> '' and error# = 0
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End!If job:invoice_number <> ''
      
          If error# = 0
              ignore_Warranty_charges_temp = job:ignore_Warranty_charges
              Do pricing
          Else!If error# = 0
              job:ignore_Warranty_charges = ignore_Warranty_charges_temp
          End!If error# = 0
      
      End!If job:Ignore_Warranty_Charges <> Ignore_Warranty_Charges_Temp
      
      End!If ~0{prop:acceptall}
    OF ?Cancel
      !Save Fields In Case Of Cancel
      saved_ref_number_temp   = job:Ref_number
      new_exchange_unit_number_temp   = job:exchange_unit_number
      new_loan_unit_number_temp       = job:loan_unit_number
      
      If thiswindow.request = Insertrecord
          Clear(parts_queue_temp)
          Free(parts_queue_temp)
          save_par_id = access:parts.savefile()
          access:parts.clearkey(par:part_number_key)
          par:ref_number  = job:ref_number
          set(par:part_number_key,par:part_number_key)
          loop
              if access:parts.next()
                 break
              end !if
              if par:ref_number  <> job:ref_number      |
                  then break.  ! end if
              partmp:Part_Ref_Number  = par:part_ref_number
              partmp:Quantity = par:quantity
              partmp:Exclude_From_Order   = par:exclude_from_order
              partmp:Pending_Ref_Number   = par:pending_ref_number
              add(parts_queue_temp)
          end !loop
          access:parts.restorefile(save_par_id)
          setcursor()
      End!If thiswindow.request = Insertrecord
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job:Auto_Search
      
       temp_string = Clip(left(job:Auto_Search))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:Auto_Search = temp_string
       Display(?job:Auto_Search)
      If ~0{prop:acceptall}
          If ThisWindow.Request = Insertrecord
              If job:auto_search <> ''
                  Do History_search
                  history_run_temp = 1
              End!If job:auto_search <> ''
          End
      
          Post(Event:Accepted,?job:Model_Number)
      End
    OF ?History_Button
      ThisWindow.Update
      If ThisWindow.Request = Insertrecord
          If history_run_temp <> 1
              Do history_search
          Else
              history_run_temp = 0
          End
      Else
          Do history_search
      End
    OF ?job:Account_Number
      If ~0{prop:acceptall}
      do_lookup# = 1
      If job:account_number <> account_number_temp
          error# = 0
          If job:date_completed <> ''
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End
          End
          If error# = 1
              job:account_number = account_number_temp
              do_lookup# = 0
          Else!If job:date_Completed <> ''
              do_lookup# = 1
          End!If job:date_Completed <> ''
      
      End!If job:account_number <> account_number_temp
      Display()
      If do_lookup# = 1
      IF job:Account_Number OR ?job:Account_Number{Prop:Req}
        sub:Account_Number = job:Account_Number
        !Save Lookup Field Incase Of error
        look:job:Account_Number        = job:Account_Number
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Account_Number = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            job:Account_Number = look:job:Account_Number
            SELECT(?job:Account_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
          stop# = 0
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number = job:account_number
          if access:subtracc.fetch(sub:account_number_key) = level:benign
              access:tradeacc.clearkey(tra:account_number_key) 
              tra:account_number = sub:main_account_number
              if access:tradeacc.fetch(tra:account_number_key) = level:benign
                  if tra:use_sub_accounts = 'YES' and tra:Invoice_Sub_Accounts = 'YES'
                      If sub:stop_account = 'YES'
                          stop# = 1
                      End!If sub:stop_account = 'YES'
                  else!if tra:use_sub_accounts = 'YES'
                      If tra:stop_account = 'YES'
                          stop# = 1
                      End!If tra:stop_account = 'YES'
                  end!if tra:use_sub_accounts = 'YES'
              end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
          end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      
          If stop# = 0
              If tra:ForceOrderNumber
                  ?job:Order_Number{prop:req} = 1
              Else !If tra:ForceOrderNumber
                  ?job:Order_Number{prop:req} = 0
              End !If tra:ForceOrderNumber
              error# = 0
              access:trantype.clearkey(trt:transit_type_key)
              trt:transit_type = job:transit_type
              if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                  If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                      Case MessageEx('Loan Units are not authorised for this Trade Account. <13,10><13,10>Amend the Trade Account details or select another Initial Transit Type.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End !If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                  If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                      Case MessageEx('Exchange Units are not authorised for this Trade Account. <13,10><13,10>Amend the Trade Account details or select another Initial Transit Type.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End !If trt:exchange_unit = 'YES' And tra:allow_loan <> 'YES'
              end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
      
              !Get New Costs
              error# = 0
              If CheckPricing('C') = 1 Or CheckPricing('W') = 1
                  error# = 1
              End!If CheckPricing('C') = 1 Or CheckPricing('W') = 1
      
              !Check to see if the IMEI matches the account number, if there is one.
              If Error# = 0 And job:ESN <> ''
                  If ExchangeAccount(job:Account_Number)
                      Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                      xch:ESN = job:ESN
                      If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                          !Found
                          !Write the relevant fields to say this unit is in repair and take completed.
                          !Otherwise the use could click cancel and screw everything up.
                      Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          Case MessageEx('The selected Trade Account is setup to repair Exchange Units, but the entered I.M.E.I. Number cannot be found in Exchange Stock.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          Error# = 1
                      End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  Else
                      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
                      xch:ESN = job:ESN
                      If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                          !Found
                          If xch:Available <> 'DES'
                              Case MessageEx('The entered I.M.E.I. matches that of an entry in the Exchange Database. This unit is not marked as "Despatched" so may still exist in your stock.'&|
                                '<13,10>'&|
                                '<13,10>If you continue, this job will be treated as a normal customer repair and not a "In House Exchange Repair". These "In House" repairs should be booked in using an Account that is marked as "Exchange Account".'&|
                                '<13,10>'&|
                                '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      Error# = 1
                              End!Case MessageEx
                          End!Case MessageEx
                      Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                      
                  End !ExchangeAccount(job:Account_Number)
              End !Error# = 0
      
      
              If error# = 0
                  If tra:use_contact_name = 'YES'
                      Unhide(?NameGroup)
                  Else!If tra:use_contact_name = 'YES'
                      Hide(?NameGroup)
                  End!If tra:use_contact_name = 'YES'
                  overwrite# = 1
                  If account_number_temp <> '' and job:account_number <> account_number_temp
                      Case MessageEx('You have selected a NEW Trade Account.<13,10><13,10>This account may assign different Prices, Address''s, Turnaround Times and Couriers to this job. <13,10><13,10>Are you sure you want to overwrite this information if necessary?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              overwrite# = 1
                          Of 2 ! &No Button
                              overwrite# = 0
                      End!Case MessageEx
                  End!If trade_account_temp <> ''
                  If overwrite# = 1
                      If tra:force_estimate = 'YES'
                          job:estimate_if_over    = tra:estimate_if_over
                          job:estimate        = 'YES'
                          Post(event:accepted,?job:estimate)
                      End!If tra:force_estimate = 'YES'
                      If tra:turnaround_time <> ''
                          job:turnaround_time = tra:turnaround_time
                          Post(event:accepted,?job:turnaround_time)
                      End!If tra:turnaround_time <> ''
                      If tra:invoice_sub_accounts <> 'YES'
                          If tra:use_customer_address <> 'YES'
                              job:postcode         = tra:postcode
                              job:company_name     = tra:company_name
                              job:address_line1    = tra:address_line1
                              job:address_line2    = tra:address_line2
                              job:address_line3    = tra:address_line3
                              job:telephone_number = tra:telephone_number
                              job:fax_number       = tra:fax_number
                          End!If tra:use_customer_address <> 'YES'
                          If tra:courier_cost <> ''
                              job:courier_cost            = sub:courier_cost
                              job:courier_cost_warranty   = sub:courier_cost
                          End!If tra:courier_cost <> '' and overwrite# = 1
                      Else!!If tra:invoice_sub_accounts <> 'YES'
                          If sub:use_customer_address <> 'YES'
                              job:postcode         = sub:postcode
                              job:company_name     = sub:company_name
                              job:address_line1    = sub:address_Line1
                              job:address_line2    = sub:address_line2
                              job:address_line3    = sub:address_line3
                              job:telephone_number = sub:telephone_number
                              job:fax_number       = sub:fax_number
                          End!If sub:use_customer_address <> 'YES'
                          If sub:courier_cost <> '' 
                              job:courier_cost            = sub:courier_cost
                              job:courier_cost_warranty   = sub:courier_cost
                          End!If sub:courier_cost <> '' And overwrite# = 1
                      End!If tra:invoice_sub_accounts <> 'YES'
                      If tra:use_sub_accounts <> 'YES'
                          If tra:use_delivery_address = 'YES'
                              job:postcode_delivery   = tra:postcode
                              job:company_name_delivery   = tra:company_name
                              job:address_line1_delivery  = tra:address_line1
                              job:address_line2_delivery  = tra:address_line2
                              job:address_line3_delivery  = tra:address_line3
                              job:telephone_delivery      = tra:telephone_number
                          End !If tra:use_delivery_address = 'YES'
                          If tra:use_collection_address = 'YES' and overwrite# = 1
                              job:postcode_collection = tra:postcode
                              job:company_name_collection = tra:company_Name
                              job:address_line1_collection    = tra:address_line1
                              job:address_line2_collection    = tra:address_line2
                              job:address_line3_collection    = tra:address_line3
                              job:telephone_collection    = tra:telephone_number
                          End!If tra:use_collection_address = 'YES'
                          If overwrite# = 1
                              job:courier = tra:courier_outgoing
                              job:incoming_courier = tra:courier_incoming
                              job:exchange_courier = job:courier
                              job:loan_courier = job:courier
                          End!If overwrite# = 1
                      Else!If tra:use_sub_accounts <> 'YES'
                          If sub:use_delivery_address = 'YES' and overwrite# = 1
                              job:postcode_delivery      = sub:postcode
                              job:company_name_delivery  = sub:company_name
                              job:address_line1_delivery = sub:address_line1
                              job:address_line2_delivery = sub:address_line2
                              job:address_line3_delivery = sub:address_line3
                              job:telephone_delivery     = sub:telephone_number
                          End
                          If sub:use_collection_address = 'YES' and overwrite# = 1
                              job:postcode_collection = sub:postcode
                              job:company_name_collection = sub:company_Name
                              job:address_line1_collection    = sub:address_line1
                              job:address_line2_collection    = sub:address_line2
                              job:address_line3_collection    = sub:address_line3
                              job:telephone_collection    = sub:telephone_number
                          End!If tra:use_collection_address = 'YES'
                          if overwrite# = 1
                              job:courier = sub:courier_outgoing
                              job:incoming_courier = sub:courier_incoming
                              job:exchange_courier = job:courier
                              job:loan_courier = job:courier
                          End!if overwrite# = 1
                      End!If tra:use_sub_accounts <> 'YES'
                      If overwrite# = 1
                          access:courier.clearkey(cou:courier_key)
                          cou:courier = job:courier
                          access:courier.tryfetch(cou:courier_key)
                          IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                              job:loaservice  = cou:service
                              job:excservice  = cou:service
                              job:jobservice  = cou:service
                          Else!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                              job:loaservice  = ''
                              job:excservice  = ''
                              job:jobservice  = ''
                          End!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                      End!If overwrite# = 1
                      account_number_temp = job:account_number
                  Else!If overwrite# = 1
                      job:account_number = account_number_temp
                  End!If overwrite# = 1
              Else!If error# = 0
                  job:account_number  = account_number_temp
              End!If error# = 0
          Else!If stop# = 0
              Case MessageEx('The selected Trade Account is on STOP.<13,10><13,10>Please select another.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              job:account_number  = account_number_temp
          End!If stop# = 0
      
      End!If do_lookup# = 1
      Do Estimate
      Display()
      End!End!If ~0{prop:acceptall}
      Do CheckRequiredFields
    OF ?LookupTradeAccount
      ThisWindow.Update
      sub:Account_Number = job:Account_Number
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Account_Number = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?job:Account_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Account_Number)
    OF ?job:Transit_Type
      If ~0{prop:acceptall}
      do_lookup# = 1
      If job:transit_type <> transit_type_temp
          error# = 0
          If job:date_completed <> ''
              Check_Access('AMEND COMPLETED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
                  job:transit_type = transit_type_temp
              End
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:transit_type = transit_Type_temp
                  error# = 1
              End
          End
          If error# = 1
              do_lookup# = 0
          End!If error# = 1
      End!If job:transit_type <> transit_type_temp
      If do_lookup# = 1
      
      IF job:Transit_Type OR ?job:Transit_Type{Prop:Req}
        trt:Transit_Type = job:Transit_Type
        !Save Lookup Field Incase Of error
        look:job:Transit_Type        = job:Transit_Type
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            job:Transit_Type = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            job:Transit_Type = look:job:Transit_Type
            SELECT(?job:Transit_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
          If job:transit_type <> transit_type_temp
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              access:trantype.clearkey(trt:transit_type_key)
              trt:transit_type = job:transit_type
              if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                  error# = 0
                  access:subtracc.clearkey(sub:account_number_key)
                  sub:account_number = job:account_number
                  if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                      access:tradeacc.clearkey(tra:account_number_key)
                      tra:account_number = sub:main_account_number
                      if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                          If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                              error# = 1
                              Case MessageEx('Loan Units are not authorised for this Trade Account.<13,10><13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              job:transit_type    = transit_type_temp
                          End!If trt:loan_unit    = 'YES' And tra:allow_loan <> 'YES'
                          If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                              Case MessageEx('Exchange Units are not authorised for this Trade Account.<13,10><13,10>Select another Initial Transit Type or amend the Trade Account defaults.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              job:transit_Type    = transit_type_temp
                              error# = 1
                          End!If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES'
                      end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                  end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
                  If error# = 0
                      hide# = 0
                      If trt:collection_address <> 'YES'
                          If job:address_line1 <> ''
                              Case MessageEx('The selected Transit Type will remove the Collection Address.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                      hide# = 1
                                  Of 2 ! &No Button
                              End!Case MessageEx
                          Else
                              hide# = 1
                          End
                          If hide# = 1
                              job:postcode_collection         = ''
                              job:company_name_collection     = ''
                              job:address_line1_collection    = ''
                              job:address_line2_collection    = ''
                              job:address_line3_collection    = ''
                              job:telephone_collection = ''
                              Hide(?collection_address_group)
                          End
                      Else
                          Unhide(?collection_address_group)
                      End
                      hide# = 0
                      If trt:delivery_address <> 'YES'
                          If job:address_line1_delivery <> ''
                              Case MessageEx('The selected Transit Type will remove the Delivery Address.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                      hide# = 1
                                  Of 2 ! &No Button
                              End!Case MessageExEnd!Case MessageEx
                          Else
                              hide# = 1
                          End
                          If hide# = 1
                              job:postcode_delivery         = ''
                              job:company_name_delivery     = ''
                              job:address_line1_delivery    = ''
                              job:address_line2_delivery    = ''
                              job:address_line3_delivery    = ''
                              job:telephone_delivery = ''
                              Hide(?delivery_address_group)
                          End
                      Else
                          Unhide(?delivery_address_group)
                      End
                      If trt:InWorkshop = 'YES'
                          job:workshop = 'YES'
                          ! Start Change 3586 BE(20/11/03)
                          tmp:InWorkshopDate  = Today()
                          tmp:InWorkshopTime  = Clock()
                          Unhide(?tmp:InWorkshopDate)
                          Unhide(?tmp:InWorkshopTime)
                          ! End Change 3586 BE(20/11/03)
                      Else!If trt:InWorkshop = 'YES'
                          job:workshop = 'NO'
                          ! Start Change 3586 BE(20/11/03)
                          tmp:InWorkshopDate  = ''
                          tmp:InWorkshopTime  = ''
                          Hide(?tmp:InWorkshopDate)
                          Hide(?tmp:InWorkshopTime)
                          ! End Change 3586 BE(20/11/03)
                      End!If trt:InWorkshop = 'YES'
                      ! Start Change 3586 BE(20/11/03)
                      Display(?tmp:InWorkshopDate)
                      Display(?tmp:InWorkshopTime)
                      ! End Change 3586 BE(20/11/03)
      
                      If trt:InternalLocation <> ''
                          job:Location    = trt:InternalLocation
                      End !If trt:InternalLocation <> ''
      
                      If trt:location <> 'YES'
                          Hide(?location_group)
                          ?job:location{prop:req} = 0
                      Else
                          If ~def:HideLocation
                              If job:workshop = 'YES'
                                  Unhide(?location_group)
                              End
                              If trt:force_location = 'YES' And job:workshop = 'YES'
                                  ?job:location{prop:req} = 1
                              Else!If trt:force_location = 'YES'
                                  ?job:location{prop:req} = 0
                              End!If trt:force_location = 'YES'
                          End !If ~def:HideLocation
                      End
                      GetStatus(Sub(trt:Exchangestatus,1,3),1,'EXC')
                      GetStatus(Sub(trt:loanstatus,1,3),1,'LOA')
                      GetStatus(Sub(trt:initial_status,1,3),1,'JOB')
      
                      If ThisWindow.Request = Insertrecord
                          
      
                          Brw21.resetsort(1)
                          Do time_remaining
      
                          
                          If trt:skip_workshop <> 'YES'
                              Unhide(?job:workshop)
                              Select(?job:workshop)
                          End
                          If trt:workshop_label = 'YES' and job:workshop = 'YES'
                              Unhide(?no_label_button)
                              print_label_temp = 'YES'
                          Else
                              Hide(?no_label_button)
                              print_label_temp = 'NO'
                          End
      
                          IF tra:refurbcharge = 'YES' and TRT:Exchange_Unit = 'YES'
                              job:chargeable_job = 'YES'
                              job:warranty_job    = 'YES'
                              job:charge_type = tra:ChargeType
                              job:Warranty_Charge_Type    = tra:WarChargeType
                              Unhide(?job:charge_type)
                              Post(Event:Accepted,?job:Charge_Type)
                              Unhide(?job:warranty_charge_type)
                              Unhide(?lookupwarrantychargetype)
                              Unhide(?lookupChargeType)
                          End!IF tra:refurbcharge = 'YES'
      
                          ! Start Change 2851 BE(02/07/03)
                          !If trt:job_card = 'YES'
                          IF ((trt:job_card = 'YES') AND (tmp:SuspendPrintJobcard = 0)) THEN
                          ! Start Change 2851 BE(02/07/03)
                              tmp:printjobcard    = 1
                          Else!If trt:job_card = 'YES'
                              tmp:printjobcard    = 0
                          End!If trt:job_card = 'YES'
                          If trt:jobReceipt = 'YES'
                              tmp:printjobreceipt = 1
                          Else!If trt:job_Receipt = 'YES'
                              tmp:printjobreceipt = 0
                          End!If trt:job_Receipt = 'YES'
      
                          turnerror# = 0
                          If job:turnaround_time <> '' and job:turnaround_time <> trt:initial_priority
                              Case MessageEx('The selected Transit Type requires a Turnaround Time of '&Clip(trt:initial_priority)&'.<13,10><13,10>A Turnaround Time has already been assigned to this job. Do you wish to change this?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      turnerror# = 1
                              End!Case MessageEx
                          End!If job:turnaround_time <> ''
                          If turnerror# = 0
                              job:turnaround_time = trt:initial_priority
                              Display(?job:Turnaround_Time)
                              Post(Event:Accepted,?job:Turnaround_Time)
      
                          End!If error# = 0
                      End
                  End!If error# = 0
              end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
              If error# = 1
                  job:transit_type = transit_type_temp
              Else
                  transit_type_temp   = job:transit_type
              End!If error# = 1
          End!If job:transit_type <> transit_type_temp
      
          End!If do_lookup# = 1
      End !If ~0{prop:acceptall}
      Do CheckRequiredFields
    OF ?LookupTransitType
      ThisWindow.Update
      trt:Transit_Type = job:Transit_Type
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          job:Transit_Type = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?job:Transit_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Transit_Type)
    OF ?no_Label_button
      ThisWindow.Update
      Case MessageEx('Are you sure you dont''t want to automatically print a job label when you have completed booking this job?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              print_label_temp = 'NO'
          Of 2 ! &No Button
      End!Case MessageEx
    OF ?job:Workshop
      IF ?job:Workshop{Prop:Checked} = True
        ENABLE(?Lookup_Engineer)
      END
      IF ?job:Workshop{Prop:Checked} = False
        DISABLE(?Lookup_Engineer)
      END
      ThisWindow.Reset
      If ~quickwindow{prop:acceptall}
      If job:workshop <> workshop_temp
          error# = 0
          If job:workshop = 'YES'
              If job:third_party_site <> ''
                  If SecurityCheck('RETURN UNIT FROM 3RD PARTY')
                      Case MessageEx('This unit as at a 3rd Party Site. You can only return it via 3rd Party Returns in Rapid 3rd Party Despatch.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  Else!If SecurityCheck('RETURN UNIT FROM 3RD PARTY')
                      Case MessageEx('This unit as at a 3rd Party Site. It should be returned it via 3rd Party Returns in Rapid 3rd Party Despatch.<13,10><13,10>You should only mark this unit as ''in the workshop'' from here in the event of an error.<13,10>Are you sure?','ServiceBase 2000',|
                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
                          Of 1 ! &Yes Button
                          Of 2 ! &No Button
                              error# = 1
                      End!Case MessageEx
                  End!If SecurityCheck('RETURN UNIT FROM 3RD PARTY')
      
              End!If job:third_party_site <> ''
      
              IF error# = 1
                  job:workshop = workshop_temp
              Else!IF error# = 1
                  workshop_temp = job:workshop
      
                  ! Start Change 2471 BE(14/04/03)
                  !tmp:InWorkshopDate  = Today()
                  !tmp:InWorkshopTime  = Clock()
                  lockdate# = GETINI('THIRDPARTY','LockInWorkshopDate',,CLIP(PATH())&'\SB2KDEF.INI')
                  IF ((lockdate# <> 1) OR ((lockdate# = 1) AND (tmp:InWorkshopDate = ''))) THEN
                      tmp:InWorkshopDate  = Today()
                      tmp:InWorkshopTime  = Clock()
                  END
                  ! End Change 2471 BE(14/04/03)
      
                  If Access:AUDIT.PrimeRecord() = level:benign
                      If job:third_party_site <> ''
                          aud:notes         = 'FROM THIRD PARTY SITE: ' & Clip(job:Third_Party_Site)
                      End!If job:third_party_site <> ''
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      aud:type          = 'JOB'
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      aud:action        = 'UNIT BROUGHT INTO WORKSHOP'
                      access:audit.insert()
                  End!�if access:audit.primerecord() = level:benign
      
      !If there is an incoming consignment note number, mark that the unit has come in.
                  If job:exchange_unit_number <> '' And job:incoming_consignment_number <> ''
                      job:incoming_date = Today()
                      Display(?job:incoming_date)
                  End!If job:exchange_unit_number <> '' And job:incoming_consignment_number <> ''
      
      !The customer unit is brought into the workshop. Mark the replacement unit as 'In Repair'
      
      !Get The Audit Number
                  access:exchange_alias.clearkey(xch_ali:ref_number_key)
                  xch_ali:ref_number = job:exchange_unit_number
                  if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign
                      If xch:audit_number <> ''         
                          access:excaudit.clearkey(exa:audit_number_key)
                          exa:stock_type   = xch_ali:stock_type
                          exa:audit_number = xch_ali:audit_number
                          if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
      !Get The Replacement Unit Detailts
                              access:exchange.clearkey(xch:ref_number_key)
                              xch:ref_number = exa:replacement_unit_number
                              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                                  xch:available = 'REP'
                                  access:exchange.update()
                                  get(exchhist,0)
                                  if access:exchhist.primerecord() = level:benign
                                      exh:ref_number   = xch:ref_number
                                      exh:date          = today()
                                      exh:time          = clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      exh:user = use:user_code
                                      exh:status        = 'UNIT PUT INTO REPAIR'
                                      access:exchhist.insert()
                                  end!if access:exchhist.primerecord() = level:benign
                                                               
                              end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                          end!if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                      End!If xch:audit_number <> ''         
                  end!if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign
      
                  access:subtracc.clearkey(sub:account_number_key)
                  sub:account_number = job:account_number
                  if access:subtracc.fetch(sub:account_number_key) = level:benign
                      access:tradeacc.clearkey(tra:account_number_key) 
                      tra:account_number = sub:main_account_number
                      if access:tradeacc.fetch(tra:account_number_key) = level:benign
                          If tra:refurbcharge = 'YES'
                              refurb# = 0
                              IF job:exchange_unit_number <> ''
                                  refurb# = 1
                              End!IF job:exchange_unit_number <> ''
                              If refurb# = 0
                                  access:trantype.clearkey(trt:transit_type_key)
                                  trt:transit_type = job:transit_type
                                  if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                                      If trt:exchange_unit = 'YES'
                                          refurb# = 1
                                      End!If trt:exchange_unit = 'YES'
                                  EnD!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                              End!If refurb# = 0
                              IF refurb# = 0
                                  IF Sub(job:exchange_status,1,3) = '108'
                                      refurb# = 1
                                  End!IF Sub(job:exchange_status,1,3) = '108'
                              End!IF refurb# = 0
                              IF refurb# = 1
                                  Case MessageEx('This Repair Unit must be Refurbished.<13,10><13,10>A Refurbishment Label will be printed when you have completed editing this job.','ServiceBase 2000',|
                                                 'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  print_label_temp = 'YES'
                              End!IF refurb# = 1
                          End!If tra:refurbcharge = 'YES'
                      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                  end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
      
                  ! Start Change 2851 BE(02/07/03)
                  IF (tmp:SuspendPrintJobcard) THEN
                      access:trantype.clearkey(trt:transit_type_key)
                      trt:transit_type = job:transit_type
                      IF ((access:trantype.fetch(trt:transit_type_key) = Level:Benign) AND (trt:job_card = 'YES')) THEN
                          tmp:PrintJobcardNow    = 1
                     END
                  END
                  ! End Change 2851 BE(02/07/03)
      
              End!IF error# = 1
          End!If job:workshop = 'YES'
      End
      
      Do location_Type
      DIsplay()
      
      End     !If ~quickwindow{prop:acceptall}
      ! Status Change
      If ~0{prop:acceptall}
          If job:date_completed = ''
      !        job:location = ''
              job:in_repair = 'NO'
              job:engineer = ''
              job:on_test = 'NO'
              GetStatus(305,thiswindow.request,'JOB') !awaiting allocation
              Brw21.resetsort(1)
              Do time_remaining
          End
      End
      
      BRW21.ResetQueue(1)
      Do CheckRequiredFields
    OF ?job:Location
      If ~0{prop:acceptall}
      do_lookup# = 1
      
      If job:location <> location_temp
          error# = 0
          If job:date_completed <> ''
              If SecurityCheck('AMEND COMPLETED JOBS')
                  Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
                  job:location = location_temp
              End !If SecurityCheck('AMEND COMPLETED JOBS')
          End
          If job:invoice_Number <> ''
              Check_Access('AMEND INVOICED JOBS',x")
              If x" = False
                  Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:location = location_temp
                  error# = 1
              End
          End
      
          If error# = 1
              do_lookup# = 0
          End!"If error# = 1
      End!If job:location <> location_temp
      Do show_tabs
      If do_lookup# = 1 and job:location <> location_temp
      
      
      IF job:Location OR ?job:Location{Prop:Req}
        loi:Location = job:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:job:Location        = job:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            job:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            job:Location = look:job:Location
            SELECT(?job:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
              If job:location <> location_temp
                  If location_temp <> ''
              !Add To Old Location
                      access:locinter.clearkey(loi:location_key)
                      loi:location = location_temp
                      If access:locinter.fetch(loi:location_key) = Level:Benign
                          If loi:allocate_spaces = 'YES'
                              loi:current_spaces+= 1
                              loi:location_available = 'YES'
                              access:locinter.update()
                          End
                      end !if
                  End!If location_temp <> ''
              !Take From New Location
                  access:locinter.clearkey(loi:location_key)
                  loi:location = job:location
                  If access:locinter.fetch(loi:location_key) = Level:Benign
                      If loi:allocate_spaces = 'YES'
                          loi:current_spaces -= 1
                          If loi:current_spaces< 1
                              loi:current_spaces = 0
                              loi:location_available = 'NO'
                          End
                          access:locinter.update()
                      End
                  end !if
                  location_temp  = job:location
      
                  brw21.resetsort(1)
      
              End!If job:location <> location_temp
          End!If do_lookup# = 1
      End!If ~0{prop:acceptall}
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = job:Location
      loi:Location_Available = 'YES'
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          job:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?job:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Location)
    OF ?job:ESN
      If ~0{prop:acceptall}
          IF LEN(CLIP(job:ESN)) = 18
            !Ericsson IMEI!
            Job:ESN = SUB(job:ESN,4,15)
            !ESN_Entry_Temp = Job:ESN
            UPDATE()
          ELSE
            !Job:ESN = ESN_Entry_Temp
          END
          
      
      
      Error# = 0
      If job:ESN <> ESN_Temp
          If ESN_Temp <> ''
              Case MessageEx('Are you sure you want to change the I.M.E.I. Number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                  Of 2 ! &No Button
                      error# = 1
              End!Case MessageEx
          End !If ESN_Temp <> ''
      
          If Error# = 0
              If job:ESN <> 'N/A'
                  Set(DEFAULTS)
                  Access:DEFAULTS.Next()
                  If def:Allow_Bouncer <> 'YES'
                      If CountBouncer(job:ref_number,job:date_booked,job:esn)
                          Case MessageEx('The I.M.E.I. Number has been entered onto the system before.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          Error# = 1
                      End!If found# = 1
                  Else
                      If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                          If LiveBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                              Case MessageEx('The selected I.M.E.I. is already entered onto a live job. You must complete this previous job before you can continue booking.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              Error# = 1
                          End !If LiveBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
                      End !If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                  End !If def:Allow_Bouncers <> 'YES'
              End !If job:ESN <> 'N/A'
          End !If Error# = 0
      End !job:ESN <> ESN_Temp
      
      If Error# = 0
          If ExchangeAccount(job:Account_Number)
              Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
              xch:ESN = job:ESN
              If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Found
                  !Write the relevant fields to say this unit is in repair and take completed.
                  !Otherwise the use could click cancel and screw everything up.
              Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
                  Case MessageEx('The selected Trade Account is setup to repair Exchange Units, but the entered I.M.E.I. Number cannot be found in Exchange Stock.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Error# = 1
              End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
          Else !ExchangeAccount(job:Account_Number)
              Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
              xch:ESN = job:ESN
              If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                  !Found
                  If xch:Available <> 'DES'
                      Case MessageEx('The entered I.M.E.I. matches that of an entry in the Exchange Database. This unit is not marked as "Despatched" so may still exist in your stock.'&|
                        '<13,10>'&|
                        '<13,10>If you continue, this job will be treated as a normal customer repair and not a "In House Exchange Repair". These "In House" repairs should be booked in using an Account that is marked as "Exchange Account".'&|
                        '<13,10>'&|
                        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                          Of 2 ! &No Button
                              Error# = 1
                      End!Case MessageEx
                  End!Case MessageEx
              Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
              
      
          End !ExchangeAccount(job:Account_Number)
      End !Error# = 0
      
      Case Error#
          Of 0
              If ESN_Temp <> ''
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      aud:notes         = 'PREVIOUS I.M.E.I. NUMBER: ' & Clip(esn_temp) & |
                                          '<13,10>NEW I.M.E.I. NUMBER: ' & Clip(job:esn)
                      aud:ref_number    = job:ref_number
                      aud:date          = Today()
                      aud:time          = Clock()
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(job:esn)
                      if access:audit.insert()
                          access:audit.cancelautoinc()     
                      end            
                  end!if access:audit.primerecord() = level:benign                
              End !If ESN_Temp <> ''
              ESN_Temp = job:ESN
              Post(Event:Accepted,?job:Model_Number)
          Of 1
              job:ESN = ESN_Temp
      End !Error#
      Do Titles
      End!If ~0{prop:acceptall}
      Select(?+1)
      Do CheckRequiredFields
    OF ?job:MSN
      If ~0{prop:acceptall}
          IF LEN(CLIP(job:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
            !Ericsson MSN!
            Job:MSN = SUB(job:MSN,2,10)
            UPDATE()
          END
          ! Start Change 4239 BE(20/05/04)
          !IF (MSN_Field_Mask <> '') THEN
          IF (job:warranty_job = 'YES' AND MSN_Field_Mask <> '') THEN
              IF ((job:MSN <> '') AND (CheckFaultFormat(job:MSN, MSN_Field_Mask))) THEN
                  BEEP()
                  MessageEx('MSN Failed Format Validation',|
                            'ServiceBase 2000','Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,|
                            CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                  job:MSN = ''
                  SELECT(?job:MSN)
                  CYCLE
              END
          END
          ! End Change 4239 BE(20/05/04)
          Post(Event:Accepted,?job:Model_Number)
      End!If ~0{prop:acceptall}
      
    OF ?job:Model_Number
      do_lookup# = 1
      If ~0{prop:acceptall}
          If job:model_number <> model_number_temp
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If job:invoice_Number <> '' and error# = 0
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If error# = 1
                  do_lookup# = 0
              End!If error# = 0
          End!If job:model_number <> model_number_temp
          If do_lookup# = 1
      
      IF job:Model_Number OR ?job:Model_Number{Prop:Req}
        mod:Model_Number = job:Model_Number
        !Save Lookup Field Incase Of error
        look:job:Model_Number        = job:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            job:Model_Number = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            job:Model_Number = look:job:Model_Number
            SELECT(?job:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
              Error# = 0
      
              job:Model_Number    = IMEIModelRoutine(job:ESN,job:Model_Number)
      
              If job:Model_Number <> Model_Number_Temp
                  If Model_Number_Temp <> ''
                      Case MessageEx('You have selected to Change the Model Number.<13,10><13,10>Warning. The Pricing Structure may change causing the Job''s Costs to change.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                          Of 2 ! &No Button
                              error# = 1
                      End!Case MessageEx        
                  End !If Model_Number_Temp <> ''
              End!If job:Model_Number <> Model_Number_Temp
      
              If Error# = 0
                  If (CheckPricing('C') = 1 Or CheckPricing('W') = 1)
                      Error# = 1
                  End !If (CheckPricing('C') = 1 Or CheckPricing('W') = 1)
              End !If Error# = 0
      
              If Error# = 0
                  
                  Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                  mod:Model_Number    = job:Model_Number
                  If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Found
                      job:Manufacturer    = mod:Manufacturer
      
                      If mod:Specify_Unit_Type = 'YES'
                          job:Unit_Type   = mod:Unit_Type
                      End !If mod:Specify_Unit_Type = 'YES'
      
                      !Only autofil the Product Code Fault Code if it's Nokia and they
                      !are not using the other Product Code field.
                      If ?job:ProductCode{prop:Hide} = 1
                          If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                              job:fault_code1 = mod:product_type
                          End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
                      Else !If ?job:ProductCode{prop:Hide} = 1
                          If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                              job:fault_code1 = job:ProductCode
                          End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
                      End !If ?job:ProductCode{prop:Hide} = 1
      
      
                      If mod:Product_Type <> ''
                          job:Fault_code1 = mod:Product_Type
                      End !If mod:Product_Type <> ''
      
                      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                      man:Manufacturer    = job:Manufacturer
                      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                          !Found
                          ! Start Change 4239 BE(20/05/04)
                          MSN_Field_Mask = man:MSNFieldMask
                          ! End Change 4239 BE(20/05/04)
      
                          If man:Use_MSN = 'YES'
                              ?job:MSN{prop:Hide} = 0
                              ?job:MSN:Prompt{prop:Hide} = 0
                          Else !If man:Use_MSN = 'YES'        
                              ?job:MSN{prop:Hide} = 1
                              ?job:MSN{prop:Hide} = 1
                          End !If man:Use_MSN = 'YES'
                          
                          If CheckLength('IMEI',job:Model_Number,job:ESN)
                              job:ESN = ''
                              ! Start 2651 BE(04/06/03)
                              SELECT(?job:ESN)
                              ! End 2651 BE(04/06/03)
                          ! Start 2651 BE(04/06/03)
                          ELSE
                              IF (?job:MSN{prop:Hide} = 0) THEN
      
                                  ! Start Change 4239 BE(20/05/04)
                                  !IF (MSN_Field_Mask <> '') THEN
                                  IF (job:warranty_job = 'YES' AND MSN_Field_Mask <> '') THEN
                                      IF ((job:MSN <> '') AND (CheckFaultFormat(job:MSN, MSN_Field_Mask))) THEN
                                          job:MSN = ''
                                          SELECT(?job:MSN)
                                      END
                                  END
                                  ! End Change 4239 BE(20/05/04)
      
                                  IF (CheckLength('MSN',job:Model_Number,job:MSN)) THEN
                                      job:MSN = ''
                                      SELECT(?job:MSN)
                                  END
                              END
                          ! End 2651 BE(04/06/03)
                          End !If CheckLength('IMEI',job:Model_Number,job:ESN)
      
                          ! Start 2651 BE(04/06/03)
                          !If ?job:MSN{prop:Hide} = 0
                          !    If CheckLength('MSN',job:Model_Number,job:MSN)
                          !        job:MSN = ''
                          !    End !If CheckLength('MSN',job:Model_Number,job:MSN)
                          !End !If ?job:MSN{prop:Hide} = 0
                          ! End 2651 BE(04/06/03)
      
                      Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
                      If Model_Number_Temp <> ''
                          If Access:AUDIT.PrimeRecord() = Level:Benign
                              aud:Ref_Number    = job:ref_number
                              aud:Date          = Today()
                              aud:Time          = Clock()
                              aud:Type          = 'JOB'
                              Access:USERS.ClearKey(use:Password_Key)
                              use:Password      = glo:Password
                              Access:USERS.Fetch(use:Password_Key)
                              aud:User          = use:User_Code
                              If (CheckPricing('C') = Level:Benign And CheckPricing('W') = Level:Benign)
                                      aud:action     = 'MODEL NUMBER CHANGED: ' & Clip(job:model_number)
                                      aud:notes      = 'NEW MODEL NUMBER: ' & Clip(job:model_number) & '<13,10>OLD MODEL NUMBER: ' & Clip(model_number_temp)
                              Else!If (CheckPricing('C') = Level:Benign And CheckPricing('W') = Level:Benign)
                                      aud:notes      = 'NEW MODEL NUMBER: ' & Clip(job:model_number) & '<13,10>OLD MODEL NUMBER: ' & Clip(model_number_temp)
                                      aud:action     = 'REPRICE JOB: MODEL NUMBER CHANGED ' & Clip(job:model_number)
                              End!If (CheckPricing('C') = Level:Benign And CheckPricing('W') = Level:Benign)                    Access:AUDIT.Insert()
                              ! Start Change crc rogue audit file write bug BE(16/10/03)
                              if access:audit.insert()
                                access:audit.cancelautoinc()
                              end
                              ! End Change crc rogue audit file write bug BE(16/10/03)
                          End!If Access:AUDIT.PrimeRecord() = Level:Benign
              
                          Do Pricing
      
                          Model_Number_Temp   = job:Model_Number
      
                          !Autofil Colour
                          count# = 0
                          setcursor(cursor:wait)                                                          !Count how many colours have
                          save_moc_id = access:modelcol.savefile()                                        !been entered for this model.
                          access:modelcol.clearkey(moc:colour_key)                                        !If there is only one, USE IT
                          moc:model_number = job:model_number
                          set(moc:colour_key,moc:colour_key)
                          loop
                              if access:modelcol.next()
                                 break
                              end !if
                              if moc:model_number <> job:model_Number      |
                                  then break.  ! end if
                              count# += 1
                              If count# > 1
                                  Break
                              End!If count# > 1
                          end !loop
                          access:modelcol.restorefile(save_moc_id)
                          setcursor()
                          If count# = 1
                              job:colour  = moc:colour
                          End!If count# = 1
                          
                      End !If Model_Number_Temp <> ''
                  Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              End !If Error# = 0
              If Error# = 1
                  job:Model_Number    = Model_Number_Temp
              End !If Error# = 1
              Post(Event:Accepted,?job:DOP)
              Display()
      
          End!If do_lookup# = 1
      End !If ~0{prop:acceptall}
      Do CheckRequiredFields
    OF ?LookupModelNumber
      ThisWindow.Update
      mod:Model_Number = job:Model_Number
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          job:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?job:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Model_Number)
    OF ?job:Unit_Type
      If ~0{prop:acceptall}
          error# = 0
      
          If job:unit_type <> unit_type_temp
              If unit_Type_temp = ''
                  Case MessageEx('Are you sure you want to change the Unit Type?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
      
                      Of 2 ! &No Button
                          error# = 1
                  End!Case MessageEx
              End!If unit_Type_temp = ''
          End!If job:unit_type <> unit_type_temp
          If error# = 0
              access:unittype.clearkey(uni:unit_type_key)
              uni:unit_type = job:unit_type
              if access:unittype.fetch(uni:unit_type_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browseunittype
                  if globalresponse = requestcompleted
                      job:unit_type = uni:unit_type
                      display()
                  end
                  globalrequest     = saverequest#
              end !if access:unittype.fetch(uni:unit_type_key)
      
              If CheckPricing('W') Or CheckPricing('C')
                  error# = 1
              End!If CheckPricing('W') Or CheckPricing('C')
      
          End!If error# = 0
          If error# = 1
              job:unit_type    = unit_type_temp
          Else
              unit_type_temp  = job:unit_type
              Do pricing
          End!If error# = 1
      
      End
    OF ?accessory_button
      ThisWindow.Update
      If job:model_number = ''
          Case MessageEx('You must enter a Model Number first.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else
          glo:select1  = job:model_number
          glo:select2  = job:ref_number
      
          Browse_job_accessories
      
          glo:select1  = ''
          glo:select2  = ''
      
          Do Update_Accessories
      End
      
      
    OF ?job:DOP
      If ~0{prop:acceptall}
          !Only check DOP is it's a Warranty Job
          If job:warranty_job = 'YES'
              access:manufact.clearkey(man:manufacturer_key)
              man:manufacturer = job:manufacturer
              if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                  If job:dop < (job:date_booked - man:warranty_period) and man:warranty_period <> 0 and job:dop <> ''
                      Case MessageEx('This unit is outside the Manufacturer''s Warranty Period.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
      
                      ! Start Change 2601 BE(09/05/03)
                      !GetStatus(130,thiswindow.request,'JOB') !DOP query
                      IF (job:current_status[1:3] <> '130') THEN
                          GetStatus(130,thiswindow.request,'JOB') !DOP query
                      END
                      ! End Change 2601 BE(09/05/03)
      
                      DO time_remaining
                      brw21.resetsort(1)
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'D.O.P. OVER WARRANTY PERIOD BY ' & (job:date_booked - (job:dop + man:warranty_period)) & 'DAY(S)'
                          ! Start Change 2561 BE(28/04/03)
                          aud:notes = 'DATE BOOKED: ' & FORMAT(job:date_booked, @d06) & |
                                      '<13><10>DOP: ' & FORMAT(job:dop, @d06) & |
                                      '<13><10>WARRANTY PERIOD: ' & FORMAT(man:warranty_period, @n6) & ' DAYS'
                          ! End Change 2561 BE(28/04/03)
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                 ! Start Change 2601 BE(09/05/03)
                  ELSE
                      IF (job:current_status[1:3] = '130') THEN
                              ! Revert to previous status if DOP now valid
                              IF (LEN(CLIP(job:PreviousStatus)) > 0) THEN
                                  GetStatus(job:PreviousStatus[1:3],thiswindow.request,'JOB')
                              ELSE
                                  ! No Previous Status Recorded so Default to no status
                                  !GetStatus(0,thiswindow.request,'JOB')
                                  job:current_status = ''
                              END
                      END
                  ! End Change 2601 BE(09/05/03)
                  End!If job:dop > man:warranty_period + Today()
              end!if access:manufact.fetch(man:manufacturer_key) = Level:Benign
          End!If job:warranty_job = 'YES'
      End!If ~0{prop:acceptall}
    OF ?Fault_Description_Text_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Fault_Description
      ThisWindow.Reset
      glo:select1 = ''
      Do AreThereNotes
      
      
      
    OF ?tmp:Network:2
      IF tmp:Network OR ?tmp:Network:2{Prop:Req}
        net:Network = tmp:Network
        !Save Lookup Field Incase Of error
        look:tmp:Network        = tmp:Network
        IF Access:NETWORKS.TryFetch(net:NetworkKey)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            tmp:Network = net:Network
          ELSE
            !Restore Lookup On Error
            tmp:Network = look:tmp:Network
            SELECT(?tmp:Network:2)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupNetwork:2
      ThisWindow.Update
      net:Network = tmp:Network
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          tmp:Network = net:Network
          Select(?+1)
      ELSE
          Select(?tmp:Network)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Network)
    OF ?job:Mobile_Number
      ! Start Change MobileNumberValidate BE (23/06/03)
       If ~0{prop:acceptall}
          len# = LEN(CLIP(job:mobile_number))
          iy# = 0
          mobile_error# = 0
          LOOP ix# = 1 TO len#
              IF (job:mobile_number[ix#] <> ' ') THEN
                  IF ((job:mobile_number[ix#] < '0') OR (job:mobile_number[ix#] > '9')) THEN
                      mobile_error# = 1
                  END
                  iy# += 1
                  temp_255string[iy#] = job:mobile_number[ix#]
              END
          END
      
          ! Check for Validation error here... (mobile_error#)
          IF ((ValidateMobileNumber) AND (mobile_error# = 1)) THEN
              MessageEx('Mobile Number must be numeric','ServiceBase 2000',|
                      'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,|
                      '',0,beep:systemhand,msgex:samewidths,84,26,0)
              SELECT(?job:mobile_number)
              job:mobile_number = ''
              CYCLE
          ELSE
              IF (iy# > 0) THEN
                  job:mobile_number = temp_255string[1 : iy#]
                  job:mobile_number[1] = UPPER(job:mobile_number[1])
              ELSE
                  job:mobile_number = ''
              END
              Display(?job:mobile_number)
          END
       END
       ! End Change MobileNumberValidate BE (23/06/03)
    OF ?job:Turnaround_Time
      If ~0{prop:acceptall}
      access:turnarnd.clearkey(tur:turnaround_time_key)
      tur:turnaround_time = job:turnaround_time
      if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
          Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
          job:turnaround_end_date = Clip(end_date")
          job:turnaround_end_time = Clip(end_time")
          Do Time_Remaining
      end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
      End!If ~0{prop:acceptall}
    OF ?job:Chargeable_Job
      IF ?job:Chargeable_Job{Prop:Checked} = True
        UNHIDE(?JOB:Charge_Type)
        UNHIDE(?LookupChargeType)
      END
      IF ?job:Chargeable_Job{Prop:Checked} = False
        HIDE(?JOB:Charge_Type)
        HIDE(?LookupChargeType)
      END
      ThisWindow.Reset
      Do show_hide_tabs
      Post(Event:Accepted,?job:Estimate)
      Do Estimate
      Do CheckRequiredFields
    OF ?job:Charge_Type
      If job:Chargeable_Job = 'YES'
      ! Start Change 3924 BE(8/3/2004)
        IF job:Charge_Type OR ?job:Charge_Type{Prop:Req}
            cha:Charge_Type = job:Charge_Type
            cha:Warranty = 'NO'
            GLO:Select1 = 'NO'
            IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
                IF SELF.Run(6,SelectRecord) = RequestCompleted
                    job:Charge_Type = cha:Charge_Type
                ELSE
                    CLEAR(cha:Warranty)
                    CLEAR(GLO:Select1)
                    job:Charge_Type = charge_type_temp
                    SELECT(?job:Charge_Type)
                    CYCLE
                END
            END
        END
        ThisWindow.Reset()
        ! make sure to skip the following temmplate code section
        SkipTemplateCode# = 1
        IF (SkipTemplateCode# = 0) THEN
        ! End Change 3924 BE(8/3/2004)
      IF job:Charge_Type OR ?job:Charge_Type{Prop:Req}
        cha:Charge_Type = job:Charge_Type
        cha:Warranty = 'NO'
        GLO:Select1 = 'NO'
        !Save Lookup Field Incase Of error
        look:job:Charge_Type        = job:Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            job:Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Charge_Type = look:job:Charge_Type
            SELECT(?job:Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! Start Change 3924 BE(8/3/2004)
      END ! SkipTemplateCode
      ! End Change 3924 BE(8/3/2004)
      
      If ~0{prop:acceptall}
          If job:chargeable_job = 'YES'
      
              no_change# = 0
              If job:charge_type <> charge_type_temp
                  error# = 0
                  If job:date_completed <> '' and error# = 0
                      If SecurityCheck('AMEND COMPLETED JOBS')
                          Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          error# = 1
                      End
                  End
                  If job:invoice_Number <> '' and error# = 0
                      If SecurityCheck('AMEND INVOICED JOBS')
                          Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          error# = 1
                      End
                  End
      
                  ! Start Change 3924 BE(27/02/2004)
                  If error# = 0
                      IF CheckPricing('C') = 1
                          error# = 1
                      End!IF CheckPricing('C')
                  End!If error# = 0
                  ! End Change 3924 BE(27/02/2004)
      
                  If error# = 1
                      job:charge_type = charge_type_temp
                  Else!If job:date_completed <> ''
                  
                      If charge_type_temp <> ''
                          Case MessageEx('Are you sure you want to change the Charge Type?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  no_change# = 0
                              Of 2 ! &No Button
                                  job:charge_type     = charge_type_temp
                                  no_change# = 1
                          End!Case MessageEx
                      End
                      If no_change# = 0
                          access:chartype.clearkey(cha:warranty_key)
                          cha:charge_type = job:charge_type
                          cha:warranty    = 'NO'
                          if access:chartype.fetch(cha:warranty_key) = Level:Benign
                              If cha:allow_estimate <> 'YES' And job:estimate = 'YES'
                                  Case MessageEx('The selected Charge Type cannot be used for an Estimated Job.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  job:charge_type = charge_type_temp
                                  no_change# = 1
                              End
                          End !if access:chartype.fetch(cha:charge_type_key) = Level:Benign
                      End
      
                      ! Start Change 3924 BE(27/02/2004)
                      !If error# = 0
                      !    IF CheckPricing('C') = 1
                      !        error# = 1
                      !    End!IF CheckPricing('C')
                      !End!If error# = 0
                      ! End Change 3924 BE(27/02/2004)
      
                      If no_change# = 0
                         charge_type_temp    = job:charge_type
                      End
                  End!If job:date_completed <> ''
              End
              If job:chargeable_job   = 'YES'
                  access:chartype.clearkey(cha:charge_type_key)
                  cha:charge_type = job:charge_type
                  If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                      If cha:allow_estimate = 'YES'
                          Unhide(?job:estimate)
                          ! Start Change 4751 BE(04/10/2004)
                          !IF (job:charge_type <> charge_type_temp) THEN
                          !    IF (cha:force_estimate <> 'YES') THEN
                          !        job:estimate = 'NO'
                          !    END
                          !END
                          ! End Change 4751 BE(04/10/2004)
                          If job:estimate <> 'YES' and cha:force_estimate = 'YES'
                              Case MessageEx('The selected Charge Type means that this job must be an Estimate.','ServiceBase 2000',|
                                             'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              job:estimate = 'YES'
                              ! Start Change 4751 BE(04/10/2004)
                              !!Post(Event:accepted,?job:estimate)
                              ! End Change 4751 BE(04/10/2004)
                          End!If job:estimate <> 'YES'
                      Else
                          ! Start Change 4751 BE(04/10/2004)
                          !job:estimate = 'NO'
                          ! End Change 4751 BE(04/10/2004)
                          Hide(?job:estimate)
                      End
                  end !If access:chartype.fetch(cha:charge_type_key) = Level:Benign
                  ! Start Change 4751 BE(04/10/2004)
                  If job:Estimate = 'YES'
                  !IF (job:Estimate <> estimate_temp) THEN
                  ! End Change 4751 BE(04/10/2004)
                      Post(Event:Accepted,?job:Estimate)
                  End !If job:Estimate = 'YES'
              End!If job:chargeable_job   = 'YES'
          End!If job:chargeable_job = 'YES'
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number    = job:Model_Number
          If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Found
              job:EDI = PendingJob(mod:Manufacturer)
              job:Manufacturer = mod:Manufacturer
          Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
      End!If ~0{prop:acceptall}
      Do Show_Tabs
      Do Pricing
      End !If job:Chargeable_Job = 'YES'
    OF ?LookupChargeType
      ThisWindow.Update
      cha:Charge_Type = job:Charge_Type
      GLO:Select1 = 'NO'
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          job:Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?job:Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Charge_Type)
    OF ?job:Warranty_Job
      IF ?job:Warranty_Job{Prop:Checked} = True
        UNHIDE(?job:charge_type:prompt)
        UNHIDE(?LookupWarrantyChargeType)
        UNHIDE(?job:Warranty_Charge_Type)
      END
      IF ?job:Warranty_Job{Prop:Checked} = False
        HIDE(?JOB:Warranty_Charge_Type)
        HIDE(?LookupWarrantyChargeType)
      END
      ThisWindow.Reset
      Do show_hide_tabs
      If job:warranty_job = 'NO'
          job:edi = 'XXX'
      End!If job:warranty_job = 'NO'
      Post(event:accepted,?job:warranty_charge_type)
      Do CheckRequiredFields
    OF ?job:Warranty_Charge_Type
      If job:Warranty_Job = 'YES'
          ! Start Change 3924 BE(8/3/2004)
          IF job:Warranty_Charge_Type OR ?job:Warranty_Charge_Type{Prop:Req}
              cha:Charge_Type = job:Warranty_Charge_Type
              cha:Warranty = 'YES'
              GLO:Select1 = 'YES'
              IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
                  IF SELF.Run(7,SelectRecord) = RequestCompleted
                      job:Warranty_Charge_Type = cha:Charge_Type
                  ELSE
                      CLEAR(cha:Warranty)
                      CLEAR(GLO:Select1)
                      job:Warranty_Charge_Type = Warranty_Charge_Type_Temp
                      SELECT(?job:Warranty_Charge_Type)
                      CYCLE
                  END
              END
          END
          ThisWindow.Reset()
          ! make sure to skip the following temmplate code section
          SkipTemplateCode# = 1
          IF (SkipTemplateCode# = 0) THEN
          ! END Change 3924 BE(8/3/2004)
      IF job:Warranty_Charge_Type OR ?job:Warranty_Charge_Type{Prop:Req}
        cha:Charge_Type = job:Warranty_Charge_Type
        cha:Warranty = 'YES'
        GLO:Select1 = 'YES'
        !Save Lookup Field Incase Of error
        look:job:Warranty_Charge_Type        = job:Warranty_Charge_Type
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            job:Warranty_Charge_Type = cha:Charge_Type
          ELSE
            CLEAR(cha:Warranty)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            job:Warranty_Charge_Type = look:job:Warranty_Charge_Type
            SELECT(?job:Warranty_Charge_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! Start Change 3924 BE(8/3/2004)
      END ! SkipTemplateCode
      ! End Change 3924 BE(8/3/2004)
      
      If ~0{prop:acceptall}
          If job:warranty_job = 'YES'
              Error# = 0
              If job:Warranty_Charge_Type <> Warranty_Charge_Type_Temp
                  If job:Date_Completed <> '' And Error# = 0
                      If SecurityCheck('AMEND COMPLETED JOBS')
                          Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          Error# = 1
                      End !If SecurityCheck('AMEND INVOICED JOBS')
                  End !If job:Date_Completed <> ''
      
                  If job:Invoice_Number_Warranty <> '' And Error# = 0
                      If SecurityCheck('AMEND INVOICED JOBS')
                          Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          Error# = 1
                      End !If SecurityCheck('AMEND INVOICED JOBS')
                  End !If job:Invoice_Number_Warranty <> '' And Error# = 0
      
                  If Error# = 0
                      If CheckPricing('W') = 1
                          Error# = 1
                      End !If CheckPricing('W')
                  End !If Error# = 0
      
                  If Warranty_Charge_Type_Temp <> '' And Error# = 0
                      Case MessageEx('Are you sure you want to change the Charge Type?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                          Of 2 ! &No Button
                              Error# = 0
                              
                      End!Case MessageEx
                  End !If Warranty_Charge_Type <> ''
                  If Error# = 0
                      Warranty_Charge_Type_Temp   = job:Warranty_Charge_Type
                  Else !If Error# = 0
                      job:Warranty_Charge_Type    = Warranty_Charge_Type_Temp
                  End !If Error# = 0
              End !If job:Warranty_Charge_Type <> Warranty_Charge_Type_Temp
              Display()
          End!If job:warranty_job = 'YES'
          Access:MODELNUM.Clearkey(mod:Model_Number_Key)
          mod:Model_Number    = job:Model_Number
          If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Found
              job:EDI = PendingJob(mod:Manufacturer)
              job:Manufacturer = mod:Manufacturer
          Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
          
          
      End!If ~0{prop:acceptall}
      Do Show_Tabs
      Do Pricing
      End !If job:Warranty_Job = 'YES'
    OF ?LookupWarrantyChargeType
      ThisWindow.Update
      cha:Charge_Type = job:Warranty_Charge_Type
      GLO:Select1 = 'YES'
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          job:Warranty_Charge_Type = cha:Charge_Type
          Select(?+1)
      ELSE
          Select(?job:Warranty_Charge_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Warranty_Charge_Type)
    OF ?job:Estimate
      If ~0{prop:acceptall}
          If job:estimate <> estimate_temp and job:Chargeable_Job = 'YES'
              If job:estimate = 'NO'
                  reset_status# = 1
                  If job:estimate_accepted = 'YES'
                      Case MessageEx('Warning! The estimate on this job has been accepted.<13,10><13,10>Do you want to reset this estimate?','ServiceBase 2000',|
                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              estimate_temp   = job:estimate
                              ! Start Change 4751 BE(04/10/2004)
                              !job:estimate_accepted = 'NO'
                              !estimate_accepted_temp = 'NO'
                              !job:estimate_ready = 'NO'
                              !estimate_ready_temp = 'NO'
                              ! Start Change 4751 BE(04/10/2004)
                          Of 2 ! &No Button
                              job:estimate    = estimate_temp
                              reset_status# = 0
                      End!Case MessageEx
                  End
                  If reset_status# = 1
                      ! Start Change 2281 BE(04/03/03)
                      IF (thiswindow.request = 1) THEN
                          estimate_temp   = job:estimate
                          IF ((LEN(CLIP(job:current_status)) > 0) AND (job:current_status[1:3] = '505')) THEN
                              IF (LEN(CLIP(job:PreviousStatus)) > 0) THEN
                                  GetStatus(job:PreviousStatus[1:3],thiswindow.request,'JOB')
                              ELSE
                                  ! No Previous Status Recorded so Default to "114 Awaiting Arrival"
                                  GetStatus(114,thiswindow.request,'JOB')
                              END
                         END
                      ELSE
                      ! End Change 2281 BE(04/03/03)
                          setcursor(cursor:wait)
                          save_aud_id = access:audit.savefile()
                          access:audit.clearkey(aud:ref_number_key)
                          aud:ref_number = job:ref_number
                          set(aud:ref_number_key,aud:ref_number_key)
                          loop
                              if access:audit.next()
                                  break
                              end !if
                              if aud:ref_number <> job:ref_number  |
                                  then break.  ! end if
                              If Instring('STATUS CHANGE',aud:action,1,1) And Instring('505',aud:action,1,1)
                                  ! Start Change 2281 BE(04/03/03)
                                  estimate_temp   = job:estimate ! Change 2281 BE(04/03/03)
                                  ! End Change 2281 BE(04/03/03
                                  GetStatus(Clip(Sub(aud:notes,18,3)),thiswindow.request,'JOB')
      
                                  Do time_remaining
                                  brw21.resetsort(1)
                                  Break
                              End!If Instring('STATUS CHANGE',aud:action,1,1) And Instring('505',aud:action,1,1)
                          end !loop
                          access:audit.restorefile(save_aud_id)
                          setcursor()
                      ! Start Change 2281 BE(04/03/03)
                      END ! IF
                      ! Start Change 2281 BE(04/03/03)
                  End!If reset_status# = 1
              Else!If job:estimate = 'NO'
                  ! Start Change 2281 BE(04/03/03)
                  estimate_temp   = job:estimate  ! Change 2281 BE(04/03/03)
                  ! Start Change 3922 BE(02/03/04)
                  !GetStatus(505,thiswindow.request,'JOB')
                  DO Pricing
                  IF ((job:estimate_if_over = 0.0) OR (total_estimate_temp > job:estimate_if_over)) THEN
                    GetStatus(505,thiswindow.request,'JOB')
                  END
                  ! End Change 3922 BE(02/03/04)
                  !GetStatus(505,1,'JOB')
                  ! End Change 2281 BE(04/03/03)
                  Brw21.resetsort(1)
                  Do time_remaining
              End!If job:estimate = 'NO'
          End
          ! Start Change 2286 BE(10/03/03)
          Do Pricing
          ! End Change 2286 BE(10/03/03)
          Do Estimate
      End!If ~0{prop:acceptall}
      Do Show_Tabs
      Do show_hide_tabs
    OF ?job:Estimate_If_Over
      Do Estimate_Check
    OF ?job:Estimate_Accepted
      If ~0{prop:acceptall}
      If job:estimate_accepted <> estimate_accepted_temp
          If job:estimate_accepted = 'NO'
              check_access('RELEASE - ESTIMATE ACCEPTED',x")
              If x" = False
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:estimate_accepted   = estimate_accepted_temp
              Else !If x" = False
                  ! Start Change 4033 BE(4/6/2004)
                  !estimate_accepted_temp  = job:estimate_accepted
                  found# = 0
                  save_par_id = access:parts.savefile()
                  access:parts.clearkey(par:part_number_key)
                  par:ref_number  = job:ref_number
                  SET(par:part_number_key,par:part_number_key)
                  LOOP
                      IF ((access:parts.next() <> Level:Benign) OR |
                          (par:ref_number <> job:ref_number)) THEN
                         BREAK
                      END
                      found# = 1
                      BREAK
                  END
                  access:parts.restorefile(save_par_id)
                  IF (found# = 1) THEN
                      MessageEx('The estimate cannot be unaccepted while chargeable parts are attached.<13,10><13,10>Please restock chargeable parts and try again.','ServiceBase 2000',|
                                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                      job:estimate_accepted   = estimate_accepted_temp
                  ELSE
                      IF (job:current_status[1 : 3] = '535') THEN ! Estimate Accepted
                          GetStatus(505,thiswindow.request,'JOB') !estimate required
                      END
                      job:estimate_ready = 'NO'
                      estimate_ready_temp = job:estimate_ready
                      estimate_accepted_temp  = job:estimate_accepted
                      DO show_hide_tabs
                  END
                  ! End Change 4033 BE(4/6/2004)
              End !If x" = False
          Else !If job:in_repair = 'NO'
              error# = 0
              If job:estimate_ready <> 'YES'
                  error# = 1
                  Case MessageEx('The Estimate has not been marked as ''Ready''.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:estimate_accepted = estimate_accepted_temp
              Else!If job:estimate_ready <> 'YES'
                  ! Start Change 4033 BE(7/6/2004)
                  job:estimate_rejected = 'NO'
                  estimate_rejected_temp = job:estimate_rejected
                  ! End Change 4033 BE(7/6/2004)
                  estimate_accepted_temp  = job:estimate_accepted
                  GetStatus(535,thiswindow.request,'JOB') !estimate accepted
                  Brw21.resetsort(1)
                  Do time_remaining
      
                  glo:select1  = 'ACCEPTED'
                  glo:select2  = ''
                  glo:select3  = ''
                  glo:select4  = ''
                  Estimate_Reason
                  glo:select1  = ''
      
                  get(audit,0)
                  aud:notes      = 'COMMUNICATION METHOD: ' & Clip(glo:select3)
                  aud:ref_number = job:ref_number
                  aud:date       = Today()
                  aud:time       = CLock()
                  aud:user       = use:user_code
                  aud:action     = 'ESTIMATE ACCEPTED FROM: ' & Clip(glo:select2)
                  access:audit.insert()
      
                  glo:select2 = ''
                  glo:select3 = ''
                  glo:select4 = ''
                  job:COurier_Cost = job:Courier_Cost_Estimate
      
                  setcursor(cursor:wait)
                  if def:PickNoteNormal or def:PickNoteMainStore then CreatePickingNote (sto:Location).
                  
                  save_epr_id = access:estparts.savefile()
                  access:estparts.clearkey(epr:part_number_key)
                  epr:ref_number  = job:ref_number
                  set(epr:part_number_key,epr:part_number_key)
                  loop
                      if access:estparts.next()
                         break
                      end !if
                      if epr:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
      
                      ! Start Change 2116 BE(26/06/03)
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number = epr:part_ref_number
                      IF (access:stock.fetch(sto:ref_number_key) = Level:benign) THEN
                          !TH 09/02/04
                          Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                          loc:Active   = 1
                          loc:Location = sto:Location
                          If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
                          if (~loc:PickNoteEnable) then
                              IF (sto:RF_BOARD) THEN
                                  glo:select2 = ''
                                  glo:select3 = job:model_number
                                  glo:select4 = epr:part_number
                                  SETCURSOR()
                                  EnterNewIMEI
                                  SETCURSOR(cursor:wait)
                                  tmp_RF_Board_IMEI = CLIP(glo:select2)
                                  glo:select2 = ''
                                  glo:select3 = ''
                                  glo:select4 = ''
                                  IF (tmp_RF_Board_IMEI = '') THEN
                                      CYCLE
                                  END
                                  GET(audit,0)
                                  IF (access:audit.primerecord() = level:benign) THEN
                                      aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                                           '<13,10>Old IMEI ' & Clip(job:esn)
                                      aud:ref_number    = job:ref_number
                                      aud:date          = Today()
                                      aud:time          = Clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      aud:user = use:user_code
                                      aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                                      IF (access:audit.insert() <> Level:benign) THEN
                                          access:audit.cancelautoinc()
                                      END
                                  END
                                  access:JOBSE.clearkey(jobe:RefNumberKey)
                                  jobe:RefNumber  = job:Ref_Number
                                  IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                                      IF (access:JOBSE.primerecord() = Level:Benign) THEN
                                          jobe:RefNumber  = job:Ref_Number
                                          IF (access:JOBSE.tryinsert()) THEN
                                              access:JOBSE.cancelautoinc()
                                          END
                                      END
                                  END
                                  IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                                      jobe:PRE_RF_BOARD_IMEI = job:ESN
                                      access:jobse.update()
                                  END
                                  job:ESN = tmp_RF_Board_IMEI
                                  access:jobs.update()
                              END
                          END !if (~loc:PickNoteEnable)
                      END
                      ! End Change 2116 BE(26/06/03)

                      get(parts,0)
                      glo:select1 = ''
                      if access:parts.primerecord() = level:benign
                          par:ref_number           = job:ref_number
                          par:adjustment           = epr:adjustment
                          par:part_number     = epr:part_number
                          par:description     = epr:description
                          par:supplier        = epr:supplier
                          par:purchase_cost   = epr:purchase_cost
                          par:sale_cost       = epr:sale_cost
                          par:retail_cost     = epr:retail_cost
                          par:quantity             = epr:quantity
                          par:exclude_from_order   = epr:exclude_from_order
                          par:part_ref_number      = epr:part_ref_number

                          If par:exclude_from_order <> 'YES'
                             If par:part_ref_number <> ''
                                  access:stock.clearkey(sto:ref_number_key)
                                  sto:ref_number = par:part_ref_number
                                  if access:stock.fetch(sto:ref_number_key)
                                      Case MessageEx('The original Stock Part for this part: '&Clip(par:part_number)&' cannot be found.<13,10><13,10>It will therefore be inserted as an External Order.','ServiceBase 2000',|
                                                     'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      Get(ordpend,0)
                                      If access:ordpend.primerecord() = Level:Benign
                                          ope:part_ref_number = ''
                                          ope:job_number  = job:ref_number
                                          ope:part_type   = 'JOB'
                                          ope:supplier    = par:supplier
                                          ope:part_number = par:part_number
                                          ope:description = par:description
                                          ope:quantity    = par:quantity
                                          access:ordpend.insert()
                                          par:pending_ref_number  = ope:ref_number
                                          if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart('chargeable').
                                          access:parts.insert()
                                      End!If access:ordpend.primerecord() = Today()
                                  Else!if access:stock.fetch(sto:ref_number_key)
                                      If sto:sundry_item <> 'YES'
                                         If par:quantity > sto:quantity_stock
                                              get(ordpend,0)
                                              if access:ordpend.primerecord() = level:benign
                                                  ope:part_ref_number = sto:ref_number
                                                  ope:job_number      = job:ref_number
                                                  ope:part_type       = 'JOB'
                                                  ope:supplier        = par:supplier
                                                  ope:part_number     = par:part_number
                                                  ope:description     = par:description
                                                  If sto:quantity_stock = 0
                                                      ope:quantity    = par:quantity
                                                      par:pending_ref_number = ope:ref_number
                                                      if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart('chargeable').
                                                      access:parts.insert()
                                                  Else!If sto:quantity_stock <= 0
                                                      ope:quantity    = par:quantity - sto:quantity_stock
                                                      par:quantity    = sto:quantity_stock
                                                      par:date_ordered    = Today()
                                                      access:parts.insert()
                                                      sto:quantity_stock  = 0
                                                      access:stock.update()
                                                      get(stohist,0)
                                                      if access:stohist.primerecord() = level:benign
                                                          shi:ref_number           = sto:ref_number
                                                          shi:transaction_type     = 'DEC'
                                                          shi:despatch_note_number = par:despatch_note_number
                                                          shi:quantity             = par:quantity
                                                          shi:date                 = Today()
                                                          shi:purchase_cost        = par:purchase_cost
                                                          shi:sale_cost            = par:sale_cost
                                                          shi:retail_cost          = par:retail_cost
                                                          shi:job_number           = job:ref_number
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          shi:user                 = use:user_code
                                                          shi:notes                = 'STOCK DECREMENTED'
                                                          shi:information          = ''
                                                          if access:stohist.insert()
                                                              access:stohist.cancelautoinc()
                                                          end
                                                      end!if access:stohist.primerecord() = level:benign
      
      
                                                      Get(parts,0)
                                                      If access:parts.primerecord() = Level:Benign
                                                          par:ref_number  = job:ref_number
                                                          par:adjustment  = epr:adjustment
                                                          par:part_ref_number = sto:ref_number
                                                          par:part_number     = epr:part_number
                                                          par:description     = epr:description
                                                          par:supplier        = epr:supplier
                                                          par:purchase_cost   = epr:purchase_cost
                                                          par:sale_cost       = epr:sale_cost
                                                          par:retail_cost     = epr:retail_cost
                                                          par:quantity    = ope:quantity
                                                          par:pending_ref_number  = ope:ref_number
                                                          if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart('chargeable').
                                                          access:parts.insert()
                                                      End!If access:parts.primerecord() = Level:Benign
                                                  End!If sto:quantity_stock = 0
                                                  access:ordpend.insert()
                                              end!if access:ordpend.primerecord() = level:benign
      
                                         Else !If par:quantity > sto:quantity_stock
                                              sto:quantity_stock -= par:quantity
                                              If sto:quantity_stock < 0
                                                  sto:quantity_stock = 0
                                              End
                                              Access:stock.update()
                                              par:date_ordered    = Today()
                                              if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart('chargeable').
                                              access:parts.insert()
      
                                              get(stohist,0)
                                              if access:stohist.primerecord() = level:benign
                                                  shi:ref_number           = sto:ref_number
                                                  shi:transaction_type     = 'DEC'
                                                  shi:despatch_note_number = par:despatch_note_number
                                                  shi:quantity             = par:quantity
                                                  shi:date                 = Today()
                                                  shi:purchase_cost        = par:purchase_cost
                                                  shi:sale_cost            = par:sale_cost
                                                  shi:retail_cost          = par:retail_cost
                                                  shi:job_number           = job:ref_number
                                                  access:users.clearkey(use:password_key)
                                                  use:password =glo:password
                                                  access:users.fetch(use:password_key)
                                                  shi:user                 = use:user_code
                                                  shi:notes                = 'STOCK DECREMENTED'
                                                  shi:information          = ''
                                                  if access:stohist.insert()
                                                      access:stohist.cancelautoinc()
                                                  end
                                              end!if access:stohist.primerecord() = level:benign
                                          End !If par:quantity > sto:quantity_stock
                                      Else!If sto:sundry_item <> 'YES'
                                          par:date_ordered = Today()
                                          !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart('chargeable').
                                          access:parts.insert()
                                      End!If sto:sundry_item <> 'YES'
                                  end!if access:stock.fetch(sto:ref_number_key)
                              Else !If par:part_ref_number <> ''
                                  get(ordpend,0)
                                  if access:ordpend.primerecord()= level:benign
                                      ope:part_ref_number = ''
                                      ope:job_number      = job:ref_number
                                      ope:part_type       = 'JOB'
                                      ope:supplier        = par:supplier
                                      ope:part_number     = par:part_number
                                      ope:description     = par:description
                                      ope:quantity        = par:quantity
                                      access:ordpend.insert()
                                      par:pending_ref_number = ope:ref_number
                                      if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart('chargeable').
                                      access:parts.insert()
                                  end!if access:ordpend.primerecord()= level:benign
                              End !If par:part_ref_number <> ''
                          Else!If par:exclude_from_order <> 'YES'
                              par:date_ordered    = 'YES'
                              !if def:PickNoteNormal or def:PickNoteMainStore then InsertPickingPart('chargeable').
                              access:parts.insert()
                          End!If par:exclude_from_order <> 'YES'
                      end!if access:parts.primerecord() = level:benign
                  end !loop
                  access:estparts.restorefile(save_epr_id)
                  setcursor()
              End!If job:estimate_ready <> 'YES'
          End !If job:in_repair = 'NO'
          Brw12.resetsort(1)
          Do pricing
          Do show_hide_tabs
          Do check_parts
          ! Start Change 2116 BE(26/06/03)
          DO CountParts
          ! End Change 2116 BE(26/06/03)
      End !If job:in_repair <> in_repair_temp
      
      End !If ~0{prop:acceptall}
    OF ?job:Estimate_Rejected
      If ~0{prop:acceptall}
      If job:estimate_rejected <> estimate_rejected_temp
          If job:estimate_rejected = 'NO'
              check_access('RELEASE - ESTIMATE REJECTED',x")
              If x" = False
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  job:estimate_rejected   = estimate_rejected_temp
              Else !If x" = False
                  estimate_rejected_temp  = job:estimate_rejected
              End !If x" = False
          Else !If job:in_repair = 'NO'
              ! Start Change 4033 BE(7/6/2004)
              found# = 0
              IF (job:estimate_accepted = 'YES') THEN
                  save_par_id = access:parts.savefile()
                  access:parts.clearkey(par:part_number_key)
                  par:ref_number  = job:ref_number
                  SET(par:part_number_key,par:part_number_key)
                  LOOP
                      IF ((access:parts.next() <> Level:Benign) OR |
                          (par:ref_number <> job:ref_number)) THEN
                          BREAK
                      END
                      found# = 1
                      BREAK
                  END
                  access:parts.restorefile(save_par_id)
              END
              IF (found# = 1) THEN
                  MessageEx('The estimate cannot be unaccepted while chargeable parts are attached.<13,10><13,10>Please restock chargeable parts and try again.','ServiceBase 2000',|
                                 'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                  job:estimate_rejected   = estimate_rejected_temp
              ELSE
                  job:estimate_ready = 'NO'
                  estimate_ready_temp = job:estimate_ready
                  job:estimate_accepted = 'NO'
                  estimate_accepted_temp = job:estimate_accepted
              ! End Change 4033 BE(7/6/2004)
                  estimate_rejected_temp  = job:estimate_rejected
                  GetStatus(540,thiswindow.request,'JOB') !estimate refused
                  Brw21.resetsort(1)
                  Do time_remaining
      
                  glo:select1  = 'REJECTED'
                  glo:select2  = ''
                  glo:select3  = ''
                  glo:select4  = ''
                  Estimate_Reason
                  glo:select1  = ''
      
                  get(audit,0)
                  aud:notes      = 'COMMUNICATION METHOD: ' & Clip(glo:select3) & |
                                   '<13,10>REASON: ' & Clip(glo:select4)
                  aud:ref_number = job:ref_number
                  aud:date       = Today()
                  aud:time       = CLock()
                  aud:user       = use:user_code
                  aud:action     = 'ESTIMATE REJECTED FROM: ' & Clip(glo:select2)
                  access:audit.insert()
      
                  glo:select2 = ''
                  glo:select3 = ''
                  glo:select4 = ''
              ! Start Change 4033 BE(7/6/2004)
                  DO show_hide_tabs
              END
              ! End Change 4033 BE(7/6/2004)
          End !If job:in_repair = 'NO'
      End !If job:in_repair <> in_repair_temp
      End !If ~0{prop:acceptall}
    OF ?LookupNetwork
      ThisWindow.Update
      GlobalRequest = SelectRecord
      PickNetworks
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              tmp:Network = net:Network  
          Of Requestcancelled
      End!Case Globalreponse
      Display(?tmp:Network)
    OF ?LookupSIMNumber
      ThisWindow.Update
      UpdateSIMNumber(job:Ref_Number)
      ThisWindow.Reset
      access:JOBSE.clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
          Display(?jobe:SIMNumber)
      Else! If access:JOBSE.tryfetch(jobe:RefNumberKey.) = Level:Benign
          !Error
          !Assert(0,'Fetch Error')
      End! If access:JOBSE.tryfetch(jobe:RefNumberKey.) = Level:Benign
    OF ?accessory_button:2
      ThisWindow.Update
      If job:model_number = ''
          Case MessageEx('You must enter a Model Number first.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else
          glo:select1  = job:model_number
          glo:select2  = job:ref_number
      
          Browse_job_accessories
      
          glo:select1  = ''
          glo:select2  = ''
      
          Do Update_Accessories
      End
      
      
    OF ?Fault_Description_Text_Button:2
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Fault_Description
      ThisWindow.Reset
      Do AreThereNotes
    OF ?show_booking_temp
      If show_booking_temp = 'YES'
          Hide(?booking2_tab)
          Select(?Sheet4,1)
          Unhide(?booking1_tab)
      End
    OF ?job:Telephone_Number
      
       temp_string = Clip(left(job:Telephone_Number))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:Telephone_Number = temp_string
       Display(?job:Telephone_Number)
    OF ?job:Fax_Number
      
       temp_string = Clip(left(job:Fax_Number))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:Fax_Number = temp_string
       Display(?job:Fax_Number)
    OF ?AmendAddresses
      ThisWindow.Update
      Show_Addresses
      ThisWindow.Reset
    OF ?job:Postcode_Collection
      If quickwindow{prop:acceptall} <> 1
      overwrite# = 1
      If job:address_line1_collection <> ''
          Case MessageEx('Overwrite exisiting address?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
              Of 2 ! &No Button
                  overwrite# = 0
          End!Case MessageEx
      End
      If overwrite# = 1
          Postcode_Routine (job:postcode_collection,job:address_line1_collection,job:address_line2_collection,job:address_line3_collection)
      End
      Display()
      Select(?job:address_line1_collection,1)
      End
    OF ?job:Telephone_Collection
      
       temp_string = Clip(left(job:Telephone_Collection))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:Telephone_Collection = temp_string
       Display(?job:Telephone_Collection)
    OF ?Collection_Text_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Collection_Text
      ThisWindow.Reset
    OF ?job:Postcode_Delivery
      If quickwindow{prop:acceptall} <> 1
      overwrite# = 1
      If job:address_line1_delivery <> ''
        Case MessageEx('Overwrite exisiting address?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
            Of 2 ! &No Button
              overwrite# = 0
        End!Case MessageEx
      End
      If overwrite# = 1
          Postcode_Routine (job:postcode_delivery,job:address_line1_delivery,job:address_line2_delivery,job:address_line3_delivery)
      End
      Display()
      Select(?job:address_line1_delivery,1)
      End
    OF ?job:Telephone_Delivery
      
       temp_string = Clip(left(job:Telephone_Delivery))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:Telephone_Delivery = temp_string
       Display(?job:Telephone_Delivery)
    OF ?Delivery_Text_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Delivery_Text
      ThisWindow.Reset
    OF ?Lookup_Engineer
      ThisWindow.Update
      Set(DEFAULT2)
      Access:DEFAULT2.Next()
      
      GlobalRequest = SelectRecord
      If de2:UserSkillLevel
          PickEngineersSkillLevel(jobe:SkillLevel)
      Else !If de2:UserSkillLevel
          Browse_Users_Job_Assignment
      End !If de2:UserSkillLevel
      
      Case GlobalResponse
          Of Requestcompleted
              job:Engineer = use:User_Code
              Do AddEngineer
              Select(?+2)
          Of Requestcancelled
      !        job:Engineer = ''
              Select(?-1)
      End!Case Globalreponse
      
      
      
    OF ?job:Repair_Type
      If ~0{prop:acceptall}
          PricingError#  = 0
          If job:Repair_Type <> Repair_Type_Temp
              If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
                  Case MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  PricingError# = 1
              End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
          End !If job:Repair_Type <> Repair_Type_Temp
          !Before Lookup
          If PricingError# = 0
              do_lookup# = 1
      
              If job:date_completed <> ''
                  If SecurityCheck('AMEND COMPLETED JOBS')
                      Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      do_lookup# = 0
                  End
              End
              If job:invoice_Number <> '' and do_lookup# = 1
                  If SecurityCheck('AMEND INVOICED JOBS')
                      Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      do_lookup# = 0
                  End
              End
      
              If job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
                  Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  do_lookup# = 0
              End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
      
              If do_lookup# = 1
      
                  IF job:Repair_Type
                      Access:REPAIRTY.ClearKey(rep:Model_Chargeable_Key)
                      rep:Model_Number = job:Model_Number
                      rep:Chargeable   = job:Chargeable_Job
                      rep:Repair_Type  = job:Repair_Type
                      If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                          !Found
                      Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          Post(Event:Accepted,?LookupChargeableChargeType)
      
                      End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                  End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      
                  If job:repair_type <> repair_type_temp
                      error# = 0
                      Case CheckPricing('C')
                          Of 1
                              error# = 1
                          Of 2
                              Case MessageEx('The selected Repair Type has a different pricing structure.<13,10><13,10>If you continue the job will be re-valued accordingly.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      error# = 1
                              End!Case MessageEx
                          Else
                              If repair_type_temp <> ''
                                  Case MessageEx('Are you sure you want to change the repair type?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                      Of 1 ! &Yes Button
                                      Of 2 ! &No Button
                                          error# = 1
                                  End!Case MessageEx
                              End!If repair_type_temp <> ''
                      End!Case CheckPricing('C')
      
                      If error# = 1
                          job:repair_type = repair_type_temp
                      Else!If job:date_completed <> ''
                          repair_type_temp = job:repair_type
                          Do pricing
      
                          Do third_party_repairer
                          Post(Event:accepted,?job:charge_type)
                      End!If job:date_completed <> ''
                  End!If job:repair_type <> repair_type_temp
                  Display()
              End !do_lookup# = 1
      
          End !If PricingError# = 0
      End !0{prop:acceptall}
    OF ?LookupChargeableChargeType
      ThisWindow.Update
      If job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
          Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
              Case MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, and Unit Type.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Else !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
              job:Repair_Type = PickRepairTypes(job:Model_Number,job:Account_Number,job:Charge_Type,job:Unit_Type,'C')
              If job:Repair_Type = ''
                  job:Repair_Type = Repair_Type_Temp
              End !job:Repair_Type = ''
              Post(Event:Accepted,?job:Repair_Type)
      
          End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Charge_Type,job:Repair_Type)
      End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
    OF ?Engineers_Notes_Button
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      EngineersText
      ThisWindow.Reset
      access:jobnotes.clearkey(JBN:RefNumberKey)
      jbn:refnumber   = job:ref_number
      If access:jobnotes.tryfetch(JBN:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:refnumber   = job:ref_number
              If access:jobnotes.tryinsert()
                  access:jobnotes.cancelautoinc()
              End!If access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End! If access:jobnotes.tryfetch(JBN:RefNumberKey) = Level:Benign
      If Records(glo:Q_Notes) <> ''
          Loop x$ = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x$)
              If tmp:engineernotes = ''
                  tmp:engineernotes = Clip(glo:notes_pointer)
              Else !If job:engineers_notes = ''
                  tmp:engineernotes = Clip(tmp:engineernotes) & '<13,10>' & Clip(glo:notes_pointer)
              End !If job:engineers_notes = ''
          End
          Display()
      End
    OF ?Button62
      ThisWindow.Update
      glo:select1 = 'ESN'
      glo:select2  = job:esn
      glo:select11 = job:ref_number
      glo:select12 = 'VIEW ONLY'
      Bouncer_History
      glo:select1 = ''
      glo:select2  = ''
      glo:select11 = ''
      glo:select12 = ''
    OF ?RemoveParts
      ThisWindow.Update
      RemovePartsAndInvoiceText()
      Do CountParts
      BRW12.ResetSort(1)
      BRW79.ResetSort(1)
    OF ?AllocateEngineer
      ThisWindow.Update
      Engineer"   = ''
      Engineer"   = InsertEngineerPassword()
      IF Engineer" <> ''
          job:Engineer = Engineer"
      
          Do AddEngineer
      
      End !Engineer" <>
    OF ?Common_Fault
      ThisWindow.Update
      !This is what should be happening with the parts:-
      !1. Check the Engineer's Stock Location
      !    If found, save sto:ref_number and sto:quantity_stock
      !2. Check Main Store
      !    If found, save sto:ref_number and sto:quantity_stock
      !
      !If engineer stock exists
      !    If sundry stock    
      !        Make part(engineer), quantity = engineer_stock        
      !    Else!    If sundry stock
      !        If quantity_requested > engineer_stock
      !            If engineer_stock = 0
      !                If main stock exists
      !                    If quantity_requested > main_stock
      !                        If main_stock = 0
      !                            Make pending part(engineer), quantity = quantity_requested
      !                        Else!If main_stock = 0
      !                            Take main_stock, set to zero
      !                            Make part(main stock), quantity = main_stock
      !                            Make pending part(engineer), quantity = quantity_request - main_stock
      !                        End!If main_stock = 0
      !                    Else!If par:quantity > main_stock
      !                        Make a part(main stock), quantity = quantity_requested
      !                        Take quantity from main_stock
      !                    End!If par:quantity > main_stock
      !                Else!                If main stock exists
      !                    Make pending part(engineer), quantity = quantity_requested                
      !                End!                If main stock exists
      !            Else!If engineer_stock = 0
      !                Take engineer_stock set to zero
      !                Make part(engineer), quantity = engineer_stock
      !                If main stock exists
      !                    If (quantity_requested - engineer_stock) > main_stock    
      !                        If main_stock = 0            
      !                            Make pending part(engineer), quantity = quantity_requested - engineer_stock                
      !                        Else!            If main_stock = 0
      !                            take main_stock, set to zero
      !                            Make part(main stock), quantity = main_stock
      !                            Make pending part(engineer), quantity = quantity_requested - (engineer_stock + main_stock)
      !                        End!            If main_stock = 0
      !                    Else!If quantity - engineer_Stock > main_stock
      !                        Make a part(stock), quantity =  (quantity_request - engineer_stock)
      !                        Take quantity from main_stock
      !                    End!If quantity - engineer_Stock > main_stock
      !                Else!                If main stock exists
      !                    Make pending part(engineer), quantity = quantity_requested - engineer_stock
      !                End!                If main stock exists
      !            End!If engineer_stock = 0
      !        Else!If par:quantity > engineer_stock
      !            Make a part(engineer), quantity = quantity_requested
      !            Take quantity from engineer_stock
      !        End!If par:quantity > engineer_stock
      !    End!    If sundry stock    
      !Else!If engineer stock exists
      !    If main stock exists
      !        If sundry stock
      !            Make part(stock), quantity = quantity_requested            
      !        Else!        If sundry stock
      !            If quantity_requested > main_stock    
      !                If main_stock = 0            
      !                    Make pending part(stock), quantity = quantity_requested
      !                Else!            If main_stock = 0
      !                    Take main_stock, set to zero
      !                    Make part(stock), quantity = main_stock
      !                    Make pending part(stock), quantity = quantity_requested - main_stock
      !                End!            If main_stock = 0
      !            Else!If quantity - engineer_Stock > main_stock
      !                Make a part(stock), quantity =  quantity_request
      !                Take quantity from main_stock
      !            End!If quantity - engineer_Stock > main_stock
      !        End!        If sundry stock
      !    Else!    If main stock exists
      !        Make a pending part(External), quantity = quantity requested
      !    End!    If main stock exists
      !End!If engineer stock exists
      
      If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
          Case MessageEx('Using Common Faults may ADD the following:<13,10>Parts, Invoice Text And Engineer''s Note<13,10><13,10>and OVERWRITE the following:<13,10>Job Types, Repair Types and Charge Types.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                         'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  saverequest#    = globalrequest
                  globalresponse  = requestcancelled
                  globalrequest   = selectrecord
                  glo:select1     = job:model_number
                  Browse_Common_Faults
                  glo:Select1     = ''
                  If globalresponse   = Requestcompleted
                      CommonFaults(com:Ref_Number)
                      If tmp:engineernotes = ''
                          tmp:engineernotes  = com:engineers_notes
                      Else!If tmp:engineersnotes = ''
                          tmp:engineernotes  = Clip(tmp:engineernotes) & '<13,10>' & com:engineers_notes
                      End!If tmp:engineersnotes = ''
                      If tmp:invoicetext = ''
                          tmp:invoicetext = com:invoice_Text
                      Else!If tmp:engineersnotes = ''
                          tmp:invoicetext  = Clip(tmp:invoicetext) & '<13,10>' & com:invoice_Text
                      End!If tmp:engineersnotes = ''
      
                  End!If globalresponse   = Requestcompleted
              Of 2 ! &No Button
          End!Case MessageEx
      End!If SecurityCheck('COMMON FAULTS - APPLY TO JOBS')
      
      
      !Include('commonfa.inc')
      !If fault_added# = 1
      !End!If fault_added# = 1
      
      Do show_hide_tabs
    OF ?Button59
      ThisWindow.Update
      BrowseEngineersOnJob(job:Ref_Number)
      ThisWindow.Reset
    OF ?job:Repair_Type_Warranty
      if ~0{prop:acceptall}
          PricingError# = 0
          If job:Repair_Type_Warranty <> Warranty_Repair_Type_Temp
              If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Warranty_Charge_type,job:Repair_Type_Warranty)
                  Case MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type, Unit Type and Repair Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  PricingError# = 1
              End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Warranty_Charge_type,job:Repair_Type_Warranty)
          End !If job:Repair_Type_Warranty <> Warranty_Repair_Type_Temp
          If PricingError# = 0
              do_lookup# = 1
      
              If job:date_completed <> ''
                  If SecurityCheck('AMEND COMPLETED JOBS')
                      Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      do_lookup# = 0
                  End
              End
              If job:invoice_Number <> '' and do_lookup# = 1
                  If SecurityCheck('AMEND INVOICED JOBS')
                      Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      do_lookup# = 0
                  End
              End
      
              If job:Model_Number = '' Or job:Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
                  Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  do_lookup# = 0
              End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
      
              If do_lookup# = 1
      
                  IF job:Repair_Type_Warranty
                      Access:REPAIRTY.ClearKey(rep:Model_Chargeable_Key)
                      rep:Model_Number = job:Model_Number
                      rep:Warranty     = job:Warranty_Job
                      rep:Repair_Type  = job:Repair_Type_Warranty
                      If Access:REPAIRTY.TryFetch(rep:Model_Warranty_Key) = Level:Benign
                          !Found
                          
                      Else!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          Post(Event:Accepted,?LookupWarrantyChargeType)
      
                      End!If Access:REPAIRTY.TryFetch(rep:Model_Chargeable_Key) = Level:Benign
                  End!IF job:Repair_Type OR ?job:Repair_Type{Prop:Req}
      
                  If job:Repair_Type_Warranty <> Warranty_Repair_Type_Temp
                      error# = 0
                      Case CheckPricing('W')
                          Of 1
                              error# = 1
                          Of 2
                              Case MessageEx('The selected Repair Type has a different pricing structure.<13,10><13,10>If you continue the job will be re-valued accordingly.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      error# = 1
                              End!Case MessageEx
                          Else
                              If Warranty_Repair_Type_Temp <> ''
                                  Case MessageEx('Are you sure you want to change the repair type?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                      Of 1 ! &Yes Button
                                      Of 2 ! &No Button
                                          error# = 1
                                  End!Case MessageEx
                              End!If repair_type_temp <> ''
                      End!Case CheckPricing('C')
      
                      If error# = 1
                          job:repair_type_Warranty = Warranty_repair_type_temp
                      Else!If job:date_completed <> ''
                          Warranty_repair_type_temp = job:repair_type_Warranty
                          Do pricing
      
                          Do third_party_repairer
                          Post(Event:accepted,?job:Warranty_Charge_Type)
                      End!If job:date_completed <> ''
                  End!If job:repair_type <> repair_type_temp
                  Display()
              End !do_lookup# = 1
      
          End !If PricingError# = 0
      End !0{prop:acceptall}
    OF ?LookupWarrantyRepairType
      ThisWindow.Update
      If job:Model_Number = '' Or job:Warranty_Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          Case MessageEx('Error! You cannot select a Repair Type until you have selected a Model Number, Charge Type, Account Number and Unit Type.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_lookup# = 0
      Else !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''
          If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Warranty_Charge_Type,job:Repair_Type_Warranty)
              Case MessageEx('Error! A pricing structure has not been setup up for this combination of Account, Model Number, Charge Type and Unit Type.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Else !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Warranty_Charge_Type,job:Repair_Type_Warranty)
              job:Repair_Type_Warranty = PickRepairTypes(job:Model_Number,job:Account_Number,job:Warranty_Charge_Type,job:Unit_Type,'W')
              If job:Repair_Type_Warranty = ''
                  job:Repair_Type = Warranty_Repair_Type_Temp
              End !job:Repair_Type = ''
              Post(Event:Accepted,?job:Repair_Type_Warranty)
      
          End !If PricingStructure(job:Account_Number,job:Model_Number,job:Unit_Type,job:Warranty_Charge_Type,job:Repair_Type_Warranty)
      End !job:Model_Number = '' Or job:Charge_Type = '' Or job:Account_Number = '' Or job:Unit_Type = ''    
      
    OF ?Today_Button
      ThisWindow.Update
      ! Start Change 2838 BE(24/07/03)  ACCESS RIGHTS SECURITY CHECK
      IF ((job:date_completed <> '') AND SecurityCheck('JOBS - NO RECOMPLETE') = Level:Benign) THEN
          MessageEx('You do not have access to this option.','ServiceBase 2000',|
                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
      ! Start Change 4217 BE(29/04/04)
!      ! Start Change 3922 BE(27/02/04)
!      ELSIF ((job:Chargeable_Job = 'YES') AND |
!             (job:Estimate = 'YES') AND |
!             ((total_estimate_temp = 0) OR (total_estimate_temp > job:estimate_If_Over)) AND |
!             (job:Estimate_Accepted <> 'YES')) THEN
!          MessageEx('This job cannot be completed. An estimate is required.','ServiceBase 2000',|
!                    'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
!                    beep:systemhand,msgex:samewidths,84,26,0)
!      ! End Change 3922 BE(27/02/04)
      ELSIF ((job:Chargeable_Job = 'YES') AND |
             (job:Estimate = 'YES') AND |
             ((total_estimate_temp = 0) OR (total_estimate_temp > job:estimate_If_Over)) AND |
             (job:Estimate_Accepted = 'NO') AND (job:Estimate_Rejected = 'NO')) THEN
          MessageEx('This job cannot be completed. An estimate is required.','ServiceBase 2000',|
                    'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
      ! Start Change 4217 BE(29/04/04)
      ELSE
      ! End Change 2838 BE(24/07/03)
    Case MessageEx('You are about to complete this job.<13,10><13,10>(Warning! You will not be able to uncomplete it if you continue.)<13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button

            Set(DEFAULTS)
            Access:DEFAULTS.Next()
            glo:errortext = ''
            Complete# = 0
            If job:date_completed = ''
                !Check to see that all the required fields are filled in
                CompulsoryFieldCheck('C')
                If glo:ErrorText <> ''
                    glo:errortext = 'You cannot complete this job due to the following error(s): <13,10>' & Clip(glo:errortext)
                    Error_Text
                    glo:errortext = ''
                Else!If glo:ErrorText <> ''
                    If GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                        If LocalValidateAccessories() = Level:Benign
                            Complete# = 1
                        Else !If LocalValidateAccessories() = Level:Benign
                            Access:JOBS.Update()
                        End !If LocalValidateAccessories() = Level:Benign
                    Else
                        Complete# = 1
                    End !If GETINI('VALIDATE','ForceAccCheckComp',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                End!If glo:ErrorText <> ''
            Else!If job:date_completed = ''
                Case MessageEx('Are you sure you want to change the Completion Date?','ServiceBase 2000',|
                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                    Of 1 ! &Yes Button
                        Complete# = 1
                    Of 2 ! &No Button
                End!Case MessageEx
            End!If job:date_completed = ''

            If Complete# = 1
                !**** TH 02/04 Time to deal with deferred IMEI changes
                !if sts:PickNoteEnable then
                ! Start Change xxxx BE(18/03/04)
                IF (jobe:Pre_RF_Board_IMEI = '') THEN
                ! End Change xxxx BE(18/03/04)
                    partpntstore = access:parts.savefile() !backup current pointer
                    warpartpntstore = access:warparts.savefile() !backup current pointer

                    access:PARTS.clearkey(par:Part_Number_Key) !check for existing picking record
                    par:Ref_Number = job:ref_number
                    set(par:Part_Number_Key,par:Part_Number_Key)
                    loop
                        if access:PARTS.next() then break. !no records
                        ! Start Change xxxx BE(18/03/04)
                        IF (par:Ref_Number <> job:ref_number) THEN
                            BREAK
                        END
                        ! End Change xxxx BE(18/03/04)
                        !if rf_board then new imei
                        UpdateRFBoardIMEI(par:part_ref_number,par:part_number)
                    end

                    access:WARPARTS.clearkey(wpr:Part_Number_Key) !check for existing picking record
                    wpr:Ref_Number = job:ref_number
                    set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                    loop
                        if access:WARPARTS.next() then break. !no records
                        ! Start Change xxxx BE(18/03/04)
                        IF (wpr:Ref_Number <> job:ref_number) THEN
                            BREAK
                        END
                        ! End Change xxxx BE(18/03/04)
                        !if rf_board then new imei
                        UpdateRFBoardIMEI(wpr:part_ref_number,wpr:part_number)
                    end

                    access:parts.restorefile(partpntstore)
                    access:warparts.restorefile(warpartpntstore)
                ! Start Change xxxx BE(18/03/04)
                END
                ! End Change xxxx BE(18/03/04)
                !end !if sts:PickNoteEnable
                !**** TH 02/04
                Print_Despatch_Note_Temp = ''
                !Fill in the totals with the right costs from the
                !Price Structure.
                Do Pricing
                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                man:Manufacturer = job:Manufacturer
                If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                    !Found
                    If man:QAAtCompletion
                        !If QA at completion is ticked, then don't actually
                        !complete the job. That is done at QA
                        GetStatus(605,1,'JOB')
                        Case MessageEx('The Repair has been completed. You must now QA this unit.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        job:On_Test = 'YES'
                        job:Date_On_Test = Today()
                        job:Time_On_Test = Clock()
                        ! Start Change 3879 BE(26/02/2004)
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                            ! Start Change 4053 BE(19/03/04)
                            !jobe:CompleteRepairType = 1
                            !jobe:CompleteRepairDate = TODAY()
                            !jobe:CompleteRepairTime = CLOCK()
                            !access:jobse.update()
                            ! Start Change 4200 BE(04/05/04)
                            !IF (jobe:CompleteRepairDate = '') THEN
                            !    jobe:CompleteRepairType = 1
                            !    jobe:CompleteRepairDate = TODAY()
                            !    jobe:CompleteRepairTime = CLOCK()
                            !    access:jobse.update()
                            !END
                            access:JOBSENG.clearkey(joe:JobNumberKey)
                            joe:JobNumber = job:Ref_Number
                            SET(joe:JobNumberKey, joe:JobNumberKey)
                            IF (access:JOBSENG.next() = Level:Benign) THEN
                                IF ((jobe:CompleteRepairDate = '') OR |
                                    (joe:DateAllocated > jobe:CompleteRepairDate) OR |
                                    ((joe:DateAllocated = jobe:CompleteRepairDate) AND |
                                     (joe:TimeAllocated > jobe:CompleteRepairTime))) THEN
                                    jobe:CompleteRepairType = 1
                                    jobe:CompleteRepairDate = TODAY()
                                    jobe:CompleteRepairTime = CLOCK()
                                    access:jobse.update()
                                END
                            Else !IF (access:JOBSENG.next() = Level:Benign) THEN
                                !Start - Oop!. Forget to allow for there being no engineer history (DBH: 21-03-2005)
                                If jobe:CompleteRepairDate = ''
                                    jobe:CompleteRepairType = 1
                                    jobe:CompleteRepairDate = TODAY()
                                    jobe:CompleteRepairTime = CLOCK()
                                    access:jobse.update()
                                End ! If jobe:CompleteRepairDate = ''
                                !End   - Oop!. Forget to allow for there being no engineer history (DBH: 21-03-2005)
                            END !IF (access:JOBSENG.next() = Level:Benign) THEN
                            ! End Change 4200 BE(04/05/04)
                            ! End Change 4053 BE(19/03/04)

                        END
                        ! End Change 3879 BE(26/02/2004)

                        Post(Event:Accepted,?OK)
                    Else !If man:QAAtCompletion
                        !To avoid not changing to the right status,
                        !change to completed, first, and then it should
                        !Change again to one of the below status's if necessary
                        GetStatus(705,1,'JOB')
                        If ExchangeAccount(job:Account_Number)
                            !Is the Trade Account used for Exchange Units
                            !if so, then there's no need to despatch the
                            !unit normal. Just auto fill the despatch fields
                            !and mark the job as to return to exchange stock.
                            ForceDespatch
                            GetStatus(707,1,'JOB')
                        Else !If ExchangeAccount(job:Account_Number)

                            !Will the job be restocked, i.e. has, or will have, an
                            !Exchange Unit attached, or is it a normal despatch.
?   Message('Not an Exchange Account','Debug Message', Icon:Hand)
                            restock# = 0
                            If ExchangeAccount(job:Account_Number)
                                restock# = 1
                            Else !If ExchangeAccount(job:Account_Number)
                                glo:select1  = job:ref_number
                                If ToBeExchanged()
                                !Has this unit got an Exchange, or is it expecting an exchange
?   Message('To Be Exchanged','Debug Message', Icon:Hand)
                                    restock# = 1
                                End!If job:exchange_unit_number <> ''
                                !If there is an loan unit attached, assume that a despatch note is required.
                                If job:loan_unit_number <> ''
                                    restock# = 0
                                End!If job:loan_unit_number <> ''
                            End !If ExchangeAccount(job:Account_Number)

                            If CompletePrintDespatch(job:Account_Number) = Level:Benign
                                Print_Despatch_Note_Temp = 'YES'
                            End !CompletePrintDespatch(job:Account_Number) = Level:Benign

                            If restock# = 0

                                !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
                                !If either of those are ticked then don't despatch yet
                                !print_despatch_note_temp = ''

                                If AccountAllowedToDespatch()
                                    !Do we need to despatch at completion?
                                    If DespatchAtClosing()
                                        !Does the Trade Account have "Skip Despatch" set?
                                        If AccountSkipDespatch(job:Account_Number) = Level:Benign
                                            access:courier.clearkey(cou:courier_key)
                                            cou:courier = job:courier
                                            if access:courier.tryfetch(cou:courier_key)
                                                Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
                                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                    Of 1 ! &OK Button
                                                End!Case MessageEx
                                            Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                                !Give the job a unique Despatch Number
                                                !then call the despsing.inc.
                                                !This calls the relevant exports etc, and different
                                                !despatch procedures depending on the Courier.
                                                !It will then print a Despatch Note and change
                                                !The status as required.
                                                If access:desbatch.primerecord() = Level:Benign
                                                    If access:desbatch.tryinsert()
                                                        access:desbatch.cancelautoinc()
                                                    Else!If access:despatch.tryinsert()
                                                        DespatchSingle()
                                                        Print_Despatch_Note_Temp = ''
                                                    End!If access:despatch.tryinsert()
                                                End!If access:desbatch.primerecord() = Level:Benign
                                            End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                        Else
                                            GetStatus(705,1,'JOB')
                                        End !If AccountSkipDespatch = Level:Benign
                                    Else !If DespatchAtClosing()
                                        If job:Loan_Unit_Number <> ''
                                            GetStatus(811,1,'JOB')
                                        Else
                                            GetStatus(705,1,'JOB')
                                        End !If job:Loan_Unit_Number <> ''
                                    End !If DespatchAtClosing()
                                Else !If AccountAllowedToDespatch
                                    GetStatus(705,1,'JOB')
                                End !If AccountAllowedToDespatch
                            Else !If restock# = 0
                                If job:Exchange_Unit_Number <> ''
                                    !Are either of the Chargeable or Warranty repair types BERs?
                                    !If so, then don't change the status to 707 because it shouldn't
                                    !be returned to the Exchange Stock.
                                    Comp# = 0
                                    If job:Chargeable_Job = 'YES'
                                        If BERRepairType(job:Repair_Type) = Level:Benign
                                             Comp# = 1
                                        End !If BERRepairType(job:Repair_Type) = Level:Benign
                                    End !If job:Chageable_Job = 'YES'
                                    If job:Warranty_Job = 'YES' and Comp# = 0
                                        If BERRepairType(job:Repair_Type_Warranty) = Level:Benign
                                            Comp# = 1
                                        End !If BERRepairType(job:Repair_Type) = Level:Benign
                                    End !If job:Warranty_Job = 'YES'
                                    If Comp# = 1
                                        Case MessageEx('This unit has been exchanged but is a B.E.R. Repair. '&|
                                          '<13,10>DO NOT place this unit into Exchange Stock.','ServiceBase 2000',|
                                                       'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                        GetStatus(705,1,'JOB')
                                        Print_Despatch_Note_Temp = 'NO'
                                    Else !If Comp# = 1
                                        Case MessageEx('This unit has been exchanged. It should now be placed into Exchange Stock.','ServiceBase 2000',|
                                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                        GetStatus(707,1,'JOB')
                                    End !If Comp# = 1
                                Else !If job:Exchange_Unit_Number <> ''
                                    GetStatus(705,1,'JOB')
                                End !If job:Exchange_Unit_Number <> ''    
                            End !If restock# = 0

                            !Does the Account care about Bouncers?
                            Check_For_Bouncers_Temp = 1
                            If CompleteCheckForBouncer(job:Account_Number)
                                Check_For_Bouncers_Temp = 0
                            End !CompleteCheckForBouncer(job:Account_Number) = Level:Benign

                        End !If ExchangeAccount(job:Account_Number)

                        !Fill in the relevant fields

                        saved_ref_number_temp   = job:ref_number
                        saved_esn_temp  = job:esn
                        saved_msn_temp  = job:msn
                        job:date_completed = Today()
                        job:time_completed = Clock()
                        date_completed_temp = job:date_completed
                        time_completed_temp = job:time_completed
                        job:completed = 'YES'
                        ! Start Change 3879 BE(26/02/2004)
                        Access:JOBSE.ClearKey(jobe:RefNumberKey)
                        jobe:RefNumber = job:Ref_number
                        IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                            IF (jobe:CompleteRepairDate = '') THEN
                                jobe:CompleteRepairType = 2
                                jobe:CompleteRepairDate = TODAY()
                                jobe:CompleteRepairTime = CLOCK()
                                access:jobse.update()
                            END
                        Else
                            !Start - Oop!. Forget to allow for there being no engineer history (DBH: 04-08-2005)
                            If jobe:CompleteRepairDate = ''
                                jobe:CompleteRepairType = 2
                                jobe:CompleteRepairDate = Today()
                                jobe:CompleteRepairTime = Clock()
                                Access:JOBSE.Update()
                            End ! If jobe:CompleteRepairDate = ''

                        END
                        ! End Change 3879 BE(26/02/2004)

                        If Check_For_Bouncers_Temp
                            !Check if the job is a Bouncer and add
                            !it to the Bouncer List
                            If CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
                                !Bouncer
                                AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                            Else !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

                            End !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

                        End !If Check_For_Bouncers_Temp

                        !Check if this job is an EDI job
                        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                        mod:Model_Number    = job:Model_Number
                        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                            !Found
                            job:edi = PendingJob(mod:Manufacturer)
                            job:Manufacturer    = mod:Manufacturer
                        Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                        End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

                        !Mark the job's exchange unit as being available
                        CompleteDoExchange(job:Exchange_Unit_Number)

                        Do QA_Group
                        Access:JOBS.TryUpdate()

                        If ExchangeAccount(job:Account_Number)
                            Case MessageEx('This is an Exchange Repair. Please re-stock the unit.','ServiceBase 2000',|
                                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                        End !If ExchangeAccount(job:Account_Number)

                        If print_despatch_note_temp = 'YES'
                            If restock# = 1
                                restocking_note
                            Else!If restock# = 1
                                despatch_note
                            End!If restock# = 1
                            glo:select1  = ''
                        End!If print_despatch_note_temp = 'YES'
                        Post(Event:Accepted,?OK)
                    End !If man:QAAtCompletion
                Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

            End !Complete# = 1
            Do Time_Remaining
            Do skip_Despatch

            brw21.resetsort(1)

        Of 2 ! &No Button
    End!Case MessageEx
    Display()
      ! Start Change 2838 BE(24/07/03)  ACCESS RIGHTS SECURITY CHECK
      END
      ! End Change 2838 BE(24/07/03)
    OF ?WarrantyPartsTransfer:2
      ThisWindow.Update
      If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('Are you sure you want to transfer all your Chargeable Parts to Warranty Parts.','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  count# = 0
                  setcursor(cursor:wait)
                  save_par_id = access:parts.savefile()
                  access:parts.clearkey(par:part_number_key)
                  par:ref_number  = job:ref_number
                  set(par:part_number_key,par:part_number_key)
                  loop
                      if access:parts.next()
                         break
                      end !if
                      if par:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      If par:pending_ref_number <> ''
                          access:ordpend.clearkey(ope:ref_number_key)
                          ope:ref_number = par:pending_ref_number
                          if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                              ope:part_type = 'JOB'
                              access:ordpend.update()
                          End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                      End!If wpr:pending_ref_number <> ''
                      If par:order_number <> ''
                          access:ordparts.clearkey(orp:record_number_key)
                          orp:record_number   = par:order_number
                          If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                              orp:part_type = 'JOB'
                              access:ordparts.update()
                          End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                      End!If wpr:order_number <> ''
                      get(warparts,0)
                      if access:warparts.primerecord() = Level:Benign
                          record_number$  = wpr:record_number
                          wpr:record      :=: par:record
                          wpr:record_number   = record_number$
                          if access:warparts.insert()
                              access:warparts.cancelautoinc()
                          End!if access:warparts.insert()
                          if def:PickNoteNormal or def:PickNoteMainStore then TransferParts('chargeable'). !transfer from chargeable
                          !**** here Transfer on picking
                      End!if access:warparts.primerecord() = Level:Benign
                      Delete(parts)
                      count# += 1
                  end !loop
                  access:parts.restorefile(save_par_id)
                  setcursor()
                  if count# <> 0
      
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          aud:type          = 'JOB'
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'CHARGEABLE PARTS TRANSFERRED TO WARRANTY'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                      If job:warranty_job <> 'YES'
                          job:Warranty_Charge_Type = ''
                          Case MessageEx('This job has not been marked as Warranty.<13,10><13,10>Do you wish to make this a Split Job, or make it a Warranty Only Job?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Make Split Job|&Make Warranty',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                              Of 1 ! &Make Split Job Button
                                  job:Warranty_job = 'YES'
                                  warranty_job_temp = job:Warranty_job
                                  Post(event:accepted,?job:Warranty_job)
                                  
                              Of 2 ! &Make Chargeable Button
                                  job:chargeable_job = 'NO'
                                  job:warranty_job = 'YES'
                                  warranty_job_temp = job:warranty_job
                                  chargeable_job_temp = job:chargeable_job
                                  Post(event:accepted,?job:chargeable_job)
                                  Post(event:accepted,?job:warranty_job)
                                  
                          End!Case MessageEx
                      Else!If job:chargeable_job <> 'YES'
                          Case MessageEx('Transfer Complete.','ServiceBase 2000',|
                                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      End!If job:warranty_job <> 'YES'
                  End!if count# <> 0
                  Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                  mod:Model_Number    = job:Model_NUmber
                  If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Found
                      job:EDI = PendingJob(mod:Manufacturer)
                      job:Manufacturer    = mod:Manufacturer
                  Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                  ! Start Change 2116 BE(26/06/03)
                  DO CountParts
                  ! End Change 2116 BE(26/06/03)
              Of 2 ! &No Button
          End!Case MessageEx
      End!If SecurityCheck('JOBS - TRANSFER PARTS')
      BRW12.ResetSort(1)
      BRW79.ResetSort(1)
    OF ?CreatePickNote
      ThisWindow.Update
      ! Start Change 4164 BE(26/04/04)
      access:jobs.update()
      ! End Change 4164 BE(26/04/04)
      PickValidate
      if job:Estimate = 'YES' and (job:Estimate <> estimate_temp) then
          Post(Event:accepted,?job:estimate)
      end
    OF ?ChargeableAdjustment
      ThisWindow.Update
      Include('genjobs.inc','ChargeableAdjustment')
      BRW12.ResetSort(1)
    OF ?CreatePickNote:2
      ThisWindow.Update
      ! Start Change 4164 BE(26/04/04)
      access:jobs.update()
      ! End Change 4164 BE(26/04/04)
      PickValidate
      if job:Estimate = 'YES' and (job:Estimate <> estimate_temp) then
          Post(Event:accepted,?job:estimate)
      end
    OF ?WarrantyPartsTransfer
      ThisWindow.Update
      If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('Are you sure you want to transfer all your Warranty Parts to Chargeable Parts.','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  count# = 0
                  setcursor(cursor:wait)
                  save_wpr_id = access:warparts.savefile()
                  access:warparts.clearkey(wpr:part_number_key)
                  wpr:ref_number  = job:ref_number
                  set(wpr:part_number_key,wpr:part_number_key)
                  loop
                      if access:warparts.next()
                         break
                      end !if
                      if wpr:ref_number  <> job:ref_number      |
                          then break.  ! end if
                      If wpr:pending_ref_number <> ''
                          access:ordpend.clearkey(ope:ref_number_key)
                          ope:ref_number = wpr:pending_ref_number
                          if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                              ope:part_type = 'JOB'
                              access:ordpend.update()
                          End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                      End!If wpr:pending_ref_number <> ''
                      If wpr:order_number <> ''
                          access:ordparts.clearkey(orp:record_number_key)
                          orp:record_number   = wpr:order_number
                          If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                              orp:part_type = 'JOB'
                              access:ordparts.update()
                          End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                      End!If wpr:order_number <> ''
                      get(parts,0)
                      if access:parts.primerecord() = Level:Benign
                          record_number$  = par:record_number
                          par:record      :=: wpr:record
                          par:record_number   = record_number$
                          if access:parts.insert()
                              access:parts.cancelautoinc()
                          End!if access:parts.insert()
                          if def:PickNoteNormal or def:PickNoteMainStore then TransferParts('warranty').
                      End!if access:parts.primerecord() = Level:Benign
                      Delete(warparts)
                      count# += 1
                  end !loop
                  access:warparts.restorefile(save_wpr_id)
                  setcursor()
                  if count# <> 0
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          aud:type          = 'JOB'
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'WARRANTY PARTS TRANSFERRED TO CHARGEABLE'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
      
                      If job:chargeable_job <> 'YES'
                          Case MessageEx('This job has not been marked as Chargeable.<13,10><13,10>Do you wish to make this a Split Job, or make it a Chargeable Only Job?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Make Split Job|&Make Chargeable',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Make Split Job Button
                                  job:chargeable_job = 'YES'
                                  chargeable_job_temp = job:chargeable_job
                                  Post(event:accepted,?job:chargeable_job)
                              Of 2 ! &Make Chargeable Button
                                  job:warranty_job = 'NO'
                                  job:chargeable_job = 'YES'
                                  warranty_job_temp = job:warranty_job
                                  chargeable_job_temp = job:chargeable_job
                                  Post(event:accepted,?job:chargeable_job)
                                  Post(event:accepted,?job:warranty_job)
                          End!Case MessageEx
                      Else!If job:chargeable_job <> 'YES'
                          Case MessageEx('Transfer Complete.','ServiceBase 2000',|
                                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      End!If job:chargeable_job <> 'YES'
      
                  End!if count# <> 0
                  Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                  mod:Model_Number    = job:Model_NUmber
                  If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Found
                      job:EDI = PendingJob(mod:Manufacturer)
                      job:Manufacturer    = mod:Manufacturer
                  Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                  ! Start Change 2116 BE(26/06/03)
                  DO CountParts
                  ! End Change 2116 BE(26/06/03)
              Of 2 ! &No Button
          End!Case MessageEx
      End!If SecurityCheck('JOBS - TRANSFER PARTS')
      BRW12.ResetSort(1)
      BRW79.ResetSort(1)
    OF ?WarrantyAdjustment
      ThisWindow.Update
      Include('genjobs.inc','WarrantyAdjustment')
      BRW79.ResetSort(1)
    OF ?job:Courier
      If ~0{prop:acceptall}
      access:courier.clearkey(cou:courier_key)
      cou:courier = job:courier
      if access:courier.fetch(cou:courier_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_courier
          if globalresponse = requestcompleted
              job:courier = cou:courier
          else
              job:courier = courier_temp
              select(?-1)
          end
          display(?job:courier)
          globalrequest     = saverequest#
      end!if access:courier.fetch(cou:courier_key)
      If job:courier <> courier_temp
          If courier_temp <> ''
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If job:invoice_Number <> ''
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If error# = 1
                  job:courier = courier_temp
              Else!If error# = 1
                  Case MessageEx('Are you sure you want to change the Courier?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          If thiswindow.request <> Insertrecord
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'NEW COURIER: ' & CLip(job:courier) & '<13,10>OLD COURIER: ' & CLip(courier_temp)
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'JOB COURIER CHANGED: ' & Clip(job:courier)
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
                          End!If thiswindow.request <> Insertrecord
                          courier_temp = job:courier
                          If job:despatch_type = 'JOB'
                              job:current_courier = job:courier
                          End
                      Of 2 ! &No Button
                          job:courier = courier_temp
                  End!Case MessageEx
              End!If error# = 1
          Else!If courier_temp <> ''
              If job:despatch_type = 'JOB'
                  job:current_courier = job:courier
                  courier_Temp = job:courier
              End
          End!If courier_temp <> ''
      
      End!If job:courier <> courier_temp
      Display(?job:courier)
      End!If ~0{prop:acceptall}
    OF ?job:Incoming_Courier
      Do UPS_Service   
    OF ?Invoice_Text_Buton
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      InvoiceText
      ThisWindow.Reset
      Display()
    OF ?CRCDespatchDateButton
      ThisWindow.Update
      CRCDespatchDates
      ThisWindow.Reset
    OF ?job:Consignment_Number
      If ~0{prop:acceptall}
          If job:consignment_number <> consignment_number_temp
              Case MessageEx('Are you sure you want to change the Consignment Number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW CONSIGNMENT NO: ' & Clip(job:consignment_number) & |
                                              '<13,10>PREVIOUS CONSIGNMENT NO: ' & Clip(consignment_number_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'CONSIGNMENT NO CHANGED'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      consignment_number_temp    = job:consignment_number
                  Of 2 ! &No Button
                      job:consignment_number     = consignment_number_temp
              End!Case MessageEx
          End!If job:exchange_consignment_number <> exchange_consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?despatched_Calendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Date_Despatched = TINCALENDARStyle1(job:Date_Despatched)
          Display(?JOB:Date_Despatched)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?job:Date_Despatched
      If ~0{prop:acceptall}
          If job:date_despatched <> date_despatched_temp
              Case MessageEx('Are you sure you want to change the Despatched Date?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW DESPATCHED DATE: ' & Clip(job:date_despatched) & |
                                              '<13,10>PREVIOUS DESPATCHED DATE: ' & Clip(date_despatched_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'DESPATCHED DATE CHANGED'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      date_despatched_temp    = job:date_despatched
                  Of 2 ! &No Button
                      job:date_despatched     = date_despatched_temp
              End!Case MessageEx
          End!If job:exchange_consignment_number <> exchange_consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?job:Despatch_User
      If ~0{prop:acceptall}
          If job:despatch_user <> despatch_user_temp
              Case MessageEx('Are you sure you want to change the Despatch User?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW USER: ' & Clip(job:despatch_user) & |
                                              '<13,10>PREVIOUS USER: ' & Clip(despatch_user_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'DESPATCHED USER CHANGED'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      despatch_user_temp  = job:despatch_user
                  Of 2 ! &No Button
                      job:despatch_user    = despatch_user_temp
              End!Case MessageEx
          End!If job:consignment_number <> consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?job:Despatch_Number
      If ~0{prop:acceptall}
          If job:despatch_number <> despatch_number_temp
              Case MessageEx('Are you sure you want to change the Despatch Number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW DESPATCH NO: ' & Clip(job:despatch_number) & |
                                              '<13,10>PREVIOUS DESPATCH NO: ' & Clip(despatch_number_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'DESPATCH NO CHANGED'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      despatch_number_temp    = job:despatch_number
                  Of 2 ! &No Button
                      job:despatch_number     = despatch_number_temp
              End!Case MessageEx
          End!If job:consignment_number <> consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?Amend_Despatch:3
      ThisWindow.Update
      Case MessageEx('Are you sure you want to amend the Despatch Details?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
          Unhide(?despatched_calendar:3)
          ?job:consignment_number{prop:readonly} = 0
          ?job:consignment_number{prop:color} = color:yellow
          ?job:despatch_user{prop:readonly} = 0
          ?job:despatch_user{prop:color} = color:yellow
          ?job:date_despatched{prop:readonly} = 0
          ?job:date_despatched{prop:color} = color:yellow
          ?job:despatch_number{prop:readonly} = 0
          ?job:despatch_number{prop:color} = color:yellow
      
        Of 2 ! &No Button
      End!Case MessageEx
    OF ?Skip_Despatch_Validation
      ThisWindow.Update
      error# = 0
      If job:despatch_type = 'LOA'
          error# = 1
          Case MessageEx('The Loan Unit attached to this job is awaiting despatch.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End!If job:despatch_type = 'LOA'
      If job:despatch_type = 'EXC'
          error# = 1
          Case MessageEx('The Exchange Unit attached to this job is awaiting despatch.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End!If job:despatch_type = 'LOA'
      
      If error# = 0
          beep(beep:systemquestion)  ;  yield()
          case message('This will skip the Despatch Validation and force this job '&|
                  'to be marked ''Ready For Despatch''.'&|
                  '||Are you sure you want to continue?', |
                  'ServiceBase 2000', icon:question, |
                   button:yes+button:no, button:no, 0)
          of button:yes
              job:despatch_type = 'JOB'
              job:despatched = 'REA'
              job:current_courier = job:courier
          of button:no
      
          end !case
      End!If error# = 1
    OF ?Lookup_Loan_Unit
      ThisWindow.Update
      !Security Check
      Error# = 0
      If job:Loan_Unit_Number <> ''
          If SecurityCheck('JOBS - AMEND LOAN UNIT')
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Error# = 1
          End !If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
      Else !If job:Exchange_Unit_Number <> ''
          If SecurityCheck('JOBS - ADD LOAN UNIT')
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Error# = 1
          End !If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
      End !If job:Exchange_Unit_Number <> ''
      
      If Error# = 0
          glo:select12 = job:Model_number
          Set(defaults)
          access:defaults.next()
          error# = 0
      
          If job:exchange_unit_number <> ''
              Case MessageEx('Cannot attached a Loan Unit. An Exchange Unit has been attached to this job.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!If job:exchange_unit_number <> ''
          remove# = 0
          If job:loan_consignment_number <> ''
              Case MessageEx('Warning! The Loan Unit for this job has already been despatched.<13,10><13,10>If you continue the existing unit on this job will be removed and returned to Exchange Stock.<13,10><13,10>(An Entry will be made in the Audit Trail)<13,10><13,10>Are you sure you want to remove the existing Loan Unit?','ServiceBase 2000',|
                             'Styles\warn.ico','|&Remove Unit|&Cancel',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Remove Unit Button
                      remove# = 1
                  Of 2 ! &Cancel Button
                      error# = 1
              End!Case MessageEx
          End!If job:loan_consingment_number <> ''
      
          If error# = 0
              access:subtracc.clearkey(sub:account_number_key)
              sub:account_number = job:account_number
              access:subtracc.fetch(sub:account_number_key)
              access:tradeacc.clearkey(tra:account_number_key) 
              tra:account_number = sub:main_account_number
              access:tradeacc.fetch(tra:account_number_key)
              If tra:Ignoredespatch = 'YES' and error# = 0
                  Case MessageEx('The selected Trade Account has been marked as ''Skip Despatch''.<13,10><13,10>A Loan Unit cannot be issued.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If tra:skip_despatch = 'YES'
          End!If error# = 0
      
          If error# = 0
              do_loan# = 1
              do_return# = 0
              old_loan_number# = job:loan_unit_number
              If job:loan_unit_number <> ''
                  do_return# = 1
                  glo:select2  = 'L'
                  glo:select1 = ''
                  Loan_Changing_Screen
      
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      If remove# = 1
                          aud:notes         = 'UNIT HAD BEEN DESPATCHED: ' & |
                                              '<13,10>UNIT NUMBER: ' & Clip(job:Loan_unit_number) & |
                                              '<13,10>COURIER: ' & CLip(job:loan_Courier) & |
                                              '<13,10>CONSIGNMENT NUMBER: ' & CLip(job:loan_consignment_number) & |
                                              '<13,10>DATE DESPATCHED: ' & Clip(Format(job:loan_despatched,@d6)) &|
                                              '<13,10>DESPATCH USER: ' & CLip(job:loan_despatched_user) &|
                                              '<13,10>DESPATCH NUMBER: ' & CLip(job:loan_despatch_number)
                      Else!If remove# = 1
                          aud:notes       = 'UNIT NUMBER: ' & CLip(job:loan_unit_Number)
                      End!If remove# = 1
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      aud:type          = 'LOA'
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      Case glo:select1
                          Of 1
                              aud:action    = 'REPLACED FAULTY LOAN'
                          Of 2
                              aud:Action    = 'ALTERNATVIE MODEL REQUIRED'
                          Of 3
                              aud:action    = 'LOAN NOT REQUIRED: RE-STOCKED'
                              GetStatus(101,1,'LOA')
      
                              job:loan_unit_number = ''
                              job:loan_accessory = ''
                              job:loan_courier            =''
                              job:loan_consignment_number =''
                              job:loan_despatched         =''
                              job:loan_despatched_user    =''
                              job:loan_despatch_number    =''
                              job:Loaservice              =''
                              job:loan_issued_date = ''
                              job:loan_user        = ''
                              If job:despatch_type = 'LOA'
                                  job:despatched = 'NO'
                                  job:despatch_type = ''
                              End
                              do_loan# = 0
                          Else
                              do_loan# = 0
                              do_return# = 0
                      End!Case glo:select1
                      access:audit.insert()
                      job:loan_consignment_number =''
                      job:loan_despatched         =''
                      job:loan_despatched_user    =''
                      job:loan_despatch_number    =''
                  end!�if access:audit.primerecord() = level:benign
                  glo:select1 = ''
                  glo:select2 = ''
      
              End!If job:loan_unit_number <> ''
              If do_loan# = 1
                  job:Loan_Unit_Number = SelectLoanExchangeUnit('LOA')
      !            glo:select1 = ''
      !            glo:select7 = ''
      !            Select_Loan_Exchange('LOA',full_search",unit_number")
      !            If full_Search" = 'Y'
      !                saverequest# = globalrequest
      !                globalrequest = selectrecord
      !                globalresponse = requestcancelled
      !
      !                access:subtracc.clearkey(sub:account_number_key)
      !                sub:account_number = job:account_number
      !                access:subtracc.fetch(sub:account_number_key)
      !                access:tradeacc.clearkey(tra:account_number_key) 
      !                tra:account_number = sub:main_account_number
      !                access:tradeacc.fetch(tra:account_number_key)
      !                loan_stock_type"    = tra:loan_stock_type
      !                Browse_Loan(loan_stock_Type")
      !            Else
      !                glo:select1 = unit_number"
      !            End
                  If job:Loan_Unit_Number <> ''
                      access:loan.clearkey(loa:ref_number_key)
                      loa:ref_number = job:loan_unit_number
                      if access:loan.fetch(loa:ref_number_key) = Level:Benign
                          continue# = 1
                          IF loa:model_number <> job:model_number
                              Case MessageEx('The selected Loan Unit has a Model Number of:<13,10>'&Clip(loa:model_number)&'.<13,10><13,10>This is different from the Model Number of the job.<13,10><13,10>Do you want to ignore this mismatch?','ServiceBase 2000',|
                                             'Styles\warn.ico','|&Ignore Mismatch|&Cancel',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Ignore Mismatch Button
                                  Of 2 ! &Cancel Button
                                      job:loan_unit_number    = ''
                                      continue# = 0
                              End!Case MessageEx
                          End!IF loa:model_number <> job:model_number
                          If continue# = 1
                              Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                              man:Manufacturer = job:Manufacturer
                              If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                  !Found
      
                                  If man:QALoanExchange
                                      loa:available   = 'QA1'
                                  Else!If def:qaexchloan = 'YES'
                                      loa:available   = 'LOA'
                                  End!If def:qaexchloan = 'YES'
                                  loa:job_number    = job:ref_number
                                  access:loan.update()
                                  get(loanhist,0)
                                  if access:loanhist.primerecord() = level:benign
                                      loh:ref_number    = loa:ref_number
                                      loh:date          = Today()
                                      loh:time          = Clock()
                                      access:users.clearkey(use:password_key)
                                      use:password    =glo:password
                                      access:users.fetch(use:password_key)
                                      loh:user          = use:user_code
                                      JOB:Loan_User     = loh:user
                                      If def:qaexchloan = 'YES'
                                          loh:status  = 'AWAITING QA. LOANED ON JOB NO: ' & Clip(job:ref_number)
                                      Else
                                          loh:status        = 'UNIT LOANED ON JOB NO: ' & clip(job:ref_number)
                                      End!If def:qaexchloan = 'YES'
                                      if access:loanhist.insert()
                                          access:loanhist.cancelautoinc()
                                      end
                                      get(audit,0)
                                      if access:audit.primerecord() = level:benign
                                          aud:notes         = 'UNIT NUMBER: ' & Clip(loa:ref_number) & |
                                                              '<13,10>MODEL NUMBER: ' & CLip(loa:model_number) & |
                                                              '<13,10>E.S.N: ' & CLip(loa:esn)
                                          aud:ref_number    = job:ref_number
                                          aud:date          = today()
                                          aud:time          = clock()
                                          aud:type          = 'LOA'
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          aud:action        = 'LOAN UNIT ATTACHED TO JOB'
                                          access:audit.insert()
                                      end!�if access:audit.primerecord() = level:benign
      
                                      If man:QALoanExchange
                                          job:despatched = ''
                                          job:despatch_type = ''
                                          GetStatus(605,thiswindow.request,'LOA') !QA Required
      
                                      Else!If def:qaexchloan   = 'YES'
                                          job:despatched = 'LAT'
                                          job:despatch_Type = 'LOA'
                                          job:loan_user   = loh:user
                                          job:current_courier = job:loan_courier
                                          access:courier.clearkey(cou:courier_key)
                                          cou:courier = job:Loan_courier
                                          access:courier.tryfetch(cou:courier_key)
                                          IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                                              job:loaservice  = cou:service
                                          Else!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                                              job:loaservice  = ''
                                          End!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
      
                                          job:Loan_Despatched = DespatchANC(job:Loan_Courier,'LOA')
                                          GetStatus(105,thiswindow.request,'LOA')
      
                                      End!If def:qaexchloan   = 'YES'
      
                                      If job:loan_courier = ''
                                          Case MessageEx('Warning! There is no courier attached to this Loan Unit. <13,10><13,10>It will not despatch correctly if you do not select one.','ServiceBase 2000',|
                                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                              Of 1 ! &OK Button
                                          End!Case MessageEx
                                      End!If job:exchange_courier = ''
      
                                      Do time_remaining
                                      Do CityLink_Service
                                      Brw21.resetsort(1)
                                      Set(Defaults)
                                      access:defaults.next()
                                      If def:use_loan_exchange_label  = 'YES'
                                          glo:select1  = job:ref_number
                                          glo:select2  = job:loan_unit_number
                                          case def:label_printer_type
                                              Of 'TEC B-440 / B-442'
                                                  Thermal_Labels_Loan
                                              Of 'TEC B-452'
                                                  Thermal_Labels_Loan_B452
                                          End!case def:label_printer_type
                                          glo:select1  = ''
                                          glo:select2  = ''
                                      End!If def:use_loan_exchange_label  = 'YES'
                                  end!if access:exchhist.primerecord() = level:benign
                              Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                  Case MessageEx('Error! Cannot find the Manufacturer attached to this job.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                              End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      
                          End!If continue# = 1
                      end!if access:loan.fetch(loa:ref_number_key) = Level:Benign
                  End!If glo:select1 <> ''
                  globalrequest = saverequest#
              End!If do_Loan# = 1
              If do_return# = 1
                  access:loan.clearkey(loa:ref_number_key)
                  loa:ref_number = old_loan_number#
                  if access:loan.fetch(loa:ref_number_key) = Level:Benign
                      loa:available = 'AVL'
                      loa:job_number = ''
                      access:loan.update()
                      get(loanhist,0)
                      if access:loanhist.primerecord() = level:benign
                          loh:ref_number    = old_loan_number#
                          loh:date          = Today()
                          loh:time          = Clock()
                          loh:user          = use:user_code
                          loh:status        = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                          if access:loanhist.insert()
                              access:loanhist.cancelautoinc()
                          end
                      end!if access:exchhist.primerecord() = level:benign
                  end!if access:loan.fetch(loa:ref_number_key)
              End!If do_return# = 1
              Do update_loan
              Do loan_box
              Do titles
              !Force the reason
              IF GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                  If job:Loan_Unit_Number <> 0
                      ?jobe:LoanReason{prop:Req} = 1
                  Else
                      ?jobe:LoanReason{prop:Req} = 0
                  End !If job:Exchange_Unit_Number <> 0
              End !IF GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          End!If error# = 0
          glo:select12 = ''
      End !If Error# = 0
    OF ?Lookup_Exchange_Unit
      ThisWindow.Update
      !Security Check
      Error# = 0
      If job:Exchange_Unit_Number <> ''
          If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Error# = 1
          End !If SecurityCheck('JOBS - AMEND EXCHANGE UNIT')
      Else !If job:Exchange_Unit_Number <> ''
          If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
              Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Error# = 1
          End !If SecurityCheck('JOBS - ADD EXCHANGE UNIT')
      End !If job:Exchange_Unit_Number <> ''
      
      If Error# = 0
      
          tmp:FaultyUnit = 0
          glo:select12 = job:model_number
          Set(defaults)
          access:defaults.next()
          error# = 0
          If job:loan_unit_number <> ''
              Case MessageEx('Cannot attached an Exchange Unit. This job has a Loan Unit attached.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              error# = 1
          End!If job:loan_unit_number <> ''
          remove# = 0
          If JOB:Exchange_Consignment_Number <> '' and error# = 0
              Case MessageEx('Warning! The Exchange Unit for this job has already been despatched.<13,10><13,10>If you continue the existing unit on this job will  be removed and returned to Exchange Stock.<13,10><13,10>(An Entry will be made in the Audit Trail)<13,10><13,10>Are you sure you want to remove the existing Exchange Unit?','ServiceBase 2000',|
                             'Styles\warn.ico','|&Remove Unit|&Cancel',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Remove Unit Button
                      remove# = 1
                  Of 2 ! &Cancel Button
                      error# = 1
              End!Case MessageEx
          End!If JOB:Exchange_Consignment_Number <> ''
      
          !CAID Fudge for BER Units
      
          If Instring('COMMUNICAID',def:User_Name,1,1)
              ExchangeMade# = 0
              !Look for BER in the Repair Types
              BER# = 0
              If job:Chargeable_Job = 'YES'
                  If Instring('BER',job:Repair_Type,1,1)
                      BER# = 1
                  End !If Instring('BER',job:Repair_Type,1,1)
              End !If job:Chargeable_Job = 'YES'
              If BER# = 0
                  If job:Warranty_Job = 'YES'
                      If Instring('BER',job:Repair_Type_Warranty,1,1)
                          BER# = 1
                      End !If Instring('BER',job:Repair_Type_Warranty,1,1)
                  End !If job:Warranty_Job = 'YES'
              End !If BER# = 0
      
              If BER#
                  tmp:BERConsignmentNumber    = ExchangeProcess()
              End !If BER#
          End !If Instring('COMMUNICAID',def:User_Name,1,1)
      
          If error# = 0
      
              do_exchange# = 1
              do_return# = 0
              old_exchange_number# = job:exchange_unit_number
      
              If job:exchange_unit_number <> ''
      
                  do_return# = 1
                  glo:select2 = 'E'
                  glo:select1 = ''
                  Loan_Changing_Screen
      
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      If remove# = 1
                          aud:notes         = 'UNIT HAD BEEN DESPATCHED: ' & |
                                              '<13,10>UNIT NUMBER: ' & Clip(job:Exchange_unit_number) & |
                                              '<13,10>COURIER: ' & CLip(job:exchange_Courier) & |
                                              '<13,10>CONSIGNMENT NUMBER: ' & CLip(job:exchange_consignment_number) & |
                                              '<13,10>DATE DESPATCHED: ' & Clip(Format(job:Exchange_despatched,@d6)) &|
                                              '<13,10>DESPATCH USER: ' & CLip(job:Exchange_despatched_user) &|
                                              '<13,10>DESPATCH NUMBER: ' & CLip(job:exchange_despatch_number)
                      Else!If remove# = 1
                          aud:notes       = 'UNIT NUMBER: ' & CLip(job:exchange_unit_Number)
                      End!If remove# = 1
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      aud:type          = 'EXC'
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      Case glo:select1
                          Of 1
                              aud:action  = 'REPLACED FAULTY EXCHANGE UNIT'
                              tmp:FaultyUnit = 1
                          Of 2
                              aud:action  = 'ALTERNATIVE EXCHANGE MODEL REQUIRED'
                          Of 3
                              aud:action   = 'EXCHANGE UNIT NOT REQUIRED: RE-STOCKED'
                              GetStatus(101,1,'EXC')
      
                              job:exchange_unit_number = ''
                              job:exchange_accessory = ''
                              job:exchange_courier            =''
                              job:exchange_consignment_number =''
                              job:exchange_despatched         =''
                              job:exchange_despatched_user    =''
                              job:exchange_despatch_number    =''
                              job:exchange_issued_date = ''
                              job:excservice                  = ''
                              job:exchange_user        = ''
                              If job:despatch_type = 'EXC'
                                  job:despatched = 'NO'
                                  job:despatch_type = ''
                              End
                              do_exchange# = 0
                          Else
                              do_exchange# = 0
                              do_return# = 0
                      End!Case glo:select1
                      access:audit.insert()
                      job:exchange_consignment_number =''
                      job:exchange_despatched         =''
                      job:exchange_despatched_user    =''
                      job:exchange_despatch_number    =''
      
                      glo:select1 = ''
                      glo:select2 = ''
                  end!�if access:audit.primerecord() = level:benign
      
              End!If job:exchange_unit_number <> ''
              If do_exchange# = 1
                  job:Exchange_Unit_Number = SelectLoanExchangeUnit('EXC')
      
                  If job:Exchange_Unit_Number <> ''
      
      
      
      !            glo:select1 = ''
      !            glo:select7 = ''
      !            Select_Loan_Exchange('EXC',full_search",unit_number")
      !                job:exchange_accessory = 'NO'
      !                If full_search" = 'Y'
      !                    saverequest# = globalrequest
      !                    globalrequest = selectrecord
      !                    globalresponse = requestcancelled
      !
      !                    access:subtracc.clearkey(sub:account_number_key)
      !                    sub:account_number = job:account_number
      !                    access:subtracc.fetch(sub:account_number_key)
      !                    access:tradeacc.clearkey(tra:account_number_key) 
      !                    tra:account_number = sub:main_account_number
      !                    access:tradeacc.fetch(tra:account_number_key)
      !                    exchange_stock_type"    = tra:exchange_stock_type
      !                    Browse_Exchange(exchange_stock_type")
      !                Else
      !                    glo:select1 = unit_number"
      !                End
      !                if glo:select1 <> ''
      !                    job:exchange_unit_number = glo:select1
                          access:exchange.clearkey(xch:ref_number_key)
                          xch:ref_number = job:exchange_unit_number
                          if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                              continue# = 1
                              access:stocktyp.clearkey(stp:stock_type_key)
                              stp:stock_type = xch:stock_type
                              if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                                  If stp:available <> 1
                                      Case MessageEx('The selected unit is NOT available.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      job:exchange_unit_number = ''
                                      continue# = 0
                                  End!If stp:available <> 1
                              End!if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                              
                              If xch:model_number <> job:model_number and continue# = 1
                                  Case MessageEx('The selected Exchange Unit has a Model Number of:<13,10><13,10>'&Clip(xch:model_number)&'.<13,10><13,10>Do you want to ignore this mismatch?','ServiceBase 2000',|
                                                 'Styles\warn.ico','|&Ignore Mismatch|&Cancel',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                      Of 1 ! &Ignore Mismatch Button
                                      Of 2 ! &Cancel Button
                                          job:exchange_unit_number = ''
                                          continue# = 0
                                  End!Case MessageEx
                              End!If xch:model_number <> job:model_number
                              If continue# = 1
                                  Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                                  man:Manufacturer = job:Manufacturer
                                  If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                      !Found
                                      If man:QALoanExchange
                                          xch:available = 'QA1'
                                      Else!If def:qaexchloan = 'YES'
                                          xch:available = 'EXC'
                                      End!If def:qaexchloan = 'YES'
      
      
                                      xch:job_number    = job:ref_number
                                      access:exchange.update()
      
                                      get(exchhist,0)
                                      if access:exchhist.primerecord() = level:benign
                                          exh:ref_number    = job:exchange_unit_number
                                          exh:date          = Today()
                                          exh:time          = Clock()
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          exh:user = use:user_code
                                          If man:QALoanExchange = 'YES'
                                              exh:status        = 'AWAITING QA. EXCHANGED ON JOB NO: ' & clip(job:ref_number)
                                          Else!If def:qaexchloan = 'YES'
                                              exh:status        = 'UNIT EXCHANGED ON JOB NO: ' & clip(job:ref_number)
                                          End!If def:qaexchloan = 'YES'
                                          if access:exchhist.insert()
                                              access:exchhist.cancelautoinc()
                                          end
                                      end!if access:exchhist.primerecord() = level:benign
          !Add Exchange Audit
                                      get(audit,0)
                                      if access:audit.primerecord() = level:benign
                                          aud:notes         = 'UNIT NUMBER: ' & CLip(xch:ref_number) & |
                                                              '<13,10>MODEL NUMBER: ' & CLip(xch:model_number) & |
                                                              '<13,10>E.S.N.: ' & CLip(xch:esn)
                                          aud:ref_number    = job:ref_number
                                          aud:date          = today()
                                          aud:time          = clock()
                                          aud:type          = 'EXC'
                                          access:users.clearkey(use:password_key)
                                          use:password =glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          aud:action        = 'EXCHANGE UNIT ATTACHED TO JOB'
                                          access:audit.insert()
                                      end!�if access:audit.primerecord() = level:benign
      
                                      ExchangeMade# = 1
      
          !Create a new exchange unit for the incoming unit (Made from the customer's unit).
          !If it has an audit number make the new unit the 'Replacement'
                                      
                                      access:exchange_alias.clearkey(xch_ali:ref_number_key)
                                      xch_ali:ref_number = job:exchange_unit_number
                                      if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign
                                          !Let see if this unit already exists first
                                          Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                                          xch:ESN = job:ESN
                                          If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                                              !Found
                                              !There is already a unit with this IMEI number
                                              !To avoid errors, we'll use that one.
                                              If job:date_Completed <> ''
                                                  xch:available   = 'RTS'
                                              Else!If job:date_Completed <> ''
                                                  If job:workshop = 'YES'
                                                      xch:available   = 'REP'
                                                  Else!If job:workshop = 'YES'
                                                      xch:available   = 'INC'
                                                  End!If job:workshop = 'YES'
                                              End!If job:date_Completed <> ''
                                              xch:Job_Number  = job:Ref_Number
                                              Access:EXCHANGE.TryUpdate()
                                          Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                                              !The IMEI number doesn't already exists. Make a new one
                                              get(exchange,0)
                                              if access:exchange.primerecord() = Level:benign
                                                  ref_number$        = xch:ref_number
                                                  xch:record        :=: xch_ali:record
                                                  xch:ref_number     = ref_number$
                                                  If job:date_Completed <> ''
                                                      xch:available   = 'RTS'
                                                  Else!If job:date_Completed <> ''
                                                      If job:workshop = 'YES'
                                                          xch:available   = 'REP'
                                                      Else!If job:workshop = 'YES'
                                                          xch:available   = 'INC'
                                                      End!If job:workshop = 'YES'
                                                  End!If job:date_Completed <> ''
                                                  xch:job_number     = job:ref_number
                                                  xch:esn            = job:esn
                                                  xch:msn            = job:msn
                                                  xch:model_number   = job:model_number
                                                  xch:manufacturer   = job:manufacturer
                                                  if access:exchange.insert()
                                                     access:exchange.cancelautoinc()
                                                  end
                                              end!if access:exchange.primerecord() = Level:benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                                          If xch_ali:audit_number <> ''
                                              access:excaudit.clearkey(exa:audit_number_key)
                                              exa:stock_type   = xch:stock_type
                                              exa:audit_number = xch:audit_number
                                              if access:excaudit.fetch(exa:audit_number_key) = Level:Benign
                                                  exa:replacement_unit_number = xch:ref_number
                                                  access:excaudit.update()
                                              end
                                          End!If xch:audit_number <> ''
                                      end!if access:exchange_alias.fetch(xch_ali:ref_number_key) = Level:Benign
      
          !New Exchange Unit added and audit number file update according with the new replacement number.
                                      If man:QALoanExchange
                                          job:despatched = ''
                                          job:despatch_type = ''
                                          job:exchange_status = 'QA REQUIRED'
                                          job:exchange_user   = exh:user
                                          GetStatus(605,thiswindow.request,'EXC') !qa required
      
                                      Else!If def:qaexchloan = 'YES'
                                          job:exchange_status = 'AWAITING DESPATCH'
          !                                job:despatched = 'REA'
          !                                job:despatch_type = 'EXC'
                                          job:exchange_user = exh:user
          !                                job:current_courier = job:exchange_courier
                                          access:courier.clearkey(cou:courier_key)
                                          cou:courier = job:Exchange_courier
                                          access:courier.tryfetch(cou:courier_key)
                                          IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                                              job:excservice  = cou:service
                                          Else!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                                              job:excservice  = ''
                                          End!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                                          job:Exchange_Despatched = DespatchANC(job:exchange_Courier,'EXC')
                                          GetStatus(110,thiswindow.request,'EXC') !depatch exchange unit
      
                                      End!If def:qaexchloan = 'YES'
                                      Do time_remaining
                                      Do CityLink_Service
                                      brw21.resetsort(1)
      
                                      If job:exchange_courier = ''
                                          Case MessageEx('Warning! There is no courier attached to this Exchange Unit. <13,10><13,10>It will not despatch correctly if you do not select one.','ServiceBase 2000',|
                                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                              Of 1 ! &OK Button
                                          End!Case MessageEx
                                      End!If job:exchange_courier = ''
      
      
                                      If def:use_loan_exchange_label = 'YES'
                                          glo:select1 = job:ref_number
                                          If Clip(GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')) = 1 and tmp:FaultyUnit
                                              ExchangeRepairLabel(job:Ref_Number)
                                          Else !If Clip(GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                                              glo:select2 = job:exchange_unit_number
                                              Set(defaults)
                                              access:defaults.next()
                                              Case def:label_printer_type
                                                  Of 'TEC B-440 / B-442'
                                                      Thermal_Labels_Exchange
                                                  Of 'TEC B-452'
                                                      Thermal_Labels_Exchange_B452
                                              End!Case def:themal_printer_type
                                          End !If Clip(GETINI('PRINTING','ExchangeRepairLabel',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                                          
                                          glo:select1 = ''
                                          glo:select2 = ''
                                      End!If def:use_loan_exchange_labels = 'YES'
                                      access:subtracc.clearkey(sub:account_number_key)
                                      sub:account_number = job:account_number
                                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                                          access:tradeacc.clearkey(tra:account_number_key) 
                                          tra:account_number = sub:main_account_number
                                          if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                              If tra:refurbcharge = 'YES' and job:workshop = 'YES'
                                                  Case MessageEx('This Repair Unit must be Refurbished.<13,10><13,10>Do you wish to print a Refurbishment Label?<13,10><13,10>(Note: It will be printed when you have completed editing this job)','ServiceBase 2000',|
                                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                                      Of 1 ! &Yes Button
                                                          print_label_temp = 'YES'
                                                      Of 2 ! &No Button
                                                  End!Case MessageEx
                                              End!If tra:refurbcharge = 'YES'
                                          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
      
          !Check to see if at third party
                                      If job:third_party_site <> ''
                                          save_trb_id = access:trdbatch.savefile()
                                          access:trdbatch.clearkey(trb:esn_only_key)
                                          trb:esn = job:esn
                                          set(trb:esn_only_key,trb:esn_only_key)
                                          loop
                                              if access:trdbatch.next()
                                                 break
                                              end !if
                                              if trb:esn <> job:esn      |
                                                  then break.  ! end if
                                              If trb:ref_number = job:ref_number
                                                  trb:exchanged = 'YES'
                                                  access:trdbatch.update()
                                              End!If trb:ref_number = job:ref_number
                                          end !loop
                                          access:trdbatch.restorefile(save_trb_id)
                                      End!If job:third_partu_site
                                  Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                      !Error
                                      Case MessageEx('Error! Cannot find the Manufacturer attached to this job.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      
                              End!If continue# = 1
                          end
                      End!if glo:select1 <> ''
                      globalrequest = saverequest#
          !        End!If glo:select7 <> ''
      
              End!If do_exchange# = 1
              If do_return# = 2
                  access:stock.clearkey(sto:ref_number_key)
                  sto:ref_number = old_exchange_number#
                  if access:stock.fetch(sto:ref_number_key) = Level:Benign
                      sto:quantity_stock += 1
                      access:stock.update()
                  end
              End!If do_return# = 2
              If do_return# = 1
                  !Delete incoming unit
                  save_xch_id = access:exchange.savefile()
                  access:exchange.clearkey(xch:esn_only_key)
                  xch:esn = job:esn
                  set(xch:esn_only_key,xch:esn_only_key)
                  loop
                      if access:exchange.next()
                         break
                      end !if
                      if xch:esn <> job:esn      |
                          then break.  ! end if
                      If xch:job_number = job:ref_number
                          relate:exchange.Delete(0)
                      End!If xch:job_number = job:ref_number
                  end !loop
                  access:exchange.restorefile(save_xch_id)
      
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number = old_exchange_number#
                  if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                      If tmp:FaultyUnit
                          xch:Available = 'INR'
                          Case MessageEx('Please book the replaced Exchange unit as a new job.','ServiceBase 2000',|
                                         'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else !If tmp:FaultUnit
                          If xch:available <> 'QAF'
                              xch:available = 'AVL'
                          End!If xch:available <> 'QAF'
      
                      End !If tmp:FaultUnit
                      xch:job_number = ''
                      access:exchange.update()
      
                      get(exchhist,0)
                      if access:exchhist.primerecord() = level:benign
                          exh:ref_number    = old_exchange_number#
                          exh:date          = Today()
                          exh:time          = Clock()
                          exh:user          = use:user_code
                          If tmp:FaultyUnit
                              exh:status        = 'FAULTY UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                          Else !If tmp:FaultyUnit
                              exh:status        = 'UNIT RE-STOCKED FROM JOB NO: ' & clip(format(job:ref_number,@p<<<<<<<#p))
                          End !If tmp:FaultyUnit
                          
                          if access:exchhist.insert()
                              access:exchhist.cancelautoinc()
                          end
                      end!if access:exchhist.primerecord() = level:benign
                  end!if access:exchange.fetch(xch:ref_number_key)
              End!If do_return# = 1
              Do update_exchange
              Do exchange_box
              Do titles
      
              !Force the reason
              IF GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                  If job:Exchange_Unit_Number <> 0
                      ?jobe:ExchangeReason{prop:Req} = 1
                  Else
                      ?jobe:ExchangeReason{prop:Req} = 0
                  End !If job:Exchange_Unit_Number <> 0
              End !IF GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
              !Exchange Made, Do CAID BER Bit now
      
              If tmp:BERConsignmentNumber <> '' and BER# and ExchangeMade#
                  !Add hardcoded invoice text.
                  Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                  jbn:RefNumber   = job:Ref_Number
                  If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                      !Found
                      !Put this text at the top of the Invoice Text
                      jbn:Invoice_Text = 'YOUR UNIT HAS BEEN EXCHANGED. PLEASE MAKE A NOTE OF THE NEW ' & |
                                          'I.M.E.I. NO. AND SECURITY CODE. BECAUSE YOUR UNIT WAS EXCHANGED ' &|
                                          'AND NOT REPAIRED YOUR WARRANTY WILL RUN IN LINE WITH YOUR ORIGINAL PHONE.<13,10>' & Clip(jbn:Invoice_Text)
                      Access:JOBNOTES.Update()
                  Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
      
                  !Change engineer to current user
                  Access:USERS.Clearkey(use:Password_Key)
                  use:Password    = glo:Password
                  If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                      !Found
                      job:Engineer    = use:User_Code
                  Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
                  !Complete Job
                  job:Date_Completed = Today()
                  job:Time_Completed = Clock()
                  job:Completed       = 'YES'
      
                  !Change job status
                  GetStatus(911,0,'JOB')
      
                  !Change Exchange Status
                  GetStatus(901,0,'EXC')
      
                  !Mark all as despatched
                  job:Consignment_Number  = tmp:BERCOnsignmentNumber
                  job:Date_Despatched     = Today()
                  job:Despatched          = 'YES'
                  job:Despatch_User       = use:User_Code
      
                  job:Exchange_Consignment_Number = tmp:BERCOnsignmentNumber
                  job:Exchange_Despatched = Today()
                  job:Exchange_Despatched_User    = use:User_Code
      
                  Access:JOBS.Update()
      
                  glo:Select1 = job:Ref_Number
                  Despatch_Note
                  glo:Select1 = ''
      
                  Post(Event:Accepted,?OK)
              End !If tmp:BERConsignmentNumber <> '' and BER#
      
              If ExchangeMade#
                  !Auto make a split job?
                  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                  sub:Account_Number  = job:Account_Number
                  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Found
                      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                      tra:Account_Number  = sub:Main_Account_Number
                      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Found
                          If tra:MakeSplitJob
                              GetStatus(Sub(tra:SplitJobStatus,1,3),1,'JOB')
                              GetStatus(Sub(tra:SplitExchangeStatus,1,3),1,'EXC')
                              job:Chargeable_Job = 'YES'
                              job:Charge_Type = tra:SplitCChargeType
                              job:Warranty_Job = 'YES'
                              job:Warranty_Charge_Type = tra:SplitWChargeType
                              Post(Event:Accepted,?job:Chargeable_Job)
                              Post(Event:Accepted,?job:Warranty_Job)
                          End !If tra:MakeSplitJob
                      Else ! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                          !Error
                      End !If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  Else ! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                      !Error
                  End !If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
              End !If ExchangeMade#
          End!If error# = 0
          glo:select12 = ''
      End !If Error# = 0
      
      Display()
    OF ?jobe:ExchangeReason
      If ~0{prop:AcceptAll}
          Access:EXREASON.Clearkey(exr:ReasonKey)
          exr:ReasonType  = 0
          exr:Reason  = jobe:ExchangeReason
          If Access:EXREASON.Tryfetch(exr:ReasonKey) = Level:Benign
              !Found
      
          Else ! If Access:EXREASON.Tryfetch(exr:ReasonKey) = Level:Benign
              !Error
              Post(Event:Accepted,?LookupExchangeReason)
          End !If Access:EXREASON.Tryfetch(exr:ReasonKey) = Level:Benign
      End !0{prop:AcceptAll}
    OF ?LookupExchangeReason
      ThisWindow.Update
      GlobalRequest = SelectRecord
      SelectExchangeLoanReasons(0)
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              jobe:ExchangeReason = exr:Reason  
              Select(?+2)
          Of Requestcancelled
      !        jobe:ExchangeReason = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:ExchangeReason)
    OF ?jobe:LoanReason
      If ~0{prop:AcceptAll}
          Access:EXREASON.Clearkey(exr:ReasonKey)
          exr:ReasonType  = 1
          exr:Reason  = jobe:LoanReason
          If Access:EXREASON.Tryfetch(exr:ReasonKey) = Level:Benign
              !Found
      
          Else ! If Access:EXREASON.Tryfetch(exr:ReasonKey) = Level:Benign
              !Error
              Post(Event:Accepted,?LookupLoanReason)
          End !If Access:EXREASON.Tryfetch(exr:ReasonKey) = Level:Benign
      End !0{prop:AcceptAll}
    OF ?LookupLoanReason
      ThisWindow.Update
      GlobalRequest = SelectRecord
      SelectExchangeLoanReasons(1)
      ThisWindow.Reset
      Case GlobalResponse
          Of Requestcompleted
              jobe:LoanReason = exr:Reason
              Select(?+2)
          Of Requestcancelled
      !        jobe:ExchangeReason = ''
              Select(?-1)
      End!Case Globalreponse
      Display(?jobe:LoanReason)
    OF ?job:Loan_Courier
      If ~0{Prop:acceptall}
      access:courier.clearkey(cou:courier_key)
      cou:courier = job:loan_courier
      if access:courier.fetch(cou:courier_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_courier
          if globalresponse = requestcompleted
              job:loan_courier = cou:courier
          else
              job:loan_courier = loan_courier_temp
              select(?-1)
          end
          display(?job:loan_courier)
          globalrequest     = saverequest#
      end!if access:courier.fetch(cou:courier_key)
      If job:loan_courier <> loan_courier_temp
          If loan_courier_temp <> ''
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If job:invoice_Number <> ''
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If error# = 1
                  job:loan_courier = loan_courier_temp
              Else!If error# = 1
                  Case MessageEx('Are you sure you want to change the Courier?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          If thiswindow.request <> Insertrecord
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'NEW COURIER: ' & CLip(job:loan_courier) & '<13,10>OLD COURIER: ' & CLip(loan_courier_temp)
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'LOAN COURIER CHANGED: ' & Clip(job:loan_courier)
                                  aud:type          = 'LOA'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
                          End!If thiswindow.request <> Insertrecord
                          If job:despatch_type = 'LOA'
                              job:current_courier = job:loan_courier
                          End
                          loan_courier_temp = job:loan_courier
                      Of 2 ! &No Button
                          job:loan_courier = loan_courier_temp
                  End!Case MessageEx
              End!If error# = 1
          Else!If loan_courier_temp <> ''
              If job:despatch_type = 'LOA'
                  job:current_courier = job:loan_courier
              End!If job:despatch_type = 'LOA'
              loan_courier_temp = job:loan_courier
          End!If loan_courier_temp <> ''
      
      End!If job:loan_courier <> loan_courier_temp
      Display(?job:loan_courier)
      End!If ~0{Prop:acceptall}
    OF ?job:Exchange_Courier
      If ~0{prop:acceptall}
      access:courier.clearkey(cou:courier_key)
      cou:courier = job:exchange_courier
      if access:courier.fetch(cou:courier_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_courier
          if globalresponse = requestcompleted
              job:exchange_courier = cou:courier
          else
              job:exchange_courier = exchange_courier_temp
              select(?-1)
          end
          display(?job:exchange_courier)
          globalrequest     = saverequest#
      end!if access:courier.fetch(cou:courier_key)
      If job:exchange_courier <> exchange_courier_temp
          If exchange_courier_temp <> ''
              error# = 0
              If job:date_completed <> ''
                  Check_Access('AMEND COMPLETED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If job:invoice_Number <> ''
                  Check_Access('AMEND INVOICED JOBS',x")
                  If x" = False
                      Case MessageEx('Cannot change! This job has been invoiced.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End
              End
              If error# = 1
                  job:exchange_courier = exchange_courier_temp
              Else!If error# = 1
                  Case MessageEx('Are you sure you want to change the Courier?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          If thiswindow.request <> Insertrecord
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'NEW COURIER: ' & CLip(job:exchange_courier) & '<13,10>OLD COURIER: ' & CLip(exchange_courier_temp)
                                  aud:ref_number    = job:ref_number
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'EXCHANGE COURIER CHANGED: ' & Clip(job:exchange_courier)
                                  aud:type          = 'EXC'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
                          End!If thiswindow.request <> Insertrecord
                          exchange_courier_temp = job:exchange_courier
                          If job:despatch_type = 'EXC'
                              job:current_courier = job:exchange_courier
                          End!If despatch_type = 'EXC'
                      Of 2 ! &No Button
                          job:exchange_courier = exchange_courier_temp
                  End!Case MessageEx
              End!If error# = 1
          Else!If exchange_courier_temp <> ''
              If job:despatch_Type = 'EXC'
                  job:current_courier = job:exchange_courier
              End
              exchange_courier_temp = job:exchange_courier
          End!If exchange_courier_temp <> ''
      
      End!If job:exchange_courier <> exchange_courier_temp
      Display(?job:exchange_courier)
      End!If ~0{prop:acceptall}
    OF ?Show_List_Button
      ThisWindow.Update
      Browse_Loan_Accessories(job:loan_unit_number)
      ThisWindow.Reset
      glo:Select1 = ''
    OF ?Exchange_Accessory_Button
      ThisWindow.Update
      Browse_Job_Exchange_Accessory(job:ref_number)
      ThisWindow.Reset
      glo:select12 = ''
      error# = 0
      If job:despatch_type = 'LOA' And job:despatched = 'REA'
          error# = 1
      End
      If error# = 0
          Do Count_accessories
          If exchange_accessory_count_temp > accessory_count_temp
              job:exchange_status = 'AWAITING DESPATCH'
              job:despatched = 'REA'
              job:despatch_type = 'EXC'
              job:exchange_user = exh:user
              job:current_courier = job:exchange_courier
              GetStatus(110,thiswindow.request,'EXC') !depatch exchange unit
              Do time_remaining
              brw21.resetsort(1)
          End!If exchange_accessory_count_temp > accessory_count_temp
          Do update_exchange
          Do exchange_box
      End!If error# = 1
    OF ?Amend_Despatch
      ThisWindow.Update
      Case MessageEx('Are you sure you want to amend the Exchange Unit''s Despatch Details?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
          Unhide(?despatched_calendar)
          ?job:Exchange_consignment_number{prop:readonly} = 0
          ?job:Exchange_consignment_number{prop:color} = color:yellow
          ?job:exchange_despatched{prop:readonly} = 0
          ?job:exchange_despatched{prop:color} = color:yellow
          ?job:exchange_despatched_user{prop:readonly} = 0
          ?job:exchange_despatched_user{prop:color} = color:yellow
          ?job:exchange_despatch_number{prop:readonly} = 0
          ?job:exchange_despatch_number{prop:color} = color:yellow
      
        Of 2 ! &No Button
      End!Case MessageEx
    OF ?job:Exchange_Consignment_Number
      If ~0{prop:acceptall}
          If job:exchange_consignment_number <> exchange_consignment_number_temp
              Case MessageEx('Are you sure you want to change the Consignment Number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW CONSIGNMENT NO: ' & Clip(job:exchange_consignment_number) & |
                                              '<13,10>PREVIOUS CONSIGNMENT NO: ' & Clip(exchange_consignment_number_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'EXCHANGE CONSIGNMENT NO CHANGED'
                          aud:type          = 'EXC'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      exchange_consignment_number_temp    = job:exchange_consignment_number
                  Of 2 ! &No Button
                      job:exchange_consignment_number     = exchange_consignment_number_temp
              End!Case MessageEx
          End!If job:exchange_consignment_number <> exchange_consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?Despatched_Calendar
      ThisWindow.Update
      Post(Event:accepted,?job:exchange_despatched)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          job:Exchange_Despatched = TINCALENDARStyle1(job:Exchange_Despatched)
          Display(?JOB:Exchange_Despatched)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?job:Exchange_Despatched
      If ~0{prop:acceptall}
          If job:exchange_despatched <> exchange_despatched_temp
              Case MessageEx('Are you sure you want to change the Despatch Date?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW DESPATCHED DATE: ' & Clip(job:exchange_despatched) & |
                                              '<13,10>PREVIOUS DESPATCHED DATE: ' & Clip(exchange_despatched_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'EXCHANGE DESPATCHED DATE CHANGED'
                          aud:type          = 'EXC'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      exchange_despatched_temp    = job:exchange_despatched
                  Of 2 ! &No Button
                      job:exchange_despatched     = exchange_despatched_temp
              End!Case MessageEx
          End!If job:exchange_consignment_number <> exchange_consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?job:Exchange_Despatch_Number
      If ~0{prop:acceptall}
          If job:exchange_despatch_number <> exchange_despatch_number_temp
              Case MessageEx('Are you sure you want to change the Despatch Number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW DESPATCH NO: ' & Clip(job:exchange_despatch_number) & |
                                              '<13,10>PREVIOUS DESPATCH NO: ' & Clip(exchange_despatch_number_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'EXCHANGE DESPATCH NO CHANGED'
                          aud:type          = 'EXC'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      exchange_despatch_number_temp    = job:exchange_despatch_number
                  Of 2 ! &No Button
                      job:exchange_despatch_number     = exchange_despatch_number_temp
              End!Case MessageEx
          End!If job:exchange_consignment_number <> exchange_consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?job:Exchange_Despatched_User
      If ~0{prop:acceptall}
          If job:exchange_despatched_user <> exchange_despatched_user_temp
              Case MessageEx('Are you sure you want to change the Despatch User?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      get(audit,0)
                      if access:audit.primerecord() = Level:Benign
                          aud:notes         = 'NEW USER: ' & Clip(job:exchange_despatched_user) & |
                                              '<13,10>PREVIOUS USER: ' & Clip(exchange_despatched_user_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'EXCHANGE DESPATCHED USER CHANGED'
                          aud:type          = 'EXC'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = Level:Benign
                      exchange_despatched_user_temp  = job:exchange_despatched_user
                  Of 2 ! &No Button
                      job:exchange_despatched_user    = exchange_despatched_user_temp
              End!Case MessageEx
          End!If job:exchange_consignment_number <> exchange_consignment_number_temp
      End!If ~0{prop:acceptall}
    OF ?Validate_Serial_Number
      ThisWindow.Update
      Serial_Number_Validation(serial_number")
      If serial_number" <> ''
          !Find what the serial number entered is equal to:
          found# = 1
          If job:esn <> serial_number"
              If job:msn <> serial_number"
                  found# = 0
              end!if access:jobs.fetch(job:msn_key)
              IF found# = 0
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number = job:exchange_unit_number
                  if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                      If xch:esn = serial_number" Or xch:msn = serial_number"
                          found# = 2
                      End!If xch:esn = serial_number_temp Or xch:msn = serial_number_temp
                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
              End!IF found# = 0
          end!if access:jobs.fetch(job:esn_key)
          !If serial number doesn't match anything about the job: found = 0
          !If serial number DOES match ESN, or MSN of the job: found = 1
          !If serial number matches the exchange unit ESN: found = 2
          Case found#
              Of 0
                  Case MessageEx('Warning! Serial Number Mismatch.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      aud:notes         = 'SERIAL NUMBER ENTERED: ' & Clip(serial_number")
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      aud:action        = 'SERIAL NUMBER MISMATCHED'
                      access:audit.insert()
                  end!�if access:audit.primerecord() = level:benign
      
              Of 2
                  Case MessageEx('The Serial Number entered matched the Exchange Unit''s Serial Number on this job.<13,10><13,10>Please bring this to the attention of your System Supervisor.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      aud:notes         = 'THE SERIAL NUMBER VALIDATED MATCHED THE EXCHANGE UNIT SERIAL NUMBER. ' & |
                                          '<13,10>SERIAL NUMBER ENTERED: ' & Clip(serial_number")
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      access:users.clearkey(use:password_key)
                      use:password =glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      aud:action        = 'SERIAL NUMBER VALIDATION ERROR'
                      access:audit.insert()
                  end!�if access:audit.primerecord() = level:benign
      
          End!Case found#
      
      End!If serial_number" <> ''
    OF ?Print_Label
      ThisWindow.Update
      Case MessageEx('A Job Label will be printed when you have finished amending this job.','ServiceBase 2000',|
                   'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
        Of 1 ! &OK Button
      End!Case MessageEx
      print_label_temp = 'YES'
    OF ?Contact_History
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      glo:select12 = ''
    OF ?Audit_Trail_Button
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      glo:select12 = ''
    OF ?UnRejectButton
      ThisWindow.Update
      Case MessageEx('Are you sure you want to Un-Reject this job and make it available for invoicing again?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              job:invoicestatus = ''
              unHide(?CharVat)
              unHide(?CharTotal)
              unHide(?vat_chargeable_temp)
              unHide(?total_temp)
              hide(?CharRejected)
              hide(?UnRejectButton)
      
          Of 2 ! &No Button
      End!Case MessageEx
      Display()
    OF ?resubmit_button
      ThisWindow.Update
      Case MessageEx('This will allow this job to be included in the next EDI Process.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      Of 1 ! &Yes Button
          job:edi = 'NO'
      Of 2 ! &No Button
      End!Case MessageEx
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Contact_History:2
      ThisWindow.Update
      BrowseStatusChanges(job:Ref_Number)
      ThisWindow.Reset
    OF ?ThirdPartyButton
      ThisWindow.Update
      BrowseJobThird
      ThisWindow.Reset
    OF ?Fault_Codes_Lookup
      ThisWindow.Update
      access:jobnotes.clearkey(jbn:refNumberKey)
      jbn:RefNumber   = job:ref_number
      access:jobnotes.tryfetch(jbn:refnumberkey)
      
      glo:FaultCode1          = job:Fault_Code1
      glo:FaultCode2          = job:Fault_Code2
      glo:FaultCode3          = job:Fault_Code3
      glo:FaultCode4          = job:Fault_Code4
      glo:FaultCode5          = job:Fault_Code5
      glo:FaultCode6          = job:Fault_code6
      glo:FaultCode7          = job:Fault_Code7
      glo:FaultCode8          = job:Fault_code8
      glo:FaultCode9          = job:Fault_Code9
      glo:FaultCode10         = job:Fault_Code10
      glo:FaultCode11         = job:Fault_Code11
      glo:FaultCode12         = job:Fault_Code12
      glo:FaultCode13         = jobe:FaultCode13
      glo:FaultCode14         = jobe:FaultCode14
      glo:FaultCode15         = jobe:FaultCode15
      glo:FaultCode16         = jobe:FaultCode16
      glo:FaultCode17         = jobe:FaultCode17
      glo:FaultCode18         = jobe:FaultCode18
      glo:FaultCode19         = jobe:FaultCode19
      glo:FaultCode20         = jobe:FaultCode20
      glo:TradeFaultCode1     = jobe:TraFaultCode1
      glo:TradeFaultCode2     = jobe:TraFaultCode2
      glo:TradeFaultCode3     = jobe:TraFaultCode3
      glo:TradeFaultCOde4     = jobe:TraFaultCode4
      glo:TradeFaultCode5     = jobe:TraFaultCOde5
      glo:TradeFaultCode6     = jobe:TraFaultCode6
      glo:TradeFaultCode7     = jobe:TraFaultCode7
      glo:TradeFaultCode8     = jobe:TraFaultCode8
      glo:TradeFaultCode9     = jobe:TraFaultCode9
      glo:TradeFaultCode10    = jobe:TraFaultCode10
      glo:TradeFaultCode11    = jobe:TraFaultCode11
      glo:TradeFaultCode12    = jobe:TraFaultCode12
      
      GenericFaultCodes(job:Ref_Number,ThisWindow.Request,job:account_Number,job:Manufacturer,|
                          job:Warranty_Charge_Type,job:Repair_Type_Warranty,job:Date_Completed)
      
      job:Fault_Code1         =  glo:FaultCode1
      job:Fault_Code2         =  glo:FaultCode2
      job:Fault_Code3         =  glo:FaultCode3
      job:Fault_Code4         =  glo:FaultCode4
      job:Fault_Code5         =  glo:FaultCode5
      job:Fault_code6         =  glo:FaultCode6
      job:Fault_Code7         =  glo:FaultCode7
      job:Fault_code8         =  glo:FaultCode8
      job:Fault_Code9         =  glo:FaultCode9
      job:Fault_Code10        =  glo:FaultCode10
      job:Fault_Code11        =  glo:FaultCode11
      job:Fault_Code12        =  glo:FaultCode12
      jobe:FaultCode13        =  glo:FaultCode13
      jobe:FaultCode14        =  glo:FaultCode14
      jobe:FaultCode15        =  glo:FaultCode15
      jobe:FaultCode16        =  glo:FaultCode16
      jobe:FaultCode17        =  glo:FaultCode17
      jobe:FaultCode18        =  glo:FaultCode18
      jobe:FaultCode19        =  glo:FaultCode19
      jobe:FaultCode20        =  glo:FaultCode20
      jobe:TraFaultCode1      =  glo:TradeFaultCode1
      jobe:TraFaultCode2      =  glo:TradeFaultCode2
      jobe:TraFaultCode3      =  glo:TradeFaultCode3
      jobe:TraFaultCode4      =  glo:TradeFaultCOde4
      jobe:TraFaultCOde5      =  glo:TradeFaultCode5
      jobe:TraFaultCode6      =  glo:TradeFaultCode6
      jobe:TraFaultCode7      =  glo:TradeFaultCode7
      jobe:TraFaultCode8      =  glo:TradeFaultCode8
      jobe:TraFaultCode9      =  glo:TradeFaultCode9
      jobe:TraFaultCode10     =  glo:TradeFaultCode10
      jobe:TraFaultCode11     =  glo:TradeFaultCode11
      jobe:TraFaultCode12     =  glo:TradeFaultCode12
      Do AreThereNotes
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Compulsory Field Check    New Booking Audit Trail
  !Right Into JOBSE
  jobe:Network    = tmp:Network
  jobe:InWorkshopDate = tmp:InWorkshopDate
  jobe:InWorkshopTime = tmp:InWorkshopTime
  Access:JOBSE.TryUpdate()
  sav:ref_number    = job:Ref_number
  If ThisWindow.request   = Insertrecord
      Set(defaults)
      access:defaults.next()
      error# = 0
  
      ! Start Change 2805 BE(01/07/03)
      ! Check Duplicate Order Number
      !IF (job:Order_Number <> '') THEN
      !    SETCURSOR(cursor:wait)
      !    save_job_ali_id = access:jobs_alias.savefile()
      !    SET(TRADEACC)
      !    LOOP
      !        IF (access:TRADEACC.next() <> Level:benign) THEN
      !            BREAK
      !        END
      !        access:JOBS_ALIAS.clearkey(job_ali:AccOrdNoKey)
      !        job_ali:Account_Number = tra:Account_Number
      !        job_ali:Order_Number = job:Order_Number
      !        IF ((access:JOBS_ALIAS.fetch(job_ali:AccOrdNoKey) = Level:benign) AND|
      !            (job_ali:Ref_Number <> job:Ref_Number) AND|
      !            (job_ali:Cancelled <> 'YES')) THEN
      !                SETCURSOR()
      !                CASE MessageEx('Duplicate Purchase Order Number<13,10><13,10>' &|
      !                           'Do you wish to continue or<13,10>' &|
      !                           'go back and change the entered value ?','ServiceBase 2000',|
      !                           'Styles\question.ico','&Continue|C&hange',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
      !                           beep:systemquestion,msgex:samewidths,84,26,0)
      !                    OF 1 ! &Continue Button
      !
      !                    OF 2 ! &Change Button
      !                        error# = 1
      !                END
      !                BREAK
      !        END
      !    END
      !    access:jobs_alias.restorefile(save_job_ali_id)
      !    SETCURSOR()
      !
      !    IF (error# <> 0) THEN
      !        SELECT(?job:Order_Number)
      !        CYCLE
      !    END
      !END
      ! End Change 2805 BE(01/07/03)
  
      If job:Date_Completed <> ''
          CompulsoryFieldCheck('C')
      Else!If job:Date_Completed <> ''
          CompulsoryFieldCheck('B')
      End!If job:Date_Completed <> ''
      
      If glo:ErrorText <> ''
          glo:errortext = 'You cannot complete booking due to the following error(s): <13,10>' & Clip(glo:errortext)
          Error_Text
          glo:errortext = ''
          Cycle
      Else!If error# = 1
          get(audit,0)
          if access:audit.primerecord() = level:benign
              aud:notes         = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                                  '<13,10>I.M.E.I. NO.: ' & Clip(job:esn) & |
                                  '<13,10>M.S.N.: ' & Clip(job:msn)
              If job:chargeable_job = 'YES'
                  aud:notes   = Clip(aud:notes) &  '<13,10,13,10>CHARGEABLE JOB: ' & Clip(job:chargeable_job) & |
                                                  '<13,10>CHARGE TYPE: ' & Clip(job:charge_type)
                  If job:repair_type <> ''
                      aud:notes = Clip(aud:notes) & '<13,10>REPAIR TYPE: ' & Clip(job:repair_type)
                  End!If job:charge_type <> ''
              End!If job:chargeable_job = 'YES'
              If job:warranty_job = 'YES'
                  aud:notes   = CLip(aud:notes) & '<13,10>WARRANTY JOB: ' & CLip(job:warranty_job) & |
                                  '<13,10>CHARGE TYPE: ' & Clip(job:warranty_charge_type)
                  If job:repair_type_warranty <> ''
                      aud:notes = Clip(aud:notes) & '<13,10>REPAIR TYPE: ' & Clip(job:repair_type_warranty)
                  End!If job:repair_type_warranty <> ''
              End!If job:warranty_job = 'YES'
  
              If job:estimate = 'YES'
                  aud:notes   = Clip(aud:notes) & '<13,10>ESTIMATE: ' & Clip(job:estimate) & |
                                                  'VALUE: ' & Clip(job:estimate_if_over)
              End!If job:estimate = 'YES'
  
              If ExchangeAccount(job:Account_Number)
                  aud:Notes   = Clip(aud:Notes) & '<13,10,13,10>EXCHANGE REPAIR'
              End !If ExchangeAccount(job:Account_Number)
  
              count# = 0
              save_jac_id = access:jobacc.savefile()
              access:jobacc.clearkey(jac:ref_number_key)
              jac:ref_number = job:ref_number
              set(jac:ref_number_key,jac:ref_number_key)
              loop
                  if access:jobacc.next()
                     break
                  end !if
                  if jac:ref_number <> job:ref_number      |
                      then break.  ! end if
                  count# += 1
                  aud:notes = Clip(aud:notes) & '<13,10>Accessory ' & Clip(count#) & ': ' & Clip(jac:accessory)
              end !loop
              access:jobacc.restorefile(save_jac_id)
  
              aud:ref_number    = job:ref_number
              aud:date          = Today()
              aud:time          = Clock()
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              aud:user = use:user_code
              aud:action        = 'NEW JOB BOOKING INITIAL ENTRY'
              if access:audit.insert()
                  access:audit.cancelautoinc()
              end
          end!if access:audit.primerecord() = level:benign
  
          !If this is a new job and it is for an Exchange Account, mark that the unit
          !assuming that it can be found in Exchange Database, that it is 'In Repair'
          If ExchangeAccount(job:Account_Number) And job:ESN <> ''
              Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
              xch:ESN = job:ESN
              If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Found
                  xch:Available = 'REP'
                  xch:Job_Number  = job:Ref_Number
                  If Access:EXCHANGE.TryUpdate() = Level:Benign
                      get(exchhist,0)
                      if access:exchhist.primerecord() = Level:Benign
                          exh:ref_number      = xch:ref_number
                          exh:date            = Today()
                          exh:time            = Clock()
                          access:users.clearkey(use:password_key)
                          use:password        = glo:password
                          access:users.fetch(use:password_key)
                          exh:user = use:user_code
                          exh:status          = 'EXCHANGE UNIT IN REPAIR ON JOB NO: ' & Clip(job:Ref_Number)
                          access:exchhist.insert()
                      end!if access:exchhist.primerecord() = Level:Benign
                  End !If Access:EXCHANGE.TryUpdate()
  
              Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
          End !If ExchangeAccount(job:Account_Number)
  
      End!If error# = 1
  Else
      CompulsoryFieldCheck('B')
      If glo:ErrorText <> ''
          glo:errortext = 'You cannot complete editing this job because of the following errors: <13,10>' & Clip(glo:errortext)
          Error_Text
          glo:errortext = ''
          Cycle
      End!If glo:ErrorText <> ''
  
      ! Start Change 2851 BE(02/07/03)
      IF (tmp:PrintJobcardNow) THEN
          glo:select1  = sav:ref_number
          Job_card
          glo:select1  = ''
      END
      ! End Change 2851 BE(02/07/03)
  
  End !If ThisWindow.request   = Insertrecord
  !Invoicing
  Set(defaults)
  access:Defaults.next()
  IF def:use_sage <> 'YES'
      Check_Access('AMEND INVOICED JOBS',x")
      If x" = True
          If job:invoice_number <> '' And job:chargeable_job = 'YES'
              If job:invoice_labour_cost <> job:labour_cost Or job:invoice_parts_cost <> job:parts_cost Or |
                  job:invoice_courier_cost <> job:courier_cost
                  Case MessageEx('Warning! The Chargeable part of this job has been invoiced and you have changed the Chargeable Costs.<13,10><13,10>If you continue, Invoice Number '&Clip(inv:invoice_Number)&' will be changed to these new values.<13,10>Are you sure?','ServiceBase 2000',|
                                 'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          get(audit,0)
                          if access:audit.primerecord() = Level:Benign
                              aud:notes         = 'NEW COSTS:-' & '<13,10>LABOUR: ' & Format(job:labour_cost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:parts_cost,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:courier_cost,@n14.2) & |
                                                  '<13,10><13,10>PREVIOUS COSTS:-' & '<13,10>LABOUR: ' & Format(job:invoice_labour_cost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:invoice_parts_cost,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:invoice_courier_cost,@n14.2)
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'CHARGEABLE INVOICED JOB AMENDED'
                              access:audit.insert()
                          end!if access:audit.primerecord() = Level:Benign
      
                          job:invoice_labour_cost = job:labour_cost
                          job:invoice_parts_cost  = job:parts_cost
                          job:invoice_courier_cost = job:courier_cost
                          access:invoice.clearkey(inv:invoice_number_key)
                          inv:invoice_number = job:invoice_number
                          if access:invoice.fetch(inv:invoice_number_key)
                              If inv:invoice_type = 'SIN'
                                  inv:total           = job:sub_total
                                  inv:labour_paid     = job:labour_cost
                                  inv:parts_paid      = job:parts_cost
                                  access:invoice.update()
                              End!If inv:invoice_type = 'SIN'
                          end
                      
                      Of 2 ! &No Button
                  End!Case MessageEx
              End!If job:invoice_labour_cost <> job:labour_cost Or job:invoice_parts_cost <> job:parts_cost Or |
          End!If job:invoice_number <> ''
          If job:invoice_number_warranty <> '' And job:warranty_job = 'YES'
              If job:winvoice_labour_cost <> job:labour_cost_warranty Or job:winvoice_parts_cost <> job:parts_cost_warranty Or |
                  job:winvoice_courier_cost <> job:courier_cost_warranty
                  Case MessageEx('Warning! The Warranty part of this job has been invoiced and you have changed the Warranty Costs.<13,10><13,10>If you continue, Invoice Number '&Clip(job:invoice_Number_warranty)&' will be changed to these new values.<13,10>Are you sure?','ServiceBase 2000',|
                                 'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          Get(audit,0)
                          if access:audit.primerecord() = Level:Benign
                              aud:notes         = 'NEW COSTS:-' & '<13,10>LABOUR: ' & Format(job:labour_cost_warranty,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:parts_cost_warranty,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:courier_cost_warranty,@n14.2) & |
                                                  '<13,10><13,10>PREVIOUS COSTS:-' & '<13,10>LABOUR: ' & Format(job:winvoice_labour_cost,@n14.2) & |
                                                  '<13,10>PARTS: ' & Format(job:winvoice_parts_cost,@n14.2) & |
                                                  '<13,10>COURIER: ' & Format(job:winvoice_courier_cost,@n14.2)
                              aud:ref_number    = job:ref_number
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'WARRANTY INVOICED JOB AMENDED'
                              access:audit.insert()
                          end!if access:audit.primerecord() = Level:Benign
      
                          job:winvoice_labour_cost = job:labour_cost_warranty
                          job:winvoice_parts_cost  = job:parts_cost_warranty
                          job:winvoice_courier_cost = job:courier_cost_warranty
                      Of 2 ! &No Button
                  End!Case MessageEx
              End!If job:invoice_labour_cost <> job:labour_cost Or job:invoice_parts_cost <> job:parts_cost Or |
          End!If job:invoice_number <> ''
      End!If x" = False
  End!IF def:use_sage <> 'YES'
  
  !Fill Fields
  If job:msn = ''
      ! Start Change 2651 BE(02/06/03)
      !job:msn = 'N/A'
      Access:manufact.Clearkey(man:manufacturer_key)
      man:manufacturer    = job:manufacturer
      IF (Access:manufact.Fetch(man:manufacturer_key) = Level:Benign) THEN
          IF (~man:DisallowNA) THEN
              job:msn = 'N/A'
          END
      ELSE
          job:msn = 'N/A'
      END
      ! End Change 2651 BE(02/06/03)
  End!If job:msn = ''
  !ANC Label
  If thiswindow.request = Insertrecord
      anc# = 0
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      if access:subtracc.fetch(sub:account_number_key) = level:benign
          access:tradeacc.clearkey(tra:account_number_key) 
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = level:benign
              if tra:use_sub_accounts = 'YES'
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = sub:courier_outgoing
                  if access:courier.tryfetch(cou:courier_key) = Level:Benign
                      If cou:courier_type = 'ANC'
                          anc# = 1
                      End!If cou:courier_type = 'ANC'
                  end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
              else!if tra:use_sub_accounts = 'YES'
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = tra:courier_outgoing
                  if access:courier.tryfetch(cou:courier_key) = Level:Benign
                      If cou:courier_type = 'ANC'
                          anc# = 1
                      End!If cou:courier_type = 'ANC'
                  end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
      IF anc# = 1
          label# = 0
          access:trantype.clearkey(trt:transit_type_key)
          trt:transit_type = job:transit_type
          if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              If trt:loan_unit = 'YES' or trt:exchange_unit = 'YES'
                  label# = 1
              End!If trt:loan_unit = 'YES' or trt:exchange_unit = 'YES'
          Else!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              label# = 0
          End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
      End!IF anc# = 1
  
      IF label# = 1
          glo:file_name = 'C:\ANC.TXT'
          Remove(expgen)
          If access:expgen.open()
              Stop(error())
              Return Level:Benign
          End!If access:expgen.open()
          access:expgen.usefile()
          Clear(gen:record)
          gen:line1   = job:ref_number
          gen:line1   = Sub(gen:line1,1,10) & job:company_name
          gen:line1   = Sub(gen:line1,1,40) & job:address_line1_delivery
          gen:line1   = Sub(gen:line1,1,70) & job:address_line2_delivery
          gen:line1   = Sub(gen:line1,1,100) & job:address_line3_delivery
          gen:line1   = Sub(gen:line1,1,130) & job:postcode_delivery
          gen:line1   = Sub(gen:line1,1,150) & job:telephone_delivery
          gen:line1   = Sub(gen:line1,1,175) & job:unit_type
          gen:line1   = Sub(gen:line1,1,195) & job:model_number
          gen:line1   = Sub(gen:line1,1,215) & cou:ContractNumber
          access:expgen.insert()
          Close(expgen)
          access:expgen.close()
          cou:anccount += 1
          If cou:ANCCount = 999
              cou:ANCCount = 1
          End
          access:courier.update()
          Set(Defaults)
          access:defaults.next()
          job:incoming_consignment_number = def:anccollectionno
          DEF:ANCCollectionNo += 10
          access:defaults.update()
          sav:path    = path()
          setpath(Clip(cou:ancpath))
          Run('ancpaper.exe',1)
          Setpath(sav:path)
  
      End!IF label# = 1
  End!If thiswindow.request = Insertrecord
  
  !Did Anything Change?
  Include('jobchnge.inc','Audit')
  
  If Old_Exchange_Unit_Number_Temp = 0 And job:Exchange_Unit_Number <> 0
      If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          If Access:AUDIT.PrimeRecord() = Level:Benign
              aud:Notes         = Clip(jobe:ExchangeReason)
              aud:Ref_Number    = job:ref_number
              aud:Date          = Today()
              aud:Time          = Clock()
              aud:Type          = 'EXC'
              Access:USERS.ClearKey(use:Password_Key)
              use:Password      = glo:Password
              Access:USERS.Fetch(use:Password_Key)
              aud:User          = use:User_Code
              aud:Action        = 'EXCHANGE UNIT ATTACHED: REASON'
              Access:AUDIT.Insert()
          End!If Access:AUDIT.PrimeRecord() = Level:Benign
      End !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  End !Old_Exchange_UNit_Number = 0 And job:Exchange_Unit_Number <> 0
  
  If Old_Loan_Unit_Number_Temp = 0 And job:Loan_Unit_Number <> 0
      If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          If Access:AUDIT.PrimeRecord() = Level:Benign
              aud:Notes         = Clip(jobe:LoanReason)
              aud:Ref_Number    = job:ref_number
              aud:Date          = Today()
              aud:Time          = Clock()
              aud:Type          = 'LOA'
              Access:USERS.ClearKey(use:Password_Key)
              use:Password      = glo:Password
              Access:USERS.Fetch(use:Password_Key)
              aud:User          = use:User_Code
              aud:Action        = 'LOAN UNIT ATTACHED: REASON'
              Access:AUDIT.Insert()
          End!If Access:AUDIT.PrimeRecord() = Level:Benign
      End !If GETINI('EXCHANGE','RecordReason',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  End !Old_Exchange_UNit_Number = 0 And job:Exchange_Unit_Number <> 0
  
  !Check Accesories incase they change
  Free(SaveAccessoryQueue)
  Clear(SaveAccessoryQueue)
  
  Open(SaveAccessory)
  If ~Error()
      Save_jac_ID = Access:JOBACC.SaveFile()
      Access:JOBACC.ClearKey(jac:Ref_Number_Key)
      jac:Ref_Number = job:Ref_Number
      Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
      Loop
          If Access:JOBACC.NEXT()
             Break
          End !If
          If jac:Ref_Number <> job:Ref_Number      |
              Then Break.  ! End If
          Clear(savacc:Record)
          savacc:Accessory    = jac:Accessory
          Get(SaveAccessory,savacc:AccessoryKey)
          If Error()
              !New Accessory Added
              savaccq:Accessory   = jac:Accessory
              savaccq:Type        = 1
              Add(SaveAccessoryQueue)
          End !If Error()
      End !Loop
      Access:JOBACC.RestoreFile(Save_jac_ID)
  
      Clear(savacc:record)
      Set(SaveAccessory,0)
      Loop
          Next(SaveAccessory)
          If Error()
              Break
          End !If Error()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          jac:Accessory  = savacc:Accessory
          If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Found
  
          Else!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              !Accessory Deleted
              savaccq:Accessory   = savacc:Accessory
              savaccq:Type        = 0
              Add(SaveAccessoryQueue)
          End!If Access:JOBACC.TryFetch(jac:Ref_Number_Key) = Level:Benign
      End !Loop
  
      If Records(SaveAccessoryQueue)
          !Some accessories have been changed
          If Access:AUDIT.PrimeRecord() = Level:Benign
              Sort(SaveAccessoryQueue,savaccq:Accessory,savaccq:Type)
              Loop x# = 1 To Records(SaveAccessoryQueue)
                  Get(SaveAccessoryQueue,x#)
                  aud:Notes   = Clip(aud:Notes) & '<13,10>' & Clip(savaccq:Accessory)
                   Case savaccq:Type
                      Of 0
                          aud:Notes = Clip(aud:Notes) & ' -DELETED'
                      Of 1
                          aud:Notes = Clip(aud:Notes) & ' -ADDED'
                   End !Case savaccq:Type
              End !Loop x# = 1 To Records(SaveAccessoryQueue)
  
              !Cut out the line break at the beginning
              aud:Notes         = Sub(aud:Notes,3,Len(aud:notes))
              aud:Ref_Number    = job:ref_number
              aud:Date          = Today()
              aud:Time          = Clock()
              aud:Type          = 'JOB'
              Access:USERS.ClearKey(use:Password_Key)
              use:Password      = glo:Password
              Access:USERS.Fetch(use:Password_Key)
              aud:User          = use:User_Code
              aud:Action        = 'JOB ACCESSORIES AMENDED'
              Access:AUDIT.Insert()
          End!If Access:AUDIT.PrimeRecord() = Level:Benign
      End !If Records(SaveAccessoryQueue)
      Free(SaveAccessoryQueue)
  End !Error()
  Close(SaveAccessory)
  Remove(SaveAccessory)
  
  
  ReturnValue = PARENT.TakeCompleted()
  If thiswindow.request = Insertrecord
      !Start Change 2851 BE(02/07/03)
      !If tmp:printjobcard = 1
      IF ((tmp:printjobcard = 1) OR  (tmp:PrintJobcardNow)) THEN
      !End Change 2851 BE(02/07/03)
          glo:select1  = sav:ref_number
          Job_card
          glo:select1  = ''
      End!If tmp:printjobcard = 1
      If tmp:printjobreceipt = 1
          glo:select1  = sav:ref_number
          Job_Receipt
          glo:select1  = ''
      End!If tmp:printjobreceipt = 1
  End!If thiswindow.request = Insertrecord
  !!Do the automatic despatch
  !If tmp:DespatchClose    = 1
  !    despatch# = 1
  !    access:subtracc.clearkey(sub:account_number_key)
  !    sub:account_number  = job:account_number
  !    If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
  !        access:tradeacc.clearkey(tra:account_number_key)
  !        tra:account_number  = sub:main_account_number
  !        If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
  !            If tra:skip_despatch = 'YES'
  !                despatch# = 0
  !            End!If tra:skip_despatch = 'YES'
  !        End!If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
  !    End!If access;subtracc.tryfetch(sub:account_number_key) = Level:Benign
  !    If despatch# = 1
  !        access:courier.clearkey(cou:courier_key)
  !        cou:courier = job:courier
  !        if access:courier.tryfetch(cou:courier_key)
  !            Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
  !                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                Of 1 ! &OK Button
  !            End!Case MessageEx
  !        Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
  !            If access:desbatch.primerecord() = Level:Benign
  !                If access:desbatch.tryinsert()
  !                    access:desbatch.cancelautoinc()
  !                Else!If access:despatch.tryinsert()
  !                    Include('despsing.inc')
  !                End!If access:despatch.tryinsert()
  !            End!If access:desbatch.primerecord() = Level:Benign
  !        End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
  !    End!If despatch# = 1
  !End!If tmp:DespatchClose    = 1
  !
  !If print_despatch_note_temp = 'YES'
  !    
  !    restock# = 0
  !    glo:select1  = job:ref_number
  !    If ToBeExchanged()
  !    !Has this unit got an Exchange, or is it expecting an exchange
  !        restock# = 1
  !    Else!If job:exchange_unit_number
  !        !Does the transit type expect an exchange?
  !        access:trantype.clearkey(trt:transit_type_key)
  !        trt:transit_type = job:transit_type
  !        if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
  !            if trt:exchange_unit = 'YES'
  !                restock# = 1
  !            End!if trt:exchange_unit = 'YES'
  !        End!if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
  !    End!If job:exchange_unit_number <> ''
  !    !If there is an loan unit attached, assume that a despatch note is required.
  !    If job:loan_unit_number <> ''
  !!        If job:despatched <> 'LAT'
  !!            restock# = 1
  !!        Else!If job:despatched <> 'LAT'
  !            restock# = 0
  !!        End!If job:despatched <> 'LAT'
  !    End!If job:loan_unit_number <> ''
  !
  !    If restock# = 1
  !        restocking_note
  !    Else!If restock# = 1
  !        despatch_note
  !    End!If restock# = 1
  !    glo:select1  = ''
  !End!If print_despatch_note_temp = 'YES'
  !
  !
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateJOBS')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?job:Date_Despatched
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?despatched_Calendar:3)
      CYCLE
    END
  OF ?job:Exchange_Despatched
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?Despatched_Calendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?job:Account_Number
    CASE EVENT()
    OF EVENT:AlertKey
      Post(event:accepted,?LookupTradeAccount)
    END
  OF ?job:Transit_Type
    CASE EVENT()
    OF EVENT:AlertKey
      Post(Event:Accepted,?LookupTransitType)
    END
  OF ?job:Location
    CASE EVENT()
    OF EVENT:AlertKey
      Post(Event:Accepted,?LookupLocation)
    END
  OF ?job:Model_Number
    CASE EVENT()
    OF EVENT:AlertKey
      Post(Event:accepted,?LookupModelNumber)
    END
  OF ?job:Charge_Type
    CASE EVENT()
    OF EVENT:AlertKey
      If Keycode() = MouseRight
         Execute Popup('Lookup Charge Type')
             Post(Event:Accepted,?LookupChargeType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupChargeType)
      End!If Keycode() = MouseRight
    END
  OF ?job:Warranty_Charge_Type
    CASE EVENT()
    OF EVENT:AlertKey
      If Keycode() = MouseRight
         Execute Popup('Lookup Charge Types')
             Post(Event:Accepted,?LookupWarrantyChargeType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupWarrantyChargeType)
      End!If Keycode() = MouseRight
    END
  OF ?job:Repair_Type
    CASE EVENT()
    OF EVENT:AlertKey
      If Keycode() = MouseRight
         Execute Popup('Lookup Charge Type')
             Post(Event:Accepted,?LookupChargeableChargeType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupChargeableChargeType)
      End!If Keycode() = MouseRight
    END
  OF ?job:Repair_Type_Warranty
    CASE EVENT()
    OF EVENT:AlertKey
      If Keycode() = MouseRight
         Execute Popup('Lookup Repair Type')
             Post(Event:Accepted,?LookupWarrantyRepairType)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupWarrantyRepairType)
      End!If Keycode() = MouseRight
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Auto_Search
      If ThisWindow.Request <> Insertrecord
          Select(?Lookup_Engineer)
          Display()
      End
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F10Key
              Post(Event:Accepted,?Today_Button)
      End !KeyCode()
    OF EVENT:OpenWindow
      quickwindow{prop:buffer} = 2
      If ThisWindow.Request = InsertRecord
          Select(?Sheet4,1)
          Unhide(?booking1_tab)
          access:users.clearkey(use:password_key)
          use:password    =glo:password
          access:users.fetch(use:password_key)
          job:who_booked  = use:user_code
          get(jobstage,0)
          if access:jobstage.primerecord() = level:benign
          
              jst:ref_number = job:ref_number
              jst:job_stage  = '000 NEW JOB'
              jst:date       = Today()
              jst:time       = Clock()
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              jst:user = use:user_code
              if access:jobstage.insert()
                  access:jobstage.cancelautoinc()
              end
              brw21.resetsort(1)
          end!if access:jobstage.primerecord() = level:benign
          Select(?job:auto_search)
      
      
      Else
          Select(?Sheet4,2)
          Unhide(?booking2_tab)
      End
      !Titles
      Do Transit_Type
      Do Location_Type
      Do Update_accessories
      Do Loan_Box
      Do Exchange_Box
      Do Third_Party_Repairer
      Do Time_Remaining
      Do Trade_Account_Status
      Do Estimate_Check
      Do Show_Tabs
      Do CountParts
      Do Charge_Type
      Do Titles
      
      !Lookups
      Do Update_Engineer
      Do Update_Loan
      Do Update_Exchange
      
      Post(Event:accepted,?job:charge_type)
      Post(event:accepted,?job:chargeable_job)
      Post(event:accepted,?job:warranty_job)
      
      Do Estimate
      Do QA_Group
      
      !Price Job On Entry
      Do Pricing
      
      Do show_hide_tabs
      Do skip_despatch
      
      
      !Workshop Tick
            If ThisWindow.Request <> Insertrecord
                Unhide(?job:workshop)
                ?tmp:InWorkshopDate{prop:Hide} = 0
                ?tmp:InWorkshopTime{prop:Hide} = 0
                Unhide(?job:location_type:prompt)
            End
      
      !Security Check
      Check_Access('JOB LABOUR COSTS - EDIT',x")
      If x" = False
          ?job:labour_cost_estimate{prop:readonly} = 1
          ?job:labour_cost{prop:readonly} = 1
          ?job:labour_cost_warranty{prop:readonly} = 1
          ?job:labour_cost_estimate{prop:color} = color:silver
          ?job:labour_cost{prop:color} = color:silver
          ?job:labour_cost_warranty{prop:color} = color:silver
      Else!If x" = False
          ?job:labour_cost_estimate{prop:readonly} = 0
          ?job:labour_cost{prop:readonly} = 0
          ?job:labour_cost_warranty{prop:readonly} = 0
          ?job:labour_cost_estimate{prop:color} = color:white
          ?job:labour_cost{prop:color} = color:white
          ?job:labour_cost_warranty{prop:color} = color:white
      
      End!"If x" = False
      Check_Access('JOB COURIER COSTS - EDIT',x")
      If x" = False
          ?job:courier_cost_estimate{prop:readonly} = 1
          ?job:courier_cost{prop:readonly} = 1
          ?job:courier_cost_warranty{prop:readonly} = 1
          ?job:courier_cost_estimate{prop:color} = color:silver
          ?job:courier_cost{prop:color} = color:silver
          ?job:courier_cost_warranty{prop:color} = color:silver
      
      Else!If x" = False
          ?job:courier_cost_estimate{prop:readonly} = 0
          ?job:courier_cost{prop:readonly} = 0
          ?job:courier_cost_warranty{prop:readonly} = 0
          ?job:courier_cost_estimate{prop:color} = color:white
          ?job:courier_cost{prop:color} = color:white
          ?job:courier_cost_warranty{prop:color} = color:white
      
      End!If x" = False
      Check_Access('AMEND COMPLETED JOBS',x")
      If x" = False
          If job:date_completed <> ''
              Hide(?OK)
          End!If job:date_completed <> ''
      Else!If x" = False
          UnHide(?OK)
      End!If x" = False
      Set(defaults)
      access:Defaults.next()
      If def:use_sage <> 'YES'
          Check_Access('AMEND INVOICED JOBS',x")
          If x" = False
              If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
                  If job:invoice_number <> '' And job:invoice_number_warranty <> ''
                      Hide(?OK)
                  End!If job:invoice_number <> '' And job:invoice_number_warranty <> ''
              End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
              If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
                  If job:invoice_number <> ''
                      Hide(?OK)
                  End!If job:invoice_number <> ''
              End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
              If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
                  If job:invoice_number_warranty <> ''
                      Hide(?OK)
                  End!If job:invoice_number_warranty <> ''
              End!If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
          Else!If x" = False
              UnHide(?OK)
          End!If x" = False
      Else!If def:use_sage <> 'YES'
          If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
              If job:invoice_number <> '' And job:invoice_number_warranty <> ''
                  Hide(?OK)
              End!If job:invoice_number <> '' And job:invoice_number_warranty <> ''
          End!If job:chargeable_job = 'YES' And job:warranty_job = 'YES'
          If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
              If job:invoice_number <> ''
                  Hide(?OK)
              End!If job:invoice_number <> ''
          End!If job:chargeable_job = 'YES' And job:warranty_job <> 'YES'
          If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
              If job:invoice_number_warranty <> ''
                  Hide(?OK)
              End!If job:invoice_number_warranty <> ''
          End!If job:chargeable_job <> 'YES' And job:warranty_job = 'YES'
      End!If def:use_sage <> 'YES'
      
      Check_Access('JOBS - AMEND DESPATCH',x")
      If x" = False
          Hide(?amend_despatch)
          Hide(?amend_despatch:2)
          Hide(?amend_despatch:3)
      Else!If x" = False
          If job:exchange_consignment_number
              UnHide(?amend_despatch)
          End!If job:exchange_consignment_number
          If job:loan_consignment_number
              UnHide(?amend_despatch:2)
          End!If job:loan_consignment_number
          If job:consignment_number
              UnHide(?amend_despatch:3)
          End!If job:consignment_number
      End!If x" = False
      
      
      !MSN?!
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If man:Use_MSN = 'YES'
              ?job:MSN{prop:Hide} = 0
              ?job:MSN:Prompt{prop:Hide} = 0
          Else !If man:Use_MSN = 'YES'
              ?job:MSN{prop:Hide} = 1
              ?job:MSN{prop:Hide} = 1
          End !If man:Use_MSN = 'YES'
      End
      !Save Fields
      account_number_temp     = job:account_number
      repair_type_temp        = job:repair_type
      warranty_repair_Type_temp   = job:repair_type_warranty
      transit_type_temp       = job:transit_type
      location_type_temp      = job:location_Type
      model_number_temp       = job:model_number
      location_temp           = job:location
      workshop_temp           = job:workshop
      charge_type_temp        = job:charge_type
      estimate_temp           = job:estimate
      msn_temp                = job:msn
      esn_temp                = job:esn
      unit_type_temp          = job:unit_type
      current_status_temp     = job:current_status
      order_number_temp       = job:order_number
      surname_temp            = job:surname
      date_completed_temp     = job:date_completed
      time_completed_temp     = job:time_completed
      engineer_temp           = job:engineer
      warranty_charge_type_temp = job:warranty_charge_type
      warranty_job_temp       = job:warranty_job
      chargeable_job_temp     = job:chargeable_job
      estimate_ready_temp     = job:estimate_ready
      in_repair_temp          = job:in_repair
      on_test_temp            = job:on_test
      qa_passed_temp          = job:qa_passed
      qa_rejected_temp        = job:qa_rejected
      qa_second_passed_temp   = job:qa_second_passed
      estimate_accepted_temp  = job:estimate_accepted
      estimate_rejected_temp  = job:estimate_rejected
      courier_temp            = job:courier
      loan_courier_temp       = job:loan_courier
      exchange_courier_temp   = job:exchange_courier
      Ignore_Chargeable_Charges_Temp  = job:Ignore_Chargeable_Charges
      Ignore_Warranty_Charges_Temp    = job:Ignore_Warranty_Charges
      Ignore_Estimate_Charges_Temp    = job:Ignore_Estimate_Charges
      exchange_despatched_temp         = JOB:Exchange_Despatched
      exchange_consignment_number_temp      = JOB:Exchange_Consignment_Number
      exchange_despatched_user_temp    = JOB:Exchange_Despatched_User
      exchange_despatch_number_temp  = JOB:Exchange_Despatch_Number
      loan_despatched_temp               = job:loan_despatched
      loan_consignment_number_temp     = job:loan_consignment_number
      loan_Despatch_number_temp      = job:loan_despatch_number
      loan_despatched_user_temp        = job:loan_despatched_user
      date_despatched_temp             = JOB:Date_Despatched
      despatch_number_temp             = JOB:Despatch_Number
      despatch_user_temp               = JOB:Despatch_User
      consignment_number_temp          = JOB:Consignment_Number
      
      !Save Fields In Case Of Cancel
      old_exchange_unit_number_temp   = job:exchange_unit_number
      old_loan_unit_number_temp       = job:loan_unit_number
      !Will these fields be changed?
      Include('jobchnge.inc','Save')
      !History Button
      history_run_temp = 0
      If ThisWindow.Request <> insertrecord
          Do History_search2
          If glo:select1 <> ''
              ?history_button{prop:text} = 'History'
          Else
              ?history_button{prop:text} = 'No History'
          End
      End
      Do Last_repair_days
      !Show Fields
      Set(defaults)
      access:defaults.next()
      If def:show_mobile_number = 'YES'
          Hide(?job:mobile_number)
          Hide(?job:mobile_number:prompt)
          Hide(?job:mobile_number:2)
          Hide(?mobilenumberdop)
      Else
          Unhide(?job:mobile_number)
          Unhide(?job:mobile_number:prompt)
          unhide(?job:mobile_number:2)
          Unhide(?mobilenumberdop)
      End
      
      If def:HideColour   = 'YES'
          Hide(?job:Colour)
          ?UnitTypeColour{prop:Text} = 'Unit Type'
      Else!If def:HideColour   = 'YES'
          Unhide(?job:colour)
          ?Prompt:Job:Unit_Type{prop:text} = 'Unit Type / Colour'
          
          If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
              ?Prompt:Job:Unit_Type{prop:Text} = 'Unit Type / ' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
              ?UnitTypeColour{prop:Text} = 'Unit Type / ' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
          End !Case ThisWindow.Request
      End!If def:HideColour   = 'YES'
      
      
      If def:hide_authority_number = 'YES'
          Hide(?job:authority_number)
          Hide(?job:authority_number:prompt)
      Else
          UnHide(?job:authority_number)
          UnHide(?job:authority_number:prompt)
      End
      
      If def:HideProduct = 'YES'
          Hide(?job:ProductCode)
          Hide(?job:ProductCode:prompt)
      End!If def:HideProduct = 'YES'
      
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      if access:subtracc.fetch(sub:account_number_key) = level:benign
          access:tradeacc.clearkey(tra:account_number_key) 
          tra:account_number = sub:main_account_number
          if access:tradeacc.fetch(tra:account_number_key) = level:benign
              if tra:use_contact_name = 'YES'
                  Unhide(?NameGroup)
                  Unhide(?Customer_Name_String_Temp:Prompt)
                  Unhide(?Customer_Name_String_Temp)
              else!if tra:use_sub_accounts = 'YES'
                  Hide(?NameGroup)
                  Hide(?Customer_Name_String_Temp:Prompt)
                  Hide(?Customer_Name_String_Temp)
              end!if tra:use_sub_accounts = 'YES'
          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
      
      !Start - Hide insurance if default set to. - TrkBs: 5589 (DBH: 21-03-2005)
      If def:Hide_Insurance = 'YES'
          ?job:Insurance_Reference_Number{prop:Hide} = True
          ?job:Insurance_Reference_Number:Prompt{prop:Hide} = True
      End ! def:Hide_Insurance = 'YES'
      !End   - Hide insurance if default set to. - TrkBs: 5589 (DBH: 21-03-2005)
      !Select First Tab
      If Thiswindow.request <> Insertrecord
          Select(?Lookup_Engineer)
      End
      !Paid To Date
      paid_chargeable_temp = ''
      paid_warranty_temp = ''
      setcursor(cursor:wait)
      save_jpt_id = access:jobpaymt.savefile()
      access:jobpaymt.clearkey(jpt:all_date_key)
      jpt:ref_number = job:ref_number
      set(jpt:all_date_key,jpt:all_date_key)
      loop
          if access:jobpaymt.next()
             break
          end !if
          if jpt:ref_number <> job:ref_number      |
              then break.  ! end if
          yldcnt# += 1
          if yldcnt# > 25
             yield() ; yldcnt# = 0
          end !if
          paid_chargeable_temp += jpt:amount
      end !loop
      access:jobpaymt.restorefile(save_jpt_id)
      setcursor()
      !Invoiced
      If job:invoice_number <> ''
          job:courier_cost = job:invoice_courier_cost
          job:labour_cost  = job:invoice_labour_cost
          job:parts_cost   = job:invoice_parts_cost
      End!If job:invoice_number <> ''
      If job:invoice_number_warranty <> ''
          job:courier_cost_warranty = job:winvoice_courier_cost
          job:labour_cost_warranty  = job:winvoice_labour_cost
          job:parts_cost_warranty  =  job:winvoice_parts_cost
      End!If job:invoice_number_warranty <> ''
      
      Case job:edi
          Of 'EXC'
              Unhide(?failed_warranty_claim)
              Unhide(?resubmit_button)
          Of 'YES'
              Unhide(?failed_warranty_Claim)
              ?failed_warranty_claim{prop:text} = 'Claim Made'
          Of 'FIN'
              Unhide(?failed_warranty_Claim)
              ?failed_warranty_claim{prop:text} = 'Claim Reconcilled'
      End!Case job:edi
      
      If job:invoiceStatus    = 'EXC'
          Hide(?CharVat)
          Hide(?CharTotal)
          Hide(?vat_chargeable_temp)
          Hide(?total_temp)
          Unhide(?CharRejected)
          Unhide(?UnRejectButton)
      End!If job:invoiceStatus    = 'EXC'
      !Cancelled Job?
      If job:cancelled = 'YES'
          Hide(?OK)
          Unhide(?canceltext)
      Else
          If jobe:FailedDelivery
              ?Canceltext{prop:Hide} = 0
              ?CancelText{prop:Text} = 'FAILED DELIVERY'
          End !If jobe:FailedDelivery
      End!If job:cancelled = 'YES'
      
      !thismakeover.refresh()
      !Date Completed Text
        If job:Date_completed = ''
           If job:Estimate = 'YES' and (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES') And |
              ?job:Estimate{prop:hide} = 0
              ?JobCompleted{prop:Text} = 'ESTIMATE JOB'
              ?JobCompleted{prop:fontcolor} = color:purple
              Unhide(?JobCompleted)
           End !If job:Estimate = 'YES' and (job:Estimate_Accepted <> 'YES' and job:Estimate_Rejected <> 'YES')
        Else !If job:Date_completed = ''
            ?JobCompleted{prop:Text} = 'JOB COMPLETED'
            ?jobCompleted{prop:FontColor} = color:Green
            Unhide(?jobcompleted)
        End!If job:Date_completed <> ''
      Do CheckRequiredFields
    OF EVENT:GainFocus
      Do Fill_Lists
    OF EVENT:Timer
      DO Totals
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    If AccessoryCheck('JOB')
        If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 1
        Else!If AccessoryMismatch(job:Ref_Number,'JOB')
            local:Errorcode = 2
        End !If AccessoryMismatch(job:Ref_Number,'JOB')
    End !If AccessoryCheck('JOB')
    If Access:AUDIT.PrimeRecord() = Level:Benign

        Case local:Errorcode
            Of 0 Orof 2
                aud:Notes         = 'ACCESSORIES:<13,10>'
            Of 1
                aud:Notes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
        End !Case local:Errorcode

        !Add the list of accessories to the Audit Trail
        If job:Despatch_Type <> 'LOAN'
            Save_jac_ID = Access:JOBACC.SaveFile()
            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
            jac:Ref_Number = job:Ref_Number
            Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
            Loop
                If Access:JOBACC.NEXT()
                   Break
                End !If
                If jac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
            End !Loop
            Access:JOBACC.RestoreFile(Save_jac_ID)
        Else !If job:Despatch_Type <> 'LOAN'
            Save_lac_ID = Access:LOANACC.SaveFile()
            Access:LOANACC.ClearKey(lac:Ref_Number_Key)
            lac:Ref_Number = job:Ref_Number
            Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
            Loop
                If Access:LOANACC.NEXT()
                   Break
                End !If
                If lac:Ref_Number <> job:Ref_Number      |
                    Then Break.  ! End If
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(lac:Accessory)
            End !Loop
            Access:LOANACC.RestoreFile(Save_lac_ID)
        End !If job:Despatch_Type <> 'LOAN'

        Case local:Errorcode
            Of 1
                !If Error show the Accessories that were actually tagged
                aud:Notes         = Clip(aud:Notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
                Clear(glo:Queue)
                Loop x# = 1 To Records(glo:Queue)
                    Get(glo:Queue,x#)
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(glo:Pointer)
                End !Loop x# = 1 To Records(glo:Queue).
        End!Case local:Errorcode


        aud:Ref_Number    = job:ref_number
        aud:Date          = Today()
        aud:Time          = Clock()
        aud:Type          = 'JOB'
        Access:USERS.ClearKey(use:Password_Key)
        use:Password      = glo:Password
        Access:USERS.Fetch(use:Password_Key)
        aud:User          = use:User_Code
        Case local:ErrorCode
            Of 0
                aud:Action        = 'COMPLETION VALIDATION: PASSED ACCESSORIES'
            Of 1
                aud:Action        = 'COMPLETION VALIDATION: FAILED ACCESSORIES'
            Of 2
                aud:Action        = 'COMPLETION VALIDATION: PASSED ACCESSORIES (FORCED)'
        End !Case local:ErrorCode
        
        Access:AUDIT.Insert()

        !Do not complete
        If local:ErrorCode = 1
            Return Level:Fatal
        End !If local:ErrorCode = 1
    End!If Access:AUDIT.PrimeRecord() = Level:Benign

    Return Level:Benign
UpdateRFBoardIMEI PROCEDURE (LONG partrefnum, STRING partnum)
    code
    ! Start Change 2116 BE(26/06/03)
    access:stock.clearkey(sto:ref_number_key)
    sto:ref_number = partrefnum
    IF (access:stock.fetch(sto:ref_number_key) = Level:benign) THEN
        IF (sto:RF_BOARD) THEN
            glo:select2 = ''
            glo:select3 = job:model_number
            glo:select4 = partnum
            SETCURSOR()
            EnterNewIMEI
            SETCURSOR(cursor:wait)
            tmp_RF_Board_IMEI = CLIP(glo:select2)
            glo:select2 = ''
            glo:select3 = ''
            glo:select4 = ''
            IF (tmp_RF_Board_IMEI = '') THEN
                RETURN
            END
            GET(audit,0)
            IF (access:audit.primerecord() = level:benign) THEN
                aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                     '<13,10>Old IMEI ' & Clip(job:esn)
                aud:ref_number    = job:ref_number
                aud:date          = Today()
                aud:time          = Clock()
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                IF (access:audit.insert() <> Level:benign) THEN
                    access:audit.cancelautoinc()
                END
            END
            access:JOBSE.clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                IF (access:JOBSE.primerecord() = Level:Benign) THEN
                    jobe:RefNumber  = job:Ref_Number
                    IF (access:JOBSE.tryinsert()) THEN
                        access:JOBSE.cancelautoinc()
                    END
                END
            END
            IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                jobe:PRE_RF_BOARD_IMEI = job:ESN
                access:jobse.update()
            END
            job:ESN = tmp_RF_Board_IMEI
            access:jobs.update()
        END
    END
    ! End Change 2116 BE(26/06/03)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW12.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW12.SetQueueRecord PROCEDURE

  CODE
  If par:adjustment <> 'YES'
      If par:exclude_from_order = 'YES'
          ordered_temp = 'EXCLUDED'
      Else!If par:exclude_from_order = 'YES'
          If par:order_number <> ''
              If par:date_received = ''
                  ordered_temp = 'ON ORDER'
              Else!If par:date_received = ''
                  ordered_temp    = 'RECD: ' & Format(par:date_received,@d6b)
              End!If par:date_received = ''
          Else!If par:order_number <> ''
              If par:pending_ref_number <> ''
                  ordered_temp = 'PENDING'
              Else!If par:pending_ref_number <> ''
                  If par:part_ref_number <> ''
                      ordered_temp = 'FROM STOCK'
                  Else!If par:part_ref_number <> ''
                      ordered_temp = 'EXCLUDED'
                  End!If par:part_ref_number <> ''
              End!If par:pending_ref_number <> ''
          End!If par:order_number <> ''
      End!If par:exclude_from_order = 'YES'
  Else
      ordered_temp = 'N/A'
  End
  chargeable_part_total_temp  = par:quantity * par:sale_cost
  PARENT.SetQueueRecord
  SELF.Q.par:Part_Number_NormalFG = -1
  SELF.Q.par:Part_Number_NormalBG = -1
  SELF.Q.par:Part_Number_SelectedFG = -1
  SELF.Q.par:Part_Number_SelectedBG = -1
  SELF.Q.par:Description_NormalFG = -1
  SELF.Q.par:Description_NormalBG = -1
  SELF.Q.par:Description_SelectedFG = -1
  SELF.Q.par:Description_SelectedBG = -1
  SELF.Q.par:Order_Number_NormalFG = -1
  SELF.Q.par:Order_Number_NormalBG = -1
  SELF.Q.par:Order_Number_SelectedFG = -1
  SELF.Q.par:Order_Number_SelectedBG = -1
  SELF.Q.ordered_temp_NormalFG = -1
  SELF.Q.ordered_temp_NormalBG = -1
  SELF.Q.ordered_temp_SelectedFG = -1
  SELF.Q.ordered_temp_SelectedBG = -1
  SELF.Q.par:Quantity_NormalFG = -1
  SELF.Q.par:Quantity_NormalBG = -1
  SELF.Q.par:Quantity_SelectedFG = -1
  SELF.Q.par:Quantity_SelectedBG = -1
  SELF.Q.chargeable_part_total_temp_NormalFG = -1
  SELF.Q.chargeable_part_total_temp_NormalBG = -1
  SELF.Q.chargeable_part_total_temp_SelectedFG = -1
  SELF.Q.chargeable_part_total_temp_SelectedBG = -1
  SELF.Q.par:Part_Ref_Number_NormalFG = -1
  SELF.Q.par:Part_Ref_Number_NormalBG = -1
  SELF.Q.par:Part_Ref_Number_SelectedFG = -1
  SELF.Q.par:Part_Ref_Number_SelectedBG = -1
   
   
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Part_Number_NormalFG = 32768
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Part_Number_NormalFG = 255
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Part_Number_NormalFG = 8421504
     SELF.Q.par:Part_Number_NormalBG = 16777215
     SELF.Q.par:Part_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Part_Number_NormalFG = -1
     SELF.Q.par:Part_Number_NormalBG = -1
     SELF.Q.par:Part_Number_SelectedFG = -1
     SELF.Q.par:Part_Number_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Description_NormalFG = 32768
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Description_NormalFG = 255
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Description_NormalFG = 8421504
     SELF.Q.par:Description_NormalBG = 16777215
     SELF.Q.par:Description_SelectedFG = 16777215
     SELF.Q.par:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Description_NormalFG = -1
     SELF.Q.par:Description_NormalBG = -1
     SELF.Q.par:Description_SelectedFG = -1
     SELF.Q.par:Description_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Order_Number_NormalFG = 32768
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Order_Number_NormalFG = 255
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Order_Number_NormalFG = 8421504
     SELF.Q.par:Order_Number_NormalBG = 16777215
     SELF.Q.par:Order_Number_SelectedFG = 16777215
     SELF.Q.par:Order_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Order_Number_NormalFG = -1
     SELF.Q.par:Order_Number_NormalBG = -1
     SELF.Q.par:Order_Number_SelectedFG = -1
     SELF.Q.par:Order_Number_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.ordered_temp_NormalFG = 32768
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.ordered_temp_NormalFG = 255
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.ordered_temp_NormalFG = 8421504
     SELF.Q.ordered_temp_NormalBG = 16777215
     SELF.Q.ordered_temp_SelectedFG = 16777215
     SELF.Q.ordered_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.ordered_temp_NormalFG = -1
     SELF.Q.ordered_temp_NormalBG = -1
     SELF.Q.ordered_temp_SelectedFG = -1
     SELF.Q.ordered_temp_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Quantity_NormalFG = 32768
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Quantity_NormalFG = 255
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Quantity_NormalFG = 8421504
     SELF.Q.par:Quantity_NormalBG = 16777215
     SELF.Q.par:Quantity_SelectedFG = 16777215
     SELF.Q.par:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Quantity_NormalFG = -1
     SELF.Q.par:Quantity_NormalBG = -1
     SELF.Q.par:Quantity_SelectedFG = -1
     SELF.Q.par:Quantity_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.chargeable_part_total_temp_NormalFG = 32768
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.chargeable_part_total_temp_NormalFG = 255
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.chargeable_part_total_temp_NormalFG = 8421504
     SELF.Q.chargeable_part_total_temp_NormalBG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedFG = 16777215
     SELF.Q.chargeable_part_total_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.chargeable_part_total_temp_NormalFG = -1
     SELF.Q.chargeable_part_total_temp_NormalBG = -1
     SELF.Q.chargeable_part_total_temp_SelectedFG = -1
     SELF.Q.chargeable_part_total_temp_SelectedBG = -1
   END
   IF (par:pending_ref_number <> '' And par:order_number = '' And par:date_ordered = '')
     SELF.Q.par:Part_Ref_Number_NormalFG = 32768
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 32768
   ELSIF(par:order_number <> '' And par:date_received ='')
     SELF.Q.par:Part_Ref_Number_NormalFG = 255
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 255
   ELSIF(par:order_number <> '' And par:date_received<> '')
     SELF.Q.par:Part_Ref_Number_NormalFG = 8421504
     SELF.Q.par:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.par:Part_Ref_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.par:Part_Ref_Number_NormalFG = -1
     SELF.Q.par:Part_Ref_Number_NormalBG = -1
     SELF.Q.par:Part_Ref_Number_SelectedFG = -1
     SELF.Q.par:Part_Ref_Number_SelectedBG = -1
   END


BRW12.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW21.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW77.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW77.SetQueueRecord PROCEDURE

  CODE
  estimate_parts_cost_temp  = epr:quantity * epr:sale_cost
  PARENT.SetQueueRecord


BRW77.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW79.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW79.SetQueueRecord PROCEDURE

  CODE
  If wpr:adjustment <> 'YES'
      If wpr:exclude_from_order = 'YES'
          warranty_ordered_temp = 'EXCLUDED'
      Else!If wpr:exclude_from_order = 'YES'
          If wpr:order_number <> ''
              If wpr:date_received = ''
                  warranty_ordered_temp = 'ON ORDER'
              Else!If wpr:date_received = ''
                  warranty_ordered_temp    = 'RECD: ' & Format(wpr:date_received,@d6b)
              End!If wpr:date_received = ''
          Else!If wpr:order_number <> ''
              If wpr:pending_ref_number <> ''
                  warranty_ordered_temp = 'PENDING'
              Else!If wpr:pending_ref_number <> ''
                  If wpr:part_ref_number <> ''
                      warranty_ordered_temp = 'FROM STOCK'
                  Else!If wpr:part_ref_number <> ''
                      warranty_ordered_temp = 'EXCLUDED'
                  End!If wpr:part_ref_number <> ''
              End!If wpr:pending_ref_number <> ''
          End!If wpr:order_number <> ''
      End!If wpr:exclude_from_order = 'YES'
  Else
      warranty_ordered_temp = 'N/A'
  End
  warranty_part_total_temp  = wpr:quantity * wpr:purchase_cost
  PARENT.SetQueueRecord
  SELF.Q.wpr:Part_Number_NormalFG = -1
  SELF.Q.wpr:Part_Number_NormalBG = -1
  SELF.Q.wpr:Part_Number_SelectedFG = -1
  SELF.Q.wpr:Part_Number_SelectedBG = -1
  SELF.Q.wpr:Description_NormalFG = -1
  SELF.Q.wpr:Description_NormalBG = -1
  SELF.Q.wpr:Description_SelectedFG = -1
  SELF.Q.wpr:Description_SelectedBG = -1
  SELF.Q.wpr:Order_Number_NormalFG = -1
  SELF.Q.wpr:Order_Number_NormalBG = -1
  SELF.Q.wpr:Order_Number_SelectedFG = -1
  SELF.Q.wpr:Order_Number_SelectedBG = -1
  SELF.Q.warranty_ordered_temp_NormalFG = -1
  SELF.Q.warranty_ordered_temp_NormalBG = -1
  SELF.Q.warranty_ordered_temp_SelectedFG = -1
  SELF.Q.warranty_ordered_temp_SelectedBG = -1
  SELF.Q.wpr:Quantity_NormalFG = -1
  SELF.Q.wpr:Quantity_NormalBG = -1
  SELF.Q.wpr:Quantity_SelectedFG = -1
  SELF.Q.wpr:Quantity_SelectedBG = -1
  SELF.Q.warranty_part_total_temp_NormalFG = -1
  SELF.Q.warranty_part_total_temp_NormalBG = -1
  SELF.Q.warranty_part_total_temp_SelectedFG = -1
  SELF.Q.warranty_part_total_temp_SelectedBG = -1
  SELF.Q.wpr:Part_Ref_Number_NormalFG = -1
  SELF.Q.wpr:Part_Ref_Number_NormalBG = -1
  SELF.Q.wpr:Part_Ref_Number_SelectedFG = -1
  SELF.Q.wpr:Part_Ref_Number_SelectedBG = -1
   
   
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Part_Number_NormalFG = 32768
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Part_Number_NormalFG = 255
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Part_Number_NormalFG = 8421504
     SELF.Q.wpr:Part_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Part_Number_NormalFG = -1
     SELF.Q.wpr:Part_Number_NormalBG = -1
     SELF.Q.wpr:Part_Number_SelectedFG = -1
     SELF.Q.wpr:Part_Number_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Description_NormalFG = 32768
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Description_NormalFG = 255
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Description_NormalFG = 8421504
     SELF.Q.wpr:Description_NormalBG = 16777215
     SELF.Q.wpr:Description_SelectedFG = 16777215
     SELF.Q.wpr:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Description_NormalFG = -1
     SELF.Q.wpr:Description_NormalBG = -1
     SELF.Q.wpr:Description_SelectedFG = -1
     SELF.Q.wpr:Description_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Order_Number_NormalFG = 32768
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Order_Number_NormalFG = 255
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Order_Number_NormalFG = 8421504
     SELF.Q.wpr:Order_Number_NormalBG = 16777215
     SELF.Q.wpr:Order_Number_SelectedFG = 16777215
     SELF.Q.wpr:Order_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Order_Number_NormalFG = -1
     SELF.Q.wpr:Order_Number_NormalBG = -1
     SELF.Q.wpr:Order_Number_SelectedFG = -1
     SELF.Q.wpr:Order_Number_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.warranty_ordered_temp_NormalFG = 32768
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.warranty_ordered_temp_NormalFG = 255
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.warranty_ordered_temp_NormalFG = 8421504
     SELF.Q.warranty_ordered_temp_NormalBG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedFG = 16777215
     SELF.Q.warranty_ordered_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.warranty_ordered_temp_NormalFG = -1
     SELF.Q.warranty_ordered_temp_NormalBG = -1
     SELF.Q.warranty_ordered_temp_SelectedFG = -1
     SELF.Q.warranty_ordered_temp_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Quantity_NormalFG = 32768
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Quantity_NormalFG = 255
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Quantity_NormalFG = 8421504
     SELF.Q.wpr:Quantity_NormalBG = 16777215
     SELF.Q.wpr:Quantity_SelectedFG = 16777215
     SELF.Q.wpr:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Quantity_NormalFG = -1
     SELF.Q.wpr:Quantity_NormalBG = -1
     SELF.Q.wpr:Quantity_SelectedFG = -1
     SELF.Q.wpr:Quantity_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.warranty_part_total_temp_NormalFG = 32768
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.warranty_part_total_temp_NormalFG = 255
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.warranty_part_total_temp_NormalFG = 8421504
     SELF.Q.warranty_part_total_temp_NormalBG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedFG = 16777215
     SELF.Q.warranty_part_total_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.warranty_part_total_temp_NormalFG = -1
     SELF.Q.warranty_part_total_temp_NormalBG = -1
     SELF.Q.warranty_part_total_temp_SelectedFG = -1
     SELF.Q.warranty_part_total_temp_SelectedBG = -1
   END
   IF (wpr:pending_ref_number <> '' And wpr:order_number = '' And wpr:date_ordered = '')
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 32768
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 32768
   ELSIF(wpr:order_number <> '' And wpr:date_received ='')
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 255
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 255
   ELSIF(wpr:order_number <> '' And wpr:date_received<> '')
     SELF.Q.wpr:Part_Ref_Number_NormalFG = 8421504
     SELF.Q.wpr:Part_Ref_Number_NormalBG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = 16777215
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.wpr:Part_Ref_Number_NormalFG = -1
     SELF.Q.wpr:Part_Ref_Number_NormalBG = -1
     SELF.Q.wpr:Part_Ref_Number_SelectedFG = -1
     SELF.Q.wpr:Part_Ref_Number_SelectedBG = -1
   END


BRW79.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

