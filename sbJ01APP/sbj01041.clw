

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01041.INC'),ONCE        !Local module procedure declarations
                     END








Browse_Invoice PROCEDURE                              !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
save_inv_id          USHORT,AUTO
pos                  STRING(255)
FilesOpened          BYTE
Invoice_Type_Temp    STRING(10)
account_number_temp  STRING(15)
invoice_Credit_temp  STRING(8)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:filter           STRING(255)
tmp:order            STRING(255)
tmp:couriercost      REAL
tmp:LabourCost       REAL
tmp:PartsCost        REAL
BRW1::View:Browse    VIEW(INVOICE)
                       PROJECT(inv:Invoice_Number)
                       PROJECT(inv:Account_Number)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Courier_Paid)
                       PROJECT(inv:Labour_Paid)
                       PROJECT(inv:Parts_Paid)
                       PROJECT(inv:Invoice_Type)
                       PROJECT(inv:Total_Claimed)
                       PROJECT(inv:InvoiceCredit)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
inv:Invoice_Number     LIKE(inv:Invoice_Number)       !List box control field - type derived from field
inv:Invoice_Number_NormalFG LONG                      !Normal forground color
inv:Invoice_Number_NormalBG LONG                      !Normal background color
inv:Invoice_Number_SelectedFG LONG                    !Selected forground color
inv:Invoice_Number_SelectedBG LONG                    !Selected background color
inv:Account_Number     LIKE(inv:Account_Number)       !List box control field - type derived from field
inv:Account_Number_NormalFG LONG                      !Normal forground color
inv:Account_Number_NormalBG LONG                      !Normal background color
inv:Account_Number_SelectedFG LONG                    !Selected forground color
inv:Account_Number_SelectedBG LONG                    !Selected background color
inv:Date_Created       LIKE(inv:Date_Created)         !List box control field - type derived from field
inv:Date_Created_NormalFG LONG                        !Normal forground color
inv:Date_Created_NormalBG LONG                        !Normal background color
inv:Date_Created_SelectedFG LONG                      !Selected forground color
inv:Date_Created_SelectedBG LONG                      !Selected background color
Invoice_Type_Temp      LIKE(Invoice_Type_Temp)        !List box control field - type derived from local data
Invoice_Type_Temp_NormalFG LONG                       !Normal forground color
Invoice_Type_Temp_NormalBG LONG                       !Normal background color
Invoice_Type_Temp_SelectedFG LONG                     !Selected forground color
Invoice_Type_Temp_SelectedBG LONG                     !Selected background color
invoice_Credit_temp    LIKE(invoice_Credit_temp)      !List box control field - type derived from local data
invoice_Credit_temp_NormalFG LONG                     !Normal forground color
invoice_Credit_temp_NormalBG LONG                     !Normal background color
invoice_Credit_temp_SelectedFG LONG                   !Selected forground color
invoice_Credit_temp_SelectedBG LONG                   !Selected background color
tmp:couriercost        LIKE(tmp:couriercost)          !List box control field - type derived from local data
tmp:couriercost_NormalFG LONG                         !Normal forground color
tmp:couriercost_NormalBG LONG                         !Normal background color
tmp:couriercost_SelectedFG LONG                       !Selected forground color
tmp:couriercost_SelectedBG LONG                       !Selected background color
tmp:LabourCost         LIKE(tmp:LabourCost)           !List box control field - type derived from local data
tmp:LabourCost_NormalFG LONG                          !Normal forground color
tmp:LabourCost_NormalBG LONG                          !Normal background color
tmp:LabourCost_SelectedFG LONG                        !Selected forground color
tmp:LabourCost_SelectedBG LONG                        !Selected background color
tmp:PartsCost          LIKE(tmp:PartsCost)            !List box control field - type derived from local data
tmp:PartsCost_NormalFG LONG                           !Normal forground color
tmp:PartsCost_NormalBG LONG                           !Normal background color
tmp:PartsCost_SelectedFG LONG                         !Selected forground color
tmp:PartsCost_SelectedBG LONG                         !Selected background color
inv:Courier_Paid       LIKE(inv:Courier_Paid)         !List box control field - type derived from field
inv:Courier_Paid_NormalFG LONG                        !Normal forground color
inv:Courier_Paid_NormalBG LONG                        !Normal background color
inv:Courier_Paid_SelectedFG LONG                      !Selected forground color
inv:Courier_Paid_SelectedBG LONG                      !Selected background color
inv:Labour_Paid        LIKE(inv:Labour_Paid)          !List box control field - type derived from field
inv:Labour_Paid_NormalFG LONG                         !Normal forground color
inv:Labour_Paid_NormalBG LONG                         !Normal background color
inv:Labour_Paid_SelectedFG LONG                       !Selected forground color
inv:Labour_Paid_SelectedBG LONG                       !Selected background color
inv:Parts_Paid         LIKE(inv:Parts_Paid)           !List box control field - type derived from field
inv:Parts_Paid_NormalFG LONG                          !Normal forground color
inv:Parts_Paid_NormalBG LONG                          !Normal background color
inv:Parts_Paid_SelectedFG LONG                        !Selected forground color
inv:Parts_Paid_SelectedBG LONG                        !Selected background color
inv:Invoice_Type       LIKE(inv:Invoice_Type)         !List box control field - type derived from field
inv:Invoice_Type_NormalFG LONG                        !Normal forground color
inv:Invoice_Type_NormalBG LONG                        !Normal background color
inv:Invoice_Type_SelectedFG LONG                      !Selected forground color
inv:Invoice_Type_SelectedBG LONG                      !Selected background color
inv:Total_Claimed      LIKE(inv:Total_Claimed)        !List box control field - type derived from field
inv:Total_Claimed_NormalFG LONG                       !Normal forground color
inv:Total_Claimed_NormalBG LONG                       !Normal background color
inv:Total_Claimed_SelectedFG LONG                     !Selected forground color
inv:Total_Claimed_SelectedBG LONG                     !Selected background color
inv:InvoiceCredit      LIKE(inv:InvoiceCredit)        !List box control field - type derived from field
inv:InvoiceCredit_NormalFG LONG                       !Normal forground color
inv:InvoiceCredit_NormalBG LONG                       !Normal background color
inv:InvoiceCredit_SelectedFG LONG                     !Selected forground color
inv:InvoiceCredit_SelectedBG LONG                     !Selected background color
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW13::View:Browse   VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Invoice_Number)
                       PROJECT(job:Invoice_Number_Warranty)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:Unit_Type          LIKE(job:Unit_Type)            !List box control field - type derived from field
job:Invoice_Number     LIKE(job:Invoice_Number)       !Browse key field - type derived from field
job:Invoice_Number_Warranty LIKE(job:Invoice_Number_Warranty) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW14::View:Browse   VIEW(RETSALES)
                       PROJECT(ret:Ref_Number)
                       PROJECT(ret:Purchase_Order_Number)
                       PROJECT(ret:Consignment_Number)
                       PROJECT(ret:Invoice_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
ret:Ref_Number         LIKE(ret:Ref_Number)           !List box control field - type derived from field
ret:Purchase_Order_Number LIKE(ret:Purchase_Order_Number) !List box control field - type derived from field
ret:Consignment_Number LIKE(ret:Consignment_Number)   !List box control field - type derived from field
ret:Invoice_Number     LIKE(ret:Invoice_Number)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,56)                !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
XploreMask13         DECIMAL(10,0,0)                  !Xplore
XploreMask113         DECIMAL(10,0,56)                !Xplore
XploreTitle13        STRING(' ')                      !Xplore
xpInitialTab13       SHORT                            !Xplore
QuickWindow          WINDOW('Browse The Invoice File'),AT(,,526,358),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Invoice'),SYSTEM,GRAY,MAX,DOUBLE
                       LIST,AT(8,36,428,160),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('43R(2)|M*~Invoice No~L@p<<<<<<<<<<<<<<#p@72L(2)|M*~Account Number~@s15@51R(2)|M*~Date C' &|
   'reated~C(0)@d6b@57L(2)|M*~Invoice Type~@s10@55L(2)|M*~Invoice / Credit~@s8@44R(2' &|
   ')|M*~Courier Cost~@n10.2@44R(2)|M*~Labour Cost~@n10.2@44R(2)|M*~Parts Cost~@n10.' &|
   '2@49R(2)|M*~Courier Cost~@n14.2@47R(2)|M*~Labour Cost~@n14.2@42R(2)|M*~Parts Cos' &|
   't~@n14.2@42L(2)|M*~Invoice Type~@s3@43R(2)|M*~Total Cost~@n14.2@72L(2)|M*~Invoic' &|
   'e Credit~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('Re-Print Invoice Batch'),AT(448,44,76,20),USE(?Reprint_Invoice:2),LEFT,ICON(ICON:Print1)
                       BUTTON('Create Sage Invoice'),AT(448,68,76,20),USE(?Create_Sage_Invoice),HIDE,LEFT,ICON('sage.ico')
                       BUTTON('Create/Print Credit Note'),AT(448,112,76,20),USE(?Print_Credit_Note),HIDE,LEFT,ICON('Invoice.gif')
                       BUTTON('&Amend Invoice'),AT(448,144,76,20),USE(?Amend_Invoice),LEFT,ICON('edit.ico')
                       ENTRY(@p<<<<<<<#pb),AT(8,20,64,10),USE(inv:Invoice_Number),FONT('Tahoma',8,,FONT:bold),UPR
                       BUTTON('&Remove Invoice'),AT(448,168,76,20),USE(?Remove_Invoice),LEFT,ICON('delete.ico')
                       BUTTON('Re-Print Invoice'),AT(448,20,76,20),USE(?Reprint_Invoice),LEFT,ICON(ICON:Print1)
                       SHEET,AT(4,4,436,212),USE(?CurrentTab),SPREAD
                         TAB('By Invoice Number'),USE(?Tab:2)
                           STRING(@s255),AT(216,20,220,10),USE(tmp:filter),HIDE
                           STRING(@s255),AT(216,202,220,10),USE(tmp:order),HIDE
                         END
                       END
                       BUTTON('Close'),AT(448,336,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       PROMPT('- Credit Note'),AT(20,204),USE(?Prompt2)
                       SHEET,AT(4,220,436,136),USE(?Sheet2),SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           PROMPT('Jobs Attached To The Selected Invoice'),AT(8,224),USE(?Prompt3),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                           LIST,AT(8,236,428,116),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('35R(2)|M~Job No~L@s8@125L(2)|M~Model Number~@s30@64L(2)|M~ESN/IMEI~@s16@120L(2)|' &|
   'M~Unit Type~@s30@'),FROM(Queue:Browse)
                         END
                         TAB('Tab 4'),USE(?Tab4)
                           PROMPT('Retail Sales Attached To The Selected Invoice'),AT(8,224),USE(?Prompt4),TRN,FONT('Tahoma',8,COLOR:White,FONT:bold),COLOR(COLOR:Gray)
                           BUTTON('&Change'),AT(448,248,76,20),USE(?Change)
                           LIST,AT(8,236,428,116),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('37R(2)|M~Sales No~@s8@120L(2)|M~Purchase Order Number~@s30@120L(2)|M~Consignment' &|
   ' Number~@s30@'),FROM(Queue:Browse:2)
                         END
                       END
                       BUTTON('&Change'),AT(448,276,76,20),USE(?Change:Job),LEFT,ICON('edit.ico')
                       BOX,AT(8,204,10,8),USE(?Box1:2),COLOR(COLOR:Black),FILL(COLOR:Red)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW13                CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse                  !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW14                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW14::Sort0:Locator StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

save_job_id   ushort,auto
windowsdir      Cstring(144)
systemdir       Cstring(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepRealClass   !REAL            !Xplore: Column displaying inv:Invoice_Number
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying inv:Invoice_Number
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying inv:Account_Number
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying inv:Account_Number
Xplore1Step3         StepCustomClass !DATE            !Xplore: Column displaying inv:Date_Created
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying inv:Date_Created
Xplore1Step4         StepCustomClass !                !Xplore: Column displaying Invoice_Type_Temp
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying Invoice_Type_Temp
Xplore1Step9         StepRealClass   !REAL            !Xplore: Column displaying inv:Courier_Paid
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying inv:Courier_Paid
Xplore1Step10        StepRealClass   !REAL            !Xplore: Column displaying inv:Labour_Paid
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying inv:Labour_Paid
Xplore1Step11        StepRealClass   !REAL            !Xplore: Column displaying inv:Parts_Paid
Xplore1Locator11     IncrementalLocatorClass          !Xplore: Column displaying inv:Parts_Paid
Xplore1Step12        StepStringClass !STRING          !Xplore: Column displaying inv:Invoice_Type
Xplore1Locator12     IncrementalLocatorClass          !Xplore: Column displaying inv:Invoice_Type
Xplore1Step13        StepRealClass   !REAL            !Xplore: Column displaying inv:Total_Claimed
Xplore1Locator13     IncrementalLocatorClass          !Xplore: Column displaying inv:Total_Claimed
Xplore1Step14        StepStringClass !STRING          !Xplore: Column displaying inv:InvoiceCredit
Xplore1Locator14     IncrementalLocatorClass          !Xplore: Column displaying inv:InvoiceCredit
Xplore13             CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore13Step1        StepLongClass   !LONG            !Xplore: Column displaying job:Ref_Number
Xplore13Locator1     IncrementalLocatorClass          !Xplore: Column displaying job:Ref_Number
Xplore13Step2        StepStringClass !STRING          !Xplore: Column displaying job:Model_Number
Xplore13Locator2     IncrementalLocatorClass          !Xplore: Column displaying job:Model_Number
Xplore13Step3        StepStringClass !STRING          !Xplore: Column displaying job:ESN
Xplore13Locator3     IncrementalLocatorClass          !Xplore: Column displaying job:ESN
Xplore13Step4        StepStringClass !STRING          !Xplore: Column displaying job:Unit_Type
Xplore13Locator4     IncrementalLocatorClass          !Xplore: Column displaying job:Unit_Type
Xplore13Step5        StepLongClass   !LONG            !Xplore: Column displaying job:Invoice_Number
Xplore13Locator5     IncrementalLocatorClass          !Xplore: Column displaying job:Invoice_Number
Xplore13Step6        StepLongClass   !LONG            !Xplore: Column displaying job:Invoice_Number_Warranty
Xplore13Locator6     IncrementalLocatorClass          !Xplore: Column displaying job:Invoice_Number_Warranty

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    If ?inv:Invoice_Number{prop:ReadOnly} = True
        ?inv:Invoice_Number{prop:FontColor} = 65793
        ?inv:Invoice_Number{prop:Color} = 15066597
    Elsif ?inv:Invoice_Number{prop:Req} = True
        ?inv:Invoice_Number{prop:FontColor} = 65793
        ?inv:Invoice_Number{prop:Color} = 8454143
    Else ! If ?inv:Invoice_Number{prop:Req} = True
        ?inv:Invoice_Number{prop:FontColor} = 65793
        ?inv:Invoice_Number{prop:Color} = 16777215
    End ! If ?inv:Invoice_Number{prop:Req} = True
    ?inv:Invoice_Number{prop:Trn} = 0
    ?inv:Invoice_Number{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?tmp:filter{prop:FontColor} = -1
    ?tmp:filter{prop:Color} = 15066597
    ?tmp:order{prop:FontColor} = -1
    ?tmp:order{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Tab4{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Invoice',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Invoice',1)
    SolaceViewVars('save_inv_id',save_inv_id,'Browse_Invoice',1)
    SolaceViewVars('pos',pos,'Browse_Invoice',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Invoice',1)
    SolaceViewVars('Invoice_Type_Temp',Invoice_Type_Temp,'Browse_Invoice',1)
    SolaceViewVars('account_number_temp',account_number_temp,'Browse_Invoice',1)
    SolaceViewVars('invoice_Credit_temp',invoice_Credit_temp,'Browse_Invoice',1)
    SolaceViewVars('tmp:filter',tmp:filter,'Browse_Invoice',1)
    SolaceViewVars('tmp:order',tmp:order,'Browse_Invoice',1)
    SolaceViewVars('tmp:couriercost',tmp:couriercost,'Browse_Invoice',1)
    SolaceViewVars('tmp:LabourCost',tmp:LabourCost,'Browse_Invoice',1)
    SolaceViewVars('tmp:PartsCost',tmp:PartsCost,'Browse_Invoice',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reprint_Invoice:2;  SolaceCtrlName = '?Reprint_Invoice:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Create_Sage_Invoice;  SolaceCtrlName = '?Create_Sage_Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Credit_Note;  SolaceCtrlName = '?Print_Credit_Note';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend_Invoice;  SolaceCtrlName = '?Amend_Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Invoice_Number;  SolaceCtrlName = '?inv:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Remove_Invoice;  SolaceCtrlName = '?Remove_Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reprint_Invoice;  SolaceCtrlName = '?Reprint_Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:filter;  SolaceCtrlName = '?tmp:filter';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:order;  SolaceCtrlName = '?tmp:order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:Job;  SolaceCtrlName = '?Change:Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:2;  SolaceCtrlName = '?Box1:2';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Invoice','?Browse:1')     !Xplore
  BRW1.SequenceNbr = 1                                !Xplore
  BRW1.ViewOrder   = True                             !Xplore
  Xplore1.RestoreHeader = True                        !Xplore
  Xplore1.AscDesc       = 0                           !Xplore
  Xplore13.GetBbSize('Browse_Invoice','?List')        !Xplore
  BRW13.SequenceNbr = 0                               !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Invoice')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Invoice')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Invoice'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBNOTES.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:INVOICE,SELF)
  BRW13.Init(?List,Queue:Browse.ViewPosition,BRW13::View:Browse,Queue:Browse,Relate:JOBS,SELF)
  BRW14.Init(?List:2,Queue:Browse:2.ViewPosition,BRW14::View:Browse,Queue:Browse:2,Relate:RETSALES,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ?Sheet2{prop:wizard} = 1
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbj01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  Xplore13.Init(ThisWindow,BRW13,Queue:Browse,QuickWindow,?List,'sbj01app.INI','>Header',0,BRW13.ViewOrder,Xplore13.RestoreHeader,BRW13.SequenceNbr,XploreMask13,XploreMask113,XploreTitle13,BRW13.FileSeqOn)
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,inv:Invoice_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?inv:Invoice_Number,inv:Invoice_Number,1,BRW1)
  BIND('Invoice_Type_Temp',Invoice_Type_Temp)
  BIND('invoice_Credit_temp',invoice_Credit_temp)
  BIND('tmp:couriercost',tmp:couriercost)
  BIND('tmp:LabourCost',tmp:LabourCost)
  BIND('tmp:PartsCost',tmp:PartsCost)
  BRW1.AddField(inv:Invoice_Number,BRW1.Q.inv:Invoice_Number)
  BRW1.AddField(inv:Account_Number,BRW1.Q.inv:Account_Number)
  BRW1.AddField(inv:Date_Created,BRW1.Q.inv:Date_Created)
  BRW1.AddField(Invoice_Type_Temp,BRW1.Q.Invoice_Type_Temp)
  BRW1.AddField(invoice_Credit_temp,BRW1.Q.invoice_Credit_temp)
  BRW1.AddField(tmp:couriercost,BRW1.Q.tmp:couriercost)
  BRW1.AddField(tmp:LabourCost,BRW1.Q.tmp:LabourCost)
  BRW1.AddField(tmp:PartsCost,BRW1.Q.tmp:PartsCost)
  BRW1.AddField(inv:Courier_Paid,BRW1.Q.inv:Courier_Paid)
  BRW1.AddField(inv:Labour_Paid,BRW1.Q.inv:Labour_Paid)
  BRW1.AddField(inv:Parts_Paid,BRW1.Q.inv:Parts_Paid)
  BRW1.AddField(inv:Invoice_Type,BRW1.Q.inv:Invoice_Type)
  BRW1.AddField(inv:Total_Claimed,BRW1.Q.inv:Total_Claimed)
  BRW1.AddField(inv:InvoiceCredit,BRW1.Q.inv:InvoiceCredit)
  BRW13.Q &= Queue:Browse
  BRW13.RetainRow = 0
  BRW13.AddSortOrder(,job:InvoiceNumberKey)
  BRW13.AddRange(job:Invoice_Number,inv:Invoice_Number)
  BRW13.AddSortOrder(,job:WarInvoiceNoKey)
  BRW13.AddRange(job:Invoice_Number_Warranty,inv:Invoice_Number)
  BRW13.AddSortOrder(,job:InvoiceNumberKey)
  BRW13.AddRange(job:Invoice_Number,inv:Invoice_Number)
  BRW13.AddField(job:Ref_Number,BRW13.Q.job:Ref_Number)
  BRW13.AddField(job:Model_Number,BRW13.Q.job:Model_Number)
  BRW13.AddField(job:ESN,BRW13.Q.job:ESN)
  BRW13.AddField(job:Unit_Type,BRW13.Q.job:Unit_Type)
  BRW13.AddField(job:Invoice_Number,BRW13.Q.job:Invoice_Number)
  BRW13.AddField(job:Invoice_Number_Warranty,BRW13.Q.job:Invoice_Number_Warranty)
  BRW14.Q &= Queue:Browse:2
  BRW14.RetainRow = 0
  BRW14.AddSortOrder(,ret:Invoice_Number_Key)
  BRW14.AddRange(ret:Invoice_Number,inv:Invoice_Number)
  BRW14.AddLocator(BRW14::Sort0:Locator)
  BRW14::Sort0:Locator.Init(,ret:Invoice_Number,1,BRW14)
  BRW14.AddField(ret:Ref_Number,BRW14.Q.ret:Ref_Number)
  BRW14.AddField(ret:Purchase_Order_Number,BRW14.Q.ret:Purchase_Order_Number)
  BRW14.AddField(ret:Consignment_Number,BRW14.Q.ret:Consignment_Number)
  BRW14.AddField(ret:Invoice_Number,BRW14.Q.ret:Invoice_Number)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=256
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  BRW13.AskProcedure = 2
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
    Xplore13.EraseVisual()                            !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Invoice','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisWindow.Opened                                !Xplore
    Xplore13.sq.Col = Xplore13.CurrentCol
    GET(Xplore13.sq,Xplore13.sq.Col)
    IF Xplore13.ListType = 1 AND BRW13.ViewOrder = False !Xplore
      BRW13.SequenceNbr = 0                           !Xplore
    END                                               !Xplore
    Xplore13.PutInix('Browse_Invoice','?List',BRW13.SequenceNbr,Xplore13.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Invoice'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
  Xplore13.Kill()                                     !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Invoice',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  If Field() <> ?Change:Job
      case request
          of changerecord
              check_access('AMEND INVOICE',x")
              if x" = false
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  do_update# = false
              Else!if x" = false
                  Case MessageEx('Are you sure you want to amend this Invoice?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          
                      Of 2 !& No Button
                          do_update# = false
                  End!Case MessagesEX
              end!if x" = false
      end !case request
  
  End!If Field() <> ?Change:Job
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Update_Invoice
      UpdateJOBS
    END
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  Xplore13.Upper = True                               !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(inv:Invoice_Number,BRW1.Q.inv:Invoice_Number)
  Xplore1.AddField(inv:Account_Number,BRW1.Q.inv:Account_Number)
  Xplore1.AddField(inv:Date_Created,BRW1.Q.inv:Date_Created)
  Xplore1.AddField(tmp:couriercost,BRW1.Q.tmp:couriercost)
  Xplore1.AddField(tmp:LabourCost,BRW1.Q.tmp:LabourCost)
  Xplore1.AddField(tmp:PartsCost,BRW1.Q.tmp:PartsCost)
  Xplore1.AddField(inv:Courier_Paid,BRW1.Q.inv:Courier_Paid)
  Xplore1.AddField(inv:Labour_Paid,BRW1.Q.inv:Labour_Paid)
  Xplore1.AddField(inv:Parts_Paid,BRW1.Q.inv:Parts_Paid)
  Xplore1.AddField(inv:Invoice_Type,BRW1.Q.inv:Invoice_Type)
  Xplore1.AddField(inv:Total_Claimed,BRW1.Q.inv:Total_Claimed)
  Xplore1.AddField(inv:InvoiceCredit,BRW1.Q.inv:InvoiceCredit)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore
  Xplore13.AddField(job:Ref_Number,BRW13.Q.job:Ref_Number)
  Xplore13.AddField(job:Model_Number,BRW13.Q.job:Model_Number)
  Xplore13.AddField(job:ESN,BRW13.Q.job:ESN)
  Xplore13.AddField(job:Unit_Type,BRW13.Q.job:Unit_Type)
  Xplore13.AddField(job:Invoice_Number,BRW13.Q.job:Invoice_Number)
  Xplore13.AddField(job:Invoice_Number_Warranty,BRW13.Q.job:Invoice_Number_Warranty)
  BRW13.FileOrderNbr = BRW13.AddSortOrder(,job:InvoiceNumberKey) !Xplore Sort Order for File Sequence
  BRW13.AddRange(job:Invoice_Number,inv:Invoice_Number) !Xplore
  BRW13.SetOrder('')                                  !Xplore
  BRW13.ViewOrder = True                              !Xplore
  Xplore13.AddAllColumnSortOrders(1)                  !Xplore
  BRW13.ViewOrder = False                             !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Reprint_Invoice:2
      ThisWindow.Update
      ! Start Change ???? BE(15/04/03)
      !glo:file_name   = 'I' & FOrmat(Clock(),@n_7) & '.DAT'
      glo:tradetmp   = 'I' & Format(Clock(),@n07) & '.DAT'
      ! End Change ???? BE(15/04/03)
      Invoice_Date_Range
      Remove(tradetmp)
      If error()
          access:tradetmp.close()
          Remove(tradetmp)
      End!If error()
      
    OF ?Create_Sage_Invoice
      ThisWindow.Update
      Case MessageEx('This facility should only be used if the selected invoice number does NOT exist in Sage.<13,10><13,10>If it does there will be an invoice number mismatch.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              systemdir   = getsystemdirectory(windowsdir,size(windowsdir))
              Case inv:invoice_type
                  Of 'SIN'
                      file_error# = 0
                      glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
          
                      Remove(paramss)
                      access:paramss.open()
                      access:paramss.usefile()
                      access:jobs.clearkey(job:InvoiceNumberKey)
                      job:invoice_number    = inv:invoice_number
                      If access:jobs.fetch(job:InvoiceNumberKey) = Level:Benign
                          access:jobnotes.clearkey(jbn:RefNumberKey)
                          jbn:refNUmber = job:Ref_Number
                          access:jobnotes.tryfetch(jbn:RefNumberKey)
              !LABOUR
                          get(paramss,0)
                          if access:paramss.primerecord() = Level:Benign
                              prm:account_ref       = Stripcomma(job:Account_number)
                              prm:name              = Stripcomma(job:company_name)
                              prm:address_1         = Stripcomma(job:address_line1)
                              prm:address_2         = Stripcomma(job:address_line2)
                              prm:address_3         = Stripcomma(job:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(job:postcode)
                              prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                              prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                              prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                              prm:del_address_4     = ''
                              prm:del_address_5     = Stripcomma(job:postcode_delivery)
                              prm:cust_tel_number   = Stripcomma(job:telephone_number)
                              If job:surname <> ''
                                  if job:title = '' and job:initial = '' 
                                      prm:contact_name = Stripcomma(clip(job:surname))
                                  elsif job:title = '' and job:initial <> ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                  elsif job:title <> '' and job:initial = ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                  elsif job:title <> '' and job:initial <> ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                  else
                                      prm:contact_name = ''
                                  end
                              else
                                  prm:contact_name = ''
                              end
                              prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                              prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                              prm:notes_3           = ''
                              prm:taken_by          = Stripcomma(job:who_booked)
                              prm:order_number      = Stripcomma(job:order_number)
                              prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                              prm:payment_ref       = ''
                              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                              prm:global_details    = ''
                              prm:items_net         = Stripcomma(job:invoice_labour_cost + job:invoice_parts_cost + job:invoice_courier_cost)
                              prm:items_tax         = Stripcomma(Round(job:invoice_labour_cost * (inv:vat_rate_labour/100),.01) + |
                                                          Round(job:invoice_parts_cost * (inv:vat_rate_parts/100),.01) + |
                                                          Round(job:invoice_courier_cost * (inv:vat_rate_labour/100),.01))
                              prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
                              prm:description       = Stripcomma(DEF:Labour_Description)
                              prm:nominal_code      = Stripcomma(DEF:Labour_Code)
                              prm:qty_order         = '1'
                              prm:unit_price        = '0'
                              prm:net_amount        = Stripcomma(job:invoice_labour_cost)
                              prm:tax_amount        = Stripcomma(Round(job:invoice_labour_cost * (inv:vat_rate_labour/100),.01))
                              prm:comment_1         = Stripcomma(job:charge_type)
                              prm:comment_2         = Stripcomma(job:warranty_charge_type)
                              prm:unit_of_sale      = '0'
                              prm:full_net_amount   = '0'
                              prm:invoice_date      = Clip(Day(inv:date_created)) & '/' & Clip(Month(inv:date_created)) & '/' & Clip(Format(Year(inv:date_Created),@n4))
                              prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                              prm:user_name         = Clip(DEF:User_Name_Sage)
                              prm:password          = Clip(DEF:Password_Sage)
                              prm:set_invoice_number= inv:invoice_number - 1
                              prm:invoice_no        = '*'
                              access:paramss.insert()
                          end!if access:paramss.primerecord() = Level:Benign
                  !PARTS
                          get(paramss,0)
                          if access:paramss.primerecord() = Level:Benign
                              prm:account_ref       = Stripcomma(job:Account_number)
                              prm:name              = Stripcomma(job:company_name)
                              prm:address_1         = Stripcomma(job:address_line1)
                              prm:address_2         = Stripcomma(job:address_line2)
                              prm:address_3         = Stripcomma(job:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(job:postcode)
                              prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                              prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                              prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                              prm:del_address_4     = ''
                              prm:del_address_5     = Stripcomma(job:postcode_delivery)
                              prm:cust_tel_number   = Stripcomma(job:telephone_number)
                              If job:surname <> ''
                                  if job:title = '' and job:initial = '' 
                                      prm:contact_name = Stripcomma(clip(job:surname))
                                  elsif job:title = '' and job:initial <> ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                  elsif job:title <> '' and job:initial = ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                  elsif job:title <> '' and job:initial <> ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                  else
                                      prm:contact_name = ''
                                  end
                              else
                                  prm:contact_name = ''
                              end
                              prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                              prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                              prm:notes_3           = ''
                              prm:taken_by          = Stripcomma(job:who_booked)
                              prm:order_number      = Stripcomma(job:order_number)
                              prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                              prm:payment_ref       = ''
                              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                              prm:global_details    = ''
                              prm:items_net         = Stripcomma(job:invoice_labour_cost + job:invoice_parts_cost + job:invoice_courier_cost)
                              prm:items_tax         = Stripcomma(Round(job:invoice_labour_cost * (inv:vat_rate_labour/100),.01) + |
                                                          Round(job:invoice_parts_cost * (inv:vat_rate_parts/100),.01) + |
                                                          Round(job:invoice_courier_cost * (inv:vat_rate_labour/100),.01))
                              prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                              prm:description       = Stripcomma(DEF:Parts_Description)
                              prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                              prm:qty_order         = '1'
                              prm:unit_price        = '0'
                              prm:net_amount        = Stripcomma(job:invoice_parts_cost)
                              prm:tax_amount        = Stripcomma(Round(job:invoice_parts_cost * (inv:vat_rate_parts/100),.01))
                              prm:comment_1         = Stripcomma(job:charge_type)
                              prm:comment_2         = Stripcomma(job:warranty_charge_type)
                              prm:unit_of_sale      = '0'
                              prm:full_net_amount   = '0'
                              prm:invoice_date      = Clip(Day(inv:date_created)) & '/' & Clip(Month(inv:date_created)) & '/' & Clip(Format(Year(inv:date_Created),@n4))
                              prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                              prm:user_name         = Clip(DEF:User_Name_Sage)
                              prm:password          = Clip(DEF:Password_Sage)
                              prm:set_invoice_number= inv:invoice_number - 1
                              prm:invoice_no        = '*'
                              access:paramss.insert()
                          end!if access:paramss.primerecord() = Level:Benign
                  !COURIER
                          get(paramss,0)
                          if access:paramss.primerecord() = Level:Benign
                              prm:account_ref       = Stripcomma(job:Account_number)
                              prm:name              = Stripcomma(job:company_name)
                              prm:address_1         = Stripcomma(job:address_line1)
                              prm:address_2         = Stripcomma(job:address_line2)
                              prm:address_3         = Stripcomma(job:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(job:postcode)
                              prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                              prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                              prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                              prm:del_address_4     = ''
                              prm:del_address_5     = Stripcomma(job:postcode_delivery)
                              prm:cust_tel_number   = Stripcomma(job:telephone_number)
                              If job:surname <> ''
                                  if job:title = '' and job:initial = '' 
                                      prm:contact_name = Stripcomma(clip(job:surname))
                                  elsif job:title = '' and job:initial <> ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                  elsif job:title <> '' and job:initial = ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                  elsif job:title <> '' and job:initial <> ''
                                      prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                  else
                                      prm:contact_name = ''
                                  end
                              else
                                  prm:contact_name = ''
                              end
                              prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                              prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                              prm:notes_3           = ''
                              prm:taken_by          = Stripcomma(job:who_booked)
                              prm:order_number      = Stripcomma(job:order_number)
                              prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                              prm:payment_ref       = ''
                              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                              prm:global_details    = ''
                              prm:items_net         = Stripcomma(job:invoice_labour_cost + job:invoice_parts_cost + job:invoice_courier_cost)
                              prm:items_tax         = Stripcomma(Round(job:invoice_labour_cost * (inv:vat_rate_labour/100),.01) + |
                                                          Round(job:invoice_parts_cost * (inv:vat_rate_parts/100),.01) + |
                                                          Round(job:invoice_courier_cost * (inv:vat_rate_labour/100),.01))
                              prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
                              prm:description       = Stripcomma(DEF:Courier_Description)
                              prm:nominal_code      = Stripcomma(DEF:Courier_Code)
                              prm:qty_order         = '1'
                              prm:unit_price        = '0'
                              prm:net_amount        = Stripcomma(job:courier_cost)
                              prm:tax_amount        = Stripcomma(Round(job:invoice_courier_cost * (inv:vat_rate_labour/100),.01))
                              prm:comment_1         = Stripcomma(job:charge_type)
                              prm:comment_2         = Stripcomma(job:warranty_charge_type)
                              prm:unit_of_sale      = '0'
                              prm:full_net_amount   = '0'
                              prm:invoice_date      = Clip(Day(inv:date_created)) & '/' & Clip(Month(inv:date_created)) & '/' & Clip(Format(Year(inv:date_Created),@n4))
                              prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                              prm:user_name         = Clip(DEF:User_Name_Sage)
                              prm:password          = Clip(DEF:Password_Sage)
                              prm:set_invoice_number= inv:invoice_number - 1
                              prm:invoice_no        = '*'
                              access:paramss.insert()
                          end!if access:paramss.primerecord() = Level:Benign
                          access:paramss.close()
                          Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
                          sage_error# = 0
                          access:paramss.open()
                          access:paramss.usefile()
                          Set(paramss,0)
                          If access:paramss.next()
                              sage_error# = 1
                          Else!If access:paramss.next()
                              If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                                  sage_error# = 1
                              Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                                  If inv:invoice_number <> prm:invoice_no
                                      Case MessageEx('Sage has NOT returned the same invoice number. This means that the selected invoice number already exists on Sage.<13,10><13,10>Sage will have created an invoice with the number '&clip(prm:invoice_no)&'.<13,10><13,10>You will need to manually delete this.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                  Else!
                                      Case MessageEx('Invoice Created on Sage','ServiceBase 2000',|
                                                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                  End!If inv:invoice_number = prm:invoice_no
                              End!If prm:invoice_no = '-1'
                          End!If access:paramss.next()
                          access:paramss.close()
                      End!If access:jobs.fetch(job:InvoiceNumberKey) = Level:Benign
          
                  Of 'WAR'
                      file_error# = 0
                      glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
                      Remove(paramss)
                      
                      access:paramss.open()
                      access:paramss.usefile()
              !LABOUR
                      access:subtracc.clearkey(sub:account_number_key)
                      sub:account_number = inv:account_number
                      access:subtracc.fetch(sub:account_number_key)
                      access:tradeacc.clearkey(tra:account_number_key)
                      tra:account_number = sub:main_account_number
                      access:tradeacc.fetch(tra:account_number_key)
          
                      get(paramss,0)
                      if access:paramss.primerecord() = Level:Benign
                          prm:account_ref       = Stripcomma(sub:Account_number)
                          If tra:invoice_sub_accounts = 'YES'
                              prm:name              = Stripcomma(sub:company_name)
                              prm:address_1         = Stripcomma(sub:address_line1)
                              prm:address_2         = Stripcomma(sub:address_line2)
                              prm:address_3         = Stripcomma(sub:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(sub:postcode)
                              prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                              prm:contact_name      = Stripcomma(sub:contact_name)
                          Else!If tra:invoice_sub_accounts = 'YES'
                              prm:name              = Stripcomma(tra:company_name)
                              prm:address_1         = Stripcomma(tra:address_line1)
                              prm:address_2         = Stripcomma(tra:address_line2)
                              prm:address_3         = Stripcomma(tra:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(tra:postcode)
                              prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                              prm:contact_name      = Stripcomma(tra:contact_name)
                          End!If tra:invoice_sub_accounts = 'YES'
                          prm:del_address_1     = ''
                          prm:del_address_2     = ''
                          prm:del_address_3     = ''
                          prm:del_address_4     = ''
                          prm:del_address_5     = ''
                          prm:notes_1           = ''
                          prm:notes_2           = ''
                          prm:notes_3           = ''
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          prm:taken_by = use:user_code
                          prm:order_number      = Stripcomma(INV:Claim_Reference)
                          prm:cust_order_number = 'B' & Stripcomma(INV:Batch_Number)
                          prm:payment_ref       = ''
                          prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                          prm:global_details    = ''
                          prm:items_net         = Stripcomma(inv:labour_paid + inv:parts_paid + inv:courier_paid)
                          prm:items_tax         = Stripcomma((Round(inv:labour_paid * (inv:vat_rate_labour/100),.01)) + (Round(inv:parts_paid * (inv:vat_rate_parts/100),.01)) + |
                                                          (Round(inv:courier_paid * (inv:vat_rate_labour/100),.01)))
                          prm:stock_code        = StripComma(DEF:Labour_Stock_Code)
                          prm:description       = StripComma(DEF:Labour_Description)
                          prm:nominal_code      = StripComma(DEF:Labour_Code)
                          prm:qty_order         = '1'
                          prm:unit_price        = '0'
                          prm:net_amount        = StripComma(inv:labour_paid)
                          prm:tax_amount        = StripComma(Round(inv:labour_paid * (inv:vat_rate_labour/100),.01))
                          prm:comment_1         = ''
                          prm:comment_2         = ''
                          prm:unit_of_sale      = '0'
                          prm:full_net_amount   = '0'
                          prm:invoice_date      = Clip(Day(inv:date_created)) & '/' & Clip(Month(inv:date_created)) & '/' & Clip(Format(Year(inv:date_Created),@n4))
                          prm:data_filepath     = Clip(def:path_sage) & '\'
                          prm:user_name         = Clip(DEF:User_Name_Sage)
                          prm:password          = Clip(DEF:Password_Sage)
                          prm:set_invoice_number= inv:invoice_number - 1
                          prm:invoice_no        = '*'
                          access:paramss.insert()
                      end!if access:paramss.primerecord() = Level:Benign
              !PARTS
                      get(paramss,0)
                      if access:paramss.primerecord() = Level:Benign
                          prm:account_ref       = Stripcomma(sub:Account_number)
                          If tra:invoice_sub_accounts = 'YES'
                              prm:name              = Stripcomma(sub:company_name)
                              prm:address_1         = Stripcomma(sub:address_line1)
                              prm:address_2         = Stripcomma(sub:address_line2)
                              prm:address_3         = Stripcomma(sub:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(sub:postcode)
                              prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                              prm:contact_name      = Stripcomma(sub:contact_name)
                          Else!If tra:invoice_sub_accounts = 'YES'
                              prm:name              = Stripcomma(tra:company_name)
                              prm:address_1         = Stripcomma(tra:address_line1)
                              prm:address_2         = Stripcomma(tra:address_line2)
                              prm:address_3         = Stripcomma(tra:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(tra:postcode)
                              prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                              prm:contact_name      = Stripcomma(tra:contact_name)
                          End!If tra:invoice_sub_accounts = 'YES'
                          prm:del_address_1     = ''
                          prm:del_address_2     = ''
                          prm:del_address_3     = ''
                          prm:del_address_4     = ''
                          prm:del_address_5     = ''
                          prm:notes_1           = ''
                          prm:notes_2           = ''
                          prm:notes_3           = ''
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          prm:taken_by = use:user_code
                          prm:order_number      = Stripcomma(INV:Claim_Reference)
                          prm:cust_order_number = 'B' & Stripcomma(INV:Batch_Number)
                          prm:payment_ref       = ''
                          prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                          prm:global_details    = ''
                          prm:items_net         = Stripcomma(inv:labour_paid + inv:parts_paid + inv:courier_paid)
                          prm:items_tax         = Stripcomma((Round(inv:labour_paid * (inv:vat_rate_labour/100),.01)) + (Round(inv:parts_paid * (inv:vat_rate_parts/100),.01)) + |
                                                          (Round(inv:courier_paid * (inv:vat_rate_labour/100),.01)))
                          prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                          prm:description       = Stripcomma(DEF:Parts_Description)
                          prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                          prm:qty_order         = '1'
                          prm:unit_price        = '0'
                          prm:net_amount        = Stripcomma(inv:parts_paid)
                          prm:tax_amount        = Stripcomma(Round(inv:parts_paid * (inv:vat_rate_parts/100),.01))
                          prm:comment_1         = ''
                          prm:comment_2         = ''
                          prm:unit_of_sale      = '0'
                          prm:full_net_amount   = '0'
                          prm:invoice_date      = Clip(Day(inv:date_created)) & '/' & Clip(Month(inv:date_created)) & '/' & Clip(Format(Year(inv:date_Created),@n4))
                          prm:data_filepath     = Clip(def:path_sage) & '\'
                          prm:user_name         = Clip(DEF:User_Name_Sage)
                          prm:password          = Clip(DEF:Password_Sage)
                          prm:set_invoice_number= inv:invoice_number - 1
                          prm:invoice_no        = '*'
                          access:paramss.insert()
                      end!if access:paramss.primerecord() = Level:Benign
              !COURIER
                      get(paramss,0)
                      if access:paramss.primerecord() = Level:Benign
                          prm:account_ref       = Stripcomma(sub:Account_number)
                          If tra:invoice_sub_accounts = 'YES'
                              prm:name              = Stripcomma(sub:company_name)
                              prm:address_1         = Stripcomma(sub:address_line1)
                              prm:address_2         = Stripcomma(sub:address_line2)
                              prm:address_3         = Stripcomma(sub:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(sub:postcode)
                              prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                              prm:contact_name      = Stripcomma(sub:contact_name)
                          Else!If tra:invoice_sub_accounts = 'YES'
                              prm:name              = Stripcomma(tra:company_name)
                              prm:address_1         = Stripcomma(tra:address_line1)
                              prm:address_2         = Stripcomma(tra:address_line2)
                              prm:address_3         = Stripcomma(tra:address_line3)
                              prm:address_4         = ''
                              prm:address_5         = Stripcomma(tra:postcode)
                              prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                              prm:contact_name      = Stripcomma(tra:contact_name)
                          End!If tra:invoice_sub_accounts = 'YES'
                          prm:del_address_1     = ''
                          prm:del_address_2     = ''
                          prm:del_address_3     = ''
                          prm:del_address_4     = ''
                          prm:del_address_5     = ''
                          prm:notes_1           = ''
                          prm:notes_2           = ''
                          prm:notes_3           = ''
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          prm:taken_by = use:user_code
                          prm:order_number      = Stripcomma(INV:Claim_Reference)
                          prm:cust_order_number = 'B' & Stripcomma(INV:Batch_Number)
                          prm:payment_ref       = ''
                          prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                          prm:global_details    = ''
                          prm:items_net         = Stripcomma(inv:labour_paid + inv:parts_paid + inv:courier_paid)
                          prm:items_tax         = Stripcomma((Round(inv:labour_paid * (inv:vat_rate_labour/100),.01)) + (Round(inv:parts_paid * (inv:vat_rate_parts/100),.01)) + |
                                                          (Round(inv:courier_paid * (inv:vat_rate_labour/100),.01)))
                          prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
                          prm:description       = Stripcomma(DEF:Courier_Description)
                          prm:nominal_code      = Stripcomma(DEF:Courier_Code)
                          prm:qty_order         = '1'
                          prm:unit_price        = '0'
                          prm:net_amount        = Stripcomma(inv:courier_paid)
                          prm:tax_amount        = Stripcomma(Round(inv:courier_paid * (inv:vat_rate_labour/100),.01))
                          prm:comment_1         = ''
                          prm:comment_2         = ''
                          prm:unit_of_sale      = '0'
                          prm:full_net_amount   = '0'
                          prm:invoice_date      = Clip(Day(inv:date_created)) & '/' & Clip(Month(inv:date_created)) & '/' & Clip(Format(Year(inv:date_Created),@n4))
                          prm:data_filepath     = Clip(def:path_sage) & '\'
                          prm:user_name         = Clip(DEF:User_Name_Sage)
                          prm:password          = Clip(DEF:Password_Sage)
                          prm:set_invoice_number= inv:invoice_number - 1
                          prm:invoice_no        = '*'
                          access:paramss.insert()
                      end!if access:paramss.primerecord() = Level:Benign
                      access:paramss.close()
                      Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
                      sage_error# = 0
                      access:paramss.open()
                      access:paramss.usefile()
                      Set(paramss,0)
                      If access:paramss.next()
                          sage_error# = 1
                      Else!If access:paramss.next()
                          If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                              sage_error# = 1
                          Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                              If inv:invoice_number <> prm:invoice_no
                                  Case MessageEx('Sage has NOT returned the same invoice number. This means that the seleceted invoice number already exists on Sage.<13,10><13,10>Sage will have created an invoice will the number '&clip(prm:invoice_no)&'.<13,10><13,10>You will need to manually delete this.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                              Else!
                                  Case MessageEx('Invoice created on Sage.','ServiceBase 2000',|
                                                 'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
          
                              End!If inv:invoice_number = prm:invoice_no
                          End!If prm:invoice_no = '-1'
                      End!If access:paramss.next()
                      access:paramss.close()
          
              End!Case inv:invoice_type
          Of 2 ! &No Button
      End!Case MessageEx
    OF ?Print_Credit_Note
      ThisWindow.Update
      thiswindow.reset(1)
      If inv:invoicecredit = 'CRE'
          !Print Credit Note
          glo:Select1  = inv:invoice_number
          Credit_Note
          glo:Select1  = ''
      Else!If inv:invoicecredit = 'CRE'
          If inv:invoice_type = 'CHA'
              Case MessageEx('You are attempting to Credit a Multiple Chargeable Invoice. This facility is not available in this version of ServiceBase 2000.<13,10><13,10>If you wish to participate in a beta test of this facility, please email/fax PC Control Systems Ltd  requesting this facility.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Else!If inv:invoice_type = 'CHA'
              Case MessageEx('Are you sure you want to create a Credit Note for this Invoice?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      glo:file_name   = 'C' & Format(clock(),@n07) & '.DAT'
                      Create_Credit_Note(inv:invoice_number)
                  Of 2 ! &No Button
              End!Case MessageEx
      
          End!If inv:invoice_type = 'CHA'
      
      End!If inv:invoicecredit = 'CRE'
    OF ?Remove_Invoice
      ThisWindow.Update
      thiswindow.reset(1)
      do_update# = 1
      check_access('REMOVE INVOICE',x")
      if x" = false
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
        do_update# = 0
      end
      If do_update# = 1
          Case MessageEx('Are you sure you want to remove invoice number '&clip(inv:invoice_number)&'?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
      
                  access:invoice.clearkey(inv:invoice_number_key)
                  inv:invoice_number  = brw1.q.inv:invoice_number
                  If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
      
                      setcursor(cursor:wait)
              
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
              
                      Case inv:invoice_type
                          Of 'WAR'
                              recordstoprocess    = Records(job:chainvoicekey)
              
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:warinvoicenokey)
                              job:invoice_number_warranty = inv:invoice_number
                              set(job:warinvoicenokey,job:warinvoicenokey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:invoice_number_warranty <> inv:invoice_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
              
                                  Do GetNextRecord2
              
                                  pos = Position(job:warinvoicenokey)
                                  JOB:WInvoice_Courier_Cost=''
                                  JOB:WInvoice_Labour_Cost=''
                                  JOB:WInvoice_Parts_Cost=''
                                  JOB:WInvoice_Sub_Total=''
                                  JOB:Invoice_Date_Warranty=''
                                  JOB:Invoice_Number_Warranty=''
                                  job:edi = 'YES'
                                  access:jobs.update()
                                  Reset(job:warinvoicenokey,pos)
                              end !loop
                              access:jobs.restorefile(save_job_id)
              
                              Delete(invoice)
              
                          Else
                              recordstoprocess    = Records(job:chainvoicekey)
              
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:InvoiceNumberKey)
                              job:invoice_number = inv:invoice_number
                              set(job:InvoiceNumberKey,job:InvoiceNumberKey)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:invoice_number <> inv:invoice_number      |
                                      then break.  ! end if
                                  yldcnt# += 1
                                  if yldcnt# > 25
                                     yield() ; yldcnt# = 0
                                  end !if
              
                                  Do GetNextRecord2
              
                                  pos = Position(job:InvoiceNumberKey)
                                  JOB:Invoice_Sub_Total=''
                                  JOB:Invoice_Parts_Cost=''
                                  JOB:Invoice_Labour_Cost=''
                                  JOB:Invoice_Courier_Cost=''
                                  JOB:Invoice_Date=''
                                  JOB:Invoice_Number=''
                                  Job:InvoiceBatch    = ''
                                  Job:InvoiceStatus   = ''
                                  Job:InvoiceQuery    = ''
                                  Job:InvoiceAccount  = ''
                                  
                                  access:jobs.update()
                                  Reset(job:InvoiceNumberKey,pos)
      Compile('***',Debug=1)
          message('After Update','Debug Message', icon:exclamation)
      ***
      
                              end !loop
                              access:jobs.restorefile(save_job_id)
              
                              Delete(invoice)
              
                      End!Case inv:invoice_type
              
                      setcursor()
                      close(progresswindow)
                  End!If access:invoice.tryfetch(inv:invoice_number_key) = Level:Benign
              Of 2 ! &No Button
          End!Case MessageEx
      End!If do_update# = True
      BRW1.ResetQueue(1)
      Select(?browse:1)
    OF ?Reprint_Invoice
      ThisWindow.Update
      ThisWindow.Update
      glo:Select1  = inv:invoice_number
      glo:Select2  = inv:batch_number
      glo:Select3  = inv:account_number
      
      Case inv:invoice_type
          Of 'CHA'
              Chargeable_Summary_Invoice
          Of 'SIN'
              Single_Invoice
          Of 'WAR'
              Summary_Warranty_Invoice
          Of 'RET'
              Retail_Single_Invoice
          Of 'BAT'
              Chargeable_Batch_Invoice
      End
      glo:Select1 = ''
      glo:Select2 = ''
      glo:Select3 = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Invoice')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore13.IgnoreEvent = True                      !Xplore
     Xplore13.IgnoreEvent = False                     !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  tmp:filter  = brw1::view:browse{prop:filter}
  tmp:order   = brw1::view:browse{prop:order}
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    Case inv:invoice_type
        Of 'RET'
            If Choice(?Sheet2) <> 2
                Select(?Sheet2,2)
            End!If Case(?Sheet2) <> 2
        Else
            If Choice(?sheet2) <> 1
                Select(?sheet2,1)
            End!If Case(?sheet2) <> 1
    End!Case inv:invoice_type
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?inv:Invoice_Number
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Set(Defaults)
      access:defaults.next()
      If def:use_sage = 'YES'
      !    Hide(?reprint_invoice)
          Hide(?remove_invoice)
          Unhide(?create_sage_invoice)
      End!If def:use_sage = 'YES'
      Compile('***',Debug=1)
          Unhide(?tmp:filter)
          Unhide(?tmp:order)
      ***
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      POST(xpEVENT:Xplore + 13)                       !Xplore
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      Xplore13.GetColumnInfo()                        !Xplore
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      Xplore13.GetColumnInfo()                        !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
      OF xpEVENT:Xplore + 13                          !Xplore
        Xplore13.GetColumnInfo()                      !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Amend_Invoice
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  Case inv:invoice_Type
      Of 'SIN'
          access:jobs.clearkey(job:invoicenumberkey)
          job:invoice_number  = inv:invoice_number
          If access:jobs.tryfetch(job:invoicenumberkey) = Level:Benign
              !Found
              tmp:labourcost  = job:invoice_labour_cost
              tmp:partscost   = job:invoice_parts_cost
              tmp:couriercost = job:invoice_courier_cost
          Else! If access:jobs.tryfetch(job:invoice_number_key) = Level:Benign
              !Error
              ! Start Change 2380 BE(25/03/03)
              ! Some Invoices do not have related jobs - this is a situation
              ! that should not occur and is a severe bug.
              ! this fix is to ensure that in such circumstances the costs displayed do not
              ! erroneously duplicate the previous entries values.
              tmp:labourcost  = 0
              tmp:partscost   = 0
              tmp:couriercost = 0
              ! End Change 2380 BE(25/03/03)
          End! If access:.tryfetch(job:invoice_number_key) = Level:Benign
      Else
          tmp:labourcost  = INV:Labour_Paid
          tmp:partscost   = INV:Parts_Paid
          tmp:couriercost = INV:Courier_Paid
          
  End!Case inv:invoice_Type
  CASE (inv:Invoice_Type)
  OF 'CHA'
    Invoice_Type_Temp = 'Chargeable'
  OF 'SIN'
    Invoice_Type_Temp = 'Chargeable'
  OF 'WAR'
    Invoice_Type_Temp = 'Warranty'
  OF 'RET'
    Invoice_Type_Temp = 'Retail'
  OF 'BAT'
    Invoice_Type_Temp = 'Chargeable'
  ELSE
    Invoice_Type_Temp = 'Error'
  END
  CASE (inv:InvoiceCredit)
  OF 'INV'
    invoice_Credit_temp = 'Invoice'
  ELSE
    invoice_Credit_temp = 'Credit'
  END
  PARENT.SetQueueRecord
  SELF.Q.inv:Invoice_Number_NormalFG = -1
  SELF.Q.inv:Invoice_Number_NormalBG = -1
  SELF.Q.inv:Invoice_Number_SelectedFG = -1
  SELF.Q.inv:Invoice_Number_SelectedBG = -1
  SELF.Q.inv:Account_Number_NormalFG = -1
  SELF.Q.inv:Account_Number_NormalBG = -1
  SELF.Q.inv:Account_Number_SelectedFG = -1
  SELF.Q.inv:Account_Number_SelectedBG = -1
  SELF.Q.inv:Date_Created_NormalFG = -1
  SELF.Q.inv:Date_Created_NormalBG = -1
  SELF.Q.inv:Date_Created_SelectedFG = -1
  SELF.Q.inv:Date_Created_SelectedBG = -1
  IF (inv:InvoiceCredit = 'CRE')
    SELF.Q.Invoice_Type_Temp_NormalFG = 255
    SELF.Q.Invoice_Type_Temp_NormalBG = 16777215
    SELF.Q.Invoice_Type_Temp_SelectedFG = 16777215
    SELF.Q.Invoice_Type_Temp_SelectedBG = 255
  ELSE
    SELF.Q.Invoice_Type_Temp_NormalFG = -1
    SELF.Q.Invoice_Type_Temp_NormalBG = -1
    SELF.Q.Invoice_Type_Temp_SelectedFG = -1
    SELF.Q.Invoice_Type_Temp_SelectedBG = -1
  END
  IF (inv:InvoiceCredit = 'CRE')
    SELF.Q.invoice_Credit_temp_NormalFG = 255
    SELF.Q.invoice_Credit_temp_NormalBG = 16777215
    SELF.Q.invoice_Credit_temp_SelectedFG = 16777215
    SELF.Q.invoice_Credit_temp_SelectedBG = 255
  ELSE
    SELF.Q.invoice_Credit_temp_NormalFG = -1
    SELF.Q.invoice_Credit_temp_NormalBG = -1
    SELF.Q.invoice_Credit_temp_SelectedFG = -1
    SELF.Q.invoice_Credit_temp_SelectedBG = -1
  END
  SELF.Q.tmp:couriercost_NormalFG = -1
  SELF.Q.tmp:couriercost_NormalBG = -1
  SELF.Q.tmp:couriercost_SelectedFG = -1
  SELF.Q.tmp:couriercost_SelectedBG = -1
  SELF.Q.tmp:LabourCost_NormalFG = -1
  SELF.Q.tmp:LabourCost_NormalBG = -1
  SELF.Q.tmp:LabourCost_SelectedFG = -1
  SELF.Q.tmp:LabourCost_SelectedBG = -1
  SELF.Q.tmp:PartsCost_NormalFG = -1
  SELF.Q.tmp:PartsCost_NormalBG = -1
  SELF.Q.tmp:PartsCost_SelectedFG = -1
  SELF.Q.tmp:PartsCost_SelectedBG = -1
  SELF.Q.inv:Courier_Paid_NormalFG = -1
  SELF.Q.inv:Courier_Paid_NormalBG = -1
  SELF.Q.inv:Courier_Paid_SelectedFG = -1
  SELF.Q.inv:Courier_Paid_SelectedBG = -1
  SELF.Q.inv:Labour_Paid_NormalFG = -1
  SELF.Q.inv:Labour_Paid_NormalBG = -1
  SELF.Q.inv:Labour_Paid_SelectedFG = -1
  SELF.Q.inv:Labour_Paid_SelectedBG = -1
  SELF.Q.inv:Parts_Paid_NormalFG = -1
  SELF.Q.inv:Parts_Paid_NormalBG = -1
  SELF.Q.inv:Parts_Paid_SelectedFG = -1
  SELF.Q.inv:Parts_Paid_SelectedBG = -1
  SELF.Q.inv:Invoice_Type_NormalFG = -1
  SELF.Q.inv:Invoice_Type_NormalBG = -1
  SELF.Q.inv:Invoice_Type_SelectedFG = -1
  SELF.Q.inv:Invoice_Type_SelectedBG = -1
  SELF.Q.inv:Total_Claimed_NormalFG = -1
  SELF.Q.inv:Total_Claimed_NormalBG = -1
  SELF.Q.inv:Total_Claimed_SelectedFG = -1
  SELF.Q.inv:Total_Claimed_SelectedBG = -1
  SELF.Q.inv:InvoiceCredit_NormalFG = -1
  SELF.Q.inv:InvoiceCredit_NormalBG = -1
  SELF.Q.inv:InvoiceCredit_SelectedFG = -1
  SELF.Q.inv:InvoiceCredit_SelectedBG = -1
  SELF.Q.Invoice_Type_Temp = Invoice_Type_Temp        !Assign formula result to display queue
  SELF.Q.invoice_Credit_temp = invoice_Credit_temp    !Assign formula result to display queue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW13.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,13,'GetFreeElementName'
  IF BRW13.ViewOrder = True                           !Xplore
     RETURN('UPPER(' & CLIP(BRW13.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW13.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,13,'GetFreeElementPosition'
  IF BRW13.ViewOrder = True                           !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW13.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change:Job
  END


BRW13.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,13
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore13.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore13.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore13.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW13.ViewOrder = True                           !Xplore
    SavePtr# = POINTER(Xplore13.sq)                   !Xplore
    Xplore13.SetupOrder(BRW13.SortOrderNbr)           !Xplore
    GET(Xplore13.sq,SavePtr#)                         !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW13.SortOrderNbr,Force)       !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW13.FileSeqOn = True                        !Xplore
    RETURN SELF.SetSort(BRW13.FileOrderNbr,Force)     !Xplore
  END                                                 !Xplore
  IF Upper(inv:invoice_type) <> 'WAR'
    RETURN SELF.SetSort(1,Force)
  ELSIF Upper(inv:invoice_type) = 'WAR'
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW13.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,13,'TakeEvent'
  Xplore13.LastEvent = EVENT()                        !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore13.AdjustAllColumns()                  !Xplore
      OF AltF12                                       !Xplore
         Xplore13.ResetToDefault()                    !Xplore
      OF AltR                                         !Xplore
         Xplore13.ToggleBar()                         !Xplore
      OF AltM                                         !Xplore
         Xplore13.InvokePopup()                       !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore13.RightButtonUp                         !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW13.FileSeqOn = False                        !Xplore
       Xplore13.LeftButtonUp(0)                       !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW13.SavePosition()                          !Xplore
       Xplore13.HandleMyEvents()                      !Xplore
       !BRW13.RestorePosition()                       !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW13.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore13.LeftButton2()                       !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW13.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW14.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !inv:Invoice_Number
  OF 6 !inv:Account_Number
  OF 11 !inv:Date_Created
  OF 16 !Invoice_Type_Temp
        ColumnString     = SUB(LEFT(BRW1.Q.Invoice_Type_Temp),1,20)
  OF 21 !invoice_Credit_temp
        ColumnString     = SUB(LEFT(BRW1.Q.invoice_Credit_temp),1,20)
  OF 26 !tmp:couriercost
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:couriercost),1,20)
  OF 31 !tmp:LabourCost
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:LabourCost),1,20)
  OF 36 !tmp:PartsCost
        ColumnString     = SUB(LEFT(BRW1.Q.tmp:PartsCost),1,20)
  OF 41 !inv:Courier_Paid
  OF 46 !inv:Labour_Paid
  OF 51 !inv:Parts_Paid
  OF 56 !inv:Invoice_Type
  OF 61 !inv:Total_Claimed
  OF 66 !inv:InvoiceCredit
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step14)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?inv:Invoice_Number,inv:Invoice_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,inv:Account_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,inv:Date_Created,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,inv:Invoice_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,inv:Courier_Paid,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,inv:Labour_Paid,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,inv:Parts_Paid,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(,inv:Invoice_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,inv:Total_Claimed,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step14.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator14)
      Xplore1Locator14.Init(,inv:InvoiceCredit,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(INVOICE)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('inv:Invoice_Number')         !Field Name
                SHORT(36)                             !Default Column Width
                PSTRING('Invoice No')                 !Header
                PSTRING('@p<<<<<<<<<<<<<<#p')         !Picture
                PSTRING('Invoice No')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('inv:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('inv:Date_Created')           !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date Created')               !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Date Created')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('Invoice_Type_Temp')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Invoice_Type_Temp')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Invoice_Type_Temp')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('invoice_Credit_temp')        !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('invoice_Credit_temp')        !Header
                PSTRING('@S20')                       !Picture
                PSTRING('invoice_Credit_temp')        !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:couriercost')            !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:couriercost')            !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:couriercost')            !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LabourCost')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LabourCost')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LabourCost')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:PartsCost')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:PartsCost')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:PartsCost')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('inv:Courier_Paid')           !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Courier Cost')               !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Courier Cost')               !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('inv:Labour_Paid')            !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Labour Cost')                !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Labour Cost')                !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('inv:Parts_Paid')             !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Parts Cost')                 !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Parts Cost')                 !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('inv:Invoice_Type')           !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Invoice Type')               !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Invoice Type')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('inv:Total_Claimed')          !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Total Cost')                 !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Total Cost')                 !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('inv:InvoiceCredit')          !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Invoice Credit')             !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Invoice Type: Invoice or Credit') !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(14)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('Invoice_Type_Temp')          !Column Field
                SHORT(1)                              !Nos of Sort Fields
                PSTRING('inv:Invoice_Type')           !Sort Field Seq:1
                BYTE(0)                               !Ascending
                PSTRING('@s3')                        !Picture
                !-------------------------
                PSTRING('invoice_Credit_temp')        !Column Field
                SHORT(1)                              !Nos of Sort Fields
                PSTRING('inv:InvoiceCredit')          !Sort Field Seq:1
                BYTE(0)                               !Ascending
                PSTRING('@s3')                        !Picture
                !-------------------------
                PSTRING('tmp:couriercost')            !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                PSTRING('tmp:LabourCost')             !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                PSTRING('tmp:PartsCost')              !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                END
XpSortDim       SHORT(5)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)
BRW13.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW13.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW13.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW13.ResetPairsQ PROCEDURE()
  CODE
Xplore13.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore13.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore13.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW13.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !job:Ref_Number
  OF 2 !job:Model_Number
  OF 3 !job:ESN
  OF 4 !job:Unit_Type
  OF 5 !job:Invoice_Number
  OF 6 !job:Invoice_Number_Warranty
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore13.SetNewOrderFields PROCEDURE()
  CODE
  BRW13.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore13.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore13Step6,job:InvoiceNumberKey)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,job:InvoiceNumberKey)
  EXECUTE SortQRecord
    BEGIN
      Xplore13Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore13Locator1)
      Xplore13Locator1.Init(,job:Ref_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore13Locator2)
      Xplore13Locator2.Init(,job:Model_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore13Locator3)
      Xplore13Locator3.Init(,job:ESN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore13Locator4)
      Xplore13Locator4.Init(,job:Unit_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step5.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore13Locator5)
      Xplore13Locator5.Init(,job:Invoice_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore13Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore13Locator6)
      Xplore13Locator6.Init(,job:Invoice_Number_Warranty,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(job:Invoice_Number,inv:Invoice_Number)
  RETURN
!================================================================================
Xplore13.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore13.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW13.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBS)
  END
  RETURN TotalRecords
!================================================================================
Xplore13.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('ESN/IMEI')                   !Header
                PSTRING('@s16')                       !Picture
                PSTRING('ESN/IMEI')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Unit_Type')              !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Unit Type')                  !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Unit Type')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Invoice_Number')         !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Invoice Number')             !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Invoice Number')             !Description
                STRING('R')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Invoice_Number_Warranty') !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Invoice Number')             !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Invoice Number')             !Description
                STRING('R')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(6)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?INV:Invoice_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

