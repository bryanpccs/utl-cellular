

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01013.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSIMNumber PROCEDURE (func:RefNumber)            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Update SIM Number'),AT(,,220,67),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,32),USE(?Sheet1),SPREAD
                         TAB('SIM Number'),USE(?SIMNumberTAB)
                           PROMPT('SIM Number'),AT(8,20),USE(?jobe:SIMNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(jobe:SIMNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SIM Number'),TIP('SIM Number'),UPR
                         END
                       END
                       PANEL,AT(4,40,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,44,56,16),USE(?Button2),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,44,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?SIMNumberTAB{prop:Color} = 15066597
    ?jobe:SIMNumber:Prompt{prop:FontColor} = -1
    ?jobe:SIMNumber:Prompt{prop:Color} = 15066597
    If ?jobe:SIMNumber{prop:ReadOnly} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 15066597
    Elsif ?jobe:SIMNumber{prop:Req} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 8454143
    Else ! If ?jobe:SIMNumber{prop:Req} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 16777215
    End ! If ?jobe:SIMNumber{prop:Req} = True
    ?jobe:SIMNumber{prop:Trn} = 0
    ?jobe:SIMNumber{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSIMNumber',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SIMNumberTAB;  SolaceCtrlName = '?SIMNumberTAB';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:SIMNumber:Prompt;  SolaceCtrlName = '?jobe:SIMNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:SIMNumber;  SolaceCtrlName = '?jobe:SIMNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSIMNumber')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSIMNumber')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jobe:SIMNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBSE.Open
  SELF.FilesOpened = True
  access:JOBSE.clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = func:RefNumber
  If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  Else! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      Assert(0,'<13,10>Fetch Error<13,10>')
  End! If access:JOBSE.tryfetch(jobe:RefNumberKey) = Level:Benign
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSIMNumber',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button2
      ThisWindow.Update
      access:JOBSE.Update()
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSIMNumber')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

