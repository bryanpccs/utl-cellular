

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01018.INC'),ONCE        !Local module procedure declarations
                     END


Update_Estimate_Part PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
sav:ExcludeFromOrder BYTE(0)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?epr:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::epr:Record  LIKE(epr:RECORD),STATIC
QuickWindow          WINDOW('Update the WARPARTS File'),AT(,,288,180),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Warranty_Part'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,280,144),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Estimate Part'),AT(8,20),USE(?Prompt8),FONT('Tahoma',10,COLOR:Navy,FONT:bold)
                           PROMPT('Part Number'),AT(8,36),USE(?WPR:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(epr:Part_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('Browse Stock'),AT(212,36,68,10),USE(?browse_stock_button),SKIP,LEFT,ICON('book.ico')
                           PROMPT('Description'),AT(8,52),USE(?WPR:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,48,124,10),USE(epr:Description),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Purchase Cost'),AT(8,64),USE(?WPR:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,64,64,10),USE(epr:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Sale Cost'),AT(8,76),USE(?WPR:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,76,64,10),USE(epr:Sale_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Supplier'),AT(8,92),USE(?Prompt9)
                           COMBO(@s30),AT(84,92,124,10),USE(epr:Supplier),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Quantity'),AT(8,108),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(84,108,64,10),USE(epr:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,RANGE(1,99999999),STEP(1)
                           CHECK('Exclude From Ordering / Decrementing'),AT(84,128,136,8),USE(epr:Exclude_From_Order),VALUE('YES','NO')
                         END
                       END
                       PANEL,AT(4,152,280,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(164,156,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(224,156,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?WPR:Part_Number:Prompt{prop:FontColor} = -1
    ?WPR:Part_Number:Prompt{prop:Color} = 15066597
    If ?epr:Part_Number{prop:ReadOnly} = True
        ?epr:Part_Number{prop:FontColor} = 65793
        ?epr:Part_Number{prop:Color} = 15066597
    Elsif ?epr:Part_Number{prop:Req} = True
        ?epr:Part_Number{prop:FontColor} = 65793
        ?epr:Part_Number{prop:Color} = 8454143
    Else ! If ?epr:Part_Number{prop:Req} = True
        ?epr:Part_Number{prop:FontColor} = 65793
        ?epr:Part_Number{prop:Color} = 16777215
    End ! If ?epr:Part_Number{prop:Req} = True
    ?epr:Part_Number{prop:Trn} = 0
    ?epr:Part_Number{prop:FontStyle} = font:Bold
    ?WPR:Description:Prompt{prop:FontColor} = -1
    ?WPR:Description:Prompt{prop:Color} = 15066597
    If ?epr:Description{prop:ReadOnly} = True
        ?epr:Description{prop:FontColor} = 65793
        ?epr:Description{prop:Color} = 15066597
    Elsif ?epr:Description{prop:Req} = True
        ?epr:Description{prop:FontColor} = 65793
        ?epr:Description{prop:Color} = 8454143
    Else ! If ?epr:Description{prop:Req} = True
        ?epr:Description{prop:FontColor} = 65793
        ?epr:Description{prop:Color} = 16777215
    End ! If ?epr:Description{prop:Req} = True
    ?epr:Description{prop:Trn} = 0
    ?epr:Description{prop:FontStyle} = font:Bold
    ?WPR:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?WPR:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?epr:Purchase_Cost{prop:ReadOnly} = True
        ?epr:Purchase_Cost{prop:FontColor} = 65793
        ?epr:Purchase_Cost{prop:Color} = 15066597
    Elsif ?epr:Purchase_Cost{prop:Req} = True
        ?epr:Purchase_Cost{prop:FontColor} = 65793
        ?epr:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?epr:Purchase_Cost{prop:Req} = True
        ?epr:Purchase_Cost{prop:FontColor} = 65793
        ?epr:Purchase_Cost{prop:Color} = 16777215
    End ! If ?epr:Purchase_Cost{prop:Req} = True
    ?epr:Purchase_Cost{prop:Trn} = 0
    ?epr:Purchase_Cost{prop:FontStyle} = font:Bold
    ?WPR:Sale_Cost:Prompt{prop:FontColor} = -1
    ?WPR:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?epr:Sale_Cost{prop:ReadOnly} = True
        ?epr:Sale_Cost{prop:FontColor} = 65793
        ?epr:Sale_Cost{prop:Color} = 15066597
    Elsif ?epr:Sale_Cost{prop:Req} = True
        ?epr:Sale_Cost{prop:FontColor} = 65793
        ?epr:Sale_Cost{prop:Color} = 8454143
    Else ! If ?epr:Sale_Cost{prop:Req} = True
        ?epr:Sale_Cost{prop:FontColor} = 65793
        ?epr:Sale_Cost{prop:Color} = 16777215
    End ! If ?epr:Sale_Cost{prop:Req} = True
    ?epr:Sale_Cost{prop:Trn} = 0
    ?epr:Sale_Cost{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?epr:Supplier{prop:ReadOnly} = True
        ?epr:Supplier{prop:FontColor} = 65793
        ?epr:Supplier{prop:Color} = 15066597
    Elsif ?epr:Supplier{prop:Req} = True
        ?epr:Supplier{prop:FontColor} = 65793
        ?epr:Supplier{prop:Color} = 8454143
    Else ! If ?epr:Supplier{prop:Req} = True
        ?epr:Supplier{prop:FontColor} = 65793
        ?epr:Supplier{prop:Color} = 16777215
    End ! If ?epr:Supplier{prop:Req} = True
    ?epr:Supplier{prop:Trn} = 0
    ?epr:Supplier{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?epr:Quantity{prop:ReadOnly} = True
        ?epr:Quantity{prop:FontColor} = 65793
        ?epr:Quantity{prop:Color} = 15066597
    Elsif ?epr:Quantity{prop:Req} = True
        ?epr:Quantity{prop:FontColor} = 65793
        ?epr:Quantity{prop:Color} = 8454143
    Else ! If ?epr:Quantity{prop:Req} = True
        ?epr:Quantity{prop:FontColor} = 65793
        ?epr:Quantity{prop:Color} = 16777215
    End ! If ?epr:Quantity{prop:Req} = True
    ?epr:Quantity{prop:Trn} = 0
    ?epr:Quantity{prop:FontStyle} = font:Bold
    ?epr:Exclude_From_Order{prop:Font,3} = -1
    ?epr:Exclude_From_Order{prop:Color} = 15066597
    ?epr:Exclude_From_Order{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Estimate_Part',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Estimate_Part',1)
    SolaceViewVars('sav:ExcludeFromOrder',sav:ExcludeFromOrder,'Update_Estimate_Part',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Update_Estimate_Part',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Estimate_Part',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Estimate_Part',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Update_Estimate_Part',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Part_Number:Prompt;  SolaceCtrlName = '?WPR:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epr:Part_Number;  SolaceCtrlName = '?epr:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?browse_stock_button;  SolaceCtrlName = '?browse_stock_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Description:Prompt;  SolaceCtrlName = '?WPR:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epr:Description;  SolaceCtrlName = '?epr:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Purchase_Cost:Prompt;  SolaceCtrlName = '?WPR:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epr:Purchase_Cost;  SolaceCtrlName = '?epr:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Sale_Cost:Prompt;  SolaceCtrlName = '?WPR:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epr:Sale_Cost;  SolaceCtrlName = '?epr:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epr:Supplier;  SolaceCtrlName = '?epr:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epr:Quantity;  SolaceCtrlName = '?epr:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?epr:Exclude_From_Order;  SolaceCtrlName = '?epr:Exclude_From_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Estimate Part'
  OF ChangeRecord
    ActionMessage = 'Changing An Estimate Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Estimate_Part')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Estimate_Part')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt8
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(epr:Record,History::epr:Record)
  SELF.AddHistoryField(?epr:Part_Number,6)
  SELF.AddHistoryField(?epr:Description,7)
  SELF.AddHistoryField(?epr:Purchase_Cost,9)
  SELF.AddHistoryField(?epr:Sale_Cost,10)
  SELF.AddHistoryField(?epr:Supplier,8)
  SELF.AddHistoryField(?epr:Quantity,12)
  SELF.AddHistoryField(?epr:Exclude_From_Order,14)
  SELF.AddUpdateFile(Access:ESTPARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:ESTPARTS.Open
  Relate:ESTPARTS_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:WARPARTS.UseFile
  Access:JOBS.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ESTPARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  !Save Fields
  sav:ExcludeFromORder  = epr:Exclude_From_Order
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(epr:Supplier,?epr:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  FDCB6.AddUpdateField(sup:Company_Name,epr:Supplier)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:ESTPARTS.Close
    Relate:ESTPARTS_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Estimate_Part',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    epr:Adjustment = 'NO'
    epr:Warranty_Part = 'NO'
    epr:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?epr:Exclude_From_Order
      If ~0{prop:acceptall}
          If sav:ExcludeFromOrder <> epr:Exclude_From_Order
              If SecurityCheck('PARTS - EXCLUDE FROM ORDER')
                  Case MessageEx('You do not have access to this option!','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  epr:Exclude_From_Order = sav:ExcludeFromOrder
              Else!If SecurityCheck('EXCLUDE FROM ORDER')
      !            If thiswindow.request = Insertrecord
                      If epr:Exclude_From_Order = 'YES' And epr:part_ref_number <> ''
                          Case MessageEx('You have selected to ''Exclude From Order''.'&|
                            '<13,10>'&|
                            '<13,10>Warning! This stock part will NOT be decremented, and an order will NOT be raised if there are insufficient items in stock.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                         'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  sav:ExcludeFromOrder    = epr:Exclude_From_Order
                              Of 2 ! &No Button
                                  epr:Exclude_From_Order  = sav:ExcludeFromOrder
                          End!Case MessageEx
                      End!If epr:Exclude_From_Order = 'YES'
      !            End!If thiswindow.request = Insertrecord
              End!If SecurityCheck('EXCLUDE FROM ORDER')
          End!If sav:ExcludeFromOrder <> epr:Exclude_From_Order
      End!If ~0{prop:acceptall}
      Display()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?epr:Part_Number
      If ~0{prop:acceptall}
      error# = 0
      access:users.clearkey(use:user_code_key)
      use:user_code = job:engineer
      if access:users.tryfetch(use:user_code_key)
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              Access:LOCATION.ClearKey(loc:Location_Key)
              loc:Location = use:Location
              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Found
                  If loc:Active = 0
                      Error# = 3
                  End !If loc:Active = 0
              Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              If Error# = 0
                  access:stock.clearkey(sto:location_manufacturer_key)
                  sto:location     = use:location
                  sto:manufacturer = job:manufacturer
                  sto:part_number  = epr:part_number
                  if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                      stm:Ref_Number   = sto:Ref_Number
                      stm:Model_Number = job:Model_Number
                      If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Found
                          If sto:Suspend
                              error# = 2
                          Else !If sto:Suspended
                              error# = 0
                          End !If sto:Suspended
                      Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Error
                          error# = 1
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                  Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      error# = 1
                  End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
              End !If Error# = 0
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      Else!if access:users.tryfetch(use:user_code_key) = Level:Benign
          Access:LOCATION.ClearKey(loc:Location_Key)
          loc:Location = use:Location
          If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Error# = 3
              End !If loc:Active = 0
          Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
          If Error# = 0
              access:stock.clearkey(sto:location_manufacturer_key)
              sto:location     = use:location
              sto:manufacturer = job:manufacturer
              sto:part_number  = epr:part_number
              if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Found
                      If sto:Suspend
                          error# = 2
                      Else !If sto:Suspended
                          error# = 0
                      End !If sto:Suspended
                  Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Error
                      error# = 1
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
      
              Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  error# = 1
              End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
          End !If Error# = 0
      End!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case error#
          Of 0
              epr:part_number     = sto:part_number
              epr:description     = sto:description
              epr:supplier        = sto:supplier
              epr:purchase_cost   = sto:purchase_cost
              epr:sale_cost       = sto:sale_cost
              epr:part_ref_number = sto:ref_number
              If sto:assign_fault_codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      epr:fault_code1    = stm:Faultcode1
                      epr:fault_code2    = stm:Faultcode2
                      epr:fault_code3    = stm:Faultcode3
                      epr:fault_code4    = stm:Faultcode4
                      epr:fault_code5    = stm:Faultcode5
                      epr:fault_code6    = stm:Faultcode6
                      epr:fault_code7    = stm:Faultcode7
                      epr:fault_code8    = stm:Faultcode8
                      epr:fault_code9    = stm:Faultcode9
                      epr:fault_code10   = stm:Faultcode10
                      epr:fault_code11   = stm:Faultcode11
                      epr:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
              End!If sto:assign_fault_codes = 'YES'
              Select(?epr:quantity)
              display()
      
          Of 1
      
              Case MessageEx('Cannot find the selected Part!<13,10><13,10>It does either not exist in the engineer''s location, or is not in stock.  <13,10><13,10>Do you wish to select another part, or continue using the entered part number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Select Another Part|&Continue Using Part',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Select Another Part Button
                      Post(event:accepted,?browse_stock_button)
                  Of 2 ! &Continue Using Part Button
                      Select(?epr:description)
              End!Case MessageEx
          Of 2
              Case MessageEx('This part cannot be used. It has been suspended.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              epr:Part_Number = ''
              Select(?epr:Part_Number)
          Of 3
              Case MessageEx('Error!  Stock for this job will be picked from location ' & Clip(use:Location) & ' by default.'&|
                '<13,10>'&|
                '<13,10>This location is not active and cannot be used.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              epr:Part_Number = ''
              Select(?epr:Part_Number)
      End!If error# = 0
      Display()
      End!If ~0{prop:acceptall}
    OF ?browse_stock_button
      ThisWindow.Update
      Error# = 0
      user_code"  = ''
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              user_code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          
          
      Else!If job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              User_Code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          
      End!If job:engineer <> ''
      
      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
      loc:Active   = 1
      loc:Location = use:Location
      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
          !Found
      
      Else!lIf Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Case MessageEx('The selected user is not allocated to an Active Site Location.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          If use:StockFromLocationOnly
              Error# = 1
          End !If use:StockFromLocationOnly
      End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
      
      If Error# = 0
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_model_stock(job:model_number,user_code")
          if globalresponse = requestcompleted
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
      
              if access:stock.fetch(sto:ref_number_key)
                  beep(beep:systemhand)  ;  yield()
                  message('Error! Cannot access the Stock File.', |
                          'ServiceBase 2000', icon:hand)
              Else!if access:stock.fetch(sto:ref_number_key)
                  If sto:Suspend
                      Case MessageEx('This part cannot be used. It has been suspended.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      epr:Part_Number = ''
                      Select(?epr:Part_Number)
                  Else !If sto:Suspend
      
                      epr:part_number     = sto:part_number
                      epr:description     = sto:description
                      epr:supplier        = sto:supplier
                      epr:purchase_cost   = sto:purchase_cost
                      epr:sale_cost       = sto:sale_cost
                      epr:part_ref_number = sto:ref_number
                      If sto:assign_fault_codes = 'YES'
                          stm:Manufacturer = sto:Manufacturer
                          stm:Model_Number = job:Model_Number
                          If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                              epr:fault_code1    = stm:Faultcode1
                              epr:fault_code2    = stm:Faultcode2
                              epr:fault_code3    = stm:Faultcode3
                              epr:fault_code4    = stm:Faultcode4
                              epr:fault_code5    = stm:Faultcode5
                              epr:fault_code6    = stm:Faultcode6
                              epr:fault_code7    = stm:Faultcode7
                              epr:fault_code8    = stm:Faultcode8
                              epr:fault_code9    = stm:Faultcode9
                              epr:fault_code10   = stm:Faultcode10
                              epr:fault_code11   = stm:Faultcode11
                              epr:fault_code12   = stm:Faultcode12
      
                          End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      End!If sto:assign_fault_codes = 'YES'
                      Select(?epr:quantity)
                  End !If sto:Suspend
                  display()
              end!if access:stock.fetch(sto:ref_number_key)
          end
          globalrequest     = saverequest#
      End !Error# = 0
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ! Start Change 4033  BE(19/03/04)
  IF ((thiswindow.Request = InsertRecord) AND (epr:Part_Ref_Number <> '')) THEN
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number = epr:Part_Ref_Number
      IF (Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign) THEN
          !Need to check for duplicates?
          IF (sto:AllowDuplicate = 0) THEN
              dup# = 0
              Access:ESTPARTS_ALIAS.ClearKey(epr_ali:Part_Number_Key)
              epr_ali:Ref_Number  = job:Ref_Number
              epr_ali:Part_Number = epr:Part_Number
              SET(epr_ali:Part_Number_Key, epr_ali:Part_Number_Key)
              LOOP
                  IF ((Access:ESTPARTS_ALIAS.NEXT() <> Level:Benign) OR |
                      (epr_ali:Ref_Number  <> job:Ref_Number) OR |
                      (epr_ali:Part_Number <> epr:Part_Number)) THEN
                     BREAK
                  END
                  IF (epr_ali:Record_Number <> epr:record_number) THEN
                      BEEP(Beep:SystemHand)  ;  YIELD()
                      MESSAGE('This part is already attached to this job.', |
                                  'ServiceBase 2000', Icon:Hand, '&OK', 1, 0)
                      SELECT(?epr:part_number)
                      dup# = 1
                  END
              END
              IF (dup# = 1) THEN
                  CYCLE
              END
          END
      END
  END
  ! End Change 4033  BE(19/03/04)
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Estimate_Part')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Start Change 2343 BE(18/03/03) !Security Check
      Check_access('JOB - AMEND PART DETAIL', x")
      IF (x" = False) THEN
          ?epr:Part_Number{prop:readonly} = 1
          ?epr:description{prop:readonly} = 1
          ?epr:Part_Number{prop:color} = color:silver
          ?epr:description{prop:color} = color:silver
      End
      ! End Change 2343 BE(18/03/03)
      ! Start Change 2804 BE(19/06/03)
      IF (ThisWindow.Request = Insertrecord)
          epr:quantity = 1
          Display()
      END
      ! End Change 2804 BE(19/06/03)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

