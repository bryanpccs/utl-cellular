

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01043.INC'),ONCE        !Local module procedure declarations
                     END


SelectJOBS PROCEDURE                                  !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Batch_Number)
                       PROJECT(job:Internal_Status)
                       PROJECT(job:Auto_Search)
                       PROJECT(job:who_booked)
                       PROJECT(job:date_booked)
                       PROJECT(job:time_booked)
                       PROJECT(job:Cancelled)
                       PROJECT(job:Bouncer)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Engineer)
                       PROJECT(job:Completed)
                       PROJECT(job:Workshop)
                       PROJECT(job:Surname)
                       PROJECT(job:Mobile_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Current_Status)
                       PROJECT(job:Location)
                       PROJECT(job:Third_Party_Site)
                       PROJECT(job:Third_Party_Printed)
                       PROJECT(job:Job_Priority)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:EDI)
                       PROJECT(job:EDI_Batch_Number)
                       PROJECT(job:Invoice_Number)
                       PROJECT(job:Invoice_Number_Warranty)
                       PROJECT(job:Chargeable_Job)
                       PROJECT(job:Invoice_Exception)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Incoming_Consignment_Number)
                       PROJECT(job:Despatched)
                       PROJECT(job:Current_Courier)
                       PROJECT(job:Despatch_Number)
                       PROJECT(job:Courier)
                       PROJECT(job:Date_Despatched)
                       PROJECT(job:Loan_Courier)
                       PROJECT(job:Loan_Despatched)
                       PROJECT(job:Exchange_Courier)
                       PROJECT(job:Exchange_Despatched)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:Exchange_Status)
                       PROJECT(job:Loan_Status)
                       PROJECT(job:InvoiceAccount)
                       PROJECT(job:InvoiceBatch)
                       PROJECT(job:InvoiceStatus)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Batch_Number       LIKE(job:Batch_Number)         !List box control field - type derived from field
job:Internal_Status    LIKE(job:Internal_Status)      !List box control field - type derived from field
job:Auto_Search        LIKE(job:Auto_Search)          !List box control field - type derived from field
job:who_booked         LIKE(job:who_booked)           !List box control field - type derived from field
job:date_booked        LIKE(job:date_booked)          !List box control field - type derived from field
job:time_booked        LIKE(job:time_booked)          !List box control field - type derived from field
job:Cancelled          LIKE(job:Cancelled)            !List box control field - type derived from field
job:Bouncer            LIKE(job:Bouncer)              !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !Browse key field - type derived from field
job:Unit_Type          LIKE(job:Unit_Type)            !Browse key field - type derived from field
job:Engineer           LIKE(job:Engineer)             !Browse key field - type derived from field
job:Completed          LIKE(job:Completed)            !Browse key field - type derived from field
job:Workshop           LIKE(job:Workshop)             !Browse key field - type derived from field
job:Surname            LIKE(job:Surname)              !Browse key field - type derived from field
job:Mobile_Number      LIKE(job:Mobile_Number)        !Browse key field - type derived from field
job:ESN                LIKE(job:ESN)                  !Browse key field - type derived from field
job:MSN                LIKE(job:MSN)                  !Browse key field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !Browse key field - type derived from field
job:Order_Number       LIKE(job:Order_Number)         !Browse key field - type derived from field
job:Date_Completed     LIKE(job:Date_Completed)       !Browse key field - type derived from field
job:Current_Status     LIKE(job:Current_Status)       !Browse key field - type derived from field
job:Location           LIKE(job:Location)             !Browse key field - type derived from field
job:Third_Party_Site   LIKE(job:Third_Party_Site)     !Browse key field - type derived from field
job:Third_Party_Printed LIKE(job:Third_Party_Printed) !Browse key field - type derived from field
job:Job_Priority       LIKE(job:Job_Priority)         !Browse key field - type derived from field
job:Manufacturer       LIKE(job:Manufacturer)         !Browse key field - type derived from field
job:EDI                LIKE(job:EDI)                  !Browse key field - type derived from field
job:EDI_Batch_Number   LIKE(job:EDI_Batch_Number)     !Browse key field - type derived from field
job:Invoice_Number     LIKE(job:Invoice_Number)       !Browse key field - type derived from field
job:Invoice_Number_Warranty LIKE(job:Invoice_Number_Warranty) !Browse key field - type derived from field
job:Chargeable_Job     LIKE(job:Chargeable_Job)       !Browse key field - type derived from field
job:Invoice_Exception  LIKE(job:Invoice_Exception)    !Browse key field - type derived from field
job:Consignment_Number LIKE(job:Consignment_Number)   !Browse key field - type derived from field
job:Incoming_Consignment_Number LIKE(job:Incoming_Consignment_Number) !Browse key field - type derived from field
job:Despatched         LIKE(job:Despatched)           !Browse key field - type derived from field
job:Current_Courier    LIKE(job:Current_Courier)      !Browse key field - type derived from field
job:Despatch_Number    LIKE(job:Despatch_Number)      !Browse key field - type derived from field
job:Courier            LIKE(job:Courier)              !Browse key field - type derived from field
job:Date_Despatched    LIKE(job:Date_Despatched)      !Browse key field - type derived from field
job:Loan_Courier       LIKE(job:Loan_Courier)         !Browse key field - type derived from field
job:Loan_Despatched    LIKE(job:Loan_Despatched)      !Browse key field - type derived from field
job:Exchange_Courier   LIKE(job:Exchange_Courier)     !Browse key field - type derived from field
job:Exchange_Despatched LIKE(job:Exchange_Despatched) !Browse key field - type derived from field
job:Repair_Type        LIKE(job:Repair_Type)          !Browse key field - type derived from field
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !Browse key field - type derived from field
job:Charge_Type        LIKE(job:Charge_Type)          !Browse key field - type derived from field
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !Browse key field - type derived from field
job:Exchange_Status    LIKE(job:Exchange_Status)      !Browse key field - type derived from field
job:Loan_Status        LIKE(job:Loan_Status)          !Browse key field - type derived from field
job:InvoiceAccount     LIKE(job:InvoiceAccount)       !Browse key field - type derived from field
job:InvoiceBatch       LIKE(job:InvoiceBatch)         !Browse key field - type derived from field
job:InvoiceStatus      LIKE(job:InvoiceStatus)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Select a JOBS Record'),AT(,,358,288),FONT('MS Sans Serif',8,,),IMM,HLP('SelectJOBS'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,120,342,124),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('44L(2)|M~Job Number~L(2)@s8@52L(2)|M~Batch Number~L(2)@s8@64L(2)|M~Internal Stat' &|
   'us~L(2)@s10@80L(2)|M~Auto Search~L(2)@s30@44L(2)|M~who booked~L(2)@s3@80R(2)|M~d' &|
   'ate booked~C(0)@d6b@80R(2)|M~time booked~C(0)@t1@40L(2)|M~Cancelled~L(2)@s3@36L(' &|
   '2)|M~Bouncer~L(2)@s8@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(305,248,45,14),USE(?Select:2)
                       SHEET,AT(4,4,350,262),USE(?CurrentTab)
                         TAB('By Job Number'),USE(?Tab:2)
                         END
                         TAB('By Job Number'),USE(?Tab:3)
                         END
                         TAB('By Job Number'),USE(?Tab:4)
                         END
                         TAB('By Job Number'),USE(?Tab:5)
                         END
                         TAB('By Surname'),USE(?Tab:6)
                         END
                         TAB('By Mobile Number'),USE(?Tab:7)
                         END
                         TAB('By E.S.N. / I.M.E.I.'),USE(?Tab:8)
                         END
                         TAB('By M.S.N.'),USE(?Tab:9)
                         END
                         TAB('By Account Number'),USE(?Tab:10)
                         END
                         TAB('By Order Number'),USE(?Tab:11)
                         END
                         TAB('By Model Number'),USE(?Tab:12)
                         END
                         TAB('By Engineer'),USE(?Tab:13)
                         END
                         TAB('By Date Booked'),USE(?Tab:14)
                         END
                         TAB('By Date Completed'),USE(?Tab:15)
                         END
                         TAB('By Completed Date'),USE(?Tab:16)
                         END
                         TAB('By Job Number'),USE(?Tab:17)
                         END
                         TAB('By Job Number'),USE(?Tab:18)
                         END
                         TAB('By Location'),USE(?Tab:19)
                         END
                         TAB('By Third Party'),USE(?Tab:20)
                         END
                         TAB('By ESN'),USE(?Tab:21)
                         END
                         TAB('By MSN'),USE(?Tab:22)
                         END
                         TAB('By Priority'),USE(?Tab:23)
                         END
                         TAB('By Unit Type'),USE(?Tab:24)
                         END
                         TAB('By Job Number'),USE(?Tab:25)
                         END
                         TAB('By Invoice_Number'),USE(?Tab:26)
                         END
                         TAB('By Invoice Number'),USE(?Tab:27)
                         END
                         TAB('By Job Number'),USE(?Tab:28)
                         END
                         TAB('By Ref Number'),USE(?Tab:29)
                         END
                         TAB('By Job Number'),USE(?Tab:30)
                         END
                         TAB('By Ref Number'),USE(?Tab:31)
                         END
                         TAB('By Job Number'),USE(?Tab:32)
                         END
                         TAB('job:ChaInvoiceKey'),USE(?Tab:33)
                         END
                         TAB('By Job Number'),USE(?Tab:34)
                         END
                         TAB('By Cosignment Number'),USE(?Tab:35)
                         END
                         TAB('By Consignment Number'),USE(?Tab:36)
                         END
                         TAB('By Job Number'),USE(?Tab:37)
                         END
                         TAB('By Job Number'),USE(?Tab:38)
                         END
                         TAB('By Job Number'),USE(?Tab:39)
                         END
                         TAB('By Job Number'),USE(?Tab:40)
                         END
                         TAB('By Job Number'),USE(?Tab:41)
                         END
                         TAB('By Job Number'),USE(?Tab:42)
                         END
                         TAB('By Job Number'),USE(?Tab:43)
                         END
                         TAB('By Job Number'),USE(?Tab:44)
                         END
                         TAB('By Chargeable Repair Type'),USE(?Tab:45)
                         END
                         TAB('By Warranty Repair Type'),USE(?Tab:46)
                         END
                         TAB('job:ChaTypeKey'),USE(?Tab:47)
                         END
                         TAB('job:WarChaTypeKey'),USE(?Tab:48)
                         END
                         TAB('By Job Number'),USE(?Tab:49)
                         END
                         TAB('By Date Completed'),USE(?Tab:50)
                         END
                         TAB('By Job Number'),USE(?Tab:51)
                         END
                         TAB('By Job Number'),USE(?Tab:52)
                         END
                         TAB('By Job Number'),USE(?Tab:53)
                         END
                         TAB('By Job Number'),USE(?Tab:54)
                         END
                         TAB('By Job Number'),USE(?Tab:55)
                         END
                         TAB('By Job Number'),USE(?Tab:56)
                         END
                       END
                       BUTTON('Close'),AT(260,270,45,14),USE(?Close)
                       BUTTON('Help'),AT(309,270,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 4
BRW1::Sort4:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 5
BRW1::Sort5:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 6
BRW1::Sort6:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 7
BRW1::Sort7:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 8
BRW1::Sort8:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 9
BRW1::Sort9:Locator  StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 10
BRW1::Sort10:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 11
BRW1::Sort11:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 12
BRW1::Sort12:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 13
BRW1::Sort13:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 14
BRW1::Sort14:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 15
BRW1::Sort15:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 16
BRW1::Sort16:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 17
BRW1::Sort17:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 18
BRW1::Sort18:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 19
BRW1::Sort19:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 20
BRW1::Sort20:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 21
BRW1::Sort21:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 22
BRW1::Sort22:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 23
BRW1::Sort23:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 24
BRW1::Sort24:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 25
BRW1::Sort25:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 26
BRW1::Sort26:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 27
BRW1::Sort27:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 28
BRW1::Sort28:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 29
BRW1::Sort29:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 30
BRW1::Sort30:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 31
BRW1::Sort31:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 32
BRW1::Sort32:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 33
BRW1::Sort33:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 34
BRW1::Sort34:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 35
BRW1::Sort35:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 36
BRW1::Sort36:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 37
BRW1::Sort37:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 38
BRW1::Sort38:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 39
BRW1::Sort39:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 40
BRW1::Sort40:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 41
BRW1::Sort41:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 42
BRW1::Sort42:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 43
BRW1::Sort43:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 44
BRW1::Sort44:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 45
BRW1::Sort45:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 46
BRW1::Sort46:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 47
BRW1::Sort47:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 48
BRW1::Sort48:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 49
BRW1::Sort49:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 50
BRW1::Sort50:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 51
BRW1::Sort51:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 52
BRW1::Sort52:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 53
BRW1::Sort53:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 54
BRW1::Sort54:Locator StepLocatorClass                 !Conditional Locator - CHOICE(?CurrentTab) = 55
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort3:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 4
BRW1::Sort4:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 5
BRW1::Sort5:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 6
BRW1::Sort6:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 7
BRW1::Sort7:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 8
BRW1::Sort8:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 9
BRW1::Sort9:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 10
BRW1::Sort10:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 11
BRW1::Sort11:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 12
BRW1::Sort12:StepClass StepRealClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 13
BRW1::Sort13:StepClass StepRealClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 14
BRW1::Sort14:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 15
BRW1::Sort15:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 16
BRW1::Sort16:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 17
BRW1::Sort17:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 18
BRW1::Sort18:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 19
BRW1::Sort19:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 20
BRW1::Sort20:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 21
BRW1::Sort21:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 22
BRW1::Sort22:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 23
BRW1::Sort23:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 24
BRW1::Sort24:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 25
BRW1::Sort25:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 26
BRW1::Sort26:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 27
BRW1::Sort27:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 28
BRW1::Sort28:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 29
BRW1::Sort29:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 30
BRW1::Sort30:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 31
BRW1::Sort31:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 32
BRW1::Sort32:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 33
BRW1::Sort33:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 34
BRW1::Sort34:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 35
BRW1::Sort35:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 36
BRW1::Sort36:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 37
BRW1::Sort37:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 38
BRW1::Sort38:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 39
BRW1::Sort39:StepClass StepLongClass                  !Conditional Step Manager - CHOICE(?CurrentTab) = 40
BRW1::Sort40:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 41
BRW1::Sort41:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 42
BRW1::Sort42:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 43
BRW1::Sort43:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 44
BRW1::Sort44:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 45
BRW1::Sort45:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 46
BRW1::Sort46:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 47
BRW1::Sort47:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 48
BRW1::Sort48:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 49
BRW1::Sort49:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 50
BRW1::Sort50:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 51
BRW1::Sort51:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 52
BRW1::Sort52:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 53
BRW1::Sort53:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 54
BRW1::Sort54:StepClass StepStringClass                !Conditional Step Manager - CHOICE(?CurrentTab) = 55
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    ?Tab:5{prop:Color} = 15066597
    ?Tab:6{prop:Color} = 15066597
    ?Tab:7{prop:Color} = 15066597
    ?Tab:8{prop:Color} = 15066597
    ?Tab:9{prop:Color} = 15066597
    ?Tab:10{prop:Color} = 15066597
    ?Tab:11{prop:Color} = 15066597
    ?Tab:12{prop:Color} = 15066597
    ?Tab:13{prop:Color} = 15066597
    ?Tab:14{prop:Color} = 15066597
    ?Tab:15{prop:Color} = 15066597
    ?Tab:16{prop:Color} = 15066597
    ?Tab:17{prop:Color} = 15066597
    ?Tab:18{prop:Color} = 15066597
    ?Tab:19{prop:Color} = 15066597
    ?Tab:20{prop:Color} = 15066597
    ?Tab:21{prop:Color} = 15066597
    ?Tab:22{prop:Color} = 15066597
    ?Tab:23{prop:Color} = 15066597
    ?Tab:24{prop:Color} = 15066597
    ?Tab:25{prop:Color} = 15066597
    ?Tab:26{prop:Color} = 15066597
    ?Tab:27{prop:Color} = 15066597
    ?Tab:28{prop:Color} = 15066597
    ?Tab:29{prop:Color} = 15066597
    ?Tab:30{prop:Color} = 15066597
    ?Tab:31{prop:Color} = 15066597
    ?Tab:32{prop:Color} = 15066597
    ?Tab:33{prop:Color} = 15066597
    ?Tab:34{prop:Color} = 15066597
    ?Tab:35{prop:Color} = 15066597
    ?Tab:36{prop:Color} = 15066597
    ?Tab:37{prop:Color} = 15066597
    ?Tab:38{prop:Color} = 15066597
    ?Tab:39{prop:Color} = 15066597
    ?Tab:40{prop:Color} = 15066597
    ?Tab:41{prop:Color} = 15066597
    ?Tab:42{prop:Color} = 15066597
    ?Tab:43{prop:Color} = 15066597
    ?Tab:44{prop:Color} = 15066597
    ?Tab:45{prop:Color} = 15066597
    ?Tab:46{prop:Color} = 15066597
    ?Tab:47{prop:Color} = 15066597
    ?Tab:48{prop:Color} = 15066597
    ?Tab:49{prop:Color} = 15066597
    ?Tab:50{prop:Color} = 15066597
    ?Tab:51{prop:Color} = 15066597
    ?Tab:52{prop:Color} = 15066597
    ?Tab:53{prop:Color} = 15066597
    ?Tab:54{prop:Color} = 15066597
    ?Tab:55{prop:Color} = 15066597
    ?Tab:56{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SelectJOBS',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'SelectJOBS',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:5;  SolaceCtrlName = '?Tab:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:6;  SolaceCtrlName = '?Tab:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:7;  SolaceCtrlName = '?Tab:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:8;  SolaceCtrlName = '?Tab:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:9;  SolaceCtrlName = '?Tab:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:10;  SolaceCtrlName = '?Tab:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:11;  SolaceCtrlName = '?Tab:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:12;  SolaceCtrlName = '?Tab:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:13;  SolaceCtrlName = '?Tab:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:14;  SolaceCtrlName = '?Tab:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:15;  SolaceCtrlName = '?Tab:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:16;  SolaceCtrlName = '?Tab:16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:17;  SolaceCtrlName = '?Tab:17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:18;  SolaceCtrlName = '?Tab:18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:19;  SolaceCtrlName = '?Tab:19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:20;  SolaceCtrlName = '?Tab:20';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:21;  SolaceCtrlName = '?Tab:21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:22;  SolaceCtrlName = '?Tab:22';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:23;  SolaceCtrlName = '?Tab:23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:24;  SolaceCtrlName = '?Tab:24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:25;  SolaceCtrlName = '?Tab:25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:26;  SolaceCtrlName = '?Tab:26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:27;  SolaceCtrlName = '?Tab:27';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:28;  SolaceCtrlName = '?Tab:28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:29;  SolaceCtrlName = '?Tab:29';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:30;  SolaceCtrlName = '?Tab:30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:31;  SolaceCtrlName = '?Tab:31';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:32;  SolaceCtrlName = '?Tab:32';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:33;  SolaceCtrlName = '?Tab:33';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:34;  SolaceCtrlName = '?Tab:34';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:35;  SolaceCtrlName = '?Tab:35';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:36;  SolaceCtrlName = '?Tab:36';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:37;  SolaceCtrlName = '?Tab:37';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:38;  SolaceCtrlName = '?Tab:38';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:39;  SolaceCtrlName = '?Tab:39';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:40;  SolaceCtrlName = '?Tab:40';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:41;  SolaceCtrlName = '?Tab:41';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:42;  SolaceCtrlName = '?Tab:42';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:43;  SolaceCtrlName = '?Tab:43';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:44;  SolaceCtrlName = '?Tab:44';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:45;  SolaceCtrlName = '?Tab:45';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:46;  SolaceCtrlName = '?Tab:46';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:47;  SolaceCtrlName = '?Tab:47';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:48;  SolaceCtrlName = '?Tab:48';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:49;  SolaceCtrlName = '?Tab:49';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:50;  SolaceCtrlName = '?Tab:50';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:51;  SolaceCtrlName = '?Tab:51';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:52;  SolaceCtrlName = '?Tab:52';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:53;  SolaceCtrlName = '?Tab:53';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:54;  SolaceCtrlName = '?Tab:54';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:55;  SolaceCtrlName = '?Tab:55';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:56;  SolaceCtrlName = '?Tab:56';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectJOBS')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'SelectJOBS')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='SelectJOBS'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,job:Model_Unit_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,job:Model_Number,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,job:EngCompKey)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,job:Engineer,1,BRW1)
  BRW1::Sort3:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort3:StepClass,job:EngWorkKey)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(,job:Engineer,1,BRW1)
  BRW1::Sort4:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort4:StepClass,job:Surname_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,job:Surname,1,BRW1)
  BRW1::Sort5:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort5:StepClass,job:MobileNumberKey)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(,job:Mobile_Number,1,BRW1)
  BRW1::Sort6:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort6:StepClass,job:ESN_Key)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(,job:ESN,1,BRW1)
  BRW1::Sort7:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort7:StepClass,job:MSN_Key)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(,job:MSN,1,BRW1)
  BRW1::Sort8:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort8:StepClass,job:AccountNumberKey)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(,job:Account_Number,1,BRW1)
  BRW1::Sort9:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort9:StepClass,job:AccOrdNoKey)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(,job:Account_Number,1,BRW1)
  BRW1::Sort10:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort10:StepClass,job:Model_Number_Key)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(,job:Model_Number,1,BRW1)
  BRW1::Sort11:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort11:StepClass,job:Engineer_Key)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(,job:Engineer,1,BRW1)
  BRW1::Sort12:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort12:StepClass,job:Date_Booked_Key)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(,job:date_booked,1,BRW1)
  BRW1::Sort13:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort13:StepClass,job:DateCompletedKey)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(,job:Date_Completed,1,BRW1)
  BRW1::Sort14:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort14:StepClass,job:ModelCompKey)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(,job:Model_Number,1,BRW1)
  BRW1::Sort15:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort15:StepClass,job:By_Status)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(,job:Current_Status,1,BRW1)
  BRW1::Sort16:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort16:StepClass,job:StatusLocKey)
  BRW1.AddLocator(BRW1::Sort16:Locator)
  BRW1::Sort16:Locator.Init(,job:Current_Status,1,BRW1)
  BRW1::Sort17:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort17:StepClass,job:Location_Key)
  BRW1.AddLocator(BRW1::Sort17:Locator)
  BRW1::Sort17:Locator.Init(,job:Location,1,BRW1)
  BRW1::Sort18:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort18:StepClass,job:Third_Party_Key)
  BRW1.AddLocator(BRW1::Sort18:Locator)
  BRW1::Sort18:Locator.Init(,job:Third_Party_Site,1,BRW1)
  BRW1::Sort19:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort19:StepClass,job:ThirdEsnKey)
  BRW1.AddLocator(BRW1::Sort19:Locator)
  BRW1::Sort19:Locator.Init(,job:Third_Party_Site,1,BRW1)
  BRW1::Sort20:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort20:StepClass,job:ThirdMsnKey)
  BRW1.AddLocator(BRW1::Sort20:Locator)
  BRW1::Sort20:Locator.Init(,job:Third_Party_Site,1,BRW1)
  BRW1::Sort21:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort21:StepClass,job:PriorityTypeKey)
  BRW1.AddLocator(BRW1::Sort21:Locator)
  BRW1::Sort21:Locator.Init(,job:Job_Priority,1,BRW1)
  BRW1::Sort22:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort22:StepClass,job:Unit_Type_Key)
  BRW1.AddLocator(BRW1::Sort22:Locator)
  BRW1::Sort22:Locator.Init(,job:Unit_Type,1,BRW1)
  BRW1::Sort23:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort23:StepClass,job:EDI_Key)
  BRW1.AddLocator(BRW1::Sort23:Locator)
  BRW1::Sort23:Locator.Init(,job:Manufacturer,1,BRW1)
  BRW1::Sort24:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort24:StepClass,job:InvoiceNumberKey)
  BRW1.AddLocator(BRW1::Sort24:Locator)
  BRW1::Sort24:Locator.Init(,job:Invoice_Number,1,BRW1)
  BRW1::Sort25:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort25:StepClass,job:WarInvoiceNoKey)
  BRW1.AddLocator(BRW1::Sort25:Locator)
  BRW1::Sort25:Locator.Init(,job:Invoice_Number_Warranty,1,BRW1)
  BRW1::Sort26:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort26:StepClass,job:Batch_Number_Key)
  BRW1.AddLocator(BRW1::Sort26:Locator)
  BRW1::Sort26:Locator.Init(,job:Batch_Number,1,BRW1)
  BRW1::Sort27:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort27:StepClass,job:Batch_Status_Key)
  BRW1.AddLocator(BRW1::Sort27:Locator)
  BRW1::Sort27:Locator.Init(,job:Batch_Number,1,BRW1)
  BRW1::Sort28:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort28:StepClass,job:BatchModelNoKey)
  BRW1.AddLocator(BRW1::Sort28:Locator)
  BRW1::Sort28:Locator.Init(,job:Batch_Number,1,BRW1)
  BRW1::Sort29:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort29:StepClass,job:BatchInvoicedKey)
  BRW1.AddLocator(BRW1::Sort29:Locator)
  BRW1::Sort29:Locator.Init(,job:Batch_Number,1,BRW1)
  BRW1::Sort30:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort30:StepClass,job:BatchCompKey)
  BRW1.AddLocator(BRW1::Sort30:Locator)
  BRW1::Sort30:Locator.Init(,job:Batch_Number,1,BRW1)
  BRW1::Sort31:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort31:StepClass,job:ChaInvoiceKey)
  BRW1.AddLocator(BRW1::Sort31:Locator)
  BRW1::Sort31:Locator.Init(,job:Chargeable_Job,1,BRW1)
  BRW1::Sort32:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort32:StepClass,job:InvoiceExceptKey)
  BRW1.AddLocator(BRW1::Sort32:Locator)
  BRW1::Sort32:Locator.Init(,job:Invoice_Exception,1,BRW1)
  BRW1::Sort33:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort33:StepClass,job:ConsignmentNoKey)
  BRW1.AddLocator(BRW1::Sort33:Locator)
  BRW1::Sort33:Locator.Init(,job:Consignment_Number,1,BRW1)
  BRW1::Sort34:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort34:StepClass,job:InConsignKey)
  BRW1.AddLocator(BRW1::Sort34:Locator)
  BRW1::Sort34:Locator.Init(,job:Incoming_Consignment_Number,1,BRW1)
  BRW1::Sort35:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort35:StepClass,job:ReadyToDespKey)
  BRW1.AddLocator(BRW1::Sort35:Locator)
  BRW1::Sort35:Locator.Init(,job:Despatched,1,BRW1)
  BRW1::Sort36:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort36:StepClass,job:ReadyToTradeKey)
  BRW1.AddLocator(BRW1::Sort36:Locator)
  BRW1::Sort36:Locator.Init(,job:Despatched,1,BRW1)
  BRW1::Sort37:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort37:StepClass,job:ReadyToCouKey)
  BRW1.AddLocator(BRW1::Sort37:Locator)
  BRW1::Sort37:Locator.Init(,job:Despatched,1,BRW1)
  BRW1::Sort38:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort38:StepClass,job:ReadyToAllKey)
  BRW1.AddLocator(BRW1::Sort38:Locator)
  BRW1::Sort38:Locator.Init(,job:Despatched,1,BRW1)
  BRW1::Sort39:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort39:StepClass,job:DespJobNumberKey)
  BRW1.AddLocator(BRW1::Sort39:Locator)
  BRW1::Sort39:Locator.Init(,job:Despatch_Number,1,BRW1)
  BRW1::Sort40:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort40:StepClass,job:DateDespatchKey)
  BRW1.AddLocator(BRW1::Sort40:Locator)
  BRW1::Sort40:Locator.Init(,job:Courier,1,BRW1)
  BRW1::Sort41:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort41:StepClass,job:DateDespLoaKey)
  BRW1.AddLocator(BRW1::Sort41:Locator)
  BRW1::Sort41:Locator.Init(,job:Loan_Courier,1,BRW1)
  BRW1::Sort42:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort42:StepClass,job:DateDespExcKey)
  BRW1.AddLocator(BRW1::Sort42:Locator)
  BRW1::Sort42:Locator.Init(,job:Exchange_Courier,1,BRW1)
  BRW1::Sort43:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort43:StepClass,job:ChaRepTypeKey)
  BRW1.AddLocator(BRW1::Sort43:Locator)
  BRW1::Sort43:Locator.Init(,job:Repair_Type,1,BRW1)
  BRW1::Sort44:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort44:StepClass,job:WarRepTypeKey)
  BRW1.AddLocator(BRW1::Sort44:Locator)
  BRW1::Sort44:Locator.Init(,job:Repair_Type_Warranty,1,BRW1)
  BRW1::Sort45:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort45:StepClass,job:ChaTypeKey)
  BRW1.AddLocator(BRW1::Sort45:Locator)
  BRW1::Sort45:Locator.Init(,job:Charge_Type,1,BRW1)
  BRW1::Sort46:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort46:StepClass,job:WarChaTypeKey)
  BRW1.AddLocator(BRW1::Sort46:Locator)
  BRW1::Sort46:Locator.Init(,job:Warranty_Charge_Type,1,BRW1)
  BRW1::Sort47:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort47:StepClass,job:Bouncer_Key)
  BRW1.AddLocator(BRW1::Sort47:Locator)
  BRW1::Sort47:Locator.Init(,job:Bouncer,1,BRW1)
  BRW1::Sort48:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort48:StepClass,job:EngDateCompKey)
  BRW1.AddLocator(BRW1::Sort48:Locator)
  BRW1::Sort48:Locator.Init(,job:Engineer,1,BRW1)
  BRW1::Sort49:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort49:StepClass,job:ExcStatusKey)
  BRW1.AddLocator(BRW1::Sort49:Locator)
  BRW1::Sort49:Locator.Init(,job:Exchange_Status,1,BRW1)
  BRW1::Sort50:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort50:StepClass,job:LoanStatusKey)
  BRW1.AddLocator(BRW1::Sort50:Locator)
  BRW1::Sort50:Locator.Init(,job:Loan_Status,1,BRW1)
  BRW1::Sort51:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort51:StepClass,job:ExchangeLocKey)
  BRW1.AddLocator(BRW1::Sort51:Locator)
  BRW1::Sort51:Locator.Init(,job:Exchange_Status,1,BRW1)
  BRW1::Sort52:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort52:StepClass,job:LoanLocKey)
  BRW1.AddLocator(BRW1::Sort52:Locator)
  BRW1::Sort52:Locator.Init(,job:Loan_Status,1,BRW1)
  BRW1::Sort53:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort53:StepClass,job:BatchJobKey)
  BRW1.AddLocator(BRW1::Sort53:Locator)
  BRW1::Sort53:Locator.Init(,job:InvoiceAccount,1,BRW1)
  BRW1::Sort54:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort54:StepClass,job:BatchStatusKey)
  BRW1.AddLocator(BRW1::Sort54:Locator)
  BRW1::Sort54:Locator.Init(,job:InvoiceAccount,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,job:Ref_Number,1,BRW1)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Batch_Number,BRW1.Q.job:Batch_Number)
  BRW1.AddField(job:Internal_Status,BRW1.Q.job:Internal_Status)
  BRW1.AddField(job:Auto_Search,BRW1.Q.job:Auto_Search)
  BRW1.AddField(job:who_booked,BRW1.Q.job:who_booked)
  BRW1.AddField(job:date_booked,BRW1.Q.job:date_booked)
  BRW1.AddField(job:time_booked,BRW1.Q.job:time_booked)
  BRW1.AddField(job:Cancelled,BRW1.Q.job:Cancelled)
  BRW1.AddField(job:Bouncer,BRW1.Q.job:Bouncer)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:Unit_Type,BRW1.Q.job:Unit_Type)
  BRW1.AddField(job:Engineer,BRW1.Q.job:Engineer)
  BRW1.AddField(job:Completed,BRW1.Q.job:Completed)
  BRW1.AddField(job:Workshop,BRW1.Q.job:Workshop)
  BRW1.AddField(job:Surname,BRW1.Q.job:Surname)
  BRW1.AddField(job:Mobile_Number,BRW1.Q.job:Mobile_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(job:MSN,BRW1.Q.job:MSN)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(job:Date_Completed,BRW1.Q.job:Date_Completed)
  BRW1.AddField(job:Current_Status,BRW1.Q.job:Current_Status)
  BRW1.AddField(job:Location,BRW1.Q.job:Location)
  BRW1.AddField(job:Third_Party_Site,BRW1.Q.job:Third_Party_Site)
  BRW1.AddField(job:Third_Party_Printed,BRW1.Q.job:Third_Party_Printed)
  BRW1.AddField(job:Job_Priority,BRW1.Q.job:Job_Priority)
  BRW1.AddField(job:Manufacturer,BRW1.Q.job:Manufacturer)
  BRW1.AddField(job:EDI,BRW1.Q.job:EDI)
  BRW1.AddField(job:EDI_Batch_Number,BRW1.Q.job:EDI_Batch_Number)
  BRW1.AddField(job:Invoice_Number,BRW1.Q.job:Invoice_Number)
  BRW1.AddField(job:Invoice_Number_Warranty,BRW1.Q.job:Invoice_Number_Warranty)
  BRW1.AddField(job:Chargeable_Job,BRW1.Q.job:Chargeable_Job)
  BRW1.AddField(job:Invoice_Exception,BRW1.Q.job:Invoice_Exception)
  BRW1.AddField(job:Consignment_Number,BRW1.Q.job:Consignment_Number)
  BRW1.AddField(job:Incoming_Consignment_Number,BRW1.Q.job:Incoming_Consignment_Number)
  BRW1.AddField(job:Despatched,BRW1.Q.job:Despatched)
  BRW1.AddField(job:Current_Courier,BRW1.Q.job:Current_Courier)
  BRW1.AddField(job:Despatch_Number,BRW1.Q.job:Despatch_Number)
  BRW1.AddField(job:Courier,BRW1.Q.job:Courier)
  BRW1.AddField(job:Date_Despatched,BRW1.Q.job:Date_Despatched)
  BRW1.AddField(job:Loan_Courier,BRW1.Q.job:Loan_Courier)
  BRW1.AddField(job:Loan_Despatched,BRW1.Q.job:Loan_Despatched)
  BRW1.AddField(job:Exchange_Courier,BRW1.Q.job:Exchange_Courier)
  BRW1.AddField(job:Exchange_Despatched,BRW1.Q.job:Exchange_Despatched)
  BRW1.AddField(job:Repair_Type,BRW1.Q.job:Repair_Type)
  BRW1.AddField(job:Repair_Type_Warranty,BRW1.Q.job:Repair_Type_Warranty)
  BRW1.AddField(job:Charge_Type,BRW1.Q.job:Charge_Type)
  BRW1.AddField(job:Warranty_Charge_Type,BRW1.Q.job:Warranty_Charge_Type)
  BRW1.AddField(job:Exchange_Status,BRW1.Q.job:Exchange_Status)
  BRW1.AddField(job:Loan_Status,BRW1.Q.job:Loan_Status)
  BRW1.AddField(job:InvoiceAccount,BRW1.Q.job:InvoiceAccount)
  BRW1.AddField(job:InvoiceBatch,BRW1.Q.job:InvoiceBatch)
  BRW1.AddField(job:InvoiceStatus,BRW1.Q.job:InvoiceStatus)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='SelectJOBS'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'SelectJOBS',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'SelectJOBS')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 6
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 7
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 8
    RETURN SELF.SetSort(7,Force)
  ELSIF CHOICE(?CurrentTab) = 9
    RETURN SELF.SetSort(8,Force)
  ELSIF CHOICE(?CurrentTab) = 10
    RETURN SELF.SetSort(9,Force)
  ELSIF CHOICE(?CurrentTab) = 11
    RETURN SELF.SetSort(10,Force)
  ELSIF CHOICE(?CurrentTab) = 12
    RETURN SELF.SetSort(11,Force)
  ELSIF CHOICE(?CurrentTab) = 13
    RETURN SELF.SetSort(12,Force)
  ELSIF CHOICE(?CurrentTab) = 14
    RETURN SELF.SetSort(13,Force)
  ELSIF CHOICE(?CurrentTab) = 15
    RETURN SELF.SetSort(14,Force)
  ELSIF CHOICE(?CurrentTab) = 16
    RETURN SELF.SetSort(15,Force)
  ELSIF CHOICE(?CurrentTab) = 17
    RETURN SELF.SetSort(16,Force)
  ELSIF CHOICE(?CurrentTab) = 18
    RETURN SELF.SetSort(17,Force)
  ELSIF CHOICE(?CurrentTab) = 19
    RETURN SELF.SetSort(18,Force)
  ELSIF CHOICE(?CurrentTab) = 20
    RETURN SELF.SetSort(19,Force)
  ELSIF CHOICE(?CurrentTab) = 21
    RETURN SELF.SetSort(20,Force)
  ELSIF CHOICE(?CurrentTab) = 22
    RETURN SELF.SetSort(21,Force)
  ELSIF CHOICE(?CurrentTab) = 23
    RETURN SELF.SetSort(22,Force)
  ELSIF CHOICE(?CurrentTab) = 24
    RETURN SELF.SetSort(23,Force)
  ELSIF CHOICE(?CurrentTab) = 25
    RETURN SELF.SetSort(24,Force)
  ELSIF CHOICE(?CurrentTab) = 26
    RETURN SELF.SetSort(25,Force)
  ELSIF CHOICE(?CurrentTab) = 27
    RETURN SELF.SetSort(26,Force)
  ELSIF CHOICE(?CurrentTab) = 28
    RETURN SELF.SetSort(27,Force)
  ELSIF CHOICE(?CurrentTab) = 29
    RETURN SELF.SetSort(28,Force)
  ELSIF CHOICE(?CurrentTab) = 30
    RETURN SELF.SetSort(29,Force)
  ELSIF CHOICE(?CurrentTab) = 31
    RETURN SELF.SetSort(30,Force)
  ELSIF CHOICE(?CurrentTab) = 32
    RETURN SELF.SetSort(31,Force)
  ELSIF CHOICE(?CurrentTab) = 33
    RETURN SELF.SetSort(32,Force)
  ELSIF CHOICE(?CurrentTab) = 34
    RETURN SELF.SetSort(33,Force)
  ELSIF CHOICE(?CurrentTab) = 35
    RETURN SELF.SetSort(34,Force)
  ELSIF CHOICE(?CurrentTab) = 36
    RETURN SELF.SetSort(35,Force)
  ELSIF CHOICE(?CurrentTab) = 37
    RETURN SELF.SetSort(36,Force)
  ELSIF CHOICE(?CurrentTab) = 38
    RETURN SELF.SetSort(37,Force)
  ELSIF CHOICE(?CurrentTab) = 39
    RETURN SELF.SetSort(38,Force)
  ELSIF CHOICE(?CurrentTab) = 40
    RETURN SELF.SetSort(39,Force)
  ELSIF CHOICE(?CurrentTab) = 41
    RETURN SELF.SetSort(40,Force)
  ELSIF CHOICE(?CurrentTab) = 42
    RETURN SELF.SetSort(41,Force)
  ELSIF CHOICE(?CurrentTab) = 43
    RETURN SELF.SetSort(42,Force)
  ELSIF CHOICE(?CurrentTab) = 44
    RETURN SELF.SetSort(43,Force)
  ELSIF CHOICE(?CurrentTab) = 45
    RETURN SELF.SetSort(44,Force)
  ELSIF CHOICE(?CurrentTab) = 46
    RETURN SELF.SetSort(45,Force)
  ELSIF CHOICE(?CurrentTab) = 47
    RETURN SELF.SetSort(46,Force)
  ELSIF CHOICE(?CurrentTab) = 48
    RETURN SELF.SetSort(47,Force)
  ELSIF CHOICE(?CurrentTab) = 49
    RETURN SELF.SetSort(48,Force)
  ELSIF CHOICE(?CurrentTab) = 50
    RETURN SELF.SetSort(49,Force)
  ELSIF CHOICE(?CurrentTab) = 51
    RETURN SELF.SetSort(50,Force)
  ELSIF CHOICE(?CurrentTab) = 52
    RETURN SELF.SetSort(51,Force)
  ELSIF CHOICE(?CurrentTab) = 53
    RETURN SELF.SetSort(52,Force)
  ELSIF CHOICE(?CurrentTab) = 54
    RETURN SELF.SetSort(53,Force)
  ELSIF CHOICE(?CurrentTab) = 55
    RETURN SELF.SetSort(54,Force)
  ELSE
    RETURN SELF.SetSort(55,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

