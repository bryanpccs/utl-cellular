

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01020.INC'),ONCE        !Local module procedure declarations
                     END


Estimate_Reason PROCEDURE                             !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Estimate Process'),AT(,,232,98),FONT('Tahoma',8,,FONT:regular),CENTER,TILED,GRAY,DOUBLE
                       SHEET,AT(4,4,224,64),USE(?Sheet1),SPREAD
                         TAB('Estimate Accepted'),USE(?Estimate_Accepted),HIDE
                           PROMPT('Estimate Accepted From'),AT(8,24,88,12),USE(?glo:select2:Prompt)
                           ENTRY(@s40),AT(96,24,124,10),USE(GLO:Select2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Communication Method'),AT(8,48),USE(?glo:select3:Prompt)
                           ENTRY(@s40),AT(96,48,124,10),USE(GLO:Select3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('Estimate Rejected'),USE(?Estimate_Rejected),HIDE
                           ENTRY(@s40),AT(96,20,124,10),USE(GLO:Select2,,?glo:select2:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Communication Method'),AT(8,36),USE(?Prompt4)
                           PROMPT('Estimate Rejected By'),AT(7,20),USE(?Prompt3)
                           ENTRY(@s40),AT(96,36,124,10),USE(GLO:Select3,,?glo:select3:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Rejection Reason'),AT(8,52),USE(?glo:select4:Prompt)
                           ENTRY(@s40),AT(96,52,124,10),USE(GLO:Select4),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('&OK'),AT(168,76,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),STD(STD:Close),DEFAULT
                       PANEL,AT(4,72,224,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Estimate_Accepted{prop:Color} = 15066597
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?GLO:Select2{prop:ReadOnly} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 15066597
    Elsif ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 8454143
    Else ! If ?GLO:Select2{prop:Req} = True
        ?GLO:Select2{prop:FontColor} = 65793
        ?GLO:Select2{prop:Color} = 16777215
    End ! If ?GLO:Select2{prop:Req} = True
    ?GLO:Select2{prop:Trn} = 0
    ?GLO:Select2{prop:FontStyle} = font:Bold
    ?glo:select3:Prompt{prop:FontColor} = -1
    ?glo:select3:Prompt{prop:Color} = 15066597
    If ?GLO:Select3{prop:ReadOnly} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 15066597
    Elsif ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 8454143
    Else ! If ?GLO:Select3{prop:Req} = True
        ?GLO:Select3{prop:FontColor} = 65793
        ?GLO:Select3{prop:Color} = 16777215
    End ! If ?GLO:Select3{prop:Req} = True
    ?GLO:Select3{prop:Trn} = 0
    ?GLO:Select3{prop:FontStyle} = font:Bold
    ?Estimate_Rejected{prop:Color} = 15066597
    If ?glo:select2:2{prop:ReadOnly} = True
        ?glo:select2:2{prop:FontColor} = 65793
        ?glo:select2:2{prop:Color} = 15066597
    Elsif ?glo:select2:2{prop:Req} = True
        ?glo:select2:2{prop:FontColor} = 65793
        ?glo:select2:2{prop:Color} = 8454143
    Else ! If ?glo:select2:2{prop:Req} = True
        ?glo:select2:2{prop:FontColor} = 65793
        ?glo:select2:2{prop:Color} = 16777215
    End ! If ?glo:select2:2{prop:Req} = True
    ?glo:select2:2{prop:Trn} = 0
    ?glo:select2:2{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?glo:select3:2{prop:ReadOnly} = True
        ?glo:select3:2{prop:FontColor} = 65793
        ?glo:select3:2{prop:Color} = 15066597
    Elsif ?glo:select3:2{prop:Req} = True
        ?glo:select3:2{prop:FontColor} = 65793
        ?glo:select3:2{prop:Color} = 8454143
    Else ! If ?glo:select3:2{prop:Req} = True
        ?glo:select3:2{prop:FontColor} = 65793
        ?glo:select3:2{prop:Color} = 16777215
    End ! If ?glo:select3:2{prop:Req} = True
    ?glo:select3:2{prop:Trn} = 0
    ?glo:select3:2{prop:FontStyle} = font:Bold
    ?glo:select4:Prompt{prop:FontColor} = -1
    ?glo:select4:Prompt{prop:Color} = 15066597
    If ?GLO:Select4{prop:ReadOnly} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 15066597
    Elsif ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 8454143
    Else ! If ?GLO:Select4{prop:Req} = True
        ?GLO:Select4{prop:FontColor} = 65793
        ?GLO:Select4{prop:Color} = 16777215
    End ! If ?GLO:Select4{prop:Req} = True
    ?GLO:Select4{prop:Trn} = 0
    ?GLO:Select4{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Estimate_Reason',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Estimate_Reason',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Estimate_Accepted;  SolaceCtrlName = '?Estimate_Accepted';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select2;  SolaceCtrlName = '?GLO:Select2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select3:Prompt;  SolaceCtrlName = '?glo:select3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select3;  SolaceCtrlName = '?GLO:Select3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Estimate_Rejected;  SolaceCtrlName = '?Estimate_Rejected';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:2;  SolaceCtrlName = '?glo:select2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select3:2;  SolaceCtrlName = '?glo:select3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Prompt;  SolaceCtrlName = '?glo:select4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select4;  SolaceCtrlName = '?GLO:Select4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Estimate_Reason')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Estimate_Reason')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?glo:select2:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(Window)
  SELF.Opened=True
  Case glo:select1
      Of 'ACCEPTED'
          Unhide(?estimate_accepted)
      Of 'REJECTED'
          Unhide(?estimate_rejected)
  End
  Display()
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Estimate_Reason',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Estimate_Reason')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

