

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01036.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Bouncers PROCEDURE                             !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
save_wpr_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_bou_id          USHORT,AUTO
alias_locator_temp   REAL
FilesOpened          BYTE
x_temp               STRING('X')
tmp:CurrentEngineer  STRING(30)
tmp:CurrentEngineer2 STRING(30)
tmp:ExchangeUnit     STRING(60)
tmp:ExchangeUnit2    STRING(60)
tmp:SecondEntryReason STRING(3)
tmp:CurrentUnitDetails STRING(60)
tmp:HistoricUnitDetails STRING(60)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(BOUNCER)
                       PROJECT(bou:Bouncer_Job_Number)
                       PROJECT(bou:Record_Number)
                       PROJECT(bou:Original_Ref_Number)
                       JOIN(job_ali:Ref_Number_Key,bou:Bouncer_Job_Number)
                         PROJECT(job_ali:date_booked)
                         PROJECT(job_ali:Account_Number)
                         PROJECT(job_ali:Company_Name)
                         PROJECT(job_ali:Date_Completed)
                         PROJECT(job_ali:Warranty_Charge_Type)
                         PROJECT(job_ali:Charge_Type)
                         PROJECT(job_ali:Repair_Type_Warranty)
                         PROJECT(job_ali:Repair_Type)
                         PROJECT(job_ali:Ref_Number)
                         PROJECT(job_ali:ESN)
                         PROJECT(job_ali:MSN)
                         PROJECT(job_ali:Engineer)
                         PROJECT(job_ali:Exchange_Unit_Number)
                         PROJECT(job_ali:Model_Number)
                         PROJECT(job_ali:Manufacturer)
                         PROJECT(job_ali:Warranty_Job)
                         PROJECT(job_ali:Chargeable_Job)
                         JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                           PROJECT(jbn_ali:Engineers_Notes)
                           PROJECT(jbn_ali:Invoice_Text)
                           PROJECT(jbn_ali:Fault_Description)
                         END
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
bou:Bouncer_Job_Number LIKE(bou:Bouncer_Job_Number)   !List box control field - type derived from field
job_ali:date_booked    LIKE(job_ali:date_booked)      !List box control field - type derived from field
job_ali:Account_Number LIKE(job_ali:Account_Number)   !List box control field - type derived from field
job_ali:Company_Name   LIKE(job_ali:Company_Name)     !List box control field - type derived from field
job_ali:Date_Completed LIKE(job_ali:Date_Completed)   !List box control field - type derived from field
job_ali:Warranty_Charge_Type LIKE(job_ali:Warranty_Charge_Type) !List box control field - type derived from field
job_ali:Charge_Type    LIKE(job_ali:Charge_Type)      !List box control field - type derived from field
job_ali:Repair_Type_Warranty LIKE(job_ali:Repair_Type_Warranty) !List box control field - type derived from field
job_ali:Repair_Type    LIKE(job_ali:Repair_Type)      !List box control field - type derived from field
job_ali:Ref_Number     LIKE(job_ali:Ref_Number)       !List box control field - type derived from field
jbn_ali:Engineers_Notes LIKE(jbn_ali:Engineers_Notes) !List box control field - type derived from field
jbn_ali:Invoice_Text   LIKE(jbn_ali:Invoice_Text)     !List box control field - type derived from field
job_ali:ESN            LIKE(job_ali:ESN)              !List box control field - type derived from field
job_ali:MSN            LIKE(job_ali:MSN)              !List box control field - type derived from field
job_ali:Engineer       LIKE(job_ali:Engineer)         !List box control field - type derived from field
job_ali:Exchange_Unit_Number LIKE(job_ali:Exchange_Unit_Number) !List box control field - type derived from field
jbn_ali:Fault_Description LIKE(jbn_ali:Fault_Description) !List box control field - type derived from field
job_ali:Model_Number   LIKE(job_ali:Model_Number)     !List box control field - type derived from field
job_ali:Manufacturer   LIKE(job_ali:Manufacturer)     !List box control field - type derived from field
job_ali:Warranty_Job   LIKE(job_ali:Warranty_Job)     !Browse hot field - type derived from field
job_ali:Chargeable_Job LIKE(job_ali:Chargeable_Job)   !Browse hot field - type derived from field
bou:Record_Number      LIKE(bou:Record_Number)        !Primary key field - type derived from field
bou:Original_Ref_Number LIKE(bou:Original_Ref_Number) !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:date_booked)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Company_Name)
                       PROJECT(job:Date_Completed)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Third_Party_Site)
                       PROJECT(job:Engineer)
                       PROJECT(job:Exchange_Unit_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:Warranty_Job)
                       PROJECT(job:Chargeable_Job)
                       PROJECT(job:Bouncer)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Engineers_Notes)
                         PROJECT(jbn:Fault_Description)
                         PROJECT(jbn:Invoice_Text)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:date_booked        LIKE(job:date_booked)          !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Company_Name       LIKE(job:Company_Name)         !List box control field - type derived from field
job:Date_Completed     LIKE(job:Date_Completed)       !List box control field - type derived from field
job:Repair_Type        LIKE(job:Repair_Type)          !List box control field - type derived from field
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !List box control field - type derived from field
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !List box control field - type derived from field
job:Charge_Type        LIKE(job:Charge_Type)          !List box control field - type derived from field
jbn:Engineers_Notes    LIKE(jbn:Engineers_Notes)      !List box control field - type derived from field
jbn:Fault_Description  LIKE(jbn:Fault_Description)    !List box control field - type derived from field
jbn:Invoice_Text       LIKE(jbn:Invoice_Text)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:MSN                LIKE(job:MSN)                  !List box control field - type derived from field
job:Third_Party_Site   LIKE(job:Third_Party_Site)     !List box control field - type derived from field
job:Engineer           LIKE(job:Engineer)             !List box control field - type derived from field
job:Exchange_Unit_Number LIKE(job:Exchange_Unit_Number) !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Manufacturer       LIKE(job:Manufacturer)         !List box control field - type derived from field
job:Warranty_Job       LIKE(job:Warranty_Job)         !Browse hot field - type derived from field
job:Chargeable_Job     LIKE(job:Chargeable_Job)       !Browse hot field - type derived from field
job:Bouncer            LIKE(job:Bouncer)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(WARPARTS)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                       PROJECT(wpr:Part_Number)
                     END
Queue:Browse:3       QUEUE                            !Queue declaration for browse/combo box using ?List:3
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
wpr:Part_Number        LIKE(wpr:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW7::View:Browse    VIEW(PARTS)
                       PROJECT(par:Description)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                       PROJECT(par:Part_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
par:Part_Number        LIKE(par:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(PARTS_ALIAS)
                       PROJECT(par_ali:Description)
                       PROJECT(par_ali:Ref_Number)
                       PROJECT(par_ali:Part_Number)
                     END
Queue:Browse:4       QUEUE                            !Queue declaration for browse/combo box using ?List:4
par_ali:Description    LIKE(par_ali:Description)      !List box control field - type derived from field
par_ali:Ref_Number     LIKE(par_ali:Ref_Number)       !Browse key field - type derived from field
par_ali:Part_Number    LIKE(par_ali:Part_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(WARPARTS_ALIAS)
                       PROJECT(war_ali:Description)
                       PROJECT(war_ali:Ref_Number)
                       PROJECT(war_ali:Part_Number)
                     END
Queue:Browse:5       QUEUE                            !Queue declaration for browse/combo box using ?List:5
war_ali:Description    LIKE(war_ali:Description)      !List box control field - type derived from field
war_ali:Ref_Number     LIKE(war_ali:Ref_Number)       !Browse key field - type derived from field
war_ali:Part_Number    LIKE(war_ali:Part_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Bouncers'),AT(,,583,339),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Bouncers'),ALRT(F3Key),ALRT(F4Key),GRAY,DOUBLE
                       MENUBAR,NOMERGE
                         ITEM('Bouncer Defaults'),USE(?BouncerDefaults)
                       END
                       PROMPT('Current Job'),AT(8,8,112,9),USE(?Prompt1),TRN,FONT('Tahoma',10,COLOR:Green,FONT:bold,CHARSET:ANSI)
                       SHEET,AT(4,4,284,305),USE(?Sheet2),WIZARD,SPREAD
                         TAB('Tab 2'),USE(?Tab2)
                           BUTTON('&View Current Job [F3]'),AT(188,9,96,12),USE(?Change),LEFT,ICON('spysm.gif')
                           BUTTON('Count Jobs'),AT(124,9,60,12),USE(?CountJobsButton),LEFT,ICON('calc1.ico')
                           PROMPT('Unit Details:'),AT(96,21),USE(?Prompt30)
                           STRING(@s60),AT(136,21),USE(tmp:CurrentUnitDetails),FONT(,,,FONT:bold)
                           PROMPT('Transfer Parts'),AT(132,161,28,16),USE(?Prompt29),CENTER,FONT(,7,,)
                           GROUP,AT(132,153,28,56),USE(?Group1),BOXED
                           END
                           BUTTON,AT(136,177,20,12),USE(?TransferCharToWarr),ICON('next.gif')
                           BUTTON,AT(136,193,20,12),USE(?TransferWarrToChar),ICON('back.gif')
                           PROMPT('Chargeable Charge Type'),AT(8,213),USE(?job:Charge_Type:Prompt)
                           PROMPT('Warranty Charge Type'),AT(148,213),USE(?job:Warranty_Charge_Type:Prompt)
                           PROMPT('Warranty Repair Type'),AT(148,233),USE(?job:Repair_Type_Warranty:Prompt)
                           PROMPT('Exchange Unit:'),AT(8,297),USE(?tmp:ExchangeUnit:Prompt),HIDE
                           STRING(@s60),AT(68,297,212,12),USE(tmp:ExchangeUnit),HIDE,FONT(,,,FONT:bold)
                           PROMPT('Chargeable Repair Type'),AT(8,233),USE(?job:Repair_Type:Prompt)
                         END
                       END
                       LIST,AT(8,33,272,80),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('36R(2)|M~Job No~@s8@48R(2)|M~Date Booked~L@d6b@64L(2)|M~Account Number~@s15@70L(' &|
   '2)|M~Company Name~@s30@39L(2)|M~Date Comp~R@D6b@120L(2)|M~Repair Type~@s30@120L(' &|
   '2)|M~Repair Type~@s30@120L(2)|M~Charge Type~@s30@120L(2)|M~Charge Type~@s30@1020' &|
   'L(2)|M~Engineers Notes~@s255@1020L(2)|M~Fault Description~@s255@1020L(2)|M~Invoi' &|
   'ce Text~@s255@80L(2)|M~I.M.E.I. Number~@s20@80L(2)|M~MSN~@s20@120L(2)|M~Third Pa' &|
   'rty Repair Site~@s30@12L(2)|M~Engineer~@s3@32R(2)|M~Exchange Unit Number~L@s8@12' &|
   '0L(2)|M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse)
                       PROMPT('Chargeable Parts'),AT(8,149),USE(?Prompt2),TRN
                       ENTRY(@s8),AT(8,21,64,10),USE(job:Ref_Number),FONT('Tahoma',8,,FONT:bold),UPR
                       STRING(@s30),AT(8,221),USE(job:Charge_Type),FONT(,,,FONT:bold)
                       STRING(@s30),AT(8,241),USE(job:Repair_Type),FONT(,,,FONT:bold)
                       BUTTON('Submit For Invoicing'),AT(8,317,68,16),USE(?Submit_For_Invoice),LEFT,ICON('Invoice.gif')
                       BUTTON('Reject From Invoicing'),AT(76,317,68,16),USE(?Reject_From_Invoice),LEFT,ICON('no_invoice.gif')
                       LIST,AT(8,157,124,52),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:2)
                       PROMPT('I.M.E.I. Number'),AT(8,253),USE(?Prompt11)
                       STRING(@s20),AT(8,261),USE(job:ESN),FONT(,,,FONT:bold)
                       PROMPT('M.S.N.'),AT(8,273),USE(?Prompt11:2)
                       STRING(@s20),AT(8,281),USE(job:MSN),FONT(,,,FONT:bold)
                       PROMPT('Engineer'),AT(148,253),USE(?tmp:CurrentEngineer:Prompt)
                       STRING(@s30),AT(148,261),USE(tmp:CurrentEngineer),FONT(,,,FONT:bold)
                       PROMPT('Third Party Site'),AT(148,273),USE(?job:Third_Party_Site:Prompt)
                       PROMPT('Warranty Parts'),AT(160,149),USE(?Prompt3),TRN
                       LIST,AT(160,157,124,52),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:3)
                       STRING(@s30),AT(148,281,100,12),USE(job:Third_Party_Site),FONT(,,,FONT:bold)
                       PROMPT('Fault Description'),AT(8,117),USE(?Prompt7)
                       TEXT,AT(8,125,136,20),USE(jbn:Fault_Description),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                       TEXT,AT(148,125,136,20),USE(jbn:Invoice_Text),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                       PROMPT('Invoice Text'),AT(148,117),USE(?Prompt7:2)
                       STRING(@s30),AT(148,221),USE(job:Warranty_Charge_Type),FONT(,,,FONT:bold)
                       STRING(@s30),AT(148,241),USE(job:Repair_Type_Warranty),FONT(,,,FONT:bold)
                       SHEET,AT(292,4,288,305),USE(?CurrentTab),WIZARD,SPREAD
                         TAB('By Job Number'),USE(?Tab:2)
                           PROMPT('Historic Job'),AT(296,8),USE(?Prompt6),TRN,FONT('Tahoma',12,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           BUTTON('View Historic Job [F4]'),AT(464,9,108,12),USE(?View_Historic_Job),LEFT,ICON('History2.gif')
                           PROMPT('Unit Details:'),AT(296,21),USE(?Prompt30:2)
                           STRING(@s60),AT(336,21),USE(tmp:HistoricUnitDetails),FONT(,,,FONT:bold)
                           LIST,AT(296,33,276,80),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('36R(2)|M~Job No~@s8@47R(2)|M~Date Booked~L@d6b@65L(2)|M~Account Number~@s15@68L(' &|
   '2)|M~Company Name~@s30@40R(2)|M~Date Comp~@D6b@120L(2)|M~Charge Type~@s30@120L(2' &|
   ')|M~Charge Type~@s30@120L(2)|M~Repair Type~@s30@120L(2)|M~Repair Type~@s30@36L(2' &|
   ')|M~Job Number~@p<<<<<<<<<<<<<<<<#p@1020L(2)|M~Engineers Notes~@s255@1020L(2)|M~Invoice ' &|
   'Text~@s255@80L(2)|M~I.M.E.I. Number~@s20@80L(2)|M~MSN~@s20@12L(2)|M~Engineer~@s3' &|
   '@32R(2)|M~Exchange Unit Number~L@s8@1020R(2)|M~Fault Description~L@s255@120L(2)|' &|
   'M~Model Number~@s30@120L(2)|M~Manufacturer~@s30@'),FROM(Queue:Browse:1)
                           PROMPT('Chargeable Parts'),AT(296,149),USE(?Prompt2:2),TRN
                           LIST,AT(296,157,136,52),USE(?List:4),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:4)
                           PROMPT('Warranty Parts'),AT(436,149),USE(?Prompt3:2),TRN
                           LIST,AT(436,157,136,52),USE(?List:5),IMM,MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:5)
                           PROMPT('Chargeable Charge Type'),AT(296,213),USE(?job_ali:Charge_Type:Prompt)
                           PROMPT('Fault Description'),AT(296,117),USE(?Prompt7:3)
                           PROMPT('Invoice Text'),AT(436,117),USE(?Prompt7:4)
                           TEXT,AT(296,125,136,21),USE(jbn_ali:Fault_Description),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           TEXT,AT(436,125,136,21),USE(jbn_ali:Invoice_Text),SKIP,VSCROLL,FONT(,,,FONT:bold,CHARSET:ANSI),READONLY
                           STRING(@s30),AT(296,221),USE(job_ali:Charge_Type),FONT(,,,FONT:bold)
                           STRING(@s30),AT(436,221),USE(job_ali:Warranty_Charge_Type),FONT(,,,FONT:bold)
                           PROMPT('Chargeable Repair Type'),AT(296,233),USE(?job_ali:Repair_Type:Prompt)
                           PROMPT('Warranty Charge Type'),AT(436,213),USE(?job_ali:Warranty_Charge_Type:Prompt)
                           STRING(@s30),AT(436,241),USE(job_ali:Repair_Type_Warranty),FONT(,,,FONT:bold)
                           PROMPT('I.M.E.I. Number:'),AT(296,253),USE(?Prompt11:5)
                           PROMPT('Engineer'),AT(436,253),USE(?tmp:CurrentEngineer2:Prompt)
                           STRING(@s20),AT(296,261),USE(job_ali:ESN),FONT(,,,FONT:bold)
                           PROMPT('M.S.N.'),AT(296,273),USE(?Prompt11:7)
                           PROMPT('Third Party Site:'),AT(436,273),USE(?job_ali:Third_Party_Site:Prompt)
                           STRING(@s20),AT(296,281),USE(job_ali:MSN),FONT(,,,FONT:bold)
                           STRING(@s30),AT(436,281,100,12),USE(job_ali:Third_Party_Site),FONT(,,,FONT:bold)
                           PROMPT('Exchange Unit:'),AT(296,293),USE(?tmp:ExchangeUnit2:Prompt),HIDE
                           STRING(@s60),AT(356,293,212,12),USE(tmp:ExchangeUnit2),HIDE,FONT(,,,FONT:bold)
                           STRING(@s30),AT(436,261),USE(tmp:CurrentEngineer2),FONT(,,,FONT:bold)
                           PROMPT('Warranty Repair Type'),AT(436,233),USE(?job_ali:Repair_Type_Warranty:Prompt)
                           STRING(@s30),AT(296,241),USE(job_ali:Repair_Type),FONT(,,,FONT:bold)
                         END
                       END
                       BUTTON('Compare Fault Codes'),AT(428,317,68,16),USE(?Compare_Fault_Codes),LEFT,ICON('fault.gif')
                       BUTTON('Cancel'),AT(520,317,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,313,576,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('Submit For Claiming'),AT(152,317,68,16),USE(?Submit_For_Invoice:2),LEFT,ICON('Invoice.gif')
                       BUTTON('Reject From Claiming'),AT(220,317,68,16),USE(?Reject_From_Invoice:2),LEFT,ICON('no_invoice.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ScrollDownOne          PROCEDURE()
                     END

BRW6::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW8                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:3                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW7                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW7::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW9                 CLASS(BrowseClass)               !Browse using ?List:4
Q                      &Queue:Browse:4                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW10                CLASS(BrowseClass)               !Browse using ?List:5
Q                      &Queue:Browse:5                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10::Sort0:Locator StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt30{prop:FontColor} = -1
    ?Prompt30{prop:Color} = 15066597
    ?tmp:CurrentUnitDetails{prop:FontColor} = -1
    ?tmp:CurrentUnitDetails{prop:Color} = 15066597
    ?Prompt29{prop:FontColor} = -1
    ?Prompt29{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?job:Charge_Type:Prompt{prop:FontColor} = -1
    ?job:Charge_Type:Prompt{prop:Color} = 15066597
    ?job:Warranty_Charge_Type:Prompt{prop:FontColor} = -1
    ?job:Warranty_Charge_Type:Prompt{prop:Color} = 15066597
    ?job:Repair_Type_Warranty:Prompt{prop:FontColor} = -1
    ?job:Repair_Type_Warranty:Prompt{prop:Color} = 15066597
    ?tmp:ExchangeUnit:Prompt{prop:FontColor} = -1
    ?tmp:ExchangeUnit:Prompt{prop:Color} = 15066597
    ?tmp:ExchangeUnit{prop:FontColor} = -1
    ?tmp:ExchangeUnit{prop:Color} = 15066597
    ?job:Repair_Type:Prompt{prop:FontColor} = -1
    ?job:Repair_Type:Prompt{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold
    ?job:Charge_Type{prop:FontColor} = -1
    ?job:Charge_Type{prop:Color} = 15066597
    ?job:Repair_Type{prop:FontColor} = -1
    ?job:Repair_Type{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?job:ESN{prop:FontColor} = -1
    ?job:ESN{prop:Color} = 15066597
    ?Prompt11:2{prop:FontColor} = -1
    ?Prompt11:2{prop:Color} = 15066597
    ?job:MSN{prop:FontColor} = -1
    ?job:MSN{prop:Color} = 15066597
    ?tmp:CurrentEngineer:Prompt{prop:FontColor} = -1
    ?tmp:CurrentEngineer:Prompt{prop:Color} = 15066597
    ?tmp:CurrentEngineer{prop:FontColor} = -1
    ?tmp:CurrentEngineer{prop:Color} = 15066597
    ?job:Third_Party_Site:Prompt{prop:FontColor} = -1
    ?job:Third_Party_Site:Prompt{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?job:Third_Party_Site{prop:FontColor} = -1
    ?job:Third_Party_Site{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?jbn:Fault_Description{prop:ReadOnly} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 15066597
    Elsif ?jbn:Fault_Description{prop:Req} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 8454143
    Else ! If ?jbn:Fault_Description{prop:Req} = True
        ?jbn:Fault_Description{prop:FontColor} = 65793
        ?jbn:Fault_Description{prop:Color} = 16777215
    End ! If ?jbn:Fault_Description{prop:Req} = True
    ?jbn:Fault_Description{prop:Trn} = 0
    ?jbn:Fault_Description{prop:FontStyle} = font:Bold
    If ?jbn:Invoice_Text{prop:ReadOnly} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 15066597
    Elsif ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 8454143
    Else ! If ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 16777215
    End ! If ?jbn:Invoice_Text{prop:Req} = True
    ?jbn:Invoice_Text{prop:Trn} = 0
    ?jbn:Invoice_Text{prop:FontStyle} = font:Bold
    ?Prompt7:2{prop:FontColor} = -1
    ?Prompt7:2{prop:Color} = 15066597
    ?job:Warranty_Charge_Type{prop:FontColor} = -1
    ?job:Warranty_Charge_Type{prop:Color} = 15066597
    ?job:Repair_Type_Warranty{prop:FontColor} = -1
    ?job:Repair_Type_Warranty{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?Prompt30:2{prop:FontColor} = -1
    ?Prompt30:2{prop:Color} = 15066597
    ?tmp:HistoricUnitDetails{prop:FontColor} = -1
    ?tmp:HistoricUnitDetails{prop:Color} = 15066597
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    ?List:4{prop:FontColor} = 65793
    ?List:4{prop:Color}= 16777215
    ?List:4{prop:Color,2} = 16777215
    ?List:4{prop:Color,3} = 12937777
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    ?List:5{prop:FontColor} = 65793
    ?List:5{prop:Color}= 16777215
    ?List:5{prop:Color,2} = 16777215
    ?List:5{prop:Color,3} = 12937777
    ?job_ali:Charge_Type:Prompt{prop:FontColor} = -1
    ?job_ali:Charge_Type:Prompt{prop:Color} = 15066597
    ?Prompt7:3{prop:FontColor} = -1
    ?Prompt7:3{prop:Color} = 15066597
    ?Prompt7:4{prop:FontColor} = -1
    ?Prompt7:4{prop:Color} = 15066597
    If ?jbn_ali:Fault_Description{prop:ReadOnly} = True
        ?jbn_ali:Fault_Description{prop:FontColor} = 65793
        ?jbn_ali:Fault_Description{prop:Color} = 15066597
    Elsif ?jbn_ali:Fault_Description{prop:Req} = True
        ?jbn_ali:Fault_Description{prop:FontColor} = 65793
        ?jbn_ali:Fault_Description{prop:Color} = 8454143
    Else ! If ?jbn_ali:Fault_Description{prop:Req} = True
        ?jbn_ali:Fault_Description{prop:FontColor} = 65793
        ?jbn_ali:Fault_Description{prop:Color} = 16777215
    End ! If ?jbn_ali:Fault_Description{prop:Req} = True
    ?jbn_ali:Fault_Description{prop:Trn} = 0
    ?jbn_ali:Fault_Description{prop:FontStyle} = font:Bold
    If ?jbn_ali:Invoice_Text{prop:ReadOnly} = True
        ?jbn_ali:Invoice_Text{prop:FontColor} = 65793
        ?jbn_ali:Invoice_Text{prop:Color} = 15066597
    Elsif ?jbn_ali:Invoice_Text{prop:Req} = True
        ?jbn_ali:Invoice_Text{prop:FontColor} = 65793
        ?jbn_ali:Invoice_Text{prop:Color} = 8454143
    Else ! If ?jbn_ali:Invoice_Text{prop:Req} = True
        ?jbn_ali:Invoice_Text{prop:FontColor} = 65793
        ?jbn_ali:Invoice_Text{prop:Color} = 16777215
    End ! If ?jbn_ali:Invoice_Text{prop:Req} = True
    ?jbn_ali:Invoice_Text{prop:Trn} = 0
    ?jbn_ali:Invoice_Text{prop:FontStyle} = font:Bold
    ?job_ali:Charge_Type{prop:FontColor} = -1
    ?job_ali:Charge_Type{prop:Color} = 15066597
    ?job_ali:Warranty_Charge_Type{prop:FontColor} = -1
    ?job_ali:Warranty_Charge_Type{prop:Color} = 15066597
    ?job_ali:Repair_Type:Prompt{prop:FontColor} = -1
    ?job_ali:Repair_Type:Prompt{prop:Color} = 15066597
    ?job_ali:Warranty_Charge_Type:Prompt{prop:FontColor} = -1
    ?job_ali:Warranty_Charge_Type:Prompt{prop:Color} = 15066597
    ?job_ali:Repair_Type_Warranty{prop:FontColor} = -1
    ?job_ali:Repair_Type_Warranty{prop:Color} = 15066597
    ?Prompt11:5{prop:FontColor} = -1
    ?Prompt11:5{prop:Color} = 15066597
    ?tmp:CurrentEngineer2:Prompt{prop:FontColor} = -1
    ?tmp:CurrentEngineer2:Prompt{prop:Color} = 15066597
    ?job_ali:ESN{prop:FontColor} = -1
    ?job_ali:ESN{prop:Color} = 15066597
    ?Prompt11:7{prop:FontColor} = -1
    ?Prompt11:7{prop:Color} = 15066597
    ?job_ali:Third_Party_Site:Prompt{prop:FontColor} = -1
    ?job_ali:Third_Party_Site:Prompt{prop:Color} = 15066597
    ?job_ali:MSN{prop:FontColor} = -1
    ?job_ali:MSN{prop:Color} = 15066597
    ?job_ali:Third_Party_Site{prop:FontColor} = -1
    ?job_ali:Third_Party_Site{prop:Color} = 15066597
    ?tmp:ExchangeUnit2:Prompt{prop:FontColor} = -1
    ?tmp:ExchangeUnit2:Prompt{prop:Color} = 15066597
    ?tmp:ExchangeUnit2{prop:FontColor} = -1
    ?tmp:ExchangeUnit2{prop:Color} = 15066597
    ?tmp:CurrentEngineer2{prop:FontColor} = -1
    ?tmp:CurrentEngineer2{prop:Color} = 15066597
    ?job_ali:Repair_Type_Warranty:Prompt{prop:FontColor} = -1
    ?job_ali:Repair_Type_Warranty:Prompt{prop:Color} = 15066597
    ?job_ali:Repair_Type{prop:FontColor} = -1
    ?job_ali:Repair_Type{prop:Color} = 15066597
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Bouncers',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Bouncers',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Browse_Bouncers',1)
    SolaceViewVars('save_par_id',save_par_id,'Browse_Bouncers',1)
    SolaceViewVars('save_bou_id',save_bou_id,'Browse_Bouncers',1)
    SolaceViewVars('alias_locator_temp',alias_locator_temp,'Browse_Bouncers',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Bouncers',1)
    SolaceViewVars('x_temp',x_temp,'Browse_Bouncers',1)
    SolaceViewVars('tmp:CurrentEngineer',tmp:CurrentEngineer,'Browse_Bouncers',1)
    SolaceViewVars('tmp:CurrentEngineer2',tmp:CurrentEngineer2,'Browse_Bouncers',1)
    SolaceViewVars('tmp:ExchangeUnit',tmp:ExchangeUnit,'Browse_Bouncers',1)
    SolaceViewVars('tmp:ExchangeUnit2',tmp:ExchangeUnit2,'Browse_Bouncers',1)
    SolaceViewVars('tmp:SecondEntryReason',tmp:SecondEntryReason,'Browse_Bouncers',1)
    SolaceViewVars('tmp:CurrentUnitDetails',tmp:CurrentUnitDetails,'Browse_Bouncers',1)
    SolaceViewVars('tmp:HistoricUnitDetails',tmp:HistoricUnitDetails,'Browse_Bouncers',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?BouncerDefaults;  SolaceCtrlName = '?BouncerDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CountJobsButton;  SolaceCtrlName = '?CountJobsButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt30;  SolaceCtrlName = '?Prompt30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentUnitDetails;  SolaceCtrlName = '?tmp:CurrentUnitDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt29;  SolaceCtrlName = '?Prompt29';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TransferCharToWarr;  SolaceCtrlName = '?TransferCharToWarr';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TransferWarrToChar;  SolaceCtrlName = '?TransferWarrToChar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type:Prompt;  SolaceCtrlName = '?job:Charge_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Charge_Type:Prompt;  SolaceCtrlName = '?job:Warranty_Charge_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type_Warranty:Prompt;  SolaceCtrlName = '?job:Repair_Type_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeUnit:Prompt;  SolaceCtrlName = '?tmp:ExchangeUnit:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeUnit;  SolaceCtrlName = '?tmp:ExchangeUnit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type:Prompt;  SolaceCtrlName = '?job:Repair_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type;  SolaceCtrlName = '?job:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type;  SolaceCtrlName = '?job:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Submit_For_Invoice;  SolaceCtrlName = '?Submit_For_Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reject_From_Invoice;  SolaceCtrlName = '?Reject_From_Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11:2;  SolaceCtrlName = '?Prompt11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:MSN;  SolaceCtrlName = '?job:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentEngineer:Prompt;  SolaceCtrlName = '?tmp:CurrentEngineer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentEngineer;  SolaceCtrlName = '?tmp:CurrentEngineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Third_Party_Site:Prompt;  SolaceCtrlName = '?job:Third_Party_Site:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Third_Party_Site;  SolaceCtrlName = '?job:Third_Party_Site';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Fault_Description;  SolaceCtrlName = '?jbn:Fault_Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Invoice_Text;  SolaceCtrlName = '?jbn:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:2;  SolaceCtrlName = '?Prompt7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Charge_Type;  SolaceCtrlName = '?job:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Repair_Type_Warranty;  SolaceCtrlName = '?job:Repair_Type_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?View_Historic_Job;  SolaceCtrlName = '?View_Historic_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt30:2;  SolaceCtrlName = '?Prompt30:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:HistoricUnitDetails;  SolaceCtrlName = '?tmp:HistoricUnitDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:4;  SolaceCtrlName = '?List:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:5;  SolaceCtrlName = '?List:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Charge_Type:Prompt;  SolaceCtrlName = '?job_ali:Charge_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:3;  SolaceCtrlName = '?Prompt7:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7:4;  SolaceCtrlName = '?Prompt7:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn_ali:Fault_Description;  SolaceCtrlName = '?jbn_ali:Fault_Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn_ali:Invoice_Text;  SolaceCtrlName = '?jbn_ali:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Charge_Type;  SolaceCtrlName = '?job_ali:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Warranty_Charge_Type;  SolaceCtrlName = '?job_ali:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Repair_Type:Prompt;  SolaceCtrlName = '?job_ali:Repair_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Warranty_Charge_Type:Prompt;  SolaceCtrlName = '?job_ali:Warranty_Charge_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Repair_Type_Warranty;  SolaceCtrlName = '?job_ali:Repair_Type_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11:5;  SolaceCtrlName = '?Prompt11:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentEngineer2:Prompt;  SolaceCtrlName = '?tmp:CurrentEngineer2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:ESN;  SolaceCtrlName = '?job_ali:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11:7;  SolaceCtrlName = '?Prompt11:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Third_Party_Site:Prompt;  SolaceCtrlName = '?job_ali:Third_Party_Site:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:MSN;  SolaceCtrlName = '?job_ali:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Third_Party_Site;  SolaceCtrlName = '?job_ali:Third_Party_Site';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeUnit2:Prompt;  SolaceCtrlName = '?tmp:ExchangeUnit2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeUnit2;  SolaceCtrlName = '?tmp:ExchangeUnit2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentEngineer2;  SolaceCtrlName = '?tmp:CurrentEngineer2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Repair_Type_Warranty:Prompt;  SolaceCtrlName = '?job_ali:Repair_Type_Warranty:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Repair_Type;  SolaceCtrlName = '?job_ali:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Compare_Fault_Codes;  SolaceCtrlName = '?Compare_Fault_Codes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Submit_For_Invoice:2;  SolaceCtrlName = '?Submit_For_Invoice:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reject_From_Invoice:2;  SolaceCtrlName = '?Reject_From_Invoice:2';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Bouncers')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Bouncers')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Bouncers'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:PARTS_ALIAS.Open
  Relate:REPTYDEF.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:JOBS_ALIAS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:MODELNUM.UseFile
  Access:CHARTYPE.UseFile
  Access:JOBSE.UseFile
  Access:REPAIRTY.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:BOUNCER,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:JOBS,SELF)
  BRW8.Init(?List:3,Queue:Browse:3.ViewPosition,BRW8::View:Browse,Queue:Browse:3,Relate:WARPARTS,SELF)
  BRW7.Init(?List:2,Queue:Browse:2.ViewPosition,BRW7::View:Browse,Queue:Browse:2,Relate:PARTS,SELF)
  BRW9.Init(?List:4,Queue:Browse:4.ViewPosition,BRW9::View:Browse,Queue:Browse:4,Relate:PARTS_ALIAS,SELF)
  BRW10.Init(?List:5,Queue:Browse:5.ViewPosition,BRW10::View:Browse,Queue:Browse:5,Relate:WARPARTS_ALIAS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  ?List:4{prop:vcr} = TRUE
  ?List:5{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,bou:Bouncer_Job_Number_Key)
  BRW1.AddRange(bou:Original_Ref_Number,job:Ref_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,bou:Bouncer_Job_Number,1,BRW1)
  BRW1.AddField(bou:Bouncer_Job_Number,BRW1.Q.bou:Bouncer_Job_Number)
  BRW1.AddField(job_ali:date_booked,BRW1.Q.job_ali:date_booked)
  BRW1.AddField(job_ali:Account_Number,BRW1.Q.job_ali:Account_Number)
  BRW1.AddField(job_ali:Company_Name,BRW1.Q.job_ali:Company_Name)
  BRW1.AddField(job_ali:Date_Completed,BRW1.Q.job_ali:Date_Completed)
  BRW1.AddField(job_ali:Warranty_Charge_Type,BRW1.Q.job_ali:Warranty_Charge_Type)
  BRW1.AddField(job_ali:Charge_Type,BRW1.Q.job_ali:Charge_Type)
  BRW1.AddField(job_ali:Repair_Type_Warranty,BRW1.Q.job_ali:Repair_Type_Warranty)
  BRW1.AddField(job_ali:Repair_Type,BRW1.Q.job_ali:Repair_Type)
  BRW1.AddField(job_ali:Ref_Number,BRW1.Q.job_ali:Ref_Number)
  BRW1.AddField(jbn_ali:Engineers_Notes,BRW1.Q.jbn_ali:Engineers_Notes)
  BRW1.AddField(jbn_ali:Invoice_Text,BRW1.Q.jbn_ali:Invoice_Text)
  BRW1.AddField(job_ali:ESN,BRW1.Q.job_ali:ESN)
  BRW1.AddField(job_ali:MSN,BRW1.Q.job_ali:MSN)
  BRW1.AddField(job_ali:Engineer,BRW1.Q.job_ali:Engineer)
  BRW1.AddField(job_ali:Exchange_Unit_Number,BRW1.Q.job_ali:Exchange_Unit_Number)
  BRW1.AddField(jbn_ali:Fault_Description,BRW1.Q.jbn_ali:Fault_Description)
  BRW1.AddField(job_ali:Model_Number,BRW1.Q.job_ali:Model_Number)
  BRW1.AddField(job_ali:Manufacturer,BRW1.Q.job_ali:Manufacturer)
  BRW1.AddField(job_ali:Warranty_Job,BRW1.Q.job_ali:Warranty_Job)
  BRW1.AddField(job_ali:Chargeable_Job,BRW1.Q.job_ali:Chargeable_Job)
  BRW1.AddField(bou:Record_Number,BRW1.Q.bou:Record_Number)
  BRW1.AddField(bou:Original_Ref_Number,BRW1.Q.bou:Original_Ref_Number)
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,job:Bouncer_Key)
  BRW6.AddRange(job:Bouncer,x_temp)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW6)
  BRW6.AddField(job:Ref_Number,BRW6.Q.job:Ref_Number)
  BRW6.AddField(job:date_booked,BRW6.Q.job:date_booked)
  BRW6.AddField(job:Account_Number,BRW6.Q.job:Account_Number)
  BRW6.AddField(job:Company_Name,BRW6.Q.job:Company_Name)
  BRW6.AddField(job:Date_Completed,BRW6.Q.job:Date_Completed)
  BRW6.AddField(job:Repair_Type,BRW6.Q.job:Repair_Type)
  BRW6.AddField(job:Repair_Type_Warranty,BRW6.Q.job:Repair_Type_Warranty)
  BRW6.AddField(job:Warranty_Charge_Type,BRW6.Q.job:Warranty_Charge_Type)
  BRW6.AddField(job:Charge_Type,BRW6.Q.job:Charge_Type)
  BRW6.AddField(jbn:Engineers_Notes,BRW6.Q.jbn:Engineers_Notes)
  BRW6.AddField(jbn:Fault_Description,BRW6.Q.jbn:Fault_Description)
  BRW6.AddField(jbn:Invoice_Text,BRW6.Q.jbn:Invoice_Text)
  BRW6.AddField(job:ESN,BRW6.Q.job:ESN)
  BRW6.AddField(job:MSN,BRW6.Q.job:MSN)
  BRW6.AddField(job:Third_Party_Site,BRW6.Q.job:Third_Party_Site)
  BRW6.AddField(job:Engineer,BRW6.Q.job:Engineer)
  BRW6.AddField(job:Exchange_Unit_Number,BRW6.Q.job:Exchange_Unit_Number)
  BRW6.AddField(job:Model_Number,BRW6.Q.job:Model_Number)
  BRW6.AddField(job:Manufacturer,BRW6.Q.job:Manufacturer)
  BRW6.AddField(job:Warranty_Job,BRW6.Q.job:Warranty_Job)
  BRW6.AddField(job:Chargeable_Job,BRW6.Q.job:Chargeable_Job)
  BRW6.AddField(job:Bouncer,BRW6.Q.job:Bouncer)
  BRW8.Q &= Queue:Browse:3
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,wpr:Part_Number_Key)
  BRW8.AddRange(wpr:Ref_Number,Relate:WARPARTS,Relate:JOBS)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,wpr:Part_Number,1,BRW8)
  BRW8.AddField(wpr:Description,BRW8.Q.wpr:Description)
  BRW8.AddField(wpr:Record_Number,BRW8.Q.wpr:Record_Number)
  BRW8.AddField(wpr:Ref_Number,BRW8.Q.wpr:Ref_Number)
  BRW8.AddField(wpr:Part_Number,BRW8.Q.wpr:Part_Number)
  BRW7.Q &= Queue:Browse:2
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,par:Part_Number_Key)
  BRW7.AddRange(par:Ref_Number,Relate:PARTS,Relate:JOBS)
  BRW7.AddLocator(BRW7::Sort0:Locator)
  BRW7::Sort0:Locator.Init(,par:Part_Number,1,BRW7)
  BRW7.AddField(par:Description,BRW7.Q.par:Description)
  BRW7.AddField(par:Record_Number,BRW7.Q.par:Record_Number)
  BRW7.AddField(par:Ref_Number,BRW7.Q.par:Ref_Number)
  BRW7.AddField(par:Part_Number,BRW7.Q.par:Part_Number)
  BRW9.Q &= Queue:Browse:4
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,par_ali:Part_Number_Key)
  BRW9.AddRange(par_ali:Ref_Number,job_ali:Ref_Number)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,par_ali:Part_Number,1,BRW9)
  BRW9.AddField(par_ali:Description,BRW9.Q.par_ali:Description)
  BRW9.AddField(par_ali:Ref_Number,BRW9.Q.par_ali:Ref_Number)
  BRW9.AddField(par_ali:Part_Number,BRW9.Q.par_ali:Part_Number)
  BRW10.Q &= Queue:Browse:5
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,war_ali:Part_Number_Key)
  BRW10.AddRange(war_ali:Ref_Number,job_ali:Ref_Number)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,war_ali:Part_Number,1,BRW10)
  BRW10.AddField(war_ali:Description,BRW10.Q.war_ali:Description)
  BRW10.AddField(war_ali:Ref_Number,BRW10.Q.war_ali:Ref_Number)
  BRW10.AddField(war_ali:Part_Number,BRW10.Q.war_ali:Part_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW6.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:PARTS_ALIAS.Close
    Relate:REPTYDEF.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Bouncers'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Bouncers',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBS
    ReturnValue = GlobalResponse
  END
  ! Start Change 4691 BE(29/09/2004)
  brw1.ResetFromFile
  brw1.PostNewSelection
  
  brw7.ResetFromFile
  brw7.PostNewSelection
  
  brw8.ResetFromFile
  brw8.PostNewSelection
  ! End Change 4691 BE(29/09/2004)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Compare_Fault_Codes
      brw1.updatebuffer()
      brw6.updatebuffer()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BouncerDefaults
      ThisWindow.Update
      BouncerDefaults
      ThisWindow.Reset
    OF ?Change
      ThisWindow.Update
      brw1.resetsort(1)
      brw6.resetsort(1)
    OF ?CountJobsButton
      ThisWindow.Update
      CASE MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?',|
                      'ServiceBase 2000',|
                      'Styles\question.ico','&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,|
                      12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
        OF 1 ! &Yes Button
          count# = 0
          SETCURSOR(cursor:wait)
          SavePosition# = POSITION(BRW6::View:Browse)
          SET(BRW6::View:Browse)
          LOOP
              NEXT(BRW6::View:Browse)
              IF (ERRORCODE()) THEN
                  BREAK
              END
              count# += 1
          END
          RESET(BRW6::View:Browse, SavePosition#)
          SETCURSOR()
          MessageEx('There are ' & clip(count#) & ' records in this browse.',|
                    'ServiceBase 2000',|
                    'Styles\idea.ico','&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,|
                    12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
        OF 2 ! &No Button
      END
    OF ?TransferCharToWarr
      ThisWindow.Update
       If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('Are you sure you want to transfer all your Chargeable Parts to Warranty Parts.','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = brw6.q.job:Ref_Number
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                  
                      count# = 0
                      setcursor(cursor:wait)
                      save_par_id = access:parts.savefile()
                      access:parts.clearkey(par:part_number_key)
                      par:ref_number  = job:ref_number
                      set(par:part_number_key,par:part_number_key)
                      loop
                          if access:parts.next()
                             break
                          end !if
                          if par:ref_number  <> job:ref_number      |
                              then break.  ! end if
                          If par:pending_ref_number <> ''
                              access:ordpend.clearkey(ope:ref_number_key)
                              ope:ref_number = par:pending_ref_number
                              if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                                  ope:part_type = 'JOB'
                                  access:ordpend.update()
                              End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                          End!If wpr:pending_ref_number <> ''
                          If par:order_number <> ''
                              access:ordparts.clearkey(orp:record_number_key)
                              orp:record_number   = par:order_number
                              If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                                  orp:part_type = 'JOB'
                                  access:ordparts.update()
                              End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                          End!If wpr:order_number <> ''
                          get(warparts,0)
                          if access:warparts.primerecord() = Level:Benign
                              record_number$  = wpr:record_number
                              wpr:record      :=: par:record
                              wpr:record_number   = record_number$
                              if access:warparts.insert()
                                  access:warparts.cancelautoinc()
                              End!if access:parts.insert()
                          End!if access:parts.primerecord() = Level:Benign
                          Delete(parts)
                          count# += 1
                      end !loop
                      access:parts.restorefile(save_par_id)
                      setcursor()
                      if count# <> 0
      
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              aud:type          = 'JOB'
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'CHARGEABLE PARTS TRANSFERRED TO WARRANTY'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
                          If job:warranty_job <> 'YES'
                              job:Warranty_Charge_Type = ''
                              Case MessageEx('This job has not been marked as Warranty.<13,10><13,10>Do you wish to make this a Split Job, or make it a Warranty Only Job?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Make Split Job|&Make Warranty',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                                  Of 1 ! &Make Split Job Button
                                      job:Warranty_job = 'YES'
      !                                warranty_job_temp = job:Warranty_job
      !                                Post(event:accepted,?job:Warranty_job)
                                      
                                  Of 2 ! &Make Chargeable Button
                                      job:chargeable_job = 'NO'
                                      job:warranty_job = 'YES'
      !                                warranty_job_temp = job:warranty_job
      !                                chargeable_job_temp = job:chargeable_job
      !                                Post(event:accepted,?job:chargeable_job)
      !                                Post(event:accepted,?job:warranty_job)
                                      
                              End!Case MessageEx
                          Else!If job:chargeable_job <> 'YES'
                              Case MessageEx('Transfer Complete.','ServiceBase 2000',|
                                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          End!If job:warranty_job <> 'YES'
                      End!if count# <> 0
                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                      mod:Model_Number    = job:Model_NUmber
                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Found
                          job:EDI = PendingJob(mod:Manufacturer)
                          job:Manufacturer    = mod:Manufacturer
                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      Access:JOBS.Update()
                  Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  
              Of 2 ! &No Button
          End!Case MessageEx
      End!If SecurityCheck('JOBS - TRANSFER PARTS')
      BRW6.ResetSort(1)
      BRW7.ResetSort(1)
      BRW8.ResetSort(1)
    OF ?TransferWarrToChar
      ThisWindow.Update
      If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('JOBS - TRANSFER PARTS')
          Case MessageEx('Are you sure you want to transfer all your Warranty Parts to Chargeable Parts.','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = brw6.q.job:Ref_Number
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
      
                      
                      count# = 0
                      setcursor(cursor:wait)
                      save_wpr_id = access:warparts.savefile()
                      access:warparts.clearkey(wpr:part_number_key)
                      wpr:ref_number  = job:ref_number
                      set(wpr:part_number_key,wpr:part_number_key)
                      loop
                          if access:warparts.next()
                             break
                          end !if
                          if wpr:ref_number  <> job:ref_number      |
                              then break.  ! end if
                          If wpr:pending_ref_number <> ''
                              access:ordpend.clearkey(ope:ref_number_key)
                              ope:ref_number = wpr:pending_ref_number
                              if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                                  ope:part_type = 'JOB'
                                  access:ordpend.update()
                              End!if access:ordpend.tryfetch(ope:ref_number_key) = Level:Benign
                          End!If wpr:pending_ref_number <> ''
                          If wpr:order_number <> ''
                              access:ordparts.clearkey(orp:record_number_key)
                              orp:record_number   = wpr:order_number
                              If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                                  orp:part_type = 'JOB'
                                  access:ordparts.update()
                              End!If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                          End!If wpr:order_number <> ''
                          get(parts,0)
                          if access:parts.primerecord() = Level:Benign
                              record_number$  = par:record_number
                              par:record      :=: wpr:record
                              par:record_number   = record_number$
                              if access:parts.insert()
                                  access:parts.cancelautoinc()
                              End!if access:parts.insert()
                          End!if access:parts.primerecord() = Level:Benign
                          Delete(warparts)
                          count# += 1
                      end !loop
                      access:warparts.restorefile(save_wpr_id)
                      setcursor()
                      if count# <> 0
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              aud:type          = 'JOB'
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'WARRANTY PARTS TRANSFERRED TO CHARGEABLE'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
      
                          If job:chargeable_job <> 'YES'
                              Case MessageEx('This job has not been marked as Chargeable.<13,10><13,10>Do you wish to make this a Split Job, or make it a Chargeable Only Job?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Make Split Job|&Make Chargeable',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Make Split Job Button
                                      job:chargeable_job = 'YES'
                                  Of 2 ! &Make Chargeable Button
                                      job:warranty_job = 'NO'
                                      job:chargeable_job = 'YES'
                              End!Case MessageEx
                          Else!If job:chargeable_job <> 'YES'
                              Case MessageEx('Transfer Complete.','ServiceBase 2000',|
                                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          End!If job:chargeable_job <> 'YES'
      
                      End!if count# <> 0
                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                      mod:Model_Number    = job:Model_NUmber
                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Found
                          job:EDI = PendingJob(mod:Manufacturer)
                          job:Manufacturer    = mod:Manufacturer
                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                      Access:JOBS.Update()
                  Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                  
              Of 2 ! &No Button
          End!Case MessageEx
      End!If SecurityCheck('JOBS - TRANSFER PARTS')
      BRW6.ResetSort(1)
      BRW7.ResetSort(1)
      BRW8.ResetSort(1)
    OF ?Submit_For_Invoice
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case MessageEx('Are you sure you want to submit this job for Invoice?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              do_audit# = 0
              Set(defaults)
              access:defaults.next()
              remove# = 0
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = brw6.q.job:ref_number
              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:chargeable_job <> 'YES'
                      !If this is not a chargeable job. Error.
                      Case MessageEx('The selected job is not marked as Chargeable.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If job:chargeable_job <> 'YES'
                      tmp:SecondEntryReason   = ConfirmSecondEntry()
      
                      If tmp:SecondEntryReason
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobse:RefNumber = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
                              jobe:CConfirmSecondEntry    = tmp:SecondEntryReason
                              Access:JOBSE.Update()
                          Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                          Access:REPAIRTY.ClearKey(rep:Model_Number_Key)
                          rep:Model_Number = job:Model_Number
                          rep:Repair_Type  = GETINI('BOUNCER','ChargeableRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
                          If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Found
                              job:Repair_Type = GETINI('BOUNCER','ChargeableRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
                              If job:Ignore_Chargeable_Charges <> 'YES'
                                  Pricing_Routine('C',LabourCost",PartsCost",Pass",x")
                                  If Pass"
                                      job:Labour_Cost = Labour_Cost"
                                      job:Parts_Cost = Parts_Cost"
                                      job:Sub_Total = Labour_Cost" + Parts_Cost"
                                  End !If Pass"
                              End !If job:Ignore_Warranty_Charges <> 'YES'
                          Else!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
      
      
                          If job:warranty_job = 'YES'
                              !If this is a split warranty
                              If job:bouncer_type = 'BOT' or job:Bouncer_Type = 'WAR'
                                  !If BOT, then mark as warranty bouncer
                                  job:bouncer_type = 'WAR'
                                  access:jobs.update()
                                  do_audit# = 1
                              Else!If job:bouncer_type = 'BOT'
                                  !Otherwise remove from browse
                                  remove# = 1
                              End!If job:bouncer_type = 'BOT'
                          Else!If job:warranty_job = 'YES'
                              !Remove from browse
                              remove# = 1
                          End!If job:warranty_job = 'YES'
      
                      End !If tmp:SecondEntryReason
                  End!If job:chargeable_job <> 'YES'
      
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = ''
                      job:bouncer_type = ''
                      If def:qa_required = 'YES'
                          If JOB:QA_Passed = 'YES'
                              GetStatus(610,1,'JOB')
                          Else!If JOB:QA_Passed = 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                                  GetStatus(615,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                                  GetStatus(610,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                              If job:qa_rejected <> 'YES'
                                  GetStatus(605,1,'JOB')
                              End!If job:qa_rejected <> 'YES'
                          End!If JOB:QA_Passed = 'YES'
                      else!If def:qa_required = 'YES'
                          If ToBeExchanged()
                              !GetStatus(707,1,'JOB')    !Amended by Neil 13/07/2001
                          Else!If ToBeExchanged()
                              !GetStatus(705,1,'JOB')    !Amended by Neil 13/07/2001
                          End!If ToBeExchanged()
                      End!If def:qa_required = 'YES'
                      access:jobs.update()
                      do_audit# = 1
      
                      ! Start Change 3416 BE(16/10/03)
                      brw6.ScrollDownOne()
                      brw6.UpdateBuffer()
                      ! End Change 3416 BE(16/10/03)
      
                  End!If remove# = 1
      
                  If do_audit# = 1
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:Notes         = 'CONFIRMED SECOND ENTRY: '
                          Case tmp:SecondEntryReason
                              Of 'ENG'
                                  aud:Notes = Clip(aud:Notes) & ' ENGINEER'
                              Of 'PAR'
                                  aud:Notes = Clip(aud:Notes) & ' PARTS'
                              Of 'MAN'
                                  aud:Notes = Clip(aud:Notes) & ' MANUFACTURER'
                              Of 'UNR'
                                  aud:Notes = Clip(aud:Notes) & ' UNRELATED'
                          End !Case tmp:SecondEntryReason
      
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'BOUNCER SUBMITTED FOR INVOICING'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                  End!If do_audit# = 1
              End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
          
              ! Start Change 3528 BE(07/11/03)
              !brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
              ! Start Change 3416 BE(17/10/03)
              brw6.RetainRow = 1
              ! End Change 3416 BE(17/10/03)
              brw6.resetsort(1)
              ! Start Change 3528 BE(07/11/03)
              brw1.updatebuffer()
              brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
          Of 2 ! &No Button
      End!Case MessageEx
    OF ?Reject_From_Invoice
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case MessageEx('Are you sure you want to REJECT this job from Invoicing?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              do_audit# = 0
              Set(Defaults)
              access:defaults.next()
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = brw6.q.job:ref_number
              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:chargeable_job <> 'YES'
                      Case MessageEx('The selected job is not marked as Chargeable.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If job:chargeable_job <> 'YES'
                      If job:warranty_job = 'YES'
                          If job:bouncer_type = 'BOT'
                              job:bouncer_type = 'WAR'
                              access:jobs.update()
                              do_audit# = 1
                          Else!If job:bouncer_type = 'BOT'
                              remove# = 1
                          End!If job:bouncer_type = 'BOT'
                      Else!If job:warranty_job = 'YES'
                          remove# = 1
                      End!If job:warranty_job = 'YES'
                  End!If job:chargeable_job <> 'YES'
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = 'R'
                      job:bouncer_type = ''
                      access:jobs.update()
                      do_audit# = 1
      
                      ! Start Change 3416 BE(16/10/03)
                      brw6.ScrollDownOne()
                      brw6.UpdateBuffer()
                      ! End Change 3416 BE(16/10/03)
      
                  End!If remove# = 1
                  If do_audit# = 1
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'BOUNCER F.O.C. NO PAYMENT REQUIRED'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                  End!If do_audit# = 1
              End!ccess:jobs.fetch(job:ref_number_key) = Level:Benign
      
              ! Start Change 3528 BE(07/11/03)
              !brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
              ! Start Change 3416 BE(16/10/03)
              brw6.RetainRow = 1
              ! End Change 3416 BE(16/10/03)
              brw6.resetsort(1)
              ! Start Change 3528 BE(07/11/03)
              brw1.updatebuffer()
              brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
          Of 2 ! &No Button
      End!Case MessageEx
    OF ?View_Historic_Job
      ThisWindow.Update
      brw6.updatebuffer()
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = job_ali:ref_number
      access:jobs.fetch(job:ref_number_key)
      Globalrequest   = changerecord
      updatejobs
      
      brw1.resetsort(1)
      brw6.resetsort(1)
    OF ?Compare_Fault_Codes
      ThisWindow.Update
      Compare_Fault_Codes(job:ref_number,job_ali:ref_number)
      ThisWindow.Reset
    OF ?Submit_For_Invoice:2
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case MessageEx('Are you sure you want to submit this job for Claiming?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Set(defaults)
              access:defaults.next()
              do_audit# = 0
              Remove# = 0
      
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = brw6.q.job:ref_number
      
              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:warranty_job <> 'YES'
                      Case MessageEx('The selected job is not marked as Warranty.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If job:warranty_job <> 'YES'
                      tmp:SecondEntryReason = ConfirmSecondEntry()
      
                      If tmp:SecondEntryReason <> ''
                          Access:JOBSE.Clearkey(jobe:RefNumberKey)
                          jobse:RefNumber = job:Ref_Number
                          If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Found
                              jobe:WConfirmSecondEntry    = tmp:SecondEntryReason
                              Access:JOBSE.Update()
                          Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                          Access:REPAIRTY.ClearKey(rep:Model_Number_Key)
                          rep:Model_Number = job:Model_Number
                          rep:Repair_Type  = GETINI('BOUNCER','WarrantyRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
                          If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Found
                              job:Repair_Type_Warranty = GETINI('BOUNCER','WarrantyRepairType',,CLIP(PATH())&'\SB2KDEF.INI')
                              If job:Ignore_Warranty_Charges <> 'YES'
                                  Pricing_Routine('W',LabourCost",PartsCost",Pass",x")
                                  If Pass"
                                      job:Labour_Cost_Warranty = Labour_Cost"
                                      job:Parts_Cost_Warranty = Parts_Cost"
                                      job:Sub_Total_Warranty = Labour_Cost" + Parts_Cost"
                                  End !If Pass"
                              End !If job:Ignore_Warranty_Charges <> 'YES'
                          Else!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:REPAIRTY.TryFetch(rep:Model_Number_Key) = Level:Benign
                          
                          If job:chargeable_job = 'YES'
                              If job:bouncer_type = 'BOT' or job:Bouncer_Type = 'CHA'
                                  job:bouncer_type = 'CHA'
                                  Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                                  mod:Model_Number  = job:Model_Number
                                  If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                    !Found
                                      job:EDI   = PendingJob(mod:Manufacturer)
                                      job:Manufacturer  = mod:Manufacturer
                                  Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
                                  access:jobs.update()
                                  do_audit# = 1
                              Else!If job:bouncer_type = 'BOT'
                                  remove# = 1
                              End!If job:bouncer_type = 'BOT'
                          Else!If job:chargeable_job = 'YES'
                              remove# = 1
                          End!If job:chargeable_job = 'YES'
      
                      End !If tmp:SecondEntryReason <> ''
      
                  End!If job:warranty_job <> 'YES'
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = ''
                      job:bouncer_type = ''
                      If job:edi = 'YEB'
                          job:edi = 'NO'
                      End!If job:edi = 'YEB'
                      If def:qa_required = 'YES'
                          If JOB:QA_Passed = 'YES'
                              GetStatus(610,1,'JOB')
                          Else!If JOB:QA_Passed = 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                                  GetStatus(615,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed <> 'YES'
                              If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                                  GetStatus(610,1,'JOB')
                              End!If job:qa_rejected = 'YES' and job:qa_second_passed = 'YES'
                              If job:qa_rejected <> 'YES'
                                  GetStatus(605,1,'JOB')
                              End!If job:qa_rejected <> 'YES'
                          End!If JOB:QA_Passed = 'YES'
                      else!If def:qa_required = 'YES'
                          If ToBeExchanged()
                              !GetStatus(707,1,'JOB')   !Amended by Neil 13/07/2001
                          Else!If ToBeExchanged()
                              !GetStatus(705,1,'JOB')   !Amended by Neil 13/07/2001
                          End!If ToBeExchanged()
                      End!If def:qa_required = 'YES'
      
                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                      mod:Model_Number  = job:Model_Number
                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Found
                          job:EDI   = PendingJob(mod:Manufacturer)
                          job:Manufacturer  = mod:Manufacturer
                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
                      SolaceViewVars(job:EDI, ,, 6)
      
                      access:jobs.update()
                      do_audit# = 1
      
                      ! Start Change 3416 BE(16/10/03)
                      brw6.ScrollDownOne()
                      brw6.UpdateBuffer()
                      ! End Change 3416 BE(16/10/03)
      
                  End!Case job:bouncer_type
                  If do_audit# = 1
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:Notes         = 'CONFIRMED SECOND ENTRY: '
                          Case tmp:SecondEntryReason
                              Of 'ENG'
                                  aud:Notes = Clip(aud:Notes) & ' ENGINEER'
                              Of 'PAR'
                                  aud:Notes = Clip(aud:Notes) & ' PARTS'
                              Of 'MAN'
                                  aud:Notes = Clip(aud:Notes) & ' MANUFACTURER'
                              Of 'UNR'
                                  aud:Notes = Clip(aud:Notes) & ' UNRELATED'
                          End !Case tmp:SecondEntryReason
      
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'BOUNCER SUBMITTED FOR CLAIMING'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                  End!If do_audit# = 1
              End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      
              ! Start Change 3528 BE(07/11/03)
              !brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
              ! Start Change 3416 BE(16/10/03)
              brw6.RetainRow = 1
              ! End Change 3416 BE(16/10/03)
              brw6.resetsort(1)
              ! Start Change 3528 BE(07/11/03)
              brw1.updatebuffer()
              brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
          Of 2 ! &No Button
      End!Case MessageEx
    OF ?Reject_From_Invoice:2
      ThisWindow.Update
      brw1.updatebuffer()
      brw6.updatebuffer()
      Case MessageEx('Are you sure you want to REJECT this job from Claiming?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              do_audit# = 0
              Set(DEFAULTS)
              Access:DEFAULTS.next()
              Access:JOBS.ClearKey(job:Ref_Number_Key)
              job:Ref_Number  = brw6.q.job:Ref_Number
              If access:jobs.fetch(job:ref_number_key) = level:benign
                  If job:warranty_job <> 'YES'
                      Case MessageEx('The selected job is not marked as Warranty.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If job:warranty_job <> 'YES'
                      If job:Chargeable_Job = 'YES'
                          If job:Bouncer_Type = 'BOT' or job:Bouncer_Type = 'CHA'
                              job:Bouncer_Type = 'CHA'
                              Access:JOBS.Update()
                              do_audit# = 1
                          Else!If job:bouncer_type = 'BOT'
                              remove# = 1
                          End!If job:bouncer_type = 'BOT'
                      Else!If job:chargeable_job = 'YES'
                          remove# = 1
                      End!If job:chargeable_job = 'YES'
                  End!If job:warranty_job <> 'YES'
                  If remove# = 1
                      setcursor(cursor:wait)
                      save_bou_id = access:bouncer.savefile()
                      access:bouncer.clearkey(bou:bouncer_job_number_key)
                      bou:original_ref_number = job:ref_number
                      set(bou:bouncer_job_number_key,bou:bouncer_job_number_key)
                      loop
                          if access:bouncer.next()
                             break
                          end !if
                          if bou:original_ref_number <> job:ref_number      |
                              then break.  ! end if
                          yldcnt# += 1
                          if yldcnt# > 25
                             yield() ; yldcnt# = 0
                          end !if
                          Delete(bouncer)
                      end !loop
                      access:bouncer.restorefile(save_bou_id)
                      setcursor()
                      job:bouncer = 'R'
                      job:bouncer_type = ''
                      Access:MODELNUM.Clearkey(mod:Model_Number_Key)
                      mod:Model_Number  = job:Model_Number
                      If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Found
                          job:EDI   = PendingJob(mod:Manufacturer)
                          job:Manufacturer  = mod:Manufacturer
                      Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
      
                      access:jobs.update()
                      do_audit# = 1
      
                      ! Start Change 3416 BE(16/10/03)
                      brw6.ScrollDownOne()
                      brw6.UpdateBuffer()
                      ! End Change 3416 BE(16/10/03)
              
                      If do_audit# = 1
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'BOUNCER F.O.C. NO PAYMENT REQUIRED'
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
                          
                      End!If do_audit# = 1
                  End!If remove# = 1
              End!If access:jobs.fetch(job:ref_number_key) = level:benign
      
              ! Start Change 3528 BE(07/11/03)
              !brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
              ! Start Change 3416 BE(16/10/03)
              brw6.RetainRow = 1
              ! End Change 3416 BE(16/10/03)
              brw6.resetsort(1)
              ! Start Change 3528 BE(07/11/03)
              brw1.updatebuffer()
              brw1.resetsort(1)
              ! End Change 3528 BE(07/11/03)
          Of 2 ! &No Button
      End!Case MessageEx
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Bouncers')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F3Key
              Post(Event:Accepted,?Change)
          Of F4Key
              Post(Event:Accepted,?View_Historic_Job)
      End !KeyCode()
    OF EVENT:OpenWindow
          0{prop:buffer} = 1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection
  IF job_ali:Chargeable_Job <> 'YES'
      ?job_ali:Charge_Type{Prop:Hide} = TRUE
      ?job_ali:Charge_Type:prompt{prop:Hide} = 1
      ?job_ali:Repair_Type{Prop:Hide} = TRUE
      ?job_ali:Repair_Type:prompt{prop:Hide} = 1
  ELSE
      ?job_ali:Charge_Type{Prop:Hide} = FALSE
      ?job_ali:Charge_Type:Prompt{prop:Hide} = 0
      ?job_ali:Repair_Type{Prop:Hide} = FALSE
      ?job_ali:Repair_Type{prop:Hide} = 0
  END
  
  IF job_ali:Warranty_Job <> 'YES'
      ?job_ali:Warranty_Charge_Type{Prop:Hide} = TRUE
      ?job_ali:Warranty_Charge_Type:Prompt{prop:Hide} = 1
      ?job_ali:Repair_Type_Warranty{Prop:Hide} = TRUE
      ?job_ali:Repair_Type_Warranty:Prompt{prop:Hide} = 1
  ELSE
      ?job_ali:Warranty_Charge_Type{Prop:Hide} = FALSE
      ?job_ali:Warranty_Charge_Type:Prompt{prop:Hide} = 0
      ?job_ali:Repair_Type_Warranty{Prop:Hide} = FALSE
      ?job_ali:Repair_Type_Warranty:Prompt{prop:Hide} = 0
  END
  
  If job_ali:Engineer = ''
      ?tmp:CurrentEngineer2{prop:Hide} = 1
      ?tmp:CurrentEngineer2:Prompt{prop:Hide} = 1
  Else !job:Engineer <> ''
      ?tmp:CurrentEngineer2{prop:Hide} = 0
      ?tmp:CurrentEngineer2:Prompt{prop:Hide} = 0
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code    = job_ali:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:CurrentEngineer2 = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
  End !job:Engineer <> ''
  
  If job_ali:Third_Party_Site <> ''
      ?job_ali:Third_Party_Site{prop:Hide} = 0
      ?job_ali:Third_Party_Site:Prompt{prop:Hide} = 0
  Else !job:Third_Party_Site <> ''
      ?job_ali:Third_Party_Site{prop:Hide} = 1
      ?job_ali:Third_Party_Site:Prompt{prop:Hide} = 1
  End !job:Third_Party_Site <> ''
  
  If job_ali:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit2{prop:Hide} = 0
      ?tmp:ExchangeUnit2:Prompt{prop:Hide} = 0
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number  = job_ali:Exchange_Unit_Number
      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN) & '  -  ' & Clip(xch:MSN)
          Else !If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN)
          End !If xch:MSN <> ''
          
      Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          tmp:ExchangeUnit2 = 'Cannot find unit!'
      End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      
  Else !job:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit2{prop:Hide} = 1
      ?tmp:ExchangeUnit2:Prompt{prop:Hide} = 1
      tmp:ExchangeUnit2 = ''
  End !job:Exchange_Unit_Number <> 0
  
  tmp:HistoricUnitDetails = Clip(job_ali:Model_Number) & ' ' & Clip(job_ali:Manufacturer)


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW6.SetQueueRecord PROCEDURE

  CODE
  alias_locator_temp = job:Ref_Number
  PARENT.SetQueueRecord


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection
  IF job:Chargeable_Job <> 'YES'
      ?job:Charge_Type{Prop:Hide} = TRUE
      ?job:Charge_Type:Prompt{prop:Hide} = 1
      ?job:Repair_Type{Prop:Hide} = TRUE
      ?job:Repair_Type:Prompt{prop:Hide} = 1
      ?TransferCharToWarr{prop:Disable} = 1
  
  ELSE
      ?job:Charge_Type{Prop:Hide} = FALSE
      ?job:Charge_Type:Prompt{prop:Hide} = 0
      ?job:Repair_Type{Prop:Hide} = FALSE
      ?job:Repair_Type:Prompt{prop:Hide} = 0
      ?TransferCharToWarr{prop:Disable} = 0
  END
  
  IF job:Warranty_Job <> 'YES'
      ?job:Warranty_Charge_Type{Prop:Hide} = TRUE
      ?job:Repair_Type_Warranty{Prop:Hide} = TRUE
      ?job:Warranty_Charge_Type:Prompt{Prop:Hide} = TRUE
      ?job:Repair_Type_Warranty:Prompt{Prop:Hide} = TRUE
      ?TransferWarrToChar{prop:Disable} = 1
  
  ELSE
      ?job:Warranty_Charge_Type{Prop:Hide} = FALSE
      ?job:Repair_Type_Warranty{Prop:Hide} = FALSE
      ?job:Warranty_Charge_Type:Prompt{Prop:Hide} = FALSE
      ?job:Repair_Type_Warranty:Prompt{Prop:Hide} = FALSE
      ?TransferWarrToChar{prop:Disable} = 0
  END
  
  If job:Engineer = ''
      ?tmp:CurrentEngineer{prop:Hide} = 1
      ?tmp:CurrentEngineer:Prompt{prop:Hide} = 1
  Else !job:Engineer <> ''
      ?tmp:CurrentEngineer{prop:Hide} = 0
      ?tmp:CurrentEngineer:prompt{prop:Hide} = 0
      Access:USERS.Clearkey(use:User_Code_Key)
      use:User_Code    = job:Engineer
      If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Found
          tmp:CurrentEngineer = Clip(use:Forename) & ' ' & Clip(use:Surname)
      Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
      
  End !job:Engineer <> ''
  
  If job:Third_Party_Site <> ''
      ?job:Third_Party_Site{prop:Hide} = 0
      ?job:Third_Party_Site:Prompt{prop:Hide} = 0
  Else !job:Third_Party_Site <> ''
      ?job:Third_Party_Site{prop:Hide} = 1
      ?job:Third_Party_Site:Prompt{prop:Hide} = 1
  End !job:Third_Party_Site <> ''
  
  If job:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit{prop:Hide} = 0
      ?tmp:ExchangeUnit:Prompt{prop:Hide} = 0
      Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
      xch:Ref_Number  = job:Exchange_Unit_Number
      If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Found
          If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN) & '  -  ' & Clip(xch:MSN)
          Else !If xch:MSN <> ''
              tmp:ExchangeUnit = Clip(xch:Model_Number) & '  -  ' & Clip(xch:ESN)
          End !If xch:MSN <> ''
          
      Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          tmp:ExchangeUnit = 'Cannot find unit!'
      End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
      
  Else !job:Exchange_Unit_Number <> 0
      ?tmp:ExchangeUnit{prop:Hide} = 1
      ?tmp:ExchangeUnit:Prompt{prop:Hide} = 1
      tmp:ExchangeUnit = ''
  End !job:Exchange_Unit_Number <> 0
  
  tmp:CurrentUnitDetails  = Clip(job:Model_Number) & ' ' & Clip(job:Manufacturer)
  

BRW6.ScrollDownOne PROCEDURE()

  CODE
  PARENT.ScrollOne(Event:ScrollDown)
  ! Start Change 3528 BE(07/11/03)
  brw1.updatebuffer()
  brw1.resetsort(1)
  ! End Change 3528 BE(07/11/03)



BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

