

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01006.INC'),ONCE        !Local module procedure declarations
                     END


Delivery_Text PROCEDURE                               !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Delivery Text'),AT(,,232,144),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,108),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           TEXT,AT(8,8,216,64),USE(jbn:Delivery_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Contact Name'),AT(8,80),USE(?JBN:DelContactName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(100,80,124,10),USE(jbn:DelContactName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Name'),TIP('Contact Name'),UPR
                           PROMPT('Department'),AT(8,96),USE(?JBN:DelDepartment:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(100,96,124,10),USE(jbn:DelDepartment),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Department'),TIP('Department'),UPR
                         END
                       END
                       PANEL,AT(4,116,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,120,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(168,120,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?jbn:Delivery_Text{prop:ReadOnly} = True
        ?jbn:Delivery_Text{prop:FontColor} = 65793
        ?jbn:Delivery_Text{prop:Color} = 15066597
    Elsif ?jbn:Delivery_Text{prop:Req} = True
        ?jbn:Delivery_Text{prop:FontColor} = 65793
        ?jbn:Delivery_Text{prop:Color} = 8454143
    Else ! If ?jbn:Delivery_Text{prop:Req} = True
        ?jbn:Delivery_Text{prop:FontColor} = 65793
        ?jbn:Delivery_Text{prop:Color} = 16777215
    End ! If ?jbn:Delivery_Text{prop:Req} = True
    ?jbn:Delivery_Text{prop:Trn} = 0
    ?jbn:Delivery_Text{prop:FontStyle} = font:Bold
    ?JBN:DelContactName:Prompt{prop:FontColor} = -1
    ?JBN:DelContactName:Prompt{prop:Color} = 15066597
    If ?jbn:DelContactName{prop:ReadOnly} = True
        ?jbn:DelContactName{prop:FontColor} = 65793
        ?jbn:DelContactName{prop:Color} = 15066597
    Elsif ?jbn:DelContactName{prop:Req} = True
        ?jbn:DelContactName{prop:FontColor} = 65793
        ?jbn:DelContactName{prop:Color} = 8454143
    Else ! If ?jbn:DelContactName{prop:Req} = True
        ?jbn:DelContactName{prop:FontColor} = 65793
        ?jbn:DelContactName{prop:Color} = 16777215
    End ! If ?jbn:DelContactName{prop:Req} = True
    ?jbn:DelContactName{prop:Trn} = 0
    ?jbn:DelContactName{prop:FontStyle} = font:Bold
    ?JBN:DelDepartment:Prompt{prop:FontColor} = -1
    ?JBN:DelDepartment:Prompt{prop:Color} = 15066597
    If ?jbn:DelDepartment{prop:ReadOnly} = True
        ?jbn:DelDepartment{prop:FontColor} = 65793
        ?jbn:DelDepartment{prop:Color} = 15066597
    Elsif ?jbn:DelDepartment{prop:Req} = True
        ?jbn:DelDepartment{prop:FontColor} = 65793
        ?jbn:DelDepartment{prop:Color} = 8454143
    Else ! If ?jbn:DelDepartment{prop:Req} = True
        ?jbn:DelDepartment{prop:FontColor} = 65793
        ?jbn:DelDepartment{prop:Color} = 16777215
    End ! If ?jbn:DelDepartment{prop:Req} = True
    ?jbn:DelDepartment{prop:Trn} = 0
    ?jbn:DelDepartment{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Delivery_Text',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Delivery_Text',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Delivery_Text',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Delivery_Text;  SolaceCtrlName = '?jbn:Delivery_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JBN:DelContactName:Prompt;  SolaceCtrlName = '?JBN:DelContactName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:DelContactName;  SolaceCtrlName = '?jbn:DelContactName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JBN:DelDepartment:Prompt;  SolaceCtrlName = '?JBN:DelDepartment:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:DelDepartment;  SolaceCtrlName = '?jbn:DelDepartment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Changing Delivery Text'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Delivery_Text')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Delivery_Text')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jbn:Delivery_Text
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Delivery_Text',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Delivery_Text')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

