

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01019.INC'),ONCE        !Local module procedure declarations
                     END


Update_Warranty_Part PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
Result               BYTE
Msg1                 STRING(256)
Msg2                 STRING(256)
tmp:exclude          STRING(3)
main_part_temp       STRING(3)
fault_codes_required_temp STRING('NO {1}')
Part_Details_Group   GROUP,PRE(TMP)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
                     END
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
tmp:location         STRING(30)
tmp:shelflocation    STRING(30)
tmp:secondlocation   STRING(30)
tmp:CreateNewOrder   BYTE(0)
tmp:StockNumber      LONG
tmp:PendingPart      LONG
tmp:NewQuantity      LONG
tmp:CurrentState     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp_RF_Board_IMEI    STRING(20)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?wpr:Supplier
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
History::wpr:Record  LIKE(wpr:RECORD),STATIC
QuickWindow          WINDOW('Update the WARPARTS File'),AT(,,535,264),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Warranty_Part'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,280,228),USE(?CurrentTab),WIZARD,SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Warranty Part'),AT(8,8),USE(?title),FONT('Tahoma',10,COLOR:Navy,FONT:bold)
                           PROMPT('Part Number'),AT(8,24),USE(?WPR:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,24,124,10),USE(wpr:Part_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('Browse Stock'),AT(212,24,68,10),USE(?browse_stock_button),SKIP,LEFT,ICON('book.ico')
                           PROMPT('Description'),AT(8,36),USE(?WPR:Description:Prompt),TRN
                           PROMPT('Despatch Note No'),AT(8,52),USE(?WPR:Despatch_Note_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(wpr:Description),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           ENTRY(@s30),AT(84,52,124,10),USE(wpr:Despatch_Note_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           GROUP,AT(4,92,224,140),USE(?adjustmentgroup)
                             PROMPT('Purchase Cost'),AT(8,100),USE(?WPR:Purchase_Cost:Prompt),TRN
                             ENTRY(@n14.2),AT(84,100,64,10),USE(wpr:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Sale Cost'),AT(8,112),USE(?WPR:Sale_Cost:Prompt),TRN
                             ENTRY(@n14.2),AT(84,112,64,10),USE(wpr:Sale_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                             ENTRY(@s30),AT(148,80,60,10),USE(tmp:secondlocation),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                             PROMPT('Supplier'),AT(8,124),USE(?Prompt9),TRN
                             COMBO(@s30),AT(84,124,124,10),USE(wpr:Supplier),VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                             PROMPT('Quantity'),AT(8,136),USE(?Prompt7),TRN
                             SPIN(@p<<<<<<<#p),AT(84,136,64,10),USE(wpr:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,RANGE(1,99999999),STEP(1)
                             STRING('RF Board IMEI'),AT(8,148),USE(?RF_BOARD_PROMPT),TRN,HIDE
                             ENTRY(@s20),AT(84,148,124,10),USE(tmp_RF_Board_IMEI),HIDE,REQ
                             CHECK('Exclude From Ordering / Decrementing'),AT(84,160,140,8),USE(wpr:Exclude_From_Order),TRN,VALUE('YES','NO')
                             CHECK('Main Part'),AT(84,172),USE(wpr:Main_Part),TRN,HIDE,VALUE('YES','NO')
                             PROMPT('Part Status'),AT(8,180),USE(?Prompt24),TRN,FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                             PROMPT('Date Ordered'),AT(8,192),USE(?WPR:Date_Ordered:Prompt),TRN
                             ENTRY(@d6b),AT(84,192,64,10),USE(wpr:Date_Ordered),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Order Number'),AT(8,204),USE(?WPR:Order_Number:Prompt)
                             ENTRY(@n012b),AT(84,204,64,10),USE(wpr:Order_Number),SKIP,FONT(,,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),READONLY,MSG('Order Number')
                             PROMPT('Date Received'),AT(8,216),USE(?WPR:Date_Received:Prompt),TRN
                             ENTRY(@d6b),AT(84,216,64,10),USE(wpr:Date_Received),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           END
                           CHECK('Fault Codes Checked'),AT(392,20),USE(wpr:Fault_Codes_Checked),HIDE,VALUE('YES','NO')
                           PROMPT('Fault Code 1:'),AT(296,36),USE(?wpr:Fault_Code1:Prompt),HIDE
                           ENTRY(@d6b),AT(340,36,124,10),USE(wpr:Fault_Code1,,?WPR:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,36,124,10),USE(wpr:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,36,10,10),USE(?Button4),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,36,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 2:'),AT(296,52),USE(?wpr:Fault_Code2:Prompt),HIDE
                           ENTRY(@d6b),AT(340,52,124,10),USE(wpr:Fault_Code2,,?WPR:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,52,124,10),USE(wpr:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,52,10,10),USE(?Button4:2),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,52,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 3:'),AT(296,68),USE(?wpr:Fault_Code3:Prompt),HIDE
                           ENTRY(@d6b),AT(340,68,124,10),USE(wpr:Fault_Code3,,?WPR:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,68,124,10),USE(wpr:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,68,10,10),USE(?Button4:3),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,68,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           PROMPT('Second/Shelf Location'),AT(8,80),USE(?tmp:location:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           PROMPT('Location'),AT(8,68),USE(?tmp:location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(tmp:location),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Fault Code 4:'),AT(296,84),USE(?wpr:Fault_Code4:Prompt),HIDE
                           ENTRY(@d6b),AT(340,84,124,10),USE(wpr:Fault_Code4,,?WPR:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,84,124,10),USE(wpr:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,84,10,10),USE(?Button4:4),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,84,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           ENTRY(@s30),AT(84,80,60,10),USE(tmp:shelflocation),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Fault Code 5:'),AT(296,100),USE(?wpr:Fault_Code5:Prompt),HIDE
                           ENTRY(@d6b),AT(340,100,124,10),USE(wpr:Fault_Code5,,?WPR:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,100,124,10),USE(wpr:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,100,10,10),USE(?Button4:5),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,100,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 6:'),AT(296,116),USE(?wpr:Fault_Code6:Prompt),HIDE
                           ENTRY(@d6b),AT(340,116,124,10),USE(wpr:Fault_Code6,,?WPR:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,116,124,10),USE(wpr:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,116,10,10),USE(?Button4:6),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,116,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 7:'),AT(296,132),USE(?WPR:Fault_Code7:Prompt),HIDE
                           ENTRY(@d6b),AT(340,132,124,10),USE(wpr:Fault_Code7,,?WPR:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,132,124,10),USE(wpr:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,132,10,10),USE(?Button4:7),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,132,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 8:'),AT(296,148),USE(?WPR:Fault_Code8:Prompt),HIDE
                           ENTRY(@d6b),AT(340,148,124,10),USE(wpr:Fault_Code8,,?WPR:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,148,124,10),USE(wpr:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,148,10,10),USE(?Button4:8),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,148,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 9:'),AT(296,164),USE(?WPR:Fault_Code9:Prompt),HIDE
                           ENTRY(@d6b),AT(340,164,124,10),USE(wpr:Fault_Code9,,?WPR:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,164,124,10),USE(wpr:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,164,10,10),USE(?Button4:9),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,164,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 10:'),AT(296,180),USE(?WPR:Fault_Code10:Prompt),HIDE
                           ENTRY(@d6b),AT(340,180,124,10),USE(wpr:Fault_Code10,,?WPR:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,180,124,10),USE(wpr:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,180,10,10),USE(?Button4:10),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,180,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 11:'),AT(296,196),USE(?WPR:Fault_Code11:Prompt),HIDE
                           ENTRY(@d6b),AT(340,196,124,10),USE(wpr:Fault_Code11,,?WPR:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,196,124,10),USE(wpr:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,196,10,10),USE(?Button4:11),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,196,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 12:'),AT(296,212),USE(?WPR:Fault_Code12:Prompt),HIDE
                           ENTRY(@d6b),AT(340,212,124,10),USE(wpr:Fault_Code12,,?WPR:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(372,212,124,10),USE(wpr:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(500,212,10,10),USE(?Button4:12),HIDE,ICON('list3.ico')
                           BUTTON,AT(516,212,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                         END
                       END
                       SHEET,AT(288,4,244,228),USE(?Sheet2),SPREAD
                         TAB('Required Fault Codes'),USE(?Tab3)
                           PROMPT('No Fault Codes Required'),AT(356,108),USE(?FaultCodesText),HIDE,FONT(,10,,FONT:bold)
                         END
                       END
                       PANEL,AT(4,236,528,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(416,240,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(472,240,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       STRING(@s40),AT(8,240),USE(GLO:Select11),HIDE
                       STRING(@s40),AT(159,240),USE(GLO:Select12),HIDE
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

save_map_id   ushort,auto
save_war_ali_id     ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?title{prop:FontColor} = -1
    ?title{prop:Color} = 15066597
    ?WPR:Part_Number:Prompt{prop:FontColor} = -1
    ?WPR:Part_Number:Prompt{prop:Color} = 15066597
    If ?wpr:Part_Number{prop:ReadOnly} = True
        ?wpr:Part_Number{prop:FontColor} = 65793
        ?wpr:Part_Number{prop:Color} = 15066597
    Elsif ?wpr:Part_Number{prop:Req} = True
        ?wpr:Part_Number{prop:FontColor} = 65793
        ?wpr:Part_Number{prop:Color} = 8454143
    Else ! If ?wpr:Part_Number{prop:Req} = True
        ?wpr:Part_Number{prop:FontColor} = 65793
        ?wpr:Part_Number{prop:Color} = 16777215
    End ! If ?wpr:Part_Number{prop:Req} = True
    ?wpr:Part_Number{prop:Trn} = 0
    ?wpr:Part_Number{prop:FontStyle} = font:Bold
    ?WPR:Description:Prompt{prop:FontColor} = -1
    ?WPR:Description:Prompt{prop:Color} = 15066597
    ?WPR:Despatch_Note_Number:Prompt{prop:FontColor} = -1
    ?WPR:Despatch_Note_Number:Prompt{prop:Color} = 15066597
    If ?wpr:Description{prop:ReadOnly} = True
        ?wpr:Description{prop:FontColor} = 65793
        ?wpr:Description{prop:Color} = 15066597
    Elsif ?wpr:Description{prop:Req} = True
        ?wpr:Description{prop:FontColor} = 65793
        ?wpr:Description{prop:Color} = 8454143
    Else ! If ?wpr:Description{prop:Req} = True
        ?wpr:Description{prop:FontColor} = 65793
        ?wpr:Description{prop:Color} = 16777215
    End ! If ?wpr:Description{prop:Req} = True
    ?wpr:Description{prop:Trn} = 0
    ?wpr:Description{prop:FontStyle} = font:Bold
    If ?wpr:Despatch_Note_Number{prop:ReadOnly} = True
        ?wpr:Despatch_Note_Number{prop:FontColor} = 65793
        ?wpr:Despatch_Note_Number{prop:Color} = 15066597
    Elsif ?wpr:Despatch_Note_Number{prop:Req} = True
        ?wpr:Despatch_Note_Number{prop:FontColor} = 65793
        ?wpr:Despatch_Note_Number{prop:Color} = 8454143
    Else ! If ?wpr:Despatch_Note_Number{prop:Req} = True
        ?wpr:Despatch_Note_Number{prop:FontColor} = 65793
        ?wpr:Despatch_Note_Number{prop:Color} = 16777215
    End ! If ?wpr:Despatch_Note_Number{prop:Req} = True
    ?wpr:Despatch_Note_Number{prop:Trn} = 0
    ?wpr:Despatch_Note_Number{prop:FontStyle} = font:Bold
    ?adjustmentgroup{prop:Font,3} = -1
    ?adjustmentgroup{prop:Color} = 15066597
    ?adjustmentgroup{prop:Trn} = 0
    ?WPR:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?WPR:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?wpr:Purchase_Cost{prop:ReadOnly} = True
        ?wpr:Purchase_Cost{prop:FontColor} = 65793
        ?wpr:Purchase_Cost{prop:Color} = 15066597
    Elsif ?wpr:Purchase_Cost{prop:Req} = True
        ?wpr:Purchase_Cost{prop:FontColor} = 65793
        ?wpr:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?wpr:Purchase_Cost{prop:Req} = True
        ?wpr:Purchase_Cost{prop:FontColor} = 65793
        ?wpr:Purchase_Cost{prop:Color} = 16777215
    End ! If ?wpr:Purchase_Cost{prop:Req} = True
    ?wpr:Purchase_Cost{prop:Trn} = 0
    ?wpr:Purchase_Cost{prop:FontStyle} = font:Bold
    ?WPR:Sale_Cost:Prompt{prop:FontColor} = -1
    ?WPR:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?wpr:Sale_Cost{prop:ReadOnly} = True
        ?wpr:Sale_Cost{prop:FontColor} = 65793
        ?wpr:Sale_Cost{prop:Color} = 15066597
    Elsif ?wpr:Sale_Cost{prop:Req} = True
        ?wpr:Sale_Cost{prop:FontColor} = 65793
        ?wpr:Sale_Cost{prop:Color} = 8454143
    Else ! If ?wpr:Sale_Cost{prop:Req} = True
        ?wpr:Sale_Cost{prop:FontColor} = 65793
        ?wpr:Sale_Cost{prop:Color} = 16777215
    End ! If ?wpr:Sale_Cost{prop:Req} = True
    ?wpr:Sale_Cost{prop:Trn} = 0
    ?wpr:Sale_Cost{prop:FontStyle} = font:Bold
    If ?tmp:secondlocation{prop:ReadOnly} = True
        ?tmp:secondlocation{prop:FontColor} = 65793
        ?tmp:secondlocation{prop:Color} = 15066597
    Elsif ?tmp:secondlocation{prop:Req} = True
        ?tmp:secondlocation{prop:FontColor} = 65793
        ?tmp:secondlocation{prop:Color} = 8454143
    Else ! If ?tmp:secondlocation{prop:Req} = True
        ?tmp:secondlocation{prop:FontColor} = 65793
        ?tmp:secondlocation{prop:Color} = 16777215
    End ! If ?tmp:secondlocation{prop:Req} = True
    ?tmp:secondlocation{prop:Trn} = 0
    ?tmp:secondlocation{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    If ?wpr:Supplier{prop:ReadOnly} = True
        ?wpr:Supplier{prop:FontColor} = 65793
        ?wpr:Supplier{prop:Color} = 15066597
    Elsif ?wpr:Supplier{prop:Req} = True
        ?wpr:Supplier{prop:FontColor} = 65793
        ?wpr:Supplier{prop:Color} = 8454143
    Else ! If ?wpr:Supplier{prop:Req} = True
        ?wpr:Supplier{prop:FontColor} = 65793
        ?wpr:Supplier{prop:Color} = 16777215
    End ! If ?wpr:Supplier{prop:Req} = True
    ?wpr:Supplier{prop:Trn} = 0
    ?wpr:Supplier{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?wpr:Quantity{prop:ReadOnly} = True
        ?wpr:Quantity{prop:FontColor} = 65793
        ?wpr:Quantity{prop:Color} = 15066597
    Elsif ?wpr:Quantity{prop:Req} = True
        ?wpr:Quantity{prop:FontColor} = 65793
        ?wpr:Quantity{prop:Color} = 8454143
    Else ! If ?wpr:Quantity{prop:Req} = True
        ?wpr:Quantity{prop:FontColor} = 65793
        ?wpr:Quantity{prop:Color} = 16777215
    End ! If ?wpr:Quantity{prop:Req} = True
    ?wpr:Quantity{prop:Trn} = 0
    ?wpr:Quantity{prop:FontStyle} = font:Bold
    ?RF_BOARD_PROMPT{prop:FontColor} = -1
    ?RF_BOARD_PROMPT{prop:Color} = 15066597
    If ?tmp_RF_Board_IMEI{prop:ReadOnly} = True
        ?tmp_RF_Board_IMEI{prop:FontColor} = 65793
        ?tmp_RF_Board_IMEI{prop:Color} = 15066597
    Elsif ?tmp_RF_Board_IMEI{prop:Req} = True
        ?tmp_RF_Board_IMEI{prop:FontColor} = 65793
        ?tmp_RF_Board_IMEI{prop:Color} = 8454143
    Else ! If ?tmp_RF_Board_IMEI{prop:Req} = True
        ?tmp_RF_Board_IMEI{prop:FontColor} = 65793
        ?tmp_RF_Board_IMEI{prop:Color} = 16777215
    End ! If ?tmp_RF_Board_IMEI{prop:Req} = True
    ?tmp_RF_Board_IMEI{prop:Trn} = 0
    ?tmp_RF_Board_IMEI{prop:FontStyle} = font:Bold
    ?wpr:Exclude_From_Order{prop:Font,3} = -1
    ?wpr:Exclude_From_Order{prop:Color} = 15066597
    ?wpr:Exclude_From_Order{prop:Trn} = 0
    ?wpr:Main_Part{prop:Font,3} = -1
    ?wpr:Main_Part{prop:Color} = 15066597
    ?wpr:Main_Part{prop:Trn} = 0
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    ?WPR:Date_Ordered:Prompt{prop:FontColor} = -1
    ?WPR:Date_Ordered:Prompt{prop:Color} = 15066597
    If ?wpr:Date_Ordered{prop:ReadOnly} = True
        ?wpr:Date_Ordered{prop:FontColor} = 65793
        ?wpr:Date_Ordered{prop:Color} = 15066597
    Elsif ?wpr:Date_Ordered{prop:Req} = True
        ?wpr:Date_Ordered{prop:FontColor} = 65793
        ?wpr:Date_Ordered{prop:Color} = 8454143
    Else ! If ?wpr:Date_Ordered{prop:Req} = True
        ?wpr:Date_Ordered{prop:FontColor} = 65793
        ?wpr:Date_Ordered{prop:Color} = 16777215
    End ! If ?wpr:Date_Ordered{prop:Req} = True
    ?wpr:Date_Ordered{prop:Trn} = 0
    ?wpr:Date_Ordered{prop:FontStyle} = font:Bold
    ?WPR:Order_Number:Prompt{prop:FontColor} = -1
    ?WPR:Order_Number:Prompt{prop:Color} = 15066597
    If ?wpr:Order_Number{prop:ReadOnly} = True
        ?wpr:Order_Number{prop:FontColor} = 65793
        ?wpr:Order_Number{prop:Color} = 15066597
    Elsif ?wpr:Order_Number{prop:Req} = True
        ?wpr:Order_Number{prop:FontColor} = 65793
        ?wpr:Order_Number{prop:Color} = 8454143
    Else ! If ?wpr:Order_Number{prop:Req} = True
        ?wpr:Order_Number{prop:FontColor} = 65793
        ?wpr:Order_Number{prop:Color} = 16777215
    End ! If ?wpr:Order_Number{prop:Req} = True
    ?wpr:Order_Number{prop:Trn} = 0
    ?wpr:Order_Number{prop:FontStyle} = font:Bold
    ?WPR:Date_Received:Prompt{prop:FontColor} = -1
    ?WPR:Date_Received:Prompt{prop:Color} = 15066597
    If ?wpr:Date_Received{prop:ReadOnly} = True
        ?wpr:Date_Received{prop:FontColor} = 65793
        ?wpr:Date_Received{prop:Color} = 15066597
    Elsif ?wpr:Date_Received{prop:Req} = True
        ?wpr:Date_Received{prop:FontColor} = 65793
        ?wpr:Date_Received{prop:Color} = 8454143
    Else ! If ?wpr:Date_Received{prop:Req} = True
        ?wpr:Date_Received{prop:FontColor} = 65793
        ?wpr:Date_Received{prop:Color} = 16777215
    End ! If ?wpr:Date_Received{prop:Req} = True
    ?wpr:Date_Received{prop:Trn} = 0
    ?wpr:Date_Received{prop:FontStyle} = font:Bold
    ?wpr:Fault_Codes_Checked{prop:Font,3} = -1
    ?wpr:Fault_Codes_Checked{prop:Color} = 15066597
    ?wpr:Fault_Codes_Checked{prop:Trn} = 0
    ?wpr:Fault_Code1:Prompt{prop:FontColor} = -1
    ?wpr:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code1:2{prop:ReadOnly} = True
        ?WPR:Fault_Code1:2{prop:FontColor} = 65793
        ?WPR:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code1:2{prop:Req} = True
        ?WPR:Fault_Code1:2{prop:FontColor} = 65793
        ?WPR:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code1:2{prop:Req} = True
        ?WPR:Fault_Code1:2{prop:FontColor} = 65793
        ?WPR:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code1:2{prop:Req} = True
    ?WPR:Fault_Code1:2{prop:Trn} = 0
    ?WPR:Fault_Code1:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code1{prop:ReadOnly} = True
        ?wpr:Fault_Code1{prop:FontColor} = 65793
        ?wpr:Fault_Code1{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code1{prop:Req} = True
        ?wpr:Fault_Code1{prop:FontColor} = 65793
        ?wpr:Fault_Code1{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code1{prop:Req} = True
        ?wpr:Fault_Code1{prop:FontColor} = 65793
        ?wpr:Fault_Code1{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code1{prop:Req} = True
    ?wpr:Fault_Code1{prop:Trn} = 0
    ?wpr:Fault_Code1{prop:FontStyle} = font:Bold
    ?wpr:Fault_Code2:Prompt{prop:FontColor} = -1
    ?wpr:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code2:2{prop:ReadOnly} = True
        ?WPR:Fault_Code2:2{prop:FontColor} = 65793
        ?WPR:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code2:2{prop:Req} = True
        ?WPR:Fault_Code2:2{prop:FontColor} = 65793
        ?WPR:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code2:2{prop:Req} = True
        ?WPR:Fault_Code2:2{prop:FontColor} = 65793
        ?WPR:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code2:2{prop:Req} = True
    ?WPR:Fault_Code2:2{prop:Trn} = 0
    ?WPR:Fault_Code2:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code2{prop:ReadOnly} = True
        ?wpr:Fault_Code2{prop:FontColor} = 65793
        ?wpr:Fault_Code2{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code2{prop:Req} = True
        ?wpr:Fault_Code2{prop:FontColor} = 65793
        ?wpr:Fault_Code2{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code2{prop:Req} = True
        ?wpr:Fault_Code2{prop:FontColor} = 65793
        ?wpr:Fault_Code2{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code2{prop:Req} = True
    ?wpr:Fault_Code2{prop:Trn} = 0
    ?wpr:Fault_Code2{prop:FontStyle} = font:Bold
    ?wpr:Fault_Code3:Prompt{prop:FontColor} = -1
    ?wpr:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code3:2{prop:ReadOnly} = True
        ?WPR:Fault_Code3:2{prop:FontColor} = 65793
        ?WPR:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code3:2{prop:Req} = True
        ?WPR:Fault_Code3:2{prop:FontColor} = 65793
        ?WPR:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code3:2{prop:Req} = True
        ?WPR:Fault_Code3:2{prop:FontColor} = 65793
        ?WPR:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code3:2{prop:Req} = True
    ?WPR:Fault_Code3:2{prop:Trn} = 0
    ?WPR:Fault_Code3:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code3{prop:ReadOnly} = True
        ?wpr:Fault_Code3{prop:FontColor} = 65793
        ?wpr:Fault_Code3{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code3{prop:Req} = True
        ?wpr:Fault_Code3{prop:FontColor} = 65793
        ?wpr:Fault_Code3{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code3{prop:Req} = True
        ?wpr:Fault_Code3{prop:FontColor} = 65793
        ?wpr:Fault_Code3{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code3{prop:Req} = True
    ?wpr:Fault_Code3{prop:Trn} = 0
    ?wpr:Fault_Code3{prop:FontStyle} = font:Bold
    ?tmp:location:Prompt:2{prop:FontColor} = -1
    ?tmp:location:Prompt:2{prop:Color} = 15066597
    ?tmp:location:Prompt{prop:FontColor} = -1
    ?tmp:location:Prompt{prop:Color} = 15066597
    If ?tmp:location{prop:ReadOnly} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 15066597
    Elsif ?tmp:location{prop:Req} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 8454143
    Else ! If ?tmp:location{prop:Req} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 16777215
    End ! If ?tmp:location{prop:Req} = True
    ?tmp:location{prop:Trn} = 0
    ?tmp:location{prop:FontStyle} = font:Bold
    ?wpr:Fault_Code4:Prompt{prop:FontColor} = -1
    ?wpr:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code4:2{prop:ReadOnly} = True
        ?WPR:Fault_Code4:2{prop:FontColor} = 65793
        ?WPR:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code4:2{prop:Req} = True
        ?WPR:Fault_Code4:2{prop:FontColor} = 65793
        ?WPR:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code4:2{prop:Req} = True
        ?WPR:Fault_Code4:2{prop:FontColor} = 65793
        ?WPR:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code4:2{prop:Req} = True
    ?WPR:Fault_Code4:2{prop:Trn} = 0
    ?WPR:Fault_Code4:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code4{prop:ReadOnly} = True
        ?wpr:Fault_Code4{prop:FontColor} = 65793
        ?wpr:Fault_Code4{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code4{prop:Req} = True
        ?wpr:Fault_Code4{prop:FontColor} = 65793
        ?wpr:Fault_Code4{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code4{prop:Req} = True
        ?wpr:Fault_Code4{prop:FontColor} = 65793
        ?wpr:Fault_Code4{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code4{prop:Req} = True
    ?wpr:Fault_Code4{prop:Trn} = 0
    ?wpr:Fault_Code4{prop:FontStyle} = font:Bold
    If ?tmp:shelflocation{prop:ReadOnly} = True
        ?tmp:shelflocation{prop:FontColor} = 65793
        ?tmp:shelflocation{prop:Color} = 15066597
    Elsif ?tmp:shelflocation{prop:Req} = True
        ?tmp:shelflocation{prop:FontColor} = 65793
        ?tmp:shelflocation{prop:Color} = 8454143
    Else ! If ?tmp:shelflocation{prop:Req} = True
        ?tmp:shelflocation{prop:FontColor} = 65793
        ?tmp:shelflocation{prop:Color} = 16777215
    End ! If ?tmp:shelflocation{prop:Req} = True
    ?tmp:shelflocation{prop:Trn} = 0
    ?tmp:shelflocation{prop:FontStyle} = font:Bold
    ?wpr:Fault_Code5:Prompt{prop:FontColor} = -1
    ?wpr:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code5:2{prop:ReadOnly} = True
        ?WPR:Fault_Code5:2{prop:FontColor} = 65793
        ?WPR:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code5:2{prop:Req} = True
        ?WPR:Fault_Code5:2{prop:FontColor} = 65793
        ?WPR:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code5:2{prop:Req} = True
        ?WPR:Fault_Code5:2{prop:FontColor} = 65793
        ?WPR:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code5:2{prop:Req} = True
    ?WPR:Fault_Code5:2{prop:Trn} = 0
    ?WPR:Fault_Code5:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code5{prop:ReadOnly} = True
        ?wpr:Fault_Code5{prop:FontColor} = 65793
        ?wpr:Fault_Code5{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code5{prop:Req} = True
        ?wpr:Fault_Code5{prop:FontColor} = 65793
        ?wpr:Fault_Code5{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code5{prop:Req} = True
        ?wpr:Fault_Code5{prop:FontColor} = 65793
        ?wpr:Fault_Code5{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code5{prop:Req} = True
    ?wpr:Fault_Code5{prop:Trn} = 0
    ?wpr:Fault_Code5{prop:FontStyle} = font:Bold
    ?wpr:Fault_Code6:Prompt{prop:FontColor} = -1
    ?wpr:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code6:2{prop:ReadOnly} = True
        ?WPR:Fault_Code6:2{prop:FontColor} = 65793
        ?WPR:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code6:2{prop:Req} = True
        ?WPR:Fault_Code6:2{prop:FontColor} = 65793
        ?WPR:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code6:2{prop:Req} = True
        ?WPR:Fault_Code6:2{prop:FontColor} = 65793
        ?WPR:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code6:2{prop:Req} = True
    ?WPR:Fault_Code6:2{prop:Trn} = 0
    ?WPR:Fault_Code6:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code6{prop:ReadOnly} = True
        ?wpr:Fault_Code6{prop:FontColor} = 65793
        ?wpr:Fault_Code6{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code6{prop:Req} = True
        ?wpr:Fault_Code6{prop:FontColor} = 65793
        ?wpr:Fault_Code6{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code6{prop:Req} = True
        ?wpr:Fault_Code6{prop:FontColor} = 65793
        ?wpr:Fault_Code6{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code6{prop:Req} = True
    ?wpr:Fault_Code6{prop:Trn} = 0
    ?wpr:Fault_Code6{prop:FontStyle} = font:Bold
    ?WPR:Fault_Code7:Prompt{prop:FontColor} = -1
    ?WPR:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code7:2{prop:ReadOnly} = True
        ?WPR:Fault_Code7:2{prop:FontColor} = 65793
        ?WPR:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code7:2{prop:Req} = True
        ?WPR:Fault_Code7:2{prop:FontColor} = 65793
        ?WPR:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code7:2{prop:Req} = True
        ?WPR:Fault_Code7:2{prop:FontColor} = 65793
        ?WPR:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code7:2{prop:Req} = True
    ?WPR:Fault_Code7:2{prop:Trn} = 0
    ?WPR:Fault_Code7:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code7{prop:ReadOnly} = True
        ?wpr:Fault_Code7{prop:FontColor} = 65793
        ?wpr:Fault_Code7{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code7{prop:Req} = True
        ?wpr:Fault_Code7{prop:FontColor} = 65793
        ?wpr:Fault_Code7{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code7{prop:Req} = True
        ?wpr:Fault_Code7{prop:FontColor} = 65793
        ?wpr:Fault_Code7{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code7{prop:Req} = True
    ?wpr:Fault_Code7{prop:Trn} = 0
    ?wpr:Fault_Code7{prop:FontStyle} = font:Bold
    ?WPR:Fault_Code8:Prompt{prop:FontColor} = -1
    ?WPR:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code8:2{prop:ReadOnly} = True
        ?WPR:Fault_Code8:2{prop:FontColor} = 65793
        ?WPR:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code8:2{prop:Req} = True
        ?WPR:Fault_Code8:2{prop:FontColor} = 65793
        ?WPR:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code8:2{prop:Req} = True
        ?WPR:Fault_Code8:2{prop:FontColor} = 65793
        ?WPR:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code8:2{prop:Req} = True
    ?WPR:Fault_Code8:2{prop:Trn} = 0
    ?WPR:Fault_Code8:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code8{prop:ReadOnly} = True
        ?wpr:Fault_Code8{prop:FontColor} = 65793
        ?wpr:Fault_Code8{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code8{prop:Req} = True
        ?wpr:Fault_Code8{prop:FontColor} = 65793
        ?wpr:Fault_Code8{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code8{prop:Req} = True
        ?wpr:Fault_Code8{prop:FontColor} = 65793
        ?wpr:Fault_Code8{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code8{prop:Req} = True
    ?wpr:Fault_Code8{prop:Trn} = 0
    ?wpr:Fault_Code8{prop:FontStyle} = font:Bold
    ?WPR:Fault_Code9:Prompt{prop:FontColor} = -1
    ?WPR:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code9:2{prop:ReadOnly} = True
        ?WPR:Fault_Code9:2{prop:FontColor} = 65793
        ?WPR:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code9:2{prop:Req} = True
        ?WPR:Fault_Code9:2{prop:FontColor} = 65793
        ?WPR:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code9:2{prop:Req} = True
        ?WPR:Fault_Code9:2{prop:FontColor} = 65793
        ?WPR:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code9:2{prop:Req} = True
    ?WPR:Fault_Code9:2{prop:Trn} = 0
    ?WPR:Fault_Code9:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code9{prop:ReadOnly} = True
        ?wpr:Fault_Code9{prop:FontColor} = 65793
        ?wpr:Fault_Code9{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code9{prop:Req} = True
        ?wpr:Fault_Code9{prop:FontColor} = 65793
        ?wpr:Fault_Code9{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code9{prop:Req} = True
        ?wpr:Fault_Code9{prop:FontColor} = 65793
        ?wpr:Fault_Code9{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code9{prop:Req} = True
    ?wpr:Fault_Code9{prop:Trn} = 0
    ?wpr:Fault_Code9{prop:FontStyle} = font:Bold
    ?WPR:Fault_Code10:Prompt{prop:FontColor} = -1
    ?WPR:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code10:2{prop:ReadOnly} = True
        ?WPR:Fault_Code10:2{prop:FontColor} = 65793
        ?WPR:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code10:2{prop:Req} = True
        ?WPR:Fault_Code10:2{prop:FontColor} = 65793
        ?WPR:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code10:2{prop:Req} = True
        ?WPR:Fault_Code10:2{prop:FontColor} = 65793
        ?WPR:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code10:2{prop:Req} = True
    ?WPR:Fault_Code10:2{prop:Trn} = 0
    ?WPR:Fault_Code10:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code10{prop:ReadOnly} = True
        ?wpr:Fault_Code10{prop:FontColor} = 65793
        ?wpr:Fault_Code10{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code10{prop:Req} = True
        ?wpr:Fault_Code10{prop:FontColor} = 65793
        ?wpr:Fault_Code10{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code10{prop:Req} = True
        ?wpr:Fault_Code10{prop:FontColor} = 65793
        ?wpr:Fault_Code10{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code10{prop:Req} = True
    ?wpr:Fault_Code10{prop:Trn} = 0
    ?wpr:Fault_Code10{prop:FontStyle} = font:Bold
    ?WPR:Fault_Code11:Prompt{prop:FontColor} = -1
    ?WPR:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code11:2{prop:ReadOnly} = True
        ?WPR:Fault_Code11:2{prop:FontColor} = 65793
        ?WPR:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code11:2{prop:Req} = True
        ?WPR:Fault_Code11:2{prop:FontColor} = 65793
        ?WPR:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code11:2{prop:Req} = True
        ?WPR:Fault_Code11:2{prop:FontColor} = 65793
        ?WPR:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code11:2{prop:Req} = True
    ?WPR:Fault_Code11:2{prop:Trn} = 0
    ?WPR:Fault_Code11:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code11{prop:ReadOnly} = True
        ?wpr:Fault_Code11{prop:FontColor} = 65793
        ?wpr:Fault_Code11{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code11{prop:Req} = True
        ?wpr:Fault_Code11{prop:FontColor} = 65793
        ?wpr:Fault_Code11{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code11{prop:Req} = True
        ?wpr:Fault_Code11{prop:FontColor} = 65793
        ?wpr:Fault_Code11{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code11{prop:Req} = True
    ?wpr:Fault_Code11{prop:Trn} = 0
    ?wpr:Fault_Code11{prop:FontStyle} = font:Bold
    ?WPR:Fault_Code12:Prompt{prop:FontColor} = -1
    ?WPR:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?WPR:Fault_Code12:2{prop:ReadOnly} = True
        ?WPR:Fault_Code12:2{prop:FontColor} = 65793
        ?WPR:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?WPR:Fault_Code12:2{prop:Req} = True
        ?WPR:Fault_Code12:2{prop:FontColor} = 65793
        ?WPR:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?WPR:Fault_Code12:2{prop:Req} = True
        ?WPR:Fault_Code12:2{prop:FontColor} = 65793
        ?WPR:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?WPR:Fault_Code12:2{prop:Req} = True
    ?WPR:Fault_Code12:2{prop:Trn} = 0
    ?WPR:Fault_Code12:2{prop:FontStyle} = font:Bold
    If ?wpr:Fault_Code12{prop:ReadOnly} = True
        ?wpr:Fault_Code12{prop:FontColor} = 65793
        ?wpr:Fault_Code12{prop:Color} = 15066597
    Elsif ?wpr:Fault_Code12{prop:Req} = True
        ?wpr:Fault_Code12{prop:FontColor} = 65793
        ?wpr:Fault_Code12{prop:Color} = 8454143
    Else ! If ?wpr:Fault_Code12{prop:Req} = True
        ?wpr:Fault_Code12{prop:FontColor} = 65793
        ?wpr:Fault_Code12{prop:Color} = 16777215
    End ! If ?wpr:Fault_Code12{prop:Req} = True
    ?wpr:Fault_Code12{prop:Trn} = 0
    ?wpr:Fault_Code12{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?FaultCodesText{prop:FontColor} = -1
    ?FaultCodesText{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597

    ?GLO:Select11{prop:FontColor} = -1
    ?GLO:Select11{prop:Color} = 15066597
    ?GLO:Select12{prop:FontColor} = -1
    ?GLO:Select12{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
WriteScrapAuditRecord ROUTINE     ! Start 2632 BE(20/05/03)
    IF (Access:AUDIT.PrimeRecord() = Level:Benign) THEN
        ! Start 2632/3 BE(02/06/03)
        !aud:Notes         = 'DESCRIPTION: ' & Clip(wpr:Description)
        aud:Notes         = 'DESCRIPTION: ' & CLIP(wpr:Description)  & '<13,10>' & |
                            'QUANTITY = ' & CLIP(LEFT(FORMAT((wpr:Quantity - Quantity_Temp), @n_8)))
        ! End 2632/3 BE(02/06/03)
        aud:Ref_Number    = job:ref_number
        aud:Date          = Today()
        aud:Time          = Clock()
        aud:Type          = 'JOB'
        Access:USERS.ClearKey(use:Password_Key)
        use:Password      = glo:Password
        Access:USERS.Fetch(use:Password_Key)
        aud:User          = use:User_Code
        aud:Action        = 'SCRAP WARRANTY  PART: ' & Clip(wpr:Part_Number)
        IF (Access:AUDIT.TryInsert() <> Level:Benign) THEN
            Access:AUDIT.CancelAutoInc()
        END
    END
! End 2632 BE(20/05/03)
CreateNewOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign  
        ope:Part_Ref_Number  = tmp:StockNumber
        ope:Job_Number       = job:Ref_Number
        ope:Part_Type        = 'JOB'
        ope:Supplier         = wpr:Supplier
        ope:Part_Number      = wpr:Part_Number
        ope:Description      = wpr:Description
        ope:Quantity         = tmp:NewQuantity
        ope:Account_Number   = job:Account_Number
        ope:PartRecordNumber = wpr:Record_Number
        
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
Lookup_Location     Routine
    If wpr:part_ref_number <> ''
        access:stock.clearkey(sto:ref_number_key)
        sto:ref_number  = wpr:part_ref_number
        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = sto:location
            tmp:shelflocation   = sto:shelf_location
            tmp:secondlocation  = sto:second_location
        Else!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = ''
            tmp:shelflocation   = ''
            tmp:secondlocation  = ''
        End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    Else!If par:part_ref_number <> ''
        tmp:location    = ''
        tmp:shelflocation   = ''
        tmp:secondlocation  = ''
    End!If par:part_ref_number <> ''
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Warranty_Part',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Warranty_Part',1)
    SolaceViewVars('Result',Result,'Update_Warranty_Part',1)
    SolaceViewVars('Msg1',Msg1,'Update_Warranty_Part',1)
    SolaceViewVars('Msg2',Msg2,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:exclude',tmp:exclude,'Update_Warranty_Part',1)
    SolaceViewVars('main_part_temp',main_part_temp,'Update_Warranty_Part',1)
    SolaceViewVars('fault_codes_required_temp',fault_codes_required_temp,'Update_Warranty_Part',1)
    SolaceViewVars('Part_Details_Group:Part_Number',Part_Details_Group:Part_Number,'Update_Warranty_Part',1)
    SolaceViewVars('Part_Details_Group:Description',Part_Details_Group:Description,'Update_Warranty_Part',1)
    SolaceViewVars('Part_Details_Group:Supplier',Part_Details_Group:Supplier,'Update_Warranty_Part',1)
    SolaceViewVars('Part_Details_Group:Purchase_Cost',Part_Details_Group:Purchase_Cost,'Update_Warranty_Part',1)
    SolaceViewVars('Part_Details_Group:Sale_Cost',Part_Details_Group:Sale_Cost,'Update_Warranty_Part',1)
    SolaceViewVars('adjustment_temp',adjustment_temp,'Update_Warranty_Part',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Update_Warranty_Part',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Update_Warranty_Part',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Warranty_Part',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Warranty_Part',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Update_Warranty_Part',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:location',tmp:location,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:shelflocation',tmp:shelflocation,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:secondlocation',tmp:secondlocation,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:CreateNewOrder',tmp:CreateNewOrder,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:StockNumber',tmp:StockNumber,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:PendingPart',tmp:PendingPart,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:NewQuantity',tmp:NewQuantity,'Update_Warranty_Part',1)
    SolaceViewVars('tmp:CurrentState',tmp:CurrentState,'Update_Warranty_Part',1)
    SolaceViewVars('tmp_RF_Board_IMEI',tmp_RF_Board_IMEI,'Update_Warranty_Part',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?title;  SolaceCtrlName = '?title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Part_Number:Prompt;  SolaceCtrlName = '?WPR:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Part_Number;  SolaceCtrlName = '?wpr:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?browse_stock_button;  SolaceCtrlName = '?browse_stock_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Description:Prompt;  SolaceCtrlName = '?WPR:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Despatch_Note_Number:Prompt;  SolaceCtrlName = '?WPR:Despatch_Note_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Description;  SolaceCtrlName = '?wpr:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Despatch_Note_Number;  SolaceCtrlName = '?wpr:Despatch_Note_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?adjustmentgroup;  SolaceCtrlName = '?adjustmentgroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Purchase_Cost:Prompt;  SolaceCtrlName = '?WPR:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Purchase_Cost;  SolaceCtrlName = '?wpr:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Sale_Cost:Prompt;  SolaceCtrlName = '?WPR:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Sale_Cost;  SolaceCtrlName = '?wpr:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:secondlocation;  SolaceCtrlName = '?tmp:secondlocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Supplier;  SolaceCtrlName = '?wpr:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Quantity;  SolaceCtrlName = '?wpr:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RF_BOARD_PROMPT;  SolaceCtrlName = '?RF_BOARD_PROMPT';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp_RF_Board_IMEI;  SolaceCtrlName = '?tmp_RF_Board_IMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Exclude_From_Order;  SolaceCtrlName = '?wpr:Exclude_From_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Main_Part;  SolaceCtrlName = '?wpr:Main_Part';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt24;  SolaceCtrlName = '?Prompt24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Date_Ordered:Prompt;  SolaceCtrlName = '?WPR:Date_Ordered:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Date_Ordered;  SolaceCtrlName = '?wpr:Date_Ordered';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Order_Number:Prompt;  SolaceCtrlName = '?WPR:Order_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Order_Number;  SolaceCtrlName = '?wpr:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Date_Received:Prompt;  SolaceCtrlName = '?WPR:Date_Received:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Date_Received;  SolaceCtrlName = '?wpr:Date_Received';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Codes_Checked;  SolaceCtrlName = '?wpr:Fault_Codes_Checked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code1:Prompt;  SolaceCtrlName = '?wpr:Fault_Code1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code1:2;  SolaceCtrlName = '?WPR:Fault_Code1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code1;  SolaceCtrlName = '?wpr:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code2:Prompt;  SolaceCtrlName = '?wpr:Fault_Code2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code2:2;  SolaceCtrlName = '?WPR:Fault_Code2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code2;  SolaceCtrlName = '?wpr:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:2;  SolaceCtrlName = '?Button4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code3:Prompt;  SolaceCtrlName = '?wpr:Fault_Code3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code3:2;  SolaceCtrlName = '?WPR:Fault_Code3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code3;  SolaceCtrlName = '?wpr:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:3;  SolaceCtrlName = '?Button4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location:Prompt:2;  SolaceCtrlName = '?tmp:location:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location:Prompt;  SolaceCtrlName = '?tmp:location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location;  SolaceCtrlName = '?tmp:location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code4:Prompt;  SolaceCtrlName = '?wpr:Fault_Code4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code4:2;  SolaceCtrlName = '?WPR:Fault_Code4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code4;  SolaceCtrlName = '?wpr:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:4;  SolaceCtrlName = '?Button4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:shelflocation;  SolaceCtrlName = '?tmp:shelflocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code5:Prompt;  SolaceCtrlName = '?wpr:Fault_Code5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code5:2;  SolaceCtrlName = '?WPR:Fault_Code5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code5;  SolaceCtrlName = '?wpr:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:5;  SolaceCtrlName = '?Button4:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code6:Prompt;  SolaceCtrlName = '?wpr:Fault_Code6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code6:2;  SolaceCtrlName = '?WPR:Fault_Code6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code6;  SolaceCtrlName = '?wpr:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:6;  SolaceCtrlName = '?Button4:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code7:Prompt;  SolaceCtrlName = '?WPR:Fault_Code7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code7:2;  SolaceCtrlName = '?WPR:Fault_Code7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code7;  SolaceCtrlName = '?wpr:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:7;  SolaceCtrlName = '?Button4:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code8:Prompt;  SolaceCtrlName = '?WPR:Fault_Code8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code8:2;  SolaceCtrlName = '?WPR:Fault_Code8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code8;  SolaceCtrlName = '?wpr:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:8;  SolaceCtrlName = '?Button4:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code9:Prompt;  SolaceCtrlName = '?WPR:Fault_Code9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code9:2;  SolaceCtrlName = '?WPR:Fault_Code9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code9;  SolaceCtrlName = '?wpr:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:9;  SolaceCtrlName = '?Button4:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code10:Prompt;  SolaceCtrlName = '?WPR:Fault_Code10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code10:2;  SolaceCtrlName = '?WPR:Fault_Code10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code10;  SolaceCtrlName = '?wpr:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:10;  SolaceCtrlName = '?Button4:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code11:Prompt;  SolaceCtrlName = '?WPR:Fault_Code11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code11:2;  SolaceCtrlName = '?WPR:Fault_Code11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code11;  SolaceCtrlName = '?wpr:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:11;  SolaceCtrlName = '?Button4:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code12:Prompt;  SolaceCtrlName = '?WPR:Fault_Code12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WPR:Fault_Code12:2;  SolaceCtrlName = '?WPR:Fault_Code12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?wpr:Fault_Code12;  SolaceCtrlName = '?wpr:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:12;  SolaceCtrlName = '?Button4:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FaultCodesText;  SolaceCtrlName = '?FaultCodesText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select11;  SolaceCtrlName = '?GLO:Select11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GLO:Select12;  SolaceCtrlName = '?GLO:Select12';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Warranty Part'
  OF ChangeRecord
    ActionMessage = 'Changing A Warranty Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Warranty_Part')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Warranty_Part')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?title
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = CtrlShiftS
  SELF.AddHistoryFile(wpr:Record,History::wpr:Record)
  SELF.AddHistoryField(?wpr:Part_Number,5)
  SELF.AddHistoryField(?wpr:Description,6)
  SELF.AddHistoryField(?wpr:Despatch_Note_Number,14)
  SELF.AddHistoryField(?wpr:Purchase_Cost,8)
  SELF.AddHistoryField(?wpr:Sale_Cost,9)
  SELF.AddHistoryField(?wpr:Supplier,7)
  SELF.AddHistoryField(?wpr:Quantity,11)
  SELF.AddHistoryField(?wpr:Exclude_From_Order,13)
  SELF.AddHistoryField(?wpr:Main_Part,21)
  SELF.AddHistoryField(?wpr:Date_Ordered,15)
  SELF.AddHistoryField(?wpr:Order_Number,17)
  SELF.AddHistoryField(?wpr:Date_Received,19)
  SELF.AddHistoryField(?wpr:Fault_Codes_Checked,22)
  SELF.AddHistoryField(?WPR:Fault_Code1:2,26)
  SELF.AddHistoryField(?wpr:Fault_Code1,26)
  SELF.AddHistoryField(?WPR:Fault_Code2:2,27)
  SELF.AddHistoryField(?wpr:Fault_Code2,27)
  SELF.AddHistoryField(?WPR:Fault_Code3:2,28)
  SELF.AddHistoryField(?wpr:Fault_Code3,28)
  SELF.AddHistoryField(?WPR:Fault_Code4:2,29)
  SELF.AddHistoryField(?wpr:Fault_Code4,29)
  SELF.AddHistoryField(?WPR:Fault_Code5:2,30)
  SELF.AddHistoryField(?wpr:Fault_Code5,30)
  SELF.AddHistoryField(?WPR:Fault_Code6:2,31)
  SELF.AddHistoryField(?wpr:Fault_Code6,31)
  SELF.AddHistoryField(?WPR:Fault_Code7:2,32)
  SELF.AddHistoryField(?wpr:Fault_Code7,32)
  SELF.AddHistoryField(?WPR:Fault_Code8:2,33)
  SELF.AddHistoryField(?wpr:Fault_Code8,33)
  SELF.AddHistoryField(?WPR:Fault_Code9:2,34)
  SELF.AddHistoryField(?wpr:Fault_Code9,34)
  SELF.AddHistoryField(?WPR:Fault_Code10:2,35)
  SELF.AddHistoryField(?wpr:Fault_Code10,35)
  SELF.AddHistoryField(?WPR:Fault_Code11:2,36)
  SELF.AddHistoryField(?wpr:Fault_Code11,36)
  SELF.AddHistoryField(?WPR:Fault_Code12:2,37)
  SELF.AddHistoryField(?wpr:Fault_Code12,37)
  SELF.AddUpdateFile(Access:WARPARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CHARTYPE.Open
  Relate:DEFAULT2.Open
  Relate:LOCATION_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:WARPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:MANFAULO.UseFile
  Access:MANFAULT.UseFile
  Access:MANFPALO.UseFile
  Access:JOBSE.UseFile
  Access:ORDPEND.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:WARPARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  Do lookup_location
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?WPR:Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?WPR:Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB6.Init(wpr:Supplier,?wpr:Supplier,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(sup:Company_Name_Key)
  FDCB6.AddField(sup:Company_Name,FDCB6.Q.sup:Company_Name)
  FDCB6.AddField(sup:RecordNumber,FDCB6.Q.sup:RecordNumber)
  FDCB6.AddUpdateField(sup:Company_Name,wpr:Supplier)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:CHARTYPE.Close
    Relate:DEFAULT2.Close
    Relate:LOCATION_ALIAS.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Warranty_Part',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    wpr:Adjustment = 'NO'
    wpr:Warranty_Part = 'YES'
    wpr:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?browse_stock_button
      Error# = 0
      user_code"  = ''
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              user_code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          
          
      Else!If job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              User_Code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          
      End!If job:engineer <> ''
      
      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
      loc:Active   = 1
      loc:Location = use:Location
      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
          !Found
      
      Else!lIf Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Case MessageEx('The selected user is not allocated to an Active Site Location.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          If use:StockFromLocationOnly
              Error# = 1
          End !If use:StockFromLocationOnly
      End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
      
      If Error# = 0
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_model_stock(job:model_number,user_code")
          if globalresponse = requestcompleted
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
      
              if access:stock.fetch(sto:ref_number_key)
                  beep(beep:systemhand)  ;  yield()
                  message('Error! Cannot access the Stock File.', |
                          'ServiceBase 2000', icon:hand)
              Else!if access:stock.fetch(sto:ref_number_key)
                  If sto:Suspend
                      Case MessageEx('This part cannot be used. It has been suspended.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      wpr:Part_Number = ''
                      Select(?wpr:Part_Number)
                  Else !If sto:Suspend
      
                      wpr:part_number     = sto:part_number
                      wpr:description     = sto:description
                      wpr:supplier        = sto:supplier
                      wpr:purchase_cost   = sto:purchase_cost
                      wpr:sale_cost       = sto:sale_cost
                      wpr:part_ref_number = sto:ref_number
                      If sto:assign_fault_codes = 'YES'
                          stm:Manufacturer = sto:Manufacturer
                          stm:Model_Number = job:Model_Number
                          If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                              wpr:fault_code1    = stm:Faultcode1
                              wpr:fault_code2    = stm:Faultcode2
                              wpr:fault_code3    = stm:Faultcode3
                              wpr:fault_code4    = stm:Faultcode4
                              wpr:fault_code5    = stm:Faultcode5
                              wpr:fault_code6    = stm:Faultcode6
                              wpr:fault_code7    = stm:Faultcode7
                              wpr:fault_code8    = stm:Faultcode8
                              wpr:fault_code9    = stm:Faultcode9
                              wpr:fault_code10   = stm:Faultcode10
                              wpr:fault_code11   = stm:Faultcode11
                              wpr:fault_code12   = stm:Faultcode12
      
                          End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      End!If sto:assign_fault_codes = 'YES'
      
                      !TH 09/02/04
                      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                      loc:Active   = 1
                      loc:Location = sto:Location
                      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
      
                      if loc:PickNoteEnable then
                          ?wpr:Exclude_From_Order{prop:disable} = true
                      else
                          ! Start Change 2116 BE(26/06/03)
                          IF (sto:RF_BOARD) THEN
                              UNHIDE(?RF_BOARD_PROMPT)
                              UNHIDE(?tmp_RF_Board_IMEI)
                              beep(beep:systemexclamation)  ;  yield()
                              message('This part will change the IMEI on the job.||Please enter the new IMEI.', |
                                      'ServiceBase 2000', icon:exclamation)
                              Select(?tmp_RF_Board_IMEI)
                          ELSE
                              HIDE(?RF_BOARD_PROMPT)
                              HIDE(?tmp_RF_Board_IMEI)
                              Select(?wpr:quantity)
                          END
                          !Select(?tmp_RF_Board_IMEI)
                      end !loc:PickNoteEnable then
                      !Select(?wpr:quantity)
                      ! End Change 2116 BE(26/06/03)
                  End !If sto:Suspend
                  display()
              end!if access:stock.fetch(sto:ref_number_key)
          end
          globalrequest     = saverequest#
          do lookup_location
      End !Error# = 0
    OF ?tmp_RF_Board_IMEI
      IF (~0{prop:acceptall}) THEN
          IF (LEN(CLIP(tmp_RF_Board_IMEI)) = 18) THEN
            !Ericsson IMEI!
            tmp_RF_Board_IMEI = SUB(tmp_RF_Board_IMEI,4,15)
          END
          IF (CheckLength('IMEI',job:Model_Number,tmp_RF_Board_IMEI)) THEN
              SELECT(?tmp_RF_Board_IMEI)
              CYCLE
          END
      END
    OF ?wpr:Exclude_From_Order
      If ~0{prop:acceptall}
          If thiswindow.request = Insertrecord
              If wpr:exclude_from_order <> tmp:exclude
                  If SecurityCheck('PARTS - EXCLUDE FROM ORDER')
                      Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      wpr:Exclude_From_Order = tmp:Exclude
                  Else !SecurityCheck('EXCLUDE FROM ORDER')
                      If wpr:Exclude_From_Order = 'YES' And wpr:part_ref_number <> ''
                          Case MessageEx('You have selected to ''Exclude From Order''.<13,10><13,10>Warning! This Part''s Stock Quantity will NOT be decremented and an order will NOT be raised if there are insufficient items in stock.<13,10><13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  tmp:exclude = wpr:Exclude_from_order
                              Of 2 ! &No Button
                                  wpr:exclude_from_order = tmp:exclude
                          End!Case MessageEx
                      Else
                          tmp:exclude = wpr:exclude_From_order
                      End!If PAR:Exclude_From_Order = 'YES'
                  End !SecurityCheck('EXCLUDE FROM ORDER')
              End !If wpr:exclude_from_order <> tmp:exclude
          End!If thiswindow.request = Insertrecord
      End!If ~0{prop:acceptall}
      Display()
    OF ?wpr:Main_Part
      If wpr:main_part <> main_part_temp
          If wpr:main_part = 'YES'
              found# = 0
              setcursor(cursor:wait)
              save_war_ali_id = access:warparts_alias.savefile()
              clear(war_ali:record, -1)
              war_ali:ref_number  = job:ref_number
              set(war_ali:part_number_key,war_ali:part_number_key)
              loop
                  next(warparts_alias)
                  if errorcode()                     |
                     or war_ali:ref_number  <> job:ref_number      |
                     then break.  ! end if
                  yldcnt# += 1
                  if yldcnt# > 25
                     yield() ; yldcnt# = 0
                  end !if
                  If war_ali:record_number <> wpr:record_number
                      If war_ali:main_part = 'YES'
                          found# = 1
                          Case MessageEx('One of the other parts attached to this job has already been marked as the ''Main Part''.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          Break
                      End!If wpr_ali:main_part = 'YES'
                  End!If wpr_ali:record_number <> wpr:record_number
              end !loop
              access:warparts_alias.restorefile(save_war_ali_id)
              setcursor()
              If found# = 0
                  main_part_temp = wpr:main_part
              Else!If found# = 0
                  wpr:main_part = main_part_temp
              End!If found# = 0
          Else!If wpr:main_part = 'YES'
              Case MessageEx('Please ensure ONE of the other parts attached to this job is marked as the ''Main Part''.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              main_part_temp  = wpr:main_part
          End!If wpr:main_part = 'YES'
      End!If wpr:main_part <> main_part_temp
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?wpr:Part_Number
      If ~0{prop:acceptall}
      error# = 0
      access:users.clearkey(use:user_code_key)
      use:user_code = job:engineer
      if access:users.tryfetch(use:user_code_key)
          !Can't find engineer's location, so use the user logged on
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
      
              Access:LOCATION.ClearKey(loc:Location_Key)
              loc:Location = use:Location
              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Found
                  If loc:Active = 0
                      Error# = 3
                  End !If loc:Active = 0
              Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              If Error# = 0
                  access:stock.clearkey(sto:location_manufacturer_key)
                  sto:location     = use:location
                  sto:manufacturer = job:manufacturer
                  sto:part_number  = wpr:part_number
                  if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                      stm:Ref_Number   = sto:Ref_Number
                      stm:Model_Number = job:Model_Number
                      If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Found
                          If sto:Suspend
                              error# = 2
                          Else !If sto:Suspended
                              error# = 0
                          End !If sto:Suspended
                      Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Error
                          error# = 1
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                  Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      error# = 1
                  End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
              End !If Error# = 0
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          
      Else!if access:users.tryfetch(use:user_code_key) = Level:Benign
          Access:LOCATION.ClearKey(loc:Location_Key)
          loc:Location = use:Location
          If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Error# = 3
              End !If loc:Active = 0
          Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
      
          If Error# = 0
              access:stock.clearkey(sto:location_manufacturer_key)
              sto:location     = use:location
              sto:manufacturer = job:manufacturer
              sto:part_number  = wpr:part_number
              if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Found
                      If sto:Suspend
                          error# = 2
                      Else !If sto:Suspended
                          error# = 0
                      End !If sto:Suspended
                  Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Error
                      error# = 1
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
              Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  error# = 1
              End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
          End !If Error# = 0
      End!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case error#
          Of 0
              wpr:part_number     = sto:part_number
              wpr:description     = sto:description
              wpr:supplier        = sto:supplier
              wpr:purchase_cost   = sto:purchase_cost
              wpr:sale_cost       = sto:sale_cost
              wpr:part_ref_number = sto:ref_number
              If sto:assign_fault_codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      wpr:fault_code1    = stm:Faultcode1
                      wpr:fault_code2    = stm:Faultcode2
                      wpr:fault_code3    = stm:Faultcode3
                      wpr:fault_code4    = stm:Faultcode4
                      wpr:fault_code5    = stm:Faultcode5
                      wpr:fault_code6    = stm:Faultcode6
                      wpr:fault_code7    = stm:Faultcode7
                      wpr:fault_code8    = stm:Faultcode8
                      wpr:fault_code9    = stm:Faultcode9
                      wpr:fault_code10   = stm:Faultcode10
                      wpr:fault_code11   = stm:Faultcode11
                      wpr:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
              End!If sto:assign_fault_codes = 'YES'
      
              !TH 09/02/04
              Access:LOCATION.ClearKey(loc:ActiveLocationKey)
              loc:Active   = 1
              loc:Location = sto:Location
              If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
      
              if loc:PickNoteEnable then
                  ?wpr:Exclude_From_Order{prop:disable} = true
              else
                  ! Start Change 2116 BE(26/06/03)
                  IF (sto:RF_BOARD) THEN
                      UNHIDE(?RF_BOARD_PROMPT)
                      UNHIDE(?tmp_RF_Board_IMEI)
                      beep(beep:systemexclamation)  ;  yield()
                      message('This part will change the IMEI on the job.||Please enter the new IMEI.', |
                              'ServiceBase 2000', icon:exclamation)
                      Select(?tmp_RF_Board_IMEI)
                  ELSE
                      HIDE(?RF_BOARD_PROMPT)
                      HIDE(?tmp_RF_Board_IMEI)
                      Select(?wpr:quantity)
                  END
                  !Select(?tmp_RF_Board_IMEI)
              end !loc:PickNoteEnable then
              !Select(?wpr:quantity)
              ! End Change 2116 BE(26/06/03)
      
              display()
      
          Of 1
      
              Case MessageEx('Cannot find the selected Part!<13,10><13,10>It does either not exist in the engineer''s location, or is not in stock.  <13,10><13,10>Do you wish to select another part, or continue using the entered part number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Select Another Part|&Continue Using Part',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Select Another Part Button
                      Post(event:accepted,?browse_stock_button)
                  Of 2 ! &Continue Using Part Button
                      Select(?wpr:description)
              End!Case MessageEx
          Of 2
              Case MessageEx('This part cannot be used. It has been suspended.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              wpr:Part_Number = ''
              Select(?wpr:Part_Number)
          Of 3
              Case MessageEx('Error!  Stock for this job will be picked from location ' & Clip(use:Location) & ' by default.'&|
                '<13,10>'&|
                '<13,10>This location is not active and cannot be used.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              wpr:Part_Number = ''
              Select(?wpr:Part_Number)
      
      End!If error# = 0
      Do Lookup_Location
      Display()
      End!If ~0{prop:acceptall}
    OF ?wpr:Supplier
      If wpr:adjustment <> 'YES'
          access:supplier.clearkey(sup:company_name_key)
          sup:company_name = wpr:supplier
          if access:supplier.fetch(sup:company_name_key)
              saverequest#      = globalrequest
              globalresponse    = requestcancelled
              globalrequest     = selectrecord
              browse_suppliers
              if globalresponse = requestcompleted
                  wpr:supplier = sup:company_name
                  display()
              end
              globalrequest     = saverequest#
          end !if access:supplier.fetch(sup:company_name_key)
      End !If wpr:adjustment <> 'YES'
    OF ?wpr:Fault_Code1
      If wpr:fault_code1 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 1
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 1
                  mfp:field        = wpr:fault_code1
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4)
                      display(?wpr:fault_code1)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wpr;fault_code1 <> ''
    OF ?Button4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 1
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code1 = mfp:field
      
      Else
          wpr:Fault_Code1 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code1 = TINCALENDARStyle1(wpr:Fault_Code1)
          Display(?WPR:Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code2
      If wpr:fault_code2 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 2
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 2
                  mfp:field        = wpr:fault_code2
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:2)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wpr:fault_code2 <> ''
    OF ?Button4:2
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 2
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code2 = mfp:field
          
      Else
          wpr:Fault_Code2 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code2 = TINCALENDARStyle1(wpr:Fault_Code2)
          Display(?WPR:Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code3
      If wpr:fault_code3 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 3
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 3
                  mfp:field        = wpr:fault_code3
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:3)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wpr:fault_code3 <> ''
    OF ?Button4:3
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 3
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code3 = mfp:field
          
      Else
          wpr:fault_Code3 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code3 = TINCALENDARStyle1(wpr:Fault_Code3)
          Display(?WPR:Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code4
      IF wpr:fault_Code4 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 4
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 4
                  mfp:field        = wpr:fault_code4
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:4)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!IF wpr:fault_Code4 <> ''
    OF ?Button4:4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 4
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code4 = mfp:field
          
      Else
          wpr:Fault_Code4 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code4 = TINCALENDARStyle1(wpr:Fault_Code4)
          Display(?WPR:Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code5
      IF wpr:fault_Code5 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 5
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 5
                  mfp:field        = wpr:fault_code5
                  if access:manfpalo.fetch(mfp:field_key)
                       Post(event:accepted,?button4:5)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!IF wpr:fault_Code5 <> ''
    OF ?Button4:5
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 5
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code5 = mfp:field
          
      Else
          wpr:Fault_Code5 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code5 = TINCALENDARStyle1(wpr:Fault_Code5)
          Display(?WPR:Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code6
      If wpr:fault_code6 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 6
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 6
                  mfp:field        = wpr:fault_code6
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:6)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wpr:fault_code6 <> ''
    OF ?Button4:6
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 6
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code6 = mfp:field
          
      Else
          wpr:Fault_Code6 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code6 = TINCALENDARStyle1(wpr:Fault_Code6)
          Display(?WPR:Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code7
      If wpr:fault_code7 <> ''
      
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = job:Manufacturer
      map:field_number = 7
      if access:manfaupa.fetch(map:field_number_key) = Level:Benign
          If map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = job:Manufacturer
              mfp:field_number = 7
              mfp:field        = wpr:fault_code7
              if access:manfpalo.fetch(mfp:field_key)
                       Post(event:accepted,?button4:7)
      
              end!if access:manfpalo.fetch(mfp:field_key)
          End!If map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      End!End!If wpr:fault_code6 <> ''
    OF ?Button4:7
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 7
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code7 = mfp:field
          
      Else
          wpr:Fault_Code7 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code7 = TINCALENDARStyle1(wpr:Fault_Code7)
          Display(?WPR:Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code8
      If wpr:fault_code8 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 8
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 8
                  mfp:field        = wpr:fault_code8
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:8)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wpr:fault_code8 <> ''
    OF ?Button4:8
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 8
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code8 = mfp:field
          
      Else
          wpr:Fault_Code8 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code8 = TINCALENDARStyle1(wpr:Fault_Code8)
          Display(?WPR:Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code9
      IF wpr:fault_code9 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 9
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 9
                  mfp:field        = wpr:fault_code9
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:9)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!IF wpr:fault_code9 <> ''
    OF ?Button4:9
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 9
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code9 = mfp:field
          
      Else
          wpr:Fault_Code9 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code9 = TINCALENDARStyle1(wpr:Fault_Code9)
          Display(?WPR:Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code10
      IF wpr:fault_code10 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 10
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 10
                  mfp:field        = wpr:fault_code10
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:10)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!IF wpr:fault_code10 <> ''
    OF ?Button4:10
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 10
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code10 = mfp:field
          
      ELse
          wpr:Fault_Code10 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code10 = TINCALENDARStyle1(wpr:Fault_Code10)
          Display(?WPR:Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code11
      If wpr:fault_code11 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 11
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 11
                  mfp:field        = wpr:fault_code11
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:11)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If wpr:fault_code11 <> ''
    OF ?Button4:11
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 11
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code11 = mfp:field
          
      Else
          wpr:Fault_Code11 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code11 = TINCALENDARStyle1(wpr:Fault_Code11)
          Display(?WPR:Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?wpr:Fault_Code12
      IF wpr:fault_code12 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:Manufacturer
          map:field_number = 12
          if access:manfaupa.fetch(map:field_number_key) = Level:Benign
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:Manufacturer
                  mfp:field_number = 12
                  mfp:field        = wpr:fault_code12
                  if access:manfpalo.fetch(mfp:field_key)
                      Post(event:accepted,?button4:12)
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!IF wpr:fault_code12 <> ''
    OF ?Button4:12
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 12
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          wpr:fault_code12 = mfp:field
          
      Else
          wpr:Fault_Code12 = ''
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      display()
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          wpr:Fault_Code12 = TINCALENDARStyle1(wpr:Fault_Code12)
          Display(?WPR:Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Fault COdes Checked
  If wpr:fault_codes_checked <> 'YES'
      If fault_codes_required_temp = 'YES'
          Case MessageEx('You must tick ''Fault Codes Checked'' before you can continue.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?wpr:fault_codes_checked)
          Cycle
      End!If fault_codes_required_temp = 'YES'
  End!If wpr:fault_codes_checked <> 'YES'
  ! If ThisWindow.Request = insertrecord
  !    Set(DEFAULTS)
  !    Access:DEFAULTS.Next()
  !
  !    found# = 0
  !!Is this part number already on the job?
  !    If wpr:part_number <> 'ADJUSTMENT'
  !        setcursor(cursor:wait)
  !        save_war_ali_id = access:warparts_alias.savefile()
  !        access:warparts_alias.clearkey(war_ali:part_number_key)
  !        war_ali:ref_number  = job:ref_number
  !        war_ali:part_number = wpr:part_number
  !        set(war_ali:part_number_key,war_ali:part_number_key)
  !        loop
  !            if access:warparts_alias.next()
  !               break
  !            end !if
  !            if war_ali:ref_number  <> job:ref_number      |
  !            or war_ali:part_number <> wpr:part_number      |
  !                then break.  ! end if
  !            IF war_ali:record_number <> wpr:record_number
  !                If war_ali:date_received = ''
  !                    found# = 1
  !                    Break
  !                End!IF par_ali:record_number <> par:record_number
  !            End!If Pointer(warparts_alias) <> Pointer#
  !        end !loop
  !        access:warparts_alias.restorefile(save_war_ali_id)
  !        setcursor()
  !        If found# = 1
  !            Case MessageEx('This part has already been attached to this job.','ServiceBase 2000',|
  !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                Of 1 ! &OK Button
  !            End!Case MessageEx
  !            Select(?wpr:part_number)
  !            Cycle
  !        End !If found# = 1
  !    End!If wpr:part_number <> 'ADJUSTMENT'
  !    If wpr:exclude_from_order <> 'YES'
  !        If wpr:part_ref_number <> ''
  !            access:stock.clearkey(sto:ref_number_key)
  !            sto:ref_number = wpr:part_ref_number
  !            if access:stock.fetch(sto:ref_number_key)
  !               beep(beep:systemhand)  ;  yield()
  !                message('An Error has occured retrieving the details of this Stock Part.', |
  !                        'ServiceBase 2000', icon:hand)
  !            Else!if access:stock.fetch(sto:ref_number_key)
  !                If sto:sundry_item <> 'YES'
  !                
  !                    If wpr:quantity > sto:quantity_stock
  !            !Back Order
  !                        Case MessageEx('There are insufficient items in stock!<13,10><13,10>Do you wish to Order the remaining items, or re-enter the quantity required?','ServiceBase 2000',|
  !                                       'Styles\warn.ico','|&Order Part|&Re-enter Quantity|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
  !                            Of 1 ! &Order Part Button
  !                                If SecurityCheck('JOBS - ORDER PARTS')
  !                                    Case MessageEx('You do not have access to create an order.','ServiceBase 2000',|
  !                                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                                        Of 1 ! &OK Button
  !                                    End!Case MessageEx
  !                                    Select(?wpr:quantity)
  !                                    Cycle
  !                                Else !If SecurityCheck('JOBS - ORDER PARTS')
  !                                    !New Order Type or Old Order Type
  !                                    tmp:CreateNewOrder = 0
  !                                    Case def:SummaryOrders
  !                                        Of 0 !Old Way
  !                                            tmp:CreateNewOrder = 1
  !
  !                                        Of 1 !New Way
  !                                            access:ORDPEND.clearkey(ope:Supplier_Key)
  !                                            ope:Supplier    = wpr:Supplier
  !                                            ope:Part_Number = wpr:Part_Number
  !                                            If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
  !                                                !Found
  !                                                ope:Quantity    += wpr:Quantity
  !                                                Access:ORDPEND.Update()
  !                                                tmp:CreateNewOrder = 0
  !                                                If sto:Quantity_stock <=0
  !                                                    wpr:Pending_Ref_Number = ope:Ref_Number
  !                                                    wpr:Requested = True
  !                                                Else!If sto:Quantity_stock <=0
  !                                                    glo:Select1     = 'NEW PENDING'
  !                                                    glo:Select2     = wpr:Part_Ref_Number
  !                                                    glo:Select3     = wpr:Quantity - sto:Quantity_Stock
  !                                                    glo:Select4     = ope:Ref_Number
  !                                                    wpr:Quantity    = sto:Quantity_Stock
  !                                                End!If sto:Quantity_stock <=0
  !                                            Else! If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
  !                                                !Error
  !                                                !Assert(0,'Fetch Error')
  !                                                tmp:CreateNewOrder = 1
  !                                            End! If access:ORDPEND.tryfetch(ope:Supplier_Key) = Level:Benign
  !                                            
  !                                    End!Case def:SummaryOrders
  !                                    If tmp:CreateNewOrder = 1
  !                                        get(ordpend,0)
  !                                        if access:ordpend.primerecord() = level:benign
  !                                            ope:part_ref_number = sto:ref_number
  !                                            ope:job_number      = job:ref_number
  !                                            ope:part_type       = 'WAR'
  !                                            ope:supplier        = wpr:supplier
  !                                            ope:part_number     = wpr:part_number
  !                                            ope:description     = wpr:description
  !                                            If sto:quantity_stock <= 0
  !                                                wpr:pending_ref_number = ope:ref_number
  !                                                sto:quantity_to_order   += wpr:quantity
  !                                                access:stock.update()
  !                                                ope:quantity    = wpr:quantity
  !                                                wpr:Requested   = True
  !                                            Else
  !                                                glo:select1      = 'NEW PENDING'
  !                                                glo:select2      = wpr:part_ref_number
  !                                                glo:select3      = wpr:quantity - sto:quantity_stock
  !                                                glo:select4      = ope:ref_number
  !                                                ope:quantity     = wpr:quantity - sto:quantity_stock
  !                                                wpr:quantity     = sto:quantity_stock
  !                                            End
  !                                            if access:ordpend.insert()
  !                                                access:ordpend.cancelautoinc()
  !                                            end
  !                                        end!if access:ordpend.primerecord() = level:benign
  !                                    End!If CreateNewOrder# = 1
  !                                End !If SecurityCheck('JOBS - ORDER PARTS')
  !                            Of 2 ! &Re-enter Quantity Button
  !                                Select(?wpr:quantity)
  !                                Cycle
  !                            Of 3 ! &Cancel Button
  !                                Select(?wpr:part_number)
  !                                Cycle
  !                        End!Case MessageEx
  !                    Else !If wpr:quantity > sto:quantity_stock
  !                        wpr:date_ordered = Today()
  !                        sto:quantity_stock -= wpr:quantity
  !                        If sto:quantity_stock < 0
  !                            sto:quantity_stock = 0
  !                        End
  !                        Access:stock.update()
  !
  !                        If def:add_stock_label = 'YES'
  !                            glo:select1  = sto:ref_number
  !                            case def:label_printer_type
  !                                of 'TEC B-440 / B-442'
  !                                    stock_request_label(Today(),Clock(),wpr:quantity,job:ref_number,job:engineer,wpr:sale_cost)
  !                                of 'TEC B-452'
  !                                    stock_request_label_B452(Today(),Clock(),wpr:quantity,job:ref_number,job:engineer,wpr:sale_cost)
  !                            end!case def:label_printer_type
  !                            glo:select1 = ''
  !                        End!If def:add_stock_label = 'YES'
  !                        get(stohist,0)
  !                        if access:stohist.primerecord() = level:benign
  !                            shi:ref_number           = sto:ref_number
  !                            shi:transaction_type     = 'DEC'
  !                            shi:despatch_note_number = wpr:despatch_note_number
  !                            shi:quantity             = wpr:quantity
  !                            shi:date                 = Today()
  !                            shi:purchase_cost        = wpr:purchase_cost
  !                            shi:sale_cost            = wpr:sale_cost
  !                            shi:retail_cost          = wpr:retail_cost
  !                            shi:job_number           = job:ref_number
  !                            access:users.clearkey(use:password_key)
  !                            use:password =glo:password
  !                            access:users.fetch(use:password_key)
  !                            shi:user                 = use:user_code
  !                            shi:notes                = 'STOCK DECREMENTED'
  !                            if access:stohist.insert()
  !                                access:stohist.cancelautoinc()
  !                            end
  !                        end!if access:stohist.primerecord() = level:benign
  !                    End !If wpr:quantity > sto:quantity_stock
  !                Else!If sto:sundry_item = 'YES'
  !                    wpr:date_ordered = Today()
  !                End!If sto:sundry_item = 'YES'
  !            end!if access:stock.fetch(sto:ref_number_key)
  !
  !        Else !If wpr:part_ref_number <> ''
  !            Case MessageEx('This is a non-stock item. Do you wish it to appear on the next Parts Order?<13,10>','ServiceBase 2000',|
  !                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
  !                Of 1 ! &Yes Button
  !
  !                    get(ordpend,0)
  !                    if access:ordpend.primerecord() = level:benign
  !                        ope:part_ref_number = ''
  !                        ope:job_number      = job:ref_number
  !                        ope:part_type       = 'WAR'
  !                        ope:supplier        = wpr:supplier
  !                        ope:part_number     = wpr:part_number
  !                        ope:description     = wpr:description
  !                        ope:quantity        = wpr:quantity
  !                        if access:ordpend.insert()
  !                            access:ordpend.cancelautoinc()
  !                        end
  !                    end!if access:ordpend.primerecord() = level:benign
  !                    wpr:pending_ref_number = ope:ref_number
  !
  !                Of 2 ! &No Button
  !                    wpr:date_ordered = Today()
  !                    wpr:exclude_from_order = 'YES'
  !            End!Case MessageEx
  !        End !If wpr:part_ref_number <> ''
  !    Else!If wpr:exclude_from_order <> 'YES'
  !        wpr:date_ordered = Today()
  !    End !If wpr:exclude_from_order <> 'YES'
  !End !If localrequest = insertrecord
  !If Thiswindow.request <> Insertrecord
  !    check_parts# = 0
  !    access:stock.clearkey(sto:ref_number_key)
  !    sto:ref_number = wpr:part_ref_number
  !    if access:stock.fetch(sto:ref_number_key) = Level:benign
  !        If sto:sundry_item <> 'YES'
  !            check_parts# = 1
  !        End!If sto:sundry_item <> 'YES'
  !
  !        If check_parts# = 1
  !            If TMP:Part_Number <> wpr:part_number Or |
  !              tmp:description <> wpr:description Or |
  !              tmp:supplier    <> wpr:supplier Or |
  !              tmp:purchase_cost <> wpr:purchase_cost Or |
  !              tmp:sale_cost <> wpr:sale_cost Or |
  !              quantity_temp <> wpr:Quantity
  !                !Does a pending exist. If so, you can't change this part.
  !                If wpr:pending_ref_number = ''
  !                    setcursor(cursor:wait)
  !                    save_war_ali_id = access:warparts_alias.savefile()
  !                    access:warparts_alias.clearkey(war_ali:part_number_key)
  !                    war_ali:ref_number  = job:ref_number
  !                    war_ali:part_number = tmp:part_number
  !                    set(war_ali:part_number_key,war_ali:part_number_key)
  !                    loop
  !                        if access:warparts_alias.next()
  !                           break
  !                        end !if
  !                        if war_ali:ref_number  <> job:ref_number      |
  !                        or war_ali:part_number <> tmp:part_number      |
  !                            then break.  ! end if
  !                        If war_ali:record_number <> wpr:record_number
  !                            If war_ali:date_received = ''
  !                                found# = 1
  !                                Break
  !                            End!If war_ali:date_received = ''
  !                        End!If war_ali:record_number <> wpr:record_number
  !                    end !loop
  !                    access:warparts_alias.restorefile(save_war_ali_id)
  !                    setcursor()
  !
  !                    If found# = 1
  !                        Case MessageEx('This Part is awaiting Order Generation.'&|
  !                          '<13,10>'&|
  !                          '<13,10>While a ''Pending'' part exisits, you cannot amend this part.','ServiceBase 2000',|
  !                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                            Of 1 ! &OK Button
  !                        End!Case MessageEx
  !                        !Reset Fields
  !                        wpr:part_number     = tmp:part_number
  !                        wpr:description     = tmp:description
  !                        wpr:supplier        = tmp:supplier
  !                        wpr:purchase_cost   = tmp:purchase_cost
  !                        wpr:sale_cost       = tmp:sale_cost
  !                        wpr:quantity        = quantity_temp
  !                        Display()
  !                        Select(?wpr:part_number)
  !                        Cycle
  !                    Else!If found# = 1
  !                        !No Pending Part Found
  !                        If wpr:quantity > quantity_temp
  !                            !The Quantity has been increased.
  !                            If wpr:quantity - quantity_temp > sto:quantity_stock
  !                                !If the difference isn't in stock
  !                                Case MessageEx('There are insufficient items in stock!'&|
  !                                  '<13,10>'&|
  !                                  '<13,10>Do you wish to Order the remaining items, or re-enter the quantity required?','ServiceBase 2000',|
  !                                               'Styles\question.ico','|&Order Part|&Re-enter Quantity|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
  !                                    Of 1 ! &Order Remaining Button
  !                                        If SecurityCheck('JOBS - ORDER PARTS')
  !                                            Case MessageEx('You do not have access to create an order.','ServiceBase 2000',|
  !                                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                                                Of 1 ! &OK Button
  !                                            End!Case MessageEx
  !                                            Select(?wpr:Quantity)
  !                                            Cycle
  !                                        Else !If SecurityCheck('JOBS - ORDER PARTS')
  !                                            get(ordpend,0)
  !                                            if access:ordpend.primerecord() = level:benign
  !                                                !Create a new pending order
  !                                                ope:part_ref_number = sto:ref_number
  !                                                ope:job_number      = job:ref_number
  !                                                ope:part_type       = 'WAR'
  !                                                ope:supplier        = wpr:supplier
  !                                                ope:part_number     = wpr:part_number
  !                                                ope:description     = wpr:description
  !                                                ope:quantity        = (wpr:quantity - quantity_temp) - sto:Quantity_Stock
  !
  !                                                !Make a new part for the difference, and reset this part
  !                                                glo:select1      = 'NEW PENDING'
  !                                                glo:select2      = wpr:part_ref_number
  !                                                glo:select3      = (wpr:quantity - quantity_temp) - sto:quantity_stock
  !                                                glo:select4      = ope:ref_number
  !                                                wpr:quantity        = quantity_temp + sto:quantity_stock
  !                                                if access:ordpend.insert()
  !                                                    access:ordpend.cancelautoinc()
  !                                                end
  !                                            end!if access:ordpend.primerecord() = level:benign
  !                                        End !If SecurityCheck('JOBS - ORDER PARTS')
  !                                    Of 2 ! &Re-Enter Quantity Button
  !                                        Select(?wpr:quantity)
  !                                        Cycle
  !                                    Of 3 ! &Cancel Button
  !                                        Select(?wpr:part_number)
  !                                        Cycle
  !                                End!Case MessageEx
  !                            Else !If wpr:quantity - quantity_temp > sto:quantity_stock
  !                                !This is sufficient in stock
  !                                sto:quantity_stock -= (wpr:quantity - quantity_temp)
  !                                If sto:quantity_stock < 0
  !                                    sto:quantity_stock = 0
  !                                End
  !                                Access:stock.Update()
  !                                get(stohist,0)
  !                                if access:stohist.primerecord()= level:benign
  !                                    shi:ref_number           = sto:ref_number
  !                                    shi:transaction_type     = 'DEC'
  !                                    shi:despatch_note_number = wpr:despatch_note_number
  !                                    shi:quantity             = wpr:quantity - quantity_temp
  !                                    shi:date                 = today()
  !                                    shi:purchase_cost        = wpr:purchase_cost
  !                                    shi:sale_cost            = wpr:sale_cost
  !                                    shi:retail_cost          = wpr:retail_cost
  !                                    shi:job_number           = job:ref_number
  !                                    access:users.clearkey(use:password_key)
  !                                    use:password =glo:password
  !                                    access:users.fetch(use:password_key)
  !                                    shi:user                 = use:user_code
  !                                    shi:notes                = 'STOCK DECREMENTED'
  !                                    shi:information          = ''
  !                                    if access:stohist.insert()
  !                                        access:stohist.cancelautoinc()
  !                                    end
  !                                end!if access:stohist.primerecord()= level:benign
  !                            End !If wpr:quantity - quantity_temp > sto:quantity_stock
  !                        Else !If wpr:quantity > quantity_temp
  !                            !If the quantity is lowered
  !                            sto:quantity_stock += quantity_temp - wpr:quantity
  !                            Access:stock.Update()
  !                            get(stohist,0)
  !                            if access:stohist.primerecord() = level:benign
  !                                shi:ref_number           = sto:ref_number
  !                                shi:transaction_type     = 'REC'
  !                                shi:despatch_note_number = wpr:despatch_note_number
  !                                shi:quantity             = quantity_temp - wpr:quantity
  !                                shi:date                 = today()
  !                                shi:purchase_cost        = wpr:purchase_cost
  !                                shi:sale_cost            = wpr:sale_cost
  !                                shi:retail_cost          = wpr:retail_cost
  !                                shi:job_number           = job:ref_number
  !                                access:users.clearkey(use:password_key)
  !                                use:password =glo:password
  !                                access:users.fetch(use:password_key)
  !                                shi:user                 = use:forename
  !                                shi:notes                = 'STOCK RECREDITED'
  !                                shi:information          = ''
  !                                if access:stohist.insert()
  !                                    access:stohist.cancelautoinc()
  !                                end
  !                            end!if access:stohist.primerecord() = level:benign
  !                        End !If wpr:quantity > quantity_temp
  !
  !                    End!If found# = 1
  !
  !                Else!If wpr:pending_ref_number = ''
  !                    !This part is a pending part.
  !                    If TMP:Part_Number <> wpr:part_number Or |
  !                        tmp:description <> wpr:description Or |
  !                        tmp:supplier    <> wpr:supplier Or |
  !                        tmp:purchase_cost <> wpr:purchase_cost Or |
  !                        tmp:sale_cost <> wpr:sale_cost
  !
  !                        Case MessageEx('Cannot amend. This part is awaiting Order Generation.','ServiceBase 2000',|
  !                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                            Of 1 ! &OK Button
  !                        End!Case MessageEx
  !                        !Cannot change a pending part, revert details.
  !                        wpr:part_number     = tmp:part_number
  !                        wpr:description     = tmp:description
  !                        wpr:supplier        = tmp:supplier
  !                        wpr:purchase_cost   = tmp:purchase_cost
  !                        wpr:sale_cost       = tmp:sale_cost
  !                        wpr:quantity        = quantity_temp
  !                        Display()
  !                        Select(?wpr:part_number)
  !                        Cycle
  !                    Else !!If part_details_group <> wpr:part_details_group
  !                        !As long as the part details haven't been changed
  !                        !Amend the pending order
  !                        
  !                        access:ordpend.clearkey(ope:ref_number_key)
  !                        ope:ref_number = wpr:Pending_Ref_number
  !                        if access:ordpend.fetch(ope:ref_number_key) = Level:Benign
  !                            If wpr:Quantity < quantity_temp
  !                                ope:quantity -= (Quantity_Temp - wpr:Quantity)
  !                            End!If wpr:Quantity < quantity_temp
  !                            If wpr:Quantity > quantity_temp
  !                                ope:quantity += (wpr:Quantity - Quantity_Temp)
  !                            End!If wpr:Quantity > quantity_temp
  !                            
  !                            Access:ordpend.Update()
  !                        end !if
  !
  !                    End!If part_details_group <> wpr:part_details_group
  !                End!If wpr:pending_ref_number = ''
  !            End!If part_details_group <> wpr:part_details_group
  !        End!If check_parts# = 1
  !    end!if access:stock.fetch(sto:ref_number_key) = Level:benign
  !End!If Thiswindow.request <> Insertrecord
  If thiswindow.Request = InsertRecord
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      !Is this a stock part?
      tmp:StockNumber = 0
      If wpr:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = wpr:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              !Need to check for duplicates?
              tmp:StockNumber = sto:Ref_Number
              Found# = 0
              If sto:AllowDuplicate = 0
                  Save_war_ali_ID = Access:WARPARTS_ALIAS.SaveFile()
                  Access:WARPARTS_ALIAS.ClearKey(war_ali:Part_Number_Key)
                  war_ali:Ref_Number  = job:Ref_Number
                  war_ali:Part_Number = wpr:Part_Number
                  Set(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
                  Loop
                      If Access:WARPARTS_ALIAS.NEXT()
                         Break
                      End !If
                      If war_ali:Ref_Number  <> job:Ref_Number      |
                      Or war_ali:Part_Number <> wpr:Part_Number     |
                          Then Break.  ! End If
                      If wpr:Record_Number <> war_ali:Record_Number
                          If war_ali:Date_Received = ''
                              Found# = 1
                              Break
                          End !If par_ali:Date_Received
                      End !If wpr:Record_Number <> par_ali:Record_Number
                  End !Loop
                  Access:WARPARTS_ALIAS.RestoreFile(Save_war_ali_ID)
                  If Found# = 1
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Message('This part is already attached to this job.', |
                              'ServiceBase 2000', Icon:Hand, |
                               '&OK', 1, 0)
                      Of 1  ! Name: &OK  (Default)
                      End !CASE
                      Select(?wpr:part_number)
                      Cycle                
                  End !If Found# = 1
              End !If sto:AllowDuplicate = 0
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      End !wpr:Part_Ref_Number <> ''
  
      If wpr:Part_Number <> 'ADJUSTMENT'
          !The part is not an adjustment
          If wpr:Exclude_From_Order <> 'YES'        
              !The part can be ordered
              If tmp:StockNumber 
                  If sto:Sundry_Item <> 'YES'
                      
                      !This is from stock
                      If wpr:Quantity > sto:Quantity_Stock
                          !There isn't enough in stock
                          ! Start Change 216 BE(26/06/03)
                          ! Start Change 4652 BE(1/09/2004)
                          !IF (sto:RF_BOARD) THEN
                          IF (NOT ?tmp_RF_Board_IMEI {PROP:HIDE}) THEN
                          ! End Change 4652 BE(1/09/2004)
                              Beep(Beep:SystemHand)  ;  Yield()
                              Message('There are insufficient items in stock!' & |
                                          '||This part requires the entry of a new IMEI when attached to a job.' & |
                                          '||This part can therefore not be added at this time.', |
                                          'ServiceBase 2000', Icon:Hand, |
                                           '&OK', 1, 0)
                              Select(?wpr:Quantity)
                              Cycle
                         END
                          ! End Change 216 BE(26/06/03)
                          Beep(Beep:SystemQuestion)  ;  Yield()
                          Case Message('There are insufficient items in stock!'&|
                                  '||Do you wish to ORDER the remaining items, or RE-ENTER the '&|
                                  'quantity required?', |
                                  'ServiceBase 2000', Icon:Question, |
                                   'Order|Re-enter', 2, 0)
                          Of 1  ! Name: Order
                              If SecurityCheck('JOBS - ORDER PARTS')
                                  Beep(Beep:SystemHand)  ;  Yield()
                                  Case Message('You do not have access to this option.', |
                                          'ServiceBase 2000', Icon:Hand, |
                                           '&OK', 1, 0)
                                  Of 1  ! Name: &OK  (Default)
                                  End !CASE                            
                              Else !If SecurityCheck('JOBS - ORDER PARTS')
                                  tmp:CreateNewOrder = 0
                                  Case def:SummaryOrders
                                      Of 0 !Old Way
                                          tmp:CreateNewOrder = 1
                                      Of 1 !New Way
                                          !Check to see if a pending order already exists.
                                          !If so, add to that. 
                                          !If not, create a new pending order
                                          Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                          ope:Supplier    = wpr:Supplier
                                          ope:Part_Number = wpr:Part_Number
                                          If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                              !Found
  
                                              ! Start Change 2224 BE(28/03/03)
                                              !
                                              ! Delay update until it is established that there is enough
                                              ! stock to fulfill a part order
                                              !
                                              !ope:Quantity += wpr:Quantity
                                              !Access:ORDPEND.Update()
                                              !
                                              ! End Change 2224 BE(28/03/03)
  
                                              tmp:CreateNewOrder = 0
                                              !If there is some in stock, then use that for this part
                                              !then pass the variables, to create another part line
                                              !for the ordered part.
                                              If sto:Quantity_Stock > 0
                                                  glo:Select1 = 'NEW PENDING'
                                                  glo:Select2 = wpr:Part_Ref_Number
                                                  glo:Select3 = wpr:Quantity-sto:quantity_stock
                                                  glo:Select4 = ope:Ref_Number
  
                                                  ! Start Change 2224 BE(28/03/03)
                                                  ope:Quantity += (wpr:Quantity - sto:Quantity_Stock)
                                                  ! End Change 2224 BE(28/03/03)
  
                                                  wpr:Quantity    = sto:Quantity_Stock
                                                  wpr:Date_Ordered    = Today()
                                              Else !If sto:Quantity_Stock > 0
  
                                                  ! Start Change 2224 BE(28/03/03)
                                                  ope:Quantity += wpr:Quantity
                                                  ! End Change 2224 BE(28/03/03)
  
                                                  wpr:Pending_Ref_Number  =  ope:Ref_Number
                                                  wpr:Requested   = True
                                                 
                                              End !If sto:Quantity_Stock > 0
  
                                              ! Start Change BE019 BE(27/11/03)
                                              Access:ORDPEND.Update()
                                              ! Start Change BE019 BE(28/03/03)
  
                                          Else! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
                                              tmp:CreateNewOrder  = 1
                                              wpr:Requested   = True
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
                                          
                                  End !Case def:SummaryOrders
  
                                  If tmp:CreateNewOrder = 1
                                      If Access:ORDPEND.PrimeRecord() = Level:Benign
                                          ope:Part_Ref_Number = sto:Ref_Number
                                          ope:Job_Number      = job:Ref_Number
                                          ope:Part_Type       = 'WAR'
                                          ope:Supplier        = wpr:Supplier
                                          ope:Part_Number     = wpr:Part_Number
                                          ope:Description     = wpr:Description
                                          
                                          If sto:Quantity_Stock <=0
                                              wpr:Pending_Ref_Number  = ope:Ref_Number
                                              ope:Quantity    = wpr:Quantity
                                              ope:PartRecordNumber    = wpr:Record_Number
                                          Else !If sto:Quantity_Stock <=0
                                              glo:Select1 = 'NEW PENDING'
                                              glo:Select2 = wpr:Part_Ref_Number
                                              glo:Select3 = wpr:Quantity - sto:Quantity_Stock
                                              glo:Select4 = ope:Ref_Number
                                              ope:Quantity    = wpr:Quantity - sto:Quantity_Stock
                                              wpr:Quantity    = sto:Quantity_Stock
                                              wpr:Date_Ordered = Today()
                                          End !If sto:Quantity_Stock <=0
                                          If Access:ORDPEND.TryInsert() = Level:Benign
                                              !Insert Successful
                                          Else !If Access:ORDPEND.TryInsert() = Level:Benign
                                              !Insert Failed
                                          End !If Access:ORDPEND.TryInsert() = Level:Benign
                                      End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                                  End !If tmp:CreateNewOrder = 1
  
                                  ! Start Change 2224 BE(28/03/03)
                                  ! Status 330 Spares Requested
                                  GetStatus(330,thiswindow.request,'JOB')
                                  ! End Change 2224 BE(28/03/03)
  
                              End !If SecurityCheck('JOBS - ORDER PARTS')
                          Of 2  ! Name: Re-enter  (Default)
                              Select(?wpr:Quantity)
                              Cycle
                          End !CASE
                      Else !If wpr:Quantity > sto:Quantity
                          !There is sufficient in stock
                          wpr:Date_Ordered    = Today()
                          !Reget Stock
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              sto:Quantity_Stock  -= wpr:Quantity
                              If sto:Quantity_Stock < 0
                                  sto:Quantity_Stock = 0
                              End !If sto:Quantity_Stock < 0
                              If Access:STOCK.Update() = Level:Benign
                                  If def:Add_Stock_Label = 'YES'
                                      glo:Select1 = sto:Ref_Number
                                      Case def:Label_Printer_Type
                                          of 'TEC B-440 / B-442'
                                              stock_request_label(Today(),Clock(),wpr:quantity,job:ref_number,job:engineer,wpr:sale_cost)
                                          of 'TEC B-452'
                                              stock_request_label_B452(Today(),Clock(),wpr:quantity,job:ref_number,job:engineer,wpr:sale_cost)                                    
                                      End !Case def:Label_Printer_Type
                                  End !If def:Add_Stock_Label = 'YES'
  
                                  If Access:STOHIST.PrimeRecord() = Level:Benign
                                      shi:Ref_Number          = sto:Ref_Number
                                      shi:Transaction_Type    = 'DEC'
                                      shi:Despatch_Note_Number    = wpr:Despatch_Note_Number
                                      shi:Quantity            = wpr:Quantity
                                      shi:Date                = Today()
                                      shi:Purchase_Cost       = wpr:Purchase_Cost
                                      shi:Sale_Cost           = wpr:Sale_Cost
                                      shi:Retail_Cost         = wpr:Retail_Cost
                                      shi:Job_Number          = job:Ref_Number
                                      Access:USERS.Clearkey(use:Password_Key)
                                      use:Password    = glo:Password
                                      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Found
  
                                      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      shi:user                = use:User_Code
                                      shi:Notes               = 'STOCK DECREMENTED'
                                      
                                      If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:STOHIST.TryInsert() = Level:Benign
                                  End !If Access:STOHIST.PrimeRecord() = Level:Benign
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      End !If wpr:Quantity > sto:Quantity
                  Else !If sto:Sundry_Item <> 'YES'
                      !Sundry Item, do nothing
                      wpr:Date_Ordered    = Today()
                  End !If sto:Sundry_Item <> 'YES'
              Else !If tmp:StockNumber
                  !A non stock part
                  Beep(Beep:SystemQuestion)  ;  Yield()
                  Case Message('This is a NON-STOCK item.'&|
                          '||Do you wish it to appear on the next parts order?', |
                          'ServiceBase 2000', Icon:Question, |
                           Button:Yes+Button:No, Button:No, 0)
                  Of Button:Yes
                      If Access:ORDPEND.PrimeRecord() = Level:Benign
                          ope:Job_Number      = job:Ref_Number
                          ope:Part_Type       = 'WAR'
                          ope:Supplier        = wpr:Supplier
                          ope:Part_Number     = wpr:Part_Number
                          ope:Description     = wpr:Description
                          ope:Quantity        = wpr:Quantity
                          ope:PartRecordNumber    = wpr:Record_Number
                          wpr:Pending_Ref_Number  = ope:Ref_Number
                          If Access:ORDPEND.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:ORDPEND.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:ORDPEND.TryInsert() = Level:Benign
                      End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                      
                  Of Button:No
                      wpr:Exclude_From_Order  = 'YES'
                      wpr:Date_Ordered    = Today()
                  End !CASE
                  
              End !If tmp:StockNumber
          Else !If wpr:Exclude_From_Order <> 'YES'        
              !excluded, do nothing
              wpr:Date_Ordered = Today()
          End !If wpr:Exclude_From_Order <> 'YES'        
          ! Start Change 2116 BE(26/06/03)
          ! Start Change 4290 BE(18/05/04)
          !IF (sto:RF_BOARD) THEN
          IF (NOT ?tmp_RF_Board_IMEI {PROP:HIDE}) THEN
          ! End Change 4290 BE(18/05/04)
              GET(audit,0)
              IF (access:audit.primerecord() = level:benign) THEN
                  aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                      '<13,10>Old IMEI ' & Clip(job:esn)
                  aud:ref_number    = job:ref_number
                  aud:date          = Today()
                  aud:time          = Clock()
                  access:users.clearkey(use:password_key)
                  use:password =glo:password
                  access:users.fetch(use:password_key)
                  aud:user = use:user_code
                  aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                  if access:audit.insert()
                      access:audit.cancelautoinc()     
                  end            
              END
              access:JOBSE.clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                  IF (access:JOBSE.primerecord() = Level:Benign) THEN
                      jobe:RefNumber  = job:Ref_Number
                      IF (access:JOBSE.tryinsert()) THEN
                          access:JOBSE.cancelautoinc()
                      END
                  END
              END
              IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                  jobe:PRE_RF_BOARD_IMEI = job:ESN
                  Access:jobse.update()
              END
              job:ESN = tmp_RF_Board_IMEI
              Access:jobs.update()
          END
          ! End Change 2116 BE(26/06/03)
      Else !wpr:Part_Number <> 'ADJUSTMENT'
          !An ajustment, do nothing
          wpr:Date_Ordered    = Today()
      End !wpr:Part_Number <> 'ADJUSTMENT'
  End !If thiswindow.Request = InsertRecord
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If thiswindow.Request <> Insertrecord
      !Is this a part from stock?
      If wpr:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = wpr:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              tmp:StockNumber = sto:Ref_Number
              
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          
      End !If wpr:Part_Ref_Number <> ''
      !Normal part
      If wpr:Date_Ordered <> '' And wpr:Order_Number = '' and wpr:Pending_Ref_Number = ''
          If wpr:Quantity > Quantity_Temp
              !Stock part?
              If tmp:StockNumber
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = tmp:StockNumber
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
                      If sto:Quantity_Stock < (wpr:Quantity - Quantity_Temp)
                          !There isn't enough in stock
  
                          Beep(Beep:SystemQuestion)  ;  Yield()
                          Case Message('You have increased the quantity required, but there are '&|
                                  'insufficient items in stock.'&|
                                  '||Do you wish to ORDER the remaining items, or RE-ENTER the '&|
                                  'quantity required?', |
                                  'ServiceBase 2000', Icon:Question, |
                                   'Order|Re-enter', 2, 0)
                          Of 1  ! Name: Order
                              !Is there any existing pending part to add to?
                              tmp:PendingPart = 0
                              Save_war_ali_ID = Access:WARPARTS_ALIAS.SaveFile()
                              Access:WARPARTS_ALIAS.ClearKey(war_ali:RefPartRefNoKey)
                              war_ali:Ref_Number      = job:Ref_Number
                              war_ali:Part_Ref_Number = tmp:StockNumber
                              Set(war_ali:RefPartRefNoKey,war_ali:RefPartRefNoKey)
                              Loop
                                  If Access:WARPARTS_ALIAS.NEXT()
                                     Break
                                  End !If
                                  If war_ali:Ref_Number      <> job:Ref_Number      |
                                  Or war_ali:Part_Ref_Number <> tmp:StockNumber      |
                                      Then Break.  ! End If
                                  If war_ali:Record_Number = wpr:Record_Number
                                      Cycle
                                  End !If wpr_ali:Record_Number = wpr:Record_Number
                                  If war_ali:Pending_Ref_Number <> ''
                                      tmp:PendingPart = war_ali:Record_Number
                                      Break
                                  End !If wpr:Pending_Ref_Number <> ''
                              End !Loop
                              Access:WARPARTS_ALIAS.RestoreFile(Save_war_ali_ID)                        
  
                              tmp:NewQuantity     = (wpr:Quantity - Quantity_Temp) - sto:Quantity_Stock
  
                              If tmp:PendingPart
                                  tmp:CurrentState    = Getstate(WARPARTS)
                                  
                                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                                  wpr:Record_Number   = tmp:PendingPart
                                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                                      !Found
                                      wpr:Quantity += tmp:NewQuantity
                                      Access:WARPARTS.Update()
  
                                      tmp:CreateNewOrder = 0
                                      !Find pending order
                                      Case def:SummaryOrders    
                                          Of 0 !Old Wauy
                                              Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                                              ope:Ref_Number  = wpr:Pending_Ref_Number
                                              If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                  !Found
                                                  ope:Quantity += tmp:NewQuantity
                                                  Access:ORDPEND.Update()
  
                                              Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                  !Error
                                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                                  Do CreateNewOrder
                                              End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                          Of 1 !New Way
                                              Access:ORDPEND.ClearKey(ope:Supplier_Key)
                                              ope:Supplier    = wpr:Supplier
                                              ope:Part_Number = wpr:Part_Number
                                              If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                                  ope:Quantity += tmp:NewQuantity
                                                  Access:ORDPEND.Update()
                                              Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Lev !Found
                                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                                  Do CreateNewOrder
  
                                              End !If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                      End !Case def:SummaryOrders    
  
                                      RestoreState(WARPARTS,tmp:CurrentState)
                                      FreeState(WARPARTS,tmp:CurrentState)
                                  Else! If Access:PARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:PARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                                  
                              Else !If tmp:PendingPart
                                  Do CreateNewOrder
  
                                  !Create new pending part
                                  glo:Select1 = 'NEW PENDING'
                                  glo:Select2 = wpr:Part_Ref_Number
                                  glo:Select3 = (wpr:Quantity - Quantity_Temp) - sto:Quantity_Stock
                                  glo:Select4 = ope:Ref_Number
                                  wpr:Quantity    = Quantity_Temp + sto:Quantity_Stock
                                  
                              End !If tmp:PendingPart
                          Of 2  ! Name: Re-enter  (Default)
                              wpr:Quantity    = Quantity_Temp
                              Select(?wpr:Quantity)
                              Cycle
                          End !CASE
  
                      End !If sto:Quantity_Stock < (wpr:Quantity - Quantity_Temp)
  
                      !There is enough in stock, take it.
                      If sto:Quantity_Stock > (wpr:Quantity - Quantity_Temp)
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              sto:Quantity_Stock -= (wpr:Quantity - Quantity_Temp)
                              If Access:STOCK.Update() = Level:Benign
                                  If Access:STOHIST.PrimeRecord() = Level:Benign
                                      shi:Ref_Number          = sto:Ref_Number
                                      shi:Transaction_Type    = 'DEC'
                                      shi:Despatch_Note_Number    = wpr:Despatch_Note_NUmber
                                      shi:Quantity            = wpr:Quantity - Quantity_Temp
                                      shi:Date                = Today()
                                      shi:Purchase_Cost       = wpr:Purchase_Cost
                                      shi:Sale_Cost           = wpr:Sale_Cost
                                      shi:Retail_Cost         = wpr:Retail_Cost
                                      shi:Job_Number          = job:Ref_Number
                                      Access:USERS.Clearkey(use:Password_Key)
                                      use:Password    = glo:Password
                                      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Found
                                      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      shi:User                = use:User_Code
                                      shi:Notes               = 'STOCK DECREMENTED'
                                      If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:STOHIST.TryInsert() = Level:Benign
                                  End !If Access:STOHIST.PrimeRecord() = Level:Benign
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          
                      End !If sto:Quantity_Stock > (wpr:Quantity - Quantity_Temp)
                  Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  
              End !If tmp:StockNumber
          End !If quantity increased
          !Quantity decreased
          If Quantity_Temp > wpr:Quantity
              ! Start Change 2466 BE(28/11/03)
              !If tmp:StockNumber
              IF ((tmp:StockNumber) AND (wpr:Exclude_From_Order <> 'YES')) THEN
              ! End Change 2466 BE(28/11/03)
                  !A Stock part. Put back into stock?
                  Beep(Beep:SystemQuestion)  ;  Yield()
                  ! Start 2632 BE(20/05/03)
                  !Case Message('You have decreased the quantity required. '&|
                  !        '||What do you wish to do with the excess? RETURN it to stock, '&|
                  !        'or SCRAP it?', |
                  !        'ServiceBase 2000', Icon:Question, |
                  !         'Return|Scrap|Cancel', 3, 0)
                  msg1 = 'You have decreased the quantity required. '
                  msg2 = 'What do you wish to do with the excess? RETURN it to stock, or SCRAP it?'
                  ScrapDialog(msg1, msg2, 'Return', 'Scrap', 'Cancel', result)
                  CASE result
                  ! End 2632 BE(20/05/03)
                  Of 1  ! Name: Return
                      Beep(Beep:SystemQuestion)  ;  Yield()
                      Case Message('This item was originally taked from location: '&|
                              Clip(sto:Location)&'.'&|
                              '||Do you wish to return it to it''s ORIGINAL location, or '&|
                              'to a NEW location?', |
                              'ServiceBase 2000', Icon:Question, |
                               'Original|New|Cancel', 3, 0)
                      Of 1  ! Name: Original
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              sto:Quantity_Stock += Quantity_Temp - wpr:Quantity
                              If Access:STOCK.Update() = Level:Benign
                                  If access:stock.update() = Level:Benign
                                      get(stohist,0)
                                      if access:stohist.primerecord() = level:benign
                                          shi:ref_number           = sto:ref_number
                                          access:users.clearkey(use:password_key)
                                          use:password              = glo:password
                                          access:users.fetch(use:password_key)
                                          shi:user                  = use:user_code    
                                          shi:date                 = today()
                                          shi:transaction_type     = 'REC'
                                          shi:despatch_note_number = wpr:despatch_note_number
                                          shi:job_number           = job:Ref_number
                                          shi:quantity             = Quantity_Temp - wpr:Quantity
                                          shi:purchase_cost        = wpr:purchase_cost
                                          shi:sale_cost            = wpr:sale_cost
                                          shi:retail_cost          = wpr:retail_cost
                                          shi:information          = 'WARRANTY PART AMENDED ON JOB'
                                          if access:stohist.insert()
                                             access:stohist.cancelautoinc()
                                          end
                                      end!if access:stohist.primerecord() = level:benign
                                  End!If access:stock.update = Level:Benign
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          
                      Of 2  ! Name: New
                          glo:select1 = ''
                          Pick_New_Location
                          If glo:select1 <> ''
                              access:stock.clearkey(sto:location_part_description_key)
                              sto:location    = glo:select1
                              sto:part_number = wpr:part_number
                              sto:description = wpr:description
                              If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  sto:quantity_stock   += Quantity_Temp - wpr:Quantity
                                  access:stock.update()
                                  do_delete# = 1
                                  get(stohist,0)
                                  if access:stohist.primerecord() = level:benign
                                      shi:ref_number           = sto:ref_number
                                      access:users.clearkey(use:password_key)
                                      use:password              = glo:password
                                      access:users.fetch(use:password_key)
                                      shi:user                  = use:user_code    
                                      shi:date                 = today()
                                      shi:transaction_type     = 'ADD'
                                      shi:despatch_note_number = wpr:despatch_note_number
                                      shi:job_number           = job:Ref_number
                                      shi:quantity             = Quantity_Temp - wpr:Quantity
                                      shi:purchase_cost        = wpr:purchase_cost
                                      shi:sale_cost            = wpr:sale_cost
                                      shi:retail_cost          = wpr:retail_cost
                                      shi:information          = 'WARRANTY PART AMENDED ON JOB'
                                      if access:stohist.insert()
                                         access:stohist.cancelautoinc()
                                      end
                                  end!if access:stohist.primerecord() = level:benign
                              Else!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  Beep(Beep:SystemQuestion)  ;  Yield()
                                  ! Start 2632 BE(20/05/03)
                                  !Case Message('Cannot find the selected part in location: '&|
                                  !        Clip(glo:Select1)&'.'&|
                                  !        '||Do you wish add this part as a NEW item in that location '&|
                                  !        'or SCRAP it?', |
                                  !        'ServiceBase 2000', Icon:Question, |
                                  !         'New|Scrap|Cancel', 3, 0)
                                  msg1 = 'Cannot find the selected part in location: ' & Clip(glo:Select1) & '.'
                                  msg2 = 'Do you wish add this part as a NEW item in that location or SCRAP it?'
                                  ScrapDialog(msg1, msg2, 'New', 'Scrap', 'Cancel', result)
                                  CASE result
                                  ! End 2632 BE(20/05/03)
                                  Of 1  ! Name: New
                                      glo:select2  = ''
                                      glo:select3  = ''
                                      Pick_Locations
                                      If glo:select1 <> ''
                                          do_delete# = 1
                                          Get(stock,0)
                                          If access:stock.primerecord() = Level:Benign
                                              sto:part_number = wpr:part_number
                                              sto:description = wpr:description
                                              sto:supplier    = wpr:supplier
                                              sto:purchase_cost   = wpr:purchase_cost
                                              sto:sale_cost   = wpr:sale_cost
                                              sto:shelf_location  = glo:select2
                                              sto:manufacturer    = job:manufacturer
                                              sto:location    = glo:select1
                                              sto:second_location = glo:select3
                                              If access:stock.insert() = Level:Benign
                                                  get(stohist,0)
                                                  if access:stohist.primerecord() = level:benign
                                                      shi:ref_number           = sto:ref_number
                                                      access:users.clearkey(use:password_key)
                                                      use:password              = glo:password
                                                      access:users.fetch(use:password_key)
                                                      shi:user                  = use:user_code    
                                                      shi:date                 = today()
                                                      shi:transaction_type     = 'ADD'
                                                      shi:despatch_note_number = wpr:despatch_note_number
                                                      shi:job_number           = job:Ref_number
                                                      shi:quantity             = Quantity_Temp - wpr:Quantity
                                                      shi:purchase_cost        = wpr:purchase_cost
                                                      shi:sale_cost            = wpr:sale_cost
                                                      shi:retail_cost          = wpr:retail_cost
                                                      shi:information          = 'WARRANTY PART AMENDED ON JOB'
                                                      if access:stohist.insert()
                                                         access:stohist.cancelautoinc()
                                                      end
                                                  end!if access:stohist.primerecord() = level:benign
                                              End!If access:stock.insert() = Level:Benign
                                          End!If access:stock.primerecord() = Level:Benign
                                      End!If glo:select1 = ''
                                  Of 2  ! Name: Scrap
  
                                      ! start 2632 BE(20/05/03)
                                      DO WriteScrapAuditRecord
                                      ! start 2632 BE(20/05/03)
  
                                  Of 3  ! Name: Cancel  (Default)
                                      ! Start Change 2600 BE(08/05/03)
                                      wpr:Quantity    = Quantity_Temp
                                      Select(?wpr:Quantity)
                                      Cycle
                                      ! End Change 2600 BE(08/05/03)
                                  End !CASE
                              End !If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                          End !If glo:select1 <> ''
                      Of 3  ! Name: Cancel  (Default)
                          ! Start Change 2600 BE(08/05/03)
                          wpr:Quantity    = Quantity_Temp
                          Select(?wpr:Quantity)
                          Cycle
                          ! End Change 2600 BE(08/05/03)
                      End !CASE
                  Of 2  ! Name: Scrap
  
                      ! start 2632 BE(20/05/03)
                      DO WriteScrapAuditRecord
                      ! start 2632 BE(20/05/03)
  
                  Of 3  ! Name: Cancel  (Default)
                      ! Start Change 2600 BE(08/05/03)
                      wpr:Quantity    = Quantity_Temp
                      Select(?wpr:Quantity)
                      Cycle
                      ! End Change 2600 BE(08/05/03)
                  End !CASE
              Else !If tmp:StockNumber
  
              End !If tmp:StockNumber
          End !If Quantity_Temp > wpr:Quantity
      End !If wpr:Date_Ordered <> '' And wpr:Order_Number = '' and wpr:Pending_Ref_Number = ''    
  
      !This is a pending part
      If wpr:Pending_Ref_Number <> '' and wpr:Order_Number = ''
          !Quantity increased
          If wpr:Quantity > Quantity_Temp
              !Find a pending order
              Case def:SummaryOrders
                  Of 0 !Old Way
                      Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                      ope:Ref_Number  = wpr:Pending_Ref_Number
                      If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Found
  
                          ope:Quantity += wpr:Quantity - Quantity_Temp
                          Access:ORDPEND.Update()
                      Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          !Cannot find a pending order, lets' make one
                          tmp:NewQuantity = wpr:Quantity - Quantity_Temp
                          Do CreateNewOrder
                          !Create new pending part
                          glo:Select1 = 'NEW PENDING'
                          glo:Select2 = wpr:Part_Ref_Number
                          glo:Select3 = (wpr:Quantity - Quantity_Temp)
                          glo:Select4 = ope:Ref_Number
                          wpr:Quantity   = Quantity_Temp 
  
                      End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                      
                  Of 1 !New Way
                      Access:ORDPEND.ClearKey(ope:Supplier_Key)
                      ope:Supplier    = wpr:Supplier
                      ope:Part_Number = wpr:Part_Number
                      If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                          !Found
                          ope:Quantity += wpr:Quantity - Quantity_Temp
                          Access:ORDPEND.Update()
                      Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          tmp:NewQuantity = wpr:Quantity - Quantity_Temp
                          Do CreateNewOrder
                      End!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
              End !Case def:SummaryOrders
          End !If wpr:Quantity > Quantity_Temp
          !Quantity decreased
          If wpr:Quantity < Quantity_Temp
              !Find a pending order
              Case def:SummaryOrders
                  Of 0 !Old Way
                      Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                      ope:Ref_Number  = wpr:Pending_Ref_Number
                      If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Found
                          ope:Quantity -= (Quantity_Temp - wpr:Quantity)
                          If ope:Quantity <= 0
                              Delete(ORDPEND)
                          Else !If ope:Quantity <= 0
                              Access:ORDPEND.Update()
                          End !If ope:Quantity <= 0
  
                      Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                      
                  Of 1 !New Way
                      Access:ORDPEND.Clearkey(ope:Supplier_Key)
                      ope:Supplier    = wpr:Supplier
                      ope:Part_Number = wpr:Part_Number
                      If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                          !Found
                          ope:Quantity -= (Quantity_Temp - wpr:Quantity)
                          If ope:Quantity <= 0
                              Delete(ORDPEND)
                          Else !If ope:Quantity <= 0
                              Access:ORDPEND.Update()
                          End !If ope:Quantity <= 0
                      Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                      
              End !Case def:SummaryOrders
          End !If wpr:Quantity < Quantity_Temp
      End !If wpr:Pending_Ref_Number <> '' and wpr:Order_Number = ''
  End !If thiswindow.Request <> Insertrecord
      
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Warranty_Part')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?WPR:Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?WPR:Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?WPR:Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?WPR:Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?WPR:Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?WPR:Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?WPR:Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?WPR:Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?WPR:Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?WPR:Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?WPR:Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?WPR:Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          0{prop:buffer} = 1
      If wpr:order_number <> ''
          ?title{prop:text} = 'Warranty Part - On Order'
      End
      If wpr:pending_ref_number <> ''
          ?title{prop:text} = 'Warranty Part - Pending Order'
      End
      If wpr:date_received <> ''
          ?title{prop:text} = 'Warranty Part - Received'
      End
      IF wpr:exclude_from_order = 'YES'
          ?title{prop:text} = 'Warranty Part - Excluded From Order'
      End!IF wpr:exclude_from_order = 'YES'
      
      !Save Fields
      quantity_temp = wpr:quantity
      tmp:part_number   = wpr:part_number
      tmp:description   = wpr:description
      tmp:supplier      = wpr:supplier
      tmp:purchase_cost = wpr:purchase_cost
      tmp:sale_cost     = wpr:sale_cost
      main_part_temp  = wpr:main_part
      adjustment_temp = wpr:adjustment
      tmp:exclude = wpr:exclude_from_order
      !Security Check
      Check_access('JOB PART COSTS - EDIT',x")
      If x" = False
          ?wpr:purchase_cost{prop:readonly} = 1
          ?wpr:sale_cost{prop:readonly} = 1
          ?wpr:purchase_cost{prop:color} = color:silver
          ?wpr:sale_cost{prop:color} = color:silver
      End!"If x" = False
      
      ! Start Change 2343 BE(18/03/03)
      Check_access('JOB - AMEND PART DETAIL', x")
      IF (x" = False) THEN
          ?wpr:Part_Number{prop:readonly} = 1
          ?wpr:description{prop:readonly} = 1
          ?wpr:Part_Number{prop:color} = color:silver
          ?wpr:description{prop:color} = color:silver
      End
      ! End Change 2343 BE(18/03/03)
      ! Fault Coding (Hopefully)
      If job:manufacturer = 'NEC'
          Unhide(?wpr:main_part)
      End!If job:manufacturer = 'NEC'
      fault_codes_required_temp = 'NO'
      required# = 0
      access:chartype.clearkey(cha:charge_type_key)
      cha:charge_type = job:warranty_charge_type
      If access:chartype.fetch(cha:charge_type_key) = Level:Benign
          If cha:force_warranty = 'YES'
              required# = 1
          End
      end !if
      
      
      found# = 0
      setcursor(cursor:wait)
      save_map_id = access:manfaupa.savefile()
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = job:manufacturer
      set(map:field_number_key,map:field_number_key)
      loop
          if access:manfaupa.next()
             break
          end !if
          if map:manufacturer <> job:manufacturer      |
              then break.  ! end if
      
          Case map:field_number
              Of 1
                  found# = 1
                  Unhide(?wpr:fault_code1:prompt)
                  ?wpr:fault_code1:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar)
                      ?popcalendar{prop:xpos} = 212
                      Unhide(?wpr:fault_code1:2)
                      ?wpr:fault_code1:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code1)
                      If map:lookup = 'YES'
                          Unhide(?button4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code1{prop:req} = 1
                      ?wpr:fault_code1:2{prop:req} = 1
                  else
                      ?wpr:fault_code1{prop:req} = 0
                      ?wpr:fault_code1:2{prop:req} = 0
                  End
              Of 2
                  found# = 1
                  Unhide(?wpr:fault_code2:prompt)
                  ?wpr:fault_code2:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:2)
                      ?popcalendar:2{prop:xpos} = 212
                      Unhide(?wpr:fault_code2:2)
                      ?wpr:fault_code2:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code2)
                      If map:lookup = 'YES'
                          Unhide(?button4:2)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code2{prop:req} = 1
                      ?wpr:fault_code2:2{prop:req} = 1
                  else
                      ?wpr:fault_code2{prop:req} = 0
                      ?wpr:fault_code2:2{prop:req} = 0
                  End
              Of 3
                  found# = 1
                  Unhide(?wpr:fault_code3:prompt)
                  ?wpr:fault_code3:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:3)
                      ?popcalendar:3{prop:xpos} = 212
                      Unhide(?wpr:fault_code3:2)
                      ?wpr:fault_code3:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code3)
                      If map:lookup = 'YES'
                          Unhide(?button4:3)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code3{prop:req} = 1
                      ?wpr:fault_code3:2{prop:req} = 1
                  else
                      ?wpr:fault_code3{prop:req} = 0
                      ?wpr:fault_code3:2{prop:req} = 0
                  End
              Of 4
                  found# = 1
                  Unhide(?wpr:fault_code4:prompt)
                  ?wpr:fault_code4:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:4)
                      ?popcalendar:4{prop:xpos} = 212
                      Unhide(?wpr:fault_code4:2)
                      ?wpr:fault_code4:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code4)
                      If map:lookup = 'YES'
                          Unhide(?button4:4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code4{prop:req} = 1
                      ?wpr:fault_code4:2{prop:req} = 1
                  else
                      ?wpr:fault_code4{prop:req} = 0
                      ?wpr:fault_code4:2{prop:req} = 0
                  End
              Of 5
                  found# = 1
                  Unhide(?wpr:fault_code5:prompt)
                  ?wpr:fault_code5:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:5)
                      ?popcalendar:5{prop:xpos} = 212
                      Unhide(?wpr:fault_code5:2)
                      ?wpr:fault_code5:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code5)
                      If map:lookup = 'YES'
                          Unhide(?button4:5)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code5{prop:req} = 1
                      ?wpr:fault_code5:2{prop:req} = 1
                  else
                      ?wpr:fault_code5{prop:req} = 0
                      ?wpr:fault_code5:2{prop:req} = 0
                  End
              Of 6
                  found# = 1
                  Unhide(?wpr:fault_code6:prompt)
                  ?wpr:fault_code6:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:6)
                      ?popcalendar:6{prop:xpos} = 212
                      Unhide(?wpr:fault_code6:2)
                      ?wpr:fault_code6:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code6)
                      If map:lookup = 'YES'
                          Unhide(?button4:6)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code6{prop:req} = 1
                      ?wpr:fault_code6:2{prop:req} = 1
                  else
                      ?wpr:fault_code6{prop:req} = 0
                      ?wpr:fault_code6:2{prop:req} = 0
                  End
              Of 7
                  found# = 1
                  Unhide(?wpr:fault_code7:prompt)
                  ?wpr:fault_code7:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:7)
                      ?popcalendar:7{prop:xpos} = 212
                      Unhide(?wpr:fault_code7:2)
                      ?wpr:fault_code7:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code7)
                      If map:lookup = 'YES'
                          Unhide(?button4:7)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code7{prop:req} = 1
                      ?wpr:fault_code7:2{prop:req} = 1
                  else
                      ?wpr:fault_code7{prop:req} = 0
                      ?wpr:fault_code7:2{prop:req} = 0
                  End
      
              Of 8
                  found# = 1
                  Unhide(?wpr:fault_code8:prompt)
                  ?wpr:fault_code8:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:8)
                      ?popcalendar:8{prop:xpos} = 212
                      Unhide(?wpr:fault_code8:2)
                      ?wpr:fault_code8:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code8)
                      If map:lookup = 'YES'
                          Unhide(?button4:8)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code8{prop:req} = 1
                      ?wpr:fault_code8:2{prop:req} = 1
                  else
                      ?wpr:fault_code8{prop:req} = 0
                      ?wpr:fault_code8:2{prop:req} = 0
                  End
      
              Of 9
                  found# = 1
                  Unhide(?wpr:fault_code9:prompt)
                  ?wpr:fault_code9:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:9)
                      ?popcalendar:9{prop:xpos} = 212
                      Unhide(?wpr:fault_code9:2)
                      ?wpr:fault_code9:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code9)
                      If map:lookup = 'YES'
                          Unhide(?button4:9)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code9{prop:req} = 1
                      ?wpr:fault_code9:2{prop:req} = 1
                  else
                      ?wpr:fault_code9{prop:req} = 0
                      ?wpr:fault_code9:2{prop:req} = 0
                  End
      
              Of 10
                  found# = 1
                  Unhide(?wpr:fault_code10:prompt)
                  ?wpr:fault_code10:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:10)
                      ?popcalendar:10{prop:xpos} = 212
                      Unhide(?wpr:fault_code10:2)
                      ?wpr:fault_code10:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code10)
                      If map:lookup = 'YES'
                          Unhide(?button4:10)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code10{prop:req} = 1
                      ?wpr:fault_code10:2{prop:req} = 1
                  else
                      ?wpr:fault_code10{prop:req} = 0
                      ?wpr:fault_code10:2{prop:req} = 0
                  End
      
              Of 11
                  found# = 1
                  Unhide(?wpr:fault_code11:prompt)
                  ?wpr:fault_code11:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:11)
                      ?popcalendar:11{prop:xpos} = 212
                      Unhide(?wpr:fault_code11:2)
                      ?wpr:fault_code11:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code11)
                      If map:lookup = 'YES'
                          Unhide(?button4:11)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code11{prop:req} = 1
                      ?wpr:fault_code11:2{prop:req} = 1
                  else
                      ?wpr:fault_code11{prop:req} = 0
                      ?wpr:fault_code11:2{prop:req} = 0
                  End
      
              Of 12
                  found# = 1
                  Unhide(?wpr:fault_code12:prompt)
                  ?wpr:fault_code12:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:12)
                      ?popcalendar:12{prop:xpos} = 212
                      Unhide(?wpr:fault_code12:2)
                      ?wpr:fault_code12:2{prop:xpos} = 84
                  Else
                      Unhide(?wpr:fault_code12)
                      If map:lookup = 'YES'
                          Unhide(?button4:12)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?wpr:fault_code12{prop:req} = 1
                      ?wpr:fault_code12:2{prop:req} = 1
                  else
                      ?wpr:fault_code12{prop:req} = 0
                      ?wpr:fault_code12:2{prop:req} = 0
                  End
      
          End !Case map:field_number
      end !loop
      access:manfaupa.restorefile(save_map_id)
      setcursor()
      If found# = 1
          unhide(?WPR:Fault_Codes_Checked)
      Else
          Unhide(?FaultCodesText)
      End!If found# = 1
      
      !Is this and adjustment
      If thiswindow.request = Insertrecord
          wpr:quantity = 1
          If glo:select1 = 'ADJUSTMENT'
              wpr:adjustment          = 'YES'
              wpr:exclude_from_order  = 'YES'
              wpr:ref_number          = glo:select2
              wpr:Purchase_Cost       = ''
              wpr:Sale_Cost           = ''
              wpr:quantity            = 1
              Disable(?adjustmentgroup)
              Access:manufact.clearkey(man:manufacturer_key)
              man:manufacturer    = job:manufacturer
              If access:manufact.tryfetch(man:manufacturer_key) = Level:Benign
                  ! Start Change 2414 BE(26/03/03)
                  !IF man:includeadjustment = 'YES' And man:adjustpart <> 1
                  IF ((man:includeadjustment = 'YES' AND man:adjustpart <> 1) OR (man:includeadjustment = 'NO')) THEN
                  ! Start Change 2414 BE(26/03/03)
                      wpr:Part_Number = 'ADJUSTMENT'
                      wpr:Description = 'ADJUSTMENT'
                      Disable(?wpr:Part_Number)
                      Disable(?wpr:description)
                      Disable(?Browse_Stock_Button)
                  End!IF man:skip_adjustment = 'YES'
              End!If access:manufact.tryfetch(man:manufacturer_key)
          End!If glo:select1 = 'ADJUSTMENT' And thiswindow.request = Insertrecord
      
      Else!If thiswindow.request = Insertrecord
          If wpr:adjustment = 'YES'
              Disable(?adjustmentgroup)
          End!If wpr:adjustment = 'YES'
          Disable(?browse_stock_button)
          Disable(?wpr:exclude_from_order)
          Disable(?wpr:Part_Number)
          Disable(?wpr:description)
      End!If thiswindow.request = Insertrecord
      !ThisMakeOver.SetWindow(Win:Form)
      Display()
      If ThisWindow.Request <> InsertRecord
      
          !There was never any code to allow a user to change
          !the details of a part, apart from the quantity,
          !once it is inserted. So I'm going to disable the fields.
          ?browse_stock_button{prop:Hide} = 1
          ?wpr:Part_Number{prop:Disable} = 1
          ?wpr:Description{prop:Disable} = 1
          ?wpr:Supplier{prop:Disable} = 1
          !�?LookupSupplier{prop:Disable} = 1
          ?wpr:Purchase_Cost{prop:Disable} = 1
          ?wpr:Sale_Cost{prop:Disable} = 1
      End !ThisWindow.Request <> InsertRecord
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

