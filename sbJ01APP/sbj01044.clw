

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01044.INC'),ONCE        !Local module procedure declarations
                     END


ExchangeProcess PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ConsignmentNumber STRING(30)
tmp:Close            BYTE(0)
hMenu                LONG
menuItemCount        LONG
hWnd                 LONG
pos                  LONG
mInfo                LIKE(MENUITEMINFO)
MenuType             CSTRING(260)
window               WINDOW('Exchange Process'),AT(,,215,83),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),ALRT(F8Key),ALRT(F10Key),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,208,48),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Do you wish to scrap the original unit and automatically close the job down?'),AT(8,8,200,20),USE(?Prompt1)
                           PROMPT('Consignment Number'),AT(8,32),USE(?tmp:ConsignmentNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,32,124,10),USE(tmp:ConsignmentNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Consignment Number'),TIP('Consignment Number'),UPR
                         END
                       END
                       PANEL,AT(4,56,208,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&Yes [F8]'),AT(88,60,60,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('No [F10]'),AT(148,60,60,16),USE(?Cancel),LEFT,ICON('cancel.gif'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ConsignmentNumber)

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:ConsignmentNumber:Prompt{prop:FontColor} = -1
    ?tmp:ConsignmentNumber:Prompt{prop:Color} = 15066597
    If ?tmp:ConsignmentNumber{prop:ReadOnly} = True
        ?tmp:ConsignmentNumber{prop:FontColor} = 65793
        ?tmp:ConsignmentNumber{prop:Color} = 15066597
    Elsif ?tmp:ConsignmentNumber{prop:Req} = True
        ?tmp:ConsignmentNumber{prop:FontColor} = 65793
        ?tmp:ConsignmentNumber{prop:Color} = 8454143
    Else ! If ?tmp:ConsignmentNumber{prop:Req} = True
        ?tmp:ConsignmentNumber{prop:FontColor} = 65793
        ?tmp:ConsignmentNumber{prop:Color} = 16777215
    End ! If ?tmp:ConsignmentNumber{prop:Req} = True
    ?tmp:ConsignmentNumber{prop:Trn} = 0
    ?tmp:ConsignmentNumber{prop:FontStyle} = font:Bold
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ExchangeProcess',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:ConsignmentNumber',tmp:ConsignmentNumber,'ExchangeProcess',1)
    SolaceViewVars('tmp:Close',tmp:Close,'ExchangeProcess',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNumber:Prompt;  SolaceCtrlName = '?tmp:ConsignmentNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ConsignmentNumber;  SolaceCtrlName = '?tmp:ConsignmentNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ExchangeProcess')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ExchangeProcess')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
   tmp:ConsignmentNumber = GETINI('CAID','ExchangeBER',,CLIP(PATH())&'\SB2KDEF.INI')
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
     hWnd = 0{PROP:Handle}
     !get the system menu handle
     hMenu = GetSystemMenu(hwnd, 0)
  
     !loop backwards through the menu
     LOOP I# = GetMenuItemCount(hMenu) To 0 BY -1
        minfo.cbSize     = Size(mInfo)
        minfo.fMask      = BOR(MIIM_TYPE,MIIM_ID)
        minfo.fType      = MFT_STRING
        MenuType         = ALL(' ',255)
        minfo.dwTypeData = Address(MenuType)
        mInfo.cch        = Size(MenuType)
        !Retrieve the current MENUITEMINFO.
        !Specifying fByPosition=True indicates
        !uItem points to an item by position.
        !Returns 1 if successful
        If GetMenuItemInfo(hMenu, I#, True, mInfo) = 1 Then
          !The close command has an ID of 61536. Once
          !that has been deleted, in a MDI Child window
          !two separators would remain (in a SDI one
          !separator would remain).
          !
          !To assure that this code will cover both
          !MDI and SDI system menus, 1 is subtracted
          !from the item number (c) to remove either
          !the top separator (of the two that will
          !remain in a MDIChild), or the single SDI
          !one (that would remain if this code was
          !applied against a SDI form.)
           If (mInfo.wID = 61536) Then
              a# = RemoveMenu(hMenu, I#,BOR(MF_REMOVE,MF_BYPOSITION))
              b# = RemoveMenu(hMenu, I#-1,BOR(MF_REMOVE,MF_BYPOSITION))
           End
        End
     END !LOOP
  
     !force a redraw of the non-client
     !area of the form to cause the
     !disabled X to paint correctly
     c# = DrawMenuBar(hwnd)
     Display
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ExchangeProcess',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      If tmp:ConsignmentNumber = ''
          Select(?tmp:ConsignmentNumber)
      Else !tmp:ConsignmentNumber = ''
          tmp:Close   = 1
          PUTINI('CAID','ExchangeBER',tmp:ConsignmentNumber,CLIP(PATH()) & '\SB2KDEF.INI')
          Post(Event:CloseWindow)
      End !tmp:ConsignmentNumber = ''
    OF ?Cancel
      ThisWindow.Update
      tmp:ConsignmentNumber = ''
      tmp:Close   = 1
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ExchangeProcess')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      If tmp:Close = 0
          Cycle
      End !tmp:Close = 0
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F8Key
              Post(Event:Accepted,?Ok)
          Of F10Key
              Post(Event:Accepted,?Cancel)
      End !KeyCode()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

