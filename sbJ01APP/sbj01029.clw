

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJ01029.INC'),ONCE        !Local module procedure declarations
                     END


PickValidate         PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'PickValidate')      !Add Procedure to Log
  end


    !**** TH 02/04 Picking check
    pick = true

    !Is this job an estimate, if so change the status
    If job:Estimate = 'YES' and job:Chargeable_Job = 'YES' then
        if job:Estimate_Accepted <> 'YES' then
            ! Start Change 3922 BE(08/03/04)
            !GetStatus(505,1,'JOB')
            Total_Price('E', vat", total", balance")
            Total_Price('C', vat2", total2", balance2")
            total$ = total" + total2"
            if ((job:estimate_if_over = 0.0) OR (total$ > job:estimate_if_over)) then
                GetStatus(505,1,'JOB')
                job:Estimate = 'YES'
                Case MessageEx('This job exceeds the maximum estimate cost permitted. Cannot print pick note.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    Of 1 ! &OK Button
                End!Case MessageEx
                pick = false
            else
                if job:Estimate_Rejected <> 'YES' then
                    Case MessageEx('This is an estimate job. The estimate has not yet been accepted or rejected. Cannot print pick note.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                else
                    Case MessageEx('This is an estimate job. The estimate has been rejected. Cannot print pick note.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
            end
            ! End Change 3922 BE(08/03/04)
        end
    End !If job:Estimate = 'YES'

    if pick then
        access:users.clearkey(use:password_key)
        use:password = glo:password
        access:users.fetch(use:password_key)
        if use:user_code <> job:Engineer
            Case MessageEx('Logged in user does not match job engineer. Cannot print pick note.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                Of 1 ! &OK Button
            End!Case MessageEx
            pick = false
        else
            access:status.clearkey(sts:status_key)
            sts:status = job:current_status
            if access:status.fetch(sts:status_key) = Level:Benign
                if ~sts:PickNoteEnable then
                    Case MessageEx('The current job status does not have picking notes enabled. Cannot print pick note.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
            end
        end
    end

    if pick then
        if job:Manufacturer = '' or job:Model_Number = '' then
    !        if job:Manufacturer = '' then message ('manufacturer blank').
    !        if job:Model_Number = '' then message ('model no blank').
    !        if job:Repair_Type = '' then message ('repair type blank').
            Case MessageEx('Cannot print pick note, one of the following fields is empty: Manufacturer, Model Number.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                Of 1 ! &OK Button
            End!Case MessageEx
            pick = false
        else
            if job:Chargeable_job = 'YES' and job:Warranty_Job = 'YES' then
                if job:Charge_Type = '' or job:Warranty_Charge_Type = '' then
                    Case MessageEx('Cannot print pick note, one of the following fields is empty: Charge Types.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
                if job:Repair_Type = '' or job:Repair_Type_Warranty = '' then
                    Case MessageEx('Cannot print pick note, one of the following fields is empty: Repair Types.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
            end
            if job:Chargeable_job = 'YES' and job:Warranty_Job <> 'YES' then
                if job:Charge_Type = '' then
                    Case MessageEx('Cannot print pick note, the following field is empty: Charageable Charge Type.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
                if job:Repair_Type = ''then
                    Case MessageEx('Cannot print pick note, the following field is empty: Charageable Repair Type.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
            end
            if job:Chargeable_job <> 'YES' and job:Warranty_Job = 'YES' then
                if job:Warranty_Charge_Type = '' then
                    Case MessageEx('Cannot print pick note, the following field is empty: Warranty Charge Type.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
                if job:Repair_Type_Warranty = '' then
                    Case MessageEx('Cannot print pick note, the following field is empty: Warranty Repair Type.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    pick = false
                end
            end
        end
    end

    if pick then !we can get on with dealing with printing the picknote(s)
        !*** Picking Note Code *** TH
        access:USERS.clearkey(use:password_key)
        use:Password = glo:Password
        access:USERS.fetch(use:password_key)

        if access:USERS.fetch(use:password_key) then !failure - problem with user login
        end

        access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
        pnt:JobReference = job:ref_number
        set(pnt:keyonjobno,pnt:keyonjobno)
        loop
            if access:picknote.next() then break. !no records
            if pnt:JobReference <> job:ref_number then break. !no matching records
            !if pnt:Location <> sto:Location then break. !no matching records on location
            if ~pnt:IsPrinted then !matching record - is it unprinted if so we can we append to it
!            PartsListSplit
!                temptot# = 0
                free(mainstoreparts)
                clear(mainstoreparts)
                sort(mainstoreparts,'+QPartRefNumber')
                access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
                pdt:PickNoteRef = pnt:PickNoteRef
                set(pdt:keyonpicknote,pdt:keyonpicknote)
                loop
                    if access:pickdet.next() then break. !no records
                    if pdt:PickNoteRef <> pnt:PickNoteRef then break.
                    mainstoreparts.QPartRefNumber = pdt:PartRefNumber
                    get(mainstoreparts,mainstoreparts.QPartRefNumber)
                    if error() then !no existing
                        clear(mainstoreparts)
                        mainstoreparts.QPartRefNumber = pdt:PartRefNumber
                        mainstoreparts.Qchargeable = pdt:IsChargeable
                        mainstoreparts.QIsInStock = pdt:IsInStock
                        mainstoreparts.QQuantity = pdt:Quantity
                        add(mainstoreparts)
                    else
                        !update value
                        mainstoreparts.QQuantity += pdt:Quantity
                        put(mainstoreparts)
                    end
!                    temptot# = temptot# + pdt:Quantity !total up quantities on pick note if value is positive then there are some parts to do
!                    if pdt:IsChargeable then !and (~pdt:IsDelete) then !its a chargeable part not deleted
!                        if pdt:PartRefNumber = par:Record_Number then !part ref number match - total up
!                        !if pdt:PartRefNumber = par:Part_Ref_Number then !part ref number match - total up
!                            tempvar = tempvar + pdt:Quantity
!                            proceed = true
!                        end
!                    else !its got to be a warranty part
!                        if pdt:PartRefNumber = wpr:Record_Number then !part ref number match - total up
!                        !if pdt:PartRefNumber = wpr:Part_Ref_Number then !part ref number match - total up
!                            tempvar = tempvar + pdt:Quantity
!                            proceed = true
!                        end
!                    end !case
                end !loop on pickdet
                sort(mainstoreparts,'+QPartRefNumber')
                pick = false
                LOOP x# = 1 TO RECORDS(mainstoreparts)
                    GET(mainstoreparts,x#)
                    if mainstoreparts.QQuantity > 0 then pick = true.
                END !loop
                !if RECORDS(mainstoreparts) > 0 then !it is worth sending the picknote to stores? - otherwise leave the pick note open
                if pick then
                    Case MessageEx('Are you sure you wish to commit the picking note to the stores?','ServiceBase 2000',|
                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
                    Of 1 ! &Yes Button
                        !print the existing record there may be multiples
                        pnt:Printedby = use:user_code
                        pnt:PrintedDate = TODAY()
                        pnt:PrintedTime = CLOCK()
                        access:PICKNOTE.update()
                        PickReport
                        pnt:IsPrinted = true
                        access:PICKNOTE.update()

                        ! Start Change 4801 BE(12/10/2004)
                        IF (access:audit.primerecord() = level:benign) THEN
                            aud:ref_number    = pnt:JobReference
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = 'JOB'
                            aud:user          = pnt:PrintedBy
                            aud:action        = 'PICKNOTE PRINTED'
                            aud:notes         = 'Pick Note Number ' & LEFT(pnt:PickNoteRef) & |
                                                ' created by engineer'
                            IF (access:audit.insert() <> Level:benign) THEN
                                access:audit.cancelautoinc()
                            END
                        END
                        ! End Change 4801 BE(12/10/2004)

                    Of 2 ! &No Button
                    End!Case MessageEx
                end
            else
!               Case MessageEx('Pick note already printed and sent to stores!','ServiceBase 2000',|
!                                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
!                   Of 1 ! &OK Button
!               End!Case MessageEx
                !PickReport
            end
        end !loop
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PickValidate',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


