

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01050.INC'),ONCE        !Local module procedure declarations
                     END


CRCDespatchDates PROCEDURE                            !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Key Vodafone Dates'),AT(,,231,77),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,46),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Customer Collection Date'),AT(8,14),USE(?jobe:CustomerCollectionDate:Prompt)
                           ENTRY(@d6b),AT(96,14,60,10),USE(jobe:CustomerCollectionDate),SKIP,FONT(,8,,FONT:bold),MSG('CustomerCollectionDate'),TIP('CustomerCollectionDate'),UPR,READONLY
                           PROMPT('Due For Despatch Date'),AT(8,32),USE(?jobe:EstimatedDespatchDate:Prompt)
                           ENTRY(@d6b),AT(96,32,60,10),USE(jobe:EstimatedDespatchDate),SKIP,FONT(,8,,FONT:bold),MSG('EstimatedDespatchDate'),TIP('EstimatedDespatchDate'),UPR,READONLY
                           ENTRY(@t1b),AT(160,32,60,10),USE(jobe:EstimatedDespatchTime),SKIP,FONT(,8,,FONT:bold),MSG('CustomerCollectionDate'),TIP('CustomerCollectionDate'),UPR,READONLY
                           ENTRY(@t1b),AT(160,14,60,10),USE(jobe:CustomerCollectionTime),SKIP,FONT(,8,,FONT:bold),MSG('CustomerCollectionDate'),TIP('CustomerCollectionDate'),UPR,READONLY
                         END
                       END
                       PANEL,AT(4,52,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(168,56,56,16),USE(?Close),LEFT,ICON('ok.ico'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?jobe:CustomerCollectionDate:Prompt{prop:FontColor} = -1
    ?jobe:CustomerCollectionDate:Prompt{prop:Color} = 15066597
    If ?jobe:CustomerCollectionDate{prop:ReadOnly} = True
        ?jobe:CustomerCollectionDate{prop:FontColor} = 65793
        ?jobe:CustomerCollectionDate{prop:Color} = 15066597
    Elsif ?jobe:CustomerCollectionDate{prop:Req} = True
        ?jobe:CustomerCollectionDate{prop:FontColor} = 65793
        ?jobe:CustomerCollectionDate{prop:Color} = 8454143
    Else ! If ?jobe:CustomerCollectionDate{prop:Req} = True
        ?jobe:CustomerCollectionDate{prop:FontColor} = 65793
        ?jobe:CustomerCollectionDate{prop:Color} = 16777215
    End ! If ?jobe:CustomerCollectionDate{prop:Req} = True
    ?jobe:CustomerCollectionDate{prop:Trn} = 0
    ?jobe:CustomerCollectionDate{prop:FontStyle} = font:Bold
    ?jobe:EstimatedDespatchDate:Prompt{prop:FontColor} = -1
    ?jobe:EstimatedDespatchDate:Prompt{prop:Color} = 15066597
    If ?jobe:EstimatedDespatchDate{prop:ReadOnly} = True
        ?jobe:EstimatedDespatchDate{prop:FontColor} = 65793
        ?jobe:EstimatedDespatchDate{prop:Color} = 15066597
    Elsif ?jobe:EstimatedDespatchDate{prop:Req} = True
        ?jobe:EstimatedDespatchDate{prop:FontColor} = 65793
        ?jobe:EstimatedDespatchDate{prop:Color} = 8454143
    Else ! If ?jobe:EstimatedDespatchDate{prop:Req} = True
        ?jobe:EstimatedDespatchDate{prop:FontColor} = 65793
        ?jobe:EstimatedDespatchDate{prop:Color} = 16777215
    End ! If ?jobe:EstimatedDespatchDate{prop:Req} = True
    ?jobe:EstimatedDespatchDate{prop:Trn} = 0
    ?jobe:EstimatedDespatchDate{prop:FontStyle} = font:Bold
    If ?jobe:EstimatedDespatchTime{prop:ReadOnly} = True
        ?jobe:EstimatedDespatchTime{prop:FontColor} = 65793
        ?jobe:EstimatedDespatchTime{prop:Color} = 15066597
    Elsif ?jobe:EstimatedDespatchTime{prop:Req} = True
        ?jobe:EstimatedDespatchTime{prop:FontColor} = 65793
        ?jobe:EstimatedDespatchTime{prop:Color} = 8454143
    Else ! If ?jobe:EstimatedDespatchTime{prop:Req} = True
        ?jobe:EstimatedDespatchTime{prop:FontColor} = 65793
        ?jobe:EstimatedDespatchTime{prop:Color} = 16777215
    End ! If ?jobe:EstimatedDespatchTime{prop:Req} = True
    ?jobe:EstimatedDespatchTime{prop:Trn} = 0
    ?jobe:EstimatedDespatchTime{prop:FontStyle} = font:Bold
    If ?jobe:CustomerCollectionTime{prop:ReadOnly} = True
        ?jobe:CustomerCollectionTime{prop:FontColor} = 65793
        ?jobe:CustomerCollectionTime{prop:Color} = 15066597
    Elsif ?jobe:CustomerCollectionTime{prop:Req} = True
        ?jobe:CustomerCollectionTime{prop:FontColor} = 65793
        ?jobe:CustomerCollectionTime{prop:Color} = 8454143
    Else ! If ?jobe:CustomerCollectionTime{prop:Req} = True
        ?jobe:CustomerCollectionTime{prop:FontColor} = 65793
        ?jobe:CustomerCollectionTime{prop:Color} = 16777215
    End ! If ?jobe:CustomerCollectionTime{prop:Req} = True
    ?jobe:CustomerCollectionTime{prop:Trn} = 0
    ?jobe:CustomerCollectionTime{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRCDespatchDates',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:CustomerCollectionDate:Prompt;  SolaceCtrlName = '?jobe:CustomerCollectionDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:CustomerCollectionDate;  SolaceCtrlName = '?jobe:CustomerCollectionDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:EstimatedDespatchDate:Prompt;  SolaceCtrlName = '?jobe:EstimatedDespatchDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:EstimatedDespatchDate;  SolaceCtrlName = '?jobe:EstimatedDespatchDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:EstimatedDespatchTime;  SolaceCtrlName = '?jobe:EstimatedDespatchTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:CustomerCollectionTime;  SolaceCtrlName = '?jobe:CustomerCollectionTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CRCDespatchDates')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRCDespatchDates')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jobe:CustomerCollectionDate:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBSE.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBSE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRCDespatchDates',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRCDespatchDates')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

