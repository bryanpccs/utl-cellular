

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01004.INC'),ONCE        !Local module procedure declarations
                     END


Collection_Text PROCEDURE                             !Generated from procedure template - Window

FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Collection Text'),AT(,,232,144),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,108),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           TEXT,AT(8,8,216,64),USE(jbn:Collection_Text),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Contact Name'),AT(8,80),USE(?JBN:ColContatName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(100,80,124,10),USE(jbn:ColContatName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Contact Name'),TIP('Contact Name'),UPR
                           PROMPT('Department'),AT(8,96),USE(?JBN:ColDepartment:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(100,96,124,10),USE(jbn:ColDepartment),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Department'),TIP('Department'),UPR
                         END
                       END
                       PANEL,AT(4,116,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(168,120,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(112,120,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?jbn:Collection_Text{prop:ReadOnly} = True
        ?jbn:Collection_Text{prop:FontColor} = 65793
        ?jbn:Collection_Text{prop:Color} = 15066597
    Elsif ?jbn:Collection_Text{prop:Req} = True
        ?jbn:Collection_Text{prop:FontColor} = 65793
        ?jbn:Collection_Text{prop:Color} = 8454143
    Else ! If ?jbn:Collection_Text{prop:Req} = True
        ?jbn:Collection_Text{prop:FontColor} = 65793
        ?jbn:Collection_Text{prop:Color} = 16777215
    End ! If ?jbn:Collection_Text{prop:Req} = True
    ?jbn:Collection_Text{prop:Trn} = 0
    ?jbn:Collection_Text{prop:FontStyle} = font:Bold
    ?JBN:ColContatName:Prompt{prop:FontColor} = -1
    ?JBN:ColContatName:Prompt{prop:Color} = 15066597
    If ?jbn:ColContatName{prop:ReadOnly} = True
        ?jbn:ColContatName{prop:FontColor} = 65793
        ?jbn:ColContatName{prop:Color} = 15066597
    Elsif ?jbn:ColContatName{prop:Req} = True
        ?jbn:ColContatName{prop:FontColor} = 65793
        ?jbn:ColContatName{prop:Color} = 8454143
    Else ! If ?jbn:ColContatName{prop:Req} = True
        ?jbn:ColContatName{prop:FontColor} = 65793
        ?jbn:ColContatName{prop:Color} = 16777215
    End ! If ?jbn:ColContatName{prop:Req} = True
    ?jbn:ColContatName{prop:Trn} = 0
    ?jbn:ColContatName{prop:FontStyle} = font:Bold
    ?JBN:ColDepartment:Prompt{prop:FontColor} = -1
    ?JBN:ColDepartment:Prompt{prop:Color} = 15066597
    If ?jbn:ColDepartment{prop:ReadOnly} = True
        ?jbn:ColDepartment{prop:FontColor} = 65793
        ?jbn:ColDepartment{prop:Color} = 15066597
    Elsif ?jbn:ColDepartment{prop:Req} = True
        ?jbn:ColDepartment{prop:FontColor} = 65793
        ?jbn:ColDepartment{prop:Color} = 8454143
    Else ! If ?jbn:ColDepartment{prop:Req} = True
        ?jbn:ColDepartment{prop:FontColor} = 65793
        ?jbn:ColDepartment{prop:Color} = 16777215
    End ! If ?jbn:ColDepartment{prop:Req} = True
    ?jbn:ColDepartment{prop:Trn} = 0
    ?jbn:ColDepartment{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Collection_Text',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Collection_Text',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Collection_Text',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Collection_Text;  SolaceCtrlName = '?jbn:Collection_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JBN:ColContatName:Prompt;  SolaceCtrlName = '?JBN:ColContatName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:ColContatName;  SolaceCtrlName = '?jbn:ColContatName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JBN:ColDepartment:Prompt;  SolaceCtrlName = '?JBN:ColDepartment:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:ColDepartment;  SolaceCtrlName = '?jbn:ColDepartment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Changing Collection Text'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Collection_Text')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Collection_Text')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?jbn:Collection_Text
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddUpdateFile(Access:JOBNOTES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBNOTES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBNOTES.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Collection_Text',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Collection_Text')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

