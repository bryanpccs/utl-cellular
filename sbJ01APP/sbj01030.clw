

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJ01030.INC'),ONCE        !Local module procedure declarations
                     END


UpdatePickPartsReceived PROCEDURE  (jobnumber,partnumber,partqty) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'UpdatePickPartsReceived')      !Add Procedure to Log
  end


    ! Start Change 4797 BE(22/10/2004)
    !set(picknote)
    !loop while access:picknote.next() = Level:benign
    !    if pnt:JobReference <> jobnumber then cycle. !not matching
    access:picknote.clearkey(pnt:keyonjobno)
    pnt:JobReference = jobnumber
    SET(pnt:keyonjobno,pnt:keyonjobno)
    LOOP
        IF ((access:picknote.next() <> Level:benign)  OR |
            (pnt:JobReference <> jobnumber)) THEN
            BREAK
        END
    ! End Change 4797 BE(22/10/2004)
        access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
        pdt:PickNoteRef = pnt:PickNoteRef
        set(pdt:keyonpicknote,pdt:keyonpicknote)
        loop !thru all pickdets
            if access:pickdet.next() then break. !no records
            if pdt:PickNoteRef <> pnt:PickNoteRef then break.
            if pdt:PartRefNumber = partnumber then !matching part
                if ~pdt:IsDelete then !if not already deleted
                    if ~pdt:IsInStock then !if not in stock check quantity
                        if pdt:Quantity =< partqty then
                            pdt:IsInStock = true
                            partqty = partqty - pdt:Quantity
                            access:pickdet.update()
                        end
                    end
                end
            end
        end
    end



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdatePickPartsReceived',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


