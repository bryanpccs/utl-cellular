

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01039.INC'),ONCE        !Local module procedure declarations
                     END


Browse_New_Features PROCEDURE                         !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ReportType       BYTE(0)
tmp:Document         STRING('0 {2}')
BRW1::View:Browse    VIEW(NEWFEAT)
                       PROJECT(fea:date)
                       PROJECT(fea:description)
                       PROJECT(fea:RefNumber)
                       PROJECT(fea:Text)
                       PROJECT(fea:DocumentPath)
                       PROJECT(fea:Record_Number)
                       PROJECT(fea:ReportType)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
fea:date               LIKE(fea:date)                 !List box control field - type derived from field
fea:description        LIKE(fea:description)          !List box control field - type derived from field
fea:RefNumber          LIKE(fea:RefNumber)            !List box control field - type derived from field
tmp:Document           LIKE(tmp:Document)             !List box control field - type derived from local data
fea:Text               LIKE(fea:Text)                 !Browse hot field - type derived from field
fea:DocumentPath       LIKE(fea:DocumentPath)         !Browse hot field - type derived from field
fea:Record_Number      LIKE(fea:Record_Number)        !Primary key field - type derived from field
fea:ReportType         LIKE(fea:ReportType)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Enhancements File'),AT(,,528,328),FONT('Tahoma',8,,),CENTER,IMM,ICON('Pc.ico'),HLP('Browse_New_Features'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,48,260,272),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('42R(2)|M~Date~L@d6@127L(2)|M~Description~@s60@39L(2)|M~Ref No~@s30@12L(2)|M~Docu' &|
   'ment~@s3@'),FROM(Queue:Browse:1)
                       PROMPT('Document Name'),AT(280,264),USE(?FEA:DocumentPath:Prompt)
                       TEXT,AT(338,264,170,10),USE(fea:DocumentPath),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Document Path'),TIP('Document Path'),UPR,READONLY
                       BUTTON('Open Document'),AT(338,280,170,10),USE(?Button5)
                       TEXT,AT(280,8,240,236),USE(fea:Text),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                       SHEET,AT(4,4,268,320),USE(?CurrentTab),SPREAD
                         TAB('By Date'),USE(?Tab:3)
                         END
                         TAB('By Description'),USE(?Tab:4)
                           ENTRY(@s60),AT(8,32,124,10),USE(fea:description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By Ref No'),USE(?Tab3)
                           TEXT,AT(8,32,64,10),USE(fea:RefNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Reference Number'),TIP('Reference Number'),UPR
                         END
                       END
                       OPTION('Report Type'),AT(132,20,136,24),USE(tmp:ReportType),BOXED
                         RADIO('Bug'),AT(140,32),USE(?tmp:ReportType:Radio1),VALUE('0')
                         RADIO('Enhancement'),AT(176,32),USE(?tmp:ReportType:Radio2),VALUE('1')
                         RADIO('All'),AT(244,32),USE(?tmp:ReportType:Radio3),VALUE('2')
                       END
                       PANEL,AT(276,4,248,288),USE(?Panel1),FILL(COLOR:Gray)
                       BUTTON('Close'),AT(448,304,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And tmp:ReportType <> 2
BRW1::Sort4:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 1 And tmp:ReportType = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And tmp:ReportType = 2
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And tmp:ReportType <> 2
BRW1::Sort6:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 3 And tmp:ReportType = 2
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And tmp:ReportType <> 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And tmp:ReportType = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Shell Execute Variables
AssocFile    CString(255)
Operation    CString(255)
Param        CString(255) 
Dir          CString(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?FEA:DocumentPath:Prompt{prop:FontColor} = -1
    ?FEA:DocumentPath:Prompt{prop:Color} = 15066597
    If ?fea:DocumentPath{prop:ReadOnly} = True
        ?fea:DocumentPath{prop:FontColor} = 65793
        ?fea:DocumentPath{prop:Color} = 15066597
    Elsif ?fea:DocumentPath{prop:Req} = True
        ?fea:DocumentPath{prop:FontColor} = 65793
        ?fea:DocumentPath{prop:Color} = 8454143
    Else ! If ?fea:DocumentPath{prop:Req} = True
        ?fea:DocumentPath{prop:FontColor} = 65793
        ?fea:DocumentPath{prop:Color} = 16777215
    End ! If ?fea:DocumentPath{prop:Req} = True
    ?fea:DocumentPath{prop:Trn} = 0
    ?fea:DocumentPath{prop:FontStyle} = font:Bold
    If ?fea:Text{prop:ReadOnly} = True
        ?fea:Text{prop:FontColor} = 65793
        ?fea:Text{prop:Color} = 15066597
    Elsif ?fea:Text{prop:Req} = True
        ?fea:Text{prop:FontColor} = 65793
        ?fea:Text{prop:Color} = 8454143
    Else ! If ?fea:Text{prop:Req} = True
        ?fea:Text{prop:FontColor} = 65793
        ?fea:Text{prop:Color} = 16777215
    End ! If ?fea:Text{prop:Req} = True
    ?fea:Text{prop:Trn} = 0
    ?fea:Text{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    If ?fea:description{prop:ReadOnly} = True
        ?fea:description{prop:FontColor} = 65793
        ?fea:description{prop:Color} = 15066597
    Elsif ?fea:description{prop:Req} = True
        ?fea:description{prop:FontColor} = 65793
        ?fea:description{prop:Color} = 8454143
    Else ! If ?fea:description{prop:Req} = True
        ?fea:description{prop:FontColor} = 65793
        ?fea:description{prop:Color} = 16777215
    End ! If ?fea:description{prop:Req} = True
    ?fea:description{prop:Trn} = 0
    ?fea:description{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?fea:RefNumber{prop:ReadOnly} = True
        ?fea:RefNumber{prop:FontColor} = 65793
        ?fea:RefNumber{prop:Color} = 15066597
    Elsif ?fea:RefNumber{prop:Req} = True
        ?fea:RefNumber{prop:FontColor} = 65793
        ?fea:RefNumber{prop:Color} = 8454143
    Else ! If ?fea:RefNumber{prop:Req} = True
        ?fea:RefNumber{prop:FontColor} = 65793
        ?fea:RefNumber{prop:Color} = 16777215
    End ! If ?fea:RefNumber{prop:Req} = True
    ?fea:RefNumber{prop:Trn} = 0
    ?fea:RefNumber{prop:FontStyle} = font:Bold
    ?tmp:ReportType{prop:Font,3} = -1
    ?tmp:ReportType{prop:Color} = 15066597
    ?tmp:ReportType{prop:Trn} = 0
    ?tmp:ReportType:Radio1{prop:Font,3} = -1
    ?tmp:ReportType:Radio1{prop:Color} = 15066597
    ?tmp:ReportType:Radio1{prop:Trn} = 0
    ?tmp:ReportType:Radio2{prop:Font,3} = -1
    ?tmp:ReportType:Radio2{prop:Color} = 15066597
    ?tmp:ReportType:Radio2{prop:Trn} = 0
    ?tmp:ReportType:Radio3{prop:Font,3} = -1
    ?tmp:ReportType:Radio3{prop:Color} = 15066597
    ?tmp:ReportType:Radio3{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674







SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_New_Features',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_New_Features',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_New_Features',1)
    SolaceViewVars('tmp:ReportType',tmp:ReportType,'Browse_New_Features',1)
    SolaceViewVars('tmp:Document',tmp:Document,'Browse_New_Features',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FEA:DocumentPath:Prompt;  SolaceCtrlName = '?FEA:DocumentPath:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?fea:DocumentPath;  SolaceCtrlName = '?fea:DocumentPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button5;  SolaceCtrlName = '?Button5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?fea:Text;  SolaceCtrlName = '?fea:Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?fea:description;  SolaceCtrlName = '?fea:description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?fea:RefNumber;  SolaceCtrlName = '?fea:RefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportType;  SolaceCtrlName = '?tmp:ReportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportType:Radio1;  SolaceCtrlName = '?tmp:ReportType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportType:Radio2;  SolaceCtrlName = '?tmp:ReportType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ReportType:Radio3;  SolaceCtrlName = '?tmp:ReportType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_New_Features')
    
    
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_New_Features')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_New_Features'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:NEWFEAT.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:NEWFEAT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,fea:DateTypeKey)
  BRW1.AddRange(fea:ReportType,tmp:ReportType)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(,fea:date,1,BRW1)
  BRW1.AddSortOrder(,fea:date_key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,fea:date,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,fea:DescriptionOnlyKey)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?fea:description,fea:description,1,BRW1)
  BRW1.AddSortOrder(,fea:DescriptionTypeKey)
  BRW1.AddRange(fea:ReportType,tmp:ReportType)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?fea:description,fea:description,1,BRW1)
  BRW1.AddSortOrder(,fea:RefNoKey)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(,fea:RefNumber,1,BRW1)
  BRW1.AddSortOrder(,fea:RefNoTypeKey)
  BRW1.AddRange(fea:ReportType,tmp:ReportType)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?fea:RefNumber,fea:RefNumber,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,fea:date_key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,fea:date,1,BRW1)
  BIND('tmp:Document',tmp:Document)
  BRW1.AddField(fea:date,BRW1.Q.fea:date)
  BRW1.AddField(fea:description,BRW1.Q.fea:description)
  BRW1.AddField(fea:RefNumber,BRW1.Q.fea:RefNumber)
  BRW1.AddField(tmp:Document,BRW1.Q.tmp:Document)
  BRW1.AddField(fea:Text,BRW1.Q.fea:Text)
  BRW1.AddField(fea:DocumentPath,BRW1.Q.fea:DocumentPath)
  BRW1.AddField(fea:Record_Number,BRW1.Q.fea:Record_Number)
  BRW1.AddField(fea:ReportType,BRW1.Q.fea:ReportType)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
    
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:3{PROP:TEXT} = 'By Date'
    ?Tab:4{PROP:TEXT} = 'By Description'
    ?Tab3{PROP:TEXT} = 'By Ref No'
    ?Browse:1{PROP:FORMAT} ='42R(2)|M~Date~L@d6@#1#127L(2)|M~Description~@s60@#2#39L(2)|M~Ref No~@s30@#3#12L(2)|M~Document~@s3@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:NEWFEAT.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_New_Features'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
    
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_New_Features',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    
    
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button5
      ThisWindow.Update
      AssocFile = Path() & '\FEATURES\' & Clip(fea:DocumentPath)
      Param = ''
      Dir = ''
      Operation = 'Open'
      ShellExecute(GetDesktopWindow(), Operation, AssocFile, Param, Dir, 5)
    OF ?tmp:ReportType
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_New_Features')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    
    
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='42R(2)|M~Date~L@d6@#1#127L(2)|M~Description~@s60@#2#39L(2)|M~Ref No~@s30@#3#12L(2)|M~Document~@s3@#4#'
          ?Tab:3{PROP:TEXT} = 'By Date'
        OF 2
          ?Browse:1{PROP:FORMAT} ='127L(2)|M~Description~@s60@#2#42R(2)|M~Date~L@d6@#1#39L(2)|M~Ref No~@s30@#3#12L(2)|M~Document~@s3@#4#'
          ?Tab:4{PROP:TEXT} = 'By Description'
        OF 3
          ?Browse:1{PROP:FORMAT} ='39L(2)|M~Ref No~@s30@#3#42R(2)|M~Date~L@d6@#1#127L(2)|M~Description~@s60@#2#12L(2)|M~Document~@s3@#4#'
          ?Tab3{PROP:TEXT} = 'By Ref No'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?fea:description
      Select(?browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        
        
        
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And tmp:ReportType <> 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And tmp:ReportType = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 And tmp:ReportType = 2
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And tmp:ReportType <> 2
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 3 And tmp:ReportType = 2
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 3 And tmp:ReportType <> 2
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  IF (fea:DocumentPath <> '')
    tmp:Document = 'YES'
  ELSE
    tmp:Document = ''
  END
  PARENT.SetQueueRecord
  SELF.Q.tmp:Document = tmp:Document                  !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?FEA:Text, Resize:FixRight+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?FEA:description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Panel1, Resize:FixRight+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)

