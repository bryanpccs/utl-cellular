

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01032.INC'),ONCE        !Local module procedure declarations
                     END


Select_Loan_Exchange PROCEDURE (f_type,f_full_search,f_unit_number) !Generated from procedure template - Window

FilesOpened          BYTE
esn_temp             STRING(16)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Search Loan/Exchange Unit'),AT(,,280,72),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,272,36),USE(?Sheet1),SPREAD
                         TAB('Search Loan/Exchange Unit'),USE(?Tab1)
                           PROMPT('I.M.E.I. Number'),AT(8,20),USE(?esn_temp:Prompt),TRN
                           ENTRY(@s16),AT(84,20,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('&Full Search'),AT(212,20,60,12),USE(?Full_Search),SKIP,LEFT,ICON('Spysm.gif')
                         END
                       END
                       PANEL,AT(4,44,272,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(160,48,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(216,48,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?esn_temp:Prompt{prop:FontColor} = -1
    ?esn_temp:Prompt{prop:Color} = 15066597
    If ?esn_temp{prop:ReadOnly} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 15066597
    Elsif ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 8454143
    Else ! If ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 16777215
    End ! If ?esn_temp{prop:Req} = True
    ?esn_temp{prop:Trn} = 0
    ?esn_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Select_Loan_Exchange',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Select_Loan_Exchange',1)
    SolaceViewVars('esn_temp',esn_temp,'Select_Loan_Exchange',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp:Prompt;  SolaceCtrlName = '?esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp;  SolaceCtrlName = '?esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Full_Search;  SolaceCtrlName = '?Full_Search';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Select_Loan_Exchange')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Select_Loan_Exchange')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?esn_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCHANGE.Open
  Relate:STOCK.Open
  Access:LOAN.UseFile
  Access:STOCKTYP.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:STOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Select_Loan_Exchange',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Full_Search
      ThisWindow.Update
      f_full_search = 'Y'
      f_unit_number = ''
      Post(Event:Closewindow)
    OF ?OkButton
      ThisWindow.Update
      f_full_search = 'N'
      close# = 0
      error# = 0
      Case f_type
          Of 'LOA'
              access:loan.clearkey(loa:esn_only_key)
              loa:esn = esn_temp
              if access:loan.fetch(loa:esn_only_key) = Level:Benign
                 IF loa:Available = 'AVL'
                   access:stocktyp.clearkey(stp:stock_type_key)
                   stp:stock_type = loa:stock_type
                   if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                      If stp:available <> 1
                         Case MessageEx('The selected unit is NOT available.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          f_unit_number = 0
                          error# = 1
                      ELSE
                          f_unit_number = loa:ref_number
                          close# = 1
                      End!If stp:available <> 1
                   End!if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                 ELSE
                   f_unit_number = 0
                   error# = 1
                 END
              Else!if access:exchange.fetch(xch:esn_only_key)
                 error# = 1
              end!if access:exchange.fetch(xch:esn_only_key)
      
          Of 'EXC'
              access:exchange.clearkey(xch:esn_only_key)
              xch:esn = esn_temp
              if access:exchange.fetch(xch:esn_only_key) = Level:Benign
                 IF xch:Available = 'AVL'
                   access:stocktyp.clearkey(stp:stock_type_key)
                   stp:stock_type = xch:stock_type
                   if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                      If stp:available <> 1
                         Case MessageEx('The selected unit is NOT available.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          f_unit_number = 0
                          error# = 1
                      ELSE
                          f_unit_number = xch:ref_number
                          close# = 1
                      End!If stp:available <> 1
                   End!if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                 ELSE
                   f_unit_number = 0
                   error# = 1
                 END
               Else!if access:exchange.fetch(xch:esn_only_key)
                  error# = 1
              end!if access:exchange.fetch(xch:esn_only_key)
      End!Case f_type
      If error# = 1
          Case MessageEx('Unable to find I.M.E.I. Number or Unit is unavailable<13,10><13,10>Do you wish to try and insert another, or go to the full search?','ServiceBase 2000',|
                         'Styles\warn.ico','|&Insert Another|&Full Search|&Cancel',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Insert Another Button
                  esn_temp = ''
                  Select(?esn_temp)
              Of 2 ! &Full Search Button
                  f_full_search = 'Y'
                  close# = 1
              Of 3 ! &Cancel Button
                  close# = 1
          End!Case MessageEx
      
      End!If error# = 1
      If close# = 1
          Post(event:closewindow)
      End!If close# = 1
    OF ?CancelButton
      ThisWindow.Update
      f_full_search = 'N'
      f_unit_number = ''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Select_Loan_Exchange')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Case f_type
          Of 'EXC'
              0{prop:text} = 'Select Exchange Unit'
              ?Tab1{prop:text} = 'Insert Exchange E.S.N. / I.M.E.I.'
          Of 'LOA'
              0{prop:text} = 'Select Loan Unit'
              ?Tab1{prop:text} = 'Insert Loan E.S.N. / I.M.E.I.'
      End!Case f_type
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

