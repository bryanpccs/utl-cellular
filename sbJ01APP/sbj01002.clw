

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01002.INC'),ONCE        !Local module procedure declarations
                     END


Show_Addresses PROCEDURE                              !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?job:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB11::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
window               WINDOW('Address Details'),AT(,,499,171),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,492,136),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Customer Address'),AT(8,8),USE(?InvoiceAddress),TRN,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Company Name'),AT(8,20),USE(?JOB:CompanyName:Prompt),TRN
                           ENTRY(@s30),AT(88,20,124,10),USE(job:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Postcode'),AT(8,32),USE(?JOB:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(88,32,64,10),USE(job:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(8,44),USE(?JOB:AddressLine1:Prompt),TRN
                           ENTRY(@s30),AT(88,44,124,10),USE(job:Address_Line1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(88,56,124,10),USE(job:Address_Line2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(88,68,124,10),USE(job:Address_Line3),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(88,80),USE(?JOB:TelephoneNumber:Prompt),TRN,FONT(,7,,)
                           ENTRY(@s15),AT(88,88,56,10),USE(job:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(228,80),USE(?JOB:TelephoneNumber:Prompt:2),TRN,FONT(,7,,)
                           PROMPT('Fax Number'),AT(156,80),USE(?JOB:FaxNumber:Prompt),TRN,FONT(,7,,)
                           ENTRY(@s15),AT(156,88,56,10),USE(job:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Collection Address'),AT(228,8),USE(?CollectionAddress),TRN,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(228,20,124,10),USE(job:Company_Name_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s10),AT(228,32,64,10),USE(job:Postcode_Collection),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON('C&lear / Copy'),AT(292,32,60,10),USE(?CollClearCopy),SKIP,LEFT,ICON(ICON:Cut)
                           ENTRY(@s30),AT(228,44,124,10),USE(job:Address_Line1_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(228,56,124,10),USE(job:Address_Line2_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(228,68,124,10),USE(job:Address_Line3_Collection),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Contact Numbers'),AT(8,88),USE(?JOB:ColTelephoneNo:Prompt),TRN
                           ENTRY(@s15),AT(228,88,56,10),USE(job:Telephone_Collection),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Delivery Address'),AT(368,8),USE(?DeliveryAddress),TRN,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           ENTRY(@s30),AT(368,20,124,10),USE(job:Company_Name_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Outgoing Courier'),AT(8,124),USE(?job:courier:prompt),TRN
                           ENTRY(@s10),AT(368,32,64,10),USE(job:Postcode_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON('Clear / Co&py'),AT(432,32,60,10),USE(?DelClearCopy),SKIP,LEFT,ICON(ICON:Cut)
                           ENTRY(@s30),AT(368,44,124,10),USE(job:Address_Line1_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(368,56,124,10),USE(job:Address_Line2_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(368,68,124,10),USE(job:Address_Line3_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(368,80),USE(?JOB:DelTelephoneNo:Prompt),TRN,FONT(,7,,)
                           BUTTON('Browse Account Addresses'),AT(368,104,124,16),USE(?BrowseAddresses),LEFT,ICON('clipbrsm.gif')
                           ENTRY(@s15),AT(368,88,56,10),USE(job:Telephone_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           COMBO(@s20),AT(88,124,124,10),USE(job:Courier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Email Address'),AT(9,104),USE(?jobe:EndUserEmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(88,104,124,10),USE(jobe:EndUserEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Address'),TIP('Email Address')
                         END
                       END
                       BUTTON('&OK'),AT(436,148,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       PANEL,AT(4,144,492,24),USE(?Panel1:2),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB11               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

szpostcode      cstring(15)
szpath          cstring(255)
szaddress       cstring(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?InvoiceAddress{prop:FontColor} = -1
    ?InvoiceAddress{prop:Color} = 15066597
    ?JOB:CompanyName:Prompt{prop:FontColor} = -1
    ?JOB:CompanyName:Prompt{prop:Color} = 15066597
    If ?job:Company_Name{prop:ReadOnly} = True
        ?job:Company_Name{prop:FontColor} = 65793
        ?job:Company_Name{prop:Color} = 15066597
    Elsif ?job:Company_Name{prop:Req} = True
        ?job:Company_Name{prop:FontColor} = 65793
        ?job:Company_Name{prop:Color} = 8454143
    Else ! If ?job:Company_Name{prop:Req} = True
        ?job:Company_Name{prop:FontColor} = 65793
        ?job:Company_Name{prop:Color} = 16777215
    End ! If ?job:Company_Name{prop:Req} = True
    ?job:Company_Name{prop:Trn} = 0
    ?job:Company_Name{prop:FontStyle} = font:Bold
    ?JOB:Postcode:Prompt{prop:FontColor} = -1
    ?JOB:Postcode:Prompt{prop:Color} = 15066597
    If ?job:Postcode{prop:ReadOnly} = True
        ?job:Postcode{prop:FontColor} = 65793
        ?job:Postcode{prop:Color} = 15066597
    Elsif ?job:Postcode{prop:Req} = True
        ?job:Postcode{prop:FontColor} = 65793
        ?job:Postcode{prop:Color} = 8454143
    Else ! If ?job:Postcode{prop:Req} = True
        ?job:Postcode{prop:FontColor} = 65793
        ?job:Postcode{prop:Color} = 16777215
    End ! If ?job:Postcode{prop:Req} = True
    ?job:Postcode{prop:Trn} = 0
    ?job:Postcode{prop:FontStyle} = font:Bold
    ?JOB:AddressLine1:Prompt{prop:FontColor} = -1
    ?JOB:AddressLine1:Prompt{prop:Color} = 15066597
    If ?job:Address_Line1{prop:ReadOnly} = True
        ?job:Address_Line1{prop:FontColor} = 65793
        ?job:Address_Line1{prop:Color} = 15066597
    Elsif ?job:Address_Line1{prop:Req} = True
        ?job:Address_Line1{prop:FontColor} = 65793
        ?job:Address_Line1{prop:Color} = 8454143
    Else ! If ?job:Address_Line1{prop:Req} = True
        ?job:Address_Line1{prop:FontColor} = 65793
        ?job:Address_Line1{prop:Color} = 16777215
    End ! If ?job:Address_Line1{prop:Req} = True
    ?job:Address_Line1{prop:Trn} = 0
    ?job:Address_Line1{prop:FontStyle} = font:Bold
    If ?job:Address_Line2{prop:ReadOnly} = True
        ?job:Address_Line2{prop:FontColor} = 65793
        ?job:Address_Line2{prop:Color} = 15066597
    Elsif ?job:Address_Line2{prop:Req} = True
        ?job:Address_Line2{prop:FontColor} = 65793
        ?job:Address_Line2{prop:Color} = 8454143
    Else ! If ?job:Address_Line2{prop:Req} = True
        ?job:Address_Line2{prop:FontColor} = 65793
        ?job:Address_Line2{prop:Color} = 16777215
    End ! If ?job:Address_Line2{prop:Req} = True
    ?job:Address_Line2{prop:Trn} = 0
    ?job:Address_Line2{prop:FontStyle} = font:Bold
    If ?job:Address_Line3{prop:ReadOnly} = True
        ?job:Address_Line3{prop:FontColor} = 65793
        ?job:Address_Line3{prop:Color} = 15066597
    Elsif ?job:Address_Line3{prop:Req} = True
        ?job:Address_Line3{prop:FontColor} = 65793
        ?job:Address_Line3{prop:Color} = 8454143
    Else ! If ?job:Address_Line3{prop:Req} = True
        ?job:Address_Line3{prop:FontColor} = 65793
        ?job:Address_Line3{prop:Color} = 16777215
    End ! If ?job:Address_Line3{prop:Req} = True
    ?job:Address_Line3{prop:Trn} = 0
    ?job:Address_Line3{prop:FontStyle} = font:Bold
    ?JOB:TelephoneNumber:Prompt{prop:FontColor} = -1
    ?JOB:TelephoneNumber:Prompt{prop:Color} = 15066597
    If ?job:Telephone_Number{prop:ReadOnly} = True
        ?job:Telephone_Number{prop:FontColor} = 65793
        ?job:Telephone_Number{prop:Color} = 15066597
    Elsif ?job:Telephone_Number{prop:Req} = True
        ?job:Telephone_Number{prop:FontColor} = 65793
        ?job:Telephone_Number{prop:Color} = 8454143
    Else ! If ?job:Telephone_Number{prop:Req} = True
        ?job:Telephone_Number{prop:FontColor} = 65793
        ?job:Telephone_Number{prop:Color} = 16777215
    End ! If ?job:Telephone_Number{prop:Req} = True
    ?job:Telephone_Number{prop:Trn} = 0
    ?job:Telephone_Number{prop:FontStyle} = font:Bold
    ?JOB:TelephoneNumber:Prompt:2{prop:FontColor} = -1
    ?JOB:TelephoneNumber:Prompt:2{prop:Color} = 15066597
    ?JOB:FaxNumber:Prompt{prop:FontColor} = -1
    ?JOB:FaxNumber:Prompt{prop:Color} = 15066597
    If ?job:Fax_Number{prop:ReadOnly} = True
        ?job:Fax_Number{prop:FontColor} = 65793
        ?job:Fax_Number{prop:Color} = 15066597
    Elsif ?job:Fax_Number{prop:Req} = True
        ?job:Fax_Number{prop:FontColor} = 65793
        ?job:Fax_Number{prop:Color} = 8454143
    Else ! If ?job:Fax_Number{prop:Req} = True
        ?job:Fax_Number{prop:FontColor} = 65793
        ?job:Fax_Number{prop:Color} = 16777215
    End ! If ?job:Fax_Number{prop:Req} = True
    ?job:Fax_Number{prop:Trn} = 0
    ?job:Fax_Number{prop:FontStyle} = font:Bold
    ?CollectionAddress{prop:FontColor} = -1
    ?CollectionAddress{prop:Color} = 15066597
    If ?job:Company_Name_Collection{prop:ReadOnly} = True
        ?job:Company_Name_Collection{prop:FontColor} = 65793
        ?job:Company_Name_Collection{prop:Color} = 15066597
    Elsif ?job:Company_Name_Collection{prop:Req} = True
        ?job:Company_Name_Collection{prop:FontColor} = 65793
        ?job:Company_Name_Collection{prop:Color} = 8454143
    Else ! If ?job:Company_Name_Collection{prop:Req} = True
        ?job:Company_Name_Collection{prop:FontColor} = 65793
        ?job:Company_Name_Collection{prop:Color} = 16777215
    End ! If ?job:Company_Name_Collection{prop:Req} = True
    ?job:Company_Name_Collection{prop:Trn} = 0
    ?job:Company_Name_Collection{prop:FontStyle} = font:Bold
    If ?job:Postcode_Collection{prop:ReadOnly} = True
        ?job:Postcode_Collection{prop:FontColor} = 65793
        ?job:Postcode_Collection{prop:Color} = 15066597
    Elsif ?job:Postcode_Collection{prop:Req} = True
        ?job:Postcode_Collection{prop:FontColor} = 65793
        ?job:Postcode_Collection{prop:Color} = 8454143
    Else ! If ?job:Postcode_Collection{prop:Req} = True
        ?job:Postcode_Collection{prop:FontColor} = 65793
        ?job:Postcode_Collection{prop:Color} = 16777215
    End ! If ?job:Postcode_Collection{prop:Req} = True
    ?job:Postcode_Collection{prop:Trn} = 0
    ?job:Postcode_Collection{prop:FontStyle} = font:Bold
    If ?job:Address_Line1_Collection{prop:ReadOnly} = True
        ?job:Address_Line1_Collection{prop:FontColor} = 65793
        ?job:Address_Line1_Collection{prop:Color} = 15066597
    Elsif ?job:Address_Line1_Collection{prop:Req} = True
        ?job:Address_Line1_Collection{prop:FontColor} = 65793
        ?job:Address_Line1_Collection{prop:Color} = 8454143
    Else ! If ?job:Address_Line1_Collection{prop:Req} = True
        ?job:Address_Line1_Collection{prop:FontColor} = 65793
        ?job:Address_Line1_Collection{prop:Color} = 16777215
    End ! If ?job:Address_Line1_Collection{prop:Req} = True
    ?job:Address_Line1_Collection{prop:Trn} = 0
    ?job:Address_Line1_Collection{prop:FontStyle} = font:Bold
    If ?job:Address_Line2_Collection{prop:ReadOnly} = True
        ?job:Address_Line2_Collection{prop:FontColor} = 65793
        ?job:Address_Line2_Collection{prop:Color} = 15066597
    Elsif ?job:Address_Line2_Collection{prop:Req} = True
        ?job:Address_Line2_Collection{prop:FontColor} = 65793
        ?job:Address_Line2_Collection{prop:Color} = 8454143
    Else ! If ?job:Address_Line2_Collection{prop:Req} = True
        ?job:Address_Line2_Collection{prop:FontColor} = 65793
        ?job:Address_Line2_Collection{prop:Color} = 16777215
    End ! If ?job:Address_Line2_Collection{prop:Req} = True
    ?job:Address_Line2_Collection{prop:Trn} = 0
    ?job:Address_Line2_Collection{prop:FontStyle} = font:Bold
    If ?job:Address_Line3_Collection{prop:ReadOnly} = True
        ?job:Address_Line3_Collection{prop:FontColor} = 65793
        ?job:Address_Line3_Collection{prop:Color} = 15066597
    Elsif ?job:Address_Line3_Collection{prop:Req} = True
        ?job:Address_Line3_Collection{prop:FontColor} = 65793
        ?job:Address_Line3_Collection{prop:Color} = 8454143
    Else ! If ?job:Address_Line3_Collection{prop:Req} = True
        ?job:Address_Line3_Collection{prop:FontColor} = 65793
        ?job:Address_Line3_Collection{prop:Color} = 16777215
    End ! If ?job:Address_Line3_Collection{prop:Req} = True
    ?job:Address_Line3_Collection{prop:Trn} = 0
    ?job:Address_Line3_Collection{prop:FontStyle} = font:Bold
    ?JOB:ColTelephoneNo:Prompt{prop:FontColor} = -1
    ?JOB:ColTelephoneNo:Prompt{prop:Color} = 15066597
    If ?job:Telephone_Collection{prop:ReadOnly} = True
        ?job:Telephone_Collection{prop:FontColor} = 65793
        ?job:Telephone_Collection{prop:Color} = 15066597
    Elsif ?job:Telephone_Collection{prop:Req} = True
        ?job:Telephone_Collection{prop:FontColor} = 65793
        ?job:Telephone_Collection{prop:Color} = 8454143
    Else ! If ?job:Telephone_Collection{prop:Req} = True
        ?job:Telephone_Collection{prop:FontColor} = 65793
        ?job:Telephone_Collection{prop:Color} = 16777215
    End ! If ?job:Telephone_Collection{prop:Req} = True
    ?job:Telephone_Collection{prop:Trn} = 0
    ?job:Telephone_Collection{prop:FontStyle} = font:Bold
    ?DeliveryAddress{prop:FontColor} = -1
    ?DeliveryAddress{prop:Color} = 15066597
    If ?job:Company_Name_Delivery{prop:ReadOnly} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 15066597
    Elsif ?job:Company_Name_Delivery{prop:Req} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 8454143
    Else ! If ?job:Company_Name_Delivery{prop:Req} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 16777215
    End ! If ?job:Company_Name_Delivery{prop:Req} = True
    ?job:Company_Name_Delivery{prop:Trn} = 0
    ?job:Company_Name_Delivery{prop:FontStyle} = font:Bold
    ?job:courier:prompt{prop:FontColor} = -1
    ?job:courier:prompt{prop:Color} = 15066597
    If ?job:Postcode_Delivery{prop:ReadOnly} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 15066597
    Elsif ?job:Postcode_Delivery{prop:Req} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 8454143
    Else ! If ?job:Postcode_Delivery{prop:Req} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 16777215
    End ! If ?job:Postcode_Delivery{prop:Req} = True
    ?job:Postcode_Delivery{prop:Trn} = 0
    ?job:Postcode_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line1_Delivery{prop:ReadOnly} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line1_Delivery{prop:Req} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line1_Delivery{prop:Req} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line1_Delivery{prop:Req} = True
    ?job:Address_Line1_Delivery{prop:Trn} = 0
    ?job:Address_Line1_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line2_Delivery{prop:ReadOnly} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line2_Delivery{prop:Req} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line2_Delivery{prop:Req} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line2_Delivery{prop:Req} = True
    ?job:Address_Line2_Delivery{prop:Trn} = 0
    ?job:Address_Line2_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line3_Delivery{prop:ReadOnly} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line3_Delivery{prop:Req} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line3_Delivery{prop:Req} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line3_Delivery{prop:Req} = True
    ?job:Address_Line3_Delivery{prop:Trn} = 0
    ?job:Address_Line3_Delivery{prop:FontStyle} = font:Bold
    ?JOB:DelTelephoneNo:Prompt{prop:FontColor} = -1
    ?JOB:DelTelephoneNo:Prompt{prop:Color} = 15066597
    If ?job:Telephone_Delivery{prop:ReadOnly} = True
        ?job:Telephone_Delivery{prop:FontColor} = 65793
        ?job:Telephone_Delivery{prop:Color} = 15066597
    Elsif ?job:Telephone_Delivery{prop:Req} = True
        ?job:Telephone_Delivery{prop:FontColor} = 65793
        ?job:Telephone_Delivery{prop:Color} = 8454143
    Else ! If ?job:Telephone_Delivery{prop:Req} = True
        ?job:Telephone_Delivery{prop:FontColor} = 65793
        ?job:Telephone_Delivery{prop:Color} = 16777215
    End ! If ?job:Telephone_Delivery{prop:Req} = True
    ?job:Telephone_Delivery{prop:Trn} = 0
    ?job:Telephone_Delivery{prop:FontStyle} = font:Bold
    If ?job:Courier{prop:ReadOnly} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 15066597
    Elsif ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 8454143
    Else ! If ?job:Courier{prop:Req} = True
        ?job:Courier{prop:FontColor} = 65793
        ?job:Courier{prop:Color} = 16777215
    End ! If ?job:Courier{prop:Req} = True
    ?job:Courier{prop:Trn} = 0
    ?job:Courier{prop:FontStyle} = font:Bold
    ?jobe:EndUserEmailAddress:Prompt{prop:FontColor} = -1
    ?jobe:EndUserEmailAddress:Prompt{prop:Color} = 15066597
    If ?jobe:EndUserEmailAddress{prop:ReadOnly} = True
        ?jobe:EndUserEmailAddress{prop:FontColor} = 65793
        ?jobe:EndUserEmailAddress{prop:Color} = 15066597
    Elsif ?jobe:EndUserEmailAddress{prop:Req} = True
        ?jobe:EndUserEmailAddress{prop:FontColor} = 65793
        ?jobe:EndUserEmailAddress{prop:Color} = 8454143
    Else ! If ?jobe:EndUserEmailAddress{prop:Req} = True
        ?jobe:EndUserEmailAddress{prop:FontColor} = 65793
        ?jobe:EndUserEmailAddress{prop:Color} = 16777215
    End ! If ?jobe:EndUserEmailAddress{prop:Req} = True
    ?jobe:EndUserEmailAddress{prop:Trn} = 0
    ?jobe:EndUserEmailAddress{prop:FontStyle} = font:Bold
    ?Panel1:2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Show_Addresses',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceAddress;  SolaceCtrlName = '?InvoiceAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:CompanyName:Prompt;  SolaceCtrlName = '?JOB:CompanyName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name;  SolaceCtrlName = '?job:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Postcode:Prompt;  SolaceCtrlName = '?JOB:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode;  SolaceCtrlName = '?job:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:AddressLine1:Prompt;  SolaceCtrlName = '?JOB:AddressLine1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1;  SolaceCtrlName = '?job:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2;  SolaceCtrlName = '?job:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3;  SolaceCtrlName = '?job:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:TelephoneNumber:Prompt;  SolaceCtrlName = '?JOB:TelephoneNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Number;  SolaceCtrlName = '?job:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:TelephoneNumber:Prompt:2;  SolaceCtrlName = '?JOB:TelephoneNumber:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:FaxNumber:Prompt;  SolaceCtrlName = '?JOB:FaxNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fax_Number;  SolaceCtrlName = '?job:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CollectionAddress;  SolaceCtrlName = '?CollectionAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Collection;  SolaceCtrlName = '?job:Company_Name_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Collection;  SolaceCtrlName = '?job:Postcode_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CollClearCopy;  SolaceCtrlName = '?CollClearCopy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Collection;  SolaceCtrlName = '?job:Address_Line1_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Collection;  SolaceCtrlName = '?job:Address_Line2_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Collection;  SolaceCtrlName = '?job:Address_Line3_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:ColTelephoneNo:Prompt;  SolaceCtrlName = '?JOB:ColTelephoneNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Collection;  SolaceCtrlName = '?job:Telephone_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DeliveryAddress;  SolaceCtrlName = '?DeliveryAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Delivery;  SolaceCtrlName = '?job:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:courier:prompt;  SolaceCtrlName = '?job:courier:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Delivery;  SolaceCtrlName = '?job:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DelClearCopy;  SolaceCtrlName = '?DelClearCopy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Delivery;  SolaceCtrlName = '?job:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Delivery;  SolaceCtrlName = '?job:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Delivery;  SolaceCtrlName = '?job:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:DelTelephoneNo:Prompt;  SolaceCtrlName = '?JOB:DelTelephoneNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BrowseAddresses;  SolaceCtrlName = '?BrowseAddresses';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Delivery;  SolaceCtrlName = '?job:Telephone_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier;  SolaceCtrlName = '?job:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:EndUserEmailAddress:Prompt;  SolaceCtrlName = '?jobe:EndUserEmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:EndUserEmailAddress;  SolaceCtrlName = '?jobe:EndUserEmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1:2;  SolaceCtrlName = '?Panel1:2';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Show_Addresses')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Show_Addresses')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?InvoiceAddress
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:COURIER.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  Access:SUBACCAD.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  !Force Fields Check
  If ForcePostcode('B')
      ?job:Postcode{prop:Req} = 1
  Else
      ?job:Postcode{prop:Req} = 0
  End !ForcePostcode('B')
  
  If ForceDeliveryPostcode('B')
      ?job:Postcode_Delivery{prop:Req} = 1
  Else !ForceDeliveryPostcode('B')
      ?job:Postcode_Delivery{prop:Req} = 0
  End !ForceDeliveryPostcode('B')
  
  If ForceCourier('B')
      ?job:Courier{prop:Req} = 1
  Else !ForceCourier('B')
      ?job:Courier{prop:Req} = 0
  End !ForceCourier('B')
  Do RecolourWindow
  FDCB11.Init(job:Courier,?job:Courier,Queue:FileDropCombo.ViewPosition,FDCB11::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB11.Q &= Queue:FileDropCombo
  FDCB11.AddSortOrder(cou:Courier_Key)
  FDCB11.AddField(cou:Courier,FDCB11.Q.cou:Courier)
  ThisWindow.AddItem(FDCB11.WindowComponent)
  FDCB11.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Show_Addresses',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job:Postcode
      Postcode_Routine(job:Postcode,job:Address_Line1,job:Address_Line2,job:Address_Line3)
      Display()
      Select(?job:Address_Line1,1)
    OF ?job:Address_Line1
      IF job:address_line2 <> ''
          Select(?job:address_line3)
      End!IF job:addressline2 <> ''
      If job:address_line3 <> ''
          Select(?job:telephone_number)
      End!If job:addressline3 <> ''
    OF ?job:Telephone_Number
      
          temp_string = Clip(left(job:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Number    = temp_string
          Display(?job:Telephone_Number)
    OF ?job:Fax_Number
      
          temp_string = Clip(left(job:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Fax_Number    = temp_string
          Display(?job:Fax_Number)
    OF ?job:Postcode_Collection
      Postcode_Routine(job:Postcode_Collection,job:Address_Line1_Collection,job:Address_Line2_Collection,job:Address_Line3_Collection)
      Display()
      Select(?job:Address_Line1_Collection,1)
    OF ?CollClearCopy
      ThisWindow.Update
      glo:G_Select1 = ''
      glo:select1 = 'COLLECTION'
      If job:address_line1_collection <> ''
          glo:select2 = '1'
      Else
          glo:select2 = '2'
      End
      If job:address_line1_delivery = ''
          glo:select3 = 'DELIVERY'
      End
      Clear_Address_Window()
      Case glo:select1
          Of '1'
              JOB:Postcode_Collection=''
              JOB:Company_Name_Collection=''
              JOB:Address_Line1_Collection=''
              JOB:Address_Line2_Collection=''
              JOB:Address_Line3_Collection=''
              JOB:Telephone_Collection=''
              Select(?Job:postcode_collection)
          Of '2'
              job:postcode_collection      = job:postcode
              job:company_name_collection  = job:company_name
              job:address_line1_collection = job:address_line1
              job:address_line2_collection = job:address_line2
              job:address_line3_collection = job:address_line3
              job:telephone_collection     = job:telephone_number
          Of '4'
              job:postcode_collection      = job:postcode_delivery
              job:company_name_collection  = job:company_name_delivery
              job:address_line1_collection = job:address_line1_delivery
              job:address_line2_collection = job:address_line2_delivery
              job:address_line3_collection = job:address_line3_delivery
              job:telephone_collection     = job:telephone_delivery
          Of 'CANCEL'
      End
      Select(?job:postcode_collection)
      Display()
      glo:select1 = ''
      glo:select2 = ''
      
      glo:G_Select1 = ''
      
    OF ?job:Address_Line1_Collection
      IF job:address_line2_collection <> ''
          Select(?job:address_line3_collection)
      End!IF job:addressline2 <> ''
      If job:address_line3_collection <> ''
          Select(?job:telephone_collection)
      End!If job:addressline3 <> ''
    OF ?job:Telephone_Collection
      
          temp_string = Clip(left(job:Telephone_Collection))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Collection    = temp_string
          Display(?job:Telephone_Collection)
    OF ?job:Postcode_Delivery
      Postcode_Routine(job:Postcode_Delivery,job:Address_Line1_Delivery,job:Address_Line2_Delivery,job:Address_Line3_Delivery)
      Display()
      Select(?job:Address_Line1_Delivery,1)
    OF ?DelClearCopy
      ThisWindow.Update
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select1 = 'DELIVERY'
      If job:address_line1_delivery <> ''
          glo:select2 = '1'
      Else
          glo:select2 = '2'
      End
      If job:address_line1_collection = ''
          glo:select3 = 'COLLECTION'
      End
      
      Clear_Address_Window
      
      Case glo:select1
          Of '1'
              job:postcode_delivery      = ''
              job:company_name_delivery  = ''
              job:address_line1_delivery = ''
              job:address_line2_delivery = ''
              job:address_line3_delivery = ''
              job:telephone_delivery     = ''
              Select(?Job:postcode_delivery)
          Of '2'
              job:postcode_delivery      = job:postcode
              job:company_name_delivery  = job:company_name
              job:address_line1_delivery = job:address_line1
              job:address_line2_delivery = job:address_line2
              job:address_line3_delivery = job:address_line3
              job:telephone_delivery     = job:telephone_number
          Of '3'
              job:postcode_delivery      = job:postcode_collection
              job:company_name_delivery  = job:company_name_collection
              job:address_line1_delivery = job:address_line1_collection
              job:address_line2_delivery = job:address_line2_collection
              job:address_line3_delivery = job:address_line3_collection
              job:telephone_delivery     = job:telephone_collection
          Of 'CANCEL'
      End
      Select(?job:postcode_delivery)
      Display()
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
    OF ?job:Address_Line1_Delivery
      IF job:address_line2_delivery <> ''
          Select(?job:address_line3_delivery)
      End!IF job:addressline2 <> ''
      If job:address_line3_delivery <> ''
          Select(?job:telephone_delivery)
      End!If job:addressline3 <> ''
    OF ?BrowseAddresses
      ThisWindow.Update
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = job:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          GlobalRequest = SelectRecord
          PickSubAddresses(sub:RecordNumber)
          If GlobalResponse = RequestCompleted
              job:Postcode_Delivery       = sua:Postcode
              job:Address_Line1_Delivery  = sua:AddressLine1
              job:Company_Name_Delivery   = sua:CompanyName
              job:Address_Line2_Delivery  = sua:AddressLine2
              job:Address_Line3_Delivery  = sua:AddressLine3
              job:Telephone_Delivery      = sua:TelephoneNumber
              Display()
          End !If GlobalRequest = RecordCompleted
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    OF ?job:Telephone_Delivery
      
          temp_string = Clip(left(job:Telephone_Delivery))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Delivery    = temp_string
          Display(?job:Telephone_Delivery)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Show_Addresses')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

