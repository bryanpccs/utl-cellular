

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJ01024.INC'),ONCE        !Local module procedure declarations
                     END


CreatePickingNote    PROCEDURE  (partLocation)        ! Declare Procedure
pickcount            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CreatePickingNote')      !Add Procedure to Log
  end


    Access:LOCATION.ClearKey(loc:Location_Key)
    loc:Location = partLocation
    If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign then
        if loc:picknoteenable then !pick notes are enabled for this location

            pickcount = 0
            !*** Picking Note Code *** TH
            access:USERS.clearkey(use:password_key)
            use:Password = glo:Password
            access:USERS.fetch(use:password_key)

            if access:USERS.fetch(use:password_key) then !failure - problem with user login
            end

            access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
            pnt:JobReference = job:ref_number
            set(pnt:keyonjobno,pnt:keyonjobno)
            IsEdited = false
            loop
                if access:picknote.next() then
                    pickcount +=1
                    break !no picknote records
                end
                pickcount +=1
                if pnt:JobReference <> job:Ref_Number then break. !no matching records
                if pnt:Location <> partLocation then cycle. !no matching records on location
                if ~pnt:IsPrinted then !matching record - is it unprinted if so we can we append to it
                    !edit the existing record
                    IsEdited = true
                    break
                end
            end !loop

            if IsEdited then
            !we can append an existing pick note
                pnt:EngineerCode = job:engineer
                pnt:Usercode = use:User_Code
                !pnt:Location = may need in the future
                access:PICKNOTE.update()
            else
            !we need to create a new pick note
                access:PICKNOTE.primerecord()
                pnt:JobReference = job:Ref_Number
                pnt:PickNoteNumber = pickcount
                pnt:EngineerCode = job:engineer
                pnt:Usercode = use:User_Code
                pnt:Location = partLocation
                access:PICKNOTE.update()
            end
        else
!            Case MessageEx('Pick note not created. Parts location not enabled for pick notes.','ServiceBase 2000',|
!                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
!                Of 1 ! &OK Button
!            End!Case MessageEx
        end
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CreatePickingNote',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('pickcount',pickcount,'CreatePickingNote',1)


