

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01035.INC'),ONCE        !Local module procedure declarations
                     END


Compare_Fault_Codes PROCEDURE (f_ref_number,f_alias_ref_number) !Generated from procedure template - Window

FilesOpened          BYTE
save_maf_id          USHORT
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Compare Fault Codes'),AT(,,456,273),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,448,236),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Current Job'),AT(8,8),USE(?Prompt25),FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Historic Job'),AT(248,8),USE(?Prompt25:2),FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(8,24),USE(?JOB:Ref_Number:Prompt),TRN
                           ENTRY(@s8),AT(84,24,64,10),USE(job:Ref_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           LINE,AT(226,22,0,211),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Job Number'),AT(248,24),USE(?JOB:Ref_Number:Prompt:2),TRN
                           ENTRY(@s8),AT(324,24,64,10),USE(job_ali:Ref_Number),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           PROMPT('Fault Code 1:'),AT(8,44),USE(?JOB:Fault_Code1:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,44,124,10),USE(job:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 1:'),AT(248,44),USE(?JOB_ALI:Fault_Code1:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,44,124,10),USE(job_ali:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,44,124,10),USE(job_ali:Fault_Code1,,?JOB_ALI:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,44,124,10),USE(job:Fault_Code1,,?JOB:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 2:'),AT(8,60),USE(?JOB:Fault_Code2:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,60,124,10),USE(job:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 2:'),AT(248,60),USE(?JOB_ALI:Fault_Code2:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,60,124,10),USE(job_ali:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,60,124,10),USE(job_ali:Fault_Code2,,?JOB_ALI:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,60,124,10),USE(job:Fault_Code2,,?JOB:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 3:'),AT(8,76),USE(?JOB:Fault_Code3:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,76,124,10),USE(job:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 3:'),AT(248,76),USE(?JOB_ALI:Fault_Code3:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,76,124,10),USE(job_ali:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,76,124,10),USE(job_ali:Fault_Code3,,?JOB_ALI:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,76,124,10),USE(job:Fault_Code3,,?JOB:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 4:'),AT(8,92),USE(?JOB:Fault_Code4:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,92,124,10),USE(job:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 4:'),AT(248,92),USE(?JOB_ALI:Fault_Code4:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,92,124,10),USE(job_ali:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,92,124,10),USE(job:Fault_Code4,,?JOB:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 5:'),AT(8,108),USE(?JOB:Fault_Code5:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,108,124,10),USE(job:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,108,124,10),USE(job:Fault_Code5,,?JOB:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 6:'),AT(8,124),USE(?JOB:Fault_Code6:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,124,124,10),USE(job:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 5:'),AT(248,108),USE(?JOB_ALI:Fault_Code5:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,108,124,10),USE(job_ali:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,108,124,10),USE(job_ali:Fault_Code5,,?JOB_ALI:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,92,124,10),USE(job_ali:Fault_Code4,,?JOB_ALI:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,124,124,10),USE(job:Fault_Code6,,?JOB:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 7:'),AT(8,140),USE(?JOB:Fault_Code7:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,140,124,10),USE(job:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 6:'),AT(248,124),USE(?JOB_ALI:Fault_Code6:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,124,124,10),USE(job_ali:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,124,124,10),USE(job_ali:Fault_Code6,,?JOB_ALI:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 7:'),AT(248,140),USE(?JOB_ALI:Fault_Code7:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,140,124,10),USE(job_ali:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,140,124,10),USE(job_ali:Fault_Code7,,?JOB_ALI:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,140,124,10),USE(job:Fault_Code7,,?JOB:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 8:'),AT(8,156),USE(?JOB:Fault_Code8:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,156,124,10),USE(job:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 8:'),AT(248,156),USE(?JOB_ALI:Fault_Code8:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,156,124,10),USE(job_ali:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,156,124,10),USE(job:Fault_Code8,,?JOB:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 9:'),AT(8,172),USE(?JOB:Fault_Code9:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,172,124,10),USE(job:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 9:'),AT(248,172),USE(?JOB_ALI:Fault_Code9:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(324,172,124,10),USE(job_ali:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,156,124,10),USE(job_ali:Fault_Code8,,?JOB_ALI:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,172,124,10),USE(job_ali:Fault_Code9,,?JOB_ALI:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,172,124,10),USE(job:Fault_Code9,,?JOB:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 10:'),AT(8,188),USE(?JOB:Fault_Code10:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(84,188,124,10),USE(job:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 10:'),AT(248,188),USE(?JOB_ALI:Fault_Code10:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(324,188,124,10),USE(job_ali:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,188,124,10),USE(job_ali:Fault_Code10,,?JOB_ALI:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,188,124,10),USE(job:Fault_Code10,,?JOB:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 11:'),AT(8,204),USE(?JOB:Fault_Code11:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(84,204,124,10),USE(job:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 11:'),AT(248,204),USE(?JOB_ALI:Fault_Code11:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(324,204,124,10),USE(job_ali:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,204,124,10),USE(job_ali:Fault_Code11,,?JOB_ALI:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,204,124,10),USE(job:Fault_Code11,,?JOB:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 12:'),AT(8,220),USE(?JOB:Fault_Code12:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(84,220,124,10),USE(job:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Fault Code 12:'),AT(248,220),USE(?JOB_ALI:Fault_Code12:Prompt),TRN,HIDE
                           ENTRY(@s255),AT(324,220,124,10),USE(job_ali:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(304,220,124,10),USE(job_ali:Fault_Code12,,?JOB_ALI:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           ENTRY(@d6b),AT(60,220,124,10),USE(job:Fault_Code12,,?JOB:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                       END
                       BUTTON('Close'),AT(392,248,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,244,448,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?Prompt25:2{prop:FontColor} = -1
    ?Prompt25:2{prop:Color} = 15066597
    ?JOB:Ref_Number:Prompt{prop:FontColor} = -1
    ?JOB:Ref_Number:Prompt{prop:Color} = 15066597
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold
    ?JOB:Ref_Number:Prompt:2{prop:FontColor} = -1
    ?JOB:Ref_Number:Prompt:2{prop:Color} = 15066597
    If ?job_ali:Ref_Number{prop:ReadOnly} = True
        ?job_ali:Ref_Number{prop:FontColor} = 65793
        ?job_ali:Ref_Number{prop:Color} = 15066597
    Elsif ?job_ali:Ref_Number{prop:Req} = True
        ?job_ali:Ref_Number{prop:FontColor} = 65793
        ?job_ali:Ref_Number{prop:Color} = 8454143
    Else ! If ?job_ali:Ref_Number{prop:Req} = True
        ?job_ali:Ref_Number{prop:FontColor} = 65793
        ?job_ali:Ref_Number{prop:Color} = 16777215
    End ! If ?job_ali:Ref_Number{prop:Req} = True
    ?job_ali:Ref_Number{prop:Trn} = 0
    ?job_ali:Ref_Number{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code1:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code1{prop:ReadOnly} = True
        ?job:Fault_Code1{prop:FontColor} = 65793
        ?job:Fault_Code1{prop:Color} = 15066597
    Elsif ?job:Fault_Code1{prop:Req} = True
        ?job:Fault_Code1{prop:FontColor} = 65793
        ?job:Fault_Code1{prop:Color} = 8454143
    Else ! If ?job:Fault_Code1{prop:Req} = True
        ?job:Fault_Code1{prop:FontColor} = 65793
        ?job:Fault_Code1{prop:Color} = 16777215
    End ! If ?job:Fault_Code1{prop:Req} = True
    ?job:Fault_Code1{prop:Trn} = 0
    ?job:Fault_Code1{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code1:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code1{prop:ReadOnly} = True
        ?job_ali:Fault_Code1{prop:FontColor} = 65793
        ?job_ali:Fault_Code1{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code1{prop:Req} = True
        ?job_ali:Fault_Code1{prop:FontColor} = 65793
        ?job_ali:Fault_Code1{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code1{prop:Req} = True
        ?job_ali:Fault_Code1{prop:FontColor} = 65793
        ?job_ali:Fault_Code1{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code1{prop:Req} = True
    ?job_ali:Fault_Code1{prop:Trn} = 0
    ?job_ali:Fault_Code1{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code1:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code1:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code1:2{prop:Req} = True
        ?JOB_ALI:Fault_Code1:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code1:2{prop:Req} = True
        ?JOB_ALI:Fault_Code1:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code1:2{prop:Req} = True
    ?JOB_ALI:Fault_Code1:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code1:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code1:2{prop:ReadOnly} = True
        ?JOB:Fault_Code1:2{prop:FontColor} = 65793
        ?JOB:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code1:2{prop:Req} = True
        ?JOB:Fault_Code1:2{prop:FontColor} = 65793
        ?JOB:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code1:2{prop:Req} = True
        ?JOB:Fault_Code1:2{prop:FontColor} = 65793
        ?JOB:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code1:2{prop:Req} = True
    ?JOB:Fault_Code1:2{prop:Trn} = 0
    ?JOB:Fault_Code1:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code2:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code2{prop:ReadOnly} = True
        ?job:Fault_Code2{prop:FontColor} = 65793
        ?job:Fault_Code2{prop:Color} = 15066597
    Elsif ?job:Fault_Code2{prop:Req} = True
        ?job:Fault_Code2{prop:FontColor} = 65793
        ?job:Fault_Code2{prop:Color} = 8454143
    Else ! If ?job:Fault_Code2{prop:Req} = True
        ?job:Fault_Code2{prop:FontColor} = 65793
        ?job:Fault_Code2{prop:Color} = 16777215
    End ! If ?job:Fault_Code2{prop:Req} = True
    ?job:Fault_Code2{prop:Trn} = 0
    ?job:Fault_Code2{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code2:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code2{prop:ReadOnly} = True
        ?job_ali:Fault_Code2{prop:FontColor} = 65793
        ?job_ali:Fault_Code2{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code2{prop:Req} = True
        ?job_ali:Fault_Code2{prop:FontColor} = 65793
        ?job_ali:Fault_Code2{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code2{prop:Req} = True
        ?job_ali:Fault_Code2{prop:FontColor} = 65793
        ?job_ali:Fault_Code2{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code2{prop:Req} = True
    ?job_ali:Fault_Code2{prop:Trn} = 0
    ?job_ali:Fault_Code2{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code2:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code2:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code2:2{prop:Req} = True
        ?JOB_ALI:Fault_Code2:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code2:2{prop:Req} = True
        ?JOB_ALI:Fault_Code2:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code2:2{prop:Req} = True
    ?JOB_ALI:Fault_Code2:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code2:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code2:2{prop:ReadOnly} = True
        ?JOB:Fault_Code2:2{prop:FontColor} = 65793
        ?JOB:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code2:2{prop:Req} = True
        ?JOB:Fault_Code2:2{prop:FontColor} = 65793
        ?JOB:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code2:2{prop:Req} = True
        ?JOB:Fault_Code2:2{prop:FontColor} = 65793
        ?JOB:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code2:2{prop:Req} = True
    ?JOB:Fault_Code2:2{prop:Trn} = 0
    ?JOB:Fault_Code2:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code3:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code3{prop:ReadOnly} = True
        ?job:Fault_Code3{prop:FontColor} = 65793
        ?job:Fault_Code3{prop:Color} = 15066597
    Elsif ?job:Fault_Code3{prop:Req} = True
        ?job:Fault_Code3{prop:FontColor} = 65793
        ?job:Fault_Code3{prop:Color} = 8454143
    Else ! If ?job:Fault_Code3{prop:Req} = True
        ?job:Fault_Code3{prop:FontColor} = 65793
        ?job:Fault_Code3{prop:Color} = 16777215
    End ! If ?job:Fault_Code3{prop:Req} = True
    ?job:Fault_Code3{prop:Trn} = 0
    ?job:Fault_Code3{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code3:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code3{prop:ReadOnly} = True
        ?job_ali:Fault_Code3{prop:FontColor} = 65793
        ?job_ali:Fault_Code3{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code3{prop:Req} = True
        ?job_ali:Fault_Code3{prop:FontColor} = 65793
        ?job_ali:Fault_Code3{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code3{prop:Req} = True
        ?job_ali:Fault_Code3{prop:FontColor} = 65793
        ?job_ali:Fault_Code3{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code3{prop:Req} = True
    ?job_ali:Fault_Code3{prop:Trn} = 0
    ?job_ali:Fault_Code3{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code3:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code3:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code3:2{prop:Req} = True
        ?JOB_ALI:Fault_Code3:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code3:2{prop:Req} = True
        ?JOB_ALI:Fault_Code3:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code3:2{prop:Req} = True
    ?JOB_ALI:Fault_Code3:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code3:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code3:2{prop:ReadOnly} = True
        ?JOB:Fault_Code3:2{prop:FontColor} = 65793
        ?JOB:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code3:2{prop:Req} = True
        ?JOB:Fault_Code3:2{prop:FontColor} = 65793
        ?JOB:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code3:2{prop:Req} = True
        ?JOB:Fault_Code3:2{prop:FontColor} = 65793
        ?JOB:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code3:2{prop:Req} = True
    ?JOB:Fault_Code3:2{prop:Trn} = 0
    ?JOB:Fault_Code3:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code4:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code4{prop:ReadOnly} = True
        ?job:Fault_Code4{prop:FontColor} = 65793
        ?job:Fault_Code4{prop:Color} = 15066597
    Elsif ?job:Fault_Code4{prop:Req} = True
        ?job:Fault_Code4{prop:FontColor} = 65793
        ?job:Fault_Code4{prop:Color} = 8454143
    Else ! If ?job:Fault_Code4{prop:Req} = True
        ?job:Fault_Code4{prop:FontColor} = 65793
        ?job:Fault_Code4{prop:Color} = 16777215
    End ! If ?job:Fault_Code4{prop:Req} = True
    ?job:Fault_Code4{prop:Trn} = 0
    ?job:Fault_Code4{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code4:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code4{prop:ReadOnly} = True
        ?job_ali:Fault_Code4{prop:FontColor} = 65793
        ?job_ali:Fault_Code4{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code4{prop:Req} = True
        ?job_ali:Fault_Code4{prop:FontColor} = 65793
        ?job_ali:Fault_Code4{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code4{prop:Req} = True
        ?job_ali:Fault_Code4{prop:FontColor} = 65793
        ?job_ali:Fault_Code4{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code4{prop:Req} = True
    ?job_ali:Fault_Code4{prop:Trn} = 0
    ?job_ali:Fault_Code4{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code4:2{prop:ReadOnly} = True
        ?JOB:Fault_Code4:2{prop:FontColor} = 65793
        ?JOB:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code4:2{prop:Req} = True
        ?JOB:Fault_Code4:2{prop:FontColor} = 65793
        ?JOB:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code4:2{prop:Req} = True
        ?JOB:Fault_Code4:2{prop:FontColor} = 65793
        ?JOB:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code4:2{prop:Req} = True
    ?JOB:Fault_Code4:2{prop:Trn} = 0
    ?JOB:Fault_Code4:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code5:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code5{prop:ReadOnly} = True
        ?job:Fault_Code5{prop:FontColor} = 65793
        ?job:Fault_Code5{prop:Color} = 15066597
    Elsif ?job:Fault_Code5{prop:Req} = True
        ?job:Fault_Code5{prop:FontColor} = 65793
        ?job:Fault_Code5{prop:Color} = 8454143
    Else ! If ?job:Fault_Code5{prop:Req} = True
        ?job:Fault_Code5{prop:FontColor} = 65793
        ?job:Fault_Code5{prop:Color} = 16777215
    End ! If ?job:Fault_Code5{prop:Req} = True
    ?job:Fault_Code5{prop:Trn} = 0
    ?job:Fault_Code5{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code5:2{prop:ReadOnly} = True
        ?JOB:Fault_Code5:2{prop:FontColor} = 65793
        ?JOB:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code5:2{prop:Req} = True
        ?JOB:Fault_Code5:2{prop:FontColor} = 65793
        ?JOB:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code5:2{prop:Req} = True
        ?JOB:Fault_Code5:2{prop:FontColor} = 65793
        ?JOB:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code5:2{prop:Req} = True
    ?JOB:Fault_Code5:2{prop:Trn} = 0
    ?JOB:Fault_Code5:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code6:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code6{prop:ReadOnly} = True
        ?job:Fault_Code6{prop:FontColor} = 65793
        ?job:Fault_Code6{prop:Color} = 15066597
    Elsif ?job:Fault_Code6{prop:Req} = True
        ?job:Fault_Code6{prop:FontColor} = 65793
        ?job:Fault_Code6{prop:Color} = 8454143
    Else ! If ?job:Fault_Code6{prop:Req} = True
        ?job:Fault_Code6{prop:FontColor} = 65793
        ?job:Fault_Code6{prop:Color} = 16777215
    End ! If ?job:Fault_Code6{prop:Req} = True
    ?job:Fault_Code6{prop:Trn} = 0
    ?job:Fault_Code6{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code5:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code5{prop:ReadOnly} = True
        ?job_ali:Fault_Code5{prop:FontColor} = 65793
        ?job_ali:Fault_Code5{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code5{prop:Req} = True
        ?job_ali:Fault_Code5{prop:FontColor} = 65793
        ?job_ali:Fault_Code5{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code5{prop:Req} = True
        ?job_ali:Fault_Code5{prop:FontColor} = 65793
        ?job_ali:Fault_Code5{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code5{prop:Req} = True
    ?job_ali:Fault_Code5{prop:Trn} = 0
    ?job_ali:Fault_Code5{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code5:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code5:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code5:2{prop:Req} = True
        ?JOB_ALI:Fault_Code5:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code5:2{prop:Req} = True
        ?JOB_ALI:Fault_Code5:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code5:2{prop:Req} = True
    ?JOB_ALI:Fault_Code5:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code5:2{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code4:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code4:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code4:2{prop:Req} = True
        ?JOB_ALI:Fault_Code4:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code4:2{prop:Req} = True
        ?JOB_ALI:Fault_Code4:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code4:2{prop:Req} = True
    ?JOB_ALI:Fault_Code4:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code4:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code6:2{prop:ReadOnly} = True
        ?JOB:Fault_Code6:2{prop:FontColor} = 65793
        ?JOB:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code6:2{prop:Req} = True
        ?JOB:Fault_Code6:2{prop:FontColor} = 65793
        ?JOB:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code6:2{prop:Req} = True
        ?JOB:Fault_Code6:2{prop:FontColor} = 65793
        ?JOB:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code6:2{prop:Req} = True
    ?JOB:Fault_Code6:2{prop:Trn} = 0
    ?JOB:Fault_Code6:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code7:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code7{prop:ReadOnly} = True
        ?job:Fault_Code7{prop:FontColor} = 65793
        ?job:Fault_Code7{prop:Color} = 15066597
    Elsif ?job:Fault_Code7{prop:Req} = True
        ?job:Fault_Code7{prop:FontColor} = 65793
        ?job:Fault_Code7{prop:Color} = 8454143
    Else ! If ?job:Fault_Code7{prop:Req} = True
        ?job:Fault_Code7{prop:FontColor} = 65793
        ?job:Fault_Code7{prop:Color} = 16777215
    End ! If ?job:Fault_Code7{prop:Req} = True
    ?job:Fault_Code7{prop:Trn} = 0
    ?job:Fault_Code7{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code6:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code6{prop:ReadOnly} = True
        ?job_ali:Fault_Code6{prop:FontColor} = 65793
        ?job_ali:Fault_Code6{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code6{prop:Req} = True
        ?job_ali:Fault_Code6{prop:FontColor} = 65793
        ?job_ali:Fault_Code6{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code6{prop:Req} = True
        ?job_ali:Fault_Code6{prop:FontColor} = 65793
        ?job_ali:Fault_Code6{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code6{prop:Req} = True
    ?job_ali:Fault_Code6{prop:Trn} = 0
    ?job_ali:Fault_Code6{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code6:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code6:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code6:2{prop:Req} = True
        ?JOB_ALI:Fault_Code6:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code6:2{prop:Req} = True
        ?JOB_ALI:Fault_Code6:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code6:2{prop:Req} = True
    ?JOB_ALI:Fault_Code6:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code6:2{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code7:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code7{prop:ReadOnly} = True
        ?job_ali:Fault_Code7{prop:FontColor} = 65793
        ?job_ali:Fault_Code7{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code7{prop:Req} = True
        ?job_ali:Fault_Code7{prop:FontColor} = 65793
        ?job_ali:Fault_Code7{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code7{prop:Req} = True
        ?job_ali:Fault_Code7{prop:FontColor} = 65793
        ?job_ali:Fault_Code7{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code7{prop:Req} = True
    ?job_ali:Fault_Code7{prop:Trn} = 0
    ?job_ali:Fault_Code7{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code7:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code7:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code7:2{prop:Req} = True
        ?JOB_ALI:Fault_Code7:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code7:2{prop:Req} = True
        ?JOB_ALI:Fault_Code7:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code7:2{prop:Req} = True
    ?JOB_ALI:Fault_Code7:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code7:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code7:2{prop:ReadOnly} = True
        ?JOB:Fault_Code7:2{prop:FontColor} = 65793
        ?JOB:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code7:2{prop:Req} = True
        ?JOB:Fault_Code7:2{prop:FontColor} = 65793
        ?JOB:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code7:2{prop:Req} = True
        ?JOB:Fault_Code7:2{prop:FontColor} = 65793
        ?JOB:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code7:2{prop:Req} = True
    ?JOB:Fault_Code7:2{prop:Trn} = 0
    ?JOB:Fault_Code7:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code8:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code8{prop:ReadOnly} = True
        ?job:Fault_Code8{prop:FontColor} = 65793
        ?job:Fault_Code8{prop:Color} = 15066597
    Elsif ?job:Fault_Code8{prop:Req} = True
        ?job:Fault_Code8{prop:FontColor} = 65793
        ?job:Fault_Code8{prop:Color} = 8454143
    Else ! If ?job:Fault_Code8{prop:Req} = True
        ?job:Fault_Code8{prop:FontColor} = 65793
        ?job:Fault_Code8{prop:Color} = 16777215
    End ! If ?job:Fault_Code8{prop:Req} = True
    ?job:Fault_Code8{prop:Trn} = 0
    ?job:Fault_Code8{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code8:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code8{prop:ReadOnly} = True
        ?job_ali:Fault_Code8{prop:FontColor} = 65793
        ?job_ali:Fault_Code8{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code8{prop:Req} = True
        ?job_ali:Fault_Code8{prop:FontColor} = 65793
        ?job_ali:Fault_Code8{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code8{prop:Req} = True
        ?job_ali:Fault_Code8{prop:FontColor} = 65793
        ?job_ali:Fault_Code8{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code8{prop:Req} = True
    ?job_ali:Fault_Code8{prop:Trn} = 0
    ?job_ali:Fault_Code8{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code8:2{prop:ReadOnly} = True
        ?JOB:Fault_Code8:2{prop:FontColor} = 65793
        ?JOB:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code8:2{prop:Req} = True
        ?JOB:Fault_Code8:2{prop:FontColor} = 65793
        ?JOB:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code8:2{prop:Req} = True
        ?JOB:Fault_Code8:2{prop:FontColor} = 65793
        ?JOB:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code8:2{prop:Req} = True
    ?JOB:Fault_Code8:2{prop:Trn} = 0
    ?JOB:Fault_Code8:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code9:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code9{prop:ReadOnly} = True
        ?job:Fault_Code9{prop:FontColor} = 65793
        ?job:Fault_Code9{prop:Color} = 15066597
    Elsif ?job:Fault_Code9{prop:Req} = True
        ?job:Fault_Code9{prop:FontColor} = 65793
        ?job:Fault_Code9{prop:Color} = 8454143
    Else ! If ?job:Fault_Code9{prop:Req} = True
        ?job:Fault_Code9{prop:FontColor} = 65793
        ?job:Fault_Code9{prop:Color} = 16777215
    End ! If ?job:Fault_Code9{prop:Req} = True
    ?job:Fault_Code9{prop:Trn} = 0
    ?job:Fault_Code9{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code9:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code9{prop:ReadOnly} = True
        ?job_ali:Fault_Code9{prop:FontColor} = 65793
        ?job_ali:Fault_Code9{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code9{prop:Req} = True
        ?job_ali:Fault_Code9{prop:FontColor} = 65793
        ?job_ali:Fault_Code9{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code9{prop:Req} = True
        ?job_ali:Fault_Code9{prop:FontColor} = 65793
        ?job_ali:Fault_Code9{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code9{prop:Req} = True
    ?job_ali:Fault_Code9{prop:Trn} = 0
    ?job_ali:Fault_Code9{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code8:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code8:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code8:2{prop:Req} = True
        ?JOB_ALI:Fault_Code8:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code8:2{prop:Req} = True
        ?JOB_ALI:Fault_Code8:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code8:2{prop:Req} = True
    ?JOB_ALI:Fault_Code8:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code8:2{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code9:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code9:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code9:2{prop:Req} = True
        ?JOB_ALI:Fault_Code9:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code9:2{prop:Req} = True
        ?JOB_ALI:Fault_Code9:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code9:2{prop:Req} = True
    ?JOB_ALI:Fault_Code9:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code9:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code9:2{prop:ReadOnly} = True
        ?JOB:Fault_Code9:2{prop:FontColor} = 65793
        ?JOB:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code9:2{prop:Req} = True
        ?JOB:Fault_Code9:2{prop:FontColor} = 65793
        ?JOB:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code9:2{prop:Req} = True
        ?JOB:Fault_Code9:2{prop:FontColor} = 65793
        ?JOB:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code9:2{prop:Req} = True
    ?JOB:Fault_Code9:2{prop:Trn} = 0
    ?JOB:Fault_Code9:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code10:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code10{prop:ReadOnly} = True
        ?job:Fault_Code10{prop:FontColor} = 65793
        ?job:Fault_Code10{prop:Color} = 15066597
    Elsif ?job:Fault_Code10{prop:Req} = True
        ?job:Fault_Code10{prop:FontColor} = 65793
        ?job:Fault_Code10{prop:Color} = 8454143
    Else ! If ?job:Fault_Code10{prop:Req} = True
        ?job:Fault_Code10{prop:FontColor} = 65793
        ?job:Fault_Code10{prop:Color} = 16777215
    End ! If ?job:Fault_Code10{prop:Req} = True
    ?job:Fault_Code10{prop:Trn} = 0
    ?job:Fault_Code10{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code10:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code10{prop:ReadOnly} = True
        ?job_ali:Fault_Code10{prop:FontColor} = 65793
        ?job_ali:Fault_Code10{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code10{prop:Req} = True
        ?job_ali:Fault_Code10{prop:FontColor} = 65793
        ?job_ali:Fault_Code10{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code10{prop:Req} = True
        ?job_ali:Fault_Code10{prop:FontColor} = 65793
        ?job_ali:Fault_Code10{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code10{prop:Req} = True
    ?job_ali:Fault_Code10{prop:Trn} = 0
    ?job_ali:Fault_Code10{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code10:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code10:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code10:2{prop:Req} = True
        ?JOB_ALI:Fault_Code10:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code10:2{prop:Req} = True
        ?JOB_ALI:Fault_Code10:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code10:2{prop:Req} = True
    ?JOB_ALI:Fault_Code10:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code10:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code10:2{prop:ReadOnly} = True
        ?JOB:Fault_Code10:2{prop:FontColor} = 65793
        ?JOB:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code10:2{prop:Req} = True
        ?JOB:Fault_Code10:2{prop:FontColor} = 65793
        ?JOB:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code10:2{prop:Req} = True
        ?JOB:Fault_Code10:2{prop:FontColor} = 65793
        ?JOB:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code10:2{prop:Req} = True
    ?JOB:Fault_Code10:2{prop:Trn} = 0
    ?JOB:Fault_Code10:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code11:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code11{prop:ReadOnly} = True
        ?job:Fault_Code11{prop:FontColor} = 65793
        ?job:Fault_Code11{prop:Color} = 15066597
    Elsif ?job:Fault_Code11{prop:Req} = True
        ?job:Fault_Code11{prop:FontColor} = 65793
        ?job:Fault_Code11{prop:Color} = 8454143
    Else ! If ?job:Fault_Code11{prop:Req} = True
        ?job:Fault_Code11{prop:FontColor} = 65793
        ?job:Fault_Code11{prop:Color} = 16777215
    End ! If ?job:Fault_Code11{prop:Req} = True
    ?job:Fault_Code11{prop:Trn} = 0
    ?job:Fault_Code11{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code11:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code11{prop:ReadOnly} = True
        ?job_ali:Fault_Code11{prop:FontColor} = 65793
        ?job_ali:Fault_Code11{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code11{prop:Req} = True
        ?job_ali:Fault_Code11{prop:FontColor} = 65793
        ?job_ali:Fault_Code11{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code11{prop:Req} = True
        ?job_ali:Fault_Code11{prop:FontColor} = 65793
        ?job_ali:Fault_Code11{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code11{prop:Req} = True
    ?job_ali:Fault_Code11{prop:Trn} = 0
    ?job_ali:Fault_Code11{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code11:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code11:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code11:2{prop:Req} = True
        ?JOB_ALI:Fault_Code11:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code11:2{prop:Req} = True
        ?JOB_ALI:Fault_Code11:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code11:2{prop:Req} = True
    ?JOB_ALI:Fault_Code11:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code11:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code11:2{prop:ReadOnly} = True
        ?JOB:Fault_Code11:2{prop:FontColor} = 65793
        ?JOB:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code11:2{prop:Req} = True
        ?JOB:Fault_Code11:2{prop:FontColor} = 65793
        ?JOB:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code11:2{prop:Req} = True
        ?JOB:Fault_Code11:2{prop:FontColor} = 65793
        ?JOB:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code11:2{prop:Req} = True
    ?JOB:Fault_Code11:2{prop:Trn} = 0
    ?JOB:Fault_Code11:2{prop:FontStyle} = font:Bold
    ?JOB:Fault_Code12:Prompt{prop:FontColor} = -1
    ?JOB:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?job:Fault_Code12{prop:ReadOnly} = True
        ?job:Fault_Code12{prop:FontColor} = 65793
        ?job:Fault_Code12{prop:Color} = 15066597
    Elsif ?job:Fault_Code12{prop:Req} = True
        ?job:Fault_Code12{prop:FontColor} = 65793
        ?job:Fault_Code12{prop:Color} = 8454143
    Else ! If ?job:Fault_Code12{prop:Req} = True
        ?job:Fault_Code12{prop:FontColor} = 65793
        ?job:Fault_Code12{prop:Color} = 16777215
    End ! If ?job:Fault_Code12{prop:Req} = True
    ?job:Fault_Code12{prop:Trn} = 0
    ?job:Fault_Code12{prop:FontStyle} = font:Bold
    ?JOB_ALI:Fault_Code12:Prompt{prop:FontColor} = -1
    ?JOB_ALI:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?job_ali:Fault_Code12{prop:ReadOnly} = True
        ?job_ali:Fault_Code12{prop:FontColor} = 65793
        ?job_ali:Fault_Code12{prop:Color} = 15066597
    Elsif ?job_ali:Fault_Code12{prop:Req} = True
        ?job_ali:Fault_Code12{prop:FontColor} = 65793
        ?job_ali:Fault_Code12{prop:Color} = 8454143
    Else ! If ?job_ali:Fault_Code12{prop:Req} = True
        ?job_ali:Fault_Code12{prop:FontColor} = 65793
        ?job_ali:Fault_Code12{prop:Color} = 16777215
    End ! If ?job_ali:Fault_Code12{prop:Req} = True
    ?job_ali:Fault_Code12{prop:Trn} = 0
    ?job_ali:Fault_Code12{prop:FontStyle} = font:Bold
    If ?JOB_ALI:Fault_Code12:2{prop:ReadOnly} = True
        ?JOB_ALI:Fault_Code12:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?JOB_ALI:Fault_Code12:2{prop:Req} = True
        ?JOB_ALI:Fault_Code12:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?JOB_ALI:Fault_Code12:2{prop:Req} = True
        ?JOB_ALI:Fault_Code12:2{prop:FontColor} = 65793
        ?JOB_ALI:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?JOB_ALI:Fault_Code12:2{prop:Req} = True
    ?JOB_ALI:Fault_Code12:2{prop:Trn} = 0
    ?JOB_ALI:Fault_Code12:2{prop:FontStyle} = font:Bold
    If ?JOB:Fault_Code12:2{prop:ReadOnly} = True
        ?JOB:Fault_Code12:2{prop:FontColor} = 65793
        ?JOB:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?JOB:Fault_Code12:2{prop:Req} = True
        ?JOB:Fault_Code12:2{prop:FontColor} = 65793
        ?JOB:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?JOB:Fault_Code12:2{prop:Req} = True
        ?JOB:Fault_Code12:2{prop:FontColor} = 65793
        ?JOB:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?JOB:Fault_Code12:2{prop:Req} = True
    ?JOB:Fault_Code12:2{prop:Trn} = 0
    ?JOB:Fault_Code12:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Compare_Fault_Codes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Compare_Fault_Codes',1)
    SolaceViewVars('save_maf_id',save_maf_id,'Compare_Fault_Codes',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25:2;  SolaceCtrlName = '?Prompt25:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:Prompt;  SolaceCtrlName = '?JOB:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:Prompt:2;  SolaceCtrlName = '?JOB:Ref_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Ref_Number;  SolaceCtrlName = '?job_ali:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code1:Prompt;  SolaceCtrlName = '?JOB:Fault_Code1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code1;  SolaceCtrlName = '?job:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code1:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code1;  SolaceCtrlName = '?job_ali:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code1:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code1:2;  SolaceCtrlName = '?JOB:Fault_Code1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code2:Prompt;  SolaceCtrlName = '?JOB:Fault_Code2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code2;  SolaceCtrlName = '?job:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code2:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code2;  SolaceCtrlName = '?job_ali:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code2:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code2:2;  SolaceCtrlName = '?JOB:Fault_Code2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code3:Prompt;  SolaceCtrlName = '?JOB:Fault_Code3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code3;  SolaceCtrlName = '?job:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code3:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code3;  SolaceCtrlName = '?job_ali:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code3:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code3:2;  SolaceCtrlName = '?JOB:Fault_Code3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code4:Prompt;  SolaceCtrlName = '?JOB:Fault_Code4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code4;  SolaceCtrlName = '?job:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code4:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code4;  SolaceCtrlName = '?job_ali:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code4:2;  SolaceCtrlName = '?JOB:Fault_Code4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code5:Prompt;  SolaceCtrlName = '?JOB:Fault_Code5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code5;  SolaceCtrlName = '?job:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code5:2;  SolaceCtrlName = '?JOB:Fault_Code5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code6:Prompt;  SolaceCtrlName = '?JOB:Fault_Code6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code6;  SolaceCtrlName = '?job:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code5:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code5;  SolaceCtrlName = '?job_ali:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code5:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code4:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code6:2;  SolaceCtrlName = '?JOB:Fault_Code6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code7:Prompt;  SolaceCtrlName = '?JOB:Fault_Code7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code7;  SolaceCtrlName = '?job:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code6:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code6;  SolaceCtrlName = '?job_ali:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code6:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code7:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code7;  SolaceCtrlName = '?job_ali:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code7:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code7:2;  SolaceCtrlName = '?JOB:Fault_Code7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code8:Prompt;  SolaceCtrlName = '?JOB:Fault_Code8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code8;  SolaceCtrlName = '?job:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code8:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code8;  SolaceCtrlName = '?job_ali:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code8:2;  SolaceCtrlName = '?JOB:Fault_Code8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code9:Prompt;  SolaceCtrlName = '?JOB:Fault_Code9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code9;  SolaceCtrlName = '?job:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code9:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code9;  SolaceCtrlName = '?job_ali:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code8:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code9:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code9:2;  SolaceCtrlName = '?JOB:Fault_Code9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code10:Prompt;  SolaceCtrlName = '?JOB:Fault_Code10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code10;  SolaceCtrlName = '?job:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code10:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code10;  SolaceCtrlName = '?job_ali:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code10:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code10:2;  SolaceCtrlName = '?JOB:Fault_Code10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code11:Prompt;  SolaceCtrlName = '?JOB:Fault_Code11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code11;  SolaceCtrlName = '?job:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code11:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code11;  SolaceCtrlName = '?job_ali:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code11:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code11:2;  SolaceCtrlName = '?JOB:Fault_Code11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code12:Prompt;  SolaceCtrlName = '?JOB:Fault_Code12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fault_Code12;  SolaceCtrlName = '?job:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code12:Prompt;  SolaceCtrlName = '?JOB_ALI:Fault_Code12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fault_Code12;  SolaceCtrlName = '?job_ali:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB_ALI:Fault_Code12:2;  SolaceCtrlName = '?JOB_ALI:Fault_Code12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fault_Code12:2;  SolaceCtrlName = '?JOB:Fault_Code12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Compare_Fault_Codes')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Compare_Fault_Codes')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt25
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  Access:JOBS_ALIAS.UseFile
  Access:MANFAULT.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Compare_Fault_Codes',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Compare_Fault_Codes')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Job Fault Codes
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = f_ref_number
      If access:jobs.fetch(job:ref_number_key) = Level:Benign
      
          setcursor(cursor:wait)
          save_maf_id = access:manfault.savefile()
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job:manufacturer
          set(maf:field_number_key,maf:field_number_key)
          loop
              if access:manfault.next()
                 break
              end !if
              if maf:manufacturer <> job:manufacturer      |
                  then break.  ! end if
              Case maf:field_number
                  Of 1
                      Unhide(?job:fault_code1:prompt)
                      ?job:fault_code1:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code1:2)
                          ?job:fault_code1:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code1)
                      End
                  Of 2
                      Unhide(?job:fault_code2:prompt)
                      ?job:fault_code2:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code2:2)
                          ?job:fault_code2:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code2)
                      End
      
                  Of 3
                      Unhide(?job:fault_code3:prompt)
                      ?job:fault_code3:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code3:2)
                          ?job:fault_code3:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code3)
                      End
      
                  Of 4
                      Unhide(?job:fault_code4:prompt)
                      ?job:fault_code4:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code4:2)
                          ?job:fault_code4:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code4)
                      End
      
                  Of 5
                      Unhide(?job:fault_code5:prompt)
                      ?job:fault_code5:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code5:2)
                          ?job:fault_code5:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code5)
                      End
      
                  Of 6
                      Unhide(?job:fault_code6:prompt)
                      ?job:fault_code6:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code6:2)
                          ?job:fault_code6:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code6)
                      End
      
                  Of 7
                      Unhide(?job:fault_code7:prompt)
                      ?job:fault_code7:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code7:2)
                          ?job:fault_code7:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code7)
                      End
      
                  Of 8
                      Unhide(?job:fault_code8:prompt)
                      ?job:fault_code8:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code8:2)
                          ?job:fault_code8:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code8)
                      End
      
                  Of 9
                      Unhide(?job:fault_code9:prompt)
                      ?job:fault_code9:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code9:2)
                          ?job:fault_code9:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code9)
                      End
      
                  Of 10
                      Unhide(?job:fault_code10:prompt)
                      ?job:fault_code10:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code10:2)
                          ?job:fault_code10:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code10)
                      End
      
                  Of 11
                      Unhide(?job:fault_code11:prompt)
                      ?job:fault_code11:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code11:2)
                          ?job:fault_code11:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code11)
                      End
      
                  Of 12
                      Unhide(?job:fault_code12:prompt)
                      ?job:fault_code12:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job:fault_code12:2)
                          ?job:fault_code12:2{prop:xpos} = 84
                      Else
                          Unhide(?job:fault_code12)
                      End
              End
          end !loop
          access:manfault.restorefile(save_maf_id)
          setcursor()
      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      !Job Alias Fault Codes
      access:jobs_alias.clearkey(job_ali:ref_number_key)
      job_ali:ref_number  = f_alias_ref_number
      If access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
      
          setcursor(cursor:wait)
          save_maf_id = access:manfault.savefile()
          access:manfault.clearkey(maf:field_number_key)
          maf:manufacturer = job_ali:manufacturer
          set(maf:field_number_key,maf:field_number_key)
          loop
              if access:manfault.next()
                 break
              end !if
              if maf:manufacturer <> job_ali:manufacturer      |
                  then break.  ! end if
              Case maf:field_number
                  Of 1
                      Unhide(?job_ali:fault_code1:prompt)
                      ?job_ali:fault_code1:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code1:2)
                          ?job_ali:fault_code1:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code1)
                      End
                  Of 2
                      Unhide(?job_ali:fault_code2:prompt)
                      ?job_ali:fault_code2:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code2:2)
                          ?job_ali:fault_code2:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code2)
                      End
      
                  Of 3
                      Unhide(?job_ali:fault_code3:prompt)
                      ?job_ali:fault_code3:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code3:2)
                          ?job_ali:fault_code3:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code3)
                      End
      
                  Of 4
                      Unhide(?job_ali:fault_code4:prompt)
                      ?job_ali:fault_code4:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code4:2)
                          ?job_ali:fault_code4:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code4)
                      End
      
                  Of 5
                      Unhide(?job_ali:fault_code5:prompt)
                      ?job_ali:fault_code5:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code5:2)
                          ?job_ali:fault_code5:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code5)
                      End
      
                  Of 6
                      Unhide(?job_ali:fault_code6:prompt)
                      ?job_ali:fault_code6:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code6:2)
                          ?job_ali:fault_code6:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code6)
                      End
      
                  Of 7
                      Unhide(?job_ali:fault_code7:prompt)
                      ?job_ali:fault_code7:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code7:2)
                          ?job_ali:fault_code7:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code7)
                      End
      
                  Of 8
                      Unhide(?job_ali:fault_code8:prompt)
                      ?job_ali:fault_code8:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code8:2)
                          ?job_ali:fault_code8:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code8)
                      End
      
                  Of 9
                      Unhide(?job_ali:fault_code9:prompt)
                      ?job_ali:fault_code9:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code9:2)
                          ?job_ali:fault_code9:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code9)
                      End
      
                  Of 10
                      Unhide(?job_ali:fault_code10:prompt)
                      ?job_ali:fault_code10:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code10:2)
                          ?job_ali:fault_code10:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code10)
                      End
      
                  Of 11
                      Unhide(?job_ali:fault_code11:prompt)
                      ?job_ali:fault_code11:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code11:2)
                          ?job_ali:fault_code11:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code11)
                      End
      
                  Of 12
                      Unhide(?job_ali:fault_code12:prompt)
                      ?job_ali:fault_code12:prompt{prop:text} = maf:field_name
                      If maf:field_type = 'DATE'
                          Unhide(?job_ali:fault_code12:2)
                          ?job_ali:fault_code12:2{prop:xpos} = 324
                      Else
                          Unhide(?job_ali:fault_code12)
                      End
              End
          end !loop
          access:manfault.restorefile(save_maf_id)
          setcursor()
      End!If access:jobs.fetch(job_ali:ref_number_key) = Level:Benign
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

