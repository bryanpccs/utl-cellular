

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJ01026.INC'),ONCE        !Local module procedure declarations
                     END


ChangePickingPart    PROCEDURE  (jobtype)             ! Declare Procedure
proceed              BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ChangePickingPart')      !Add Procedure to Log
  end


    !*** Picking Note Code *** TH
    access:USERS.clearkey(use:password_key)
    use:Password = glo:Password
    access:USERS.fetch(use:password_key)

    if access:USERS.fetch(use:password_key) then !failure - problem with user login
    end

    tempvar = 0
    proceed = false

    access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
    pnt:JobReference = job:ref_number
    set(pnt:keyonjobno,pnt:keyonjobno)
    loop
        if access:picknote.next() then break. !no records
        if pnt:JobReference <> job:ref_number then break.
        !if pnt:IsPrinted then cycle. need all totals I think
        if ~pnt:IsPrinted then tempkey# = pnt:PickNoteRef.
        !otherwise a record
        access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
        pdt:PickNoteRef = pnt:PickNoteRef
        set(pdt:keyonpicknote,pdt:keyonpicknote)
        loop
            if access:pickdet.next() then break. !no records
            if pdt:PickNoteRef <> pnt:PickNoteRef then break.
            case jobtype
            of 'chargeable' !then !and (~pdt:IsDelete) then !its a chargeable part not deleted
                if pdt:PartRefNumber = par:Record_Number then !part ref number match - total up
                !if pdt:PartRefNumber = par:Part_Ref_Number then !part ref number match - total up
                    tempvar = tempvar + pdt:Quantity
                    if ~pnt:IsPrinted then proceed = true.
                end
            of 'warranty' !else !its got to be a warranty part
                if pdt:PartRefNumber = wpr:Record_Number then !part ref number match - total up
                !if pdt:PartRefNumber = wpr:Part_Ref_Number then !part ref number match - total up
                    tempvar = tempvar + pdt:Quantity
                    if ~pnt:IsPrinted then proceed = true.
                end
            end !case
        end !loop through pickdets
    end !loop PICKNOTE

    if proceed then
        !create new pickdet record to adjust new amount
        access:PICKNOTE.clearkey(pnt:keypicknote) !check for existing picking record
        pnt:PickNoteRef = tempkey#
        access:PICKNOTE.fetch(pnt:keypicknote)

        access:PICKDET.primerecord()
        pdt:PickNoteRef = pnt:PickNoteRef
        case jobtype
        of 'chargeable'
            pdt:IsChargeable = true
            pdt:PartRefNumber = par:Record_Number
            pdt:Quantity = par:Quantity - tempvar
            pdt:StockPartRefNumber = par:Part_Ref_Number
            pdt:PartNumber = par:Part_Number
        of 'warranty'
            pdt:IsChargeable = false
            pdt:PartRefNumber = wpr:Record_Number
            pdt:Quantity = wpr:Quantity - tempvar
            pdt:StockPartRefNumber = wpr:Part_Ref_Number
            pdt:PartNumber = wpr:Part_Number
        end !case
        pdt:RequestDate = TODAY()
        pdt:RequestTime = CLOCK()
        pdt:EngineerCode = job:engineer !needed because a different engineer may take over adding parts on the PICKNOTE - gives an audit trail
        pdt:Usercode = use:User_Code
        !pdt:IsInStock =
        !IsPrinted - not needed in related PICKNOTE may be needed for audit purposes.
        pdt:IsDelete = false
        pdt:IsInStock = true
        !pdt:Quantity = tmp_Quantity
        access:PICKDET.update()
    else !there is no existing record
        Access:LOCATION.ClearKey(loc:Location_Key)
        loc:Location = sto:Location
        If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign then
            if loc:picknoteenable then !pick notes are enabled for this location
!                access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
!                pnt:JobReference = job:ref_number
!                set(pnt:keyonjobno,pnt:keyonjobno)
!                loop
!                    if access:picknote.next() then break. !no records
!                    if pnt:JobReference <> job:ref_number then break.
!                    !otherwise a record
!                    access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
!                    pdt:PickNoteRef = pnt:PickNoteRef
!                    set(pdt:keyonpicknote,pdt:keyonpicknote)
!                    loop
!                        if access:pickdet.next() then break. !no records
!                        if pdt:PickNoteRef <> pnt:PickNoteRef then break.
!                        if pdt:IsChargeable then !and (~pdt:IsDelete) then !its a chargeable part not deleted
!                            if pdt:PartRefNumber = par:Record_Number then !part ref number match - total up
!                            !if pdt:PartRefNumber = par:Part_Ref_Number then !part ref number match - total up
!                                tempvar = tempvar + pdt:Quantity
!                            end
!                        else !its got to be a warranty part
!                            if pdt:PartRefNumber = wpr:Record_Number then !part ref number match - total up
!                            !if pdt:PartRefNumber = wpr:Part_Ref_Number then !part ref number match - total up
!                                tempvar = tempvar + pdt:Quantity
!                            end
!                        end !case
!                    end !loop through pickdets
!                end !loop PICKNOTE
!                message(pnt:PickNoteRef)
                CreatePickingNote(sto:location)
!                message(pnt:PickNoteRef)
                access:PICKDET.primerecord()
                pdt:PickNoteRef = pnt:PickNoteRef
                pdt:RequestDate = TODAY()
                pdt:RequestTime = CLOCK()
                pdt:EngineerCode = job:engineer !needed because a different engineer may take over adding parts on the PICKNOTE - gives an audit trail
                pdt:Usercode = use:User_Code
                case jobtype
                of 'chargeable'
                    pdt:isChargeable = true
                    pdt:PartRefNumber = par:Record_Number
                    !pdt:PartRefNumber = par:Part_Ref_Number
                    pdt:Quantity = par:Quantity - tempvar
                    pdt:IsInStock = true
                    pdt:StockPartRefNumber = par:Part_Ref_Number
                    pdt:PartNumber = par:Part_Number
                    access:PICKDET.update()
                of 'warranty'
                    pdt:isChargeable = false
                    pdt:PartRefNumber = wpr:Record_Number
                    !pdt:PartRefNumber = wpr:Part_Ref_number
                    pdt:Quantity = wpr:Quantity - tempvar
                    pdt:IsInStock = true
                    pdt:StockPartRefNumber = wpr:Part_Ref_Number
                    pdt:PartNumber = wpr:Part_Number
                    access:PICKDET.update()
                end !case
            end
        end
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ChangePickingPart',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('proceed',proceed,'ChangePickingPart',1)


