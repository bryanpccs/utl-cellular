

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01021.INC'),ONCE        !Local module procedure declarations
                     END


SelectLoanExchangeUnit PROCEDURE (func:Type)          !Generated from procedure template - Window

FilesOpened          BYTE
esn_temp             STRING(16)
tmp:RefNumber        LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Search Loan/Exchange Unit'),AT(,,280,72),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,272,36),USE(?Sheet1),SPREAD
                         TAB('Search Loan/Exchange Unit'),USE(?Tab1)
                           PROMPT('I.M.E.I. Number'),AT(8,20),USE(?esn_temp:Prompt),TRN
                           ENTRY(@s16),AT(84,20,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('&Full Search'),AT(212,20,60,12),USE(?Full_Search),SKIP,LEFT,ICON('Spysm.gif')
                         END
                       END
                       PANEL,AT(4,44,272,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(160,48,56,16),USE(?Ok),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(216,48,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:RefNumber)

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?esn_temp:Prompt{prop:FontColor} = -1
    ?esn_temp:Prompt{prop:Color} = 15066597
    If ?esn_temp{prop:ReadOnly} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 15066597
    Elsif ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 8454143
    Else ! If ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 16777215
    End ! If ?esn_temp{prop:Req} = True
    ?esn_temp{prop:Trn} = 0
    ?esn_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SelectLoanExchangeUnit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'SelectLoanExchangeUnit',1)
    SolaceViewVars('esn_temp',esn_temp,'SelectLoanExchangeUnit',1)
    SolaceViewVars('tmp:RefNumber',tmp:RefNumber,'SelectLoanExchangeUnit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp:Prompt;  SolaceCtrlName = '?esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp;  SolaceCtrlName = '?esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Full_Search;  SolaceCtrlName = '?Full_Search';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ok;  SolaceCtrlName = '?Ok';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('SelectLoanExchangeUnit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'SelectLoanExchangeUnit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?esn_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:EXCHANGE.Open
  Relate:JOBS.Open
  Access:LOAN.UseFile
  Access:STOCK.UseFile
  Access:STOCKTYP.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:EXCHANGE.Close
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'SelectLoanExchangeUnit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Full_Search
      ThisWindow.Update
      tmp:RefNumber = ''
      ESN_Temp    = ''
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = job:Account_Number
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              Case func:Type
              Of 'EXC'
                  SaveRequest#      = GlobalRequest
                  GlobalResponse    = RequestCancelled
                  GlobalRequest     = SelectRecord
                  Browse_Exchange(tra:exchange_stock_type)
                  If Globalresponse = RequestCompleted
                      ESN_Temp    = xch:ESN
                      tmp:RefNumber   = xch:Ref_Number
                      Post(Event:Accepted,?OK)
                  Else
                      esn_temp    = ''
                      tmp:RefNumber  = ''
                      Select(?ESN_Temp)
                  End
                  GlobalRequest     = SaveRequest#
      
              Of 'LOA'
                  SaveRequest#      = GlobalRequest
                  GlobalResponse    = RequestCancelled
                  GlobalRequest     = SelectRecord
                  Browse_Loan(tra:Loan_Stock_Type)
                  If Globalresponse = RequestCompleted
                      ESN_Temp    = loa:ESN
                      tmp:RefNumber   = loa:Ref_Number
                      Post(Event:Accepted,?OK)
                  Else
                      esn_temp    = ''
                      tmp:RefNumber  = ''
                      Select(?ESN_Temp)
                  End
                  GlobalRequest     = SaveRequest#
               END
          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          
      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
    OF ?Ok
      ThisWindow.Update
    close# = 0
    error# = 0
    Case func:Type
        Of 'LOA'
            If tmp:RefNumber = '' And ESN_Temp <> ''
                Access:LOAN.Clearkey(loa:ESN_Only_Key)
                loa:ESN = ESN_Temp
                If Access:LOAN.Tryfetch(loa:ESN_Only_Key) = Level:Benign
                    !Found
                    tmp:RefNumber = loa:Ref_Number
                Else ! If Access:LOAN.Tryfetch(loa:ESN_Only_Key) = Level:Benign
                    !Error
                    Error# = 1
                End !If Access:LOAN.Tryfetch(loa:ESN_Only_Key) = Level:Benign
            End !If tmp:RefNumber = '' And ESN_Temp <> ''
            If tmp:RefNumber <> ''
                Access:LOAN.Clearkey(loa:Ref_Number_Key)
                loa:Ref_Number  = tmp:RefNumber
                If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                    !Found

                Else ! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                    !Error
                    Error# = 1
                End !If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            End !If tmp:RefNumber <> ''
            If Error# = 0
                access:loan.clearkey(loa:esn_only_key)
                loa:esn = esn_temp
                if access:loan.fetch(loa:esn_only_key) = Level:Benign
                    IF loa:Available = 'AVL'
                        access:stocktyp.clearkey(stp:stock_type_key)
                        stp:stock_type = loa:stock_type
                        if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                           If stp:available <> 1
                             Case MessageEx('The selected Stock Type is not available.','ServiceBase 2000',|
                                            'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                 Of 1 ! &OK Button
                             End!Case MessageEx
                               error# = 1
                           End!If stp:available <> 1
                        End!if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                    ELSE
                        Case MessageEx('The selected Loan Unit is not available.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        error# = 1
                    END
                Else!if access:exchange.fetch(xch:esn_only_key)
                   error# = 1
                end!if access:exchange.fetch(xch:esn_only_key)
            End !If Error# = 0

        Of 'EXC'
            !If there is no ref_number, then the full search button was not pressed
            !and the user hasjust typed/scanned in a imei number.
            If tmp:RefNumber = '' And ESN_temp <> ''
                Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                xch:ESN = ESN_Temp
                If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                    !Found
                    tmp:RefNumber   = xch:Ref_Number
                Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Error# = 1
                End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
            End !If tmp:RefNumber = '' And ESN_temp <> ''
            !If full search is selected, then should have a ref_number
            If tmp:RefNumber <> ''
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = tmp:RefNumber
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Found
                Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Error# = 1
                End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            End !If tmp:RefNumber <> ''
            If Error# = 0
                !Found
                IF xch:Available = 'AVL'
                    access:stocktyp.clearkey(stp:stock_type_key)
                    stp:stock_type = xch:stock_type
                    if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                        If stp:available <> 1
                              Case MessageEx('The selected Stock Type is not available.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              error# = 1
                        ELSE
                            close# = 1
                        End!If stp:available <> 1
                    End!if access:stocktyp.tryfetch(stp:stock_type_key) = Level:Benign
                ELSE
                  Case MessageEx('The selected Exchange Unit is not available.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                   error# = 1
                END
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End!Case f_type
    If error# = 1
        esn_temp = ''
        tmp:RefNumber = ''
        Select(?esn_temp)
        Display()
    Else
        Post(event:closewindow)
    End!If close# = 1
    OF ?CancelButton
      ThisWindow.Update
      tmp:RefNumber = ''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'SelectLoanExchangeUnit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      Case func:Type
          Of 'EXC'
              0{prop:text} = 'Select Exchange Unit'
              ?Tab1{prop:text} = 'Insert Exchange I.M.E.I. Number'
          Of 'LOA'
              0{prop:text} = 'Select Loan Unit'
              ?Tab1{prop:text} = 'Insert Loan I.M.E.I. Number'
      End!Case f_type
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

