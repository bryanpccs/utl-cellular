

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJ01028.INC'),ONCE        !Local module procedure declarations
                     END


TransferParts        PROCEDURE  (jobtype)             ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'TransferParts')      !Add Procedure to Log
  end


    !*** Picking Note Code *** TH
    access:USERS.clearkey(use:password_key)
    use:Password = glo:Password
    access:USERS.fetch(use:password_key)

    if access:USERS.fetch(use:password_key) then !failure - problem with user login
    end

    access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
    pnt:JobReference = job:ref_number
    set(pnt:keyonjobno,pnt:keyonjobno)
    loop
        if access:picknote.next() then break. !no records
        if pnt:JobReference <> job:Ref_Number then break. !no matching records
        if ~pnt:IsPrinted then !matching record - is it unprinted if so we can we append to it
            !edit the existing record
            if jobtype = 'chargeable' then
                par:Quantity = 0
                ChangePickingPart('chargeable')
                DeletePickingParts('chargeable',1)
                InsertPickingPart('warranty')
                break
            end
            if jobtype = 'warranty'
                wpr:Quantity = 0
                ChangePickingPart('warranty')
                DeletePickingParts('warranty',1)
                InsertPickingPart('chargeable')
                break
            end
        end
    end !loop


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'TransferParts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


