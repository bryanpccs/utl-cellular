

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01048.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Part_Delete_Reason PROCEDURE                   !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
select_temp          STRING(3)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(NOTESDEL)
                       PROJECT(nod:ReasonCode)
                       PROJECT(nod:ReasonText)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
nod:ReasonCode         LIKE(nod:ReasonCode)           !List box control field - type derived from field
nod:ReasonText         LIKE(nod:ReasonText)           !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Standard Delete Part Text'),AT(,,523,228),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Fault_Description_Text'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,268,220),USE(?CurrentTab),SPREAD
                         TAB('By Notes'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,16,72,10),USE(nod:ReasonCode)
                           LIST,AT(8,32,260,164),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M~Reason Code~L(2)@s30@80L(2)|M~Reason Text~L(2)@s80@'),FROM(Queue:Browse:1),DRAGID('FromList')
                           BUTTON('&Add Selected Text To List'),AT(192,200,76,16),USE(?add),LEFT,ICON('select2.ico')
                           BUTTON('&Insert'),AT(8,200,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(68,200,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(128,200,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       SHEET,AT(276,4,160,220),USE(?Sheet2),SPREAD
                         TAB('Text To Appear On Job'),USE(?Tab2)
                           LIST,AT(280,16,152,180),USE(?List2),VSCROLL,FORMAT('320L(2)~Notes~@s80@'),FROM(glo:Q_Notes),DROPID('FromList')
                           BUTTON('&Remove Text From List'),AT(280,200,76,16),USE(?remove_text),LEFT,ICON('select.ico')
                         END
                       END
                       BUTTON('&Select'),AT(444,4,76,20),USE(?Select_Button),LEFT,ICON('select.ico')
                       BUTTON('Close'),AT(444,204,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?nod:ReasonCode{prop:ReadOnly} = True
        ?nod:ReasonCode{prop:FontColor} = 65793
        ?nod:ReasonCode{prop:Color} = 15066597
    Elsif ?nod:ReasonCode{prop:Req} = True
        ?nod:ReasonCode{prop:FontColor} = 65793
        ?nod:ReasonCode{prop:Color} = 8454143
    Else ! If ?nod:ReasonCode{prop:Req} = True
        ?nod:ReasonCode{prop:FontColor} = 65793
        ?nod:ReasonCode{prop:Color} = 16777215
    End ! If ?nod:ReasonCode{prop:Req} = True
    ?nod:ReasonCode{prop:Trn} = 0
    ?nod:ReasonCode{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List2{prop:FontColor} = 65793
    ?List2{prop:Color}= 16777215
    ?List2{prop:Color,2} = 16777215
    ?List2{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Part_Delete_Reason',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Part_Delete_Reason',1)
    SolaceViewVars('select_temp',select_temp,'Browse_Part_Delete_Reason',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?nod:ReasonCode;  SolaceCtrlName = '?nod:ReasonCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?add;  SolaceCtrlName = '?add';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List2;  SolaceCtrlName = '?List2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?remove_text;  SolaceCtrlName = '?remove_text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Button;  SolaceCtrlName = '?Select_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Part_Delete_Reason')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Part_Delete_Reason')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?nod:ReasonCode
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Part_Delete_Reason'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:NOTESDEL.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:NOTESDEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Clear(glo:Q_Notes)
  Free(glo:Q_Notes)
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,nod:keyonnotesdel)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?nod:ReasonCode,nod:ReasonCode,1,BRW1)
  BRW1.AddField(nod:ReasonCode,BRW1.Q.nod:ReasonCode)
  BRW1.AddField(nod:ReasonText,BRW1.Q.nod:ReasonText)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:NOTESDEL.Close
  If select_Temp <> 'Y'
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
  End
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Part_Delete_Reason'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Part_Delete_Reason',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
  !  case request
  !      of insertrecord
  !          check_access('PARTS DELETE REASONS - INSERT',x")
  !          if x" = false
  !              case messageex('You do not have access to this option.','ServiceBase 2000',|
  !                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                  of 1 ! &ok button
  !              end!case messageex
  !              do_update# = false
  !          end
  !      of changerecord
  !          check_access('PARTS DELETE REASONS - CHANGE',x")
  !          if x" = false
  !              case messageex('You do not have access to this option.','ServiceBase 2000',|
  !                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                  of 1 ! &ok button
  !              end!case messageex
  !              do_update# = false
  !          end
  !      of deleterecord
  !          check_access('PARTS DELETE REASONS - DELETE',x")
  !          if x" = false
  !              case messageex('You do not have access to this option.','ServiceBase 2000',|
  !                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                  of 1 ! &ok button
  !              end!case messageex
  !              do_update# = false
  !          end
  !  end !case request
  
    if do_update# = true
  end!if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdatePartsDeleteReasons
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?nod:ReasonCode
      BRW1.ResetSort(1)
      Select(?Browse:1)
    OF ?add
      ThisWindow.Update
      glo:notes_pointer = nod:ReasonText
      Add(glo:Q_Notes)
      Select(?Browse:1)
      
    OF ?remove_text
      ThisWindow.Update
      glo:notes_pointer = nod:ReasonText
      Get(glo:Q_Notes,glo:notes_pointer)
      Delete(glo:Q_Notes)
      Select(?Browse:1)
    OF ?Select_Button
      ThisWindow.Update
      select_temp = 'Y'
       POST(Event:CloseWindow)
    OF ?Close
      ThisWindow.Update
      nod:ReasonText =''
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Part_Delete_Reason')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List2
    CASE EVENT()
    OF EVENT:Drop
      if dropid()
          glo:notes_pointer = nof:notes
          Add(glo:Q_Notes)
          Select(?Browse:1)
      end!if dropid()
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      If glo:select1 <> ''
          Select(?Browse:1)
      End!If glo:select1 <> ''
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

