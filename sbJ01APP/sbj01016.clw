

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01016.INC'),ONCE        !Local module procedure declarations
                     END


Update_Parts PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
Result               BYTE
Msg1                 STRING(256)
Msg2                 STRING(256)
TotalPartsCost       REAL
tmpCheckChargeableParts BYTE(0)
fault_codes_required_temp STRING('NO {1}')
pos                  STRING(255)
Part_Details_Group   GROUP,PRE(TMP)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
                     END
adjustment_temp      STRING(3)
quantity_temp        LONG
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
tmp:location         STRING(30)
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
sav:ExcludeFromOrder STRING(3)
sav:Supplier         STRING(30)
tmp:CreateNewOrder   BYTE(0)
tmp:StockNumber      LONG
tmp:NewQuantity      LONG
tmp:PendingPart      LONG
tmp:CurrentState     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp_RF_Board_IMEI    STRING(20)
History::par:Record  LIKE(par:RECORD),STATIC
QuickWindow          WINDOW('Chargeable Part'),AT(,,291,264),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Warranty_Part'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,284,228),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Chargeable Part'),AT(8,20),USE(?Title),FONT('Tahoma',10,COLOR:Navy,FONT:bold)
                           PROMPT('Part Number'),AT(8,36),USE(?par:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(par:Part_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           BUTTON('Browse Stock'),AT(212,36,68,10),USE(?browse_stock_button),SKIP,LEFT,ICON('stockvsm.gif')
                           PROMPT('Description'),AT(8,48),USE(?par:Description:Prompt),TRN
                           ENTRY(@s30),AT(84,48,124,10),USE(par:Description),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Location'),AT(8,64),USE(?tmp:location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,64,124,10),USE(tmp:location),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Shelf/Second Location'),AT(8,76),USE(?tmp:location:Prompt:2),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,76,60,10),USE(tmp:ShelfLocation),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           ENTRY(@s30),AT(148,76,60,10),USE(tmp:SecondLocation),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           GROUP,AT(4,92,248,136),USE(?AdjustmentGroup)
                             PROMPT('Purchase Cost'),AT(8,92),USE(?par:Purchase_Cost:Prompt),TRN
                             ENTRY(@n14.2),AT(84,92,64,10),USE(par:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Sale Cost'),AT(8,104),USE(?par:Sale_Cost:Prompt),TRN
                             ENTRY(@n14.2),AT(84,104,64,10),USE(par:Sale_Cost),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('Supplier'),AT(8,116),USE(?par:Supplier:Prompt),TRN
                             ENTRY(@s30),AT(84,116,124,10),USE(par:Supplier),FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                             BUTTON,AT(212,116,10,10),USE(?LookupSupplier),SKIP,ICON('List3.ico')
                             PROMPT('Quantity'),AT(8,128),USE(?Prompt7),TRN
                             SPIN(@p<<<<<<<#p),AT(84,128,64,10),USE(par:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold),REQ,RANGE(1,99999999),STEP(1)
                             PROMPT('Despatch Note No'),AT(8,140),USE(?par:Despatch_Note_Number:Prompt),TRN
                             ENTRY(@s30),AT(84,140,124,10),USE(par:Despatch_Note_Number),FONT('Tahoma',8,,FONT:bold),UPR
                             STRING('RF Board IMEI'),AT(8,152),USE(?RF_BOARD_PROMPT),TRN,HIDE
                             ENTRY(@s20),AT(84,152,124,10),USE(tmp_RF_Board_IMEI),HIDE,REQ
                             CHECK('Exclude From Ordering / Decrementing'),AT(84,164,144,8),USE(par:Exclude_From_Order),TRN,VALUE('YES','NO')
                             PROMPT('Part Status'),AT(8,180),USE(?Prompt24),FONT(,10,COLOR:Navy,FONT:bold)
                             PROMPT('Date Ordered'),AT(8,192),USE(?par:Date_Ordered:Prompt),TRN
                             ENTRY(@d6b),AT(84,192,64,10),USE(par:Date_Ordered),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                             PROMPT('Order Number'),AT(8,204),USE(?par:Order_Number:Prompt)
                             ENTRY(@n012b),AT(84,204,64,10),USE(par:Order_Number),SKIP,FONT(,,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),READONLY,MSG('Order Number')
                             PROMPT('Date Received'),AT(8,216),USE(?PAR:Date_Received:Prompt),TRN
                             ENTRY(@d6b),AT(84,216,64,10),USE(par:Date_Received),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           END
                         END
                         TAB('Fault Coding'),USE(?Fault_Code_Tab),HIDE
                           CHECK('Fault Codes Checked'),AT(104,21),USE(par:Fault_Codes_Checked),VALUE('YES','NO')
                           PROMPT('Fault Code 1:'),AT(8,36),USE(?par:Fault_Code1:Prompt),HIDE
                           ENTRY(@s30),AT(84,36,124,10),USE(par:Fault_Code1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,36,124,10),USE(par:Fault_Code1,,?par:Fault_Code1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,36,10,10),USE(?Button4),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,36,10,10),USE(?PopCalendar),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 2:'),AT(8,52),USE(?par:Fault_Code2:Prompt),HIDE
                           ENTRY(@s30),AT(84,52,124,10),USE(par:Fault_Code2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,52,124,10),USE(par:Fault_Code2,,?par:Fault_Code2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,52,10,10),USE(?Button4:2),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,52,10,10),USE(?PopCalendar:2),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 3:'),AT(8,68),USE(?par:Fault_Code3:Prompt),HIDE
                           ENTRY(@s30),AT(84,68,124,10),USE(par:Fault_Code3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,68,124,10),USE(par:Fault_Code3,,?par:Fault_Code3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,68,10,10),USE(?Button4:3),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,68,10,10),USE(?PopCalendar:3),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 4:'),AT(8,84),USE(?par:Fault_Code4:Prompt),HIDE
                           ENTRY(@s30),AT(84,84,124,10),USE(par:Fault_Code4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,84,10,10),USE(?Button4:4),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,84,10,10),USE(?PopCalendar:4),HIDE,ICON('Calenda2.ico')
                           ENTRY(@d6b),AT(52,84,124,10),USE(par:Fault_Code4,,?par:Fault_Code4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fault Code 5:'),AT(8,100),USE(?par:Fault_Code5:Prompt),HIDE
                           ENTRY(@s30),AT(84,100,124,10),USE(par:Fault_Code5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,100,124,10),USE(par:Fault_Code5,,?par:Fault_Code5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,100,10,10),USE(?Button4:5),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,100,10,10),USE(?PopCalendar:5),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 6:'),AT(8,116),USE(?par:Fault_Code6:Prompt),HIDE
                           ENTRY(@s30),AT(84,116,124,10),USE(par:Fault_Code6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,116,124,10),USE(par:Fault_Code6,,?par:Fault_Code6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,116,10,10),USE(?Button4:6),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,116,10,10),USE(?PopCalendar:6),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 7:'),AT(8,132),USE(?par:Fault_Code7:Prompt),HIDE
                           ENTRY(@s30),AT(84,132,124,10),USE(par:Fault_Code7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,132,124,10),USE(par:Fault_Code7,,?par:Fault_Code7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,132,10,10),USE(?Button4:7),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,132,10,10),USE(?PopCalendar:7),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 8:'),AT(8,148),USE(?par:Fault_Code8:Prompt),HIDE
                           ENTRY(@s30),AT(84,148,124,10),USE(par:Fault_Code8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,148,124,10),USE(par:Fault_Code8,,?par:Fault_Code8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,148,10,10),USE(?Button4:8),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,148,10,10),USE(?PopCalendar:8),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 9:'),AT(8,164),USE(?par:Fault_Code9:Prompt),HIDE
                           ENTRY(@s30),AT(84,164,124,10),USE(par:Fault_Code9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,164,124,10),USE(par:Fault_Code9,,?par:Fault_Code9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,164,10,10),USE(?Button4:9),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,164,10,10),USE(?PopCalendar:9),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 10:'),AT(8,180),USE(?par:Fault_Code10:Prompt),HIDE
                           ENTRY(@s30),AT(84,180,124,10),USE(par:Fault_Code10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,180,124,10),USE(par:Fault_Code10,,?par:Fault_Code10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,180,10,10),USE(?PopCalendar:10),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,180,10,10),USE(?Button4:10),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 11:'),AT(8,196),USE(?par:Fault_Code11:Prompt),HIDE
                           ENTRY(@s30),AT(84,196,124,10),USE(par:Fault_Code11),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,196,124,10),USE(par:Fault_Code11,,?par:Fault_Code11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(212,196,10,10),USE(?Button4:11),HIDE,ICON('list3.ico')
                           BUTTON,AT(228,196,10,10),USE(?PopCalendar:11),HIDE,ICON('Calenda2.ico')
                           PROMPT('Fault Code 12:'),AT(8,212),USE(?par:Fault_Code12:Prompt),HIDE
                           ENTRY(@s30),AT(84,212,124,10),USE(par:Fault_Code12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@d6b),AT(52,212,124,10),USE(par:Fault_Code12,,?par:Fault_Code12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(228,212,10,10),USE(?PopCalendar:12),HIDE,ICON('Calenda2.ico')
                           BUTTON,AT(212,212,10,10),USE(?Button4:12),HIDE,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,236,284,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(172,240,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(228,240,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

save_map_id   ushort,auto
save_par_ali_id     ushort,auto

!Save Entry Fields Incase Of Lookup
look:par:Supplier                Like(par:Supplier)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?Title{prop:FontColor} = -1
    ?Title{prop:Color} = 15066597
    ?par:Part_Number:Prompt{prop:FontColor} = -1
    ?par:Part_Number:Prompt{prop:Color} = 15066597
    If ?par:Part_Number{prop:ReadOnly} = True
        ?par:Part_Number{prop:FontColor} = 65793
        ?par:Part_Number{prop:Color} = 15066597
    Elsif ?par:Part_Number{prop:Req} = True
        ?par:Part_Number{prop:FontColor} = 65793
        ?par:Part_Number{prop:Color} = 8454143
    Else ! If ?par:Part_Number{prop:Req} = True
        ?par:Part_Number{prop:FontColor} = 65793
        ?par:Part_Number{prop:Color} = 16777215
    End ! If ?par:Part_Number{prop:Req} = True
    ?par:Part_Number{prop:Trn} = 0
    ?par:Part_Number{prop:FontStyle} = font:Bold
    ?par:Description:Prompt{prop:FontColor} = -1
    ?par:Description:Prompt{prop:Color} = 15066597
    If ?par:Description{prop:ReadOnly} = True
        ?par:Description{prop:FontColor} = 65793
        ?par:Description{prop:Color} = 15066597
    Elsif ?par:Description{prop:Req} = True
        ?par:Description{prop:FontColor} = 65793
        ?par:Description{prop:Color} = 8454143
    Else ! If ?par:Description{prop:Req} = True
        ?par:Description{prop:FontColor} = 65793
        ?par:Description{prop:Color} = 16777215
    End ! If ?par:Description{prop:Req} = True
    ?par:Description{prop:Trn} = 0
    ?par:Description{prop:FontStyle} = font:Bold
    ?tmp:location:Prompt{prop:FontColor} = -1
    ?tmp:location:Prompt{prop:Color} = 15066597
    If ?tmp:location{prop:ReadOnly} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 15066597
    Elsif ?tmp:location{prop:Req} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 8454143
    Else ! If ?tmp:location{prop:Req} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 16777215
    End ! If ?tmp:location{prop:Req} = True
    ?tmp:location{prop:Trn} = 0
    ?tmp:location{prop:FontStyle} = font:Bold
    ?tmp:location:Prompt:2{prop:FontColor} = -1
    ?tmp:location:Prompt:2{prop:Color} = 15066597
    If ?tmp:ShelfLocation{prop:ReadOnly} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 15066597
    Elsif ?tmp:ShelfLocation{prop:Req} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 8454143
    Else ! If ?tmp:ShelfLocation{prop:Req} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 16777215
    End ! If ?tmp:ShelfLocation{prop:Req} = True
    ?tmp:ShelfLocation{prop:Trn} = 0
    ?tmp:ShelfLocation{prop:FontStyle} = font:Bold
    If ?tmp:SecondLocation{prop:ReadOnly} = True
        ?tmp:SecondLocation{prop:FontColor} = 65793
        ?tmp:SecondLocation{prop:Color} = 15066597
    Elsif ?tmp:SecondLocation{prop:Req} = True
        ?tmp:SecondLocation{prop:FontColor} = 65793
        ?tmp:SecondLocation{prop:Color} = 8454143
    Else ! If ?tmp:SecondLocation{prop:Req} = True
        ?tmp:SecondLocation{prop:FontColor} = 65793
        ?tmp:SecondLocation{prop:Color} = 16777215
    End ! If ?tmp:SecondLocation{prop:Req} = True
    ?tmp:SecondLocation{prop:Trn} = 0
    ?tmp:SecondLocation{prop:FontStyle} = font:Bold
    ?AdjustmentGroup{prop:Font,3} = -1
    ?AdjustmentGroup{prop:Color} = 15066597
    ?AdjustmentGroup{prop:Trn} = 0
    ?par:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?par:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?par:Purchase_Cost{prop:ReadOnly} = True
        ?par:Purchase_Cost{prop:FontColor} = 65793
        ?par:Purchase_Cost{prop:Color} = 15066597
    Elsif ?par:Purchase_Cost{prop:Req} = True
        ?par:Purchase_Cost{prop:FontColor} = 65793
        ?par:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?par:Purchase_Cost{prop:Req} = True
        ?par:Purchase_Cost{prop:FontColor} = 65793
        ?par:Purchase_Cost{prop:Color} = 16777215
    End ! If ?par:Purchase_Cost{prop:Req} = True
    ?par:Purchase_Cost{prop:Trn} = 0
    ?par:Purchase_Cost{prop:FontStyle} = font:Bold
    ?par:Sale_Cost:Prompt{prop:FontColor} = -1
    ?par:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?par:Sale_Cost{prop:ReadOnly} = True
        ?par:Sale_Cost{prop:FontColor} = 65793
        ?par:Sale_Cost{prop:Color} = 15066597
    Elsif ?par:Sale_Cost{prop:Req} = True
        ?par:Sale_Cost{prop:FontColor} = 65793
        ?par:Sale_Cost{prop:Color} = 8454143
    Else ! If ?par:Sale_Cost{prop:Req} = True
        ?par:Sale_Cost{prop:FontColor} = 65793
        ?par:Sale_Cost{prop:Color} = 16777215
    End ! If ?par:Sale_Cost{prop:Req} = True
    ?par:Sale_Cost{prop:Trn} = 0
    ?par:Sale_Cost{prop:FontStyle} = font:Bold
    ?par:Supplier:Prompt{prop:FontColor} = -1
    ?par:Supplier:Prompt{prop:Color} = 15066597
    If ?par:Supplier{prop:ReadOnly} = True
        ?par:Supplier{prop:FontColor} = 65793
        ?par:Supplier{prop:Color} = 15066597
    Elsif ?par:Supplier{prop:Req} = True
        ?par:Supplier{prop:FontColor} = 65793
        ?par:Supplier{prop:Color} = 8454143
    Else ! If ?par:Supplier{prop:Req} = True
        ?par:Supplier{prop:FontColor} = 65793
        ?par:Supplier{prop:Color} = 16777215
    End ! If ?par:Supplier{prop:Req} = True
    ?par:Supplier{prop:Trn} = 0
    ?par:Supplier{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?par:Quantity{prop:ReadOnly} = True
        ?par:Quantity{prop:FontColor} = 65793
        ?par:Quantity{prop:Color} = 15066597
    Elsif ?par:Quantity{prop:Req} = True
        ?par:Quantity{prop:FontColor} = 65793
        ?par:Quantity{prop:Color} = 8454143
    Else ! If ?par:Quantity{prop:Req} = True
        ?par:Quantity{prop:FontColor} = 65793
        ?par:Quantity{prop:Color} = 16777215
    End ! If ?par:Quantity{prop:Req} = True
    ?par:Quantity{prop:Trn} = 0
    ?par:Quantity{prop:FontStyle} = font:Bold
    ?par:Despatch_Note_Number:Prompt{prop:FontColor} = -1
    ?par:Despatch_Note_Number:Prompt{prop:Color} = 15066597
    If ?par:Despatch_Note_Number{prop:ReadOnly} = True
        ?par:Despatch_Note_Number{prop:FontColor} = 65793
        ?par:Despatch_Note_Number{prop:Color} = 15066597
    Elsif ?par:Despatch_Note_Number{prop:Req} = True
        ?par:Despatch_Note_Number{prop:FontColor} = 65793
        ?par:Despatch_Note_Number{prop:Color} = 8454143
    Else ! If ?par:Despatch_Note_Number{prop:Req} = True
        ?par:Despatch_Note_Number{prop:FontColor} = 65793
        ?par:Despatch_Note_Number{prop:Color} = 16777215
    End ! If ?par:Despatch_Note_Number{prop:Req} = True
    ?par:Despatch_Note_Number{prop:Trn} = 0
    ?par:Despatch_Note_Number{prop:FontStyle} = font:Bold
    ?RF_BOARD_PROMPT{prop:FontColor} = -1
    ?RF_BOARD_PROMPT{prop:Color} = 15066597
    If ?tmp_RF_Board_IMEI{prop:ReadOnly} = True
        ?tmp_RF_Board_IMEI{prop:FontColor} = 65793
        ?tmp_RF_Board_IMEI{prop:Color} = 15066597
    Elsif ?tmp_RF_Board_IMEI{prop:Req} = True
        ?tmp_RF_Board_IMEI{prop:FontColor} = 65793
        ?tmp_RF_Board_IMEI{prop:Color} = 8454143
    Else ! If ?tmp_RF_Board_IMEI{prop:Req} = True
        ?tmp_RF_Board_IMEI{prop:FontColor} = 65793
        ?tmp_RF_Board_IMEI{prop:Color} = 16777215
    End ! If ?tmp_RF_Board_IMEI{prop:Req} = True
    ?tmp_RF_Board_IMEI{prop:Trn} = 0
    ?tmp_RF_Board_IMEI{prop:FontStyle} = font:Bold
    ?par:Exclude_From_Order{prop:Font,3} = -1
    ?par:Exclude_From_Order{prop:Color} = 15066597
    ?par:Exclude_From_Order{prop:Trn} = 0
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    ?par:Date_Ordered:Prompt{prop:FontColor} = -1
    ?par:Date_Ordered:Prompt{prop:Color} = 15066597
    If ?par:Date_Ordered{prop:ReadOnly} = True
        ?par:Date_Ordered{prop:FontColor} = 65793
        ?par:Date_Ordered{prop:Color} = 15066597
    Elsif ?par:Date_Ordered{prop:Req} = True
        ?par:Date_Ordered{prop:FontColor} = 65793
        ?par:Date_Ordered{prop:Color} = 8454143
    Else ! If ?par:Date_Ordered{prop:Req} = True
        ?par:Date_Ordered{prop:FontColor} = 65793
        ?par:Date_Ordered{prop:Color} = 16777215
    End ! If ?par:Date_Ordered{prop:Req} = True
    ?par:Date_Ordered{prop:Trn} = 0
    ?par:Date_Ordered{prop:FontStyle} = font:Bold
    ?par:Order_Number:Prompt{prop:FontColor} = -1
    ?par:Order_Number:Prompt{prop:Color} = 15066597
    If ?par:Order_Number{prop:ReadOnly} = True
        ?par:Order_Number{prop:FontColor} = 65793
        ?par:Order_Number{prop:Color} = 15066597
    Elsif ?par:Order_Number{prop:Req} = True
        ?par:Order_Number{prop:FontColor} = 65793
        ?par:Order_Number{prop:Color} = 8454143
    Else ! If ?par:Order_Number{prop:Req} = True
        ?par:Order_Number{prop:FontColor} = 65793
        ?par:Order_Number{prop:Color} = 16777215
    End ! If ?par:Order_Number{prop:Req} = True
    ?par:Order_Number{prop:Trn} = 0
    ?par:Order_Number{prop:FontStyle} = font:Bold
    ?PAR:Date_Received:Prompt{prop:FontColor} = -1
    ?PAR:Date_Received:Prompt{prop:Color} = 15066597
    If ?par:Date_Received{prop:ReadOnly} = True
        ?par:Date_Received{prop:FontColor} = 65793
        ?par:Date_Received{prop:Color} = 15066597
    Elsif ?par:Date_Received{prop:Req} = True
        ?par:Date_Received{prop:FontColor} = 65793
        ?par:Date_Received{prop:Color} = 8454143
    Else ! If ?par:Date_Received{prop:Req} = True
        ?par:Date_Received{prop:FontColor} = 65793
        ?par:Date_Received{prop:Color} = 16777215
    End ! If ?par:Date_Received{prop:Req} = True
    ?par:Date_Received{prop:Trn} = 0
    ?par:Date_Received{prop:FontStyle} = font:Bold
    ?Fault_Code_Tab{prop:Color} = 15066597
    ?par:Fault_Codes_Checked{prop:Font,3} = -1
    ?par:Fault_Codes_Checked{prop:Color} = 15066597
    ?par:Fault_Codes_Checked{prop:Trn} = 0
    ?par:Fault_Code1:Prompt{prop:FontColor} = -1
    ?par:Fault_Code1:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code1{prop:ReadOnly} = True
        ?par:Fault_Code1{prop:FontColor} = 65793
        ?par:Fault_Code1{prop:Color} = 15066597
    Elsif ?par:Fault_Code1{prop:Req} = True
        ?par:Fault_Code1{prop:FontColor} = 65793
        ?par:Fault_Code1{prop:Color} = 8454143
    Else ! If ?par:Fault_Code1{prop:Req} = True
        ?par:Fault_Code1{prop:FontColor} = 65793
        ?par:Fault_Code1{prop:Color} = 16777215
    End ! If ?par:Fault_Code1{prop:Req} = True
    ?par:Fault_Code1{prop:Trn} = 0
    ?par:Fault_Code1{prop:FontStyle} = font:Bold
    If ?par:Fault_Code1:2{prop:ReadOnly} = True
        ?par:Fault_Code1:2{prop:FontColor} = 65793
        ?par:Fault_Code1:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code1:2{prop:Req} = True
        ?par:Fault_Code1:2{prop:FontColor} = 65793
        ?par:Fault_Code1:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code1:2{prop:Req} = True
        ?par:Fault_Code1:2{prop:FontColor} = 65793
        ?par:Fault_Code1:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code1:2{prop:Req} = True
    ?par:Fault_Code1:2{prop:Trn} = 0
    ?par:Fault_Code1:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code2:Prompt{prop:FontColor} = -1
    ?par:Fault_Code2:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code2{prop:ReadOnly} = True
        ?par:Fault_Code2{prop:FontColor} = 65793
        ?par:Fault_Code2{prop:Color} = 15066597
    Elsif ?par:Fault_Code2{prop:Req} = True
        ?par:Fault_Code2{prop:FontColor} = 65793
        ?par:Fault_Code2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code2{prop:Req} = True
        ?par:Fault_Code2{prop:FontColor} = 65793
        ?par:Fault_Code2{prop:Color} = 16777215
    End ! If ?par:Fault_Code2{prop:Req} = True
    ?par:Fault_Code2{prop:Trn} = 0
    ?par:Fault_Code2{prop:FontStyle} = font:Bold
    If ?par:Fault_Code2:2{prop:ReadOnly} = True
        ?par:Fault_Code2:2{prop:FontColor} = 65793
        ?par:Fault_Code2:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code2:2{prop:Req} = True
        ?par:Fault_Code2:2{prop:FontColor} = 65793
        ?par:Fault_Code2:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code2:2{prop:Req} = True
        ?par:Fault_Code2:2{prop:FontColor} = 65793
        ?par:Fault_Code2:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code2:2{prop:Req} = True
    ?par:Fault_Code2:2{prop:Trn} = 0
    ?par:Fault_Code2:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code3:Prompt{prop:FontColor} = -1
    ?par:Fault_Code3:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code3{prop:ReadOnly} = True
        ?par:Fault_Code3{prop:FontColor} = 65793
        ?par:Fault_Code3{prop:Color} = 15066597
    Elsif ?par:Fault_Code3{prop:Req} = True
        ?par:Fault_Code3{prop:FontColor} = 65793
        ?par:Fault_Code3{prop:Color} = 8454143
    Else ! If ?par:Fault_Code3{prop:Req} = True
        ?par:Fault_Code3{prop:FontColor} = 65793
        ?par:Fault_Code3{prop:Color} = 16777215
    End ! If ?par:Fault_Code3{prop:Req} = True
    ?par:Fault_Code3{prop:Trn} = 0
    ?par:Fault_Code3{prop:FontStyle} = font:Bold
    If ?par:Fault_Code3:2{prop:ReadOnly} = True
        ?par:Fault_Code3:2{prop:FontColor} = 65793
        ?par:Fault_Code3:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code3:2{prop:Req} = True
        ?par:Fault_Code3:2{prop:FontColor} = 65793
        ?par:Fault_Code3:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code3:2{prop:Req} = True
        ?par:Fault_Code3:2{prop:FontColor} = 65793
        ?par:Fault_Code3:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code3:2{prop:Req} = True
    ?par:Fault_Code3:2{prop:Trn} = 0
    ?par:Fault_Code3:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code4:Prompt{prop:FontColor} = -1
    ?par:Fault_Code4:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code4{prop:ReadOnly} = True
        ?par:Fault_Code4{prop:FontColor} = 65793
        ?par:Fault_Code4{prop:Color} = 15066597
    Elsif ?par:Fault_Code4{prop:Req} = True
        ?par:Fault_Code4{prop:FontColor} = 65793
        ?par:Fault_Code4{prop:Color} = 8454143
    Else ! If ?par:Fault_Code4{prop:Req} = True
        ?par:Fault_Code4{prop:FontColor} = 65793
        ?par:Fault_Code4{prop:Color} = 16777215
    End ! If ?par:Fault_Code4{prop:Req} = True
    ?par:Fault_Code4{prop:Trn} = 0
    ?par:Fault_Code4{prop:FontStyle} = font:Bold
    If ?par:Fault_Code4:2{prop:ReadOnly} = True
        ?par:Fault_Code4:2{prop:FontColor} = 65793
        ?par:Fault_Code4:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code4:2{prop:Req} = True
        ?par:Fault_Code4:2{prop:FontColor} = 65793
        ?par:Fault_Code4:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code4:2{prop:Req} = True
        ?par:Fault_Code4:2{prop:FontColor} = 65793
        ?par:Fault_Code4:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code4:2{prop:Req} = True
    ?par:Fault_Code4:2{prop:Trn} = 0
    ?par:Fault_Code4:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code5:Prompt{prop:FontColor} = -1
    ?par:Fault_Code5:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code5{prop:ReadOnly} = True
        ?par:Fault_Code5{prop:FontColor} = 65793
        ?par:Fault_Code5{prop:Color} = 15066597
    Elsif ?par:Fault_Code5{prop:Req} = True
        ?par:Fault_Code5{prop:FontColor} = 65793
        ?par:Fault_Code5{prop:Color} = 8454143
    Else ! If ?par:Fault_Code5{prop:Req} = True
        ?par:Fault_Code5{prop:FontColor} = 65793
        ?par:Fault_Code5{prop:Color} = 16777215
    End ! If ?par:Fault_Code5{prop:Req} = True
    ?par:Fault_Code5{prop:Trn} = 0
    ?par:Fault_Code5{prop:FontStyle} = font:Bold
    If ?par:Fault_Code5:2{prop:ReadOnly} = True
        ?par:Fault_Code5:2{prop:FontColor} = 65793
        ?par:Fault_Code5:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code5:2{prop:Req} = True
        ?par:Fault_Code5:2{prop:FontColor} = 65793
        ?par:Fault_Code5:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code5:2{prop:Req} = True
        ?par:Fault_Code5:2{prop:FontColor} = 65793
        ?par:Fault_Code5:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code5:2{prop:Req} = True
    ?par:Fault_Code5:2{prop:Trn} = 0
    ?par:Fault_Code5:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code6:Prompt{prop:FontColor} = -1
    ?par:Fault_Code6:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code6{prop:ReadOnly} = True
        ?par:Fault_Code6{prop:FontColor} = 65793
        ?par:Fault_Code6{prop:Color} = 15066597
    Elsif ?par:Fault_Code6{prop:Req} = True
        ?par:Fault_Code6{prop:FontColor} = 65793
        ?par:Fault_Code6{prop:Color} = 8454143
    Else ! If ?par:Fault_Code6{prop:Req} = True
        ?par:Fault_Code6{prop:FontColor} = 65793
        ?par:Fault_Code6{prop:Color} = 16777215
    End ! If ?par:Fault_Code6{prop:Req} = True
    ?par:Fault_Code6{prop:Trn} = 0
    ?par:Fault_Code6{prop:FontStyle} = font:Bold
    If ?par:Fault_Code6:2{prop:ReadOnly} = True
        ?par:Fault_Code6:2{prop:FontColor} = 65793
        ?par:Fault_Code6:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code6:2{prop:Req} = True
        ?par:Fault_Code6:2{prop:FontColor} = 65793
        ?par:Fault_Code6:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code6:2{prop:Req} = True
        ?par:Fault_Code6:2{prop:FontColor} = 65793
        ?par:Fault_Code6:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code6:2{prop:Req} = True
    ?par:Fault_Code6:2{prop:Trn} = 0
    ?par:Fault_Code6:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code7:Prompt{prop:FontColor} = -1
    ?par:Fault_Code7:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code7{prop:ReadOnly} = True
        ?par:Fault_Code7{prop:FontColor} = 65793
        ?par:Fault_Code7{prop:Color} = 15066597
    Elsif ?par:Fault_Code7{prop:Req} = True
        ?par:Fault_Code7{prop:FontColor} = 65793
        ?par:Fault_Code7{prop:Color} = 8454143
    Else ! If ?par:Fault_Code7{prop:Req} = True
        ?par:Fault_Code7{prop:FontColor} = 65793
        ?par:Fault_Code7{prop:Color} = 16777215
    End ! If ?par:Fault_Code7{prop:Req} = True
    ?par:Fault_Code7{prop:Trn} = 0
    ?par:Fault_Code7{prop:FontStyle} = font:Bold
    If ?par:Fault_Code7:2{prop:ReadOnly} = True
        ?par:Fault_Code7:2{prop:FontColor} = 65793
        ?par:Fault_Code7:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code7:2{prop:Req} = True
        ?par:Fault_Code7:2{prop:FontColor} = 65793
        ?par:Fault_Code7:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code7:2{prop:Req} = True
        ?par:Fault_Code7:2{prop:FontColor} = 65793
        ?par:Fault_Code7:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code7:2{prop:Req} = True
    ?par:Fault_Code7:2{prop:Trn} = 0
    ?par:Fault_Code7:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code8:Prompt{prop:FontColor} = -1
    ?par:Fault_Code8:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code8{prop:ReadOnly} = True
        ?par:Fault_Code8{prop:FontColor} = 65793
        ?par:Fault_Code8{prop:Color} = 15066597
    Elsif ?par:Fault_Code8{prop:Req} = True
        ?par:Fault_Code8{prop:FontColor} = 65793
        ?par:Fault_Code8{prop:Color} = 8454143
    Else ! If ?par:Fault_Code8{prop:Req} = True
        ?par:Fault_Code8{prop:FontColor} = 65793
        ?par:Fault_Code8{prop:Color} = 16777215
    End ! If ?par:Fault_Code8{prop:Req} = True
    ?par:Fault_Code8{prop:Trn} = 0
    ?par:Fault_Code8{prop:FontStyle} = font:Bold
    If ?par:Fault_Code8:2{prop:ReadOnly} = True
        ?par:Fault_Code8:2{prop:FontColor} = 65793
        ?par:Fault_Code8:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code8:2{prop:Req} = True
        ?par:Fault_Code8:2{prop:FontColor} = 65793
        ?par:Fault_Code8:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code8:2{prop:Req} = True
        ?par:Fault_Code8:2{prop:FontColor} = 65793
        ?par:Fault_Code8:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code8:2{prop:Req} = True
    ?par:Fault_Code8:2{prop:Trn} = 0
    ?par:Fault_Code8:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code9:Prompt{prop:FontColor} = -1
    ?par:Fault_Code9:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code9{prop:ReadOnly} = True
        ?par:Fault_Code9{prop:FontColor} = 65793
        ?par:Fault_Code9{prop:Color} = 15066597
    Elsif ?par:Fault_Code9{prop:Req} = True
        ?par:Fault_Code9{prop:FontColor} = 65793
        ?par:Fault_Code9{prop:Color} = 8454143
    Else ! If ?par:Fault_Code9{prop:Req} = True
        ?par:Fault_Code9{prop:FontColor} = 65793
        ?par:Fault_Code9{prop:Color} = 16777215
    End ! If ?par:Fault_Code9{prop:Req} = True
    ?par:Fault_Code9{prop:Trn} = 0
    ?par:Fault_Code9{prop:FontStyle} = font:Bold
    If ?par:Fault_Code9:2{prop:ReadOnly} = True
        ?par:Fault_Code9:2{prop:FontColor} = 65793
        ?par:Fault_Code9:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code9:2{prop:Req} = True
        ?par:Fault_Code9:2{prop:FontColor} = 65793
        ?par:Fault_Code9:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code9:2{prop:Req} = True
        ?par:Fault_Code9:2{prop:FontColor} = 65793
        ?par:Fault_Code9:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code9:2{prop:Req} = True
    ?par:Fault_Code9:2{prop:Trn} = 0
    ?par:Fault_Code9:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code10:Prompt{prop:FontColor} = -1
    ?par:Fault_Code10:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code10{prop:ReadOnly} = True
        ?par:Fault_Code10{prop:FontColor} = 65793
        ?par:Fault_Code10{prop:Color} = 15066597
    Elsif ?par:Fault_Code10{prop:Req} = True
        ?par:Fault_Code10{prop:FontColor} = 65793
        ?par:Fault_Code10{prop:Color} = 8454143
    Else ! If ?par:Fault_Code10{prop:Req} = True
        ?par:Fault_Code10{prop:FontColor} = 65793
        ?par:Fault_Code10{prop:Color} = 16777215
    End ! If ?par:Fault_Code10{prop:Req} = True
    ?par:Fault_Code10{prop:Trn} = 0
    ?par:Fault_Code10{prop:FontStyle} = font:Bold
    If ?par:Fault_Code10:2{prop:ReadOnly} = True
        ?par:Fault_Code10:2{prop:FontColor} = 65793
        ?par:Fault_Code10:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code10:2{prop:Req} = True
        ?par:Fault_Code10:2{prop:FontColor} = 65793
        ?par:Fault_Code10:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code10:2{prop:Req} = True
        ?par:Fault_Code10:2{prop:FontColor} = 65793
        ?par:Fault_Code10:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code10:2{prop:Req} = True
    ?par:Fault_Code10:2{prop:Trn} = 0
    ?par:Fault_Code10:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code11:Prompt{prop:FontColor} = -1
    ?par:Fault_Code11:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code11{prop:ReadOnly} = True
        ?par:Fault_Code11{prop:FontColor} = 65793
        ?par:Fault_Code11{prop:Color} = 15066597
    Elsif ?par:Fault_Code11{prop:Req} = True
        ?par:Fault_Code11{prop:FontColor} = 65793
        ?par:Fault_Code11{prop:Color} = 8454143
    Else ! If ?par:Fault_Code11{prop:Req} = True
        ?par:Fault_Code11{prop:FontColor} = 65793
        ?par:Fault_Code11{prop:Color} = 16777215
    End ! If ?par:Fault_Code11{prop:Req} = True
    ?par:Fault_Code11{prop:Trn} = 0
    ?par:Fault_Code11{prop:FontStyle} = font:Bold
    If ?par:Fault_Code11:2{prop:ReadOnly} = True
        ?par:Fault_Code11:2{prop:FontColor} = 65793
        ?par:Fault_Code11:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code11:2{prop:Req} = True
        ?par:Fault_Code11:2{prop:FontColor} = 65793
        ?par:Fault_Code11:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code11:2{prop:Req} = True
        ?par:Fault_Code11:2{prop:FontColor} = 65793
        ?par:Fault_Code11:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code11:2{prop:Req} = True
    ?par:Fault_Code11:2{prop:Trn} = 0
    ?par:Fault_Code11:2{prop:FontStyle} = font:Bold
    ?par:Fault_Code12:Prompt{prop:FontColor} = -1
    ?par:Fault_Code12:Prompt{prop:Color} = 15066597
    If ?par:Fault_Code12{prop:ReadOnly} = True
        ?par:Fault_Code12{prop:FontColor} = 65793
        ?par:Fault_Code12{prop:Color} = 15066597
    Elsif ?par:Fault_Code12{prop:Req} = True
        ?par:Fault_Code12{prop:FontColor} = 65793
        ?par:Fault_Code12{prop:Color} = 8454143
    Else ! If ?par:Fault_Code12{prop:Req} = True
        ?par:Fault_Code12{prop:FontColor} = 65793
        ?par:Fault_Code12{prop:Color} = 16777215
    End ! If ?par:Fault_Code12{prop:Req} = True
    ?par:Fault_Code12{prop:Trn} = 0
    ?par:Fault_Code12{prop:FontStyle} = font:Bold
    If ?par:Fault_Code12:2{prop:ReadOnly} = True
        ?par:Fault_Code12:2{prop:FontColor} = 65793
        ?par:Fault_Code12:2{prop:Color} = 15066597
    Elsif ?par:Fault_Code12:2{prop:Req} = True
        ?par:Fault_Code12:2{prop:FontColor} = 65793
        ?par:Fault_Code12:2{prop:Color} = 8454143
    Else ! If ?par:Fault_Code12:2{prop:Req} = True
        ?par:Fault_Code12:2{prop:FontColor} = 65793
        ?par:Fault_Code12:2{prop:Color} = 16777215
    End ! If ?par:Fault_Code12:2{prop:Req} = True
    ?par:Fault_Code12:2{prop:Trn} = 0
    ?par:Fault_Code12:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
WriteScrapAuditRecord ROUTINE     ! Start 2632 BE(20/05/03)
    IF (Access:AUDIT.PrimeRecord() = Level:Benign) THEN
        ! Start 2632/3 BE(02/06/03)
        !aud:Notes         = 'DESCRIPTION: ' & Clip(par:Description)
        aud:Notes         = 'DESCRIPTION: ' & CLIP(par:Description)  & '<13,10>' & |
                            'QUANTITY = ' & CLIP(LEFT(FORMAT((par:Quantity - Quantity_Temp), @n_8)))
        ! End 2632/3 BE(02/06/03)
        aud:Ref_Number    = job:ref_number
        aud:Date          = Today()
        aud:Time          = Clock()
        aud:Type          = 'JOB'
        Access:USERS.ClearKey(use:Password_Key)
        use:Password      = glo:Password
        Access:USERS.Fetch(use:Password_Key)
        aud:User          = use:User_Code
        aud:Action        = 'SCRAP CHARGEABLE PART: ' & Clip(par:Part_Number)
        IF (Access:AUDIT.TryInsert() <> Level:Benign) THEN
            Access:AUDIT.CancelAutoInc()
        END
    END
! End 2632 BE(20/05/03)
InitMaxParts    ROUTINE    !Start Change 1895 BE(15/05/03)
    tmpCheckChargeableParts = 0
    TotalPartsCost = 0.0
    access:SUBTRACC.clearkey(sub:account_number_key)
    sub:Account_Number  = job:Account_Number
    IF (access:SUBTRACC.tryfetch(sub:account_number_key) = Level:Benign) THEN
        access:TRADEACC.clearkey(tra:account_number_key)
        tra:Account_Number  = sub:Main_Account_Number
        IF (access:TRADEACC.tryfetch(tra:account_number_key) = Level:Benign) THEN
            TmpCheckChargeableParts = tra:CheckChargeablePartsCost
        END
    END
!End Change 1895 BE(15/05/03)
TotalChargeableParts    ROUTINE ! Start Change 1895 BE(15/05/03)
    TotalPartsCost = 0
    Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
    Access:PARTS_ALIAS.ClearKey(par_ali:Part_Number_Key)
    par_ali:Ref_Number  = job:Ref_Number
    SET(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
    LOOP
        IF ((Access:PARTS_ALIAS.NEXT() <> Level:Benign) OR |
            (par_ali:Ref_Number  <> job:Ref_Number) ) THEN
            BREAK
        END
        IF ((par_ali:Adjustment <> 'YES') AND (par_ali:Record_Number <> par:Record_Number)) THEN
            TotalPartsCost += par_ali:Purchase_Cost * par_ali:Quantity
        END
    END
    Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)
! End Change 1895 BE(15/05/03)
CreateNewOrder      Routine
    If Access:ORDPEND.PrimeRecord() = Level:Benign  
        ope:Part_Ref_Number  = tmp:StockNumber
        ope:Job_Number       = job:Ref_Number
        ope:Part_Type        = 'JOB'
        ope:Supplier         = par:Supplier
        ope:Part_Number      = par:Part_Number
        ope:Description      = par:Description
        ope:Quantity         = tmp:NewQuantity
        ope:Account_Number   = job:Account_Number
        ope:PartRecordNumber = par:Record_Number
        
        If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Successful
        Else !If Access:ORDPEND.TryInsert() = Level:Benign
            !Insert Failed
        End !If Access:ORDPEND.TryInsert() = Level:Benign
    End !If Access:ORDPEND.PrimeRecord() = Level:Benign
Adjustment          Routine
    If par:adjustment = 'YES'
        par:Part_Number = 'ADJUSTMENT'
        par:Description = 'ADJUSTMENT'
        par:Purchase_Cost = ''
        par:Sale_Cost     = ''
        par:quantity = 1


       ?par:Part_Number{prop:readonly} = 0
       ?par:Part_Number{prop:skip} = 0
       ?par:Part_Number{prop:disable} = 1
   

       ?par:Description{prop:readonly} = 0
       ?par:Description{prop:skip} = 0
       ?par:Description{prop:disable} = 1
   

       ?par:Quantity{prop:readonly} = 0
       ?par:Quantity{prop:skip} = 0
       ?par:Quantity{prop:disable} = 1
   

       ?par:Purchase_Cost{prop:readonly} = 0
       ?par:Purchase_Cost{prop:skip} = 0
       ?par:Purchase_Cost{prop:disable} = 1
   

       ?par:Sale_Cost{prop:readonly} = 0
       ?par:Sale_Cost{prop:skip} = 0
       ?par:Sale_Cost{prop:disable} = 1
   

       ?par:Supplier{prop:readonly} = 0
       ?par:Supplier{prop:skip} = 0
       ?par:Supplier{prop:disable} = 1

       ?par:Exclude_From_Order{prop:readonly} = 0
       ?par:Exclude_From_Order{prop:skip} = 0
       ?par:Exclude_From_Order{prop:disable} = 1
   
    Else
   

       ?par:Part_Number{prop:readonly} = 0
       ?par:Part_Number{prop:skip} = 0
       ?par:Part_Number{prop:disable} = 0
   

       ?par:Description{prop:readonly} = 0
       ?par:Description{prop:skip} = 0
       ?par:Description{prop:disable} = 0
   

       ?par:Quantity{prop:readonly} = 0
       ?par:Quantity{prop:skip} = 0
       ?par:Quantity{prop:disable} = 0
   

       ?par:Purchase_Cost{prop:readonly} = 0
       ?par:Purchase_Cost{prop:skip} = 0
       ?par:Purchase_Cost{prop:disable} = 0
   

       ?par:Sale_Cost{prop:readonly} = 0
       ?par:Sale_Cost{prop:skip} = 0
       ?par:Sale_Cost{prop:disable} = 0

       ?par:Supplier{prop:readonly} = 0
       ?par:Supplier{prop:skip} = 0
       ?par:Supplier{prop:disable} = 0
   

       ?par:Exclude_From_Order{prop:readonly} = 0
       ?par:Exclude_From_Order{prop:skip} = 0
       ?par:Exclude_From_Order{prop:disable} = 0
   
    End
    Display()

Hide_Fields     Routine
    If par:part_ref_number = ''
        Do Show_parts
    Else
        Do Hide_parts
    End
Show_Parts      Routine
   
       ?par:Part_Number{prop:readonly} = 0
       ?par:Part_Number{prop:skip} = 0
       ?par:Part_Number{prop:disable} = 0
   
       ?par:Description{prop:readonly} = 0
       ?par:Description{prop:skip} = 0
       ?par:Description{prop:disable} = 0

       ?par:Purchase_Cost{prop:readonly} = 0
       ?par:Purchase_Cost{prop:skip} = 0
       ?par:Purchase_Cost{prop:disable} = 0
   

       ?par:Sale_Cost{prop:readonly} = 0
       ?par:Sale_Cost{prop:skip} = 0
       ?par:Sale_Cost{prop:disable} = 0
   

       ?par:Supplier{prop:readonly} = 0
       ?par:Supplier{prop:skip} = 0
       ?par:Supplier{prop:disable} = 0
   
       Display()
   
   
Hide_Parts      Routine
   

       ?par:Part_Number{prop:readonly} = 1
       ?par:Part_Number{prop:skip} = 1
       ?par:Part_Number{prop:disable} = 0
   

       ?par:Description{prop:readonly} = 1
       ?par:Description{prop:skip} = 1
       ?par:Description{prop:disable} = 0
   

       ?par:Purchase_Cost{prop:readonly} = 1
       ?par:Purchase_Cost{prop:skip} = 1
       ?par:Purchase_Cost{prop:disable} = 0
   

       ?par:Sale_Cost{prop:readonly} = 1
       ?par:Sale_Cost{prop:skip} = 1
       ?par:Sale_Cost{prop:disable} = 0
   

       ?par:Supplier{prop:readonly} = 1
       ?par:Supplier{prop:skip} = 1
       ?par:Supplier{prop:disable} = 0
   
       Display()

Lookup_Location     Routine
    If par:part_ref_number <> ''
        access:stock.clearkey(sto:ref_number_key)
        sto:ref_number  = par:part_ref_number
        If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = sto:location
            tmp:shelflocation   = sto:shelf_location
            tmp:secondlocation  = sto:second_location
        Else!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
            tmp:location    = ''
            tmp:shelflocation   = ''
            tmp:secondlocation  = ''
        End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
    Else!If par:part_ref_number <> ''
        tmp:location    = ''
        tmp:shelflocation   = ''
        tmp:secondlocation  = ''
    End!If par:part_ref_number <> ''
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Parts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Parts',1)
    SolaceViewVars('Result',Result,'Update_Parts',1)
    SolaceViewVars('Msg1',Msg1,'Update_Parts',1)
    SolaceViewVars('Msg2',Msg2,'Update_Parts',1)
    SolaceViewVars('TotalPartsCost',TotalPartsCost,'Update_Parts',1)
    SolaceViewVars('tmpCheckChargeableParts',tmpCheckChargeableParts,'Update_Parts',1)
    SolaceViewVars('fault_codes_required_temp',fault_codes_required_temp,'Update_Parts',1)
    SolaceViewVars('pos',pos,'Update_Parts',1)
    SolaceViewVars('Part_Details_Group:Part_Number',Part_Details_Group:Part_Number,'Update_Parts',1)
    SolaceViewVars('Part_Details_Group:Description',Part_Details_Group:Description,'Update_Parts',1)
    SolaceViewVars('Part_Details_Group:Supplier',Part_Details_Group:Supplier,'Update_Parts',1)
    SolaceViewVars('Part_Details_Group:Purchase_Cost',Part_Details_Group:Purchase_Cost,'Update_Parts',1)
    SolaceViewVars('Part_Details_Group:Sale_Cost',Part_Details_Group:Sale_Cost,'Update_Parts',1)
    SolaceViewVars('adjustment_temp',adjustment_temp,'Update_Parts',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Update_Parts',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Update_Parts',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Parts',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Parts',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Update_Parts',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Update_Parts',1)
    SolaceViewVars('tmp:location',tmp:location,'Update_Parts',1)
    SolaceViewVars('tmp:ShelfLocation',tmp:ShelfLocation,'Update_Parts',1)
    SolaceViewVars('tmp:SecondLocation',tmp:SecondLocation,'Update_Parts',1)
    SolaceViewVars('sav:ExcludeFromOrder',sav:ExcludeFromOrder,'Update_Parts',1)
    SolaceViewVars('sav:Supplier',sav:Supplier,'Update_Parts',1)
    SolaceViewVars('tmp:CreateNewOrder',tmp:CreateNewOrder,'Update_Parts',1)
    SolaceViewVars('tmp:StockNumber',tmp:StockNumber,'Update_Parts',1)
    SolaceViewVars('tmp:NewQuantity',tmp:NewQuantity,'Update_Parts',1)
    SolaceViewVars('tmp:PendingPart',tmp:PendingPart,'Update_Parts',1)
    SolaceViewVars('tmp:CurrentState',tmp:CurrentState,'Update_Parts',1)
    SolaceViewVars('tmp_RF_Board_IMEI',tmp_RF_Board_IMEI,'Update_Parts',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title;  SolaceCtrlName = '?Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Part_Number:Prompt;  SolaceCtrlName = '?par:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Part_Number;  SolaceCtrlName = '?par:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?browse_stock_button;  SolaceCtrlName = '?browse_stock_button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Description:Prompt;  SolaceCtrlName = '?par:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Description;  SolaceCtrlName = '?par:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location:Prompt;  SolaceCtrlName = '?tmp:location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location;  SolaceCtrlName = '?tmp:location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location:Prompt:2;  SolaceCtrlName = '?tmp:location:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ShelfLocation;  SolaceCtrlName = '?tmp:ShelfLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SecondLocation;  SolaceCtrlName = '?tmp:SecondLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AdjustmentGroup;  SolaceCtrlName = '?AdjustmentGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Purchase_Cost:Prompt;  SolaceCtrlName = '?par:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Purchase_Cost;  SolaceCtrlName = '?par:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Sale_Cost:Prompt;  SolaceCtrlName = '?par:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Sale_Cost;  SolaceCtrlName = '?par:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Supplier:Prompt;  SolaceCtrlName = '?par:Supplier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Supplier;  SolaceCtrlName = '?par:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSupplier;  SolaceCtrlName = '?LookupSupplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Quantity;  SolaceCtrlName = '?par:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Despatch_Note_Number:Prompt;  SolaceCtrlName = '?par:Despatch_Note_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Despatch_Note_Number;  SolaceCtrlName = '?par:Despatch_Note_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RF_BOARD_PROMPT;  SolaceCtrlName = '?RF_BOARD_PROMPT';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp_RF_Board_IMEI;  SolaceCtrlName = '?tmp_RF_Board_IMEI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Exclude_From_Order;  SolaceCtrlName = '?par:Exclude_From_Order';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt24;  SolaceCtrlName = '?Prompt24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Date_Ordered:Prompt;  SolaceCtrlName = '?par:Date_Ordered:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Date_Ordered;  SolaceCtrlName = '?par:Date_Ordered';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Order_Number:Prompt;  SolaceCtrlName = '?par:Order_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Order_Number;  SolaceCtrlName = '?par:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PAR:Date_Received:Prompt;  SolaceCtrlName = '?PAR:Date_Received:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Date_Received;  SolaceCtrlName = '?par:Date_Received';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Code_Tab;  SolaceCtrlName = '?Fault_Code_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Codes_Checked;  SolaceCtrlName = '?par:Fault_Codes_Checked';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code1:Prompt;  SolaceCtrlName = '?par:Fault_Code1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code1;  SolaceCtrlName = '?par:Fault_Code1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code1:2;  SolaceCtrlName = '?par:Fault_Code1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code2:Prompt;  SolaceCtrlName = '?par:Fault_Code2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code2;  SolaceCtrlName = '?par:Fault_Code2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code2:2;  SolaceCtrlName = '?par:Fault_Code2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:2;  SolaceCtrlName = '?Button4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code3:Prompt;  SolaceCtrlName = '?par:Fault_Code3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code3;  SolaceCtrlName = '?par:Fault_Code3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code3:2;  SolaceCtrlName = '?par:Fault_Code3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:3;  SolaceCtrlName = '?Button4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code4:Prompt;  SolaceCtrlName = '?par:Fault_Code4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code4;  SolaceCtrlName = '?par:Fault_Code4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:4;  SolaceCtrlName = '?Button4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code4:2;  SolaceCtrlName = '?par:Fault_Code4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code5:Prompt;  SolaceCtrlName = '?par:Fault_Code5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code5;  SolaceCtrlName = '?par:Fault_Code5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code5:2;  SolaceCtrlName = '?par:Fault_Code5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:5;  SolaceCtrlName = '?Button4:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code6:Prompt;  SolaceCtrlName = '?par:Fault_Code6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code6;  SolaceCtrlName = '?par:Fault_Code6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code6:2;  SolaceCtrlName = '?par:Fault_Code6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:6;  SolaceCtrlName = '?Button4:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code7:Prompt;  SolaceCtrlName = '?par:Fault_Code7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code7;  SolaceCtrlName = '?par:Fault_Code7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code7:2;  SolaceCtrlName = '?par:Fault_Code7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:7;  SolaceCtrlName = '?Button4:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code8:Prompt;  SolaceCtrlName = '?par:Fault_Code8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code8;  SolaceCtrlName = '?par:Fault_Code8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code8:2;  SolaceCtrlName = '?par:Fault_Code8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:8;  SolaceCtrlName = '?Button4:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code9:Prompt;  SolaceCtrlName = '?par:Fault_Code9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code9;  SolaceCtrlName = '?par:Fault_Code9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code9:2;  SolaceCtrlName = '?par:Fault_Code9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:9;  SolaceCtrlName = '?Button4:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code10:Prompt;  SolaceCtrlName = '?par:Fault_Code10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code10;  SolaceCtrlName = '?par:Fault_Code10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code10:2;  SolaceCtrlName = '?par:Fault_Code10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:10;  SolaceCtrlName = '?Button4:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code11:Prompt;  SolaceCtrlName = '?par:Fault_Code11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code11;  SolaceCtrlName = '?par:Fault_Code11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code11:2;  SolaceCtrlName = '?par:Fault_Code11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:11;  SolaceCtrlName = '?Button4:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code12:Prompt;  SolaceCtrlName = '?par:Fault_Code12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code12;  SolaceCtrlName = '?par:Fault_Code12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?par:Fault_Code12:2;  SolaceCtrlName = '?par:Fault_Code12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4:12;  SolaceCtrlName = '?Button4:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Chargeable Part'
  OF ChangeRecord
    ActionMessage = 'Changing A Chargeable Part'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Parts')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Parts')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Title
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(par:Record,History::par:Record)
  SELF.AddHistoryField(?par:Part_Number,5)
  SELF.AddHistoryField(?par:Description,6)
  SELF.AddHistoryField(?par:Purchase_Cost,8)
  SELF.AddHistoryField(?par:Sale_Cost,9)
  SELF.AddHistoryField(?par:Supplier,7)
  SELF.AddHistoryField(?par:Quantity,11)
  SELF.AddHistoryField(?par:Despatch_Note_Number,14)
  SELF.AddHistoryField(?par:Exclude_From_Order,13)
  SELF.AddHistoryField(?par:Date_Ordered,15)
  SELF.AddHistoryField(?par:Order_Number,17)
  SELF.AddHistoryField(?par:Date_Received,19)
  SELF.AddHistoryField(?par:Fault_Codes_Checked,21)
  SELF.AddHistoryField(?par:Fault_Code1,25)
  SELF.AddHistoryField(?par:Fault_Code1:2,25)
  SELF.AddHistoryField(?par:Fault_Code2,26)
  SELF.AddHistoryField(?par:Fault_Code2:2,26)
  SELF.AddHistoryField(?par:Fault_Code3,27)
  SELF.AddHistoryField(?par:Fault_Code3:2,27)
  SELF.AddHistoryField(?par:Fault_Code4,28)
  SELF.AddHistoryField(?par:Fault_Code4:2,28)
  SELF.AddHistoryField(?par:Fault_Code5,29)
  SELF.AddHistoryField(?par:Fault_Code5:2,29)
  SELF.AddHistoryField(?par:Fault_Code6,30)
  SELF.AddHistoryField(?par:Fault_Code6:2,30)
  SELF.AddHistoryField(?par:Fault_Code7,31)
  SELF.AddHistoryField(?par:Fault_Code7:2,31)
  SELF.AddHistoryField(?par:Fault_Code8,32)
  SELF.AddHistoryField(?par:Fault_Code8:2,32)
  SELF.AddHistoryField(?par:Fault_Code9,33)
  SELF.AddHistoryField(?par:Fault_Code9:2,33)
  SELF.AddHistoryField(?par:Fault_Code10,34)
  SELF.AddHistoryField(?par:Fault_Code10:2,34)
  SELF.AddHistoryField(?par:Fault_Code11,35)
  SELF.AddHistoryField(?par:Fault_Code11:2,35)
  SELF.AddHistoryField(?par:Fault_Code12,36)
  SELF.AddHistoryField(?par:Fault_Code12:2,36)
  SELF.AddUpdateFile(Access:PARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:SUPPLIER.UseFile
  Access:MANFAUPA.UseFile
  Access:JOBS.UseFile
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:PARTS.UseFile
  Access:USERS.UseFile
  Access:LOCATION.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:ORDPEND.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  Do Lookup_Location
  
  sav:ExcludeFromOrder    = par:Exclude_From_Order
  sav:Supplier = par:Supplier
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?par:Fault_Code7:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code8:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code9:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code10:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code11:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code12:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code1:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code2:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code3:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code4:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code5:2{Prop:Alrt,255} = MouseLeft2
  ?par:Fault_Code6:2{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?par:Supplier{Prop:Tip} AND ~?LookupSupplier{Prop:Tip}
     ?LookupSupplier{Prop:Tip} = 'Select ' & ?par:Supplier{Prop:Tip}
  END
  IF ?par:Supplier{Prop:Msg} AND ~?LookupSupplier{Prop:Msg}
     ?LookupSupplier{Prop:Msg} = 'Select ' & ?par:Supplier{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Parts',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    par:Adjustment = 'NO'
    par:Warranty_Part = 'NO'
    par:Exclude_From_Order = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Suppliers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?browse_stock_button
      Error# = 0
      user_code"  = ''
      If job:engineer <> ''
          Access:USERS.Clearkey(use:User_Code_Key)
          use:User_Code   = job:Engineer
          If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Found
              user_code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:User_Code_Key) = Level:Benign
          
          
      Else!If job:engineer <> ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              User_Code"  = use:User_Code
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
          
      End!If job:engineer <> ''
      
      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
      loc:Active   = 1
      loc:Location = use:Location
      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
          !Found
      
      Else!lIf Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Case MessageEx('The selected user is not allocated to an Active Site Location.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          If use:StockFromLocationOnly
              Error# = 1
          End !If use:StockFromLocationOnly
      End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey). = Level:Benign
      
      If Error# = 0
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_model_stock(job:model_number,user_code")
          if globalresponse = requestcompleted
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = stm:ref_number
      
              if access:stock.fetch(sto:ref_number_key)
                  beep(beep:systemhand)  ;  yield()
                  message('Error! Cannot access the Stock File.', |
                          'ServiceBase 2000', icon:hand)
              Else!if access:stock.fetch(sto:ref_number_key)
                  If sto:Suspend
                      Case MessageEx('This part cannot be used. It has been suspended.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      par:Part_Number = ''
                      Select(?par:Part_Number)
                  Else !If sto:Suspend
      
                      par:part_number     = sto:part_number
                      par:description     = sto:description
                      par:supplier        = sto:supplier
                      par:purchase_cost   = sto:purchase_cost
                      par:sale_cost       = sto:sale_cost
                      par:part_ref_number = sto:ref_number
                      If sto:assign_fault_codes = 'YES'
                          stm:Manufacturer = sto:Manufacturer
                          stm:Model_Number = job:Model_Number
                          If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                              par:fault_code1    = stm:Faultcode1
                              par:fault_code2    = stm:Faultcode2
                              par:fault_code3    = stm:Faultcode3
                              par:fault_code4    = stm:Faultcode4
                              par:fault_code5    = stm:Faultcode5
                              par:fault_code6    = stm:Faultcode6
                              par:fault_code7    = stm:Faultcode7
                              par:fault_code8    = stm:Faultcode8
                              par:fault_code9    = stm:Faultcode9
                              par:fault_code10   = stm:Faultcode10
                              par:fault_code11   = stm:Faultcode11
                              par:fault_code12   = stm:Faultcode12
      
                          End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      End!If sto:assign_fault_codes = 'YES'
      
                      !TH 09/02/04
                      Access:LOCATION.ClearKey(loc:ActiveLocationKey)
                      loc:Active   = 1
                      loc:Location = sto:Location
                      If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
      
                      if loc:PickNoteEnable then
                          ?par:Exclude_From_Order{prop:disable} = true
                      else
                          ! Start Change 2116 BE(26/06/03)
                          IF (sto:RF_BOARD) THEN
                              UNHIDE(?RF_BOARD_PROMPT)
                              UNHIDE(?tmp_RF_Board_IMEI)
                              beep(beep:systemexclamation)  ;  yield()
                              message('This part will change the IMEI on the job.||Please enter the new IMEI.', |
                                      'ServiceBase 2000', icon:exclamation)
                              Select(?tmp_RF_Board_IMEI)
                          ELSE
                              HIDE(?RF_BOARD_PROMPT)
                              HIDE(?tmp_RF_Board_IMEI)
                              Select(?par:quantity)
                          END
                          !Select(?tmp_RF_Board_IMEI)
                      end !if ~loc:PickNoteEnable TH 09/02/04
      
                      !Select(?par:quantity)
                      ! End Change 2116 BE(26/06/03)
                  End !If sto:Suspend
                  display()
              end!if access:stock.fetch(sto:ref_number_key)
      
          end
          globalrequest     = saverequest#
          do lookup_location
      End !Error# = 0
    OF ?tmp_RF_Board_IMEI
      IF (~0{prop:acceptall}) THEN
          IF (LEN(CLIP(tmp_RF_Board_IMEI)) = 18) THEN
            !Ericsson IMEI!
            tmp_RF_Board_IMEI = SUB(tmp_RF_Board_IMEI,4,15)
          END
          IF (CheckLength('IMEI',job:Model_Number,tmp_RF_Board_IMEI)) THEN
              SELECT(?tmp_RF_Board_IMEI)
              CYCLE
          END
      END
    OF ?par:Exclude_From_Order
      If ~0{prop:acceptall}
          If sav:ExcludeFromOrder <> par:Exclude_From_Order
              If SecurityCheck('PARTS - EXCLUDE FROM ORDER')
                  Case MessageEx('You do not have access to this option!','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  par:Exclude_From_Order = sav:ExcludeFromOrder
              Else!If SecurityCheck('EXCLUDE FROM ORDER')
                  If thiswindow.request = Insertrecord
                      If PAR:Exclude_From_Order = 'YES' And par:part_ref_number <> ''
                          Case MessageEx('You have selected to ''Exclude From Order''.'&|
                            '<13,10>'&|
                            '<13,10>Warning! This stock part will NOT be decremented, and an order will NOT be raised if there are insufficient items in stock.'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                         'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                                  sav:ExcludeFromOrder    = par:Exclude_From_Order
                              Of 2 ! &No Button
                                  par:Exclude_From_Order  = sav:ExcludeFromOrder
                          End!Case MessageEx
                      End!If PAR:Exclude_From_Order = 'YES'
                  End!If thiswindow.request = Insertrecord
              End!If SecurityCheck('EXCLUDE FROM ORDER')
          End!If sav:ExcludeFromOrder <> par:Exclude_From_Order
      End!If ~0{prop:acceptall}
      Display()
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?par:Part_Number
      If ~0{prop:acceptall}
      error# = 0
      access:users.clearkey(use:user_code_key)
      use:user_code = job:engineer
      if access:users.tryfetch(use:user_code_key)
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
              Access:LOCATION.ClearKey(loc:Location_Key)
              loc:Location = use:Location
              If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Found
                  If loc:Active = 0
                      Error# = 3
                  End !If loc:Active = 0
              Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              If Error# = 0
                  access:stock.clearkey(sto:location_manufacturer_key)
                  sto:location     = use:location
                  sto:manufacturer = job:manufacturer
                  sto:part_number  = par:part_number
                  if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                      stm:Ref_Number   = sto:Ref_Number
                      stm:Model_Number = job:Model_Number
                      If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Found
                          If sto:Suspend
                              error# = 2
                          Else !If sto:Suspended
                              error# = 0
                          End !If sto:Suspended
                      Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                          !Error
                          error# = 1
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                  Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                      error# = 1
                  End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
      
              End !If Error# = 0
      
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
      Else!if access:users.tryfetch(use:user_code_key) = Level:Benign
          Access:LOCATION.ClearKey(loc:Location_Key)
          loc:Location = use:Location
          If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Found
              If loc:Active = 0
                  Error# = 3
              End !If loc:Active = 0
          Else!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign
          If Error# = 0
              access:stock.clearkey(sto:location_manufacturer_key)
              sto:location     = use:location
              sto:manufacturer = job:manufacturer
              sto:part_number  = par:part_number
              if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  Access:STOMODEL.ClearKey(stm:Mode_Number_Only_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Found
                      If sto:Suspend
                          error# = 2
                      Else !If sto:Suspended
                          error# = 0
                      End !If sto:Suspended
                  Else!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
                      !Error
                      error# = 1
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:STOMODEL.TryFetch(stm:Mode_Number_Only_Key) = Level:Benign
      
              Else!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
                  error# = 1
              End!if access:stock.tryfetch(sto:location_manufacturer_key) = Level:Benign
          End !If Error# = 0
      End!if access:users.tryfetch(use:user_code_key) = Level:Benign
      
      Case error#
          Of 0
              par:part_number     = sto:part_number
              par:description     = sto:description
              par:supplier        = sto:supplier
              par:purchase_cost   = sto:purchase_cost
              par:sale_cost       = sto:sale_cost
              par:part_ref_number = sto:ref_number
              If sto:assign_fault_codes = 'YES'
                  Access:STOMODEL.ClearKey(stm:Model_Number_Key)
                  stm:Ref_Number   = sto:Ref_Number
                  stm:Manufacturer = sto:Manufacturer
                  stm:Model_Number = job:Model_Number
                  If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
                      par:fault_code1    = stm:Faultcode1
                      par:fault_code2    = stm:Faultcode2
                      par:fault_code3    = stm:Faultcode3
                      par:fault_code4    = stm:Faultcode4
                      par:fault_code5    = stm:Faultcode5
                      par:fault_code6    = stm:Faultcode6
                      par:fault_code7    = stm:Faultcode7
                      par:fault_code8    = stm:Faultcode8
                      par:fault_code9    = stm:Faultcode9
                      par:fault_code10   = stm:Faultcode10
                      par:fault_code11   = stm:Faultcode11
                      par:fault_code12   = stm:Faultcode12
      
                  End!If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
              End!If sto:assign_fault_codes = 'YES'
      
              !TH 09/02/04
              Access:LOCATION.ClearKey(loc:ActiveLocationKey)
              loc:Active   = 1
              loc:Location = sto:Location
              If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.
      
              if loc:PickNoteEnable then
                  ?par:Exclude_From_Order{prop:disable} = true
              else
                  ! Start Change 2116 BE(26/06/03)
                  IF (sto:RF_BOARD) THEN
                      UNHIDE(?RF_BOARD_PROMPT)
                      UNHIDE(?tmp_RF_Board_IMEI)
                      beep(beep:systemexclamation)  ;  yield()
                      message('This part will change the IMEI on the job.||Please enter the new IMEI.', |
                              'ServiceBase 2000', icon:exclamation)
                      Select(?tmp_RF_Board_IMEI)
                  ELSE
                      HIDE(?RF_BOARD_PROMPT)
                      HIDE(?tmp_RF_Board_IMEI)
                      Select(?par:quantity)
                  END
                  !Select(?tmp_RF_Board_IMEI)
              end !if ~loc:PickNoteEnable TH 09/02/04
      
              !Select(?par:quantity)
              ! End Change 2116 BE(26/06/03)
      
              Select(?par:quantity)
              display()
      
          Of 1
      
              Case MessageEx('Cannot find the selected Part!<13,10><13,10>It does either not exist in the engineer''s location, or is not in stock.  <13,10><13,10>Do you wish to select another part, or continue using the entered part number?','ServiceBase 2000',|
                             'Styles\question.ico','|&Select Another Part|&Continue Using Part',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Select Another Part Button
                      Post(event:accepted,?browse_stock_button)
                  Of 2 ! &Continue Using Part Button
                      Select(?par:description)
              End!Case MessageEx
          Of 2
              Case MessageEx('This part cannot be used. It has been suspended.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              par:Part_Number = ''
              Select(?par:Part_Number)
          Of 3
              Case MessageEx('Error!  Stock for this job will be picked from location ' & Clip(use:Location) & ' by default.'&|
                '<13,10>'&|
                '<13,10>This location is not active and cannot be used.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              par:Part_Number = ''
              Select(?par:Part_Number)
      End!If error# = 0
      Do Lookup_Location
      Display()
      End!If ~0{prop:acceptall}
    OF ?par:Supplier
      IF par:Supplier OR ?par:Supplier{Prop:Req}
        sup:Company_Name = par:Supplier
        !Save Lookup Field Incase Of error
        look:par:Supplier        = par:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            par:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            par:Supplier = look:par:Supplier
            SELECT(?par:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          If sav:Supplier <> par:Supplier
              If SecurityCheck('PARTS - AMEND SUPPLIER')
                  Case MessageEx('You do not have access to change the Supplier.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  par:Supplier    = sav:Supplier
              Else!If SecurityCheck('PARTS - AMEND SUPPLIER')
                  sav:Supplier    = par:Supplier
              End!If SecurityCheck('PARTS - AMEND SUPPLIER')
          End!If sav:Supplier <> par:Supplier
      End!If ~0{prop:acceptall}
      Display(?Par:Supplier)
    OF ?LookupSupplier
      ThisWindow.Update
      sup:Company_Name = par:Supplier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          par:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?par:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?par:Supplier)
    OF ?par:Fault_Code1
      If par:fault_code1 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 1
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 1
                  mfp:field        = par:fault_code1
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code1 = mfp:field
                      else
                          par:fault_code1 = ''
                          select(?-1)
                      end
                      display(?par:fault_code1)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code1 <> ''
    OF ?Button4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 1
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code1 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code1 = TINCALENDARStyle1(par:Fault_Code1)
          Display(?par:Fault_Code1:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code2
      If par:fault_Code2 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 2
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 2
                  mfp:field        = par:fault_code2
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code2 = mfp:field
                      else
                          par:fault_code2 = ''
                          select(?-1)
                      end
                      display(?par:fault_code2)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_Code2 <> ''
    OF ?Button4:2
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 2
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code2 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code2 = TINCALENDARStyle1(par:Fault_Code2)
          Display(?par:Fault_Code2:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code3
      If par:fault_code3 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 3
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 3
                  mfp:field        = par:fault_code3
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code3 = mfp:field
                      else
                          par:fault_code3 = ''
                          select(?-1)
                      end
                      display(?par:fault_code3)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code3 <> ''
    OF ?Button4:3
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 3
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code3 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code3 = TINCALENDARStyle1(par:Fault_Code3)
          Display(?par:Fault_Code3:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code4
      If par:fault_code4 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 4
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 4
                  mfp:field        = par:fault_code4
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code4 = mfp:field
                      else
                          par:fault_code4 = ''
                          select(?-1)
                      end
                      display(?par:fault_code4)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code4 <> ''
    OF ?Button4:4
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 4
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code4 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code4 = TINCALENDARStyle1(par:Fault_Code4)
          Display(?par:Fault_Code4:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code5
      If par:fault_code5 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 5
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 5
                  mfp:field        = par:fault_code5
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code5 = mfp:field
                      else
                          par:fault_code5 = ''
                          select(?-1)
                      end
                      display(?par:fault_code5)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code5 <> ''
    OF ?Button4:5
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 5
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code5 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code5 = TINCALENDARStyle1(par:Fault_Code5)
          Display(?par:Fault_Code5:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code6
      IF par:fault_code6 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 6
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 6
                  mfp:field        = par:fault_code6
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code6 = mfp:field
                      else
                          par:fault_code6 = ''
                          select(?-1)
                      end
                      display(?par:fault_code6)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!IF par:fault_code6 <> ''
    OF ?Button4:6
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 6
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code6 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code6 = TINCALENDARStyle1(par:Fault_Code6)
          Display(?par:Fault_Code6:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code7
      If par:fault_code7 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 7
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 7
                  mfp:field        = par:fault_code7
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code7 = mfp:field
                      else
                          par:fault_code7 = ''
                          select(?-1)
                      end
                      display(?par:fault_code7)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code7 <> ''
    OF ?Button4:7
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 7
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code7 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code7 = TINCALENDARStyle1(par:Fault_Code7)
          Display(?par:Fault_Code7:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code8
      If par:fault_Code8 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 8
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 8
                  mfp:field        = par:fault_code8
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code8 = mfp:field
                      else
                          par:fault_code8 = ''
                          select(?-1)
                      end
                      display(?par:fault_code8)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_Code8 <> ''
    OF ?Button4:8
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 8
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code8 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code8 = TINCALENDARStyle1(par:Fault_Code8)
          Display(?par:Fault_Code8:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code9
      If par:fault_code9 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 9
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 9
                  mfp:field        = par:fault_code9
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code9 = mfp:field
                      else
                          par:fault_code9 = ''
                          select(?-1)
                      end
                      display(?par:fault_code9)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code9 <> ''
    OF ?Button4:9
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 9
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code9 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code9 = TINCALENDARStyle1(par:Fault_Code9)
          Display(?par:Fault_Code9:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code10
      If par:fault_code10 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 10
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 10
                  mfp:field        = par:fault_code10
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code10 = mfp:field
                      else
                          par:fault_code10 = ''
                          select(?-1)
                      end
                      display(?par:fault_code10)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code10 <> ''
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code10 = TINCALENDARStyle1(par:Fault_Code10)
          Display(?par:Fault_Code10:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:10
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 10
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code10 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?par:Fault_Code11
      If par:fault_code11 <> ''
      
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 11
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 11
                  mfp:field        = par:fault_code11
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code11 = mfp:field
                      else
                          par:fault_code11 = ''
                          select(?-1)
                      end
                      display(?par:fault_code11)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code11 <> ''
    OF ?Button4:11
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 11
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code11 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code11 = TINCALENDARStyle1(par:Fault_Code11)
          Display(?par:Fault_Code11:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?par:Fault_Code12
      If par:fault_code12 <> ''
          access:manfaupa.clearkey(map:field_number_key)
          map:manufacturer = job:manufacturer
          map:field_number = 12
          if access:manfaupa.fetch(map:field_number_key)
              If map:force_lookup = 'YES'
                  access:manfpalo.clearkey(mfp:field_key)
                  mfp:manufacturer = job:manufacturer
                  mfp:field_number = 12
                  mfp:field        = par:fault_code12
                  if access:manfpalo.fetch(mfp:field_key)
                      saverequest#      = globalrequest
                      globalresponse    = requestcancelled
                      globalrequest     = selectrecord
                      browse_manufacturer_part_lookup
                      if globalresponse = requestcompleted
                          par:fault_code12 = mfp:field
                      else
                          par:fault_code12 = ''
                          select(?-1)
                      end
                      display(?par:fault_code12)
                      globalrequest     = saverequest#
                  end!if access:manfpalo.fetch(mfp:field_key)
              End!If map:force_lookup = 'YES'
          end!if access:manfaupa.fetch(map:field_number_key)
      End!If par:fault_code12 <> ''
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          par:Fault_Code12 = TINCALENDARStyle1(par:Fault_Code12)
          Display(?par:Fault_Code12:2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4:12
      ThisWindow.Update
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = job:manufacturer
      glo:select2  = 12
      glo:select3  = ''
      Browse_Manufacturer_Part_Lookup
      if globalresponse = requestcompleted
          par:fault_code12 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Fault Codes Checked
  If par:fault_codes_checked <> 'YES'
      If fault_codes_required_temp = 'YES'
          beep(beep:systemhand)  ;  yield()
          message('You must tick the ''Fault Codes Checked'' box before you can continue.', |
                  'ServiceBase 2000', icon:hand)
          Select(?par:fault_codes_checked)
          Cycle
      End!If fault_codes_required_temp = 'YES'
  End!If wpr:fault_codes_checked <> 'YES'
  If thiswindow.Request = InsertRecord
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      !Is this a stock part?
      tmp:StockNumber = 0
      If par:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = par:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              !Need to check for duplicates?
              tmp:StockNumber = sto:Ref_Number
              Found# = 0
              If sto:AllowDuplicate = 0
                  Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
                  Access:PARTS_ALIAS.ClearKey(par_ali:Part_Number_Key)
                  par_ali:Ref_Number  = job:Ref_Number
                  par_ali:Part_Number = par:Part_Number
                  Set(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
                  Loop
                      If Access:PARTS_ALIAS.NEXT()
                         Break
                      End !If
                      If par_ali:Ref_Number  <> job:Ref_Number      |
                      Or par_ali:Part_Number <> par:Part_Number     |
                          Then Break.  ! End If
                      If par:Record_Number <> par_ali:Record_Number
                          If par_ali:Date_Received = ''
                              Found# = 1
                              Break
                          End !If par_ali:Date_Received
                      End !If par:Record_Number <> par_ali:Record_Number
                  End !Loop
                  Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)
                  If Found# = 1
                      Beep(Beep:SystemHand)  ;  Yield()
                      Case Message('This part is already attached to this job.', |
                              'ServiceBase 2000', Icon:Hand, |
                               '&OK', 1, 0)
                      Of 1  ! Name: &OK  (Default)
                      End !CASE
                      Select(?par:part_number)
                      Cycle                
                  End !If Found# = 1
              End !If sto:AllowDuplicate = 0
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      End !par:Part_Ref_Number <> ''
  
      If par:Part_Number <> 'ADJUSTMENT'
          !The part is not an adjustment
          If par:Exclude_From_Order <> 'YES'        
              !The part can be ordered
              If tmp:StockNumber
  
                  ! Start Change 1895 BE(15/05/03)
                  DO InitMaxParts
                  IF (tmpCheckChargeableParts) THEN
                      DO TotalChargeableParts
                      TotalPartsCost += par:Purchase_cost * par:Quantity
                      IF (TotalPartsCost > tra:MaxChargeablePartsCost) THEN
                          MESSAGE('This part cannot be attached to the job.<13,10>' & |
                              'This would exceed the Maximum Chargeable Parts Cost of the Trade Account.', |
                              'ServiceBase 2000', Icon:Question, 'OK', 1, 0)
                          SELECT(?par:Quantity)
                          CYCLE
                      END
                  END
                  ! End Change 1895 BE(15/05/03)
  
                  If sto:Sundry_Item <> 'YES'
                      !This is from stock
                      If par:Quantity > sto:Quantity_Stock
                          !There isn't enough in stock
                          ! Start Change 216 BE(26/06/03)
                          ! Start Change 4652 BE(1/09/2004)
                          !IF (sto:RF_BOARD) THEN
                          IF (NOT ?tmp_RF_Board_IMEI {PROP:HIDE}) THEN
                          ! End Change 4652 BE(1/09/2004)
                              Beep(Beep:SystemHand)  ;  Yield()
                              Message('There are insufficient items in stock!' & |
                                          '||This part requires the entry of a new IMEI when attached to a job.' & |
                                          '||This part can therefore not be added at this time.', |
                                          'ServiceBase 2000', Icon:Hand, |
                                           '&OK', 1, 0)
                              Select(?par:Quantity)
                              Cycle
                          END
                          ! End Change 216 BE(26/06/03)
                          Beep(Beep:SystemQuestion)  ;  Yield()
                          Case Message('There are insufficient items in stock!'&|
                                  '||Do you wish to ORDER the remaining items, or RE-ENTER the '&|
                                  'quantity required?', |
                                  'ServiceBase 2000', Icon:Question, |
                                   'Order|Re-enter', 2, 0)
                          Of 1  ! Name: Order
                              If SecurityCheck('JOBS - ORDER PARTS')
                                  Beep(Beep:SystemHand)  ;  Yield()
                                  Case Message('You do not have access to this option.', |
                                          'ServiceBase 2000', Icon:Hand, |
                                           '&OK', 1, 0)
                                  Of 1  ! Name: &OK  (Default)
                                  End !CASE                            
                              Else !If SecurityCheck('JOBS - ORDER PARTS')
                                  tmp:CreateNewOrder = 0
                                  Case def:SummaryOrders
                                      Of 0 !Old Way
                                          tmp:CreateNewOrder = 1
                                      Of 1 !New Way
                                          !Check to see if a pending order already exists.
                                          !If so, add to that. 
                                          !If not, create a new pending order
                                          Access:ORDPEND.Clearkey(ope:Supplier_Key)
                                          ope:Supplier    = par:Supplier
                                          ope:Part_Number = par:Part_Number
                                          If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                                              !Found
  
                                              ! Start Change 2224 BE(28/03/03)
                                              !
                                              ! Delay update until it is established that there is enough
                                              ! stock to fulfill a part order
                                              !
                                              !ope:Quantity += par:Quantity
                                              !Access:ORDPEND.Update()
                                              !
                                              ! End Change 2224 BE(28/03/03)
  
                                              tmp:CreateNewOrder = 0
                                              !If there is some in stock, then use that for this part
                                              !then pass the variables, to create another part line
                                              !for the ordered part.
                                              If sto:Quantity_Stock > 0
                                                  glo:Select1 = 'NEW PENDING'
                                                  glo:Select2 = par:Part_Ref_Number
                                                  glo:Select3 = par:Quantity-sto:quantity_stock
                                                  glo:Select4 = ope:Ref_Number
  
                                                  ! Start Change 2224 BE(28/03/03)
                                                  ope:Quantity += (par:Quantity - sto:Quantity_Stock)
                                                  ! End Change 2224 BE(28/03/03)
  
                                                  par:Quantity    = sto:Quantity_Stock
                                                  par:Date_Ordered    = Today()
                                              Else !If sto:Quantity_Stock > 0
  
                                                  ! Start Change 2224 BE(28/03/03)
                                                  ope:Quantity += par:Quantity
                                                  ! End Change 2224 BE(28/03/03)
  
                                                  par:Pending_Ref_Number  =  ope:Ref_Number
                                                  par:Requested   = True
                                                 
                                              End !If sto:Quantity_Stock > 0
  
                                              ! Start Change 2224 BE(28/03/03)
                                              Access:ORDPEND.Update()
                                              ! Start Change 2224 BE(28/03/03)
  
                                          Else! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
                                              tmp:CreateNewOrder  = 1
                                              par:Requested   = True
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:ORDPEND.Tryfetch(orp:Supplier_Key) = Level:Benign
                                          
                                  End !Case def:SummaryOrders
  
                                  If tmp:CreateNewOrder = 1
                                      If Access:ORDPEND.PrimeRecord() = Level:Benign
                                          ope:Part_Ref_Number = sto:Ref_Number
                                          ope:Job_Number      = job:Ref_Number
                                          ope:Part_Type       = 'JOB'
                                          ope:Supplier        = par:Supplier
                                          ope:Part_Number     = par:Part_Number
                                          ope:Description     = par:Description
                                          
                                          If sto:Quantity_Stock <=0
                                              par:Pending_Ref_Number  = ope:Ref_Number
                                              ope:Quantity    = par:Quantity
                                              ope:PartRecordNumber    = par:Record_Number
                                          Else !If sto:Quantity_Stock <=0
                                              glo:Select1 = 'NEW PENDING'
                                              glo:Select2 = par:Part_Ref_Number
                                              glo:Select3 = par:Quantity - sto:Quantity_Stock
                                              glo:Select4 = ope:Ref_Number
                                              ope:Quantity    = par:Quantity - sto:Quantity_Stock
                                              par:Quantity    = sto:Quantity_Stock
                                              par:Date_Ordered = Today()
                                          End !If sto:Quantity_Stock <=0
                                          If Access:ORDPEND.TryInsert() = Level:Benign
                                              !Insert Successful
                                          Else !If Access:ORDPEND.TryInsert() = Level:Benign
                                              !Insert Failed
                                          End !If Access:ORDPEND.TryInsert() = Level:Benign
                                      End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                                  End !If tmp:CreateNewOrder = 1
  
                                  ! Start Change 2224 BE(28/03/03)
                                  ! Status 330 Spares Requested
                                  GetStatus(330,thiswindow.request,'JOB')
                                  ! End Change 2224 BE(28/03/03)
  
                              End !If SecurityCheck('JOBS - ORDER PARTS')
                          Of 2  ! Name: Re-enter  (Default)
                              Select(?par:Quantity)
                              Cycle
                          End !CASE
                      Else !If par:Quantity > sto:Quantity
                          !There is sufficient in stock
                          par:Date_Ordered    = Today()
                          !Reget Stock
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              sto:Quantity_Stock  -= par:Quantity
                              If sto:Quantity_Stock < 0
                                  sto:Quantity_Stock = 0
                              End !If sto:Quantity_Stock < 0
                              If Access:STOCK.Update() = Level:Benign
                                  If def:Add_Stock_Label = 'YES'
                                      glo:Select1 = sto:Ref_Number
                                      Case def:Label_Printer_Type
                                          of 'TEC B-440 / B-442'
                                              stock_request_label(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)
                                          of 'TEC B-452'
                                              stock_request_label_B452(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)                                    
                                      End !Case def:Label_Printer_Type
                                  End !If def:Add_Stock_Label = 'YES'
  
                                  If Access:STOHIST.PrimeRecord() = Level:Benign
                                      shi:Ref_Number          = sto:Ref_Number
                                      shi:Transaction_Type    = 'DEC'
                                      shi:Despatch_Note_Number    = par:Despatch_Note_Number
                                      shi:Quantity            = par:Quantity
                                      shi:Date                = Today()
                                      shi:Purchase_Cost       = par:Purchase_Cost
                                      shi:Sale_Cost           = par:Sale_Cost
                                      shi:Retail_Cost         = par:Retail_Cost
                                      shi:Job_Number          = job:Ref_Number
                                      Access:USERS.Clearkey(use:Password_Key)
                                      use:Password    = glo:Password
                                      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Found
  
                                      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      shi:user                = use:User_Code
                                      shi:Notes               = 'STOCK DECREMENTED'
                                      
                                      If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:STOHIST.TryInsert() = Level:Benign
                                  End !If Access:STOHIST.PrimeRecord() = Level:Benign
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      End !If par:Quantity > sto:Quantity
                  Else !If sto:Sundry_Item <> 'YES'
                      !Sundry Item, do nothing
                      par:Date_Ordered    = Today()
                  End !If sto:Sundry_Item <> 'YES'
              Else !If tmp:StockNumber
                  !A non stock part
                  Beep(Beep:SystemQuestion)  ;  Yield()
                  Case Message('This is a NON-STOCK item.'&|
                          '||Do you wish it to appear on the next parts order?', |
                          'ServiceBase 2000', Icon:Question, |
                           Button:Yes+Button:No, Button:No, 0)
                  Of Button:Yes
                      If Access:ORDPEND.PrimeRecord() = Level:Benign
                          ope:Job_Number      = job:Ref_Number
                          ope:Part_Type       = 'JOB'
                          ope:Supplier        = par:Supplier
                          ope:Part_Number     = par:Part_Number
                          ope:Description     = par:Description
                          ope:Quantity        = par:Quantity
                          ope:PartRecordNumber    = par:Record_Number
                          par:Pending_Ref_Number  = ope:Ref_Number
                          If Access:ORDPEND.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:ORDPEND.TryInsert() = Level:Benign
                              !Insert Failed
                          End !If Access:ORDPEND.TryInsert() = Level:Benign
                      End !If Access:ORDPEND.PrimeRecord() = Level:Benign
                      
                  Of Button:No
                      par:Exclude_From_Order  = 'YES'
                      par:Date_Ordered    = Today()
                  End !CASE
                  
              End !If tmp:StockNumber
          Else !If par:Exclude_From_Order <> 'YES'        
              !excluded, do nothing
              par:Date_Ordered = Today()
          End !If par:Exclude_From_Order <> 'YES'
          ! Start Change 2116 BE(26/06/03)
          ! Start Change 4290 BE(18/05/04)
          !IF (sto:RF_BOARD) THEN
          IF (NOT ?tmp_RF_Board_IMEI {PROP:HIDE}) THEN
          ! End Change 4290 BE(18/05/04)
              GET(audit,0)
              IF (access:audit.primerecord() = level:benign) THEN
                  aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                      '<13,10>Old IMEI ' & Clip(job:esn)
                  aud:ref_number    = job:ref_number
                  aud:date          = Today()
                  aud:time          = Clock()
                  access:users.clearkey(use:password_key)
                  use:password =glo:password
                  access:users.fetch(use:password_key)
                  aud:user = use:user_code
                  aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                  if access:audit.insert()
                      access:audit.cancelautoinc()     
                  end            
              END
              access:JOBSE.clearkey(jobe:RefNumberKey)
              jobe:RefNumber  = job:Ref_Number
              IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                  IF (access:JOBSE.primerecord() = Level:Benign) THEN
                      jobe:RefNumber  = job:Ref_Number
                      IF (access:JOBSE.tryinsert()) THEN
                          access:JOBSE.cancelautoinc()
                      END
                  END
              END
              IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                  jobe:PRE_RF_BOARD_IMEI = job:ESN
                  Access:jobse.update()
              END
              job:ESN = tmp_RF_Board_IMEI
              Access:jobs.update()
          END
          ! End Change 2116 BE(26/06/03)
      Else !par:Part_Number <> 'ADJUSTMENT'
          !An ajustment, do nothing
          par:Date_Ordered    = Today()
      End !par:Part_Number <> 'ADJUSTMENT'
  End !If thiswindow.Request = InsertRecord
!    If ThisWindow.Request = insertrecord
!        Set(DEFAULTS)
!        Access:DEFAULTS.Next()
!        found# = 0
!    !Is this part number already on the job?
!        If par:part_number <> 'ADJUSTMENT'
!
!
!            setcursor(cursor:wait)
!            save_par_ali_id = access:parts_alias.savefile()
!            access:parts_alias.clearkey(par_ali:part_number_key)
!            par_ali:ref_number  = job:ref_number
!            par_ali:part_number = par:part_number
!            set(par_ali:part_number_key,par_ali:part_number_key)
!            loop
!                if access:parts_alias.next()
!                   break
!                end !if
!                if par_ali:ref_number  <> job:ref_number      |
!                or par_ali:part_number <> par:part_number      |
!                    then break.  ! end if
!                If par_ali:record_number <> par:record_number
!                    If par_ali:date_received = ''
!                        found# = 1
!                        Break
!                    End!If par_ali:date_received <> ''
!                End!If par_ali:record_number <> par:record_number
!            end !loop
!            access:parts_alias.restorefile(save_par_ali_id)
!            setcursor()
!            If found# = 1
!                Case MessageEx('This part has already been attached to this job.','ServiceBase 2000',|
!                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                    Of 1 ! &OK Button
!                End!Case MessageEx
!                Select(?par:part_number)
!                Cycle
!            End !If found# = 1
!        End!If par:part_number <> 'ADJUSTMENT'
!        If par:exclude_from_order <> 'YES'
!            If par:part_ref_number <> ''                                                        !Using A STock Item
!                access:stock.clearkey(sto:ref_number_key)
!                sto:ref_number = par:part_ref_number
!                if access:stock.fetch(sto:ref_number_key)
!                   beep(beep:systemhand)  ;  yield()
!                    message('An Error has occured retrieving the details of this Stock Part.', |
!                            'ServiceBase 2000', icon:hand)
!                Else!if access:stock.fetch(sto:ref_number_key)
!                    If sto:sundry_item <> 'YES'
!                    
!                        If par:quantity > sto:quantity_stock
!                !Back Order
!                            Case MessageEx('There are insufficient items in stock!'&|
!                              '<13,10>'&|
!                              '<13,10>Do you wish to Order the remaining items, or re-enter the quantity required?','ServiceBase 2000',|
!                                           'Styles\warn.ico','|&Order Part|&Re-enter Quantity|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
!                                Of 1 ! &Order Part Button
!                                    If SecurityCheck('JOBS - ORDER PARTS')
!                                        Case MessageEx('You do not have access to Order Parts.','ServiceBase 2000',|
!                                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                            Of 1 ! &OK Button
!                                        End!Case MessageEx
!                                        Select(?par:quantity)
!                                        Cycle
!
!                                    Else !If SecurityCheck('JOBS - ORDER PARTS')
!
!                                    
!                                        tmp:CreateNewOrder = 0
!                                        Case def:SummaryOrders
!                                            Of 0 !Old Way
!                                                tmp:CreateNewOrder = 1
!                                            Of 1 !New Way
!                                                Access:ORDPEND.Clearkey(ope:Supplier_Key)
!                                                ope:Supplier    = par:Supplier
!                                                ope:Part_Number = par:Part_Number
!                                                If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
!                                                    ope:Quantity    += par:Quantity
!                                                    Access:ORDPEND.Update()
!                                                    tmp:CreateNewOrder = 0
!                                                    If sto:Quantity_Stock > 0
!                                                        glo:Select1     = 'NEW PENDING'
!                                                        glo:Select2     = par:Part_Ref_Number
!                                                        glo:Select3     = par:Quantity - sto:Quantity_Stock
!                                                        glo:Select4     = ope:Ref_Number
!                                                        par:Quantity    = sto:Quantity_Stock
!                                                    Else !If sto:Quantity_Stock <= 0
!                                                        par:Pending_Ref_Number  = ope:Ref_Number
!                                                        par:Requested   = True
!                                                    End !If sto:Quantity_Stock <= 0
!                                                Else !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
!                                                    tmp:CreateNewOrder  = 1
!                                                    par:Requested = True
!                                                End !If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
!                                        End !Case def:SummaryOrders
!                                        IF tmp:CreateNewOrder = 1
!                                            get(ordpend,0)
!                                            if access:ordpend.primerecord() = level:benign
!                                                ope:part_ref_number = sto:ref_number
!                                                ope:job_number      = job:ref_number
!                                                ope:part_type       = 'JOB'
!                                                ope:supplier        = par:supplier
!                                                ope:part_number     = par:part_number
!                                                ope:description = par:description
!                                                If sto:quantity_stock <= 0
!                                                    par:pending_ref_number = ope:ref_number
!                                                    sto:quantity_to_order   += par:quantity
!                                                    access:stock.update()
!                                                    ope:quantity    = par:quantity
!                                                Else!If sto:quantity_stock <= 0
!                                                    glo:select1      = 'NEW PENDING'
!                                                    glo:select2      = par:part_ref_number
!                                                    glo:select3      = par:quantity - sto:quantity_stock
!                                                    glo:select4      = ope:ref_number
!                                                    ope:quantity        = par:quantity - sto:quantity_stock
!                                                    par:quantity        = sto:quantity_stock
!                                                End!If sto:quantity_stock <= 0
!                                                if access:ordpend.insert()
!                                                    access:ordpend.cancelautoinc()
!                                                end
!                                            end!if access:ordpend.primerecord() = level:benign
!
!                                        End !IF tmp:CreateNewOrder = 1
!                                    End !If SecurityCheck('JOBS - ORDER PARTS')
!                                Of 2 ! &Re-enter Quantity Button
!                                    Select(?par:quantity)
!                                    Cycle
!                                Of 3 ! &Cancel Button
!                                    Select(?par:part_number)
!                                    Cycle
!                            end !case
!                        Else !If par:quantity > sto:quantity_stock                              !This is sufficient in stock
!                            par:date_ordered = Today()
!                            sto:quantity_stock -= par:quantity
!                            If sto:quantity_stock < 0
!                                sto:quantity_stock = 0
!                            End
!                            Access:stock.update()
!                            set(defaults)
!                            access:defaults.next()
!                            If def:add_stock_label = 'YES'
!                                glo:select1  = sto:ref_number
!                                case def:label_printer_type
!                                    of 'TEC B-440 / B-442'
!                                        stock_request_label(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)
!                                    of 'TEC B-452'
!                                        stock_request_label_B452(Today(),Clock(),par:quantity,job:ref_number,job:engineer,par:sale_cost)
!                                end!case def:label_printer_type
!                                glo:select1 = ''
!                            End!If def:add_stock_label = 'YES'
!                            get(stohist,0)
!                            if access:stohist.primerecord() = level:benign
!                                shi:ref_number           = sto:ref_number
!                                shi:transaction_type     = 'DEC'
!                                shi:despatch_note_number = par:despatch_note_number
!                                shi:quantity             = par:quantity
!                                shi:date                 = Today()
!                                shi:purchase_cost        = par:purchase_cost
!                                shi:sale_cost            = par:sale_cost
!                                shi:retail_cost          = par:retail_cost
!                                shi:job_number           = job:ref_number
!                                access:users.clearkey(use:password_key)
!                                use:password =glo:password
!                                access:users.fetch(use:password_key)
!                                shi:user                 = use:user_code
!                                shi:notes                = 'STOCK DECREMENTED'
!                                shi:information          = ''
!                                if access:stohist.insert()
!                                    access:stohist.cancelautoinc()
!                                end
!                            end!if access:stohist.primerecord() = level:benign
!                        End !If par:quantity > sto:quantity_stock
!                    Else!!!If sto:sundry_item = 'YES'                                           !Using A Sundry Stock Item
!                        par:date_ordered = Today()                                              !Not need to order or decrement !
!                    End!If sto:sundry_item = 'YES'                                              
!                end!if access:stock.fetch(sto:ref_number_key)
!            Else !If par:part_ref_number <> ''                                                  !A Non Stock Item
!                Case MessageEx('This is a non-stock item. Do you wish it appear on the next parts order?','ServiceBase 2000',|
!                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!                    Of 1 ! &Yes Button
!                        get(ordpend,0)
!                        if access:ordpend.primerecord()= level:benign
!                            ope:part_ref_number = ''
!                            ope:job_number      = job:ref_number
!                            ope:part_type       = 'JOB'
!                            ope:supplier        = par:supplier
!                            ope:part_number     = par:part_number
!                            ope:description     = par:description
!                            ope:quantity        = par:quantity
!                            if access:ordpend.insert()
!                                access:ordpend.cancelautoinc()
!                            end
!                        end!if access:ordpend.primerecord()= level:benign
!                        par:pending_ref_number = ope:ref_number
!
!                    Of 2 ! &No Button
!                        par:exclude_from_order = 'YES'
!                        par:date_ordered = Today()
!                End!Case MessageEx
!            End !If par:part_ref_number <> ''
!        Else!End !If par:exclude_from_order <> 'YES'
!            par:date_ordered = Today()
!        End !If par:exclude_from_order <> 'YES'
!    End !If localrequest = insertrecord
!
  !!If Thiswindow.request <> Insertrecord
  !    check_parts# = 0
  !    access:stock.clearkey(sto:ref_number_key)
  !    sto:ref_number = par:part_ref_number
  !    if access:stock.fetch(sto:ref_number_key) = Level:benign
  !        If sto:sundry_item <> 'YES'
  !            check_parts# = 1
  !        End!If sto:sundry_item <> 'YES'
  !
  !        If check_parts# = 1
  !            If TMP:Part_Number <> par:part_number Or |
  !              TMP:Description <> par:description Or |
  !              TMP:Supplier <> par:supplier Or |
  !              TMP:Purchase_Cost <> par:purchase_cost Or |
  !              TMP:Sale_Cost <> par:sale_cost Or |
  !              quantity_temp <> par:quantity
  !            
  !                !Does a pending part exist. If so, you can't change this part.
  !                If par:pending_ref_number = ''
  !                    setcursor(cursor:wait)
  !                    save_par_ali_id = access:parts_alias.savefile()
  !                    access:parts_alias.clearkey(par_ali:part_number_key)
  !                    par_ali:ref_number  = job:ref_number
  !                    par_ali:part_number = tmp:part_number
  !                    set(par_ali:part_number_key,par_ali:part_number_key)
  !                    loop
  !                        if access:parts_alias.next()
  !                           break
  !                        end !if
  !                        if par_ali:ref_number  <> job:ref_number      |
  !                        or par_ali:part_number <> tmp:part_number      |
  !                            then break.  ! end if
  !                        If par_ali:record_number <> par:record_number
  !                            If par_ali:date_received = ''
  !                                found# = 1
  !                                Break
  !                            End!If par_ali:date_received = ''
  !                        End!If par_ali:record_number <> par:record_number
  !                    end !loop
  !                    access:parts_alias.restorefile(save_par_ali_id)
  !                    setcursor()
  !
  !                    If found# = 1
  !                        Case MessageEx('This Part is awaiting Order Generation.'&|
  !                          '<13,10>'&|
  !                          '<13,10>While a ''Pending'' part exists, you cannot amend this part','ServiceBase 2000',|
  !                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                            Of 1 ! &OK Button
  !                        End!Case MessageEx
  !
  !                        par:part_number     = tmp:part_number
  !                        par:description     = tmp:description
  !                        par:supplier        = tmp:supplier
  !                        par:purchase_cost   = tmp:purchase_cost
  !                        par:sale_cost       = tmp:sale_cost
  !                        par:quantity    = quantity_temp
  !                        Display()
  !                        Select(?par:part_number)
  !                        Cycle
  !                    Else!If found# = 1
  !                        If par:quantity > quantity_temp
  !                            If par:quantity - quantity_temp > sto:quantity_stock
  !                                Case MessageEx('There are insufficient items in stock.'&|
  !                                  '<13,10>'&|
  !                                  '<13,10>Do you wish to Order the remaining items, or re-enter the quantity required?','ServiceBase 2000',|
  !                                               'Styles\question.ico','|&Order Part|&Re-enter Quantity|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
  !                                    Of 1 ! &Order Part Button
  !                                        If SecurityCheck('JOBS - ORDER PARTS')
  !                                            Case MessageEx('You do not have access to Order Parts.','ServiceBase 2000',|
  !                                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                                                Of 1 ! &OK Button
  !                                            End!Case MessageEx
  !                                            Select(?par:Quantity)
  !                                            Cycle
  !                                        Else !If SecurityCheck('JOBS - ORDER PARTS')
  !                                            get(ordpend,0)
  !                                            if access:ordpend.primerecord() = level:benign
  !                                                ope:part_ref_number = sto:ref_number
  !                                                ope:job_number      = job:ref_number
  !                                                ope:part_type       = 'JOB'
  !                                                ope:supplier        = par:supplier
  !                                                ope:part_number     = par:part_number
  !                                                ope:description     = par:description
  !                                                ope:quantity        = par:quantity - quantity_temp
  !                                                if access:ordpend.insert()
  !                                                    access:ordpend.cancelautoinc()
  !                                                end
  !                                            end!if access:ordpend.primerecord() = level:benign
  !                                            glo:select1      = 'NEW PENDING'
  !                                            glo:select2      = par:part_ref_number
  !                                            glo:select3      = (par:quantity - quantity_temp) - sto:quantity_stock
  !                                            glo:select4      = ope:ref_number
  !                                            par:quantity     = quantity_temp + sto:quantity_stock
  !                                        End !If SecurityCheck('JOBS - ORDER PARTS')
  !                                    Of 2 ! &Re-enter Quantity Button
  !                                        Select(?par:quantity)
  !                                        Cycle
  !                                    Of 3 ! &Cancel Button
  !                                        Select(?par:part_number)
  !                                        Cycle
  !                                End!Case MessageEx
  !                            Else !If par:quantity - quantity_temp > sto:quantity_stock
  !                                sto:quantity_stock -= (par:quantity - quantity_temp)
  !                                If sto:quantity_stock < 0
  !                                    sto:quantity_stock = 0
  !                                End
  !                                Access:stock.Update()
  !                                get(stohist,0)
  !                                if access:stohist.primerecord() = level:benign
  !                                    shi:ref_number           = sto:ref_number
  !                                    shi:transaction_type     = 'DEC'
  !                                    shi:despatch_note_number = par:despatch_note_number
  !                                    shi:quantity             = par:quantity - quantity_temp
  !                                    shi:date                 = today()
  !                                    shi:purchase_cost        = par:purchase_cost
  !                                    shi:sale_cost            = par:sale_cost
  !                                    shi:retail_cost          = par:retail_cost
  !                                    shi:job_number           = job:ref_number
  !                                    access:users.clearkey(use:password_key)
  !                                    use:password =glo:password
  !                                    access:users.fetch(use:password_key)
  !                                    shi:user                 = use:user_code
  !                                    shi:notes                = 'STOCK DECREMENTED'
  !                                    shi:information          = ''
  !                                    if access:stohist.insert()
  !                                        access:stohist.cancelautoinc()
  !                                    end
  !                                end!if access:stohist.primerecord() = level:benign
  !                            End !If par:quantity - quantity_temp > sto:quantity_stock
  !                        Else !If par:quantity > quantity_temp
  !                            !If quantity is lowered
  !                            sto:quantity_stock += quantity_temp - par:quantity
  !                            Access:stock.Update()
  !                            get(stohist,0)
  !                            if access:stohist.primerecord() = level:benign
  !                                shi:ref_number           = sto:ref_number
  !                                shi:transaction_type     = 'REC'
  !                                shi:despatch_note_number = par:despatch_note_number
  !                                shi:quantity             = quantity_temp - par:quantity
  !                                shi:date                 = today()
  !                                shi:purchase_cost        = par:purchase_cost
  !                                shi:sale_cost            = par:sale_cost
  !                                shi:retail_cost          = par:retail_cost
  !                                shi:job_number           = job:ref_number
  !                                access:users.clearkey(use:password_key)
  !                                use:password =glo:password
  !                                access:users.fetch(use:password_key)
  !                                shi:user              = use:user_code
  !                                shi:notes                = 'STOCK RECREDITED'
  !                                shi:information          = ''
  !                                if access:stohist.insert()
  !                                    access:stohist.cancelautoinc()
  !                                end
  !                            End!if access:stohist.primerecord() = level:benign
  !                        End !If par:quantity > quantity_temp
  !                    End!If found# = 1
  !                Else!If par:pending_ref_number = ''
  !                    If TMP:Part_Number <> par:part_number Or |
  !                      TMP:Description <> par:description Or |
  !                      TMP:Supplier <> par:supplier Or |
  !                      TMP:Purchase_Cost <> par:purchase_cost Or |
  !                      TMP:Sale_Cost <> par:sale_cost
  !
  !                        Case MessageEx('Cannot amend. This part is awaiting Order Generation.','ServiceBase 2000',|
  !                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                            Of 1 ! &OK Button
  !                        End!Case MessageEx
  !                        par:part_number     = tmp:part_number
  !                        par:description     = tmp:description
  !                        par:supplier        = tmp:supplier
  !                        par:purchase_cost   = tmp:purchase_cost
  !                        par:sale_cost       = tmp:sale_cost
  !                        par:quantity    = quantity_temp
  !                        Display()
  !                        Select(?par:part_number)
  !                        Cycle
  !                    Else !!If part_details_group <> par:part_details_group
  !                        !As long as the part details haven't changed
  !                        !Amend the pending order
  !
  !                        access:ordpend.clearkey(ope:ref_number_key)
  !                        ope:ref_number = job:ref_number
  !                        if access:ordpend.fetch(ope:ref_number_key) = Level:Benign
  !                            If par:Quantity < Quantity_Temp
  !                                ope:Quantity -= (Quantity_temp - par:Quantity)
  !                            End!If par:Quantity < Quantity_Temp
  !                            If par:Quantity > Quantity_Temp
  !                                ope:Quantity += (par:Quantity - Quantity_Temp)
  !                            End!If par:Quantity > Quantity_Temp
  !
  !                            Access:ordpend.Update()
  !                        end !if
  !
  !                    End!If part_details_group <> par:part_details_group
  !                End!If par:pending_ref_number = ''
  !            End!If part_details_group <> par:part_details_group
  !        End!If check_parts# = 1
  !    End!if access:stock.fetch(sto:ref_number_key) = Level:benign
  !End!If Thiswindow.request <> Insertrecord
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If thiswindow.Request <> Insertrecord
      !Is this a part from stock?
      If par:Part_Ref_Number <> ''
          Access:STOCK.Clearkey(sto:Ref_Number_Key)
          sto:Ref_Number  = par:Part_Ref_Number
          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Found
              tmp:StockNumber = sto:Ref_Number
              
          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          
      End !If par:Part_Ref_Number <> ''
      !Normal part
      If par:Date_Ordered <> '' And par:Order_Number = '' and par:Pending_Ref_Number = ''
          If par:Quantity > Quantity_Temp
              !Stock part?
              If tmp:StockNumber
  
                  ! Start Change 1895 BE(15/05/03)
                  DO InitMaxParts
                  IF (tmpCheckChargeableParts) THEN
                      DO TotalChargeableParts
                      TotalPartsCost += par:Purchase_cost * par:Quantity
                      IF (TotalPartsCost > tra:MaxChargeablePartsCost) THEN
                          MESSAGE('This part cannot be attached to the job.<13,10>' & |
                                  'This would exceed the Maximum Chargeable Parts Cost of the Trade Account.', |
                                  'ServiceBase 2000', Icon:Question, 'OK', 1, 0)
                          Select(?par:Quantity)
                          Cycle
                      END
                  END
                  ! End Change 1895 BE(15/05/03)
  
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = tmp:StockNumber
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
                      If sto:Quantity_Stock < (par:Quantity - Quantity_Temp)
                          !There isn't enough in stock
  
                          Beep(Beep:SystemQuestion)  ;  Yield()
                          Case Message('You have increased the quantity required, but there are '&|
                                  'insufficient items in stock.'&|
                                  '||Do you wish to ORDER the remaining items, or RE-ENTER the '&|
                                  'quantity required?', |
                                  'ServiceBase 2000', Icon:Question, |
                                   'Order|Re-enter', 2, 0)
                          Of 1  ! Name: Order
                              !Is there any existing pending part to add to?
                              tmp:PendingPart = 0
                              Save_par_ali_ID = Access:PARTS_ALIAS.SaveFile()
                              Access:PARTS_ALIAS.ClearKey(par_ali:RefPartRefNoKey)
                              par_ali:Ref_Number      = job:Ref_Number
                              par_ali:Part_Ref_Number = tmp:StockNumber
                              Set(par_ali:RefPartRefNoKey,par_ali:RefPartRefNoKey)
                              Loop
                                  If Access:PARTS_ALIAS.NEXT()
                                     Break
                                  End !If
                                  If par_ali:Ref_Number      <> job:Ref_Number      |
                                  Or par_ali:Part_Ref_Number <> tmp:StockNumber      |
                                      Then Break.  ! End If
                                  If par_ali:Record_Number = par:Record_Number
                                      Cycle
                                  End !If par_ali:Record_Number = par:Record_Number
                                  If par_ali:Pending_Ref_Number <> ''
                                      tmp:PendingPart = par_ali:Record_Number
                                      Break
                                  End !If par:Pending_Ref_Number <> ''
                              End !Loop
                              Access:PARTS_ALIAS.RestoreFile(Save_par_ali_ID)                        
  
                              tmp:NewQuantity     = (par:Quantity - Quantity_Temp) - sto:Quantity_Stock
  
                              If tmp:PendingPart
                                  tmp:CurrentState    = Getstate(PARTS)
                                  
                                  Access:PARTS.Clearkey(par:RecordNumberKey)
                                  par:Record_Number   = tmp:PendingPart
                                  If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                                      !Found
                                      par:Quantity += tmp:NewQuantity
                                      Access:PARTS.Update()
  
                                      tmp:CreateNewOrder = 0
                                      !Find pending order
                                      Case def:SummaryOrders    
                                          Of 0 !Old Wauy
                                              Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                                              ope:Ref_Number  = par:Pending_Ref_Number
                                              If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                  !Found
                                                  ope:Quantity += tmp:NewQuantity
                                                  Access:ORDPEND.Update()
  
                                              Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                                  !Error
                                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                                  Do CreateNewOrder
                                              End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                                          Of 1 !New Way
                                              Access:ORDPEND.ClearKey(ope:Supplier_Key)
                                              ope:Supplier    = par:Supplier
                                              ope:Part_Number = par:Part_Number
                                              If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                                  ope:Quantity += tmp:NewQuantity
                                                  Access:ORDPEND.Update()
                                              Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Lev !Found
                                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                                  Do CreateNewOrder
  
                                              End !If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                                      End !Case def:SummaryOrders    
  
                                      RestoreState(PARTS,tmp:CurrentState)
                                      FreeState(PARTS,tmp:CurrentState)
                                  Else! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                                  
                              Else !If tmp:PendingPart
                                  Do CreateNewOrder
  
                                  !Create new pending part
                                  glo:Select1 = 'NEW PENDING'
                                  glo:Select2 = par:Part_Ref_Number
                                  glo:Select3 = (par:Quantity - Quantity_Temp) - sto:Quantity_Stock
                                  glo:Select4 = ope:Ref_Number
                                  par:Quantity    = Quantity_Temp + sto:Quantity_Stock
                                  
                              End !If tmp:PendingPart
                          Of 2  ! Name: Re-enter  (Default)
                              par:Quantity    = Quantity_Temp
                              Select(?par:Quantity)
                              Cycle
                          End !CASE
  
                      End !If sto:Quantity_Stock < (par:Quantity - Quantity_Temp)
  
                      !There is enough in stock, take it.
                      If sto:Quantity_Stock > (par:Quantity - Quantity_Temp)
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              sto:Quantity_Stock -= (par:Quantity - Quantity_Temp)
                              If Access:STOCK.Update() = Level:Benign
                                  If Access:STOHIST.PrimeRecord() = Level:Benign
                                      shi:Ref_Number          = sto:Ref_Number
                                      shi:Transaction_Type    = 'DEC'
                                      shi:Despatch_Note_Number    = par:Despatch_Note_NUmber
                                      shi:Quantity            = par:Quantity - Quantity_Temp
                                      shi:Date                = Today()
                                      shi:Purchase_Cost       = par:Purchase_Cost
                                      shi:Sale_Cost           = par:Sale_Cost
                                      shi:Retail_Cost         = par:Retail_Cost
                                      shi:Job_Number          = job:Ref_Number
                                      Access:USERS.Clearkey(use:Password_Key)
                                      use:Password    = glo:Password
                                      If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Found
                                      Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
                                      shi:User                = use:User_Code
                                      shi:Notes               = 'STOCK DECREMENTED'
                                      If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Successful
                                      Else !If Access:STOHIST.TryInsert() = Level:Benign
                                          !Insert Failed
                                      End !If Access:STOHIST.TryInsert() = Level:Benign
                                  End !If Access:STOHIST.PrimeRecord() = Level:Benign
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          
                      End !If sto:Quantity_Stock > (par:Quantity - Quantity_Temp)
                  Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  
              End !If tmp:StockNumber
          End !If quantity increased
          !Quantity decreased
          If Quantity_Temp > par:Quantity
              ! Start Change 2466 BE(28/11/03)
              !If tmp:StockNumber
              IF ((tmp:StockNumber) AND (par:Exclude_From_Order <> 'YES')) THEN
              ! End Change 2466 BE(28/11/03)
                  !A Stock part. Put back into stock?
                  Beep(Beep:SystemQuestion)  ;  Yield()
                  ! Start 2632 BE(20/05/03)
                  !Case Message('You have decreased the quantity required. '&|
                  !        '||What do you wish to do with the excess? RETURN it to stock, '&|
                  !        'or SCRAP it?', |
                  !        'ServiceBase 2000', Icon:Question, |
                  !         'Return|Scrap|Cancel', 3, 0)
                  msg1 = 'You have decreased the quantity required. '
                  msg2 = 'What do you wish to do with the excess? RETURN it to stock, or SCRAP it?'
                  ScrapDialog(msg1, msg2, 'Return', 'Scrap', 'Cancel', result)
                  CASE result
                  ! End 2632 BE(20/05/03)
                  Of 1  ! Name: Return
                      Beep(Beep:SystemQuestion)  ;  Yield()
                      Case Message('This item was originally taked from location: '&|
                              Clip(sto:Location)&'.'&|
                              '||Do you wish to return it to it''s ORIGINAL location, or '&|
                              'to a NEW location?', |
                              'ServiceBase 2000', Icon:Question, |
                               'Original|New|Cancel', 3, 0)
                      Of 1  ! Name: Original
                          Access:STOCK.Clearkey(sto:Ref_Number_Key)
                          sto:Ref_Number  = tmp:StockNumber
                          If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Found
                              sto:Quantity_Stock += Quantity_Temp - par:Quantity
                              If Access:STOCK.Update() = Level:Benign
                                  If access:stock.update() = Level:Benign
                                      get(stohist,0)
                                      if access:stohist.primerecord() = level:benign
                                          shi:ref_number           = sto:ref_number
                                          access:users.clearkey(use:password_key)
                                          use:password              = glo:password
                                          access:users.fetch(use:password_key)
                                          shi:user                  = use:user_code    
                                          shi:date                 = today()
                                          shi:transaction_type     = 'REC'
                                          shi:despatch_note_number = par:despatch_note_number
                                          shi:job_number           = job:Ref_number
                                          shi:quantity             = Quantity_Temp - par:Quantity
                                          shi:purchase_cost        = par:purchase_cost
                                          shi:sale_cost            = par:sale_cost
                                          shi:retail_cost          = par:retail_cost
                                          shi:information          = 'CHARGEABLE PART AMENDED ON JOB'
                                          if access:stohist.insert()
                                             access:stohist.cancelautoinc()
                                          end
                                      end!if access:stohist.primerecord() = level:benign
                                  End!If access:stock.update = Level:Benign
                              End !If Access:STOCK.Update() = Level:Benign
                          Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                          
                      Of 2  ! Name: New
                          glo:select1 = ''
                          Pick_New_Location
                          If glo:select1 <> ''
                              access:stock.clearkey(sto:location_part_description_key)
                              sto:location    = glo:select1
                              sto:part_number = par:part_number
                              sto:description = par:description
                              If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  sto:quantity_stock   += Quantity_Temp - par:Quantity
                                  access:stock.update()
                                  do_delete# = 1
                                  get(stohist,0)
                                  if access:stohist.primerecord() = level:benign
                                      shi:ref_number           = sto:ref_number
                                      access:users.clearkey(use:password_key)
                                      use:password              = glo:password
                                      access:users.fetch(use:password_key)
                                      shi:user                  = use:user_code    
                                      shi:date                 = today()
                                      shi:transaction_type     = 'ADD'
                                      shi:despatch_note_number = par:despatch_note_number
                                      shi:job_number           = job:Ref_number
                                      shi:quantity             = Quantity_Temp - par:Quantity
                                      shi:purchase_cost        = par:purchase_cost
                                      shi:sale_cost            = par:sale_cost
                                      shi:retail_cost          = par:retail_cost
                                      shi:information          = 'CHARGEABLE PART AMENDED ON JOB'
                                      if access:stohist.insert()
                                         access:stohist.cancelautoinc()
                                      end
                                  end!if access:stohist.primerecord() = level:benign
                              Else!If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                                  Beep(Beep:SystemQuestion)  ;  Yield()
                                  ! Start 2632 BE(20/05/03)
                                  !Case Message('Cannot find the selected part in location: '&|
                                  !        Clip(glo:Select1)&'.'&|
                                  !        '||Do you wish add this part as a NEW item in that location '&|
                                  !        'or SCRAP it?', |
                                  !        'ServiceBase 2000', Icon:Question, |
                                  !         'New|Scrap|Cancel', 3, 0)
                                  msg1 = 'Cannot find the selected part in location: ' & Clip(glo:Select1) & '.'
                                  msg2 = 'Do you wish add this part as a NEW item in that location or SCRAP it?'
                                  ScrapDialog(msg1, msg2, 'New', 'Scrap', 'Cancel', result)
                                  CASE result
                                  ! End 2632 BE(20/05/03)
                                  Of 1  ! Name: New
                                      glo:select2  = ''
                                      glo:select3  = ''
                                      Pick_Locations
                                      If glo:select1 <> ''
                                          do_delete# = 1
                                          Get(stock,0)
                                          If access:stock.primerecord() = Level:Benign
                                              sto:part_number = par:part_number
                                              sto:description = par:description
                                              sto:supplier    = par:supplier
                                              sto:purchase_cost   = par:purchase_cost
                                              sto:sale_cost   = par:sale_cost
                                              sto:shelf_location  = glo:select2
                                              sto:manufacturer    = job:manufacturer
                                              sto:location    = glo:select1
                                              sto:second_location = glo:select3
                                              If access:stock.insert() = Level:Benign
                                                  get(stohist,0)
                                                  if access:stohist.primerecord() = level:benign
                                                      shi:ref_number           = sto:ref_number
                                                      access:users.clearkey(use:password_key)
                                                      use:password              = glo:password
                                                      access:users.fetch(use:password_key)
                                                      shi:user                  = use:user_code    
                                                      shi:date                 = today()
                                                      shi:transaction_type     = 'ADD'
                                                      shi:despatch_note_number = par:despatch_note_number
                                                      shi:job_number           = job:Ref_number
                                                      shi:quantity             = Quantity_Temp - par:Quantity
                                                      shi:purchase_cost        = par:purchase_cost
                                                      shi:sale_cost            = par:sale_cost
                                                      shi:retail_cost          = par:retail_cost
                                                      shi:information          = 'CHARGEABLE PART AMENDED ON JOB'
                                                      if access:stohist.insert()
                                                         access:stohist.cancelautoinc()
                                                      end
                                                  end!if access:stohist.primerecord() = level:benign
                                              End!If access:stock.insert() = Level:Benign
                                          End!If access:stock.primerecord() = Level:Benign
                                      End!If glo:select1 = ''
                                  Of 2  ! Name: Scrap
  
                                      ! start 2632 BE(20/05/03)
                                      DO WriteScrapAuditRecord
                                      ! start 2632 BE(20/05/03)
  
                                  Of 3  ! Name: Cancel  (Default)
                                      ! Start Change 2600 BE(08/05/03)
                                      par:Quantity = Quantity_Temp
                                      Select(?par:Quantity)
                                      Cycle
                                      ! End Change 2600 BE(08/05/03)
                                  End !CASE
                              End !If access:stock.tryfetch(sto:location_part_description_key) = Level:Benign
                          End !If glo:select1 <> ''
                      Of 3  ! Name: Cancel  (Default)
                          ! Start Change 2600 BE(08/05/03)
                          par:Quantity = Quantity_Temp
                          Select(?par:Quantity)
                          Cycle
                          ! End Change 2600 BE(08/05/03)
                      End !CASE
                  Of 2  ! Name: Scrap
  
                      ! start 2632 BE(20/05/03)
                      DO WriteScrapAuditRecord
                      ! start 2632 BE(20/05/03)
  
                  Of 3  ! Name: Cancel  (Default)
                      ! Start Change 2600 BE(08/05/03)
                      par:Quantity = Quantity_Temp
                      Select(?par:Quantity)
                      Cycle
                      ! End Change 2600 BE(08/05/03)
                  End !CASE
              Else !If tmp:StockNumber
  
              End !If tmp:StockNumber
          End !If Quantity_Temp > par:Quantity
      End !If par:Date_Ordered <> '' And par:Order_Number = '' and par:Pending_Ref_Number = ''    
  
      !This is a pending part
      If par:Pending_Ref_Number <> '' and par:Order_Number = ''
          !Quantity increased
          If par:Quantity > Quantity_Temp
              !Find a pending order
              Case def:SummaryOrders
                  Of 0 !Old Way
                      Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                      ope:Ref_Number  = par:Pending_Ref_Number
                      If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Found
  
                          ope:Quantity += par:Quantity - Quantity_Temp
                          Access:ORDPEND.Update()
                      Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          !Cannot find a pending order, lets' make one
                          tmp:NewQuantity = par:Quantity - Quantity_Temp
                          Do CreateNewOrder
                          !Create new pending part
                          glo:Select1 = 'NEW PENDING'
                          glo:Select2 = par:Part_Ref_Number
                          glo:Select3 = (par:Quantity - Quantity_Temp)
                          glo:Select4 = ope:Ref_Number
                          par:Quantity   = Quantity_Temp 
  
                      End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                      
                  Of 1 !New Way
                      Access:ORDPEND.ClearKey(ope:Supplier_Key)
                      ope:Supplier    = par:Supplier
                      ope:Part_Number = par:Part_Number
                      If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                          !Found
                          ope:Quantity += par:Quantity - Quantity_Temp
                          Access:ORDPEND.Update()
                      Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          tmp:NewQuantity = par:Quantity - Quantity_Temp
                          Do CreateNewOrder
                      End!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
              End !Case def:SummaryOrders
          End !If par:Quantity > Quantity_Temp
          !Quantity decreased
          If par:Quantity < Quantity_Temp
              !Find a pending order
              Case def:SummaryOrders
                  Of 0 !Old Way
                      Access:ORDPEND.Clearkey(ope:Ref_Number_Key)
                      ope:Ref_Number  = par:Pending_Ref_Number
                      If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Found
                          ope:Quantity -= (Quantity_Temp - par:Quantity)
                          If ope:Quantity <= 0
                              Delete(ORDPEND)
                          Else !If ope:Quantity <= 0
                              Access:ORDPEND.Update()
                          End !If ope:Quantity <= 0
  
                      Else! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:ORDPEND.Tryfetch(ope:Ref_Number_Key) = Level:Benign
                      
                  Of 1 !New Way
                      Access:ORDPEND.Clearkey(ope:Supplier_Key)
                      ope:Supplier    = par:Supplier
                      ope:Part_Number = par:Part_Number
                      If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                          !Found
                          ope:Quantity -= (Quantity_Temp - par:Quantity)
                          If ope:Quantity <= 0
                              Delete(ORDPEND)
                          Else !If ope:Quantity <= 0
                              Access:ORDPEND.Update()
                          End !If ope:Quantity <= 0
                      Else! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:ORDPEND.Tryfetch(ope:Supplier_Key) = Level:Benign
                      
              End !Case def:SummaryOrders
          End !If par:Quantity < Quantity_Temp
      End !If par:Pending_Ref_Number <> '' and par:Order_Number = ''
  End !If thiswindow.Request <> Insertrecord
      
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Parts')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?par:Fault_Code1:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?par:Fault_Code2:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?par:Fault_Code3:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?par:Fault_Code4:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?par:Fault_Code5:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?par:Fault_Code6:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?par:Fault_Code7:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?par:Fault_Code8:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?par:Fault_Code9:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?par:Fault_Code10:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?par:Fault_Code11:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?par:Fault_Code12:2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?par:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      If Keycode() = MouseRight
         Execute Popup('Lookup Supplier')
             Post(Event:Accepted,?LookupSupplier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupSupplier)
      End!If Keycode() = MouseRight
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      !Is this An Adjustment?
      If glo:select1 = 'ADJUSTMENT' And thiswindow.request = Insertrecord
          par:adjustment = 'YES'
          par:part_number = 'ADJUSTMENT'
          par:description = 'ADJUSTMENT'
          
      End!If glo:select1 = 'ADJUSTMENT' And thiswindow.request = Insertrecord
      adjustment_temp = par:Adjustment
      If ThisWindow.Request = Insertrecord
          par:quantity = 1
          Do show_parts
      Else
      
      End
      Do Adjustment
      If par:order_number <> ''
          ?title{prop:text} = 'Chargeable Part - On Order'
      End
      If par:pending_ref_number <> ''
          ?title{prop:text} = 'Chargeable Part - Pending Order'
      End
      If par:date_received <> ''
          ?title{prop:text} = 'Chargeable Part - Received'
      End
      If PAR:Exclude_From_Order = 'YES'
          ?title{prop:text} = 'Chargeable Part - Excluded From Order'
      End!If PAR:Exclude_From_Order = 'YES'
      
      If Thiswindow.request <> Insertrecord
          Disable(?par:exclude_from_order)
      End!If THiswindow.request <> Insertrecord
      
      
      
      !Save Fields
      quantity_temp = par:quantity
      tmp:part_number     = par:part_number
      tmp:description     = par:description
      tmp:supplier        = par:supplier
      tmp:purchase_cost   = par:purchase_cost
      tmp:sale_cost       = par:sale_cost
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
          0{prop:buffer} = 1
      !Security Check
      Check_access('JOB PART COSTS - EDIT',x")
      If x" = False
          ?par:purchase_cost{prop:readonly} = 1
          ?par:sale_cost{prop:readonly} = 1
          ?par:purchase_cost{prop:color} = color:silver
          ?par:sale_cost{prop:color} = color:silver
      End!"If x" = False
      
      ! Start Change 2343 BE(18/03/03)
      Check_access('JOB - AMEND PART DETAIL', x")
      IF (x" = False) THEN
          ?par:Part_Number{prop:readonly} = 1
          ?par:description{prop:readonly} = 1
          ?par:Part_Number{prop:color} = color:silver
          ?par:description{prop:color} = color:silver
      End
      ! End Change 2343 BE(18/03/03)
      ! Fault Coding (Hopefully)
      fault_codes_required_temp = 'NO'
      required# = 0
      access:chartype.clearkey(cha:charge_type_key)
      cha:charge_type = job:charge_type
      If access:chartype.fetch(cha:charge_type_key) = Level:Benign
          If cha:force_warranty = 'YES'
              required# = 1
          End
      end !if
      
      found# = 0
      setcursor(cursor:wait)
      save_map_id = access:manfaupa.savefile()
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = job:manufacturer
      set(map:field_number_key,map:field_number_key)
      loop
          if access:manfaupa.next()
             break
          end !if
          if map:manufacturer <> job:manufacturer      |
              then break.  ! end if
      
          Case map:field_number
              Of 1
                  found# = 1
                  Unhide(?par:fault_code1:prompt)
                  ?par:fault_code1:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar)
                      ?popcalendar{prop:xpos} = 212
                      Unhide(?par:fault_code1:2)
                      ?par:fault_code1:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code1)
                      If map:lookup = 'YES'
                          Unhide(?button4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code1{prop:req} = 1
                      ?par:fault_code1:2{prop:req} = 1
                  else
                      ?par:fault_code1{prop:req} = 0
                      ?par:fault_code1:2{prop:req} = 0
                  End
              Of 2
                  found# = 1
                  Unhide(?par:fault_code2:prompt)
                  ?par:fault_code2:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:2)
                      ?popcalendar:2{prop:xpos} = 212
                      Unhide(?par:fault_code2:2)
                      ?par:fault_code2:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code2)
                      If map:lookup = 'YES'
                          Unhide(?button4:2)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code2{prop:req} = 1
                      ?par:fault_code2:2{prop:req} = 1
                  else
                      ?par:fault_code2{prop:req} = 0
                      ?par:fault_code2:2{prop:req} = 0
                  End
              Of 3
                  found# = 1
                  Unhide(?par:fault_code3:prompt)
                  ?par:fault_code3:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:3)
                      ?popcalendar:3{prop:xpos} = 212
                      Unhide(?par:fault_code3:2)
                      ?par:fault_code3:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code3)
                      If map:lookup = 'YES'
                          Unhide(?button4:3)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code3{prop:req} = 1
                      ?par:fault_code3:2{prop:req} = 1
                  else
                      ?par:fault_code3{prop:req} = 0
                      ?par:fault_code3:2{prop:req} = 0
                  End
              Of 4
                  found# = 1
                  Unhide(?par:fault_code4:prompt)
                  ?par:fault_code4:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:4)
                      ?popcalendar:4{prop:xpos} = 212
                      Unhide(?par:fault_code4:2)
                      ?par:fault_code4:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code4)
                      If map:lookup = 'YES'
                          Unhide(?button4:4)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code4{prop:req} = 1
                      ?par:fault_code4:2{prop:req} = 1
                  else
                      ?par:fault_code4{prop:req} = 0
                      ?par:fault_code4:2{prop:req} = 0
                  End
              Of 5
                  found# = 1
                  Unhide(?par:fault_code5:prompt)
                  ?par:fault_code5:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:5)
                      ?popcalendar:5{prop:xpos} = 212
                      Unhide(?par:fault_code5:2)
                      ?par:fault_code5:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code5)
                      If map:lookup = 'YES'
                          Unhide(?button4:5)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code5{prop:req} = 1
                      ?par:fault_code5:2{prop:req} = 1
                  else
                      ?par:fault_code5{prop:req} = 0
                      ?par:fault_code5:2{prop:req} = 0
                  End
              Of 6
                  found# = 1
                  Unhide(?par:fault_code6:prompt)
                  ?par:fault_code6:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:6)
                      ?popcalendar:6{prop:xpos} = 212
                      Unhide(?par:fault_code6:2)
                      ?par:fault_code6:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code6)
                      If map:lookup = 'YES'
                          Unhide(?button4:6)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code6{prop:req} = 1
                      ?par:fault_code6:2{prop:req} = 1
                  else
                      ?par:fault_code6{prop:req} = 0
                      ?par:fault_code6:2{prop:req} = 0
                  End
              Of 7
                  found# = 1
                  Unhide(?par:fault_code7:prompt)
                  ?par:fault_code7:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:7)
                      ?popcalendar:7{prop:xpos} = 212
                      Unhide(?par:fault_code7:2)
                      ?par:fault_code7:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code7)
                      If map:lookup = 'YES'
                          Unhide(?button4:7)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code7{prop:req} = 1
                      ?par:fault_code7:2{prop:req} = 1
                  else
                      ?par:fault_code7{prop:req} = 0
                      ?par:fault_code7:2{prop:req} = 0
                  End
      
              Of 8
                  found# = 1
                  Unhide(?par:fault_code8:prompt)
                  ?par:fault_code8:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:8)
                      ?popcalendar:8{prop:xpos} = 212
                      Unhide(?par:fault_code8:2)
                      ?par:fault_code8:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code8)
                      If map:lookup = 'YES'
                          Unhide(?button4:8)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code8{prop:req} = 1
                      ?par:fault_code8:2{prop:req} = 1
                  else
                      ?par:fault_code8{prop:req} = 0
                      ?par:fault_code8:2{prop:req} = 0
                  End
      
              Of 9
                  found# = 1
                  Unhide(?par:fault_code9:prompt)
                  ?par:fault_code9:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:9)
                      ?popcalendar:9{prop:xpos} = 212
                      Unhide(?par:fault_code9:2)
                      ?par:fault_code9:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code9)
                      If map:lookup = 'YES'
                          Unhide(?button4:9)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code9{prop:req} = 1
                      ?par:fault_code9:2{prop:req} = 1
                  else
                      ?par:fault_code9{prop:req} = 0
                      ?par:fault_code9:2{prop:req} = 0
                  End
      
              Of 10
                  found# = 1
                  Unhide(?par:fault_code10:prompt)
                  ?par:fault_code10:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:10)
                      ?popcalendar:10{prop:xpos} = 212
                      Unhide(?par:fault_code10:2)
                      ?par:fault_code10:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code10)
                      If map:lookup = 'YES'
                          Unhide(?button4:10)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code10{prop:req} = 1
                      ?par:fault_code10:2{prop:req} = 1
                  else
                      ?par:fault_code10{prop:req} = 0
                      ?par:fault_code10:2{prop:req} = 0
                  End
      
              Of 11
                  found# = 1
                  Unhide(?par:fault_code11:prompt)
                  ?par:fault_code11:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:11)
                      ?popcalendar:11{prop:xpos} = 212
                      Unhide(?par:fault_code11:2)
                      ?par:fault_code11:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code11)
                      If map:lookup = 'YES'
                          Unhide(?button4:11)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code11{prop:req} = 1
                      ?par:fault_code11:2{prop:req} = 1
                  else
                      ?par:fault_code11{prop:req} = 0
                      ?par:fault_code11:2{prop:req} = 0
                  End
      
              Of 12
                  found# = 1
                  Unhide(?par:fault_code12:prompt)
                  ?par:fault_code12:prompt{prop:text} = map:field_name
                  If map:field_type = 'DATE'
                      Unhide(?popcalendar:12)
                      ?popcalendar:12{prop:xpos} = 212
                      Unhide(?par:fault_code12:2)
                      ?par:fault_code12:2{prop:xpos} = 84
                  Else
                      Unhide(?par:fault_code12)
                      If map:lookup = 'YES'
                          Unhide(?button4:12)
                      End
                  End
                  if required# = 1 And map:compulsory = 'YES'
                      fault_codes_required_temp = 'YES'
                      ?par:fault_code12{prop:req} = 1
                      ?par:fault_code12:2{prop:req} = 1
                  else
                      ?par:fault_code12{prop:req} = 0
                      ?par:fault_code12:2{prop:req} = 0
                  End
      
          End !Case map:field_number
      end !loop
      access:manfaupa.restorefile(save_map_id)
      setcursor()
      If found# = 1
          Unhide(?fault_code_tab)
      End
      If ThisWindow.Request <> InsertRecord
      
          !There was never any code to allow a user to change
          !the details of a part, apart from the quantity,
          !once it is inserted. So I'm going to disable the fields.
          ?browse_stock_button{prop:Hide} = 1
          ?par:Part_Number{prop:Disable} = 1
          ?par:Description{prop:Disable} = 1
          ?par:Supplier{prop:Disable} = 1
          ?LookupSupplier{prop:Disable} = 1
          ?par:Purchase_Cost{prop:Disable} = 1
          ?par:Sale_Cost{prop:Disable} = 1
      End !ThisWindow.Request <> InsertRecord
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

