

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJ01027.INC'),ONCE        !Local module procedure declarations
                     END


DeletePickingParts   PROCEDURE  (jobtype, trans)      ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'DeletePickingParts')      !Add Procedure to Log
  end


    Access:LOCATION.ClearKey(loc:Location_Key)
    loc:Location = sto:Location
    If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign then
        if loc:picknoteenable then !pick notes are enabled for this location

            !*** Picking Note Code *** TH
            access:USERS.clearkey(use:password_key)
            use:Password = glo:Password
            access:USERS.fetch(use:password_key)

            if access:USERS.fetch(use:password_key) then !failure - problem with user login
            end

            if ~trans then
                Part_Delete_Reason !get reason for deleting the part
            else
                if jobtype = 'chargeable' then
                    glo:select2 = 'Parts transferred to warranty'
                end
                if jobtype ='warranty' then
                    glo:select2 = 'Parts transferred to chargeable'
                end
            end

            access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
            pnt:JobReference = job:ref_number
            set(pnt:keyonjobno,pnt:keyonjobno)
            loop
                if access:picknote.next() then break.
        !            Access:LOCATION.ClearKey(loc:Location_Key)
        !            loc:Location = sto:Location
        !            If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign then
        !                if loc:picknoteenable then !pick notes are enabled for this location
        !                    !CreatePickingNote(sto:location)
        !                    !InsertPickingPart(jobtype) !create new picking part ready to delete
        !                    access:PICKNOTE.primerecord() !create bespoke record just for this part
        !                    pnt:JobReference = job:Ref_Number
        !                    !pnt:PickNoteNumber = ?
        !                    pnt:EngineerCode = job:engineer
        !                    pnt:Usercode = use:User_Code
        !                    pnt:Location = sto:Location
        !                    access:PICKNOTE.update()
        !                    access:PICKDET.primerecord()
        !                    pdt:PickNoteRef = pnt:PickNoteRef
        !                    pdt:RequestDate = TODAY()
        !                    pdt:RequestTime = CLOCK()
        !                    pdt:EngineerCode = job:engineer !needed because a different engineer may take over adding parts on the PICKNOTE - gives an audit trail
        !                    pdt:Usercode = use:User_Code
        !                    pnt:IsPrinted = false
        !                    pdt:IsDelete = true
        !                    pdt:DeleteReason = glo:select2
        !                    case jobtype
        !                    of 'chargeable'
        !                        pdt:isChargeable = true
        !                        pdt:PartRefNumber = par:Record_Number
        !                        !pdt:PartRefNumber = par:Part_Ref_Number
        !                        pdt:Quantity = par:Quantity
        !                        pdt:IsInStock = true
        !                        access:PICKDET.update()
        !                        IF (access:audit.primerecord() = level:benign) THEN
        !                            aud:ref_number    = job:ref_number
        !                            aud:date          = today()
        !                            aud:time          = clock()
        !                            aud:type          = 'JOB'
        !                            access:users.clearkey(use:password_key)
        !                            use:password = glo:password
        !                            access:users.fetch(use:password_key)
        !                            aud:user          = use:user_code
        !                            aud:action        = 'CHARGEABLE PART DELETED: ' & CLIP(pdt:PartRefNumber)
        !                            aud:notes         = glo:select2
        !                            access:audit.update()
        !                        END
        !                    of 'warranty'
        !                        pdt:isChargeable = false
        !                        pdt:PartRefNumber = wpr:Record_Number
        !                        !pdt:PartRefNumber = wpr:Part_Ref_number
        !                        pdt:Quantity = wpr:Quantity
        !                        pdt:IsInStock = true
        !                        access:PICKDET.update()
        !                        IF (access:audit.primerecord() = level:benign) THEN
        !                            aud:ref_number    = job:ref_number
        !                            aud:date          = today()
        !                            aud:time          = clock()
        !                            aud:type          = 'JOB'
        !                            access:users.clearkey(use:password_key)
        !                            use:password = glo:password
        !                            access:users.fetch(use:password_key)
        !                            aud:user          = use:user_code
        !                            aud:action        = 'WARRANTY PART DELETED: ' & CLIP(pdt:PartRefNumber)
        !                            aud:notes         = glo:select2
        !                            access:audit.update()
        !                        END
        !                    end !case
        !
        !        !            pnt:JobReference = job:ref_number
        !        !            set(pnt:keyonjobno,pnt:keyonjobno)
        !        !            cycle !no open pick note records - create a record
        !                    break
        !                end
        !            end
        !        end
                if pnt:JobReference <> job:ref_number then break.
                if pnt:IsPrinted then cycle. !already comitted check next
                !otherwise a valid record
                access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
                pdt:PickNoteRef = pnt:PickNoteRef
                set(pdt:keyonpicknote,pdt:keyonpicknote)
                loop
                    if access:pickdet.next() then break. !no records
                    if pdt:PickNoteRef <> pnt:PickNoteRef then break.
                    if ~pdt:IsDelete then !if not already deleted or printed
                        if pdt:IsChargeable then !it is a chargeble part
                            !if pdt:PartRefNumber = par:Part_Ref_Number then !part ref number match - total up
                            if pdt:PartRefNumber = par:Record_Number then !part ref number match - total up
                                pdt:IsDelete = true
                                !pdt:Quantity = pdt:Quantity
                                pdt:DeleteReason = glo:select2
                                pdt:StockPartRefNumber = par:Part_Ref_Number
                                pdt:PartNumber = par:Part_Number
                                access:PICKDET.update
                                IF (access:audit.primerecord() = level:benign) THEN
                                    aud:ref_number    = job:ref_number
                                    aud:date          = today()
                                    aud:time          = clock()
                                    aud:type          = 'JOB'
                                    access:users.clearkey(use:password_key)
                                    use:password = glo:password
                                    access:users.fetch(use:password_key)
                                    aud:user          = use:user_code
                                    aud:action        = 'CHARGEABLE PART DELETED'
                                    aud:notes         = glo:select2 & CLIP(pdt:PartNumber)
                                    access:audit.update()
                                END
                            end
                        else
                            !it is a warranty part
                            !if pdt:PartRefNumber = wpr:Part_Ref_Number then !part ref number match - total up
                            if pdt:PartRefNumber = wpr:Record_Number then !part ref number match - total up
                                pdt:IsDelete = true
                                !pdt:Quantity = pdt:Quantity
                                pdt:DeleteReason = glo:select2
                                pdt:StockPartRefNumber = wpr:Part_Ref_Number
                                pdt:PartNumber = wpr:Part_Number
                                access:PICKDET.update()
                                IF (access:audit.primerecord() = level:benign) THEN
                                    aud:ref_number    = job:ref_number
                                    aud:date          = today()
                                    aud:time          = clock()
                                    aud:type          = 'JOB'
                                    access:users.clearkey(use:password_key)
                                    use:password = glo:password
                                    access:users.fetch(use:password_key)
                                    aud:user          = use:user_code
                                    aud:action        = 'WARRANTY PART DELETED'
                                    aud:notes         = glo:select2 & CLIP(pdt:PartNumber)
                                    access:audit.update()
                                END
                            end
                        end
                    end !is~pdt:IsDelete
                end !loop through pickdets
                break ! part deleted no need to look for more
            end !loop through pick notes
        end
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DeletePickingParts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


