

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01037.INC'),ONCE        !Local module procedure declarations
                     END


Update_Contacts PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::con:Record  LIKE(con:RECORD),STATIC
QuickWindow          WINDOW('Update the CONTACTS File'),AT(,,220,188),FONT('Tahoma',8,,),CENTER,IMM,HLP('Update_Contacts'),TILED,SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,212,152),USE(?Sheet1),SPREAD
                         TAB('General'),USE(?Tab1)
                           PROMPT('Name'),AT(8,20),USE(?CON:Name:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(con:Name),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Postcode'),AT(8,36),USE(?CON:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(84,36,64,10),USE(con:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Clear'),AT(152,36,56,10),USE(?Clear_Address),SKIP,LEFT,ICON(ICON:Cut)
                           PROMPT('Address'),AT(8,48),USE(?CON:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,48,124,10),USE(con:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,60,124,10),USE(con:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,72,124,10),USE(con:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(8,88),USE(?CON:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,88,124,10),USE(con:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fax Number'),AT(8,104),USE(?CON:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,104,124,10),USE(con:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Contact Name 1'),AT(8,120),USE(?CON:Contact_Name1:Prompt),TRN
                           ENTRY(@s30),AT(84,120,124,10),USE(con:Contact_Name1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Contact Name 2'),AT(8,136),USE(?CON:Contact_Name2:Prompt),TRN
                           ENTRY(@s30),AT(84,136,124,10),USE(con:Contact_Name2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(4,160,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(96,164,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,164,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?CON:Name:Prompt{prop:FontColor} = -1
    ?CON:Name:Prompt{prop:Color} = 15066597
    If ?con:Name{prop:ReadOnly} = True
        ?con:Name{prop:FontColor} = 65793
        ?con:Name{prop:Color} = 15066597
    Elsif ?con:Name{prop:Req} = True
        ?con:Name{prop:FontColor} = 65793
        ?con:Name{prop:Color} = 8454143
    Else ! If ?con:Name{prop:Req} = True
        ?con:Name{prop:FontColor} = 65793
        ?con:Name{prop:Color} = 16777215
    End ! If ?con:Name{prop:Req} = True
    ?con:Name{prop:Trn} = 0
    ?con:Name{prop:FontStyle} = font:Bold
    ?CON:Postcode:Prompt{prop:FontColor} = -1
    ?CON:Postcode:Prompt{prop:Color} = 15066597
    If ?con:Postcode{prop:ReadOnly} = True
        ?con:Postcode{prop:FontColor} = 65793
        ?con:Postcode{prop:Color} = 15066597
    Elsif ?con:Postcode{prop:Req} = True
        ?con:Postcode{prop:FontColor} = 65793
        ?con:Postcode{prop:Color} = 8454143
    Else ! If ?con:Postcode{prop:Req} = True
        ?con:Postcode{prop:FontColor} = 65793
        ?con:Postcode{prop:Color} = 16777215
    End ! If ?con:Postcode{prop:Req} = True
    ?con:Postcode{prop:Trn} = 0
    ?con:Postcode{prop:FontStyle} = font:Bold
    ?CON:Address_Line1:Prompt{prop:FontColor} = -1
    ?CON:Address_Line1:Prompt{prop:Color} = 15066597
    If ?con:Address_Line1{prop:ReadOnly} = True
        ?con:Address_Line1{prop:FontColor} = 65793
        ?con:Address_Line1{prop:Color} = 15066597
    Elsif ?con:Address_Line1{prop:Req} = True
        ?con:Address_Line1{prop:FontColor} = 65793
        ?con:Address_Line1{prop:Color} = 8454143
    Else ! If ?con:Address_Line1{prop:Req} = True
        ?con:Address_Line1{prop:FontColor} = 65793
        ?con:Address_Line1{prop:Color} = 16777215
    End ! If ?con:Address_Line1{prop:Req} = True
    ?con:Address_Line1{prop:Trn} = 0
    ?con:Address_Line1{prop:FontStyle} = font:Bold
    If ?con:Address_Line2{prop:ReadOnly} = True
        ?con:Address_Line2{prop:FontColor} = 65793
        ?con:Address_Line2{prop:Color} = 15066597
    Elsif ?con:Address_Line2{prop:Req} = True
        ?con:Address_Line2{prop:FontColor} = 65793
        ?con:Address_Line2{prop:Color} = 8454143
    Else ! If ?con:Address_Line2{prop:Req} = True
        ?con:Address_Line2{prop:FontColor} = 65793
        ?con:Address_Line2{prop:Color} = 16777215
    End ! If ?con:Address_Line2{prop:Req} = True
    ?con:Address_Line2{prop:Trn} = 0
    ?con:Address_Line2{prop:FontStyle} = font:Bold
    If ?con:Address_Line3{prop:ReadOnly} = True
        ?con:Address_Line3{prop:FontColor} = 65793
        ?con:Address_Line3{prop:Color} = 15066597
    Elsif ?con:Address_Line3{prop:Req} = True
        ?con:Address_Line3{prop:FontColor} = 65793
        ?con:Address_Line3{prop:Color} = 8454143
    Else ! If ?con:Address_Line3{prop:Req} = True
        ?con:Address_Line3{prop:FontColor} = 65793
        ?con:Address_Line3{prop:Color} = 16777215
    End ! If ?con:Address_Line3{prop:Req} = True
    ?con:Address_Line3{prop:Trn} = 0
    ?con:Address_Line3{prop:FontStyle} = font:Bold
    ?CON:Telephone_Number:Prompt{prop:FontColor} = -1
    ?CON:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?con:Telephone_Number{prop:ReadOnly} = True
        ?con:Telephone_Number{prop:FontColor} = 65793
        ?con:Telephone_Number{prop:Color} = 15066597
    Elsif ?con:Telephone_Number{prop:Req} = True
        ?con:Telephone_Number{prop:FontColor} = 65793
        ?con:Telephone_Number{prop:Color} = 8454143
    Else ! If ?con:Telephone_Number{prop:Req} = True
        ?con:Telephone_Number{prop:FontColor} = 65793
        ?con:Telephone_Number{prop:Color} = 16777215
    End ! If ?con:Telephone_Number{prop:Req} = True
    ?con:Telephone_Number{prop:Trn} = 0
    ?con:Telephone_Number{prop:FontStyle} = font:Bold
    ?CON:Fax_Number:Prompt{prop:FontColor} = -1
    ?CON:Fax_Number:Prompt{prop:Color} = 15066597
    If ?con:Fax_Number{prop:ReadOnly} = True
        ?con:Fax_Number{prop:FontColor} = 65793
        ?con:Fax_Number{prop:Color} = 15066597
    Elsif ?con:Fax_Number{prop:Req} = True
        ?con:Fax_Number{prop:FontColor} = 65793
        ?con:Fax_Number{prop:Color} = 8454143
    Else ! If ?con:Fax_Number{prop:Req} = True
        ?con:Fax_Number{prop:FontColor} = 65793
        ?con:Fax_Number{prop:Color} = 16777215
    End ! If ?con:Fax_Number{prop:Req} = True
    ?con:Fax_Number{prop:Trn} = 0
    ?con:Fax_Number{prop:FontStyle} = font:Bold
    ?CON:Contact_Name1:Prompt{prop:FontColor} = -1
    ?CON:Contact_Name1:Prompt{prop:Color} = 15066597
    If ?con:Contact_Name1{prop:ReadOnly} = True
        ?con:Contact_Name1{prop:FontColor} = 65793
        ?con:Contact_Name1{prop:Color} = 15066597
    Elsif ?con:Contact_Name1{prop:Req} = True
        ?con:Contact_Name1{prop:FontColor} = 65793
        ?con:Contact_Name1{prop:Color} = 8454143
    Else ! If ?con:Contact_Name1{prop:Req} = True
        ?con:Contact_Name1{prop:FontColor} = 65793
        ?con:Contact_Name1{prop:Color} = 16777215
    End ! If ?con:Contact_Name1{prop:Req} = True
    ?con:Contact_Name1{prop:Trn} = 0
    ?con:Contact_Name1{prop:FontStyle} = font:Bold
    ?CON:Contact_Name2:Prompt{prop:FontColor} = -1
    ?CON:Contact_Name2:Prompt{prop:Color} = 15066597
    If ?con:Contact_Name2{prop:ReadOnly} = True
        ?con:Contact_Name2{prop:FontColor} = 65793
        ?con:Contact_Name2{prop:Color} = 15066597
    Elsif ?con:Contact_Name2{prop:Req} = True
        ?con:Contact_Name2{prop:FontColor} = 65793
        ?con:Contact_Name2{prop:Color} = 8454143
    Else ! If ?con:Contact_Name2{prop:Req} = True
        ?con:Contact_Name2{prop:FontColor} = 65793
        ?con:Contact_Name2{prop:Color} = 16777215
    End ! If ?con:Contact_Name2{prop:Req} = True
    ?con:Contact_Name2{prop:Trn} = 0
    ?con:Contact_Name2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Contacts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Contacts',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Contacts',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Contacts',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CON:Name:Prompt;  SolaceCtrlName = '?CON:Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Name;  SolaceCtrlName = '?con:Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CON:Postcode:Prompt;  SolaceCtrlName = '?CON:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Postcode;  SolaceCtrlName = '?con:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Address;  SolaceCtrlName = '?Clear_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CON:Address_Line1:Prompt;  SolaceCtrlName = '?CON:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Address_Line1;  SolaceCtrlName = '?con:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Address_Line2;  SolaceCtrlName = '?con:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Address_Line3;  SolaceCtrlName = '?con:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CON:Telephone_Number:Prompt;  SolaceCtrlName = '?CON:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Telephone_Number;  SolaceCtrlName = '?con:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CON:Fax_Number:Prompt;  SolaceCtrlName = '?CON:Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Fax_Number;  SolaceCtrlName = '?con:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CON:Contact_Name1:Prompt;  SolaceCtrlName = '?CON:Contact_Name1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Contact_Name1;  SolaceCtrlName = '?con:Contact_Name1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CON:Contact_Name2:Prompt;  SolaceCtrlName = '?CON:Contact_Name2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?con:Contact_Name2;  SolaceCtrlName = '?con:Contact_Name2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Contact'
  OF ChangeRecord
    ActionMessage = 'Changing A Contact'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Contacts')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Contacts')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CON:Name:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(con:Record,History::con:Record)
  SELF.AddHistoryField(?con:Name,1)
  SELF.AddHistoryField(?con:Postcode,2)
  SELF.AddHistoryField(?con:Address_Line1,3)
  SELF.AddHistoryField(?con:Address_Line2,4)
  SELF.AddHistoryField(?con:Address_Line3,5)
  SELF.AddHistoryField(?con:Telephone_Number,6)
  SELF.AddHistoryField(?con:Fax_Number,7)
  SELF.AddHistoryField(?con:Contact_Name1,8)
  SELF.AddHistoryField(?con:Contact_Name2,9)
  SELF.AddUpdateFile(Access:CONTACTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CONTACTS.Open
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:CONTACTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CONTACTS.Close
    Relate:DEFAULTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Contacts',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?con:Postcode
      Postcode_Routine(con:postcode,con:address_line1,con:address_line2,con:address_line3)
      If con:address_line1 <> ''
          Select(?con:address_line1,1)
      End
      Display()
    OF ?Clear_Address
      ThisWindow.Update
      Case MessageEx('Are you sure you want to clear the address?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
          CON:Postcode = ''
          CON:Address_Line1 = ''
          CON:Address_Line2 = ''
          CON:Address_Line3 = ''
          Select(?con:postcode)
          Display()
        Of 2 ! &No Button
      End!Case MessageEx
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Contacts')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

