

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01017.INC'),ONCE        !Local module procedure declarations
                     END


Pick_Model_Stock PROCEDURE                            !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ThisThreadActive BYTE
CurrentTab           STRING(80)
model_number_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
location_temp        STRING(30)
no_temp              STRING('NO')
tag_temp             STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:One              BYTE(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Description)
                       PROJECT(stm:Part_Number)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:Manufacturer)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:Location)
                       JOIN(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                         PROJECT(sto:Purchase_Cost)
                         PROJECT(sto:Sale_Cost)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Location)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
stm:Description        LIKE(stm:Description)          !List box control field - type derived from field
stm:Part_Number        LIKE(stm:Part_Number)          !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !List box control field - type derived from field
stm:Manufacturer       LIKE(stm:Manufacturer)         !Primary key field - type derived from field
stm:Model_Number       LIKE(stm:Model_Number)         !Primary key field - type derived from field
stm:Location           LIKE(stm:Location)             !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Related join file key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK9::stm:Location         LIKE(stm:Location)
HK9::stm:Model_Number     LIKE(stm:Model_Number)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Stock File'),AT(,,491,232),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Model_Stock'),ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F10Key),SYSTEM,GRAY,MAX,DOUBLE
                       PANEL,AT(4,4,400,20),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('Site Location'),AT(8,8),USE(?Prompt1)
                       COMBO(@s30),AT(64,8,124,10),USE(location_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       BUTTON('&Tag   [F5]'),AT(8,208,56,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                       BUTTON('Tag &All [F6]'),AT(64,208,56,16),USE(?DASTAGAll),HIDE,LEFT,ICON('tagall.gif')
                       BUTTON('&Untag All [F7]'),AT(120,208,56,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                       LIST,AT(8,60,392,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@124L(2)|M~Description~@s30@124L(2)|M~Part Number~@s30@38R(2)|M~Purcha' &|
   'se~L@n7.2@33R(2)|M~Sale~L@n7.2@47R(2)|M~Qty In Stock~L@N8@48L(2)|M~Ref Number~@n' &|
   '012@'),FROM(Queue:Browse:1)
                       BUTTON('&Rev tags'),AT(420,142,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(420,166,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON('&Select [F10]'),AT(412,32,76,20),USE(?Select:2),LEFT,ICON('SELECT.ICO')
                       SHEET,AT(4,28,400,200),USE(?CurrentTab),SPREAD
                         TAB('By Description'),USE(?Tab2)
                           ENTRY(@s30),AT(8,44,124,10),USE(stm:Description),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Part Number'),USE(?Tab:6)
                           ENTRY(@s30),AT(8,44,124,10),USE(stm:Part_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(412,208,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?location_temp{prop:ReadOnly} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 15066597
    Elsif ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 8454143
    Else ! If ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 16777215
    End ! If ?location_temp{prop:Req} = True
    ?location_temp{prop:Trn} = 0
    ?location_temp{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?stm:Description{prop:ReadOnly} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 15066597
    Elsif ?stm:Description{prop:Req} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 8454143
    Else ! If ?stm:Description{prop:Req} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 16777215
    End ! If ?stm:Description{prop:Req} = True
    ?stm:Description{prop:Trn} = 0
    ?stm:Description{prop:FontStyle} = font:Bold
    ?Tab:6{prop:Color} = 15066597
    If ?stm:Part_Number{prop:ReadOnly} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 15066597
    Elsif ?stm:Part_Number{prop:Req} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 8454143
    Else ! If ?stm:Part_Number{prop:Req} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 16777215
    End ! If ?stm:Part_Number{prop:Req} = True
    ?stm:Part_Number{prop:Trn} = 0
    ?stm:Part_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = stm:Ref_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = stm:Ref_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = stm:Ref_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::12:QUEUE = GLO:Queue
    ADD(DASBRW::12:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Pointer = stm:Ref_Number
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = stm:Ref_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Pick_Model_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Pick_Model_Stock',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Pick_Model_Stock',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Pick_Model_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Pick_Model_Stock',1)
    SolaceViewVars('location_temp',location_temp,'Pick_Model_Stock',1)
    SolaceViewVars('no_temp',no_temp,'Pick_Model_Stock',1)
    SolaceViewVars('tag_temp',tag_temp,'Pick_Model_Stock',1)
    SolaceViewVars('tmp:One',tmp:One,'Pick_Model_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?location_temp;  SolaceCtrlName = '?location_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Description;  SolaceCtrlName = '?stm:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:6;  SolaceCtrlName = '?Tab:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Part_Number;  SolaceCtrlName = '?stm:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Model_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Pick_Model_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Pick_Model_Stock'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  error# = 1
  If job:Engineer <> ''
      access:users.clearkey(use:user_code_key)
      use:user_code = job:Engineer
      if access:users.fetch(use:user_code_key) = Level:Benign
          If use:location <> ''
              error# = 0
              location_temp = use:location
          End!If use:location <> ''
      End!if access:users.fetch(use:user_code_key) = Level:Benign
  ! Start Change 3369 BE(07/10/03)
  ELSE
      access:users.clearkey(use:password_key)
      use:password =glo:password
      if access:users.fetch(use:password_key) = Level:Benign
          If use:location <> ''
              error# = 0
              location_temp = use:location
          End!If use:location <> ''
      End!if access:users.fetch(use:user_code_key) = Level:Benign
  ! End Change 3369 BE(07/10/03)
  End!If glo:select11 <> ''
  If error# = 1
      Set(defstock)
      Access:defstock.next()
      If dst:site_location <> ''
          location_temp = dst:site_location
      End
  End!If error# = 1
  
  Access:LOCATION.ClearKey(loc:ActiveLocationKey)
  loc:Active   = 1
  loc:Location = Location_Temp
  If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Found
  
  Else!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      Location_Temp = ''
  End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
  
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMODEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Start Change 3369 BE(07/10/03)
  !Set(DEFAULT2)
  !Access:DEFAULT2.Next()
  !If de2:PickEngStockOnly
  !    ?Location_Temp{prop:Disable} = 1
  !End !de2:PickEngStockOnly
  !Have already got the user
  IF (use:StockFromLocationOnly) THEN
      ?Location_Temp{prop:Disable} = 1
  END
  ! End Change 3369 BE(07/10/03)
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STM:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stm:Location_Description_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STM:Description,stm:Description,1,BRW1)
  BRW1.AddResetField(location_temp)
  BIND('tag_temp',tag_temp)
  BIND('GLO:Select12',GLO:Select12)
  BIND('location_temp',location_temp)
  BIND('no_temp',no_temp)
  BIND('model_number_temp',model_number_temp)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(stm:Description,BRW1.Q.stm:Description)
  BRW1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  BRW1.AddField(stm:Manufacturer,BRW1.Q.stm:Manufacturer)
  BRW1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  BRW1.AddField(stm:Location,BRW1.Q.stm:Location)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=584
  QuickWindow{PROP:MinHeight}=210
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB6.Init(location_temp,?location_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:ActiveLocationKey)
  FDCB6.AddRange(loc:Active,tmp:One)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  FDCB6.AddUpdateField(loc:Location,location_temp)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Tab:6{PROP:TEXT} = 'By Part Number'
    ?Browse:1{PROP:FORMAT} ='11L(2)I@s1@#1#124L(2)|M~Description~@s30@#3#124L(2)|M~Part Number~@s30@#4#38R(2)|M~Purchase~L@n7.2@#5#33R(2)|M~Sale~L@n7.2@#6#47R(2)|M~Qty In Stock~L@N8@#7#48L(2)|M~Ref Number~@n012@#8#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:DEFAULT2.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Pick_Model_Stock'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Pick_Model_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?location_temp
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Pick_Model_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='11L(2)I@s1@#1#124L(2)|M~Description~@s30@#3#124L(2)|M~Part Number~@s30@#4#38R(2)|M~Purchase~L@n7.2@#5#33R(2)|M~Sale~L@n7.2@#6#47R(2)|M~Qty In Stock~L@N8@#7#48L(2)|M~Ref Number~@n012@#8#'
          ?Tab2{PROP:TEXT} = 'By Description'
        OF 2
          ?Browse:1{PROP:FORMAT} ='11L(2)I@s1@#1#124L(2)|M~Part Number~@s30@#4#124L(2)|M~Description~@s30@#3#38R(2)|M~Purchase~L@n7.2@#5#33R(2)|M~Sale~L@n7.2@#6#47R(2)|M~Qty In Stock~L@N8@#7#48L(2)|M~Ref Number~@n012@#8#'
          ?Tab:6{PROP:TEXT} = 'By Part Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?stm:Description
      Select(?browse:1)
    OF ?stm:Part_Number
      Select(?browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      Case KeyCode()
          Of F5Key
              Post(Event:Accepted,?DASTAG)
          Of F6Key
              Post(Event:Accepted,?DASTAGALL)
          Of F7Key
              Post(Event:Accepted,?DASUNTAGALL)
          Of F10Key
              Post(Event:Accepted,?Select:2)
      End !KeyCode()
    OF EVENT:OpenWindow
      QuickWindow{prop:text} = 'Available Stock For Model: ' & Clip(glo:select12)
      !Title
      DO DASBRW::12:DASUNTAGALL
      
      Select(?Browse:1)
    OF EVENT:GainFocus
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  ELSE
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = location_temp
     end
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = stm:Ref_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = stm:Ref_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?location_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STM:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?STM:Description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

