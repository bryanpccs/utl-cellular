

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01010.INC'),ONCE        !Local module procedure declarations
                     END


Update_Job_Exchange_Accessory PROCEDURE               !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::jea:Record  LIKE(jea:RECORD),STATIC
QuickWindow          WINDOW('Update the JOBEXACC File'),AT(,,208,112),FONT('MS Sans Serif',8,,),IMM,HLP('Update_Job_Exchange_Accessory'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,200,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number:'),AT(8,20),USE(?JEA:Record_Number:Prompt)
                           ENTRY(@n10.2),AT(76,20,44,10),USE(jea:Record_Number),DECIMAL(14)
                           PROMPT('Reference Number'),AT(8,34),USE(?JEA:Job_Ref_Number:Prompt)
                           ENTRY(@p<<<<<<<<#p),AT(76,34,44,10),USE(jea:Job_Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Reference Number'),AT(8,48),USE(?JEA:Stock_Ref_Number:Prompt)
                           ENTRY(@p<<<<<<<<#p),AT(76,48,44,10),USE(jea:Stock_Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Part Number'),AT(8,62),USE(?JEA:Part_Number:Prompt),TRN
                           ENTRY(@s30),AT(76,62,124,10),USE(jea:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Description'),AT(8,76),USE(?JEA:Description:Prompt),TRN
                           ENTRY(@s30),AT(76,76,124,10),USE(jea:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('OK'),AT(110,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(159,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(159,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?JEA:Record_Number:Prompt{prop:FontColor} = -1
    ?JEA:Record_Number:Prompt{prop:Color} = 15066597
    If ?jea:Record_Number{prop:ReadOnly} = True
        ?jea:Record_Number{prop:FontColor} = 65793
        ?jea:Record_Number{prop:Color} = 15066597
    Elsif ?jea:Record_Number{prop:Req} = True
        ?jea:Record_Number{prop:FontColor} = 65793
        ?jea:Record_Number{prop:Color} = 8454143
    Else ! If ?jea:Record_Number{prop:Req} = True
        ?jea:Record_Number{prop:FontColor} = 65793
        ?jea:Record_Number{prop:Color} = 16777215
    End ! If ?jea:Record_Number{prop:Req} = True
    ?jea:Record_Number{prop:Trn} = 0
    ?jea:Record_Number{prop:FontStyle} = font:Bold
    ?JEA:Job_Ref_Number:Prompt{prop:FontColor} = -1
    ?JEA:Job_Ref_Number:Prompt{prop:Color} = 15066597
    If ?jea:Job_Ref_Number{prop:ReadOnly} = True
        ?jea:Job_Ref_Number{prop:FontColor} = 65793
        ?jea:Job_Ref_Number{prop:Color} = 15066597
    Elsif ?jea:Job_Ref_Number{prop:Req} = True
        ?jea:Job_Ref_Number{prop:FontColor} = 65793
        ?jea:Job_Ref_Number{prop:Color} = 8454143
    Else ! If ?jea:Job_Ref_Number{prop:Req} = True
        ?jea:Job_Ref_Number{prop:FontColor} = 65793
        ?jea:Job_Ref_Number{prop:Color} = 16777215
    End ! If ?jea:Job_Ref_Number{prop:Req} = True
    ?jea:Job_Ref_Number{prop:Trn} = 0
    ?jea:Job_Ref_Number{prop:FontStyle} = font:Bold
    ?JEA:Stock_Ref_Number:Prompt{prop:FontColor} = -1
    ?JEA:Stock_Ref_Number:Prompt{prop:Color} = 15066597
    If ?jea:Stock_Ref_Number{prop:ReadOnly} = True
        ?jea:Stock_Ref_Number{prop:FontColor} = 65793
        ?jea:Stock_Ref_Number{prop:Color} = 15066597
    Elsif ?jea:Stock_Ref_Number{prop:Req} = True
        ?jea:Stock_Ref_Number{prop:FontColor} = 65793
        ?jea:Stock_Ref_Number{prop:Color} = 8454143
    Else ! If ?jea:Stock_Ref_Number{prop:Req} = True
        ?jea:Stock_Ref_Number{prop:FontColor} = 65793
        ?jea:Stock_Ref_Number{prop:Color} = 16777215
    End ! If ?jea:Stock_Ref_Number{prop:Req} = True
    ?jea:Stock_Ref_Number{prop:Trn} = 0
    ?jea:Stock_Ref_Number{prop:FontStyle} = font:Bold
    ?JEA:Part_Number:Prompt{prop:FontColor} = -1
    ?JEA:Part_Number:Prompt{prop:Color} = 15066597
    If ?jea:Part_Number{prop:ReadOnly} = True
        ?jea:Part_Number{prop:FontColor} = 65793
        ?jea:Part_Number{prop:Color} = 15066597
    Elsif ?jea:Part_Number{prop:Req} = True
        ?jea:Part_Number{prop:FontColor} = 65793
        ?jea:Part_Number{prop:Color} = 8454143
    Else ! If ?jea:Part_Number{prop:Req} = True
        ?jea:Part_Number{prop:FontColor} = 65793
        ?jea:Part_Number{prop:Color} = 16777215
    End ! If ?jea:Part_Number{prop:Req} = True
    ?jea:Part_Number{prop:Trn} = 0
    ?jea:Part_Number{prop:FontStyle} = font:Bold
    ?JEA:Description:Prompt{prop:FontColor} = -1
    ?JEA:Description:Prompt{prop:Color} = 15066597
    If ?jea:Description{prop:ReadOnly} = True
        ?jea:Description{prop:FontColor} = 65793
        ?jea:Description{prop:Color} = 15066597
    Elsif ?jea:Description{prop:Req} = True
        ?jea:Description{prop:FontColor} = 65793
        ?jea:Description{prop:Color} = 8454143
    Else ! If ?jea:Description{prop:Req} = True
        ?jea:Description{prop:FontColor} = 65793
        ?jea:Description{prop:Color} = 16777215
    End ! If ?jea:Description{prop:Req} = True
    ?jea:Description{prop:Trn} = 0
    ?jea:Description{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Job_Exchange_Accessory',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Job_Exchange_Accessory',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Job_Exchange_Accessory',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Job_Exchange_Accessory',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JEA:Record_Number:Prompt;  SolaceCtrlName = '?JEA:Record_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jea:Record_Number;  SolaceCtrlName = '?jea:Record_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JEA:Job_Ref_Number:Prompt;  SolaceCtrlName = '?JEA:Job_Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jea:Job_Ref_Number;  SolaceCtrlName = '?jea:Job_Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JEA:Stock_Ref_Number:Prompt;  SolaceCtrlName = '?JEA:Stock_Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jea:Stock_Ref_Number;  SolaceCtrlName = '?jea:Stock_Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JEA:Part_Number:Prompt;  SolaceCtrlName = '?JEA:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jea:Part_Number;  SolaceCtrlName = '?jea:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JEA:Description:Prompt;  SolaceCtrlName = '?JEA:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jea:Description;  SolaceCtrlName = '?jea:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Job_Exchange_Accessory')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Job_Exchange_Accessory')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JEA:Record_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(jea:Record,History::jea:Record)
  SELF.AddHistoryField(?jea:Record_Number,1)
  SELF.AddHistoryField(?jea:Job_Ref_Number,2)
  SELF.AddHistoryField(?jea:Stock_Ref_Number,3)
  SELF.AddHistoryField(?jea:Part_Number,4)
  SELF.AddHistoryField(?jea:Description,5)
  SELF.AddUpdateFile(Access:JOBEXACC)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBEXACC.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBEXACC
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBEXACC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Job_Exchange_Accessory',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Job_Exchange_Accessory')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

