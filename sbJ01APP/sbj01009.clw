

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01009.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Job_Exchange_Accessory PROCEDURE (f_ref_number) !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
stock_ref_number_temp REAL
FilesOpened          BYTE
Ref_Number_Temp      REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBEXACC)
                       PROJECT(jea:Part_Number)
                       PROJECT(jea:Description)
                       PROJECT(jea:Record_Number)
                       PROJECT(jea:Job_Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jea:Part_Number        LIKE(jea:Part_Number)          !List box control field - type derived from field
jea:Description        LIKE(jea:Description)          !List box control field - type derived from field
jea:Record_Number      LIKE(jea:Record_Number)        !Primary key field - type derived from field
jea:Job_Ref_Number     LIKE(jea:Job_Ref_Number)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Exchange Accessories'),AT(,,378,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Job_Exchange_Accessory'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,276,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('124L(2)|M~Part Number~@s30@80L(2)|M~Description~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(296,88,76,20),USE(?Insert),LEFT,ICON('Insert.ico')
                       SHEET,AT(4,4,284,180),USE(?CurrentTab),SPREAD
                         TAB('By Part Number'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(jea:Part_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&Delete'),AT(296,140,76,20),USE(?Delete),LEFT,ICON('Delete.ico')
                       BUTTON('Close'),AT(296,164,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?jea:Part_Number{prop:ReadOnly} = True
        ?jea:Part_Number{prop:FontColor} = 65793
        ?jea:Part_Number{prop:Color} = 15066597
    Elsif ?jea:Part_Number{prop:Req} = True
        ?jea:Part_Number{prop:FontColor} = 65793
        ?jea:Part_Number{prop:Color} = 8454143
    Else ! If ?jea:Part_Number{prop:Req} = True
        ?jea:Part_Number{prop:FontColor} = 65793
        ?jea:Part_Number{prop:Color} = 16777215
    End ! If ?jea:Part_Number{prop:Req} = True
    ?jea:Part_Number{prop:Trn} = 0
    ?jea:Part_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Job_Exchange_Accessory',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Job_Exchange_Accessory',1)
    SolaceViewVars('stock_ref_number_temp',stock_ref_number_temp,'Browse_Job_Exchange_Accessory',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Job_Exchange_Accessory',1)
    SolaceViewVars('Ref_Number_Temp',Ref_Number_Temp,'Browse_Job_Exchange_Accessory',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jea:Part_Number;  SolaceCtrlName = '?jea:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Job_Exchange_Accessory')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Job_Exchange_Accessory')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Job_Exchange_Accessory'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBEXACC.Open
  Relate:STOCK.Open
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  ref_number_temp = f_ref_number
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBEXACC,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,jea:Part_Number_Key)
  BRW1.AddRange(jea:Job_Ref_Number,Ref_Number_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?jea:Part_Number,jea:Part_Number,1,BRW1)
  BRW1.AddField(jea:Part_Number,BRW1.Q.jea:Part_Number)
  BRW1.AddField(jea:Description,BRW1.Q.jea:Description)
  BRW1.AddField(jea:Record_Number,BRW1.Q.jea:Record_Number)
  BRW1.AddField(jea:Job_Ref_Number,BRW1.Q.jea:Job_Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBEXACC.Close
    Relate:STOCK.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Job_Exchange_Accessory'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Job_Exchange_Accessory',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  stock_ref_number_Temp = jea:stock_ref_number
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Job_Exchange_Accessory
    ReturnValue = GlobalResponse
  END
  If globalresponse = 1
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number  = stock_ref_number_temp
      If access:stock.fetch(sto:ref_number_key) = Level:Benign
          sto:quantity_stock += 1
          access:stock.update()
          If access:stohist.primerecord() = Level:Benign
              shi:information          = 'REMOVED FROM EXCHANGE'
              shi:job_number           = ref_number_temp
              shi:ref_number           = sto:ref_number
              shi:transaction_type     = 'REC'
              shi:despatch_note_number = ''
              shi:quantity             = 1
              shi:date                 = Today()
              shi:purchase_cost        = sto:purchase_cost
              shi:sale_cost            = sto:sale_cost
              shi:retail_cost          = sto:retail_cost
              Access:users.clearkey(use:password_key)
              use:password =glo:password
              Access:users.Fetch(use:password_key)
              shi:user                 = use:user_code
              shi:notes                = 'STOCK RECREDITED'
              Access:stohist.Insert()
          End!If access:stohist.primerecord() = Level:Benign
      End!If access:stock.fetch(sto:ref_number_key)
  End!If globalresponse = 1
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Insert
      glo:select7 = ''
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Insert
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Accessory_Stock
      ThisWindow.Reset
      If glo:select7 <> ''
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number  = glo:select7
          If access:stock.fetch(sto:ref_number_key)
          Else!If access:stock.fetch(sto:ref_number_key)
              If sto:quantity_stock <= 0
                  Case MessageEx('There is insufficient stock.<13,10><13,10>Please select another Accessory.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              Else!If sto:quantity_stock <= 0
                  get(jobexacc,0)
                  if access:jobexacc.primerecord() = Level:benign
                      jea:job_ref_number   = ref_number_temp
                      jea:stock_ref_number = sto:ref_number
                      jea:part_number      = sto:part_number
                      jea:description      = sto:description
                      access:jobexacc.insert()
      
                      sto:quantity_stock -= 1
                      If sto:quantity_stock < 0
                          sto:quantity_stock = 0
                      End!If sto:quantity_stock < 0
                      access:stock.update()
                      get(stohist,0)
                      If access:stohist.primerecord() = Level:Benign
                          shi:information = ''
                          shi:ref_number  = sto:ref_number
                          access:users.clearkey(use:password_key)
                          use:password    =glo:password
                          access:users.fetch(use:password_key)
                          shi:user = use:user_code
                          shi:transaction_type = 'DEC'
                          shi:despatch_note_number = ''
                          shi:job_number = ref_number_temp
                          shi:quantity = 1
                          shi:date = Today()
                          shi:purchase_cost = sto:purchase_cost
                          shi:sale_cost = sto:sale_cost
                          shi:retail_cost = sto:retail_cost
                          shi:notes = 'ACCESSORY EXCHANGE ON JOB NUMBER: ' & Clip(ref_number_temp)
                          access:stohist.insert()
                      End!If access:stohist.primerecord() = Level:Benign
                  end!if access:jobexacc.primerecord() = Level:benign
              End!If sto:quantity_stock <= 0
          End!If access:stock.fetch(sto:ref_number_key)
      End!If glo:select7 <> ''
      BRW1.ResetSort(1)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Job_Exchange_Accessory')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?jea:Part_Number
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

