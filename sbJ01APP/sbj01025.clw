

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJ01025.INC'),ONCE        !Local module procedure declarations
                     END


InsertPickingPart    PROCEDURE  (jobtype)             ! Declare Procedure
MSpend               BYTE
pickcount            LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'InsertPickingPart')      !Add Procedure to Log
  end


    Access:LOCATION.ClearKey(loc:Location_Key)
    loc:Location = sto:Location
    If Access:LOCATION.TryFetch(loc:Location_Key) = Level:Benign then
        if loc:picknoteenable then !pick notes are enabled for this location

            MSpend = false
            !*** Picking Note Code *** TH
            access:USERS.clearkey(use:password_key)
            use:Password = glo:Password
            access:USERS.fetch(use:password_key)

            if access:USERS.fetch(use:password_key) then !failure - problem with user login
            end

            access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
            pnt:JobReference = job:ref_number
            set(pnt:keyonjobno,pnt:keyonjobno)
            loop

                if access:picknote.next() then break. !no records
                if pnt:JobReference <> job:Ref_Number then break. !no matching records
                if pnt:Location <> sto:Location then cycle. !no matching location
                if ~pnt:IsPrinted then !matching record - is it unprinted if so we can we append to it
                    !edit the existing record
                    !prepare the new pick parts detail record

!                    free(mainstoreparts)
!                    clear(mainstoreparts)

                    access:PICKDET.primerecord()
                    pdt:PickNoteRef = pnt:PickNoteRef
                    !pdt:IsChargeable =
                    pdt:RequestDate = TODAY()
                    pdt:RequestTime = CLOCK()
                    pdt:EngineerCode = job:engineer !needed because a different engineer may take over adding parts on the PICKNOTE - gives an audit trail
                    pdt:Usercode = use:User_Code
                    !pdt:IsInStock =
                    !IsPrinted - not needed in related PICKNOTE may be needed for audit purposes.
                    pdt:IsDelete = false
                    !pdt:DeleteReason - not needed yet
                    case jobtype
                    of 'chargeable'
                        pdt:PartRefNumber = par:Record_Number
                        !pdt:PartRefNumber = par:Part_Ref_Number
                        pdt:Quantity = par:Quantity
                        tmp_exclude_from_order = par:exclude_from_order
                        tmp_part_ref_number = par:part_ref_number
                        pdt:StockPartRefNumber = par:Part_Ref_Number
                        pdt:PartNumber = par:Part_Number

                    of 'warranty'
                        pdt:PartRefNumber = wpr:Record_Number
                        !pdt:PartRefNumber = wpr:Part_Ref_number
                        pdt:Quantity = wpr:Quantity
                        tmp_exclude_from_order = wpr:exclude_from_order
                        tmp_part_ref_number = wpr:part_ref_number
                        pdt:StockPartRefNumber = wpr:Part_Ref_Number
                        pdt:PartNumber = wpr:Part_Number
                    end !case

                    If tmp_exclude_from_order <> 'YES'
                       If tmp_part_ref_number <> ''
                            access:stock.clearkey(sto:ref_number_key)
                            sto:ref_number = tmp_part_ref_number
                            if access:stock.fetch(sto:ref_number_key) !complete failure not in stock record
                                pdt:IsInStock = false
                                access:PICKDET.update()
                            Else!if access:stock.fetch(sto:ref_number_key)
                                If sto:sundry_item <> 'YES'
                                    !If pdt:quantity > sto:quantity_stock !MORE NEEDED THAN WHAT IS AVAILABLE AT CURRENT LOCATION
                                        !If sto:quantity_stock = 0 !NO STOCK AVAILABLE AT CURRENT LOCATION
                                        if (jobtype = 'chargeable' and par:Pending_Ref_Number <> 0) or (jobtype = 'warranty' and wpr:Pending_Ref_Number <> 0) then
                                            if def:PickNoteNormal and (~def:PickNoteMainStore) !PICK NOTE AT ENGINEERS LOC ONLY

                                                if jobtype = 'chargeable' then
                                                    pdt:IsChargeable = true
                                                end

                                                pdt:IsInStock = false

                                                access:PICKDET.update()
                                            end
                                            if def:PickNoteMainStore !PICK NOTE AT MAIN STORE ONLY - NO POINT LOCAL IF SET
                                                !we need to fulfil from Main Store

                                                if jobtype = 'chargeable' then
                                                    pdt:IsChargeable = true
                                                end

                                                pdt:IsInStock = false
                                                MSpend = true
                                                !access:PICKDET.update()
!                                                mainstoreparts.QPartRefNumber = pdt:PartRefNumber
!                                                mainstoreparts.QQuantity = pdt:Quantity
!                                                mainstoreparts.Qchargeable = pdt:IsChargeable
!                                                mainstoreparts.QIsInStock = pdt:IsInStock
!                                                mainstoreparts.QStockPart = pdt:StockPartRefNumber
!                                                mainstoreparts.QPartNumber = pdt:PartNumber
!                                                ADD(mainstoreparts)
                                            end
                                            !otherwise NO PICK NOTE DEFAULTS do nothing
                                        Else!If sto:quantity_stock <= 0 !SOME AVAILABLE AT CURRENT LOCATION
        !                                    if def:PickNoteNormal and def:PickNoteMainStore !PICK NOTE AT BOTH LOCATIONS
        !
        !                                        if jobtype = 'chargeable' then
        !                                            pdt:IsChargeable = true
        !                                        end
        !
        !                                        temp# = pdt:Quantity - sto:quantity_stock
        !                                        pdt:Quantity = sto:quantity_stock !all the current stock
        !
        !                                        access:PICKDET.update()
        !
        !                                        pdt:Quantity = temp# !remainder of stock quantity
        !                                        mainstoreparts.QPartRefNumber = pdt:PartRefNumber
        !                                        mainstoreparts.QQuantity = pdt:Quantity
        !                                        mainstoreparts.Qchargeable = pdt:IsChargeable
        !                                        mainstoreparts.QIsInStock = pdt:IsInStock
        !                                        ADD(mainstoreparts)
        !                                    end
        !                                    if def:PickNoteNormal and (~def:PickNoteMainStore) !PICK NOTE AT ENGINEERS LOC ONLY
        !
        !                                        if jobtype = 'chargeable' then
        !                                            pdt:IsChargeable = true
        !                                        end
        !
        !                                        temp# = pdt:Quantity - sto:quantity_stock
        !                                        pdt:Quantity = sto:quantity_stock !all the current stock
        !                                        access:PICKDET.update()
        !
        !                                        pdt:Quantity = temp# !remainder of stock quantity
        !                                        pdt:IsInStock = true
        !
        !                                        access:PICKDET.insert()
        !                                    end
        !                                    if (~def:PickNoteNormal) and def:PickNoteMainStore !PICK NOTE AT MAIN STORE ONLY
        !                                        mainstoreparts.QPartRefNumber = pdt:PartRefNumber
        !                                        mainstoreparts.QQuantity = pdt:Quantity
        !                                        mainstoreparts.Qchargeable = pdt:IsChargeable
        !                                        mainstoreparts.QIsInStock = pdt:IsInStock
        !                                        ADD(mainstoreparts)
        !                                    end
        !                                    !otherwise NO PICK NOTE DEFAULTS do nothing
        !                                End!If sto:quantity_stock = 0
        !                            Else !If pdt:quantity > sto:quantity_stock ENOUGH IN CURRENT LOCATION

                                        if jobtype = 'chargeable' then
                                            pdt:IsChargeable = true
                                        end

                                        pdt:IsInStock = true
                                        access:PICKDET.update()

                                    End !If pdt:quantity > sto:quantity_stock
                                End!If sto:sundry_item <> 'YES'
                            end!if access:stock.fetch(sto:ref_number_key)
                        End !If tmp_part_ref_number <> ''
                    End!If tmp_exclude_from_order <> 'YES'
                    !break
                end
                !break !we do not wish to append this item to any other open picking notes
            end !loop
            !Process the MAIN STORE queue loop
            !if anything in the queue then
!            IF RECORDS(mainstoreparts) > 0 then
             if MSpend then
                pickcount = 0
                access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
                pnt:JobReference = job:ref_number
                set(pnt:keyonjobno,pnt:keyonjobno)
                IsEdited = false
                loop
                    if access:picknote.next() then
                        pickcount +=1
                        break !no records
                    end
                    pickcount +=1
                    if pnt:JobReference <> job:Ref_Number then break. !no matching records
                    if pnt:Location <> 'MAIN STORE' then break. !no matching location
                    if ~pnt:IsPrinted then !matching record - is it unprinted if so we can we append to it
                         !edit the existing record
                        IsEdited = true
                        break
                    end
                end !loop
!
                if IsEdited then
                !we can append an existing pick note
                    pnt:EngineerCode = job:engineer
                    pnt:Usercode = use:User_Code
                    !pnt:Location = may need in the future
                    access:PICKNOTE.update()
                else
                !we need to create a new pick note
                    access:PICKNOTE.primerecord()
                    pnt:JobReference = job:Ref_Number
                    pnt:PickNoteNumber = pickcount
                    pnt:EngineerCode = job:engineer
                    pnt:Usercode = use:User_Code
                    pnt:Location = 'MAIN STORE'
                    access:PICKNOTE.update()
                end

                access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
                pnt:JobReference = job:ref_number
                set(pnt:keyonjobno,pnt:keyonjobno)
                loop
                    if access:picknote.next() then break. !no records
                    if pnt:JobReference <> job:Ref_Number then break. !no matching records
                    if pnt:Location <> 'MAIN STORE' then cycle. !no matching location
                    if ~pnt:IsPrinted then !matching record - is it unprinted if so we can we append to it
                    !go thru' pickdets in the queue
!                        access:PICKDET.cancelautoinc() !clear the previous primerecord ready for this loop
!                        LOOP X# = 1 TO RECORDS(mainstoreparts)
!                            GET(mainstoreparts,X#)
!                            access:PICKDET.primerecord()
                            pdt:PickNoteRef = pnt:PickNoteRef
!                            pdt:PartRefNumber = mainstoreparts.QPartRefNumber
!                            pdt:Quantity = mainstoreparts.QQuantity
!                            pdt:IsChargeable = mainstoreparts.Qchargeable
                            pdt:RequestDate = TODAY()
                            pdt:RequestTime = CLOCK()
                            pdt:EngineerCode = job:engineer !needed because a different engineer may take over adding parts on the PICKNOTE - gives an audit trail
                            pdt:Usercode = use:User_Code
!                            pdt:IsInStock = mainstoreparts.QIsInStock
                            pdt:IsPrinted = false
                            pdt:IsDelete = false
!                            pdt:StockPartRefNumber = mainstoreparts.QStockPart
!                            pdt:PartNumber = mainstoreparts.QPartNumber
                            access:PICKDET.update()
!                        END !LOOP
                    end
                end
            END
        end
    end


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'InsertPickingPart',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('MSpend',MSpend,'InsertPickingPart',1)
    SolaceViewVars('pickcount',pickcount,'InsertPickingPart',1)


