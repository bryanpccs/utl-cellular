

   MEMBER('sbj01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJ01042.INC'),ONCE        !Local module procedure declarations
                     END


Invoice_Date_Range PROCEDURE                          !Generated from procedure template - Window

FilesOpened          BYTE
save_inv_ali_id      USHORT,AUTO
save_inv_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
pos                  STRING(255)
save_sub_id          USHORT,AUTO
start_date_temp      DATE
end_Date_temp        DATE
tmp:accountnumber    STRING(15)
tmp:SelectAccount    BYTE(0)
tmp:SelectBatch      BYTE(0)
tmp:BatchNumber      LONG
tmp:InvoiceType      BYTE(1)
tmp:TradeList        BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:StartNumber      LONG
tmp:EndNumber        LONG
tmp:RangeType        BYTE(0)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:accountnumber
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:RefNumber)
                     END
window               WINDOW('Date Range'),AT(,,230,263),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,224,228),USE(?Sheet1),SPREAD
                         TAB('Date Range'),USE(?Tab1)
                           CHECK('Select Account Number'),AT(84,20),USE(tmp:SelectAccount),RIGHT,VALUE('1','0')
                           COMBO(@s15),AT(84,32,124,10),USE(tmp:accountnumber),IMM,DISABLE,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('60L(2)|M@s15@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           CHECK('Select Chargeable Batch Number'),AT(84,48),USE(tmp:SelectBatch),RIGHT,VALUE('1','0')
                           PROMPT('Batch Number'),AT(8,64),USE(?tmp:BatchNumber:Prompt)
                           ENTRY(@n8),AT(84,64,64,10),USE(tmp:BatchNumber),DISABLE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           OPTION('Invoice Type'),AT(84,76,132,52),USE(tmp:InvoiceType),BOXED
                             RADIO('All Invoices'),AT(92,85),USE(?tmp:InvoiceType:Radio1)
                             RADIO('Service Invoices Only'),AT(92,100),USE(?tmp:InvoiceType:Radio2)
                             RADIO('Retail Invoices Only'),AT(92,116),USE(?tmp:InvoiceType:Radio3)
                           END
                           OPTION('Range Type'),AT(84,128,132,32),USE(tmp:RangeType),BOXED
                             RADIO('Invoice Date'),AT(92,140),USE(?Option2:Radio1),VALUE('0')
                             RADIO('Invoice No'),AT(160,140),USE(?Option2:Radio2),VALUE('1')
                           END
                           GROUP,AT(9,165,179,35),USE(?InvoiceDates)
                             PROMPT('Start Date'),AT(8,168),USE(?Select_Global1:Prompt)
                             ENTRY(@d6),AT(84,168,64,10),USE(start_date_temp),FONT(,8,,FONT:bold),ALRT(AltD),CAP
                             BUTTON,AT(152,168,10,10),USE(?PopCalendar),ICON('calenda2.ico')
                             PROMPT('End Date'),AT(8,184),USE(?Select_Global2:Prompt)
                             ENTRY(@d6b),AT(84,184,64,10),USE(end_Date_temp),FONT(,8,,FONT:bold),ALRT(AltD),UPR
                             BUTTON,AT(152,184,10,10),USE(?PopCalendar:2),ICON('Calenda2.ico')
                           END
                           GROUP,AT(9,197,175,35),USE(?InvoiceNumbers),DISABLE
                             PROMPT('Start Number'),AT(8,200),USE(?tmp:StartNumber:Prompt),TRN
                             ENTRY(@s8),AT(84,200,64,10),USE(tmp:StartNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Start NU'),TIP('Start NU'),UPR
                             PROMPT('End Number'),AT(8,216),USE(?tmp:EndNumber:Prompt),TRN
                             ENTRY(@s8),AT(84,216,64,10),USE(tmp:EndNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('End Number'),TIP('End Number'),UPR
                           END
                           PROMPT('Account Number'),AT(8,32),USE(?Prompt3)
                         END
                       END
                       PANEL,AT(4,236,224,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,240,56,16),USE(?Button4),LEFT,ICON('ok.gif')
                       BUTTON('Close'),AT(168,240,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?tmp:SelectAccount{prop:Font,3} = -1
    ?tmp:SelectAccount{prop:Color} = 15066597
    ?tmp:SelectAccount{prop:Trn} = 0
    If ?tmp:accountnumber{prop:ReadOnly} = True
        ?tmp:accountnumber{prop:FontColor} = 65793
        ?tmp:accountnumber{prop:Color} = 15066597
    Elsif ?tmp:accountnumber{prop:Req} = True
        ?tmp:accountnumber{prop:FontColor} = 65793
        ?tmp:accountnumber{prop:Color} = 8454143
    Else ! If ?tmp:accountnumber{prop:Req} = True
        ?tmp:accountnumber{prop:FontColor} = 65793
        ?tmp:accountnumber{prop:Color} = 16777215
    End ! If ?tmp:accountnumber{prop:Req} = True
    ?tmp:accountnumber{prop:Trn} = 0
    ?tmp:accountnumber{prop:FontStyle} = font:Bold
    ?tmp:SelectBatch{prop:Font,3} = -1
    ?tmp:SelectBatch{prop:Color} = 15066597
    ?tmp:SelectBatch{prop:Trn} = 0
    ?tmp:BatchNumber:Prompt{prop:FontColor} = -1
    ?tmp:BatchNumber:Prompt{prop:Color} = 15066597
    If ?tmp:BatchNumber{prop:ReadOnly} = True
        ?tmp:BatchNumber{prop:FontColor} = 65793
        ?tmp:BatchNumber{prop:Color} = 15066597
    Elsif ?tmp:BatchNumber{prop:Req} = True
        ?tmp:BatchNumber{prop:FontColor} = 65793
        ?tmp:BatchNumber{prop:Color} = 8454143
    Else ! If ?tmp:BatchNumber{prop:Req} = True
        ?tmp:BatchNumber{prop:FontColor} = 65793
        ?tmp:BatchNumber{prop:Color} = 16777215
    End ! If ?tmp:BatchNumber{prop:Req} = True
    ?tmp:BatchNumber{prop:Trn} = 0
    ?tmp:BatchNumber{prop:FontStyle} = font:Bold
    ?tmp:InvoiceType{prop:Font,3} = -1
    ?tmp:InvoiceType{prop:Color} = 15066597
    ?tmp:InvoiceType{prop:Trn} = 0
    ?tmp:InvoiceType:Radio1{prop:Font,3} = -1
    ?tmp:InvoiceType:Radio1{prop:Color} = 15066597
    ?tmp:InvoiceType:Radio1{prop:Trn} = 0
    ?tmp:InvoiceType:Radio2{prop:Font,3} = -1
    ?tmp:InvoiceType:Radio2{prop:Color} = 15066597
    ?tmp:InvoiceType:Radio2{prop:Trn} = 0
    ?tmp:InvoiceType:Radio3{prop:Font,3} = -1
    ?tmp:InvoiceType:Radio3{prop:Color} = 15066597
    ?tmp:InvoiceType:Radio3{prop:Trn} = 0
    ?tmp:RangeType{prop:Font,3} = -1
    ?tmp:RangeType{prop:Color} = 15066597
    ?tmp:RangeType{prop:Trn} = 0
    ?Option2:Radio1{prop:Font,3} = -1
    ?Option2:Radio1{prop:Color} = 15066597
    ?Option2:Radio1{prop:Trn} = 0
    ?Option2:Radio2{prop:Font,3} = -1
    ?Option2:Radio2{prop:Color} = 15066597
    ?Option2:Radio2{prop:Trn} = 0
    ?InvoiceDates{prop:Font,3} = -1
    ?InvoiceDates{prop:Color} = 15066597
    ?InvoiceDates{prop:Trn} = 0
    ?Select_Global1:Prompt{prop:FontColor} = -1
    ?Select_Global1:Prompt{prop:Color} = 15066597
    If ?start_date_temp{prop:ReadOnly} = True
        ?start_date_temp{prop:FontColor} = 65793
        ?start_date_temp{prop:Color} = 15066597
    Elsif ?start_date_temp{prop:Req} = True
        ?start_date_temp{prop:FontColor} = 65793
        ?start_date_temp{prop:Color} = 8454143
    Else ! If ?start_date_temp{prop:Req} = True
        ?start_date_temp{prop:FontColor} = 65793
        ?start_date_temp{prop:Color} = 16777215
    End ! If ?start_date_temp{prop:Req} = True
    ?start_date_temp{prop:Trn} = 0
    ?start_date_temp{prop:FontStyle} = font:Bold
    ?Select_Global2:Prompt{prop:FontColor} = -1
    ?Select_Global2:Prompt{prop:Color} = 15066597
    If ?end_Date_temp{prop:ReadOnly} = True
        ?end_Date_temp{prop:FontColor} = 65793
        ?end_Date_temp{prop:Color} = 15066597
    Elsif ?end_Date_temp{prop:Req} = True
        ?end_Date_temp{prop:FontColor} = 65793
        ?end_Date_temp{prop:Color} = 8454143
    Else ! If ?end_Date_temp{prop:Req} = True
        ?end_Date_temp{prop:FontColor} = 65793
        ?end_Date_temp{prop:Color} = 16777215
    End ! If ?end_Date_temp{prop:Req} = True
    ?end_Date_temp{prop:Trn} = 0
    ?end_Date_temp{prop:FontStyle} = font:Bold
    ?InvoiceNumbers{prop:Font,3} = -1
    ?InvoiceNumbers{prop:Color} = 15066597
    ?InvoiceNumbers{prop:Trn} = 0
    ?tmp:StartNumber:Prompt{prop:FontColor} = -1
    ?tmp:StartNumber:Prompt{prop:Color} = 15066597
    If ?tmp:StartNumber{prop:ReadOnly} = True
        ?tmp:StartNumber{prop:FontColor} = 65793
        ?tmp:StartNumber{prop:Color} = 15066597
    Elsif ?tmp:StartNumber{prop:Req} = True
        ?tmp:StartNumber{prop:FontColor} = 65793
        ?tmp:StartNumber{prop:Color} = 8454143
    Else ! If ?tmp:StartNumber{prop:Req} = True
        ?tmp:StartNumber{prop:FontColor} = 65793
        ?tmp:StartNumber{prop:Color} = 16777215
    End ! If ?tmp:StartNumber{prop:Req} = True
    ?tmp:StartNumber{prop:Trn} = 0
    ?tmp:StartNumber{prop:FontStyle} = font:Bold
    ?tmp:EndNumber:Prompt{prop:FontColor} = -1
    ?tmp:EndNumber:Prompt{prop:Color} = 15066597
    If ?tmp:EndNumber{prop:ReadOnly} = True
        ?tmp:EndNumber{prop:FontColor} = 65793
        ?tmp:EndNumber{prop:Color} = 15066597
    Elsif ?tmp:EndNumber{prop:Req} = True
        ?tmp:EndNumber{prop:FontColor} = 65793
        ?tmp:EndNumber{prop:Color} = 8454143
    Else ! If ?tmp:EndNumber{prop:Req} = True
        ?tmp:EndNumber{prop:FontColor} = 65793
        ?tmp:EndNumber{prop:Color} = 16777215
    End ! If ?tmp:EndNumber{prop:Req} = True
    ?tmp:EndNumber{prop:Trn} = 0
    ?tmp:EndNumber{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Invoice_Date_Range',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Invoice_Date_Range',1)
    SolaceViewVars('save_inv_ali_id',save_inv_ali_id,'Invoice_Date_Range',1)
    SolaceViewVars('save_inv_id',save_inv_id,'Invoice_Date_Range',1)
    SolaceViewVars('save_tra_id',save_tra_id,'Invoice_Date_Range',1)
    SolaceViewVars('pos',pos,'Invoice_Date_Range',1)
    SolaceViewVars('save_sub_id',save_sub_id,'Invoice_Date_Range',1)
    SolaceViewVars('start_date_temp',start_date_temp,'Invoice_Date_Range',1)
    SolaceViewVars('end_Date_temp',end_Date_temp,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:accountnumber',tmp:accountnumber,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:SelectAccount',tmp:SelectAccount,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:SelectBatch',tmp:SelectBatch,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:BatchNumber',tmp:BatchNumber,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:InvoiceType',tmp:InvoiceType,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:TradeList',tmp:TradeList,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:StartNumber',tmp:StartNumber,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:EndNumber',tmp:EndNumber,'Invoice_Date_Range',1)
    SolaceViewVars('tmp:RangeType',tmp:RangeType,'Invoice_Date_Range',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SelectAccount;  SolaceCtrlName = '?tmp:SelectAccount';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:accountnumber;  SolaceCtrlName = '?tmp:accountnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SelectBatch;  SolaceCtrlName = '?tmp:SelectBatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchNumber:Prompt;  SolaceCtrlName = '?tmp:BatchNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchNumber;  SolaceCtrlName = '?tmp:BatchNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType;  SolaceCtrlName = '?tmp:InvoiceType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType:Radio1;  SolaceCtrlName = '?tmp:InvoiceType:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType:Radio2;  SolaceCtrlName = '?tmp:InvoiceType:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:InvoiceType:Radio3;  SolaceCtrlName = '?tmp:InvoiceType:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RangeType;  SolaceCtrlName = '?tmp:RangeType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio1;  SolaceCtrlName = '?Option2:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option2:Radio2;  SolaceCtrlName = '?Option2:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceDates;  SolaceCtrlName = '?InvoiceDates';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global1:Prompt;  SolaceCtrlName = '?Select_Global1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?start_date_temp;  SolaceCtrlName = '?start_date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global2:Prompt;  SolaceCtrlName = '?Select_Global2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?end_Date_temp;  SolaceCtrlName = '?end_Date_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceNumbers;  SolaceCtrlName = '?InvoiceNumbers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartNumber:Prompt;  SolaceCtrlName = '?tmp:StartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartNumber;  SolaceCtrlName = '?tmp:StartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndNumber:Prompt;  SolaceCtrlName = '?tmp:EndNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndNumber;  SolaceCtrlName = '?tmp:EndNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button4;  SolaceCtrlName = '?Button4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Invoice_Date_Range')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Invoice_Date_Range')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:SelectAccount
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:INVOICE.Open
  Relate:INVOICE_ALIAS.Open
  Relate:TRADETMP.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  start_date_temp = Deformat('1/1/1990',@d6b)
  end_date_temp = Today()
  Display()
  Do RecolourWindow
  ?start_date_temp{Prop:Alrt,255} = MouseLeft2
  ?end_Date_temp{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?tmp:SelectAccount{Prop:Checked} = True
    ENABLE(?tmp:accountnumber)
  END
  IF ?tmp:SelectAccount{Prop:Checked} = False
    DISABLE(?tmp:accountnumber)
  END
  IF ?tmp:SelectBatch{Prop:Checked} = True
    ENABLE(?tmp:BatchNumber)
  END
  IF ?tmp:SelectBatch{Prop:Checked} = False
    DISABLE(?tmp:BatchNumber)
  END
  FDCB7.Init(tmp:accountnumber,?tmp:accountnumber,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADETMP,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(tratmp:AccountNoKey)
  FDCB7.AddField(tratmp:Account_Number,FDCB7.Q.tratmp:Account_Number)
  FDCB7.AddField(tratmp:RefNumber,FDCB7.Q.tratmp:RefNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:INVOICE_ALIAS.Close
    Relate:TRADETMP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Invoice_Date_Range',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tmp:SelectAccount
      If ~0{prop:acceptall}
          IF tmp:TradeList = 0
              If tmp:SelectAccount = 1
                  Case MessageEx('It will take a few moments to build the list of available accounts.<13,10><13,10>Do you wish to continue?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          TradeList('INVOICE')
                          tmp:TradeList = 1
                      Of 2 ! &No Button
                          tmp:SelectAccount = 0
                  End!Case MessageEx
              End!If tmp:SelectAccount = 1
          End!IF tmp:TradeList = 0
      End!If ~0{prop:acceptall}
      FDCB7.ResetQueue(1)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:SelectAccount
      IF ?tmp:SelectAccount{Prop:Checked} = True
        ENABLE(?tmp:accountnumber)
      END
      IF ?tmp:SelectAccount{Prop:Checked} = False
        DISABLE(?tmp:accountnumber)
      END
      ThisWindow.Reset
    OF ?tmp:SelectBatch
      IF ?tmp:SelectBatch{Prop:Checked} = True
        ENABLE(?tmp:BatchNumber)
      END
      IF ?tmp:SelectBatch{Prop:Checked} = False
        DISABLE(?tmp:BatchNumber)
      END
      ThisWindow.Reset
    OF ?tmp:RangeType
      Case tmp:RangeType
          Of 0!Dates
              ?InvoiceDates{prop:Disable} = 0
              ?InvoiceNumbers{prop:Disable} = 1
          Of 1!Numbers
              ?InvoiceNumbers{prop:Disable} = 0
              ?InvoiceDates{prop:Disable} = 1
      End !tmp:RangeType
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          start_date_temp = TINCALENDARStyle1()
          Display(?start_date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          end_Date_temp = TINCALENDARStyle1(end_Date_temp)
          Display(?end_Date_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Button4
      ThisWindow.Update
      error# = 0
      If tmp:selectaccount = 1
          If tmp:accountnumber = ''
              Select(?tmp:accountnumber)
              error# = 1
          End!If tmp:accountnumber = ''
      End!If tmp:selectaccount = 1
      
      If tmp:selectbatch = 1
          If tmp:batchnumber = ''
              Select(?tmp:batchnumber)
              error# = 1
          End!If tmp:batchnumber = ''
      End!If tmp:selectbatch = 1
      If error# = 0
          RecordsToProcess = 0
          If tmp:RangeType = 0
      
              setcursor(cursor:wait)
              save_inv_ali_id = access:INVOICE_ALIAS.savefile()
              access:INVOICE_ALIAS.clearkey(inv_ali:date_created_key)
              inv_ali:date_created = start_date_temp
              set(inv_ali:date_created_key,inv_ali:date_created_key)
              loop
                  if access:INVOICE_ALIAS.next()
                     break
                  end !if
                  if inv_ali:date_created > end_Date_temp      |
                      then break.  ! end if
                  RecordsToProcess += 1
                  If RecordsToProcess > 5000
                      RecordsToProcess = Records(INVOICE)
                      Break
                  End !If RecordsToProcess > 5000
      
              end !loop
              access:INVOICE_ALIAS.restorefile(save_inv_ali_id)
              setcursor()
      
          Else
              RecordsToProcess = tmp:EndNumber - tmp:StartNumber
          End !If tmp:RangeType = 0
      
          recordspercycle         = 25
          recordsprocessed        = 0
          percentprogress         = 0
          progress:thermometer    = 0
          thiswindow.reset(1)
          open(progresswindow)
      
          ?progress:userstring{prop:text} = 'Running...'
          ?progress:pcttext{prop:text} = '0% Completed'
      
      
          save_inv_ali_id = access:INVOICE_ALIAS.savefile()
          Case tmp:RangeType
              Of 0
                  access:INVOICE_ALIAS.clearkey(inv_ali:date_created_key)
                  inv_ali:date_created = start_date_temp
                  set(inv_ali:date_created_key,inv_ali:date_created_key)
              Of 1
                  access:INVOICE_ALIAS.clearkey(inv_ali:INVOICE_Number_key)
                  inv_ali:INVOICE_Number = tmp:StartNumber
                  set(inv_ali:INVOICE_Number_Key,inv_ali:INVOICE_Number_Key)
          End !Case tmp:RangeType
          loop
              if access:INVOICE_ALIAS.next()
                 break
              end !if
              Case tmp:RangeType
                  Of 0
                      if inv_ali:date_created > end_Date_temp      |
                          then break.  ! end if
      
                  Of 1
                      If inv_ali:INVOICE_Number > tmp:EndNumber
                          Break
                      End !If inv_ali:INVOICE_ALIAS_Number > tmp:EndNumber
              End !Case tmp:RangeType
      
              Do GetNextRecord2
              cancelcheck# += 1
              If cancelcheck# > (RecordsToProcess/100)
                  Do cancelcheck
                  If tmp:cancel = 1
                      Break
                  End!If tmp:cancel = 1
                  cancelcheck# = 0
              End!If cancelcheck# > 50
      
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              If tmp:SelectAccount = 1
                  If inv_ali:account_number <> tmp:accountnumber
                      Cycle
                  End!If inv_ali:account_number <> tmp:account"
              End!If tmp:account" <> ''
              If tmp:SelectBatch = 1
                  If inv_ali:batch_number <> tmp:BatchNumber
                      Cycle
                  End!If inv_ali:batch_number <> batch"
              End!If tmp:batch" <> ''
              glo:select1  = inv_ali:INVOICE_number
              glo:select2  = inv_ali:batch_number
              glo:select3  = inv_ali:account_number
              Case tmp:INVOICEType
                  Of 2 !Service Only
                      If inv_ali:INVOICE_Type = 'RET'
                          Cycle
                      End!If inv_ali:INVOICE_ALIAS_Type = 'RET'
                  Of 3 !Retail Only
                      If inv_ali:INVOICE_type <> 'RET'
                          Cycle
                      End!If inv_ali:INVOICE_ALIAS_type <> 'RET'
              End!Case tmp:INVOICE_ALIASType
              Case inv_ali:INVOICE_type
                  Of 'CHA'
                      Chargeable_Summary_Invoice
                  Of 'SIN'
                      Single_Invoice
                  Of 'WAR'
                      Summary_Warranty_Invoice
                  Of 'RET'
                      Retail_Single_Invoice
                  Of 'BAT'
                      Chargeable_Batch_Invoice
              End
      
          end !loop
          access:INVOICE_ALIAS.restorefile(save_inv_ali_id)
          Do EndPrintRun
          close(progresswindow)
      
          glo:select1  = ''
          glo:select2  = ''
          glo:select3  = ''
      
      End!If error# = 0
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Invoice_Date_Range')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?start_date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?end_Date_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?start_date_temp
    CASE EVENT()
    OF EVENT:AlertKey
      start_date_temp = Deformat('1/1/1990',@d6b)
      Display(?start_date_temp)
    END
  OF ?end_Date_temp
    CASE EVENT()
    OF EVENT:AlertKey
      end_date_temp = Today()
      Display(?end_Date_temp)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

