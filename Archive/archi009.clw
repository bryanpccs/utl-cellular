

   MEMBER('archive.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ARCHI009.INC'),ONCE        !Local module procedure declarations
                     END


UnArchive PROCEDURE                                   !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:LivePath         STRING(255)
tmp:StartJobNumber   LONG
tmp:EndJobNumber     LONG
save_job_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_cht_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
save_jobarc_id       USHORT,AUTO
save_audarc_id       USHORT,AUTO
save_chtarc_id       USHORT,AUTO
save_ausarc_id       USHORT,AUTO
save_jst_id          USHORT,AUTO
save_jstarc_id       USHORT,AUTO
save_jbn_id          USHORT,AUTO
save_jbnarc_id       USHORT,AUTO
save_jot_id          USHORT,AUTO
save_jotarc_id       USHORT,AUTO
save_jobe_id         USHORT,AUTO
save_jobearc_id      USHORT,AUTO
save_par_id          USHORT,AUTO
save_pararc_id       USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_wprarc_id       USHORT,AUTO
save_epr_id          USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_eprarc_id       USHORT,AUTO
save_jptarc_id       USHORT,AUTO
tmp:Completed        BYTE(1)
tmp:Despatched       BYTE(1)
tmp:Invoiced         BYTE(1)
tmp:JobsRestored     LONG
mo:SelectedTab::Sheet1    Long  ! Makeover Template      LocalTreat = Wizard
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Wizard
mo:SelectedField  Long
mo:WinType        equate(Win:Wizard)
window               WINDOW('UnArchive Routine'),AT(,,423,203),FONT('Arial',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,416,168),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('UN-ARCHIVE ROUTINE'),AT(110,8),USE(?Prompt1),FONT(,20,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           PROMPT('Data Files'),AT(8,32),USE(?Prompt5),FONT(,,,FONT:bold+FONT:underline)
                           PROMPT('Select the job number range of archive files:'),AT(8,120),USE(?Prompt8)
                           PROMPT('Start Job Number'),AT(8,136),USE(?tmp:StartJobNumber:Prompt),TRN
                           ENTRY(@s8),AT(84,132,64,10),USE(tmp:StartJobNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Start Job Number'),TIP('Start Job Number'),UPR
                           PROMPT('End Job Number'),AT(8,152),USE(?tmp:EndJobNumber:Prompt),TRN
                           ENTRY(@s8),AT(84,152,64,10),USE(tmp:EndJobNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('End Job Number'),TIP('End Job Number'),UPR
                           PROMPT('Archive Data Files Path:'),AT(8,44),USE(?Prompt2),FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s255),AT(164,44,248,12),USE(tmp:LivePath),FONT(,12,COLOR:Navy,FONT:bold)
                           STRING(@s255),AT(164,68,248,12),USE(DEF:ArchivePath),FONT(,12,COLOR:Green,FONT:bold)
                           PROMPT('Important: The Archive Data File Path CANNOT be the same as the Live Data File P' &|
   'ath'),AT(24,92),USE(?Prompt4),FONT('Arial',10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           PROMPT('ServiceBase Data Files Path:'),AT(8,68),USE(?Prompt2:2),FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(4,176,416,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Begin UnArchive'),AT(8,180,68,16),USE(?BeginArchive),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(360,180,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
CloseFiles      Routine
        Access:JOBS.Close()
        Access:JOBSARC.Close()
        Access:AUDIT.Close()
        Access:AUDITARC.Close()
        Access:AUDSTATS.Close()
        Access:AUDSTATSARC.Close()
        Access:CONTHIST.Close()
        Access:CONTHISTARC.Close()
        Access:JOBNOTES.Close()
        Access:JOBNOTESARC.Close()
        Access:JOBSE.Close()
        Access:JOBSEARC.Close()
        Access:JOBSTAGE.Close()
        Access:JOBSTAGEARC.Close()
        Access:JOBTHIRD.Close()
        Access:JOBTHIRDARC.Close()
        Access:PARTS.Close()
        Access:PARTSARC.Close()
        Access:WARPARTS.Close()
        Access:WARPARTSARC.Close()
        Access:ESTPARTS.Close()
        Access:ESTPARTSARC.Close()
        Access:JOBPAYMT.Close()
        Access:JOBPAYMTARC.Close()
OpenFiles       Routine
        Access:JOBS.Open()
        Access:JOBS.Usefile()
        Access:JOBSARC.Open()
        Access:JOBSARC.UseFile()
        Access:AUDIT.Open()
        Access:AUDIT.Usefile()
        Access:AUDITARC.Open()
        Access:AUDITARC.Usefile()
        Access:AUDSTATS.Open()
        Access:AUDSTATS.Usefile()
        Access:AUDSTATSARC.Open()
        Access:AUDSTATSARC.Usefile()
        Access:CONTHIST.Open()
        Access:CONTHIST.Usefile()
        Access:CONTHISTARC.Open()
        Access:CONTHISTARC.Usefile()
        Access:JOBNOTES.Open()
        Access:JOBNOTES.Usefile()
        Access:JOBNOTESARC.Open()
        Access:JOBNOTESARC.UseFile()
        Access:JOBSE.Open()
        Access:JOBSE.Usefile()
        Access:JOBSEARC.Open()
        Access:JOBSEARC.Usefile()
        Access:JOBSTAGE.Open()
        Access:JOBSTAGE.Usefile()
        Access:JOBSTAGEARC.Open()
        Access:JOBSTAGEARC.Usefile()
        Access:JOBTHIRD.Open()
        Access:JOBTHIRD.Usefile()
        Access:JOBTHIRDARC.Open()
        Access:JOBTHIRDARC.Usefile()
        Access:PARTS.Open()
        Access:PARTS.Usefile()
        Access:PARTSARC.Open()
        Access:PARTSARC.Usefile()
        Access:WARPARTS.Open()
        Access:WARPARTS.Usefile()
        Access:WARPARTSARC.Open()
        Access:WARPARTSARC.Usefile()
        Access:ESTPARTS.Open()
        Access:ESTPARTS.UseFile()
        Access:ESTPARTSARC.Open()
        Access:ESTPARTSARC.UseFile()
        Access:JOBPAYMT.Open()
        Access:JOBPAYMT.UseFile()
        Access:JOBPAYMTARC.Open()
        Access:JOBPAYMTARC.UseFile()

        
        


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UnArchive',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:LivePath',tmp:LivePath,'UnArchive',1)
    SolaceViewVars('tmp:StartJobNumber',tmp:StartJobNumber,'UnArchive',1)
    SolaceViewVars('tmp:EndJobNumber',tmp:EndJobNumber,'UnArchive',1)
    SolaceViewVars('save_job_id',save_job_id,'UnArchive',1)
    SolaceViewVars('save_aud_id',save_aud_id,'UnArchive',1)
    SolaceViewVars('save_cht_id',save_cht_id,'UnArchive',1)
    SolaceViewVars('save_aus_id',save_aus_id,'UnArchive',1)
    SolaceViewVars('save_jobarc_id',save_jobarc_id,'UnArchive',1)
    SolaceViewVars('save_audarc_id',save_audarc_id,'UnArchive',1)
    SolaceViewVars('save_chtarc_id',save_chtarc_id,'UnArchive',1)
    SolaceViewVars('save_ausarc_id',save_ausarc_id,'UnArchive',1)
    SolaceViewVars('save_jst_id',save_jst_id,'UnArchive',1)
    SolaceViewVars('save_jstarc_id',save_jstarc_id,'UnArchive',1)
    SolaceViewVars('save_jbn_id',save_jbn_id,'UnArchive',1)
    SolaceViewVars('save_jbnarc_id',save_jbnarc_id,'UnArchive',1)
    SolaceViewVars('save_jot_id',save_jot_id,'UnArchive',1)
    SolaceViewVars('save_jotarc_id',save_jotarc_id,'UnArchive',1)
    SolaceViewVars('save_jobe_id',save_jobe_id,'UnArchive',1)
    SolaceViewVars('save_jobearc_id',save_jobearc_id,'UnArchive',1)
    SolaceViewVars('save_par_id',save_par_id,'UnArchive',1)
    SolaceViewVars('save_pararc_id',save_pararc_id,'UnArchive',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'UnArchive',1)
    SolaceViewVars('save_wprarc_id',save_wprarc_id,'UnArchive',1)
    SolaceViewVars('save_epr_id',save_epr_id,'UnArchive',1)
    SolaceViewVars('save_jpt_id',save_jpt_id,'UnArchive',1)
    SolaceViewVars('save_eprarc_id',save_eprarc_id,'UnArchive',1)
    SolaceViewVars('save_jptarc_id',save_jptarc_id,'UnArchive',1)
    SolaceViewVars('tmp:Completed',tmp:Completed,'UnArchive',1)
    SolaceViewVars('tmp:Despatched',tmp:Despatched,'UnArchive',1)
    SolaceViewVars('tmp:Invoiced',tmp:Invoiced,'UnArchive',1)
    SolaceViewVars('tmp:JobsRestored',tmp:JobsRestored,'UnArchive',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartJobNumber:Prompt;  SolaceCtrlName = '?tmp:StartJobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartJobNumber;  SolaceCtrlName = '?tmp:StartJobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndJobNumber:Prompt;  SolaceCtrlName = '?tmp:EndJobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndJobNumber;  SolaceCtrlName = '?tmp:EndJobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LivePath;  SolaceCtrlName = '?tmp:LivePath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DEF:ArchivePath;  SolaceCtrlName = '?DEF:ArchivePath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BeginArchive;  SolaceCtrlName = '?BeginArchive';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UnArchive')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UnArchive')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFARC.Open
  SELF.FilesOpened = True
  Set(DEFARC)
  Access:DEFARC.Next()
  If Error()
      If Access:DEFARC.PrimeRecord() = Level:Benign
          def:ArchivePath = Clip(Path())
          If Access:DEFARC.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFARC.TryInsert() = Level:Benign
              !Insert Failed
          End!If Access:DEFARC.TryInsert() = Level:Benign
      End !If Access:DEFARC.PrimeRecord() = Level:Benign
  End !Error()
  
  tmp:LivePath    = Path()
  OPEN(window)
  SELF.Opened=True
  ! support for CPCS
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFARC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UnArchive',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?BeginArchive
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BeginArchive, Accepted)
      Error# = 0
      If tmp:EndJobNumber < tmp:StartJobNumber And Error# = 0
          Error# = 1
          Case MessageEx('The end job number is lower than the start job number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !tmp:EndJobNumber < tmp:StartJobNumber.
      If tmp:EndJobNumber = 0 or tmp:StartJobNumber = 0 And Error# =0
          Error#  = 1
          Case MessageEx('Invalid Job Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !tmp:EndJobNumber = 0 or tmp:StartJobNumber = 0
      If tmp:LivePath = def:ArchivePath and Error# = 0
          Case MessageEx('The Live Data File Path and the Archive Data File Path cannot be the same.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !tmp:LivePath = def:ArchivePath
      
      Countslash# = 0
      Loop x# = 1 To Len(def:ArchivePath)
          If Sub(def:ArchivePath,x#,1) = '\'
              countslash# += 1
              If Countslash# > 1
                  Break
              End !If Countslash# > 1
          End !If Sub(def:ArchivePath,1,1) = '\'
      End !x# = 1 To Len(def:ArchivePath)
      If Countslash# = 0 And Error# = 0
          Error# = 1
          Case MessageEx('Invalid Archive Path.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !Countslash# = 1
      
      Case MessageEx('You have chosen to UnArchive any jobs between Job Numbers ' & tmp:StartJobNumber & ' and ' & tmp:EndJobNumber & '.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              If Sub(def:ArchivePath,Len(Clip(def:ArchivePath)),1) = '\'
                  def:ArchivePath = Sub(def:ArchivePath,1,Len(Clip(def:ArchivePath)) - 1)
              End !If Sub(def:ArchivePath,Len(Clip(def:ArchivePath)),1) = '\'
      
              GLO:FileNameJobs        = Clip(def:ArchivePath) & '\JOBS.DAT'
              GLO:FileNameAudit       = Clip(def:ArchivePath) & '\AUDIT.DAT'
              GLO:FileNameJobStage    = Clip(def:ArchivePath) & '\JOBSTAGE.DAT'
              GLO:FileNameJobThird    = Clip(def:ArchivePath) & '\JOBTHIRD.DAT'
              GLO:FileNameParts       = Clip(def:ArchivePath) & '\PARTS.DAT'
              GLO:FileNameWarParts    = Clip(def:ArchivePath) & '\WARPARTS.DAT'
              GLO:FileNameContHist    = Clip(def:ArchivePath) & '\CONTHIST.DAT'
              GLO:FileNameAudStats    = Clip(def:ArchivePath) & '\AUDSTATS.DAT'
              GLO:FileNameJobse       = Clip(def:ArchivePath) & '\JOBSE.DAT'
              GLO:FileNameJobNotes    = Clip(def:ArchivePath) & '\JOBNOTES.DAT'
              GLO:FileNameESTPARTS    = Clip(def:ArchivePath) & '\ESTPARTS.DAT'
              GLO:FileNameJOBPAYMT    = Clip(def:ArchivePath) & '\JOBPAYMT.DAT'
      
      !        Rename('JOBS.DAT','AOBS.DAT')
      !        Rename('JOBS.^01','AOBS.^01')
      !        Rename('JOBS.^02','AOBS.^02')
      !        Rename('JOBS.^03','AOBS.^03')
      !        Rename('JOBS.^04','AOBS.^04')
      !        Rename('JOBS.^05','AOBS.^05')
      !        Rename('JOBS.^06','AOBS.^06')
      
              Do OpenFiles
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              thiswindow.reset(1)
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Running...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
              recordstoprocess    = tmp:EndJobNumber - tmp:StartJobNumber
      
              tmp:JobsRestored = 0
      
              Save_JOBARC_ID = Access:JOBSARC.SaveFile()
              Access:JOBSARC.ClearKey(JOBARC:Ref_Number_Key)
              JOBARC:Ref_Number = tmp:StartJobNumber
              Set(JOBARC:Ref_Number_Key,JOBARC:Ref_Number_Key)
              Loop
                  If Access:JOBSARC.NEXT()
                     Break
                  End !If
                  If JOBARC:Ref_Number > tmp:EndJobNumber      |
                      Then Break.  ! End If
                  ?progress:userstring{prop:text} = 'Jobs Restored: ' & Clip(tmp:JobsRestored)
                  Do GetNextRecord2
                  cancelcheck# += 1
                  If cancelcheck# > (RecordsToProcess/100)
                      Do cancelcheck
                      If tmp:cancel = 1
                          Break
                      End!If tmp:cancel = 1
                      cancelcheck# = 0
                  End!If cancelcheck# > 50
      
                  If Access:JOBS.PrimeRecord() = Level:Benign
                      job:Record  :=: jobarc:Record
                      If Access:JOBS.TryInsert() = Level:Benign
                          !Insert Successful
      
                          Save_audarc_ID = Access:AUDITARC.SaveFile()
                          Access:AUDITARC.ClearKey(audarc:RefNumberKey)
                          audarc:Ref_Number = job:Ref_Number
                          Set(audarc:RefNumberKey,audarc:RefNumberKey)
                          Loop
                              If Access:AUDITARC.NEXT()
                                 Break
                              End !If
                              If audarc:Ref_Number <> job:Ref_Number      |
                                  Then Break.  ! End If
                              If Access:AUDIT.PrimeRecord() = Level:Benign
                                  aud:Record :=: audarc:Record
                                  If Access:AUDIT.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:AUDIT.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:AUDIT.TryInsert() = Level:Benign
                              End !If Access:AUDIT.PrimeRecord() = Level:Benign
      
                          End !Loop
                          Access:AUDITARC.RestoreFile(Save_audarc_ID)
      
                          Save_ausarc_ID = Access:AUDSTATSARC.SaveFile()
                          Access:AUDSTATSARC.ClearKey(ausarc:RefNumberKey)
                          ausarc:RefNumber = job:Ref_Number
                          Set(ausarc:RefNumberKey,ausarc:RefNumberKey)
                          Loop
                              If Access:AUDSTATSARC.NEXT()
                                 Break
                              End !If
                              If ausarc:RefNumber <> job:Ref_Number      |
                                  Then Break.  ! End If
                              If Access:AUDSTATS.PrimeRecord() = Level:Benign
                                  aus:Record :=: ausarc:Record
                                  If Access:AUDSTATS.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:AUDSTAT.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:AUDSTAT.TryInsert() = Level:Benign
                              End !If Access:AUDSTAT.PrimeRecord() = Level:Benign
      
                          End !Loop
                          Access:AUDSTATSARC.RestoreFile(Save_ausarc_ID)
      
                          Save_chtarc_ID = Access:CONTHISTARC.SaveFile()
                          Access:CONTHISTARC.ClearKey(chtarc:RefNumberKey)
                          chtarc:Ref_Number = job:Ref_Number
                          Set(chtarc:RefNumberKey,chtarc:RefNumberKey)
                          Loop
                              If Access:CONTHISTARC.NEXT()
                                 Break
                              End !If
                              If chtarc:Ref_Number <> job:Ref_Number      |
                                  Then Break.  ! End If
                              If Access:CONTHIST.PrimeRecord() = Level:Benign
                                  cht:Record :=: chtarc:Record
                                  If Access:CONTHIST.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:CONTHIST.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:CONTHIST.TryInsert() = Level:Benign
                              End !If Access:CONTHIST.PrimeRecord() = Level:Benign
      
                          End !Loop
                          Access:CONTHISTARC.RestoreFile(Save_chtarc_ID)
      
                          Access:JOBNOTESARC.ClearKey(jbnarc:RefNumberKey)
                          jbnarc:RefNumber = job:Ref_Number
                          If Access:JOBNOTESARC.TryFetch(jbnarc:RefNumberKey) = Level:Benign
                              !Found
                              If Access:JOBNOTES.PrimeRecord() = Level:Benign
                                  jbn:Record :=: jbnarc:Record
                                  If Access:JOBNOTES.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:JOBNOTES.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:JOBNOTES.TryInsert() = Level:Benign
                              End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
                          Else!If Access:JOBNOTESARC.TryFetch(jbnarc:RefNumberKey) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:JOBNOTESARC.TryFetch(jbnarc:RefNumberKey) = Level:Benign
      
                          Access:JOBSEARC.ClearKey(jobearc:RefNumberKey)
                          jobearc:RefNumber = job:Ref_Number
                          If Access:JOBSEARC.TryFetch(jobearc:RefNumberKey) = Level:Benign
                              !Found
                              If Access:JOBSE.PrimeRecord() = Level:Benign
                                  jobe:Record :=: jobearc:Record
                                  If Access:JOBSE.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:JOBSE.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End!If Access:JOBSE.TryInsert() = Level:Benign
                              End !If Access:JOBSE.PrimeRecord() = Level:Benign
                          Else!If Access:JOBSEARC.TryFetch(jobearc:RefNumberKey) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:JOBSEARC.TryFetch(jobearc:RefNumberKey) = Level:Benign
      
                          Save_jstarc_ID = Access:JOBSTAGEARC.SaveFile()
                          Access:JOBSTAGEARC.ClearKey(jstarc:Ref_Number_Key)
                          jstarc:Ref_Number = job:Ref_Number
                          Set(jstarc:Ref_Number_Key,jstarc:Ref_Number_Key)
                          Loop
                              If Access:JOBSTAGEARC.NEXT()
                                 Break
                              End !If
                              If jstarc:Ref_Number <> job:Ref_Number      |
                                  Then Break.  ! End If
                              If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                                  jst:Record :=: jstarc:Record
                                  If Access:JOBSTAGE.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:JOBSTAGE.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:JOBSTAGE.TryInsert() = Level:Benign
                              End !If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                          End !Loop
                          Access:JOBSTAGEARC.RestoreFile(Save_jstarc_ID)
                      Else !If Access:JOBS.TryInsert() = Level:Benign
                          !Insert Failed
                      End!If Access:JOBS.TryInsert() = Level:Benign
      
                      Save_jotarc_ID = Access:JOBTHIRDARC.SaveFile()
                      Access:JOBTHIRDARC.ClearKey(jotarc:RefNumberKey)
                      jotarc:RefNumber = job:Ref_Number
                      Set(jotarc:RefNumberKey,jotarc:RefNumberKey)
                      Loop
                          If Access:JOBTHIRDARC.NEXT()
                             Break
                          End !If
                          If jotarc:RefNumber <> job:Ref_Number      |
                              Then Break.  ! End If
                          If Access:JOBTHIRD.PrimeRecord() = Level:Benign
                              jot:Record :=: jotarc:Record
                              If Access:JOBTHIRD.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBTHIRD.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:JOBTHIRD.TryInsert() = Level:Benign
                          End !If Access:JOBTHIRD.PrimeRecord() = Level:Benign
                      End !Loop
                      Access:JOBTHIRDARC.RestoreFile(Save_jotarc_ID)
      
                      Save_pararc_ID = Access:PARTSARC.SaveFile()
                      Access:PARTSARC.ClearKey(pararc:Part_Number_Key)
                      pararc:Ref_Number = job:Ref_Number
                      Set(pararc:Part_Number_Key,pararc:Part_Number_Key)
                      Loop
                          If Access:PARTSARC.NEXT()
                             Break
                          End !If
                          If pararc:Ref_Number <> job:Ref_Number      |
                              Then Break.  ! End If
                          If Access:PARTS.PrimeRecord() = Level:Benign
                              par:Record :=: pararc:Record
                              If Access:PARTS.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:PARTS.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:PARTS.TryInsert() = Level:Benign
                          End !If Access:PARTS.PrimeRecord() = Level:Benign
                      End !Loop
                      Access:PARTSARC.RestoreFile(Save_pararc_ID)
      
                      Save_wprarc_ID = Access:WARPARTSARC.SaveFile()
                      Access:WARPARTSARC.ClearKey(wprarc:Part_Number_Key)
                      wprarc:Ref_Number = job:Ref_Number
                      Set(wprarc:Part_Number_Key,wprarc:Part_Number_Key)
                      Loop
                          If Access:WARPARTSARC.NEXT()
                             Break
                          End !If
                          If wprarc:Ref_Number <> job:Ref_Number      |
                              Then Break.  ! End If
                          If Access:WARPARTS.PrimeRecord() = Level:Benign
                              wpr:Record :=: wprarc:Record
                              If Access:WARPARTS.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:WARPARTS.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:WARPARTS.TryInsert() = Level:Benign
                          End !If Access:WARPARTS.PrimeRecord() = Level:Benign
                      End !Loop
                      Access:WARPARTSARC.RestoreFile(Save_wprarc_ID)
      
                      Save_eprarc_ID = Access:ESTPARTSARC.SaveFile()
                      Access:ESTPARTSARC.ClearKey(eprarc:Part_Number_Key)
                      eprarc:Ref_Number  = job:Ref_Number
                      Set(eprarc:Part_Number_Key,eprarc:Part_Number_Key)
                      Loop
                          If Access:ESTPARTSARC.NEXT()
                             Break
                          End !If
                          If eprarc:Ref_Number  <> job:Ref_Number      |
                              Then Break.  ! End If
                          If Access:ESTPARTS.PrimeRecord() = Level:Benign
                              epr:Record  :=: eprarc:Record
                              If Access:ESTPARTS.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:ESTPARTS.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:ESTPARTS.TryInsert() = Level:Benign
                          End !If Access:ESTPARTS.PrimeRecord() = Level:Benign
                      End !Loop
                      Access:ESTPARTSARC.RestoreFile(Save_eprarc_ID)
      
                      Save_jptarc_ID = Access:JOBPAYMTARC.SaveFile()
                      Access:JOBPAYMTARC.ClearKey(jptarc:All_Date_Key)
                      jptarc:Ref_Number = job:Ref_Number
                      Set(jptarc:All_Date_Key,jptarc:All_Date_Key)
                      Loop
                          If Access:JOBPAYMTARC.NEXT()
                             Break
                          End !If
                          If jptarc:Ref_Number <> job:Ref_Number      |
                              Then Break.  ! End If
                          If Access:JOBPAYMT.PrimeRecord() = Level:Benign
                              jpt:Record  :=: jptarc:Record
                              If Access:JOBPAYMT.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBPAYMT.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:JOBPAYMT.TryInsert() = Level:Benign
                          End !If Access:JOBPAYMT.PrimeRecord() = Level:Benign
                      End !Loop
                      Access:JOBPAYMTARC.RestoreFile(Save_jptarc_ID)
                      tmp:JobsRestored += 1
                  End !If Access:JOBS.PrimeRecord() = Level:Benign
      
              End !Loop
              Access:JOBSARC.RestoreFile(Save_JOBARC_ID)
      
              Do EndPrintRun
              close(progresswindow)
      
              Do CloseFiles
      
      
              Case MessageEx('The UnArchive has completed.','ServiceBase 2000',|
                             'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Of 2 ! &No Button
      
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BeginArchive, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UnArchive')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Wizard,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab::Sheet1,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

