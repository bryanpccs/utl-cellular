

   MEMBER('archive.clw')                              ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ARCHI006.INC'),ONCE        !Local module procedure declarations
                     END


DebugToolbox PROCEDURE (P:Message)                    !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
SOLLocal             FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLVVF),CREATE
Record                 RECORD,PRE()
ProcName                CString(60)
VariableName            CString(60)
VariableValue           CString(2048)
                       END
                     END                       
SOLGlobal             FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLGLOF),CREATE
Record                 RECORD,PRE()
VariableName            CString(60)
VariableValue           CString(2048)
                       END
                     END
SOLFiles             FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLFSF),CREATE
Record                 RECORD,PRE()
FileName                CString(60)
OpenStatus              CString(15)
NoRecords               ULong
                       END
                     END
SOLAllFiles          FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLALLF),CREATE
Record                 RECORD,PRE()
FileName                CString(60)
FieldName               CString(60)
FieldValue              CString(2048)
                       END
                     END
SOLEnviron           FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLENVF),CREATE
Record                 RECORD,PRE()
EnvName                 CString(60)
EnvValue                CString(255)
                       END
                     END
SOLWatch             FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLWATF),CREATE
Record                 RECORD,PRE()
FieldName               CString(60)
OldValue                CString(2048)
FieldValue              CString(2048)
CtrlName                CString(30)
EventName               CString(20)
ProcName                CString(60)
                       END
                     END
SOLEvents            FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLEVTF),CREATE
Record                 RECORD,PRE()
CtrlName                CString(30)
EventName               CString(20)
ProcName                CString(60)
                       END
                     END
SOLMessage            FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLMESF),CREATE
Record                 RECORD,PRE()
Value1                  CString(255)
Value2                  CString(255)
Value3                  CString(255)
                       END
                     END
SolProc              FILE,DRIVER('TOPSPEED'),Name(SolaceApplyFileName),PRE(SOLPROF),CREATE
Record                 RECORD,PRE()
ProcName                CString(255)
                       END
                     end


LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Loc:Minimized        BYTE
LOC:SaveHeight       SHORT
mo:SelectedTab::Sheet1    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Window
mo:SelectedField  Long
mo:WinType        equate(Win:Window)
Window               WINDOW('Solace Variable Viewer'),AT(,,243,224),FONT('MS Sans Serif',8,,FONT:regular),IMM,ICON('wrench.ico'),TOOLBOX,GRAY,RESIZE
                       SHEET,AT(3,2,238,198),USE(?Sheet1),RIGHT
                         TAB('Local'),USE(?Tab1)
                           LIST,AT(9,15,193,180),USE(?List1),VSCROLL,ALRT(MouseLeft2),FORMAT('59L(2)|FM~Procedure~S(200)@s60@67L(2)|FM~Variable Name~S(200)@s60@80L(2)|M~Value' &|
   '~S(1000)@s255@'),FROM(SolaceVarView)
                         END
                         TAB('Global'),USE(?Tab2)
                           LIST,AT(9,15,193,180),USE(?List2),VSCROLL,ALRT(MouseLeft2),FORMAT('81L(2)|FM~Variable Name~S(200)@s60@1020L(2)|M~Variable Value~S(1000)@s255@'),FROM(SolaceGlobView)
                         END
                         TAB('One File'),USE(?Tab3)
                           PROMPT('Filename:'),AT(8,15),USE(?Prompt1)
                           LIST,AT(9,27,193,167),USE(?List3),VSCROLL,ALRT(MouseLeft2),FORMAT('81L(2)|FM~Variable Name~S(200)@s60@80L(2)|M~Value~S(1000)@s255@'),FROM(FileFieldsQueue)
                           LIST,AT(45,15,157,10),USE(?List3:2),VSCROLL,FORMAT('81L(2)|FMS(200)@s60@'),DROP(10),FROM(SolaceFilesNames)
                         END
                         TAB('All Files'),USE(?Tab4)
                           PROMPT('NOTE: Only files open on current thread are shown.'),AT(8,14),USE(?Prompt2),FONT(,,COLOR:Navy,FONT:regular,CHARSET:ANSI)
                           LIST,AT(9,24,193,172),USE(?List3:3),VSCROLL,ALRT(MouseLeft2),FORMAT('64L(2)|FM~Variable Name~S(200)@s60@64L(2)|M~Value~S(1000)@s255@1020L(2)|M~File N' &|
   'ame~S(1000)@s255@'),FROM(SolaceAllFiles)
                         END
                         TAB('File Stat'),USE(?Tab9)
                           LIST,AT(9,15,193,180),USE(?List1:5),VSCROLL,ALRT(MouseLeft2),FORMAT('59L(2)|FM~Filename~S(200)@s60@75L(2)|FM~Open Status (Thread)~S(200)@s60@80L(2)|M' &|
   '~Records~S(1000)@n16b@'),FROM(SolaceFiles)
                         END
                         TAB('Watch'),USE(?Tab6)
                           BUTTON('Clear Watch'),AT(3,207,52,14),USE(?Button4)
                           LIST,AT(9,15,193,180),USE(?List1:4),VSCROLL,ALRT(MouseLeft2),FORMAT('59L(2)|FM~Fieldname~S(200)@s60@51L(2)|FM~Old Value~S(200)@s60@51L(2)|FM~New Valu' &|
   'e~S(200)@s60@49L(2)|M~Control~S(1000)@s255@59L(2)|FM~Event~S(200)@s60@'),FROM(SolaceWatchQueue)
                         END
                         TAB('Environ'),USE(?Tab5)
                           LIST,AT(9,15,193,180),USE(?List1:2),VSCROLL,ALRT(MouseLeft2),FORMAT('87L(2)|FM~Setting~S(200)@s60@67L(2)|FM~Value~S(200)@s60@'),FROM(SolaceEnvQueue)
                         END
                         TAB('Events'),USE(?Tab8)
                           LIST,AT(9,15,193,180),USE(?List1:3),VSCROLL,ALRT(MouseLeft2),FORMAT('59L(2)|FM~Control~S(200)@s60@67L(2)|FM~Event~S(200)@s60@80L(2)|M~Procedure~S(100' &|
   '0)@s255@'),FROM(SolaceEventQueue)
                           BUTTON('Clear Events'),AT(3,207,53,14),USE(?Button3)
                         END
                         TAB('Message'),USE(?Tab10)
                           LIST,AT(9,15,193,180),USE(?List1:6),VSCROLL,ALRT(MouseLeft2),FORMAT('59L(2)|FM~Value 1~S(200)@s255@67L(2)|FM~Value 2~S(200)@s255@80L(2)|M~Value 3~S(1' &|
   '000)@s255@'),FROM(SolaceMessQueue)
                           BUTTON('Clear Messages'),AT(3,207,61,14),USE(?Button7)
                         END
                         TAB('Procs'),USE(?Tab11)
                           LIST,AT(9,15,193,180),USE(?ProcList),VSCROLL,ALRT(MouseLeft2),FORMAT('59L(2)|FM~Procedure~S(200)@s60@'),FROM(SolaceProcsQueue)
                           BUTTON('Clear Procedures'),AT(3,207,61,14),USE(?Button8)
                         END
                       END
                       BUTTON('View'),AT(105,207,45,14),USE(?ViewButton)
                       BUTTON('Save To File'),AT(153,207,48,14),USE(?Button6)
                       BUTTON('Close'),AT(204,207,36,14),USE(?CancelButton)
                       STRING(@s45),AT(9,3,193,10),USE(SolaceCurThreadNo),FONT(,,COLOR:Maroon,,CHARSET:ANSI)
                     END

! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
Resize                 PROCEDURE(),BYTE,PROC,DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
ReFreshSelectedFile     Routine
  Free(FileFieldsQueue)
  Loop x# = 1 to Records(SolaceAllFiles)
    Get(SolaceAllFiles,x#)
    if Clip(SOLALL:FileName) = clip(SOLFN:Filename) then
      SOLFFV:FieldName = SOLALL:FieldName
      SOLFFV:FieldValue = SOLALL:FieldValue
      Add(FileFieldsQueue)
    end
  end
        





! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DebugToolbox')
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(Window)
  SELF.Opened=True
   Loc:Minimized = False
  ?List1{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  ?List3:2{prop:vcr} = TRUE
  ?List3:3{prop:vcr} = TRUE
  ?List1:5{prop:vcr} = TRUE
  ?List1:4{prop:vcr} = TRUE
  ?List1:2{prop:vcr} = TRUE
  ?List1:3{prop:vcr} = TRUE
  ?List1:6{prop:vcr} = TRUE
  ?ProcList{prop:vcr} = TRUE
  ! support for CPCS
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  Window{prop:buffer} = 1
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
   if Window{Prop:Height} < 10 then
     Window{Prop:Height} = 224
   end
   SolaceViewVars('',0,'SolaceGlobalRefresh',1)   !Populate fields
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  Window{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    
    
    CASE ACCEPTED()
    OF ?List3:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3:2, Accepted)
      Get(SolaceFilesNames,Choice(?List3:2))
      SolaceCurrentFile = SOLFN:Filename
      do RefreshSelectedFile
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3:2, Accepted)
    OF ?Button4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
      Free(SolaceWatchQueue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button4, Accepted)
    OF ?Button3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      free(SolaceEventQueue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?Button7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
      Free(SolaceMessQueue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
    OF ?Button8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button8, Accepted)
      Free(SolaceProcsQueue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button8, Accepted)
    OF ?ViewButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewButton, Accepted)
      Case Choice(?Sheet1)
      of 1
        Get(SolaceVarView,Choice(?List1))
        Message('Value for ' & SOLVV:VariableName & '||' & SOLVV:VariableValue,'View Value')
      of 2
        Get(SolaceGlobView,Choice(?List2))
        Message('Value for ' & SOLGLO:VariableName & '||' & SOLGLO:VariableValue,'View Value')
      of 3
        Get(FileFieldsQueue,Choice(?List3))
        Message('Value for ' & SOLFFV:FieldName & '||' & SOLFFV:FieldValue,'View Value')
      of 4
        Get(SolaceAllFiles,Choice(?List3:3))
        Message('Value for ' & SOLALL:FieldName & '||' & SOLALL:FieldValue,'View Value')
      of 6
        Get(SolaceWatchQueue,Choice(?List1:4))
        Message('Watch values for ' & SOLWAT:FieldName & '||Old Value: |' & SOLWAT:OldValue & '||New Value: |' & SOLWAT:FieldValue,'View Value')
      of 9
        Get(SolaceMessQueue,Choice(?List1:6))
        Message('Value 1: ' & SOLMES:Value1 & '||Value 2: |' & SOLMES:Value2 & '||Value 3: |' & SOLMES:Value3,'View Value')
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ViewButton, Accepted)
    OF ?Button6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
      SolaceSaveToFile = true
      SolaceViewVars('',0,'SolaceViewVars',3)
      SolaceViewVars('',0,'SolaceGlobalRefresh',0)
      SolaceSaveToFile = false
      Setcursor(Cursor:Wait)
      !******** Local Variables
      SolaceApplyFileName = SolaceSaveFile & '\!Local'
      Create(SOLLocal)
      Open(SOLLocal,12h)
      Stream(SOLLocal)
      Loop SolDump# = 1 to records(SolaceVarView)
        Get(SolaceVarView,SolDump#)
        SOLVVF:Record :=: SolaceVarView
        Add(SOLLocal)
      end
      Flush(SOLLocal)
      Close(SOLLocal)
      
      !******** Global Variables
      SolaceApplyFileName = SolaceSaveFile & '\!Global'
      Create(SOLGlobal)
      Open(SOLGlobal,12h)
      Stream(SOLGlobal)
      Loop SolDump# = 1 to records(SolaceGlobView)
        Get(SolaceGlobView,SolDump#)
        SOLGLOF:Record :=: SolaceGlobView
        Add(SOLGlobal)
      end
      Flush(SOLGlobal)
      Close(SOLGlobal)
      
      !******* File Status
      SolaceApplyFileName = SolaceSaveFile & '\!Files'
      Create(SOLFiles)
      Open(SOLFiles,12h)
      Stream(SOLFiles)
      Loop SolDump# = 1 to records(SolaceFiles)
        Get(SolaceFiles,SolDump#)
        SOLFSF:Record :=: SolaceFiles
        Add(SOLFiles)
      end
      Flush(SOLFiles)
      Close(SOLFiles)
      
      !******* All files
      SolaceApplyFileName = SolaceSaveFile & '\!AllFiles'
      Create(SOLAllFiles)
      Open(SOLAllFiles,12h)
      Stream(SOLAllFiles)
      Loop SolDump# = 1 to records(SolaceAllFiles)
        Get(SolaceAllFiles,SolDump#)
        SOLALLF:Record :=: SolaceAllFiles
        Add(SOLAllFiles)
      end
      Flush(SOLAllFiles)
      Close(SOLAllFiles)
      
      !******* Watch Variables
      SolaceApplyFileName = SolaceSaveFile & '\!Watch'
      Create(SOLWatch)
      Open(SOLWatch,12h)
      Stream(SOLWatch)
      Loop SolDump# = 1 to records(SolaceWatchQueue)
        Get(SolaceWatchQueue,SolDump#)
        SOLWATF:Record :=: SolaceWatchQueue
        Add(SOLWatch)
      end
      Flush(SOLWatch)
      Close(SOLWatch)
      
      !******* Environment
      SolaceApplyFileName = SolaceSaveFile & '\!Environ'
      Create(SolEnviron)
      Open(SolEnviron,12h)
      Stream(SolEnviron)
      Loop SolDump# = 1 to records(SolaceEnvQueue)
        Get(SolaceEnvQueue,SolDump#)
        SOLENVF:Record :=: SolaceEnvQueue
        Add(SolEnviron)
      end
      Flush(SolEnviron)
      Close(SolEnviron)
      
      !******** Events
      SolaceApplyFileName = SolaceSaveFile & '\!Events'
      Create(SOLEvents)
      Open(SOLEvents,12h)
      Stream(SOLEvents)
      Loop SolDump# = records(SolaceEventQueue) to 1 by -1
        Get(SolaceEventQueue,SolDump#)
        SOLEVTF:Record :=: SolaceEventQueue
        Add(SOLEvents)
      end
      Flush(SOLEvents)
      Close(SOLEvents)
      
      !******** Messages
      SolaceApplyFileName = SolaceSaveFile & '\!Messages'
      Create(SOLMessage)
      Open(SOLMessage,12h)
      Stream(SOLMessage)
      Loop SolDump# = 1 to records(SolaceMessQueue)
        Get(SolaceMessQueue,SolDump#)
        SOLMESF:Record :=: SolaceMessQueue
        Add(SOLMessage)
      end
      Flush(SOLMessage)
      Close(SOLMessage)
      
      !******** Procedures
      SolaceApplyFileName = SolaceSaveFile & '\!Procedure'
      Create(SOLProc)
      Open(SOLProc,12h)
      Stream(SOLProc)
      Loop SolDump# = 1 to records(SolaceProcsQueue)
        Get(SolaceProcsQueue,SolDump#)
        SOLPROF:Record :=: SolaceProcsQueue
        Add(SOLProc)
      end
      Flush(SOLProc)
      Close(SOLProc)
      
      
      
      Setcursor(Cursor:Arrow)
      
      Message('Log file created was ' & SolaceSaveFile & '.tps','Created Log file',ICON:Asterisk)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button6, Accepted)
    OF ?CancelButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    
    ThisMakeover.TakeEvent(Win:Window,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    
    
  CASE FIELD()
  OF ?List1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, AlertKey)
      if keycode() = MouseLeft2 then
        Post(Event:Accepted,?ViewButton)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1, AlertKey)
    END
  OF ?List2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List2, AlertKey)
      if keycode() = MouseLeft2 then
        Post(Event:Accepted,?ViewButton)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List2, AlertKey)
    END
  OF ?List3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3, AlertKey)
      if keycode() = MouseLeft2 then
        Post(Event:Accepted,?ViewButton)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3, AlertKey)
    END
  OF ?List3:3
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3:3, AlertKey)
      if keycode() = MouseLeft2 then
        Post(Event:Accepted,?ViewButton)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List3:3, AlertKey)
    END
  OF ?List1:4
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1:4, AlertKey)
      if keycode() = MouseLeft2 then
        Post(Event:Accepted,?ViewButton)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1:4, AlertKey)
    END
  OF ?List1:6
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1:6, AlertKey)
      if keycode() = MouseLeft2 then
        Post(Event:Accepted,?ViewButton)
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List1:6, AlertKey)
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Sheet1, NewSelection)
      if inlist(choice(?Sheet1),'1','2','3','4','6','9') then
        Unhide(?ViewButton)
      else
        Hide(?ViewButton)
      end
      SolaceCurrentTab = Choice(?Sheet1)
      !SolaceViewVars('',0,'SolaceViewVars',3)
      !SolaceViewVars('',0,'SolaceGlobalRefresh',0)
      !Display
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Sheet1, NewSelection)
      mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      GLO:SolaceViewVariables = false
      Free(SolaceGlobView)
      Free(SolaceFiles)
      Free(SolaceAllFiles)
      Free(SolaceVarView)
      Free(FileFieldsQueue)
      free(SolaceEventQueue)
      Free(SolaceWatchQueue)
      Free(SolaceMessQueue)
      Free(SolaceProcsQueue)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:Iconize
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Iconize)
       If Loc:Minimized = true                !If currently Minimized
         Window {Prop:Height} = LOC:SaveHeight
         Loc:Minimized = False
       Else
         LOC:SaveHeight = Window{Prop:Height}        !Save the current height
         Window {Prop:Height} = 0
         Loc:Minimized = True
       End
       Cycle
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Iconize)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?ViewButton, Resize:FixRight+Resize:FixBottom, Resize:LockSize)


Resizer.Resize PROCEDURE

ReturnValue          BYTE,AUTO


  CODE
  ReturnValue = PARENT.Resize()
    ThisMakeover.Refresh()
  RETURN ReturnValue

