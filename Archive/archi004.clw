

   MEMBER('archive.clw')                              ! This is a MEMBER module


!------------ File: JOBSEARC, version 1 ----------------------
!------------ modified 11.03.2002 at 12:32:21 -----------------
MOD:stFileName:JOBSEARCVer1  STRING(260)
JOBSEARCVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEARCVer1),PRE(JOBSEARCVer1)
RecordNumberKey          KEY(JOBSEARCVer1:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer1:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
                         END
                       END
!------------ End File: JOBSEARC, version 1 ------------------

!------------ File: JOBSEARC, version 2 ----------------------
!------------ modified 29.08.2002 at 15:47:19 -----------------
MOD:stFileName:JOBSEARCVer2  STRING(260)
JOBSEARCVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEARCVer2),PRE(JOBSEARCVer2),CREATE
RecordNumberKey          KEY(JOBSEARCVer2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
                         END
                       END
!------------ End File: JOBSEARC, version 2 ------------------

!------------ File: JOBSEARC, version 3 ----------------------
!------------ modified 03.01.2003 at 10:19:50 -----------------
MOD:stFileName:JOBSEARCVer3  STRING(260)
JOBSEARCVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEARCVer3),PRE(JOBSEARCVer3),CREATE
RecordNumberKey          KEY(JOBSEARCVer3:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
                         END
                       END
!------------ End File: JOBSEARC, version 3 ------------------

!------------ File: JOBSEARC, version 4 ----------------------
!------------ modified 17.03.2004 at 15:21:00 -----------------
MOD:stFileName:JOBSEARCVer4  STRING(260)
JOBSEARCVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEARCVer4),PRE(JOBSEARCVer4),CREATE
RecordNumberKey          KEY(JOBSEARCVer4:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer4:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
                         END
                       END
!------------ End File: JOBSEARC, version 4 ------------------

!------------ File: JOBSEARC, version 5 ----------------------
!------------ modified 17.03.2004 at 15:26:19 -----------------
MOD:stFileName:JOBSEARCVer5  STRING(260)
JOBSEARCVer5           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEARCVer5),PRE(JOBSEARCVer5),CREATE
RecordNumberKey          KEY(JOBSEARCVer5:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer5:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
                         END
                       END
!------------ End File: JOBSEARC, version 5 ------------------

!------------ File: JOBSEARC, version 6 ----------------------
!------------ modified 17.03.2004 at 15:26:35 -----------------
MOD:stFileName:JOBSEARCVer6  STRING(260)
JOBSEARCVer6           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEARCVer6),PRE(JOBSEARCVer6),CREATE
RecordNumberKey          KEY(JOBSEARCVer6:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer6:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
                         END
                       END
!------------ End File: JOBSEARC, version 6 ------------------

!------------ File: JOBSEARC, version 7 ----------------------
!------------ modified 05.04.2005 at 13:19:33 -----------------
MOD:stFileName:JOBSEARCVer7  STRING(260)
JOBSEARCVer7           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEARCVer7),PRE(JOBSEARCVer7),CREATE
RecordNumberKey          KEY(JOBSEARCVer7:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer7:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
                         END
                       END
!------------ End File: JOBSEARC, version 7 ------------------

!------------ File: JOBTHIRD, version 1 ----------------------
!------------ modified 11.03.2002 at 12:32:21 -----------------
MOD:stFileName:JOBTHIRDVer1  STRING(260)
JOBTHIRDVer1           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBTHIRDVer1),PRE(JOBTHIRDVer1)
RecordNumberKey          KEY(JOBTHIRDVer1:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBTHIRDVer1:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(JOBTHIRDVer1:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(JOBTHIRDVer1:InIMEI),DUP,NOCASE
OutDateKey               KEY(JOBTHIRDVer1:RefNumber,JOBTHIRDVer1:DateOut,JOBTHIRDVer1:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(JOBTHIRDVer1:ThirdPartyNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                       END
!------------ End File: JOBTHIRD, version 1 ------------------

!------------ File: JOBTHIRD, version 2 ----------------------
!------------ modified 03.01.2003 at 10:19:50 -----------------
MOD:stFileName:JOBTHIRDVer2  STRING(260)
JOBTHIRDVer2           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBTHIRDVer2),PRE(JOBTHIRDVer2),CREATE
RecordNumberKey          KEY(JOBTHIRDVer2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBTHIRDVer2:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(JOBTHIRDVer2:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(JOBTHIRDVer2:InIMEI),DUP,NOCASE
OutDateKey               KEY(JOBTHIRDVer2:RefNumber,JOBTHIRDVer2:DateOut,JOBTHIRDVer2:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(JOBTHIRDVer2:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(JOBTHIRDVer2:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                       END
!------------ End File: JOBTHIRD, version 2 ------------------

!------------ File: JOBTHIRD, version 3 ----------------------
!------------ modified 17.03.2004 at 15:21:00 -----------------
MOD:stFileName:JOBTHIRDVer3  STRING(260)
JOBTHIRDVer3           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBTHIRDVer3),PRE(JOBTHIRDVer3),CREATE
RecordNumberKey          KEY(JOBTHIRDVer3:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBTHIRDVer3:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(JOBTHIRDVer3:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(JOBTHIRDVer3:InIMEI),DUP,NOCASE
OutDateKey               KEY(JOBTHIRDVer3:RefNumber,JOBTHIRDVer3:DateOut,JOBTHIRDVer3:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(JOBTHIRDVer3:ThirdPartyNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                       END
!------------ End File: JOBTHIRD, version 3 ------------------

!------------ File: JOBSE, version 1 ----------------------
!------------ modified 11.03.2002 at 12:32:21 -----------------
MOD:stFileName:JOBSEVer1  STRING(260)
JOBSEVer1              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer1),PRE(JOBSEVer1)
RecordNumberKey          KEY(JOBSEVer1:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer1:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
                         END
                       END
!------------ End File: JOBSE, version 1 ------------------

!------------ File: JOBSE, version 2 ----------------------
!------------ modified 29.08.2002 at 15:47:19 -----------------
MOD:stFileName:JOBSEVer2  STRING(260)
JOBSEVer2              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer2),PRE(JOBSEVer2),CREATE
RecordNumberKey          KEY(JOBSEVer2:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
                         END
                       END
!------------ End File: JOBSE, version 2 ------------------

!------------ File: JOBSE, version 3 ----------------------
!------------ modified 03.01.2003 at 10:19:50 -----------------
MOD:stFileName:JOBSEVer3  STRING(260)
JOBSEVer3              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer3),PRE(JOBSEVer3),CREATE
RecordNumberKey          KEY(JOBSEVer3:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer3:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
                         END
                       END
!------------ End File: JOBSE, version 3 ------------------

!------------ File: JOBSE, version 4 ----------------------
!------------ modified 17.03.2004 at 15:21:00 -----------------
MOD:stFileName:JOBSEVer4  STRING(260)
JOBSEVer4              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer4),PRE(JOBSEVer4),CREATE
RecordNumberKey          KEY(JOBSEVer4:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer4:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
                         END
                       END
!------------ End File: JOBSE, version 4 ------------------

!------------ File: JOBSE, version 5 ----------------------
!------------ modified 17.03.2004 at 15:26:19 -----------------
MOD:stFileName:JOBSEVer5  STRING(260)
JOBSEVer5              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer5),PRE(JOBSEVer5),CREATE
RecordNumberKey          KEY(JOBSEVer5:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer5:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
                         END
                       END
!------------ End File: JOBSE, version 5 ------------------

!------------ File: JOBSE, version 6 ----------------------
!------------ modified 17.03.2004 at 15:26:35 -----------------
MOD:stFileName:JOBSEVer6  STRING(260)
JOBSEVer6              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer6),PRE(JOBSEVer6),CREATE
RecordNumberKey          KEY(JOBSEVer6:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer6:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
                         END
                       END
!------------ End File: JOBSE, version 6 ------------------

!------------ File: JOBSE, version 7 ----------------------
!------------ modified 05.04.2005 at 13:19:33 -----------------
MOD:stFileName:JOBSEVer7  STRING(260)
JOBSEVer7              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer7),PRE(JOBSEVer7),CREATE
RecordNumberKey          KEY(JOBSEVer7:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer7:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
                         END
                       END
!------------ End File: JOBSE, version 7 ------------------

!------------ Declaration Current Files Structure --------------------

!------------ modified 23.12.2005 at 10:30:25 -----------------
MOD:stFileName:JOBSEARCVer8  STRING(260)
JOBSEARCVer8           FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.ARC'),PRE(JOBSEARCVer8),CREATE
RecordNumberKey          KEY(JOBSEARCVer8:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEARCVer8:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                       END

!------------ modified 17.03.2004 at 15:26:19 -----------------
MOD:stFileName:JOBTHIRDVer4  STRING(260)
JOBTHIRDVer4           FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBTHIRDVer4),PRE(JOBTHIRDVer4),CREATE
RecordNumberKey          KEY(JOBTHIRDVer4:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBTHIRDVer4:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(JOBTHIRDVer4:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(JOBTHIRDVer4:InIMEI),DUP,NOCASE
OutDateKey               KEY(JOBTHIRDVer4:RefNumber,JOBTHIRDVer4:DateOut,JOBTHIRDVer4:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(JOBTHIRDVer4:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(JOBTHIRDVer4:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                       END

!------------ modified 23.12.2005 at 10:30:25 -----------------
MOD:stFileName:JOBSEVer8  STRING(260)
JOBSEVer8              FILE,DRIVER('Btrieve'),OEM,NAME(MOD:stFileName:JOBSEVer8),PRE(JOBSEVer8),CREATE
RecordNumberKey          KEY(JOBSEVer8:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(JOBSEVer8:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                       END

!------------ End of Declaration Current Files Structure -------------

DCDummySQLTableOwner   STRING(260)
MOD:stFileName STRING(260)

FileQueue QUEUE,PRE()
FileLabel   &FILE
FileName    &STRING
          END

TempFileQueue QUEUE,PRE()
TempFileLabel   &FILE
TempFileName    &STRING
              END

ListExt       QUEUE,PRE()
Extention       CSTRING(260)
              END

                     MAP
                       INCLUDE('ARCHI004.INC'),ONCE        !Local module procedure declarations
ConvertJOBSEARC        PROCEDURE(BYTE),BYTE
ConvertJOBTHIRD        PROCEDURE(BYTE),BYTE
ConvertJOBSE           PROCEDURE(BYTE),BYTE
ProcessFileQueue       PROCEDURE(),BYTE
ErrorBox               PROCEDURE(STRING)
FileErrorBox           PROCEDURE()
CreateTempName         PROCEDURE(*STRING,STRING,FILE)
RemoveFiles            PROCEDURE(FILE,STRING)
GetFileNameWithoutOwner PROCEDURE(STRING),STRING
RenameFile             PROCEDURE(FILE,STRING)
PrepareExtentionQueue  PROCEDURE(FILE)
PrepareMultiTableFileName PROCEDURE(FILE,STRING,*STRING),BYTE
PrepareDestFile        PROCEDURE(FILE,STRING,FILE,STRING,STRING),BYTE
RenameDestFile         PROCEDURE(FILE,STRING,STRING),BYTE
                       MODULE('ClarionRTL')
DC:FnMerge               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),RAW,NAME('_fnmerge')
DC:FnSplit               PROCEDURE(*CSTRING,*CSTRING,*CSTRING,*CSTRING,*CSTRING),SHORT,RAW,NAME('_fnsplit')
DC:SetError              PROCEDURE(LONG),NAME('Cla$seterror')
DC:SetErrorFile          PROCEDURE(FILE),NAME('Cla$SetErrorFile')
                       END
                     END



DC:FilesConverter PROCEDURE(<STRING PAR:FileLabel>,<STRING PAR:FileName>)
  CODE
  IF ~OMITTED(1) AND ~OMITTED(2) AND PAR:FileLabel AND PAR:FileName
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBSEARC')
      JOBSEARCVer8{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBSEARC(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBTHIRD')
      JOBTHIRDVer4{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBTHIRD(0) THEN RETURN(1).
    END
    IF UPPER(CLIP(PAR:FileLabel)) = UPPER('JOBSE')
      JOBSEVer8{PROP:Name} = CLIP(PAR:FileName)
      IF ConvertJOBSE(0) THEN RETURN(1).
    END
  ELSE
    IF ConvertJOBSEARC(1) THEN RETURN(1).
    IF ConvertJOBTHIRD(1) THEN RETURN(1).
    IF ConvertJOBSE(1) THEN RETURN(1).
  END
  RETURN(0)
!---------------------------------------------------------------------

ConvertJOBSEARC PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  MOD:stFileName = JOBSEARCVer8{PROP:Name}
  MOD:stFileName:JOBSEARCVer1 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer1
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer1
  ADD(FileQueue)
  MOD:stFileName:JOBSEARCVer2 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer2
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer2
  ADD(FileQueue)
  MOD:stFileName:JOBSEARCVer3 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer3
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer3
  ADD(FileQueue)
  MOD:stFileName:JOBSEARCVer4 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer4
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer4
  ADD(FileQueue)
  MOD:stFileName:JOBSEARCVer5 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer5
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer5
  ADD(FileQueue)
  MOD:stFileName:JOBSEARCVer6 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer6
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer6
  ADD(FileQueue)
  MOD:stFileName:JOBSEARCVer7 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer7
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer7
  ADD(FileQueue)
  MOD:stFileName:JOBSEARCVer8 = 'JOBSE.ARC'
  FileQueue.FileLabel &= JOBSEARCVer8
  FileQueue.FileName &= MOD:stFileName:JOBSEARCVer8
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBSEARC'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBTHIRD PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  IF PAR:Mode = 0
    MOD:stFileName = JOBTHIRDVer4{PROP:Name}
  ELSE
    MOD:stFileName = glo:FileNameJOBTHIRD
  END
  MOD:stFileName:JOBTHIRDVer1 = MOD:stFileName
  FileQueue.FileLabel &= JOBTHIRDVer1
  FileQueue.FileName &= MOD:stFileName:JOBTHIRDVer1
  ADD(FileQueue)
  MOD:stFileName:JOBTHIRDVer2 = MOD:stFileName
  FileQueue.FileLabel &= JOBTHIRDVer2
  FileQueue.FileName &= MOD:stFileName:JOBTHIRDVer2
  ADD(FileQueue)
  MOD:stFileName:JOBTHIRDVer3 = MOD:stFileName
  FileQueue.FileLabel &= JOBTHIRDVer3
  FileQueue.FileName &= MOD:stFileName:JOBTHIRDVer3
  ADD(FileQueue)
  MOD:stFileName:JOBTHIRDVer4 = MOD:stFileName
  FileQueue.FileLabel &= JOBTHIRDVer4
  FileQueue.FileName &= MOD:stFileName:JOBTHIRDVer4
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBTHIRD'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ConvertJOBSE PROCEDURE(BYTE PAR:Mode)
  CODE
  FREE(FileQueue)
  IF PAR:Mode = 0
    MOD:stFileName = JOBSEVer8{PROP:Name}
  ELSE
    MOD:stFileName = glo:FileNameJOBSE
  END
  MOD:stFileName:JOBSEVer1 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer1
  FileQueue.FileName &= MOD:stFileName:JOBSEVer1
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer2 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer2
  FileQueue.FileName &= MOD:stFileName:JOBSEVer2
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer3 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer3
  FileQueue.FileName &= MOD:stFileName:JOBSEVer3
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer4 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer4
  FileQueue.FileName &= MOD:stFileName:JOBSEVer4
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer5 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer5
  FileQueue.FileName &= MOD:stFileName:JOBSEVer5
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer6 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer6
  FileQueue.FileName &= MOD:stFileName:JOBSEVer6
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer7 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer7
  FileQueue.FileName &= MOD:stFileName:JOBSEVer7
  ADD(FileQueue)
  MOD:stFileName:JOBSEVer8 = MOD:stFileName
  FileQueue.FileLabel &= JOBSEVer8
  FileQueue.FileName &= MOD:stFileName:JOBSEVer8
  ADD(FileQueue)
  IF ~MOD:stFileName THEN MOD:stFileName = 'JOBSE'.
  RETURN(ProcessFileQueue())
!---------------------------------------------------------------------

ProcessFileQueue PROCEDURE

LOC:lLoopIndex        LONG,AUTO
LOC:lTempIndex        LONG,AUTO
LOC:stStatusFullBuild STRING(3),AUTO
LOC:byReturnCode      BYTE(1)
LOC:byErrorStatus     BYTE(0)

  CODE
  FREE(TempFileQueue)
  LOOP LOC:lLoopIndex = RECORDS(FileQueue) TO 1 BY -1
    GET(FileQueue,LOC:lLoopIndex)
    OPEN(FileQueue.FileLabel,42h)
    IF ERRORCODE()
      IF ERRORCODE() = BadKeyErr
        IF UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'CLARION' AND UPPER(FileQueue.FileLabel{PROP:Driver}) <> 'BTRIEVE'
          CLOSE(FileQueue.FileLabel)
          OPEN(FileQueue.FileLabel,12h)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            LOC:stStatusFullBuild = SEND(FileQueue.FileLabel,'FULLBUILD')
            Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
          END
          BUILD(FileQueue.FileLabel)
          IF UPPER(FileQueue.FileLabel{PROP:Driver}) = 'TOPSPEED'
            IF CLIP(UPPER(LOC:stStatusFullBuild)) = 'ON'
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=On')
            ELSE
              Stat" = SEND(FileQueue.FileLabel,'FULLBUILD=Off')
            END
          END
        ELSE
          LOC:byErrorStatus = 1
          IF LOC:lLoopIndex <> 1
            CYCLE
          END
        END
      END
    ELSE
      IF LOC:lLoopIndex = RECORDS(FileQueue)
        CLOSE(FileQueue.FileLabel)
        LOC:byReturnCode = 0
        DO ProcedureReturn
      END
    END
    CLOSE(FileQueue.FileLabel)
    OPEN(FileQueue.FileLabel,12h)
    IF ERRORCODE()
      CASE ERRORCODE()
      OF NoAccessErr
        IF MESSAGE('Converting can not be finished because data file|"' & CLIP(ERRORFILE()) & '"|is locked by other station.|Ask users to quit all programs, which uses file|"' & CLIP(ERRORFILE()) & '"|and try again.','Conversion Error',ICON:Question,BUTTON:RETRY+BUTTON:ABORT,BUTTON:RETRY) = BUTTON:RETRY
          LOC:lLoopIndex += 1
          CYCLE
        ELSE
          DO ProcedureReturn
        END
      OF InvalidFileErr
      OROF NoPathErr
      OROF NoFileErr
      OROF BadKeyErr
        CASE ERRORCODE()
        OF NoPathErr
        OROF NoFileErr      ; IF LOC:byErrorStatus = 0 THEN LOC:byErrorStatus = 2.
        OF InvalidFileErr   ; IF LOC:byErrorStatus = 2 THEN LOC:byErrorStatus = 0.
        END
        IF LOC:lLoopIndex = 1
          CASE LOC:byErrorStatus
          OF 1 ; ErrorBox('Keys must be rebuild|File: ' & ERRORFILE())
          OF 2 ; LOC:byReturnCode = 0
          ELSE ; ErrorBox('No match file structure|File: ' & ERRORFILE())
          END  
          DO ProcedureReturn
        ELSE
        END
      ELSE
        IF LOC:lLoopIndex = 1 THEN FileErrorBox(); DO ProcedureReturn.
      END
    ELSE
      CLOSE(FileQueue.FileLabel)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CreateTempName(TempFileQueue.TempFileName,'m' & LOC:lTempIndex,TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
        CREATE(TempFileQueue.TempFileLabel)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
        OPEN(TempFileQueue.TempFileLabel,12h)
        IF ERRORCODE() THEN FileErrorBox(); DO ProcedureReturn.
      END
      LOC:byReturnCode = Conversion(LOC:lLoopIndex)
      LOOP LOC:lTempIndex = 1 TO RECORDS(TempFileQueue)
        GET(TempFileQueue,LOC:lTempIndex)
        CLOSE(TempFileQueue.TempFileLabel)
        CASE UPPER(TempFileQueue.TempFileLabel{PROP:Driver})
        ELSE
          RemoveFiles(TempFileQueue.TempFileLabel,TempFileQueue.TempFileName)
        END
      END
      DO ProcedureReturn
    END
  END
  LOC:byReturnCode = 0
  DO ProcedureReturn
!----------------------------------------------------------

AddQueueTempFile ROUTINE
  CLEAR(TempFileQueue)
  TempFileQueue.TempFileLabel &= FileQueue.FileLabel
  TempFileQueue.TempFileName  &= FileQueue.FileName
  ADD(TempFileQueue)
  EXIT
!---------------------------------------------------------------------

ProcedureReturn ROUTINE
  FREE(FileQueue)
  FREE(TempFileQueue)
  RETURN(LOC:byReturnCode)
!---------------------------------------------------------------------

ErrorBox PROCEDURE(STRING ErrorMessage)
  CODE
  MESSAGE('Conversion impossible!|' & CLIP(ErrorMessage),'Conversion Error',ICON:Exclamation,BUTTON:ABORT)
  RETURN
!---------------------------------------------------------------------

FileErrorBox PROCEDURE
  CODE
  ErrorBox('Error: ' & CLIP(CHOOSE(ERRORCODE() = 90,FILEERROR(),ERROR())) & '|Error File: ' & CHOOSE(LEN(ERRORFILE()) > 0,CLIP(ERRORFILE()),CLIP(MOD:stFileName)))
  RETURN
!---------------------------------------------------------------------

CreateTempName PROCEDURE(*STRING PAR:ProcessFileName,STRING PAR:Postfix,FILE PAR:File)

LOC:cstPath     CSTRING(260),AUTO
LOC:cstDrive    CSTRING(260),AUTO
LOC:cstDir      CSTRING(260),AUTO
LOC:cstName     CSTRING(260),AUTO
LOC:cstExt      CSTRING(260),AUTO
LOC:cstCopyName CSTRING(260),AUTO
LOC:cstCopyExt  CSTRING(260),AUTO

  CODE
  CASE UPPER(PAR:File{PROP:Driver})
  OF 'MSSQL'
  OROF 'SQLANYWHERE'
    LOC:cstPath = CLIP(PAR:ProcessFileName) & '_' & CLIP(PAR:Postfix)
  ELSE
    LOC:cstPath = CLIP(PAR:ProcessFileName)
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF LOC:cstName[1] = '!'
      LOC:cstCopyName = LOC:cstName
      LOC:cstCopyExt  = LOC:cstExt
      LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      LOC:cstDir = LOC:cstDir & LOC:cstName & '$' & LOC:cstCopyName[2 : LEN(LOC:cstCopyName)] & '$' & CLIP(PAR:Postfix) & LOC:cstExt & '\'
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstCopyName,LOC:cstCopyExt)
    ELSE
      LOC:cstName = CLIP(LOC:cstName & '$' & PAR:Postfix)
      DC:FnMerge(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    END
  END
  PAR:ProcessFileName = LOC:cstPath
  RETURN
!---------------------------------------------------------------------

RemoveFiles PROCEDURE(FILE PAR:ProcessFileLabel,STRING PAR:ProcessFileName)

LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO
LOC:lLoopIndex LONG,AUTO
LOC:lLoopExt   LONG,AUTO
ListDir        QUEUE(FILE:Queue),PRE(LD)
               END

  CODE
  PrepareExtentionQueue(PAR:ProcessFileLabel)
  LOC:cstPath = CLIP(PAR:ProcessFileName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstDrive & LOC:cstDir & LOC:cstName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      REMOVE(LOC:cstDrive & LOC:cstDir & LD:Name)
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

GetFileNameWithoutOwner PROCEDURE(STRING PAR:FileName)

LOC:lPosition LONG,AUTO

  CODE
  IF PAR:FileName
    LOC:lPosition = INSTRING('.',PAR:FileName,1,1)
    IF LOC:lPosition
      RETURN(PAR:FileName[LOC:lPosition + 1 : LEN(CLIP(PAR:FileName))])
    ELSE
      RETURN(PAR:FileName)
    END
  ELSE
    RETURN('')
  END
!---------------------------------------------------------------------

RenameFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName)

LOC:lLoopIndex       LONG,AUTO
LOC:cstPath          CSTRING(260),AUTO
LOC:cstDrive         CSTRING(260),AUTO
LOC:cstDir           CSTRING(260),AUTO
LOC:cstName          CSTRING(260),AUTO
LOC:cstExt           CSTRING(260),AUTO
ListDir              QUEUE(FILE:Queue),PRE(LD)
                     END
LOC:cstTempName      CSTRING(260),AUTO
LOC:stTempSourceName STRING(260),AUTO
LOC:stTempTargetName STRING(260),AUTO
LOC:lErrorCode       LONG,AUTO
LOC:lLoopExt         LONG,AUTO
loc:extcount         LONG,AUTO
loc:Result           CSTRING(34),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:SourceFile{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:cstTempName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  LOC:stTempSourceName = LOC:cstDrive & LOC:cstDir
  LOC:cstPath = CLIP(PAR:TargetName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF LOC:cstName[1] = '!'
    RemoveFiles(PAR:SourceFile,LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1])
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  END
  LOC:stTempTargetName = LOC:cstDrive & LOC:cstDir & LOC:cstName
  PrepareExtentionQueue(PAR:SourceFile)
  LOOP LOC:lLoopExt = 1 TO RECORDS(ListExt)
    GET(ListExt,LOC:lLoopExt)
    FREE(ListDir)
    DIRECTORY(ListDir,LOC:cstTempName & ListExt.Extention,ff_:ARCHIVE)
    LOOP LOC:lLoopIndex = RECORDS(ListDir) TO 1 BY -1
      GET(ListDir,LOC:lLoopIndex)
      IF CLIP(LD:ShortName) = '..' OR CLIP(LD:ShortName) = '.' THEN CYCLE.
      LOC:cstPath = CLIP(LOC:stTempSourceName) & CLIP(LD:Name)
      Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
      RENAME(CLIP(LOC:stTempSourceName) & LD:Name,CLIP(LOC:stTempTargetName) & LOC:cstExt)
      !Look for and rename extensions
      Loop loc:extcount = 1 to 255
          LtoA(loc:extcount,loc:Result,16)
          If Len(loc:Result) = 1
              loc:Result = '0' & loc:Result
          End ! If Len(loc:Result) = 1
          loc:Result = UPPER(loc:Result)

          If Exists(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result)
              Rename(CLIP(LOC:stTempSourceName) & Clip(loc:cstName) & '.^' & loc:Result,Clip(loc:stTempTargetName) & '.^' & loc:Result)
          Else
              Break
          End ! If Exists(CLip(loc:stTempSourceName) & '.^' & loc:Result)
      End ! Loop loc:extcount = 1 to 255

      IF ERRORCODE() 
        LOC:lErrorCode = ERRORCODE()
        FREE(ListExt)
        FREE(ListDir)
        DC:SetError(LOC:lErrorCode)
        RETURN
      END
    END
  END
  FREE(ListExt)
  FREE(ListDir)
  RETURN
!---------------------------------------------------------------------

PrepareExtentionQueue PROCEDURE(FILE PAR:ProcessFileLabel)

LOC:lLoopIndex LONG,AUTO
LOC:cstPath    CSTRING(260),AUTO
LOC:cstDrive   CSTRING(260),AUTO
LOC:cstDir     CSTRING(260),AUTO
LOC:cstName    CSTRING(260),AUTO
LOC:cstExt     CSTRING(260),AUTO

  CODE
  LOC:cstPath = CLIP(PAR:ProcessFileLabel{PROP:Name})
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  FREE(ListExt)
  IF LOC:cstName[1] = '!'
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      ListExt.Extention = '.tps'
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
  ELSE
    IF ~LOC:cstExt
      CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
      OF   'CLA'
      OROF   'BTR'
        ListExt.Extention = '.dat'
      OF   'TOP'
        ListExt.Extention = '.tps'
      OF   'DBA'
      OROF 'CLI'
      OROF 'FOX'
        ListExt.Extention = '.dbf'
      END
    ELSE
      ListExt.Extention = LOC:cstExt
    END
    ADD(ListExt)
    CASE UPPER(SUB(PAR:ProcessFileLabel{PROP:Driver},1,3))
    OF 'CLA'
      ListExt.Extention = '.mem'
      ADD(ListExt)
      LOOP LOC:lLoopIndex = 1 TO 99
        ListExt.Extention = '.k' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
        ListExt.Extention = '.i' & FORMAT(LOC:lLoopIndex,@N02)
        ADD(ListExt)
      END
    OF 'BTR'
      ListExt.Extention = '.dbt'
      ADD(ListExt)
      ListExt.Extention = '.ndx'
      ADD(ListExt)
    OF 'DBA'
      ListExt.Extention = '.ndx'
      ADD(ListExt)
      ListExt.Extention = '.mdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'CLI'
      ListExt.Extention = '.ntx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.dbt'
      ADD(ListExt)
    OF 'FOX'
      ListExt.Extention = '.inx'
      ADD(ListExt)
      ListExt.Extention = '.cdx'
      ADD(ListExt)
      ListExt.Extention = '.fbt'
      ADD(ListExt)
    END
  END
  RETURN
!---------------------------------------------------------------------

PrepareMultiTableFileName PROCEDURE(FILE PAR:File,STRING PAR:SourceName,*STRING PAR:TargetName)

LOC:cstPath      CSTRING(260),AUTO
LOC:cstDrive     CSTRING(260),AUTO
LOC:cstDir       CSTRING(260),AUTO
LOC:cstName      CSTRING(260),AUTO
LOC:cstExt       CSTRING(260),AUTO
LOC:byMultyTable BYTE(False)

  CODE
  CLEAR(PAR:TargetName)
  LOC:cstPath = CLIP(PAR:SourceName)
  Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
  IF UPPER(PAR:File{PROP:Driver}) = 'TOPSPEED' AND LOC:cstName[1] = '!'
    PAR:TargetName = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    LOC:cstPath = LOC:cstDrive & LOC:cstDir[1 : LEN(LOC:cstDir) - 1]
    Err# = DC:FnSplit(LOC:cstPath,LOC:cstDrive,LOC:cstDir,LOC:cstName,LOC:cstExt)
    IF ~LOC:cstExt
      PAR:TargetName = CLIP(PAR:TargetName) & '.tps'
    END
    LOC:byMultyTable = True
  END
  RETURN(LOC:byMultyTable)
!---------------------------------------------------------------------

PrepareDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:SourceName,FILE PAR:TargetFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)

  CODE
  IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      SETCURSOR(CURSOR:Wait)
      COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
      SETCURSOR
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:TargetName
      REMOVE(PAR:SourceFile)
      IF ERRORCODE()
        DC:SetErrorFile(PAR:SourceFile)
        RETURN(1)
      END
      PAR:SourceFile{PROP:Name} = PAR:SourceName
      LOC:byMultyTable = True
    END
  ELSE
    IF PrepareMultiTableFileName(PAR:TargetFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:TargetFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
        IF EXISTS(LOC:stTempSourceFile)
          SETCURSOR(CURSOR:Wait)
          COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
          SETCURSOR
          IF ERRORCODE()
            DC:SetErrorFile(PAR:SourceFile)
            RETURN(1)
          END
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RemoveFiles(PAR:TargetFile,PAR:TargetName)
  END
  RETURN(0)
!---------------------------------------------------------------------

RenameDestFile PROCEDURE(FILE PAR:SourceFile,STRING PAR:TargetName,STRING PAR:OriginalTargetName)

LOC:stTempSourceFile STRING(260),AUTO
LOC:stTempTargetFile STRING(260),AUTO
LOC:byMultyTable     BYTE(False)
ROU:lErrorCode       LONG,AUTO

  CODE
  IF ~PrepareMultiTableFileName(PAR:SourceFile,PAR:OriginalTargetName,LOC:stTempSourceFile)
    IF PrepareMultiTableFileName(PAR:SourceFile,PAR:TargetName,LOC:stTempTargetFile)
      IF PrepareMultiTableFileName(PAR:SourceFile,PAR:SourceFile{PROP:Name},LOC:stTempSourceFile)
        SETCURSOR(CURSOR:Wait)
        COPY(LOC:stTempSourceFile,LOC:stTempTargetFile)
        SETCURSOR
        IF ERRORCODE()
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        REMOVE(PAR:SourceFile)
        IF ERRORCODE()
          ROU:lErrorCode = ERRORCODE()
          RemoveFiles(PAR:SourceFile,LOC:stTempTargetFile)
          DC:SetError(ROU:lErrorCode)
          DC:SetErrorFile(PAR:SourceFile)
          RETURN(1)
        END
        LOC:byMultyTable = True
      END
    END
  END
  IF LOC:byMultyTable = False
    RenameFile(PAR:SourceFile,PAR:TargetName)
    IF ERRORCODE()
      RETURN(1)
    END
  END
  RETURN(0)
!---------------------------------------------------------------------

Conversion FUNCTION(LONG Version)

LocalRequest         LONG
OriginalRequest      LONG
LocalResponse        LONG
FilesOpened          LONG
WindowOpened         LONG
WindowInitialized    LONG
ForceRefresh         LONG
ReturnCode           BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
LOC:byTPSDriver         BYTE,AUTO
LOC:byDestSQLDriver     BYTE,AUTO
LOC:byDestMSSQLDriver   BYTE,AUTO
LOC:byDestASADriver     BYTE,AUTO
LOC:bySourceSQLDriver   BYTE,AUTO
LOC:bySourceMSSQLDriver BYTE,AUTO
LOC:bySourceASADriver   BYTE,AUTO
LOC:byTransactionActive BYTE,AUTO
LOC:rfSourceFileLabel   &FILE
LOC:stSourceFileName    STRING(260),AUTO
LOC:rfDestFileLabel     &FILE
LOC:stDestFileName      STRING(260),AUTO
LOC:stTargetFileName    STRING(260),AUTO
RecordProcessed      LONG
RecordTotal          LONG
InfoString           STRING(50)
DummyFileName1       STRING(260)
DummyFileName2       STRING(260)
SaveRecordProcessed  LONG
SavePercent          LONG
SaveFillBar          LONG

SPBBorderControl     SIGNED
SPBFillControl       SIGNED
SPBPercentControl    SIGNED

mo:SelectedButton Long  ! ProcedureType = DCDataConverter  WinType = Window
mo:SelectedField  Long
mo:WinType        equate(Win:Window)
window               WINDOW('File Conversion'),AT(,,224,52),FONT('MS Sans Serif',8,,),CENTER,GRAY
                       PANEL,AT(4,4,216,44),USE(?Panel1),BEVEL(-1)
                       PROGRESS,USE(RecordProcessed),AT(12,12,200,12),RANGE(0,100)
                       STRING(@s50),AT(12,32,200,),USE(InfoString),CENTER,FONT(,,COLOR:Navy,,CHARSET:ANSI)
                     END
  CODE
  PUSHBIND
  SETCURSOR
  ReturnCode = 1
  LocalRequest = GlobalRequest
  OriginalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  ForceRefresh = False
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  IF KEYCODE() = MouseRight
    SETKEYCODE(0)
  END
  DO PrepareProcedure
  ACCEPT
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      END
      SELECT(?Panel1)
    OF EVENT:GainFocus
      ForceRefresh = True
      IF NOT WindowInitialized
        DO InitializeWindow
        WindowInitialized = True
      ELSE
        DO RefreshWindow
      END
    OF Event:Rejected
      BEEP
      DISPLAY(?)
      SELECT(?)
    END
  END
  DO ProcedureReturn
!---------------------------------------------------------------------------
PrepareProcedure ROUTINE
  FilesOpened = True
  OPEN(window)
  WindowOpened=True
  window{PROP:Text} = CLIP(window{PROP:Text}) & ' - ' & MOD:stFileName
  DO ConversionProcess
  DO ProcedureReturn
!---------------------------------------------------------------------------
ProcedureReturn ROUTINE
!|
!| This routine provides a common procedure exit point for all template
!| generated procedures.
!|
!| First, all of the files opened by this procedure are closed.
!|
!| Next, if it was opened by this procedure, the window is closed.
!|
!| Next, GlobalResponse is assigned a value to signal the calling procedure
!| what happened in this procedure.
!|
!| Next, we replace the BINDings that were in place when the procedure initialized
!| (and saved with PUSHBIND) using POPBIND.
!|
!| Finally, we return to the calling procedure, passing ReturnCode back.
!|
  IF FilesOpened
  END
  IF WindowOpened
    CLOSE(window)
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  POPBIND
  RETURN(ReturnCode)
!---------------------------------------------------------------------------
InitializeWindow ROUTINE
!|
!| This routine is used to prepare any control templates for use. It should be called once
!| per procedure.
!|
  DO RefreshWindow
!---------------------------------------------------------------------------
RefreshWindow ROUTINE
!|
!| This routine is used to keep all displays and control templates current.
!|
  IF window{Prop:AcceptAll} THEN EXIT.
  DISPLAY()
  ForceRefresh = False
!---------------------------------------------------------------------------
SyncWindow ROUTINE
!|
!| This routine is used to insure that any records pointed to in control
!| templates are fetched before any procedures are called via buttons or menu
!| options.
!|
!---------------------------------------------------------------------------

ConversionProcess ROUTINE
  DATA
ROU:fCurrentFile      &FILE
ROU:lConvertedRecNum  LONG,AUTO
ROU:lRecordsThisCycle LONG,AUTO
ROU:lDim1             LONG,AUTO
ROU:lDim2             LONG,AUTO
ROU:lDim3             LONG,AUTO
ROU:lDim4             LONG,AUTO
ROU:byError           BYTE,AUTO
  CODE
  GET(FileQueue,Version)
  LOC:rfSourceFileLabel &= FileQueue.FileLabel
  LOC:stSourceFileName   = FileQueue.FileName
  GET(FileQueue,RECORDS(FileQueue))
  LOC:rfDestFileLabel &= FileQueue.FileLabel
  LOC:stDestFileName   = FileQueue.FileName
  LOC:stTargetFileName = LOC:stDestFileName
  LOC:byTransactionActive = False
  LOC:byTPSDriver         = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'TOPSPEED',True,False)
  LOC:bySourceMSSQLDriver = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:bySourceASADriver   = CHOOSE(UPPER(LOC:rfSourceFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:bySourceSQLDriver   = LOC:bySourceMSSQLDriver + LOC:bySourceASADriver
  LOC:byDestMSSQLDriver   = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'MSSQL',True,False)
  LOC:byDestASADriver     = CHOOSE(UPPER(LOC:rfDestFileLabel{PROP:Driver}) = 'SQLANYWHERE',True,False)
  LOC:byDestSQLDriver     = LOC:byDestMSSQLDriver + LOC:byDestASADriver
  CreateTempName(LOC:stDestFileName,'tmp',LOC:rfDestFileLabel)
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    IF PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stDestFileName,DummyFileName1) AND |
       (PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stSourceFileName,DummyFileName1) OR |
        (PrepareMultiTableFileName(LOC:rfDestFileLabel,LOC:stTargetFileName,DummyFileName2) AND EXISTS(DummyFileName2)) |
       )
      InfoString = 'File copying...'
      DISPLAY
    END
    IF PrepareDestFile(LOC:rfSourceFileLabel,LOC:stSourceFileName,LOC:rfDestFileLabel,LOC:stDestFileName,LOC:stTargetFileName) THEN DO PreReturn; EXIT.
  END
  OPEN(LOC:rfSourceFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  CREATE(LOC:rfDestFileLabel)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
  OPEN(LOC:rfDestFileLabel,12h)
  IF ERRORCODE() THEN DO PreReturn; EXIT.
  IF LOC:byTPSDriver 
    ROU:lConvertedRecNum = 0
  ELSE
    IF ~LOC:byDestSQLDriver THEN STREAM(LOC:rfDestFileLabel).
  END
  ?RecordProcessed{PROP:Use} = RecordProcessed
  ?InfoString{PROP:Use} = InfoString
  DO InitalizeSolidProgressBar
  ?RecordProcessed{PROP:RangeHigh} = RECORDS(LOC:rfSourceFileLabel)
  RecordProcessed = 0
  RecordTotal     = RECORDS(LOC:rfSourceFileLabel)
  SaveRecordProcessed = 0
  SavePercent         = 0
  SaveFillBar         = 0
  SETCURSOR(CURSOR:Wait)
  DISPLAY
  SET(LOC:rfSourceFileLabel)
  IF LOC:byTPSDriver
    LOGOUT(1,LOC:rfDestFileLabel)
    IF ERRORCODE() THEN DO PreReturn; EXIT.
    LOC:byTransactionActive = True
  END
  ROU:byError = 0
  window{PROP:Timer} = 1
  ACCEPT
    CASE EVENT()
    OF EVENT:GainFocus
      DISPLAY
    OF EVENT:CloseWindow
      CYCLE
    OF EVENT:Completed
      BREAK
    OF EVENT:Timer
      ROU:lRecordsThisCycle = 0
      LOOP WHILE ROU:lRecordsThisCycle < 25
        NEXT(LOC:rfSourceFileLabel)
        IF ERRORCODE()
          POST(EVENT:Completed)
          BREAK
        END
        ROU:fCurrentFile &= LOC:rfSourceFileLabel
        IF ROU:fCurrentFile &= JOBSEARCVer1
          ! Field "FailedDelivery" added
          ! Field "CConfirmSecondEntry" added
          ! Field "WConfirmSecondEntry" added
          ! Field "EndUserEmailAddress" added
          ! Field "Network" added
          CLEAR(JOBSEARCVer2:RECORD)
          JOBSEARCVer2:RECORD :=: JOBSEARCVer1:RECORD
          ROU:fCurrentFile &= JOBSEARCVer2
        END
        IF ROU:fCurrentFile &= JOBSEARCVer2
          ! Field "ExchangeReason" added
          ! Field "LoanReason" added
          ! Field "InWorkshopDate" added
          ! Field "InWorkshopTime" added
          CLEAR(JOBSEARCVer3:RECORD)
          JOBSEARCVer3:RECORD :=: JOBSEARCVer2:RECORD
          ROU:fCurrentFile &= JOBSEARCVer3
        END
        IF ROU:fCurrentFile &= JOBSEARCVer3
          ! Field "FailedDelivery" removed
          ! Field "CConfirmSecondEntry" removed
          ! Field "WConfirmSecondEntry" removed
          ! Field "EndUserEmailAddress" removed
          ! Field "Network" removed
          ! Field "ExchangeReason" removed
          ! Field "LoanReason" removed
          ! Field "InWorkshopDate" removed
          ! Field "InWorkshopTime" removed
          CLEAR(JOBSEARCVer4:RECORD)
          JOBSEARCVer4:RECORD :=: JOBSEARCVer3:RECORD
          ROU:fCurrentFile &= JOBSEARCVer4
        END
        IF ROU:fCurrentFile &= JOBSEARCVer4
          ! Field "FailedDelivery" added
          ! Field "CConfirmSecondEntry" added
          ! Field "WConfirmSecondEntry" added
          ! Field "EndUserEmailAddress" added
          ! Field "Network" added
          ! Field "ExchangeReason" added
          ! Field "LoanReason" added
          ! Field "InWorkshopDate" added
          ! Field "InWorkshopTime" added
          ! Field "Pre_RF_Board_IMEI" added
          CLEAR(JOBSEARCVer5:RECORD)
          JOBSEARCVer5:RECORD :=: JOBSEARCVer4:RECORD
          ROU:fCurrentFile &= JOBSEARCVer5
        END
        IF ROU:fCurrentFile &= JOBSEARCVer5
          ! Field "Pre_RF_Board_IMEI" removed
          CLEAR(JOBSEARCVer6:RECORD)
          JOBSEARCVer6:RECORD :=: JOBSEARCVer5:RECORD
          ROU:fCurrentFile &= JOBSEARCVer6
        END
        IF ROU:fCurrentFile &= JOBSEARCVer6
          ! Field "Pre_RF_Board_IMEI" added
          ! Field "CompleteRepairType" added
          ! Field "CompleteRepairDate" added
          ! Field "CompleteRepairTime" added
          ! Field "CustomerCollectionDate" added
          ! Field "CustomerCollectionTime" added
          ! Field "EstimatedDespatchDate" added
          ! Field "EstimatedDespatchTime" added
          CLEAR(JOBSEARCVer7:RECORD)
          JOBSEARCVer7:RECORD :=: JOBSEARCVer6:RECORD
          ROU:fCurrentFile &= JOBSEARCVer7
        END
        IF ROU:fCurrentFile &= JOBSEARCVer7
          ! Field "FaultCode13" added
          ! Field "FaultCode14" added
          ! Field "FaultCode15" added
          ! Field "FaultCode16" added
          ! Field "FaultCode17" added
          ! Field "FaultCode18" added
          ! Field "FaultCode19" added
          ! Field "FaultCode20" added
          CLEAR(JOBSEARCVer8:RECORD)
          JOBSEARCVer8:RECORD :=: JOBSEARCVer7:RECORD
        END
        IF ROU:fCurrentFile &= JOBTHIRDVer1
          ! Key/Index "OriginalIMEIKey" added
          CLEAR(JOBTHIRDVer2:RECORD)
          JOBTHIRDVer2:RECORD :=: JOBTHIRDVer1:RECORD
          ROU:fCurrentFile &= JOBTHIRDVer2
        END
        IF ROU:fCurrentFile &= JOBTHIRDVer2
          ! Key/Index "OriginalIMEIKey" removed
          CLEAR(JOBTHIRDVer3:RECORD)
          JOBTHIRDVer3:RECORD :=: JOBTHIRDVer2:RECORD
          ROU:fCurrentFile &= JOBTHIRDVer3
        END
        IF ROU:fCurrentFile &= JOBTHIRDVer3
          ! Key/Index "OriginalIMEIKey" added
          CLEAR(JOBTHIRDVer4:RECORD)
          JOBTHIRDVer4:RECORD :=: JOBTHIRDVer3:RECORD
        END
        IF ROU:fCurrentFile &= JOBSEVer1
          ! Field "FailedDelivery" added
          ! Field "CConfirmSecondEntry" added
          ! Field "WConfirmSecondEntry" added
          ! Field "EndUserEmailAddress" added
          ! Field "Network" added
          CLEAR(JOBSEVer2:RECORD)
          JOBSEVer2:RECORD :=: JOBSEVer1:RECORD
          ROU:fCurrentFile &= JOBSEVer2
        END
        IF ROU:fCurrentFile &= JOBSEVer2
          ! Field "ExchangeReason" added
          ! Field "LoanReason" added
          ! Field "InWorkshopDate" added
          ! Field "InWorkshopTime" added
          CLEAR(JOBSEVer3:RECORD)
          JOBSEVer3:RECORD :=: JOBSEVer2:RECORD
          ROU:fCurrentFile &= JOBSEVer3
        END
        IF ROU:fCurrentFile &= JOBSEVer3
          ! Field "FailedDelivery" removed
          ! Field "CConfirmSecondEntry" removed
          ! Field "WConfirmSecondEntry" removed
          ! Field "EndUserEmailAddress" removed
          ! Field "Network" removed
          ! Field "ExchangeReason" removed
          ! Field "LoanReason" removed
          ! Field "InWorkshopDate" removed
          ! Field "InWorkshopTime" removed
          CLEAR(JOBSEVer4:RECORD)
          JOBSEVer4:RECORD :=: JOBSEVer3:RECORD
          ROU:fCurrentFile &= JOBSEVer4
        END
        IF ROU:fCurrentFile &= JOBSEVer4
          ! Field "FailedDelivery" added
          ! Field "CConfirmSecondEntry" added
          ! Field "WConfirmSecondEntry" added
          ! Field "EndUserEmailAddress" added
          ! Field "Network" added
          ! Field "ExchangeReason" added
          ! Field "LoanReason" added
          ! Field "InWorkshopDate" added
          ! Field "InWorkshopTime" added
          ! Field "Pre_RF_Board_IMEI" added
          CLEAR(JOBSEVer5:RECORD)
          JOBSEVer5:RECORD :=: JOBSEVer4:RECORD
          ROU:fCurrentFile &= JOBSEVer5
        END
        IF ROU:fCurrentFile &= JOBSEVer5
          ! Field "CompleteRepairType" added
          ! Field "CompleteRepairDate" added
          ! Field "CompleteRepairTime" added
          CLEAR(JOBSEVer6:RECORD)
          JOBSEVer6:RECORD :=: JOBSEVer5:RECORD
          ROU:fCurrentFile &= JOBSEVer6
        END
        IF ROU:fCurrentFile &= JOBSEVer6
          ! Field "CustomerCollectionDate" added
          ! Field "CustomerCollectionTime" added
          ! Field "EstimatedDespatchDate" added
          ! Field "EstimatedDespatchTime" added
          CLEAR(JOBSEVer7:RECORD)
          JOBSEVer7:RECORD :=: JOBSEVer6:RECORD
          ROU:fCurrentFile &= JOBSEVer7
        END
        IF ROU:fCurrentFile &= JOBSEVer7
          ! Field "FaultCode13" added
          ! Field "FaultCode14" added
          ! Field "FaultCode15" added
          ! Field "FaultCode16" added
          ! Field "FaultCode17" added
          ! Field "FaultCode18" added
          ! Field "FaultCode19" added
          ! Field "FaultCode20" added
          CLEAR(JOBSEVer8:RECORD)
          JOBSEVer8:RECORD :=: JOBSEVer7:RECORD
        END
        IF ROU:fCurrentFile &= JOBSEARCVer7
          IF ~JOBSEARCVer8:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBTHIRDVer3
          IF ~JOBTHIRDVer4:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF ROU:fCurrentFile &= JOBSEVer7
          IF ~JOBSEVer8:RecordNumber
            RecordProcessed += 1
            ROU:lRecordsThisCycle += 1
            CYCLE
          END
        END
        IF LOC:byTPSDriver
          ADD(LOC:rfDestFileLabel)
        ELSE
          APPEND(LOC:rfDestFileLabel)
        END
        IF ERRORCODE()
          DO PreReturn
          ROU:byError = 1
          POST(EVENT:Completed)
          BREAK
        END
        IF LOC:byTPSDriver
          ROU:lConvertedRecNum += 1
          IF ROU:lConvertedRecNum % 1000 = 0
            COMMIT
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = False
            LOGOUT(1,LOC:rfDestFileLabel)
            IF ERRORCODE()
              DO PreReturn
              ROU:byError = 1
              POST(EVENT:Completed)
              BREAK
            END
            LOC:byTransactionActive = True
          END
        END
        RecordProcessed += 1
        DO DisplaySolidProgressBar
        InfoString = 'Records processed ' & RecordProcessed & ' from ' & RecordTotal
        DISPLAY
        ROU:lRecordsThisCycle += 1
      END
    END
  END
  IF ROU:byError = 0
    IF LOC:byTPSDriver
      COMMIT
      IF ERRORCODE() THEN DO PreReturn; EXIT.
      LOC:byTransactionActive = False
    ELSE
      InfoString = 'Now rebuild keys...'
      DISPLAY
      IF ~LOC:byDestSQLDriver THEN FLUSH(LOC:rfDestFileLabel).
      BUILD(LOC:rfDestFileLabel)
    END

    ReturnCode = 0
    LocalResponse = RequestCompleted
    DO PreReturn
  END
  EXIT
!----------------------------------------------------------

PreReturn ROUTINE
  DATA
ROU:lErrorCode        LONG,AUTO
ROU:stTempFile        STRING(260),AUTO
  CODE
  SETCURSOR
  IF ReturnCode
    IF LOC:byTPSDriver AND LOC:byTransactionActive
      ROU:lErrorCode = ERRORCODE()
      ROLLBACK
      DC:SetError(ROU:lErrorCode)
      DC:SetErrorFile(LOC:rfSourceFileLabel)
    END
    IF ~LOC:byDestSQLDriver AND ~LOC:byTPSDriver THEN FLUSH(LOC:rfDestFileLabel).
    FileErrorBox()
  END
  CLOSE(LOC:rfSourceFileLabel)
  CLOSE(LOC:rfDestFileLabel)
  IF ReturnCode
    DO RemoveTempFile
  ELSE
    IF LOC:bySourceSQLDriver
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      ROU:stTempFile = LOC:stSourceFileName
      CreateTempName(ROU:stTempFile,FORMAT(Version,@N03),LOC:rfSourceFileLabel)
      RemoveFiles(LOC:rfSourceFileLabel,ROU:stTempFile)
      IF ~PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:stTargetFileName,DummyFileName1)
        IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,ROU:stTempFile,DummyFileName1)
          IF PrepareMultiTableFileName(LOC:rfSourceFileLabel,LOC:rfSourceFileLabel{PROP:Name},DummyFileName1)
            InfoString = 'File copying...'
            DISPLAY
          END
        END
      END
      IF RenameDestFile(LOC:rfSourceFileLabel,ROU:stTempFile,LOC:stTargetFileName)
        ROU:lErrorCode = ERRORCODE()
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:byDestSQLDriver
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    ELSE
      LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
      RemoveFiles(LOC:rfDestFileLabel,LOC:stTargetFileName)
      RenameFile(LOC:rfDestFileLabel,LOC:stTargetFileName)
      IF ERRORCODE() 
        ROU:lErrorCode = ERRORCODE()
        IF LOC:bySourceSQLDriver
          DCDummySQLTableOwner = LOC:rfSourceFileLabel{PROP:Owner}
        ELSE
          LOC:rfSourceFileLabel{PROP:Name} = ROU:stTempFile
          RenameFile(LOC:rfSourceFileLabel,LOC:stSourceFileName)
        END
        DO RemoveTempFile
        ReturnCode = 1
        DC:SetError(ROU:lErrorCode)
        DC:SetErrorFile(LOC:rfDestFileLabel)
        FileErrorBox()
        EXIT
      END
    END
    IF LOC:byDestSQLDriver
      DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
    ELSE
      RemoveFiles(LOC:rfDestFileLabel,LOC:stDestFileName)
    END
  END
  EXIT
!----------------------------------------------------------

RemoveTempFile ROUTINE
  IF LOC:byDestSQLDriver
    DCDummySQLTableOwner = LOC:rfDestFileLabel{PROP:Owner}
  ELSE
    LOC:rfDestFileLabel{PROP:Name} = LOC:stDestFileName
    REMOVE(LOC:rfDestFileLabel)
  END
  EXIT
!----------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Conversion',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Conversion',1)
    SolaceViewVars('OriginalRequest',OriginalRequest,'Conversion',1)
    SolaceViewVars('LocalResponse',LocalResponse,'Conversion',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Conversion',1)
    SolaceViewVars('WindowOpened',WindowOpened,'Conversion',1)
    SolaceViewVars('WindowInitialized',WindowInitialized,'Conversion',1)
    SolaceViewVars('ForceRefresh',ForceRefresh,'Conversion',1)
    SolaceViewVars('ReturnCode',ReturnCode,'Conversion',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RecordProcessed;  SolaceCtrlName = '?RecordProcessed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InfoString;  SolaceCtrlName = '?InfoString';Add(LSolCtrlQ,+SolaceUseRef)



InitalizeSolidProgressBar ROUTINE
  DATA
ROU:lPercentControlWidth   LONG
ROU:lPercentControlHeight  LONG
ROU:lProgressControlHeight LONG
ROU:byOldPropPixels        BYTE
  CODE
  ROU:byOldPropPixels = window{PROP:Pixels}
  window{PROP:Buffer} = True
  window{PROP:Pixels} = True
  ?RecordProcessed{PROP:Hide} = True

  SPBBorderControl = CREATE(0,CREATE:Panel,?RecordProcessed{PROP:Parent})
  SPBBorderControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos}
  SPBBorderControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos}
  SPBBorderControl{PROP:Width}  = ?RecordProcessed{PROP:Width}
  SPBBorderControl{PROP:Height} = ?RecordProcessed{PROP:Height}
  IF ?RecordProcessed{PROP:Background} <> 0FFFFFFFFH
    SPBBorderControl{PROP:Fill} = ?RecordProcessed{PROP:Background}
  END
  SPBBorderControl{PROP:BevelOuter} = -1
  SPBBorderControl{PROP:Hide} = False

  SPBFillControl = CREATE(0,CREATE:Box,?RecordProcessed{PROP:Parent})
  SPBFillControl{PROP:Xpos}   = ?RecordProcessed{PROP:Xpos} + 1
  SPBFillControl{PROP:Ypos}   = ?RecordProcessed{PROP:Ypos} + 1
  SPBFillControl{PROP:Width}  = 0
  SPBFillControl{PROP:Height} = ?RecordProcessed{PROP:Height} - 1
  IF ?RecordProcessed{PROP:SelectedFillColor} <> 0FFFFFFFFH
    SPBFillControl{PROP:Fill} = ?RecordProcessed{PROP:SelectedFillColor}
  ELSE
    SPBFillControl{PROP:Fill} = 08000000DH
  END
  SPBFillControl{PROP:Hide} = False

  SPBPercentControl = CREATE(0,CREATE:String,?RecordProcessed{PROP:Parent})
  SPBPercentControl{PROP:FontStyle} = FONT:bold
  SPBPercentControl{PROP:FontSize}  = ?RecordProcessed{PROP:Height}
  LOOP
    ROU:lPercentControlHeight  = SPBPercentControl{PROP:Height}
    ROU:lProgressControlHeight = ?RecordProcessed{PROP:Height}
    IF ROU:lPercentControlHeight <= ROU:lProgressControlHeight OR SPBPercentControl{PROP:FontSize} = 1
      BREAK
    END
    IF SPBPercentControl{PROP:FontSize} < 8
      SPBPercentControl{PROP:FontName} = 'Small Fonts'
    END
    SPBPercentControl{PROP:FontSize} = SPBPercentControl{PROP:FontSize} - 1
  END

  SPBPercentControl{PROP:Text}      = '100%'
  ROU:lPercentControlWidth          = SPBPercentControl{PROP:Width}
  SPBPercentControl{PROP:Xpos}      = ?RecordProcessed{PROP:Xpos} + (?RecordProcessed{PROP:Width} - ROU:lPercentControlWidth) / 2
  SPBPercentControl{PROP:Width}     = ROU:lPercentControlWidth
  SPBPercentControl{PROP:Ypos}      = ?RecordProcessed{PROP:Ypos} + (?RecordProcessed{PROP:Height} - SPBPercentControl{PROP:Height}) / 2 + 1
  IF ?RecordProcessed{PROP:SelectedColor} <> 0FFFFFFFFH
    SPBPercentControl{PROP:FontColor} = ?RecordProcessed{PROP:SelectedColor}
  ELSE
    SPBPercentControl{PROP:FontColor} = 0FFFFFFH
  END
  SPBPercentControl{PROP:Trn}       = True
  SPBPercentControl{PROP:Center}    = True
  SPBPercentControl{PROP:Text}      = '0%'
  SPBPercentControl{PROP:Hide}      = False
  window{PROP:Pixels} = ROU:byOldPropPixels
  EXIT
!---------------------------------------------------------------------

DisplaySolidProgressBar ROUTINE
  DATA
ROU:lRangeLow  LONG,AUTO
ROU:lRangeHigh LONG,AUTO
ROU:lPercent   LONG,AUTO
ROU:lFillBar   LONG,AUTO
  CODE
  IF SaveRecordProcessed <> RecordProcessed
    ROU:lRangeLow  = ?RecordProcessed{PROP:RangeLow}
    ROU:lRangeHigh = ?RecordProcessed{PROP:RangeHigh}
    IF RecordProcessed <= ROU:lRangeHigh
      ROU:lFillBar = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * (?RecordProcessed{PROP:Width} - 1)
      IF ROU:lFillBar <> SaveFillBar
        SPBFillControl{PROP:Width} = ROU:lFillBar
        DISPLAY(SPBFillControl)
        SaveFillBar = ROU:lFillBar
      END
      ROU:lPercent = (RecordProcessed / (ROU:lRangeHigh - ROU:lRangeLow)) * 100
      IF ROU:lPercent <> SavePercent
        SPBPercentControl{PROP:Text} = FORMAT(ROU:lPercent,@N3) & '%'
        DISPLAY(SPBPercentControl)
        SavePercent = ROU:lPercent
      END
      SaveRecordProcessed = RecordProcessed
    ELSE
      RecordProcessed = ROU:lRangeHigh
    END
  END
  EXIT
!---------------------------------------------------------------------
