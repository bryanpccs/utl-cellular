          MEMBER('archive.clw')                       ! This is a MEMBER module

   Map
   End

SolaceAddEvents         Procedure(P:Event,P:Proc,P:Control)
  Code
  Case P:Event
  of EVENT:Accepted
    SOLEVT:EventName = 'Accepted'
  of EVENT:Selected
    SOLEVT:EventName = 'Selected'
  of EVENT:NewSelection
    SOLEVT:EventName = 'NewSelection'
  of EVENT:MouseDown
    SOLEVT:EventName = 'MouseDown'
  of EVENT:MouseUp
    SOLEVT:EventName = 'MouseUp'
  of EVENT:MouseIn
    SOLEVT:EventName = 'MouseIn'
  of EVENT:MouseOut
    SOLEVT:EventName = 'MouseOut'
  of EVENT:MouseMove
    SOLEVT:EventName = 'MouseMove'
  of EVENT:AlertKey
    SOLEVT:EventName = 'AlertKey'
  of EVENT:PreAlertKey
    SOLEVT:EventName = 'PreAlertKey'
  of EVENT:Dragging
    SOLEVT:EventName = 'Dragging'
  of EVENT:Drag
    SOLEVT:EventName = 'Drag'
  of EVENT:Drop
    SOLEVT:EventName = 'Drop'
  of EVENT:ScrollDrag
    SOLEVT:EventName = 'ScrollDrag'
  of EVENT:ScrollUp
    SOLEVT:EventName = 'ScrollUp'
  of EVENT:ScrollDown
    SOLEVT:EventName = 'ScrollDown'
  of Event:PageUp
    SOLEVT:EventName = 'PageUp'
  of Event:PageDown
    SOLEVT:EventName = 'PageDown'
  of EVENT:ScrollTop
    SOLEVT:EventName = 'ScrollTop'
  of EVENT:ScrollBottom
    SOLEVT:EventName = 'ScrollBottom'
  of EVENT:Locate
    SOLEVT:EventName = 'Locate'
  of EVENT:VBXevent
    SOLEVT:EventName = 'VBXevent'
  of EVENT:TabChanging
    SOLEVT:EventName = 'TabChanging'
  of EVENT:Expanding
    SOLEVT:EventName = 'Expanding'
  of EVENT:Contracting
    SOLEVT:EventName = 'Contracting'
  of EVENT:Contracted
    SOLEVT:EventName = 'Contracted'
  of EVENT:Rejected
    SOLEVT:EventName = 'Rejected'
  of EVENT:DroppingDown
    SOLEVT:EventName = 'DroppingDown'
  of EVENT:DroppedDown
    SOLEVT:EventName = 'DroppedDown'
  of EVENT:ScrollTrack
    SOLEVT:EventName = 'ScrollTrack'
  of EVENT:ColumnResize
    SOLEVT:EventName = 'ColumnResize'
  of EVENT:Selecting
    SOLEVT:EventName = 'Selecting'
  of EVENT:Selected
    SOLEVT:EventName = 'Selected'
  of EVENT:CloseWindow
    SOLEVT:EventName = 'CloseWindow'
  of EVENT:CloseDown
    SOLEVT:EventName = 'CloseDown'
  of EVENT:OpenWindow
    SOLEVT:EventName = 'OpenWindow'
  of EVENT:OpenFailed
    SOLEVT:EventName = 'OpenFailed'
  of EVENT:LoseFocus
    SOLEVT:EventName = 'LoseFocus'
  of EVENT:GainFocus
    SOLEVT:EventName = 'GainFocus'
  of EVENT:Suspend
    SOLEVT:EventName = 'Suspend'
  of EVENT:Resume
    SOLEVT:EventName = 'Resume'
  of EVENT:Timer
    SOLEVT:EventName = 'Timer'
  of EVENT:DDErequest
    SOLEVT:EventName = 'DDErequest'
  of EVENT:DDEadvise
    SOLEVT:EventName = 'DDEadvise'
  of EVENT:DDEdata
    SOLEVT:EventName = 'DDEdata'
  of EVENT:DDEcommand orof EVENT:DDEexecute
    SOLEVT:EventName = 'DDEcommand/DDEexecute'
  of EVENT:DDEpoke
    SOLEVT:EventName = 'DDEpoke'
  of EVENT:DDEclosed
    SOLEVT:EventName = 'DDEclosed'
  of EVENT:Move
    SOLEVT:EventName = 'Move'
  of EVENT:Size
    SOLEVT:EventName = 'Size'
  of EVENT:Restore
    SOLEVT:EventName = 'Restore'
  of EVENT:Maximize
    SOLEVT:EventName = 'Maximize'
  of EVENT:Iconize
    SOLEVT:EventName = 'Iconize'
  of EVENT:Completed
    SOLEVT:EventName = 'Completed'
  of EVENT:Moved
    SOLEVT:EventName = 'Moved'
  of EVENT:Sized
    SOLEVT:EventName = 'Sized'
  of EVENT:Restored
    SOLEVT:EventName = 'Restored'
  of EVENT:Maximized
    SOLEVT:EventName = 'Maximized'
  of EVENT:Iconized
    SOLEVT:EventName = 'Iconized'
  of EVENT:Docked
    SOLEVT:EventName = 'Docked'
  of EVENT:Undocked
    SOLEVT:EventName = 'Undocked'
  of EVENT:BuildFile
    SOLEVT:EventName = 'BuildFile'
  of EVENT:BuildKey
    SOLEVT:EventName = 'BuildKey'
  of EVENT:BuildDone
    SOLEVT:EventName = 'BuildDone'
  else
    SOLEVT:EventName = 'Unknown Event ' & P:Event
  end
  SOLEVT:ProcName = P:Proc
  SOLEVT:CtrlName = P:Control
  Add(SolaceEventQueue,1)


SolaceGlobalVariableView        Procedure
   Code
   do SolFileList
   do SolEnvironment
   do SolGlobalData
   SolDictFiles1
   SolFileStatus
   SolSingleFileStatus



SolFileList        Routine
   Free(SolaceGlobView)
   Free(SolaceFiles)
   Free(SolaceAllFiles)
   Free(SolaceEnvQueue)
   If ~Records(SolaceFilesNames)
    SOLFN:FileName = 'JOBS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ESTPARTSARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'ESTPARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'DEFARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDITARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDSTATSARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDSTATS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CONTHISTARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBPAYMTARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBPAYMT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'AUDIT'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'CONTHIST'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'WARPARTSARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'WARPARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PARTSARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'PARTS'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBNOTESARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBNOTES'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'LOGGED'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSEARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBTHIRDARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSTAGEARC'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBTHIRD'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSTAGE'
    Add(SolaceFilesNames,+SOLFN:FileName)
    SOLFN:FileName = 'JOBSE'
    Add(SolaceFilesNames,+SOLFN:FileName)
   END


SolEnvironment     Routine
   if (SolaceCurrentTab = 7 and SolaceNoRefreshEnvironment = true) or SolaceNoRefreshEnvironment = false or SolaceSaveToFile = true then
     GlobalMemoryStatus(CurrentMem)
     SOLENV:EnvName = 'Total Physical Memory'
     SOLENV:EnvValue = CurrentMem.dwTotalPhys & '  (' & Int(CurrentMem.dwTotalPhys / 1024000) & 'MB)'
     Add(SolaceEnvQueue)

     SOLENV:EnvName = 'Available Physical Memory'
     SOLENV:EnvValue = CurrentMem.dwAvailPhys & '  (' & Int(CurrentMem.dwAvailPhys / 1024000) & 'MB)'
     Add(SolaceEnvQueue)

     SOLENV:EnvName = 'Total Virtual Memory'
     SOLENV:EnvValue = CurrentMem.dwTotalVirtual & '  (' & Int(CurrentMem.dwTotalVirtual / 1024000) & 'MB)'
     Add(SolaceEnvQueue)

     SOLENV:EnvName = 'Available Virtual Memory'
     SOLENV:EnvValue = CurrentMem.dwAvailVirtual & '  (' & Int(CurrentMem.dwAvailVirtual / 1024000) & 'MB)'
     Add(SolaceEnvQueue)
     SOLENV:EnvName = 'Current Path'
     SOLENV:EnvValue = LongPath()
     Add(SolaceEnvQueue)

     drives" = 'CDEFGHIJKLMNOPQRSTUVWXYZ'
     Loop x# = 1 to 24
       SolaceRootPath = Sub(drives",x#,1) & ':'
       Case GetDriveType(SolaceRootPath)
       of 0 orof 1
         cycle
       of 2
         SOLENV:EnvName = clip(SolaceRootPath) & ' - Removeable Drive'
       of 3
         SOLENV:EnvName = clip(SolaceRootPath) & ' - Local Hard Disk'
       of 4
         SOLENV:EnvName = clip(SolaceRootPath) & ' - Network Drive'
       of 5
         SOLENV:EnvName = clip(SolaceRootPath) & ' - CD Rom'
       of 6
         SOLENV:EnvName = clip(SolaceRootPath) & ' - RAM Disk'
       end
       SolaceRootPath = SolaceRootPath & '\'
       SOLENV:EnvValue = ''
       if GetDriveType(SolaceRootPath) <> 4 and Sub(Path(),1,1) <> drives" then       !Ignore Network drives
       if GetDriveType(SolaceRootPath) <> 5 then       !Ignore CD ROMs
         Ret# = GetDiskFreeSpaceA(SolaceRootPath,SolaceSectPerClust,SolaceBytesPerSect,SolaceFreeClust,SolaceClusters)
         TotalDiskSpace# = (SolaceBytesPerSect * SolaceSectPerClust * SolaceClusters) / 1024000
         FreeDiskSpace# = (SolaceBytesPerSect * SolaceSectPerClust * SolaceFreeClust) / 1024000
         SOLENV:EnvValue = 'Disk Space: ' & TotalDiskSpace# & 'MB  Free: ' & FreeDiskSpace# & 'MB'
       End
       End
       Add(SolaceEnvQueue)
     end


     WinVersion.dwOSVersionInfoSize = Size(OSVERSIONINFO)

     Ret# = GetVersionEx(WinVersion)
     SOLENV:EnvName = 'OS Version'
     Case WinVersion.dwPlatformId
     of 1
       SOLENV:EnvValue = 'Win95/98/Me'
     of 2
       if WinVersion.dwMajorVersion >= 5 then
         SOLENV:EnvValue = '2000'
       else
         SOLENV:EnvValue = 'NT'
       end
     of 0
       SOLENV:EnvValue = 'Win 3.1 with Win32s'
     end

     SOLENV:EnvValue = clip(SOLENV:EnvValue) & '   ' & WinVersion.dwMajorVersion & '.' & WinVersion.dwMinorVersion & '.' & WinVersion.dwBuildNumber
     Add(SolaceEnvQueue)
   end


SolGlobalData       Routine
   SolaceViewVars('GLO:Password',GLO:Password,'Solace_Global',1)
   SolaceViewVars('GLO:PassAccount',GLO:PassAccount,'Solace_Global',1)
   SolaceViewVars('GLO:TimeLogged',GLO:TimeLogged,'Solace_Global',1)
   SolaceViewVars('GLO:Select1',GLO:Select1,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameJobs',GLO:FileNameJobs,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameAudit',GLO:FileNameAudit,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameJobStage',GLO:FileNameJobStage,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameJobThird',GLO:FileNameJobThird,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameParts',GLO:FileNameParts,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameWarParts',GLO:FileNameWarParts,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameContHist',GLO:FileNameContHist,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameAudStats',GLO:FileNameAudStats,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameJobse',GLO:FileNameJobse,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameJobNotes',GLO:FileNameJobNotes,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameESTPARTS',GLO:FileNameESTPARTS,'Solace_Global',1)
   SolaceViewVars('GLO:FileNameJOBPAYMT',GLO:FileNameJOBPAYMT,'Solace_Global',1)
   SolaceViewVars('filename3',filename3,'Solace_Global',1)
