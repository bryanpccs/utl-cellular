

   MEMBER('archive.clw')                              ! This is a MEMBER module

                     MAP
                       INCLUDE('ARCHI007.INC'),ONCE        !Local module procedure declarations
                     END


SolaceViewVars       PROCEDURE  (P:Name,P:Value,P:Type,P:Action,P:FileName) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()


!(P:Name,P:Value,P:Type,P:Action,P:FileName)

!P:Name
!P:Value
!P:Type
!P:Action
 If P:Action = 6 then             !Message Streaming
   SOLMES:Value1 = P:Name
   SOLMES:Value2 = P:Value
   SOLMES:Value3 = P:Type
   Add(SolaceMessQueue)
 else
   Case P:Type
   of 'LogProc'
     SOLPRC:ProcName = P:FileName
     Add(SolaceProcsQueue)
   of 'EventRefresh'
     SolaceAddEvents(P:Value,P:FileName,P:Name)
   of 'SolaceGlobalRefresh'
     SolaceInit = P:Action
     SolaceGlobalVariableView
   of 'Solace_Global'
     SOLGLO:VariableName = P:Name
     SOLGLO:VariableValue = P:Value
     Add(SolaceGlobView)
   of 'Solace_Files'
     SOLALL:FileName = P:FileName
     SOLALL:FieldName = P:Name
     SOLALL:FieldValue = P:Value
     Add(SolaceAllFiles,+SOLALL:FileName)
   of 'FileStatus'
      if P:Value <> SolaceThreadNo then
        SOLFS:FileName = P:Name
        If ~P:Action then
          SOLFS:OpenStatus = 'Closed' & ' (' & P:Value & ')'
        else
          SOLFS:OpenStatus = '      Open' & ' (' & P:Value & ')'
        end
        SOLFS:NoRecords = P:FileName
        Add(SolaceFiles,+SOLFS:FileName)
      end
   else
     Case P:Action
     of 1
       do AddToQueue
     of 3
       do RemoveFromQueue
     of 4
       Free(SolaceVarView)
     end
   end
 end

                 
AddToQueue      Routine
  SOLVV:VariableName = P:Name
  SOLVV:VariableValue = P:Value
  SOLVV:ProcName = P:Type
  Add(SolaceVarView)
RemoveFromQueue     Routine
   Loop
     x# += 1
     if x# > Records(SolaceVarView) then
       Break
     end
     Get(SolaceVarView,x#)
     if clip(SOLVV:ProcName) = clip(P:Type) then
       Delete(SolaceVarView)
       x# -= 1
     end
   end



! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
