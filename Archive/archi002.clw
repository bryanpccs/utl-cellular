

   MEMBER('archive.clw')                              ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ARCHI002.INC'),ONCE        !Local module procedure declarations
                     END


BeginArchive PROCEDURE                                !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:LivePath         STRING(255)
tmp:StartJobNumber   LONG
tmp:EndJobNumber     LONG
save_job_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
save_cht_id          USHORT,AUTO
save_aus_id          USHORT,AUTO
save_jobarc_id       USHORT,AUTO
save_audarc_id       USHORT,AUTO
save_chtarc_id       USHORT,AUTO
save_ausarc_id       USHORT,AUTO
save_jst_id          USHORT,AUTO
save_jstarc_id       USHORT,AUTO
save_jbn_id          USHORT,AUTO
save_jbnarc_id       USHORT,AUTO
save_jot_id          USHORT,AUTO
save_jotarc_id       USHORT,AUTO
save_jobe_id         USHORT,AUTO
save_jobearc_id      USHORT,AUTO
save_par_id          USHORT,AUTO
save_pararc_id       USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_wprarc_id       USHORT,AUTO
save_epr_id          USHORT,AUTO
save_eprarc_id       USHORT,AUTO
save_jpt_id          USHORT,AUTO
save_jptarc_id       USHORT,AUTO
tmp:Completed        BYTE(1)
tmp:Despatched       BYTE(1)
tmp:Invoiced         BYTE(1)
tmp:StartDate        LONG
tmp:EndDate          DATE
tmp:JobsDeleted      LONG
mo:SelectedTab::Sheet1    Long  ! Makeover Template      LocalTreat = Wizard
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Wizard
mo:SelectedField  Long
mo:WinType        equate(Win:Wizard)
window               WINDOW('Archive Routine'),AT(,,423,227),FONT('Arial',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,416,192),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('ARCHIVE ROUTINE'),AT(126,8),USE(?Prompt1),FONT(,20,COLOR:Green,FONT:bold,CHARSET:ANSI)
                           PROMPT('Data Files'),AT(8,36),USE(?Prompt5),FONT(,,,FONT:bold+FONT:underline)
                           PROMPT('File Types'),AT(8,112),USE(?Prompt5:2),FONT(,,,FONT:bold+FONT:underline)
                           CHECK('Archive Completed Jobs Only'),AT(304,128),USE(tmp:Completed),MSG('Archive Completed Jobs'),TIP('Archive Completed Jobs'),VALUE('1','0')
                           CHECK('Archive Despatched Jobs Only'),AT(8,128),USE(tmp:Despatched),MSG('Archive Despatched Jobs'),TIP('Archive Despatched Jobs'),VALUE('1','0')
                           CHECK('Archive Invoiced Jobs Only'),AT(160,128),USE(tmp:Invoiced),MSG('Archive Invoiced Jobs'),TIP('Archive Invoiced Jobs'),VALUE('1','0')
                           PROMPT('Select the job number range of archive files:'),AT(8,144),USE(?Prompt8)
                           PROMPT('Start Date'),AT(276,156),USE(?tmp:StartDate:Prompt),TRN
                           ENTRY(@D6),AT(340,156,64,10),USE(tmp:StartDate),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Start Date'),TIP('Start Date'),UPR
                           BUTTON,AT(408,156,10,10),USE(?LookupStartDate),SKIP,ICON('Calenda2.ico')
                           PROMPT('Start Job Number'),AT(8,160),USE(?tmp:StartJobNumber:Prompt),TRN
                           PROMPT('End Date'),AT(276,176),USE(?tmp:EndDate:Prompt)
                           ENTRY(@d6),AT(340,176,64,10),USE(tmp:EndDate),LEFT,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('End Date'),TIP('End Date'),UPR
                           BUTTON,AT(408,176,10,10),USE(?LookupEndDate),SKIP,ICON('Calenda2.ico')
                           ENTRY(@s8),AT(84,156,64,10),USE(tmp:StartJobNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('Start Job Number'),TIP('Start Job Number'),UPR
                           PROMPT('Only Archive jobs completed within the Date Range'),AT(252,144),USE(?CompletedText)
                           PROMPT('End Job Number'),AT(8,176),USE(?tmp:EndJobNumber:Prompt),TRN
                           ENTRY(@s8),AT(84,176,64,10),USE(tmp:EndJobNumber),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),MSG('End Job Number'),TIP('End Job Number'),UPR
                           PROMPT('Archive Data Files Path:'),AT(8,48),USE(?Prompt2),FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                           STRING(@s255),AT(164,48,248,12),USE(tmp:LivePath),FONT(,12,COLOR:Navy,FONT:bold)
                           STRING(@s255),AT(164,72,248,12),USE(DEF:ArchivePath),FONT(,12,COLOR:Green,FONT:bold)
                           PROMPT('Important: The Archive Data File Path CANNOT be the same as the Live Data File P' &|
   'ath'),AT(24,96),USE(?Prompt4),FONT('Arial',10,COLOR:Red,FONT:bold,CHARSET:ANSI)
                           PROMPT('ServiceBase Data Files Path:'),AT(8,72),USE(?Prompt2:2),FONT('Arial',12,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(4,200,416,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Begin Archive'),AT(8,204,56,16),USE(?BeginArchive),LEFT,ICON('DISK.GIF')
                       BUTTON('Cancel'),AT(360,204,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
!! Start Change xxx BE(26/07/2004)
!StartTransaction    ROUTINE
!  LOGOUT(10000, |
!         AUDSTATS, |
!         JOBSE, |
!         JOBNOTES, |
!         JOBPAYMT, |
!         ESTPARTS, |
!         JOBSARC, |
!         AUDITARC, |
!         JOBSTAGEARC, |
!         JOBTHIRDARC, |
!         PARTSARC, |
!         WARPARTSARC, |
!         CONTHISTARC, |
!         AUDSTATSARC, |
!         JOBSEARC, |
!         JOBNOTESARC, |
!         JOBPAYMTARC, |
!         ESTPARTSARC)
!
!! End Change xxx BE(26/07/2004)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
CloseFiles      Routine
        Access:JOBS.Close()
        Access:JOBSARC.Close()
        Access:AUDIT.Close()
        Access:AUDITARC.Close()
        Access:AUDSTATS.Close()
        Access:AUDSTATSARC.Close()
        Access:CONTHIST.Close()
        Access:CONTHISTARC.Close()
        Access:JOBNOTES.Close()
        Access:JOBNOTESARC.Close()
        Access:JOBSE.Close()
        Access:JOBSEARC.Close()
        Access:JOBSTAGE.Close()
        Access:JOBSTAGEARC.Close()
        Access:JOBTHIRD.Close()
        Access:JOBTHIRDARC.Close()
        Access:PARTS.Close()
        Access:PARTSARC.Close()
        Access:WARPARTS.Close()
        Access:WARPARTSARC.Close()
        Access:ESTPARTS.Close()
        Access:ESTPARTSARC.Close()
        Access:JOBPAYMT.Close()
        Access:JOBPAYMTARC.Close()
OpenFiles       Routine
        Access:JOBS.Open()
        Access:JOBS.Usefile()
        Access:JOBSARC.Open()
        Access:JOBSARC.UseFile()
        Access:AUDIT.Open()
        Access:AUDIT.Usefile()
        Access:AUDITARC.Open()
        Access:AUDITARC.Usefile()
        Access:AUDSTATS.Open()
        Access:AUDSTATS.Usefile()
        Access:AUDSTATSARC.Open()
        Access:AUDSTATSARC.Usefile()
        Access:CONTHIST.Open()
        Access:CONTHIST.Usefile()
        Access:CONTHISTARC.Open()
        Access:CONTHISTARC.Usefile()
        Access:JOBNOTES.Open()
        Access:JOBNOTES.Usefile()
        Access:JOBNOTESARC.Open()
        Access:JOBNOTESARC.UseFile()
        Access:JOBSE.Open()
        Access:JOBSE.Usefile()
        Access:JOBSEARC.Open()
        Access:JOBSEARC.Usefile()
        Access:JOBSTAGE.Open()
        Access:JOBSTAGE.Usefile()
        Access:JOBSTAGEARC.Open()
        Access:JOBSTAGEARC.Usefile()
        Access:JOBTHIRD.Open()
        Access:JOBTHIRD.Usefile()
        Access:JOBTHIRDARC.Open()
        Access:JOBTHIRDARC.Usefile()
        Access:PARTS.Open()
        Access:PARTS.Usefile()
        Access:PARTSARC.Open()
        Access:PARTSARC.Usefile()
        Access:WARPARTS.Open()
        Access:WARPARTS.Usefile()
        Access:WARPARTSARC.Open()
        Access:WARPARTSARC.Usefile()
        Access:ESTPARTS.Open()
        Access:ESTPARTS.UseFile()
        Access:ESTPARTSARC.Open()
        Access:ESTPARTSARC.UseFile()
        Access:JOBPAYMT.Open()
        Access:JOBPAYMT.UseFile()
        Access:JOBPAYMTARC.Open()
        Access:JOBPAYMTARC.UseFile()

        
        


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'BeginArchive',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:LivePath',tmp:LivePath,'BeginArchive',1)
    SolaceViewVars('tmp:StartJobNumber',tmp:StartJobNumber,'BeginArchive',1)
    SolaceViewVars('tmp:EndJobNumber',tmp:EndJobNumber,'BeginArchive',1)
    SolaceViewVars('save_job_id',save_job_id,'BeginArchive',1)
    SolaceViewVars('save_aud_id',save_aud_id,'BeginArchive',1)
    SolaceViewVars('save_cht_id',save_cht_id,'BeginArchive',1)
    SolaceViewVars('save_aus_id',save_aus_id,'BeginArchive',1)
    SolaceViewVars('save_jobarc_id',save_jobarc_id,'BeginArchive',1)
    SolaceViewVars('save_audarc_id',save_audarc_id,'BeginArchive',1)
    SolaceViewVars('save_chtarc_id',save_chtarc_id,'BeginArchive',1)
    SolaceViewVars('save_ausarc_id',save_ausarc_id,'BeginArchive',1)
    SolaceViewVars('save_jst_id',save_jst_id,'BeginArchive',1)
    SolaceViewVars('save_jstarc_id',save_jstarc_id,'BeginArchive',1)
    SolaceViewVars('save_jbn_id',save_jbn_id,'BeginArchive',1)
    SolaceViewVars('save_jbnarc_id',save_jbnarc_id,'BeginArchive',1)
    SolaceViewVars('save_jot_id',save_jot_id,'BeginArchive',1)
    SolaceViewVars('save_jotarc_id',save_jotarc_id,'BeginArchive',1)
    SolaceViewVars('save_jobe_id',save_jobe_id,'BeginArchive',1)
    SolaceViewVars('save_jobearc_id',save_jobearc_id,'BeginArchive',1)
    SolaceViewVars('save_par_id',save_par_id,'BeginArchive',1)
    SolaceViewVars('save_pararc_id',save_pararc_id,'BeginArchive',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'BeginArchive',1)
    SolaceViewVars('save_wprarc_id',save_wprarc_id,'BeginArchive',1)
    SolaceViewVars('save_epr_id',save_epr_id,'BeginArchive',1)
    SolaceViewVars('save_eprarc_id',save_eprarc_id,'BeginArchive',1)
    SolaceViewVars('save_jpt_id',save_jpt_id,'BeginArchive',1)
    SolaceViewVars('save_jptarc_id',save_jptarc_id,'BeginArchive',1)
    SolaceViewVars('tmp:Completed',tmp:Completed,'BeginArchive',1)
    SolaceViewVars('tmp:Despatched',tmp:Despatched,'BeginArchive',1)
    SolaceViewVars('tmp:Invoiced',tmp:Invoiced,'BeginArchive',1)
    SolaceViewVars('tmp:StartDate',tmp:StartDate,'BeginArchive',1)
    SolaceViewVars('tmp:EndDate',tmp:EndDate,'BeginArchive',1)
    SolaceViewVars('tmp:JobsDeleted',tmp:JobsDeleted,'BeginArchive',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Completed;  SolaceCtrlName = '?tmp:Completed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Despatched;  SolaceCtrlName = '?tmp:Despatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Invoiced;  SolaceCtrlName = '?tmp:Invoiced';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate:Prompt;  SolaceCtrlName = '?tmp:StartDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartDate;  SolaceCtrlName = '?tmp:StartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStartDate;  SolaceCtrlName = '?LookupStartDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartJobNumber:Prompt;  SolaceCtrlName = '?tmp:StartJobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate:Prompt;  SolaceCtrlName = '?tmp:EndDate:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndDate;  SolaceCtrlName = '?tmp:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEndDate;  SolaceCtrlName = '?LookupEndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StartJobNumber;  SolaceCtrlName = '?tmp:StartJobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompletedText;  SolaceCtrlName = '?CompletedText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndJobNumber:Prompt;  SolaceCtrlName = '?tmp:EndJobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:EndJobNumber;  SolaceCtrlName = '?tmp:EndJobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LivePath;  SolaceCtrlName = '?tmp:LivePath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DEF:ArchivePath;  SolaceCtrlName = '?DEF:ArchivePath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BeginArchive;  SolaceCtrlName = '?BeginArchive';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('BeginArchive')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'BeginArchive')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFARC.Open
  SELF.FilesOpened = True
  Set(DEFARC)
  Access:DEFARC.Next()
  If Error()
      If Access:DEFARC.PrimeRecord() = Level:Benign
          def:ArchivePath = Clip(Path())
          If Access:DEFARC.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:DEFARC.TryInsert() = Level:Benign
              !Insert Failed
          End!If Access:DEFARC.TryInsert() = Level:Benign
      End !If Access:DEFARC.PrimeRecord() = Level:Benign
  End !Error()
  
  tmp:LivePath    = Path()
  tmp:StartDate   = Deformat('1/1/1990',@d6)
  tmp:EndDate     = Deformat('1/1/1990',@d6)
  OPEN(window)
  SELF.Opened=True
  ?tmp:StartDate{Prop:Alrt,255} = MouseLeft2
  ?tmp:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  ThisMakeover.SetWindow(Win:Wizard)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  IF ?tmp:Completed{Prop:Checked} = True
    UNHIDE(?CompletedText)
    UNHIDE(?tmp:EndDate)
    UNHIDE(?tmp:EndDate:Prompt)
    UNHIDE(?tmp:StartDate)
    UNHIDE(?tmp:StartDate:Prompt)
    UNHIDE(?LookupStartDate)
    UNHIDE(?LookupEndDate)
  END
  IF ?tmp:Completed{Prop:Checked} = False
    HIDE(?CompletedText)
    HIDE(?tmp:EndDate)
    HIDE(?tmp:EndDate:Prompt)
    HIDE(?tmp:StartDate)
    HIDE(?tmp:StartDate:Prompt)
    HIDE(?LookupStartDate)
    HIDE(?LookupEndDate)
  END
  SELF.SetAlerts()
    ThisMakeover.Refresh()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFARC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'BeginArchive',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Completed
      IF ?tmp:Completed{Prop:Checked} = True
        UNHIDE(?CompletedText)
        UNHIDE(?tmp:EndDate)
        UNHIDE(?tmp:EndDate:Prompt)
        UNHIDE(?tmp:StartDate)
        UNHIDE(?tmp:StartDate:Prompt)
        UNHIDE(?LookupStartDate)
        UNHIDE(?LookupEndDate)
      END
      IF ?tmp:Completed{Prop:Checked} = False
        HIDE(?CompletedText)
        HIDE(?tmp:EndDate)
        HIDE(?tmp:EndDate:Prompt)
        HIDE(?tmp:StartDate)
        HIDE(?tmp:StartDate:Prompt)
        HIDE(?LookupStartDate)
        HIDE(?LookupEndDate)
      END
      ThisWindow.Reset
    OF ?LookupStartDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:StartDate = TINCALENDARStyle1(tmp:StartDate)
          Display(?tmp:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?LookupEndDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          tmp:EndDate = TINCALENDARStyle1(tmp:EndDate)
          Display(?tmp:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?BeginArchive
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BeginArchive, Accepted)
      Error# = 0
      If tmp:EndJobNumber < tmp:StartJobNumber And Error# = 0
          Error# = 1
          Case MessageEx('The end job number is lower than the start job number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !tmp:EndJobNumber < tmp:StartJobNumber.
      If tmp:EndJobNumber = 0 or tmp:StartJobNumber = 0 And Error# =0
          Error#  = 1
          Case MessageEx('Invalid Job Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !tmp:EndJobNumber = 0 or tmp:StartJobNumber = 0
      If tmp:LivePath = def:ArchivePath and Error# = 0
          Case MessageEx('The Live Data File Path and the Archive Data File Path cannot be the same.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !tmp:LivePath = def:ArchivePath
      
      Countslash# = 0
      Loop x# = 1 To Len(def:ArchivePath)
          If Sub(def:ArchivePath,x#,1) = '\'
              countslash# += 1
              If Countslash# > 1
                  Break
              End !If Countslash# > 1
          End !If Sub(def:ArchivePath,1,1) = '\'
      End !x# = 1 To Len(def:ArchivePath)
      If Countslash# = 0 And Error# = 0
          Error# = 1
          Case MessageEx('Invalid Archive Path.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End !Countslash# = 1
      
      Case MessageEx('You have chosen to Archive any jobs that match the criteria between Job Numbers ' & tmp:StartJobNumber & ' and ' & tmp:EndJobNumber & '.'&|
        '<13,10>'&|
        '<13,10>Warning! You will no longer be able to access these jobs from ServiceBase.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              If Sub(def:ArchivePath,Len(Clip(def:ArchivePath)),1) = '\'
                  def:ArchivePath = Sub(def:ArchivePath,1,Len(Clip(def:ArchivePath)) - 1)
              End !If Sub(def:ArchivePath,Len(Clip(def:ArchivePath)),1) = '\'
      
              GLO:FileNameJobs        = Clip(def:ArchivePath) & '\JOBS.DAT'
              GLO:FileNameAudit       = Clip(def:ArchivePath) & '\AUDIT.DAT'
              GLO:FileNameJobStage    = Clip(def:ArchivePath) & '\JOBSTAGE.DAT'
              GLO:FileNameJobThird    = Clip(def:ArchivePath) & '\JOBTHIRD.DAT'
              GLO:FileNameParts       = Clip(def:ArchivePath) & '\PARTS.DAT'
              GLO:FileNameWarParts    = Clip(def:ArchivePath) & '\WARPARTS.DAT'
              GLO:FileNameContHist    = Clip(def:ArchivePath) & '\CONTHIST.DAT'
              GLO:FileNameAudStats    = Clip(def:ArchivePath) & '\AUDSTATS.DAT'
              GLO:FileNameJobse       = Clip(def:ArchivePath) & '\JOBSE.DAT'
              GLO:FileNameJobNotes    = Clip(def:ArchivePath) & '\JOBNOTES.DAT'
              GLO:FileNameJobPaymt    = Clip(def:ArchivePath) & '\JOBPAYMT.DAT'
              GLO:FileNameEstParts    = Clip(def:ArchivePath) & '\ESTPARTS.DAT'
      
      !        Rename('JOBS.DAT','AOBS.DAT')
      !        Rename('JOBS.^01','AOBS.^01')
      !        Rename('JOBS.^02','AOBS.^02')
      !        Rename('JOBS.^03','AOBS.^03')
      !        Rename('JOBS.^04','AOBS.^04')
      !        Rename('JOBS.^05','AOBS.^05')
      !        Rename('JOBS.^06','AOBS.^06')
      
              Do OpenFiles
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              thiswindow.reset(1)
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Running...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
              recordstoprocess    = tmp:EndJobNumber - tmp:StartJobNumber
      
              tmp:JobsDeleted = 0
      
              Save_JOB_ID = Access:JOBS.SaveFile()
      
      !        ! Start Change xxx BE(26/07/2004)
      !        TransactionCount# = 0
      !        DO StartTransaction
      !        ! End Change xxx BE(26/07/2004)
      
              Access:JOBS.ClearKey(JOB:Ref_Number_Key)
              JOB:Ref_Number = tmp:StartJobNumber
              Set(JOB:Ref_Number_Key,JOB:Ref_Number_Key)
              Loop
                  If Access:JOBS.NEXT()
                     Break
                  End !If
                  If JOB:Ref_Number > tmp:EndJobNumber      |
                      Then Break.  ! End If
                  ?progress:userstring{prop:text} = 'Jobs Deleted: ' & Clip(tmp:JobsDeleted)
                  Do GetNextRecord2
      !            cancelcheck# += 1
      !            If cancelcheck# > (RecordsToProcess/100)
                      Do cancelcheck
                      If tmp:cancel = 1
                          Break
                      End!If tmp:cancel = 1
                      cancelcheck# = 0
      !            End!If cancelcheck# > 50
      
                  If tmp:Completed
                      If job:Date_Completed < tmp:StartDate Or job:Date_Completed > tmp:EndDate
                          Cycle
                      End !If job:Date_Completed < tmp:StartDate Or job:Date_Completed > tmp:EndDate
                      If job:Date_Completed = ''
                          Cycle
                      End !If job:Date_Completed = ''
                  End !If tmp:Completed
                  If tmp:Invoiced
                      If job:Chargeable_Job = 'YES' and job:Invoice_Number = ''
                          Cycle
                      End !If job:Chargeable_Job = 'YES' and job:Invoice_Number = ''
                      If job:Warranty_Job = 'YES' and job:Invoice_Number_Warranty = ''
                          Cycle
                      End !If job:Warranty_Job = 'YES' and job:Invoice_Number_Warranty = ''
                  End !If tmp:Invoiced
      
                  If tmp:Despatched
                      If job:Exchange_Unit_Number = ''
                          If job:Date_Despatched = '' Or job:Consignment_Number = ''
                              Cycle
                          End !If job:Date_Despatched = ''
                      Else !If job:Exchange_Unit_Number = ''
                          If job:Exchange_Despatched = '' Or job:Exchange_Consignment_Number = ''
                              Cycle
                          End !If job:Exchange_Despatched = '' Or job:Exchange_COnsignment_NUmber = ''
                      End !If job:Exchange_Unit_Number = ''
      
                      If job:Loan_Unit_Number <> ''
                          If job:Loan_Despatched = '' Or job:Loan_Consignment_Number = ''
                              Cycle
                          End !If job:Loan_Despatched = '' Or job:Loan_Consignment_Number = ''
                      End !If job:Loan_Unit_Number <> ''
                  End !If tmp:Despatched
      
                  Access:JOBSARC.Clearkey(jobarc:Ref_Number_Key)
                  jobarc:Ref_Number   = job:Ref_Number
                  If Access:JOBSARC.Tryfetch(jobarc:Ref_Number_Key) = Level:Benign
                      !Found
                  Else! If Access:JOBSARC.Tryfetch(jobarc:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      If Access:JOBSARC.PrimeRecord() = Level:Benign
                          jobarc:Record   :=: Job:Record
                          If Access:JOBSARC.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:JOBS.TryInsert() = Level:Benign
                              !Insert Failed
                          End ! If Access:JOBS.TryInsert() = Level:Benign
                      End !If Access:JOBS.PrimeRecord() = Level:Benign
                  End! If Access:JOBSARC.Tryfetch(jobarc:Ref_Number_Key) = Level:Benign
                  
                  !Audit
                  Save_aud_ID = Access:AUDIT.SaveFile()
                  Access:AUDIT.ClearKey(aud:Ref_Number_Key)
                  aud:Ref_Number = job:Ref_Number
                  Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
                  Loop
                      If Access:AUDIT.NEXT()
                         Break
                      End !If
                      If aud:Ref_Number <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:AUDITARC.ClearKey(audarc:Record_Number_Key)
                      audarc:record_number = aud:Record_Number
                      If Access:AUDITARC.TryFetch(audarc:Record_Number_Key) = Level:Benign
                          !Found
      
                      Else!If Access:AUDITARC.TryFetch(audarc:Record_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:AUDITARC.PrimeRecord() = Level:Benign
                              audarc:Record   :=: aud:Record
                              If Access:AUDITARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:AUDITARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:AUDITARC.TryInsert() = Level:Benign
                          End !If Access:AUDITARC.PrimeRecord() = Level:Benign
      
                      End!If Access:AUDITARC.TryFetch(audarc:Record_Number_Key) = Level:Benign
                      Delete(AUDIT)
                  End !Loop
                  Access:AUDIT.RestoreFile(Save_aud_ID)
      
                  Save_aus_ID = Access:AUDSTATS.SaveFile()
                  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
                  aus:RefNumber   = job:Ref_Number
                  Set(aus:DateChangedKey,aus:DateChangedKey)
                  Loop
                      If Access:AUDSTATS.NEXT()
                         Break
                      End !If
                      If aus:RefNumber   <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:AUDSTATSARC.ClearKey(aus:RecordNumberKey)
                      ausarc:RecordNumber = aus:RecordNumber
                      If Access:AUDSTATSARC.TryFetch(aus:RecordNumberKey) = Level:Benign
                          !Found
                      Else!If Access:AUDARCSTATS.TryFetch(aus:RecordNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:AUDSTATSARC.PrimeRecord() = Level:Benign
                              ausarc:Record :=: aus:Record
                              If Access:AUDSTATSARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:AUDSTATSARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !AUDIf Access:AUDSTATSARC.TryInsert() = Level:Benign
                          End !If Access:AUDSTATSARC.PrimeRecord() = Level:Benign
      
                      End!If Access:AUDARCSTATS.TryFetch(aus:RecordNumberKey) = Level:Benign
      
                      Delete(AUDSTATS)
                  End !Loop
                  Access:AUDSTATS.RestoreFile(Save_aus_ID)
      
                  Save_cht_ID = Access:CONTHIST.SaveFile()
                  Access:CONTHIST.ClearKey(cht:Ref_Number_Key)
                  cht:Ref_Number = job:Ref_Number
                  Set(cht:Ref_Number_Key,cht:Ref_Number_Key)
                  Loop
                      If Access:CONTHIST.NEXT()
                         Break
                      End !If
                      If cht:Ref_Number <> job:Ref_Number      |
                          Then Break.  ! End If
      
                      Access:CONTHISTARC.ClearKey(chtarc:Record_Number_Key)
                      chtarc:record_number = cht:Record_Number
                      If Access:CONTHISTARC.TryFetch(chtarc:Record_Number_Key) = Level:Benign
                          !Found
                      Else!If Access:CONTHISTARC.TryFetch(chtarc:Record_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:CONTHISTARC.PrimeRecord() = Level:Benign
                              chtarc:Record :=: cht:Record
                              If Access:CONTHISTARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:CONTHISTARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !CONIf Access:CONTHISTARC.TryInsert() = Level:Benign
                          End !If Access:CONTHISTARC.PrimeRecord() = Level:Benign
      
                      End!If Access:CONTHISTARC.TryFetch(chtarc:Record_Number_Key) = Level:Benign
                      Delete(CONTHIST)
                  End !Loop
                  Access:CONTHIST.RestoreFile(Save_cht_ID)
      
                  Save_jbn_ID = Access:JOBNOTES.SaveFile()
                  Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
                  jbn:RefNumber = job:Ref_Number
                  Set(jbn:RefNumberKey,jbn:RefNumberKey)
                  Loop
                      If Access:JOBNOTES.NEXT()
                         Break
                      End !If
                      If jbn:RefNumber <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:JOBNOTESARC.Clearkey(jbnarc:RefNumberKey)
                      jbnarc:RefNumber    = job:Ref_Number
                      If Access:JOBNOTESARC.Tryfetch(jbnarc:RefNumberKey) = Level:Benign
                          !Found
      
                      Else! If Access:JOBNOTESARC.Tryfetch(jbnarc:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:JOBNOTESARC.PrimeRecord() = Level:Benign
                              jbnarc:Record   :=: jbn:Record
                              If Access:JOBNOTESARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBNOTESARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !JOBIf Access:JOBNOTESARC.TryInsert() = Level:Benign
                          End !If Access:JOBNOTESARC.PrimeRecord() = Level:Benign
                      End! If Access:.Tryfetch(jbnarc:RefNumberKey) = Level:Benign
                      Delete(JOBNOTES)
                  End !Loop
                  Access:JOBNOTES.RestoreFile(Save_jbn_ID)
      
                  Save_jobe_ID = Access:JOBSE.SaveFile()
                  Access:JOBSE.ClearKey(jobe:RefNumberKey)
                  jobe:RefNumber = job:Ref_Number
                  Set(jobe:RefNumberKey,jobe:RefNumberKey)
                  Loop
                      If Access:JOBSE.NEXT()
                         Break
                      End !If
                      If jobe:RefNumber <> job:Ref_Number      |
                          Then Break.  ! End If
      
                      Access:JOBSEARC.ClearKey(jobearc:RefNumberKey)
                      jobearc:RefNumber = job:Ref_Number
                      If Access:JOBSEARC.TryFetch(jobearc:RefNumberKey) = Level:Benign
                          !Found
      
                      Else!If Access:JOBSEARC.TryFetch(jobearc:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:JOBSEARC.PrimeRecord() = Level:Benign
                              jobearc:Record  :=: jobe:Record
                              If Access:JOBSEARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBSEARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !If Access:JOBSEARC.TryInsert() = Level:Benign
                          End !If Access:JOBSEARC.PrimeRecord() = Level:Benign
                      End!If Access:JOBSEARC.TryFetch(jobearc:RefNumberKey) = Level:Benign
                      Delete(JOBSE)
                  End !Loop
                  Access:JOBSE.RestoreFile(Save_jobe_ID)
      
                  Save_jst_ID = Access:JOBSTAGE.SaveFile()
                  Access:JOBSTAGE.ClearKey(jst:Ref_Number_Key)
                  jst:Ref_Number = job:Ref_Number
                  Set(jst:Ref_Number_Key,jst:Ref_Number_Key)
                  Loop
                      If Access:JOBSTAGE.NEXT()
                         Break
                      End !If
                      If jst:Ref_Number <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:JOBSTAGEARC.ClearKey(jstarc:Job_Stage_Key)
                      jstarc:Ref_Number = job:Ref_Number
                      jstarc:Job_Stage  = jst:Job_Stage
                      If Access:JOBSTAGEARC.TryFetch(jstarc:Job_Stage_Key) = Level:Benign
                          !Found
      
                      Else!If Access:JOBSTAGEARC.TryFetch(jstarc:Job_Stage_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:JOBSTAGEARC.PrimeRecord() = Level:Benign
                              jstarc:Record   :=: jst:Record
                              If Access:JOBSTAGEARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBSTAGEARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !JOBIf Access:JOBSTAGEARC.TryInsert() = Level:Benign
                          End !If Access:.PrimeRecord() = Level:Benign
                      End!If Access:JOBSTAGEARC.TryFetch(jstarc:Job_Stage_Key) = Level:Benign
                      Delete(JOBSTAGE)
                  End !Loop
                  Access:JOBSTAGE.RestoreFile(Save_jst_ID)
      
                  Save_jot_ID = Access:JOBTHIRD.SaveFile()
                  Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                  jot:RefNumber = job:Ref_Number
                  Set(jot:RefNumberKey,jot:RefNumberKey)
                  Loop
                      If Access:JOBTHIRD.NEXT()
                         Break
                      End !If
                      If jot:RefNumber <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:JOBTHIRDARC.ClearKey(jotarc:RecordNumberKey)
                      jotarc:RecordNumber = jot:RecordNumber
                      If Access:JOBTHIRDARC.TryFetch(jotarc:RecordNumberKey) = Level:Benign
                          !Found
      
                      Else!If Access:JOBTHIRDARC.TryFetch(jotarc:RecordNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:JOBTHIRDARC.PrimeRecord() = Level:Benign
                              jotarc:Record   :=: jot:Record
                              If Access:JOBTHIRDARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBTHIRDARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !JOBIf Access:JOBTHIRDARC.TryInsert() = Level:Benign
                          End !If Access:JOBTHIRDARC.PrimeRecord() = Level:Benign
                      End!If Access:JOBTHIRDARC.TryFetch(jotarc:RecordNumberKey) = Level:Benign
                      Delete(JOBTHIRD)
                  End !Loop
                  Access:JOBTHIRD.RestoreFile(Save_jot_ID)
      
                  Save_par_ID = Access:PARTS.SaveFile()
                  Access:PARTS.ClearKey(par:Part_Number_Key)
                  par:Ref_Number  = job:Ref_Number
                  Set(par:Part_Number_Key,par:Part_Number_Key)
                  Loop
                      If Access:PARTS.NEXT()
                         Break
                      End !If
                      If par:Ref_Number  <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:PARTSARC.ClearKey(pararc:recordnumberkey)
                      pararc:Record_Number = par:Record_Number
                      If Access:PARTSARC.TryFetch(pararc:recordnumberkey) = Level:Benign
                          !Found
      
                      Else!If Access:PARTSARC.TryFetch(pararc:recordnumberkey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:PARTSARC.PrimeRecord() = Level:Benign
                              pararc:Record   :=: par:Record
                              If Access:PARTSARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:PARTS.TryInsert() = Level:Benign
                                  !Insert Failed
                              End!If Access:PARTS.TryInsert() = Level:Benign
                          End !If Access:PARTS.PrimeRecord() = Level:Benign
                      End!If Access:PARTSARC.TryFetch(pararc:recordnumberkey) = Level:Benign
                      Delete(PARTS)
                  End !Loop
                  Access:PARTS.RestoreFile(Save_par_ID)
      
                  Save_wpr_ID = Access:WARPARTS.SaveFile()
                  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                  wpr:Ref_Number  = job:Ref_Number
                  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop
                      If Access:WARPARTS.NEXT()
                         Break
                      End !If
                      If wpr:Ref_Number  <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:WARPARTSARC.ClearKey(wprarc:RecordNumberKey)
                      wprarc:Record_Number = wpr:Record_Number
                      If Access:WARPARTSARC.TryFetch(wprarc:RecordNumberKey) = Level:Benign
                          !Found
      
                      Else!If Access:WARPARTSARC.TryFetch(wprarc:RecordNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:WARPARTSARC.PrimeRecord() = Level:Benign
                              wprarc:Record   :=: wpr:Record
                              If Access:WARPARTSARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:WARPARTSARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !WARIf Access:WARPARTSARC.TryInsert() = Level:Benign
                          End !If Access:WARPARTSARC.PrimeRecord() = Level:Benign
                      End!If Access:WARPARTSARC.TryFetch(wprarc:RecordNumberKey) = Level:Benign
                      DELETE(WARPARTS)
                  End !Loop
                  Access:WARPARTS.RestoreFile(Save_wpr_ID)
      
                  Save_epr_ID = Access:ESTPARTS.SaveFile()
                  Access:ESTPARTS.ClearKey(epr:Part_Number_Key)
                  epr:Ref_Number  = job:Ref_Number
                  Set(epr:Part_Number_Key,epr:Part_Number_Key)
                  Loop
                      If Access:ESTPARTS.NEXT()
                         Break
                      End !If
                      If epr:Ref_Number  <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:ESTPARTSARC.Clearkey(eprarc:Record_Number_Key)
                      eprarc:Record_Number   = epr:Record_Number
                      If Access:ESTPARTSARC.Tryfetch(eprarc:Record_Number_Key) = Level:Benign
                          !Found
      
                      Else! If Access:ESTPARTSARC.Tryfetch(eprarc:RecordNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:ESTPARTSARC.PrimeRecord() = Level:Benign
                              eprarc:Record   :=: epr:Record
                              If Access:ESTPARTSARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:ESTPARTSARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !ESTIf Access:ESTPARTSARC.TryInsert() = Level:Benign
                          End !If Access:ESTPARTSARC.PrimeRecord() = Level:Benign
                      End! If Access:ESTPARTSARC.Tryfetch(eprarc:RecordNumberKey) = Level:Benign
                      DELETE(ESTPARTS)
                  End !Loop
                  Access:ESTPARTS.RestoreFile(Save_epr_ID)
      
                  Save_jpt_ID = Access:JOBPAYMT.SaveFile()
                  Access:JOBPAYMT.ClearKey(jpt:All_Date_Key)
                  jpt:Ref_Number = job:Ref_Number
                  Set(jpt:All_Date_Key,jpt:All_Date_Key)
                  Loop
                      If Access:JOBPAYMT.NEXT()
                         Break
                      End !If
                      If jpt:Ref_Number <> job:Ref_Number      |
                          Then Break.  ! End If
                      Access:JOBPAYMTARC.Clearkey(jptarc:Record_Number_Key)
                      jptarc:Record_Number    = jpt:Record_Number
                      If Access:JOBPAYMTARC.Tryfetch(jptarc:Record_Number_Key) = Level:Benign
                          !Found
      
                      Else! If Access:JOBPAYMTARC.Tryfetch(jptarc:Record_Number_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:JOBPAYMTARC.PrimeRecord() = Level:Benign
                              jptarc:Record   :=: jpt:Record
                              If Access:JOBPAYMTARC.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBPAYMTARC.TryInsert() = Level:Benign
                                  !Insert Failed
                              End !JOBIf Access:JOBPAYMTARC.TryInsert() = Level:Benign
                          End !If Access:JOBPAYMTARC.PrimeRecord() = Level:Benign
                      End! If Access:JOBPAYMTARC.Tryfetch(jptarc:Record_Number_Key) = Level:Benign
                      DELETE(JOBPAYMT)
                  End !Loop
                  Access:JOBPAYMT.RestoreFile(Save_jpt_ID)
                  Delete(JOBS)
                  tmp:JobsDeleted += 1
      
      !            ! Start Change xxx BE(26/07/2004)
      !            TransactionCount# += 1
      !            IF (TransactionCount# > 500) THEN
      !                COMMIT()
      !                TransactionCount# = 0
      !                DO StartTransaction
      !            END
      !            ! End Change xxx BE(26/07/2004)
      
              End !Loop
      
      !        ! Start Change xxx BE(26/07/2004)
      !        COMMIT()
      !        ! End Change xxx BE(26/07/2004)
      
              Access:JOBS.RestoreFile(Save_JOB_ID)
      
              Do EndPrintRun
              close(progresswindow)
      
              Do CloseFiles
      
      !        RUN('BTPACK.BAT JOBS.DAT',1)
      !        RUN('BTPACK.BAT AUDIT.DAT',1)
      !        RUN('BTPACK.BAT AUDSTATS.DAT',1)
      !        RUN('BTPACK.BAT CONTHIST.DAT',1)
      !        RUN('BTPACK.BAT JOBSE.DAT',1)
      !        RUN('BTPACK.BAT JOBSTAGE.DAT',1)
      !        RUN('BTPACK.BAT JOBTHIRD.DAT',1)
      !        RUN('BTPACK.BAT PARTS.DAT',1)
      !        RUN('BTPACK.BAT WARPARTS.DAT',1)
      
              Case MessageEx('The Archive has completed.'&|
                '<13,10>'&|
                '<13,10>You may now compact the files using the "Pervasive Information Editor".'&|
                '<13,10>(Instructions on how to do this can be downloaded from the ServiceBase Cellular Update site.)','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Of 2 ! &No Button
      
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?BeginArchive, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'BeginArchive')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
    ThisMakeover.TakeEvent(Win:Wizard,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?tmp:StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupStartDate)
      CYCLE
    END
  OF ?tmp:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupEndDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Wizard,mo:SelectedTab::Sheet1,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()

