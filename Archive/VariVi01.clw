          MEMBER('archive.clw')                       ! This is a MEMBER module

   Map
   End

SolFileStatus       Procedure

  Code
  !if (SolaceCurrentTab = 5 and SolaceNoRefreshFileStatus = true) or SolaceNoRefreshFileStatus = false or SolaceSaveToFile = true then
    if Status(JOBS) then
      SolaceViewVars('JOBS',Thread(),'FileStatus',Status(JOBS),Records(JOBS))
    else
      SolaceViewVars('JOBS',Thread(),'FileStatus',Status(JOBS),0)
    end
    if Status(ESTPARTSARC) then
      SolaceViewVars('ESTPARTSARC',Thread(),'FileStatus',Status(ESTPARTSARC),Records(ESTPARTSARC))
    else
      SolaceViewVars('ESTPARTSARC',Thread(),'FileStatus',Status(ESTPARTSARC),0)
    end
    if Status(ESTPARTS) then
      SolaceViewVars('ESTPARTS',Thread(),'FileStatus',Status(ESTPARTS),Records(ESTPARTS))
    else
      SolaceViewVars('ESTPARTS',Thread(),'FileStatus',Status(ESTPARTS),0)
    end
    if Status(DEFARC) then
      SolaceViewVars('DEFARC',Thread(),'FileStatus',Status(DEFARC),Records(DEFARC))
    else
      SolaceViewVars('DEFARC',Thread(),'FileStatus',Status(DEFARC),0)
    end
    if Status(JOBSARC) then
      SolaceViewVars('JOBSARC',Thread(),'FileStatus',Status(JOBSARC),Records(JOBSARC))
    else
      SolaceViewVars('JOBSARC',Thread(),'FileStatus',Status(JOBSARC),0)
    end
    if Status(AUDITARC) then
      SolaceViewVars('AUDITARC',Thread(),'FileStatus',Status(AUDITARC),Records(AUDITARC))
    else
      SolaceViewVars('AUDITARC',Thread(),'FileStatus',Status(AUDITARC),0)
    end
    if Status(AUDSTATSARC) then
      SolaceViewVars('AUDSTATSARC',Thread(),'FileStatus',Status(AUDSTATSARC),Records(AUDSTATSARC))
    else
      SolaceViewVars('AUDSTATSARC',Thread(),'FileStatus',Status(AUDSTATSARC),0)
    end
    if Status(AUDSTATS) then
      SolaceViewVars('AUDSTATS',Thread(),'FileStatus',Status(AUDSTATS),Records(AUDSTATS))
    else
      SolaceViewVars('AUDSTATS',Thread(),'FileStatus',Status(AUDSTATS),0)
    end
    if Status(CONTHISTARC) then
      SolaceViewVars('CONTHISTARC',Thread(),'FileStatus',Status(CONTHISTARC),Records(CONTHISTARC))
    else
      SolaceViewVars('CONTHISTARC',Thread(),'FileStatus',Status(CONTHISTARC),0)
    end
    if Status(JOBPAYMTARC) then
      SolaceViewVars('JOBPAYMTARC',Thread(),'FileStatus',Status(JOBPAYMTARC),Records(JOBPAYMTARC))
    else
      SolaceViewVars('JOBPAYMTARC',Thread(),'FileStatus',Status(JOBPAYMTARC),0)
    end
    if Status(JOBPAYMT) then
      SolaceViewVars('JOBPAYMT',Thread(),'FileStatus',Status(JOBPAYMT),Records(JOBPAYMT))
    else
      SolaceViewVars('JOBPAYMT',Thread(),'FileStatus',Status(JOBPAYMT),0)
    end
    if Status(AUDIT) then
      SolaceViewVars('AUDIT',Thread(),'FileStatus',Status(AUDIT),Records(AUDIT))
    else
      SolaceViewVars('AUDIT',Thread(),'FileStatus',Status(AUDIT),0)
    end
    if Status(CONTHIST) then
      SolaceViewVars('CONTHIST',Thread(),'FileStatus',Status(CONTHIST),Records(CONTHIST))
    else
      SolaceViewVars('CONTHIST',Thread(),'FileStatus',Status(CONTHIST),0)
    end
    if Status(WARPARTSARC) then
      SolaceViewVars('WARPARTSARC',Thread(),'FileStatus',Status(WARPARTSARC),Records(WARPARTSARC))
    else
      SolaceViewVars('WARPARTSARC',Thread(),'FileStatus',Status(WARPARTSARC),0)
    end
    if Status(WARPARTS) then
      SolaceViewVars('WARPARTS',Thread(),'FileStatus',Status(WARPARTS),Records(WARPARTS))
    else
      SolaceViewVars('WARPARTS',Thread(),'FileStatus',Status(WARPARTS),0)
    end
    if Status(PARTSARC) then
      SolaceViewVars('PARTSARC',Thread(),'FileStatus',Status(PARTSARC),Records(PARTSARC))
    else
      SolaceViewVars('PARTSARC',Thread(),'FileStatus',Status(PARTSARC),0)
    end
    if Status(PARTS) then
      SolaceViewVars('PARTS',Thread(),'FileStatus',Status(PARTS),Records(PARTS))
    else
      SolaceViewVars('PARTS',Thread(),'FileStatus',Status(PARTS),0)
    end
    if Status(JOBNOTESARC) then
      SolaceViewVars('JOBNOTESARC',Thread(),'FileStatus',Status(JOBNOTESARC),Records(JOBNOTESARC))
    else
      SolaceViewVars('JOBNOTESARC',Thread(),'FileStatus',Status(JOBNOTESARC),0)
    end
    if Status(JOBNOTES) then
      SolaceViewVars('JOBNOTES',Thread(),'FileStatus',Status(JOBNOTES),Records(JOBNOTES))
    else
      SolaceViewVars('JOBNOTES',Thread(),'FileStatus',Status(JOBNOTES),0)
    end
    if Status(LOGGED) then
      SolaceViewVars('LOGGED',Thread(),'FileStatus',Status(LOGGED),Records(LOGGED))
    else
      SolaceViewVars('LOGGED',Thread(),'FileStatus',Status(LOGGED),0)
    end
    if Status(JOBSEARC) then
      SolaceViewVars('JOBSEARC',Thread(),'FileStatus',Status(JOBSEARC),Records(JOBSEARC))
    else
      SolaceViewVars('JOBSEARC',Thread(),'FileStatus',Status(JOBSEARC),0)
    end
    if Status(JOBTHIRDARC) then
      SolaceViewVars('JOBTHIRDARC',Thread(),'FileStatus',Status(JOBTHIRDARC),Records(JOBTHIRDARC))
    else
      SolaceViewVars('JOBTHIRDARC',Thread(),'FileStatus',Status(JOBTHIRDARC),0)
    end
    if Status(JOBSTAGEARC) then
      SolaceViewVars('JOBSTAGEARC',Thread(),'FileStatus',Status(JOBSTAGEARC),Records(JOBSTAGEARC))
    else
      SolaceViewVars('JOBSTAGEARC',Thread(),'FileStatus',Status(JOBSTAGEARC),0)
    end
    if Status(JOBTHIRD) then
      SolaceViewVars('JOBTHIRD',Thread(),'FileStatus',Status(JOBTHIRD),Records(JOBTHIRD))
    else
      SolaceViewVars('JOBTHIRD',Thread(),'FileStatus',Status(JOBTHIRD),0)
    end
    if Status(JOBSTAGE) then
      SolaceViewVars('JOBSTAGE',Thread(),'FileStatus',Status(JOBSTAGE),Records(JOBSTAGE))
    else
      SolaceViewVars('JOBSTAGE',Thread(),'FileStatus',Status(JOBSTAGE),0)
    end
    if Status(JOBSE) then
      SolaceViewVars('JOBSE',Thread(),'FileStatus',Status(JOBSE),Records(JOBSE))
    else
      SolaceViewVars('JOBSE',Thread(),'FileStatus',Status(JOBSE),0)
    end
  !End


SolSingleFileStatus     Procedure
  Code
  !if (SolaceCurrentTab = 3 and SolaceNoRefreshOneFile = true) or SolaceNoRefreshOneFile = false or SolaceSaveToFile = true then
    Free(FileFieldsQueue)
    Loop Solace# = 1 to Records(SolaceAllFiles)
      Get(SolaceAllFiles,Solace#)
      if Clip(SOLALL:FileName) = clip(SolaceCurrentFile) then
        SOLFFV:FieldName = SOLALL:FieldName
        SOLFFV:FieldValue = SOLALL:FieldValue
        Add(FileFieldsQueue)
      end
    end
  !end
