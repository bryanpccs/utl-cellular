

   MEMBER('celrapda.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA003.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA005.INC'),ONCE        !Req'd for module callout resolution
                     END


Update_Stock_Audit PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
Found_Match          BYTE
ActionMessage        CSTRING(40)
List_Query           STRING(30)
Available_Counter    LONG
Stock_Level          LONG
pos                  STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::stoa:Record LIKE(stoa:RECORD),STATIC
QuickWindow          WINDOW('Stock Take Update'),AT(,,292,116),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,HLP('Update_Stock_Audit'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,284,80),USE(?CurrentTab),SPREAD
                         TAB('Stock Details'),USE(?Tab:1)
                           STRING('PRELIMINARY STOCK CHECK TAKEN'),AT(128,68),USE(?String2),HIDE,FONT(,10,COLOR:Red,FONT:bold)
                           PROMPT('Part Number'),AT(8,20),USE(?STO:Internal_Stock_Code:Prompt)
                           ENTRY(@s30),AT(68,20,,10),USE(sto:Part_Number),SKIP,FONT(,,,FONT:bold),READONLY
                           PROMPT('Description'),AT(8,36),USE(?STO:Description:Prompt)
                           ENTRY(@s30),AT(68,36,,10),USE(sto:Description),SKIP,FONT(,,,FONT:bold),READONLY
                           PROMPT('Shelf Location'),AT(8,52),USE(?STO:Category:Prompt)
                           ENTRY(@s30),AT(68,52,,10),USE(sto:Shelf_Location),SKIP,FONT(,,,FONT:bold),READONLY
                           SPIN(@n-14),AT(68,68,56,10),USE(stoa:New_Level),RIGHT,FONT(,,,FONT:bold)
                           STRING('Stock Take Level'),AT(8,68),USE(?String1)
                         END
                       END
                       PANEL,AT(4,88,284,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(228,92,56,16),USE(?OK),FLAT,LEFT,ICON('OK.gif'),DEFAULT
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    ?STO:Internal_Stock_Code:Prompt{prop:FontColor} = -1
    ?STO:Internal_Stock_Code:Prompt{prop:Color} = 15066597
    If ?sto:Part_Number{prop:ReadOnly} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 15066597
    Elsif ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 8454143
    Else ! If ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 16777215
    End ! If ?sto:Part_Number{prop:Req} = True
    ?sto:Part_Number{prop:Trn} = 0
    ?sto:Part_Number{prop:FontStyle} = font:Bold
    ?STO:Description:Prompt{prop:FontColor} = -1
    ?STO:Description:Prompt{prop:Color} = 15066597
    If ?sto:Description{prop:ReadOnly} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 15066597
    Elsif ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 8454143
    Else ! If ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 16777215
    End ! If ?sto:Description{prop:Req} = True
    ?sto:Description{prop:Trn} = 0
    ?sto:Description{prop:FontStyle} = font:Bold
    ?STO:Category:Prompt{prop:FontColor} = -1
    ?STO:Category:Prompt{prop:Color} = 15066597
    If ?sto:Shelf_Location{prop:ReadOnly} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 15066597
    Elsif ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 8454143
    Else ! If ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 16777215
    End ! If ?sto:Shelf_Location{prop:Req} = True
    ?sto:Shelf_Location{prop:Trn} = 0
    ?sto:Shelf_Location{prop:FontStyle} = font:Bold
    If ?stoa:New_Level{prop:ReadOnly} = True
        ?stoa:New_Level{prop:FontColor} = 65793
        ?stoa:New_Level{prop:Color} = 15066597
    Elsif ?stoa:New_Level{prop:Req} = True
        ?stoa:New_Level{prop:FontColor} = 65793
        ?stoa:New_Level{prop:Color} = 8454143
    Else ! If ?stoa:New_Level{prop:Req} = True
        ?stoa:New_Level{prop:FontColor} = 65793
        ?stoa:New_Level{prop:Color} = 16777215
    End ! If ?stoa:New_Level{prop:Req} = True
    ?stoa:New_Level{prop:Trn} = 0
    ?stoa:New_Level{prop:FontStyle} = font:Bold
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Stock_Audit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Stock_Audit',1)
    SolaceViewVars('Found_Match',Found_Match,'Update_Stock_Audit',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Stock_Audit',1)
    SolaceViewVars('List_Query',List_Query,'Update_Stock_Audit',1)
    SolaceViewVars('Available_Counter',Available_Counter,'Update_Stock_Audit',1)
    SolaceViewVars('Stock_Level',Stock_Level,'Update_Stock_Audit',1)
    SolaceViewVars('pos',pos,'Update_Stock_Audit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Internal_Stock_Code:Prompt;  SolaceCtrlName = '?STO:Internal_Stock_Code:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Description:Prompt;  SolaceCtrlName = '?STO:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Category:Prompt;  SolaceCtrlName = '?STO:Category:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location;  SolaceCtrlName = '?sto:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stoa:New_Level;  SolaceCtrlName = '?stoa:New_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Stock_Audit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Stock_Audit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(stoa:Record,History::stoa:Record)
  SELF.AddHistoryField(?stoa:New_Level,5)
  SELF.AddUpdateFile(Access:STOAUDIT)
  Relate:GENSHORT.Open
  Relate:STOAUDIT.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOAUDIT
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:GENSHORT.Close
    Relate:STOAUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Stock_Audit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  Case MessageEx('Would you like to confirm this quantity, or mark this stock item to be checked again? (Preliminary check)'&|
    '<13,10>'&|
    '<13,10>If you do not press either button, the system will assume that you are confirming this quantity.','ServiceBase 2000',|
                 'Styles\stop.ico','|&Confirm|&Preliminary',1,2,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,3000) 
      Of 1 ! &Confirm Button
        STOA:Confirmed = 'Y'
        STOA:Preliminary = ''
      Of 2 ! &Preliminary Button
        STOA:Preliminary = 'Y'
        STOA:Confirmed = ''
      ELSE
        STOA:Confirmed = 'Y'
        STOA:Preliminary = ''
  End!Case MessageEx
  
  IF STOA:Preliminary = ''
    !Nothing Recorded!
    IF STOA:New_Level <> STOA:Original_Level
      IF STOA:New_Level < STOA:Original_Level
        Global_Long = sto:Ref_Number
        !hah!
        GlobalRequest=InsertRecord
        Deplete_Stock(STOA:Original_Level-STOA:New_Level)
        IF Global_Long <> 0 THEN
          !sto:Quantity_Stock -= STOA:Original_Level-STOA:New_Level
          !STO:Quantity_Available -= STOA:Original_Level-STOA:New_Level
          !STO:Send = 'Y'
        END !IF
        Access:Stock.Update()
        GlobalRequest=ChangeRecord
        !Add one the general shortage!
        Access:GenShort.PrimeRecord()
        GENS:Audit_No      = STOA:Audit_Ref_No
        GENS:Stock_Ref_No  = sto:Ref_Number
        GENS:Stock_Qty     = (STOA:Original_Level-STOA:New_Level) * -1
        Access:GenShort.Update()
      ELSE
        Global_Long = sto:Ref_Number
        !Global_One_String = STO:Stock_Type
        GlobalRequest=InsertRecord
        Add_Stock(STOA:New_Level-STOA:Original_Level)
        IF Global_Long <> 0 THEN
          !sto:Quantity_Stock += STOA:New_Level-STOA:Original_Level
          !STO:Quantity_Available += STOA:New_Level-STOA:Original_Level
          !STO:Send = 'Y'
        END !IF
        Access:Stock.Update()
        GlobalRequest=ChangeRecord
        !Add one the general shortage!
        Access:GenShort.PrimeRecord()
        GENS:Audit_No      = STOA:Audit_Ref_No
        GENS:Stock_Ref_No  = sto:Ref_Number
        GENS:Stock_Qty     = STOA:New_Level-STOA:Original_Level
        Access:GenShort.Update()
      END
    END
  END
  
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Stock_Audit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      IF Keycode() = ESCKey
        CYCLE
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

