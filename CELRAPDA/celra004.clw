

   MEMBER('celrapda.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA004.INC'),ONCE        !Local module procedure declarations
                     END


Add_Stock PROCEDURE (Passed_Quantity)                 !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
percentage_mark_up_temp REAL
no_of_labels_temp    REAL
retail_cost_temp     REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Add Stock'),AT(,,219,211),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),TILED,GRAY,DOUBLE,MDI,IMM
                       SHEET,AT(4,4,212,176),USE(?Sheet1),SPREAD
                         TAB('Add Stock'),USE(?AddStock)
                           PROMPT('Quantity'),AT(8,20),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(84,20,64,10),USE(quantity_temp),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY,RANGE(1,99999999)
                           PROMPT('Despatch Note'),AT(8,36),USE(?glo:select2:Prompt)
                           ENTRY(@s30),AT(84,36,124,10),USE(despatch_note_temp),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           PROMPT('Purchase Cost'),AT(8,52),USE(?glo:select3:Prompt)
                           ENTRY(@n14.2),AT(84,52,64,10),USE(purchase_cost_Temp),SKIP,FONT('Tahoma',8,,FONT:bold),READONLY
                           PROMPT('Percentage Mark Up'),AT(8,68),USE(?percentage_mark_up_temp:Prompt)
                           ENTRY(@n7.2),AT(84,68,64,10),USE(percentage_mark_up_temp),SKIP,FONT('Tahoma',8,,FONT:bold),READONLY
                           PROMPT('  %  '),AT(148,68),USE(?Prompt8)
                           PROMPT('Trade Price'),AT(8,84),USE(?glo:select4:Prompt)
                           ENTRY(@n14.2),AT(84,84,64,10),USE(sale_cost_temp),SKIP,FONT('Tahoma',8,,FONT:bold),READONLY
                           PROMPT('Retail Price'),AT(8,100),USE(?retail_cost_temp:Prompt),TRN
                           ENTRY(@n14.2),AT(84,100,64,10),USE(retail_cost_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),READONLY
                           PROMPT('Date Received'),AT(8,116),USE(?glo:select5:Prompt)
                           ENTRY(@d6b),AT(84,116,64,10),USE(date_received_temp),SKIP,FONT('Tahoma',,,FONT:bold),READONLY
                           PROMPT('Additional Notes'),AT(8,132),USE(?Prompt6)
                           TEXT,AT(84,132,124,28),USE(additional_notes_temp),FONT(,,,FONT:bold),REQ,UPR
                           PROMPT('Number Of Labels'),AT(8,164),USE(?no_of_labels_temp:Prompt),TRN
                           SPIN(@n6),AT(84,164,64,10),USE(no_of_labels_temp),DISABLE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,STEP(1)
                         END
                       END
                       PANEL,AT(4,184,212,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(156,188,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(8,188,56,16),USE(?CancelButton),HIDE,LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?AddStock{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?quantity_temp{prop:ReadOnly} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 15066597
    Elsif ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 8454143
    Else ! If ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 16777215
    End ! If ?quantity_temp{prop:Req} = True
    ?quantity_temp{prop:Trn} = 0
    ?quantity_temp{prop:FontStyle} = font:Bold
    ?glo:select2:Prompt{prop:FontColor} = -1
    ?glo:select2:Prompt{prop:Color} = 15066597
    If ?despatch_note_temp{prop:ReadOnly} = True
        ?despatch_note_temp{prop:FontColor} = 65793
        ?despatch_note_temp{prop:Color} = 15066597
    Elsif ?despatch_note_temp{prop:Req} = True
        ?despatch_note_temp{prop:FontColor} = 65793
        ?despatch_note_temp{prop:Color} = 8454143
    Else ! If ?despatch_note_temp{prop:Req} = True
        ?despatch_note_temp{prop:FontColor} = 65793
        ?despatch_note_temp{prop:Color} = 16777215
    End ! If ?despatch_note_temp{prop:Req} = True
    ?despatch_note_temp{prop:Trn} = 0
    ?despatch_note_temp{prop:FontStyle} = font:Bold
    ?glo:select3:Prompt{prop:FontColor} = -1
    ?glo:select3:Prompt{prop:Color} = 15066597
    If ?purchase_cost_Temp{prop:ReadOnly} = True
        ?purchase_cost_Temp{prop:FontColor} = 65793
        ?purchase_cost_Temp{prop:Color} = 15066597
    Elsif ?purchase_cost_Temp{prop:Req} = True
        ?purchase_cost_Temp{prop:FontColor} = 65793
        ?purchase_cost_Temp{prop:Color} = 8454143
    Else ! If ?purchase_cost_Temp{prop:Req} = True
        ?purchase_cost_Temp{prop:FontColor} = 65793
        ?purchase_cost_Temp{prop:Color} = 16777215
    End ! If ?purchase_cost_Temp{prop:Req} = True
    ?purchase_cost_Temp{prop:Trn} = 0
    ?purchase_cost_Temp{prop:FontStyle} = font:Bold
    ?percentage_mark_up_temp:Prompt{prop:FontColor} = -1
    ?percentage_mark_up_temp:Prompt{prop:Color} = 15066597
    If ?percentage_mark_up_temp{prop:ReadOnly} = True
        ?percentage_mark_up_temp{prop:FontColor} = 65793
        ?percentage_mark_up_temp{prop:Color} = 15066597
    Elsif ?percentage_mark_up_temp{prop:Req} = True
        ?percentage_mark_up_temp{prop:FontColor} = 65793
        ?percentage_mark_up_temp{prop:Color} = 8454143
    Else ! If ?percentage_mark_up_temp{prop:Req} = True
        ?percentage_mark_up_temp{prop:FontColor} = 65793
        ?percentage_mark_up_temp{prop:Color} = 16777215
    End ! If ?percentage_mark_up_temp{prop:Req} = True
    ?percentage_mark_up_temp{prop:Trn} = 0
    ?percentage_mark_up_temp{prop:FontStyle} = font:Bold
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?glo:select4:Prompt{prop:FontColor} = -1
    ?glo:select4:Prompt{prop:Color} = 15066597
    If ?sale_cost_temp{prop:ReadOnly} = True
        ?sale_cost_temp{prop:FontColor} = 65793
        ?sale_cost_temp{prop:Color} = 15066597
    Elsif ?sale_cost_temp{prop:Req} = True
        ?sale_cost_temp{prop:FontColor} = 65793
        ?sale_cost_temp{prop:Color} = 8454143
    Else ! If ?sale_cost_temp{prop:Req} = True
        ?sale_cost_temp{prop:FontColor} = 65793
        ?sale_cost_temp{prop:Color} = 16777215
    End ! If ?sale_cost_temp{prop:Req} = True
    ?sale_cost_temp{prop:Trn} = 0
    ?sale_cost_temp{prop:FontStyle} = font:Bold
    ?retail_cost_temp:Prompt{prop:FontColor} = -1
    ?retail_cost_temp:Prompt{prop:Color} = 15066597
    If ?retail_cost_temp{prop:ReadOnly} = True
        ?retail_cost_temp{prop:FontColor} = 65793
        ?retail_cost_temp{prop:Color} = 15066597
    Elsif ?retail_cost_temp{prop:Req} = True
        ?retail_cost_temp{prop:FontColor} = 65793
        ?retail_cost_temp{prop:Color} = 8454143
    Else ! If ?retail_cost_temp{prop:Req} = True
        ?retail_cost_temp{prop:FontColor} = 65793
        ?retail_cost_temp{prop:Color} = 16777215
    End ! If ?retail_cost_temp{prop:Req} = True
    ?retail_cost_temp{prop:Trn} = 0
    ?retail_cost_temp{prop:FontStyle} = font:Bold
    ?glo:select5:Prompt{prop:FontColor} = -1
    ?glo:select5:Prompt{prop:Color} = 15066597
    If ?date_received_temp{prop:ReadOnly} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 15066597
    Elsif ?date_received_temp{prop:Req} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 8454143
    Else ! If ?date_received_temp{prop:Req} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 16777215
    End ! If ?date_received_temp{prop:Req} = True
    ?date_received_temp{prop:Trn} = 0
    ?date_received_temp{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?additional_notes_temp{prop:ReadOnly} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 15066597
    Elsif ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 8454143
    Else ! If ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 16777215
    End ! If ?additional_notes_temp{prop:Req} = True
    ?additional_notes_temp{prop:Trn} = 0
    ?additional_notes_temp{prop:FontStyle} = font:Bold
    ?no_of_labels_temp:Prompt{prop:FontColor} = -1
    ?no_of_labels_temp:Prompt{prop:Color} = 15066597
    If ?no_of_labels_temp{prop:ReadOnly} = True
        ?no_of_labels_temp{prop:FontColor} = 65793
        ?no_of_labels_temp{prop:Color} = 15066597
    Elsif ?no_of_labels_temp{prop:Req} = True
        ?no_of_labels_temp{prop:FontColor} = 65793
        ?no_of_labels_temp{prop:Color} = 8454143
    Else ! If ?no_of_labels_temp{prop:Req} = True
        ?no_of_labels_temp{prop:FontColor} = 65793
        ?no_of_labels_temp{prop:Color} = 16777215
    End ! If ?no_of_labels_temp{prop:Req} = True
    ?no_of_labels_temp{prop:Trn} = 0
    ?no_of_labels_temp{prop:FontStyle} = font:Bold
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Hide_Fields     Routine
    If percentage_mark_up_temp <> ''
        ?sale_cost_temp{prop:readonly} = 1
        ?sale_cost_temp{prop:skip} = 1
        ?sale_cost_temp{prop:color} = color:silver
        sale_cost_temp = purchase_cost_temp + (purchase_cost_temp * (percentage_mark_up_temp/100))
    Else
        ?sale_cost_temp{prop:readonly} = 0
        ?sale_cost_temp{prop:skip} = 0
        ?sale_cost_temp{prop:color} = color:white
    End
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Add_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Add_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Add_Stock',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Add_Stock',1)
    SolaceViewVars('purchase_cost_Temp',purchase_cost_Temp,'Add_Stock',1)
    SolaceViewVars('sale_cost_temp',sale_cost_temp,'Add_Stock',1)
    SolaceViewVars('date_received_temp',date_received_temp,'Add_Stock',1)
    SolaceViewVars('additional_notes_temp',additional_notes_temp,'Add_Stock',1)
    SolaceViewVars('despatch_note_temp',despatch_note_temp,'Add_Stock',1)
    SolaceViewVars('percentage_mark_up_temp',percentage_mark_up_temp,'Add_Stock',1)
    SolaceViewVars('no_of_labels_temp',no_of_labels_temp,'Add_Stock',1)
    SolaceViewVars('retail_cost_temp',retail_cost_temp,'Add_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AddStock;  SolaceCtrlName = '?AddStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_temp;  SolaceCtrlName = '?quantity_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select2:Prompt;  SolaceCtrlName = '?glo:select2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_note_temp;  SolaceCtrlName = '?despatch_note_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select3:Prompt;  SolaceCtrlName = '?glo:select3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?purchase_cost_Temp;  SolaceCtrlName = '?purchase_cost_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?percentage_mark_up_temp:Prompt;  SolaceCtrlName = '?percentage_mark_up_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?percentage_mark_up_temp;  SolaceCtrlName = '?percentage_mark_up_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select4:Prompt;  SolaceCtrlName = '?glo:select4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sale_cost_temp;  SolaceCtrlName = '?sale_cost_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retail_cost_temp:Prompt;  SolaceCtrlName = '?retail_cost_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retail_cost_temp;  SolaceCtrlName = '?retail_cost_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?glo:select5:Prompt;  SolaceCtrlName = '?glo:select5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?date_received_temp;  SolaceCtrlName = '?date_received_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?additional_notes_temp;  SolaceCtrlName = '?additional_notes_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?no_of_labels_temp:Prompt;  SolaceCtrlName = '?no_of_labels_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?no_of_labels_temp;  SolaceCtrlName = '?no_of_labels_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Add_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Add_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt7
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:STOAUDIT.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:STOAUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Add_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?purchase_cost_Temp
      !Do Hide_Fields
    OF ?percentage_mark_up_temp
      !Do Hide_Fields
    OF ?OkButton
      ThisWindow.Update
      finish# = 1
      If date_received_temp = ''
          Select(?date_received_temp)
          finish# = 0
      End
      If finish# = 1
          If quantity_temp = ''
              Select(?quantity_temp)
              finish# = 0
          End
      End
      If finish# = 1
          If sto:purchase_cost <> purchase_cost_temp Or sto:sale_cost <> sale_cost_temp Or sto:retail_cost <> retail_cost_temp
              Case MessageEx('Are you sure you want to alter the costings of this stock item?','ServiceBase 2000','Styles\question.ico','|&OK|&Cancel',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
                  Of 2 ! &Cancel Button
                      purchase_cost_temp = sto:purchase_cost
                      sale_cost_temp = sto:sale_cost
                      retail_cost_temp    = sto:retail_cost
                      percentage_mark_up_temp = sto:percentage_mark_up
                      Do Hide_Fields
                      Display()
              End!Case MessageEx
          End
          sto:purchase_cost           = purchase_cost_temp
          sto:sale_cost               = sale_cost_temp
          sto:retail_cost             = retail_cost_temp
          sto:percentage_mark_up      = percentage_mark_up_temp
          sto:quantity_stock          += quantity_temp
          access:stock.update()
          Set(defaults)
          access:Defaults.next()
          Loop x# = 1 to no_of_labels_temp
              glo:select1 = sto:ref_number
              Case def:label_printer_type
                  Of 'TEC B-440 / B-442'
                      Stock_Label(Today(),quantity_temp,'N/A',despatch_note_temp,sale_cost_temp)
                  Of 'TEC B-452'
                      Stock_Label_B452(Today(),quantity_temp,'N/A',despatch_note_temp,sale_cost_temp)
              End!Case def:label_printer_type
          End!Loop x# = 1 to no_of_labels_temp
          
          glo:select1 = ''
      
          shi:ref_number              = sto:ref_number
          shi:transaction_type        = 'ADD'
          shi:despatch_note_number    = despatch_note_temp
          shi:quantity                = quantity_temp
          shi:date                    = Today()
          shi:purchase_cost           = purchase_cost_temp
          shi:sale_cost               = sale_cost_temp
          shi:retail_cost             = retail_cost_temp
          shi:information             = additional_notes_temp
          shi:job_number              = ''
          access:users.clearkey(use:password_key)
          use:password =glo:password
          access:users.fetch(use:password_key)
          shi:user                    = use:user_code
          shi:notes                   =   'STOCK ADDED'
          access:stohist.insert()
          Post(Event:CloseWindow)
      End
    OF ?CancelButton
      ThisWindow.Update
       POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Add_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      IF Keycode() = ESCKey
        CYCLE
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      quantity_temp = Passed_Quantity
      !access:stock.clearkey(sto:ref_number_key)
      !sto:ref_number  = glo:select1
      !If access:stock.fetch(sto:ref_number_key)
      !    Return Level:Fatal
      !End
      date_received_temp = Today()
      purchase_cost_Temp = STO:Purchase_Cost
      sale_cost_temp = STO:Sale_Cost
      retail_cost_temp    = sto:Retail_cost
      percentage_mark_up_temp = STO:Percentage_Mark_Up
      despatch_note_temp = 'STOCK AUDIT'
      
      !Do Hide_Fields
      !Labels
      set(defaults)
      access:defaults.next()
      If def:receive_stock_label = 'YES'
          Enable(?no_of_labels_temp)
          no_of_labels_temp = 1
      Else!If def:receive_stock_label = 'YES'
          Disable(?no_of_labels_temp)
          no_of_labels_temp = 0
      End!If def:receive_stock_label = 'YES'
      Display()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

