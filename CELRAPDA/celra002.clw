

   MEMBER('celrapda.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA003.INC'),ONCE        !Req'd for module callout resolution
                     END


Stock_Check_Procedure PROCEDURE                       !Generated from procedure template - Window

CurrentTab           STRING(80)
Stock_Query          STRING(30)
LOC:Amount           DECIMAL(15,2)
No_Find              BYTE
BrLocator1           STRING(50)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Master_Location      STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Master_Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOAUDIT)
                       PROJECT(stoa:Original_Level)
                       PROJECT(stoa:New_Level)
                       PROJECT(stoa:Internal_AutoNumber)
                       PROJECT(stoa:Audit_Ref_No)
                       PROJECT(stoa:Site_Location)
                       PROJECT(stoa:Stock_Ref_No)
                       JOIN(sto:Ref_Number_Key,stoa:Stock_Ref_No)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                         PROJECT(sto:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Part_Number_NormalFG LONG                         !Normal forground color
sto:Part_Number_NormalBG LONG                         !Normal background color
sto:Part_Number_SelectedFG LONG                       !Selected forground color
sto:Part_Number_SelectedBG LONG                       !Selected background color
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Description_NormalFG LONG                         !Normal forground color
sto:Description_NormalBG LONG                         !Normal background color
sto:Description_SelectedFG LONG                       !Selected forground color
sto:Description_SelectedBG LONG                       !Selected background color
stoa:Original_Level    LIKE(stoa:Original_Level)      !List box control field - type derived from field
stoa:Original_Level_NormalFG LONG                     !Normal forground color
stoa:Original_Level_NormalBG LONG                     !Normal background color
stoa:Original_Level_SelectedFG LONG                   !Selected forground color
stoa:Original_Level_SelectedBG LONG                   !Selected background color
stoa:New_Level         LIKE(stoa:New_Level)           !List box control field - type derived from field
stoa:New_Level_NormalFG LONG                          !Normal forground color
stoa:New_Level_NormalBG LONG                          !Normal background color
stoa:New_Level_SelectedFG LONG                        !Selected forground color
stoa:New_Level_SelectedBG LONG                        !Selected background color
Master_Location        LIKE(Master_Location)          !Browse hot field - type derived from local data
stoa:Internal_AutoNumber LIKE(stoa:Internal_AutoNumber) !Primary key field - type derived from field
stoa:Audit_Ref_No      LIKE(stoa:Audit_Ref_No)        !Browse key field - type derived from field
stoa:Site_Location     LIKE(stoa:Site_Location)       !Browse key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK8::stoa:Audit_Ref_No    LIKE(stoa:Audit_Ref_No)
HK8::stoa:Site_Location   LIKE(stoa:Site_Location)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB4::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse Stock '),AT(,,320,168),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,HLP('Stock_Check_Procedure'),SYSTEM,GRAY,RESIZE,MDI
                       SHEET,AT(4,4,312,160),USE(?CurrentTab),SPREAD
                         TAB('Stock Audit'),USE(?Tab:2)
                           COMBO(@s30),AT(8,20,124,10),USE(Master_Location),IMM,FONT(,,,FONT:bold),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           ENTRY(@s30),AT(8,36,124,10),USE(Stock_Query),FONT(,,,FONT:bold),UPR
                           BOX,AT(244,40,8,8),USE(?Box1),COLOR(COLOR:Red),FILL(COLOR:Red)
                           STRING('Preliminary Check'),AT(256,40),USE(?String1)
                         END
                       END
                       LIST,AT(8,52,304,108),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('124L(2)|M*~Part Number~@s30@125L(2)|M*~Description~@s30@56R(2)|M*~Original Level' &|
   '~L@n-14@56R(2)|M*~Audited~L@n-14@'),FROM(Queue:Browse:1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?Master_Location{prop:ReadOnly} = True
        ?Master_Location{prop:FontColor} = 65793
        ?Master_Location{prop:Color} = 15066597
    Elsif ?Master_Location{prop:Req} = True
        ?Master_Location{prop:FontColor} = 65793
        ?Master_Location{prop:Color} = 8454143
    Else ! If ?Master_Location{prop:Req} = True
        ?Master_Location{prop:FontColor} = 65793
        ?Master_Location{prop:Color} = 16777215
    End ! If ?Master_Location{prop:Req} = True
    ?Master_Location{prop:Trn} = 0
    ?Master_Location{prop:FontStyle} = font:Bold
    If ?Stock_Query{prop:ReadOnly} = True
        ?Stock_Query{prop:FontColor} = 65793
        ?Stock_Query{prop:Color} = 15066597
    Elsif ?Stock_Query{prop:Req} = True
        ?Stock_Query{prop:FontColor} = 65793
        ?Stock_Query{prop:Color} = 8454143
    Else ! If ?Stock_Query{prop:Req} = True
        ?Stock_Query{prop:FontColor} = 65793
        ?Stock_Query{prop:Color} = 16777215
    End ! If ?Stock_Query{prop:Req} = True
    ?Stock_Query{prop:Trn} = 0
    ?Stock_Query{prop:FontStyle} = font:Bold
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Check_Procedure',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Stock_Check_Procedure',1)
    SolaceViewVars('Stock_Query',Stock_Query,'Stock_Check_Procedure',1)
    SolaceViewVars('LOC:Amount',LOC:Amount,'Stock_Check_Procedure',1)
    SolaceViewVars('No_Find',No_Find,'Stock_Check_Procedure',1)
    SolaceViewVars('BrLocator1',BrLocator1,'Stock_Check_Procedure',1)
    SolaceViewVars('Master_Location',Master_Location,'Stock_Check_Procedure',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Master_Location;  SolaceCtrlName = '?Master_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Query;  SolaceCtrlName = '?Stock_Query';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Check_Procedure')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Check_Procedure')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Master_Location
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Access:STMASAUD.UseFile
  SELF.FilesOpened = True
  SELECT(?Stock_Query)
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOAUDIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stoa:Stock_Site_Number_Key)
  BRW1.AddRange(stoa:Site_Location)
  BIND('Master_Location',Master_Location)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(stoa:Original_Level,BRW1.Q.stoa:Original_Level)
  BRW1.AddField(stoa:New_Level,BRW1.Q.stoa:New_Level)
  BRW1.AddField(Master_Location,BRW1.Q.Master_Location)
  BRW1.AddField(stoa:Internal_AutoNumber,BRW1.Q.stoa:Internal_AutoNumber)
  BRW1.AddField(stoa:Audit_Ref_No,BRW1.Q.stoa:Audit_Ref_No)
  BRW1.AddField(stoa:Site_Location,BRW1.Q.stoa:Site_Location)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB4.Init(Master_Location,?Master_Location,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder()
  FDCB4.AddField(loc:Location,FDCB4.Q.loc:Location)
  FDCB4.AddField(loc:RecordNumber,FDCB4.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Check_Procedure',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Stock_Query)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Stock_Query
      !First up, lets get the stock code!
      No_Find = FALSE
      Access:Stock.ClearKey(sto:Location_Key)
      sto:Location = Master_Location
      sto:Part_Number = Stock_Query
      IF Access:Stock.Fetch(sto:Location_Key)
        No_Find = True
        BEEP(BEEP:SystemQuestion)  ;  YIELD()
        CASE MESSAGE('This stock code cannot be located currently.'&|
           '|Please confirm the stock code again.', |
           'Sellfone 2000', ICON:Question, |
            BUTTON:OK, BUTTON:OK, 0)
        OF BUTTON:OK
        END !CASE
      END
      
      IF No_Find = FALSE  !We got a code!
        !Check to see if it exists and we are not amending!
        Access:StoAudit.ClearKey(STOA:Lock_Down_Key)
        STOA:Audit_Ref_No = STOM:Audit_No
        STOA:Stock_Ref_No = sto:Ref_Number
        IF Access:StoAudit.Fetch(STOA:Lock_Down_Key)
          !Error, do this
          Access:StoAudit.PrimeRecord() !This will autonumber it for me!
          STOA:Stock_Ref_No = sto:Ref_Number !This will lock it to the stock item in question!
          STOA:Original_Level = sto:Quantity_Stock !Establish PC's stock level
          STOA:Audit_Ref_No = STOM:Audit_No  !When implemented, this will make this part of a sub-audit.
          STOA:Site_Location = Master_Location
          Access:StoAudit.Update()
          GlobalRequest = ChangeRecord
          Update_Stock_Audit
        ELSE
          !Change it - but ONLY if not completed
          IF STOA:Confirmed <> 'Y'
            GlobalRequest = ChangeRecord
            Update_Stock_Audit
          ELSE
            Case MessageEx('This item has been audited already.','Sellfone 2000',icon:asterisk,'|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemasterisk,0+2+0+0,84,26,600) 
              Of 1 ! &OK Button
            End!Case MessagheEx
          END
        END
        !locate it and select it in the list...er....!
      END
      !Reset Locator!
      BRW1.ResetSort(1)
      Stock_Query = ''
      UPDATE()
      DISPLAY()
      SELECT(?Stock_Query)
      
      
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Check_Procedure')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Master_Location
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Stock_Query)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = STOM:Audit_No
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = Master_Location
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  IF (STOA:Preliminary = 'Y')
    SELF.Q.sto:Part_Number_NormalFG = 255
    SELF.Q.sto:Part_Number_NormalBG = 16777215
    SELF.Q.sto:Part_Number_SelectedFG = 16777215
    SELF.Q.sto:Part_Number_SelectedBG = 255
  ELSE
    SELF.Q.sto:Part_Number_NormalFG = -1
    SELF.Q.sto:Part_Number_NormalBG = -1
    SELF.Q.sto:Part_Number_SelectedFG = -1
    SELF.Q.sto:Part_Number_SelectedBG = -1
  END
  IF (STOA:Preliminary = 'Y')
    SELF.Q.sto:Description_NormalFG = 255
    SELF.Q.sto:Description_NormalBG = 16777215
    SELF.Q.sto:Description_SelectedFG = 16777215
    SELF.Q.sto:Description_SelectedBG = 255
  ELSE
    SELF.Q.sto:Description_NormalFG = -1
    SELF.Q.sto:Description_NormalBG = -1
    SELF.Q.sto:Description_SelectedFG = -1
    SELF.Q.sto:Description_SelectedBG = -1
  END
  SELF.Q.stoa:Original_Level_NormalFG = -1
  SELF.Q.stoa:Original_Level_NormalBG = -1
  SELF.Q.stoa:Original_Level_SelectedFG = -1
  SELF.Q.stoa:Original_Level_SelectedBG = -1
  SELF.Q.stoa:New_Level_NormalFG = -1
  SELF.Q.stoa:New_Level_NormalBG = -1
  SELF.Q.stoa:New_Level_SelectedFG = -1
  SELF.Q.stoa:New_Level_SelectedBG = -1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

