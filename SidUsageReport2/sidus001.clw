

   MEMBER('sidusag2.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SIDUS001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::2:TAGFLAG          BYTE(0)
DASBRW::2:TAGMOUSE         BYTE(0)
DASBRW::2:TAGDISPSTATUS    BYTE(0)
DASBRW::2:QUEUE           QUEUE
Pointer10                     LIKE(GLO:Pointer10)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
AccountTag           STRING(1)
Pathname             STRING(255)
BRW1::View:Browse    VIEW(TRADEACC)
                       PROJECT(tra:Account_Number)
                       PROJECT(tra:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
AccountTag             LIKE(AccountTag)               !List box control field - type derived from local data
AccountTag_Icon        LONG                           !Entry's icon ID
tra:Account_Number     LIKE(tra:Account_Number)       !List box control field - type derived from field
tra:RecordNumber       LIKE(tra:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('SID Usage Report'),AT(,,212,203),FONT('Arial',8,,),CENTER,ICON('pc.ico'),SYSTEM,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,204,168),USE(?Sheet1),WIZARD,SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Filename'),AT(8,12),USE(?Prompt2)
                           ENTRY(@s255),AT(40,12,148,10),USE(Pathname)
                           BUTTON('...'),AT(192,12,10,10),USE(?FileButton),LEFT,ICON('list3.ico')
                           PROMPT('Choose Trade Accounts'),AT(8,28),USE(?Prompt1)
                           LIST,AT(8,40,196,104),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LJ@s1@60L|M~Account Number~L(2)@s15@'),FROM(Queue:Browse)
                           BUTTON('sho&W tags'),AT(124,92,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Rev tags'),AT(136,116,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('&Untag all'),AT(140,148,60,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           BUTTON('tag &All'),AT(76,148,60,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Tag'),AT(12,148,60,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                         END
                       END
                       PANEL,AT(4,176,204,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Export'),AT(76,180,60,16),USE(?ExportButton),LEFT,ICON('ok.ico')
                       BUTTON('Cancel'),AT(140,180,60,16),USE(?CancelButton),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
! ProgressWindow Declarations

RecordsToProcess        LONG,AUTO
RecordsProcessed        LONG,AUTO
RecordsPerCycle         LONG,AUTO
RecordsThisCycle        LONG,AUTO
PercentProgress         LONG,AUTO
ProgressThermometer     LONG,AUTO
AppName                 STRING(30)
ProgressWindow          WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
                            PROGRESS,USE(ProgressThermometer),AT(25,15,111,12),RANGE(0,100)
                            STRING(''),AT(0,3,161,10),USE(?ProgressUserString),CENTER,FONT('Arial',8,,)
                            STRING(''),AT(0,30,161,10),USE(?ProgressText),TRN,CENTER,FONT('Arial',8,,)
                            BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
                        END

               MAP
ProgressBarShow         PROCEDURE(STRING arg:AppNameString, LONG arg:RecordCount=1000,  |
                                  LONG arg:CycleCount=25,   <STRING arg:UserText>)
ProgressBarReset        PROCEDURE(LONG arg:RecordCount=1000, <LONG arg:CycleCount>, <STRING arg:UserText>)
ProgressBarUpdate       PROCEDURE(),LONG
ProgressBarCancelled    PROCEDURE(),LONG
ProgressBarClose        PROCEDURE()
              END
LocalVariables      GROUP,PRE(LOC)
ApplicationName         STRING('ServiceBase 2000')
ProgramName             STRING('SID Usage Report')
UserName                STRING(100)
UserCode                STRING(3)
                    END

IniFilepath          STRING(255)
RecordCount          LONG
StartDate            DATE
EndDate              DATE


ExportQueue         Queue, PRE(EXQ)
SubAccount              STRING(15)
BookingType             STRING(5)
MonthArray              GROUP, DIM(6)
DayArray                    GROUP, DIM(7)
Period_1                         LONG
Period_2                         LONG
Period_3                         LONG
                            END
                        END
                    END

EXFILE              FILE,DRIVER('BASIC'),PRE(expfile),CREATE,BINDABLE,THREAD
EXRECORD                 RECORD,PRE(exprec)
Account                     STRING(30)
SubAccount                  STRING(15)
BookingType                 STRING(5)
MonthArray                  GROUP, DIM(6)
DayArray                       GROUP, DIM(7)
Period_1                           STRING(9)
Period_2                           STRING(9)
Period_3                           STRING(9)
                                END
                             END
                         END
                    END

    MAP
RequestedAccount    PROCEDURE(STRING arg:AccountNumber), LONG
GetBookingType      PROCEDURE(LONG arg:JobNumber), STRING
GetAccountNumber    PROCEDURE(STRING arg:SubAccount), STRING
GetMonth            PROCEDURE(LONG arg:MonthNumber), STRING
    END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?Pathname{prop:ReadOnly} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 15066597
    Elsif ?Pathname{prop:Req} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 8454143
    Else ! If ?Pathname{prop:Req} = True
        ?Pathname{prop:FontColor} = 65793
        ?Pathname{prop:Color} = 16777215
    End ! If ?Pathname{prop:Req} = True
    ?Pathname{prop:Trn} = 0
    ?Pathname{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::2:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW1.UpdateBuffer
   glo:Queue10.Pointer10 = tra:Account_Number
   GET(glo:Queue10,glo:Queue10.Pointer10)
  IF ERRORCODE()
     glo:Queue10.Pointer10 = tra:Account_Number
     ADD(glo:Queue10,glo:Queue10.Pointer10)
    AccountTag = '*'
  ELSE
    DELETE(glo:Queue10)
    AccountTag = ''
  END
    Queue:Browse.AccountTag = AccountTag
  IF (AccountTag='*')
    Queue:Browse.AccountTag_Icon = 2
  ELSE
    Queue:Browse.AccountTag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:Queue10)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue10.Pointer10 = tra:Account_Number
     ADD(glo:Queue10,glo:Queue10.Pointer10)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue10)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::2:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue10)
    GET(glo:Queue10,QR#)
    DASBRW::2:QUEUE = glo:Queue10
    ADD(DASBRW::2:QUEUE)
  END
  FREE(glo:Queue10)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::2:QUEUE.Pointer10 = tra:Account_Number
     GET(DASBRW::2:QUEUE,DASBRW::2:QUEUE.Pointer10)
    IF ERRORCODE()
       glo:Queue10.Pointer10 = tra:Account_Number
       ADD(glo:Queue10,glo:Queue10.Pointer10)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::2:DASSHOWTAG Routine
   CASE DASBRW::2:TAGDISPSTATUS
   OF 0
      DASBRW::2:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::2:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::2:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
GetUserName         ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        CommandLine = CLIP(COMMAND(''))
        tmpPos = INSTRING('%', CommandLine)
        IF (NOT tmpPos) THEN
            MessageEx('Attempting to use ' & LOC:ProgramName & '<10,13>'          & |
                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
                LOC:ApplicationName,                                                           |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                beep:systemhand,msgex:samewidths,84,26,0)
           HALT
        END

        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:Benign) THEN
            MessageEx('Unable to find your logged in user details.', |
                    LOC:ApplicationName,                                  |
                    'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
            HALT
        END

        LOC:UserCode = use:User_Code

        LOC:UserName = use:Forename
        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END

    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:TRADEACC,SELF)
  DO GetUserName
  
  IniFilepath =  CLIP(PATH()) & '\SIDUSAG2.ini'
  Pathname = CLIP(GETINI('DEFAULTS','Pathname','',CLIP(IniFilepath)))
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse
  BRW1.AddSortOrder(,tra:Account_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,tra:Account_Number,1,BRW1)
  BIND('AccountTag',AccountTag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(AccountTag,BRW1.Q.AccountTag)
  BRW1.AddField(tra:Account_Number,BRW1.Q.tra:Account_Number)
  BRW1.AddField(tra:RecordNumber,BRW1.Q.tra:RecordNumber)
  BRW1.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue10)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?FileButton
      ThisWindow.Update
      FILEDIALOG('Choose Export File',pathname,'CSV Files|*.csv', |
                        file:save + file:keepdir + file:noerror + file:longname)
      strlen# = LEN(CLIP(Pathname))
      IF ((strlen# < 4) OR (UPPER(Pathname[strlen#-3 : strlen#]) <> '.CSV')) THEN
          Pathname = CLIP(Pathname) & '.csv'
      END
      DISPLAY(?pathname)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::2:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ExportButton
      ThisWindow.Update
      IF (Pathname = '') THEN
          MESSAGE('Please Select a Filename')
          SELECT(?Pathname)
          CYCLE
      END
      
      EXFILE{Prop:NAME} = CLIP(Pathname)
      
      IF (EXISTS(CLIP(Pathname))) THEN
          REMOVE(EXFILE)
      END
      CREATE(EXFILE)
      
      OPEN(EXFILE, 42h)
      IF (ERRORCODE()) THEN
          MESSAGE('Error Opening ' & CLIP(Pathname))
          SELECT(?Pathname)
          CYCLE
      END
      SET(EXFILE)
      
      RecordCount = 0
      Cancelled# = 0
      
      StartMonth# = MONTH(TODAY()) - 6
      StartYear#  = YEAR(TODAY())
      
      IF (StartMonth# < 0) THEN
          StartMonth# = 12 + StartMonth#
          StartYear# -= 1
      END
      
      StartDate = DATE(StartMonth#, 1, StartYear#)
      EndDate = DATE(MONTH(TODAY()), 1, YEAR(TODAY())) - 1
      !MESSAGE('Start Date ' & FORMAT(StartDate, @d06))    ! DEBUG
      !MESSAGE('End Date ' & FORMAT(EndDate, @d06))    ! DEBUG
      
      P1_Start# = DEFORMAT('08:00', @t1)
      P2_Start# = DEFORMAT('17:00', @t1)
      P3_Start# = DEFORMAT('20:00', @t1)
      
      CurrentMonth# = MONTH(TODAY())
      IF (CurrentMonth# > 1) THEN
          CurrentMonth# -= 1
      ELSE
          CurrentMonth# = 12
      END
      
      FREE(ExportQueue)
      
      access:JOBS.clearkey(job:Date_Booked_Key)
      job:Date_Booked = StartDate
      SET(job:Date_Booked_Key, job:Date_Booked_Key)
      access:jobs.next()
      job1# = job:Ref_Number
      
      SET(JOBS)
      access:jobs.previous()
      job2# = job:Ref_Number
      count# = job2# - job1#
      IF (count# < 0) THEN
          count# = RECORDS(JOBS)
      END
      
      ProgressBarShow(LOC:ApplicationName, count#)
      
      access:JOBS.clearkey(job:Date_Booked_Key)
      job:Date_Booked = StartDate
      SET(job:Date_Booked_Key, job:Date_Booked_Key)
      LOOP
          IF ((access:JOBS.next() <> Level:Benign)  OR (job:Date_Booked > EndDate)) THEN
              BREAK
          END
      
          IF (NOT ProgressBarUpdate()) THEN
              Cancelled# = 1
              BREAK
          END
      
          IF (NOT RequestedAccount(job:Account_Number)) THEN
              CYCLE
          END
      
          BookingType" = GetBookingType(job:Ref_Number)
      
          CLEAR(ExportQueue)
          EXQ:SubAccount = job:Account_Number
          EXQ:BookingType = CLIP(BookingType")
      
          GET(ExportQueue, EXQ:SubAccount, EXQ:BookingType)
          IF (ERRORCODE()) THEN
              CLEAR(ExportQueue)
              EXQ:SubAccount = job:Account_Number
              EXQ:BookingType = 'WEB'
              LOOP ix# = 1 TO 6
                  LOOP iy# = 1 TO 7
                      EXQ:MonthArray[ix#].DayArray[iy#].Period_1 = 0
                      EXQ:MonthArray[ix#].DayArray[iy#].Period_2 = 0
                      EXQ:MonthArray[ix#].DayArray[iy#].Period_3 = 0
                  END
              END
              ADD(ExportQueue, EXQ:SubAccount, EXQ:BookingType)
              CLEAR(ExportQueue)
              EXQ:SubAccount = job:Account_Number
              EXQ:BookingType = 'OTHER'
              LOOP ix# = 1 TO 6
                  LOOP iy# = 1 TO 7
                      EXQ:MonthArray[ix#].DayArray[iy#].Period_1 = 0
                      EXQ:MonthArray[ix#].DayArray[iy#].Period_2 = 0
                      EXQ:MonthArray[ix#].DayArray[iy#].Period_3 = 0
                  END
              END
              ADD(ExportQueue, EXQ:SubAccount, EXQ:BookingType)
              CLEAR(ExportQueue)
              EXQ:SubAccount = job:Account_Number
              EXQ:BookingType = CLIP(BookingType")
              GET(ExportQueue, EXQ:SubAccount, EXQ:BookingType)
              IF (ERRORCODE()) THEN              ! DEBUG
                  MESSAGE('UNEXPECTED ERROR')
              END
          END
      
          TheMonth# = MONTH(job:date_Booked)
          IF (TheMonth# > CurrentMonth#) THEN
              TheMonth# = TheMonth# - 12
          END
          MonthIndex# = CurrentMonth# - TheMonth# + 1
      
          !MESSAGE('Date Booked ' & FORMAT(job:Date_Booked, @d06) & ' Index: ' & MonthIndex#)   ! DEBUG
      
          IF (MonthIndex# > 6) OR (MonthIndex# < 1) THEN      ! DEBUG
              MESSAGE('Invalid Month Index ' & MonthIndex#)
              BREAK
          END
      
          DayIndex# = (job:date_Booked % 7) + 1
      
          IF (job:Time_Booked < P1_Start#) THEN
              EXQ:MonthArray[MonthIndex#].DayArray[DayIndex#].Period_3 += 1
          ELSIF (job:Time_Booked < P2_Start#) THEN
              EXQ:MonthArray[MonthIndex#].DayArray[DayIndex#].Period_1 += 1
          ELSIF (job:Time_Booked < P3_Start#) THEN
              EXQ:MonthArray[MonthIndex#].DayArray[DayIndex#].Period_2 += 1
          ELSE
              EXQ:MonthArray[MonthIndex#].DayArray[DayIndex#].Period_3 += 1
          END
          PUT(ExportQueue, EXQ:SubAccount, EXQ:BookingType)
      
          RecordCount += 1
      END
      ProgressBarClose()
      
      Month_1" = GetMonth(CurrentMonth# - 5)
      Month_2" = GetMonth(CurrentMonth# - 4)
      Month_3" = GetMonth(CurrentMonth# - 3)
      Month_4" = GetMonth(CurrentMonth# - 2)
      Month_5" = GetMonth(CurrentMonth# - 1)
      Month_6" = GetMonth(CurrentMonth#)
      
      CLEAR(EXFILE)
      exprec:MonthArray[1].DayArray[1].Period_1 = CLIP(Month_1")
      exprec:MonthArray[2].DayArray[1].Period_1 = CLIP(Month_2")
      exprec:MonthArray[3].DayArray[1].Period_1 = CLIP(Month_3")
      exprec:MonthArray[4].DayArray[1].Period_1 = CLIP(Month_4")
      exprec:MonthArray[5].DayArray[1].Period_1 = CLIP(Month_5")
      exprec:MonthArray[6].DayArray[1].Period_1 = CLIP(Month_6")
      
      ADD(EXFILE)
      
      CLEAR(EXFILE)
      LOOP ix# = 1 TO 6
          exprec:MonthArray[ix#].DayArray[1].Period_1 = 'Sunday'
          exprec:MonthArray[ix#].DayArray[2].Period_1 = 'Monday'
          exprec:MonthArray[ix#].DayArray[3].Period_1 = 'Tuesday'
          exprec:MonthArray[ix#].DayArray[4].Period_1 = 'Wednesday'
          exprec:MonthArray[ix#].DayArray[5].Period_1 = 'Thursday'
          exprec:MonthArray[ix#].DayArray[6].Period_1 = 'Friday'
          exprec:MonthArray[ix#].DayArray[7].Period_1 = 'Saturday'
      END
      
      ADD(EXFILE)
      
      CLEAR(EXFILE)
      LOOP ix# = 1 TO 6
          LOOP iy# = 1 TO 7
              exprec:MonthArray[ix#].DayArray[iy#].Period_1 = '8am-5pm'
              exprec:MonthArray[ix#].DayArray[iy#].Period_2 = '5pm-8pm'
              exprec:MonthArray[ix#].DayArray[iy#].Period_3 = '8pm-8am'
          END
      END
      
      ADD(EXFILE)
      
      CLEAR(EXFILE)
      ADD(EXFILE)
      
      QCount# = RECORDS(ExportQueue)
      LOOP rec# = 1 TO QCount#
          GET(ExportQueue, rec#)
      
          CLEAR(EXFILE)
          exprec:Account = GetAccountNumber(EXQ:SubAccount)
          exprec:SubAccount = EXQ:SubAccount
          exprec:BookingType = EXQ:BookingType
      
          LOOP ix# = 1 TO 6
              LOOP iy# = 1 TO 7
                  exprec:MonthArray[ix#].DayArray[iy#].Period_1 = FORMAT(EXQ:MonthArray[ix#].DayArray[iy#].Period_1, @n6)
                  exprec:MonthArray[ix#].DayArray[iy#].Period_2 = FORMAT(EXQ:MonthArray[ix#].DayArray[iy#].Period_2, @n6)
                  exprec:MonthArray[ix#].DayArray[iy#].Period_3 = FORMAT(EXQ:MonthArray[ix#].DayArray[iy#].Period_3, @n6)
              END
          END
      
          ADD(EXFILE)
      END
      
      CLOSE(EXFILE)
      
      PUTINI('DEFAULTS','Pathname',CLIP(Pathname),CLIP(IniFilepath))
      
      IF (cancelled#) THEN
          REMOVE(EXFILE)
      ELSIF (RecordCount = 0) THEN
          REMOVE(EXFILE)
          MessageEx('No Records Found',  |
                    LOC:ApplicationName,'Styles\warn.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
      ELSE
          MessageEx('Export Completed OK',  |
                    LOC:ApplicationName,,'&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                    beep:systemhand,msgex:samewidths,84,26,0)
          POST(Event:CloseWindow)
      END
      
      
    OF ?CancelButton
      ThisWindow.Update
      POST(EVENT:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::2:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Progress Bar Window
ProgressBarShow    PROCEDURE(arg:AppNameString, arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    AppName              = arg:AppNameString
    RecordsPerCycle      = arg:CycleCount
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    !thiswindow.reset(1) !This may error, if so, remove this line.
    open(ProgressWindow)
    !ProgressWindow{PROP:Timer} = 1
         
    IF (OMITTED(4)) THEN
        ?ProgressUserString{PROP:Text} = 'Running...'
    ELSE
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarReset    PROCEDURE(arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    IF (NOT OMITTED(2)) THEN
        RecordsPerCycle      = arg:CycleCount
    END
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    IF (NOT OMITTED(3)) THEN
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarUpdate     PROCEDURE()
result  LONG
    CODE
    result = -1
    RecordsProcessed += 1
    RecordsThisCycle += 1
    IF (RecordsThisCycle > RecordsPerCycle) THEN
        RecordsThisCycle = 0
        IF (ProgressBarCancelled()) THEN
            !CLOSE(ProgressWindow)
            result = 0
        ELSE
            IF (PercentProgress < 100) THEN
                PercentProgress = ROUND((RecordsProcessed / RecordsToProcess) * 100.0, 1)
                IF (PercentProgress > 100) THEN
                    PercentProgress = 100
                END
                IF (PercentProgress <> ProgressThermometer) THEN
                    ProgressThermometer = PercentProgress
                    ?ProgressText{prop:text} = format(PercentProgress,@n3) & '% Completed'
                    DISPLAY()
                END
            END
        END
    END
    RETURN result

ProgressBarCancelled       PROCEDURE()
result  LONG
    CODE
    result = 0
    cancel# = 0

    ACCEPT
        CASE EVENT()
        OF Event:Timer
            BREAK
        OF Event:CloseWindow
            cancel# = 1
            BREAK
        OF Event:accepted
            IF (FIELD() = ?ProgressCancel) THEN
                cancel# = 1
                BREAK
            END
        END
    END

    IF (cancel# = 1) THEN
        BEEP(BEEP:SystemAsterisk)
        CASE MessageEx('Are you sure you want to cancel?', CLIP(AppName),|
                       'Styles\question.ico','&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                       beep:systemquestion,msgex:samewidths,84,26,0)
            OF 1 ! &Yes Button
                result = -1
            OF 2 ! &No Button
        END
    END
    RETURN result

ProgressBarClose       PROCEDURE()
    CODE
    CLOSE(ProgressWindow)
    RETURN
RequestedAccount    PROCEDURE(arg:AccountNumber)
result  LONG
    CODE
    result = 0
    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = arg:AccountNumber
    IF (access:SUBTRACC.fetch(sub:account_number_key) = Level:Benign)
        glo:Pointer10 = sub:Main_Account_Number
        GET(glo:Queue10, glo:Pointer10)
        IF (NOT ERRORCODE()) THEN
            result = 1
        END
    END
    RETURN result
GetBookingType    PROCEDURE(arg:JobNumber)
result      STRING(5)
        CODE
        result = 'OTHER'
        access:AUDIT.clearkey(aud:ref_number_key)
        aud:Ref_Number = arg:JobNumber
        SET(aud:ref_number_key, aud:ref_number_key)
        LOOP WHILE(access:AUDIT.next() = Level:Benign)
            IF (aud:Ref_Number <> arg:JobNumber) THEN
                BREAK
            END
            IF (aud:Notes[1 : 24] = 'JOB CREATED BY WEBMASTER') THEN
                result = 'WEB'
                BREAK
            END
        END
        RETURN result
GetAccountNumber    PROCEDURE(arg:SubAccount)
result  STRING(30)
    CODE
    result = ''
    access:subtracc.clearkey(sub:account_number_key)
    sub:account_number = arg:SubAccount
    IF (access:SUBTRACC.fetch(sub:account_number_key) = Level:Benign)
        result = sub:Main_Account_Number
    END
    RETURN result
GetMonth    PROCEDURE(arg:MonthNumber)
result  STRING(9)
    CODE
    IF (arg:MonthNumber < 1) THEN
        arg:MonthNumber += 12
    END
    IF (arg:MonthNumber > 12) THEN
        arg:MonthNumber -= 12
    END
    CASE arg:MonthNumber
        OF 1
            result = 'January'
        OF 2
            result = 'February'
        OF 3
            result = 'March'
        OF 4
            result = 'April'
        OF 5
            result = 'May'
        OF 6
            result = 'June'
        OF 7
            result = 'July'
        OF 8
            result = 'August'
        OF 9
            result = 'September'
        OF 10
            result = 'October'
        OF 11
            result = 'November'
        OF 12
            result = 'December'
    END
    return result

BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = tra:Account_Number
     GET(glo:Queue10,glo:Queue10.Pointer10)
    IF ERRORCODE()
      AccountTag = ''
    ELSE
      AccountTag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (AccountTag='*')
    SELF.Q.AccountTag_Icon = 2
  ELSE
    SELF.Q.AccountTag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue10.Pointer10 = tra:Account_Number
     GET(glo:Queue10,glo:Queue10.Pointer10)
    EXECUTE DASBRW::2:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

