

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01029.INC'),ONCE        !Local module procedure declarations
                     END


View_Proforma PROCEDURE                               !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::8:TAGFLAG          BYTE(0)
DASBRW::8:TAGMOUSE         BYTE(0)
DASBRW::8:TAGDISPSTATUS    BYTE(0)
DASBRW::8:QUEUE           QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ThisThreadActive BYTE
CurrentTab           STRING(80)
save_prv_id          USHORT,AUTO
save_job_id          USHORT,AUTO
pos                  STRING(255)
tag_temp             STRING(1)
tmp:unr              STRING('UNR')
tmp:rea              STRING('REA')
tmp:una              STRING('UNA')
tmp:aut              STRING('AUT')
tmp:AllAccounts      STRING('YES')
tmp:accountnumber    STRING(15)
tmp:no               STRING('NO {1}')
tmp:que              STRING('QUE')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:accountnumber
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(PROINV)
                       PROJECT(prv:AccountNumber)
                       PROJECT(prv:AccountName)
                       PROJECT(prv:BatchNumber)
                       PROJECT(prv:User)
                       PROJECT(prv:BatchValue)
                       PROJECT(prv:NoOfJobs)
                       PROJECT(prv:DateCreated)
                       PROJECT(prv:RefNumber)
                       PROJECT(prv:AccountType)
                       PROJECT(prv:Invoiced)
                       PROJECT(prv:Status)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
prv:AccountNumber      LIKE(prv:AccountNumber)        !List box control field - type derived from field
prv:AccountNumber_NormalFG LONG                       !Normal forground color
prv:AccountNumber_NormalBG LONG                       !Normal background color
prv:AccountNumber_SelectedFG LONG                     !Selected forground color
prv:AccountNumber_SelectedBG LONG                     !Selected background color
prv:AccountName        LIKE(prv:AccountName)          !List box control field - type derived from field
prv:AccountName_NormalFG LONG                         !Normal forground color
prv:AccountName_NormalBG LONG                         !Normal background color
prv:AccountName_SelectedFG LONG                       !Selected forground color
prv:AccountName_SelectedBG LONG                       !Selected background color
prv:BatchNumber        LIKE(prv:BatchNumber)          !List box control field - type derived from field
prv:BatchNumber_NormalFG LONG                         !Normal forground color
prv:BatchNumber_NormalBG LONG                         !Normal background color
prv:BatchNumber_SelectedFG LONG                       !Selected forground color
prv:BatchNumber_SelectedBG LONG                       !Selected background color
prv:User               LIKE(prv:User)                 !List box control field - type derived from field
prv:User_NormalFG      LONG                           !Normal forground color
prv:User_NormalBG      LONG                           !Normal background color
prv:User_SelectedFG    LONG                           !Selected forground color
prv:User_SelectedBG    LONG                           !Selected background color
prv:BatchValue         LIKE(prv:BatchValue)           !List box control field - type derived from field
prv:BatchValue_NormalFG LONG                          !Normal forground color
prv:BatchValue_NormalBG LONG                          !Normal background color
prv:BatchValue_SelectedFG LONG                        !Selected forground color
prv:BatchValue_SelectedBG LONG                        !Selected background color
prv:NoOfJobs           LIKE(prv:NoOfJobs)             !List box control field - type derived from field
prv:NoOfJobs_NormalFG  LONG                           !Normal forground color
prv:NoOfJobs_NormalBG  LONG                           !Normal background color
prv:NoOfJobs_SelectedFG LONG                          !Selected forground color
prv:NoOfJobs_SelectedBG LONG                          !Selected background color
prv:DateCreated        LIKE(prv:DateCreated)          !List box control field - type derived from field
prv:DateCreated_NormalFG LONG                         !Normal forground color
prv:DateCreated_NormalBG LONG                         !Normal background color
prv:DateCreated_SelectedFG LONG                       !Selected forground color
prv:DateCreated_SelectedBG LONG                       !Selected background color
prv:RefNumber          LIKE(prv:RefNumber)            !List box control field - type derived from field
prv:RefNumber_NormalFG LONG                           !Normal forground color
prv:RefNumber_NormalBG LONG                           !Normal background color
prv:RefNumber_SelectedFG LONG                         !Selected forground color
prv:RefNumber_SelectedBG LONG                         !Selected background color
prv:AccountType        LIKE(prv:AccountType)          !List box control field - type derived from field
prv:AccountType_NormalFG LONG                         !Normal forground color
prv:AccountType_NormalBG LONG                         !Normal background color
prv:AccountType_SelectedFG LONG                       !Selected forground color
prv:AccountType_SelectedBG LONG                       !Selected background color
prv:Invoiced           LIKE(prv:Invoiced)             !Browse key field - type derived from field
prv:Status             LIKE(prv:Status)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Repair_Type)
                       PROJECT(job:Courier_Cost)
                       PROJECT(job:Labour_Cost)
                       PROJECT(job:Parts_Cost)
                       PROJECT(job:InvoiceAccount)
                       PROJECT(job:InvoiceBatch)
                       PROJECT(job:InvoiceStatus)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                         PROJECT(jbn:Engineers_Notes)
                         PROJECT(jbn:Invoice_Text)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Ref_Number_NormalFG LONG                          !Normal forground color
job:Ref_Number_NormalBG LONG                          !Normal background color
job:Ref_Number_SelectedFG LONG                        !Selected forground color
job:Ref_Number_SelectedBG LONG                        !Selected background color
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
job:Order_Number_NormalFG LONG                        !Normal forground color
job:Order_Number_NormalBG LONG                        !Normal background color
job:Order_Number_SelectedFG LONG                      !Selected forground color
job:Order_Number_SelectedBG LONG                      !Selected background color
job:Charge_Type        LIKE(job:Charge_Type)          !List box control field - type derived from field
job:Charge_Type_NormalFG LONG                         !Normal forground color
job:Charge_Type_NormalBG LONG                         !Normal background color
job:Charge_Type_SelectedFG LONG                       !Selected forground color
job:Charge_Type_SelectedBG LONG                       !Selected background color
job:Repair_Type        LIKE(job:Repair_Type)          !List box control field - type derived from field
job:Repair_Type_NormalFG LONG                         !Normal forground color
job:Repair_Type_NormalBG LONG                         !Normal background color
job:Repair_Type_SelectedFG LONG                       !Selected forground color
job:Repair_Type_SelectedBG LONG                       !Selected background color
job:Courier_Cost       LIKE(job:Courier_Cost)         !List box control field - type derived from field
job:Courier_Cost_NormalFG LONG                        !Normal forground color
job:Courier_Cost_NormalBG LONG                        !Normal background color
job:Courier_Cost_SelectedFG LONG                      !Selected forground color
job:Courier_Cost_SelectedBG LONG                      !Selected background color
job:Labour_Cost        LIKE(job:Labour_Cost)          !List box control field - type derived from field
job:Labour_Cost_NormalFG LONG                         !Normal forground color
job:Labour_Cost_NormalBG LONG                         !Normal background color
job:Labour_Cost_SelectedFG LONG                       !Selected forground color
job:Labour_Cost_SelectedBG LONG                       !Selected background color
job:Parts_Cost         LIKE(job:Parts_Cost)           !List box control field - type derived from field
job:Parts_Cost_NormalFG LONG                          !Normal forground color
job:Parts_Cost_NormalBG LONG                          !Normal background color
job:Parts_Cost_SelectedFG LONG                        !Selected forground color
job:Parts_Cost_SelectedBG LONG                        !Selected background color
jbn:Engineers_Notes    LIKE(jbn:Engineers_Notes)      !List box control field - type derived from field
jbn:Engineers_Notes_NormalFG LONG                     !Normal forground color
jbn:Engineers_Notes_NormalBG LONG                     !Normal background color
jbn:Engineers_Notes_SelectedFG LONG                   !Selected forground color
jbn:Engineers_Notes_SelectedBG LONG                   !Selected background color
jbn:Invoice_Text       LIKE(jbn:Invoice_Text)         !List box control field - type derived from field
jbn:Invoice_Text_NormalFG LONG                        !Normal forground color
jbn:Invoice_Text_NormalBG LONG                        !Normal background color
jbn:Invoice_Text_SelectedFG LONG                      !Selected forground color
jbn:Invoice_Text_SelectedBG LONG                      !Selected background color
job:InvoiceAccount     LIKE(job:InvoiceAccount)       !Browse key field - type derived from field
job:InvoiceBatch       LIKE(job:InvoiceBatch)         !Browse key field - type derived from field
job:InvoiceStatus      LIKE(job:InvoiceStatus)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(PARTS)
                       PROJECT(par:Quantity)
                       PROJECT(par:Description)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                       PROJECT(par:Part_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
par:Part_Number        LIKE(par:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK7::job:InvoiceAccount   LIKE(job:InvoiceAccount)
HK7::job:InvoiceBatch     LIKE(job:InvoiceBatch)
HK7::job:InvoiceStatus    LIKE(job:InvoiceStatus)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK20::prv:AccountNumber   LIKE(prv:AccountNumber)
HK20::prv:Invoiced        LIKE(prv:Invoiced)
HK20::prv:Status          LIKE(prv:Status)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB19::View:FileDropCombo VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:RefNumber)
                     END
QuickWindow          WINDOW('Chargeable Job Validation And Invoice Procedure'),AT(,,662,343),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('View_Proforma'),SYSTEM,GRAY,DOUBLE,IMM
                       PROMPT('- Un-Read Batch'),AT(524,108),USE(?Prompt4)
                       ENTRY(@s8),AT(8,140,64,10),USE(job:Ref_Number),FONT('Tahoma',8,,FONT:bold),UPR
                       CHECK('All Accounts'),AT(84,20),USE(tmp:AllAccounts),RIGHT,VALUE('YES','NO')
                       COMBO(@s15),AT(148,20,124,10),USE(tmp:accountnumber),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('60L(2)|M@s15@'),DROP(10,124),FROM(Queue:FileDropCombo)
                       ENTRY(@s15),AT(8,20,60,9),USE(prv:AccountNumber),FULL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),MSG('Trade Account Number'),TIP('Trade Account Number'),UPR
                       LIST,AT(8,36,568,68),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('64L(2)|M*~Account Number~@s15@125L(2)|M*~Account Name~@s30@52L(2)|M*~Batch Numbe' &|
   'r~@s8@25L(2)|M*~User~@s3@60R(2)|M*~Batch Value~@n14.2@44L(2)|M*~No Of Jobs~@s8@5' &|
   '8L(2)|M*~Date Created~@d6@32L(2)|M*~Ref Number~@s8@12L(2)|M*~Account Type~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('Print Routine'),AT(584,48,76,20),USE(?Print_Routine),LEFT,ICON(ICON:Print1)
                       SHEET,AT(4,124,576,124),USE(?Sheet2),SPREAD
                         TAB('Un-Authorised Jobs'),USE(?Tab3)
                         END
                         TAB('Authorised Jobs'),USE(?Tab4)
                         END
                         TAB('Query Jobs'),USE(?Tab8)
                         END
                         TAB('All Jobs'),USE(?Tab2)
                           BUTTON('&Rev tags'),AT(168,176,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(232,176,70,13),USE(?DASSHOWTAG),HIDE
                         END
                       END
                       LIST,AT(8,152,568,80),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@37L(2)|M*~Job No~@s8@93L(2)|M*~Order Number~@s30@120L(2)|M*~Charge Ty' &|
   'pe~@s30@120L(2)|M*~Repair Type~@s30@56L(2)|M*~Courier Cost~@n14.2@56L(2)|M*~Labo' &|
   'ur Cost~@n14.2@56L(2)|M*~Parts Cost~@n14.2@1020L(2)|M*~Engineers Notes~@s255@102' &|
   '0L(2)|M*~Invoice Text~@s255@'),FROM(Queue:Browse)
                       BUTTON('&View Job'),AT(584,140,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('Remove Job From Batch'),AT(584,192,76,20),USE(?Remove_Job),LEFT,ICON('delete.ico')
                       BUTTON('Remove Batch'),AT(584,72,76,20),USE(?Remove_Batch),LEFT,ICON('delete.ico')
                       BUTTON('Re-valuate Batch'),AT(584,96,76,20),USE(?Revaluate_Batch),LEFT,ICON('money.gif')
                       BUTTON('Mark As Read'),AT(508,20,68,12),USE(?Mark_As_Read),LEFT,ICON('bluetick.ico')
                       BUTTON('Reject Job From Batch'),AT(584,216,76,20),USE(?Reject_Job),LEFT,ICON('nogo.ico')
                       SHEET,AT(4,4,576,116),USE(?CurrentTab),SPREAD
                         TAB('By Account Number'),USE(?Tab:2)
                         END
                         TAB('Un-Read Batches'),USE(?Tab6)
                         END
                         TAB('Read Batches'),USE(?Tab7)
                         END
                       END
                       BUTTON('Close'),AT(584,320,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       BUTTON('Query Selected Job'),AT(504,320,72,16),USE(?Button12:2),LEFT,ICON(ICON:VCRlocate)
                       BUTTON('T&ag All'),AT(56,320,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                       SHEET,AT(4,252,576,60),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Tab 5'),USE(?Tab5)
                           PROMPT('Engineers Notes'),AT(8,256),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Invoice Text'),AT(188,256),USE(?Prompt1:2),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Parts Used'),AT(352,256),USE(?Prompt1:3),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           LIST,AT(396,256,180,52),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('32R(2)|M~Quantity~L@n8@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:2)
                           TEXT,AT(72,256,104,52),USE(jbn:Engineers_Notes),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           TEXT,AT(236,256,104,52),USE(jbn:Invoice_Text),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                         END
                       END
                       BOX,AT(440,236,9,8),USE(?Box1:3),COLOR(COLOR:Black),FILL(COLOR:Blue)
                       PROMPT('- Under Query'),AT(452,236),USE(?Prompt4:3)
                       BOX,AT(512,236,9,8),USE(?Box1:2),COLOR(COLOR:Black),FILL(COLOR:Red)
                       PROMPT('- Un-Authorised'),AT(524,236),USE(?Prompt4:2)
                       PROMPT('- Authorised'),AT(376,236),USE(?Prompt4:4)
                       BOX,AT(364,236,9,8),USE(?Box1:4),COLOR(COLOR:Black),FILL(COLOR:Green)
                       PANEL,AT(4,316,576,24),USE(?Panel1),FILL(COLOR:Silver)
                       BOX,AT(512,108,9,8),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Red)
                       BUTTON('&Tag'),AT(8,320,48,16),USE(?DASTAG),LEFT,ICON('TAG.GIF')
                       BUTTON('&Untag All'),AT(104,320,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                       BUTTON('Authorise Tagged Jobs'),AT(428,320,72,16),USE(?Button12),LEFT,ICON('CHECK2.ICO')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 and Upper(tmp:AllAccounts) = 'YES'
BRW1::Sort4:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 1 and Upper(tmp:AllAccounts) = 'NO'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 and Upper(tmp:AllAccounts) = 'YES'
BRW1::Sort5:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2 and Upper(tmp:AllAccounts) = 'NO'
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 and Upper(tmp:AllAccounts) = 'YES'
BRW1::Sort6:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 3 and Upper(tmp:AllAccounts) = 'NO'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW5::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 1
BRW5::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 2
BRW5::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 3
BRW5::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 4
BRW6                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB19               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold
    ?tmp:AllAccounts{prop:Font,3} = -1
    ?tmp:AllAccounts{prop:Color} = 15066597
    ?tmp:AllAccounts{prop:Trn} = 0
    If ?tmp:accountnumber{prop:ReadOnly} = True
        ?tmp:accountnumber{prop:FontColor} = 65793
        ?tmp:accountnumber{prop:Color} = 15066597
    Elsif ?tmp:accountnumber{prop:Req} = True
        ?tmp:accountnumber{prop:FontColor} = 65793
        ?tmp:accountnumber{prop:Color} = 8454143
    Else ! If ?tmp:accountnumber{prop:Req} = True
        ?tmp:accountnumber{prop:FontColor} = 65793
        ?tmp:accountnumber{prop:Color} = 16777215
    End ! If ?tmp:accountnumber{prop:Req} = True
    ?tmp:accountnumber{prop:Trn} = 0
    ?tmp:accountnumber{prop:FontStyle} = font:Bold
    If ?prv:AccountNumber{prop:ReadOnly} = True
        ?prv:AccountNumber{prop:FontColor} = 65793
        ?prv:AccountNumber{prop:Color} = 15066597
    Elsif ?prv:AccountNumber{prop:Req} = True
        ?prv:AccountNumber{prop:FontColor} = 65793
        ?prv:AccountNumber{prop:Color} = 8454143
    Else ! If ?prv:AccountNumber{prop:Req} = True
        ?prv:AccountNumber{prop:FontColor} = 65793
        ?prv:AccountNumber{prop:Color} = 16777215
    End ! If ?prv:AccountNumber{prop:Req} = True
    ?prv:AccountNumber{prop:Trn} = 0
    ?prv:AccountNumber{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?Tab8{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Tab6{prop:Color} = 15066597
    ?Tab7{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?Prompt1:3{prop:FontColor} = -1
    ?Prompt1:3{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    If ?jbn:Engineers_Notes{prop:ReadOnly} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 15066597
    Elsif ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 8454143
    Else ! If ?jbn:Engineers_Notes{prop:Req} = True
        ?jbn:Engineers_Notes{prop:FontColor} = 65793
        ?jbn:Engineers_Notes{prop:Color} = 16777215
    End ! If ?jbn:Engineers_Notes{prop:Req} = True
    ?jbn:Engineers_Notes{prop:Trn} = 0
    ?jbn:Engineers_Notes{prop:FontStyle} = font:Bold
    If ?jbn:Invoice_Text{prop:ReadOnly} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 15066597
    Elsif ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 8454143
    Else ! If ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 16777215
    End ! If ?jbn:Invoice_Text{prop:Req} = True
    ?jbn:Invoice_Text{prop:Trn} = 0
    ?jbn:Invoice_Text{prop:FontStyle} = font:Bold
    ?Prompt4:3{prop:FontColor} = -1
    ?Prompt4:3{prop:Color} = 15066597
    ?Prompt4:2{prop:FontColor} = -1
    ?Prompt4:2{prop:Color} = 15066597
    ?Prompt4:4{prop:FontColor} = -1
    ?Prompt4:4{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::8:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   glo:Queue2.Pointer2 = job:Ref_Number
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = job:Ref_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tag_temp = '*'
  ELSE
    DELETE(glo:Queue2)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = job:Ref_Number
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::8:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::8:QUEUE = glo:Queue2
    ADD(DASBRW::8:QUEUE)
  END
  FREE(glo:Queue2)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::8:QUEUE.Pointer2 = job:Ref_Number
     GET(DASBRW::8:QUEUE,DASBRW::8:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = job:Ref_Number
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::8:DASSHOWTAG Routine
   CASE DASBRW::8:TAGDISPSTATUS
   OF 0
      DASBRW::8:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::8:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::8:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'View_Proforma',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'View_Proforma',1)
    SolaceViewVars('save_prv_id',save_prv_id,'View_Proforma',1)
    SolaceViewVars('save_job_id',save_job_id,'View_Proforma',1)
    SolaceViewVars('pos',pos,'View_Proforma',1)
    SolaceViewVars('tag_temp',tag_temp,'View_Proforma',1)
    SolaceViewVars('tmp:unr',tmp:unr,'View_Proforma',1)
    SolaceViewVars('tmp:rea',tmp:rea,'View_Proforma',1)
    SolaceViewVars('tmp:una',tmp:una,'View_Proforma',1)
    SolaceViewVars('tmp:aut',tmp:aut,'View_Proforma',1)
    SolaceViewVars('tmp:AllAccounts',tmp:AllAccounts,'View_Proforma',1)
    SolaceViewVars('tmp:accountnumber',tmp:accountnumber,'View_Proforma',1)
    SolaceViewVars('tmp:no',tmp:no,'View_Proforma',1)
    SolaceViewVars('tmp:que',tmp:que,'View_Proforma',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AllAccounts;  SolaceCtrlName = '?tmp:AllAccounts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:accountnumber;  SolaceCtrlName = '?tmp:accountnumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?prv:AccountNumber;  SolaceCtrlName = '?prv:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Routine;  SolaceCtrlName = '?Print_Routine';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab8;  SolaceCtrlName = '?Tab8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Remove_Job;  SolaceCtrlName = '?Remove_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Remove_Batch;  SolaceCtrlName = '?Remove_Batch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Revaluate_Batch;  SolaceCtrlName = '?Revaluate_Batch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Mark_As_Read;  SolaceCtrlName = '?Mark_As_Read';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reject_Job;  SolaceCtrlName = '?Reject_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button12:2;  SolaceCtrlName = '?Button12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:3;  SolaceCtrlName = '?Prompt1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Engineers_Notes;  SolaceCtrlName = '?jbn:Engineers_Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Invoice_Text;  SolaceCtrlName = '?jbn:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:3;  SolaceCtrlName = '?Box1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:3;  SolaceCtrlName = '?Prompt4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:2;  SolaceCtrlName = '?Box1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:2;  SolaceCtrlName = '?Prompt4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:4;  SolaceCtrlName = '?Prompt4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1:4;  SolaceCtrlName = '?Box1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button12;  SolaceCtrlName = '?Button12';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('View_Proforma')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'View_Proforma')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt4
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='View_Proforma'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBNOTES.Open
  Relate:PROINV.Open
  Relate:TRADETMP.Open
  SELF.FilesOpened = True
  access:tradetmp.clearkey(tratmp:refnumberkey)
  set(tratmp:refnumberkey)
  loop
      if access:tradetmp.next()
         break
      end !if
      Delete(tradetmp)
  end !loop
  
  save_prv_id = access:proinv.savefile()
  access:proinv.clearkey(prv:accountkey)
  prv:invoiced      = 'NO'
  set(prv:accountkey,prv:accountkey)
  loop
      if access:proinv.next()
         break
      end !if
      if prv:invoiced      <> 'NO'      |
          then break.  ! end if
      access:tradetmp.clearkey(tratmp:accountnokey)
      tratmp:account_number = prv:accountNumber
      if access:tradetmp.tryfetch(tratmp:accountnokey)
          get(tradetmp,0)
          if access:tradetmp.primerecord() = Level:Benign
              tratmp:account_number = prv:accountNumber
              tratmp:company_name   = prv:accountname
              tratmp:type           = ''
              if access:tradetmp.insert()
                  access:tradetmp.cancelautoinc()
              end
          end!if access:tradetmp.primerecord() = Level:Benign
      end!if access:tradetmp.tryfetch(tratmp:accountnokey)
  end !loop
  access:proinv.restorefile(save_prv_id)
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PROINV,SELF)
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:JOBS,SELF)
  BRW6.Init(?List:2,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:PARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,prv:AccountKey)
  BRW1.AddRange(prv:Invoiced)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?PRV:AccountNumber,prv:AccountNumber,1,BRW1)
  BRW1.SetFilter('(Upper(prv:batchnumber) <<> '''')')
  BRW1.AddSortOrder(,prv:AccountKey)
  BRW1.AddRange(prv:AccountNumber)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,prv:BatchNumber,1,BRW1)
  BRW1.AddSortOrder(,prv:AccStatusKey)
  BRW1.AddRange(prv:Status)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?PRV:AccountNumber,prv:AccountNumber,1,BRW1)
  BRW1.SetFilter('(Upper(prv:batchnumber) <<> '''')')
  BRW1.AddSortOrder(,prv:AccStatusKey)
  BRW1.AddRange(prv:AccountNumber)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(,prv:BatchNumber,1,BRW1)
  BRW1.AddSortOrder(,prv:AccStatusKey)
  BRW1.AddRange(prv:Status)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?PRV:AccountNumber,prv:AccountNumber,1,BRW1)
  BRW1.SetFilter('(Upper(prv:batchnumber) <<> '''')')
  BRW1.AddSortOrder(,prv:AccStatusKey)
  BRW1.AddRange(prv:AccountNumber)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(,prv:BatchNumber,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,prv:AccountKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,prv:Invoiced,1,BRW1)
  BRW1.AddResetField(tmp:AllAccounts)
  BRW1.AddResetField(tmp:accountnumber)
  BRW1.AddField(prv:AccountNumber,BRW1.Q.prv:AccountNumber)
  BRW1.AddField(prv:AccountName,BRW1.Q.prv:AccountName)
  BRW1.AddField(prv:BatchNumber,BRW1.Q.prv:BatchNumber)
  BRW1.AddField(prv:User,BRW1.Q.prv:User)
  BRW1.AddField(prv:BatchValue,BRW1.Q.prv:BatchValue)
  BRW1.AddField(prv:NoOfJobs,BRW1.Q.prv:NoOfJobs)
  BRW1.AddField(prv:DateCreated,BRW1.Q.prv:DateCreated)
  BRW1.AddField(prv:RefNumber,BRW1.Q.prv:RefNumber)
  BRW1.AddField(prv:AccountType,BRW1.Q.prv:AccountType)
  BRW1.AddField(prv:Invoiced,BRW1.Q.prv:Invoiced)
  BRW1.AddField(prv:Status,BRW1.Q.prv:Status)
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,job:BatchStatusKey)
  BRW5.AddRange(job:InvoiceStatus,tmp:una)
  BRW5.AddLocator(BRW5::Sort2:Locator)
  BRW5::Sort2:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW5)
  BRW5.AddSortOrder(,job:BatchStatusKey)
  BRW5.AddRange(job:InvoiceStatus,tmp:aut)
  BRW5.AddLocator(BRW5::Sort3:Locator)
  BRW5::Sort3:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW5)
  BRW5.AddSortOrder(,job:BatchStatusKey)
  BRW5.AddRange(job:InvoiceStatus,tmp:que)
  BRW5.AddLocator(BRW5::Sort4:Locator)
  BRW5::Sort4:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW5)
  BRW5.AddSortOrder(,job:BatchJobKey)
  BRW5.AddRange(job:InvoiceBatch,prv:BatchNumber)
  BRW5.AddLocator(BRW5::Sort1:Locator)
  BRW5::Sort1:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW5)
  BRW5.AddSortOrder(,job:BatchJobKey)
  BRW5.AddRange(job:InvoiceAccount,tmp:accountnumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,job:InvoiceBatch,1,BRW5)
  BRW5.SetFilter('(Upper(prv:batchnumber) <<> 0)')
  BRW5.AddResetField(prv:AccountName)
  BRW5.AddResetField(prv:BatchNumber)
  BIND('tag_temp',tag_temp)
  BIND('tmp:no',tmp:no)
  BIND('tmp:accountnumber',tmp:accountnumber)
  BIND('tmp:aut',tmp:aut)
  BIND('tmp:rea',tmp:rea)
  BIND('tmp:una',tmp:una)
  BIND('tmp:unr',tmp:unr)
  BIND('tmp:AllAccounts',tmp:AllAccounts)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(tag_temp,BRW5.Q.tag_temp)
  BRW5.AddField(job:Ref_Number,BRW5.Q.job:Ref_Number)
  BRW5.AddField(job:Order_Number,BRW5.Q.job:Order_Number)
  BRW5.AddField(job:Charge_Type,BRW5.Q.job:Charge_Type)
  BRW5.AddField(job:Repair_Type,BRW5.Q.job:Repair_Type)
  BRW5.AddField(job:Courier_Cost,BRW5.Q.job:Courier_Cost)
  BRW5.AddField(job:Labour_Cost,BRW5.Q.job:Labour_Cost)
  BRW5.AddField(job:Parts_Cost,BRW5.Q.job:Parts_Cost)
  BRW5.AddField(jbn:Engineers_Notes,BRW5.Q.jbn:Engineers_Notes)
  BRW5.AddField(jbn:Invoice_Text,BRW5.Q.jbn:Invoice_Text)
  BRW5.AddField(job:InvoiceAccount,BRW5.Q.job:InvoiceAccount)
  BRW5.AddField(job:InvoiceBatch,BRW5.Q.job:InvoiceBatch)
  BRW5.AddField(job:InvoiceStatus,BRW5.Q.job:InvoiceStatus)
  BRW6.Q &= Queue:Browse:2
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,par:Part_Number_Key)
  BRW6.AddRange(par:Ref_Number,job:Ref_Number)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,par:Part_Number,1,BRW6)
  BRW6.AddField(par:Quantity,BRW6.Q.par:Quantity)
  BRW6.AddField(par:Description,BRW6.Q.par:Description)
  BRW6.AddField(par:Record_Number,BRW6.Q.par:Record_Number)
  BRW6.AddField(par:Ref_Number,BRW6.Q.par:Ref_Number)
  BRW6.AddField(par:Part_Number,BRW6.Q.par:Part_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?tmp:AllAccounts{Prop:Checked} = True
    HIDE(?tmp:accountnumber)
  END
  IF ?tmp:AllAccounts{Prop:Checked} = False
    UNHIDE(?tmp:accountnumber)
  END
  BRW5.AskProcedure = 1
  FDCB19.Init(tmp:accountnumber,?tmp:accountnumber,Queue:FileDropCombo.ViewPosition,FDCB19::View:FileDropCombo,Queue:FileDropCombo,Relate:TRADETMP,ThisWindow,GlobalErrors,0,1,0)
  FDCB19.Q &= Queue:FileDropCombo
  FDCB19.AddSortOrder(tratmp:AccountNoKey)
  FDCB19.AddField(tratmp:Account_Number,FDCB19.Q.tratmp:Account_Number)
  FDCB19.AddField(tratmp:RefNumber,FDCB19.Q.tratmp:RefNumber)
  ThisWindow.AddItem(FDCB19.WindowComponent)
  FDCB19.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Account Number'
    ?Tab6{PROP:TEXT} = 'Un-Read Batches'
    ?Tab7{PROP:TEXT} = 'Read Batches'
    ?Browse:1{PROP:FORMAT} ='64L(2)|M*~Account Number~@s15@#1#125L(2)|M*~Account Name~@s30@#6#52L(2)|M*~Batch Number~@s8@#11#25L(2)|M*~User~@s3@#16#60R(2)|M*~Batch Value~@n14.2@#21#44L(2)|M*~No Of Jobs~@s8@#26#58L(2)|M*~Date Created~@d6@#31#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:JOBNOTES.Close
    Relate:PROINV.Close
    Relate:TRADETMP.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='View_Proforma'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  Remove(tradetmp)
  If error()
      Compile('***',Debug=1)
          Message(error())
      ***
      access:tradetmp.close()
      Remove(tradetmp)
      If error()
          Compile('***',Debug=1)
              Message('Still Not Shut: ' & error())
          ***
      End!If error()
  End!If error()
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'View_Proforma',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBS
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:AllAccounts
      IF ?tmp:AllAccounts{Prop:Checked} = True
        HIDE(?tmp:accountnumber)
      END
      IF ?tmp:AllAccounts{Prop:Checked} = False
        UNHIDE(?tmp:accountnumber)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllAccounts, Accepted)
      BRW1.ResetSort(1)
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AllAccounts, Accepted)
    OF ?tmp:accountnumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:accountnumber, Accepted)
      BRW1.ResetSort(1)
      BRW5.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:accountnumber, Accepted)
    OF ?Print_Routine
      ThisWindow.Update
      Print_Proforma_Routines(brw1.q.prv:RefNumber)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routine, Accepted)
      BRW1.ResetSort(1)
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routine, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Remove_Job
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Job, Accepted)
      thiswindow.reset(1)
      Case MessageEx('Are you sure you want to remove the tagged job(s) from this batch.<13,10><13,10>(Note: The job(s) will appear on the next batch creation)','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Loop x# = 1 To Records(glo:queue2)
                  Get(glo:queue2,x#)
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number  = glo:pointer2
                  If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                      JOB:InvoiceStatus = ''
                      JOB:InvoiceBatch = ''
                      JOB:InvoiceQuery = ''
                      access:jobs.update()
                  End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
              End!Loop x# = 1 To Records(Queue2_global)
      
          Of 2 ! &No Button
      End!Case MessageEx
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Job, Accepted)
    OF ?Remove_Batch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Batch, Accepted)
      x# =  MessageEx('You are about to delete this batch.<13,10><13,10>Do you wish to Reject the remaining jobs, or just Remove them from the batch and make them available again?','ServiceBase 2000',|
                     'Styles\Trash.ico','|&Reject Jobs|&Remove Jobs|&Cancel Delete',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
      
      If x# <> 3
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:batchjobkey)
          job:invoiceaccount = brw1.q.prv:AccountNumber
          job:invoicebatch   = brw1.q.prv:BatchNumber
          set(job:batchjobkey,job:batchjobkey)
          loop
              if access:jobs.next()
                 break
              end !if
              if job:invoiceaccount <> brw1.q.prv:AccountNumber      |
              or job:invoicebatch   <> brw1.q.prv:BatchNumber      |
                  then break.  ! end if
              Case x#
                  Of 1
                      pos = Position(job:batchjobkey)
                      JOB:InvoiceAccount  = ''
                      JOB:InvoiceStatus   = 'EXC'
                      JOB:InvoiceBatch    = ''
                      JOB:InvoiceBatch    = ''
                      job:InvoiceQuery    = ''
                      access:jobs.update()
                      Reset(job:batchjobkey,pos)
                  Of 2
                      pos = Position(job:batchjobkey)
                      JOB:InvoiceAccount  = ''
                      JOB:InvoiceStatus   = ''
                      JOB:InvoiceBatch    = ''
                      JOB:InvoiceBatch    = ''
                      Job:invoicequery    = ''
                      access:jobs.update()
                      Reset(job:batchjobkey,pos)
              End!Case x#
          end !loop
          access:jobs.restorefile(save_job_id)
          access:proinv.clearkey(prv:refnumberkey)
          prv:refnumber = brw1.q.prv:refnumber
          if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
              Delete(proinv)
          end
      End!If x# <> 3
      BRW1.ResetSort(1)
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Batch, Accepted)
    OF ?Revaluate_Batch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Revaluate_Batch, Accepted)
      thiswindow.reset(1)
      access:proinv.clearkey(prv:refnumberkey)
      prv:refnumber = brw1.q.prv:refnumber
      if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
          prv:NoOfJobs    = 0
          prv:BatchValue  = 0
          setcursor(cursor:wait)
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:batchjobkey)
          job:invoiceaccount = prv:AccountNumber
          job:invoicebatch   = prv:BatchNumber
          set(job:batchjobkey,job:batchjobkey)
          loop
              if access:jobs.next()
                 break
              end !if
              if job:invoiceaccount <> brw1.q.prv:AccountNumber      |
              or job:invoicebatch   <> brw1.q.prv:BatchNumber      |
                  then break.  ! end if
              PRV:NoOfJobs += 1
              If job:ignore_Chargeable_Charges = 'YES'
                  
                  job:sub_total   = job:courier_cost + job:parts_cost + job:labour_cost
              Else!If job:ignore_Chargeable_Costs = 'YES'
                  Pricing_Routine('C',labour",parts",pass",a")
                  IF pass" = True
                      job:parts_cost  = parts"
                      job:labour_cost = labour"
                  Else!IF pass" = False
                      job:parts_cost  = 0
                      job:labour_cost = 0
                  End!IF pass" = False
                  job:sub_total   = job:parts_cost + job:labour_cost + job:courier_cost
      
              End!If job:ignore_Chargeable_Costs = 'YES'
              access:jobs.update()
              PRV:BatchValue += job:sub_total
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          access:proinv.update()
      End!if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
      BRW1.ResetSort(1)
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Revaluate_Batch, Accepted)
    OF ?Mark_As_Read
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Mark_As_Read, Accepted)
      access:proinv.clearkey(prv:refnumberkey)
      prv:refnumber = brw1.q.prv:refnumber
      if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
          prv:status = 'REA'
          access:proinv.update()
      end!if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Mark_As_Read, Accepted)
    OF ?Reject_Job
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reject_Job, Accepted)
      thiswindow.reset(1)
      Case MessageEx('Are you sure you want to REJECT the tagged job(s) from this batch.<13,10><13,10>(Note: The job(s) will be marked as rejected and will NOT be able to be invoiced until Unrejected)','ServiceBase 2000',|
                     'Styles\Trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Loop x# = 1 To Records(glo:queue2)
                  Get(glo:queue2,x#)
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number  = glo:pointer2
                  If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                      JOB:InvoiceStatus = 'EXC'
                      JOB:InvoiceBatch = ''
                      JOB:InvoiceQuery = ''
                      access:jobs.update()
                  End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
              End!Loop x# = 1 To Records(Queue2_global)
          Of 2 ! &No Button
      End!Case MessageEx
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reject_Job, Accepted)
    OF ?Button12:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button12:2, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = brw5.q.job:ref_number
      If access:jobs.fetch(job:ref_number_key) = Level:Benign
          continue# = 1
          If job:Invoicestatus = 'AUT'
              Case MessageEx('Warning! This selected job has previously been AUTHORISED.<13,10><13,10>Are you sure you want to change this, and mark the selected job as a QUERY instead?','ServiceBase 2000',|
                             'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                  Of 2 ! &No Button
                      continue# = 0
              End!Case MessageEx
          Else!If job:Invoicestatus = 'AUT'
              If job:InvoiceStatus <> 'QUE'
                  Case MessageEx('Are you sure you want to mark this job as a QUERY?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          continue# = 0
                  End!Case MessageEx
              End!If job:InvoiceStatus <> 'QUE'
          End!If job:Invoicestatus = 'AUT'
          If continue# = 1
              Invoice_Query_Reason
              If job:InvoiceQuery <> ''
                  job:invoiceStatus = 'QUE'
                  access:jobs.update()
      
                  !Start - Remove selected job from the "tagged" list - TrkBs: 5211 (DBH: 05-01-2005)
                  glo:Pointer2 = job:Ref_Number
                  Get(glo:Queue2,glo:Pointer2)
                  If ~Error()
                      Delete(glo:Queue2)
                  End ! Error()
                  !End   - Remove selected job from the "tagged" list - TrkBs: 5211 (DBH: 05-01-2005)
              End!If job:INvoiceQuery <> ''
          End!If continue# = 1
      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button12:2, Accepted)
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::8:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Button12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button12, Accepted)
      Case MessageEx('Are you sure you want to Authorise the tagged jobs?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Loop x# = 1 To Records(glo:queue2)
                  Get(glo:queue2,x#)
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number   = glo:pointer2
                  If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                      job:InvoiceStatus   = 'AUT'
                      job:InvoiceQuery    = ''
                      access:jobs.update()
                  End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
              End!Loop x# = 1 To Records(glo:queue2)
              !Start - Untag the jobs after authorisation - TrkBs: 5211 (DBH: 05-01-2005)
              Post(Event:Accepted,?DASUNTAGALL)
              !End   - Untag the jobs after authorisation - TrkBs: 5211 (DBH: 05-01-2005)
          Of 2 ! &No Button
      End!Case MessageEx
      BRW5.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button12, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'View_Proforma')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
    BRW5.ResetSort(1)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?tmp:accountnumber
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Browse:1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, NewSelection)
      !Do DASBRW::8:DASUNTAGALL
      FREE(glo:queue2)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, NewSelection)
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::8:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='64L(2)|M*~Account Number~@s15@#1#125L(2)|M*~Account Name~@s30@#6#52L(2)|M*~Batch Number~@s8@#11#25L(2)|M*~User~@s3@#16#60R(2)|M*~Batch Value~@n14.2@#21#44L(2)|M*~No Of Jobs~@s8@#26#58L(2)|M*~Date Created~@d6@#31#'
          ?Tab:2{PROP:TEXT} = 'By Account Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='64L(2)|M*~Account Number~@s15@#1#125L(2)|M*~Account Name~@s30@#6#52L(2)|M*~Batch Number~@s8@#11#25L(2)|M*~User~@s3@#16#60R(2)|M*~Batch Value~@n14.2@#21#44L(2)|M*~No Of Jobs~@s8@#26#58L(2)|M*~Date Created~@d6@#31#'
          ?Tab6{PROP:TEXT} = 'Un-Read Batches'
        OF 3
          ?Browse:1{PROP:FORMAT} ='64L(2)|M*~Account Number~@s15@#1#125L(2)|M*~Account Name~@s30@#6#52L(2)|M*~Batch Number~@s8@#11#25L(2)|M*~User~@s3@#16#60R(2)|M*~Batch Value~@n14.2@#21#44L(2)|M*~No Of Jobs~@s8@#26#58L(2)|M*~Date Created~@d6@#31#'
          ?Tab7{PROP:TEXT} = 'Read Batches'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
    OF ?prv:AccountNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?prv:AccountNumber, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?prv:AccountNumber, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::8:DASUNTAGALL
      BRW5.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 and Upper(tmp:AllAccounts) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:no
  ELSIF Choice(?CurrentTab) = 1 and Upper(tmp:AllAccounts) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:no
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:accountnumber
  ELSIF Choice(?CurrentTab) = 2 and Upper(tmp:AllAccounts) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:unr
  ELSIF Choice(?CurrentTab) = 2 and Upper(tmp:AllAccounts) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:unr
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:accountnumber
  ELSIF Choice(?CurrentTab) = 3 and Upper(tmp:AllAccounts) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:rea
  ELSIF Choice(?CurrentTab) = 3 and Upper(tmp:AllAccounts) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:rea
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = tmp:accountnumber
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 and Upper(tmp:AllAccounts) = 'YES'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 and Upper(tmp:AllAccounts) = 'NO'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 and Upper(tmp:AllAccounts) = 'YES'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 and Upper(tmp:AllAccounts) = 'NO'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 3 and Upper(tmp:AllAccounts) = 'YES'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 3 and Upper(tmp:AllAccounts) = 'NO'
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.prv:AccountNumber_NormalFG = -1
  SELF.Q.prv:AccountNumber_NormalBG = -1
  SELF.Q.prv:AccountNumber_SelectedFG = -1
  SELF.Q.prv:AccountNumber_SelectedBG = -1
  SELF.Q.prv:AccountName_NormalFG = -1
  SELF.Q.prv:AccountName_NormalBG = -1
  SELF.Q.prv:AccountName_SelectedFG = -1
  SELF.Q.prv:AccountName_SelectedBG = -1
  SELF.Q.prv:BatchNumber_NormalFG = -1
  SELF.Q.prv:BatchNumber_NormalBG = -1
  SELF.Q.prv:BatchNumber_SelectedFG = -1
  SELF.Q.prv:BatchNumber_SelectedBG = -1
  SELF.Q.prv:User_NormalFG = -1
  SELF.Q.prv:User_NormalBG = -1
  SELF.Q.prv:User_SelectedFG = -1
  SELF.Q.prv:User_SelectedBG = -1
  SELF.Q.prv:BatchValue_NormalFG = -1
  SELF.Q.prv:BatchValue_NormalBG = -1
  SELF.Q.prv:BatchValue_SelectedFG = -1
  SELF.Q.prv:BatchValue_SelectedBG = -1
  SELF.Q.prv:NoOfJobs_NormalFG = -1
  SELF.Q.prv:NoOfJobs_NormalBG = -1
  SELF.Q.prv:NoOfJobs_SelectedFG = -1
  SELF.Q.prv:NoOfJobs_SelectedBG = -1
  SELF.Q.prv:DateCreated_NormalFG = -1
  SELF.Q.prv:DateCreated_NormalBG = -1
  SELF.Q.prv:DateCreated_SelectedFG = -1
  SELF.Q.prv:DateCreated_SelectedBG = -1
  SELF.Q.prv:RefNumber_NormalFG = -1
  SELF.Q.prv:RefNumber_NormalBG = -1
  SELF.Q.prv:RefNumber_SelectedFG = -1
  SELF.Q.prv:RefNumber_SelectedBG = -1
  SELF.Q.prv:AccountType_NormalFG = -1
  SELF.Q.prv:AccountType_NormalBG = -1
  SELF.Q.prv:AccountType_SelectedFG = -1
  SELF.Q.prv:AccountType_SelectedBG = -1
   
   
   IF (prv:Status <> 'REA')
     SELF.Q.prv:AccountNumber_NormalFG = 255
     SELF.Q.prv:AccountNumber_NormalBG = 16777215
     SELF.Q.prv:AccountNumber_SelectedFG = 16777215
     SELF.Q.prv:AccountNumber_SelectedBG = 255
   ELSE
     SELF.Q.prv:AccountNumber_NormalFG = -1
     SELF.Q.prv:AccountNumber_NormalBG = -1
     SELF.Q.prv:AccountNumber_SelectedFG = -1
     SELF.Q.prv:AccountNumber_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:AccountName_NormalFG = 255
     SELF.Q.prv:AccountName_NormalBG = 16777215
     SELF.Q.prv:AccountName_SelectedFG = 16777215
     SELF.Q.prv:AccountName_SelectedBG = 255
   ELSE
     SELF.Q.prv:AccountName_NormalFG = -1
     SELF.Q.prv:AccountName_NormalBG = -1
     SELF.Q.prv:AccountName_SelectedFG = -1
     SELF.Q.prv:AccountName_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:BatchNumber_NormalFG = 255
     SELF.Q.prv:BatchNumber_NormalBG = 16777215
     SELF.Q.prv:BatchNumber_SelectedFG = 16777215
     SELF.Q.prv:BatchNumber_SelectedBG = 255
   ELSE
     SELF.Q.prv:BatchNumber_NormalFG = -1
     SELF.Q.prv:BatchNumber_NormalBG = -1
     SELF.Q.prv:BatchNumber_SelectedFG = -1
     SELF.Q.prv:BatchNumber_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:User_NormalFG = 255
     SELF.Q.prv:User_NormalBG = 16777215
     SELF.Q.prv:User_SelectedFG = 16777215
     SELF.Q.prv:User_SelectedBG = 255
   ELSE
     SELF.Q.prv:User_NormalFG = -1
     SELF.Q.prv:User_NormalBG = -1
     SELF.Q.prv:User_SelectedFG = -1
     SELF.Q.prv:User_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:BatchValue_NormalFG = 255
     SELF.Q.prv:BatchValue_NormalBG = 16777215
     SELF.Q.prv:BatchValue_SelectedFG = 16777215
     SELF.Q.prv:BatchValue_SelectedBG = 255
   ELSE
     SELF.Q.prv:BatchValue_NormalFG = -1
     SELF.Q.prv:BatchValue_NormalBG = -1
     SELF.Q.prv:BatchValue_SelectedFG = -1
     SELF.Q.prv:BatchValue_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:NoOfJobs_NormalFG = 255
     SELF.Q.prv:NoOfJobs_NormalBG = 16777215
     SELF.Q.prv:NoOfJobs_SelectedFG = 16777215
     SELF.Q.prv:NoOfJobs_SelectedBG = 255
   ELSE
     SELF.Q.prv:NoOfJobs_NormalFG = -1
     SELF.Q.prv:NoOfJobs_NormalBG = -1
     SELF.Q.prv:NoOfJobs_SelectedFG = -1
     SELF.Q.prv:NoOfJobs_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:DateCreated_NormalFG = 255
     SELF.Q.prv:DateCreated_NormalBG = 16777215
     SELF.Q.prv:DateCreated_SelectedFG = 16777215
     SELF.Q.prv:DateCreated_SelectedBG = 255
   ELSE
     SELF.Q.prv:DateCreated_NormalFG = -1
     SELF.Q.prv:DateCreated_NormalBG = -1
     SELF.Q.prv:DateCreated_SelectedFG = -1
     SELF.Q.prv:DateCreated_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:RefNumber_NormalFG = 255
     SELF.Q.prv:RefNumber_NormalBG = 16777215
     SELF.Q.prv:RefNumber_SelectedFG = 16777215
     SELF.Q.prv:RefNumber_SelectedBG = 255
   ELSE
     SELF.Q.prv:RefNumber_NormalFG = -1
     SELF.Q.prv:RefNumber_NormalBG = -1
     SELF.Q.prv:RefNumber_SelectedFG = -1
     SELF.Q.prv:RefNumber_SelectedBG = -1
   END
   IF (prv:Status <> 'REA')
     SELF.Q.prv:AccountType_NormalFG = 255
     SELF.Q.prv:AccountType_NormalBG = 16777215
     SELF.Q.prv:AccountType_SelectedFG = 16777215
     SELF.Q.prv:AccountType_SelectedBG = 255
   ELSE
     SELF.Q.prv:AccountType_NormalFG = -1
     SELF.Q.prv:AccountType_NormalBG = -1
     SELF.Q.prv:AccountType_SelectedFG = -1
     SELF.Q.prv:AccountType_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?Sheet2) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = PRV:AccountNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = PRV:BatchNumber
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:una
  ELSIF Choice(?Sheet2) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = PRV:AccountNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = PRV:BatchNumber
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:aut
  ELSIF Choice(?Sheet2) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = PRV:AccountNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = PRV:BatchNumber
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = tmp:que
  ELSIF Choice(?Sheet2) = 4
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = PRV:AccountNumber
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = PRV:BatchNumber
  ELSE
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW5.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet2) = 3
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?Sheet2) = 4
    RETURN SELF.SetSort(4,Force)
  ELSE
    RETURN SELF.SetSort(5,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = job:Ref_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END
  SELF.Q.job:Ref_Number_NormalFG = -1
  SELF.Q.job:Ref_Number_NormalBG = -1
  SELF.Q.job:Ref_Number_SelectedFG = -1
  SELF.Q.job:Ref_Number_SelectedBG = -1
  SELF.Q.job:Order_Number_NormalFG = -1
  SELF.Q.job:Order_Number_NormalBG = -1
  SELF.Q.job:Order_Number_SelectedFG = -1
  SELF.Q.job:Order_Number_SelectedBG = -1
  SELF.Q.job:Charge_Type_NormalFG = -1
  SELF.Q.job:Charge_Type_NormalBG = -1
  SELF.Q.job:Charge_Type_SelectedFG = -1
  SELF.Q.job:Charge_Type_SelectedBG = -1
  SELF.Q.job:Repair_Type_NormalFG = -1
  SELF.Q.job:Repair_Type_NormalBG = -1
  SELF.Q.job:Repair_Type_SelectedFG = -1
  SELF.Q.job:Repair_Type_SelectedBG = -1
  SELF.Q.job:Courier_Cost_NormalFG = -1
  SELF.Q.job:Courier_Cost_NormalBG = -1
  SELF.Q.job:Courier_Cost_SelectedFG = -1
  SELF.Q.job:Courier_Cost_SelectedBG = -1
  SELF.Q.job:Labour_Cost_NormalFG = -1
  SELF.Q.job:Labour_Cost_NormalBG = -1
  SELF.Q.job:Labour_Cost_SelectedFG = -1
  SELF.Q.job:Labour_Cost_SelectedBG = -1
  SELF.Q.job:Parts_Cost_NormalFG = -1
  SELF.Q.job:Parts_Cost_NormalBG = -1
  SELF.Q.job:Parts_Cost_SelectedFG = -1
  SELF.Q.job:Parts_Cost_SelectedBG = -1
  SELF.Q.jbn:Engineers_Notes_NormalFG = -1
  SELF.Q.jbn:Engineers_Notes_NormalBG = -1
  SELF.Q.jbn:Engineers_Notes_SelectedFG = -1
  SELF.Q.jbn:Engineers_Notes_SelectedBG = -1
  SELF.Q.jbn:Invoice_Text_NormalFG = -1
  SELF.Q.jbn:Invoice_Text_NormalBG = -1
  SELF.Q.jbn:Invoice_Text_SelectedFG = -1
  SELF.Q.jbn:Invoice_Text_SelectedBG = -1
   
   
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.job:Ref_Number_NormalFG = 32768
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.job:Ref_Number_NormalFG = 8388608
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.job:Ref_Number_NormalFG = 255
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Ref_Number_NormalFG = -1
     SELF.Q.job:Ref_Number_NormalBG = -1
     SELF.Q.job:Ref_Number_SelectedFG = -1
     SELF.Q.job:Ref_Number_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.job:Order_Number_NormalFG = 32768
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.job:Order_Number_NormalFG = 8388608
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.job:Order_Number_NormalFG = 255
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Order_Number_NormalFG = -1
     SELF.Q.job:Order_Number_NormalBG = -1
     SELF.Q.job:Order_Number_SelectedFG = -1
     SELF.Q.job:Order_Number_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.job:Charge_Type_NormalFG = 32768
     SELF.Q.job:Charge_Type_NormalBG = 16777215
     SELF.Q.job:Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Charge_Type_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.job:Charge_Type_NormalFG = 8388608
     SELF.Q.job:Charge_Type_NormalBG = 16777215
     SELF.Q.job:Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Charge_Type_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.job:Charge_Type_NormalFG = 255
     SELF.Q.job:Charge_Type_NormalBG = 16777215
     SELF.Q.job:Charge_Type_SelectedFG = 16777215
     SELF.Q.job:Charge_Type_SelectedBG = 255
   ELSE
     SELF.Q.job:Charge_Type_NormalFG = -1
     SELF.Q.job:Charge_Type_NormalBG = -1
     SELF.Q.job:Charge_Type_SelectedFG = -1
     SELF.Q.job:Charge_Type_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.job:Repair_Type_NormalFG = 32768
     SELF.Q.job:Repair_Type_NormalBG = 16777215
     SELF.Q.job:Repair_Type_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.job:Repair_Type_NormalFG = 8388608
     SELF.Q.job:Repair_Type_NormalBG = 16777215
     SELF.Q.job:Repair_Type_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.job:Repair_Type_NormalFG = 255
     SELF.Q.job:Repair_Type_NormalBG = 16777215
     SELF.Q.job:Repair_Type_SelectedFG = 16777215
     SELF.Q.job:Repair_Type_SelectedBG = 255
   ELSE
     SELF.Q.job:Repair_Type_NormalFG = -1
     SELF.Q.job:Repair_Type_NormalBG = -1
     SELF.Q.job:Repair_Type_SelectedFG = -1
     SELF.Q.job:Repair_Type_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.job:Courier_Cost_NormalFG = 32768
     SELF.Q.job:Courier_Cost_NormalBG = 16777215
     SELF.Q.job:Courier_Cost_SelectedFG = 16777215
     SELF.Q.job:Courier_Cost_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.job:Courier_Cost_NormalFG = 8388608
     SELF.Q.job:Courier_Cost_NormalBG = 16777215
     SELF.Q.job:Courier_Cost_SelectedFG = 16777215
     SELF.Q.job:Courier_Cost_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.job:Courier_Cost_NormalFG = 255
     SELF.Q.job:Courier_Cost_NormalBG = 16777215
     SELF.Q.job:Courier_Cost_SelectedFG = 16777215
     SELF.Q.job:Courier_Cost_SelectedBG = 255
   ELSE
     SELF.Q.job:Courier_Cost_NormalFG = -1
     SELF.Q.job:Courier_Cost_NormalBG = -1
     SELF.Q.job:Courier_Cost_SelectedFG = -1
     SELF.Q.job:Courier_Cost_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.job:Labour_Cost_NormalFG = 32768
     SELF.Q.job:Labour_Cost_NormalBG = 16777215
     SELF.Q.job:Labour_Cost_SelectedFG = 16777215
     SELF.Q.job:Labour_Cost_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.job:Labour_Cost_NormalFG = 8388608
     SELF.Q.job:Labour_Cost_NormalBG = 16777215
     SELF.Q.job:Labour_Cost_SelectedFG = 16777215
     SELF.Q.job:Labour_Cost_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.job:Labour_Cost_NormalFG = 255
     SELF.Q.job:Labour_Cost_NormalBG = 16777215
     SELF.Q.job:Labour_Cost_SelectedFG = 16777215
     SELF.Q.job:Labour_Cost_SelectedBG = 255
   ELSE
     SELF.Q.job:Labour_Cost_NormalFG = -1
     SELF.Q.job:Labour_Cost_NormalBG = -1
     SELF.Q.job:Labour_Cost_SelectedFG = -1
     SELF.Q.job:Labour_Cost_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.job:Parts_Cost_NormalFG = 32768
     SELF.Q.job:Parts_Cost_NormalBG = 16777215
     SELF.Q.job:Parts_Cost_SelectedFG = 16777215
     SELF.Q.job:Parts_Cost_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.job:Parts_Cost_NormalFG = 8388608
     SELF.Q.job:Parts_Cost_NormalBG = 16777215
     SELF.Q.job:Parts_Cost_SelectedFG = 16777215
     SELF.Q.job:Parts_Cost_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.job:Parts_Cost_NormalFG = 255
     SELF.Q.job:Parts_Cost_NormalBG = 16777215
     SELF.Q.job:Parts_Cost_SelectedFG = 16777215
     SELF.Q.job:Parts_Cost_SelectedBG = 255
   ELSE
     SELF.Q.job:Parts_Cost_NormalFG = -1
     SELF.Q.job:Parts_Cost_NormalBG = -1
     SELF.Q.job:Parts_Cost_SelectedFG = -1
     SELF.Q.job:Parts_Cost_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.jbn:Engineers_Notes_NormalFG = 32768
     SELF.Q.jbn:Engineers_Notes_NormalBG = 16777215
     SELF.Q.jbn:Engineers_Notes_SelectedFG = 16777215
     SELF.Q.jbn:Engineers_Notes_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.jbn:Engineers_Notes_NormalFG = 8388608
     SELF.Q.jbn:Engineers_Notes_NormalBG = 16777215
     SELF.Q.jbn:Engineers_Notes_SelectedFG = 16777215
     SELF.Q.jbn:Engineers_Notes_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.jbn:Engineers_Notes_NormalFG = 255
     SELF.Q.jbn:Engineers_Notes_NormalBG = 16777215
     SELF.Q.jbn:Engineers_Notes_SelectedFG = 16777215
     SELF.Q.jbn:Engineers_Notes_SelectedBG = 255
   ELSE
     SELF.Q.jbn:Engineers_Notes_NormalFG = -1
     SELF.Q.jbn:Engineers_Notes_NormalBG = -1
     SELF.Q.jbn:Engineers_Notes_SelectedFG = -1
     SELF.Q.jbn:Engineers_Notes_SelectedBG = -1
   END
   IF (job:InvoiceStatus = 'AUT')
     SELF.Q.jbn:Invoice_Text_NormalFG = 32768
     SELF.Q.jbn:Invoice_Text_NormalBG = 16777215
     SELF.Q.jbn:Invoice_Text_SelectedFG = 16777215
     SELF.Q.jbn:Invoice_Text_SelectedBG = 32768
   ELSIF(job:InvoiceStatus = 'QUE')
     SELF.Q.jbn:Invoice_Text_NormalFG = 8388608
     SELF.Q.jbn:Invoice_Text_NormalBG = 16777215
     SELF.Q.jbn:Invoice_Text_SelectedFG = 16777215
     SELF.Q.jbn:Invoice_Text_SelectedBG = 8388608
   ELSIF(job:InvoiceStatus = 'UNA')
     SELF.Q.jbn:Invoice_Text_NormalFG = 255
     SELF.Q.jbn:Invoice_Text_NormalBG = 16777215
     SELF.Q.jbn:Invoice_Text_SelectedFG = 16777215
     SELF.Q.jbn:Invoice_Text_SelectedBG = 255
   ELSE
     SELF.Q.jbn:Invoice_Text_NormalFG = -1
     SELF.Q.jbn:Invoice_Text_NormalBG = -1
     SELF.Q.jbn:Invoice_Text_SelectedFG = -1
     SELF.Q.jbn:Invoice_Text_SelectedBG = -1
   END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = job:Ref_Number
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::8:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

