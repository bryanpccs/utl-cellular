

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABREPORT.INC'),ONCE

                     MAP
                       INCLUDE('SBK01038.INC'),ONCE        !Local module procedure declarations
                     END


AuthoriseAll PROCEDURE                                !Generated from procedure template - Process

Progress:Thermometer BYTE
pos                  STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:yes              STRING('YES')
Process:View         VIEW(JOBS)
                     END
ProgressWindow       WINDOW('Progress...'),AT(,,142,68),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,TIMER(1),GRAY,DOUBLE
                       PROGRESS,USE(Progress:Thermometer),AT(15,20,111,12),RANGE(0,100)
                       STRING(''),AT(12,8,120,8),USE(?Progress:UserString),CENTER
                       STRING(''),AT(12,36,120,8),USE(?Progress:PctText),CENTER
                       BUTTON('Cancel'),AT(43,48,56,16),USE(?Progress:Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(ReportManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeRecord             PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

ThisProcess          ProcessClass                     !Process Manager
ProgressMgr          StepLongClass                    !Progress Manager
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Progress:UserString{prop:FontColor} = -1
    ?Progress:UserString{prop:Color} = 15066597
    ?Progress:PctText{prop:FontColor} = -1
    ?Progress:PctText{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AuthoriseAll',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Progress:Thermometer',Progress:Thermometer,'AuthoriseAll',1)
    SolaceViewVars('pos',pos,'AuthoriseAll',1)
    SolaceViewVars('tmp:yes',tmp:yes,'AuthoriseAll',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Progress:Thermometer;  SolaceCtrlName = '?Progress:Thermometer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:UserString;  SolaceCtrlName = '?Progress:UserString';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:PctText;  SolaceCtrlName = '?Progress:PctText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Progress:Cancel;  SolaceCtrlName = '?Progress:Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('AuthoriseAll')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'AuthoriseAll')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Progress:Thermometer
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Access:CHARTYPE.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:STDCHRGE.UseFile
  SELF.FilesOpened = True
  OPEN(ProgressWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  ProgressMgr.Init(ScrollSort:AllowNumeric,)
  ThisProcess.Init(Process:View, Relate:JOBS, ?Progress:PctText, Progress:Thermometer, ProgressMgr, job:Ref_Number)
  ThisProcess.AddSortOrder(job:InvoiceExceptKey)
  ThisProcess.AddRange(job:Invoice_Exception,tmp:yes)
  ProgressWindow{Prop:Text} = 'Processing Records'
  ?Progress:PctText{Prop:Text} = '0% Completed'
  SELF.Init(ThisProcess)
  ?Progress:UserString{Prop:Text}='Authorising Jobs'
  SELF.AddItem(?Progress:Cancel, RequestCancelled)
  SEND(JOBS,'QUICKSCAN=on')
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Init PROCEDURE(ProcessClass PC,<REPORT R>,<PrintPreviewClass PV>)

  CODE
  PARENT.Init(PC,R,PV)
  Do RecolourWindow


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'AuthoriseAll',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  ProgressMgr.Kill()
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'AuthoriseAll')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeRecord PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  error# = 0
  if job:chargeable_job <> 'YES'
      pos = Position(job:InvoiceExceptKey)
      job:invoice_exception = 'NO'
      job:invoice_failure_reason = ''
      access:jobs.update()
      reset(job:InvoiceExceptKey,pos)
      error# = 1
  End!if job:chargeable_job <> 'YES'
  
  If job:Cancelled = 'YES'
      pos = Position(job:InvoiceExceptKey)
      job:invoice_exception = 'NO'
      job:invoice_failure_reason = ''
      access:jobs.update()
      reset(job:InvoiceExceptKey,pos)
      error# = 1
  End!if job:chargeable_job <> 'YES'
  
  If ExcludeFromInvoice(job:charge_type) and Job:chargeable_job = 'YES' And error# = 0
      pos = Position(job:InvoiceExceptKey)
      job:invoice_exception = 'NO'
      job:invoice_failure_reason = ''
      access:jobs.update()
      reset(job:InvoiceExceptKey,pos)
      error# = 1
  End!If ExcludeFromInvoice(job:charge_type)
  
  If job:date_completed = '' and error# = 0
      error# = 1
  End!If job:date_completed = ''
  count# = 0
  ! Start Change 4776 BE922/10/2004)
  !If job:bouncer = 'X' and error# = 0
  !    job:invoice_failure_reason = 'BOUNCER'
  !    access:jobs.update()
  !    count# += 1
  !    error# = 1
  !End!If job:bouncer <> ''
  If job:bouncer = 'X'
      pos = Position(job:InvoiceExceptKey)
      job:invoice_exception = 'NO'
      job:invoice_failure_reason = ''
      access:jobs.update()
      reset(job:InvoiceExceptKey,pos)
      error# = 1
      count# += 1
  End!if job:chargeable_job <> 'YES'
  ! End Change 4776 BE922/10/2004)
  
  If def:qa_required = 'YES' and error# = 0
      If job:qa_passed <> 'YES'
          job:invoice_failure_reason   = 'QA NOT PASSED'
          access:jobs.update()
          count# += 1
          error# = 1
      End!If job:qa_passed <> 'YES'
  End!If def:qa_required = 'YES'
  
  If error# = 0
      Case PricingRoutine('C')
          Of 1 Orof 3
              job:invoice_exception   = 'YES'
              job:invoice_failure_reason  = 'ZERO VALUE: NO PRICING STRUCTURE'
              access:jobs.update()
              count# += 1
              error# = 1
          Of 2
              job:sub_total    = job:labour_cost + job:parts_cost + job:courier_cost
              access:jobs.update()
              If job:sub_total = 0
                  job:invoice_failure_reason  = 'ZERO VALUE'
                  access:jobs.update()
                  count# += 1
                  error# = 1
              End!If job:sub_total = 0
          Else
              job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
      End!Case PricingRoutine('C')
  
  End!If error# = 0
  
  If count# = 0
      pos = Position(job:InvoiceExceptKey)
      job:invoice_exception = 'NO'
      job:invoice_failure_reason = ''
      access:jobs.update()
      get(audit,0)
      if access:audit.primerecord() = Level:Benign
          aud:notes         = ''
          aud:ref_number    = job:ref_number
          aud:date          = Today()
          aud:time          = Clock()
          access:users.clearkey(use:password_key)
          use:password      = glo:password
          access:users.fetch(use:password_key)
          aud:user          = use:user_code
          aud:action        = 'AUTHORISED INVOICE EXCEPTION'
          if access:audit.insert()
             access:audit.cancelautoinc()
          end
      end!if access:audit.primerecord() = Level:Benign
  
      reset(job:InvoiceExceptKey,pos)
  End!If count# = 0
  ReturnValue = PARENT.TakeRecord()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeRecord, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

