

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01049.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMulDesp PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
BRW2::View:Browse    VIEW(MULDESPJ)
                       PROJECT(mulj:RecordNumber)
                       PROJECT(mulj:RefNumber)
                       PROJECT(mulj:JobNumber)
                       PROJECT(mulj:IMEINumber)
                       PROJECT(mulj:MSN)
                       PROJECT(mulj:AccountNumber)
                       PROJECT(mulj:Courier)
                       PROJECT(mulj:Current)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?Browse:2
mulj:RecordNumber      LIKE(mulj:RecordNumber)        !List box control field - type derived from field
mulj:RefNumber         LIKE(mulj:RefNumber)           !List box control field - type derived from field
mulj:JobNumber         LIKE(mulj:JobNumber)           !List box control field - type derived from field
mulj:IMEINumber        LIKE(mulj:IMEINumber)          !List box control field - type derived from field
mulj:MSN               LIKE(mulj:MSN)                 !List box control field - type derived from field
mulj:AccountNumber     LIKE(mulj:AccountNumber)       !List box control field - type derived from field
mulj:Courier           LIKE(mulj:Courier)             !List box control field - type derived from field
mulj:Current           LIKE(mulj:Current)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::muld:Record LIKE(muld:RECORD),STATIC
QuickWindow          WINDOW('Update the MULDESP File'),AT(,,200,138),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateMulDesp'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,192,112),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?muld:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(68,20,40,10),USE(muld:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Batch Number'),AT(8,34),USE(?muld:BatchNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(68,34,124,10),USE(muld:BatchNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Batch Number'),TIP('Batch Number'),UPR
                           PROMPT('Account Number'),AT(8,48),USE(?muld:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(68,48,124,10),USE(muld:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),UPR
                         END
                         TAB('MULDESPJ'),USE(?Tab:2)
                           LIST,AT(8,20,184,92),USE(?Browse:2),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('56L(2)|M~Record Number~L(2)@s8@40L(2)|M~RefNumber~L(2)@s8@44L(2)|M~Job Number~L(' &|
   '2)@s8@80L(2)|M~I.M.E.I. Number~L(2)@s30@80L(2)|M~M.S.N.~L(2)@s30@80L(2)|M~Accoun' &|
   'tNumber~L(2)@s30@80L(2)|M~Courier~L(2)@s30@32R(2)|M~Current~C(0)@n1@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(49,80,45,14),USE(?Insert:3),HIDE
                           BUTTON('&Change'),AT(98,80,45,14),USE(?Change:3),HIDE
                           BUTTON('&Delete'),AT(147,80,45,14),USE(?Delete:3),HIDE
                         END
                       END
                       BUTTON('OK'),AT(102,120,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(151,120,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(151,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW2                 CLASS(BrowseClass)               !Browse using ?Browse:2
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW2::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW2::Sort0:StepClass StepLongClass                   !Default Step Manager
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?muld:RecordNumber:Prompt{prop:FontColor} = -1
    ?muld:RecordNumber:Prompt{prop:Color} = 15066597
    If ?muld:RecordNumber{prop:ReadOnly} = True
        ?muld:RecordNumber{prop:FontColor} = 65793
        ?muld:RecordNumber{prop:Color} = 15066597
    Elsif ?muld:RecordNumber{prop:Req} = True
        ?muld:RecordNumber{prop:FontColor} = 65793
        ?muld:RecordNumber{prop:Color} = 8454143
    Else ! If ?muld:RecordNumber{prop:Req} = True
        ?muld:RecordNumber{prop:FontColor} = 65793
        ?muld:RecordNumber{prop:Color} = 16777215
    End ! If ?muld:RecordNumber{prop:Req} = True
    ?muld:RecordNumber{prop:Trn} = 0
    ?muld:RecordNumber{prop:FontStyle} = font:Bold
    ?muld:BatchNumber:Prompt{prop:FontColor} = -1
    ?muld:BatchNumber:Prompt{prop:Color} = 15066597
    If ?muld:BatchNumber{prop:ReadOnly} = True
        ?muld:BatchNumber{prop:FontColor} = 65793
        ?muld:BatchNumber{prop:Color} = 15066597
    Elsif ?muld:BatchNumber{prop:Req} = True
        ?muld:BatchNumber{prop:FontColor} = 65793
        ?muld:BatchNumber{prop:Color} = 8454143
    Else ! If ?muld:BatchNumber{prop:Req} = True
        ?muld:BatchNumber{prop:FontColor} = 65793
        ?muld:BatchNumber{prop:Color} = 16777215
    End ! If ?muld:BatchNumber{prop:Req} = True
    ?muld:BatchNumber{prop:Trn} = 0
    ?muld:BatchNumber{prop:FontStyle} = font:Bold
    ?muld:AccountNumber:Prompt{prop:FontColor} = -1
    ?muld:AccountNumber:Prompt{prop:Color} = 15066597
    If ?muld:AccountNumber{prop:ReadOnly} = True
        ?muld:AccountNumber{prop:FontColor} = 65793
        ?muld:AccountNumber{prop:Color} = 15066597
    Elsif ?muld:AccountNumber{prop:Req} = True
        ?muld:AccountNumber{prop:FontColor} = 65793
        ?muld:AccountNumber{prop:Color} = 8454143
    Else ! If ?muld:AccountNumber{prop:Req} = True
        ?muld:AccountNumber{prop:FontColor} = 65793
        ?muld:AccountNumber{prop:Color} = 16777215
    End ! If ?muld:AccountNumber{prop:Req} = True
    ?muld:AccountNumber{prop:Trn} = 0
    ?muld:AccountNumber{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    ?Browse:2{prop:FontColor} = 65793
    ?Browse:2{prop:Color}= 16777215
    ?Browse:2{prop:Color,2} = 16777215
    ?Browse:2{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMulDesp',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMulDesp',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMulDesp',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?muld:RecordNumber:Prompt;  SolaceCtrlName = '?muld:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?muld:RecordNumber;  SolaceCtrlName = '?muld:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?muld:BatchNumber:Prompt;  SolaceCtrlName = '?muld:BatchNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?muld:BatchNumber;  SolaceCtrlName = '?muld:BatchNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?muld:AccountNumber:Prompt;  SolaceCtrlName = '?muld:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?muld:AccountNumber;  SolaceCtrlName = '?muld:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:2;  SolaceCtrlName = '?Browse:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMulDesp')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMulDesp')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?muld:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(muld:Record,History::muld:Record)
  SELF.AddHistoryField(?muld:RecordNumber,1)
  SELF.AddHistoryField(?muld:BatchNumber,2)
  SELF.AddHistoryField(?muld:AccountNumber,3)
  SELF.AddUpdateFile(Access:MULDESP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MULDESP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MULDESP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW2.Init(?Browse:2,Queue:Browse:2.ViewPosition,BRW2::View:Browse,Queue:Browse:2,Relate:MULDESPJ,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:2{prop:vcr} = TRUE
  ! support for CPCS
  BRW2.Q &= Queue:Browse:2
  BRW2.RetainRow = 0
  BRW2::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW2.AddSortOrder(BRW2::Sort0:StepClass,mulj:JobNumberKey)
  BRW2.AddRange(mulj:RefNumber,Relate:MULDESPJ,Relate:MULDESP)
  BRW2.AddLocator(BRW2::Sort0:Locator)
  BRW2::Sort0:Locator.Init(,mulj:JobNumber,1,BRW2)
  BRW2.AddField(mulj:RecordNumber,BRW2.Q.mulj:RecordNumber)
  BRW2.AddField(mulj:RefNumber,BRW2.Q.mulj:RefNumber)
  BRW2.AddField(mulj:JobNumber,BRW2.Q.mulj:JobNumber)
  BRW2.AddField(mulj:IMEINumber,BRW2.Q.mulj:IMEINumber)
  BRW2.AddField(mulj:MSN,BRW2.Q.mulj:MSN)
  BRW2.AddField(mulj:AccountNumber,BRW2.Q.mulj:AccountNumber)
  BRW2.AddField(mulj:Courier,BRW2.Q.mulj:Courier)
  BRW2.AddField(mulj:Current,BRW2.Q.mulj:Current)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW2.AskProcedure = 1
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  BRW2.AddToolbarTarget(Toolbar)
  BRW2.ToolbarItem.HelpButton = ?Help
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MULDESP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMulDesp',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMULDESPJ
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMulDesp')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW2.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW2.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

