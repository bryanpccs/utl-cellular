

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01018.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Stock_Locations PROCEDURE                      !Generated from procedure template - Window

ThisThreadActive BYTE
save_sto_id          USHORT,AUTO
CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:Location         BYTE(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                       PROJECT(loc:Active)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
loc:Active             LIKE(loc:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Site Location File'),AT(,,248,218),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Stock_Locations'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,68,148,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M*~Site Location~@s30@'),FROM(Queue:Browse:1)
                       ENTRY(@s30),AT(8,52,124,10),USE(loc:Location),FONT('Tahoma',8,,FONT:regular+FONT:italic)
                       BUTTON('&Select'),AT(168,20,76,20),USE(?Select:2),LEFT,ICON('SELECT.ICO')
                       BUTTON('&Insert'),AT(168,120,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(168,144,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO'),DEFAULT
                       BUTTON('&Delete'),AT(168,168,76,20),USE(?Delete:3),LEFT,ICON('DELETE.ICO')
                       SHEET,AT(4,4,156,212),USE(?CurrentTab),SPREAD
                         TAB('Site Location'),USE(?Tab:Site)
                           OPTION,AT(8,16,148,28),USE(tmp:Location),BOXED
                             RADIO('Active'),AT(12,28),USE(?tmp:Location:Radio1),VALUE('1')
                             RADIO('Inactive'),AT(68,28),USE(?tmp:Location:Radio2),VALUE('0')
                             RADIO('All'),AT(128,28),USE(?tmp:Location:Radio3),VALUE('2')
                           END
                         END
                       END
                       BUTTON('Close'),AT(168,196,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - tmp:Location = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - tmp:Location <> 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    If ?loc:Location{prop:ReadOnly} = True
        ?loc:Location{prop:FontColor} = 65793
        ?loc:Location{prop:Color} = 15066597
    Elsif ?loc:Location{prop:Req} = True
        ?loc:Location{prop:FontColor} = 65793
        ?loc:Location{prop:Color} = 8454143
    Else ! If ?loc:Location{prop:Req} = True
        ?loc:Location{prop:FontColor} = 65793
        ?loc:Location{prop:Color} = 16777215
    End ! If ?loc:Location{prop:Req} = True
    ?loc:Location{prop:Trn} = 0
    ?loc:Location{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:Site{prop:Color} = 15066597
    ?tmp:Location{prop:Font,3} = -1
    ?tmp:Location{prop:Color} = 15066597
    ?tmp:Location{prop:Trn} = 0
    ?tmp:Location:Radio1{prop:Font,3} = -1
    ?tmp:Location:Radio1{prop:Color} = 15066597
    ?tmp:Location:Radio1{prop:Trn} = 0
    ?tmp:Location:Radio2{prop:Font,3} = -1
    ?tmp:Location:Radio2{prop:Color} = 15066597
    ?tmp:Location:Radio2{prop:Trn} = 0
    ?tmp:Location:Radio3{prop:Font,3} = -1
    ?tmp:Location:Radio3{prop:Color} = 15066597
    ?tmp:Location:Radio3{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Stock_Locations',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_sto_id',save_sto_id,'Browse_Stock_Locations',1)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Stock_Locations',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Stock_Locations',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Stock_Locations',1)
    SolaceViewVars('tmp:Location',tmp:Location,'Browse_Stock_Locations',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loc:Location;  SolaceCtrlName = '?loc:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:Site;  SolaceCtrlName = '?Tab:Site';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location;  SolaceCtrlName = '?tmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Radio1;  SolaceCtrlName = '?tmp:Location:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Radio2;  SolaceCtrlName = '?tmp:Location:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Location:Radio3;  SolaceCtrlName = '?tmp:Location:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Stock_Locations')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Stock_Locations')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Stock_Locations'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:LOCATION.Open
  Relate:USERS_ALIAS.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:LOCATION,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,loc:Location_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?loc:Location,loc:Location,1,BRW1)
  BRW1.AddSortOrder(,loc:ActiveLocationKey)
  BRW1.AddRange(loc:Active,tmp:Location)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?loc:Location,loc:Location,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,loc:Location_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?loc:Location,loc:Location,1,BRW1)
  BRW1.AddResetField(tmp:Location)
  BRW1.AddField(loc:Location,BRW1.Q.loc:Location)
  BRW1.AddField(loc:RecordNumber,BRW1.Q.loc:RecordNumber)
  BRW1.AddField(loc:Active,BRW1.Q.loc:Active)
  QuickWindow{PROP:MinWidth}=248
  QuickWindow{PROP:MinHeight}=188
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:LOCATION.Close
    Relate:USERS_ALIAS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Stock_Locations'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Stock_Locations',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            If SecurityCheck('SITE LOCATIONS - INSERT')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            If SecurityCheck('SITE LOCATIONS - CHANGE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            If SecurityCheck('SITE LOCATIONS - DELETE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                  do_update# = false
              Else
                  Found# = 0
  
                  Save_sto_ID = Access:STOCK.SaveFile()
                  Access:STOCK.ClearKey(sto:Location_Key)
                  sto:Location    = brw1.q.loc:Location
                  Set(sto:Location_Key,sto:Location_Key)
                  Loop
                      If Access:STOCK.NEXT()
                         Break
                      End !If
                      If sto:Location    <> brw1.q.loc:Location      |
                          Then Break.  ! End If
                      Found# = 1
                      Break
                  End !Loop
                  Access:STOCK.RestoreFile(Save_sto_ID)
  
  
                  If Found#
                      Case MessageEx('You are attemping to delete site location ' & Clip(loc:Location) & '.'&|
                        '<13,10>'&|
                        '<13,10>There are parts in Stock Control under this location.'&|
                        '<13,10>'&|
                        '<13,10>If you continue ALL parts under this location will be DELETED.'&|
                        '<13,10>'&|
                        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                     'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,700,CHARSET:ANSI,8421631,'',0,48,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              Case MessageEx('You have selected to delete ALL Parts under location ' & Clip(loc:Location) & '.'&|
                                '<13,10>'&|
                                '<13,10>You will not be able to recover those parts.'&|
                                '<13,10>'&|
                                '<13,10>Are you sure you want to delete this location?','ServiceBase 2000',|
                                             'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,16777215,700,CHARSET:ANSI,255,'',0,48,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      Do_Update# = False
                              End!Case MessageEx
                          Of 2 ! &No Button
                              Do_Update# = False
                      End!Case MessageEx
                  End !If Found#
  
            end
    end !case request
  
    if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateLOCATION
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Location, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Stock_Locations')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF tmp:Location = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF tmp:Location <> 2
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?LOC:Location, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

