

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01048.INC'),ONCE        !Local module procedure declarations
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
MultipleBatchDespatch PROCEDURE                       !Generated from procedure template - Window

ThisThreadActive BYTE
tmp:CloseWindow      BYTE(0)
save_muld_id         USHORT,AUTO
save_mulj_id         USHORT,AUTO
save_lac_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
tmp:BatchNumber      LONG
tmp:Error            BYTE(0)
tmp:AccountNumber    STRING(30)
tmp:Courier          STRING(30)
tmp:JobNumber        STRING(30)
tmp:IMEINumber       STRING(30)
tmp:ErrorMessage     STRING(255)
tmp:BeginBatch       BYTE(0)
tmp:PrintSummaryNote BYTE(0)
tmp:FirstRecord      BYTE(0)
tmp:PrintDespatchNote BYTE(0)
Despatch_Label_Type_Temp STRING(3)
tmp:DoDespatch       BYTE(0)
save_job_ali_id      USHORT,AUTO
sav:Path             STRING(255)
tmp:LabelError       STRING(30)
Account_Number2_Temp STRING(30)
tmp:OldConsignNo     STRING(30)
tmp:ParcellineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
tmp:ConsignNo        STRING(30)
tmp:IndividualDespatch BYTE(0)
Courier_Temp         STRING(30)
tmp:PassedConsignNo  STRING(30)
local:FileName       STRING(255),STATIC
tmp:ReturnedBatchNumber LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW4::View:Browse    VIEW(MULDESP)
                       PROJECT(muld:AccountNumber)
                       PROJECT(muld:Courier)
                       PROJECT(muld:BatchNumber)
                       PROJECT(muld:BatchTotal)
                       PROJECT(muld:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
muld:AccountNumber     LIKE(muld:AccountNumber)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
muld:Courier           LIKE(muld:Courier)             !List box control field - type derived from field
muld:BatchNumber       LIKE(muld:BatchNumber)         !List box control field - type derived from field
muld:BatchTotal        LIKE(muld:BatchTotal)          !List box control field - type derived from field
muld:RecordNumber      LIKE(muld:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Multiple Batch Despatch'),AT(,,488,271),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),ALRT(F2Key),ALRT(F3Key),ALRT(EscKey),GRAY,DOUBLE
                       SHEET,AT(4,188,480,52),USE(?Sheet2),SPREAD
                         TAB('Input Batch'),USE(?Tab2)
                           PROMPT('Job Number'),AT(12,204),USE(?tmp:JobNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(88,204,64,10),USE(tmp:JobNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),REQ,UPR
                           PROMPT('Batch Number'),AT(236,204),USE(?TheBatchNumber),HIDE,FONT(,14,,FONT:bold)
                           PROMPT('I.M.E.I. Number'),AT(12,220),USE(?tmp:IMEINumber:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(88,220,124,10),USE(tmp:IMEINumber),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),REQ,UPR
                         END
                       END
                       SHEET,AT(4,4,480,180),USE(?Sheet1),SPREAD
                         TAB('Batches In Progress'),USE(?Tab1)
                           ENTRY(@s30),AT(8,20,124,10),USE(muld:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),UPR
                           GROUP,AT(396,32,83,151),USE(?BatchButtons)
                             BUTTON('Finish Current Batch [F10]'),AT(400,49,76,20),USE(?FinishCurrentBatch),LEFT,ICON('stopbox.gif')
                             BUTTON('Delete Batch'),AT(399,129,76,20),USE(?Delete),LEFT,ICON('delete.ico')
                             BUTTON('&List Jobs In Batch'),AT(399,161,76,20),USE(?ListJobsInBatch),LEFT,ICON('listbox.gif')
                           END
                           LIST,AT(8,36,384,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('68L(2)|M~Account Number~@s30@115L(2)|M~Company Name~@s30@91L(2)|M~Courier~@s30@4' &|
   '3L(2)|M~Batch No~@s4@32L(2)|M~Total In Batch~@s8@'),FROM(Queue:Browse)
                         END
                       END
                       PANEL,AT(4,244,480,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Close Multi - Despatch'),AT(400,248,80,16),USE(?CloseDespatch),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  IncrementalLocatorClass          !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
    Map
LocalValidateAccessories    Procedure(),Byte
LocalValidateIMEI           Procedure(),Byte
LocalValidateAccountNumber  Procedure(),Byte
    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?tmp:JobNumber:Prompt{prop:FontColor} = -1
    ?tmp:JobNumber:Prompt{prop:Color} = 15066597
    If ?tmp:JobNumber{prop:ReadOnly} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 15066597
    Elsif ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 8454143
    Else ! If ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 16777215
    End ! If ?tmp:JobNumber{prop:Req} = True
    ?tmp:JobNumber{prop:Trn} = 0
    ?tmp:JobNumber{prop:FontStyle} = font:Bold
    ?TheBatchNumber{prop:FontColor} = -1
    ?TheBatchNumber{prop:Color} = 15066597
    ?tmp:IMEINumber:Prompt{prop:FontColor} = -1
    ?tmp:IMEINumber:Prompt{prop:Color} = 15066597
    If ?tmp:IMEINumber{prop:ReadOnly} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 15066597
    Elsif ?tmp:IMEINumber{prop:Req} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 8454143
    Else ! If ?tmp:IMEINumber{prop:Req} = True
        ?tmp:IMEINumber{prop:FontColor} = 65793
        ?tmp:IMEINumber{prop:Color} = 16777215
    End ! If ?tmp:IMEINumber{prop:Req} = True
    ?tmp:IMEINumber{prop:Trn} = 0
    ?tmp:IMEINumber{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?muld:AccountNumber{prop:ReadOnly} = True
        ?muld:AccountNumber{prop:FontColor} = 65793
        ?muld:AccountNumber{prop:Color} = 15066597
    Elsif ?muld:AccountNumber{prop:Req} = True
        ?muld:AccountNumber{prop:FontColor} = 65793
        ?muld:AccountNumber{prop:Color} = 8454143
    Else ! If ?muld:AccountNumber{prop:Req} = True
        ?muld:AccountNumber{prop:FontColor} = 65793
        ?muld:AccountNumber{prop:Color} = 16777215
    End ! If ?muld:AccountNumber{prop:Req} = True
    ?muld:AccountNumber{prop:Trn} = 0
    ?muld:AccountNumber{prop:FontStyle} = font:Bold
    ?BatchButtons{prop:Font,3} = -1
    ?BatchButtons{prop:Color} = 15066597
    ?BatchButtons{prop:Trn} = 0
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
UpdateBatch     Routine

    tmp:IndividualDespatch = 0
    Access:JOBS.Clearkey(job:Ref_Number_Key)
    job:Ref_Number  = tmp:JobNumber
    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        !Found
        Error# = 0

        !Is the job actually ready to despatch
        If Error# = 0
            !Is the job actually ready to despatch?
            If job:Despatch_Type <> 'JOB' And job:Despatch_Type <> 'EXC' and job:Despatch_Type <> 'LOA'
                Error# = 1
            End !If job:Despatch_Type <> 'JOB' And job:Despatch_Type <> 'EXC' and job:Despatch_Type <> 'LOA'
            If Error# = 0
                Case job:Despatch_Type
                    Of 'JOB'
                        If job:Consignment_Number <> ''
                            Error# = 1
                        End !If job:Consignment_Number <> ''
                    Of 'EXC'
                        If job:Exchange_Consignment_Number <> ''
                            Error# = 1
                        End !If job:Exchange_Consignment_Number <> ''
                    Of 'LOA'
                        If job:Loan_Consignment_Number <> ''
                            Error# = 1
                        End !If job:Loan_Consignment_Number <> ''
                End !Case job:Despatch_Type
            End !If tmp:Error = 0.
            If Error# = 1
                Case MessageEx('The selected job is not ready to despatch.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End !If Error# = 1
        End !If tmp:Error = 0

        !Does a courier exist?
        Access:COURIER.ClearKey(cou:Courier_Key)

        Case job:Despatch_Type
            Of 'JOB'
                If job:Courier = ''
                    Case MessageEx('The selected Job does not have a courier assigned to it.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End !If job:Courier = ''
                cou:Courier = job:Courier

            Of 'EXC'
                If job:Exchange_Courier = ''
                    Case MessageEx('The selected Job''s Exchange Unit does not have a courier assigned to it.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End !If job:Exchange_Courier = ''
                cou:Courier = job:Exchange_Courier
            Of 'LOA'
                If job:Loan_Courier = ''
                    Case MessageEx('The selected Job''s Loan Unit does not have a courier assigned to it.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End !If job:Loan_Courier = ''
                cou:Courier = job:Loan_Courier
        End !Case job:Despatch_Type

        !Should the courier be despatched from here?
        If Error# = 0
            If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                !Found
                If cou:courier_Type = 'ANC' Or cou:Courier_Type = 'ROYAL MAIL' Or |
                    cou:Courier_Type = 'PARCELINE' or cou:Courier_Type = 'UPS'
                    Case MessageEx('The selected job''s Courier can only be despatched from "Despatch Procedures".','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End !
                !If Tote, force Individual Despatch
                ! Start Change 3813 BE(19/01/04)
                !If cou:Courier_Type = 'TOTE'
                IF ((cou:Courier_Type = 'TOTE') OR (cou:Courier_Type = 'TOTE PARCELINE')) THEN
                ! End Change 3813 BE(19/01/04)
                    tmp:IndividualDespatch = 1
                End !If cou:Courier_Type = 'TOTE'
            Else!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
        End !If Error# = 0

        !Is this Job already attached to a batch?
        If Error# = 0
            Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
            mulj:JobNumber = tmp:JobNumber
            If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                !Found
                Case MessageEx('The selected Job is already attached to a batch.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                Error# = 1
            Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
        End !If Error# = 0

        !Is the job's account only used for Batch Despatch
        !Although I don't think any uses this feature.
        If Error# = 0
            Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
            sub:Account_Number = job:Account_Number
            If Access:SUBTRACC.Fetch(sub:Account_Number_Key) = Level:Benign
                Access:TRADEACC.ClearKey(tra:Account_Number_Key)
                tra:Account_Number = sub:Main_Account_Number
                If Access:TRADEACC.Fetch(tra:Account_Number_Key) = Level:Benign
                    If tra:Skip_Despatch = 'YES'
                        Case MessageEx('The selected job''s Account can only be used for "Batch Despatch.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    end!if tra:use_sub_accounts = 'YES'
                    If Error# = 0
                        If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                            If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                                Case MessageEx('The selected job''s Trade Account is on stop.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Error# = 1
                            End !If job:Chargeable_Job = 'YES' And sub:Stop_Account = 'YES'
                            If sub:UseCustDespAdd = 'YES'
                                tmp:IndividualDespatch = 1
                            End !If sub:UseCustDespAdd = 'YES'
                        Else !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                            If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                                Case MessageEx('The selected job''s Trade Account is on stop.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Error# = 1
                            End !If job:Chargeable_Job = 'YES' And tra:Stop_Account = 'YES'
                            If tra:UseCustDespAdd = 'YES'
                                tmp:IndividualDespatch = 1
                            End !If tra:UseCustDespAdd = 'YES'
                        End !If tra:Use_Sub_Accounts = 'YES' And tra:Invoice_Sub_Accounts = 'YES'
                    End !If Error# = 0

                end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
            end!if access:subtracc.fetch(sub:account_number_key) = level:benign  

        End !If Error# = 0

        !If the IMEI number has been used, is it correct?
        If Error# = 0
            If ?tmp:IMEINumber{prop:hide} = 0
                !If the IMEI number is required, does it match the job/loan/exchange?
                Case job:Despatch_Type
                    Of 'JOB'
                        If job:ESN <> tmp:IMEINumber
                            Case MessageEx('The I.M.E.I. Number does not match the selected job.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            Error# = 1
                        End !If job:ESN <> tmp:IMEINumber
                    Of 'EXC'
                        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
                        xch:Ref_Number = job:Exchange_Unit_Number
                        If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                            !Found

                        Else!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            Case MessageEx('The I.M.E.I. Number does not match the selected job''s Exchange Unit.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            Error# = 1
                        End!If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                    Of 'LOA'
                        Access:LOAN.ClearKey(loa:Ref_Number_Key)
                        loa:Ref_Number = job:Loan_Unit_Number
                        If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                            !Found

                        Else!If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                            !Error
                            !Assert(0,'<13,10>Fetch Error<13,10>')
                            Case MessageEx('The I.M.E.I. Number does not match the selected job''s Loan Unit.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            Error# = 1
                        End!If Access:LOAN.TryFetch(loa:Ref_Number_Key) = Level:Benign
                End !Case job:Despatch_Type
            End !If ?tmp:IMEINumber{prop:hide} = 0
        End !If Error# = 0

        
    Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
        Case MessageEx('The selected Job Number does not exist.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        Error# = 1
    End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign

    If Error# = 0
        If tmp:IndividualDespatch
            Error# = 0
            If def:Force_Accessory_Check = 'YES'
                If LocalValidateAccessories()
                    Access:JOBS.Update()
                    !Failed Check
                    Error# = 1
                End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
            End !If def:Force_Accessory_Check = 'YES'

            If def:ValidateDesp = 'YES'
                If LocalValidateIMEI()
                    !Failed Check
                    Error# = 1
                End !If LocalValidateIMEI()
            End !If def:ValidateDesp = 'YES'

            If Error# = 0
                !Get the Courier. Required by 'despsing.inc'
                Access:COURIER.ClearKey(cou:Courier_Key)
                Case job:Despatch_Type
                    Of 'JOB'
                        cou:Courier = job:Courier
                    Of 'EXC'
                        cou:Courier = job:Exchange_Courier
                    Of 'LOA'
                        cou:Courier = job:Loan_Courier
                End !Case job:Despatch_Type
                If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Found
                    If access:desbatch.primerecord() = Level:Benign
                        If access:desbatch.tryinsert()
                            access:desbatch.cancelautoinc()
                        Else!If access:desbatch.tryinsert()
                            DespatchSingle()
                            ?TheBatchNumber{prop:Hide} = 1
                        End!If access:desbatch.tryinsert()
                    End!If access:desbatch.primerecord() = Level:Benign

                Else!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign

            End !If Error# = 0

        Else !If tmp:IndividualDespatch
            If job:Chargeable_Job = 'YES' and job:Despatch_Type = 'JOB'
                If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                    ! Start Change 2893 BE(21/07/03)
                    !If CheckPaid() = Level:Benign
                    Total_Price('C', paid_vat", paid_total", paid_balance")
                    !p_total$ = paid_total"
                    p_balance$ = paid_balance"
                    IF (p_balance$ > 0) THEN
                    ! End Change 2893 BE(21/07/03)
                        glo:Select1 = job:Ref_Number
                        Browse_Payments
                        glo:Select1 = ''
                    End !If CheckPaid() = Level:Benign
                End !If GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI') = 1
            End !If job:Chargeable_Job = 'YES'
            tmp:ReturnedBatchNumber = DespatchProcess(tmp:JobNumber)
            If tmp:ReturnedBatchNumber <> 0
                ?TheBatchNumber{prop:Text} = 'Last Job On Batch Number: ' & tmp:ReturnedBatchNumber
                ?TheBatchNumber{prop:Hide} = 0
            Else
                ?TheBatchNumber{prop:Hide} = 1
            End !If tmp:ReturnedBatchNumber <> 0
        End !If tmp:IndividualDespatch
        
    End !If Error# = 0

    !Error Checking
    If Error# = 1
        ?TheBatchNumber{prop:Hide} = 1
    End !If Error# = 1

    Brw4.ResetSort(1)
    Select(?tmp:JobNumber)
ShowError       Routine
!    Case tmp:Error
!        Of 0
!            tmp:ErrorMessage    = ''
!        Of 1
!            tmp:ErrorMessage    = 'Job Number Does Not Exist'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 2
!            tmp:ErrorMessage    = 'Job Number / I.M.E.I. Number Mismatch'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 3
!            tmp:ErrorMessage    = 'Unit Not Ready To Despatch'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 4
!            tmp:ErrorMessage    = 'Account On Stop - Do Not Despatch'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 5
!            tmp:ErrorMessage    = 'Job Number / Account Number Mismatch'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 6
!            tmp:ErrorMessage    = 'Job Number Aleady In A Batch'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Black
!        Of 7
!            tmp:ErrorMessage    = 'Courier Mismatch Not Accepted'
!            ?tmp:ErrorMEssage{prop:FontColor} = Color:Red
!            Access:JOBS.Clearkey(job:Ref_Number_Key)
!            job:Ref_Number  = tmp:JobNumber
!            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!                !Found
!                GetStatus(850,1,job:Despatch_Type)
!                If Access:AUDIT.PrimeRecord() = Level:Benign
!                    aud:Notes         = 'COURIER MISMATCH<13,10,13,10>JOB COURIER: ' & Clip(job:Courier) &|
!                                        '<13,10>DESPATCH COURIER: ' & Clip(tmp:Courier)
!                    aud:Ref_Number    = job:ref_number
!                    aud:Date          = Today()
!                    aud:Time          = Clock()
!                    aud:Type          = job:Despatch_Type
!                    Access:USERS.ClearKey(use:Password_Key)
!                    use:Password      = glo:Password
!                    Access:USERS.Fetch(use:Password_Key)
!                    aud:User          = use:User_Code
!                    aud:Action        = 'FAILED DESPATCH VALIDATION'
!                    Access:AUDIT.Insert()
!                End!If Access:AUDIT.PrimeRecord() = Level:Benign
!                Access:JOBS.Update()
!            Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!
!
!        Of 8
!            tmp:ErrorMessage    = 'Accessory Mismatch Not Accepted'
!            ?tmp:ErrorMessage{prop:FontColor} = color:Red
!            Access:JOBS.Clearkey(job:Ref_number_Key)
!            job:Ref_Number  = tmp:JobNumber
!            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!                !Found
!                GetStatus(850,1,job:Despatch_Type)
!                If Access:AUDIT.PrimeRecord() = Level:Benign
!                    aud:Notes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
!                    If job:Despatch_Type <> 'LOAN'
!                        Save_jac_ID = Access:JOBACC.SaveFile()
!                        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
!                        jac:Ref_Number = job:Ref_Number
!                        Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
!                        Loop
!                            If Access:JOBACC.NEXT()
!                               Break
!                            End !If
!                            If jac:Ref_Number <> job:Ref_Number      |
!                                Then Break.  ! End If
!                            aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
!                        End !Loop
!                        Access:JOBACC.RestoreFile(Save_jac_ID)
!                    Else !If job:Despatch_Type <> 'LOAN'
!                        Save_lac_ID = Access:LOANACC.SaveFile()
!                        Access:LOANACC.ClearKey(lac:Ref_Number_Key)
!                        lac:Ref_Number = job:Ref_Number
!                        Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
!                        Loop
!                            If Access:LOANACC.NEXT()
!                               Break
!                            End !If
!                            If lac:Ref_Number <> job:Ref_Number      |
!                                Then Break.  ! End If
!                            aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(lac:Accessory)
!                        End !Loop
!                        Access:LOANACC.RestoreFile(Save_lac_ID)
!                    End !If job:Despatch_Type <> 'LOAN'
!                    aud:Notes         = Clip(aud:Notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
!                    Clear(glo:Queue)
!                    Loop x# = 1 To Records(glo:Queue)
!                        Get(glo:Queue,x#)
!                        aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(glo:Pointer)
!                    End !Loop x# = 1 To Records(glo:Queue).
!
!                    aud:Ref_Number    = job:ref_number
!                    aud:Date          = Today()
!                    aud:Time          = Clock()
!                    aud:Type          = job:Despatch_Type
!                    Access:USERS.ClearKey(use:Password_Key)
!                    use:Password      = glo:Password
!                    Access:USERS.Fetch(use:Password_Key)
!                    aud:User          = use:User_Code
!                    aud:Action        = 'FAILED DESPATCH VALIDATION'
!                    Access:AUDIT.Insert()
!                End!If Access:AUDIT.PrimeRecord() = Level:Benign
!                Access:JOBS.Update()
!            Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!                !Error
!                !Assert(0,'<13,10>Fetch Error<13,10>')
!            End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
!        Of 9
!            tmp:ErrorMessage    = 'Account Number Missing'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 10
!            tmp:ErrorMessage    = 'Courier Missing'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        OF 11
!            tmp:ErrorMessage    = 'Account Used For Batch Despatch ONLY'
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 12
!            tmp:ErrorMessage    = 'Error! Must be despatched via ''Despatch Procedures'''
!            ?tmp:ErrorMessage{prop:FontColor} = Color:Red
!        Of 13
!            Do MarkJobs
!            Do DeleteAnyRecords
!            Do ResetWindow
!
!    End !Case tmp:Error
!    Display(?tmp:ErrorMessage)
!    If tmp:Error
!        Select(?tmp:JobNumber)
!        If tmp:FirstRecord
!            tmp:AccountNumber = ''
!            tmp:Courier = ''
!        End !If tmp:FirstRecord
!    End !If tmp:Error
!    Display()
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'MultipleBatchDespatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:CloseWindow',tmp:CloseWindow,'MultipleBatchDespatch',1)
    SolaceViewVars('save_muld_id',save_muld_id,'MultipleBatchDespatch',1)
    SolaceViewVars('save_mulj_id',save_mulj_id,'MultipleBatchDespatch',1)
    SolaceViewVars('save_lac_id',save_lac_id,'MultipleBatchDespatch',1)
    SolaceViewVars('save_jac_id',save_jac_id,'MultipleBatchDespatch',1)
    SolaceViewVars('save_cou_ali_id',save_cou_ali_id,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:BatchNumber',tmp:BatchNumber,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:Error',tmp:Error,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:Courier',tmp:Courier,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:IMEINumber',tmp:IMEINumber,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:ErrorMessage',tmp:ErrorMessage,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:BeginBatch',tmp:BeginBatch,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:PrintSummaryNote',tmp:PrintSummaryNote,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:FirstRecord',tmp:FirstRecord,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:PrintDespatchNote',tmp:PrintDespatchNote,'MultipleBatchDespatch',1)
    SolaceViewVars('Despatch_Label_Type_Temp',Despatch_Label_Type_Temp,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:DoDespatch',tmp:DoDespatch,'MultipleBatchDespatch',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'MultipleBatchDespatch',1)
    SolaceViewVars('sav:Path',sav:Path,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:LabelError',tmp:LabelError,'MultipleBatchDespatch',1)
    SolaceViewVars('Account_Number2_Temp',Account_Number2_Temp,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:OldConsignNo',tmp:OldConsignNo,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:ParcellineName',tmp:ParcellineName,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:WorkstationName',tmp:WorkstationName,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:IndividualDespatch',tmp:IndividualDespatch,'MultipleBatchDespatch',1)
    SolaceViewVars('Courier_Temp',Courier_Temp,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:PassedConsignNo',tmp:PassedConsignNo,'MultipleBatchDespatch',1)
    SolaceViewVars('local:FileName',local:FileName,'MultipleBatchDespatch',1)
    SolaceViewVars('tmp:ReturnedBatchNumber',tmp:ReturnedBatchNumber,'MultipleBatchDespatch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber:Prompt;  SolaceCtrlName = '?tmp:JobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber;  SolaceCtrlName = '?tmp:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TheBatchNumber;  SolaceCtrlName = '?TheBatchNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IMEINumber:Prompt;  SolaceCtrlName = '?tmp:IMEINumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:IMEINumber;  SolaceCtrlName = '?tmp:IMEINumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?muld:AccountNumber;  SolaceCtrlName = '?muld:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?BatchButtons;  SolaceCtrlName = '?BatchButtons';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FinishCurrentBatch;  SolaceCtrlName = '?FinishCurrentBatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ListJobsInBatch;  SolaceCtrlName = '?ListJobsInBatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CloseDespatch;  SolaceCtrlName = '?CloseDespatch';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('MultipleBatchDespatch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'MultipleBatchDespatch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:JobNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='MultipleBatchDespatch'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:LOANACC.Open
  Relate:MULDESP.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:COURIER.UseFile
  Access:JOBS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBACC.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:MULDESPJ.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:MULDESP,SELF)
  OPEN(window)
  SELF.Opened=True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:ValidateDesp = 'YES'
      ?tmp:IMEINumber{prop:Hide} = 0
      ?tmp:IMEINumber:Prompt{prop:Hide} = 0
  Else
      ?tmp:IMEINumber{prop:Hide} = 1
      ?tmp:IMEINumber:Prompt{prop:Hide} = 1
  End !def:ValidateDesp
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,muld:AccountNumberKey)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(?muld:AccountNumber,muld:AccountNumber,1,BRW4)
  BRW4.AddField(muld:AccountNumber,BRW4.Q.muld:AccountNumber)
  BRW4.AddField(sub:Company_Name,BRW4.Q.sub:Company_Name)
  BRW4.AddField(muld:Courier,BRW4.Q.muld:Courier)
  BRW4.AddField(muld:BatchNumber,BRW4.Q.muld:BatchNumber)
  BRW4.AddField(muld:BatchTotal,BRW4.Q.muld:BatchTotal)
  BRW4.AddField(muld:RecordNumber,BRW4.Q.muld:RecordNumber)
  BRW4.AskProcedure = 1
  SELF.SetAlerts()
  Brw4.InsertControl = 0
  Brw4.ChangeControl = 0
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:LOANACC.Close
    Relate:MULDESP.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='MultipleBatchDespatch'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'MultipleBatchDespatch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateMulDesp
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:JobNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobNumber, Accepted)
      If ?tmp:IMEINumber{prop:Hide} = 1
          Do UpdateBatch
      End !?tmp:IMEINumber{prop:Hide} = 1
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobNumber, Accepted)
    OF ?tmp:IMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:IMEINumber, Accepted)
      Do UpdateBatch
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:IMEINumber, Accepted)
    OF ?FinishCurrentBatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishCurrentBatch, Accepted)
      Case MessageEx('Are you sure you want to finish and despatch this batch?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Brw4.UpdateViewRecord()
              tmp:BatchNumber = muld:RecordNumber
      
              tmp:PrintSummaryNote = 0
              Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
              sub:Account_Number  = muld:AccountNumber
              If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Found
                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                  tra:Account_Number  = sub:Main_Account_Number
                  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      !Found
                      If tra:Use_Sub_Accounts = 'YES'
                          If sub:Print_Despatch_Despatch  = 'YES'
                              If sub:Summary_Despatch_Notes = 'YES'
                                  tmp:PrintSummaryNote = 1
                              End !If sub:Summary_Despatch_Notes = 'YES'
                          End !If sub:Print_Despatch_Despatch  = 'YES'
                          Despatch_Label_Type_temp    = 'SUB'
                      Else !If tra:Use_Sub_Accounts = 'YES'
                          If tra:Print_Despatch_Despatch = 'YES'
                              If tra:Summary_Despatch_Notes = 'YES'
                                  tmp:PrintSummaryNote = 1
                              End !If tra:Summary_Despatch_Notes = 'YES'
                          End !If tra:Print_Despatch_Despatch = 'YES'
                          Despatch_Label_Type_Temp = 'TRA'
      
                      End !If tra:Use_Sub_Accounts = 'YES'
                  Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  
              Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
              !Fill in old variables
              Courier_Temp = muld:Courier
              Account_Number2_Temp = muld:AccountNumber
              If access:desbatch.primerecord() = Level:Benign
                  If access:desbatch.tryinsert()
                      access:desbatch.cancelautoinc()
                  Else!If access:desbatch.tryinsert()
                      DespatchMultiple(muld:Courier,muld:AccountNumber,tmp:BatchNumber,Despatch_Label_Type_Temp)
                  End!
              End!If access:desbatch.tryinsert()
      
              !Delete the records from the files.
              !Relate delete didn't work, as usual, will do it manually
              Save_mulj_ID = Access:MULDESPJ.SaveFile()
              Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
              mulj:RefNumber = tmp:BatchNumber
              Set(mulj:JobNumberKey,mulj:JobNumberKey)
              Loop
                  If Access:MULDESPJ.NEXT()
                     Break
                  End !If
                  If mulj:RefNumber <> tmp:BatchNumber   |
                      Then Break.  ! End If
                  Delete(MULDESPJ)
              End !Loop
              Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
              !Reget the record to make sure I'm deleteing the right thing
              Access:MULDESP.ClearKey(muld:RecordNumberKey)
              muld:RecordNumber = tmp:BatchNumber
              If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                  !Found
                  Delete(MULDESP)
              Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              
      
          Of 2 ! &No Button
      End!Case MessageEx
      BRW4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishCurrentBatch, Accepted)
    OF ?ListJobsInBatch
      ThisWindow.Update
      ListJobsOnBatch(brw4.q.muld:RecordNumber)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ListJobsInBatch, Accepted)
      brw4.UpdateViewRecord
      !Count how many jobs there are on the batch.
      !I could just add one to the record, but I can see
      !that being too inaccurate. So I'll physically count
      !all the records in the batch instead.
      
      CountBatch# = 0
      Save_mulj_ID = Access:MULDESPJ.SaveFile()
      Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
      mulj:RefNumber = muld:RecordNumber
      Set(mulj:JobNumberKey,mulj:JobNumberKey)
      Loop
          If Access:MULDESPJ.NEXT()
             Break
          End !If
          If mulj:RefNumber <> muld:RecordNumber      |
              Then Break.  ! End If
          CountBatch# += 1
      End !Loop
      Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
      muld:BatchTotal = CountBatch#
      Access:MULDESP.TryUpdate()
      
      Brw4.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ListJobsInBatch, Accepted)
    OF ?CloseDespatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CloseDespatch, Accepted)
          tmp:CloseWindow = 1
          Post(Event:CloseWindow)
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CloseDespatch, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'MultipleBatchDespatch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      If ~tmp:CloseWindow
          Cycle
      End !tmp:CloseWindow
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F10Key
              Post(Event:Accepted,?FinishCurrentBatch)
      
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
        IF GetComputerName(JB:ComputerName,JB:ComputerLen).
        tmp:WorkstationName=JB:ComputerName
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    Case job:Despatch_Type
        Of 'JOB'
            If AccessoryCheck('JOB')
                If AccessoryMismatch(job:Ref_Number,'JOB')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'JOB')
            End !If AccessoryCheck('JOB')
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,job:Despatch_Type)
        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Notes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If job:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            aud:Notes         = Clip(aud:Notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            aud:Ref_Number    = job:Ref_Number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = job:Despatch_Type
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'FAILED DESPATCH VALIDATION'
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
        Access:JOBS.Update()
        Return Level:Fatal
        
    End !If Return Level:Fatal

    Return Level:Benign
LocalValidateIMEI    Procedure()
local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            
        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_Number) & '.' &|
          '<13,10>'&|
          '<13,10>I.M.E.I. Number mismatch.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        GetStatus(850,1,job:Despatch_Type)
        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Notes         = 'I.M.E.I. NUMBER MISMATCH'
            aud:Ref_Number    = job:ref_number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = job:Despatch_Type
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'FAILED DESPATCH VALIDATION'
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccountNumber      Procedure()
    Code

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Case MessageEx('The Trade Account attached to Job Number '&Clip(job:ref_number)&' is only used for ''Batch Despatch''.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'Skip Futher Errors',skip_error#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                    Of 1 ! &OK Button
                End!Case MessageEx
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The Trade Account is on stop and it is not a Warranty Job.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                 If tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The Trade Account is on stop and it is not a Warranty Job.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Benign

BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete
  END


BRW4.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, SetQueueRecord, ())
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number  = muld:AccountNumber
  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Found
  
  Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:SUBTRACC.Tryfetch() = Level:Benign
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(4, SetQueueRecord, ())


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
