

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01041.INC'),ONCE        !Local module procedure declarations
                     END


UpdateRAPIDLST PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::RAP:Record  LIKE(RAP:RECORD),STATIC
QuickWindow          WINDOW('Update Rapid'),AT(,,334,83),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateRAPIDLST'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,328,48),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Program Name'),AT(8,20),USE(?RAP:ProgramName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s60),AT(84,20,244,10),USE(RAP:ProgramName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Program Name'),TIP('Program Name'),REQ,CAP
                           PROMPT('EXE Path'),AT(8,36),USE(?RAP:EXEPath:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,36,124,10),USE(RAP:EXEPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('EXE Path'),TIP('EXE Path'),REQ,UPR
                           BUTTON,AT(212,36,10,10),USE(?LookupEXEPath),SKIP,ICON('List3.ico')
                         END
                       END
                       BUTTON('&OK'),AT(216,60,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(272,60,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,56,328,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FileLookup8          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?RAP:ProgramName:Prompt{prop:FontColor} = -1
    ?RAP:ProgramName:Prompt{prop:Color} = 15066597
    If ?RAP:ProgramName{prop:ReadOnly} = True
        ?RAP:ProgramName{prop:FontColor} = 65793
        ?RAP:ProgramName{prop:Color} = 15066597
    Elsif ?RAP:ProgramName{prop:Req} = True
        ?RAP:ProgramName{prop:FontColor} = 65793
        ?RAP:ProgramName{prop:Color} = 8454143
    Else ! If ?RAP:ProgramName{prop:Req} = True
        ?RAP:ProgramName{prop:FontColor} = 65793
        ?RAP:ProgramName{prop:Color} = 16777215
    End ! If ?RAP:ProgramName{prop:Req} = True
    ?RAP:ProgramName{prop:Trn} = 0
    ?RAP:ProgramName{prop:FontStyle} = font:Bold
    ?RAP:EXEPath:Prompt{prop:FontColor} = -1
    ?RAP:EXEPath:Prompt{prop:Color} = 15066597
    If ?RAP:EXEPath{prop:ReadOnly} = True
        ?RAP:EXEPath{prop:FontColor} = 65793
        ?RAP:EXEPath{prop:Color} = 15066597
    Elsif ?RAP:EXEPath{prop:Req} = True
        ?RAP:EXEPath{prop:FontColor} = 65793
        ?RAP:EXEPath{prop:Color} = 8454143
    Else ! If ?RAP:EXEPath{prop:Req} = True
        ?RAP:EXEPath{prop:FontColor} = 65793
        ?RAP:EXEPath{prop:Color} = 16777215
    End ! If ?RAP:EXEPath{prop:Req} = True
    ?RAP:EXEPath{prop:Trn} = 0
    ?RAP:EXEPath{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateRAPIDLST',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateRAPIDLST',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateRAPIDLST',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RAP:ProgramName:Prompt;  SolaceCtrlName = '?RAP:ProgramName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RAP:ProgramName;  SolaceCtrlName = '?RAP:ProgramName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RAP:EXEPath:Prompt;  SolaceCtrlName = '?RAP:EXEPath:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RAP:EXEPath;  SolaceCtrlName = '?RAP:EXEPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEXEPath;  SolaceCtrlName = '?LookupEXEPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a RAPIDLST Record'
  OF ChangeRecord
    ActionMessage = 'Changing a RAPIDLST Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateRAPIDLST')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateRAPIDLST')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RAP:ProgramName:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(RAP:Record,History::RAP:Record)
  SELF.AddHistoryField(?RAP:ProgramName,3)
  SELF.AddHistoryField(?RAP:EXEPath,5)
  SELF.AddUpdateFile(Access:RAPIDLST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:RAPIDLST.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:RAPIDLST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FileLookup8.Init
  FileLookup8.Flags=BOR(FileLookup8.Flags,FILE:LongName)
  FileLookup8.SetMask('EXE Files','*.EXE')
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RAPIDLST.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateRAPIDLST',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupEXEPath
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEXEPath, Accepted)
      RAP:EXEPath = Upper(FileLookup8.Ask(1)  )
      DISPLAY
      Loop x# = Len(Clip(rap:EXEPath)) To 1 By -1
          If Sub(rap:EXEPath,x#,1) = '\'
              rap:EXEPath = Clip(Sub(rap:EXEPath,x# + 1,Len(Clip(rap:EXEPath))))
              Display()
              Break
          End!If Sub(rap:EXEPath,x#,1) = '\'
      End!Loop x# = Len(Clip(rap:EXEPath)) To 1 By -1.
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEXEPath, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateRAPIDLST')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

