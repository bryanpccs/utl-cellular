

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBK01001.INC'),ONCE        !Local module procedure declarations
                     END





Browse_Jobs PROCEDURE                                 !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::26:TAGFLAG         BYTE(0)
DASBRW::26:TAGMOUSE        BYTE(0)
DASBRW::26:TAGDISPSTATUS   BYTE(0)
DASBRW::26:QUEUE          QUEUE
Job_Number_Pointer            LIKE(GLO:Job_Number_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ThisThreadActive BYTE
CurrentTab           STRING(80)
DespatchLocateFlag   BYTE(0)
InWorkshopDate_temp  DATE
unallocated_temp     STRING('INW')
save_sub_id          USHORT,AUTO
save_jot_id          USHORT,AUTO
save_mulj_id         USHORT,AUTO
save_aus_id          USHORT,AUTO
invoice_number_temp  LONG
tmp:accountnumber    STRING(15)
tmp:labelerror       STRING('0 {19}')
tmp:OldConsignNo     STRING(20)
save_job_ali_id      USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
sav:path             STRING(255)
tmp:ConsignNo        STRING(30)
despatch_type_temp   STRING(3)
status2_temp         STRING(30)
rea_temp             STRING('REA')
yes_temp             STRING('YES')
no_temp              STRING('NO')
save_lac_id          USHORT,AUTO
save_xca_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
blank_temp           STRING(1)
count_jobs_temp      REAL
save_xch_id          USHORT,AUTO
select_courier_temp  STRING('ALL')
Select_Trade_Account_Temp STRING('ALL')
Location_Temp        STRING(30)
LocalRequest         LONG
multiple_despatch_temp STRING(3)
FilesOpened          BYTE
Completed_Temp       STRING('ALL')
Completed2_Temp      STRING('ALL')
Invoiced_Temp        STRING(3)
Collected_Temp       STRING(3)
Paid_Temp            STRING(3)
status_temp          STRING(30)
model_unit_temp      STRING(60)
Overdue_Temp         STRING(1)
account_number_temp  STRING(15)
engineer_temp        STRING(3)
address_temp         STRING(90)
model_unit2_temp     STRING(60)
model_unit3_temp     STRING(60)
model_number_temp    STRING(30)
Overdue2_temp        STRING('NO {1}')
batch_number_temp    REAL(1)
courier_temp         STRING(30)
account_number2_temp STRING(15)
tag_temp             STRING(1)
tag_count_temp       REAL
despatch_batch_number_temp REAL
despatch_label_type_temp STRING(3)
despatch_label_number_temp STRING(30)
Warranty_Temp        STRING(1)
model_unit4_temp     STRING(60)
model_unit5_temp     STRING(60)
select_trade_account2_temp STRING('ALL')
account_number3_temp STRING(15)
select_model_number_temp STRING('ALL')
model_number2_temp   STRING(30)
Company_Name_Temp    STRING(30)
Address_Line1_Temp   STRING(30)
Address_Line2_Temp   STRING(30)
Address_Line3_Temp   STRING(30)
Postcode_Temp        STRING(10)
status3_temp         STRING(30)
status4_temp         STRING(30)
tmp:SelectLocation   BYTE(1)
tmp:JobLocation      STRING(30)
tmp:ExchangeText     STRING(40)
tmp:Address          STRING(255)
tmp:Location         STRING(30)
tmp:ModelUnit1       STRING(255)
tmp:ModelUnit2       STRING(255)
tmp:ModelUnit3       STRING(255)
tmp:StatusDate       DATE
tmp:ExchangeStatusDate DATE
tmp:LoanStatusDate   DATE
tmp:LocationStatus   STRING(30)
tmp:PrintDespatchNote BYTE(0)
tmp:FirstRecord      BYTE(0)
tmp:BatchNumber      LONG
tmp:BatchCreated     BYTE(0)
tmp:AccountSearchOrder BYTE(0)
tmp:GlobalIMEINumber STRING(30)
tmp:OrderNumber      STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:date_booked)
                       PROJECT(job:Current_Status)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Surname)
                       PROJECT(job:Mobile_Number)
                       PROJECT(job:Postcode)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Consignment_Number)
                       PROJECT(job:Current_Courier)
                       PROJECT(job:Loan_Status)
                       PROJECT(job:Exchange_Status)
                       PROJECT(job:Location)
                       PROJECT(job:Incoming_Consignment_Number)
                       PROJECT(job:Exchange_Unit_Number)
                       PROJECT(job:Loan_Unit_Number)
                       PROJECT(job:Address_Line1)
                       PROJECT(job:Company_Name)
                       PROJECT(job:Address_Line2)
                       PROJECT(job:Address_Line3)
                       PROJECT(job:Engineer)
                       PROJECT(job:Completed)
                       PROJECT(job:Workshop)
                       PROJECT(job:Batch_Number)
                       PROJECT(job:Despatched)
                       PROJECT(job:Despatch_Number)
                       JOIN(jobe:RefNumberKey,job:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Ref_Number_NormalFG LONG                          !Normal forground color
job:Ref_Number_NormalBG LONG                          !Normal background color
job:Ref_Number_SelectedFG LONG                        !Selected forground color
job:Ref_Number_SelectedBG LONG                        !Selected background color
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Account_Number_NormalFG LONG                      !Normal forground color
job:Account_Number_NormalBG LONG                      !Normal background color
job:Account_Number_SelectedFG LONG                    !Selected forground color
job:Account_Number_SelectedBG LONG                    !Selected background color
model_unit_temp        LIKE(model_unit_temp)          !List box control field - type derived from local data
model_unit_temp_NormalFG LONG                         !Normal forground color
model_unit_temp_NormalBG LONG                         !Normal background color
model_unit_temp_SelectedFG LONG                       !Selected forground color
model_unit_temp_SelectedBG LONG                       !Selected background color
job:date_booked        LIKE(job:date_booked)          !List box control field - type derived from field
job:date_booked_NormalFG LONG                         !Normal forground color
job:date_booked_NormalBG LONG                         !Normal background color
job:date_booked_SelectedFG LONG                       !Selected forground color
job:date_booked_SelectedBG LONG                       !Selected background color
job:Current_Status     LIKE(job:Current_Status)       !List box control field - type derived from field
job:Current_Status_NormalFG LONG                      !Normal forground color
job:Current_Status_NormalBG LONG                      !Normal background color
job:Current_Status_SelectedFG LONG                    !Selected forground color
job:Current_Status_SelectedBG LONG                    !Selected background color
Overdue_Temp           LIKE(Overdue_Temp)             !List box control field - type derived from local data
Overdue_Temp_Icon      LONG                           !Entry's icon ID
Completed_Temp         LIKE(Completed_Temp)           !List box control field - type derived from local data
Completed_Temp_Icon    LONG                           !Entry's icon ID
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
Invoiced_Temp          LIKE(Invoiced_Temp)            !List box control field - type derived from local data
Invoiced_Temp_Icon     LONG                           !Entry's icon ID
Warranty_Temp          LIKE(Warranty_Temp)            !List box control field - type derived from local data
Warranty_Temp_Icon     LONG                           !Entry's icon ID
Collected_Temp         LIKE(Collected_Temp)           !List box control field - type derived from local data
Collected_Temp_Icon    LONG                           !Entry's icon ID
Paid_Temp              LIKE(Paid_Temp)                !List box control field - type derived from local data
Paid_Temp_Icon         LONG                           !Entry's icon ID
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:ESN_NormalFG       LONG                           !Normal forground color
job:ESN_NormalBG       LONG                           !Normal background color
job:ESN_SelectedFG     LONG                           !Selected forground color
job:ESN_SelectedBG     LONG                           !Selected background color
job:MSN                LIKE(job:MSN)                  !List box control field - type derived from field
job:MSN_NormalFG       LONG                           !Normal forground color
job:MSN_NormalBG       LONG                           !Normal background color
job:MSN_SelectedFG     LONG                           !Selected forground color
job:MSN_SelectedBG     LONG                           !Selected background color
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
job:Order_Number_NormalFG LONG                        !Normal forground color
job:Order_Number_NormalBG LONG                        !Normal background color
job:Order_Number_SelectedFG LONG                      !Selected forground color
job:Order_Number_SelectedBG LONG                      !Selected background color
job:Surname            LIKE(job:Surname)              !List box control field - type derived from field
job:Surname_NormalFG   LONG                           !Normal forground color
job:Surname_NormalBG   LONG                           !Normal background color
job:Surname_SelectedFG LONG                           !Selected forground color
job:Surname_SelectedBG LONG                           !Selected background color
job:Mobile_Number      LIKE(job:Mobile_Number)        !List box control field - type derived from field
job:Mobile_Number_NormalFG LONG                       !Normal forground color
job:Mobile_Number_NormalBG LONG                       !Normal background color
job:Mobile_Number_SelectedFG LONG                     !Selected forground color
job:Mobile_Number_SelectedBG LONG                     !Selected background color
job:Postcode           LIKE(job:Postcode)             !List box control field - type derived from field
job:Postcode_NormalFG  LONG                           !Normal forground color
job:Postcode_NormalBG  LONG                           !Normal background color
job:Postcode_SelectedFG LONG                          !Selected forground color
job:Postcode_SelectedBG LONG                          !Selected background color
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Model_Number_NormalFG LONG                        !Normal forground color
job:Model_Number_NormalBG LONG                        !Normal background color
job:Model_Number_SelectedFG LONG                      !Selected forground color
job:Model_Number_SelectedBG LONG                      !Selected background color
job:Unit_Type          LIKE(job:Unit_Type)            !List box control field - type derived from field
job:Unit_Type_NormalFG LONG                           !Normal forground color
job:Unit_Type_NormalBG LONG                           !Normal background color
job:Unit_Type_SelectedFG LONG                         !Selected forground color
job:Unit_Type_SelectedBG LONG                         !Selected background color
address_temp           LIKE(address_temp)             !List box control field - type derived from local data
address_temp_NormalFG  LONG                           !Normal forground color
address_temp_NormalBG  LONG                           !Normal background color
address_temp_SelectedFG LONG                          !Selected forground color
address_temp_SelectedBG LONG                          !Selected background color
model_unit2_temp       LIKE(model_unit2_temp)         !List box control field - type derived from local data
model_unit2_temp_NormalFG LONG                        !Normal forground color
model_unit2_temp_NormalBG LONG                        !Normal background color
model_unit2_temp_SelectedFG LONG                      !Selected forground color
model_unit2_temp_SelectedBG LONG                      !Selected background color
model_unit3_temp       LIKE(model_unit3_temp)         !List box control field - type derived from local data
model_unit3_temp_NormalFG LONG                        !Normal forground color
model_unit3_temp_NormalBG LONG                        !Normal background color
model_unit3_temp_SelectedFG LONG                      !Selected forground color
model_unit3_temp_SelectedBG LONG                      !Selected background color
model_unit4_temp       LIKE(model_unit4_temp)         !List box control field - type derived from local data
model_unit4_temp_NormalFG LONG                        !Normal forground color
model_unit4_temp_NormalBG LONG                        !Normal background color
model_unit4_temp_SelectedFG LONG                      !Selected forground color
model_unit4_temp_SelectedBG LONG                      !Selected background color
model_unit5_temp       LIKE(model_unit5_temp)         !List box control field - type derived from local data
model_unit5_temp_NormalFG LONG                        !Normal forground color
model_unit5_temp_NormalBG LONG                        !Normal background color
model_unit5_temp_SelectedFG LONG                      !Selected forground color
model_unit5_temp_SelectedBG LONG                      !Selected background color
job:Consignment_Number LIKE(job:Consignment_Number)   !List box control field - type derived from field
job:Consignment_Number_NormalFG LONG                  !Normal forground color
job:Consignment_Number_NormalBG LONG                  !Normal background color
job:Consignment_Number_SelectedFG LONG                !Selected forground color
job:Consignment_Number_SelectedBG LONG                !Selected background color
job:Current_Courier    LIKE(job:Current_Courier)      !List box control field - type derived from field
job:Current_Courier_NormalFG LONG                     !Normal forground color
job:Current_Courier_NormalBG LONG                     !Normal background color
job:Current_Courier_SelectedFG LONG                   !Selected forground color
job:Current_Courier_SelectedBG LONG                   !Selected background color
despatch_type_temp     LIKE(despatch_type_temp)       !List box control field - type derived from local data
despatch_type_temp_NormalFG LONG                      !Normal forground color
despatch_type_temp_NormalBG LONG                      !Normal background color
despatch_type_temp_SelectedFG LONG                    !Selected forground color
despatch_type_temp_SelectedBG LONG                    !Selected background color
status2_temp           LIKE(status2_temp)             !List box control field - type derived from local data
status2_temp_NormalFG  LONG                           !Normal forground color
status2_temp_NormalBG  LONG                           !Normal background color
status2_temp_SelectedFG LONG                          !Selected forground color
status2_temp_SelectedBG LONG                          !Selected background color
job:Loan_Status        LIKE(job:Loan_Status)          !List box control field - type derived from field
job:Loan_Status_NormalFG LONG                         !Normal forground color
job:Loan_Status_NormalBG LONG                         !Normal background color
job:Loan_Status_SelectedFG LONG                       !Selected forground color
job:Loan_Status_SelectedBG LONG                       !Selected background color
job:Exchange_Status    LIKE(job:Exchange_Status)      !List box control field - type derived from field
job:Exchange_Status_NormalFG LONG                     !Normal forground color
job:Exchange_Status_NormalBG LONG                     !Normal background color
job:Exchange_Status_SelectedFG LONG                   !Selected forground color
job:Exchange_Status_SelectedBG LONG                   !Selected background color
job:Location           LIKE(job:Location)             !List box control field - type derived from field
job:Location_NormalFG  LONG                           !Normal forground color
job:Location_NormalBG  LONG                           !Normal background color
job:Location_SelectedFG LONG                          !Selected forground color
job:Location_SelectedBG LONG                          !Selected background color
job:Incoming_Consignment_Number LIKE(job:Incoming_Consignment_Number) !List box control field - type derived from field
job:Incoming_Consignment_Number_NormalFG LONG         !Normal forground color
job:Incoming_Consignment_Number_NormalBG LONG         !Normal background color
job:Incoming_Consignment_Number_SelectedFG LONG       !Selected forground color
job:Incoming_Consignment_Number_SelectedBG LONG       !Selected background color
job:Exchange_Unit_Number LIKE(job:Exchange_Unit_Number) !List box control field - type derived from field
job:Exchange_Unit_Number_NormalFG LONG                !Normal forground color
job:Exchange_Unit_Number_NormalBG LONG                !Normal background color
job:Exchange_Unit_Number_SelectedFG LONG              !Selected forground color
job:Exchange_Unit_Number_SelectedBG LONG              !Selected background color
job:Loan_Unit_Number   LIKE(job:Loan_Unit_Number)     !List box control field - type derived from field
job:Loan_Unit_Number_NormalFG LONG                    !Normal forground color
job:Loan_Unit_Number_NormalBG LONG                    !Normal background color
job:Loan_Unit_Number_SelectedFG LONG                  !Selected forground color
job:Loan_Unit_Number_SelectedBG LONG                  !Selected background color
tmp:ExchangeText       LIKE(tmp:ExchangeText)         !List box control field - type derived from local data
tmp:ExchangeText_NormalFG LONG                        !Normal forground color
tmp:ExchangeText_NormalBG LONG                        !Normal background color
tmp:ExchangeText_SelectedFG LONG                      !Selected forground color
tmp:ExchangeText_SelectedBG LONG                      !Selected background color
tmp:Address            LIKE(tmp:Address)              !List box control field - type derived from local data
tmp:Address_NormalFG   LONG                           !Normal forground color
tmp:Address_NormalBG   LONG                           !Normal background color
tmp:Address_SelectedFG LONG                           !Selected forground color
tmp:Address_SelectedBG LONG                           !Selected background color
tmp:Location           LIKE(tmp:Location)             !List box control field - type derived from local data
tmp:Location_NormalFG  LONG                           !Normal forground color
tmp:Location_NormalBG  LONG                           !Normal background color
tmp:Location_SelectedFG LONG                          !Selected forground color
tmp:Location_SelectedBG LONG                          !Selected background color
tmp:StatusDate         LIKE(tmp:StatusDate)           !List box control field - type derived from local data
tmp:StatusDate_NormalFG LONG                          !Normal forground color
tmp:StatusDate_NormalBG LONG                          !Normal background color
tmp:StatusDate_SelectedFG LONG                        !Selected forground color
tmp:StatusDate_SelectedBG LONG                        !Selected background color
tmp:ModelUnit1         LIKE(tmp:ModelUnit1)           !List box control field - type derived from local data
tmp:ModelUnit1_NormalFG LONG                          !Normal forground color
tmp:ModelUnit1_NormalBG LONG                          !Normal background color
tmp:ModelUnit1_SelectedFG LONG                        !Selected forground color
tmp:ModelUnit1_SelectedBG LONG                        !Selected background color
tmp:ModelUnit2         LIKE(tmp:ModelUnit2)           !List box control field - type derived from local data
tmp:ModelUnit2_NormalFG LONG                          !Normal forground color
tmp:ModelUnit2_NormalBG LONG                          !Normal background color
tmp:ModelUnit2_SelectedFG LONG                        !Selected forground color
tmp:ModelUnit2_SelectedBG LONG                        !Selected background color
tmp:ModelUnit3         LIKE(tmp:ModelUnit3)           !List box control field - type derived from local data
tmp:ModelUnit3_NormalFG LONG                          !Normal forground color
tmp:ModelUnit3_NormalBG LONG                          !Normal background color
tmp:ModelUnit3_SelectedFG LONG                        !Selected forground color
tmp:ModelUnit3_SelectedBG LONG                        !Selected background color
tmp:ExchangeStatusDate LIKE(tmp:ExchangeStatusDate)   !List box control field - type derived from local data
tmp:ExchangeStatusDate_NormalFG LONG                  !Normal forground color
tmp:ExchangeStatusDate_NormalBG LONG                  !Normal background color
tmp:ExchangeStatusDate_SelectedFG LONG                !Selected forground color
tmp:ExchangeStatusDate_SelectedBG LONG                !Selected background color
tmp:LoanStatusDate     LIKE(tmp:LoanStatusDate)       !List box control field - type derived from local data
tmp:LoanStatusDate_NormalFG LONG                      !Normal forground color
tmp:LoanStatusDate_NormalBG LONG                      !Normal background color
tmp:LoanStatusDate_SelectedFG LONG                    !Selected forground color
tmp:LoanStatusDate_SelectedBG LONG                    !Selected background color
tmp:LocationStatus     LIKE(tmp:LocationStatus)       !List box control field - type derived from local data
tmp:LocationStatus_NormalFG LONG                      !Normal forground color
tmp:LocationStatus_NormalBG LONG                      !Normal background color
tmp:LocationStatus_SelectedFG LONG                    !Selected forground color
tmp:LocationStatus_SelectedBG LONG                    !Selected background color
job:Address_Line1      LIKE(job:Address_Line1)        !List box control field - type derived from field
job:Address_Line1_NormalFG LONG                       !Normal forground color
job:Address_Line1_NormalBG LONG                       !Normal background color
job:Address_Line1_SelectedFG LONG                     !Selected forground color
job:Address_Line1_SelectedBG LONG                     !Selected background color
job:Company_Name       LIKE(job:Company_Name)         !Browse hot field - type derived from field
job:Address_Line2      LIKE(job:Address_Line2)        !Browse hot field - type derived from field
job:Address_Line3      LIKE(job:Address_Line3)        !Browse hot field - type derived from field
Postcode_Temp          LIKE(Postcode_Temp)            !Browse hot field - type derived from local data
Address_Line3_Temp     LIKE(Address_Line3_Temp)       !Browse hot field - type derived from local data
Address_Line2_Temp     LIKE(Address_Line2_Temp)       !Browse hot field - type derived from local data
Address_Line1_Temp     LIKE(Address_Line1_Temp)       !Browse hot field - type derived from local data
Company_Name_Temp      LIKE(Company_Name_Temp)        !Browse hot field - type derived from local data
job:Engineer           LIKE(job:Engineer)             !Browse key field - type derived from field
job:Completed          LIKE(job:Completed)            !Browse key field - type derived from field
job:Workshop           LIKE(job:Workshop)             !Browse key field - type derived from field
job:Batch_Number       LIKE(job:Batch_Number)         !Browse key field - type derived from field
job:Despatched         LIKE(job:Despatched)           !Browse key field - type derived from field
job:Despatch_Number    LIKE(job:Despatch_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,48)                !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the Jobs File'),AT(,,648,331),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Jobs'),ALRT(F4Key),ALRT(F5Key),ALRT(F8Key),GRAY,DOUBLE
                       MENUBAR,NOMERGE
                         MENU('Print Routines'),USE(?PrintRoutines),ICON('Printsm.gif')
                           ITEM('Job Receipt'),USE(?JobReceipt)
                           ITEM('Job Card'),USE(?JobCard)
                           ITEM('Job Label'),USE(?JobLabel)
                           ITEM('Virgin Label'),USE(?VirginLabel)
                           ITEM,SEPARATOR
                           ITEM('Delivery Label'),USE(?DeliveryLabel)
                           ITEM('Collection Label'),USE(?CollectionLabel)
                           ITEM,SEPARATOR
                           ITEM('Letters'),USE(?Letters)
                           ITEM('Mail Merge'),USE(?MailMerge)
                           ITEM,SEPARATOR
                           ITEM('ANC Collection Note'),USE(?ANCCollectionNote)
                           ITEM('Despatch Note'),USE(?DespatchNote)
                           ITEM,SEPARATOR
                           ITEM('Exchange Unit Label'),USE(?Menu1ExchangeUnitLabel)
                           ITEM('Loan Unit Label'),USE(?Menu1LoanUnitLabel)
                           ITEM,SEPARATOR
                           ITEM('Estimate'),USE(?Menu1Estimate)
                           ITEM('Invoice'),USE(?Menu1Invoice)
                         END
                       END
                       STRING(@d6b),AT(505,285),USE(tmp:LoanStatusDate),RIGHT,FONT('Tahoma',7,COLOR:White,FONT:bold,CHARSET:ANSI)
                       STRING(@d6b),AT(329,285),USE(tmp:ExchangeStatusDate),RIGHT,FONT('Tahoma',7,COLOR:White,FONT:bold,CHARSET:ANSI)
                       STRING(@d6b),AT(153,285),USE(tmp:StatusDate),RIGHT,FONT('Tahoma',7,COLOR:White,FONT:bold,CHARSET:ANSI)
                       LIST,AT(76,69,476,208),USE(?Browse:1),IMM,MSG('Browsing Records'),TIP('Right Click For Print Options'),FORMAT('34R(2)|FM*~Job No~@s7@60L(2)|FM*~Account No~@s15@147L(2)|FM*~Model Number / Unit' &|
   ' Type~@s60@42R(2)|FM*~Booked~@d6b@128L(2)|FM*~Status~@s30@10CFI~O~@s1@10CFI~F~@s' &|
   '1@11L(2)FI@s1@10CFI~C~@s1@10CFI~W~@s1@10CFI~D~@s1@10C|FI~P~@s1@68L(2)|FM*~ESN/IM' &|
   'EI~@s16@68L(2)|FM*~MSN~@s15@96L(2)|FM*~Order Number~@s30@60L(2)|FM*~Surname~@s30' &|
   '@60L(2)|FM*~Mobile Number~@s15@46L(2)|FM*~Postcode~@s10@60L(2)|FM*~Model Number~' &|
   '@s30@60L(2)|FM*~Unit Type~@s30@220L(2)|FM*~Address~@s90@111L(2)|FM*~Model Number' &|
   ' / Unit Type~@s60@85L(2)|FM*~Model Number / Unit Type~@s60@93L(2)|FM*~Model Numb' &|
   'er / Unit Type~@s60@124L(2)|FM*~Model Number / Unit Type~@s60@60L(2)|FM*~Consign' &|
   'ment No~@s30@80L(2)|FM*~Courier~@s30@34L(2)|FM*~Type~@s3@124L(2)|FM*~Status~@s30' &|
   '@92L(2)|FM*~Loan Status~@s30@92L(2)|FM*~Exchange Status~@s30@60L(2)|FM*~Location' &|
   '~@s30@60L(2)|FM*~Consignment No~@s30@32L(2)|FM*~Exchange Unit Number~@s8@32L(2)|' &|
   'FM*~Loan Unit Number~@s8@120L(2)|FM*~tmp : Exchange Text~@s30@1020L(2)|FM*~Entry' &|
   ' 255~@s255@120L(2)|FM*~Location~@s30@36R(2)|FM*~Changed~@d6@88L(2)|FM*~Model Num' &|
   'ber / Unit Type~@s255@93L(2)|FM*~Model Number / Unit Type~@s255@75L(2)|FM*~Model' &|
   ' Number / Unit Type~@s255@36L(2)|FM*~Changed~R@d6@36L(2)|FM*~Changed~R@d6@120L(2' &|
   ')|FM*~Location~R@s30@128L(2)|FM*~Location~@s30@'),FROM(Queue:Browse:1)
                       STRING(@s40),AT(76,8,196,12),USE(tmp:ExchangeText),TRN,LEFT,FONT(,10,COLOR:Red,FONT:bold)
                       TEXT,AT(392,8,160,60),USE(tmp:Address),SKIP,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),READONLY
                       GROUP,AT(564,0,84,121),USE(?Button_Group)
                         BUTTON('Print Routines'),AT(568,4,76,20),USE(?Print_Routines),LEFT,ICON(ICON:Print1)
                         BUTTON('Change Status'),AT(568,28,76,20),USE(?ChangeStatus),LEFT,ICON('Capture2.ico')
                         BUTTON('Payment Details'),AT(568,52,76,20),USE(?Payment_Details),LEFT,ICON('Invoice2.ico')
                         BUTTON('Cancel Job'),AT(568,76,76,20),USE(?CancelJob),LEFT,ICON('xtool.gif')
                         BUTTON('Failed Delivery'),AT(568,101,76,20),USE(?FailedDelivery),LEFT,ICON('no_deliv.gif')
                       END
                       BUTTON('&Create New Job'),AT(568,233,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Amend Job'),AT(568,256,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO'),DEFAULT
                       BUTTON('&Remove Job'),AT(568,280,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       BUTTON('Key'),AT(352,17,36,12),USE(?KeyColour),LEFT,ICON('key.ico')
                       SHEET,AT(4,4,556,324),USE(?CurrentTab),LEFT,SPREAD
                         TAB('By Job Number'),USE(?Job_Number_Tab)
                           ENTRY(@s8b),AT(76,57,60,10),USE(job:Ref_Number),RIGHT,FONT('Tahoma',8,,FONT:bold)
                           PROMPT('Job Number'),AT(140,57),USE(?Prompt7),FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('In Workshop Date'),AT(77,310),USE(?InWorkshopDatePrompt),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs),LEFT,ICON('Calc1.ico')
                           STRING(@d6b),AT(140,310),USE(InWorkshopDate_temp),FONT(,,COLOR:White,,CHARSET:ANSI)
                         END
                         TAB('By I.M.E.I. No'),USE(?Esn_tab)
                           PROMPT('Global I.M.E.I. Number Search'),AT(272,49),USE(?tmp:GlobalIMEINumber:Prompt),TRN,LEFT,FONT('Tahoma',7,COLOR:White,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(272,57,116,10),USE(tmp:GlobalIMEINumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Global I.M.E.I. Number'),TIP('Global I.M.E.I. Number'),UPR
                           ENTRY(@s20),AT(76,57,124,10),USE(job:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('I.M.E.I. Number'),AT(204,57),USE(?Prompt8),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:2),LEFT,ICON('Calc1.ico')
                         END
                         TAB('By M.S.N.'),USE(?msn_tab)
                           ENTRY(@s30),AT(76,56,124,10),USE(job:MSN),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('M.S.N.'),AT(208,57),USE(?Prompt9),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:3),LEFT,ICON('Calc1.ico')
                         END
                         TAB('By Account No'),USE(?Account_No_Tab)
                           OPTION('Search Order'),AT(296,33,92,32),USE(tmp:AccountSearchOrder),BOXED
                             RADIO('By Order Number'),AT(304,41),USE(?tmp:AccountSearchOrder:Radio1),VALUE('0')
                             RADIO('By Job Number'),AT(304,53),USE(?tmp:AccountSearchOrder:Radio2),VALUE('1')
                           END
                           ENTRY(@s30),AT(76,25,124,10),USE(tmp:OrderNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Order Number'),TIP('Order Number'),UPR
                           ENTRY(@s15),AT(76,41,124,10),USE(account_number_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(204,41,10,10),USE(?Lookup_Account_number),SKIP,ICON('list3.ico')
                           PROMPT('Account Number'),AT(224,41),USE(?Prompt4),FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Global Order Number Search'),AT(76,17),USE(?Prompt4:2),FONT(,7,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s30),AT(76,57,96,10),USE(job:Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(172,57,64,10),USE(job:Ref_Number,,?job:Ref_Number:10),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Order Number'),AT(176,57),USE(?OrderNumber),FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(240,57),USE(?JobNumber),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:4),LEFT,ICON('Calc1.ico')
                         END
                         TAB('By Surname'),USE(?surname_tab)
                           ENTRY(@s30),AT(76,57,124,10),USE(job:Surname),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Surname'),AT(204,57),USE(?Prompt11),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Search By Address [F8]'),AT(272,57,108,12),USE(?SearchByAddress),LEFT,ICON('spysm.gif')
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:5),LEFT,ICON('Calc1.ico')
                         END
                         TAB('Job Status'),USE(?status_type_tab)
                           OPTION('Select Location'),AT(76,17,240,20),USE(tmp:SelectLocation),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                             RADIO('All'),AT(84,25),USE(?tmp:SelectLocation:Radio1),VALUE('1')
                             RADIO('Individual'),AT(120,25),USE(?tmp:SelectLocation:Radio2),VALUE('2')
                           END
                           ENTRY(@s30),AT(172,25,124,10),USE(tmp:JobLocation),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(300,25,10,10),USE(?Lookup_Location),SKIP,HIDE,ICON('list3.ico')
                           ENTRY(@s30),AT(76,44,124,10),USE(status_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),UPR
                           BUTTON,AT(204,45,10,10),USE(?Lookup_Job_Status),SKIP,ICON('list3.ico')
                           PROMPT('Status Type'),AT(76,37),USE(?Prompt5),FONT(,7,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(144,57),USE(?Prompt5:2),FONT(,7,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s8b),AT(76,57,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:2),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:6),LEFT,ICON('Calc1.ico')
                         END
                         TAB('Exchange Status'),USE(?Exchange_Status)
                           ENTRY(@s30),AT(76,45,124,10),USE(status3_temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(204,44,10,10),USE(?Lookup_Status3),SKIP,ICON('list3.ico')
                           PROMPT('Exchange Status'),AT(76,33),USE(?Prompt25),FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(144,57),USE(?Prompt25:2),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:15),LEFT,ICON('Calc1.ico')
                           ENTRY(@s8b),AT(76,57,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:7),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('Loan Status'),USE(?Loan_Status)
                           ENTRY(@s30),AT(76,44,124,10),USE(status4_temp),FONT(,,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey)
                           BUTTON,AT(204,44,10,10),USE(?Lookup_Status4),SKIP,ICON('list3.ico')
                           PROMPT('Loan Status'),AT(76,33),USE(?JOB:Ref_Number:Prompt:3),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           PROMPT('Job Number'),AT(144,57),USE(?JOB:Ref_Number:Prompt:2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:8),LEFT,ICON('Calc1.ico')
                           ENTRY(@s8b),AT(76,57,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:8),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By Mobile No.'),USE(?mobile_tab)
                           ENTRY(@s15),AT(76,57,64,10),USE(job:Mobile_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Mobile Number'),AT(144,57),USE(?Prompt13),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:7),LEFT,ICON('Calc1.ico')
                         END
                         TAB('By Model No'),USE(?Model_No_Tab)
                           ENTRY(@s30),AT(76,44,124,10),USE(model_number_temp),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(204,44,10,10),USE(?Lookup_Model_Number),SKIP,ICON('list3.ico')
                           PROMPT('Model Number'),AT(76,33),USE(?Prompt6),FONT(,,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s30),AT(76,57,124,10),USE(job:Unit_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Unit Type'),AT(204,57),USE(?Prompt15),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:9),LEFT,ICON('Calc1.ico')
                         END
                         TAB('By Engineer'),USE(?Engineer_Tab)
                           ENTRY(@s3),AT(76,44,24,10),USE(engineer_temp),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON,AT(104,44,10,10),USE(?Lookup_Engineer),SKIP,ICON('list3.ico')
                           PROMPT('Engineer'),AT(120,44),USE(?Prompt16),FONT(,,COLOR:White,,CHARSET:ANSI)
                           OPTION('Job Type'),AT(204,40,160,28),USE(Completed2_Temp,,?Completed2_Temp:2),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                             RADIO('Incomplete'),AT(212,52),USE(?Completed_Temp:Radio1),VALUE('NO')
                             RADIO('All'),AT(332,52),USE(?Completed_Temp:Radio3),VALUE('ALL')
                             RADIO('Completed'),AT(272,52),USE(?Completed_Temp:Radio2),VALUE('YES')
                           END
                           ENTRY(@s8b),AT(76,57,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:3),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Job Number'),AT(144,57),USE(?Prompt17),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:10),LEFT,ICON('Calc1.ico')
                         END
                         TAB('Unallocated Jobs'),USE(?Unallocated_Jobs_Tab)
                           ENTRY(@s8b),AT(76,53,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:4),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Job Number'),AT(144,53),USE(?Prompt18),FONT(,,COLOR:White,,CHARSET:ANSI)
                           OPTION('Job Type'),AT(192,37,152,28),USE(unallocated_temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                             RADIO('In Workshop'),AT(200,49),USE(?unallocated_temp:Radio1),VALUE('INW')
                             RADIO('Not In Workshop'),AT(268,49),USE(?unallocated_temp:Radio2),VALUE('NIW')
                           END
                           BUTTON('Unallocated Jobs Report'),AT(424,308,68,16),USE(?UnallocatedJobsReport),LEFT,ICON(ICON:Print1)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:11),LEFT,ICON('Calc1.ico')
                         END
                         TAB('In Consign No'),USE(?Tab16)
                           ENTRY(@s30),AT(76,57,124,10),USE(job:Incoming_Consignment_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Consignment Number'),AT(204,57),USE(?Prompt30),FONT(,,COLOR:White,,CHARSET:ANSI)
                         END
                         TAB('Out Consign No'),USE(?Consignment_No_Tab)
                           PROMPT('This Browse is for the Job Consignment Number Only'),AT(76,40),USE(?Prompt24),FONT(,,COLOR:Yellow,FONT:bold,CHARSET:ANSI)
                           PROMPT('Consignment Number'),AT(204,57),USE(?JOB:Consignment_Number:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:12),LEFT,ICON('Calc1.ico')
                           ENTRY(@s30),AT(76,57,124,10),USE(job:Consignment_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By Batch No'),USE(?Batch_Number_Tab)
                           ENTRY(@p<<<<<<<<#pb),AT(76,44,63,11),USE(batch_number_temp)
                           PROMPT('Batch Number'),AT(144,44),USE(?Prompt21),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           OPTION('Job Type'),AT(204,40,160,28),USE(Completed2_Temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                             RADIO('Incomplete'),AT(212,52),USE(?Completed2_Temp:Radio1),VALUE('NO')
                             RADIO('Completed'),AT(268,52),USE(?Completed2_Temp:Radio2),VALUE('YES')
                             RADIO('All'),AT(328,52),USE(?Completed2_Temp:Radio3),VALUE('ALL')
                           END
                           PROMPT('Job Number'),AT(144,57),USE(?JOB:Ref_Number:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('Despatch Batch'),AT(568,101,76,20),USE(?Despatch_Batch),LEFT,ICON('desp_mul.gif')
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:13),LEFT,ICON('Calc1.ico')
                           ENTRY(@s8b),AT(76,57,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:5),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('Despatch'),USE(?Ready_To_Despatch)
                           OPTION('Select Courier'),AT(76,16,232,20),USE(select_courier_temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI)
                             RADIO('All'),AT(84,24),USE(?select_courier_temp:Radio1),VALUE('ALL')
                             RADIO('Individual'),AT(108,24),USE(?select_courier_temp:Radio2),VALUE('IND')
                           END
                           ENTRY(@s30),AT(164,24,124,10),USE(courier_temp),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(292,24,10,10),USE(?Lookup_Courier),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Job Number:'),AT(316,33),USE(?Prompt28),FONT(,,COLOR:White,,CHARSET:ANSI)
                           OPTION('Select Trade Account'),AT(76,36,232,20),USE(Select_Trade_Account_Temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI)
                             RADIO('All'),AT(84,44),USE(?Select_Trade_Account_Temp:Radio1),VALUE('ALL')
                             RADIO('Individual'),AT(108,44),USE(?Select_Trade_Account_Temp:Radio2),VALUE('IND')
                           END
                           ENTRY(@s15),AT(164,44,124,10),USE(account_number2_temp),HIDE,FONT(,8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(292,44,10,10),USE(?Lookup_Account_Number:2),SKIP,HIDE,ICON('list3.ico')
                           STRING(@s8),AT(316,41),USE(job:Ref_Number,,?job:Ref_Number:9),TRN,LEFT,FONT(,18,COLOR:White,FONT:bold)
                           GROUP,AT(564,129,84,100),USE(?Button_Group2)
                             BUTTON('&Multiple Despatch'),AT(568,133,76,20),USE(?Multiple_Despatch2),LEFT,ICON('desp_mul.gif')
                             BUTTON('I&ndividual Despatch'),AT(568,157,76,20),USE(?Individual_Despatch),LEFT,ICON('despatch.gif')
                             BUTTON('Validate ESN / Acc. (F4)'),AT(568,180,76,20),USE(?Validate_Esn),LEFT,ICON(ICON:Help)
                             BUTTON('Change Courier'),AT(568,204,76,20),USE(?Change_Courier),LEFT,ICON('courier.gif')
                           END
                           BUTTON('sho&W tags'),AT(8,264,56,16),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Tag'),AT(76,308,56,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('Tag &All'),AT(136,308,56,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           ENTRY(@s8b),AT(76,57,64,10),USE(job:Ref_Number,,?JOB:Ref_Number:6),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Job Number'),AT(144,57),USE(?Prompt23),FONT(,,COLOR:White,,CHARSET:ANSI)
                           BUTTON('&Untag All'),AT(196,308,56,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(8,284,56,16),USE(?DASREVTAG),HIDE
                           BUTTON('Count Jobs'),AT(496,308,56,16),USE(?Count_Jobs:14),LEFT,ICON('Calc1.ico')
                         END
                       END
                       PROMPT('Address'),AT(356,8),USE(?Prompt27)
                       PROMPT('Job Status'),AT(76,284),USE(?Prompt26),FONT(,7,,)
                       PROMPT('Exchange Status'),AT(252,284),USE(?Prompt26:2),FONT(,7,,)
                       PROMPT('Loan Status'),AT(428,284),USE(?Prompt26:3),FONT(,7,,)
                       ENTRY(@s30),AT(252,292,124,10),USE(job:Exchange_Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                       ENTRY(@s30),AT(428,292,124,10),USE(job:Loan_Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                       BUTTON('Mark Line'),AT(8,308,56,16),USE(?MarkLine),LEFT,ICON('bluetick.ico')
                       ENTRY(@s30),AT(76,292,124,10),USE(job:Current_Status),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                       BUTTON('Close'),AT(568,308,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 3
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 0
BRW1::Sort27:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 1
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 5
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 6 And Upper(tmp:SelectLocation) = 1
BRW1::Sort25:Locator EntryLocatorClass                !Conditional Locator - CHOICE(?CurrentTab) = 6 And Upper(tmp:SelectLocation) = 2
BRW1::Sort23:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 7
BRW1::Sort24:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 8
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 9
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 10
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'YES'
BRW1::Sort13:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'NO'
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'ALL'
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 12 And Upper(unallocated_temp) = 'INW'
BRW1::Sort28:Locator StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 12 And Upper(unallocated_temp) = 'NIW'
BRW1::Sort26:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 13
BRW1::Sort11:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 14
BRW1::Sort12:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 15 And Upper(Completed2_Temp) = 'ALL'
BRW1::Sort15:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 15 And Upper(Completed2_Temp) = 'NO'
BRW1::Sort16:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 15 And Upper(Completed2_Temp) = 'YES'
BRW1::Sort17:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'ALL' And Upper(Select_trade_account_temp) = 'ALL'
BRW1::Sort18:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'ALL' And Upper(select_trade_account_temp) = 'IND'
BRW1::Sort19:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'ALL'
BRW1::Sort20:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'IND'
BRW1::Sort21:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 16
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort1:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 2
BRW1::Sort2:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 3
BRW1::Sort3:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 0
BRW1::Sort4:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 5
BRW1::Sort5:StepClass StepClass                       !Conditional Step Manager - CHOICE(?CurrentTab) = 6 And Upper(tmp:SelectLocation) = 1
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
WindowsDir      LPSTR(144)
SystemDir       WORD
save_jpt_ali_id        ushort,auto
save_job_id   ushort,auto
TempFilePath         CSTRING(255)

! Start Change 2618 BE(06/08/03)
!
! include definition here rather than in CLARION Data section
! in order to exclude from Solace View which appears unable
! to cope with a GROUP ARRAY data item.
!
JobStatusColours     GROUP,PRE(JSC),DIM(8)
Colour               LONG
Status               STRING(30)
                     END
! Start Change 2618 BE(06/08/03)

    Map
LocalValidateAccessories    Procedure(),Byte
LocalValidateIMEI           Procedure(),Byte
LocalValidateAccountNumber  Procedure(),Byte
LocalFailedDeliveryAudit    Procedure(String    func:Type)
    End
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!Save Entry Fields Incase Of Lookup
look:account_number_temp                Like(account_number_temp)
look:tmp:JobLocation                Like(tmp:JobLocation)
look:status_temp                Like(status_temp)
look:status3_temp                Like(status3_temp)
look:status4_temp                Like(status4_temp)
look:model_number_temp                Like(model_number_temp)
look:courier_temp                Like(courier_temp)
look:account_number2_temp                Like(account_number2_temp)
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
DBH33:PopupText String(1000)
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?tmp:LoanStatusDate{prop:FontColor} = -1
    ?tmp:LoanStatusDate{prop:Color} = 15066597
    ?tmp:ExchangeStatusDate{prop:FontColor} = -1
    ?tmp:ExchangeStatusDate{prop:Color} = 15066597
    ?tmp:StatusDate{prop:FontColor} = -1
    ?tmp:StatusDate{prop:Color} = 15066597
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?tmp:ExchangeText{prop:FontColor} = -1
    ?tmp:ExchangeText{prop:Color} = 15066597
    If ?tmp:Address{prop:ReadOnly} = True
        ?tmp:Address{prop:FontColor} = 65793
        ?tmp:Address{prop:Color} = 15066597
    Elsif ?tmp:Address{prop:Req} = True
        ?tmp:Address{prop:FontColor} = 65793
        ?tmp:Address{prop:Color} = 8454143
    Else ! If ?tmp:Address{prop:Req} = True
        ?tmp:Address{prop:FontColor} = 65793
        ?tmp:Address{prop:Color} = 16777215
    End ! If ?tmp:Address{prop:Req} = True
    ?tmp:Address{prop:Trn} = 0
    ?tmp:Address{prop:FontStyle} = font:Bold
    ?Button_Group{prop:Font,3} = -1
    ?Button_Group{prop:Color} = 15066597
    ?Button_Group{prop:Trn} = 0
    ?CurrentTab{prop:Color} = 15066597
    ?Job_Number_Tab{prop:Color} = 15066597
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    ?InWorkshopDatePrompt{prop:FontColor} = -1
    ?InWorkshopDatePrompt{prop:Color} = 15066597
    ?InWorkshopDate_temp{prop:FontColor} = -1
    ?InWorkshopDate_temp{prop:Color} = 15066597
    ?Esn_tab{prop:Color} = 15066597
    ?tmp:GlobalIMEINumber:Prompt{prop:FontColor} = -1
    ?tmp:GlobalIMEINumber:Prompt{prop:Color} = 15066597
    If ?tmp:GlobalIMEINumber{prop:ReadOnly} = True
        ?tmp:GlobalIMEINumber{prop:FontColor} = 65793
        ?tmp:GlobalIMEINumber{prop:Color} = 15066597
    Elsif ?tmp:GlobalIMEINumber{prop:Req} = True
        ?tmp:GlobalIMEINumber{prop:FontColor} = 65793
        ?tmp:GlobalIMEINumber{prop:Color} = 8454143
    Else ! If ?tmp:GlobalIMEINumber{prop:Req} = True
        ?tmp:GlobalIMEINumber{prop:FontColor} = 65793
        ?tmp:GlobalIMEINumber{prop:Color} = 16777215
    End ! If ?tmp:GlobalIMEINumber{prop:Req} = True
    ?tmp:GlobalIMEINumber{prop:Trn} = 0
    ?tmp:GlobalIMEINumber{prop:FontStyle} = font:Bold
    If ?job:ESN{prop:ReadOnly} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 15066597
    Elsif ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 8454143
    Else ! If ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 16777215
    End ! If ?job:ESN{prop:Req} = True
    ?job:ESN{prop:Trn} = 0
    ?job:ESN{prop:FontStyle} = font:Bold
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?msn_tab{prop:Color} = 15066597
    If ?job:MSN{prop:ReadOnly} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 15066597
    Elsif ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 8454143
    Else ! If ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 16777215
    End ! If ?job:MSN{prop:Req} = True
    ?job:MSN{prop:Trn} = 0
    ?job:MSN{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?Account_No_Tab{prop:Color} = 15066597
    ?tmp:AccountSearchOrder{prop:Font,3} = -1
    ?tmp:AccountSearchOrder{prop:Color} = 15066597
    ?tmp:AccountSearchOrder{prop:Trn} = 0
    ?tmp:AccountSearchOrder:Radio1{prop:Font,3} = -1
    ?tmp:AccountSearchOrder:Radio1{prop:Color} = 15066597
    ?tmp:AccountSearchOrder:Radio1{prop:Trn} = 0
    ?tmp:AccountSearchOrder:Radio2{prop:Font,3} = -1
    ?tmp:AccountSearchOrder:Radio2{prop:Color} = 15066597
    ?tmp:AccountSearchOrder:Radio2{prop:Trn} = 0
    If ?tmp:OrderNumber{prop:ReadOnly} = True
        ?tmp:OrderNumber{prop:FontColor} = 65793
        ?tmp:OrderNumber{prop:Color} = 15066597
    Elsif ?tmp:OrderNumber{prop:Req} = True
        ?tmp:OrderNumber{prop:FontColor} = 65793
        ?tmp:OrderNumber{prop:Color} = 8454143
    Else ! If ?tmp:OrderNumber{prop:Req} = True
        ?tmp:OrderNumber{prop:FontColor} = 65793
        ?tmp:OrderNumber{prop:Color} = 16777215
    End ! If ?tmp:OrderNumber{prop:Req} = True
    ?tmp:OrderNumber{prop:Trn} = 0
    ?tmp:OrderNumber{prop:FontStyle} = font:Bold
    If ?account_number_temp{prop:ReadOnly} = True
        ?account_number_temp{prop:FontColor} = 65793
        ?account_number_temp{prop:Color} = 15066597
    Elsif ?account_number_temp{prop:Req} = True
        ?account_number_temp{prop:FontColor} = 65793
        ?account_number_temp{prop:Color} = 8454143
    Else ! If ?account_number_temp{prop:Req} = True
        ?account_number_temp{prop:FontColor} = 65793
        ?account_number_temp{prop:Color} = 16777215
    End ! If ?account_number_temp{prop:Req} = True
    ?account_number_temp{prop:Trn} = 0
    ?account_number_temp{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt4:2{prop:FontColor} = -1
    ?Prompt4:2{prop:Color} = 15066597
    If ?job:Order_Number{prop:ReadOnly} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 15066597
    Elsif ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 8454143
    Else ! If ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 16777215
    End ! If ?job:Order_Number{prop:Req} = True
    ?job:Order_Number{prop:Trn} = 0
    ?job:Order_Number{prop:FontStyle} = font:Bold
    If ?job:Ref_Number:10{prop:ReadOnly} = True
        ?job:Ref_Number:10{prop:FontColor} = 65793
        ?job:Ref_Number:10{prop:Color} = 15066597
    Elsif ?job:Ref_Number:10{prop:Req} = True
        ?job:Ref_Number:10{prop:FontColor} = 65793
        ?job:Ref_Number:10{prop:Color} = 8454143
    Else ! If ?job:Ref_Number:10{prop:Req} = True
        ?job:Ref_Number:10{prop:FontColor} = 65793
        ?job:Ref_Number:10{prop:Color} = 16777215
    End ! If ?job:Ref_Number:10{prop:Req} = True
    ?job:Ref_Number:10{prop:Trn} = 0
    ?job:Ref_Number:10{prop:FontStyle} = font:Bold
    ?OrderNumber{prop:FontColor} = -1
    ?OrderNumber{prop:Color} = 15066597
    ?JobNumber{prop:FontColor} = -1
    ?JobNumber{prop:Color} = 15066597
    ?surname_tab{prop:Color} = 15066597
    If ?job:Surname{prop:ReadOnly} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 15066597
    Elsif ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 8454143
    Else ! If ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 16777215
    End ! If ?job:Surname{prop:Req} = True
    ?job:Surname{prop:Trn} = 0
    ?job:Surname{prop:FontStyle} = font:Bold
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    ?status_type_tab{prop:Color} = 15066597
    ?tmp:SelectLocation{prop:Font,3} = -1
    ?tmp:SelectLocation{prop:Color} = 15066597
    ?tmp:SelectLocation{prop:Trn} = 0
    ?tmp:SelectLocation:Radio1{prop:Font,3} = -1
    ?tmp:SelectLocation:Radio1{prop:Color} = 15066597
    ?tmp:SelectLocation:Radio1{prop:Trn} = 0
    ?tmp:SelectLocation:Radio2{prop:Font,3} = -1
    ?tmp:SelectLocation:Radio2{prop:Color} = 15066597
    ?tmp:SelectLocation:Radio2{prop:Trn} = 0
    If ?tmp:JobLocation{prop:ReadOnly} = True
        ?tmp:JobLocation{prop:FontColor} = 65793
        ?tmp:JobLocation{prop:Color} = 15066597
    Elsif ?tmp:JobLocation{prop:Req} = True
        ?tmp:JobLocation{prop:FontColor} = 65793
        ?tmp:JobLocation{prop:Color} = 8454143
    Else ! If ?tmp:JobLocation{prop:Req} = True
        ?tmp:JobLocation{prop:FontColor} = 65793
        ?tmp:JobLocation{prop:Color} = 16777215
    End ! If ?tmp:JobLocation{prop:Req} = True
    ?tmp:JobLocation{prop:Trn} = 0
    ?tmp:JobLocation{prop:FontStyle} = font:Bold
    If ?status_temp{prop:ReadOnly} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 15066597
    Elsif ?status_temp{prop:Req} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 8454143
    Else ! If ?status_temp{prop:Req} = True
        ?status_temp{prop:FontColor} = 65793
        ?status_temp{prop:Color} = 16777215
    End ! If ?status_temp{prop:Req} = True
    ?status_temp{prop:Trn} = 0
    ?status_temp{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    If ?JOB:Ref_Number:2{prop:ReadOnly} = True
        ?JOB:Ref_Number:2{prop:FontColor} = 65793
        ?JOB:Ref_Number:2{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:2{prop:Req} = True
        ?JOB:Ref_Number:2{prop:FontColor} = 65793
        ?JOB:Ref_Number:2{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:2{prop:Req} = True
        ?JOB:Ref_Number:2{prop:FontColor} = 65793
        ?JOB:Ref_Number:2{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:2{prop:Req} = True
    ?JOB:Ref_Number:2{prop:Trn} = 0
    ?JOB:Ref_Number:2{prop:FontStyle} = font:Bold
    ?Exchange_Status{prop:Color} = 15066597
    If ?status3_temp{prop:ReadOnly} = True
        ?status3_temp{prop:FontColor} = 65793
        ?status3_temp{prop:Color} = 15066597
    Elsif ?status3_temp{prop:Req} = True
        ?status3_temp{prop:FontColor} = 65793
        ?status3_temp{prop:Color} = 8454143
    Else ! If ?status3_temp{prop:Req} = True
        ?status3_temp{prop:FontColor} = 65793
        ?status3_temp{prop:Color} = 16777215
    End ! If ?status3_temp{prop:Req} = True
    ?status3_temp{prop:Trn} = 0
    ?status3_temp{prop:FontStyle} = font:Bold
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?Prompt25:2{prop:FontColor} = -1
    ?Prompt25:2{prop:Color} = 15066597
    If ?JOB:Ref_Number:7{prop:ReadOnly} = True
        ?JOB:Ref_Number:7{prop:FontColor} = 65793
        ?JOB:Ref_Number:7{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:7{prop:Req} = True
        ?JOB:Ref_Number:7{prop:FontColor} = 65793
        ?JOB:Ref_Number:7{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:7{prop:Req} = True
        ?JOB:Ref_Number:7{prop:FontColor} = 65793
        ?JOB:Ref_Number:7{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:7{prop:Req} = True
    ?JOB:Ref_Number:7{prop:Trn} = 0
    ?JOB:Ref_Number:7{prop:FontStyle} = font:Bold
    ?Loan_Status{prop:Color} = 15066597
    If ?status4_temp{prop:ReadOnly} = True
        ?status4_temp{prop:FontColor} = 65793
        ?status4_temp{prop:Color} = 15066597
    Elsif ?status4_temp{prop:Req} = True
        ?status4_temp{prop:FontColor} = 65793
        ?status4_temp{prop:Color} = 8454143
    Else ! If ?status4_temp{prop:Req} = True
        ?status4_temp{prop:FontColor} = 65793
        ?status4_temp{prop:Color} = 16777215
    End ! If ?status4_temp{prop:Req} = True
    ?status4_temp{prop:Trn} = 0
    ?status4_temp{prop:FontStyle} = font:Bold
    ?JOB:Ref_Number:Prompt:3{prop:FontColor} = -1
    ?JOB:Ref_Number:Prompt:3{prop:Color} = 15066597
    ?JOB:Ref_Number:Prompt:2{prop:FontColor} = -1
    ?JOB:Ref_Number:Prompt:2{prop:Color} = 15066597
    If ?JOB:Ref_Number:8{prop:ReadOnly} = True
        ?JOB:Ref_Number:8{prop:FontColor} = 65793
        ?JOB:Ref_Number:8{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:8{prop:Req} = True
        ?JOB:Ref_Number:8{prop:FontColor} = 65793
        ?JOB:Ref_Number:8{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:8{prop:Req} = True
        ?JOB:Ref_Number:8{prop:FontColor} = 65793
        ?JOB:Ref_Number:8{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:8{prop:Req} = True
    ?JOB:Ref_Number:8{prop:Trn} = 0
    ?JOB:Ref_Number:8{prop:FontStyle} = font:Bold
    ?mobile_tab{prop:Color} = 15066597
    If ?job:Mobile_Number{prop:ReadOnly} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 15066597
    Elsif ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 8454143
    Else ! If ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 16777215
    End ! If ?job:Mobile_Number{prop:Req} = True
    ?job:Mobile_Number{prop:Trn} = 0
    ?job:Mobile_Number{prop:FontStyle} = font:Bold
    ?Prompt13{prop:FontColor} = -1
    ?Prompt13{prop:Color} = 15066597
    ?Model_No_Tab{prop:Color} = 15066597
    If ?model_number_temp{prop:ReadOnly} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 15066597
    Elsif ?model_number_temp{prop:Req} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 8454143
    Else ! If ?model_number_temp{prop:Req} = True
        ?model_number_temp{prop:FontColor} = 65793
        ?model_number_temp{prop:Color} = 16777215
    End ! If ?model_number_temp{prop:Req} = True
    ?model_number_temp{prop:Trn} = 0
    ?model_number_temp{prop:FontStyle} = font:Bold
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    If ?job:Unit_Type{prop:ReadOnly} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 15066597
    Elsif ?job:Unit_Type{prop:Req} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 8454143
    Else ! If ?job:Unit_Type{prop:Req} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 16777215
    End ! If ?job:Unit_Type{prop:Req} = True
    ?job:Unit_Type{prop:Trn} = 0
    ?job:Unit_Type{prop:FontStyle} = font:Bold
    ?Prompt15{prop:FontColor} = -1
    ?Prompt15{prop:Color} = 15066597
    ?Engineer_Tab{prop:Color} = 15066597
    If ?engineer_temp{prop:ReadOnly} = True
        ?engineer_temp{prop:FontColor} = 65793
        ?engineer_temp{prop:Color} = 15066597
    Elsif ?engineer_temp{prop:Req} = True
        ?engineer_temp{prop:FontColor} = 65793
        ?engineer_temp{prop:Color} = 8454143
    Else ! If ?engineer_temp{prop:Req} = True
        ?engineer_temp{prop:FontColor} = 65793
        ?engineer_temp{prop:Color} = 16777215
    End ! If ?engineer_temp{prop:Req} = True
    ?engineer_temp{prop:Trn} = 0
    ?engineer_temp{prop:FontStyle} = font:Bold
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?Completed2_Temp:2{prop:Font,3} = -1
    ?Completed2_Temp:2{prop:Color} = 15066597
    ?Completed2_Temp:2{prop:Trn} = 0
    ?Completed_Temp:Radio1{prop:Font,3} = -1
    ?Completed_Temp:Radio1{prop:Color} = 15066597
    ?Completed_Temp:Radio1{prop:Trn} = 0
    ?Completed_Temp:Radio3{prop:Font,3} = -1
    ?Completed_Temp:Radio3{prop:Color} = 15066597
    ?Completed_Temp:Radio3{prop:Trn} = 0
    ?Completed_Temp:Radio2{prop:Font,3} = -1
    ?Completed_Temp:Radio2{prop:Color} = 15066597
    ?Completed_Temp:Radio2{prop:Trn} = 0
    If ?JOB:Ref_Number:3{prop:ReadOnly} = True
        ?JOB:Ref_Number:3{prop:FontColor} = 65793
        ?JOB:Ref_Number:3{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:3{prop:Req} = True
        ?JOB:Ref_Number:3{prop:FontColor} = 65793
        ?JOB:Ref_Number:3{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:3{prop:Req} = True
        ?JOB:Ref_Number:3{prop:FontColor} = 65793
        ?JOB:Ref_Number:3{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:3{prop:Req} = True
    ?JOB:Ref_Number:3{prop:Trn} = 0
    ?JOB:Ref_Number:3{prop:FontStyle} = font:Bold
    ?Prompt17{prop:FontColor} = -1
    ?Prompt17{prop:Color} = 15066597
    ?Unallocated_Jobs_Tab{prop:Color} = 15066597
    If ?JOB:Ref_Number:4{prop:ReadOnly} = True
        ?JOB:Ref_Number:4{prop:FontColor} = 65793
        ?JOB:Ref_Number:4{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:4{prop:Req} = True
        ?JOB:Ref_Number:4{prop:FontColor} = 65793
        ?JOB:Ref_Number:4{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:4{prop:Req} = True
        ?JOB:Ref_Number:4{prop:FontColor} = 65793
        ?JOB:Ref_Number:4{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:4{prop:Req} = True
    ?JOB:Ref_Number:4{prop:Trn} = 0
    ?JOB:Ref_Number:4{prop:FontStyle} = font:Bold
    ?Prompt18{prop:FontColor} = -1
    ?Prompt18{prop:Color} = 15066597
    ?unallocated_temp{prop:Font,3} = -1
    ?unallocated_temp{prop:Color} = 15066597
    ?unallocated_temp{prop:Trn} = 0
    ?unallocated_temp:Radio1{prop:Font,3} = -1
    ?unallocated_temp:Radio1{prop:Color} = 15066597
    ?unallocated_temp:Radio1{prop:Trn} = 0
    ?unallocated_temp:Radio2{prop:Font,3} = -1
    ?unallocated_temp:Radio2{prop:Color} = 15066597
    ?unallocated_temp:Radio2{prop:Trn} = 0
    ?Tab16{prop:Color} = 15066597
    If ?job:Incoming_Consignment_Number{prop:ReadOnly} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Incoming_Consignment_Number{prop:Req} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Incoming_Consignment_Number{prop:Req} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Incoming_Consignment_Number{prop:Req} = True
    ?job:Incoming_Consignment_Number{prop:Trn} = 0
    ?job:Incoming_Consignment_Number{prop:FontStyle} = font:Bold
    ?Prompt30{prop:FontColor} = -1
    ?Prompt30{prop:Color} = 15066597
    ?Consignment_No_Tab{prop:Color} = 15066597
    ?Prompt24{prop:FontColor} = -1
    ?Prompt24{prop:Color} = 15066597
    ?JOB:Consignment_Number:Prompt{prop:FontColor} = -1
    ?JOB:Consignment_Number:Prompt{prop:Color} = 15066597
    If ?job:Consignment_Number{prop:ReadOnly} = True
        ?job:Consignment_Number{prop:FontColor} = 65793
        ?job:Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Consignment_Number{prop:Req} = True
        ?job:Consignment_Number{prop:FontColor} = 65793
        ?job:Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Consignment_Number{prop:Req} = True
        ?job:Consignment_Number{prop:FontColor} = 65793
        ?job:Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Consignment_Number{prop:Req} = True
    ?job:Consignment_Number{prop:Trn} = 0
    ?job:Consignment_Number{prop:FontStyle} = font:Bold
    ?Batch_Number_Tab{prop:Color} = 15066597
    If ?batch_number_temp{prop:ReadOnly} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 15066597
    Elsif ?batch_number_temp{prop:Req} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 8454143
    Else ! If ?batch_number_temp{prop:Req} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 16777215
    End ! If ?batch_number_temp{prop:Req} = True
    ?batch_number_temp{prop:Trn} = 0
    ?batch_number_temp{prop:FontStyle} = font:Bold
    ?Prompt21{prop:FontColor} = -1
    ?Prompt21{prop:Color} = 15066597
    ?Completed2_Temp{prop:Font,3} = -1
    ?Completed2_Temp{prop:Color} = 15066597
    ?Completed2_Temp{prop:Trn} = 0
    ?Completed2_Temp:Radio1{prop:Font,3} = -1
    ?Completed2_Temp:Radio1{prop:Color} = 15066597
    ?Completed2_Temp:Radio1{prop:Trn} = 0
    ?Completed2_Temp:Radio2{prop:Font,3} = -1
    ?Completed2_Temp:Radio2{prop:Color} = 15066597
    ?Completed2_Temp:Radio2{prop:Trn} = 0
    ?Completed2_Temp:Radio3{prop:Font,3} = -1
    ?Completed2_Temp:Radio3{prop:Color} = 15066597
    ?Completed2_Temp:Radio3{prop:Trn} = 0
    ?JOB:Ref_Number:Prompt{prop:FontColor} = -1
    ?JOB:Ref_Number:Prompt{prop:Color} = 15066597
    If ?JOB:Ref_Number:5{prop:ReadOnly} = True
        ?JOB:Ref_Number:5{prop:FontColor} = 65793
        ?JOB:Ref_Number:5{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:5{prop:Req} = True
        ?JOB:Ref_Number:5{prop:FontColor} = 65793
        ?JOB:Ref_Number:5{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:5{prop:Req} = True
        ?JOB:Ref_Number:5{prop:FontColor} = 65793
        ?JOB:Ref_Number:5{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:5{prop:Req} = True
    ?JOB:Ref_Number:5{prop:Trn} = 0
    ?JOB:Ref_Number:5{prop:FontStyle} = font:Bold
    ?Ready_To_Despatch{prop:Color} = 15066597
    ?select_courier_temp{prop:Font,3} = -1
    ?select_courier_temp{prop:Color} = 15066597
    ?select_courier_temp{prop:Trn} = 0
    ?select_courier_temp:Radio1{prop:Font,3} = -1
    ?select_courier_temp:Radio1{prop:Color} = 15066597
    ?select_courier_temp:Radio1{prop:Trn} = 0
    ?select_courier_temp:Radio2{prop:Font,3} = -1
    ?select_courier_temp:Radio2{prop:Color} = 15066597
    ?select_courier_temp:Radio2{prop:Trn} = 0
    If ?courier_temp{prop:ReadOnly} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 15066597
    Elsif ?courier_temp{prop:Req} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 8454143
    Else ! If ?courier_temp{prop:Req} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 16777215
    End ! If ?courier_temp{prop:Req} = True
    ?courier_temp{prop:Trn} = 0
    ?courier_temp{prop:FontStyle} = font:Bold
    ?Prompt28{prop:FontColor} = -1
    ?Prompt28{prop:Color} = 15066597
    ?Select_Trade_Account_Temp{prop:Font,3} = -1
    ?Select_Trade_Account_Temp{prop:Color} = 15066597
    ?Select_Trade_Account_Temp{prop:Trn} = 0
    ?Select_Trade_Account_Temp:Radio1{prop:Font,3} = -1
    ?Select_Trade_Account_Temp:Radio1{prop:Color} = 15066597
    ?Select_Trade_Account_Temp:Radio1{prop:Trn} = 0
    ?Select_Trade_Account_Temp:Radio2{prop:Font,3} = -1
    ?Select_Trade_Account_Temp:Radio2{prop:Color} = 15066597
    ?Select_Trade_Account_Temp:Radio2{prop:Trn} = 0
    If ?account_number2_temp{prop:ReadOnly} = True
        ?account_number2_temp{prop:FontColor} = 65793
        ?account_number2_temp{prop:Color} = 15066597
    Elsif ?account_number2_temp{prop:Req} = True
        ?account_number2_temp{prop:FontColor} = 65793
        ?account_number2_temp{prop:Color} = 8454143
    Else ! If ?account_number2_temp{prop:Req} = True
        ?account_number2_temp{prop:FontColor} = 65793
        ?account_number2_temp{prop:Color} = 16777215
    End ! If ?account_number2_temp{prop:Req} = True
    ?account_number2_temp{prop:Trn} = 0
    ?account_number2_temp{prop:FontStyle} = font:Bold
    ?job:Ref_Number:9{prop:FontColor} = -1
    ?job:Ref_Number:9{prop:Color} = 15066597
    ?Button_Group2{prop:Font,3} = -1
    ?Button_Group2{prop:Color} = 15066597
    ?Button_Group2{prop:Trn} = 0
    If ?JOB:Ref_Number:6{prop:ReadOnly} = True
        ?JOB:Ref_Number:6{prop:FontColor} = 65793
        ?JOB:Ref_Number:6{prop:Color} = 15066597
    Elsif ?JOB:Ref_Number:6{prop:Req} = True
        ?JOB:Ref_Number:6{prop:FontColor} = 65793
        ?JOB:Ref_Number:6{prop:Color} = 8454143
    Else ! If ?JOB:Ref_Number:6{prop:Req} = True
        ?JOB:Ref_Number:6{prop:FontColor} = 65793
        ?JOB:Ref_Number:6{prop:Color} = 16777215
    End ! If ?JOB:Ref_Number:6{prop:Req} = True
    ?JOB:Ref_Number:6{prop:Trn} = 0
    ?JOB:Ref_Number:6{prop:FontStyle} = font:Bold
    ?Prompt23{prop:FontColor} = -1
    ?Prompt23{prop:Color} = 15066597
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    ?Prompt26:2{prop:FontColor} = -1
    ?Prompt26:2{prop:Color} = 15066597
    ?Prompt26:3{prop:FontColor} = -1
    ?Prompt26:3{prop:Color} = 15066597
    If ?job:Exchange_Status{prop:ReadOnly} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 15066597
    Elsif ?job:Exchange_Status{prop:Req} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 8454143
    Else ! If ?job:Exchange_Status{prop:Req} = True
        ?job:Exchange_Status{prop:FontColor} = 65793
        ?job:Exchange_Status{prop:Color} = 16777215
    End ! If ?job:Exchange_Status{prop:Req} = True
    ?job:Exchange_Status{prop:Trn} = 0
    ?job:Exchange_Status{prop:FontStyle} = font:Bold
    If ?job:Loan_Status{prop:ReadOnly} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 15066597
    Elsif ?job:Loan_Status{prop:Req} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 8454143
    Else ! If ?job:Loan_Status{prop:Req} = True
        ?job:Loan_Status{prop:FontColor} = 65793
        ?job:Loan_Status{prop:Color} = 16777215
    End ! If ?job:Loan_Status{prop:Req} = True
    ?job:Loan_Status{prop:Trn} = 0
    ?job:Loan_Status{prop:FontStyle} = font:Bold
    If ?job:Current_Status{prop:ReadOnly} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 15066597
    Elsif ?job:Current_Status{prop:Req} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 8454143
    Else ! If ?job:Current_Status{prop:Req} = True
        ?job:Current_Status{prop:FontColor} = 65793
        ?job:Current_Status{prop:Color} = 16777215
    End ! If ?job:Current_Status{prop:Req} = True
    ?job:Current_Status{prop:Trn} = 0
    ?job:Current_Status{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::26:DASTAGONOFF Routine
  ! Before Embed Point: %DasTagBeforeTagOnOff) DESC(Tagging Before Tag On / Off) ARG(26)
  Brw1.updatebuffer
  IF despatch_type_temp = 'HLD'
  
      Case MessageEx('This job is held, pending an accessory match.<13,10><13,10>To check the accessories are now present click the ''Validate E.S.N. / Acc.'' Button.','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
          Of 1 ! &OK Button
      End!Case MessageEx
      glo:job_number_pointer = job:ref_number
      Get(glo:q_jobnumber,glo:job_number_pointer)
      If ~error()
          Delete(glo:q_jobnumber)
          tag_temp = ''
          queue:browse:1.tag_temp = tag_temp
          IF (tag_temp = '*')
            Queue:Browse:1.tag_temp_Icon = 12
          ELSE
            Queue:Browse:1.tag_temp_Icon = 1
          END
          PUT(Queue:Browse:1)
          SELECT(?Browse:1,CHOICE(?Browse:1))
  
      End!If ~error()
  Else!IF despatch_type_temp = 'HLD'
  ! After Embed Point: %DasTagBeforeTagOnOff) DESC(Tagging Before Tag On / Off) ARG(26)
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   glo:q_JobNumber.Job_Number_Pointer = job:Ref_Number
   GET(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
  IF ERRORCODE()
     glo:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     ADD(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    tag_temp = '*'
  ELSE
    DELETE(glo:q_JobNumber)
    tag_temp = ''
  END
    Queue:Browse:1.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse:1.tag_temp_Icon = 12
  ELSE
    Queue:Browse:1.tag_temp_Icon = 11
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
  ! Before Embed Point: %DasTagAfterTagOnOff) DESC(Tagging After Tag On / Off) ARG(26)
  End!IF despatch_type_temp = 'HLD'
  ! After Embed Point: %DasTagAfterTagOnOff) DESC(Tagging After Tag On / Off) ARG(26)
DASBRW::26:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(glo:q_JobNumber)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     ADD(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::26:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:q_JobNumber)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::26:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::26:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:q_JobNumber)
    GET(glo:q_JobNumber,QR#)
    DASBRW::26:QUEUE = glo:q_JobNumber
    ADD(DASBRW::26:QUEUE)
  END
  FREE(glo:q_JobNumber)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::26:QUEUE.Job_Number_Pointer = job:Ref_Number
     GET(DASBRW::26:QUEUE,DASBRW::26:QUEUE.Job_Number_Pointer)
    IF ERRORCODE()
       glo:q_JobNumber.Job_Number_Pointer = job:Ref_Number
       ADD(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::26:DASSHOWTAG Routine
   CASE DASBRW::26:TAGDISPSTATUS
   OF 0
      DASBRW::26:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::26:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::26:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Create_Invoice      Routine
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))
    Set(defaults)
    access:defaults.next()
    invoice_number_temp = 0
    Case MessageEx('Are you sure you want to print a Single Invoice?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            error# = 0
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = brw1.q.job:Ref_number
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                access:jobnotes.clearkey(jbn:RefNumberKey)
                jbn:RefNumber   = job:ref_number
                access:jobnotes.tryfetch(jbn:RefNumberKey)
                If job:bouncer <> ''
                    Case MessageEx('This job has been marked as a Bouncer.<13,10><13,10>You must authorise this job for Invoicing before you can continue.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                        Of 1 ! &Close Button
                    End!Case MessageEx
                    error# = 1
                End!If job:bouncer <> ''
                If job:invoice_number <> '' and error# = 0
                    access:invoice.clearkey(inv:invoice_number_key)
                    inv:invoice_number = job:invoice_number
                    if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
                        If inv:invoice_type = 'SIN'
                            glo:select1 = inv:invoice_number
                            Single_Invoice
                            glo:select1 = ''
                        Else!End!If inv:invoice_type = 'SIN'
                            Case MessageEx('The selected job is part of a Multiple Invoice, or is a Warranty job.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                Of 1 ! &Close Button
                            End!Case MessageEx
                        End!If inv:invoice_type = 'SIN'
                    end

                Else!If job:invoice_number <> ''
                    error# = 0
                    If job:chargeable_job <> 'YES'
                        Case MessageEx('You are attempting to Invoice a Warranty Only Job.<13,10><13,10>This can only be processed through the Warranty Procedures.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                            Of 1 ! &Close Button
                        End!Case MessageEx
                        error# = 1
                    End!If job:chargeable_job <> 'YES'
                    If job:date_completed <> '' And error# = 0
                        Set(defaults)
                        access:defaults.next()
                        If def:qa_required = 'YES'
                            If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
                                Case MessageEx('The selected job has not passed QA checks','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                error# = 1
                            End!If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
                        End!If def:qa_required = 'YES'
                    Else!If job:date_completed <> ''
                        Case MessageEx('The selected job has not been completed.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        error# = 1
                    End!If job:date_completed <> ''
                    If job:ignore_chargeable_charges = 'YES' And error# = 0
                        If job:sub_total = 0
                            Case MessageEx('You are attempting to invoice a job than has not been priced.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            error# = 1
                        End
                    Else!If job:ignore_chargeable_charges = 'YES'
                        If error# = 0
                            Pricing_Routine('C',labour",parts",pass",a")
                            If pass" = False
                                Case MessageEx('You are attempting to Invoice a job for which a Default Charge Structure has not been established.<13,10><13,10>If you wish to proceed you must access the Chargeable Tab and tick the "Ignore Default Charge" option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                error# = 1
                            Else!If pass" = False
                                job:labour_cost = labour"
                                job:parts_cost  = parts"
                                job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
                            End!If pass" = False
                        End!If error# = 0
                    End!If job:ignore_chargeable_charges = 'YES'
                    If error# = 0
                        fetch_error# = 0
                        despatch# = 0
                        access:subtracc.clearkey(sub:account_number_key)
                        sub:account_number = job:account_number
                        if access:subtracc.fetch(sub:account_number_key)
                            Case MessageEx('Error! Cannot find the Sub Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                        Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                            access:tradeacc.clearkey(tra:account_number_key) 
                            tra:account_number = sub:main_account_number
                            if access:tradeacc.fetch(tra:account_number_key)
                                Case MessageEx('Error! Cannot find the Trade Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                fetch_error# = 1
                            Else!if access:tradeacc.fetch(tra:account_number_key)
                                If tra:use_sub_accounts = 'YES'
                                    If sub:despatch_invoiced_jobs = 'YES'
                                        If sub:despatch_paid_jobs = 'YES'
                                            If job:paid = 'YES'
                                                despatch# = 1
                                            Else
                                                despatch# = 2
                                            End
                                        Else
                                            despatch# = 1
                                        End!If sub:despatch_paid_jobs = 'YES' And job:paid = 'YES'
                                        
                                    End!If sub:despatch_invoiced_jobs = 'YES'
                                End!If tra:use_sub_accounts = 'YES'
                                if tra:invoice_sub_accounts = 'YES'
                                    vat_number" = sub:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = sub:labour_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                        fetch_error# = 1
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       labour_rate$ = vat:vat_rate
                                    end!if access:vatcode.fetch(vat:vat_code_key)
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = sub:parts_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                            Case MessageEx('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                                Of 1 ! &OK Button
                                            End!Case MessageEx
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       parts_rate$ = vat:vat_rate
                                    end!if access:vatcode.fetch(vat:vat_code_key)
                                else!if tra:use_sub_accounts = 'YES'
                                    If tra:despatch_invoiced_jobs = 'YES'
                                        If tra:despatch_paid_jobs = 'YES'
                                            If job:paid = 'YES'
                                                despatch# = 1
                                            Else
                                                despatch# = 2
                                            End!If job:paid = 'YES'
                                        Else
                                            despatch# = 1
                                        End!If tra:despatch_paid_jobs = 'YES' and job:paid = 'YES'
                                    End!If tra:despatch_invoiced_jobs = 'YES'
                                    vat_number" = tra:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = tra:labour_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       labour_rate$ = vat:vat_rate
                                    end
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = tra:parts_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       parts_rate$ = vat:vat_rate
                                    end
                                end!if tra:use_sub_accounts = 'YES'
                            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                        end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                        If fetch_error# = 0
        !SAGE BIT
                            If def:use_sage = 'YES'
                                glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
                                Remove(paramss)
                                access:paramss.open()
                                access:paramss.usefile()
             !LABOUR
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Labour_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Labour_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:labour_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
            !PARTS
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Parts_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:parts_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:parts_cost * (parts_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
            !COURIER
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'

                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Courier_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Courier_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:courier_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
                                access:paramss.close()
                                Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
                                sage_error# = 0
                                access:paramss.open()
                                access:paramss.usefile()
                                Set(paramss,0)
                                If access:paramss.next()
                                    sage_error# = 1
                                Else!If access:paramss.next()
                                    If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                                        sage_error# = 1
                                    Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                                        invoice_number_temp = prm:invoice_no
                                        If invoice_number_temp = 0
                                            sage_error# = 1
                                        End!If invoice_number_temp = 0
                                    End!If prm:invoice_no = '-1'
                                End!If access:paramss.next()
                                access:paramss.close()
                            End!If def:use_sage = 'YES'
                            If sage_error# = 0
                                invoice_error# = 1
                                get(invoice,0)
                                if access:invoice.primerecord() = level:benign
                                    If def:use_sage = 'YES'
                                        inv:invoice_number = invoice_number_temp
                                    End!If def:use_sage = 'YES'
                                    inv:invoice_type       = 'SIN'
                                    inv:job_number         = job:ref_number
                                    inv:date_created       = Today()
                                    inv:account_number     = job:account_number
                                    If tra:invoice_sub_accounts = 'YES'
                                        inv:AccountType = 'SUB'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        inv:AccountType = 'MAI'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    inv:total              = job:sub_total
                                    inv:vat_rate_labour    = labour_rate$
                                    inv:vat_rate_parts     = parts_rate$
                                    inv:vat_rate_retail    = labour_rate$
                                    inv:vat_number         = def:vat_number
                                    INV:Courier_Paid       = job:courier_cost
                                    inv:parts_paid         = job:parts_cost
                                    inv:labour_paid        = job:labour_cost
                                    inv:invoice_vat_number = vat_number"
                                    invoice_error# = 0
                                    If access:invoice.insert()
                                        invoice_error# = 1
                                        access:invoice.cancelautoinc()
                                    End!If access:invoice.insert()
                                end!if access:invoice.primerecord() = level:benign
                                If Invoice_error# = 0
                                    Case despatch#
                                        Of 1
                                            If job:despatched = 'YES'
                                                If job:paid = 'YES'
                                                    GetStatus(910,0,'JOB') !Despatch Paid
                                                Else!If job:paid = 'YES'
                                                    GetStatus(905,0,'JOB') !Despatch Unpaid
                                                End!If job:paid = 'YES'
                                            Else!If job:despatched = 'YES'
                                                GetStatus(810,0,'JOB') !Ready To Despatch
                                                job:despatched = 'REA'
                                                job:despatch_type = 'JOB'
                                                job:current_courier = job:courier
                                            End!If job:despatched <> 'YES'
                                        Of 2
                                            GetStatus(803,0,'JOB') !Ready To Despatch

                                    End!Case despatch#

                                    job:invoice_number        = inv:invoice_number
                                    job:invoice_date          = Today()
                                    job:invoice_courier_cost  = job:courier_cost
                                    job:invoice_labour_cost   = job:labour_cost
                                    job:invoice_parts_cost    = job:parts_cost
                                    job:invoice_sub_total     = job:sub_total
                                    access:jobs.update()
                                    get(audit,0)
                                    if access:audit.primerecord() = level:benign
                                        aud:notes         = 'INVOICE NUMBER: ' & inv:invoice_number
                                        aud:ref_number    = job:ref_number
                                        aud:date          = today()
                                        aud:time          = clock()
                                        aud:type          = 'JOB'
                                        access:users.clearkey(use:password_key)
                                        use:password = glo:password
                                        access:users.fetch(use:password_key)
                                        aud:user = use:user_code
                                        aud:action        = 'INVOICE CREATED'
                                        access:audit.insert()
                                    end!�if access:audit.primerecord() = level:benign

                                    glo:select1 = inv:invoice_number
                                    Single_Invoice
                                    glo:select1 = ''
                                End!If Invoice_error# = 0
                            End!If def:use_sage = 'YES' and sage_error# = 1
                        End!If fetch_error# = 0
                    End!If error# = 0
                End!If job:invoice_number <> ''
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign

        Of 2 ! &No Button
    End!Case MessageEx
!Printing Routines
JobReceipt      Routine
    glo:select1 = brw1.q.job:ref_number
    Job_Receipt
    glo:Select1 = ''

JobCard         Routine
    glo:select1 = brw1.q.job:ref_number
    Job_Card
    glo:Select1 = ''
JobLabel        Routine
    glo:select1 = brw1.q.job:ref_number
    Set(defaults)
    access:defaults.next()
    Case def:label_printer_type
        Of 'TEC B-440 / B-442'
            If job:bouncer = 'B'
                Thermal_Labels('SE')
            Else!If job:bouncer = 'B'
                Thermal_Labels('')
            End!If job:bouncer = 'B'
            
        Of 'TEC B-452'
            Thermal_Labels_B452
    End!Case def:label_printer_type
    glo:select1 = ''

! Start Change 4120 BE(31/08/2004)
VirginLabel        Routine
    glo:select1 = brw1.q.job:ref_number
    Virgin_Labels()
    glo:select1 = ''
! End Change 4120 BE(31/08/2004)

Letters         Routine
    glo:Select1 = brw1.q.job:ref_number
    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    Pick_Letter
    if globalresponse = requestcompleted
        glo:select2 = let:description
        Standard_Letter
        access:letters.clearkey(let:descriptionkey)
        let:description = glo:select2
        access:letters.tryfetch(LET:DescriptionKey)
        If let:usestatus = 'YES'
            GetStatus(Sub(let:status,1,3),0,'JOB') !Letter Status
            Access:jobs.update()

        End!If let:use_status = 'YES'
        get(audit,0)
        if access:audit.primerecord() = level:benign
            If let:usestatus = 'YES'

                aud:notes         = 'PREVIOUS STATUS: ' & CLip(job:PreviousStatus) & |
                                      '<13,10>NEW STATUS: ' & Clip(job:current_status)
            End!If let:use_status = 'YES'
            aud:ref_number    = job:ref_number
            aud:date          = today()
            aud:time          = clock()
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'LETTER SENT: ' & Clip(let:description)
            access:audit.insert()
        end!�if access:audit.primerecord() = level:benign

    end
    globalrequest     = saverequest#
    glo:select1 = ''
    glo:select2  = ''

MailMerge       Routine
    glo:select1 = brw1.q.job:ref_number
    saverequest#      = globalrequest
    globalresponse    = requestcancelled
    globalrequest     = selectrecord
    Pickletters
    if globalresponse = requestcompleted
        glo:select2  = mrg:lettername
        ViewLetter
        access:mergelet.clearkey(MRg:LetterNameKey)
        mrg:LetterName  = glo:select2
        access:mergelet.fetch(mrg:LetterNameKey)
Compile('***',Debug=1)
    Message('glo:select2: ' & glo:select2 & |
        'mrg:usestatus: ' & mrg:usestatus,'Debug Message',icon:exclamation)
***
        If MRG:UseStatus = 'YES'
Compile('***',Debug=1)
    Message('Use New Status: ' & CLip(mrg:status),'Debug Message',icon:exclamation)
***
            GetStatus(Sub(mrg:status,1,3),0,'JOB') !Letter Status
            Access:jobs.update()

        End!If let:use_status = 'YES'
        get(audit,0)
        if access:audit.primerecord() = level:benign
            If mrg:usestatus = 'YES'

                aud:notes         = 'PREVIOUS STATUS: ' & CLip(job:PreviousStatus) & |
                                      '<13,10>NEW STATUS: ' & Clip(job:current_status)
            End!If let:use_status = 'YES'
            aud:ref_number    = job:ref_number
            aud:date          = today()
            aud:time          = clock()
            access:users.clearkey(use:password_key)
            use:password = glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = 'LETTER SENT: ' & Clip(mrg:LetterName)
            access:audit.insert()
        end!�if access:audit.primerecord() = level:benign
    end
    globalrequest     = saverequest#
    glo:select1 = ''


ANCCollNote     Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = glo:select1
    If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
        error# = 0
        access:courier.clearkey(cou:courier_key)
        cou:courier = job:incoming_courier
        if access:courier.tryfetch(cou:courier_key) = Level:Benign
            If cou:courier_type <> 'ANC'
                error# = 1
            End!If job:courier_type = 'ANC'
        Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
            error# = 1
        End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
        If error# = 1
            Case MessageEx('This job has not been assigned to an ANC courier.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If error# = 1
            glo:file_name = 'C:\ANC.TXT'
            Remove(expgen)
            If access:expgen.open()
                Stop(error())
                
            
                access:expgen.usefile()
                Clear(gen:record)
                gen:line1   = job:ref_number
                gen:line1   = Sub(gen:line1,1,10) & job:company_name
                gen:line1   = Sub(gen:line1,1,40) & job:address_line1_delivery
                gen:line1   = Sub(gen:line1,1,70) & job:address_line2_delivery
                gen:line1   = Sub(gen:line1,1,100) & job:address_line3_delivery
                gen:line1   = Sub(gen:line1,1,130) & job:postcode_delivery
                gen:line1   = Sub(gen:line1,1,150) & job:telephone_delivery
                gen:line1   = Sub(gen:line1,1,175) & job:unit_type
                gen:line1   = Sub(gen:line1,1,195) & job:model_number
                gen:line1   = Sub(gen:line1,1,215) & cou:ContractNumber
                access:expgen.insert()
                Close(expgen)
                access:expgen.close()
                cou:anccount += 1
                If cou:ANCCount = 999
                    cou:ANCCount = 1
                End
                access:courier.update()
                Set(Defaults)
                access:defaults.next()
                job:incoming_consignment_number = def:ANCCollectionNo
                access:jobs.update()
                get(audit,0)
                if access:audit.primerecord() = level:benign
                    aud:notes         = 'COLLECTION NOTE NUMBER: ' & Clip(def:ANCCollectionNo)
                    aud:ref_number    = job:ref_number
                    aud:date          = today()
                    aud:time          = clock()
                    access:users.clearkey(use:password_key)
                    use:password = glo:password
                    access:users.fetch(use:password_key)
                    aud:user = use:user_code
                    aud:action        = 'ANC COLLECTION NOTE REPRINT'
                    access:audit.insert()
                end!�if access:audit.primerecord() = level:benign

                DEF:ANCCollectionNo += 10
                access:defaults.update()

                sav:path    = path()
                setcursor(cursor:wait)
                setpath(Clip(cou:ancpath))
                Run('ancpaper.exe',1)
                Setpath(sav:path)
            End!If access:expgen.open()
            setcursor()


        End!If error# = 1
    End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
    glo:select1 = ''

DespatchNote        Routine
    glo:select1 = brw1.q.job:ref_number
    Despatch_Note
    glo:select1 = ''

ExchangeUnitLabel   Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = glo:select1
    If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
        If job:exchange_unit_number = ''
            Case MessageEx('There is no Exchange Unit attached to this job.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If job:exchange_unit_number = ''
            job_number# = glo:select1                                                    !A fudge becuase I can't be
            glo:select1 = job:exchange_unit_number                                       !bothered to change the prog.
            Exchange_Unit_Label
            glo:select1  = job_number#
        End!If job:exchange_unit_number = ''
    End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
    glo:select1 = ''

LoanUnitLabel       Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = glo:select1
    If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
        If job:loan_unit_number = ''
            Case MessageEx('There is no Loan Unit attached to this job.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If job:loan_unit_number = ''
            job_number# = glo:select1
            glo:select1  = job:loan_unit_number
            Loan_Unit_Label
            glo:select1  = job_number#
        End!If job:loan_unit_number = ''
    End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
    glo:select1 = ''

Estimate        Routine
    glo:select1 = brw1.q.job:ref_number
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = glo:select1
    if access:jobs.fetch(job:ref_number_key) = Level:Benign
        If job:estimate <> 'YES'
            Case MessageEx('The selected job has NOT been marked as an Estimate.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If job:estimate <> 'YES'
            Estimate
        End!If job:estimate <> 'YES'
    end
    glo:Select1 = ''

Invoice         Routine
    Do Create_Invoice
refresh_job_status_tab      Routine
    Case tmp:SelectLocation
        Of 1
            job:current_status  = status_temp
            brw1.addrange(job:current_status,status_temp)
            Brw1.applyrange
        Of 2
            job:current_status = status_temp
            job:location    = tmp:joblocation
            brw1.addrange(job:location,tmp:joblocation)
            Brw1.applyrange
    End!Case Select_Courier
    Thiswindow.reset(1)
show_hide       Routine
    If select_trade_account_temp = 'IND'
        Unhide(?account_number2_temp)
        Unhide(?Lookup_account_number:2)
    Else
        Hide(?account_number2_temp)
        hide(?Lookup_account_number:2)
    End

    If select_courier_temp = 'IND'
        Unhide(?courier_temp)
        unhide(?lookup_Courier)
    Else
        Hide(?courier_temp)
        Hide(?lookup_courier)
    End

    If tmp:selectlocation = 2
        Unhide(?tmp:JobLocation)
        unhide(?Lookup_location)
    Else!If tmp:selectlocation = 1
        Hide(?tmp:jobLocation)
        hide(?lookup_location)
    End!If tmp:selectlocation = 1
    Display()    
refresh_despatch_tab        Routine
    Case Select_Courier_Temp
        Of 'ALL'
            Case Select_Trade_Account_Temp
                Of 'ALL'
                    rea" = 'REA'
                    Brw1.addrange(job:despatched,rea")
                    Brw1.applyrange
                Of 'IND'
                    job:despatched = 'REA'
                    Brw1.addrange(job:account_number,account_number2_temp)
                    Brw1.applyrange    
            End!Case Select_Trade_Account_Temp
        Of 'IND'
            Case Select_Trade_Account_Temp
                Of 'ALL'
                    job:despatched = 'REA'
                    Brw1.addrange(job:current_courier,courier_temp)
                    Brw1.applyrange
                Of 'IND'
                    job:despatched = 'REA'
                    job:account_number    = account_number2_temp
                    Brw1.addrange(job:current_courier,courier_temp)
                    Brw1.applyrange
            End!Case Select_Trade_Account_Temp    
    End!Case Select_Courier
    Thiswindow.reset(1)
Update_City_Link    Routine
    epc:account_number    = Left(Sub(cou:account_number,1,8))
    dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
    If multiple_despatch_temp = 'YES'
        dbt:batch_number = Format(dbt:batch_number,@p<<<<<<<#p)
        epc:ref_number        = '|DB' & Clip(Format(dbt:batch_number,@s9))
    Else
        epc:ref_number        = '|J' & Clip(Format(job:ref_number,@s9)) & '/DB' & Clip(Format(dbt:batch_number,@s9)) & |
                                    '/' & Clip(job:order_number)
    End
    If multiple_despatch_temp = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = level:benign
            access:tradeacc.clearkey(tra:account_number_key) 
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = level:benign
                if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(sub:contact_name)
                    epc:address_line1 = '|' & Left(sub:company_name)
                    epc:address_line2 = '|' & Left(sub:address_line1)
                    epc:town = '|' & Left(sub:address_line2)
                    epc:county = '|' & Left(sub:address_line3)
                    epc:postcode = '|' & Left(sub:postcode)
                else!if tra:use_sub_accounts = 'YES'
                    epc:contact_name = '|' & Left(tra:contact_name)
                    epc:address_line1 = '|' & Left(tra:company_name)
                    epc:address_line1 = '|' & Left(tra:address_line1)
                    epc:town = '|' & Left(tra:address_line2)
                    epc:county = '|' & Left(tra:address_line3)
                    epc:postcode = '|' & Left(tra:postcode)
                end!if tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
        end!if access:subtracc.fetch(sub:account_number_key) = level:benign
    Else!If multiple_despatch_temp = 'YES'
        epc:contact_name      = '|' & Left(Sub(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),1,30))
        epc:address_line1     = '|' & Left(Sub(job:company_name_delivery,1,30))
        epc:address_line2     = '|' & Left(Sub(job:address_line1_delivery,1,30))
        epc:town              = '|' & Left(Sub(job:address_line2_delivery,1,30))
        epc:county            = '|' & Left(Sub(job:address_line3_delivery,1,30))
        epc:postcode          = '|' & Left(Sub(job:postcode_delivery,1,8))
    End!If multiple_despatch_temp = 'YES'
    epc:nol               = '|' & '01'
    epc:customer_name     = '|' & Left(Sub(job:account_number,1,30))
    epc:city_service      = '|' & Left(Sub(cou:service,1,2))
    access:jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:ref_number
    access:jobnotes.tryfetch(jbn:REfNumberKey)
    epc:city_instructions = '|' & Left(Sub(jbn:delivery_text,1,30))
    epc:pudamt            = '|' & Left(Sub('0.00',1,4))
    If job:despatch_type = 'EXC' or job:despatch_type = 'LOA'
        epc:return_it     = '|' & Left('Y')
    Else
        epc:return_it     = '|' & Left('N')
    End
    epc:saturday          = '|' & Left('N')
    epc:dog               = '|' & Left(Sub('Mobile Phone Goods',1,30))
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Jobs',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Jobs',1)
    SolaceViewVars('DespatchLocateFlag',DespatchLocateFlag,'Browse_Jobs',1)
    SolaceViewVars('InWorkshopDate_temp',InWorkshopDate_temp,'Browse_Jobs',1)
    SolaceViewVars('unallocated_temp',unallocated_temp,'Browse_Jobs',1)
    SolaceViewVars('save_sub_id',save_sub_id,'Browse_Jobs',1)
    SolaceViewVars('save_jot_id',save_jot_id,'Browse_Jobs',1)
    SolaceViewVars('save_mulj_id',save_mulj_id,'Browse_Jobs',1)
    SolaceViewVars('save_aus_id',save_aus_id,'Browse_Jobs',1)
    SolaceViewVars('invoice_number_temp',invoice_number_temp,'Browse_Jobs',1)
    SolaceViewVars('tmp:accountnumber',tmp:accountnumber,'Browse_Jobs',1)
    SolaceViewVars('tmp:labelerror',tmp:labelerror,'Browse_Jobs',1)
    SolaceViewVars('tmp:OldConsignNo',tmp:OldConsignNo,'Browse_Jobs',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Browse_Jobs',1)
    SolaceViewVars('save_cou_ali_id',save_cou_ali_id,'Browse_Jobs',1)
    SolaceViewVars('sav:path',sav:path,'Browse_Jobs',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'Browse_Jobs',1)
    SolaceViewVars('despatch_type_temp',despatch_type_temp,'Browse_Jobs',1)
    SolaceViewVars('status2_temp',status2_temp,'Browse_Jobs',1)
    SolaceViewVars('rea_temp',rea_temp,'Browse_Jobs',1)
    SolaceViewVars('yes_temp',yes_temp,'Browse_Jobs',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Jobs',1)
    SolaceViewVars('save_lac_id',save_lac_id,'Browse_Jobs',1)
    SolaceViewVars('save_xca_id',save_xca_id,'Browse_Jobs',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Browse_Jobs',1)
    SolaceViewVars('blank_temp',blank_temp,'Browse_Jobs',1)
    SolaceViewVars('count_jobs_temp',count_jobs_temp,'Browse_Jobs',1)
    SolaceViewVars('save_xch_id',save_xch_id,'Browse_Jobs',1)
    SolaceViewVars('select_courier_temp',select_courier_temp,'Browse_Jobs',1)
    SolaceViewVars('Select_Trade_Account_Temp',Select_Trade_Account_Temp,'Browse_Jobs',1)
    SolaceViewVars('Location_Temp',Location_Temp,'Browse_Jobs',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Jobs',1)
    SolaceViewVars('multiple_despatch_temp',multiple_despatch_temp,'Browse_Jobs',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Jobs',1)
    SolaceViewVars('Completed_Temp',Completed_Temp,'Browse_Jobs',1)
    SolaceViewVars('Completed2_Temp',Completed2_Temp,'Browse_Jobs',1)
    SolaceViewVars('Invoiced_Temp',Invoiced_Temp,'Browse_Jobs',1)
    SolaceViewVars('Collected_Temp',Collected_Temp,'Browse_Jobs',1)
    SolaceViewVars('Paid_Temp',Paid_Temp,'Browse_Jobs',1)
    SolaceViewVars('status_temp',status_temp,'Browse_Jobs',1)
    SolaceViewVars('model_unit_temp',model_unit_temp,'Browse_Jobs',1)
    SolaceViewVars('Overdue_Temp',Overdue_Temp,'Browse_Jobs',1)
    SolaceViewVars('account_number_temp',account_number_temp,'Browse_Jobs',1)
    SolaceViewVars('engineer_temp',engineer_temp,'Browse_Jobs',1)
    SolaceViewVars('address_temp',address_temp,'Browse_Jobs',1)
    SolaceViewVars('model_unit2_temp',model_unit2_temp,'Browse_Jobs',1)
    SolaceViewVars('model_unit3_temp',model_unit3_temp,'Browse_Jobs',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Browse_Jobs',1)
    SolaceViewVars('Overdue2_temp',Overdue2_temp,'Browse_Jobs',1)
    SolaceViewVars('batch_number_temp',batch_number_temp,'Browse_Jobs',1)
    SolaceViewVars('courier_temp',courier_temp,'Browse_Jobs',1)
    SolaceViewVars('account_number2_temp',account_number2_temp,'Browse_Jobs',1)
    SolaceViewVars('tag_temp',tag_temp,'Browse_Jobs',1)
    SolaceViewVars('tag_count_temp',tag_count_temp,'Browse_Jobs',1)
    SolaceViewVars('despatch_batch_number_temp',despatch_batch_number_temp,'Browse_Jobs',1)
    SolaceViewVars('despatch_label_type_temp',despatch_label_type_temp,'Browse_Jobs',1)
    SolaceViewVars('despatch_label_number_temp',despatch_label_number_temp,'Browse_Jobs',1)
    SolaceViewVars('Warranty_Temp',Warranty_Temp,'Browse_Jobs',1)
    SolaceViewVars('model_unit4_temp',model_unit4_temp,'Browse_Jobs',1)
    SolaceViewVars('model_unit5_temp',model_unit5_temp,'Browse_Jobs',1)
    SolaceViewVars('select_trade_account2_temp',select_trade_account2_temp,'Browse_Jobs',1)
    SolaceViewVars('account_number3_temp',account_number3_temp,'Browse_Jobs',1)
    SolaceViewVars('select_model_number_temp',select_model_number_temp,'Browse_Jobs',1)
    SolaceViewVars('model_number2_temp',model_number2_temp,'Browse_Jobs',1)
    SolaceViewVars('Company_Name_Temp',Company_Name_Temp,'Browse_Jobs',1)
    SolaceViewVars('Address_Line1_Temp',Address_Line1_Temp,'Browse_Jobs',1)
    SolaceViewVars('Address_Line2_Temp',Address_Line2_Temp,'Browse_Jobs',1)
    SolaceViewVars('Address_Line3_Temp',Address_Line3_Temp,'Browse_Jobs',1)
    SolaceViewVars('Postcode_Temp',Postcode_Temp,'Browse_Jobs',1)
    SolaceViewVars('status3_temp',status3_temp,'Browse_Jobs',1)
    SolaceViewVars('status4_temp',status4_temp,'Browse_Jobs',1)
    SolaceViewVars('tmp:SelectLocation',tmp:SelectLocation,'Browse_Jobs',1)
    SolaceViewVars('tmp:JobLocation',tmp:JobLocation,'Browse_Jobs',1)
    SolaceViewVars('tmp:ExchangeText',tmp:ExchangeText,'Browse_Jobs',1)
    SolaceViewVars('tmp:Address',tmp:Address,'Browse_Jobs',1)
    SolaceViewVars('tmp:Location',tmp:Location,'Browse_Jobs',1)
    SolaceViewVars('tmp:ModelUnit1',tmp:ModelUnit1,'Browse_Jobs',1)
    SolaceViewVars('tmp:ModelUnit2',tmp:ModelUnit2,'Browse_Jobs',1)
    SolaceViewVars('tmp:ModelUnit3',tmp:ModelUnit3,'Browse_Jobs',1)
    SolaceViewVars('tmp:StatusDate',tmp:StatusDate,'Browse_Jobs',1)
    SolaceViewVars('tmp:ExchangeStatusDate',tmp:ExchangeStatusDate,'Browse_Jobs',1)
    SolaceViewVars('tmp:LoanStatusDate',tmp:LoanStatusDate,'Browse_Jobs',1)
    SolaceViewVars('tmp:LocationStatus',tmp:LocationStatus,'Browse_Jobs',1)
    SolaceViewVars('tmp:PrintDespatchNote',tmp:PrintDespatchNote,'Browse_Jobs',1)
    SolaceViewVars('tmp:FirstRecord',tmp:FirstRecord,'Browse_Jobs',1)
    SolaceViewVars('tmp:BatchNumber',tmp:BatchNumber,'Browse_Jobs',1)
    SolaceViewVars('tmp:BatchCreated',tmp:BatchCreated,'Browse_Jobs',1)
    SolaceViewVars('tmp:AccountSearchOrder',tmp:AccountSearchOrder,'Browse_Jobs',1)
    SolaceViewVars('tmp:GlobalIMEINumber',tmp:GlobalIMEINumber,'Browse_Jobs',1)
    SolaceViewVars('tmp:OrderNumber',tmp:OrderNumber,'Browse_Jobs',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?PrintRoutines;  SolaceCtrlName = '?PrintRoutines';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobReceipt;  SolaceCtrlName = '?JobReceipt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobCard;  SolaceCtrlName = '?JobCard';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobLabel;  SolaceCtrlName = '?JobLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VirginLabel;  SolaceCtrlName = '?VirginLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DeliveryLabel;  SolaceCtrlName = '?DeliveryLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CollectionLabel;  SolaceCtrlName = '?CollectionLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Letters;  SolaceCtrlName = '?Letters';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MailMerge;  SolaceCtrlName = '?MailMerge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ANCCollectionNote;  SolaceCtrlName = '?ANCCollectionNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DespatchNote;  SolaceCtrlName = '?DespatchNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Menu1ExchangeUnitLabel;  SolaceCtrlName = '?Menu1ExchangeUnitLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Menu1LoanUnitLabel;  SolaceCtrlName = '?Menu1LoanUnitLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Menu1Estimate;  SolaceCtrlName = '?Menu1Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Menu1Invoice;  SolaceCtrlName = '?Menu1Invoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanStatusDate;  SolaceCtrlName = '?tmp:LoanStatusDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeStatusDate;  SolaceCtrlName = '?tmp:ExchangeStatusDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusDate;  SolaceCtrlName = '?tmp:StatusDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeText;  SolaceCtrlName = '?tmp:ExchangeText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Address;  SolaceCtrlName = '?tmp:Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button_Group;  SolaceCtrlName = '?Button_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Routines;  SolaceCtrlName = '?Print_Routines';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChangeStatus;  SolaceCtrlName = '?ChangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Payment_Details;  SolaceCtrlName = '?Payment_Details';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelJob;  SolaceCtrlName = '?CancelJob';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FailedDelivery;  SolaceCtrlName = '?FailedDelivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?KeyColour;  SolaceCtrlName = '?KeyColour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job_Number_Tab;  SolaceCtrlName = '?Job_Number_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InWorkshopDatePrompt;  SolaceCtrlName = '?InWorkshopDatePrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs;  SolaceCtrlName = '?Count_Jobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InWorkshopDate_temp;  SolaceCtrlName = '?InWorkshopDate_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Esn_tab;  SolaceCtrlName = '?Esn_tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:GlobalIMEINumber:Prompt;  SolaceCtrlName = '?tmp:GlobalIMEINumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:GlobalIMEINumber;  SolaceCtrlName = '?tmp:GlobalIMEINumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:2;  SolaceCtrlName = '?Count_Jobs:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?msn_tab;  SolaceCtrlName = '?msn_tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:MSN;  SolaceCtrlName = '?job:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:3;  SolaceCtrlName = '?Count_Jobs:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Account_No_Tab;  SolaceCtrlName = '?Account_No_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountSearchOrder;  SolaceCtrlName = '?tmp:AccountSearchOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountSearchOrder:Radio1;  SolaceCtrlName = '?tmp:AccountSearchOrder:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountSearchOrder:Radio2;  SolaceCtrlName = '?tmp:AccountSearchOrder:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OrderNumber;  SolaceCtrlName = '?tmp:OrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?account_number_temp;  SolaceCtrlName = '?account_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Account_number;  SolaceCtrlName = '?Lookup_Account_number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:2;  SolaceCtrlName = '?Prompt4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Order_Number;  SolaceCtrlName = '?job:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number:10;  SolaceCtrlName = '?job:Ref_Number:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OrderNumber;  SolaceCtrlName = '?OrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JobNumber;  SolaceCtrlName = '?JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:4;  SolaceCtrlName = '?Count_Jobs:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?surname_tab;  SolaceCtrlName = '?surname_tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Surname;  SolaceCtrlName = '?job:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SearchByAddress;  SolaceCtrlName = '?SearchByAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:5;  SolaceCtrlName = '?Count_Jobs:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_type_tab;  SolaceCtrlName = '?status_type_tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SelectLocation;  SolaceCtrlName = '?tmp:SelectLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SelectLocation:Radio1;  SolaceCtrlName = '?tmp:SelectLocation:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SelectLocation:Radio2;  SolaceCtrlName = '?tmp:SelectLocation:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobLocation;  SolaceCtrlName = '?tmp:JobLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Location;  SolaceCtrlName = '?Lookup_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp;  SolaceCtrlName = '?status_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Job_Status;  SolaceCtrlName = '?Lookup_Job_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:2;  SolaceCtrlName = '?JOB:Ref_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:6;  SolaceCtrlName = '?Count_Jobs:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange_Status;  SolaceCtrlName = '?Exchange_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status3_temp;  SolaceCtrlName = '?status3_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Status3;  SolaceCtrlName = '?Lookup_Status3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25:2;  SolaceCtrlName = '?Prompt25:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:15;  SolaceCtrlName = '?Count_Jobs:15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:7;  SolaceCtrlName = '?JOB:Ref_Number:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Loan_Status;  SolaceCtrlName = '?Loan_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status4_temp;  SolaceCtrlName = '?status4_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Status4;  SolaceCtrlName = '?Lookup_Status4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:Prompt:3;  SolaceCtrlName = '?JOB:Ref_Number:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:Prompt:2;  SolaceCtrlName = '?JOB:Ref_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:8;  SolaceCtrlName = '?Count_Jobs:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:8;  SolaceCtrlName = '?JOB:Ref_Number:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mobile_tab;  SolaceCtrlName = '?mobile_tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Mobile_Number;  SolaceCtrlName = '?job:Mobile_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt13;  SolaceCtrlName = '?Prompt13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:7;  SolaceCtrlName = '?Count_Jobs:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_No_Tab;  SolaceCtrlName = '?Model_No_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?model_number_temp;  SolaceCtrlName = '?model_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Model_Number;  SolaceCtrlName = '?Lookup_Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Unit_Type;  SolaceCtrlName = '?job:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt15;  SolaceCtrlName = '?Prompt15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:9;  SolaceCtrlName = '?Count_Jobs:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Engineer_Tab;  SolaceCtrlName = '?Engineer_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?engineer_temp;  SolaceCtrlName = '?engineer_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Engineer;  SolaceCtrlName = '?Lookup_Engineer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16;  SolaceCtrlName = '?Prompt16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed2_Temp:2;  SolaceCtrlName = '?Completed2_Temp:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed_Temp:Radio1;  SolaceCtrlName = '?Completed_Temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed_Temp:Radio3;  SolaceCtrlName = '?Completed_Temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed_Temp:Radio2;  SolaceCtrlName = '?Completed_Temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:3;  SolaceCtrlName = '?JOB:Ref_Number:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt17;  SolaceCtrlName = '?Prompt17';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:10;  SolaceCtrlName = '?Count_Jobs:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Unallocated_Jobs_Tab;  SolaceCtrlName = '?Unallocated_Jobs_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:4;  SolaceCtrlName = '?JOB:Ref_Number:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt18;  SolaceCtrlName = '?Prompt18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?unallocated_temp;  SolaceCtrlName = '?unallocated_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?unallocated_temp:Radio1;  SolaceCtrlName = '?unallocated_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?unallocated_temp:Radio2;  SolaceCtrlName = '?unallocated_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UnallocatedJobsReport;  SolaceCtrlName = '?UnallocatedJobsReport';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:11;  SolaceCtrlName = '?Count_Jobs:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab16;  SolaceCtrlName = '?Tab16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Incoming_Consignment_Number;  SolaceCtrlName = '?job:Incoming_Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt30;  SolaceCtrlName = '?Prompt30';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Consignment_No_Tab;  SolaceCtrlName = '?Consignment_No_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt24;  SolaceCtrlName = '?Prompt24';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Consignment_Number:Prompt;  SolaceCtrlName = '?JOB:Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:12;  SolaceCtrlName = '?Count_Jobs:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Consignment_Number;  SolaceCtrlName = '?job:Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Batch_Number_Tab;  SolaceCtrlName = '?Batch_Number_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?batch_number_temp;  SolaceCtrlName = '?batch_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt21;  SolaceCtrlName = '?Prompt21';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed2_Temp;  SolaceCtrlName = '?Completed2_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed2_Temp:Radio1;  SolaceCtrlName = '?Completed2_Temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed2_Temp:Radio2;  SolaceCtrlName = '?Completed2_Temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Completed2_Temp:Radio3;  SolaceCtrlName = '?Completed2_Temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:Prompt;  SolaceCtrlName = '?JOB:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Despatch_Batch;  SolaceCtrlName = '?Despatch_Batch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:13;  SolaceCtrlName = '?Count_Jobs:13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:5;  SolaceCtrlName = '?JOB:Ref_Number:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Ready_To_Despatch;  SolaceCtrlName = '?Ready_To_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?select_courier_temp;  SolaceCtrlName = '?select_courier_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?select_courier_temp:Radio1;  SolaceCtrlName = '?select_courier_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?select_courier_temp:Radio2;  SolaceCtrlName = '?select_courier_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?courier_temp;  SolaceCtrlName = '?courier_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Courier;  SolaceCtrlName = '?Lookup_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt28;  SolaceCtrlName = '?Prompt28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Trade_Account_Temp;  SolaceCtrlName = '?Select_Trade_Account_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Trade_Account_Temp:Radio1;  SolaceCtrlName = '?Select_Trade_Account_Temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Trade_Account_Temp:Radio2;  SolaceCtrlName = '?Select_Trade_Account_Temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?account_number2_temp;  SolaceCtrlName = '?account_number2_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Account_Number:2;  SolaceCtrlName = '?Lookup_Account_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number:9;  SolaceCtrlName = '?job:Ref_Number:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button_Group2;  SolaceCtrlName = '?Button_Group2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Multiple_Despatch2;  SolaceCtrlName = '?Multiple_Despatch2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Individual_Despatch;  SolaceCtrlName = '?Individual_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Validate_Esn;  SolaceCtrlName = '?Validate_Esn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change_Courier;  SolaceCtrlName = '?Change_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Ref_Number:6;  SolaceCtrlName = '?JOB:Ref_Number:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt23;  SolaceCtrlName = '?Prompt23';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Count_Jobs:14;  SolaceCtrlName = '?Count_Jobs:14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt27;  SolaceCtrlName = '?Prompt27';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26;  SolaceCtrlName = '?Prompt26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26:2;  SolaceCtrlName = '?Prompt26:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26:3;  SolaceCtrlName = '?Prompt26:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Exchange_Status;  SolaceCtrlName = '?job:Exchange_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Loan_Status;  SolaceCtrlName = '?job:Loan_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MarkLine;  SolaceCtrlName = '?MarkLine';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Current_Status;  SolaceCtrlName = '?job:Current_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Jobs','?Browse:1')        !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Jobs')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Jobs')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:LoanStatusDate
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Jobs'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS.Open
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHACC.Open
  Relate:JOBBATCH.Open
  Relate:MULDESP.Open
  Relate:MULDESP_ALIAS.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:USERS.UseFile
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:LOCINTER.UseFile
  Access:JOBACC.UseFile
  Access:SUBTRACC.UseFile
  Access:MODELNUM.UseFile
  Access:EXCHANGE.UseFile
  Access:LOAN.UseFile
  Access:COURIER.UseFile
  Access:MULDESPJ.UseFile
  Access:INVOICE.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  Set(Defaults)
  access:defaults.next()
  
  ! Start Change 2618 BE(05/08/03)
  JSC:Colour[1] = COLOR:Maroon
  JSC:Status[1] = GETINI('JobStatusColours','Maroon',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[2] = COLOR:Green
  JSC:Status[2] = GETINI('JobStatusColours','Green',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[3] = COLOR:Olive
  JSC:Status[3] = GETINI('JobStatusColours','Olive',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[4] = COLOR:Navy
  JSC:Status[4] = GETINI('JobStatusColours','Navy',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[5] = COLOR:Purple
  JSC:Status[5] = GETINI('JobStatusColours','Purple',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[6] = COLOR:Teal
  JSC:Status[6] = GETINI('JobStatusColours','Teal',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[7] = COLOR:Gray
  JSC:Status[7] = GETINI('JobStatusColours','Gray',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[8] = COLOR:Blue
  JSC:Status[8] = GETINI('JobStatusColours','Blue',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2618 BE(05/08/03)
  
  ! Start Change xxxx BE(9/11/2004)
  set(jbt:Batch_number_Key)
  access:jobbatch.previous()
  batch_number_temp = jbt:Batch_Number
  ! End Change xxxx BE(9/11/2004)
  
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbk01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ! support for CPCS
  IF ?account_number_temp{Prop:Tip} AND ~?Lookup_Account_number{Prop:Tip}
     ?Lookup_Account_number{Prop:Tip} = 'Select ' & ?account_number_temp{Prop:Tip}
  END
  IF ?account_number_temp{Prop:Msg} AND ~?Lookup_Account_number{Prop:Msg}
     ?Lookup_Account_number{Prop:Msg} = 'Select ' & ?account_number_temp{Prop:Msg}
  END
  IF ?status_temp{Prop:Tip} AND ~?Lookup_Job_Status{Prop:Tip}
     ?Lookup_Job_Status{Prop:Tip} = 'Select ' & ?status_temp{Prop:Tip}
  END
  IF ?status_temp{Prop:Msg} AND ~?Lookup_Job_Status{Prop:Msg}
     ?Lookup_Job_Status{Prop:Msg} = 'Select ' & ?status_temp{Prop:Msg}
  END
  IF ?status3_temp{Prop:Tip} AND ~?Lookup_Status3{Prop:Tip}
     ?Lookup_Status3{Prop:Tip} = 'Select ' & ?status3_temp{Prop:Tip}
  END
  IF ?status3_temp{Prop:Msg} AND ~?Lookup_Status3{Prop:Msg}
     ?Lookup_Status3{Prop:Msg} = 'Select ' & ?status3_temp{Prop:Msg}
  END
  IF ?status4_temp{Prop:Tip} AND ~?Lookup_Status4{Prop:Tip}
     ?Lookup_Status4{Prop:Tip} = 'Select ' & ?status4_temp{Prop:Tip}
  END
  IF ?status4_temp{Prop:Msg} AND ~?Lookup_Status4{Prop:Msg}
     ?Lookup_Status4{Prop:Msg} = 'Select ' & ?status4_temp{Prop:Msg}
  END
  IF ?tmp:JobLocation{Prop:Tip} AND ~?Lookup_Location{Prop:Tip}
     ?Lookup_Location{Prop:Tip} = 'Select ' & ?tmp:JobLocation{Prop:Tip}
  END
  IF ?tmp:JobLocation{Prop:Msg} AND ~?Lookup_Location{Prop:Msg}
     ?Lookup_Location{Prop:Msg} = 'Select ' & ?tmp:JobLocation{Prop:Msg}
  END
  IF ?model_number_temp{Prop:Tip} AND ~?Lookup_Model_Number{Prop:Tip}
     ?Lookup_Model_Number{Prop:Tip} = 'Select ' & ?model_number_temp{Prop:Tip}
  END
  IF ?model_number_temp{Prop:Msg} AND ~?Lookup_Model_Number{Prop:Msg}
     ?Lookup_Model_Number{Prop:Msg} = 'Select ' & ?model_number_temp{Prop:Msg}
  END
  IF ?courier_temp{Prop:Tip} AND ~?Lookup_Courier{Prop:Tip}
     ?Lookup_Courier{Prop:Tip} = 'Select ' & ?courier_temp{Prop:Tip}
  END
  IF ?courier_temp{Prop:Msg} AND ~?Lookup_Courier{Prop:Msg}
     ?Lookup_Courier{Prop:Msg} = 'Select ' & ?courier_temp{Prop:Msg}
  END
  IF ?account_number2_temp{Prop:Tip} AND ~?Lookup_Account_Number:2{Prop:Tip}
     ?Lookup_Account_Number:2{Prop:Tip} = 'Select ' & ?account_number2_temp{Prop:Tip}
  END
  IF ?account_number2_temp{Prop:Msg} AND ~?Lookup_Account_Number:2{Prop:Msg}
     ?Lookup_Account_Number:2{Prop:Msg} = 'Select ' & ?account_number2_temp{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,job:ESN_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?job:ESN,job:ESN,1,BRW1)
  BRW1.AddSortOrder(,job:MSN_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?job:MSN,job:MSN,1,BRW1)
  BRW1.AddSortOrder(,job:AccOrdNoKey)
  BRW1.AddRange(job:Account_Number,account_number_temp)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?JOB:Order_Number,job:Order_Number,1,BRW1)
  BRW1.AddSortOrder(,job:AccountNumberKey)
  BRW1.AddRange(job:Account_Number,account_number_temp)
  BRW1.AddLocator(BRW1::Sort27:Locator)
  BRW1::Sort27:Locator.Init(?job:Ref_Number:10,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Surname_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?job:Surname,job:Surname,1,BRW1)
  BRW1.AddSortOrder(,job:By_Status)
  BRW1.AddRange(job:Current_Status,status_temp)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?JOB:Ref_Number:2,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:StatusLocKey)
  BRW1.AddRange(job:Location,tmp:JobLocation)
  BRW1.AddLocator(BRW1::Sort25:Locator)
  BRW1::Sort25:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ExcStatusKey)
  BRW1.AddRange(job:Exchange_Status,status3_temp)
  BRW1.AddLocator(BRW1::Sort23:Locator)
  BRW1::Sort23:Locator.Init(?JOB:Ref_Number:7,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:LoanStatusKey)
  BRW1.AddRange(job:Loan_Status,status4_temp)
  BRW1.AddLocator(BRW1::Sort24:Locator)
  BRW1::Sort24:Locator.Init(?JOB:Ref_Number:8,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:MobileNumberKey)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?job:Mobile_Number,job:Mobile_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Model_Unit_Key)
  BRW1.AddRange(job:Model_Number,model_number_temp)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?job:Unit_Type,job:Unit_Type,1,BRW1)
  BRW1.AddSortOrder(,job:EngCompKey)
  BRW1.AddRange(job:Engineer,engineer_temp)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?JOB:Ref_Number:3,job:Completed,1,BRW1)
  BRW1.SetFilter('(Upper(job:completed) = Upper(yes_temp))')
  BRW1.AddSortOrder(,job:EngCompKey)
  BRW1.AddRange(job:Engineer,engineer_temp)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?JOB:Ref_Number:3,job:Completed,1,BRW1)
  BRW1.SetFilter('(Upper(job:completed) = Upper(no_temp))')
  BRW1.AddSortOrder(,job:Engineer_Key)
  BRW1.AddRange(job:Engineer,engineer_temp)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?JOB:Ref_Number:3,job:Model_Number,1,BRW1)
  BRW1.AddSortOrder(,job:EngWorkKey)
  BRW1.AddRange(job:Engineer,blank_temp)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?JOB:Ref_Number:4,job:Workshop,1,BRW1)
  BRW1.SetFilter('(Upper(job:workshop) = ''YES'')')
  BRW1.AddSortOrder(,job:EngWorkKey)
  BRW1.AddRange(job:Engineer,blank_temp)
  BRW1.AddLocator(BRW1::Sort28:Locator)
  BRW1::Sort28:Locator.Init(,job:Workshop,1,BRW1)
  BRW1.SetFilter('(Upper(job:workshop) <<> ''YES'')')
  BRW1.AddSortOrder(,job:InConsignKey)
  BRW1.AddLocator(BRW1::Sort26:Locator)
  BRW1::Sort26:Locator.Init(?job:Incoming_Consignment_Number,job:Incoming_Consignment_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ConsignmentNoKey)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?job:Consignment_Number,job:Consignment_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Batch_Number_Key)
  BRW1.AddRange(job:Batch_Number,batch_number_temp)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?JOB:Ref_Number:5,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:BatchCompKey)
  BRW1.AddRange(job:Batch_Number,batch_number_temp)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?JOB:Ref_Number:5,job:Completed,1,BRW1)
  BRW1.SetFilter('(Upper(job:Completed) = ''NO'')')
  BRW1.AddSortOrder(,job:BatchCompKey)
  BRW1.AddRange(job:Batch_Number,batch_number_temp)
  BRW1.AddLocator(BRW1::Sort16:Locator)
  BRW1::Sort16:Locator.Init(?JOB:Ref_Number:5,job:Completed,1,BRW1)
  BRW1.SetFilter('(Upper(job:Completed) = ''YES'')')
  BRW1.AddSortOrder(,job:ReadyToDespKey)
  BRW1.AddRange(job:Despatched,rea_temp)
  BRW1.AddLocator(BRW1::Sort17:Locator)
  BRW1::Sort17:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ReadyToTradeKey)
  BRW1.AddRange(job:Account_Number,account_number2_temp)
  BRW1.AddLocator(BRW1::Sort18:Locator)
  BRW1::Sort18:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ReadyToCouKey)
  BRW1.AddRange(job:Current_Courier,courier_temp)
  BRW1.AddLocator(BRW1::Sort19:Locator)
  BRW1::Sort19:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ReadyToAllKey)
  BRW1.AddRange(job:Current_Courier,courier_temp)
  BRW1.AddLocator(BRW1::Sort20:Locator)
  BRW1::Sort20:Locator.Init(?JOB:Ref_Number:6,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:DespJobNumberKey)
  BRW1.AddRange(job:Despatch_Number,despatch_batch_number_temp)
  BRW1.AddLocator(BRW1::Sort21:Locator)
  BRW1::Sort21:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BIND('model_unit_temp',model_unit_temp)
  BIND('Overdue_Temp',Overdue_Temp)
  BIND('Completed_Temp',Completed_Temp)
  BIND('tag_temp',tag_temp)
  BIND('Invoiced_Temp',Invoiced_Temp)
  BIND('Warranty_Temp',Warranty_Temp)
  BIND('Collected_Temp',Collected_Temp)
  BIND('Paid_Temp',Paid_Temp)
  BIND('address_temp',address_temp)
  BIND('model_unit2_temp',model_unit2_temp)
  BIND('model_unit3_temp',model_unit3_temp)
  BIND('model_unit4_temp',model_unit4_temp)
  BIND('model_unit5_temp',model_unit5_temp)
  BIND('despatch_type_temp',despatch_type_temp)
  BIND('status2_temp',status2_temp)
  BIND('tmp:ExchangeText',tmp:ExchangeText)
  BIND('tmp:Address',tmp:Address)
  BIND('tmp:Location',tmp:Location)
  BIND('tmp:StatusDate',tmp:StatusDate)
  BIND('tmp:ModelUnit1',tmp:ModelUnit1)
  BIND('tmp:ModelUnit2',tmp:ModelUnit2)
  BIND('tmp:ModelUnit3',tmp:ModelUnit3)
  BIND('tmp:ExchangeStatusDate',tmp:ExchangeStatusDate)
  BIND('tmp:LoanStatusDate',tmp:LoanStatusDate)
  BIND('tmp:LocationStatus',tmp:LocationStatus)
  BIND('Postcode_Temp',Postcode_Temp)
  BIND('Address_Line3_Temp',Address_Line3_Temp)
  BIND('Address_Line2_Temp',Address_Line2_Temp)
  BIND('Address_Line1_Temp',Address_Line1_Temp)
  BIND('Company_Name_Temp',Company_Name_Temp)
  BIND('status_temp',status_temp)
  BIND('engineer_temp',engineer_temp)
  BIND('model_number_temp',model_number_temp)
  BIND('batch_number_temp',batch_number_temp)
  BIND('courier_temp',courier_temp)
  BIND('account_number2_temp',account_number2_temp)
  BIND('yes_temp',yes_temp)
  BIND('no_temp',no_temp)
  BIND('blank_temp',blank_temp)
  BIND('rea_temp',rea_temp)
  ?Browse:1{PROP:IconList,1} = '~Block_Blue.ico'
  ?Browse:1{PROP:IconList,2} = '~Block_Blue_R.ico'
  ?Browse:1{PROP:IconList,3} = '~Block_Blue_left.ico'
  ?Browse:1{PROP:IconList,4} = '~Block_Cyan.ico'
  ?Browse:1{PROP:IconList,5} = '~bl_y_lt.ico'
  ?Browse:1{PROP:IconList,6} = '~block_green.ico'
  ?Browse:1{PROP:IconList,7} = '~block_red.ico'
  ?Browse:1{PROP:IconList,8} = '~block_red_left.ico'
  ?Browse:1{PROP:IconList,9} = '~block_red_right.ico'
  ?Browse:1{PROP:IconList,10} = '~block_yellow.ico'
  ?Browse:1{PROP:IconList,11} = '~notick1.ico'
  ?Browse:1{PROP:IconList,12} = '~tick1.ico'
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(model_unit_temp,BRW1.Q.model_unit_temp)
  BRW1.AddField(job:date_booked,BRW1.Q.job:date_booked)
  BRW1.AddField(job:Current_Status,BRW1.Q.job:Current_Status)
  BRW1.AddField(Overdue_Temp,BRW1.Q.Overdue_Temp)
  BRW1.AddField(Completed_Temp,BRW1.Q.Completed_Temp)
  BRW1.AddField(tag_temp,BRW1.Q.tag_temp)
  BRW1.AddField(Invoiced_Temp,BRW1.Q.Invoiced_Temp)
  BRW1.AddField(Warranty_Temp,BRW1.Q.Warranty_Temp)
  BRW1.AddField(Collected_Temp,BRW1.Q.Collected_Temp)
  BRW1.AddField(Paid_Temp,BRW1.Q.Paid_Temp)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(job:MSN,BRW1.Q.job:MSN)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(job:Surname,BRW1.Q.job:Surname)
  BRW1.AddField(job:Mobile_Number,BRW1.Q.job:Mobile_Number)
  BRW1.AddField(job:Postcode,BRW1.Q.job:Postcode)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:Unit_Type,BRW1.Q.job:Unit_Type)
  BRW1.AddField(address_temp,BRW1.Q.address_temp)
  BRW1.AddField(model_unit2_temp,BRW1.Q.model_unit2_temp)
  BRW1.AddField(model_unit3_temp,BRW1.Q.model_unit3_temp)
  BRW1.AddField(model_unit4_temp,BRW1.Q.model_unit4_temp)
  BRW1.AddField(model_unit5_temp,BRW1.Q.model_unit5_temp)
  BRW1.AddField(job:Consignment_Number,BRW1.Q.job:Consignment_Number)
  BRW1.AddField(job:Current_Courier,BRW1.Q.job:Current_Courier)
  BRW1.AddField(despatch_type_temp,BRW1.Q.despatch_type_temp)
  BRW1.AddField(status2_temp,BRW1.Q.status2_temp)
  BRW1.AddField(job:Loan_Status,BRW1.Q.job:Loan_Status)
  BRW1.AddField(job:Exchange_Status,BRW1.Q.job:Exchange_Status)
  BRW1.AddField(job:Location,BRW1.Q.job:Location)
  BRW1.AddField(job:Incoming_Consignment_Number,BRW1.Q.job:Incoming_Consignment_Number)
  BRW1.AddField(job:Exchange_Unit_Number,BRW1.Q.job:Exchange_Unit_Number)
  BRW1.AddField(job:Loan_Unit_Number,BRW1.Q.job:Loan_Unit_Number)
  BRW1.AddField(tmp:ExchangeText,BRW1.Q.tmp:ExchangeText)
  BRW1.AddField(tmp:Address,BRW1.Q.tmp:Address)
  BRW1.AddField(tmp:Location,BRW1.Q.tmp:Location)
  BRW1.AddField(tmp:StatusDate,BRW1.Q.tmp:StatusDate)
  BRW1.AddField(tmp:ModelUnit1,BRW1.Q.tmp:ModelUnit1)
  BRW1.AddField(tmp:ModelUnit2,BRW1.Q.tmp:ModelUnit2)
  BRW1.AddField(tmp:ModelUnit3,BRW1.Q.tmp:ModelUnit3)
  BRW1.AddField(tmp:ExchangeStatusDate,BRW1.Q.tmp:ExchangeStatusDate)
  BRW1.AddField(tmp:LoanStatusDate,BRW1.Q.tmp:LoanStatusDate)
  BRW1.AddField(tmp:LocationStatus,BRW1.Q.tmp:LocationStatus)
  BRW1.AddField(job:Address_Line1,BRW1.Q.job:Address_Line1)
  BRW1.AddField(job:Company_Name,BRW1.Q.job:Company_Name)
  BRW1.AddField(job:Address_Line2,BRW1.Q.job:Address_Line2)
  BRW1.AddField(job:Address_Line3,BRW1.Q.job:Address_Line3)
  BRW1.AddField(Postcode_Temp,BRW1.Q.Postcode_Temp)
  BRW1.AddField(Address_Line3_Temp,BRW1.Q.Address_Line3_Temp)
  BRW1.AddField(Address_Line2_Temp,BRW1.Q.Address_Line2_Temp)
  BRW1.AddField(Address_Line1_Temp,BRW1.Q.Address_Line1_Temp)
  BRW1.AddField(Company_Name_Temp,BRW1.Q.Company_Name_Temp)
  BRW1.AddField(job:Engineer,BRW1.Q.job:Engineer)
  BRW1.AddField(job:Completed,BRW1.Q.job:Completed)
  BRW1.AddField(job:Workshop,BRW1.Q.job:Workshop)
  BRW1.AddField(job:Batch_Number,BRW1.Q.job:Batch_Number)
  BRW1.AddField(job:Despatched,BRW1.Q.job:Despatched)
  BRW1.AddField(job:Despatch_Number,BRW1.Q.job:Despatch_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 9
  BRW1.popup.additem('Job Reports','Popup2','',1)
  BRW1.popup.seticon('Popup2','Point.ico')
  BRW1.popup.additem('Job Receipt','SPopup21','Popup2',2)
  BRW1.popup.additemevent('SPopup21',event:accepted,?JobReceipt)
  BRW1.popup.seticon('SPopup21','receipt.ico')
  BRW1.popup.additem('Job Card','SPopup22','SPopup1',2)
  BRW1.popup.additemevent('SPopup22',event:accepted,?JobCard)
  BRW1.popup.seticon('SPopup22','jobcard.ico')
  BRW1.popup.additem('Job Label','SPopup23','SPopup2',2)
  BRW1.popup.additemevent('SPopup23',event:accepted,?JobLabel)
  BRW1.popup.seticon('SPopup23','label.ico')
  BRW1.popup.additem('Virgin Label','SPopup24','SPopup3',2)
  BRW1.popup.additemevent('SPopup24',event:accepted,?VirginLabel)
  BRW1.popup.seticon('SPopup24','label.ico')
  BRW1.popup.additem('-','SPopup25','SPopup4',2)
  BRW1.popup.additem('Letters','SPopup26','SPopup5',2)
  BRW1.popup.additemevent('SPopup26',event:accepted,?Letters)
  BRW1.popup.seticon('SPopup26','letter.ico')
  BRW1.popup.additem('Mail Merge','SPopup27','SPopup6',2)
  BRW1.popup.additemevent('SPopup27',event:accepted,?MailMerge)
  BRW1.popup.seticon('SPopup27','letter.ico')
  BRW1.popup.additem('Coll/Desp Reports','Popup3','SPopup7',1)
  BRW1.popup.seticon('Popup3','Point.ico')
  BRW1.popup.additem('ANC Collection Note','SPopup31','Popup3',2)
  BRW1.popup.additemevent('SPopup31',event:accepted,?ANCCollectionNote)
  BRW1.popup.seticon('SPopup31','label.ico')
  BRW1.popup.additem('Despatch Note','SPopup32','SPopup1',2)
  BRW1.popup.additemevent('SPopup32',event:accepted,?DespatchNote)
  BRW1.popup.seticon('SPopup32','jobcard.ico')
  BRW1.popup.additem('-','SPopup33','SPopup2',2)
  BRW1.popup.additem('Delivery Label','SPopup34','SPopup3',2)
  BRW1.popup.additemevent('SPopup34',event:accepted,?DeliveryLabel)
  BRW1.popup.seticon('SPopup34','label.ico')
  BRW1.popup.additem('Collection Label','SPopup35','SPopup4',2)
  BRW1.popup.additemevent('SPopup35',event:accepted,?CollectionLabel)
  BRW1.popup.seticon('SPopup35','label.ico')
  BRW1.popup.additem('Loan/Exchange Reports','Popup4','SPopup5',1)
  BRW1.popup.seticon('Popup4','Point.ico')
  BRW1.popup.additem('Exchange Unit Label','SPopup41','Popup4',2)
  BRW1.popup.additemevent('SPopup41',event:accepted,?Menu1ExchangeUnitLabel)
  BRW1.popup.seticon('SPopup41','label.ico')
  BRW1.popup.additem('Loan Unit Label','SPopup42','SPopup1',2)
  BRW1.popup.additemevent('SPopup42',event:accepted,?Menu1LoanUnitLabel)
  BRW1.popup.seticon('SPopup42','label.ico')
  BRW1.popup.additem('Financial Reports','Popup5','SPopup2',1)
  BRW1.popup.seticon('Popup5','Point.ico')
  BRW1.popup.additem('Estimate','SPopup51','Popup5',2)
  BRW1.popup.additemevent('SPopup51',event:accepted,?Menu1Estimate)
  BRW1.popup.seticon('SPopup51','receipt.ico')
  BRW1.popup.additem('Invoice','SPopup52','SPopup1',2)
  BRW1.popup.additemevent('SPopup52',event:accepted,?Menu1Invoice)
  BRW1.popup.seticon('SPopup52','jobcard.ico')
  BRW1.popup.additem('-','EPopup52','SPopup2',1)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:q_JobNumber)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:q_JobNumber)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS.Close
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHACC.Close
    Relate:JOBBATCH.Close
    Relate:MULDESP.Close
    Relate:MULDESP_ALIAS.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Jobs','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Jobs'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Jobs',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  BRW1.Popup.SetItemEnable('Popup2',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup21',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup22',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup23',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup24',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup25',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup26',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup27',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('Popup3',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup31',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup32',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup33',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup34',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup35',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('Popup4',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup41',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup42',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('Popup5',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup51',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup52',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  do_update# = true
  
  If number = 9
      case request
          of insertrecord
              check_access('JOBS - INSERT',x")
              if x" = false
                  case messageex('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      of 1 ! &ok button
                  end!case messageex
                  do_update# = false
              end
          of changerecord
              check_access('JOBS - CHANGE',x")
              if x" = false
                  case messageex('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      of 1 ! &ok button
                  end!case messageex
                  do_update# = false
              end
          of deleterecord
              check_access('JOBS - DELETE',x")
              if x" = false
                  case messageex('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      of 1 ! &ok button
                  end!case messageex
                  do_update# = false
              end
      end !case request
  End!If number = 9
  
  if do_update# = true
      If number = 9
          location_temp = ''
          !If you are deleting a job, save the location so you
          !can make a space available.
          If request = DeleteRecord
              If job:date_completed = ''
                  location_temp = job:location
              End!If job:date_completed = ''
          Else !If request = DeleteRecord
              If job:cancelled = 'YES'
                  request = Viewrecord
              End!If job:cancelled = 'YES'
          End!If request = DeleteRecord
  
      End!If number = 9
  ReturnValue = PARENT.Run(Number,Request)
  ! Start Chenge xxx BE(27/10/2004)
  SET(DEFAULTS)
  access:defaults.next()
  IF Request = InsertRecord AND Number = 9 AND (NOT def:UseOriginalBookingtemplate) THEN
    GlobalRequest = Request
    ! *******************************
    ! Use Rapid Booking Form
    Update_JOBS
    ! ***************************
    ReturnValue = GlobalResponse
  ELSE
  ! End Chenge xxx BE(27/10/2004)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSubAccounts
      PickAvailableLocations
      PickJobStatus
      PickExchangeStatus
      PickLoanStatus
      Browse_Model_Numbers
      PickCouriers
      PickSubAccounts
      UpdateJOBS
    END
    ReturnValue = GlobalResponse
  END
  ! Start Chenge xxx BE(27/10/2004)
  END
  ! End Chenge xxx BE(27/10/2004)
      If number = 9
          If request = DeleteRecord And Globalresponse = 1
              access:locinter.clearkey(loi:location_key)
              If location_temp <> ''
                loi:location = location_temp
                if access:locinter.fetch(loi:location_key) = Level:Benign
                    loi:current_spaces += 1
                    If loi:location_available = 'NO'
                        loi:location_available = 'YES'
                    End
                    access:locinter.update()
                end
              End!loi:location_available
          End
  
      End!If number = 9
  End !If do_update# = True
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Xplore1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  Xplore1.AddField(model_unit_temp,BRW1.Q.model_unit_temp)
  Xplore1.AddField(job:date_booked,BRW1.Q.job:date_booked)
  Xplore1.AddField(job:Current_Status,BRW1.Q.job:Current_Status)
  Xplore1.AddField(Overdue_Temp,BRW1.Q.Overdue_Temp)
  Xplore1.AddField(Completed_Temp,BRW1.Q.Completed_Temp)
  Xplore1.AddField(tag_temp,BRW1.Q.tag_temp)
  Xplore1.AddField(Invoiced_Temp,BRW1.Q.Invoiced_Temp)
  Xplore1.AddField(Warranty_Temp,BRW1.Q.Warranty_Temp)
  Xplore1.AddField(Collected_Temp,BRW1.Q.Collected_Temp)
  Xplore1.AddField(Paid_Temp,BRW1.Q.Paid_Temp)
  Xplore1.AddField(job:ESN,BRW1.Q.job:ESN)
  Xplore1.AddField(job:MSN,BRW1.Q.job:MSN)
  Xplore1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  Xplore1.AddField(job:Surname,BRW1.Q.job:Surname)
  Xplore1.AddField(job:Mobile_Number,BRW1.Q.job:Mobile_Number)
  Xplore1.AddField(job:Postcode,BRW1.Q.job:Postcode)
  Xplore1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  Xplore1.AddField(job:Unit_Type,BRW1.Q.job:Unit_Type)
  Xplore1.AddField(address_temp,BRW1.Q.address_temp)
  Xplore1.AddField(model_unit2_temp,BRW1.Q.model_unit2_temp)
  Xplore1.AddField(model_unit3_temp,BRW1.Q.model_unit3_temp)
  Xplore1.AddField(model_unit4_temp,BRW1.Q.model_unit4_temp)
  Xplore1.AddField(model_unit5_temp,BRW1.Q.model_unit5_temp)
  Xplore1.AddField(job:Consignment_Number,BRW1.Q.job:Consignment_Number)
  Xplore1.AddField(job:Current_Courier,BRW1.Q.job:Current_Courier)
  Xplore1.AddField(despatch_type_temp,BRW1.Q.despatch_type_temp)
  Xplore1.AddField(status2_temp,BRW1.Q.status2_temp)
  Xplore1.AddField(job:Loan_Status,BRW1.Q.job:Loan_Status)
  Xplore1.AddField(job:Exchange_Status,BRW1.Q.job:Exchange_Status)
  Xplore1.AddField(job:Location,BRW1.Q.job:Location)
  Xplore1.AddField(job:Incoming_Consignment_Number,BRW1.Q.job:Incoming_Consignment_Number)
  Xplore1.AddField(job:Exchange_Unit_Number,BRW1.Q.job:Exchange_Unit_Number)
  Xplore1.AddField(job:Loan_Unit_Number,BRW1.Q.job:Loan_Unit_Number)
  Xplore1.AddField(tmp:ExchangeText,BRW1.Q.tmp:ExchangeText)
  Xplore1.AddField(tmp:Address,BRW1.Q.tmp:Address)
  Xplore1.AddField(tmp:Location,BRW1.Q.tmp:Location)
  Xplore1.AddField(tmp:StatusDate,BRW1.Q.tmp:StatusDate)
  Xplore1.AddField(tmp:ModelUnit1,BRW1.Q.tmp:ModelUnit1)
  Xplore1.AddField(tmp:ModelUnit2,BRW1.Q.tmp:ModelUnit2)
  Xplore1.AddField(tmp:ModelUnit3,BRW1.Q.tmp:ModelUnit3)
  Xplore1.AddField(tmp:ExchangeStatusDate,BRW1.Q.tmp:ExchangeStatusDate)
  Xplore1.AddField(tmp:LoanStatusDate,BRW1.Q.tmp:LoanStatusDate)
  Xplore1.AddField(tmp:LocationStatus,BRW1.Q.tmp:LocationStatus)
  Xplore1.AddField(job:Address_Line1,BRW1.Q.job:Address_Line1)
  Xplore1.AddField(job:Company_Name,BRW1.Q.job:Company_Name)
  Xplore1.AddField(job:Address_Line2,BRW1.Q.job:Address_Line2)
  Xplore1.AddField(job:Address_Line3,BRW1.Q.job:Address_Line3)
  Xplore1.AddField(Postcode_Temp,BRW1.Q.Postcode_Temp)
  Xplore1.AddField(Address_Line3_Temp,BRW1.Q.Address_Line3_Temp)
  Xplore1.AddField(Address_Line2_Temp,BRW1.Q.Address_Line2_Temp)
  Xplore1.AddField(Address_Line1_Temp,BRW1.Q.Address_Line1_Temp)
  Xplore1.AddField(Company_Name_Temp,BRW1.Q.Company_Name_Temp)
  Xplore1.AddField(job:Engineer,BRW1.Q.job:Engineer)
  Xplore1.AddField(job:Completed,BRW1.Q.job:Completed)
  Xplore1.AddField(job:Workshop,BRW1.Q.job:Workshop)
  Xplore1.AddField(job:Batch_Number,BRW1.Q.job:Batch_Number)
  Xplore1.AddField(job:Despatched,BRW1.Q.job:Despatched)
  Xplore1.AddField(job:Despatch_Number,BRW1.Q.job:Despatch_Number)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?job:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
          !Added by Neil - Code to strip 18 char IMEI's and 15 char MSN's
          IF LEN(CLIP(job:ESN)) = 18
            !Ericsson IMEI!
            Job:ESN = SUB(job:ESN,4,15)
            !ESN_Entry_Temp = Job:ESN
            UPDATE()
          ELSE
            !Job:ESN = ESN_Entry_Temp
          END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
    OF ?unallocated_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?unallocated_temp, Accepted)
      Thiswindow.reset  ! Change 2400 BE(21/03/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?unallocated_temp, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?JobReceipt
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobReceipt, Accepted)
      Do JobReceipt
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobReceipt, Accepted)
    OF ?JobCard
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobCard, Accepted)
      Do JobCard
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobCard, Accepted)
    OF ?JobLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobLabel, Accepted)
      Do JobLabel
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JobLabel, Accepted)
    OF ?VirginLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VirginLabel, Accepted)
      ! Start Change 4120 BE(31/08/2004)
      DO VirginLabel
      ! End Change 4120 BE(31/08/2004)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?VirginLabel, Accepted)
    OF ?DeliveryLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeliveryLabel, Accepted)
      glo:Select1 = brw1.q.job:Ref_Number
      Address_Label('DEL','')
      glo:Select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DeliveryLabel, Accepted)
    OF ?CollectionLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CollectionLabel, Accepted)
      glo:Select1 = brw1.q.job:Ref_Number
      Address_Label('COL','')
      glo:Select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CollectionLabel, Accepted)
    OF ?Letters
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Letters, Accepted)
      Do letters
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Letters, Accepted)
    OF ?MailMerge
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MailMerge, Accepted)
      Do MailMerge
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MailMerge, Accepted)
    OF ?ANCCollectionNote
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ANCCollectionNote, Accepted)
      Do ANCCollNote
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ANCCollectionNote, Accepted)
    OF ?DespatchNote
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DespatchNote, Accepted)
      Do DespatchNote
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?DespatchNote, Accepted)
    OF ?Menu1ExchangeUnitLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1ExchangeUnitLabel, Accepted)
      Do ExchangeUnitLabel
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1ExchangeUnitLabel, Accepted)
    OF ?Menu1LoanUnitLabel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1LoanUnitLabel, Accepted)
      Do LoanUnitLabel
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1LoanUnitLabel, Accepted)
    OF ?Menu1Estimate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Estimate, Accepted)
      Do Estimate
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Estimate, Accepted)
    OF ?Menu1Invoice
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Invoice, Accepted)
      DO Invoice
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Menu1Invoice, Accepted)
    OF ?Browse:1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Accepted)
      ! Start Change 2239 BE(30/04/03)
      Access:jobse.Clearkey(jobe:refnumberkey)
      jobe:refnumber  = brw1.q.job:ref_number
      IF (Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign) THEN
          ! Start Change 2239/2 BE(09/05/03)
          !InWorkshopDate_temp = jobe:InWorkshopDate
          IF (jobe:InWorkshopDate = '') THEN
             InWorkshopDate_temp = ''
             HIDE(?InWorkshopDatePrompt)
          ELSE
              InWorkShopDate_temp = jobe:InWorkshopDate
              UNHIDE(?InWorkshopDatePrompt)
          END
          ! End Change 2239/2 BE(09/05/03)
      ELSE
          InWorkshopDate_temp = ''
          ! Start Change 2239/2 BE(09/05/03)
          HIDE(?InWorkshopDatePrompt)
          ! End Change 2239/2 BE(09/05/03)
      END
      DISPLAY(?InWorkshopDate_temp)
      ! End Change 2239 BE(30/04/03)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Accepted)
    OF ?Print_Routines
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
      check_access('PRINT ROUTINES',x")
      If x" = False
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If x# = False
      
          Thiswindow.reset
          glo:select1  = job:ref_number
          Print_Routines
          glo:select1 = ''
      End!If x# = False
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
    OF ?ChangeStatus
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeStatus, Accepted)
      check_access('CHANGE STATUS',x")
      If x" = False
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If x" = False
          Thiswindow.reset
          error# = 0
          clear(sts:record)
          access:status.clearkey(sts:status_key)
          sts:status = job:current_status
          if access:status.fetch(sts:status_key) = Level:Benign
              If sts:ref_number = 799
                  check_access('JOBS - UNCANCEL',x")
                  If x" = False
                      Case MessageEx('This job has been Cancelled and you do not have a sufficient access level to Uncancel it.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  Else!If x" = False
                      Case MessageEx('This job has been Cancelled. If you proceed it will be Uncancelled.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              job:cancelled = 'NO'
                              access:jobs.update()
                          Of 2 ! &No Button
                              error# = 1
                      End!Case MessageEx
      
                  End!If x" = False
              End!If sts:ref_number = 799
          End!if access:status.fetch(sts:status_key) = Level:Benign
          If error# = 0
              Change_Status(job:ref_number)
          End!If error# = 0
      End!If x" = False
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChangeStatus, Accepted)
    OF ?Payment_Details
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment_Details, Accepted)
      check_access('PAYMENT DETAILS',passed")
      if passed" = False
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',icon:hand,'|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessagheEx
      Else!if passed" = False
      
          Thiswindow.reset(1)
          access:jobs_alias.clearkey(job_ali:ref_number_key)
          job_ali:ref_number = brw1.q.job:ref_number
          If access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
              continue# = 1
              If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
                  continue# = 0
                    Case MessageEx('This job is marked as a Warranty Job Only.<13,10><13,10>It can only be marked as paid by Reconciling the Claim.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
              End!If job_ali:warranty_job = 'YES' And job_ali:chargeable_job <> 'YES'
              If job_ali:date_completed = '' And continue# = 1
                  Case MessageEx('This job has not been completed.<13,10><13,10>Are you sure you want to add/amend a payment?','ServiceBase 2000',icon:question,'|&OK|&Cancel',2,2,'',,'Arial',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,0+2+0+0,84,26,0) 
                      Of 1 ! &OK Button
                          continue# = 1
                      Of 2 ! &Cancel Button
                          continue# = 0
                  End!Case MessagheEx
              End!If job_ali:date_completed = ''
      
              If continue# = 1
              
                  glo:select1 = brw1.q.job:ref_number
      
                  Browse_Payments
      
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = glo:select1
                  if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      Include('ChkPaid.inc')
                  End!if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  glo:select1 = ''
              End!If continue# = 1
          End!If access:jobs_alias.fetch(job_ali:ref_number_key) = Level:Benign
      
      
      End!if passed" = False
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Payment_Details, Accepted)
    OF ?CancelJob
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJob, Accepted)
      check_access('JOBS - CANCELLING',x")
      if x" = false
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
          Case MessageEx('You are about to CANCEL the selected job.<13,10><13,10>If you continue, an entry will be made in the Audit Trail, you WILL be able to despatch the job back to the customer<13,10><13,10>*** You will NOT be able to reverse this process. ***<13,10><13,10>Are you sure you want to continue.','ServiceBase 2000',|
                         'Styles\cancel.ico','|&Yes|&No',2,2,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,'',msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  brw1.updatebuffer
                  error# = 0
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number  = brw1.q.job:ref_number
                  If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                      If job:consignment_number <> ''
                          Case MessageEx('Error! This job has already been despatched.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          error# = 1
                      End!If job:consignment_number <> ''
                      If job:exchange_unit_number <> ''
                          Case MessageEx('Error! Cannot cancel this job.<13,10><13,10>This job has an Exchange Unit attached.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          error# = 1
                      End!If job:exchange_unit_number <> ''
                      If error# = 0
                          If job:loan_unit_number <> ''
                              Case MessageEx('Error! Cannot cancel this job.<13,10><13,10>This job has an Loan Unit attached.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              error# = 1
                          End!If job:loan_unit_number <> ''
                      End!If error# = 0
                  End!If access:jobs.tryfetch(job:ref_number_key) = Level:benign
                  If error# = 0
                      !Complete Job
                      If job:date_completed = ''
                          job:date_completed  = Today()
                          job:time_completed  = Clock()
                          job:completed       = 'YES'
                          job:edi             = 'XXX'
      
                          ! Start Change 3879 BE(26/02/2004)
                          Access:JOBSE.ClearKey(jobe:RefNumberKey)
                          jobe:RefNumber = job:Ref_number
                          IF (Access:JOBSE.TryFetch(jobe:RefNumberKey) = Level:Benign) THEN
                              IF (jobe:CompleteRepairDate = '') THEN
                                  jobe:CompleteRepairType = 2
                                  jobe:CompleteRepairDate = TODAY()
                                  jobe:CompleteRepairTime = CLOCK()
                                  access:jobse.update()
                              END
                          END
                          ! End Change 3879 BE(26/02/2004)
      
                      ! Start Change 2905 BE(15/07/03)
                      ELSE
                          IF ((job:edi = 'NO') AND |
                              GETINI('VALIDATE','ExcludeCancelledJobs',,CLIP(PATH())&'\SB2KDEF.INI')) THEN
                              job:edi = 'XXX'
                          END
                      ! End Change 2905 BE(15/07/03)
                      End!If job:date_completed = ''
                      If job:consignment_number = ''
                          Include('companc.inc')
                      End!If job:consignment_number = ''
                      GetStatus(799,1,'JOB')
                      job:cancelled       = 'YES'
                      job:Bouncer         = ''
                      ! Start Change 3240 BE(23/10/03)
                      IF (GETINI('CancelledJobs','RepairType',,CLIP(PATH())&'\SB2KDEF.INI')) THEN
                          job:repair_type = 'Job Cancelled'
                          job:repair_type_warranty = 'Job Cancelled'
                      END
                      ! End Change 3240 BE(23/10/03)
                      access:jobs.update()
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          aud:type          = 'JOB'
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'JOB CANCELLED'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
      
                  End!If error# = 0
              Of 2 ! &No Button
          End!Case MessageEx
      
      End!if x" = false
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelJob, Accepted)
    OF ?FailedDelivery
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailedDelivery, Accepted)
      If SecurityCheck('JOBS - FAILED DELIVERY')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !SecurityCheck('FAILED DELIVERY')
          !Which type of delivery has failed
          brw1.UpdateViewRecord()
          Case MessageEx('Are you sure you want to mark the selected job as a Failed Delivery.'&|
            '<13,10>'&|
            '<13,10>WARNING! You will not be able to undo this action.'&|
            '<13,10>'&|
            '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                         'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
      
                  !Which of the units attached to the job can be failed,
                  !i.e., already been desaptched.
      
                  Job# = 0
                  Exchange# = 0
                  Loan# = 0
                  If job:Consignment_Number <> ''
                      Job# = 1
                  End !job:Consignment_Number <> ''
                  If job:Exchange_Consignment_Number <> '' and job:Exchange_Unit_Number <> ''
                      Exchange# = 1
                  End !job:Exchange_Consignment_Number <> ''
                  If job:Loan_Consignment_Number <> '' And job:Loan_Unit_Number <> ''
                      Loan# = 1
                  End !job:Loan_Consignment_Number <> ''
      
                  !Work out which unit to fail
                  !Job Exchange or Loan
                  If Job# And Exchange#
                      Case MessageEx('Which unit do you want to mark as a "Failed Delivery"?','ServiceBase 2000',|
                                     'Styles\question.ico','|Fail &Job|Fail &Exchange Unit|&Cancel',3,3,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! Fail &Job Button
                              Job# = 1
                              Exchange# = 0
                              Loan# = 0
                          Of 2 ! Fail &Exchange Unit Button
                              Job# = 0
                              Exchange# = 1
                              Loan# = 0
                          Of 3 ! &Cancel Button
                              Job# = 0
                              Exchange# = 0
                              Loan# = 0
                      End!Case MessageEx
                  End !If Job# And Exchange#
      
                  If Job# And Loan#
                      Case MessageEx('Which unit do you want to mark as a "Failed Delivery"?','ServiceBase 2000',|
                                     'Styles\question.ico','|Fail &Job|Fail &Loan Unit|&Cancel',3,3,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! Fail &Job Button
                              Job# = 1
                              Exchange# = 0
                              Loan# = 0
                          Of 2 ! Fail &Loan Unit Button
                              Job# = 0
                              Exchange# = 0
                              Loan# = 1
                          Of 3 ! &Cancel Button
                              Job# = 0
                              Exchange# = 0
                              Loan# = 0
                      End!Case MessageEx
                  End !Job# And Exchange# Or Job# And Loan#
      
                  If Job# Or Exchange# Or Loan#
                      !Get the JOBSE file and check if this job isn't already a failed delivery
                      Access:JOBSE.Clearkey(jobe:RefNumberKey)
                      jobe:RefNumber  = job:Ref_Number
                      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Found
      
                      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          If Access:JOBSE.PrimeRecord() = Level:Benign
                              jobe:RefNumber  = job:Ref_Number
                              If Access:JOBSE.TryInsert() = Level:Benign
                                  !Insert Successful
                              Else !If Access:JOBSE.TryInsert() = Level:Benign
                                  !Insert Failed
                              End!If Access:JOBSE.TryInsert() = Level:Benign
                          End !If Access:JOBSE.PrimeRecord() = Level:Benign
                      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
                      !Failed Delivery Types
                      !1 - Job Only
                      !2 - Exchange Only
                      !3 - Loan Only
                      !4 - Job And Exchange
                      !5 - Job And Loan
      
                      If Job#
                          If jobe:FailedDelivery = 1 Or jobe:FailedDelivery = 4 Or jobe:FailedDelivery = 5
                              Case MessageEx('This job has already been marked as a Failed Delivery.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          Else
                              !Incase this job is already a failed delivery
                              Case jobe:FailedDelivery
                                  Of 2 !Already an Exchange
                                      jobe:FailedDelivery = 4
                                  Of 3 !Already a Loan
                                      jobe:FailedDelivery = 5
                                  Else
                                      jobe:FailedDelivery = 1
                              End !Case jobe:FailedDelivery
                              Access:JOBSE.Update()
                              LocalFailedDeliveryAudit('JOB')
                          End !if jobe:FailedDelivery = 1
                      End !If Job#
                      If Exchange#
                          If jobe:FailedDelivery = 2 Or jobe:FailedDelivery = 4
                              Case MessageEx('This exchange unit has already been marked as a Failed Delivery.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          Else
                              !Incase this job is already a failed delivery
                              Case jobe:FailedDelivery
                                  Of 1 !Already a job
                                      jobe:FailedDelivery = 4
                                  Else
                                      jobe:FailedDelivery = 2
                              End !Case jobe:FailedDelivery
                              Access:JOBSE.Update()
                              LocalFailedDeliveryAudit('EXC')
                          End !if jobe:FailedDelivery = 1
                      End !If Job#
                      If Loan#
                          If jobe:FailedDelivery = 3 Or jobe:FailedDelivery = 5
                              Case MessageEx('This loan unit has already been marked as a Failed Delivery.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          Else
                              !Incase this job is already a failed delivery
                              Case jobe:FailedDelivery
                                  Of 1 !Already a job
                                      jobe:FailedDelivery = 5
                                  Else
                                      jobe:FailedDelivery = 3
                              End !Case jobe:FailedDelivery
                              Access:JOBSE.Update()
                              LocalFailedDeliveryAudit('LOA')
                          End !if jobe:FailedDelivery = 1
                      End !If Job#
                  Else !If Job# Or Exchange# Or Loan#
                      Case MessageEx('The unit(s) attached to this job have not been despatched.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End !If Job# Or Exchange# Or Loan#
      
          Of 2 ! &No Button
          End!Case MessageEx
      End !SecurityCheck('FAILED DELIVERY')
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FailedDelivery, Accepted)
    OF ?KeyColour
      ThisWindow.Update
      Colour_Key
      ThisWindow.Reset
    OF ?Count_Jobs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs, Accepted)
      ! Start Change COUNT JOBS BE(20/06/03)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      !  Of 1 ! &Yes Button
      !
      !
      !    count# = 0
      !    setcursor(cursor:wait)
      !
      !    save_job_id = access:jobs.savefile()
      !    set(job:ref_number_key)
      !    loop
      !        if access:jobs.next()
      !           break
      !        end !if
      !        yldcnt# += 1
      !        if yldcnt# > 25
      !           yield() ; yldcnt# = 0
      !        end !if
      !        count# += 1
      !    end !loop
      !    access:jobs.restorefile(save_job_id)
      !    setcursor()
      !    Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
      !                   'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
      !        Of 1 ! &OK Button
      !    End!Case MessageEx
      !
      !  Of 2 ! &No Button
      !End!Case MessageEx
      ! End Change COUNT JOBS BE(20/06/03)
      
      MessageEx('There are '& RECORDS(JOBS) &' records in this browse.','ServiceBase 2000',|
                'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs, Accepted)
    OF ?tmp:GlobalIMEINumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:GlobalIMEINumber, Accepted)
      !Added by Neil - Code to strip 18 char IMEI's and 15 char MSN's
      IF LEN(CLIP(tmp:GlobalIMEINumber)) = 18
        !Ericsson IMEI!
        tmp:GlobalIMEINumber = SUB(tmp:GlobalIMEINumber,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      If GetTempPathA(255,TempFilePath)
          If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & 'GLOBIMEI' & Clock() & '.TMP'
          Else !If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & '\GLOBIMEI' & Clock() & '.TMP'
          End !If Sub(Clip(TempFilePath),-1,1) = '\'
      End
      
      Remove(glo:File_Name)
      Access:ADDSEARCH.Open()
      Access:ADDSEARCH.UseFile()
      
      Count# = 0
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      thiswindow.reset(1)
      open(progresswindow)
      
      ?progress:userstring{prop:text} = 'Running...'
      ?progress:pcttext{prop:text} = '0% Completed'
      
      recordstoprocess    = Count#
      
      Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
      Access:JOBS_ALIAS.ClearKey(job_ali:ESN_Key)
      job_ali:ESN = tmp:GlobalIMEINumber
      Set(job_ali:ESN_Key,job_ali:ESN_Key)
      Loop
          If Access:JOBS_ALIAS.NEXT()
             Break
          End !If
          If job_ali:ESN <> tmp:GlobalIMEINumber      |
              Then Break.  ! End If
      
          Do GetNextRecord2
          Do cancelcheck
          If tmp:cancel = 1
              Break
          End!If tmp:cancel = 1
      
          If Access:ADDSEARCH.PrimeRecord() = Level:Benign
              addtmp:AddressLine1  = job_ali:Address_Line1
              addtmp:AddressLine2  = job_ali:Address_Line2
              addtmp:AddressLine3  = job_ali:Address_Line3
              addtmp:Postcode      = job_ali:Postcode
              addtmp:JobNumber     = job_ali:Ref_Number
              addtmp:FinalIMEI     = job_ali:ESN
              addtmp:Surname       = job_ali:Surname
              If Access:ADDSEARCH.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                  !Insert Failed
              End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
          End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
      End !Loop
      Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
      
      Save_jot_ID = Access:JOBTHIRD.SaveFile()
      Access:JOBTHIRD.ClearKey(jot:OriginalIMEIKey)
      jot:OriginalIMEI = tmp:GlobalIMEINumber
      Set(jot:OriginalIMEIKey,jot:OriginalIMEIKey)
      Loop
          If Access:JOBTHIRD.NEXT()
             Break
          End !If
          If jot:OriginalIMEI <> tmp:GlobalIMEINumber      |
              Then Break.  ! End If
          Access:ADDSEARCH.ClearKey(addtmp:JobNumberKey)
          addtmp:JobNumber = jot:RefNumber
          If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
              !Found
              addtmp:IncomingIMEI  = jot:OriginalIMEI
              Access:ADDSEARCH.Update()
          Else!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                  addtmp:IncomingIMEI  = jot:OriginalIMEI
                  addtmp:JobNumber     = jot:RefNumber
                  Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
                  job_ali:Ref_Number  = jot:RefNumber
                  If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                      !Found
                      addtmp:AddressLine1  = job_ali:Address_Line1
                      addtmp:AddressLine2  = job_ali:Address_Line2
                      addtmp:AddressLine3  = job_ali:Address_Line3
                      addtmp:Postcode      = job_ali:Postcode
                      addtmp:JobNumber     = job_ali:Ref_Number
                      addtmp:Surname       = job_ali:Surname
                      If Access:ADDSEARCH.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                          !Insert Failed
                      End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
      
                  Else! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                  
              End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
          End!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
      
      End !Loop
      Access:JOBTHIRD.RestoreFile(Save_jot_ID)
      
      Save_xch_ID = Access:EXCHANGE.SaveFile()
      Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
      xch:ESN = tmp:GlobalIMEINumber
      Set(xch:ESN_Only_Key,xch:ESN_Only_Key)
      Loop
          If Access:EXCHANGE.NEXT()
             Break
          End !If
          If xch:ESN <> tmp:GlobalIMEINumber      |
              Then Break.  ! End If
          If xch:Job_number <> ''
              Access:ADDSEARCH.ClearKey(addtmp:JobNumberKey)
              addtmp:JobNumber = xch:Job_Number
              If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
                  !Found
                  addtmp:ExchangedIMEI    = xch:ESN
                  Access:ADDSEARCH.Update()
              Else!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
                  Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
                  job_ali:Ref_Number  = xch:Job_Number
                  If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                      !Found
                      addtmp:AddressLine1  = job_ali:Address_Line1
                      addtmp:AddressLine2  = job_ali:Address_Line2
                      addtmp:AddressLine3  = job_ali:Address_Line3
                      addtmp:Postcode      = job_ali:Postcode
                      addtmp:JobNumber     = job_ali:Ref_Number
                      addtmp:Surname       = job_ali:Surname
                      If Access:ADDSEARCH.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                          !Insert Failed
                      End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
      
                  Else! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
      
              End!If Access:ADDSEARCH.TryFetch(addtmp:JobNumberKey) = Level:Benign
          End !if xch:Job_number <> ''
      End !Loop
      Access:EXCHANGE.RestoreFile(Save_xch_ID)
      
      Do EndPrintRun
      close(progresswindow)
      
      Access:ADDSEARCH.Close()
      
      IMEISearch(tmp:GlobalIMEINumber)
      
      Remove(glo:File_Name)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:GlobalIMEINumber, Accepted)
    OF ?Count_Jobs:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:2, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          set(job:ref_number_key)
          loop
              if access:jobs.next()
                 break
              end !if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
        Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:2, Accepted)
    OF ?Count_Jobs:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:3, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          set(job:ref_number_key)
          loop
              if access:jobs.next()
                 break
              end !if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
        Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:3, Accepted)
    OF ?tmp:AccountSearchOrder
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountSearchOrder, Accepted)
      Case tmp:AccountSearchOrder
          Of 0
              ?JobNumber{prop:Hide} = 1
              ?job:Ref_Number:10{prop:Hide} = 1
              ?OrderNumber{prop:Hide} = 0
              ?job:Order_Number{prop:Hide} = 0
          Of 1
              ?JobNumber{prop:Hide} = 0
              ?job:Ref_Number:10{prop:Hide} = 0
              ?OrderNumber{prop:Hide} = 1
              ?job:Order_Number{prop:Hide} = 1
      End !tmp:AccountSearchOrder
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountSearchOrder, Accepted)
    OF ?tmp:OrderNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OrderNumber, Accepted)
      If GetTempPathA(255,TempFilePath)
          If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & 'GLOBORD' & Clock() & '.TMP'
          Else !If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & '\GLOBORD' & Clock() & '.TMP'
          End !If Sub(Clip(TempFilePath),-1,1) = '\'
      End
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      thiswindow.reset(1)
      open(progresswindow)
      
      ?progress:userstring{prop:text} = 'Running...'
      ?progress:pcttext{prop:text} = '0% Completed'
      
      recordstoprocess    = Records(SUBTRACC)
      
      Remove(glo:File_Name)
      Access:ADDSEARCH.Open()
      Access:ADDSEARCH.UseFile()
      
      Save_sub_ID = Access:SUBTRACC.SaveFile()
      Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
      Set(sub:Account_Number_Key,sub:Account_Number_Key)
      Loop
          If Access:SUBTRACC.NEXT()
             Break
          End !If
          Do GetNextRecord2
          Do cancelcheck
          If tmp:cancel = 1
             Break
          End!If tmp:cancel = 1
      
          Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
          Access:JOBS_ALIAS.ClearKey(job_ali:AccOrdNoKey)
          job_ali:Account_Number = sub:Account_Number
          job_ali:Order_Number   = tmp:OrderNumber
          Set(job_ali:AccOrdNoKey,job_ali:AccOrdNoKey)
          Loop
              If Access:JOBS_ALIAS.NEXT()
                 Break
              End !If
              If job_ali:Account_Number <> sub:Account_Number      |
              Or job_ali:Order_Number   <> tmp:OrderNumber      |
                  Then Break.  ! End If
              If Access:ADDSEARCH.PrimeRecord() = Level:Benign
                  addtmp:AddressLine1  = job_ali:Address_Line1
                  addtmp:AddressLine2  = job_ali:Address_Line2
                  addtmp:AddressLine3  = job_ali:Address_Line3
                  addtmp:Postcode      = job_ali:Postcode
                  addtmp:JobNumber     = job_ali:Ref_Number
                  addtmp:FinalIMEI     = job_ali:ESN
                  addtmp:Surname       = job_ali:Surname
                  If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Successful
                  Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                      !Insert Failed
                  End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
              End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
          End !Loop
          Access:JOBS.RestoreFile(Save_job_ID)
      End !Loop
      Access:SUBTRACC.RestoreFile(Save_sub_ID)
      Do EndPrintRun
      close(progresswindow)
      
      Access:ADDSEARCH.Close()
      
      OrderSearch(tmp:OrderNumber)
      
      Remove(glo:File_Name)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:OrderNumber, Accepted)
    OF ?account_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number_temp, Accepted)
      IF account_number_temp OR ?account_number_temp{Prop:Req}
        sub:Account_Number = account_number_temp
        !Save Lookup Field Incase Of error
        look:account_number_temp        = account_number_temp
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            account_number_temp = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            account_number_temp = look:account_number_temp
            SELECT(?account_number_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number_temp, Accepted)
    OF ?Lookup_Account_number
      ThisWindow.Update
      sub:Account_Number = account_number_temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          account_number_temp = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?account_number_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?account_number_temp)
    OF ?Count_Jobs:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:4, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:accordnokey)
          job:account_number = account_number_temp
          set(job:accordnokey,job:accordnokey)
          loop
              if access:jobs.next()
                 break
              end !if
              if job:account_number <> account_number_temp      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:4, Accepted)
    OF ?SearchByAddress
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SearchByAddress, Accepted)
      If GetTempPathA(255,TempFilePath)
          If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & 'GLOBADD' & Clock() & '.TMP'
          Else !If Sub(Clip(TempFilePath),-1,1) = '\'
              glo:File_Name   = Clip(TempFilePath) & '\GLOBADD' & Clock() & '.TMP'
          End !If Sub(Clip(TempFilePath),-1,1) = '\'
      End
      Remove(glo:File_Name)
      Access:ADDSEARCH.Open()
      Access:ADDSEARCH.UseFile()
      
      Count# = 0
      
      Setcursor(Cursor:Wait)
      Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
      Access:JOBS_ALIAS.ClearKey(job_ali:Surname_Key)
      job_ali:Surname = brw1.q.job:Surname
      Set(job_ali:Surname_Key,job_ali:Surname_Key)
      Loop
          If Access:JOBS_ALIAS.NEXT()
             Break
          End !If
          If job_ali:Surname <> brw1.q.job:Surname      |
              Then Break.  ! End If
          Count# += 1
          If Count# > 300
              Count# = Records(JOBS)
              Break
          End !If Count# > 300
      
      End !Loop
      Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
      Setcursor()
      
      recordspercycle         = 25
      recordsprocessed        = 0
      percentprogress         = 0
      progress:thermometer    = 0
      thiswindow.reset(1)
      open(progresswindow)
      
      ?progress:userstring{prop:text} = 'Running...'
      ?progress:pcttext{prop:text} = '0% Completed'
      
      recordstoprocess    = Count#
      
      Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
      Access:JOBS_ALIAS.ClearKey(job_ali:Surname_Key)
      job_ali:Surname = brw1.q.job:Surname
      Set(job_ali:Surname_Key,job_ali:Surname_Key)
      Loop
          If Access:JOBS_ALIAS.NEXT()
             Break
          End !If
          If job_ali:Surname <> brw1.q.job:Surname      |
              Then Break.  ! End If
      
          Do GetNextRecord2
          Do cancelcheck
          If tmp:cancel = 1
              Break
          End!If tmp:cancel = 1
      
          If Access:ADDSEARCH.PrimeRecord() = Level:Benign
              addtmp:AddressLine1 = job_ali:Address_Line1
              addtmp:AddressLine2 = job_ali:Address_Line2
              addtmp:AddressLine3 = job_ali:Address_Line3
              addtmp:Postcode     = job_ali:Postcode
              addtmp:JobNumber    = job_ali:Ref_Number
              If Access:ADDSEARCH.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:ADDSEARCH.TryInsert() = Level:Benign
                  !Insert Failed
              End !AIf Access:ADDSEARCH.TryInsert() = Level:Benign
          End !If Access:ADDSEARCH.PrimeRecord() = Level:Benign
      End !Loop
      Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
      
      Do EndPrintRun
      close(progresswindow)
      
      Access:ADDSEARCH.Close()
      
      AddressSearch(brw1.q.job:Surname)
      
      Remove(glo:File_Name)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?SearchByAddress, Accepted)
    OF ?Count_Jobs:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:5, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          set(job:ref_number_key)
          loop
              if access:jobs.next()
                 break
              end !if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:5, Accepted)
    OF ?tmp:SelectLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectLocation, Accepted)
      DO refresh_job_status_tab
      BRW1.ResetSort(1)
      do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:SelectLocation, Accepted)
    OF ?tmp:JobLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobLocation, Accepted)
      Do refresh_job_status_tab
      BRW1.ResetSort(1)
      IF tmp:JobLocation OR ?tmp:JobLocation{Prop:Req}
        loi:Location = tmp:JobLocation
        !Save Lookup Field Incase Of error
        look:tmp:JobLocation        = tmp:JobLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:JobLocation = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:JobLocation = look:tmp:JobLocation
            SELECT(?tmp:JobLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobLocation, Accepted)
    OF ?Lookup_Location
      ThisWindow.Update
      loi:Location = tmp:JobLocation
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:JobLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:JobLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:JobLocation)
    OF ?status_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
      glo:select1    = 'JOB'
      sts:job   = 'YES'
      Do refresh_job_status_tab
      IF status_temp OR ?status_temp{Prop:Req}
        sts:Status = status_temp
        !Save Lookup Field Incase Of error
        look:status_temp        = status_temp
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            status_temp = sts:Status
          ELSE
            !Restore Lookup On Error
            status_temp = look:status_temp
            SELECT(?status_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BRW1.ResetSort(1)
      glo:select1    = ''
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
    OF ?Lookup_Job_Status
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Job_Status, Accepted)
      sts:job   = 'YES'
      glo:select1    = 'JOB'
      sts:Status = status_temp
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          status_temp = sts:Status
          Select(?+1)
      ELSE
          Select(?status_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?status_temp)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Job_Status, Accepted)
    OF ?Count_Jobs:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:6, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:by_status)
          job:current_status = status_temp
          set(job:by_status,job:by_status)
          loop
              if access:jobs.next()
                 break
              end !if
              if job:current_status <> status_temp      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:6, Accepted)
    OF ?status3_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status3_temp, Accepted)
      glo:select1    = 'EXC'
      sts:exchange   = 'YES'
      IF status3_temp OR ?status3_temp{Prop:Req}
        sts:Status = status3_temp
        !Save Lookup Field Incase Of error
        look:status3_temp        = status3_temp
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            status3_temp = sts:Status
          ELSE
            !Restore Lookup On Error
            status3_temp = look:status3_temp
            SELECT(?status3_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      BRW1.ResetSort(1)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status3_temp, Accepted)
    OF ?Lookup_Status3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status3, Accepted)
      glo:select1    = 'EXC'
      sts:exchange      = 'YES'
      sts:Status = status3_temp
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          status3_temp = sts:Status
          Select(?+1)
      ELSE
          Select(?status3_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?status3_temp)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status3, Accepted)
    OF ?Count_Jobs:15
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:15, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          job:Exchange_Status  = status3_temp
          set(job:ExcStatusKey,job:ExcStatusKey)
          loop
              if access:jobs.next()
                 break
              end !if
              If job:exchange_status <> status3_temp
                  Break
              End!If job:exchangestatus <> status3_temp
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:15, Accepted)
    OF ?status4_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status4_temp, Accepted)
      glo:select1    = 'LOA'
      sts:loan      = 'YES'
      IF status4_temp OR ?status4_temp{Prop:Req}
        sts:Status = status4_temp
        !Save Lookup Field Incase Of error
        look:status4_temp        = status4_temp
        IF Access:STATUS.TryFetch(sts:LoanKey)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            status4_temp = sts:Status
          ELSE
            !Restore Lookup On Error
            status4_temp = look:status4_temp
            SELECT(?status4_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      glo:select1    = ''
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status4_temp, Accepted)
    OF ?Lookup_Status4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status4, Accepted)
      glo:select1    = 'LOA'
      sts:loan      = 'YES'
      sts:Status = status4_temp
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          status4_temp = sts:Status
          Select(?+1)
      ELSE
          Select(?status4_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?status4_temp)
      glo:select1    = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Status4, Accepted)
    OF ?Count_Jobs:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:8, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          job:Loan_Status  = status4_temp
          set(job:LoanStatuskey,job:LoanStatusKey)
          loop
              if access:jobs.next()
                 break
              end !if
              If job:Loan_Status <> status4_temp
                  Break
              End!If job:LoanStatus <> statu4_temp
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:8, Accepted)
    OF ?Count_Jobs:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:7, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          set(job:ref_number_key)
          loop
              if access:jobs.next()
                 break
              end !if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:7, Accepted)
    OF ?model_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?model_number_temp, Accepted)
      BRW1.ResetQueue(1)
      IF model_number_temp OR ?model_number_temp{Prop:Req}
        mod:Model_Number = model_number_temp
        !Save Lookup Field Incase Of error
        look:model_number_temp        = model_number_temp
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            model_number_temp = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            model_number_temp = look:model_number_temp
            SELECT(?model_number_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?model_number_temp, Accepted)
    OF ?Lookup_Model_Number
      ThisWindow.Update
      mod:Model_Number = model_number_temp
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          model_number_temp = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?model_number_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?model_number_temp)
    OF ?Count_Jobs:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:9, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:model_unit_key)
          job:model_number = model_number_temp
          set(job:model_unit_key,job:model_unit_key)
          loop
              if access:jobs.next()
                 break
              end !if
              if job:model_number <> model_number_temp      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:9, Accepted)
    OF ?engineer_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?engineer_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?engineer_temp, Accepted)
    OF ?Lookup_Engineer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      Browse_Users_job_Assignment
      if globalresponse = requestcompleted
          engineer_temp = use:user_code
          display()
      end
      globalrequest     = saverequest#
      BRW1.ResetQueue(1)
      thiswindow.reset
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Engineer, Accepted)
    OF ?Completed2_Temp:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Completed2_Temp:2, Accepted)
      Thiswindow.reset
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Completed2_Temp:2, Accepted)
    OF ?Count_Jobs:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:10, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          Case completed2_temp
              Of 'YES'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:EngCompKey)
                  job:engineer   = engineer_temp
                  job:completed  = 'YES'
                  set(job:EngCompKey,job:EngCompKey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:engineer   <> engineer_temp      |
                      or job:completed  <> 'YES'      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
              Of 'NO'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:EngCompKey)
                  job:engineer   = engineer_temp
                  job:completed  = 'NO'
                  set(job:EngCompKey,job:EngCompKey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:engineer   <> engineer_temp      |
                      or job:completed  <> 'NO'      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
              Of 'ALL'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:engineer_key)
                  job:engineer   = engineer_temp
                  set(job:engineer_key,job:engineer_key)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:engineer   <> engineer_temp |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
          End!Case completed2_temp
      
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:10, Accepted)
    OF ?UnallocatedJobsReport
      ThisWindow.Update
      Unallocated_Criteria
      ThisWindow.Reset
    OF ?Count_Jobs:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:11, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
          count# = 0
          setcursor(cursor:wait)
          save_job_id = access:jobs.savefile()
          access:jobs.clearkey(job:EngWorkKey)
          job:engineer   = ''
          ! Start Change 2400 BE(21/03/03)
          !job:workshop   = 'YES'
          IF (unallocated_temp = 'INW') THEN
              InWorkshop"   = 'YES'
          ELSE
              InWorkshop"   = 'NO'
          END
          job:workshop   = CLIP(InWorkshop")
          ! End Change 2400 BE(21/03/03)
          set(job:EngWorkKey,job:EngWorkKey)
          loop
              if access:jobs.next()
                 break
              end !if
              ! Start Change 2400 BE(21/03/03)
              !if job:engineer   <> ''    |
              !or job:workshop   <> 'YES' |
              !    then break.  ! end if
              IF ((job:engineer <> '') OR (job:workshop <> CLIP(InWorkshop"))) THEN
                  BREAK
              END
              ! End Change 2400 BE(21/03/03)
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:11, Accepted)
    OF ?Count_Jobs:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:12, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          setcursor(cursor:wait)
      
          save_job_id = access:jobs.savefile()
          set(job:ref_number_key)
          loop
              if access:jobs.next()
                 break
              end !if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              count# += 1
          end !loop
          access:jobs.restorefile(save_job_id)
          setcursor()
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:12, Accepted)
    OF ?batch_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?batch_number_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?batch_number_temp, Accepted)
    OF ?Completed2_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Completed2_Temp, Accepted)
      Thiswindow.reset
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Completed2_Temp, Accepted)
    OF ?Despatch_Batch
      ThisWindow.Update
      Despatch_Batch(batch_number_temp)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Despatch_Batch, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Despatch_Batch, Accepted)
    OF ?Count_Jobs:13
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:13, Accepted)
      ! Start Change 4912 BE(28/10/2004)
      !Case MessageEx('Couting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      ! End Change 4912 BE(28/10/2004)
        Of 1 ! &Yes Button
      
          count# = 0
          Case completed2_temp
              Of 'ALL'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:batch_number_key)
                  job:batch_number = batch_number_temp
                  set(job:batch_number_key,job:batch_number_key)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:batch_number <> batch_number_temp |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
              Of 'YES'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:BatchCompKey)
                  job:batch_number = batch_number_temp
                  job:completed    = 'YES'
                  set(job:BatchCompKey,job:BatchCompKey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:batch_number <> batch_number_temp      |
                      or job:completed    <> 'YES'      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
              Of 'NO'
                  setcursor(cursor:wait)
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:BatchCompKey)
                  job:batch_number = batch_number_temp
                  job:completed    = 'NO'
                  set(job:BatchCompKey,job:BatchCompKey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:batch_number <> batch_number_temp      |
                      or job:completed    <> 'NO'      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      count# += 1
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
      
          End!Case completed2_temp = 'ALL'
      
          Case MessageEx('There are '&clip(count#)&' records in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      of 2
      end !case
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:13, Accepted)
    OF ?select_courier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?select_courier_temp, Accepted)
      Do DASBRW::26:DASUNTAGALL
      Do refresh_despatch_tab
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?select_courier_temp, Accepted)
    OF ?courier_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
      Do refresh_despatch_tab
      BRW1.ResetSort(1)
      IF courier_temp OR ?courier_temp{Prop:Req}
        cou:Courier = courier_temp
        !Save Lookup Field Incase Of error
        look:courier_temp        = courier_temp
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            courier_temp = cou:Courier
          ELSE
            !Restore Lookup On Error
            courier_temp = look:courier_temp
            SELECT(?courier_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, Accepted)
    OF ?Lookup_Courier
      ThisWindow.Update
      cou:Courier = courier_temp
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          courier_temp = cou:Courier
          Select(?+1)
      ELSE
          Select(?courier_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?courier_temp)
    OF ?Select_Trade_Account_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select_Trade_Account_Temp, Accepted)
      Do DASBRW::26:DASUNTAGALL
      Do refresh_despatch_tab
      Do show_hide
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select_Trade_Account_Temp, Accepted)
    OF ?account_number2_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number2_temp, Accepted)
      Do refresh_despatch_tab
      BRW1.ResetSort(1)
      IF account_number2_temp OR ?account_number2_temp{Prop:Req}
        sub:Account_Number = account_number2_temp
        !Save Lookup Field Incase Of error
        look:account_number2_temp        = account_number2_temp
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            account_number2_temp = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            account_number2_temp = look:account_number2_temp
            SELECT(?account_number2_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number2_temp, Accepted)
    OF ?Lookup_Account_Number:2
      ThisWindow.Update
      sub:Account_Number = account_number2_temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          account_number2_temp = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?account_number2_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?account_number2_temp)
    OF ?Multiple_Despatch2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multiple_Despatch2, Accepted)
      check_access('MULTIPLE DESPATCH',x")
      if x" = false
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      else!if x" = false
      !Has something been tagged already.
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
      
          If Select_Courier_Temp = 'IND' And Select_Trade_Account_Temp = 'IND'
              If Courier_Temp <> '' And Account_Number2_Temp <> ''
                  If Records(glo:Q_JobNumber)
                      Case MessageEx('Jobs have been tagged to be despatched.'&|
                        '<13,10>'&|
                        '<13,10>Do you want to automatically create a Multiple Despatch Batch from these Jobs?','ServiceBase 2000',|
                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              If NormalDespatchCourier(Courier_Temp)
                                  Case MessageEx('The Selected Courier can only despatch jobs through "Despatch Procedures".','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                              Else !If NormalDespatchCourier(Courier_Temp)
                                  ! Start Change 3813 BE(19/01/04)
                                  !If cou:Courier_Type = 'TOTE'
                                  IF ((cou:Courier_Type = 'TOTE') OR (cou:Courier_Type = 'TOTE PARCELINE')) THEN
                                  ! End Change 3813 BE(19/01/04)
                                      Case MessageEx('The selected Courier can only be despatched Individually.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                  ELse !If cou:Courier_Type = 'TOTE'
                                  
                                      tmp:PrintDespatchNote = 0
                                      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                                      sub:Account_Number  = Account_Number2_Temp
                                      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                          !Found
                                          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                                          tra:Account_Number  = sub:Main_Account_Number
                                          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                              !Found
                                              If tra:Use_Sub_Accounts = 'YES'
                                                  If sub:Print_Despatch_Despatch = 'YES' And sub:Despatch_Note_Per_Item = 'YES'
                                                      tmp:PrintDespatchNote = 1
                                                  End !If sub:Print_Despatch_Despatch = 'YES'
                                              Else !If tra:Use_Sub_Accounts = 'YES'
                                                  If tra:Print_Despatch_Despatch = 'YES' And tra:Despatch_Note_Per_Item = 'YES'
                                                      tmp:PrintDespatchNote = 1
                                                  End !If tra:Print_Despatch_Despatch = 'YES'
                                              End !If tra:Use_Sub_Accounts = 'YES'
                                          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                                          
                                      Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                                          !Error
                                          !Assert(0,'<13,10>Fetch Error<13,10>')
                                      End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
                                      If tmp:PrintDespatchNote
                                          Case MessageEx('Do you wish to print an Individual Despatch Note for each job in the batch?','ServiceBase 2000',|
                                                         'Styles\question.ico','|&Yes|&No',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                              Of 1 ! &Yes Button
                                              Of 2 ! &No Button
                                                  tmp:PrintDespatchNote = 0
                                          End!Case MessageEx
                                      End !If tmp:PrintDespatchNote
      
                                      ! Start Change 2893 BE(21/01/04)
                                      ShowPaymentDetails# = GETINI('DESPATCH','ShowPaymentDetails',,CLIP(PATH())&'\SB2KDEF.INI')
                                      ! End Change 2893 BE(21/01/04)
      
                                      tmp:FirstRecord = 1
                                      tmp:BatchNumber = 0
                                      tmp:BatchCreated = 0
                                      Loop x# = 1 To Records(glo:Q_JobNumber)
                                          Get(glo:Q_JobNumber,x#)
                                          Access:JOBS.Clearkey(job:Ref_Number_Key)
                                          job:Ref_Number  = glo:Job_Number_Pointer
                                          If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                              !Found
      
                                              If LocalValidateAccountNumber()
                                                  Cycle
                                              End !If LocalValidateAccountNumber()
      
                                              Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
                                              mulj:JobNumber = job:Ref_Number
                                              If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                                  !Found
                                                  Case MessageEx('Cannot Despatch job number: ' & Clip(job:Ref_number) & '.'&|
                                                    '<13,10>'&|
                                                    '<13,10>This job is part of a Multiple Despatch Batch that has not been despatched.','ServiceBase 2000',|
                                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                      Of 1 ! &OK Button
                                                  End!Case MessageEx
                                                  Cycle
                                              Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                                  !Error
                                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                              End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
      
      
                                              If def:Force_Accessory_Check = 'YES'
                                                  If LocalValidateAccessories()
                                                      Cycle
                                                  End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
                                              End !If def:Force_Accessory_Check = 'YES'
      
                                              If def:ValidateDesp = 'YES'
                                                  If LocalValidateIMEI()
                                                      Cycle
                                                  End !If LocalValidateIMEI()
                                              End !If def:ValidateDesp = 'YES'
      
                                              ! Start Change 2893 BE(21/01/04)
                                              IF ((ShowPaymentDetails# = 1) AND (job:Chargeable_Job = 'YES') AND |
                                                  (job:Despatch_Type = 'JOB')) THEN
                                                  Total_Price('C', paid_vat", paid_total", paid_balance")
                                                  p_balance$ = paid_balance"
                                                  IF (p_balance$ > 0) THEN
                                                      glo:Select1 = job:Ref_Number
                                                      Browse_Payments
                                                      glo:Select1 = ''
                                                  END
                                              END
                                              ! End Change 2893 BE(21/01/04)
      
                                              If tmp:FirstRecord = 1
                                                  If Access:MULDESP.PrimeRecord() = Level:Benign
                                                      muld:AccountNumber  = Account_Number2_Temp
                                                      muld:Courier        = Courier_temp
                                                      tmp:BatchNumber = muld:RecordNumber
                                                      tmp:FirstRecord = 0
                                                      tmp:BatchCreated = 1
                                                      
                                                      If Access:MULDESP.TryInsert() = Level:Benign
                                                          !Insert Successful
                                                      Else !If Access:MULDESP.TryInsert() = Level:Benign
                                                          !Insert Failed
                                                      End !If Access:MULDESP.TryInsert() = Level:Benign
                                                  End !If Access:MULDESP.PrimeRecord() = Level:Benign
                                              End !If tmp:FirstRecord = 1
      
                                              If Access:MULDESPJ.PrimeRecord() = Level:Benign
                                                  mulj:RefNumber     = tmp:BatchNumber
                                                  mulj:JobNumber     = job:Ref_Number
                                                  Case job:Despatch_Type
                                                      Of 'JOB'
                                                          mulj:IMEINumber    = job:ESN
                                                          mulj:MSN           = job:MSN
                                                      Of 'EXC'
                                                          Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                                          xch:Ref_Number  = job:Exchange_Unit_Number
                                                          If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                              !Found
                                                              mulj:IMEINumber = xch:ESN
                                                              mulj:MSN        = xch:MSN
                                                          Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                              !Error
                                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                                                      Of 'LOA'
                                                          Access:LOAN.Clearkey(loa:Ref_Number_Key)
                                                          loa:Ref_Number  = job:Loan_Unit_Number
                                                          If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                              !Found
                                                              mulj:IMEINumber = loa:ESN
                                                              mulj:MSN        = loa:MSN
                                                          Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                              !Error
                                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                                          End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                                                  End !job:Despatch_Type
                                                  mulj:AccountNumber = Account_Number2_Temp
                                                  mulj:Courier       = Courier_Temp
      
                                                  Access:COURIER.Clearkey(cou:Courier_Key)
                                                  cou:Courier = courier_Temp
                                                  If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                                                      !Found
      
                                                  Else! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                                                      !Error
                                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                                  End! If Access:COURIER.Tryfetch(cou:Courier_Key) = Level:Benign
                                                  
      
                                                  If tmp:PrintDespatchNote
                                                      glo:Select1 = job:Ref_Number
                                                      If cou:CustomerCollection = 1
                                                          glo:Select2 = cou:NoOfDespatchNotes
                                                      Else!If cou:CustomerCollection = 1
                                                          glo:Select2 = 1
                                                      End!If cou:CustomerCollection = 1
                                                      Despatch_Note
                                                      glo:Select1 = ''
                                                  End !If tmp:PrintDespatchNote
                                                  If Access:MULDESPJ.TryInsert() = Level:Benign
                                                      !Insert Successful
                                                  Else !If Access:MULDESPJ.TryInsert() = Level:Benign
                                                      !Insert Failed
                                                  End !If Access:MULDESPJ.TryInsert() = Level:Benign
                                              End !If Access:MULDESPJ.PrimeRecord() = Level:Benign
      
                                          Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                                      End !Loop x# = 1 To Records(glo:Q_JobNumber)
                                      If tmp:BatchCreated = 1
                                          !Count how many jobs there are on the batch.
                                          !I could just add one to the record, but I can see
                                          !that being too inaccurate. So I'll physically count
                                          !all the records in the batch instead.
      
                                          CountBatch# = 0
                                          Save_mulj_ID = Access:MULDESPJ.SaveFile()
                                          Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                                          mulj:RefNumber = tmp:BatchNumber
                                          Set(mulj:JobNumberKey,mulj:JobNumberKey)
                                          Loop
                                              If Access:MULDESPJ.NEXT()
                                                 Break
                                              End !If
                                              If mulj:RefNumber <> tmp:BatchNumber      |
                                                  Then Break.  ! End If
                                              CountBatch# += 1
                                          End !Loop
                                          Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
                                          Access:MULDESP.ClearKey(muld:RecordNumberKey)
                                          muld:RecordNumber = tmp:BatchNumber
                                          If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                                              !Found
                                              muld:BatchTotal = CountBatch#
                                              !Allocate a Batch Number
                                              !Count between 1 to 1000, (that should be enough)
                                              !and if I can't find a Batch with that batch number, then
                                              !assign that batch number to this batch.
                                              BatchNumber# = 0
                                              Loop BatchNumber# = 1 To 1000
                                                  Access:MULDESP_ALIAS.ClearKey(muld_ali:BatchNumberKey)
                                                  muld_ali:BatchNumber = BatchNumber#
                                                  If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                                                      !Found
                                                  Else!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                                                      !Error
                                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                                      muld:BatchNumber    = BatchNumber#
                                                      Break
                                                  End!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                                              End !BatchNumber# = 1 To 1000
      
                                              Access:MULDESP.Update()
                                          Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                                              !Error
                                              !Assert(0,'<13,10>Fetch Error<13,10>')
                                          End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                                          Case MessageEx('A Multiple Despatch Batch has been created for Account Number ' & clip(Account_Number2_Temp) & ' and Courier ' & Clip(Courier_Temp) & '.','ServiceBase 2000',|
                                                         'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                                              Of 1 ! &OK Button
                                          End!Case MessageEx
                                      End !If tmp:BatchCreated = 1
                                  End !If cou:Courier_Type = 'TOTE'
                              End !If NormalDespatchCourier(Courier_Temp)
                          Of 2 ! &No Button
                      End!Case MessageEx
                  End !If Recods(glo:Q_JobNumber)
              End !If Courier_Temp <> '' And Account_Number2_Temp <> ''
          End !If Select_Courier_Temp = 'IND' And Select_Trade_Account_Temp = 'IND'
      
          MultipleBatchDespatch
      end!if x" = f
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Multiple_Despatch2, Accepted)
    OF ?Individual_Despatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Individual_Despatch, Accepted)
      check_access('INDIVIDUAL DESPATCH',x")
      if x" = false
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      else!if x" = false
      
          Case MessageEx('You are about to despatch all the tagged jobs, and assign each one an INDIVIDUAL Consingment Number.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
      
                  Thiswindow.reset
                  Set(DEFAULTS)
                  Access:DEFAULTS.Next()
                  error# = 0
                  Loop x# = 1 To Records(glo:q_jobnumber)
                      Get(glo:q_jobnumber,x#)
                      Error# = 0
                      access:jobs.clearkey(job:ref_number_key)
                      job:ref_number  = glo:job_number_pointer
                      If access:jobs.fetch(job:ref_number_key) = Level:Benign
                          Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
                          mulj:JobNumber = job:Ref_Number
                          If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                              !Found
                              Case MessageEx('Cannot Despatch job number: ' & Clip(job:Ref_number) & '.'&|
                                '<13,10>'&|
                                '<13,10>This job is part of a Multiple Despatch Batch that has not been despatched.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              Cycle
      
                          Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                              !Error
                              !Assert(0,'<13,10>Fetch Error<13,10>')
                          End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
      
                          If LocalValidateAccountNumber()
                              Cycle
                          End !If LocalValidateAccountNumber()
      
                          access:courier.clearkey(cou:courier_key)
                          cou:courier = job:current_courier
                          if access:courier.fetch(cou:courier_key)
                              Case MessageEx('Cannot Despatch!'&|
                                '<13,10>'&|
                                '<13,10>There is no Courier attached to Job Number: ' & Clip(job:Ref_Number) & '.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              Cycle
                          Else!if access:courier.fetch(cou:courier_key)
                              If NormalDespatchCourier(job:Current_Courier)
                                  Case MessageEx('Job Number ' & Clip(job:Ref_Number) & ' has been assigned to a ' & Clip(cou:Courier_Type) & ' courier.'&|
                                    '<13,10>'&|
                                    '<13,10>This should be despatched from ''Despatch Procedures''.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  Cycle
                              Else !If NormalDespatchCourier(job:Current_Courier)
                                  If cou:Courier_Type = 'SDS'
                                      Case MessageEx('You can only despatch SDS jobs using Multiple Despatch.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      Cycle
                                  End !If cou:Courier_Type = 'SDS'
                              End !If NormalDespatchCourier(job:Current_Courier)
                          End!if access:courier.fetch(cou:courier_key)
      
                          If def:Force_Accessory_Check = 'YES'
                              If LocalValidateAccessories()
                                  Access:JOBS.Update()
                                  Cycle
                              End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
                          End !If def:Force_Accessory_Check = 'YES'
      
                          If def:ValidateDesp = 'YES'
                              If LocalValidateIMEI()
                                  Cycle
                              End !If LocalValidateIMEI()
                          End !If def:ValidateDesp = 'YES'
      
                          If access:desbatch.primerecord() = Level:Benign
                              If access:desbatch.tryinsert()
                                  access:desbatch.cancelautoinc()
                              Else!If access:desbatch.tryinsert()
                                  DespatchSingle()
                              End!If access:desbatch.tryinsert()
                          End!If access:desbatch.primerecord() = Level:Benign
                      End!If access:jobs.clearkey(job:ref_number_key) = Level:Benign
                  End!Loop x# = 1 To Records(glo:q_jobnumber)
                  Post(event:accepted,?Dasuntagall)
      
              Of 2 ! &No Button
          End!Case MessageEx
      
      end!if x" = false
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Individual_Despatch, Accepted)
    OF ?Validate_Esn
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Esn, Accepted)
      check_access('VALIDATE ESN',x")
      if x" = false
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      else!if x" = false
          thiswindow.reset
          Case job:despatch_type
              Of 'JOB' Orof 'JHL'
                  Validate_Esn(job:esn,esn",validated")
                  If esn" <> ''
      
                      If validated" = False
                          Case MessageEx('You are attempting to despatch a unit that differs from the E.S.N. / I.M.E.I. issued by ServiceBase 2000.<13,10><13,10>Please inform your System Supervisor.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                          
                              aud:notes         = 'THE E.S.N / I.M.E.I. AT POINT OF DESPATCH DIFFERED FROM THE REQUIRED SERIAL NUMBER. ' & |
                                                  '<13,10>REQUIRED NUMBER: ' & CLIP(JOB:ESN) & '<13,10>ACTUAL NUMBER: ' & CLIP(ESN")
                              aud:ref_number    = JOB:REF_NUMBER
                              aud:date          = Today()
                              aud:time          = Clock()
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'E.S.N. / I.M.E.I. MISMATCH - JOB'
                              if access:audit.insert()
                                  access:audit.cancelautoinc()
                              end
                          end!if access:audit.primerecord() = level:benign
                      Else!If validated" = False
                          type# = 1 !Job
                          Include('accjchk.inc')
                      End!If validated" = True
                  End!If esn" <> ''
              Of 'EXC' Orof 'EHL'
                  access:exchange.clearkey(xch:ref_number_key)
                  xch:ref_number = job:exchange_unit_number
                  if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                      Validate_Esn(xch:esn,esn",validated")
                      If esn" <> ''
                          If validated" = False
                              Case MessageEx('You are attempting to despatch a unit that differs from the E.S.N. / I.M.E.I. issued by ServiceBase 2000.<13,10><13,10>Please inform your System Supervisor.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                              
                                  aud:notes         = 'THE E.S.N / I.M.E.I. AT POINT OF DESPATCH DIFFERED FROM THE REQUIRED SERIAL NUMBER. ' & |
                                                      '<13,10>REQUIRED NUMBER: ' & CLIP(XCH:ESN) & '<13,10>ACTUAL NUMBER: ' & CLIP(ESN")
                                  aud:ref_number    = JOB:REF_NUMBER
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password = glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'E.S.N. / I.M.E.I. MISMATCH - EXCHANGE'
                                  aud:type          = 'EXC'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
                          Else!If validated" = False
                              Include('accxchk.inc')
                          End!If validated" = True
                      End!If esn" <> ''
                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
              Of 'LOA' Orof 'LHL'
                  access:loan.clearkey(loa:ref_number_key)
                  loa:ref_number = job:loan_unit_number
                  if access:loan.fetch(loa:ref_number_key) = Level:Benign
                      Validate_Esn(loa:esn,esn",validated")
                      If esn" <> ''
                          If validated" = False
                              Case MessageEx('You are attempting to despatch a unit that differs from the E.S.N. / I.M.E.I. issued by ServiceBase 2000.<13,10><13,10>Please inform your System Supervisor.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                              
                                  aud:notes         = 'THE E.S.N / I.M.E.I. AT POINT OF DESPATCH DIFFERED FROM THE REQUIRED SERIAL NUMBER. ' & |
                                                      '<13,10>REQUIRED NUMBER: ' & CLIP(loa:ESN) & '<13,10>ACTUAL NUMBER: ' & CLIP(ESN")
                                  aud:ref_number    = JOB:REF_NUMBER
                                  aud:date          = Today()
                                  aud:time          = Clock()
                                  access:users.clearkey(use:password_key)
                                  use:password = glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  aud:action        = 'E.S.N. / I.M.E.I. MISMATCH - LOAN'
                                  aud:type      = 'LOA'
                                  if access:audit.insert()
                                      access:audit.cancelautoinc()
                                  end
                              end!if access:audit.primerecord() = level:benign
                          Else!If validated" = False
                              include('acclchk.inc')
                          End!If validated" = True
                      End!If esn" <> ''
                  end!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
      
          End!Case job:despatch_type
      end!if x" = false
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Validate_Esn, Accepted)
    OF ?Change_Courier
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change_Courier, Accepted)
      check_access('CHANGE COURIER',x")
      if x" = false
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      else!if x" = false
          If ~Records(glo:Q_JobNumber)
              Case MessageEx('You have not tagged any jobs.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Else !If ~Records(glo:Q_JobNumber)
              saverequest#      = globalrequest
              globalresponse    = requestcancelled
              globalrequest     = selectrecord
              PickCouriers
              if globalresponse = requestcompleted
                  Case MessageEx('This will change the Courier of the tagged jobs to '&Clip(cou:courier)&'.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                          Loop x# = 1 To Records(glo:q_jobnumber)
                              Get(glo:q_jobnumber,x#)
                              access:jobs.clearkey(job:ref_number_key)
                              job:ref_number  = glo:job_number_pointer
                              If access:jobs.fetch(job:ref_number_key) = Level:Benign
                                  Access:MULDESPJ.ClearKey(mulj:JobNumberOnlyKey)
                                  mulj:JobNumber = job:Ref_Number
                                  If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                      !Found
                                      Case MessageEx('Error! Job Number ' & Clip(job:Ref_Number) & ' is part of a multiple batch. '&|
                                        '<13,10>You cannot change it''s courier until you remove it from it''s batch.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      Cycle
                                  Else!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                      !Error
                                      !Assert(0,'<13,10>Fetch Error<13,10>')
                                  End!If Access:MULDESPJ.TryFetch(mulj:JobNumberOnlyKey) = Level:Benign
                                  Case job:despatch_type
                                      Of 'JOB'
                                          job:courier = cou:courier
                                          job:current_courier = job:courier
                                      Of 'EXC'
                                          job:exchange_courier = cou:courier
                                          job:current_courier = job:exchange_courier
                                      Of 'LOA'
                                          job:loan_courier = cou:courier
                                          job:current_courier = job:loan_courier
                                  End!Case job:despatch_type
                                  access:jobs.update()
                              End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
                          End!Loop x# = 1 To Records(glo:q_jobnumber)
                      Of 2 ! &No Button
                  End!Case MessageEx
      
              end
              globalrequest     = saverequest#
              Post(event:accepted,?dasuntagall)
          End !If ~Records(glo:Q_JobNumber)
      
      end!if x" = false
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Change_Courier, Accepted)
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?JOB:Ref_Number:6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JOB:Ref_Number:6, Accepted)
      ! Start Change 4437 BE(26/07/2004)
      IF Select_Courier_Temp = 'IND' AND Courier_Temp <> '' AND  |
         Select_Trade_Account_Temp = 'IND' AND Account_Number2_Temp <> '' THEN
          DespatchLocateFlag = true
      END
      ! End Change 4437 BE(26/07/2004)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?JOB:Ref_Number:6, Accepted)
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::26:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Count_Jobs:14
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:14, Accepted)
      Case MessageEx('Counting the jobs may take a very long time to complete.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
      
          Case Select_Courier_Temp
              Of 'ALL'
                  Case Select_Trade_Account_temp
                      Of 'ALL'
                          setcursor(cursor:wait)
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:ReadyToDespKey)
                          job:despatched = 'REA'
                          set(job:ReadyToDespKey,job:ReadyToDespKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:despatched <> 'REA'      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              count# += 1
                          end !loop
                          access:jobs.restorefile(save_job_id)
                          setcursor()
                      Of 'IND'
                          setcursor(cursor:wait)
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:ReadyToTradeKey)
                          job:despatched     = 'REA'
                          job:account_number = account_number2_temp
                          set(job:ReadyToTradeKey,job:ReadyToTradeKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:despatched     <> 'REA'      |
                              or job:account_number <> account_number2_temp      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              count# += 1
                          end !loop
                          access:jobs.restorefile(save_job_id)
                          setcursor()
                  End!Case Select_Trade_Account_temp
              Of 'IND'
                  Case Select_Trade_Account_temp
                      Of 'ALL'
                          setcursor(cursor:wait)
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:ReadyToCouKey)
                          job:despatched      = 'REA'
                          job:current_courier = courier_temp
                          set(job:ReadyToCouKey,job:ReadyToCouKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:despatched      <> 'REA' |
                              or job:current_courier <> courier_temp |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              count# += 1
                          end !loop
                          access:jobs.restorefile(save_job_id)
                          setcursor()
                      Of 'IND'
                          setcursor(cursor:wait)
                          save_job_id = access:jobs.savefile()
                          access:jobs.clearkey(job:ReadyToAllKey)
                          job:despatched      = 'REA'
                          job:account_number  = account_number2_temp
                          job:current_courier = courier_temp
                          set(job:ReadyToAllKey,job:ReadyToAllKey)
                          loop
                              if access:jobs.next()
                                 break
                              end !if
                              if job:despatched      <> 'REA' |
                              or job:account_number  <> account_number2_temp |
                              or job:current_courier <> courier_temp      |
                                  then break.  ! end if
                              yldcnt# += 1
                              if yldcnt# > 25
                                 yield() ; yldcnt# = 0
                              end !if
                              count# += 1
                          end !loop
                          access:jobs.restorefile(save_job_id)
                          setcursor()
                  End!Case Select_Trade_Account_temp
          End!Case Select_Courier_Temp
      
          Case MessageEx('There are '&Clip(count#)&' jobs in this browse.','ServiceBase 2000',|
                         'Styles\idea.ico','',0,0,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
          End!Case MessageEx
        Of 2 ! &No Button
      End!Case MessageEx
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Count_Jobs:14, Accepted)
    OF ?MarkLine
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MarkLine, Accepted)
      If SecurityCheck('MARK LINE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('MARK LINE')
          Access:jobse.Clearkey(jobe:refnumberkey)
          jobe:refnumber  = brw1.q.job:ref_number
          If Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign
              !Found
              If jobe:jobmark = 1
                  jobe:jobmark = 0
              Else!If jobe:jobmark = 1
                  jobe:jobmark = 1
              End!If jobe:jobmark = 1
              access:jobse.update()
          Else! If Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign
              !Error
              If Access:jobse.Primerecord() = Level:Benign
                  jobe:jobmark = 1
                  jobe:refnumber  = brw1.q.job:ref_number
                  If Access:jobse.Tryinsert()
                      Access:jobse.Cancelautoinc()
                  End!If Access:jobse.Tryinsert()
              End!If Access:jobse.Primerecord() = Level:Benign
          End! If Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign
      
      
      End!If SecurityCheck('MARK LINE')
      BRW1.ResetQueue(reset:queue)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?MarkLine, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Jobs')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Job_Number_Tab
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Job_Number_Tab)
    BRW1.ResetQueue(1)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Job_Number_Tab)
  OF ?Ready_To_Despatch
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Ready_To_Despatch)
    Do show_hide
    Hide(?insert:3)
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Ready_To_Despatch)
  OF ?courier_temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Courier')
             Post(Event:Accepted,?Lookup_Courier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Courier)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?courier_temp, AlertKey)
    END
  OF ?account_number2_temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number2_temp, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Courier')
             Post(Event:Accepted,?Lookup_Account_Number:2)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Account_Number:2)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?account_number2_temp, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, NewSelection)
      ! Start Change 2239 BE(30/04/03)
      Access:jobse.Clearkey(jobe:refnumberkey)
      jobe:refnumber  = brw1.q.job:ref_number
      IF (Access:jobse.Tryfetch(jobe:refnumberkey) = Level:Benign) THEN
          ! Start Change 2239/2 BE(09/05/03)
          !InWorkshopDate_temp = jobe:InWorkshopDate
          IF (jobe:InWorkshopDate = '') THEN
             InWorkshopDate_temp = ''
             HIDE(?InWorkshopDatePrompt)
          ELSE
              InWorkShopDate_temp = jobe:InWorkshopDate
              UNHIDE(?InWorkshopDatePrompt)
          END
          ! End Change 2239/2 BE(09/05/03)
      ELSE
          InWorkshopDate_temp = ''
          ! Start Change 2239/2 BE(09/05/03)
          HIDE(?InWorkshopDatePrompt)
          ! End Change 2239/2 BE(09/05/03)
      END
      DISPLAY(?InWorkshopDate_temp)
      ! End Change 2239 BE(30/04/03)
      
      ! Start Change 4437 BE(26/07/2004)
      IF ((CHOICE(?CurrentTab) = 16) AND (DespatchLocateFlag)) THEN
          DespatchLocateFlag = false
          IF (BRW1.Q.job:ref_number <> ?job:ref_number:6{Prop:ScreenText}) THEN
              MessageEx('Job ' & CLIP(?job:ref_number:6{Prop:ScreenText}) & ' Not Found.','ServiceBase 2000',|
                        'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,|
                        CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
          ELSE
              glo:job_number_pointer = job:ref_number
              GET(glo:q_jobnumber,glo:job_number_pointer)
              IF (ERRORCODE()) THEN
                  DO DASBRW::26:DASTAGONOFF
              END
          END
          SELECT(?job:ref_number:6)
      END
      ! End Change 4437 BE(26/07/2004)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, NewSelection)
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 8
            DASBRW::26:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      CASE CHOICE(?CurrentTab)
        OF 1
          If def:HideLocation
              ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#147L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Status~@s30@#21#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          Else !If tmp:HideLocation
              ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#147L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Location~@s30@#165#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          End !If tmp:HideLocation
          
          ?Job_Number_Tab{PROP:TEXT} = 'By Job Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='68L(2)|FM*~IMEI No~@s16@#40#34R(2)|FM*~Job No~@s7@#1#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#96L(2)|FM*~Order Number~@s30@#50#111L(2)|FM*~Model Number / Unit Type~@s60@#85#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Esn_tab{PROP:TEXT} = 'By I.M.E.I. No'
        OF 3
          ?Browse:1{PROP:FORMAT} ='68L(2)|FM*~MSN~@s15@#45#34R(2)|FM*~Job No~@s7@#1#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#96L(2)|FM*~Order Number~@s30@#50#111L(2)|FM*~Model Number / Unit Type~@s60@#85#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?msn_tab{PROP:TEXT} = 'By M.S.N.'
        OF 4
          ?Browse:1{PROP:FORMAT} ='96L(2)|FM*~Order Number~@s30@#50#34R(2)|FM*~Job No~@s7@#1#75L(2)|FM*~Model Number / Unit Type~@s255@#185#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Status~@s30@#21#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Account_No_Tab{PROP:TEXT} = 'By Account No'
        OF 5 !Surname
          ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Surname~@s30@#55#34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#93L(2)|FM*~Model Number / Unit Type~@s255@#11#46L(2)|FM*~Postcode~@s30@#65#118L(2)|FM*~Address~@s30@#205#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?surname_tab{PROP:TEXT} = 'By Surname'
        OF 6
          ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#111L(2)|FM*~Model Number / Unit Type~@s60@#85#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Location~@s30@#135#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?status_type_tab{PROP:TEXT} = 'Job Status'
        OF 7
          ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#147L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#92L(2)|FM*~Exchange Status~@s30@#130#36R(2)|FM*~Changed~@d6b@#190#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Exchange_Status{PROP:TEXT} = 'Exchange Status'
        OF 8
          ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#147L(2)|FM*~Model Number / Unit Type~@s60@#11#42R(2)|FM*~Booked~@d6b@#16#92L(2)|FM*~Loan Status~@s30@#125#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Loan_Status{PROP:TEXT} = 'Loan Status'
        OF 9
          ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Mobile Number~@s15@#60#60L(2)|FM*~Account No~@s15@#6#34R(2)|FM*~Job No~@s7@#1#93L(2)|FM*~Model Number / Unit Type~@s255@#180#128L(2)|FM*~Status~@s30@#21#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?mobile_tab{PROP:TEXT} = 'By Mobile No.'
        OF 10
          ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Unit Type~@s30@#75#34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#88L(2)|FM*~Model Number / Unit Type~@s255@#175#128L(2)|FM*~Status~@s30@#21#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Model_No_Tab{PROP:TEXT} = 'By Model No'
        OF 11
          ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#111L(2)|FM*~Model Number / Unit Type~@s60@#85#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Status~@s30@#21#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Engineer_Tab{PROP:TEXT} = 'By Engineer'
        OF 12
          If def:HideLocation
              ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#147L(2)|FM*~Model Number / Unit Type~@s60@#11#68L(2)|FM*~IMEI No~@s16@#40#60L(2)|FM*~Status~@s30@#21#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          Else !If def:HideLocation
              ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#147L(2)|FM*~Model Number / Unit Type~@s60@#11#68L(2)|FM*~IMEI No~@s16@#40#60L(2)|FM*~Location~@s30@#135#42R(2)|FM*~Booked~@d6b@#16#60L(2)|FM*~Account No~@s15@#6#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          End !If def:HideLocation
          
          ?Unallocated_Jobs_Tab{PROP:TEXT} = 'Unallocated Jobs'
        OF 13
          ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Consignment No~@s30@#140#34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#93L(2)|FM*~Model Number / Unit Type~@s255@#180#128L(2)|FM*~Status~@s30@#21#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Tab16{PROP:TEXT} = 'In Consign No'
        OF 14
          ?Browse:1{PROP:FORMAT} ='60L(2)|FM*~Consignment No~@s30@#105#34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#88L(2)|FM*~Model Number / Unit Type~@s255@#175#128L(2)|FM*~Status~@s30@#21#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Consignment_No_Tab{PROP:TEXT} = 'Out Consign No'
        OF 15
          ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#111L(2)|FM*~Model Number / Unit Type~@s60@#85#42R(2)|FM*~Booked~@d6b@#16#128L(2)|FM*~Status~@s30@#21#36R(2)|FM*~Changed~@d6b@#170#10CFI~O~@s1@#26#10CFI~F~@s1@#28#10CFI~C~@s1@#32#10CFI~W~@s1@#34#10CFI~D~@s1@#36#10C|FI~P~@s1@#38#'
          ?Batch_Number_Tab{PROP:TEXT} = 'By Batch No'
        OF 16
          ?Browse:1{PROP:FORMAT} ='34R(2)|FM*~Job No~@s7@#1#60L(2)|FM*~Account No~@s15@#6#68L(2)|FM*~IMEI No~@s16@#40#60L(2)|FM*~Surname~@s30@#55#124L(2)|FM*~Model Number / Unit Type~@s60@#100#80L(2)|FM*~Courier~@s30@#110#34L(2)|FM*~Type~@s3@#115#11L(2)FI@s1@#30#'
          ?Ready_To_Despatch{PROP:TEXT} = 'Despatch'
      END
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case Keycode()
          Of f4key
              thiswindow.reset
              Post(Event:accepted,?Validate_ESN)
          Of f5key
              brw1.resetsort(1)
          Of F8Key
              Case Choice(?CurrentTab)
                  Of 5
                      Post(Event:Accepted,?SearchByAddress)
              End !Case Choice(?CurrentTab)
      End!Case Keycode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
          0{prop:buffer} = 1
      Post(Event:Accepted,?tmp:AccountSearchOrder)
      Post(event:newselection,?CurrentTab)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    Case job:Despatch_Type
        Of 'JOB'
            If AccessoryCheck('JOB')
                If AccessoryMismatch(job:Ref_Number,'JOB')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'JOB')
            End !If AccessoryCheck('JOB')
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,job:Despatch_Type)
        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Notes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If job:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            aud:Notes         = Clip(aud:Notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            aud:Ref_Number    = job:Ref_Number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = job:Despatch_Type
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'FAILED DESPATCH VALIDATION'
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
        Access:JOBS.Update()
        Return Level:Fatal
        
    End !If Return Level:Fatal

    Return Level:Benign
LocalValidateIMEI    Procedure()
local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            
        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_Number) & '.' &|
          '<13,10>'&|
          '<13,10>I.M.E.I. Number mismatch.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        GetStatus(850,1,job:Despatch_Type)
        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Notes         = 'I.M.E.I. NUMBER MISMATCH'
            aud:Ref_Number    = job:ref_number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = job:Despatch_Type
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'FAILED DESPATCH VALIDATION'
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccountNumber      Procedure()
    Code

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Case MessageEx('The Trade Account attached to Job Number '&Clip(job:ref_number)&' is only used for ''Batch Despatch''.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'Skip Futher Errors',skip_error#,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                    Of 1 ! &OK Button
                End!Case MessageEx
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The Trade Account is on stop and it is not a Warranty Job.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                 If tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The Trade Account is on stop and it is not a Warranty Job.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Benign
LocalFailedDeliveryAudit        Procedure(String  func:Type)
    Code
    GetStatus(899,1,func:Type)
    Access:JOBS.Update()

    If Access:AUDIT.PrimeRecord() = Level:Benign
        aud:Ref_Number    = job:ref_number
        aud:Date          = Today()
        aud:Time          = Clock()
        aud:Type          = func:Type
        Access:USERS.ClearKey(use:Password_Key)
        use:Password      = glo:Password
        Access:USERS.Fetch(use:Password_Key)
        aud:User          = use:User_Code
        aud:Action        = 'FAILED DELIVERY (' & Clip(func:Type) & ')'
        Access:AUDIT.Insert()
    End!If Access:AUDIT.PrimeRecord() = Level:Benign

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))
  PARENT.ResetQueue(ResetMode)
  If Records(Queue:Browse:1)
      Enable(?Button_Group)
      Enable(?Button_Group2)
  
  Else!If Records(Queue:Browse:1)
      Disable(?Button_Group)
      Disable(?Button_Group2)
  End!If Records(Queue:Browse:1)
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF CHOICE(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 0
    RETURN SELF.SetSort(3,Force)
  ELSIF CHOICE(?CurrentTab) = 4 And tmp:AccountSearchOrder = 1
    RETURN SELF.SetSort(4,Force)
  ELSIF CHOICE(?CurrentTab) = 5
    RETURN SELF.SetSort(5,Force)
  ELSIF CHOICE(?CurrentTab) = 6 And Upper(tmp:SelectLocation) = 1
    RETURN SELF.SetSort(6,Force)
  ELSIF CHOICE(?CurrentTab) = 6 And Upper(tmp:SelectLocation) = 2
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 7
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 8
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 9
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 10
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'YES'
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'NO'
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 11 And Upper(completed2_temp) = 'ALL'
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 12 And Upper(unallocated_temp) = 'INW'
    RETURN SELF.SetSort(15,Force)
  ELSIF Choice(?CurrentTab) = 12 And Upper(unallocated_temp) = 'NIW'
    RETURN SELF.SetSort(16,Force)
  ELSIF Choice(?CurrentTab) = 13
    RETURN SELF.SetSort(17,Force)
  ELSIF Choice(?CurrentTab) = 14
    RETURN SELF.SetSort(18,Force)
  ELSIF Choice(?CurrentTab) = 15 And Upper(Completed2_Temp) = 'ALL'
    RETURN SELF.SetSort(19,Force)
  ELSIF Choice(?CurrentTab) = 15 And Upper(Completed2_Temp) = 'NO'
    RETURN SELF.SetSort(20,Force)
  ELSIF Choice(?CurrentTab) = 15 And Upper(Completed2_Temp) = 'YES'
    RETURN SELF.SetSort(21,Force)
  ELSIF Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'ALL' And Upper(Select_trade_account_temp) = 'ALL'
    RETURN SELF.SetSort(22,Force)
  ELSIF Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'ALL' And Upper(select_trade_account_temp) = 'IND'
    RETURN SELF.SetSort(23,Force)
  ELSIF Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'ALL'
    RETURN SELF.SetSort(24,Force)
  ELSIF Choice(?CurrentTab) = 16 And Upper(select_courier_temp) = 'IND' And Upper(select_trade_account_temp) = 'IND'
    RETURN SELF.SetSort(25,Force)
  ELSIF Choice(?CurrentTab) = 16
    RETURN SELF.SetSort(26,Force)
  ELSE
    RETURN SELF.SetSort(27,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     GET(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  model_unit_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  address_temp = CLIP(job:Address_Line1) & ',  ' & CLIP(job:Address_Line2) & ',  ' & CLIP(job:Address_Line3)
  model_unit2_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit3_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit3_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit4_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  model_unit5_temp = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  despatch_type_temp = job:Despatch_Type
  tmp:Location = job:Location
  tmp:ModelUnit3 = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  tmp:ModelUnit1 = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  tmp:ModelUnit3 = CLIP(job:Model_Number) & ' - ' & CLIP(job:Unit_Type)
  Case def:browse_option
      Of 'INV'
          tmp:address         = job:company_Name & '<13,10>' & |
                                job:address_line1 & '<13,10>' & |
                                job:address_line2 & '<13,10>' & |
                                job:address_line3 & '<13,10>' & |
                                job:postcode & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_number) & '<13,10>' & |
                                'Fax No: ' & Clip(job:fax_number)
      Of 'COL'
          tmp:address         = job:company_Name_collection & '<13,10>' & |
                                job:address_line1_collection & '<13,10>' & |
                                job:address_line2_collection & '<13,10>' & |
                                job:address_line3_collection & '<13,10>' & |
                                job:postcode_collection & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_collection)
      Of 'DEL'
          tmp:address         = job:company_Name_delivery & '<13,10>' & |
                                job:address_line1_delivery & '<13,10>' & |
                                job:address_line2_Delivery & '<13,10>' & |
                                job:address_line3_delivery & '<13,10>' & |
                                job:postcode_delivery & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_delivery)
      Else
          tmp:address         = job:company_Name & '<13,10>' & |
                                job:address_line1 & '<13,10>' & |
                                job:address_line2 & '<13,10>' & |
                                job:address_line3 & '<13,10>' & |
                                job:postcode & '<13,10>' & |
                                'Telephone No: ' & Clip(job:telephone_number) & '<13,10>' & |
                                'Fax No: ' & Clip(job:fax_number)
  
  End!Case def:browse_option
  
  If job:exchange_unit_number <> ''
      access:exchange.clearkey(xch:ref_number_key)
      xch:ref_number  = job:exchange_unit_number
      If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
          tmp:exchangetext    = 'EXCHANGE unit issued: ' & CLip(xch:esn)
      End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
  ElsIf job:loan_unit_number <> ''
      access:loan.clearkey(loa:ref_number_key)
      loa:ref_number  = job:loan_unit_number
      If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
          tmp:exchangetext    = 'LOAN unit issued: ' & Clip(loa:esn)
      End!If access:loan.tryfetch(loa:ref_number_key) = Level:Benign
  Else
      tmp:exchangetext    = ''
  End!If job:loan_unit_number <> ''
  
  tmp:StatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = job:Ref_Number
  aus:Type        = 'JOB'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> job:Ref_Number      |
      Or aus:Type        <> 'JOB'      |
          Then Break.  ! End If
      tmp:StatusDate  =  aus:DateChanged
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:LoanStatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = job:Ref_Number
  aus:Type        = 'LOA'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> job:Ref_Number      |
      Or aus:Type        <> 'LOA'      |
          Then Break.  ! End If
      tmp:LoanStatusDate  =  aus:DateChanged
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  tmp:ExchangeStatusDate = ''
  Save_aus_ID = Access:AUDSTATS.SaveFile()
  Access:AUDSTATS.ClearKey(aus:DateChangedKey)
  aus:RefNumber   = job:Ref_Number
  aus:Type        = 'EXC'
  aus:DateChanged = Today()
  Set(aus:DateChangedKey,aus:DateChangedKey)
  Loop
      If Access:AUDSTATS.PREVIOUS()
         Break
      End !If
      If aus:RefNumber   <> job:Ref_Number      |
      Or aus:Type        <> 'EXC'      |
          Then Break.  ! End If
      tmp:ExchangeStatusDate  =  aus:DateChanged
      Break
  End !Loop
  Access:AUDSTATS.RestoreFile(Save_aus_ID)
  
  
  
  
  
  
  PARENT.SetQueueRecord
  overdue2_temp = 'NO'
  turnaround# = 0
  status# = 0
  If job:turnaround_end_date <> ''
      If job:date_completed = '' And job:turnaround_end_date <> 90950
          If job:turnaround_end_date < Today()
              turnaround# = 1
          Else!If job:turnaround_end_date < Today()
              If job:turnaround_end_date = Today() And job:turnaround_end_time < Clock()
                  turnaround# = 1
              End!If job:turnaround_end_date = Today() And job:turnaround_end_time < Clock()
          End!If job:turnaround_end_date < Today()
      End!If job:date_completed = '' And job:turnaround_end_date <> 90950
  End!If job:turnaround_end_date <> ''
  If job:status_end_date <> ''
      If overdue2_temp = 'NO' And job:date_completed = '' And job:status_end_date <> 90950
          If job:status_end_date < Today()
              status# = 1
          Else!If job:status_end_date < Today()
              If job:status_end_date = Today() and job:status_end_time < Clock()
                  status# = 1
              End!If job:status_end_date = Today() and job:status_end_time < Clock()
          End!If job:status_end_date < Today()
      End!If overdue# = 0 And job:date_completed And job:status_end_date <> 90950
  End!If job:status_end_date <> ''
  If turnaround# = 1 And status# = 1
      overdue2_temp = 'BOT'
  End!If turnaround# = 1 And status# = 1
  If turnaround# = 0 and status# = 1
      overdue2_temp = 'STA'
  End!If turnaround# = 0 and status# = 1
  If turnaround# = 1 and status# = 0
      overdue2_temp = 'TUR'
  End!If turnaround# = 1 and status# = 0
  
  
  SELF.Q.job:Ref_Number_NormalFG = -1
  SELF.Q.job:Ref_Number_NormalBG = -1
  SELF.Q.job:Ref_Number_SelectedFG = -1
  SELF.Q.job:Ref_Number_SelectedBG = -1
  SELF.Q.job:Account_Number_NormalFG = -1
  SELF.Q.job:Account_Number_NormalBG = -1
  SELF.Q.job:Account_Number_SelectedFG = -1
  SELF.Q.job:Account_Number_SelectedBG = -1
  SELF.Q.model_unit_temp_NormalFG = -1
  SELF.Q.model_unit_temp_NormalBG = -1
  SELF.Q.model_unit_temp_SelectedFG = -1
  SELF.Q.model_unit_temp_SelectedBG = -1
  SELF.Q.job:date_booked_NormalFG = -1
  SELF.Q.job:date_booked_NormalBG = -1
  SELF.Q.job:date_booked_SelectedFG = -1
  SELF.Q.job:date_booked_SelectedBG = -1
  SELF.Q.job:Current_Status_NormalFG = -1
  SELF.Q.job:Current_Status_NormalBG = -1
  SELF.Q.job:Current_Status_SelectedFG = -1
  SELF.Q.job:Current_Status_SelectedBG = -1
  IF (overdue2_temp = 'BOT')
    SELF.Q.Overdue_Temp_Icon = 7
  ELSIF (overdue2_temp = 'STA')
    SELF.Q.Overdue_Temp_Icon = 8
  ELSIF (overdue2_temp = 'TUR')
    SELF.Q.Overdue_Temp_Icon = 9
  ELSE
    SELF.Q.Overdue_Temp_Icon = 11
  END
  IF (job:date_completed <> '')
    SELF.Q.Completed_Temp_Icon = 6
  ELSE
    SELF.Q.Completed_Temp_Icon = 11
  END
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 12
  ELSE
    SELF.Q.tag_temp_Icon = 11
  END
  IF (job:chargeable_job = 'YES' And job:invoice_number <> '')
    SELF.Q.Invoiced_Temp_Icon = 10
  ELSIF (job:chargeable_job = 'YES' and job:invoice_number = '')
    SELF.Q.Invoiced_Temp_Icon = 5
  ELSE
    SELF.Q.Invoiced_Temp_Icon = 11
  END
  IF (job:warranty_job = 'YES' And job:edi_batch_number <> '')
    SELF.Q.Warranty_Temp_Icon = 10
  ELSIF (job:warranty_job = 'YES' and job:edi_batch_number = '')
    SELF.Q.Warranty_Temp_Icon = 5
  ELSE
    SELF.Q.Warranty_Temp_Icon = 11
  END
  IF (job:despatched = 'YES')
    SELF.Q.Collected_Temp_Icon = 4
  ELSE
    SELF.Q.Collected_Temp_Icon = 11
  END
  IF (job:paid = 'YES' And job:invoice_number_warranty <> '')
    SELF.Q.Paid_Temp_Icon = 1
  ELSIF (job:paid = 'YES' And job:invoice_number_warranty = '')
    SELF.Q.Paid_Temp_Icon = 3
  ELSIF (job:paid <> 'YES' and job:invoice_number_warranty <> '')
    SELF.Q.Paid_Temp_Icon = 2
  ELSE
    SELF.Q.Paid_Temp_Icon = 11
  END
  SELF.Q.job:ESN_NormalFG = -1
  SELF.Q.job:ESN_NormalBG = -1
  SELF.Q.job:ESN_SelectedFG = -1
  SELF.Q.job:ESN_SelectedBG = -1
  SELF.Q.job:MSN_NormalFG = -1
  SELF.Q.job:MSN_NormalBG = -1
  SELF.Q.job:MSN_SelectedFG = -1
  SELF.Q.job:MSN_SelectedBG = -1
  SELF.Q.job:Order_Number_NormalFG = -1
  SELF.Q.job:Order_Number_NormalBG = -1
  SELF.Q.job:Order_Number_SelectedFG = -1
  SELF.Q.job:Order_Number_SelectedBG = -1
  SELF.Q.job:Surname_NormalFG = -1
  SELF.Q.job:Surname_NormalBG = -1
  SELF.Q.job:Surname_SelectedFG = -1
  SELF.Q.job:Surname_SelectedBG = -1
  SELF.Q.job:Mobile_Number_NormalFG = -1
  SELF.Q.job:Mobile_Number_NormalBG = -1
  SELF.Q.job:Mobile_Number_SelectedFG = -1
  SELF.Q.job:Mobile_Number_SelectedBG = -1
  SELF.Q.job:Postcode_NormalFG = -1
  SELF.Q.job:Postcode_NormalBG = -1
  SELF.Q.job:Postcode_SelectedFG = -1
  SELF.Q.job:Postcode_SelectedBG = -1
  SELF.Q.job:Model_Number_NormalFG = -1
  SELF.Q.job:Model_Number_NormalBG = -1
  SELF.Q.job:Model_Number_SelectedFG = -1
  SELF.Q.job:Model_Number_SelectedBG = -1
  SELF.Q.job:Unit_Type_NormalFG = -1
  SELF.Q.job:Unit_Type_NormalBG = -1
  SELF.Q.job:Unit_Type_SelectedFG = -1
  SELF.Q.job:Unit_Type_SelectedBG = -1
  SELF.Q.address_temp_NormalFG = -1
  SELF.Q.address_temp_NormalBG = -1
  SELF.Q.address_temp_SelectedFG = -1
  SELF.Q.address_temp_SelectedBG = -1
  SELF.Q.model_unit2_temp_NormalFG = -1
  SELF.Q.model_unit2_temp_NormalBG = -1
  SELF.Q.model_unit2_temp_SelectedFG = -1
  SELF.Q.model_unit2_temp_SelectedBG = -1
  SELF.Q.model_unit3_temp_NormalFG = -1
  SELF.Q.model_unit3_temp_NormalBG = -1
  SELF.Q.model_unit3_temp_SelectedFG = -1
  SELF.Q.model_unit3_temp_SelectedBG = -1
  SELF.Q.model_unit4_temp_NormalFG = -1
  SELF.Q.model_unit4_temp_NormalBG = -1
  SELF.Q.model_unit4_temp_SelectedFG = -1
  SELF.Q.model_unit4_temp_SelectedBG = -1
  SELF.Q.model_unit5_temp_NormalFG = -1
  SELF.Q.model_unit5_temp_NormalBG = -1
  SELF.Q.model_unit5_temp_SelectedFG = -1
  SELF.Q.model_unit5_temp_SelectedBG = -1
  SELF.Q.job:Consignment_Number_NormalFG = -1
  SELF.Q.job:Consignment_Number_NormalBG = -1
  SELF.Q.job:Consignment_Number_SelectedFG = -1
  SELF.Q.job:Consignment_Number_SelectedBG = -1
  SELF.Q.job:Current_Courier_NormalFG = -1
  SELF.Q.job:Current_Courier_NormalBG = -1
  SELF.Q.job:Current_Courier_SelectedFG = -1
  SELF.Q.job:Current_Courier_SelectedBG = -1
  SELF.Q.despatch_type_temp_NormalFG = -1
  SELF.Q.despatch_type_temp_NormalBG = -1
  SELF.Q.despatch_type_temp_SelectedFG = -1
  SELF.Q.despatch_type_temp_SelectedBG = -1
  SELF.Q.status2_temp_NormalFG = -1
  SELF.Q.status2_temp_NormalBG = -1
  SELF.Q.status2_temp_SelectedFG = -1
  SELF.Q.status2_temp_SelectedBG = -1
  SELF.Q.job:Loan_Status_NormalFG = -1
  SELF.Q.job:Loan_Status_NormalBG = -1
  SELF.Q.job:Loan_Status_SelectedFG = -1
  SELF.Q.job:Loan_Status_SelectedBG = -1
  SELF.Q.job:Exchange_Status_NormalFG = -1
  SELF.Q.job:Exchange_Status_NormalBG = -1
  SELF.Q.job:Exchange_Status_SelectedFG = -1
  SELF.Q.job:Exchange_Status_SelectedBG = -1
  SELF.Q.job:Location_NormalFG = -1
  SELF.Q.job:Location_NormalBG = -1
  SELF.Q.job:Location_SelectedFG = -1
  SELF.Q.job:Location_SelectedBG = -1
  SELF.Q.job:Incoming_Consignment_Number_NormalFG = -1
  SELF.Q.job:Incoming_Consignment_Number_NormalBG = -1
  SELF.Q.job:Incoming_Consignment_Number_SelectedFG = -1
  SELF.Q.job:Incoming_Consignment_Number_SelectedBG = -1
  SELF.Q.job:Exchange_Unit_Number_NormalFG = -1
  SELF.Q.job:Exchange_Unit_Number_NormalBG = -1
  SELF.Q.job:Exchange_Unit_Number_SelectedFG = -1
  SELF.Q.job:Exchange_Unit_Number_SelectedBG = -1
  SELF.Q.job:Loan_Unit_Number_NormalFG = -1
  SELF.Q.job:Loan_Unit_Number_NormalBG = -1
  SELF.Q.job:Loan_Unit_Number_SelectedFG = -1
  SELF.Q.job:Loan_Unit_Number_SelectedBG = -1
  SELF.Q.tmp:ExchangeText_NormalFG = -1
  SELF.Q.tmp:ExchangeText_NormalBG = -1
  SELF.Q.tmp:ExchangeText_SelectedFG = -1
  SELF.Q.tmp:ExchangeText_SelectedBG = -1
  SELF.Q.tmp:Address_NormalFG = -1
  SELF.Q.tmp:Address_NormalBG = -1
  SELF.Q.tmp:Address_SelectedFG = -1
  SELF.Q.tmp:Address_SelectedBG = -1
  SELF.Q.tmp:Location_NormalFG = -1
  SELF.Q.tmp:Location_NormalBG = -1
  SELF.Q.tmp:Location_SelectedFG = -1
  SELF.Q.tmp:Location_SelectedBG = -1
  SELF.Q.tmp:StatusDate_NormalFG = -1
  SELF.Q.tmp:StatusDate_NormalBG = -1
  SELF.Q.tmp:StatusDate_SelectedFG = -1
  SELF.Q.tmp:StatusDate_SelectedBG = -1
  SELF.Q.tmp:ModelUnit1_NormalFG = -1
  SELF.Q.tmp:ModelUnit1_NormalBG = -1
  SELF.Q.tmp:ModelUnit1_SelectedFG = -1
  SELF.Q.tmp:ModelUnit1_SelectedBG = -1
  SELF.Q.tmp:ModelUnit2_NormalFG = -1
  SELF.Q.tmp:ModelUnit2_NormalBG = -1
  SELF.Q.tmp:ModelUnit2_SelectedFG = -1
  SELF.Q.tmp:ModelUnit2_SelectedBG = -1
  SELF.Q.tmp:ModelUnit3_NormalFG = -1
  SELF.Q.tmp:ModelUnit3_NormalBG = -1
  SELF.Q.tmp:ModelUnit3_SelectedFG = -1
  SELF.Q.tmp:ModelUnit3_SelectedBG = -1
  SELF.Q.tmp:ExchangeStatusDate_NormalFG = -1
  SELF.Q.tmp:ExchangeStatusDate_NormalBG = -1
  SELF.Q.tmp:ExchangeStatusDate_SelectedFG = -1
  SELF.Q.tmp:ExchangeStatusDate_SelectedBG = -1
  SELF.Q.tmp:LoanStatusDate_NormalFG = -1
  SELF.Q.tmp:LoanStatusDate_NormalBG = -1
  SELF.Q.tmp:LoanStatusDate_SelectedFG = -1
  SELF.Q.tmp:LoanStatusDate_SelectedBG = -1
  SELF.Q.tmp:LocationStatus_NormalFG = -1
  SELF.Q.tmp:LocationStatus_NormalBG = -1
  SELF.Q.tmp:LocationStatus_SelectedFG = -1
  SELF.Q.tmp:LocationStatus_SelectedBG = -1
  SELF.Q.job:Address_Line1_NormalFG = -1
  SELF.Q.job:Address_Line1_NormalBG = -1
  SELF.Q.job:Address_Line1_SelectedFG = -1
  SELF.Q.job:Address_Line1_SelectedBG = -1
  SELF.Q.model_unit_temp = model_unit_temp            !Assign formula result to display queue
  SELF.Q.address_temp = address_temp                  !Assign formula result to display queue
  SELF.Q.model_unit2_temp = model_unit2_temp          !Assign formula result to display queue
  SELF.Q.model_unit3_temp = model_unit3_temp          !Assign formula result to display queue
  SELF.Q.model_unit3_temp = model_unit3_temp          !Assign formula result to display queue
  SELF.Q.model_unit4_temp = model_unit4_temp          !Assign formula result to display queue
  SELF.Q.model_unit5_temp = model_unit5_temp          !Assign formula result to display queue
  SELF.Q.despatch_type_temp = despatch_type_temp      !Assign formula result to display queue
  SELF.Q.tmp:Location = tmp:Location                  !Assign formula result to display queue
  SELF.Q.tmp:ModelUnit1 = tmp:ModelUnit1              !Assign formula result to display queue
  SELF.Q.tmp:ModelUnit3 = tmp:ModelUnit3              !Assign formula result to display queue
  SELF.Q.tmp:ModelUnit3 = tmp:ModelUnit3              !Assign formula result to display queue
   
   
   IF (jobe:jobmark = 1)
     SELF.Q.job:Ref_Number_NormalFG = 255
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Ref_Number_NormalFG = 0
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Account_Number_NormalFG = 255
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Account_Number_NormalFG = 0
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.model_unit_temp_NormalFG = 255
     SELF.Q.model_unit_temp_NormalBG = 16777215
     SELF.Q.model_unit_temp_SelectedFG = 16777215
     SELF.Q.model_unit_temp_SelectedBG = 255
   ELSE
     SELF.Q.model_unit_temp_NormalFG = 0
     SELF.Q.model_unit_temp_NormalBG = 16777215
     SELF.Q.model_unit_temp_SelectedFG = 16777215
     SELF.Q.model_unit_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:date_booked_NormalFG = 255
     SELF.Q.job:date_booked_NormalBG = 16777215
     SELF.Q.job:date_booked_SelectedFG = 16777215
     SELF.Q.job:date_booked_SelectedBG = 255
   ELSE
     SELF.Q.job:date_booked_NormalFG = 0
     SELF.Q.job:date_booked_NormalBG = 16777215
     SELF.Q.job:date_booked_SelectedFG = 16777215
     SELF.Q.job:date_booked_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Current_Status_NormalFG = 255
     SELF.Q.job:Current_Status_NormalBG = 16777215
     SELF.Q.job:Current_Status_SelectedFG = 16777215
     SELF.Q.job:Current_Status_SelectedBG = 255
   ELSE
     SELF.Q.job:Current_Status_NormalFG = 0
     SELF.Q.job:Current_Status_NormalBG = 16777215
     SELF.Q.job:Current_Status_SelectedFG = 16777215
     SELF.Q.job:Current_Status_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:ESN_NormalFG = 255
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 255
   ELSE
     SELF.Q.job:ESN_NormalFG = 0
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:MSN_NormalFG = 255
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 255
   ELSE
     SELF.Q.job:MSN_NormalFG = 0
     SELF.Q.job:MSN_NormalBG = 16777215
     SELF.Q.job:MSN_SelectedFG = 16777215
     SELF.Q.job:MSN_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Order_Number_NormalFG = 255
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Order_Number_NormalFG = 0
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Surname_NormalFG = 255
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 255
   ELSE
     SELF.Q.job:Surname_NormalFG = 0
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Mobile_Number_NormalFG = 255
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Mobile_Number_NormalFG = 0
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Postcode_NormalFG = 255
     SELF.Q.job:Postcode_NormalBG = 16777215
     SELF.Q.job:Postcode_SelectedFG = 16777215
     SELF.Q.job:Postcode_SelectedBG = 255
   ELSE
     SELF.Q.job:Postcode_NormalFG = 0
     SELF.Q.job:Postcode_NormalBG = 16777215
     SELF.Q.job:Postcode_SelectedFG = 16777215
     SELF.Q.job:Postcode_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Model_Number_NormalFG = 255
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Model_Number_NormalFG = 0
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Unit_Type_NormalFG = 255
     SELF.Q.job:Unit_Type_NormalBG = 16777215
     SELF.Q.job:Unit_Type_SelectedFG = 16777215
     SELF.Q.job:Unit_Type_SelectedBG = 255
   ELSE
     SELF.Q.job:Unit_Type_NormalFG = 0
     SELF.Q.job:Unit_Type_NormalBG = 16777215
     SELF.Q.job:Unit_Type_SelectedFG = 16777215
     SELF.Q.job:Unit_Type_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.address_temp_NormalFG = 255
     SELF.Q.address_temp_NormalBG = 16777215
     SELF.Q.address_temp_SelectedFG = 16777215
     SELF.Q.address_temp_SelectedBG = 255
   ELSE
     SELF.Q.address_temp_NormalFG = 0
     SELF.Q.address_temp_NormalBG = 16777215
     SELF.Q.address_temp_SelectedFG = 16777215
     SELF.Q.address_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.model_unit2_temp_NormalFG = 255
     SELF.Q.model_unit2_temp_NormalBG = 16777215
     SELF.Q.model_unit2_temp_SelectedFG = 16777215
     SELF.Q.model_unit2_temp_SelectedBG = 255
   ELSE
     SELF.Q.model_unit2_temp_NormalFG = 0
     SELF.Q.model_unit2_temp_NormalBG = 16777215
     SELF.Q.model_unit2_temp_SelectedFG = 16777215
     SELF.Q.model_unit2_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.model_unit3_temp_NormalFG = 255
     SELF.Q.model_unit3_temp_NormalBG = 16777215
     SELF.Q.model_unit3_temp_SelectedFG = 16777215
     SELF.Q.model_unit3_temp_SelectedBG = 255
   ELSE
     SELF.Q.model_unit3_temp_NormalFG = 0
     SELF.Q.model_unit3_temp_NormalBG = 16777215
     SELF.Q.model_unit3_temp_SelectedFG = 16777215
     SELF.Q.model_unit3_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.model_unit4_temp_NormalFG = 255
     SELF.Q.model_unit4_temp_NormalBG = 16777215
     SELF.Q.model_unit4_temp_SelectedFG = 16777215
     SELF.Q.model_unit4_temp_SelectedBG = 255
   ELSE
     SELF.Q.model_unit4_temp_NormalFG = 0
     SELF.Q.model_unit4_temp_NormalBG = 16777215
     SELF.Q.model_unit4_temp_SelectedFG = 16777215
     SELF.Q.model_unit4_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.model_unit5_temp_NormalFG = 255
     SELF.Q.model_unit5_temp_NormalBG = 16777215
     SELF.Q.model_unit5_temp_SelectedFG = 16777215
     SELF.Q.model_unit5_temp_SelectedBG = 255
   ELSE
     SELF.Q.model_unit5_temp_NormalFG = 0
     SELF.Q.model_unit5_temp_NormalBG = 16777215
     SELF.Q.model_unit5_temp_SelectedFG = 16777215
     SELF.Q.model_unit5_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Consignment_Number_NormalFG = 255
     SELF.Q.job:Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Consignment_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Consignment_Number_NormalFG = 0
     SELF.Q.job:Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Consignment_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Current_Courier_NormalFG = 255
     SELF.Q.job:Current_Courier_NormalBG = 16777215
     SELF.Q.job:Current_Courier_SelectedFG = 16777215
     SELF.Q.job:Current_Courier_SelectedBG = 255
   ELSE
     SELF.Q.job:Current_Courier_NormalFG = 0
     SELF.Q.job:Current_Courier_NormalBG = 16777215
     SELF.Q.job:Current_Courier_SelectedFG = 16777215
     SELF.Q.job:Current_Courier_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.despatch_type_temp_NormalFG = 255
     SELF.Q.despatch_type_temp_NormalBG = 16777215
     SELF.Q.despatch_type_temp_SelectedFG = 16777215
     SELF.Q.despatch_type_temp_SelectedBG = 255
   ELSE
     SELF.Q.despatch_type_temp_NormalFG = 0
     SELF.Q.despatch_type_temp_NormalBG = 16777215
     SELF.Q.despatch_type_temp_SelectedFG = 16777215
     SELF.Q.despatch_type_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.status2_temp_NormalFG = 255
     SELF.Q.status2_temp_NormalBG = 16777215
     SELF.Q.status2_temp_SelectedFG = 16777215
     SELF.Q.status2_temp_SelectedBG = 255
   ELSE
     SELF.Q.status2_temp_NormalFG = 0
     SELF.Q.status2_temp_NormalBG = 16777215
     SELF.Q.status2_temp_SelectedFG = 16777215
     SELF.Q.status2_temp_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Loan_Status_NormalFG = 255
     SELF.Q.job:Loan_Status_NormalBG = 16777215
     SELF.Q.job:Loan_Status_SelectedFG = 16777215
     SELF.Q.job:Loan_Status_SelectedBG = 255
   ELSE
     SELF.Q.job:Loan_Status_NormalFG = 0
     SELF.Q.job:Loan_Status_NormalBG = 16777215
     SELF.Q.job:Loan_Status_SelectedFG = 16777215
     SELF.Q.job:Loan_Status_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Exchange_Status_NormalFG = 255
     SELF.Q.job:Exchange_Status_NormalBG = 16777215
     SELF.Q.job:Exchange_Status_SelectedFG = 16777215
     SELF.Q.job:Exchange_Status_SelectedBG = 255
   ELSE
     SELF.Q.job:Exchange_Status_NormalFG = 0
     SELF.Q.job:Exchange_Status_NormalBG = 16777215
     SELF.Q.job:Exchange_Status_SelectedFG = 16777215
     SELF.Q.job:Exchange_Status_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Location_NormalFG = 255
     SELF.Q.job:Location_NormalBG = 16777215
     SELF.Q.job:Location_SelectedFG = 16777215
     SELF.Q.job:Location_SelectedBG = 255
   ELSE
     SELF.Q.job:Location_NormalFG = 0
     SELF.Q.job:Location_NormalBG = 16777215
     SELF.Q.job:Location_SelectedFG = 16777215
     SELF.Q.job:Location_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 255
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Incoming_Consignment_Number_NormalFG = 0
     SELF.Q.job:Incoming_Consignment_Number_NormalBG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedFG = 16777215
     SELF.Q.job:Incoming_Consignment_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 255
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Exchange_Unit_Number_NormalFG = 0
     SELF.Q.job:Exchange_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Exchange_Unit_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Loan_Unit_Number_NormalFG = 255
     SELF.Q.job:Loan_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Loan_Unit_Number_NormalFG = 0
     SELF.Q.job:Loan_Unit_Number_NormalBG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedFG = 16777215
     SELF.Q.job:Loan_Unit_Number_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:ExchangeText_NormalFG = 255
     SELF.Q.tmp:ExchangeText_NormalBG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ExchangeText_NormalFG = 0
     SELF.Q.tmp:ExchangeText_NormalBG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeText_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:Address_NormalFG = 255
     SELF.Q.tmp:Address_NormalBG = 16777215
     SELF.Q.tmp:Address_SelectedFG = 16777215
     SELF.Q.tmp:Address_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Address_NormalFG = 0
     SELF.Q.tmp:Address_NormalBG = 16777215
     SELF.Q.tmp:Address_SelectedFG = 16777215
     SELF.Q.tmp:Address_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:Location_NormalFG = 255
     SELF.Q.tmp:Location_NormalBG = 16777215
     SELF.Q.tmp:Location_SelectedFG = 16777215
     SELF.Q.tmp:Location_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Location_NormalFG = 0
     SELF.Q.tmp:Location_NormalBG = 16777215
     SELF.Q.tmp:Location_SelectedFG = 16777215
     SELF.Q.tmp:Location_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:StatusDate_NormalFG = 255
     SELF.Q.tmp:StatusDate_NormalBG = 16777215
     SELF.Q.tmp:StatusDate_SelectedFG = 16777215
     SELF.Q.tmp:StatusDate_SelectedBG = 255
   ELSE
     SELF.Q.tmp:StatusDate_NormalFG = 0
     SELF.Q.tmp:StatusDate_NormalBG = 16777215
     SELF.Q.tmp:StatusDate_SelectedFG = 16777215
     SELF.Q.tmp:StatusDate_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:ModelUnit1_NormalFG = 255
     SELF.Q.tmp:ModelUnit1_NormalBG = 16777215
     SELF.Q.tmp:ModelUnit1_SelectedFG = 16777215
     SELF.Q.tmp:ModelUnit1_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ModelUnit1_NormalFG = 0
     SELF.Q.tmp:ModelUnit1_NormalBG = 16777215
     SELF.Q.tmp:ModelUnit1_SelectedFG = 16777215
     SELF.Q.tmp:ModelUnit1_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:ModelUnit2_NormalFG = 255
     SELF.Q.tmp:ModelUnit2_NormalBG = 16777215
     SELF.Q.tmp:ModelUnit2_SelectedFG = 16777215
     SELF.Q.tmp:ModelUnit2_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ModelUnit2_NormalFG = 0
     SELF.Q.tmp:ModelUnit2_NormalBG = 16777215
     SELF.Q.tmp:ModelUnit2_SelectedFG = 16777215
     SELF.Q.tmp:ModelUnit2_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:ModelUnit3_NormalFG = 255
     SELF.Q.tmp:ModelUnit3_NormalBG = 16777215
     SELF.Q.tmp:ModelUnit3_SelectedFG = 16777215
     SELF.Q.tmp:ModelUnit3_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ModelUnit3_NormalFG = 0
     SELF.Q.tmp:ModelUnit3_NormalBG = 16777215
     SELF.Q.tmp:ModelUnit3_SelectedFG = 16777215
     SELF.Q.tmp:ModelUnit3_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 255
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 255
   ELSE
     SELF.Q.tmp:ExchangeStatusDate_NormalFG = 0
     SELF.Q.tmp:ExchangeStatusDate_NormalBG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:ExchangeStatusDate_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:LoanStatusDate_NormalFG = 255
     SELF.Q.tmp:LoanStatusDate_NormalBG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 255
   ELSE
     SELF.Q.tmp:LoanStatusDate_NormalFG = 0
     SELF.Q.tmp:LoanStatusDate_NormalBG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedFG = 16777215
     SELF.Q.tmp:LoanStatusDate_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.tmp:LocationStatus_NormalFG = 255
     SELF.Q.tmp:LocationStatus_NormalBG = 16777215
     SELF.Q.tmp:LocationStatus_SelectedFG = 16777215
     SELF.Q.tmp:LocationStatus_SelectedBG = 255
   ELSE
     SELF.Q.tmp:LocationStatus_NormalFG = 0
     SELF.Q.tmp:LocationStatus_NormalBG = 16777215
     SELF.Q.tmp:LocationStatus_SelectedFG = 16777215
     SELF.Q.tmp:LocationStatus_SelectedBG = 0
   END
   IF (jobe:jobmark = 1)
     SELF.Q.job:Address_Line1_NormalFG = 255
     SELF.Q.job:Address_Line1_NormalBG = 16777215
     SELF.Q.job:Address_Line1_SelectedFG = 16777215
     SELF.Q.job:Address_Line1_SelectedBG = 255
   ELSE
     SELF.Q.job:Address_Line1_NormalFG = 0
     SELF.Q.job:Address_Line1_NormalBG = 16777215
     SELF.Q.job:Address_Line1_SelectedFG = 16777215
     SELF.Q.job:Address_Line1_SelectedBG = 0
   END
      ! Start Change 2618 BE(06/08/03)
      IF (SELF.Q.job:Ref_Number_NormalFG <> 255) THEN
        LOOP ix# = 1 TO 8
          IF ((JSC:Status[ix#] <> '') AND (job:current_status = JSC:Status[ix#])) THEN
  
              SELF.Q.job:Ref_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Ref_Number_NormalBG =  COLOR:White
              SELF.Q.job:Ref_Number_SelectedFG = COLOR:White
              SELF.Q.job:Ref_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Account_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Account_Number_NormalBG = COLOR:White
              SELF.Q.job:Account_Number_SelectedFG = COLOR:White
              SELF.Q.job:Account_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.model_unit_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.model_unit_temp_NormalBG = COLOR:White
              SELF.Q.model_unit_temp_SelectedFG = COLOR:White
              SELF.Q.model_unit_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:date_booked_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:date_booked_NormalBG = COLOR:White
              SELF.Q.job:date_booked_SelectedFG = COLOR:White
              SELF.Q.job:date_booked_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Current_Status_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Current_Status_NormalBG = COLOR:White
              SELF.Q.job:Current_Status_SelectedFG = COLOR:White
              SELF.Q.job:Current_Status_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:ESN_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:ESN_NormalBG = COLOR:White
              SELF.Q.job:ESN_SelectedFG = COLOR:White
              SELF.Q.job:ESN_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:MSN_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:MSN_NormalBG = COLOR:White
              SELF.Q.job:MSN_SelectedFG = COLOR:White
              SELF.Q.job:MSN_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Order_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Order_Number_NormalBG = COLOR:White
              SELF.Q.job:Order_Number_SelectedFG = COLOR:White
              SELF.Q.job:Order_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Surname_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Surname_NormalBG = COLOR:White
              SELF.Q.job:Surname_SelectedFG = COLOR:White
              SELF.Q.job:Surname_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Mobile_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Mobile_Number_NormalBG = COLOR:White
              SELF.Q.job:Mobile_Number_SelectedFG = COLOR:White
              SELF.Q.job:Mobile_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Postcode_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Postcode_NormalBG = COLOR:White
              SELF.Q.job:Postcode_SelectedFG = COLOR:White
              SELF.Q.job:Postcode_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Model_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Model_Number_NormalBG = COLOR:White
              SELF.Q.job:Model_Number_SelectedFG = COLOR:White
              SELF.Q.job:Model_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Unit_Type_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Unit_Type_NormalBG = COLOR:White
              SELF.Q.job:Unit_Type_SelectedFG = COLOR:White
              SELF.Q.job:Unit_Type_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.address_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.address_temp_NormalBG = COLOR:White
              SELF.Q.address_temp_SelectedFG = COLOR:White
              SELF.Q.address_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.model_unit2_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.model_unit2_temp_NormalBG = COLOR:White
              SELF.Q.model_unit2_temp_SelectedFG = COLOR:White
              SELF.Q.model_unit2_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.model_unit3_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.model_unit3_temp_NormalBG = COLOR:White
              SELF.Q.model_unit3_temp_SelectedFG = COLOR:White
              SELF.Q.model_unit3_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.model_unit4_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.model_unit4_temp_NormalBG = COLOR:White
              SELF.Q.model_unit4_temp_SelectedFG = COLOR:White
              SELF.Q.model_unit4_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.model_unit5_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.model_unit5_temp_NormalBG = COLOR:White
              SELF.Q.model_unit5_temp_SelectedFG = COLOR:White
              SELF.Q.model_unit5_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Consignment_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Consignment_Number_NormalBG = COLOR:White
              SELF.Q.job:Consignment_Number_SelectedFG = COLOR:White
              SELF.Q.job:Consignment_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Current_Courier_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Current_Courier_NormalBG = COLOR:White
              SELF.Q.job:Current_Courier_SelectedFG = COLOR:White
              SELF.Q.job:Current_Courier_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.despatch_type_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.despatch_type_temp_NormalBG = COLOR:White
              SELF.Q.despatch_type_temp_SelectedFG = COLOR:White
              SELF.Q.despatch_type_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.status2_temp_NormalFG = JSC:Colour[ix#]
              SELF.Q.status2_temp_NormalBG = COLOR:White
              SELF.Q.status2_temp_SelectedFG = COLOR:White
              SELF.Q.status2_temp_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Loan_Status_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Loan_Status_NormalBG = COLOR:White
              SELF.Q.job:Loan_Status_SelectedFG = COLOR:White
              SELF.Q.job:Loan_Status_SelectedBG = JSC:Colour[ix#]
   
              SELF.Q.job:Exchange_Status_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Exchange_Status_NormalBG = COLOR:White
              SELF.Q.job:Exchange_Status_SelectedFG = COLOR:White
              SELF.Q.job:Exchange_Status_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Location_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Location_NormalBG = COLOR:White
              SELF.Q.job:Location_SelectedFG = COLOR:White
              SELF.Q.job:Location_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Incoming_Consignment_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Incoming_Consignment_Number_NormalBG = COLOR:White
              SELF.Q.job:Incoming_Consignment_Number_SelectedFG = COLOR:White
              SELF.Q.job:Incoming_Consignment_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Exchange_Unit_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Exchange_Unit_Number_NormalBG = COLOR:White
              SELF.Q.job:Exchange_Unit_Number_SelectedFG = COLOR:White
              SELF.Q.job:Exchange_Unit_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Loan_Unit_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Loan_Unit_Number_NormalBG = COLOR:White
              SELF.Q.job:Loan_Unit_Number_SelectedFG = COLOR:White
              SELF.Q.job:Loan_Unit_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:ExchangeText_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:ExchangeText_NormalBG = COLOR:White
              SELF.Q.tmp:ExchangeText_SelectedFG = COLOR:White
              SELF.Q.tmp:ExchangeText_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:Address_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:Address_NormalBG = COLOR:White
              SELF.Q.tmp:Address_SelectedFG = COLOR:White
              SELF.Q.tmp:Address_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:Location_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:Location_NormalBG = COLOR:White
              SELF.Q.tmp:Location_SelectedFG = COLOR:White
              SELF.Q.tmp:Location_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:StatusDate_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:StatusDate_NormalBG = COLOR:White
              SELF.Q.tmp:StatusDate_SelectedFG = COLOR:White
              SELF.Q.tmp:StatusDate_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:ModelUnit1_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:ModelUnit1_NormalBG = COLOR:White
              SELF.Q.tmp:ModelUnit1_SelectedFG = COLOR:White
              SELF.Q.tmp:ModelUnit1_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:ModelUnit2_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:ModelUnit2_NormalBG = COLOR:White
              SELF.Q.tmp:ModelUnit2_SelectedFG = COLOR:White
              SELF.Q.tmp:ModelUnit2_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:ModelUnit3_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:ModelUnit3_NormalBG = COLOR:White
              SELF.Q.tmp:ModelUnit3_SelectedFG = COLOR:White
              SELF.Q.tmp:ModelUnit3_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:ExchangeStatusDate_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:ExchangeStatusDate_NormalBG = COLOR:White
              SELF.Q.tmp:ExchangeStatusDate_SelectedFG = COLOR:White
              SELF.Q.tmp:ExchangeStatusDate_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:LoanStatusDate_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:LoanStatusDate_NormalBG = COLOR:White
              SELF.Q.tmp:LoanStatusDate_SelectedFG = COLOR:White
              SELF.Q.tmp:LoanStatusDate_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:LocationStatus_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:LocationStatus_NormalBG = COLOR:White
              SELF.Q.tmp:LocationStatus_SelectedFG = COLOR:White
              SELF.Q.tmp:LocationStatus_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Address_Line1_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Address_Line1_NormalBG = COLOR:White
              SELF.Q.job:Address_Line1_SelectedFG = COLOR:White
              SELF.Q.job:Address_Line1_SelectedBG = JSC:Colour[ix#]
              BREAK
          END
        END
      END
      ! End Change 2618 BE(06/08/03)
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:q_JobNumber.Job_Number_Pointer = job:Ref_Number
     GET(glo:q_JobNumber,glo:q_JobNumber.Job_Number_Pointer)
    EXECUTE DASBRW::26:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !job:Ref_Number
  OF 6 !job:Account_Number
  OF 11 !model_unit_temp
  OF 16 !job:date_booked
  OF 21 !job:Current_Status
  OF 26 !Overdue_Temp
  OF 28 !Completed_Temp
  OF 30 !tag_temp
  OF 32 !Invoiced_Temp
  OF 34 !Warranty_Temp
  OF 36 !Collected_Temp
  OF 38 !Paid_Temp
  OF 40 !job:ESN
  OF 45 !job:MSN
  OF 50 !job:Order_Number
  OF 55 !job:Surname
  OF 60 !job:Mobile_Number
  OF 65 !job:Postcode
  OF 70 !job:Model_Number
  OF 75 !job:Unit_Type
  OF 80 !address_temp
  OF 85 !model_unit2_temp
  OF 90 !model_unit3_temp
  OF 95 !model_unit4_temp
  OF 100 !model_unit5_temp
  OF 105 !job:Consignment_Number
  OF 110 !job:Current_Courier
  OF 115 !despatch_type_temp
  OF 120 !status2_temp
  OF 125 !job:Loan_Status
  OF 130 !job:Exchange_Status
  OF 135 !job:Location
  OF 140 !job:Incoming_Consignment_Number
  OF 145 !job:Exchange_Unit_Number
  OF 150 !job:Loan_Unit_Number
  OF 155 !tmp:ExchangeText
  OF 160 !tmp:Address
  OF 165 !tmp:Location
  OF 170 !tmp:StatusDate
  OF 175 !tmp:ModelUnit1
  OF 180 !tmp:ModelUnit2
  OF 185 !tmp:ModelUnit3
  OF 190 !tmp:ExchangeStatusDate
  OF 195 !tmp:LoanStatusDate
  OF 200 !tmp:LocationStatus
  OF 205 !job:Address_Line1
  OF 210 !job:Company_Name
  OF 211 !job:Address_Line2
  OF 212 !job:Address_Line3
  OF 213 !Postcode_Temp
  OF 214 !Address_Line3_Temp
  OF 215 !Address_Line2_Temp
  OF 216 !Address_Line1_Temp
  OF 217 !Company_Name_Temp
  OF 218 !job:Engineer
  OF 219 !job:Completed
  OF 220 !job:Workshop
  OF 221 !job:Batch_Number
  OF 222 !job:Despatched
  OF 223 !job:Despatch_Number
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBS)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s7')                        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account No')                 !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account No')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('model_unit_temp')            !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('model_unit_temp')            !Header
                PSTRING('@S20')                       !Picture
                PSTRING('model_unit_temp')            !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:date_booked')            !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Booked')                     !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Booked')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Current_Status')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Status')                     !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Status')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Overdue_Temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Overdue_Temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Overdue_Temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Completed_Temp')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Completed_Temp')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Completed_Temp')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tag_temp')                   !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tag_temp')                   !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tag_temp')                   !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Invoiced_Temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Invoiced_Temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Invoiced_Temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Warranty_Temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Warranty_Temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Warranty_Temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Collected_Temp')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Collected_Temp')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Collected_Temp')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Paid_Temp')                  !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Paid_Temp')                  !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Paid_Temp')                  !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('ESN/IMEI')                   !Header
                PSTRING('@s16')                       !Picture
                PSTRING('ESN/IMEI')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:MSN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('MSN')                        !Header
                PSTRING('@s15')                       !Picture
                PSTRING('MSN')                        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Order_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Order Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Order Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Surname')                !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Surname')                    !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Surname')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Mobile_Number')          !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Mobile Number')              !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Mobile Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Postcode')               !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Postcode')                   !Header
                PSTRING('@s10')                       !Picture
                PSTRING('Postcode')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Unit_Type')              !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Unit Type')                  !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Unit Type')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('address_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('address_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('address_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('model_unit2_temp')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('model_unit2_temp')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('model_unit2_temp')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('model_unit3_temp')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('model_unit3_temp')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('model_unit3_temp')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('model_unit4_temp')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('model_unit4_temp')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('model_unit4_temp')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('model_unit5_temp')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('model_unit5_temp')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('model_unit5_temp')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Consignment_Number')     !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Consignment No')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Consignment No')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Current_Courier')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Courier')                    !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Courier')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('despatch_type_temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('despatch_type_temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('despatch_type_temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('status2_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('status2_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('status2_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Loan_Status')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Loan Status')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Loan Status')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Exchange_Status')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Exchange Status')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Exchange Status')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Location')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Incoming_Consignment_Number') !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Consignment No')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Consignment No')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Exchange_Unit_Number')   !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Exchange Unit Number')       !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Exchange Unit Number')       !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Loan_Unit_Number')       !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Loan Unit Number')           !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Loan Unit Number')           !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ExchangeText')           !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ExchangeText')           !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ExchangeText')           !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:Address')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Address')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Address')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:Location')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:Location')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:Location')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:StatusDate')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:StatusDate')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:StatusDate')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ModelUnit1')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ModelUnit1')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ModelUnit1')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ModelUnit2')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ModelUnit2')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ModelUnit2')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ModelUnit3')             !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ModelUnit3')             !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ModelUnit3')             !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:ExchangeStatusDate')     !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:ExchangeStatusDate')     !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:ExchangeStatusDate')     !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LoanStatusDate')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LoanStatusDate')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LoanStatusDate')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('tmp:LocationStatus')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:LocationStatus')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:LocationStatus')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Address_Line1')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Company_Name')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Company Name')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Company Name')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Address_Line2')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('DELETE THIS')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('DELETE THIS')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Address_Line3')          !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('DELETE THIS')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('DELETE THIS')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Postcode_Temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Postcode_Temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Postcode_Temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Address_Line3_Temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Address_Line3_Temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Address_Line3_Temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Address_Line2_Temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Address_Line2_Temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Address_Line2_Temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Address_Line1_Temp')         !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Address_Line1_Temp')         !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Address_Line1_Temp')         !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('Company_Name_Temp')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('Company_Name_Temp')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('Company_Name_Temp')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Engineer')               !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Engineer')                   !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Engineer')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Completed')              !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Completed')                  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Completed')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Workshop')               !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Workshop')                   !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Workshop')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Batch_Number')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Batch Number')               !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Batch Number')               !Description
                STRING('R')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Despatched')             !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Despatched')                 !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Despatched')                 !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Despatch_Number')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Despatch Number')            !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Despatch Number')            !Description
                STRING('R')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                END
XpDim           SHORT(60)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
