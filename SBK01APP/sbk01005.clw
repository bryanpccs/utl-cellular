

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01005.INC'),ONCE        !Local module procedure declarations
                     END


Auto_Change_Courier PROCEDURE                         !Generated from procedure template - Window

tmp:FromCourier      STRING(30)
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
tmp:ToCourier        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:FromCourier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?tmp:ToCourier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
FDCB3::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Auto Change Courier'),AT(,,220,143),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(3,4,213,108),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('This facility is used for Automatically changing the Incoming and Outgoing Couri' &|
   'er of ALL the Trade / Sub Accounts where applicable.'),AT(8,8,204,32),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Change Courier From'),AT(8,44),USE(?Prompt2),FONT(,,COLOR:Red,FONT:bold)
                           PROMPT('Change Courier To'),AT(8,76),USE(?Prompt2:3),FONT(,,COLOR:Green,FONT:bold)
                           COMBO(@s30),AT(88,56,124,10),USE(tmp:FromCourier),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           COMBO(@s30),AT(88,88,124,10),USE(tmp:ToCourier),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:1)
                           PROMPT('Courier'),AT(8,56),USE(?Prompt2:2)
                           PROMPT('Courier'),AT(8,88),USE(?Prompt2:4)
                         END
                       END
                       PANEL,AT(4,116,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Close'),AT(156,120,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(100,120,56,16),USE(?Button2),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB3                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt2:3{prop:FontColor} = -1
    ?Prompt2:3{prop:Color} = 15066597
    If ?tmp:FromCourier{prop:ReadOnly} = True
        ?tmp:FromCourier{prop:FontColor} = 65793
        ?tmp:FromCourier{prop:Color} = 15066597
    Elsif ?tmp:FromCourier{prop:Req} = True
        ?tmp:FromCourier{prop:FontColor} = 65793
        ?tmp:FromCourier{prop:Color} = 8454143
    Else ! If ?tmp:FromCourier{prop:Req} = True
        ?tmp:FromCourier{prop:FontColor} = 65793
        ?tmp:FromCourier{prop:Color} = 16777215
    End ! If ?tmp:FromCourier{prop:Req} = True
    ?tmp:FromCourier{prop:Trn} = 0
    ?tmp:FromCourier{prop:FontStyle} = font:Bold
    If ?tmp:ToCourier{prop:ReadOnly} = True
        ?tmp:ToCourier{prop:FontColor} = 65793
        ?tmp:ToCourier{prop:Color} = 15066597
    Elsif ?tmp:ToCourier{prop:Req} = True
        ?tmp:ToCourier{prop:FontColor} = 65793
        ?tmp:ToCourier{prop:Color} = 8454143
    Else ! If ?tmp:ToCourier{prop:Req} = True
        ?tmp:ToCourier{prop:FontColor} = 65793
        ?tmp:ToCourier{prop:Color} = 16777215
    End ! If ?tmp:ToCourier{prop:Req} = True
    ?tmp:ToCourier{prop:Trn} = 0
    ?tmp:ToCourier{prop:FontStyle} = font:Bold
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    ?Prompt2:4{prop:FontColor} = -1
    ?Prompt2:4{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Auto_Change_Courier',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:FromCourier',tmp:FromCourier,'Auto_Change_Courier',1)
    SolaceViewVars('save_tra_id',save_tra_id,'Auto_Change_Courier',1)
    SolaceViewVars('save_sub_id',save_sub_id,'Auto_Change_Courier',1)
    SolaceViewVars('tmp:ToCourier',tmp:ToCourier,'Auto_Change_Courier',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:3;  SolaceCtrlName = '?Prompt2:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FromCourier;  SolaceCtrlName = '?tmp:FromCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ToCourier;  SolaceCtrlName = '?tmp:ToCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:4;  SolaceCtrlName = '?Prompt2:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Auto_Change_Courier')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Auto_Change_Courier')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:COURIER.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB2.Init(tmp:FromCourier,?tmp:FromCourier,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(cou:Courier_Key)
  FDCB2.AddField(cou:Courier,FDCB2.Q.cou:Courier)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  FDCB3.Init(tmp:ToCourier,?tmp:ToCourier,Queue:FileDropCombo:1.ViewPosition,FDCB3::View:FileDropCombo,Queue:FileDropCombo:1,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB3.Q &= Queue:FileDropCombo:1
  FDCB3.AddSortOrder(cou:Courier_Key)
  FDCB3.AddField(cou:Courier,FDCB3.Q.cou:Courier)
  ThisWindow.AddItem(FDCB3.WindowComponent)
  FDCB3.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Auto_Change_Courier',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      Case MessageEx('This routine will now go through the Trade Account file and change every occurance of the Courier '&Clip(tmp:FromCourier)&' to the Courier '&Clip(tmp:ToCourier)&'.','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              setcursor(cursor:wait)
              save_tra_id = access:tradeacc.savefile()
              set(tra:account_number_key)
              loop
                  if access:tradeacc.next()
                     break
                  end !if
                  If tra:courier_incoming = tmp:FromCourier
                      tra:courier_incoming    = tmp:ToCourier
                      access:tradeacc.update()
                  End!If tra:courier_incoming = tmp:FromCourier
                  If tra:courier_outgoing = tmp:FromCourier
                      tra:courier_outgoing    = tmp:ToCourier
                      access:tradeacc.update()
                  End!If tra:courier_outgoing = tmp:FromCourier
              end !loop
              access:tradeacc.restorefile(save_tra_id)
      
              save_sub_id = access:subtracc.savefile()
              set(sub:account_number_key)
              loop
                  if access:subtracc.next()
                     break
                  end !if
                  If sub:courier_incoming = tmp:FromCourier
                      sub:courier_incoming    = tmp:ToCourier
                      access:subtracc.update()
                  End!If tra:courier_incoming = tmp:FromCourier
                  If sub:courier_outgoing = tmp:FromCourier
                      sub:courier_outgoing    = tmp:ToCourier
                      access:subtracc.update()
                  End!If tra:courier_outgoing = tmp:FromCourier
              end !loop
              access:subtracc.restorefile(save_sub_id)
      
              setcursor()
              Case MessageEx('Process Completed.','ServiceBase 2000',|
                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Auto_Change_Courier')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

