

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01014.INC'),ONCE        !Local module procedure declarations
                     END


Rapid_Reception_Despatch PROCEDURE                    !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
tmp:vat              REAL
tmp:Total            REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:rea              STRING('REA')
tmp:O                STRING(3)
tmp:F                STRING(1)
tmp:C                STRING(1)
tmp:W                STRING(1)
tmp:D                STRING(1)
tmp:P                STRING(1)
tmp:Blank            STRING(20)
tmp:Status           STRING(30)
tmp:AccountNumber    STRING(30)
tmp:Courier          STRING(30)
tmp:ConsignNo        STRING(30)
tmp:PassedConsignNo  STRING(30)
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Surname)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Mobile_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Parts_Cost)
                       PROJECT(job:Courier_Cost)
                       PROJECT(job:Labour_Cost)
                       PROJECT(job:Account_Number)
                       JOIN(jbn:RefNumberKey,job:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Ref_Number_NormalFG LONG                          !Normal forground color
job:Ref_Number_NormalBG LONG                          !Normal background color
job:Ref_Number_SelectedFG LONG                        !Selected forground color
job:Ref_Number_SelectedBG LONG                        !Selected background color
job:Surname            LIKE(job:Surname)              !List box control field - type derived from field
job:Surname_NormalFG   LONG                           !Normal forground color
job:Surname_NormalBG   LONG                           !Normal background color
job:Surname_SelectedFG LONG                           !Selected forground color
job:Surname_SelectedBG LONG                           !Selected background color
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Model_Number_NormalFG LONG                        !Normal forground color
job:Model_Number_NormalBG LONG                        !Normal background color
job:Model_Number_SelectedFG LONG                      !Selected forground color
job:Model_Number_SelectedBG LONG                      !Selected background color
tmp:Status             LIKE(tmp:Status)               !List box control field - type derived from local data
tmp:Status_NormalFG    LONG                           !Normal forground color
tmp:Status_NormalBG    LONG                           !Normal background color
tmp:Status_SelectedFG  LONG                           !Selected forground color
tmp:Status_SelectedBG  LONG                           !Selected background color
job:Mobile_Number      LIKE(job:Mobile_Number)        !List box control field - type derived from field
job:Mobile_Number_NormalFG LONG                       !Normal forground color
job:Mobile_Number_NormalBG LONG                       !Normal background color
job:Mobile_Number_SelectedFG LONG                     !Selected forground color
job:Mobile_Number_SelectedBG LONG                     !Selected background color
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:ESN_NormalFG       LONG                           !Normal forground color
job:ESN_NormalBG       LONG                           !Normal background color
job:ESN_SelectedFG     LONG                           !Selected forground color
job:ESN_SelectedBG     LONG                           !Selected background color
tmp:O                  LIKE(tmp:O)                    !List box control field - type derived from local data
tmp:O_NormalFG         LONG                           !Normal forground color
tmp:O_NormalBG         LONG                           !Normal background color
tmp:O_SelectedFG       LONG                           !Selected forground color
tmp:O_SelectedBG       LONG                           !Selected background color
tmp:O_Icon             LONG                           !Entry's icon ID
tmp:F                  LIKE(tmp:F)                    !List box control field - type derived from local data
tmp:F_NormalFG         LONG                           !Normal forground color
tmp:F_NormalBG         LONG                           !Normal background color
tmp:F_SelectedFG       LONG                           !Selected forground color
tmp:F_SelectedBG       LONG                           !Selected background color
tmp:F_Icon             LONG                           !Entry's icon ID
tmp:C                  LIKE(tmp:C)                    !List box control field - type derived from local data
tmp:C_NormalFG         LONG                           !Normal forground color
tmp:C_NormalBG         LONG                           !Normal background color
tmp:C_SelectedFG       LONG                           !Selected forground color
tmp:C_SelectedBG       LONG                           !Selected background color
tmp:C_Icon             LONG                           !Entry's icon ID
tmp:W                  LIKE(tmp:W)                    !List box control field - type derived from local data
tmp:W_NormalFG         LONG                           !Normal forground color
tmp:W_NormalBG         LONG                           !Normal background color
tmp:W_SelectedFG       LONG                           !Selected forground color
tmp:W_SelectedBG       LONG                           !Selected background color
tmp:W_Icon             LONG                           !Entry's icon ID
tmp:D                  LIKE(tmp:D)                    !List box control field - type derived from local data
tmp:D_NormalFG         LONG                           !Normal forground color
tmp:D_NormalBG         LONG                           !Normal background color
tmp:D_SelectedFG       LONG                           !Selected forground color
tmp:D_SelectedBG       LONG                           !Selected background color
tmp:D_Icon             LONG                           !Entry's icon ID
tmp:P                  LIKE(tmp:P)                    !List box control field - type derived from local data
tmp:P_NormalFG         LONG                           !Normal forground color
tmp:P_NormalBG         LONG                           !Normal background color
tmp:P_SelectedFG       LONG                           !Selected forground color
tmp:P_SelectedBG       LONG                           !Selected background color
tmp:P_Icon             LONG                           !Entry's icon ID
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
job:Order_Number_NormalFG LONG                        !Normal forground color
job:Order_Number_NormalBG LONG                        !Normal background color
job:Order_Number_SelectedFG LONG                      !Selected forground color
job:Order_Number_SelectedBG LONG                      !Selected background color
job:Order_Number_Icon  LONG                           !Entry's icon ID
job:Parts_Cost         LIKE(job:Parts_Cost)           !Browse hot field - type derived from field
job:Courier_Cost       LIKE(job:Courier_Cost)         !Browse hot field - type derived from field
job:Labour_Cost        LIKE(job:Labour_Cost)          !Browse hot field - type derived from field
tmp:Total              LIKE(tmp:Total)                !Browse hot field - type derived from local data
tmp:vat                LIKE(tmp:vat)                  !Browse hot field - type derived from local data
job:Account_Number     LIKE(job:Account_Number)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(PARTS)
                       PROJECT(par:Description)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                       PROJECT(par:Part_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
par:Part_Number        LIKE(par:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(WARPARTS)
                       PROJECT(wpr:Description)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                       PROJECT(wpr:Part_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:2
wpr:Description        LIKE(wpr:Description)          !List box control field - type derived from field
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
wpr:Part_Number        LIKE(wpr:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Rapid Reception Despatch'),AT(,,668,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('Rapid_Reception_Despatch'),SYSTEM,GRAY,DOUBLE,IMM
                       PROMPT('- Not Despatched'),AT(388,20),USE(?NotDespatched)
                       LIST,AT(8,36,564,212),USE(?Browse:1),IMM,MSG('Browsing Records'),FORMAT('38R(2)|M*~Job No~L@s8@106L(2)|M*~Surname~@s30@98L(2)|M*~Model Number~@s30@125L(2' &|
   ')|M*~Status~@s30@60L(2)|M*~Mobile Number~@s15@64L(2)|M*~I.M.E.I. Number~@s16@11L' &|
   '(2)*I~O~@s3@11L(2)*I~F~@s1@11L(2)*I~C~@s1@11L(2)*I~W~@s1@11L(2)*I~D~@s1@11L(2)*I' &|
   '~P~@s1@125L(2)|M*I~Order Number~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Change'),AT(588,112,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('&Process Despatch'),AT(588,36,76,20),USE(?Process_Despatch),LEFT,ICON('despatch.gif')
                       SHEET,AT(4,4,572,248),USE(?CurrentTab),SPREAD
                         TAB('By Job Number'),USE(?Tab:2)
                           ENTRY(@s8),AT(8,20,64,10),USE(job:Ref_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON('Key'),AT(536,20,36,12),USE(?Key),LEFT,ICON('key.ico')
                         END
                         TAB('By Surname'),USE(?Tab2)
                           ENTRY(@s30),AT(8,20,124,10),USE(job:Surname),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By Mobile Number'),USE(?Tab3)
                           ENTRY(@s15),AT(8,20,124,10),USE(job:Mobile_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By I.M.E.I. Number'),USE(?Tab4)
                           ENTRY(@s16),AT(8,20,124,10),USE(job:ESN),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By Account Number'),USE(?Tab6)
                           ENTRY(@s30),AT(136,20,124,10),USE(tmp:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),ALRT(EnterKey),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                           BUTTON,AT(264,20,10,10),USE(?LookupAccountNumber),SKIP,ICON('List3.ico')
                           ENTRY(@s30),AT(8,20,124,10),USE(job:Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('Close'),AT(588,324,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       SHEET,AT(4,256,572,88),USE(?Sheet2),WIZARD,SPREAD
                         TAB('Tab 5'),USE(?Tab5)
                           PROMPT('Engineers Report'),AT(8,260),USE(?Prompt1)
                           PROMPT('Chargeable Parts Used'),AT(248,260,84,12),USE(?Prompt1:2)
                           LIST,AT(248,272,104,68),USE(?List),IMM,VSCROLL,COLOR(COLOR:Silver),MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse)
                           LIST,AT(356,272,104,68),USE(?List:2),IMM,VSCROLL,COLOR(COLOR:Silver),MSG('Browsing Records'),FORMAT('120L(2)|M~Description~@s30@'),FROM(Queue:Browse:2)
                           TEXT,AT(8,272,236,68),USE(jbn:Invoice_Text),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           GROUP('Financial Details'),AT(464,260,108,80),USE(?Group1),BOXED
                             PROMPT('Parts Cost'),AT(468,296),USE(?Prompt3)
                             PROMPT('Total'),AT(468,324),USE(?Prompt3:4),FONT(,,,FONT:bold)
                             LINE,AT(501,320,67,0),USE(?Line1),COLOR(COLOR:Black)
                             PROMPT('V.A.T.'),AT(468,308),USE(?Prompt3:5)
                             STRING(@n10.2),AT(530,308),USE(tmp:vat),RIGHT
                             STRING(@n14.2),AT(516,324),USE(tmp:Total),RIGHT,FONT(,,,FONT:bold)
                             PROMPT('Labour Cost'),AT(468,284),USE(?Prompt3:2)
                             PROMPT('Courier Cost'),AT(468,272),USE(?Prompt3:3)
                             STRING(@n14.2),AT(516,272),USE(job:Courier_Cost),RIGHT
                             STRING(@n14.2),AT(516,284),USE(job:Labour_Cost),RIGHT
                             STRING(@n14.2),AT(516,296),USE(job:Parts_Cost),RIGHT
                           END
                           PROMPT('Warranty Parts Used'),AT(356,260,80,12),USE(?Prompt1:3)
                         END
                       END
                       BOX,AT(372,20,10,10),USE(?Box1),COLOR(COLOR:Red),FILL(COLOR:Red)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! Start Change 2618 BE(06/08/03)
!
! include definition here rather than in CLARION Data section
! in order to exclude from Solace View which appears unable
! to cope with a GROUP ARRAY data item.
!
JobStatusColours     GROUP,PRE(JSC),DIM(8)
Colour               LONG
Status               STRING(30)
                     END
! Start Change 2618 BE(06/08/03)
!Save Entry Fields Incase Of Lookup
look:tmp:AccountNumber                Like(tmp:AccountNumber)
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?NotDespatched{prop:FontColor} = -1
    ?NotDespatched{prop:Color} = 15066597
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?job:Surname{prop:ReadOnly} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 15066597
    Elsif ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 8454143
    Else ! If ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 16777215
    End ! If ?job:Surname{prop:Req} = True
    ?job:Surname{prop:Trn} = 0
    ?job:Surname{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?job:Mobile_Number{prop:ReadOnly} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 15066597
    Elsif ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 8454143
    Else ! If ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 16777215
    End ! If ?job:Mobile_Number{prop:Req} = True
    ?job:Mobile_Number{prop:Trn} = 0
    ?job:Mobile_Number{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?job:ESN{prop:ReadOnly} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 15066597
    Elsif ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 8454143
    Else ! If ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 16777215
    End ! If ?job:ESN{prop:Req} = True
    ?job:ESN{prop:Trn} = 0
    ?job:ESN{prop:FontStyle} = font:Bold
    ?Tab6{prop:Color} = 15066597
    If ?tmp:AccountNumber{prop:ReadOnly} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 15066597
    Elsif ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 8454143
    Else ! If ?tmp:AccountNumber{prop:Req} = True
        ?tmp:AccountNumber{prop:FontColor} = 65793
        ?tmp:AccountNumber{prop:Color} = 16777215
    End ! If ?tmp:AccountNumber{prop:Req} = True
    ?tmp:AccountNumber{prop:Trn} = 0
    ?tmp:AccountNumber{prop:FontStyle} = font:Bold
    If ?job:Order_Number{prop:ReadOnly} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 15066597
    Elsif ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 8454143
    Else ! If ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 16777215
    End ! If ?job:Order_Number{prop:Req} = True
    ?job:Order_Number{prop:Trn} = 0
    ?job:Order_Number{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    If ?jbn:Invoice_Text{prop:ReadOnly} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 15066597
    Elsif ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 8454143
    Else ! If ?jbn:Invoice_Text{prop:Req} = True
        ?jbn:Invoice_Text{prop:FontColor} = 65793
        ?jbn:Invoice_Text{prop:Color} = 16777215
    End ! If ?jbn:Invoice_Text{prop:Req} = True
    ?jbn:Invoice_Text{prop:Trn} = 0
    ?jbn:Invoice_Text{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt3:4{prop:FontColor} = -1
    ?Prompt3:4{prop:Color} = 15066597
    ?Prompt3:5{prop:FontColor} = -1
    ?Prompt3:5{prop:Color} = 15066597
    ?tmp:vat{prop:FontColor} = -1
    ?tmp:vat{prop:Color} = 15066597
    ?tmp:Total{prop:FontColor} = -1
    ?tmp:Total{prop:Color} = 15066597
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    ?Prompt3:3{prop:FontColor} = -1
    ?Prompt3:3{prop:Color} = 15066597
    ?job:Courier_Cost{prop:FontColor} = -1
    ?job:Courier_Cost{prop:Color} = 15066597
    ?job:Labour_Cost{prop:FontColor} = -1
    ?job:Labour_Cost{prop:Color} = 15066597
    ?job:Parts_Cost{prop:FontColor} = -1
    ?job:Parts_Cost{prop:Color} = 15066597
    ?Prompt1:3{prop:FontColor} = -1
    ?Prompt1:3{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Rapid_Reception_Despatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:vat',tmp:vat,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:Total',tmp:Total,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:rea',tmp:rea,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:O',tmp:O,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:F',tmp:F,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:C',tmp:C,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:W',tmp:W,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:D',tmp:D,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:P',tmp:P,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:Blank',tmp:Blank,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:Status',tmp:Status,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:Courier',tmp:Courier,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'Rapid_Reception_Despatch',1)
    SolaceViewVars('tmp:PassedConsignNo',tmp:PassedConsignNo,'Rapid_Reception_Despatch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?NotDespatched;  SolaceCtrlName = '?NotDespatched';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Process_Despatch;  SolaceCtrlName = '?Process_Despatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Key;  SolaceCtrlName = '?Key';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Surname;  SolaceCtrlName = '?job:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Mobile_Number;  SolaceCtrlName = '?job:Mobile_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AccountNumber;  SolaceCtrlName = '?tmp:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupAccountNumber;  SolaceCtrlName = '?LookupAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Order_Number;  SolaceCtrlName = '?job:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jbn:Invoice_Text;  SolaceCtrlName = '?jbn:Invoice_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:4;  SolaceCtrlName = '?Prompt3:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:5;  SolaceCtrlName = '?Prompt3:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:vat;  SolaceCtrlName = '?tmp:vat';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Total;  SolaceCtrlName = '?tmp:Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:3;  SolaceCtrlName = '?Prompt3:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Courier_Cost;  SolaceCtrlName = '?job:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Labour_Cost;  SolaceCtrlName = '?job:Labour_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Parts_Cost;  SolaceCtrlName = '?job:Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:3;  SolaceCtrlName = '?Prompt1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Rapid_Reception_Despatch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Rapid_Reception_Despatch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?NotDespatched
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Rapid_Reception_Despatch'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:INVOICE.UseFile
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  ! Start Change 2618 BE(05/08/03)
  JSC:Colour[1] = COLOR:Maroon
  JSC:Status[1] = GETINI('JobStatusColours','Maroon',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[2] = COLOR:Green
  JSC:Status[2] = GETINI('JobStatusColours','Green',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[3] = COLOR:Olive
  JSC:Status[3] = GETINI('JobStatusColours','Olive',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[4] = COLOR:Navy
  JSC:Status[4] = GETINI('JobStatusColours','Navy',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[5] = COLOR:Purple
  JSC:Status[5] = GETINI('JobStatusColours','Purple',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[6] = COLOR:Teal
  JSC:Status[6] = GETINI('JobStatusColours','Teal',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[7] = COLOR:Gray
  JSC:Status[7] = GETINI('JobStatusColours','Gray',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Colour[8] = COLOR:Blue
  JSC:Status[8] = GETINI('JobStatusColours','Blue',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2618 BE(05/08/03)
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:PARTS,SELF)
  BRW6.Init(?List:2,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:WARPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  IF ?tmp:AccountNumber{Prop:Tip} AND ~?LookupAccountNumber{Prop:Tip}
     ?LookupAccountNumber{Prop:Tip} = 'Select ' & ?tmp:AccountNumber{Prop:Tip}
  END
  IF ?tmp:AccountNumber{Prop:Msg} AND ~?LookupAccountNumber{Prop:Msg}
     ?LookupAccountNumber{Prop:Msg} = 'Select ' & ?tmp:AccountNumber{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Surname_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?job:Surname,job:Surname,1,BRW1)
  BRW1.AddSortOrder(,job:MobileNumberKey)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?job:Mobile_Number,job:Mobile_Number,1,BRW1)
  BRW1.AddSortOrder(,job:ESN_Key)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?job:ESN,job:ESN,1,BRW1)
  BRW1.AddSortOrder(,job:AccOrdNoKey)
  BRW1.AddRange(job:Account_Number,tmp:AccountNumber)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?job:Order_Number,job:Order_Number,1,BRW1)
  BRW1.AddSortOrder(,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,job:Ref_Number,1,BRW1)
  BIND('tmp:Status',tmp:Status)
  BIND('tmp:O',tmp:O)
  BIND('tmp:F',tmp:F)
  BIND('tmp:C',tmp:C)
  BIND('tmp:W',tmp:W)
  BIND('tmp:D',tmp:D)
  BIND('tmp:P',tmp:P)
  BIND('tmp:Total',tmp:Total)
  BIND('tmp:vat',tmp:vat)
  ?Browse:1{PROP:IconList,1} = '~Block_Blue.ico'
  ?Browse:1{PROP:IconList,2} = '~Block_Blue_Left.ico'
  ?Browse:1{PROP:IconList,3} = '~Block_Blue_R.ico'
  ?Browse:1{PROP:IconList,4} = '~Block_Cyan.ico'
  ?Browse:1{PROP:IconList,5} = '~Block_Green.ico'
  ?Browse:1{PROP:IconList,6} = '~Block_Red.ico'
  ?Browse:1{PROP:IconList,7} = '~Block_Red_Left.ico'
  ?Browse:1{PROP:IconList,8} = '~Block_Red_Right.ico'
  ?Browse:1{PROP:IconList,9} = '~Block_Yellow.ico'
  ?Browse:1{PROP:IconList,10} = '~bl_y_lt.ico'
  ?Browse:1{PROP:IconList,11} = '~block_yellow.ico'
  ?Browse:1{PROP:IconList,12} = '~notick1.ico'
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Surname,BRW1.Q.job:Surname)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(tmp:Status,BRW1.Q.tmp:Status)
  BRW1.AddField(job:Mobile_Number,BRW1.Q.job:Mobile_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(tmp:O,BRW1.Q.tmp:O)
  BRW1.AddField(tmp:F,BRW1.Q.tmp:F)
  BRW1.AddField(tmp:C,BRW1.Q.tmp:C)
  BRW1.AddField(tmp:W,BRW1.Q.tmp:W)
  BRW1.AddField(tmp:D,BRW1.Q.tmp:D)
  BRW1.AddField(tmp:P,BRW1.Q.tmp:P)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(job:Parts_Cost,BRW1.Q.job:Parts_Cost)
  BRW1.AddField(job:Courier_Cost,BRW1.Q.job:Courier_Cost)
  BRW1.AddField(job:Labour_Cost,BRW1.Q.job:Labour_Cost)
  BRW1.AddField(tmp:Total,BRW1.Q.tmp:Total)
  BRW1.AddField(tmp:vat,BRW1.Q.tmp:vat)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,par:Part_Number_Key)
  BRW5.AddRange(par:Ref_Number,Relate:PARTS,Relate:JOBS)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,par:Part_Number,1,BRW5)
  BRW5.AddField(par:Description,BRW5.Q.par:Description)
  BRW5.AddField(par:Record_Number,BRW5.Q.par:Record_Number)
  BRW5.AddField(par:Ref_Number,BRW5.Q.par:Ref_Number)
  BRW5.AddField(par:Part_Number,BRW5.Q.par:Part_Number)
  BRW6.Q &= Queue:Browse:2
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,wpr:Part_Number_Key)
  BRW6.AddRange(wpr:Ref_Number,Relate:WARPARTS,Relate:JOBS)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,wpr:Part_Number,1,BRW6)
  BRW6.AddField(wpr:Description,BRW6.Q.wpr:Description)
  BRW6.AddField(wpr:Record_Number,BRW6.Q.wpr:Record_Number)
  BRW6.AddField(wpr:Ref_Number,BRW6.Q.wpr:Ref_Number)
  BRW6.AddField(wpr:Part_Number,BRW6.Q.wpr:Part_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 2
  BRW5.AddToolbarTarget(Toolbar)
  BRW6.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Job Number'
    ?Tab2{PROP:TEXT} = 'By Surname'
    ?Tab3{PROP:TEXT} = 'By Mobile Number'
    ?Tab4{PROP:TEXT} = 'By I.M.E.I. Number'
    ?Tab6{PROP:TEXT} = 'By Account Number'
    ?Browse:1{PROP:FORMAT} ='38R(2)|M*~Job No~L@s8@#1#106L(2)|M*~Surname~@s30@#6#98L(2)|M*~Model Number~@s30@#11#125L(2)|M*~Status~@s30@#16#60L(2)|M*~Mobile Number~@s15@#21#64L(2)|M*~I.M.E.I. Number~@s16@#26#11L(2)*I~O~@s3@#31#11L(2)*I~F~@s1@#37#11L(2)*I~C~@s1@#43#11L(2)*I~W~@s1@#49#11L(2)*I~D~@s1@#55#11L(2)*I~P~@s1@#61#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  glo:select1 = ''
  glo:select2 = ''
  glo:select3 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Rapid_Reception_Despatch'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Rapid_Reception_Despatch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Sub_Accounts
      UpdateJOBS
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Process_Despatch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Despatch, Accepted)
      Brw1.UpdateViewRecord()
      
      !Had job already been invoiced?
      If job:Invoice_Number <> ''
          Case MessageEx('This job has already been invoiced.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !job:Invoice_Number <> ''
          ChangeCourier# = 1
      
          glo:Select1 = job:Ref_Number
          Job_Receipt
      
          !If it's a chargeable job, take a payment
          If job:Chargeable_Job = 'YES'
              glo:Select2 = 'PROCESS'
              Browse_Payments
              SolaceViewVars('glo:Select2: ' & glo:Select2,'','',6)
              If glo:Select2  = 'COMPLETE'
                  !Process has been completed, check to see if an invoice can be produced
                  If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
                      !Bring up the Chargeable Invoice screen
                      DespatchInvoice()
                      ChangeCourier# = 0
                  Else!If CanInvoiceBePrinted(job:Ref_Number,1) = Level:Benign
                      If CreateInvoice() = Level:Benign
                          Single_Invoice
                      End !If CreateInvoice() = Level:Benign
                  End!If CanInvoiceBePrinted(job:Ref_Number)
                  glo:Select2 = ''
              End!If glo:Select2  = 'COMPLETE'
          End!If job:Chargeable_Job = 'YES'
      
          If ChangeCourier#
              tmp:Courier = RapidReceptionCourier(job:Courier)
              If tmp:Courier <> ''
                  job:Courier = tmp:Courier
              End !If tmp:Courier <> ''
          End !If ChangeCourier#
      
          !Bring Up The Consignment Number screen
          tmp:ConsignNo = ''
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Found
      
          Else! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
      
          !Bring up the Consignment Number Screen
          tmp:PassedConsignNo = ''
          Insert_Consignment_Note_Number('DEF',job:Ref_Number,x",tmp:ConsignNo,tmp:PassedConsignNo,1)
          If tmp:ConsignNo <> ''
              !Automatically do the despatch
              If Access:DESBATCH.PrimeRecord() = Level:Benign
                  If Access:DESBATCH.TryInsert() = Level:Benign
                      !Insert Successful
                      job:Date_Despatched = Today()
                      job:Despatch_Number = dbt:Batch_Number
                      job:Despatched      = 'YES'
                      job:Despatch_User   = use:User_Code
                      job:Consignment_Number  = tmp:ConsignNo
                      If job:Chargeable_Job = 'YES'
                          If CheckPaid() = Level:Benign
                              GetStatus(910,1,'JOB')
                          Else !If CheckPaid() = Level:Benign
                              GetStatus(905,1,'JOB')
                          End !If CheckPaid() = Level:Benign
                      Else!If job:Chargeable_Job = 'YES'
                          If job:invoice_number_warranty <> ''
                              GetStatus(910,1,'JOB') !Despatch Paid
                          Else!If job:invoice_number_warranty <> ''
                              GetStatus(905,1,'JOB') !Despatch Paid
                          End!If job:invoice_number_warranty <> ''
                      End!If job:Chargeable_Job = 'YES'
                      Access:JOBS.Update()
      
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          If job:Chargeable_Job = 'YES' and job:Invoice_NUmber <> ''
                              aud:notes         = 'INVOICE CREATED: ' & Clip(job:invoice_number)    
                          End !If job:Chargeable_Job = 'YES' and job:Invoice_NUmber <> ''
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          aud:type          = 'JOB'
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'RECEPTION DESPATCH'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                  Else !If Access:DESBATCH.TryInsert() = Level:Benign
                      !Insert Failed
                  End !If Access:DESBATCH.TryInsert() = Level:Benign
              End !If Access:DESBATCH.PrimeRecord() = Level:Benign
          End !If tmp:ConsignNo <> ''
      End !job:Invoice_Number <> ''
      !Brw1.UpdateViewRecord()
      !
      !!Had job already been invoiced?
      !If job:Invoice_Number <> ''
      !    Case MessageEx('This job has already been invoiced.','ServiceBase 2000',|
      !                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      !        Of 1 ! &OK Button
      !    End!Case MessageEx
      !Else !job:Invoice_Number <> ''
      !    tmp:Courier = RapidReceptionCourier(job:Courier)
      !    If tmp:Courier <> ''
      !        job:Courier = tmp:Courier
      !
      !        glo:Select1 = job:Ref_Number
      !        Job_Receipt
      !
      !        !If it's a chargeable job, take a payment
      !        If job:Chargeable_Job = 'YES'
      !            glo:Select2 = 'PROCESS'
      !            Browse_Payments
      !            If CheckPaid() = Level:Benign
      !                GetStatus(910,1,'JOB')
      !            Else !If CheckPaid() = Level:Benign
      !                GetStatus(905,1,'JOB')
      !            End !If CheckPaid() = Level:Benign
      !            Access:JOBS.Update()
      !        Else !If job:Chargeable_Job = 'YES'
      !
      !        End !If job:Chargeable_Job = 'YES'
      !
      !
      !    End !If tmp:Courier <> ''
      !End !job:Invoice_Number <> ''
      !
      !access:jobs.clearkey(job:ref_number_key)
      !job:ref_number  = brw1.q.job:ref_number
      !If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      !    If job:invoice_Number <> ''
      !        Case MessageEx('This job has already been invoiced.','ServiceBase 2000',|
      !                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      !            Of 1 ! &OK Button
      !        End!Case MessageEx
      !    Else!If job:invoice_Number <> ''
      !        glo:Select3= job:courier
      !        RapidReceptionCourier
      !
      !        If glo:select3 <> ''
      !            job:courier = glo:select3
      !            glo:select1  = brw1.q.job:ref_number
      !            Job_Receipt
      !            If job:chargeable_job = 'YES'
      !                glo:select2  = 'PROCESS'
      !                Browse_Payments
      !                IF CheckPaid() = Level:Benign
      !                    GetStatus(910,1,'JOB') !Despatch Paid
      !                Else!IF CheckPaid() = Level:Benign
      !                    GetStatus(905,1,'JOB') !Despatch Paid
      !                End!IF CheckPaid() = Level:Benign
      !                access:jobs.update()
      !            Else!If job:chargeable_job = 'YES'
      !                If access:desbatch.primerecord() = Level:Benign
      !                    access:desbatch.tryinsert()
      !                    job:date_despatched = Today()
      !                    job:despatch_number = dbt:batch_number
      !                    job:despatched = 'YES'
      !                    access:users.clearkey(use:password_key)
      !                    use:password =glo:password
      !                    access:users.fetch(use:password_key)
      !                    job:despatch_user = use:user_code
      !                    job:consignment_number = use:user_code
      !    !                Include('ChkPaid.inc')
      !    !
      !    !                job:invoice_number  = inv:invoice_number
      !    !                job:invoice_Date    = Today()
      !    !                job:Invoice_Courier_Cost    = job:courier_Cost
      !    !                job:invoice_labour_cost     = job:labour_cost
      !    !                job:invoice_parts_cost      = job:parts_cost
      !    !                job:invoice_sub_total       = job:sub_total
      !                    If job:invoice_number_warranty <> ''
      !                        GetStatus(910,1,'JOB') !Despatch Paid
      !                    Else!If job:invoice_number_warranty <> ''
      !                        GetStatus(905,1,'JOB') !Despatch Paid
      !                    End!If job:invoice_number_warranty <> ''
      !                    
      !                    access:jobs.update()
      !
      !                    get(audit,0)
      !                    if access:audit.primerecord() = level:benign
      !                        aud:ref_number    = job:ref_number
      !                        aud:date          = today()
      !                        aud:time          = clock()
      !                        aud:type          = 'JOB'
      !                        access:users.clearkey(use:password_key)
      !                        use:password =glo:password
      !                        access:users.fetch(use:password_key)
      !                        aud:user = use:user_code
      !                        aud:action        = 'RECEPTION DESPATCH'
      !                        access:audit.insert()
      !                    end!�if access:audit.primerecord() = level:benign
      !                End!If access:desbatch.primerecord() = Level:Benign
      !            End!If job:chargeable_job = 'YES'
      !        End!If glo:select3 <> ''
      !    End!If job:invoice_Number <> ''
      !End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Despatch, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Process_Despatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Despatch, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Despatch, Accepted)
    OF ?Key
      ThisWindow.Update
      Colour_Key
      ThisWindow.Reset
    OF ?tmp:AccountNumber
      IF tmp:AccountNumber OR ?tmp:AccountNumber{Prop:Req}
        sub:Account_Number = tmp:AccountNumber
        !Save Lookup Field Incase Of error
        look:tmp:AccountNumber        = tmp:AccountNumber
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:AccountNumber = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            tmp:AccountNumber = look:tmp:AccountNumber
            SELECT(?tmp:AccountNumber)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupAccountNumber
      ThisWindow.Update
      sub:Account_Number = tmp:AccountNumber
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:AccountNumber = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?tmp:AccountNumber)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:AccountNumber)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Rapid_Reception_Despatch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:AccountNumber
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountNumber, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?LookupAccountNumber)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupAccountNumber)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:AccountNumber, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='38R(2)|M*~Job No~L@s8@#1#106L(2)|M*~Surname~@s30@#6#98L(2)|M*~Model Number~@s30@#11#125L(2)|M*~Status~@s30@#16#60L(2)|M*~Mobile Number~@s15@#21#64L(2)|M*~I.M.E.I. Number~@s16@#26#11L(2)*I~O~@s3@#31#11L(2)*I~F~@s1@#37#11L(2)*I~C~@s1@#43#11L(2)*I~W~@s1@#49#11L(2)*I~D~@s1@#55#11L(2)*I~P~@s1@#61#'
          ?Tab:2{PROP:TEXT} = 'By Job Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='38R(2)|M*~Job No~L@s8@#1#106L(2)|M*~Surname~@s30@#6#98L(2)|M*~Model Number~@s30@#11#125L(2)|M*~Status~@s30@#16#60L(2)|M*~Mobile Number~@s15@#21#64L(2)|M*~I.M.E.I. Number~@s16@#26#11L(2)*I~O~@s3@#31#11L(2)*I~F~@s1@#37#11L(2)*I~C~@s1@#43#11L(2)*I~W~@s1@#49#11L(2)*I~D~@s1@#55#11L(2)*I~P~@s1@#61#'
          ?Tab2{PROP:TEXT} = 'By Surname'
        OF 3
          ?Browse:1{PROP:FORMAT} ='38R(2)|M*~Job No~L@s8@#1#106L(2)|M*~Surname~@s30@#6#98L(2)|M*~Model Number~@s30@#11#125L(2)|M*~Status~@s30@#16#60L(2)|M*~Mobile Number~@s15@#21#64L(2)|M*~I.M.E.I. Number~@s16@#26#11L(2)*I~O~@s3@#31#11L(2)*I~F~@s1@#37#11L(2)*I~C~@s1@#43#11L(2)*I~W~@s1@#49#11L(2)*I~D~@s1@#55#11L(2)*I~P~@s1@#61#'
          ?Tab3{PROP:TEXT} = 'By Mobile Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='38R(2)|M*~Job No~L@s8@#1#106L(2)|M*~Surname~@s30@#6#98L(2)|M*~Model Number~@s30@#11#125L(2)|M*~Status~@s30@#16#60L(2)|M*~Mobile Number~@s15@#21#64L(2)|M*~I.M.E.I. Number~@s16@#26#11L(2)*I~O~@s3@#31#11L(2)*I~F~@s1@#37#11L(2)*I~C~@s1@#43#11L(2)*I~W~@s1@#49#11L(2)*I~D~@s1@#55#11L(2)*I~P~@s1@#61#120L(2)*I~Order Number~@s30@#67#'
          ?Tab4{PROP:TEXT} = 'By I.M.E.I. Number'
        OF 5
          ?Browse:1{PROP:FORMAT} ='125L(2)|M*I~Order Number~@s30@#67#38R(2)|M*~Job No~L@s8@#1#106L(2)|M*~Surname~@s30@#6#98L(2)|M*~Model Number~@s30@#11#60L(2)|M*~Mobile Number~@s15@#21#64L(2)|M*~I.M.E.I. Number~@s16@#26#11L(2)*I~O~@s3@#31#11L(2)*I~F~@s1@#37#11L(2)*I~C~@s1@#43#11L(2)*I~W~@s1@#49#11L(2)*I~D~@s1@#55#11L(2)*I~P~@s1@#61#'
          ?Tab6{PROP:TEXT} = 'By Account Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
    OF ?job:Surname
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Surname, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Surname, Selected)
    OF ?job:Mobile_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Mobile_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Mobile_Number, Selected)
    OF ?job:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Selected)
    OF ?job:Order_Number
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 4
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 5
    RETURN SELF.SetSort(5,Force)
  ELSE
    RETURN SELF.SetSort(6,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  IF (job:Exchange_Unit_Number <> '')
    tmp:Status = job:Exchange_Status
  ELSE
    tmp:Status = job:Current_Status
  END
  !Calculate VAT and total
  access:subtracc.clearkey(sub:account_number_key)
  sub:account_number = job:account_number
  if access:subtracc.fetch(sub:account_number_key) = level:benign
      access:tradeacc.clearkey(tra:account_number_key) 
      tra:account_number = sub:main_account_number
      if access:tradeacc.fetch(tra:account_number_key) = level:benign
          if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:labour_vat_code
              if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                  labour_rate$ = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = sub:parts_vat_code
              if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                  parts_rate$ = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          else!if tra:invoice_sub_accounts = 'YES'
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:labour_vat_code
              if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                  labour_rate$ = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
              access:vatcode.clearkey(vat:vat_code_key)
              vat:vat_code = tra:parts_vat_code
              if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                  parts_rate$ = vat:vat_rate
              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
          end!if tra:invoice_sub_accounts = 'YES'
      end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
  end!if access:subtracc.fetch(sub:account_number_key) = level:benign
  tmp:vat = Round(job:labour_cost * labour_rate$/100,.01) + |
              Round(job:parts_Cost * parts_rate$/100,.01) + |
              Round(job:courier_Cost * labour_rate$/100,.01)
  tmp:total = Round(job:parts_cost + job:labour_cost + job:courier_cost + tmp:vat,.01)
  
  tmp:O = 'NO'
  turnaround# = 0
  status# = 0
  If job:turnaround_end_date <> ''
      If job:date_completed = '' And job:turnaround_end_date <> 90950
          If job:turnaround_end_date < Today()
              turnaround# = 1
          Else!If job:turnaround_end_date < Today()
              If job:turnaround_end_date = Today() And job:turnaround_end_time < Clock()
                  turnaround# = 1
              End!If job:turnaround_end_date = Today() And job:turnaround_end_time < Clock()
          End!If job:turnaround_end_date < Today()
      End!If job:date_completed = '' And job:turnaround_end_date <> 90950
  End!If job:turnaround_end_date <> ''
  If job:status_end_date <> ''
      If tmp:O = 'NO' And job:date_completed = '' And job:status_end_date <> 90950
          If job:status_end_date < Today()
              status# = 1
          Else!If job:status_end_date < Today()
              If job:status_end_date = Today() and job:status_end_time < Clock()
                  status# = 1
              End!If job:status_end_date = Today() and job:status_end_time < Clock()
          End!If job:status_end_date < Today()
      End!If overdue# = 0 And job:date_completed And job:status_end_date <> 90950
  End!If job:status_end_date <> ''
  If turnaround# = 1 And status# = 1
      tmp:O = 'BOT'
  End!If turnaround# = 1 And status# = 1
  If turnaround# = 0 and status# = 1
      tmp:O = 'STA'
  End!If turnaround# = 0 and status# = 1
  If turnaround# = 1 and status# = 0
      tmp:O = 'TUR'
  End!If turnaround# = 1 and status# = 0
  
  PARENT.SetQueueRecord
  SELF.Q.job:Ref_Number_NormalFG = -1
  SELF.Q.job:Ref_Number_NormalBG = -1
  SELF.Q.job:Ref_Number_SelectedFG = -1
  SELF.Q.job:Ref_Number_SelectedBG = -1
  SELF.Q.job:Surname_NormalFG = -1
  SELF.Q.job:Surname_NormalBG = -1
  SELF.Q.job:Surname_SelectedFG = -1
  SELF.Q.job:Surname_SelectedBG = -1
  SELF.Q.job:Model_Number_NormalFG = -1
  SELF.Q.job:Model_Number_NormalBG = -1
  SELF.Q.job:Model_Number_SelectedFG = -1
  SELF.Q.job:Model_Number_SelectedBG = -1
  SELF.Q.tmp:Status_NormalFG = -1
  SELF.Q.tmp:Status_NormalBG = -1
  SELF.Q.tmp:Status_SelectedFG = -1
  SELF.Q.tmp:Status_SelectedBG = -1
  SELF.Q.job:Mobile_Number_NormalFG = -1
  SELF.Q.job:Mobile_Number_NormalBG = -1
  SELF.Q.job:Mobile_Number_SelectedFG = -1
  SELF.Q.job:Mobile_Number_SelectedBG = -1
  SELF.Q.job:ESN_NormalFG = -1
  SELF.Q.job:ESN_NormalBG = -1
  SELF.Q.job:ESN_SelectedFG = -1
  SELF.Q.job:ESN_SelectedBG = -1
  SELF.Q.tmp:O_NormalFG = -1
  SELF.Q.tmp:O_NormalBG = -1
  SELF.Q.tmp:O_SelectedFG = -1
  SELF.Q.tmp:O_SelectedBG = -1
  IF (tmp:O = 'BOT')
    SELF.Q.tmp:O_Icon = 6
  ELSIF (tmp:O = 'STA')
    SELF.Q.tmp:O_Icon = 7
  ELSIF (tmp:O = 'TUR')
    SELF.Q.tmp:O_Icon = 8
  ELSE
    SELF.Q.tmp:O_Icon = 12
  END
  SELF.Q.tmp:F_NormalFG = -1
  SELF.Q.tmp:F_NormalBG = -1
  SELF.Q.tmp:F_SelectedFG = -1
  SELF.Q.tmp:F_SelectedBG = -1
  IF (job:Date_Completed <> '')
    SELF.Q.tmp:F_Icon = 5
  ELSE
    SELF.Q.tmp:F_Icon = 12
  END
  SELF.Q.tmp:C_NormalFG = -1
  SELF.Q.tmp:C_NormalBG = -1
  SELF.Q.tmp:C_SelectedFG = -1
  SELF.Q.tmp:C_SelectedBG = -1
  IF (job:Chargeable_Job = 'YES' and job:Invoice_Number <> '')
    SELF.Q.tmp:C_Icon = 9
  ELSIF (job:Chargeable_Job = 'YES' and job:Invoice_Number = '')
    SELF.Q.tmp:C_Icon = 10
  ELSE
    SELF.Q.tmp:C_Icon = 12
  END
  SELF.Q.tmp:W_NormalFG = -1
  SELF.Q.tmp:W_NormalBG = -1
  SELF.Q.tmp:W_SelectedFG = -1
  SELF.Q.tmp:W_SelectedBG = -1
  IF (job:warranty_job = 'YES' And job:edi_batch_number <> '')
    SELF.Q.tmp:W_Icon = 11
  ELSIF (job:warranty_job = 'YES' and job:edi_batch_number = '')
    SELF.Q.tmp:W_Icon = 10
  ELSE
    SELF.Q.tmp:W_Icon = 12
  END
  SELF.Q.tmp:D_NormalFG = -1
  SELF.Q.tmp:D_NormalBG = -1
  SELF.Q.tmp:D_SelectedFG = -1
  SELF.Q.tmp:D_SelectedBG = -1
  IF (job:despatched = 'YES')
    SELF.Q.tmp:D_Icon = 4
  ELSE
    SELF.Q.tmp:D_Icon = 12
  END
  SELF.Q.tmp:P_NormalFG = -1
  SELF.Q.tmp:P_NormalBG = -1
  SELF.Q.tmp:P_SelectedFG = -1
  SELF.Q.tmp:P_SelectedBG = -1
  IF (job:paid = 'YES' And job:invoice_number_warranty <> '')
    SELF.Q.tmp:P_Icon = 1
  ELSIF (job:paid = 'YES' And job:invoice_number_warranty = '')
    SELF.Q.tmp:P_Icon = 2
  ELSIF (job:paid <> 'YES' and job:invoice_number_warranty <> '')
    SELF.Q.tmp:P_Icon = 3
  ELSE
    SELF.Q.tmp:P_Icon = 12
  END
  SELF.Q.job:Order_Number_NormalFG = -1
  SELF.Q.job:Order_Number_NormalBG = -1
  SELF.Q.job:Order_Number_SelectedFG = -1
  SELF.Q.job:Order_Number_SelectedBG = -1
  SELF.Q.job:Order_Number_Icon = 0
  SELF.Q.tmp:Status = tmp:Status                      !Assign formula result to display queue
   
   
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.job:Ref_Number_NormalFG = 255
     SELF.Q.job:Ref_Number_NormalBG = 16777215
     SELF.Q.job:Ref_Number_SelectedFG = 16777215
     SELF.Q.job:Ref_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Ref_Number_NormalFG = -1
     SELF.Q.job:Ref_Number_NormalBG = -1
     SELF.Q.job:Ref_Number_SelectedFG = -1
     SELF.Q.job:Ref_Number_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.job:Surname_NormalFG = 255
     SELF.Q.job:Surname_NormalBG = 16777215
     SELF.Q.job:Surname_SelectedFG = 16777215
     SELF.Q.job:Surname_SelectedBG = 255
   ELSE
     SELF.Q.job:Surname_NormalFG = -1
     SELF.Q.job:Surname_NormalBG = -1
     SELF.Q.job:Surname_SelectedFG = -1
     SELF.Q.job:Surname_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.job:Model_Number_NormalFG = 255
     SELF.Q.job:Model_Number_NormalBG = 16777215
     SELF.Q.job:Model_Number_SelectedFG = 16777215
     SELF.Q.job:Model_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Model_Number_NormalFG = -1
     SELF.Q.job:Model_Number_NormalBG = -1
     SELF.Q.job:Model_Number_SelectedFG = -1
     SELF.Q.job:Model_Number_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.tmp:Status_NormalFG = 255
     SELF.Q.tmp:Status_NormalBG = 16777215
     SELF.Q.tmp:Status_SelectedFG = 16777215
     SELF.Q.tmp:Status_SelectedBG = 255
   ELSE
     SELF.Q.tmp:Status_NormalFG = -1
     SELF.Q.tmp:Status_NormalBG = -1
     SELF.Q.tmp:Status_SelectedFG = -1
     SELF.Q.tmp:Status_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.job:Mobile_Number_NormalFG = 255
     SELF.Q.job:Mobile_Number_NormalBG = 16777215
     SELF.Q.job:Mobile_Number_SelectedFG = 16777215
     SELF.Q.job:Mobile_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Mobile_Number_NormalFG = -1
     SELF.Q.job:Mobile_Number_NormalBG = -1
     SELF.Q.job:Mobile_Number_SelectedFG = -1
     SELF.Q.job:Mobile_Number_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.job:ESN_NormalFG = 255
     SELF.Q.job:ESN_NormalBG = 16777215
     SELF.Q.job:ESN_SelectedFG = 16777215
     SELF.Q.job:ESN_SelectedBG = 255
   ELSE
     SELF.Q.job:ESN_NormalFG = -1
     SELF.Q.job:ESN_NormalBG = -1
     SELF.Q.job:ESN_SelectedFG = -1
     SELF.Q.job:ESN_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.tmp:O_NormalFG = 255
     SELF.Q.tmp:O_NormalBG = 16777215
     SELF.Q.tmp:O_SelectedFG = 16777215
     SELF.Q.tmp:O_SelectedBG = 255
   ELSE
     SELF.Q.tmp:O_NormalFG = -1
     SELF.Q.tmp:O_NormalBG = -1
     SELF.Q.tmp:O_SelectedFG = -1
     SELF.Q.tmp:O_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.tmp:F_NormalFG = 255
     SELF.Q.tmp:F_NormalBG = 16777215
     SELF.Q.tmp:F_SelectedFG = 16777215
     SELF.Q.tmp:F_SelectedBG = 255
   ELSE
     SELF.Q.tmp:F_NormalFG = -1
     SELF.Q.tmp:F_NormalBG = -1
     SELF.Q.tmp:F_SelectedFG = -1
     SELF.Q.tmp:F_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.tmp:C_NormalFG = 255
     SELF.Q.tmp:C_NormalBG = 16777215
     SELF.Q.tmp:C_SelectedFG = 16777215
     SELF.Q.tmp:C_SelectedBG = 255
   ELSE
     SELF.Q.tmp:C_NormalFG = -1
     SELF.Q.tmp:C_NormalBG = -1
     SELF.Q.tmp:C_SelectedFG = -1
     SELF.Q.tmp:C_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.tmp:W_NormalFG = 255
     SELF.Q.tmp:W_NormalBG = 16777215
     SELF.Q.tmp:W_SelectedFG = 16777215
     SELF.Q.tmp:W_SelectedBG = 255
   ELSE
     SELF.Q.tmp:W_NormalFG = -1
     SELF.Q.tmp:W_NormalBG = -1
     SELF.Q.tmp:W_SelectedFG = -1
     SELF.Q.tmp:W_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.tmp:D_NormalFG = 255
     SELF.Q.tmp:D_NormalBG = 16777215
     SELF.Q.tmp:D_SelectedFG = 16777215
     SELF.Q.tmp:D_SelectedBG = 255
   ELSE
     SELF.Q.tmp:D_NormalFG = -1
     SELF.Q.tmp:D_NormalBG = -1
     SELF.Q.tmp:D_SelectedFG = -1
     SELF.Q.tmp:D_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.tmp:P_NormalFG = 255
     SELF.Q.tmp:P_NormalBG = 16777215
     SELF.Q.tmp:P_SelectedFG = 16777215
     SELF.Q.tmp:P_SelectedBG = 255
   ELSE
     SELF.Q.tmp:P_NormalFG = -1
     SELF.Q.tmp:P_NormalBG = -1
     SELF.Q.tmp:P_SelectedFG = -1
     SELF.Q.tmp:P_SelectedBG = -1
   END
   IF (job:Date_Despatched = '' ANd job:date_Completed <> '')
     SELF.Q.job:Order_Number_NormalFG = 255
     SELF.Q.job:Order_Number_NormalBG = 16777215
     SELF.Q.job:Order_Number_SelectedFG = 16777215
     SELF.Q.job:Order_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Order_Number_NormalFG = -1
     SELF.Q.job:Order_Number_NormalBG = -1
     SELF.Q.job:Order_Number_SelectedFG = -1
     SELF.Q.job:Order_Number_SelectedBG = -1
   END
      ! Start Change 2618 BE(06/08/03)
      IF (SELF.Q.job:Ref_Number_NormalFG <> 255) THEN
      LOOP ix# = 1 TO 8
          IF ((JSC:Status[ix#] <> '') AND (job:current_status = JSC:Status[ix#])) THEN
  
              SELF.Q.job:Ref_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Ref_Number_NormalBG = COLOR:White
              SELF.Q.job:Ref_Number_SelectedFG = COLOR:White
              SELF.Q.job:Ref_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Surname_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Surname_NormalBG = COLOR:White
              SELF.Q.job:Surname_SelectedFG = COLOR:White
              SELF.Q.job:Surname_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Model_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Model_Number_NormalBG = COLOR:White
              SELF.Q.job:Model_Number_SelectedFG = COLOR:White
              SELF.Q.job:Model_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:Status_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:Status_NormalBG = COLOR:White
              SELF.Q.tmp:Status_SelectedFG = COLOR:White
              SELF.Q.tmp:Status_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Mobile_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Mobile_Number_NormalBG = COLOR:White
              SELF.Q.job:Mobile_Number_SelectedFG = COLOR:White
              SELF.Q.job:Mobile_Number_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:ESN_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:ESN_NormalBG = COLOR:White
              SELF.Q.job:ESN_SelectedFG = COLOR:White
              SELF.Q.job:ESN_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:O_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:O_NormalBG = COLOR:White
              SELF.Q.tmp:O_SelectedFG = COLOR:White
              SELF.Q.tmp:O_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:F_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:F_NormalBG = COLOR:White
              SELF.Q.tmp:F_SelectedFG = COLOR:White
              SELF.Q.tmp:F_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:C_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:C_NormalBG = COLOR:White
              SELF.Q.tmp:C_SelectedFG = COLOR:White
              SELF.Q.tmp:C_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:W_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:W_NormalBG = COLOR:White
              SELF.Q.tmp:W_SelectedFG = COLOR:White
              SELF.Q.tmp:W_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:D_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:D_NormalBG = COLOR:White
              SELF.Q.tmp:D_SelectedFG = COLOR:White
              SELF.Q.tmp:D_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.tmp:P_NormalFG = JSC:Colour[ix#]
              SELF.Q.tmp:P_NormalBG = COLOR:White
              SELF.Q.tmp:P_SelectedFG = COLOR:White
              SELF.Q.tmp:P_SelectedBG = JSC:Colour[ix#]
  
              SELF.Q.job:Order_Number_NormalFG = JSC:Colour[ix#]
              SELF.Q.job:Order_Number_NormalBG = COLOR:White
              SELF.Q.job:Order_Number_SelectedFG = COLOR:White
              SELF.Q.job:Order_Number_SelectedBG = JSC:Colour[ix#]
              BREAK
          END
      END
      END
      ! End Change 2618 BE(06/08/03)
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

