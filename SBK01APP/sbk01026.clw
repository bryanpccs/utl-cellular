

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01026.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Invoice_Exceptions PROCEDURE                   !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisThreadActive BYTE
CurrentTab           STRING(80)
pos                  STRING(255)
FilesOpened          BYTE
tmp:yes              STRING('YES')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:Unit_Type)
                       PROJECT(job:Charge_Type)
                       PROJECT(job:Parts_Cost)
                       PROJECT(job:Labour_Cost)
                       PROJECT(job:Invoice_Failure_Reason)
                       PROJECT(job:Invoice_Exception)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:Unit_Type          LIKE(job:Unit_Type)            !List box control field - type derived from field
job:Charge_Type        LIKE(job:Charge_Type)          !List box control field - type derived from field
job:Parts_Cost         LIKE(job:Parts_Cost)           !List box control field - type derived from field
job:Labour_Cost        LIKE(job:Labour_Cost)          !List box control field - type derived from field
job:Invoice_Failure_Reason LIKE(job:Invoice_Failure_Reason) !List box control field - type derived from field
job:Invoice_Exception  LIKE(job:Invoice_Exception)    !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Chargeable Invoice Exceptions'),AT(,,668,260),FONT('Tahoma',8,,),CENTER,IMM,ICON('Pc.ico'),HLP('Browse_Invoice_Exceptions'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,36,568,216),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('48R(2)|M~Job Number~@s8@63L(2)|M~Account Number~@s15@64L(2)|M~Model Number~@s30@' &|
   '64L(2)|M~Unit Type~@s30@64L(2)|M~Charge Type~@s30@56R(2)|M~Parts Cost~@n14.2@56R' &|
   '(2)|M~Labour Cost~@n14.2@320L(2)|M~Failure Reason~@s80@'),FROM(Queue:Browse:1)
                       BUTTON('Authorise All Jobs'),AT(588,52,76,20),USE(?Reauthorise),LEFT,ICON('Check1.gif')
                       BUTTON('Authorise Selected  Job'),AT(588,28,76,20),USE(?Authorise),LEFT,ICON('Check1.gif')
                       SHEET,AT(4,4,576,252),USE(?CurrentTab),SPREAD
                         TAB('By Job Number'),USE(?Tab:2)
                           ENTRY(@s8),AT(8,20,64,10),USE(job:Ref_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&View/Amend Job'),AT(588,168,76,20),USE(?Change),LEFT,ICON('Edit.ico')
                       BUTTON('Close'),AT(588,236,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

!! moving bar window
!rejectrecord         long
!recordstoprocess     long,auto
!recordsprocessed     long,auto
!recordspercycle      long,auto
!recordsthiscycle     long,auto
!percentprogress      byte
!recordstatus         byte,auto
!
!progress:thermometer byte
!progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
!         double
!       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
!       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
!       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
!     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_job_id   ushort,auto
! Start Change 4776 BE(22/10/2004)
    MAP
AuthoriseJob    PROCEDURE(),BYTE          
    END
! End Change 4776 BE(22/10/2004)
! Progress Bar Window Declarations

RecordsToProcess        LONG,AUTO
RecordsProcessed        LONG,AUTO
RecordsPerCycle         LONG,AUTO
RecordsThisCycle        LONG,AUTO
PercentProgress         LONG,AUTO
ProgressThermometer     LONG,AUTO
AppName                 STRING(30)
ProgressWindow          WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
                            PROGRESS,USE(ProgressThermometer),AT(25,15,111,12),RANGE(0,100)
                            STRING(''),AT(0,3,161,10),USE(?ProgressUserString),CENTER,FONT('Tahoma',8,,)
                            STRING(''),AT(0,30,161,10),USE(?ProgressText),TRN,CENTER,FONT('Tahoma',8,,)
                            BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
                        END

               MAP
ProgressBarShow         PROCEDURE(STRING arg:AppNameString, LONG arg:RecordCount=1000,  |
                                  LONG arg:CycleCount=100,   <STRING arg:UserText>)
ProgressBarReset        PROCEDURE(LONG arg:RecordCount=1000, <LONG arg:CycleCount>, <STRING arg:UserText>)
ProgressBarUpdate       PROCEDURE(),LONG
ProgressBarCancelled    PROCEDURE(),LONG
ProgressBarClose        PROCEDURE()
              END
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!getnextrecord2      routine
!  recordsprocessed += 1
!  recordsthiscycle += 1
!  if percentprogress < 100
!    percentprogress = (recordsprocessed / recordstoprocess)*100
!    if percentprogress > 100
!      percentprogress = 100
!    end
!    if percentprogress <> progress:thermometer then
!      progress:thermometer = percentprogress
!      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
!      display()
!    end
!  end
!endprintrun         routine
!    progress:thermometer = 100
!    ?progress:pcttext{prop:text} = '100% Completed'
!    close(progresswindow)
!    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Invoice_Exceptions',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Invoice_Exceptions',1)
    SolaceViewVars('pos',pos,'Browse_Invoice_Exceptions',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Invoice_Exceptions',1)
    SolaceViewVars('tmp:yes',tmp:yes,'Browse_Invoice_Exceptions',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reauthorise;  SolaceCtrlName = '?Reauthorise';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Authorise;  SolaceCtrlName = '?Authorise';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Invoice_Exceptions')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Invoice_Exceptions')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Invoice_Exceptions'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:VATCODE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:STDCHRGE.UseFile
  Access:WARPARTS.UseFile
  Access:PARTS.UseFile
  Access:TRACHAR.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,job:InvoiceExceptKey)
  BRW1.AddRange(job:Invoice_Exception,tmp:yes)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?JOB:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.SetFilter('(job:chargeable_job=''YES'' AND job:bouncer<<>''X'')')
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:Unit_Type,BRW1.Q.job:Unit_Type)
  BRW1.AddField(job:Charge_Type,BRW1.Q.job:Charge_Type)
  BRW1.AddField(job:Parts_Cost,BRW1.Q.job:Parts_Cost)
  BRW1.AddField(job:Labour_Cost,BRW1.Q.job:Labour_Cost)
  BRW1.AddField(job:Invoice_Failure_Reason,BRW1.Q.job:Invoice_Failure_Reason)
  BRW1.AddField(job:Invoice_Exception,BRW1.Q.job:Invoice_Exception)
  QuickWindow{PROP:MinWidth}=668
  QuickWindow{PROP:MinHeight}=260
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Invoice_Exceptions'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Invoice_Exceptions',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBS
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Reauthorise
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reauthorise, Accepted)
      Case MessageEx('Are you sure you want to Authorise all the jobs?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
      
          ! Start Change 4776 BE(22/10/2004)
          !AuthoriseAll
          count# = 0
          cancel# = 0
      
          ProgressBarShow('Zero Exceptions', RECORDS(jobs))
      
          access:jobs.clearkey(job:InvoiceExceptKey)
          job:Invoice_Exception = 'YES'
          SET(job:InvoiceExceptKey,job:InvoiceExceptKey)
          LOOP
              IF ((access:jobs.next() <> Level:Benign) OR |
                  (job:Invoice_Exception <> 'YES')) THEN
                  BREAK
              END
              IF (NOT ProgressBarUpdate()) THEN
                  cancel# = 1
                  BREAK
              END
              IF (NOT AuthoriseJob()) THEN
                  count# += 1
              END
          END
          ProgressBarClose()
      
          IF ((count# > 0) AND (cancel# = 0)) THEN
              MessageEx('Authorisation Failed!','ServiceBase 2000',|
                        'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,|
                        '',0,beep:systemhand,msgex:samewidths,84,26,0)
          END
          ! Start Change 4776 BE(22/10/2004)
      !
      !    recordspercycle     = 25
      !    recordsprocessed    = 0
      !    percentprogress     = 0
      !    setcursor(cursor:wait)
      !    open(progresswindow)
      !    progress:thermometer    = 0
      !    ?progress:pcttext{prop:text} = '0% Completed'
      !
      !    recordstoprocess    = Records(jobs)
      !
      !    save_job_id = access:jobs.savefile()
      !    access:jobs.clearkey(job:InvoiceExceptKey)
      !    job:invoice_exception = 'YES'
      !    set(job:InvoiceExceptKey,job:InvoiceExceptKey)
      !    loop
      !        if access:jobs.next()
      !           break
      !        end !if
      !        Do getnextrecord2
      !        if job:invoice_exception <> 'YES'      |
      !            then break.  ! end if
      !        yldcnt# += 1
      !        if yldcnt# > 25
      !           yield() ; yldcnt# = 0
      !        end !if
      !!No Warranty Jobs
      !        if job:chargeable_job <> 'YES'
      !            pos = Position(job:InvoiceExceptKey)
      !            job:invoice_exception = 'NO'
      !            job:invoice_failure_reason = ''
      !            access:jobs.update()
      !            reset(job:InvoiceExceptKey,pos)
      !            Cycle
      !        End!if job:chargeable_job <> 'YES'
      !
      !        If ExcludeFromInvoice(job:charge_type) and Job:chargeable_job = 'YES'
      !            pos = Position(job:InvoiceExceptKey)
      !            job:invoice_exception = 'NO'
      !            job:invoice_failure_reason = ''
      !            access:jobs.update()
      !            reset(job:InvoiceExceptKey,pos)
      !            Cycle
      !        End!If ExcludeFromInvoice(job:charge_type)
      !
      !        If job:date_completed = ''
      !            Cycle
      !        End!If job:date_completed = ''
      !        count# = 0
      !        If job:bouncer = 'X'
      !            job:invoice_failure_reason = 'BOUNCER'
      !            access:jobs.update()
      !            count# += 1
      !            Cycle
      !        End!If job:bouncer <> ''
      !
      !        If def:qa_required = 'YES'
      !            If job:qa_passed <> 'YES'
      !                job:invoice_failure_reason   = 'QA NOT PASSED'
      !                access:jobs.update()
      !                count# += 1
      !                Cycle
      !            End!If job:qa_passed <> 'YES'
      !        End!If def:qa_required = 'YES'
      !
      !        Case PricingRoutine('C')
      !            Of 1 Orof 3
      !                job:invoice_exception   = 'YES'
      !                job:invoice_failure_reason  = 'ZERO VALUE: NO PRICING STRUCTURE'
      !                access:jobs.update()
      !                count# += 1
      !                Cycle
      !            Of 2
      !                job:sub_total    = job:labour_cost + job:parts_cost + job:courier_cost
      !                access:jobs.update()
      !                If job:sub_total = 0
      !                    job:invoice_failure_reason  = 'ZERO VALUE'
      !                    access:jobs.update()
      !                    count# += 1
      !                    Cycle
      !                End!If job:sub_total = 0
      !            Else
      !                job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
      !        End!Case PricingRoutine('C')
      !
      !        If count# = 0
      !            pos = Position(job:InvoiceExceptKey)
      !            job:invoice_exception = 'NO'
      !            job:invoice_failure_reason = ''
      !            access:jobs.update()
      !            get(audit,0)
      !            if access:audit.primerecord() = Level:Benign
      !                aud:notes         = ''
      !                aud:ref_number    = job:ref_number
      !                aud:date          = Today()
      !                aud:time          = Clock()
      !                access:users.clearkey(use:password_key)
      !                use:password      = glo:password
      !                access:users.fetch(use:password_key)
      !                aud:user          = use:user_code
      !                aud:action        = 'AUTHORISED INVOICE EXCEPTION'
      !                if access:audit.insert()
      !                   access:audit.cancelautoinc()
      !                end
      !            end!if access:audit.primerecord() = Level:Benign
      !
      !            reset(job:InvoiceExceptKey,pos)
      !        End!If count# = 0
      !    end !loop
      !    access:jobs.restorefile(save_job_id)
      !    setcursor()
      !    close(progresswindow)
      
        Of 2 ! &No Button
      End!Case MessageEx
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reauthorise, Accepted)
    OF ?Authorise
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Authorise, Accepted)
      !Case MessageEx('This will Authorise the selected job for Invoicing.<13,10><13,10>Are you sure?','ServiceBase 2000',|
      !             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      !  Of 1 ! &Yes Button
      !    Thiswindow.reset
      !    access:jobs.clearkey(job:ref_number_key)
      !    job:ref_number  = brw1.q.job:ref_number
      !    If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      !!Only chargeable jobs should appear in this list
      !        If job:chargeable_job <> 'YES'
      !            job:invoice_exception = 'NO'
      !            job:invoice_failure_reason = ''
      !            access:jobs.update()
      !            Cycle
      !        End!If job:chargeable_job <> 'YES'
      !
      !!Don't show cancelled jobs
      !        If job:Cancelled = 'YES'
      !            job:Invoice_Exception = 'NO'
      !            job:Invoice_Failure_Reason = ''
      !            Access:JOBS.Update()
      !            ! Start Change 4776 BE(22/10/2004)
      !            CYCLE
      !            ! End Change 4776 BE(22/10/2004)
      !        End !If job:Cancelled = 'YES'.
      !
      !        If ExcludeFromInvoice(job:charge_type) and Job:chargeable_job = 'YES'
      !            job:invoice_exception = 'NO'
      !            job:invoice_failure_reason = ''
      !            access:jobs.update()
      !            Cycle
      !        End!If ExcludeFromInvoice(job:charge_type)
      !        count# = 0
      !
      !        If job:bouncer = 'X'
      !        ! Start Change 4776 BE(22/10/2004)
      !        !    job:invoice_exception      = 'YES'
      !        !    job:invoice_failure_reason = 'BOUNCER'
      !        !    access:jobs.update()
      !        !   count# += 1
      !            job:invoice_exception      = 'NO'
      !            job:invoice_failure_reason = ''
      !            access:jobs.update()
      !            CYCLE
      !        ! End Change 4776 BE(22/10/2004)
      !        End!If job:bouncer <> ''
      !
      !        If def:qa_required = 'YES'
      !            If job:qa_passed <> 'YES'
      !                job:invoice_exception   = 'YES'
      !                job:invoice_failure_reason   = 'QA NOT PASSED'
      !                access:jobs.update()
      !                count# += 1
      !            End!If job:qa_passed <> 'YES'
      !        End!If def:qa_required = 'YES'
      !
      !        Case PricingRoutine('C')
      !            Of 1 Orof 3
      !                job:invoice_exception   = 'YES'
      !                job:invoice_failure_reason  = 'ZERO VALUE: NO PRICING STRUCTURE'
      !                access:jobs.update()
      !                count# += 1
      !            Of 2
      !                job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
      !            Else
      !                job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
      !                If job:sub_total = 0
      !                    job:invoice_exception   = 'YES'
      !                    job:invoice_failure_reason  = 'ZERO VALUE'
      !                    access:jobs.update()
      !                    count# += 1
      !                End!If job:sub_total = 0
      !        End!Case PricingRoutine('C')
      !
      !        If count# <> 0
      !            Case MessageEx('Authorisation Failed!','ServiceBase 2000',|
      !                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      !                Of 1 ! &OK Button
      !            End!Case MessageEx
      !        Else!If pass" = False
      !            job:invoice_exception = 'NO'
      !            job:invoice_failure_reason = ''
      !            access:jobs.update()
      !            get(audit,0)
      !            if access:audit.primerecord() = Level:Benign
      !                aud:notes         = ''
      !                aud:ref_number    = job:ref_number
      !                aud:date          = Today()
      !                aud:time          = Clock()
      !                access:users.clearkey(use:password_key)
      !                use:password      = glo:password
      !                access:users.fetch(use:password_key)
      !                aud:user          = use:user_code
      !                aud:action        = 'AUTHORISED INVOICE EXCEPTION'
      !                if access:audit.insert()
      !                   access:audit.cancelautoinc()
      !                end
      !            end!if access:audit.primerecord() = Level:Benign
      !        End!If pass" = False
      !    End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
      !
      !  Of 2 ! &No Button
      !End!Case MessageEx
      ! Start Change 4776 BE(22/10/2004)
      Case MessageEx('This will Authorise the selected job for Invoicing.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
          Thiswindow.reset
          access:jobs.clearkey(job:ref_number_key)
          job:ref_number  = brw1.q.job:ref_number
          If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
             IF (NOT AuthoriseJob()) THEN
                  MessageEx('Authorisation Failed!','ServiceBase 2000',|
                            'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,|
                            '',0,beep:systemhand,msgex:samewidths,84,26,0)
             END
          END
        Of 2 ! &No Button
      End!Case MessageEx
      ! End Change 4776 BE(22/10/2004)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Authorise, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Invoice_Exceptions')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
AuthoriseJob    PROCEDURE()          ! Start Change 4776 BE(22/10/2004)
    CODE
    !Only chargeable jobs should appear in this list
    If job:chargeable_job <> 'YES'
        job:invoice_exception = 'NO'
        job:invoice_failure_reason = ''
        access:jobs.update()
        RETURN false
    End!If job:chargeable_job <> 'YES'

    !Don't show cancelled jobs
    If job:Cancelled = 'YES'
        job:Invoice_Exception = 'NO'
        job:Invoice_Failure_Reason = ''
        Access:JOBS.Update()
        RETURN false
    End !If job:Cancelled = 'YES'.

    If ExcludeFromInvoice(job:charge_type) and Job:chargeable_job = 'YES'
        job:invoice_exception = 'NO'
        job:invoice_failure_reason = ''
        access:jobs.update()
        RETURN false
    End

    If job:bouncer = 'X'
        job:invoice_exception      = 'NO'
        job:invoice_failure_reason = ''
        access:jobs.update()
        RETURN false
    End

    If def:qa_required = 'YES'
        If job:qa_passed <> 'YES'
            job:invoice_exception   = 'YES'
            job:invoice_failure_reason   = 'QA NOT PASSED'
            access:jobs.update()
            RETURN false
        End!If job:qa_passed <> 'YES'
    End!If def:qa_required = 'YES'

    IF job:ignore_chargeable_charges = 'YES'
        job:sub_total    = job:labour_cost + job:parts_cost + job:courier_cost
        access:jobs.update()
        If job:sub_total = 0
            job:invoice_exception   = 'YES'
            job:invoice_failure_reason  = 'ZERO VALUE'
            access:jobs.update()
            RETURN false
        End!If job:sub_total = 0
    Else!IF job:ignore_chargeable_charges = 'YES'
        Pricing_Routine('C',labour",parts",pass",a")
        If pass"    = False
            job:invoice_exception   = 'YES'
            job:invoice_failure_reason  = 'ZERO VALUE: NO PRICING STRUCTURE'
            access:jobs.update()
            RETURN false
        Else!If pass"    = False
            job:labour_cost = labour"
            job:parts_cost  = parts"
            job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
        End!If pass"    = False
    End!IF job:ignore_chargeable_charges = 'YES'

    job:invoice_exception = 'NO'
    job:invoice_failure_reason = ''
    access:jobs.update()
    get(audit,0)
    if access:audit.primerecord() = Level:Benign
        aud:notes         = ''
        aud:ref_number    = job:ref_number
        aud:date          = Today()
        aud:time          = Clock()
        access:users.clearkey(use:password_key)
        use:password      = glo:password
        access:users.fetch(use:password_key)
        aud:user          = use:user_code
        aud:action        = 'AUTHORISED INVOICE EXCEPTION'
        if access:audit.insert()
           access:audit.cancelautoinc()
        end
    end!if access:audit.primerecord() = Level:Benign

    RETURN true
    ! End Change 4776 BE(22/10/2004)
! Progress Bar Window Procedures
ProgressBarShow    PROCEDURE(arg:AppNameString, arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    AppName              = arg:AppNameString
    RecordsPerCycle      = arg:RecordCount  / arg:CycleCount
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    open(ProgressWindow)
         
    IF (OMITTED(4)) THEN
        ?ProgressUserString{PROP:Text} = 'Running...'
    ELSE
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarReset    PROCEDURE(arg:RecordCount, arg:CycleCount, arg:UserText)
    CODE
    IF (NOT OMITTED(2)) THEN
        RecordsPerCycle      = arg:CycleCount
    END
    RecordsThisCycle     = 0
    RecordsProcessed     = 0
    RecordsToProcess     = arg:RecordCount
    PercentProgress      = 0
    ProgressThermometer  = 0

    IF (NOT OMITTED(3)) THEN
        ?ProgressUserString{PROP:Text} = CLIP(arg:UserText)
    END
    ?progressText{prop:text}    = '0% Completed'
    DISPLAY()

    RETURN

ProgressBarUpdate     PROCEDURE()
result  LONG
    CODE
    result = -1
    RecordsProcessed += 1
    RecordsThisCycle += 1
    IF (RecordsThisCycle > RecordsPerCycle) THEN
        RecordsThisCycle = 0
        IF (ProgressBarCancelled()) THEN
            !CLOSE(ProgressWindow)
            result = 0
        ELSE
            IF (PercentProgress < 100) THEN
                PercentProgress = ROUND((RecordsProcessed / RecordsToProcess) * 100.0, 1)
                IF (PercentProgress > 100) THEN
                    PercentProgress = 100
                END
                IF (PercentProgress <> ProgressThermometer) THEN
                    ProgressThermometer = PercentProgress
                    ?ProgressText{prop:text} = format(PercentProgress,@n3) & '% Completed'
                    DISPLAY()
                END
            END
        END
    END
    RETURN result

ProgressBarCancelled       PROCEDURE()
result  LONG
    CODE
    result = 0
    cancel# = 0

    ACCEPT
        CASE EVENT()
        OF Event:Timer
            BREAK
        OF Event:CloseWindow
            cancel# = 1
            BREAK
        OF Event:accepted
            IF (FIELD() = ?ProgressCancel) THEN
                cancel# = 1
                BREAK
            END
        END
    END

    IF (cancel# = 1) THEN
          BEEP(beep:systemquestion)
          CASE MESSAGE('Are you sure you want to cancel? ', |
                                      CLIP(AppName) , ICON:Question, |
                                      BUTTON:Yes+Button:No, BUTTON:No, 0)
               OF BUTTON:Yes
         result = -1
               !OF BUTTON:No
          END !CASE
    END
    RETURN result

ProgressBarClose       PROCEDURE()
    CODE
    CLOSE(ProgressWindow)
    RETURN

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetSort, (BYTE Force),BYTE)
  ReturnValue = PARENT.ResetSort(Force)
  If Records(Queue:Browse:1)
      Enable(?Authorise)
      Enable(?reauthorise)
  Else
      Disable(?Authorise)
      Disable(?reauthorise)
  End
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetSort, (BYTE Force),BYTE)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?JOB:Ref_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
