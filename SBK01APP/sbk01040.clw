

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01040.INC'),ONCE        !Local module procedure declarations
                     END


CallRapids PROCEDURE                                  !Generated from procedure template - Window

MoveSeq:BRW1::MoveType BYTE
MoveSeq:BRW1::OutOfRange BYTE
MoveSeq:BRW1::OrigSequence LONG
MoveSeq:BRW1::TargetSequence LONG
MoveSeq:BRW1::SwapSequence LONG
MoveSeq:BRW1::SavePosition STRING(255)
MoveSeq:BRW1::SaveSequence LONG
MoveSeq:BRW1::InsertSequence LONG
MoveSeq:BRW1::DeleteSequence LONG
MoveSeq:BRW1::Section CSTRING(24)
MoveSeq:BRW1::DoNotCommit BYTE
MoveSeq:BRW1::DragSequence LONG
MoveSeq:BRW1::DropSequence LONG
ThisThreadActive BYTE
CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
MS::MoveType         BYTE
MS::OutOfRange       BYTE
MS::OrigSequence     LONG
MS::TargetSequence   LONG
MS::SwapSequence     LONG
MS::SavePosition     STRING(255)
MS::SaveSequence     LONG
MS::InsertSequence   LONG
MS::DeleteSequence   LONG
MS::Section          CSTRING(24)
MS::DoNotCommit      BYTE
BRW1::View:Browse    VIEW(RAPIDLST)
                       PROJECT(RAP:ProgramName)
                       PROJECT(RAP:EXEPath)
                       PROJECT(RAP:Description)
                       PROJECT(RAP:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
RAP:ProgramName        LIKE(RAP:ProgramName)          !List box control field - type derived from field
RAP:ProgramName_NormalFG LONG                         !Normal forground color
RAP:ProgramName_NormalBG LONG                         !Normal background color
RAP:ProgramName_SelectedFG LONG                       !Selected forground color
RAP:ProgramName_SelectedBG LONG                       !Selected background color
RAP:EXEPath            LIKE(RAP:EXEPath)              !List box control field - type derived from field
RAP:EXEPath_NormalFG   LONG                           !Normal forground color
RAP:EXEPath_NormalBG   LONG                           !Normal background color
RAP:EXEPath_SelectedFG LONG                           !Selected forground color
RAP:EXEPath_SelectedBG LONG                           !Selected background color
RAP:Description        LIKE(RAP:Description)          !List box control field - type derived from field
RAP:Description_NormalFG LONG                         !Normal forground color
RAP:Description_NormalBG LONG                         !Normal background color
RAP:Description_SelectedFG LONG                       !Selected forground color
RAP:Description_SelectedBG LONG                       !Selected background color
RAP:RecordNumber       LIKE(RAP:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Rapid Programs'),AT(,,179,191),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('CallRapids'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,44,164,140),USE(?Browse:1),IMM,MSG('Browsing Records'),ALRT(MouseLeft2),ALRT(EnterKey),FORMAT('240L(2)|M*@s60@1020L(2)|M*@s255@1020L(2)|M*~Description~@s255@'),FROM(Queue:Browse:1)
                       SHEET,AT(4,4,172,184),USE(?CurrentTab),WIZARD,SPREAD
                         TAB('By Order'),USE(?Tab:2)
                           PROMPT('Double-Click the program to run'),AT(8,8,104,16),USE(?Prompt1),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           BUTTON,AT(40,80,10,10),USE(?Insert),HIDE,LEFT,ICON('insert.ico')
                           BUTTON,AT(48,80,10,10),USE(?Change),HIDE,LEFT,ICON('edit.ico'),DEFAULT
                           BUTTON,AT(60,84,10,10),USE(?Delete),HIDE,LEFT,ICON('delete.ico')
                           BUTTON('Close'),AT(116,8,56,16),USE(?Close),LEFT,ICON('cancel.ico')
                           BUTTON('Move Up'),AT(8,28,64,12),USE(?MoveUp),LEFT,TIP('Move record up one'),ICON('VSMOVEUP.ICO')
                           BUTTON,AT(20,60,56,12),USE(?MoveBegin),HIDE,TIP('Move record to the top'),ICON('VSMOVEBG.ICO')
                           BUTTON('Move Down'),AT(108,28,64,12),USE(?MoveDown),LEFT,TIP('Move record down one'),ICON('VSMOVEDN.ICO')
                           BUTTON,AT(76,60,56,12),USE(?MoveEnd),HIDE,TIP('Move record to the bottom'),ICON('VSMOVEND.ICO')
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CallRapids',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'CallRapids',1)
    SolaceViewVars('MS::MoveType',MS::MoveType,'CallRapids',1)
    SolaceViewVars('MS::OutOfRange',MS::OutOfRange,'CallRapids',1)
    SolaceViewVars('MS::OrigSequence',MS::OrigSequence,'CallRapids',1)
    SolaceViewVars('MS::TargetSequence',MS::TargetSequence,'CallRapids',1)
    SolaceViewVars('MS::SwapSequence',MS::SwapSequence,'CallRapids',1)
    SolaceViewVars('MS::SavePosition',MS::SavePosition,'CallRapids',1)
    SolaceViewVars('MS::SaveSequence',MS::SaveSequence,'CallRapids',1)
    SolaceViewVars('MS::InsertSequence',MS::InsertSequence,'CallRapids',1)
    SolaceViewVars('MS::DeleteSequence',MS::DeleteSequence,'CallRapids',1)
    SolaceViewVars('MS::Section',MS::Section,'CallRapids',1)
    SolaceViewVars('MS::DoNotCommit',MS::DoNotCommit,'CallRapids',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveUp;  SolaceCtrlName = '?MoveUp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveBegin;  SolaceCtrlName = '?MoveBegin';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveDown;  SolaceCtrlName = '?MoveDown';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveEnd;  SolaceCtrlName = '?MoveEnd';Add(LSolCtrlQ,+SolaceUseRef)


BRW1::SequencedMoveUp ROUTINE
  MoveSeq:BRW1::MoveType=1
  DO BRW1::SequencedMove

BRW1::SequencedMoveDown ROUTINE
  MoveSeq:BRW1::MoveType=2                            ! "Up" to the user is "down" sequentially
  DO BRW1::SequencedMove

BRW1::SequencedMoveBegin ROUTINE
  MoveSeq:BRW1::MoveType=3
  DO BRW1::SequencedMove

BRW1::SequencedMoveEnd ROUTINE
  MoveSeq:BRW1::MoveType=4
  DO BRW1::SequencedMove

BRW1::SequencedMoveDragAndDrop ROUTINE
  IF MoveSeq:BRW1::DragSequence AND MoveSeq:BRW1::DropSequence
     MoveSeq:BRW1::MoveType=CHOOSE(MoveSeq:BRW1::DropSequence<MoveSeq:BRW1::DragSequence,7,8)
     DO BRW1::SequencedMove
  END
  CLEAR(MoveSeq:BRW1::DragSequence)
  CLEAR(MoveSeq:BRW1::DropSequence)

BRW1::SequencedMove ROUTINE
  !--------------------- Initialization
  MoveSeq:BRW1::OrigSequence=RAP:RecordNumber         ! Save original sequence
  !--------------------- End:  Initialization
  !--------------------- Test if we can move
  DO MoveSeq:BRW1::SetToOriginalRecord
  DO MoveSeq:BRW1::GetNextSequencedRecord             ! Try to get next record
  IF MoveSeq:BRW1::OutOfRange                         ! If nothing to swap with,
     DO MoveSeq:BRW1::CleanUp                         ! quietly exit
     EXIT                                             ! No movement possible
  END
  MoveSeq:BRW1::TargetSequence=RAP:RecordNumber       ! Save target sequence value

  !--------------------- End:  Test if we can move
  !--------------------- Move current record to a temporary location
  DO MoveSeq:BRW1::PrimeFieldsHigh
  SET(RAP:RecordNumberKey,RAP:RecordNumberKey)
  MoveSeq:BRW1::OutOfRange=Access:RAPIDLST.Previous() ! Find last in sequence
  MoveSeq:BRW1::SwapSequence=RAP:RecordNumber+1       ! Save the sequence number of the last in this order

  DO MoveSeq:BRW1::PrimeFieldsLow
  RAP:RecordNumber=MoveSeq:BRW1::OrigSequence         ! Restore original record
  MoveSeq:BRW1::OutOfRange=Access:RAPIDLST.Fetch(RAP:RecordNumberKey)

  RAP:RecordNumber=MoveSeq:BRW1::SwapSequence         ! Place the current record in the new location
  IF Access:RAPIDLST.Update()
     MoveSeq:BRW1::Section='Put Swap Out'
     IF DUPLICATE(RAPIDLST) AND NOT(DUPLICATE(RAP:RecordNumberKey))
        MoveSeq:BRW1::Section='Put Swap Out (NKD)'
     END
     DO MoveSeq:BRW1::ErrorCheck
     DO MoveSeq:BRW1::CleanUp                         ! quietly exit
     EXIT
  END
  !--------------------- End:  Move current record to a temporary location

  !--------------------- Move all other records
  DO MoveSeq:BRW1::SetToOriginalRecord
  LOOP WHILE NOT(MoveSeq:BRW1::OutOfRange) AND RAP:RecordNumber<>MoveSeq:BRW1::SwapSequence
     MoveSeq:BRW1::TargetSequence=RAP:RecordNumber    ! Save next target
     RAP:RecordNumber=MoveSeq:BRW1::OrigSequence      ! Change to previous record's location
     IF Access:RAPIDLST.Update()                      ! Update the file
        MoveSeq:BRW1::Section='Shift Stage'
        DO MoveSeq:BRW1::ErrorCheck
     END
     IF MoveSeq:BRW1::MoveType<3                      ! Move, or finish
        BREAK                                         ! Move once, then done
     ELSE
        DO MoveSeq:BRW1::GetNextSequencedRecord
     END
     MoveSeq:BRW1::OrigSequence=MoveSeq:BRW1::TargetSequence
  END
  !--------------------- End:  Move all other records

  !--------------------- Move original record into final position
  IF RAP:RecordNumber<>MoveSeq:BRW1::SwapSequence OR MoveSeq:BRW1::OutOfRange
     DO MoveSeq:BRW1::PrimeFieldsLow
     RAP:RecordNumber=MoveSeq:BRW1::SwapSequence      ! Restore original record
     MoveSeq:BRW1::OutOfRange=Access:RAPIDLST.Fetch(RAP:RecordNumberKey)
     IF MoveSeq:BRW1::OutOfRange
        MoveSeq:BRW1::Section='Prepare for Final Move'
        DO MoveSeq:BRW1::ErrorCheck
     END
  END

  RAP:RecordNumber=MoveSeq:BRW1::TargetSequence
  IF Access:RAPIDLST.Update()                         ! Update the file
     MoveSeq:BRW1::Section='Final Move'
     DO MoveSeq:BRW1::ErrorCheck
  END
  !--------------------- End:  Move original record into final position

  DO MoveSeq:BRW1::CleanUp                            ! Logout, flush, etc.

  DO MoveSeq:BRW1::SetToTargetRecord
  BRW1.ResetFromBuffer()
  SELECT(?Browse:1)


MoveSeq:BRW1::ErrorCheck ROUTINE
  IF ERRORCODE()
     MESSAGE('Record swap error: <13>' & ERROR() & '<13>RAPIDLST, RAP:RecordNumberKey' |
       & '<13>Key Field RAP:RecordNumber = ' & RAP:RecordNumber |
       & '<13>Section = ' & MoveSeq:BRW1::Section |
       & '<13>' ,'Error!')

  END

MoveSeq:BRW1::CleanUp ROUTINE

MoveSeq:BRW1::PrimeFieldsLow ROUTINE
   CLEAR(RAP:Record,-1)

MoveSeq:BRW1::PrimeFieldsHigh ROUTINE
   CLEAR(RAP:Record,1)

MoveSeq:BRW1::GetNextSequencedRecord ROUTINE
  MoveSeq:BRW1::OutOfRange=0
  EXECUTE (2 - MoveSeq:BRW1::MoveType % 2)
     MoveSeq:BRW1::OutOfRange=Access:RAPIDLST.TryPrevious()
     MoveSeq:BRW1::OutOfRange=Access:RAPIDLST.TryNext()
  END
  DO MoveSeq:BRW1::TestSequencedRange

  ! Extra drag and drop range check
  CASE MoveSeq:BRW1::MoveType
  OF 7
     IF RAP:RecordNumber<=MoveSeq:BRW1::DropSequence
        MoveSeq:BRW1::OutOfRange=1
     END
  OF 8
     IF RAP:RecordNumber>MoveSeq:BRW1::DropSequence
        MoveSeq:BRW1::OutOfRange=1
     END
  END

MoveSeq:BRW1::SetToOriginalRecord ROUTINE
  DO MoveSeq:BRW1::PrimeFieldsLow
  RAP:RecordNumber=MoveSeq:BRW1::OrigSequence         ! Start with the current record
  SET(RAP:RecordNumberKey,RAP:RecordNumberKey)
  DO MoveSeq:BRW1::GetNextSequencedRecord             ! Get current record

MoveSeq:BRW1::SetToTargetRecord ROUTINE
    DO MoveSeq:BRW1::PrimeFieldsLow
    RAP:RecordNumber=MoveSeq:BRW1::TargetSequence     ! Start with the current record
    SET(RAP:RecordNumberKey,RAP:RecordNumberKey)
    DO MoveSeq:BRW1::GetNextSequencedRecord           ! Get current record

MoveSeq:BRW1::TestSequencedRange ROUTINE
  !IF ERRORCODE()=33 |
  IF MoveSeq:BRW1::OutOfRange=1 |
     THEN
        MoveSeq:BRW1::OutOfRange=1
  END


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('CallRapids')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CallRapids')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='CallRapids'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:RAPIDLST.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:RAPIDLST,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = False
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,RAP:RecordNumberKey)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,RAP:RecordNumber,1,BRW1)
  BRW1.AddField(RAP:ProgramName,BRW1.Q.RAP:ProgramName)
  BRW1.AddField(RAP:EXEPath,BRW1.Q.RAP:EXEPath)
  BRW1.AddField(RAP:Description,BRW1.Q.RAP:Description)
  BRW1.AddField(RAP:RecordNumber,BRW1.Q.RAP:RecordNumber)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RAPIDLST.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='CallRapids'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CallRapids',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  DoUpdate# = 0
  If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
      If KeyCode() <> MouseLeft2
          DoUpdate# = 1
      End!If KeyCode() <> MouseLeft2
  End!If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
  If DoUpdate# = 0
      If Request = InsertRecord
          Access:RAPIDLST.CancelAutoInc()
      End!If Request = InsertRecord
  Else!If DoUpdate# = 0
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateRAPIDLST
    ReturnValue = GlobalResponse
  END
  End!If DoUpdate# = 0
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?MoveUp
      ThisWindow.Update
      DO BRW1::SequencedMoveUp
    OF ?MoveBegin
      ThisWindow.Update
      DO BRW1::SequencedMoveBegin
    OF ?MoveDown
      ThisWindow.Update
      DO BRW1::SequencedMoveDown
    OF ?MoveEnd
      ThisWindow.Update
      DO BRW1::SequencedMoveEnd
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CallRapids')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, PreAlertKey)
      If KeyCode() = MouseLeft2
          RUN(Clip(brw1.q.RAP:EXEPath) & ' %' & Clip(glo:Password))
      End!If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, PreAlertKey)
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !?browse:1{prop:items} = Records(RAPIDLST) + 1
      !?CurrentTab{prop:height} = ?Browse:1{prop:height} + 24
      !0{prop:height} = ?CurrentTab{prop:height} + 8
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.RAP:ProgramName_NormalFG = -1
  SELF.Q.RAP:ProgramName_NormalBG = -1
  SELF.Q.RAP:ProgramName_SelectedFG = -1
  SELF.Q.RAP:ProgramName_SelectedBG = -1
  SELF.Q.RAP:EXEPath_NormalFG = -1
  SELF.Q.RAP:EXEPath_NormalBG = -1
  SELF.Q.RAP:EXEPath_SelectedFG = -1
  SELF.Q.RAP:EXEPath_SelectedBG = -1
  SELF.Q.RAP:Description_NormalFG = -1
  SELF.Q.RAP:Description_NormalBG = -1
  SELF.Q.RAP:Description_SelectedFG = -1
  SELF.Q.RAP:Description_SelectedBG = -1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

