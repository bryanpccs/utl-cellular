

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01021.INC'),ONCE        !Local module procedure declarations
                     END


Colour_Key PROCEDURE                                  !Generated from procedure template - Window

JobStatusColours     GROUP,PRE(JSC)
Maroon               STRING(30)
Green                STRING(30)
Olive                STRING(30)
Navy                 STRING(30)
Purple               STRING(30)
Teal                 STRING(30)
Gray                 STRING(30)
Blue                 STRING(30)
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Colour Code Key'),AT(,,220,186),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,152),USE(?Sheet1),SPREAD
                         TAB('Standard'),USE(?Tab1)
                           PROMPT('O = Overdue Status:'),AT(8,28),USE(?Prompt1)
                           IMAGE('Block_Red_Right.ico'),AT(192,24),USE(?Image1:10)
                           PROMPT('F = Finished Repair:'),AT(8,48),USE(?Prompt1:6)
                           IMAGE('Block_Green.ico'),AT(84,44),USE(?Image1:9)
                           IMAGE('Block_Yellow_Left.ico'),AT(84,64),USE(?Image1:2)
                           PROMPT('C = Chargeable Repair:'),AT(8,68),USE(?Prompt1:2)
                           PROMPT('Invoiced:'),AT(116,68),USE(?Prompt3)
                           PROMPT('Overdue Job:'),AT(116,28),USE(?Prompt3:4)
                           IMAGE('Block_Yellow.ico'),AT(192,84),USE(?Image1:7)
                           PROMPT('Claim Raised:'),AT(116,88),USE(?Prompt3:2)
                           PROMPT('Warranty Reconciled:'),AT(116,128),USE(?Prompt3:3)
                           IMAGE('Block_Blue_Right.ico'),AT(192,124),USE(?Image1:8)
                           PROMPT('D = Despatched:'),AT(8,108),USE(?Prompt1:4)
                           PROMPT('P = Chargeable Paid'),AT(8,128),USE(?Prompt1:5)
                           IMAGE('Block_Yellow_Left.ico'),AT(84,84),USE(?Image1:4)
                           PROMPT('W = Warranty Repair:'),AT(8,88),USE(?Prompt1:3)
                           IMAGE('Block_Cyan.ico'),AT(84,104),USE(?Image1:5)
                           IMAGE('Block_Blue_Left.ico'),AT(84,124),USE(?Image1:6)
                           IMAGE('Block_Yellow.ico'),AT(192,64),USE(?Image1:3)
                           IMAGE('Block_Red_Left.ico'),AT(84,24),USE(?Image1)
                         END
                         TAB('Custom'),USE(?Tab2)
                           PANEL,AT(20,24,12,12),USE(?Panel2),FILL(COLOR:Maroon),BEVEL(-1)
                           STRING(@s30),AT(36,25),USE(JSC:Maroon),TRN
                           PANEL,AT(20,40,12,12),USE(?Panel3),FILL(COLOR:Green),BEVEL(-1)
                           STRING(@s30),AT(36,41),USE(JSC:Green),TRN
                           PANEL,AT(20,88,12,12),USE(?Panel9),FILL(COLOR:Purple),BEVEL(-1)
                           STRING(@s30),AT(36,89),USE(JSC:Purple),TRN
                           PANEL,AT(20,56,12,12),USE(?Panel4),FILL(COLOR:Olive),BEVEL(-1)
                           STRING(@s30),AT(36,57),USE(JSC:Olive),TRN
                           PANEL,AT(20,72,12,12),USE(?Panel5),FILL(COLOR:Navy),BEVEL(-1)
                           STRING(@s30),AT(36,73),USE(JSC:Navy),TRN
                           PANEL,AT(20,136,12,12),USE(?Panel8),FILL(COLOR:Blue),BEVEL(-1)
                           STRING(@s30),AT(36,137),USE(JSC:Blue),TRN
                           PANEL,AT(20,104,12,12),USE(?Panel6),FILL(COLOR:Teal),BEVEL(-1)
                           STRING(@s30),AT(36,105),USE(JSC:Teal),TRN
                           PANEL,AT(20,120,12,12),USE(?Panel7),FILL(COLOR:Gray),BEVEL(-1)
                           STRING(@s30),AT(36,121),USE(JSC:Gray),TRN
                         END
                       END
                       PANEL,AT(4,160,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(156,164,56,16),USE(?Close),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt1:6{prop:FontColor} = -1
    ?Prompt1:6{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt3:4{prop:FontColor} = -1
    ?Prompt3:4{prop:Color} = 15066597
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    ?Prompt3:3{prop:FontColor} = -1
    ?Prompt3:3{prop:Color} = 15066597
    ?Prompt1:4{prop:FontColor} = -1
    ?Prompt1:4{prop:Color} = 15066597
    ?Prompt1:5{prop:FontColor} = -1
    ?Prompt1:5{prop:Color} = 15066597
    ?Prompt1:3{prop:FontColor} = -1
    ?Prompt1:3{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Panel2{prop:Fill} = 15066597

    ?JSC:Maroon{prop:FontColor} = -1
    ?JSC:Maroon{prop:Color} = 15066597
    ?Panel3{prop:Fill} = 15066597

    ?JSC:Green{prop:FontColor} = -1
    ?JSC:Green{prop:Color} = 15066597
    ?Panel9{prop:Fill} = 15066597

    ?JSC:Purple{prop:FontColor} = -1
    ?JSC:Purple{prop:Color} = 15066597
    ?Panel4{prop:Fill} = 15066597

    ?JSC:Olive{prop:FontColor} = -1
    ?JSC:Olive{prop:Color} = 15066597
    ?Panel5{prop:Fill} = 15066597

    ?JSC:Navy{prop:FontColor} = -1
    ?JSC:Navy{prop:Color} = 15066597
    ?Panel8{prop:Fill} = 15066597

    ?JSC:Blue{prop:FontColor} = -1
    ?JSC:Blue{prop:Color} = 15066597
    ?Panel6{prop:Fill} = 15066597

    ?JSC:Teal{prop:FontColor} = -1
    ?JSC:Teal{prop:Color} = 15066597
    ?Panel7{prop:Fill} = 15066597

    ?JSC:Gray{prop:FontColor} = -1
    ?JSC:Gray{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Colour_Key',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('JobStatusColours:Maroon',JobStatusColours:Maroon,'Colour_Key',1)
    SolaceViewVars('JobStatusColours:Green',JobStatusColours:Green,'Colour_Key',1)
    SolaceViewVars('JobStatusColours:Olive',JobStatusColours:Olive,'Colour_Key',1)
    SolaceViewVars('JobStatusColours:Navy',JobStatusColours:Navy,'Colour_Key',1)
    SolaceViewVars('JobStatusColours:Purple',JobStatusColours:Purple,'Colour_Key',1)
    SolaceViewVars('JobStatusColours:Teal',JobStatusColours:Teal,'Colour_Key',1)
    SolaceViewVars('JobStatusColours:Gray',JobStatusColours:Gray,'Colour_Key',1)
    SolaceViewVars('JobStatusColours:Blue',JobStatusColours:Blue,'Colour_Key',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:10;  SolaceCtrlName = '?Image1:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:6;  SolaceCtrlName = '?Prompt1:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:9;  SolaceCtrlName = '?Image1:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:2;  SolaceCtrlName = '?Image1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:4;  SolaceCtrlName = '?Prompt3:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:7;  SolaceCtrlName = '?Image1:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:3;  SolaceCtrlName = '?Prompt3:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:8;  SolaceCtrlName = '?Image1:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:4;  SolaceCtrlName = '?Prompt1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:5;  SolaceCtrlName = '?Prompt1:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:4;  SolaceCtrlName = '?Image1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:3;  SolaceCtrlName = '?Prompt1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:5;  SolaceCtrlName = '?Image1:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:6;  SolaceCtrlName = '?Image1:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1:3;  SolaceCtrlName = '?Image1:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Maroon;  SolaceCtrlName = '?JSC:Maroon';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel3;  SolaceCtrlName = '?Panel3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Green;  SolaceCtrlName = '?JSC:Green';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel9;  SolaceCtrlName = '?Panel9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Purple;  SolaceCtrlName = '?JSC:Purple';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel4;  SolaceCtrlName = '?Panel4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Olive;  SolaceCtrlName = '?JSC:Olive';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel5;  SolaceCtrlName = '?Panel5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Navy;  SolaceCtrlName = '?JSC:Navy';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel8;  SolaceCtrlName = '?Panel8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Blue;  SolaceCtrlName = '?JSC:Blue';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel6;  SolaceCtrlName = '?Panel6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Teal;  SolaceCtrlName = '?JSC:Teal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel7;  SolaceCtrlName = '?Panel7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JSC:Gray;  SolaceCtrlName = '?JSC:Gray';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Colour_Key')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Colour_Key')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  ! Start Change 2618 BE(05/08/03)
  JSC:Maroon = GETINI('JobStatusColours','Maroon',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Green = GETINI('JobStatusColours','Green',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Olive = GETINI('JobStatusColours','Olive',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Navy = GETINI('JobStatusColours','Navy',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Purple = GETINI('JobStatusColours','Purple',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Teal = GETINI('JobStatusColours','Teal',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Gray = GETINI('JobStatusColours','Gray',,CLIP(PATH())&'\SB2KDEF.INI')
  JSC:Blue = GETINI('JobStatusColours','Blue',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2618 BE(05/08/03)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Colour_Key',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Colour_Key')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

