

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01028.INC'),ONCE        !Local module procedure declarations
                     END


Print_Proforma_Routines PROCEDURE (f_RefNumber)       !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
FilesOpened          BYTE
tmp:invoicedate      DATE
pos                  STRING(255)
save_job_id          USHORT,AUTO
tmp:VatRateLabour    REAL
tmp:VatRateParts     REAL
tmp:VatRateRetail    REAL
tmp:InvoiceVatNumber REAL
sav:path             STRING(255)
invoice_number_temp  REAL
report_type_temp     STRING(2)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Reports'),AT(,,155,119),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       PANEL,AT(4,4,148,84),USE(?Panel2),FILL(COLOR:Silver)
                       OPTION('Select A Report'),AT(8,8,140,76),USE(report_type_temp),BOXED,COLOR(COLOR:Silver)
                         RADIO('Query Report'),AT(36,20),USE(?report_type_temp:Radio1),VALUE('1')
                         RADIO('Create Proforma Invoice'),AT(36,36),USE(?report_type_temp:Radio4),VALUE('2')
                         RADIO('Invoice Current Batch'),AT(36,52),USE(?report_type_temp:Radio2),VALUE('3')
                         RADIO('Invoice All Batches'),AT(36,68),USE(?report_type_temp:Radio3),HIDE,VALUE('4')
                       END
                       PANEL,AT(4,92,148,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Close'),AT(92,96,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       BUTTON('Print'),AT(8,96,56,16),USE(?print),LEFT,ICON(ICON:Print1)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
WindowsDir      LPSTR(144)
SystemDir       WORD
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel2{prop:Fill} = 15066597

    ?report_type_temp{prop:Font,3} = -1
    ?report_type_temp{prop:Color} = 15066597
    ?report_type_temp{prop:Trn} = 0
    ?report_type_temp:Radio1{prop:Font,3} = -1
    ?report_type_temp:Radio1{prop:Color} = 15066597
    ?report_type_temp:Radio1{prop:Trn} = 0
    ?report_type_temp:Radio4{prop:Font,3} = -1
    ?report_type_temp:Radio4{prop:Color} = 15066597
    ?report_type_temp:Radio4{prop:Trn} = 0
    ?report_type_temp:Radio2{prop:Font,3} = -1
    ?report_type_temp:Radio2{prop:Color} = 15066597
    ?report_type_temp:Radio2{prop:Trn} = 0
    ?report_type_temp:Radio3{prop:Font,3} = -1
    ?report_type_temp:Radio3{prop:Color} = 15066597
    ?report_type_temp:Radio3{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
Create_Invoice      Routine
    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))
    Set(defaults)
    access:defaults.next()
    invoice_number_temp = 0
    Case MessageEx('Are you sure you want to print a Single Invoice?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            error# = 0
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = glo:select1
            if access:jobs.fetch(job:ref_number_key) = Level:Benign
                access:jobnotes.clearkey(jbn:RefNumberKey)
                jbn:RefNumber  = job:ref_number
                access:jobnotes.tryfetch(jbn:RefNumberKey)
                If job:bouncer <> ''
                    Case MessageEx('This job has been marked as a Bouncer.<13,10><13,10>You must authorise this job for Invoicing before you can continue.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                        Of 1 ! &Close Button
                    End!Case MessageEx
                    error# = 1
                End!If job:bouncer <> ''
                If job:invoice_number <> '' and error# = 0
                    access:invoice.clearkey(inv:invoice_number_key)
                    inv:invoice_number = job:invoice_number
                    if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
                        If inv:invoice_type = 'SIN'
                            glo:select1 = inv:invoice_number
                            Single_Invoice
                            glo:select1 = ''
                        Else!End!If inv:invoice_type = 'SIN'
                            Case MessageEx('The selected job is part of a Multiple Invoice, or is a Warranty job.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                Of 1 ! &Close Button
                            End!Case MessageEx
                        End!If inv:invoice_type = 'SIN'
                    end

                Else!If job:invoice_number <> ''
                    error# = 0
                    If job:chargeable_job <> 'YES'
                        Case MessageEx('You are attempting to Invoice a Warranty Only Job.<13,10><13,10>This can only be processed through the Warranty Procedures.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                            Of 1 ! &Close Button
                        End!Case MessageEx
                        error# = 1
                    End!If job:chargeable_job <> 'YES'
                    If job:date_completed <> '' And error# = 0
                        Set(defaults)
                        access:defaults.next()
                        If def:qa_required = 'YES'
                            If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
                                Case MessageEx('The selected job has not passed QA checks','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                error# = 1
                            End!If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
                        End!If def:qa_required = 'YES'
                    Else!If job:date_completed <> ''
                        Case MessageEx('The selected job has not been completed.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        error# = 1
                    End!If job:date_completed <> ''
                    If job:ignore_chargeable_charges = 'YES' And error# = 0
                        If job:sub_total = 0
                            Case MessageEx('You are attempting to invoice a job than has not been priced.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            error# = 1
                        End
                    Else!If job:ignore_chargeable_charges = 'YES'
                        If error# = 0
                            Pricing_Routine('C',labour",parts",pass",a")
                            If pass" = False
                                Case MessageEx('You are attempting to Invoice a job for which a Default Charge Structure has not been established.<13,10><13,10>If you wish to proceed you must access the Chargeable Tab and tick the "Ignore Default Charge" option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                error# = 1
                            Else!If pass" = False
                                job:labour_cost = labour"
                                job:parts_cost  = parts"
                                job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
                            End!If pass" = False
                        End!If error# = 0
                    End!If job:ignore_chargeable_charges = 'YES'
                    If error# = 0
                        fetch_error# = 0
                        despatch# = 0
                        access:subtracc.clearkey(sub:account_number_key)
                        sub:account_number = job:account_number
                        if access:subtracc.fetch(sub:account_number_key)
                            Case MessageEx('Error! Cannot find the Sub Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                        Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
                            access:tradeacc.clearkey(tra:account_number_key) 
                            tra:account_number = sub:main_account_number
                            if access:tradeacc.fetch(tra:account_number_key)
                                Case MessageEx('Error! Cannot find the Trade Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                fetch_error# = 1
                            Else!if access:tradeacc.fetch(tra:account_number_key)
                                If tra:use_sub_accounts = 'YES'
                                    If sub:despatch_invoiced_jobs = 'YES'
                                        If sub:despatch_paid_jobs = 'YES'
                                            If job:paid = 'YES'
                                                despatch# = 1
                                            Else
                                                despatch# = 2
                                            End
                                        Else
                                            despatch# = 1
                                        End!If sub:despatch_paid_jobs = 'YES' And job:paid = 'YES'
                                        
                                    End!If sub:despatch_invoiced_jobs = 'YES'
                                End!If tra:use_sub_accounts = 'YES'
                                if tra:invoice_sub_accounts = 'YES'
                                    vat_number" = sub:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = sub:labour_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                        fetch_error# = 1
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       labour_rate$ = vat:vat_rate
                                    end!if access:vatcode.fetch(vat:vat_code_key)
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = sub:parts_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                            Case MessageEx('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                                Of 1 ! &OK Button
                                            End!Case MessageEx
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       parts_rate$ = vat:vat_rate
                                    end!if access:vatcode.fetch(vat:vat_code_key)
                                else!if tra:use_sub_accounts = 'YES'
                                    If tra:despatch_invoiced_jobs = 'YES'
                                        If tra:despatch_paid_jobs = 'YES'
                                            If job:paid = 'YES'
                                                despatch# = 1
                                            Else
                                                despatch# = 2
                                            End!If job:paid = 'YES'
                                        Else
                                            despatch# = 1
                                        End!If tra:despatch_paid_jobs = 'YES' and job:paid = 'YES'
                                    End!If tra:despatch_invoiced_jobs = 'YES'
                                    vat_number" = tra:vat_number
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = tra:labour_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       labour_rate$ = vat:vat_rate
                                    end
                                    access:vatcode.clearkey(vat:vat_code_key)
                                    vat:vat_code = tra:parts_vat_code
                                    if access:vatcode.fetch(vat:vat_code_key)
                                        fetch_error# = 1
                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                            Of 1 ! &OK Button
                                        End!Case MessageEx
                                    Else!if access:vatcode.fetch(vat:vat_code_key)
                                       parts_rate$ = vat:vat_rate
                                    end
                                end!if tra:use_sub_accounts = 'YES'
                            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                        end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                        If fetch_error# = 0
        !SAGE BIT
                            If def:use_sage = 'YES'
                                glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
                                Remove(paramss)
                                access:paramss.open()
                                access:paramss.usefile()
             !LABOUR
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Labour_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Labour_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:labour_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
            !PARTS
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'
                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Parts_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:parts_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:parts_cost * (parts_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
            !COURIER
                                get(paramss,0)
                                if access:paramss.primerecord() = Level:Benign
                                    prm:account_ref       = Stripcomma(job:Account_number)
                                    If tra:invoice_sub_accounts = 'YES'
                                        If sub:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(sub:company_name)
                                            prm:address_1         = Stripcomma(sub:address_line1)
                                            prm:address_2         = Stripcomma(sub:address_line2)
                                            prm:address_3         = Stripcomma(sub:address_line3)
                                            prm:address_5         = Stripcomma(sub:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)

                                        End!If tra:invoice_customer_address = 'YES'
                                    Else!If tra:invoice_sub_accounts = 'YES'
                                        If tra:invoice_customer_address <> 'YES'
                                            prm:name              = Stripcomma(tra:company_name)
                                            prm:address_1         = Stripcomma(tra:address_line1)
                                            prm:address_2         = Stripcomma(tra:address_line2)
                                            prm:address_3         = Stripcomma(tra:address_line3)
                                            prm:address_5         = Stripcomma(tra:postcode)
                                        Else!If tra:invoice_customer_address = 'YES'
                                            prm:name              = Stripcomma(job:company_name)
                                            prm:address_1         = Stripcomma(job:address_line1)
                                            prm:address_2         = Stripcomma(job:address_line2)
                                            prm:address_3         = Stripcomma(job:address_line3)
                                            prm:address_5         = Stripcomma(job:postcode)
                                        End!If tra:invoice_customer_address = 'YES'
                                    End!If tra:invoice_sub_accounts = 'YES'

                                    prm:address_4         = ''
                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
                                    prm:del_address_4     = ''
                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
                                    If job:surname <> ''
                                        if job:title = '' and job:initial = '' 
                                            prm:contact_name = Stripcomma(clip(job:surname))
                                        elsif job:title = '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
                                        elsif job:title <> '' and job:initial = ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
                                        elsif job:title <> '' and job:initial <> ''
                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
                                        else
                                            prm:contact_name = ''
                                        end
                                    else
                                        prm:contact_name = ''
                                    end
                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
                                    prm:notes_3           = ''
                                    prm:taken_by          = Stripcomma(job:who_booked)
                                    prm:order_number      = Stripcomma(job:order_number)
                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
                                    prm:payment_ref       = ''
                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                                    prm:global_details    = ''
                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
                                                                Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
                                    prm:description       = Stripcomma(DEF:Courier_Description)
                                    prm:nominal_code      = Stripcomma(DEF:Courier_Code)
                                    prm:qty_order         = '1'
                                    prm:unit_price        = '0'
                                    prm:net_amount        = Stripcomma(job:courier_cost)
                                    prm:tax_amount        = Stripcomma(Round(job:courier_cost * (labour_rate$/100),.01))
                                    prm:comment_1         = Stripcomma(job:charge_type)
                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
                                    prm:unit_of_sale      = '0'
                                    prm:full_net_amount   = '0'
                                    prm:invoice_date      = '*'
                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
                                    prm:user_name         = Clip(DEF:User_Name_Sage)
                                    prm:password          = Clip(DEF:Password_Sage)
                                    prm:set_invoice_number= '*'
                                    prm:invoice_no        = '*'
                                    access:paramss.insert()
                                end!if access:paramss.primerecord() = Level:Benign
                                access:paramss.close()
                                Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
                                sage_error# = 0
                                access:paramss.open()
                                access:paramss.usefile()
                                Set(paramss,0)
                                If access:paramss.next()
                                    sage_error# = 1
                                Else!If access:paramss.next()
                                    If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                                        sage_error# = 1
                                    Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                                        invoice_number_temp = prm:invoice_no
                                        If invoice_number_temp = 0
                                            sage_error# = 1
                                        End!If invoice_number_temp = 0
                                    End!If prm:invoice_no = '-1'
                                End!If access:paramss.next()
                                access:paramss.close()
                            End!If def:use_sage = 'YES'
                            If sage_error# = 0
                                invoice_error# = 1
                                get(invoice,0)
                                if access:invoice.primerecord() = level:benign
                                    If def:use_sage = 'YES'
                                        inv:invoice_number = invoice_number_temp
                                    End!If def:use_sage = 'YES'
                                    inv:invoice_type       = 'SIN'
                                    inv:job_number         = job:ref_number
                                    inv:date_created       = Today()
                                    inv:account_number     = job:account_number
                                    inv:total              = job:sub_total
                                    inv:vat_rate_labour    = labour_rate$
                                    inv:vat_rate_parts     = parts_rate$
                                    inv:vat_rate_retail    = labour_rate$
                                    inv:vat_number         = def:vat_number
                                    INV:Courier_Paid       = job:courier_cost
                                    inv:parts_paid         = job:parts_cost
                                    inv:labour_paid        = job:labour_cost
                                    inv:invoice_vat_number = vat_number"
                                    invoice_error# = 0
                                    If access:invoice.insert()
                                        invoice_error# = 1
                                        access:invoice.cancelautoinc()
                                    End!If access:invoice.insert()
                                end!if access:invoice.primerecord() = level:benign
                                If Invoice_error# = 0
                                    Case despatch#
                                        Of 1
                                            If job:despatched = 'YES'
                                                If job:paid = 'YES'
                                                    GetStatus(910,0,'JOB') !despatch Paid
                                                Else!If job:paid = 'YES'
                                                    GetStatus(905,0,'JOB') !Despatch Unpaid
                                                End!If job:paid = 'YES'
                                            Else!If job:despatched = 'YES'
                                                GetStatus(810,0,'JOB') !Ready To Despatch
                                                job:despatched = 'REA'
                                                job:despatch_type = 'JOB'
                                                job:current_courier = job:courier
                                            End!If job:despatched <> 'YES'
                                        Of 2
                                            GetStatus(803,0,'JOB') !Ready To Despatch

                                    End!Case despatch#

                                    job:invoice_number        = inv:invoice_number
                                    job:invoice_date          = Today()
                                    job:invoice_courier_cost  = job:courier_cost
                                    job:invoice_labour_cost   = job:labour_cost
                                    job:invoice_parts_cost    = job:parts_cost
                                    job:invoice_sub_total     = job:sub_total
                                    access:jobs.update()
                                    get(audit,0)
                                    if access:audit.primerecord() = level:benign
                                        aud:notes         = 'INVOICE NUMBER: ' & CLip(inv:invoice_number) 
                                        aud:ref_number    = job:ref_number
                                        aud:date          = today()
                                        aud:time          = clock()
                                        aud:type          = 'JOB'
                                        access:users.clearkey(use:password_key)
                                        use:password = glo:password
                                        access:users.fetch(use:password_key)
                                        aud:user = use:user_code
                                        aud:action        = 'INVOICE CREATED'
                                        access:audit.insert()
                                    end!�if access:audit.primerecord() = level:benign

                                    
                                    glo:select1 = inv:invoice_number
                                    Single_Invoice
                                    glo:select1 = ''
                                End!If Invoice_error# = 0
                            End!If def:use_sage = 'YES' and sage_error# = 1
                        End!If fetch_error# = 0
                    End!If error# = 0
                End!If job:invoice_number <> ''
            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign

        Of 2 ! &No Button
    End!Case MessageEx


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Print_Proforma_Routines',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Print_Proforma_Routines',1)
    SolaceViewVars('tmp:invoicedate',tmp:invoicedate,'Print_Proforma_Routines',1)
    SolaceViewVars('pos',pos,'Print_Proforma_Routines',1)
    SolaceViewVars('save_job_id',save_job_id,'Print_Proforma_Routines',1)
    SolaceViewVars('tmp:VatRateLabour',tmp:VatRateLabour,'Print_Proforma_Routines',1)
    SolaceViewVars('tmp:VatRateParts',tmp:VatRateParts,'Print_Proforma_Routines',1)
    SolaceViewVars('tmp:VatRateRetail',tmp:VatRateRetail,'Print_Proforma_Routines',1)
    SolaceViewVars('tmp:InvoiceVatNumber',tmp:InvoiceVatNumber,'Print_Proforma_Routines',1)
    SolaceViewVars('sav:path',sav:path,'Print_Proforma_Routines',1)
    SolaceViewVars('invoice_number_temp',invoice_number_temp,'Print_Proforma_Routines',1)
    SolaceViewVars('report_type_temp',report_type_temp,'Print_Proforma_Routines',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp;  SolaceCtrlName = '?report_type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio1;  SolaceCtrlName = '?report_type_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio4;  SolaceCtrlName = '?report_type_temp:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio2;  SolaceCtrlName = '?report_type_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio3;  SolaceCtrlName = '?report_type_temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?print;  SolaceCtrlName = '?print';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Proforma_Routines')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Print_Proforma_Routines')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:LETTERS.Open
  Relate:PROINV.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:CHARTYPE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:JOBNOTES.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:LETTERS.Close
    Relate:PROINV.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Print_Proforma_Routines',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print, Accepted)
      access:proinv.clearkey(prv:refnumberkey)
      prv:refnumber = f_RefNumber
      if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
          Case report_type_temp
              Of 1 !Query Report
                  glo:select1  = prv:refnumber
                  Chargeable_Query_Report
                  glo:select1  = ''
              Of 2 !Proforma Invoice
                  glo:select1  = prv:refnumber
                  Chargeable_Proforma_Invoice
                  glo:select1  = ''
              Of 3 !Invoice Batch
                  noprint# = 0
                  Case MessageEx('Do you wish to Print any Invoices created at this time?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          noprint# = 1
                  End!Case MessageEx
      
                  tmp:invoicedate = SelectInvoiceDate()
      
                  if tmp:invoicedate <> 0
      
                      recordspercycle     = 25
                      recordsprocessed    = 0
                      percentprogress     = 0
                      setcursor(cursor:wait)
                      open(progresswindow)
                      progress:thermometer    = 0
                      ?progress:pcttext{prop:text} = '0% Completed'
      
                      recordstoprocess    = Records(Jobs)
                      count_jobs# = 0
                      ?Progress:UserString{prop:text} = 'Checking Jobs..'
                      ?Progress:PctText{prop:text} = ''
      
                      error# = 0
                      date_error# = 0
                      save_job_id = access:jobs.savefile()
                      access:jobs.clearkey(job:batchjobkey)
                      job:invoiceaccount = prv:AccountNumber
                      job:invoicebatch   = prv:BatchNumber
                      set(job:batchjobkey,job:batchjobkey)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          if job:invoiceaccount <> prv:AccountNumber      |
                          or job:invoicebatch   <> prv:BatchNumber      |
                              then break.  ! end if
      
                          count_jobs# += 1
                          If job:InvoiceStatus <> 'AUT'
                              error# += 1
                              ?Progress:PctText{prop:text} = 'Errors: ' & error#
                          End!If job:InvoiceStatus <> 'AUT'
                          Do GetNextRecord2
                      end !loop
                      access:jobs.restorefile(save_job_id)
      
                      setcursor()
                      close(progresswindow)
      
                      If count_jobs# = 0
                          Case MessageEx('There are no jobs to invoice.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!If count_jobs# = 0
                          IF error# <> 0
                              Case MessageEx('Cannot Invoice! <13,10><13,10>One or more of the jobs in this batch have not been Authorised.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          Else!IF error# = 1
      
                              use_sub# = 0
                              inv_Type# = 0
                              Case prv:AccountType
                                  Of 'MAI'
                                      access:tradeacc.clearkey(tra:account_number_key)
                                      tra:account_number = prv:AccountNumber
                                      if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                                          access:vatcode.clearkey(vat:vat_code_key)
                                          vat:vat_code = tra:labour_vat_code
                                          if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                                              tmp:vatratelabour = vat:vat_rate
                                          end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                                          access:vatcode.clearkey(vat:vat_code_key)
                                          vat:vat_code = tra:parts_vat_code
                                          if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                                              tmp:vatrateparts = vat:vat_rate
                                          end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                                          Case tra:MultiInvoice
                                              Of 'SIN'
                                                  inv_type# = 1
                                              Of 'SIM'
                                                  inv_type# = 2
                                              Of 'MUL'
                                                  inv_type# = 3
                                              Of 'BAT'
                                                  inv_type# = 4
                                              Else
                                                  If maininvoice# = 0
                                                      Case MessageEx('You have not selected an ''Invoice Type'' for this Main Trade Account: '&Clip(tra:account_number)&'.','ServiceBase 2000',|
                                                                     'Styles\stop.ico','|&OK',1,1,'Skip Further Errors',maininvoice#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                          Of 1 ! &OK Button
                                                      End!Case MessageEx
                                                  End!If maininvoice# = 0
                                          End!Case sub:MultiInvoice
                                      end!if access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                                  Of 'SUB'
                                      access:subtracc.clearkey(sub:account_number_key)
                                      sub:account_number = prv:accountnumber
                                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                                          access:vatcode.clearkey(vat:vat_code_key)
                                          vat:vat_code = sub:labour_vat_code
                                          if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                                              tmp:vatratelabour = vat:vat_rate
                                          end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                                          access:vatcode.clearkey(vat:vat_code_key)
                                          vat:vat_code = sub:parts_vat_code
                                          if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
                                              tmp:vatrateparts = vat:vat_rate
                                          end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
      
                                          access:tradeacc.clearkey(tra:account_number_key)
                                          tra:account_number = sub:main_account_number
                                          if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                              Case sub:MultiInvoice
                                                  Of 'SIN'
                                                      inv_type# = 1
                                                  Of 'SIM'
                                                      inv_type# = 2
                                                  Of 'MUL'
                                                      inv_type# = 3
                                                  Of 'BAT'
                                                      inv_type# = 4
                                                  Else
                                                      If subinvoice# = 0
                                                          Case MessageEx('You have not selected an ''Invoice Type'' for this Sub Trade Account: '&Clip(sub:account_number)&'.','ServiceBase 2000',|
                                                                         'Styles\stop.ico','|&OK',1,1,'Skip Further Errors',maininvoice#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                              Of 1 ! &OK Button
                                                          End!Case MessageEx
                                                      End!If subinvoice# = 0
                                              End!Case sub:MultiInvoice
                                          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
                              End!Case prv:AccountType
      
                              Case inv_type#
                                  OF 1 Orof 2 !Single Invoice
                                      Clear(glo:queue3)
                                      Free(glo:Queue3)
                                      save_job_id = access:jobs.savefile()
                                      access:jobs.clearkey(job:batchjobkey)
                                      job:invoiceaccount = prv:accountnumber
                                      job:invoicebatch   = prv:batchNumber
                                      set(job:batchjobkey,job:batchjobkey)
                                      loop
                                          if access:jobs.next()
                                             break
                                          end !if
                                          if job:invoiceaccount <> prv:accountnumber      |
                                          or job:invoicebatch   <> prv:batchnumber      |
                                              then break.  ! end if
                                          If job:invoice_number <> ''
                                              Cycle
                                          End!If job:invoice_number <> ''
                                          If job:ignore_chargeable_charges = 'YES'
                                              If job:sub_total = 0
                                                  error# = 1
                                              End!If job:sub_total = 0
                                          Else!If job:ignore_chargeable_charges = 'YES'
                                              Pricing_Routine('C',labour",parts",pass",a")
                                              If pass" = False
                                                  error# = 1
                                              Else!If pass" = False
                                                  job:labour_cost = labour"
                                                  job:parts_cost  = parts"
                                                  job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
                                              End!If pass" = False
                                          End!If job:ignore_chargeable_charges = 'YES'
      
                                          If error# = 1
                                              Case MessageEx('Unable to create invoice for Job Number: '&Clip(job:ref_number)&'. <13,10><13,10>If has been removed from the batch and placed in the Invoice Exceptions List.','ServiceBase 2000',|
                                                             'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',errortext#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &OK Button
                                              End!Case MessageEx
                                          Else!If error# = 1
      
                                              get(invoice,0)
                                              if access:invoice.primerecord() = Level:Benign
                                                  inv:invoice_type       = 'SIN'
                                                  inv:job_number         = job:ref_number
                                                  inv:date_created       = tmp:invoicedate
                                                  inv:account_number     = prv:accountnumber
                                                  inv:AccountType        = prv:AccountType
                                                  inv:total              = job:sub_total
                                                  inv:vat_rate_labour    = tmp:VatRateLabour
                                                  inv:vat_rate_parts     = tmp:VatRateParts
                                                  inv:vat_rate_retail    = tmp:VatRateRetail
                                                  inv:vat_number         = Def:Vat_Number
                                                  inv:invoice_vat_number = tmp:InvoiceVatNumber
                                                  inv:batch_number       = prv:BatchNumber
                                                  inv:courier_paid       = job:Courier_Cost
                                                  inv:labour_paid        = job:Labour_Cost
                                                  inv:parts_paid         = job:Parts_Cost
                                                  inv:jobs_count         = 1
                                                  if access:invoice.insert()
                                                      access:invoice.cancelautoinc()
                                                  end
                                                  job:invoice_number  = inv:invoice_number
                                                  job:invoice_Date    = Today()
                                                  job:Invoice_Courier_Cost    = job:courier_Cost
                                                  job:invoice_labour_cost     = job:labour_cost
                                                  job:invoice_parts_cost      = job:parts_cost
                                                  job:invoice_sub_total       = job:sub_total
                                                  access:jobs.update()
                                                  get(audit,0)
                                                  if access:audit.primerecord() = level:benign
                                                      aud:notes         = 'INVOICE NUMBER: ' & inv:invoice_number
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = today()
                                                      aud:time          = clock()
                                                      aud:type          = 'JOB'
                                                      access:users.clearkey(use:password_key)
                                                      use:password = glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      aud:action        = 'INVOICE CREATED'
                                                      access:audit.insert()
                                                  end!�if access:audit.primerecord() = level:benign
      
      
                                                  glo:pointer3    = inv:invoice_number
                                                  Add(glo:queue3)
                                                  If noprint# <> 1
                                                      glo:select1  = inv:invoice_number
                                                      Single_Invoice
                                                      glo:select1  = ''
                                                  End!If noprint# <> 1
                                              End!if access:invoice.primerecord() = Level:Benign
      
                                          End!If error# = 0
      
                                      end !loop
                                      access:jobs.restorefile(save_job_id)
      
      !                                If noprint# <> 1
                                      Case MessageEx('Do you wish to print the Invoice Summary?','ServiceBase 2000',|
                                                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                          Of 1 ! &Yes Button
                                              If inv_type# = 2
                                                  Single_Invoice_Summary
                                                  Clear(glo:queue3)
                                                  Free(glo:Queue3)
                                              End!Case inv_type#
      
                                          Of 2 ! &No Button
                                      End!Case MessageEx
      !                                End!If noprint# <> 1
      
                                      prv:invoiced = 'YES'
                                      prv:status = ''
                                      access:proinv.update()
                                      Case MessageEx('Invoice Creation Completed.','ServiceBase 2000',|
                                                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                  Of 3 Orof 4
                                      count# = 0
      
                                      get(invoice,0)
                                      If access:invoice.primerecord() = Level:Benign
                                          If access:invoice.insert() = Level:Benign
                                              save_job_id = access:jobs.savefile()
                                              access:jobs.clearkey(job:batchjobkey)
                                              job:invoiceaccount = prv:accountnumber
                                              job:invoicebatch   = prv:batchNumber
                                              set(job:batchjobkey,job:batchjobkey)
                                              loop
                                                  if access:jobs.next()
                                                     break
                                                  end !if
                                                  if job:invoiceaccount <> prv:accountnumber      |
                                                  or job:invoicebatch   <> prv:batchnumber      |
                                                      then break.  ! end if
                                                  If job:invoice_Number <> ''
                                                      Cycle
                                                  End!If job:invoice_Number <> ''
                                                  If job:ignore_chargeable_charges = 'YES'
                                                      If job:sub_total = 0
                                                          error# = 1
                                                      End!If job:sub_total = 0
                                                  Else!If job:ignore_chargeable_charges = 'YES'
                                                      Pricing_Routine('C',labour",parts",pass",a")
                                                      If pass" = False
                                                          error# = 1
                                                      Else!If pass" = False
                                                          job:labour_cost = labour"
                                                          job:parts_cost  = parts"
                                                          job:sub_total   = job:labour_cost + job:parts_cost + job:courier_cost
                                                      End!If pass" = False
                                                  End!If job:ignore_chargeable_charges = 'YES'
      
                                                  If error# = 1
                                                      Case MessageEx('Unable to create invoice for Job Number: '&Clip(job:ref_number)&'. <13,10><13,10>If has been removed from the batch and placed in the Invoice Exceptions List.','ServiceBase 2000',|
                                                                     'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',errortext#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                          Of 1 ! &OK Button
                                                      End!Case MessageEx
                                                  Else!If error# = 1
                                                      job:invoice_number  = inv:invoice_number
                                                      job:invoice_Date    = tmp:invoicedate
                                                      job:Invoice_Courier_Cost    = job:courier_Cost
                                                      job:invoice_labour_cost     = job:labour_cost
                                                      job:invoice_parts_cost      = job:parts_cost
                                                      job:invoice_sub_total       = job:sub_total
                                                      inv:courier_paid    += job:courier_cost
                                                      inv:labour_paid     += job:labour_cost
                                                      inv:parts_paid      += job:parts_cost
                                                      inv:total           += job:sub_total
                                                      count# += 1
                                                      access:jobs.update()
                                                      get(audit,0)
                                                      if access:audit.primerecord() = level:benign
                                                          aud:notes         = 'INVOICE NUMBER: '  & inv:invoice_number
                                                          aud:ref_number    = job:ref_number
                                                          aud:date          = today()
                                                          aud:time          = clock()
                                                          aud:type          = 'JOB'
                                                          access:users.clearkey(use:password_key)
                                                          use:password = glo:password
                                                          access:users.fetch(use:password_key)
                                                          aud:user = use:user_code
                                                          aud:action        = 'INVOICE CREATED'
                                                          access:audit.insert()
                                                      end!�if access:audit.primerecord() = level:benign
                                                  End!If error# = 1
                                              End!Loop
                                              Case inv_type#
                                                  Of 3
                                                      inv:invoice_Type    = 'CHA'
                                                  Of 4
                                                      inv:invoice_Type    = 'BAT'
                                              End!Case inv_type#
                                              inv:job_number         = job:ref_number
                                              inv:date_created       = tmp:invoicedate
                                              inv:account_number     = prv:accountnumber
                                              inv:AccountType        = prv:AccountType
                                              inv:vat_rate_labour    = tmp:VatRateLabour
                                              inv:vat_rate_parts     = tmp:VatRateParts
                                              inv:vat_rate_retail    = tmp:VatRateRetail
                                              inv:vat_number         = Def:Vat_Number
                                              inv:invoice_vat_number = tmp:InvoiceVatNumber
                                              inv:batch_number       = prv:BatchNumber
                                              inv:jobs_count         = count#
                                              access:invoice.update()
                                          End!If access:invoice.insert() = Level:Benign
                                          prv:invoiced = 'YES'
                                          prv:status = ''
                                          access:proinv.update()
                                          If noprint# <> 1
                                              glo:select1   = inv:invoice_number
                                              Case inv_type#
                                                  Of 3
                                                      Chargeable_Summary_Invoice
                                                  Of 4
                                                      Chargeable_Batch_Invoice
                                              End!Case inv_type#
                                              glo:select1  = ''
                                          End!If noprint# <> 1
                                          Case MessageEx('Invoice Creation Completed.','ServiceBase 2000',|
                                                         'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                              Of 1 ! &OK Button
                                          End!Case MessageEx
                                      End!If access:invoice.primerecord() = Level:Benign
      
                              End!Case inv_type#
                          End!IF error# = 1
                      End!If count_jobs# = 0
      
                  End!if tmp:invoicedate <> 0
              Of 4 !Invoice All Batches
      
          End!Case report_type_temp
      end!if access:proinv.tryfetch(prv:refnumberkey) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Print_Proforma_Routines')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

