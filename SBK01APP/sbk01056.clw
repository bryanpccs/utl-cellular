

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01056.INC'),ONCE        !Local module procedure declarations
                     END


DespatchProcess PROCEDURE (func:JobNumber)            !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_muld_id         USHORT,AUTO
save_mulj_id         USHORT,AUTO
save_cou_ali_id      USHORT,AUTO
save_job_ali_id      USHORT,AUTO
save_jac_id          USHORT,AUTO
save_lac_id          USHORT,AUTO
tmp:Close            BYTE(0)
tmp:BatchAccountNumber STRING(30)
tmp:BatchCourier     STRING(30)
tmp:NumberInBatch    LONG
tmp:BatchAccountName STRING(30)
tmp:ExistingBatchNumber LONG
tmp:ConsignNo        STRING(30)
tmp:LabelError       STRING(30)
sav:path             STRING(255)
Account_Number2_Temp STRING(30)
tmp:AccountNumber    STRING(30)
tmp:OldConsignNo     STRING(30)
tmp:ParcelLineName   STRING(255),STATIC
tmp:WorkStationName  STRING(255)
tmp:BatchNumber      LONG
tmp:PrintSummaryNote BYTE(0)
Despatch_Label_Type_Temp STRING(3)
Courier_Temp         STRING(30)
tmp:DoDespatch       BYTE(0)
tmp:PassedConsignNo  STRING(30)
local:FileName       STRING(255),STATIC
tmp:ReturnedBatchNumber LONG
window               WINDOW('Despatch Process'),AT(,,367,187),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,360,152),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           GROUP('Batch Details'),AT(224,8,136,144),USE(?Group1),BOXED
                             PROMPT('Account Number'),AT(229,24),USE(?Prompt1)
                             STRING(@s30),AT(228,38),USE(tmp:BatchAccountNumber),FONT(,,,FONT:bold)
                             PROMPT('Account Name'),AT(228,52),USE(?Prompt2)
                             STRING(@s30),AT(228,66),USE(tmp:BatchAccountName),FONT(,,,FONT:bold)
                             PROMPT('Courier'),AT(229,84),USE(?Prompt3)
                             STRING(@s30),AT(228,98),USE(tmp:BatchCourier),FONT(,,,FONT:bold)
                             PROMPT('Units in Batch'),AT(228,116),USE(?Prompt4)
                             STRING(@s3),AT(228,128),USE(tmp:NumberInBatch),FONT(,20,,FONT:bold)
                           END
                           PROMPT('StatusText'),AT(8,16,212,16),USE(?StatusText),CENTER,FONT(,14,,FONT:bold)
                           BUTTON('Add Unit To Existing Batch [F6]'),AT(48,44,136,24),USE(?AddUnit),LEFT,ICON('proc_in.gif')
                           BUTTON('Create New Batch [F7]'),AT(48,80,136,24),USE(?CreateNewBatch),LEFT,ICON('CLIPBRD.GIF')
                           BUTTON('Force Individual Despatch [F8]'),AT(48,116,136,24),USE(?ForceIndividualDespatch),LEFT,ICON('COURIER.GIF')
                         END
                       END
                       PANEL,AT(4,160,360,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(304,164,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
    Map
LocalValidateAccessories    Procedure(),Byte
LocalValidateIMEI           Procedure(),Byte
LocalValidateAccountNumber  Procedure(),Byte
    End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ReturnedBatchNumber)

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:BatchAccountNumber{prop:FontColor} = -1
    ?tmp:BatchAccountNumber{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?tmp:BatchAccountName{prop:FontColor} = -1
    ?tmp:BatchAccountName{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?tmp:BatchCourier{prop:FontColor} = -1
    ?tmp:BatchCourier{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?tmp:NumberInBatch{prop:FontColor} = -1
    ?tmp:NumberInBatch{prop:Color} = 15066597
    ?StatusText{prop:FontColor} = -1
    ?StatusText{prop:Color} = 15066597
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'DespatchProcess',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_muld_id',save_muld_id,'DespatchProcess',1)
    SolaceViewVars('save_mulj_id',save_mulj_id,'DespatchProcess',1)
    SolaceViewVars('save_cou_ali_id',save_cou_ali_id,'DespatchProcess',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'DespatchProcess',1)
    SolaceViewVars('save_jac_id',save_jac_id,'DespatchProcess',1)
    SolaceViewVars('save_lac_id',save_lac_id,'DespatchProcess',1)
    SolaceViewVars('tmp:Close',tmp:Close,'DespatchProcess',1)
    SolaceViewVars('tmp:BatchAccountNumber',tmp:BatchAccountNumber,'DespatchProcess',1)
    SolaceViewVars('tmp:BatchCourier',tmp:BatchCourier,'DespatchProcess',1)
    SolaceViewVars('tmp:NumberInBatch',tmp:NumberInBatch,'DespatchProcess',1)
    SolaceViewVars('tmp:BatchAccountName',tmp:BatchAccountName,'DespatchProcess',1)
    SolaceViewVars('tmp:ExistingBatchNumber',tmp:ExistingBatchNumber,'DespatchProcess',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'DespatchProcess',1)
    SolaceViewVars('tmp:LabelError',tmp:LabelError,'DespatchProcess',1)
    SolaceViewVars('sav:path',sav:path,'DespatchProcess',1)
    SolaceViewVars('Account_Number2_Temp',Account_Number2_Temp,'DespatchProcess',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'DespatchProcess',1)
    SolaceViewVars('tmp:OldConsignNo',tmp:OldConsignNo,'DespatchProcess',1)
    SolaceViewVars('tmp:ParcelLineName',tmp:ParcelLineName,'DespatchProcess',1)
    SolaceViewVars('tmp:WorkStationName',tmp:WorkStationName,'DespatchProcess',1)
    SolaceViewVars('tmp:BatchNumber',tmp:BatchNumber,'DespatchProcess',1)
    SolaceViewVars('tmp:PrintSummaryNote',tmp:PrintSummaryNote,'DespatchProcess',1)
    SolaceViewVars('Despatch_Label_Type_Temp',Despatch_Label_Type_Temp,'DespatchProcess',1)
    SolaceViewVars('Courier_Temp',Courier_Temp,'DespatchProcess',1)
    SolaceViewVars('tmp:DoDespatch',tmp:DoDespatch,'DespatchProcess',1)
    SolaceViewVars('tmp:PassedConsignNo',tmp:PassedConsignNo,'DespatchProcess',1)
    SolaceViewVars('local:FileName',local:FileName,'DespatchProcess',1)
    SolaceViewVars('tmp:ReturnedBatchNumber',tmp:ReturnedBatchNumber,'DespatchProcess',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchAccountNumber;  SolaceCtrlName = '?tmp:BatchAccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchAccountName;  SolaceCtrlName = '?tmp:BatchAccountName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchCourier;  SolaceCtrlName = '?tmp:BatchCourier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:NumberInBatch;  SolaceCtrlName = '?tmp:NumberInBatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?StatusText;  SolaceCtrlName = '?StatusText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AddUnit;  SolaceCtrlName = '?AddUnit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CreateNewBatch;  SolaceCtrlName = '?CreateNewBatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ForceIndividualDespatch;  SolaceCtrlName = '?ForceIndividualDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('DespatchProcess')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'DespatchProcess')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Relate:MULDESP.Open
  Relate:MULDESP_ALIAS.Open
  Relate:STATUS.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  Access:MULDESPJ.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBACC.UseFile
  Access:LOAN.UseFile
  Access:TRADEACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:EXCHHIST.UseFile
  Access:LOANHIST.UseFile
  Access:LOCINTER.UseFile
  Access:USERS.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  !First thing. Get the job details
  Access:JOBS_ALIAS.ClearKey(job_ali:Ref_Number_Key)
  job_ali:Ref_Number = func:JobNumber
  If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
      !Found
      !Does this job's details match an existing batch?
      Setcursor(Cursor:Wait)
      tmp:ExistingBatchNumber = 0
      Save_muld_ID = Access:MULDESP.SaveFile()
      Access:MULDESP.ClearKey(muld:AccountNumberKey)
      muld:AccountNumber = job_ali:Account_Number
      Set(muld:AccountNumberKey,muld:AccountNumberKey)
      Loop
          If Access:MULDESP.NEXT()
             Break
          End !If
          If muld:AccountNumber <> job_ali:Account_Number      |
              Then Break.  ! End If
          !Is it the job/exchange or loan being despatch?
          Case job_ali:Despatch_Type
              Of 'JOB'
                  If job_ali:Courier = muld:Courier
                      tmp:ExistingBatchNumber = muld:RecordNumber
                      Break
                  End !If job_ali:Courier = muld:Courier
              Of 'EXC'
                  If job_ali:Exchange_Courier = muld:Courier
                      tmp:ExistingBatchNumber = muld:RecordNumber
                      Break
                  End !If job_Ali:Exchange_Courier = muld:Courier
              Of 'LOA'
                  If job_ali:Loan_Courier = muld:Courier
                      tmp:ExistingBatchNumber = muld:RecordNumber
                      Break
                  End !If job_ali:Loan_Courier = muld:Courier
          End !Case job_ali:Despatch_Type
      End !Loop
      Access:MULDESP.RestoreFile(Save_muld_ID)
      Setcursor()
  
      If tmp:ExistingBatchNumber = 0
          !No Existing Batch Found
          ?StatusText{prop:Text} = 'No Batch Found'
          ?StatusText{prop:FontColor} = color:Red
          ?AddUnit{prop:Disable} = 1
      Else !If tmp:ExistingBatchNumber = 0
          !Found a Batch, fill in it's details.
          Access:MULDESP.ClearKey(muld:RecordNumberKey)
          muld:RecordNumber = tmp:ExistingBatchNumber
          If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Found
              tmp:BatchAccountNumber  = muld:AccountNumber
              tmp:BatchCourier        = muld:Courier
              !Count the Jobs in the Batch
              tmp:NumberInBatch       = 0
              Save_mulj_ID = Access:MULDESPJ.SaveFile()
              Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
              mulj:RefNumber = muld:RecordNumber
              Set(mulj:JobNumberKey,mulj:JobNumberKey)
              Loop
                  If Access:MULDESPJ.NEXT()
                     Break
                  End !If
                  If mulj:RefNumber <> muld:RecordNumber      |
                      Then Break.  ! End If
                  tmp:NumberInBatch   += 1
              End !Loop
              Access:MULDESPJ.RestoreFile(Save_mulj_ID)
  
              !Get the Sub Account Name
              Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
              sub:Account_Number  = muld:AccountNumber
              If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Found
                  tmp:BatchAccountName    = sub:Company_Name
              Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
              ?StatusText{prop:Text} = 'Existing Batch Found'
              ?StatusText{prop:FontColor} = color:Green
              ?tmp:NumberInBatch{prop:FontColor} = color:Green
          Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
      End !If tmp:ExistingBatchNumber = 0
  
  Else!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBS_ALIAS.TryFetch(job_ali:Ref_Number_Key) = Level:Benign
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
    Relate:MULDESP.Close
    Relate:MULDESP_ALIAS.Close
    Relate:STATUS.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'DespatchProcess',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AddUnit
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddUnit, Accepted)
      !Add job to existing Batch
      !Reget Job
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = func:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If Access:MULDESPJ.PrimeRecord() = Level:Benign
              mulj:RefNumber     = tmp:ExistingBatchNumber
              mulj:JobNumber     = job:Ref_Number
              mulj:IMEINumber    = job:ESN
              mulj:MSN           = job:MSN
              mulj:AccountNumber = job:Account_Number
              Case job:Despatch_Type
                  Of 'JOB'
                      mulj:Courier    = job:Courier
                  Of 'EXC'
                      mulj:Courier    = job:Exchange_Courier
                  Of 'LOA'
                      mulj:Courier    = job:Loan_Courier
              End !Case job:Despatch_Type
      
              If Access:MULDESPJ.TryInsert() = Level:Benign
                  !Insert Successful
      
                  !Count how many jobs there are on the batch.
                  !I could just add one to the record, but I can see
                  !that being too inaccurate. So I'll physically count
                  !all the records in the batch instead.
      
                  CountBatch# = 0
                  Save_mulj_ID = Access:MULDESPJ.SaveFile()
                  Access:MULDESPJ.ClearKey(mulj:JobNumberKey)
                  mulj:RefNumber = tmp:ExistingBatchNumber
                  Set(mulj:JobNumberKey,mulj:JobNumberKey)
                  Loop
                      If Access:MULDESPJ.NEXT()
                         Break
                      End !If
                      If mulj:RefNumber <> tmp:ExistingBatchNumber      |
                          Then Break.  ! End If
                      CountBatch# += 1
                  End !Loop
                  Access:MULDESPJ.RestoreFile(Save_mulj_ID)
      
                  Access:MULDESP.ClearKey(muld:RecordNumberKey)
                  muld:RecordNumber = tmp:ExistingBatchNumber
                  If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                      !Found
                      muld:BatchTotal = CountBatch#
                      tmp:ReturnedBatchNumber = muld:BatchNumber
                      Access:MULDESP.Update()
                  Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              Else !If Access:MULDESPJ.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:MULDESPJ.TryInsert() = Level:Benign
          End !If Access:MULDESPJ.PrimeRecord() = Level:Benign
          tmp:Close = 1
          Post(Event:CloseWindow)
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddUnit, Accepted)
    OF ?CreateNewBatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateNewBatch, Accepted)
      If tmp:ExistingBatchNumber <> 0
          !Create a new batch when one already exists:
          !Finish and despatch the current batch, i.e. the one that was found
          !and start a new batch for this job.
      
          Access:MULDESP.ClearKey(muld:RecordNumberKey)
          muld:RecordNumber = tmp:ExistingBatchNumber
          If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Found
      
              !The following code was copied/pasted from the other Multiple Despatch
              !screen.
              tmp:BatchNumber = muld:RecordNumber
      
              tmp:PrintSummaryNote = 0
              Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
              sub:Account_Number  = muld:AccountNumber
              If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Found
                  Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                  tra:Account_Number  = sub:Main_Account_Number
                  If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      !Found
                      If tra:Use_Sub_Accounts = 'YES'
                          If sub:Print_Despatch_Despatch  = 'YES'
                              If sub:Summary_Despatch_Notes = 'YES'
                                  tmp:PrintSummaryNote = 1
                              End !If sub:Summary_Despatch_Notes = 'YES'
                          End !If sub:Print_Despatch_Despatch  = 'YES'
                          Despatch_Label_Type_temp    = 'SUB'
                      Else !If tra:Use_Sub_Accounts = 'YES'
                          If tra:Print_Despatch_Despatch = 'YES'
                              If tra:Summary_Despatch_Notes = 'YES'
                                  tmp:PrintSummaryNote = 1
                              End !If tra:Summary_Despatch_Notes = 'YES'
                          End !If tra:Print_Despatch_Despatch = 'YES'
                          Despatch_Label_Type_Temp = 'TRA'
      
                      End !If tra:Use_Sub_Accounts = 'YES'
                  Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                  
              Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      
              !Fill in old variables
              Courier_Temp = muld:Courier
              Account_Number2_Temp = muld:AccountNumber
              If access:desbatch.primerecord() = Level:Benign
                  If access:desbatch.tryinsert()
                      access:desbatch.cancelautoinc()
                  Else!If access:desbatch.tryinsert()
                      DespatchMultiple(muld:Courier,muld:AccountNumber,tmp:BatchNumber,Despatch_Label_Type_Temp)
      
                  End!
              End!If access:desbatch.tryinsert()
          Else!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MULDESP.TryFetch(muld:RecordNumberKey) = Level:Benign
      
      End !tmp:ExistingBatchNumber = 0
      
      !Now that's all finished create a new batch,
      !for the original job.
      
      !Reget the live job.
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = func:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          If Access:MULDESP.PrimeRecord() = Level:Benign
              muld:AccountNumber = job:Account_Number
              Case job:Despatch_Type
                  Of 'JOB'
                      muld:Courier    = job:Courier
                  Of 'LOA'
                      muld:Courier    = job:Loan_Courier
                  Of 'EXC'
                      muld:Courier    = job:Exchange_Courier
              End !Case job:Despatch_Type
              muld:BatchTotal         = 1
      
              !Allocate a Batch Number
              !Count between 1 to 1000, (that should be enough)
              !and if I can't find a Batch with that batch number, then
              !assign that batch number to this batch.
              BatchNumber# = 0
              Loop BatchNumber# = 1 To 1000
                  Access:MULDESP_ALIAS.ClearKey(muld_ali:BatchNumberKey)
                  muld_ali:BatchNumber = BatchNumber#
                  If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Found
                  Else!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      muld:BatchNumber    = BatchNumber#
                      tmp:ReturnedBatchNumber = muld:BatchNumber
                      Break
      
                  End!If Access:MULDESP_ALIAS.TryFetch(muld_ali:BatchNumberKey) = Level:Benign
              End !BatchNumber# = 1 To 1000
      
              If Access:MULDESP.TryInsert() = Level:Benign
                  !Insert Successful
                  If Access:MULDESPJ.PrimeRecord() = Level:Benign
                      mulj:RefNumber     = muld:RecordNumber
                      mulj:JobNumber     = job:Ref_Number
                      mulj:IMEINumber    = job:ESN
                      mulj:MSN           = job:MSN
                      mulj:AccountNumber = job:Account_Number
                      mulj:Courier       = muld:Courier
                      mulj:Current       = 1
                      If Access:MULDESPJ.TryInsert() = Level:Benign
                          !Insert Successful
                      Else !If Access:MULDESPJ.TryInsert() = Level:Benign
                          !Insert Failed
                      End !If Access:MULDESPJ.TryInsert() = Level:Benign
                  End !If Access:MULDESPJ.PrimeRecord() = Level:Benign
              Else !If Access:MULDESP.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:MULDESP.TryInsert() = Level:Benign
          End !If Access:MULDESP.PrimeRecord() = Level:Benign
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      tmp:Close = 1
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CreateNewBatch, Accepted)
    OF ?ForceIndividualDespatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ForceIndividualDespatch, Accepted)
      !Do not add the job to a batch, just
      !despatch it individually.
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      
      !Reget Job
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = func:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Error# = 0
          If def:Force_Accessory_Check = 'YES'
              If LocalValidateAccessories()
                  Access:JOBS.Update()
                  !Failed Check
                  Error# = 1
              End !If LocalValidateAccessories(job:Despatch_Type,job:Ref_Number)
          End !If def:Force_Accessory_Check = 'YES'
      
          If def:ValidateDesp = 'YES'
              If LocalValidateIMEI()
                  !Failed Check
                  Error# = 1
              End !If LocalValidateIMEI()
          End !If def:ValidateDesp = 'YES'
      
          If Error# = 0
              !Get the Courier. Required by 'despsing.inc'
              Access:COURIER.ClearKey(cou:Courier_Key)
              Case job:Despatch_Type
                  Of 'JOB'
                      cou:Courier = job:Courier
                  Of 'EXC'
                      cou:Courier = job:Exchange_Courier
                  Of 'LOA'
                      cou:Courier = job:Loan_Courier
              End !Case job:Despatch_Type
              If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                  !Found
                  If access:desbatch.primerecord() = Level:Benign
                      If access:desbatch.tryinsert()
                          access:desbatch.cancelautoinc()
                      Else!If access:desbatch.tryinsert()
                          DespatchSingle()
                      End!If access:desbatch.tryinsert()
                  End!If access:desbatch.primerecord() = Level:Benign
      
              Else!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End!If Access:COURIER.TryFetch(cou:Courier_Key) = Level:Benign
      
          End !If Error# = 0
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      tmp:Close = 1
      tmp:ReturnedBatchNumber = 0
      Post(Event:CloseWindow)
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ForceIndividualDespatch, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      tmp:Close = 0
      tmp:ReturnedBatchNumber = 0
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'DespatchProcess')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
      If tmp:Close = 0
          Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
              Of 2 ! &No Button
                  Cycle
          End!Case MessageEx
      End !tmp:Close = 0
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(CloseWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F6Key
              !This button can be disabled. Only alert the key if enabled.
              If ?AddUnit{prop:Disable} = 0
                  Post(Event:Accepted,?AddUnit)
              End !If ?AddUnit{prop:Disable} = 0
          Of F7Key
              Post(Event:Accepted,?CreateNewBatch)
          Of F8Key
              Post(Event:Accepted,?ForceIndividualDespatch)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
LocalValidateAccessories    Procedure()
local:ErrorCode                   Byte
    Code

    local:ErrorCode  = 0
    Case job:Despatch_Type
        Of 'JOB'
            If AccessoryCheck('JOB')
                If AccessoryMismatch(job:Ref_Number,'JOB')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'JOB')
            End !If AccessoryCheck('JOB')
        Of 'LOA'
            If AccessoryCheck('LOAN')
                If AccessoryMismatch(job:Ref_Number,'LOA')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'LOA')
            End !If AccessoryCheck('LOAN')
        Of 'EXC'
            If AccessoryCheck('EXC')
                If AccessoryMismatch(job:Ref_Number,'EXC')
                    local:Errorcode = 1
                End !If AccessoryMismatch(job:Ref_Number,'EXC')
            End !If AccesoryCheck('EXC')
    End !Case job:Despatch_Type
    If local:Errorcode = 1
        GetStatus(850,1,job:Despatch_Type)
        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Notes         = 'ACCESSORY MISMATCH<13,10,13,10>ACCESSORIES BOOKED IN:'
            If job:Despatch_Type <> 'LOAN'
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)
            Else !If job:Despatch_Type <> 'LOAN'
                Save_lac_ID = Access:LOANACC.SaveFile()
                Access:LOANACC.ClearKey(lac:Ref_Number_Key)
                lac:Ref_Number = job:Ref_Number
                Set(lac:Ref_Number_Key,lac:Ref_Number_Key)
                Loop
                    If Access:LOANACC.NEXT()
                       Break
                    End !If
                    If lac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(lac:Accessory)
                End !Loop
                Access:LOANACC.RestoreFile(Save_lac_ID)
            End !If job:Despatch_Type <> 'LOAN'
            aud:Notes         = Clip(aud:Notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT: '
            Clear(glo:Queue)
            Loop x# = 1 To Records(glo:Queue)
                Get(glo:Queue,x#)
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(glo:Pointer)
            End !Loop x# = 1 To Records(glo:Queue).

            aud:Ref_Number    = job:Ref_Number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = job:Despatch_Type
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'FAILED DESPATCH VALIDATION'
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
        Access:JOBS.Update()
        Return Level:Fatal
        
    End !If Return Level:Fatal

    Return Level:Benign
LocalValidateIMEI    Procedure()
local:ErrorCode                   Byte
    Code

    Case job:Despatch_Type
        Of 'JOB'
            IF ValidateIMEI(job:ESN)
                local:ErrorCode = 1
            End !IF ValidateIMEI(job:ESN)
        Of 'LOA'
            Access:LOAN.Clearkey(loa:Ref_Number_Key)
            loa:Ref_Number  = job:Loan_Unit_Number
            If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(loa:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(loa:ESN)
            Else! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:LOAN.Tryfetch(loa:Ref_Number_Key) = Level:Benign
            
        Of 'EXC'
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                If ValidateIMEI(xch:ESN)
                    local:ErrorCode = 1
                End !If ValidateIMEI(xch:ESN)
            Else! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
    End !Case job:Despatch_Type
    If local:ErrorCode = 1
        Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_Number) & '.' &|
          '<13,10>'&|
          '<13,10>I.M.E.I. Number mismatch.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        GetStatus(850,1,job:Despatch_Type)
        If Access:AUDIT.PrimeRecord() = Level:Benign
            aud:Notes         = 'I.M.E.I. NUMBER MISMATCH'
            aud:Ref_Number    = job:ref_number
            aud:Date          = Today()
            aud:Time          = Clock()
            aud:Type          = job:Despatch_Type
            Access:USERS.ClearKey(use:Password_Key)
            use:Password      = glo:Password
            Access:USERS.Fetch(use:Password_Key)
            aud:User          = use:User_Code
            aud:Action        = 'FAILED DESPATCH VALIDATION'
            Access:AUDIT.Insert()
        End!If Access:AUDIT.PrimeRecord() = Level:Benign
        Access:JOBS.Update()
        Return Level:Fatal
    End !If Error# = 1

    Return Level:Benign
LocalValidateAccountNumber      Procedure()
    Code

    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = job:Account_Number
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:Skip_Despatch = 'YES'
                Case MessageEx('The Trade Account attached to Job Number '&Clip(job:ref_number)&' is only used for ''Batch Despatch''.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'Skip Futher Errors',skip_error#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                    Of 1 ! &OK Button
                End!Case MessageEx
                Return Level:Fatal
            End !If tra:Skip_Despatch = 'YES'
            If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The Trade Account is on stop and it is not a Warranty Job.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            Else !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
                 If tra:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
                    Case MessageEx('Cannot Despatch Job Number: ' & Clip(job:Ref_number) & '.'&|
                      '<13,10>'&|
                      '<13,10>The Trade Account is on stop and it is not a Warranty Job.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return Level:Fatal
                End !If sub:Stop_Account = 'YES' and job:Warranty_Job <> 'YES'
            End !If tra:Use_Sub_Accounts = 'YES' and tra:Invoice_Sub_ACcounts = 'YES'
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign

    Return Level:Benign
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
