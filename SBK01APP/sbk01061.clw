

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBK01061.INC'),ONCE        !Local module procedure declarations
                     END


FormatJobFaultCode   PROCEDURE  (arg:FaultCode, arg:FaultCodeNumber, arg:Manufacturer) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'FormatJobFaultCode')      !Add Procedure to Log
  end


IF (arg:FaultCode <> '') THEN
    access:manfault.clearkey(maf:field_number_key)
    maf:manufacturer = arg:manufacturer
    maf:field_number = arg:FaultCodeNumber
    IF (access:manfault.fetch(maf:field_number_key) = Level:Benign) THEN
        IF (maf:field_type = 'DATE') THEN
            RETURN FORMAT(arg:FaultCode, maf:DateType)
        END
    END
END
RETURN arg:FaultCode


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'FormatJobFaultCode',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
