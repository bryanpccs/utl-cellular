

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01047.INC'),ONCE        !Local module procedure declarations
                     END


ProformaMaintenance PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_prv_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
save_job_id          USHORT,AUTO
window               WINDOW('Proforma Invoice Maintenance Routine'),AT(,,220,119),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,84),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('This routine will search through the Invoiced Proforma batches and find any jobs' &|
   ' that have not been invoiced. It will remove these jobs from that batch and make' &|
   ' them available to be invoiced again.'),AT(8,8,204,32),USE(?Prompt2),FONT(,,,FONT:bold)
                           PROMPT('Note: This routine may take a while, depending on the amount of Batches that hav' &|
   'e been created.'),AT(8,52,204,20),USE(?Prompt3),FONT(,,COLOR:Red,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(4,92,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Close'),AT(156,96,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       BUTTON('&OK'),AT(100,96,56,16),USE(?Button2),LEFT,ICON('ok.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ProformaMaintenance',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_prv_id',save_prv_id,'ProformaMaintenance',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'ProformaMaintenance',1)
    SolaceViewVars('save_job_id',save_job_id,'ProformaMaintenance',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button2;  SolaceCtrlName = '?Button2';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ProformaMaintenance')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ProformaMaintenance')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  Relate:PROINV.Open
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:PROINV.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ProformaMaintenance',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
      Case MessageEx('Are you sure you want to begin.','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
      
              recordspercycle         = 25
              recordsprocessed        = 0
              percentprogress         = 0
              progress:thermometer    = 0
              thiswindow.reset(1)
              open(progresswindow)
      
              ?progress:userstring{prop:text} = 'Running...'
              ?progress:pcttext{prop:text} = '0% Completed'
      
      
              recordstoprocess    = Records(JOBS)
      
              Save_job_ID = Access:JOBS.SaveFile()
              Access:JOBS.ClearKey(job:InvoiceNumberKey)
              job:Invoice_Number = 0
              Set(job:InvoiceNumberKey,job:InvoiceNumberKey)
              Loop
                  If Access:JOBS.NEXT()
                     Break
                  End !If
                  If job:Invoice_Number <> 0      |
                      Then Break.  ! End If
                  Do GetNextRecord2
                  cancelcheck# += 1
                  If cancelcheck# > (RecordsToProcess/100)
                      Do cancelcheck
                      If tmp:cancel = 1
                          Break
                      End!If tmp:cancel = 1
                      cancelcheck# = 0
                  End!If cancelcheck# > 50
      
                  If job:InvoiceBatch <> 0 And job:Date_Completed <> ''
                      Access:PROINV.ClearKey(prv:RefNumberKey)
                      prv:RefNumber = job:InvoiceBatch
                      If Access:PROINV.TryFetch(prv:RefNumberKey) = Level:Benign
                          !Found
                          If prv:Invoiced = 'YES'
                              job:InvoiceBatch = ''
                              job:InvoiceStatus = ''
                              job:InvoiceAccount = ''
                              job:InvoiceQuery = ''
                              Access:JOBS.Update()
                          End !If prv:Invoiced = 'YES'
                      Else!If Access:PROINV.TryFetch(prv:RefNumberKey) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End!If Access:PROINV.TryFetch(prv:RefNumberKey) = Level:Benign
                  End !If job:InvoiceBatch <> 0 And job:Date_Completed <> ''
              End !Loop
              Access:JOBS.RestoreFile(Save_job_ID)
      
              Do EndPrintRun
              close(progresswindow)
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button2, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ProformaMaintenance')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

