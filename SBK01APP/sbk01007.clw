

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01007.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTURNARND PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::tur:Record  LIKE(tur:RECORD),STATIC
QuickWindow          WINDOW('Update the TURNARND File'),AT(,,203,92),FONT('Tahoma',8,,),CENTER,IMM,ICON('Pc.ico'),HLP('UpdateTURNARND'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,196,56),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Turnaround Time'),AT(8,20),USE(?TUR:Turnaround_Time:Prompt),TRN
                           ENTRY(@s30),AT(72,20,124,10),USE(tur:Turnaround_Time),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Time'),AT(8,40),USE(?TUR:Days:Prompt),TRN
                           SPIN(@n3),AT(72,40,40,10),USE(tur:Days),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(0,9999),STEP(1)
                           PROMPT('Hours'),AT(116,32),USE(?TUR:Hours:Prompt),TRN,FONT(,7,,)
                           SPIN(@n2),AT(116,40,40,10),USE(tur:Hours),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(0,23),STEP(1)
                           PROMPT('Days'),AT(72,32),USE(?Prompt4),TRN,FONT(,7,,)
                         END
                       END
                       PANEL,AT(4,64,196,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(84,68,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(140,68,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?TUR:Turnaround_Time:Prompt{prop:FontColor} = -1
    ?TUR:Turnaround_Time:Prompt{prop:Color} = 15066597
    If ?tur:Turnaround_Time{prop:ReadOnly} = True
        ?tur:Turnaround_Time{prop:FontColor} = 65793
        ?tur:Turnaround_Time{prop:Color} = 15066597
    Elsif ?tur:Turnaround_Time{prop:Req} = True
        ?tur:Turnaround_Time{prop:FontColor} = 65793
        ?tur:Turnaround_Time{prop:Color} = 8454143
    Else ! If ?tur:Turnaround_Time{prop:Req} = True
        ?tur:Turnaround_Time{prop:FontColor} = 65793
        ?tur:Turnaround_Time{prop:Color} = 16777215
    End ! If ?tur:Turnaround_Time{prop:Req} = True
    ?tur:Turnaround_Time{prop:Trn} = 0
    ?tur:Turnaround_Time{prop:FontStyle} = font:Bold
    ?TUR:Days:Prompt{prop:FontColor} = -1
    ?TUR:Days:Prompt{prop:Color} = 15066597
    If ?tur:Days{prop:ReadOnly} = True
        ?tur:Days{prop:FontColor} = 65793
        ?tur:Days{prop:Color} = 15066597
    Elsif ?tur:Days{prop:Req} = True
        ?tur:Days{prop:FontColor} = 65793
        ?tur:Days{prop:Color} = 8454143
    Else ! If ?tur:Days{prop:Req} = True
        ?tur:Days{prop:FontColor} = 65793
        ?tur:Days{prop:Color} = 16777215
    End ! If ?tur:Days{prop:Req} = True
    ?tur:Days{prop:Trn} = 0
    ?tur:Days{prop:FontStyle} = font:Bold
    ?TUR:Hours:Prompt{prop:FontColor} = -1
    ?TUR:Hours:Prompt{prop:Color} = 15066597
    If ?tur:Hours{prop:ReadOnly} = True
        ?tur:Hours{prop:FontColor} = 65793
        ?tur:Hours{prop:Color} = 15066597
    Elsif ?tur:Hours{prop:Req} = True
        ?tur:Hours{prop:FontColor} = 65793
        ?tur:Hours{prop:Color} = 8454143
    Else ! If ?tur:Hours{prop:Req} = True
        ?tur:Hours{prop:FontColor} = 65793
        ?tur:Hours{prop:Color} = 16777215
    End ! If ?tur:Hours{prop:Req} = True
    ?tur:Hours{prop:Trn} = 0
    ?tur:Hours{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTURNARND',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTURNARND',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateTURNARND',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTURNARND',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TUR:Turnaround_Time:Prompt;  SolaceCtrlName = '?TUR:Turnaround_Time:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tur:Turnaround_Time;  SolaceCtrlName = '?tur:Turnaround_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TUR:Days:Prompt;  SolaceCtrlName = '?TUR:Days:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tur:Days;  SolaceCtrlName = '?tur:Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TUR:Hours:Prompt;  SolaceCtrlName = '?TUR:Hours:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tur:Hours;  SolaceCtrlName = '?tur:Hours';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job Turnaround Time'
  OF ChangeRecord
    ActionMessage = 'Changing A Job Turnaround Time'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTURNARND')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTURNARND')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TUR:Turnaround_Time:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tur:Record,History::tur:Record)
  SELF.AddHistoryField(?tur:Turnaround_Time,1)
  SELF.AddHistoryField(?tur:Days,2)
  SELF.AddHistoryField(?tur:Hours,3)
  SELF.AddUpdateFile(Access:TURNARND)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TURNARND.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TURNARND
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTURNARND',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTURNARND')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

