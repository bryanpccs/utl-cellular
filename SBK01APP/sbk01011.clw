

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBK01011.INC'),ONCE        !Local module procedure declarations
                     END





EDI_Claims PROCEDURE                                  !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisThreadActive BYTE
CurrentTab           STRING(80)
save_job_id          USHORT,AUTO
FilesOpened          BYTE
Manufacturer_Temp    STRING(30)
Batch_Number_Temp    LONG
jobs_tag             STRING(1)
jobs_tag_temp        STRING(1)
Charge_Repair_Type_Temp STRING(60)
BrFilter1            STRING(300)
BrLocator1           STRING(50)
BrFilter2            STRING(300)
BrLocator2           STRING(50)
jfc1                 STRING(30)
jfc2                 STRING(30)
jfc3                 STRING(30)
jfc4                 STRING(30)
jfc5                 STRING(30)
jfc6                 STRING(30)
jfc7                 STRING(30)
jfc8                 STRING(30)
jfc9                 STRING(30)
jfc10                STRING(255)
jfc11                STRING(255)
jfc12                STRING(255)
tmp:BrowseString     STRING(1000)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Manufacturer_Temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Courier_Cost_Warranty)
                       PROJECT(job:Labour_Cost_Warranty)
                       PROJECT(job:Parts_Cost_Warranty)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:EDI)
                       PROJECT(job:EDI_Batch_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:MSN                LIKE(job:MSN)                  !List box control field - type derived from field
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !List box control field - type derived from field
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !List box control field - type derived from field
job:Courier_Cost_Warranty LIKE(job:Courier_Cost_Warranty) !List box control field - type derived from field
job:Labour_Cost_Warranty LIKE(job:Labour_Cost_Warranty) !List box control field - type derived from field
job:Parts_Cost_Warranty LIKE(job:Parts_Cost_Warranty) !List box control field - type derived from field
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
jfc1                   LIKE(jfc1)                     !List box control field - type derived from local data
jfc2                   LIKE(jfc2)                     !List box control field - type derived from local data
jfc3                   LIKE(jfc3)                     !List box control field - type derived from local data
jfc4                   LIKE(jfc4)                     !List box control field - type derived from local data
jfc5                   LIKE(jfc5)                     !List box control field - type derived from local data
jfc6                   LIKE(jfc6)                     !List box control field - type derived from local data
jfc7                   LIKE(jfc7)                     !List box control field - type derived from local data
jfc8                   LIKE(jfc8)                     !List box control field - type derived from local data
jfc9                   LIKE(jfc9)                     !List box control field - type derived from local data
jfc10                  LIKE(jfc10)                    !List box control field - type derived from local data
jfc11                  LIKE(jfc11)                    !List box control field - type derived from local data
jfc12                  LIKE(jfc12)                    !List box control field - type derived from local data
job:Manufacturer       LIKE(job:Manufacturer)         !Browse key field - type derived from field
job:EDI                LIKE(job:EDI)                  !Browse key field - type derived from field
job:EDI_Batch_Number   LIKE(job:EDI_Batch_Number)     !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK15::job:EDI             LIKE(job:EDI)
HK15::job:Manufacturer    LIKE(job:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB4::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,538770663)          !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse Jobs Ready For Warranty Process'),AT(,,668,250),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,HLP('Reconcile_Claims'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,568,208),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('36R(2)|M~Job No~L@P<<<<<<<<<<<<<<#PB@65L(2)|M~Account Number~@s15@69L(2)|M~Model Number' &|
   '~@s30@69L(2)|M~ESN/IMEI~@s16@61L(2)|M~MSN~@s14@74L(2)|M~Charge Type~@s30@74L(2)|' &|
   'M~Repair Type~@s30@33L(2)|M~Courier~@n7.2@33L(2)|M~Labour~@n7.2@54L(2)|M~Parts~@' &|
   'n7.2@120L(2)|M~Order Number~@s30@120L(2)|M~Fault Code 1~@s30@120L(2)|M~Fault Cod' &|
   'e 2~@s30@120L(2)|M~Fault Code 3~@s30@120L(2)|M~Fault Code 4~@s30@120L(2)|M~Fault' &|
   ' Code 5~@s30@120L(2)|M~Fault Code 6~@s30@120L(2)|M~Fault Code 7~@s30@120L(2)|M~F' &|
   'ault Code 8~@s30@120L(2)|M~Fault Code 9~@s30@120L(2)|M~Fault Code 10~@s30@120L(2' &|
   ')|M~Fault Code 11~@s30@120L(2)|M~Fault Code 12~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('Valuate Claims'),AT(588,8,76,20),USE(?Valudate_Claims),LEFT,ICON('Invoice.gif')
                       SHEET,AT(4,4,576,244),USE(?CurrentTab),SPREAD
                         TAB('Job Ready For Warranty Process'),USE(?Tab:2)
                           PROMPT('Manufacturer'),AT(8,20),USE(?Prompt1)
                           COMBO(@s30),AT(68,20,124,10),USE(Manufacturer_Temp),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           BUTTON('&View Job'),AT(588,112,76,20),USE(?Change),LEFT,ICON('edit.ico')
                         END
                       END
                       BUTTON('&Process Claims'),AT(588,32,76,20),USE(?edi_process),LEFT,ICON(ICON:Save)
                       BUTTON('Close'),AT(588,228,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,imm,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
       button('Cancel'),at(54,44,56,16),use(?cancel),left,icon('cancel.gif')
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepLongClass   !LONG            !Xplore: Column displaying job:Ref_Number
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying job:Ref_Number
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying job:Account_Number
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying job:Account_Number
Xplore1Step3         StepStringClass !STRING          !Xplore: Column displaying job:Model_Number
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying job:Model_Number
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying job:ESN
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying job:ESN
Xplore1Step5         StepStringClass !STRING          !Xplore: Column displaying job:MSN
Xplore1Locator5      IncrementalLocatorClass          !Xplore: Column displaying job:MSN
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying job:Warranty_Charge_Type
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying job:Warranty_Charge_Type
Xplore1Step7         StepStringClass !STRING          !Xplore: Column displaying job:Repair_Type_Warranty
Xplore1Locator7      IncrementalLocatorClass          !Xplore: Column displaying job:Repair_Type_Warranty
Xplore1Step8         StepRealClass   !REAL            !Xplore: Column displaying job:Courier_Cost_Warranty
Xplore1Locator8      IncrementalLocatorClass          !Xplore: Column displaying job:Courier_Cost_Warranty
Xplore1Step9         StepRealClass   !REAL            !Xplore: Column displaying job:Labour_Cost_Warranty
Xplore1Locator9      IncrementalLocatorClass          !Xplore: Column displaying job:Labour_Cost_Warranty
Xplore1Step10        StepRealClass   !REAL            !Xplore: Column displaying job:Parts_Cost_Warranty
Xplore1Locator10     IncrementalLocatorClass          !Xplore: Column displaying job:Parts_Cost_Warranty
Xplore1Step11        StepStringClass !STRING          !Xplore: Column displaying job:Order_Number
Xplore1Locator11     IncrementalLocatorClass          !Xplore: Column displaying job:Order_Number
Xplore1Step12        StepCustomClass !                !Xplore: Column displaying jfc1
Xplore1Locator12     IncrementalLocatorClass          !Xplore: Column displaying jfc1
Xplore1Step13        StepCustomClass !                !Xplore: Column displaying jfc2
Xplore1Locator13     IncrementalLocatorClass          !Xplore: Column displaying jfc2
Xplore1Step14        StepCustomClass !                !Xplore: Column displaying jfc3
Xplore1Locator14     IncrementalLocatorClass          !Xplore: Column displaying jfc3
Xplore1Step15        StepCustomClass !                !Xplore: Column displaying jfc4
Xplore1Locator15     IncrementalLocatorClass          !Xplore: Column displaying jfc4
Xplore1Step16        StepCustomClass !                !Xplore: Column displaying jfc5
Xplore1Locator16     IncrementalLocatorClass          !Xplore: Column displaying jfc5
Xplore1Step17        StepCustomClass !                !Xplore: Column displaying jfc6
Xplore1Locator17     IncrementalLocatorClass          !Xplore: Column displaying jfc6
Xplore1Step18        StepCustomClass !                !Xplore: Column displaying jfc7
Xplore1Locator18     IncrementalLocatorClass          !Xplore: Column displaying jfc7
Xplore1Step19        StepCustomClass !                !Xplore: Column displaying jfc8
Xplore1Locator19     IncrementalLocatorClass          !Xplore: Column displaying jfc8
Xplore1Step20        StepCustomClass !                !Xplore: Column displaying jfc9
Xplore1Locator20     IncrementalLocatorClass          !Xplore: Column displaying jfc9
Xplore1Step21        StepCustomClass !                !Xplore: Column displaying jfc10
Xplore1Locator21     IncrementalLocatorClass          !Xplore: Column displaying jfc10
Xplore1Step22        StepCustomClass !                !Xplore: Column displaying jfc11
Xplore1Locator22     IncrementalLocatorClass          !Xplore: Column displaying jfc11
Xplore1Step23        StepCustomClass !                !Xplore: Column displaying jfc12
Xplore1Locator23     IncrementalLocatorClass          !Xplore: Column displaying jfc12
Xplore1Step24        StepStringClass !STRING          !Xplore: Column displaying job:Manufacturer
Xplore1Locator24     IncrementalLocatorClass          !Xplore: Column displaying job:Manufacturer
Xplore1Step25        StepStringClass !STRING          !Xplore: Column displaying job:EDI
Xplore1Locator25     IncrementalLocatorClass          !Xplore: Column displaying job:EDI
Xplore1Step26        StepRealClass   !REAL            !Xplore: Column displaying job:EDI_Batch_Number
Xplore1Locator26     IncrementalLocatorClass          !Xplore: Column displaying job:EDI_Batch_Number

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?Manufacturer_Temp{prop:ReadOnly} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 15066597
    Elsif ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 8454143
    Else ! If ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 16777215
    End ! If ?Manufacturer_Temp{prop:Req} = True
    ?Manufacturer_Temp{prop:Trn} = 0
    ?Manufacturer_Temp{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        display()
      end
    end

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        case event()
            of event:timer
                break
            of event:closewindow
                cancel# = 1
                break
            of event:accepted
                if field() = ?cancel
                    cancel# = 1
                    break
                end!if field() = ?button1
        end!case event()
    end!accept
    if cancel# = 1
        case messageex('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,charset:ansi,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            of 1 ! &yes button
                tmp:cancel = 1
            of 2 ! &no button
        end!case messageex
    end!if cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'EDI_Claims',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'EDI_Claims',1)
    SolaceViewVars('save_job_id',save_job_id,'EDI_Claims',1)
    SolaceViewVars('FilesOpened',FilesOpened,'EDI_Claims',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'EDI_Claims',1)
    SolaceViewVars('Batch_Number_Temp',Batch_Number_Temp,'EDI_Claims',1)
    SolaceViewVars('jobs_tag',jobs_tag,'EDI_Claims',1)
    SolaceViewVars('jobs_tag_temp',jobs_tag_temp,'EDI_Claims',1)
    SolaceViewVars('Charge_Repair_Type_Temp',Charge_Repair_Type_Temp,'EDI_Claims',1)
    SolaceViewVars('BrFilter1',BrFilter1,'EDI_Claims',1)
    SolaceViewVars('BrLocator1',BrLocator1,'EDI_Claims',1)
    SolaceViewVars('BrFilter2',BrFilter2,'EDI_Claims',1)
    SolaceViewVars('BrLocator2',BrLocator2,'EDI_Claims',1)
    SolaceViewVars('jfc1',jfc1,'EDI_Claims',1)
    SolaceViewVars('jfc2',jfc2,'EDI_Claims',1)
    SolaceViewVars('jfc3',jfc3,'EDI_Claims',1)
    SolaceViewVars('jfc4',jfc4,'EDI_Claims',1)
    SolaceViewVars('jfc5',jfc5,'EDI_Claims',1)
    SolaceViewVars('jfc6',jfc6,'EDI_Claims',1)
    SolaceViewVars('jfc7',jfc7,'EDI_Claims',1)
    SolaceViewVars('jfc8',jfc8,'EDI_Claims',1)
    SolaceViewVars('jfc9',jfc9,'EDI_Claims',1)
    SolaceViewVars('jfc10',jfc10,'EDI_Claims',1)
    SolaceViewVars('jfc11',jfc11,'EDI_Claims',1)
    SolaceViewVars('jfc12',jfc12,'EDI_Claims',1)
    SolaceViewVars('tmp:BrowseString',tmp:BrowseString,'EDI_Claims',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Valudate_Claims;  SolaceCtrlName = '?Valudate_Claims';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer_Temp;  SolaceCtrlName = '?Manufacturer_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?edi_process;  SolaceCtrlName = '?edi_process';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('EDI_Claims','?Browse:1')         !Xplore
  BRW1.SequenceNbr = 0                                !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('EDI_Claims')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'EDI_Claims')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='EDI_Claims'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:DISCOUNT.Open
  Relate:EDIBATCH.Open
  Relate:EXCHANGE.Open
  Access:WARPARTS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:TRACHAR.UseFile
  Access:SUBCHRGE.UseFile
  Access:STDCHRGE.UseFile
  Access:MANFAULT.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbk01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,job:EDI_Key)
  BRW1.AddRange(job:EDI)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,job:EDI_Batch_Number,1,BRW1)
  BIND('jfc1',jfc1)
  BIND('jfc2',jfc2)
  BIND('jfc3',jfc3)
  BIND('jfc4',jfc4)
  BIND('jfc5',jfc5)
  BIND('jfc6',jfc6)
  BIND('jfc7',jfc7)
  BIND('jfc8',jfc8)
  BIND('jfc9',jfc9)
  BIND('jfc10',jfc10)
  BIND('jfc11',jfc11)
  BIND('jfc12',jfc12)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(job:MSN,BRW1.Q.job:MSN)
  BRW1.AddField(job:Warranty_Charge_Type,BRW1.Q.job:Warranty_Charge_Type)
  BRW1.AddField(job:Repair_Type_Warranty,BRW1.Q.job:Repair_Type_Warranty)
  BRW1.AddField(job:Courier_Cost_Warranty,BRW1.Q.job:Courier_Cost_Warranty)
  BRW1.AddField(job:Labour_Cost_Warranty,BRW1.Q.job:Labour_Cost_Warranty)
  BRW1.AddField(job:Parts_Cost_Warranty,BRW1.Q.job:Parts_Cost_Warranty)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(jfc1,BRW1.Q.jfc1)
  BRW1.AddField(jfc2,BRW1.Q.jfc2)
  BRW1.AddField(jfc3,BRW1.Q.jfc3)
  BRW1.AddField(jfc4,BRW1.Q.jfc4)
  BRW1.AddField(jfc5,BRW1.Q.jfc5)
  BRW1.AddField(jfc6,BRW1.Q.jfc6)
  BRW1.AddField(jfc7,BRW1.Q.jfc7)
  BRW1.AddField(jfc8,BRW1.Q.jfc8)
  BRW1.AddField(jfc9,BRW1.Q.jfc9)
  BRW1.AddField(jfc10,BRW1.Q.jfc10)
  BRW1.AddField(jfc11,BRW1.Q.jfc11)
  BRW1.AddField(jfc12,BRW1.Q.jfc12)
  BRW1.AddField(job:Manufacturer,BRW1.Q.job:Manufacturer)
  BRW1.AddField(job:EDI,BRW1.Q.job:EDI)
  BRW1.AddField(job:EDI_Batch_Number,BRW1.Q.job:EDI_Batch_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB4.Init(Manufacturer_Temp,?Manufacturer_Temp,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(man:Manufacturer_Key)
  FDCB4.AddField(man:Manufacturer,FDCB4.Q.man:Manufacturer)
  FDCB4.AddField(man:RecordNumber,FDCB4.Q.man:RecordNumber)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:DISCOUNT.Close
    Relate:EDIBATCH.Close
    Relate:EXCHANGE.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('EDI_Claims','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisThreadActive
    ThreadQ.Proc='EDI_Claims'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'EDI_Claims',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBS
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Xplore1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  Xplore1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  Xplore1.AddField(job:ESN,BRW1.Q.job:ESN)
  Xplore1.AddField(job:MSN,BRW1.Q.job:MSN)
  Xplore1.AddField(job:Warranty_Charge_Type,BRW1.Q.job:Warranty_Charge_Type)
  Xplore1.AddField(job:Repair_Type_Warranty,BRW1.Q.job:Repair_Type_Warranty)
  Xplore1.AddField(job:Courier_Cost_Warranty,BRW1.Q.job:Courier_Cost_Warranty)
  Xplore1.AddField(job:Labour_Cost_Warranty,BRW1.Q.job:Labour_Cost_Warranty)
  Xplore1.AddField(job:Parts_Cost_Warranty,BRW1.Q.job:Parts_Cost_Warranty)
  Xplore1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  Xplore1.AddField(job:Manufacturer,BRW1.Q.job:Manufacturer)
  Xplore1.AddField(job:EDI,BRW1.Q.job:EDI)
  Xplore1.AddField(job:EDI_Batch_Number,BRW1.Q.job:EDI_Batch_Number)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,job:EDI_Key) !Xplore Sort Order for File Sequence
  BRW1.AddRange(job:EDI)                              !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Valudate_Claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valudate_Claims, Accepted)
      Case MessageEx('This will valuate the pending claims.'&|
        '<13,10>'&|
        '<13,10>(This may take a long time depending on the amount of claims)'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              recordspercycle     = 25
              recordsprocessed    = 0
              percentprogress     = 0
              open(progresswindow)
              progress:thermometer    = 0
              ?progress:pcttext{prop:text} = '0% Completed'
      
          !---before routine
      
              claim_value$ = 0
      
          !Count records
      
              count_records# = 0
              save_job_id = access:jobs.savefile()
              job:manufacturer    = manufacturer_temp
              job:edi             = 'NO'
              Set(job:edi_key,job:edi_key)
              Loop
                  If access:jobs.next()
                      Break
                  End!If access:jobs.next()
                  If job:manufacturer     <> manufacturer_temp    |
                      Or job:edi          <> 'NO'                 |
                      Then Break.
                  count_records# += 1
              End!Loop
              access:jobs.restorefile(save_job_id)
      
              recordstoprocess    = count_records#
      
              count# = 0
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:edi_key)
              job:manufacturer     = manufacturer_temp
              job:edi              = 'NO'
              set(job:edi_key,job:edi_key)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:manufacturer     <> manufacturer_temp      |
                  or job:edi              <> 'NO'      |
                      then break.  ! end if
          !---insert routine
                  do getnextrecord2
                  cancelcheck# += 1
                  if cancelcheck# > (recordstoprocess/100)
                      do cancelcheck
                      if tmp:cancel = 1
                          break
                      end!if tmp:cancel = 1
                      cancelcheck# = 0
                  end!if cancelcheck# > 50
                  If job:ignore_warranty_charges <> 'YES'
                      Pricing_Routine('W',labour",parts",pass",a")
                      If pass" = True
                          job:labour_cost_warranty = Round(labour",.01)
                          job:parts_cost_warranty  = Round(parts",.01)
                          job:sub_total_warranty = Round(job:labour_cost_warranty,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01)
                          access:jobs.update()
                      End!If pass" = False
                  End!If job:ignore_warranty_charges <> 'YES'
                  count# += 1
                  claim_value$ += Round((Round(job:labour_cost_warranty,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01)),.01)
              end !loop
              access:jobs.restorefile(save_job_id)
          !---after routine
              do endprintrun
              close(progresswindow)
              Case MessageEx('Number of Claims: ' & count# & |
                '<13,10>'&|
                '<13,10>Value of Claims: ' & Format(claim_value$,@n14.2) & '.','ServiceBase 2000',|
                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
      Of 2 ! &No Button
      End!Case MessageEx
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valudate_Claims, Accepted)
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
            !5043 - Only show the fault codes that apply to the selected Manufacturer  (DBH: 02-12-2004)
            tmp:BrowseString = '36R(2)|M~Job No~L@P<<<<<<<<<<<<<<#PB@65L(2)|M~Account Number~@s15@69L(2)|M~Model Number' &|
                 '~@s30@69L(2)|M~ESN/IMEI~@s16@61L(2)|M~MSN~@s14@74L(2)|M~Charge Type~@s30@74L(2)|' &|
                 'M~Repair Type~@s30@33L(2)|M~Courier~@n7.2@33L(2)|M~Labour~@n7.2@54L(2)|M~Parts~@' &|
                 'n7.2@120L(2)|M~Order Number~@s30@'
            
            Loop x# = 1 To 12
                Access:MANFAULT.ClearKey(maf:Field_Number_Key)
                maf:Manufacturer = Manufacturer_Temp
                maf:Field_Number = x#
                If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
                    !Found
                    If x# = 11 Or x# = 12
                        tmp:BrowseString = Clip(tmp:BrowseString) & '120L(2)|M~Fault Code ' & x# & '~@s255@'
                    Else ! If x# = 11 Or x# = 12
                        tmp:BrowseString = Clip(tmp:BrowseString) & '120L(2)|M~Fault Code ' & x# & '~@s30@'
                    End ! If x# = 11 Or x# = 12
      
                Else !If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
                    !Error
                End !If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
            End ! Loop x# = 1 To 12
            
            ?Browse:1{prop:Format} = tmp:BrowseString
        
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    OF ?edi_process
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?edi_process, Accepted)
      exception# = 0
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = Manufacturer_Temp
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          ! Found
      
      Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          ! Error
      End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      If man:EDIFileType = 'NOKIA'
          Case MessageEx('E.D.I. Process.  Please note:-'&|
            '<13,10>'&|
            '<13,10>1. Accessory Parts will be automatically removed from this claim and will appear on an "Accessory Report" generated afterwards.'&|
            '<13,10>'&|
            '<13,10>2. Accessory Only jobs will be Excluded from this claim.'&|
            '<13,10>'&|
            '<13,10>3. Third Party Repairs will be Excluded from this claim.'&|
            '<13,10>'&|
            '<13,10>Click ''OK'' to begin.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK|&Cancel',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
                  exception# = 1
                  Nokia_Edi(0)
              Of 2 ! &Cancel Button
          End!Case MessageEx
      
      End!If manufacturer_temp = 'NOKIA'
      If man:EDIFileType = 'NEC'
          Case MessageEx('E.D.I. Process.  Please note this procedure will :-'&|
            '<13,10>'&|
            '<13,10>1. Create a Primary Export File containing up to 5 parts.'&|
            '<13,10>'&|
            '<13,10>2. Create a Secondary Export File containing any records with MORE than 5 parts.'&|
            '<13,10>(N.B. The path for this file will be the same as the Primary File. The format will be CSV and labelled "NEC" followed by the Batch Number)'&|
            '<13,10>'&|
            '<13,10>Click ''OK'' to begin.','ServiceBase 2000',|
                         'Styles\warn.ico','|&OK|&Cancel',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
                  exception# = 1
                  NEC_Edi(0)
              Of 2 ! &Cancel Button
          End!Case MessageEx
      End!If manufacturer_temp = 'NEC'
      If exception# = 0
      
      Case MessageEx('Are you sure you want to run the EDI Process?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Case man:EDIFileType
                  Of 'ALCATEL'
                      Alcatel_EDI(0)
                  Of 'BOSCH'
                      Bosch_EDI(0)
                  Of 'ERICSSON'
                      Ericsson_EDI(0)
                  Of 'MAXON'
                      Maxon_EDI(0)
                  Of 'MOTOROLA'
                      Motorola_EDI(0)
                  Of 'SHARP'
                      Sharp_Excel_Edi(0)
                  Of 'SIEMENS'
                      Siemens_Edi(0)
                  Of 'SIEMENS DECT'
                      Siemens_Dect_Edi(0)
                  Of 'TELITAL'
                      Telital_EDI(0)
                  Of 'SAMSUNG'
                      Samsung_EDI(0)
                  Of 'MITSUBISHI'
                      Mitsibushi_EDI(0)
                  Of 'SAGEM'
                      Sagem_EDI(0)
                  Of 'PANASONIC'
                      Panasonic_EDI(0)
                  Of 'SONY'
                      Sony_EDI(0)
                  ! Start Change 3524 BE(14/01/04)
                  OF 'SENDO'
                      Sendo_EDI(0)
                  ! End Change 3524 BE(14/01/04)
                  ! Start Change 4018 BE(06/04/04)
                  OF 'VK'
                      VK_EDI(0)
                  ! End Change 4018 BE(06/04/04)
                  ! Start Change 3516 BE(27/09/04)
                  OF 'LG'
                      LG_EDI(0)
                  ! End Change 3516 BE(27/09/04)
                  Of 'STANDARD FORMAT'
                      ! Call ARC's Standard CSV - TrkBs: 5855 (DBH: 28-09-2005)
                      StandardFormat_EDI(0,Manufacturer_Temp)
              End
          Of 2 ! &No Button
          End!Case MessageEx
      End!If exception# = 0
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?edi_process, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'EDI_Claims')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Manufacturer_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = Manufacturer_Temp
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = 'NO'
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  Charge_Repair_Type_Temp = CLIP(job:Warranty_Charge_Type) & ' / ' & CLIP(job:Repair_Type_Warranty)
  ! Start Change 4585 BE(04/10/2004)
  jfc1 = FormatJobFaultCode(job:Fault_Code1, 1, job:Manufacturer)
  jfc2 = FormatJobFaultCode(job:Fault_Code2, 2, job:Manufacturer)
  jfc3 = FormatJobFaultCode(job:Fault_Code3, 3, job:Manufacturer)
  jfc4 = FormatJobFaultCode(job:Fault_Code4, 4, job:Manufacturer)
  jfc5 = FormatJobFaultCode(job:Fault_Code5, 5, job:Manufacturer)
  jfc6 = FormatJobFaultCode(job:Fault_Code6, 6, job:Manufacturer)
  jfc7 = FormatJobFaultCode(job:Fault_Code7, 7, job:Manufacturer)
  jfc8 = FormatJobFaultCode(job:Fault_Code8, 8, job:Manufacturer)
  jfc9 = FormatJobFaultCode(job:Fault_Code9, 9, job:Manufacturer)
  jfc10 = FormatJobFaultCode(job:Fault_Code10, 10, job:Manufacturer)
  jfc11 = FormatJobFaultCode(job:Fault_Code11, 11, job:Manufacturer)
  jfc12 = FormatJobFaultCode(job:Fault_Code12, 12, job:Manufacturer)
  ! End Change 4585 BE(04/10/2004)
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(1)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !job:Ref_Number
  OF 2 !job:Account_Number
  OF 3 !job:Model_Number
  OF 4 !job:ESN
  OF 5 !job:MSN
  OF 6 !job:Warranty_Charge_Type
  OF 7 !job:Repair_Type_Warranty
  OF 8 !job:Courier_Cost_Warranty
  OF 9 !job:Labour_Cost_Warranty
  OF 10 !job:Parts_Cost_Warranty
  OF 11 !job:Order_Number
  OF 12 !jfc1
  OF 13 !jfc2
  OF 14 !jfc3
  OF 15 !jfc4
  OF 16 !jfc5
  OF 17 !jfc6
  OF 18 !jfc7
  OF 19 !jfc8
  OF 20 !jfc9
  OF 21 !jfc10
  OF 22 !jfc11
  OF 23 !jfc12
  OF 24 !job:Manufacturer
  OF 25 !job:EDI
  OF 26 !job:EDI_Batch_Number
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step26,job:EDI_Key)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,job:EDI_Key)
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(,job:Ref_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,job:Account_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,job:Model_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(,job:ESN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step5.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator5)
      Xplore1Locator5.Init(,job:MSN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,job:Warranty_Charge_Type,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step7.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator7)
      Xplore1Locator7.Init(,job:Repair_Type_Warranty,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step8.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator8)
      Xplore1Locator8.Init(,job:Courier_Cost_Warranty,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step9.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator9)
      Xplore1Locator9.Init(,job:Labour_Cost_Warranty,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step10.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator10)
      Xplore1Locator10.Init(,job:Parts_Cost_Warranty,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator11)
      Xplore1Locator11.Init(,job:Order_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step12.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator12)
      Xplore1Locator12.Init(,jfc1,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step13.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator13)
      Xplore1Locator13.Init(,jfc2,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step14.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator14)
      Xplore1Locator14.Init(,jfc3,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step15.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator15)
      Xplore1Locator15.Init(,jfc4,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step16.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator16)
      Xplore1Locator16.Init(,jfc5,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step17.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator17)
      Xplore1Locator17.Init(,jfc6,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step18.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator18)
      Xplore1Locator18.Init(,jfc7,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step19.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator19)
      Xplore1Locator19.Init(,jfc8,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step20.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator20)
      Xplore1Locator20.Init(,jfc9,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step21.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator21)
      Xplore1Locator21.Init(,jfc10,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step22.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator22)
      Xplore1Locator22.Init(,jfc11,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step23.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator23)
      Xplore1Locator23.Init(,jfc12,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step24.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator24)
      Xplore1Locator24.Init(,job:Manufacturer,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step25.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator25)
      Xplore1Locator25.Init(,job:EDI,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step26.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator26)
      Xplore1Locator26.Init(,job:EDI_Batch_Number,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(job:EDI)
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBS)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@P<<<<<<<<<<<<<<#PB')        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('ESN/IMEI')                   !Header
                PSTRING('@s16')                       !Picture
                PSTRING('ESN/IMEI')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:MSN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('MSN')                        !Header
                PSTRING('@s14')                       !Picture
                PSTRING('MSN')                        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Warranty_Charge_Type')   !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Charge Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Charge Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Repair_Type_Warranty')   !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Repair Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Repair Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Courier_Cost_Warranty')  !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Courier')                    !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Courier')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Labour_Cost_Warranty')   !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Labour')                     !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Labour')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Parts_Cost_Warranty')    !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Parts')                      !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Parts')                      !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Order_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Order Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Order Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc1')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc1')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc1')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc2')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc2')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc2')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc3')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc3')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc3')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc4')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc4')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc4')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc5')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc5')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc5')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc6')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc6')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc6')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc7')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc7')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc7')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc8')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc8')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc8')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc9')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc9')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc9')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc10')                      !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc10')                      !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc10')                      !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc11')                      !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc11')                      !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc11')                      !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('jfc12')                      !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc12')                      !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc12')                      !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Manufacturer')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Manufacturer')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Manufacturer')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:EDI')                    !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('EDI')                        !Header
                PSTRING('@s3')                        !Picture
                PSTRING('EDI')                        !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:EDI_Batch_Number')       !Field Name
                SHORT(24)                             !Default Column Width
                PSTRING('EDI Batch Number')           !Header
                PSTRING('@n6')                        !Picture
                PSTRING('EDI Batch Number')           !Description
                STRING('D')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(26)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

