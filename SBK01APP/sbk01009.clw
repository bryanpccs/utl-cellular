

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01009.INC'),ONCE        !Local module procedure declarations
                     END


Update_Payment_Types PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::pay:Record  LIKE(pay:RECORD),STATIC
QuickWindow          WINDOW('Update the PAYTYPES File'),AT(,,220,84),FONT('Tahoma',8,,),CENTER,IMM,ICON('Pc.ico'),HLP('Update_Payment_Types'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,48),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Payment Type'),AT(8,20),USE(?PAY:Payment_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(pay:Payment_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Credit Card'),AT(84,36,70,8),USE(pay:Credit_Card),VALUE('YES','NO')
                         END
                       END
                       PANEL,AT(4,56,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,60,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(156,60,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?PAY:Payment_Type:Prompt{prop:FontColor} = -1
    ?PAY:Payment_Type:Prompt{prop:Color} = 15066597
    If ?pay:Payment_Type{prop:ReadOnly} = True
        ?pay:Payment_Type{prop:FontColor} = 65793
        ?pay:Payment_Type{prop:Color} = 15066597
    Elsif ?pay:Payment_Type{prop:Req} = True
        ?pay:Payment_Type{prop:FontColor} = 65793
        ?pay:Payment_Type{prop:Color} = 8454143
    Else ! If ?pay:Payment_Type{prop:Req} = True
        ?pay:Payment_Type{prop:FontColor} = 65793
        ?pay:Payment_Type{prop:Color} = 16777215
    End ! If ?pay:Payment_Type{prop:Req} = True
    ?pay:Payment_Type{prop:Trn} = 0
    ?pay:Payment_Type{prop:FontStyle} = font:Bold
    ?pay:Credit_Card{prop:Font,3} = -1
    ?pay:Credit_Card{prop:Color} = 15066597
    ?pay:Credit_Card{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Payment_Types',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Payment_Types',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Payment_Types',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Payment_Types',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PAY:Payment_Type:Prompt;  SolaceCtrlName = '?PAY:Payment_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pay:Payment_Type;  SolaceCtrlName = '?pay:Payment_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?pay:Credit_Card;  SolaceCtrlName = '?pay:Credit_Card';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Payment Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Payment Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Payment_Types')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Payment_Types')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PAY:Payment_Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(pay:Record,History::pay:Record)
  SELF.AddHistoryField(?pay:Payment_Type,1)
  SELF.AddHistoryField(?pay:Credit_Card,2)
  SELF.AddUpdateFile(Access:PAYTYPES)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PAYTYPES.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PAYTYPES
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PAYTYPES.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Payment_Types',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    pay:Credit_Card = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Payment_Types')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

