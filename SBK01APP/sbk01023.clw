

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01023.INC'),ONCE        !Local module procedure declarations
                     END


Print_Routines PROCEDURE                              !Generated from procedure template - Window

FilesOpened          BYTE
sav:path             STRING(255)
invoice_number_temp  REAL
report_type_temp     STRING(2)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Reports'),AT(,,312,256),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,304,220),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           OPTION('Select A Report'),AT(8,8,296,212),USE(report_type_temp),BOXED,COLOR(COLOR:Silver)
                             RADIO('Job Receipt'),AT(40,32),USE(?report_type_temp:Radio1),VALUE('1')
                             RADIO('Virgin Label'),AT(40,144),USE(?report_type_temp:Radio16),VALUE('16')
                             RADIO('Bouncer Report'),AT(40,96),USE(?report_type_temp:Radio12),VALUE('12')
                             RADIO('Job Card'),AT(40,48),USE(?report_type_temp:Radio2),VALUE('2')
                             RADIO('Job Label'),AT(40,64),USE(?report_type_temp:Radio3),VALUE('3')
                             RADIO('Delivery Label'),AT(184,88),USE(?report_type_temp:Radio13),VALUE('13')
                             RADIO('Letters'),AT(40,80),USE(?report_type_temp:Radio10),VALUE('10')
                             RADIO('Mail Merge'),AT(40,112),USE(?report_type_temp:Radio10:2),VALUE('11')
                             RADIO('Collection Label'),AT(184,104),USE(?report_type_temp:Radio14),VALUE('14')
                             RADIO('Exchange Unit Label'),AT(40,176),USE(?report_type_temp:Radio11),VALUE('4')
                             RADIO('Loan Unit Label'),AT(40,192),USE(?report_type_temp:Radio11:2),VALUE('5')
                             RADIO('ANC Collection Note'),AT(184,56),USE(?report_type_temp:Radio6),VALUE('6')
                             RADIO('Despatch Note'),AT(184,72),USE(?report_type_temp:Radio7),VALUE('7')
                             RADIO('Estimate'),AT(184,176),USE(?report_type_temp:Radio8),VALUE('8')
                             RADIO('Invoice'),AT(184,192),USE(?report_type_temp:Radio9),VALUE('9')
                             RADIO('Reprint Pick Note(s)'),AT(40,128),USE(?report_type_temp:Radio15),VALUE('15')
                           END
                         END
                       END
                       GROUP('Job Reports'),AT(24,20,124,140),USE(?Group5),BOXED,COLOR(COLOR:Silver)
                       END
                       GROUP('Loan/Exchange Reports'),AT(24,164,124,48),USE(?Group5:2),BOXED,COLOR(COLOR:Silver)
                       END
                       GROUP('Collection/Despatch Report'),AT(164,20,124,140),USE(?Group5:3),BOXED,COLOR(COLOR:Silver)
                       END
                       GROUP('Financial Reports'),AT(164,164,124,48),USE(?Group5:4),BOXED,COLOR(COLOR:Silver)
                       END
                       PANEL,AT(4,228,304,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Close'),AT(248,232,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       BUTTON('Print'),AT(8,232,56,16),USE(?print),LEFT,ICON(ICON:Print1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
WindowsDir      LPSTR(144)
SystemDir       WORD
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?report_type_temp{prop:Font,3} = -1
    ?report_type_temp{prop:Color} = 15066597
    ?report_type_temp{prop:Trn} = 0
    ?report_type_temp:Radio1{prop:Font,3} = -1
    ?report_type_temp:Radio1{prop:Color} = 15066597
    ?report_type_temp:Radio1{prop:Trn} = 0
    ?report_type_temp:Radio16{prop:Font,3} = -1
    ?report_type_temp:Radio16{prop:Color} = 15066597
    ?report_type_temp:Radio16{prop:Trn} = 0
    ?report_type_temp:Radio12{prop:Font,3} = -1
    ?report_type_temp:Radio12{prop:Color} = 15066597
    ?report_type_temp:Radio12{prop:Trn} = 0
    ?report_type_temp:Radio2{prop:Font,3} = -1
    ?report_type_temp:Radio2{prop:Color} = 15066597
    ?report_type_temp:Radio2{prop:Trn} = 0
    ?report_type_temp:Radio3{prop:Font,3} = -1
    ?report_type_temp:Radio3{prop:Color} = 15066597
    ?report_type_temp:Radio3{prop:Trn} = 0
    ?report_type_temp:Radio13{prop:Font,3} = -1
    ?report_type_temp:Radio13{prop:Color} = 15066597
    ?report_type_temp:Radio13{prop:Trn} = 0
    ?report_type_temp:Radio10{prop:Font,3} = -1
    ?report_type_temp:Radio10{prop:Color} = 15066597
    ?report_type_temp:Radio10{prop:Trn} = 0
    ?report_type_temp:Radio10:2{prop:Font,3} = -1
    ?report_type_temp:Radio10:2{prop:Color} = 15066597
    ?report_type_temp:Radio10:2{prop:Trn} = 0
    ?report_type_temp:Radio14{prop:Font,3} = -1
    ?report_type_temp:Radio14{prop:Color} = 15066597
    ?report_type_temp:Radio14{prop:Trn} = 0
    ?report_type_temp:Radio11{prop:Font,3} = -1
    ?report_type_temp:Radio11{prop:Color} = 15066597
    ?report_type_temp:Radio11{prop:Trn} = 0
    ?report_type_temp:Radio11:2{prop:Font,3} = -1
    ?report_type_temp:Radio11:2{prop:Color} = 15066597
    ?report_type_temp:Radio11:2{prop:Trn} = 0
    ?report_type_temp:Radio6{prop:Font,3} = -1
    ?report_type_temp:Radio6{prop:Color} = 15066597
    ?report_type_temp:Radio6{prop:Trn} = 0
    ?report_type_temp:Radio7{prop:Font,3} = -1
    ?report_type_temp:Radio7{prop:Color} = 15066597
    ?report_type_temp:Radio7{prop:Trn} = 0
    ?report_type_temp:Radio8{prop:Font,3} = -1
    ?report_type_temp:Radio8{prop:Color} = 15066597
    ?report_type_temp:Radio8{prop:Trn} = 0
    ?report_type_temp:Radio9{prop:Font,3} = -1
    ?report_type_temp:Radio9{prop:Color} = 15066597
    ?report_type_temp:Radio9{prop:Trn} = 0
    ?report_type_temp:Radio15{prop:Font,3} = -1
    ?report_type_temp:Radio15{prop:Color} = 15066597
    ?report_type_temp:Radio15{prop:Trn} = 0
    ?Group5{prop:Font,3} = -1
    ?Group5{prop:Color} = 15066597
    ?Group5{prop:Trn} = 0
    ?Group5:2{prop:Font,3} = -1
    ?Group5:2{prop:Color} = 15066597
    ?Group5:2{prop:Trn} = 0
    ?Group5:3{prop:Font,3} = -1
    ?Group5:3{prop:Color} = 15066597
    ?Group5:3{prop:Trn} = 0
    ?Group5:4{prop:Font,3} = -1
    ?Group5:4{prop:Color} = 15066597
    ?Group5:4{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Create_Invoice      Routine
    Case MessageEx('Are you sure you want to print a Single Invoice?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
            If CanInvoiceBePrinted(glo:Select1,1) = Level:Benign
                Access:JOBS.Clearkey(job:Ref_Number_Key)
                job:Ref_NUmber  = glo:Select1
                If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Found
                    If CreateInvoice() = Level:Benign
                        glo:Select1  = job:Invoice_Number
                        Single_Invoice
                        glo:Select1  = job:Ref_Number
                    End !If CreateInvoice() = Level:Benign
                Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                
            End !If CanInvoiceBePrinted(glo:Select1) = Level:Benign
        Of 2 ! &No Button
    End!Case MessageEx


!    SystemDir   = GetSystemDirectory(WindowsDir,Size(WindowsDir))
!    Set(defaults)
!    access:defaults.next()
!    invoice_number_temp = 0
!    Case MessageEx('Are you sure you want to print a Single Invoice?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!        Of 1 ! &Yes Button
!            error# = 0
!            access:jobs.clearkey(job:ref_number_key)
!            job:ref_number = glo:select1
!            if access:jobs.fetch(job:ref_number_key) = Level:Benign
!                access:jobnotes.clearkey(jbn:RefNumberKey)
!                jbn:RefNumber   = job:ref_number
!                access:jobnotes.tryfetch(jbn:RefNumberKey)
!
!                If CanInvoiceBePrinted(job:Ref_Number) = Level:Benign
!                    If job:Ignore_Chargeable_Charges <> 'YES'
!                        Pricing_Routine('C',labour",parts",pass",a")
!                        If pass" = False
!                            Case MessageEx('You are attempting to Invoice a job for which a Default Charge Structure has not been established.<13,10><13,10>If you wish to proceed you must access the Chargeable Tab and tick the "Ignore Default Charge" option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                Of 1 ! &OK Button
!                            End!Case MessageEx
!                            error# = 1
!                        Else!If pass" = False
!                            job:labour_cost = labour"
!                            job:parts_cost  = parts"
!                            job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
!                        End!If pass" = False
!                    End !If job:Ignore_Chargeable_Charges <> 'YES'
!
!                    If InvoiceSubAccounts(job:Account_Number)
!
!                    Else !If InvoiceSubAccounts(job:Account_Number)
!
!                    End !If InvoiceSubAccounts(job:Account_Number)
!                End !If CanInvoiceBePrinted(job:Ref_Number) = Level:Benign
!
!                If job:bouncer <> ''
!                    Case MessageEx('This job has been marked as a Bouncer.<13,10><13,10>You must authorise this job for Invoicing before you can continue.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                        Of 1 ! &Close Button
!                    End!Case MessageEx
!                    error# = 1
!                End!If job:bouncer <> ''
!                If job:invoice_number <> '' and error# = 0
!                    access:invoice.clearkey(inv:invoice_number_key)
!                    inv:invoice_number = job:invoice_number
!                    if access:invoice.fetch(inv:invoice_number_key) = Level:Benign
!                        If inv:invoice_type = 'SIN'
!                            glo:select1 = inv:invoice_number
!                            Single_Invoice
!                            glo:select1 = ''
!                        Else!End!If inv:invoice_type = 'SIN'
!                            Case MessageEx('The selected job is part of a Multiple Invoice, or is a Warranty job.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                                Of 1 ! &Close Button
!                            End!Case MessageEx
!                        End!If inv:invoice_type = 'SIN'
!                    end
!
!                Else!If job:invoice_number <> ''
!                    error# = 0
!                    If job:chargeable_job <> 'YES'
!                        Case MessageEx('You are attempting to Invoice a Warranty Only Job.<13,10><13,10>This can only be processed through the Warranty Procedures.','ServiceBase 2000','Styles\stop.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                            Of 1 ! &Close Button
!                        End!Case MessageEx
!                        error# = 1
!                    End!If job:chargeable_job <> 'YES'
!                    If job:date_completed <> '' And error# = 0
!                        Set(defaults)
!                        access:defaults.next()
!                        If def:qa_required = 'YES'
!                            If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
!                                Case MessageEx('The selected job has not passed QA checks','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
!                                    Of 1 ! &OK Button
!                                End!Case MessageEx
!                                error# = 1
!                            End!If job:qa_passed <> 'YES' AND job:qa_second_passed <> 'YES'
!                        End!If def:qa_required = 'YES'
!                    Else!If job:date_completed <> ''
!                        Case MessageEx('The selected job has not been completed.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
!                            Of 1 ! &OK Button
!                        End!Case MessageEx
!                        error# = 1
!                    End!If job:date_completed <> ''
!                    If job:ignore_chargeable_charges = 'YES' And error# = 0
!                        If job:sub_total = 0
!                            Case MessageEx('You are attempting to invoice a job than has not been priced.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,800) 
!                                Of 1 ! &OK Button
!                            End!Case MessageEx
!                            error# = 1
!                        End
!                    Else!If job:ignore_chargeable_charges = 'YES'
!                        If error# = 0
!                            Pricing_Routine('C',labour",parts",pass",a")
!                            If pass" = False
!                                Case MessageEx('You are attempting to Invoice a job for which a Default Charge Structure has not been established.<13,10><13,10>If you wish to proceed you must access the Chargeable Tab and tick the "Ignore Default Charge" option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                    Of 1 ! &OK Button
!                                End!Case MessageEx
!                                error# = 1
!                            Else!If pass" = False
!                                job:labour_cost = labour"
!                                job:parts_cost  = parts"
!                                job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
!                            End!If pass" = False
!                        End!If error# = 0
!                    End!If job:ignore_chargeable_charges = 'YES'
!                    If error# = 0
!                        fetch_error# = 0
!                        despatch# = 0
!                        access:subtracc.clearkey(sub:account_number_key)
!                        sub:account_number = job:account_number
!                        if access:subtracc.fetch(sub:account_number_key)
!                            Case MessageEx('Error! Cannot find the Sub Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                Of 1 ! &OK Button
!                            End!Case MessageEx
!                        Else!if access:subtracc.fetch(sub:account_number_key) = level:benign
!                            access:tradeacc.clearkey(tra:account_number_key) 
!                            tra:account_number = sub:main_account_number
!                            if access:tradeacc.fetch(tra:account_number_key)
!                                Case MessageEx('Error! Cannot find the Trade Account Details.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                    Of 1 ! &OK Button
!                                End!Case MessageEx
!                                fetch_error# = 1
!                            Else!if access:tradeacc.fetch(tra:account_number_key)
!                                If tra:use_sub_accounts = 'YES'
!                                    If sub:despatch_invoiced_jobs = 'YES'
!                                        If sub:despatch_paid_jobs = 'YES'
!                                            If job:paid = 'YES'
!                                                despatch# = 1
!                                            Else
!                                                despatch# = 2
!                                            End
!                                        Else
!                                            despatch# = 1
!                                        End!If sub:despatch_paid_jobs = 'YES' And job:paid = 'YES'
!                                        
!                                    End!If sub:despatch_invoiced_jobs = 'YES'
!                                End!If tra:use_sub_accounts = 'YES'
!                                if tra:invoice_sub_accounts = 'YES'
!                                    vat_number" = sub:vat_number
!                                    access:vatcode.clearkey(vat:vat_code_key)
!                                    vat:vat_code = sub:labour_vat_code
!                                    if access:vatcode.fetch(vat:vat_code_key)
!                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                            Of 1 ! &OK Button
!                                        End!Case MessageEx
!                                        fetch_error# = 1
!                                    Else!if access:vatcode.fetch(vat:vat_code_key)
!                                       labour_rate$ = vat:vat_rate
!                                    end!if access:vatcode.fetch(vat:vat_code_key)
!                                    access:vatcode.clearkey(vat:vat_code_key)
!                                    vat:vat_code = sub:parts_vat_code
!                                    if access:vatcode.fetch(vat:vat_code_key)
!                                        fetch_error# = 1
!                                            Case MessageEx('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                                                Of 1 ! &OK Button
!                                            End!Case MessageEx
!                                    Else!if access:vatcode.fetch(vat:vat_code_key)
!                                       parts_rate$ = vat:vat_rate
!                                    end!if access:vatcode.fetch(vat:vat_code_key)
!                                else!if tra:use_sub_accounts = 'YES'
!                                    If tra:despatch_invoiced_jobs = 'YES'
!                                        If tra:despatch_paid_jobs = 'YES'
!                                            If job:paid = 'YES'
!                                                despatch# = 1
!                                            Else
!                                                despatch# = 2
!                                            End!If job:paid = 'YES'
!                                        Else
!                                            despatch# = 1
!                                        End!If tra:despatch_paid_jobs = 'YES' and job:paid = 'YES'
!                                    End!If tra:despatch_invoiced_jobs = 'YES'
!                                    vat_number" = tra:vat_number
!                                    access:vatcode.clearkey(vat:vat_code_key)
!                                    vat:vat_code = tra:labour_vat_code
!                                    if access:vatcode.fetch(vat:vat_code_key)
!                                        fetch_error# = 1
!                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Trade Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
!                                            Of 1 ! &OK Button
!                                        End!Case MessageEx
!                                    Else!if access:vatcode.fetch(vat:vat_code_key)
!                                       labour_rate$ = vat:vat_rate
!                                    end
!                                    access:vatcode.clearkey(vat:vat_code_key)
!                                    vat:vat_code = tra:parts_vat_code
!                                    if access:vatcode.fetch(vat:vat_code_key)
!                                        fetch_error# = 1
!                                        Case MessageEx('Error! A V.A.T. Rate has not been setup for this Sub Account.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                                            Of 1 ! &OK Button
!                                        End!Case MessageEx
!                                    Else!if access:vatcode.fetch(vat:vat_code_key)
!                                       parts_rate$ = vat:vat_rate
!                                    end
!                                end!if tra:use_sub_accounts = 'YES'
!                            end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
!                        end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
!                        If fetch_error# = 0
!        !SAGE BIT
!                            If def:use_sage = 'YES'
!                                glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
!                                Remove(paramss)
!                                access:paramss.open()
!                                access:paramss.usefile()
!             !LABOUR
!                                get(paramss,0)
!                                if access:paramss.primerecord() = Level:Benign
!                                    prm:account_ref       = Stripcomma(job:Account_number)
!                                    If tra:invoice_sub_accounts = 'YES'
!                                        If sub:invoice_customer_address <> 'YES'
!                                            prm:name              = Stripcomma(sub:company_name)
!                                            prm:address_1         = Stripcomma(sub:address_line1)
!                                            prm:address_2         = Stripcomma(sub:address_line2)
!                                            prm:address_3         = Stripcomma(sub:address_line3)
!                                            prm:address_5         = Stripcomma(sub:postcode)
!                                        Else!If tra:invoice_customer_address = 'YES'
!                                            prm:name              = Stripcomma(job:company_name)
!                                            prm:address_1         = Stripcomma(job:address_line1)
!                                            prm:address_2         = Stripcomma(job:address_line2)
!                                            prm:address_3         = Stripcomma(job:address_line3)
!                                            prm:address_5         = Stripcomma(job:postcode)
!
!                                        End!If tra:invoice_customer_address = 'YES'
!                                    Else!If tra:invoice_sub_accounts = 'YES'
!                                        If tra:invoice_customer_address <> 'YES'
!                                            prm:name              = Stripcomma(tra:company_name)
!                                            prm:address_1         = Stripcomma(tra:address_line1)
!                                            prm:address_2         = Stripcomma(tra:address_line2)
!                                            prm:address_3         = Stripcomma(tra:address_line3)
!                                            prm:address_5         = Stripcomma(tra:postcode)
!                                        Else!If tra:invoice_customer_address = 'YES'
!                                            prm:name              = Stripcomma(job:company_name)
!                                            prm:address_1         = Stripcomma(job:address_line1)
!                                            prm:address_2         = Stripcomma(job:address_line2)
!                                            prm:address_3         = Stripcomma(job:address_line3)
!                                            prm:address_5         = Stripcomma(job:postcode)
!                                        End!If tra:invoice_customer_address = 'YES'
!                                    End!If tra:invoice_sub_accounts = 'YES'
!                                    prm:address_4         = ''
!                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
!                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
!                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
!                                    prm:del_address_4     = ''
!                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
!                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
!                                    If job:surname <> ''
!                                        if job:title = '' and job:initial = '' 
!                                            prm:contact_name = Stripcomma(clip(job:surname))
!                                        elsif job:title = '' and job:initial <> ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
!                                        elsif job:title <> '' and job:initial = ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
!                                        elsif job:title <> '' and job:initial <> ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
!                                        else
!                                            prm:contact_name = ''
!                                        end
!                                    else
!                                        prm:contact_name = ''
!                                    end
!                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
!                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
!                                    prm:notes_3           = ''
!                                    prm:taken_by          = Stripcomma(job:who_booked)
!                                    prm:order_number      = Stripcomma(job:order_number)
!                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
!                                    prm:payment_ref       = ''
!                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!                                    prm:global_details    = ''
!                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
!                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
!                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
!                                                                Round(job:courier_cost * (labour_rate$/100),.01))
!                                    prm:stock_code        = Stripcomma(DEF:Labour_Stock_Code)
!                                    prm:description       = Stripcomma(DEF:Labour_Description)
!                                    prm:nominal_code      = Stripcomma(DEF:Labour_Code)
!                                    prm:qty_order         = '1'
!                                    prm:unit_price        = '0'
!                                    prm:net_amount        = Stripcomma(job:labour_cost)
!                                    prm:tax_amount        = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01))
!                                    prm:comment_1         = Stripcomma(job:charge_type)
!                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
!                                    prm:unit_of_sale      = '0'
!                                    prm:full_net_amount   = '0'
!                                    prm:invoice_date      = '*'
!                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!                                    prm:user_name         = Clip(DEF:User_Name_Sage)
!                                    prm:password          = Clip(DEF:Password_Sage)
!                                    prm:set_invoice_number= '*'
!                                    prm:invoice_no        = '*'
!                                    access:paramss.insert()
!                                end!if access:paramss.primerecord() = Level:Benign
!            !PARTS
!                                get(paramss,0)
!                                if access:paramss.primerecord() = Level:Benign
!                                    prm:account_ref       = Stripcomma(job:Account_number)
!                                    If tra:invoice_sub_accounts = 'YES'
!                                        If sub:invoice_customer_address <> 'YES'
!                                            prm:name              = Stripcomma(sub:company_name)
!                                            prm:address_1         = Stripcomma(sub:address_line1)
!                                            prm:address_2         = Stripcomma(sub:address_line2)
!                                            prm:address_3         = Stripcomma(sub:address_line3)
!                                            prm:address_5         = Stripcomma(sub:postcode)
!                                        Else!If tra:invoice_customer_address = 'YES'
!                                            prm:name              = Stripcomma(job:company_name)
!                                            prm:address_1         = Stripcomma(job:address_line1)
!                                            prm:address_2         = Stripcomma(job:address_line2)
!                                            prm:address_3         = Stripcomma(job:address_line3)
!                                            prm:address_5         = Stripcomma(job:postcode)
!
!                                        End!If tra:invoice_customer_address = 'YES'
!                                    Else!If tra:invoice_sub_accounts = 'YES'
!                                        If tra:invoice_customer_address <> 'YES'
!                                            prm:name              = Stripcomma(tra:company_name)
!                                            prm:address_1         = Stripcomma(tra:address_line1)
!                                            prm:address_2         = Stripcomma(tra:address_line2)
!                                            prm:address_3         = Stripcomma(tra:address_line3)
!                                            prm:address_5         = Stripcomma(tra:postcode)
!                                        Else!If tra:invoice_customer_address = 'YES'
!                                            prm:name              = Stripcomma(job:company_name)
!                                            prm:address_1         = Stripcomma(job:address_line1)
!                                            prm:address_2         = Stripcomma(job:address_line2)
!                                            prm:address_3         = Stripcomma(job:address_line3)
!                                            prm:address_5         = Stripcomma(job:postcode)
!                                        End!If tra:invoice_customer_address = 'YES'
!                                    End!If tra:invoice_sub_accounts = 'YES'
!                                    prm:address_4         = ''
!                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
!                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
!                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
!                                    prm:del_address_4     = ''
!                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
!                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
!                                    If job:surname <> ''
!                                        if job:title = '' and job:initial = '' 
!                                            prm:contact_name = Stripcomma(clip(job:surname))
!                                        elsif job:title = '' and job:initial <> ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
!                                        elsif job:title <> '' and job:initial = ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
!                                        elsif job:title <> '' and job:initial <> ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
!                                        else
!                                            prm:contact_name = ''
!                                        end
!                                    else
!                                        prm:contact_name = ''
!                                    end
!                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
!                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
!                                    prm:notes_3           = ''
!                                    prm:taken_by          = Stripcomma(job:who_booked)
!                                    prm:order_number      = Stripcomma(job:order_number)
!                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
!                                    prm:payment_ref       = ''
!                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!                                    prm:global_details    = ''
!                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
!                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
!                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
!                                                                Round(job:courier_cost * (labour_rate$/100),.01))
!                                    prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
!                                    prm:description       = Stripcomma(DEF:Parts_Description)
!                                    prm:nominal_code      = Stripcomma(DEF:Parts_Code)
!                                    prm:qty_order         = '1'
!                                    prm:unit_price        = '0'
!                                    prm:net_amount        = Stripcomma(job:parts_cost)
!                                    prm:tax_amount        = Stripcomma(Round(job:parts_cost * (parts_rate$/100),.01))
!                                    prm:comment_1         = Stripcomma(job:charge_type)
!                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
!                                    prm:unit_of_sale      = '0'
!                                    prm:full_net_amount   = '0'
!                                    prm:invoice_date      = '*'
!                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!                                    prm:user_name         = Clip(DEF:User_Name_Sage)
!                                    prm:password          = Clip(DEF:Password_Sage)
!                                    prm:set_invoice_number= '*'
!                                    prm:invoice_no        = '*'
!                                    access:paramss.insert()
!                                end!if access:paramss.primerecord() = Level:Benign
!            !COURIER
!                                get(paramss,0)
!                                if access:paramss.primerecord() = Level:Benign
!                                    prm:account_ref       = Stripcomma(job:Account_number)
!                                    If tra:invoice_sub_accounts = 'YES'
!                                        If sub:invoice_customer_address <> 'YES'
!                                            prm:name              = Stripcomma(sub:company_name)
!                                            prm:address_1         = Stripcomma(sub:address_line1)
!                                            prm:address_2         = Stripcomma(sub:address_line2)
!                                            prm:address_3         = Stripcomma(sub:address_line3)
!                                            prm:address_5         = Stripcomma(sub:postcode)
!                                        Else!If tra:invoice_customer_address = 'YES'
!                                            prm:name              = Stripcomma(job:company_name)
!                                            prm:address_1         = Stripcomma(job:address_line1)
!                                            prm:address_2         = Stripcomma(job:address_line2)
!                                            prm:address_3         = Stripcomma(job:address_line3)
!                                            prm:address_5         = Stripcomma(job:postcode)
!
!                                        End!If tra:invoice_customer_address = 'YES'
!                                    Else!If tra:invoice_sub_accounts = 'YES'
!                                        If tra:invoice_customer_address <> 'YES'
!                                            prm:name              = Stripcomma(tra:company_name)
!                                            prm:address_1         = Stripcomma(tra:address_line1)
!                                            prm:address_2         = Stripcomma(tra:address_line2)
!                                            prm:address_3         = Stripcomma(tra:address_line3)
!                                            prm:address_5         = Stripcomma(tra:postcode)
!                                        Else!If tra:invoice_customer_address = 'YES'
!                                            prm:name              = Stripcomma(job:company_name)
!                                            prm:address_1         = Stripcomma(job:address_line1)
!                                            prm:address_2         = Stripcomma(job:address_line2)
!                                            prm:address_3         = Stripcomma(job:address_line3)
!                                            prm:address_5         = Stripcomma(job:postcode)
!                                        End!If tra:invoice_customer_address = 'YES'
!                                    End!If tra:invoice_sub_accounts = 'YES'
!
!                                    prm:address_4         = ''
!                                    prm:del_address_1     = Stripcomma(job:address_line1_delivery)
!                                    prm:del_address_2     = Stripcomma(job:address_line2_delivery)
!                                    prm:del_address_3     = Stripcomma(job:address_line3_delivery)
!                                    prm:del_address_4     = ''
!                                    prm:del_address_5     = Stripcomma(job:postcode_delivery)
!                                    prm:cust_tel_number   = Stripcomma(job:telephone_number)
!                                    If job:surname <> ''
!                                        if job:title = '' and job:initial = '' 
!                                            prm:contact_name = Stripcomma(clip(job:surname))
!                                        elsif job:title = '' and job:initial <> ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:initial))
!                                        elsif job:title <> '' and job:initial = ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title))
!                                        elsif job:title <> '' and job:initial <> ''
!                                            prm:contact_name = Stripcomma(clip(job:surname) & ' ' & clip(job:title) & ' ' & clip(job:initial))
!                                        else
!                                            prm:contact_name = ''
!                                        end
!                                    else
!                                        prm:contact_name = ''
!                                    end
!                                    prm:notes_1           = Stripcomma(Stripreturn(jbn:fault_description))
!                                    prm:notes_2           = Stripcomma(Stripreturn(jbn:invoice_text))
!                                    prm:notes_3           = ''
!                                    prm:taken_by          = Stripcomma(job:who_booked)
!                                    prm:order_number      = Stripcomma(job:order_number)
!                                    prm:cust_order_number = 'J' & Stripcomma(job:ref_number)
!                                    prm:payment_ref       = ''
!                                    prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
!                                    prm:global_details    = ''
!                                    prm:items_net         = Stripcomma(job:labour_cost + job:parts_cost + job:courier_cost)
!                                    prm:items_tax         = Stripcomma(Round(job:labour_cost * (labour_rate$/100),.01) + |
!                                                                Round(job:parts_cost * (parts_rate$/100),.01) + |
!                                                                Round(job:courier_cost * (labour_rate$/100),.01))
!                                    prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
!                                    prm:description       = Stripcomma(DEF:Courier_Description)
!                                    prm:nominal_code      = Stripcomma(DEF:Courier_Code)
!                                    prm:qty_order         = '1'
!                                    prm:unit_price        = '0'
!                                    prm:net_amount        = Stripcomma(job:courier_cost)
!                                    prm:tax_amount        = Stripcomma(Round(job:courier_cost * (labour_rate$/100),.01))
!                                    prm:comment_1         = Stripcomma(job:charge_type)
!                                    prm:comment_2         = Stripcomma(job:warranty_charge_type)
!                                    prm:unit_of_sale      = '0'
!                                    prm:full_net_amount   = '0'
!                                    prm:invoice_date      = '*'
!                                    prm:data_filepath     = Clip(DEF:Path_Sage) & '\'
!                                    prm:user_name         = Clip(DEF:User_Name_Sage)
!                                    prm:password          = Clip(DEF:Password_Sage)
!                                    prm:set_invoice_number= '*'
!                                    prm:invoice_no        = '*'
!                                    access:paramss.insert()
!                                end!if access:paramss.primerecord() = Level:Benign
!                                access:paramss.close()
!                                Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
!                                sage_error# = 0
!                                access:paramss.open()
!                                access:paramss.usefile()
!                                Set(paramss,0)
!                                If access:paramss.next()
!                                    sage_error# = 1
!                                Else!If access:paramss.next()
!                                    If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
!                                        sage_error# = 1
!                                    Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
!                                        invoice_number_temp = prm:invoice_no
!                                        If invoice_number_temp = 0
!                                            sage_error# = 1
!                                        End!If invoice_number_temp = 0
!                                    End!If prm:invoice_no = '-1'
!                                End!If access:paramss.next()
!                                access:paramss.close()
!                            End!If def:use_sage = 'YES'
!                            If sage_error# = 0
!                                invoice_error# = 1
!                                get(invoice,0)
!                                if access:invoice.primerecord() = level:benign
!                                    If def:use_sage = 'YES'
!                                        inv:invoice_number = invoice_number_temp
!                                    End!If def:use_sage = 'YES'
!                                    inv:invoice_type       = 'SIN'
!                                    inv:job_number         = job:ref_number
!                                    inv:date_created       = Today()
!                                    inv:account_number     = job:account_number
!                                    If tra:invoice_sub_accounts = 'YES'
!                                        inv:AccountType = 'SUB'
!                                    Else!If tra:invoice_sub_accounts = 'YES'
!                                        inv:AccountType = 'MAI'
!                                    End!If tra:invoice_sub_accounts = 'YES'
!                                    inv:total              = job:sub_total
!                                    inv:vat_rate_labour    = labour_rate$
!                                    inv:vat_rate_parts     = parts_rate$
!                                    inv:vat_rate_retail    = labour_rate$
!                                    inv:vat_number         = def:vat_number
!                                    INV:Courier_Paid       = job:courier_cost
!                                    inv:parts_paid         = job:parts_cost
!                                    inv:labour_paid        = job:labour_cost
!                                    inv:invoice_vat_number = vat_number"
!                                    invoice_error# = 0
!                                    If access:invoice.insert()
!                                        invoice_error# = 1
!                                        access:invoice.cancelautoinc()
!                                    End!If access:invoice.insert()
!                                end!if access:invoice.primerecord() = level:benign
!                                If Invoice_error# = 0
!                                    Case despatch#
!                                        Of 1
!                                            If job:despatched = 'YES'
!                                                If job:paid = 'YES'
!                                                    GetStatus(910,0,'JOB') !Despatch Paid
!                                                Else!If job:paid = 'YES'
!                                                    GetStatus(905,0,'JOB') !Despatch Unpaid
!                                                End!If job:paid = 'YES'
!                                            Else!If job:despatched = 'YES'
!                                                GetStatus(810,0,'JOB') !Ready To Despatch
!                                                job:despatched = 'REA'
!                                                job:despatch_type = 'JOB'
!                                                job:current_courier = job:courier
!                                            End!If job:despatched <> 'YES'
!                                        Of 2
!                                            GetStatus(803,0,'JOB') !Ready To Despatch
!
!                                    End!Case despatch#
!
!                                    job:invoice_number        = inv:invoice_number
!                                    job:invoice_date          = Today()
!                                    job:invoice_courier_cost  = job:courier_cost
!                                    job:invoice_labour_cost   = job:labour_cost
!                                    job:invoice_parts_cost    = job:parts_cost
!                                    job:invoice_sub_total     = job:sub_total
!                                    access:jobs.update()
!                                    get(audit,0)
!                                    if access:audit.primerecord() = level:benign
!                                        aud:notes         = 'INVOICE NUMBER: ' & inv:invoice_number
!                                        aud:ref_number    = job:ref_number
!                                        aud:date          = today()
!                                        aud:time          = clock()
!                                        aud:type          = 'JOB'
!                                        access:users.clearkey(use:password_key)
!                                        use:password = glo:password
!                                        access:users.fetch(use:password_key)
!                                        aud:user = use:user_code
!                                        aud:action        = 'INVOICE CREATED'
!                                        access:audit.insert()
!                                    end!�if access:audit.primerecord() = level:benign
!
!                                    glo:select1 = inv:invoice_number
!                                    Single_Invoice
!                                    glo:select1 = ''
!                                End!If Invoice_error# = 0
!                            End!If def:use_sage = 'YES' and sage_error# = 1
!                        End!If fetch_error# = 0
!                    End!If error# = 0
!                End!If job:invoice_number <> ''
!            end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
!
!        Of 2 ! &No Button
!    End!Case MessageEx


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Print_Routines',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Print_Routines',1)
    SolaceViewVars('sav:path',sav:path,'Print_Routines',1)
    SolaceViewVars('invoice_number_temp',invoice_number_temp,'Print_Routines',1)
    SolaceViewVars('report_type_temp',report_type_temp,'Print_Routines',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp;  SolaceCtrlName = '?report_type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio1;  SolaceCtrlName = '?report_type_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio16;  SolaceCtrlName = '?report_type_temp:Radio16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio12;  SolaceCtrlName = '?report_type_temp:Radio12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio2;  SolaceCtrlName = '?report_type_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio3;  SolaceCtrlName = '?report_type_temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio13;  SolaceCtrlName = '?report_type_temp:Radio13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio10;  SolaceCtrlName = '?report_type_temp:Radio10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio10:2;  SolaceCtrlName = '?report_type_temp:Radio10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio14;  SolaceCtrlName = '?report_type_temp:Radio14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio11;  SolaceCtrlName = '?report_type_temp:Radio11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio11:2;  SolaceCtrlName = '?report_type_temp:Radio11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio6;  SolaceCtrlName = '?report_type_temp:Radio6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio7;  SolaceCtrlName = '?report_type_temp:Radio7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio8;  SolaceCtrlName = '?report_type_temp:Radio8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio9;  SolaceCtrlName = '?report_type_temp:Radio9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?report_type_temp:Radio15;  SolaceCtrlName = '?report_type_temp:Radio15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group5;  SolaceCtrlName = '?Group5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group5:2;  SolaceCtrlName = '?Group5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group5:3;  SolaceCtrlName = '?Group5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group5:4;  SolaceCtrlName = '?Group5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?print;  SolaceCtrlName = '?print';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Print_Routines')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Print_Routines')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?report_type_temp:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:LETTERS.Open
  Relate:MERGELET.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:STAHEAD.Open
  Relate:VATCODE.Open
  Access:JOBS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:INVOICE.UseFile
  Access:CHARTYPE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:JOBSTAGE.UseFile
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:LETTERS.Close
    Relate:MERGELET.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:STAHEAD.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Print_Routines',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Open, ())
  ! Start Change 2851 BE(08/07/03)
  SuspendPrintingJobCard# = GETINI('PRINTING','SuspendPrintingJobCard',,CLIP(PATH())&'\SB2KDEF.INI')
  IF (SuspendPrintingJobCard#) THEN
          access:JOBS_ALIAS.clearkey(job_ali:ref_number_key)
          job_ali:ref_number  = glo:select1
          IF ((access:JOBS_ALIAS.fetch(job_ali:ref_number_key) = Level:benign) AND |
              (job_ali:Workshop <> 'YES'))THEN
              DISABLE(?report_type_temp:RADIO2)
          END
  END
  ! Emd Change 2851 BE(08/07/03)
  PARENT.Open
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Open, ())


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?print
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print, Accepted)
      Case report_type_temp
          Of 1 !Job Receipt
              Job_Receipt
          Of 2 !Job Card
              Job_Card
          Of 3 !Job Label
              Set(defaults)
              access:defaults.next()
              Case def:label_printer_type
                  Of 'TEC B-440 / B-442'
                      If job:bouncer = 'B'
                          Thermal_Labels('SE')
                      Else!If job:bouncer = 'B'
                          Thermal_Labels('')
                      End!If job:bouncer = 'B'
                      
                  Of 'TEC B-452'
                      Thermal_Labels_B452
              End!Case def:label_printer_type
          
          OF 4 !Exchange Label
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = glo:select1
              If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
                  If job:exchange_unit_number = ''
                      Case MessageEx('There is no Exchange Unit attached to this job.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If job:exchange_unit_number = ''
                      job_number# = glo:select1                                                    !A fudge becuase I can't be
                      glo:select1 = job:exchange_unit_number                                       !bothered to change the prog.
                      Exchange_Unit_Label
                      glo:select1  = job_number#
      
                  End!If job:exchange_unit_number = ''
              End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
          Of 5 !Loan Label
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = glo:select1
              If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
                  If job:loan_unit_number = ''
                      Case MessageEx('There is no Loan Unit attached to this job.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If job:loan_unit_number = ''
                      job_number# = glo:select1
                      glo:select1  = job:loan_unit_number
                      Loan_Unit_Label
                      glo:select1  = job_number#
                  End!If job:loan_unit_number = ''
              End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
          Of 6 !Collection Note
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number  = glo:select1
              If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
                  error# = 0
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = job:incoming_courier
                  if access:courier.tryfetch(cou:courier_key) = Level:Benign
                      If cou:courier_type <> 'ANC'
                          error# = 1
                      End!If job:courier_type = 'ANC'
                  Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                      error# = 1
                  End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                  If error# = 1
                      Case MessageEx('This job has not been assigned to an ANC courier.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If error# = 1
                      glo:file_name = 'C:\ANC.TXT'
                      Remove(expgen)
                      If access:expgen.open()
                          Stop(error())
                          Return Level:Benign
                      End!If access:expgen.open()
                      access:expgen.usefile()
                      Clear(gen:record)
                      gen:line1   = job:ref_number
                      gen:line1   = Sub(gen:line1,1,10) & job:company_name
                      gen:line1   = Sub(gen:line1,1,40) & job:address_line1_delivery
                      gen:line1   = Sub(gen:line1,1,70) & job:address_line2_delivery
                      gen:line1   = Sub(gen:line1,1,100) & job:address_line3_delivery
                      gen:line1   = Sub(gen:line1,1,130) & job:postcode_delivery
                      gen:line1   = Sub(gen:line1,1,150) & job:telephone_delivery
                      gen:line1   = Sub(gen:line1,1,175) & job:unit_type
                      gen:line1   = Sub(gen:line1,1,195) & job:model_number
                      gen:line1   = Sub(gen:line1,1,215) & cou:ContractNumber
                      access:expgen.insert()
                      Close(expgen)
                      access:expgen.close()
                      cou:anccount += 1
                      If cou:ANCCount = 999
                          cou:ANCCount = 1
                      End
                      access:courier.update()
                      Set(Defaults)
                      access:defaults.next()
                      job:incoming_consignment_number = def:ANCCollectionNo
                      access:jobs.update()
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'COLLECTION NOTE NUMBER: ' & Clip(def:ANCCollectionNo)
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'ANC COLLECTION NOTE REPRINT'
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
      
                      DEF:ANCCollectionNo += 10
                      access:defaults.update()
      
                      sav:path    = path()
                      setcursor(cursor:wait)
                      setpath(Clip(cou:ancpath))
                      Run('ancpaper.exe',1)
                      Setpath(sav:path)
                      setcursor()
      
      
                  End!If error# = 1
              End!If access:jobs.tryfetch(job:ref_number_Key) = Level:Benign
          Of 7 !Despatch Note
              ! Start Change 3779 BE(29/01/04)
              !Despatch_Note
              no_print_despatch# = GETINI('PRINTING','NoDespatchNote4IncompleteJob',,CLIP(PATH())&'\SB2KDEF.INI')
              IF (no_print_despatch# = 0) THEN
                  Despatch_Note
              ELSE
                  access:JOBS_ALIAS.clearkey(job_ali:ref_number_key)
                  job_ali:ref_number  = glo:select1
                  IF ((access:JOBS_ALIAS.fetch(job_ali:ref_number_key) = Level:benign) AND |
                      (job_ali:completed = 'YES')) THEN
                      Despatch_Note
                  ELSE
                      MessageEx('A Despatch Note has NOT been generated. The job is not completed','ServiceBase 2000',|
                                'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                beep:systemhand,msgex:samewidths,84,26,0)
                  END
              END
              ! End Change 3779 BE(29/01/04)
          Of 8 !Estimate
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number = glo:select1
              if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If job:estimate <> 'YES'
                      Case MessageEx('The selected job has NOT been marked as an Estimate.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If job:estimate <> 'YES'
                      Estimate
                  End!If job:estimate <> 'YES'
              end
          
          Of 9 !Invoice
              Do Create_Invoice
          Of 10
              saverequest#      = globalrequest
              globalresponse    = requestcancelled
              globalrequest     = selectrecord
              Pick_Letter
              if globalresponse = requestcompleted
                  glo:select2 = let:description
                  Standard_Letter
                  access:letters.clearkey(let:descriptionkey)
                  let:description = glo:select2
                  access:letters.tryfetch(LET:DescriptionKey)
                  If let:usestatus = 'YES'
                      GetStatus(Sub(let:status,1,3),0,'JOB') !Letter Status
                      Access:jobs.update()
      
                  End!If let:use_status = 'YES'
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      If let:usestatus = 'YES'
      
                          aud:notes         = 'PREVIOUS STATUS: ' & CLip(job:PreviousStatus) & |
                                                '<13,10>NEW STATUS: ' & Clip(job:current_status)
                      End!If let:use_status = 'YES'
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      access:users.clearkey(use:password_key)
                      use:password = glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      aud:action        = 'LETTER SENT: ' & Clip(let:description)
                      access:audit.insert()
                  end!�if access:audit.primerecord() = level:benign
      
              end
              globalrequest     = saverequest#
              glo:select2  = ''
      
          Of 11 !Letter
              saverequest#      = globalrequest
              globalresponse    = requestcancelled
              globalrequest     = selectrecord
              Pickletters
              if globalresponse = requestcompleted
                  glo:select2  = mrg:lettername
                  ViewLetter
                  access:mergelet.clearkey(MRg:LetterNameKey)
                  mrg:LetterName  = glo:select2
                  access:mergelet.fetch(mrg:LetterNameKey)
      Compile('***',Debug=1)
          Message('glo:select2: ' & glo:select2 & |
                  'mrg:usestatus: ' & mrg:usestatus,'Debug Message',icon:exclamation)
      ***
                  If MRG:UseStatus = 'YES'
          Compile('***',Debug=1)
              Message('Use New Status: ' & CLip(mrg:status),'Debug Message',icon:exclamation)
          ***
                      GetStatus(Sub(mrg:status,1,3),0,'JOB') !Letter Status
                      Access:jobs.update()
      
                  End!If let:use_status = 'YES'
                  get(audit,0)
                  if access:audit.primerecord() = level:benign
                      If mrg:usestatus = 'YES'
      
                          aud:notes         = 'PREVIOUS STATUS: ' & CLip(job:PreviousStatus) & |
                                                '<13,10>NEW STATUS: ' & Clip(job:current_status)
                      End!If let:use_status = 'YES'
                      aud:ref_number    = job:ref_number
                      aud:date          = today()
                      aud:time          = clock()
                      access:users.clearkey(use:password_key)
                      use:password = glo:password
                      access:users.fetch(use:password_key)
                      aud:user = use:user_code
                      aud:action        = 'LETTER SENT: ' & Clip(mrg:LetterName)
                      access:audit.insert()
                  end!�if access:audit.primerecord() = level:benign
              end
              globalrequest     = saverequest#
           Of 12 !Bounce Report
                  glo:select2 = job:ESN
                  Bouncer_History
                  glo:select2 = ''
          Of 13
              Address_Label('DEL','')
          Of 14
              Address_Label('COL','')
          Of 15
              access:PICKNOTE.clearkey(pnt:keyonjobno) !check for existing picking record
              pnt:JobReference = job:ref_number
              set(pnt:keyonjobno,pnt:keyonjobno)
              loop
                  if access:picknote.next() then break. !no records
                  if pnt:JobReference <> job:Ref_Number then break. !no matching records
                  !if pnt:Location <> sto:Location then break. !no matching records on location
                  if pnt:IsPrinted then !matching record - is it unprinted if so we can we append to it
                      PickReport
      !            else
      !               Case MessageEx('Pick note not yet sent to stores. Use jobs screen to commit.','ServiceBase 2000',|
      !                                    'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
      !                   Of 1 ! &OK Button
      !               End!Case MessageEx
                     !PickReport
                  end
              end !loop
      
          ! Start Change 4120 BE(31/08/2004)
          Of 16
              Virgin_Labels()
          ! End Change 4120 BE(31/08/2004)
      
      End!Case report_type_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?print, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Print_Routines')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

