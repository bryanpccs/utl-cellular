

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01044.INC'),ONCE        !Local module procedure declarations
                     END


UpdateReportNames PROCEDURE                           !Generated from procedure template - Window

CurrentTab           STRING(80)
tmp:EXEName          STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::rex:Record  LIKE(rex:RECORD),STATIC
QuickWindow          WINDOW('Update Report Names'),AT(,,359,99),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateReportNames'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,352,64),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Report Type'),AT(8,20),USE(?REX:ReportType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s80),AT(64,20,160,10),USE(rex:ReportType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Report Type'),TIP('Report Type'),REQ,UPR
                           PROMPT('Report Type'),AT(8,36),USE(?REX:ReportName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s80),AT(64,36,286,10),USE(rex:ReportName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Report Type'),TIP('Report Type'),REQ,CAP
                           PROMPT('EXEName'),AT(8,52),USE(?REX:EXEName:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,52,160,10),USE(rex:EXEName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('EXEName'),TIP('EXEName'),REQ,UPR
                           BUTTON,AT(228,52,10,10),USE(?LookupEXEName),SKIP,ICON('List3.ico')
                         END
                       END
                       BUTTON('&OK'),AT(240,76,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(296,76,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,72,352,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FileLookup9          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?REX:ReportType:Prompt{prop:FontColor} = -1
    ?REX:ReportType:Prompt{prop:Color} = 15066597
    If ?rex:ReportType{prop:ReadOnly} = True
        ?rex:ReportType{prop:FontColor} = 65793
        ?rex:ReportType{prop:Color} = 15066597
    Elsif ?rex:ReportType{prop:Req} = True
        ?rex:ReportType{prop:FontColor} = 65793
        ?rex:ReportType{prop:Color} = 8454143
    Else ! If ?rex:ReportType{prop:Req} = True
        ?rex:ReportType{prop:FontColor} = 65793
        ?rex:ReportType{prop:Color} = 16777215
    End ! If ?rex:ReportType{prop:Req} = True
    ?rex:ReportType{prop:Trn} = 0
    ?rex:ReportType{prop:FontStyle} = font:Bold
    ?REX:ReportName:Prompt{prop:FontColor} = -1
    ?REX:ReportName:Prompt{prop:Color} = 15066597
    If ?rex:ReportName{prop:ReadOnly} = True
        ?rex:ReportName{prop:FontColor} = 65793
        ?rex:ReportName{prop:Color} = 15066597
    Elsif ?rex:ReportName{prop:Req} = True
        ?rex:ReportName{prop:FontColor} = 65793
        ?rex:ReportName{prop:Color} = 8454143
    Else ! If ?rex:ReportName{prop:Req} = True
        ?rex:ReportName{prop:FontColor} = 65793
        ?rex:ReportName{prop:Color} = 16777215
    End ! If ?rex:ReportName{prop:Req} = True
    ?rex:ReportName{prop:Trn} = 0
    ?rex:ReportName{prop:FontStyle} = font:Bold
    ?REX:EXEName:Prompt{prop:FontColor} = -1
    ?REX:EXEName:Prompt{prop:Color} = 15066597
    If ?rex:EXEName{prop:ReadOnly} = True
        ?rex:EXEName{prop:FontColor} = 65793
        ?rex:EXEName{prop:Color} = 15066597
    Elsif ?rex:EXEName{prop:Req} = True
        ?rex:EXEName{prop:FontColor} = 65793
        ?rex:EXEName{prop:Color} = 8454143
    Else ! If ?rex:EXEName{prop:Req} = True
        ?rex:EXEName{prop:FontColor} = 65793
        ?rex:EXEName{prop:Color} = 16777215
    End ! If ?rex:EXEName{prop:Req} = True
    ?rex:EXEName{prop:Trn} = 0
    ?rex:EXEName{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateReportNames',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateReportNames',1)
    SolaceViewVars('tmp:EXEName',tmp:EXEName,'UpdateReportNames',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateReportNames',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?REX:ReportType:Prompt;  SolaceCtrlName = '?REX:ReportType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rex:ReportType;  SolaceCtrlName = '?rex:ReportType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?REX:ReportName:Prompt;  SolaceCtrlName = '?REX:ReportName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rex:ReportName;  SolaceCtrlName = '?rex:ReportName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?REX:EXEName:Prompt;  SolaceCtrlName = '?REX:EXEName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rex:EXEName;  SolaceCtrlName = '?rex:EXEName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupEXEName;  SolaceCtrlName = '?LookupEXEName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateReportNames')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateReportNames')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REX:ReportType:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rex:Record,History::rex:Record)
  SELF.AddHistoryField(?rex:ReportType,2)
  SELF.AddHistoryField(?rex:ReportName,3)
  SELF.AddHistoryField(?rex:EXEName,4)
  SELF.AddUpdateFile(Access:REPEXTRP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:REPEXTRP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REPEXTRP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FileLookup9.Init
  FileLookup9.Flags=BOR(FileLookup9.Flags,FILE:LongName)
  FileLookup9.SetMask('EXE Files','*.EXE')
  FileLookup9.WindowTitle='Report Name'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPEXTRP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateReportNames',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupEXEName
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEXEName, Accepted)
      tmp:EXEName = Upper(FileLookup9.Ask(1)  )
      DISPLAY
      Loop x# = Len(Clip(tmp:EXEName)) To 1 By -1
          If Sub(tmp:EXEName,x#,1) = '\'
              REX:EXEName = Clip(Sub(tmp:EXEName,x# + 1,Len(Clip(tmp:EXEName))))
              Display()
              Break
          End!If Sub(rap:EXEPath,x#,1) = '\'
      End!Loop x# = Len(Clip(rap:EXEPath)) To 1 By -1.
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupEXEName, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateReportNames')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

