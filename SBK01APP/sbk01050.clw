

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01050.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMULDESPJ PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::mulj:Record LIKE(mulj:RECORD),STATIC
QuickWindow          WINDOW('Update the MULDESPJ File'),AT(,,204,152),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateMULDESPJ'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,196,126),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?mulj:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,20,40,10),USE(mulj:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('RefNumber'),AT(8,34),USE(?mulj:RefNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,34,40,10),USE(mulj:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('RefNumber'),TIP('RefNumber'),UPR
                           PROMPT('Job Number'),AT(8,48),USE(?mulj:JobNumber:Prompt),TRN
                           ENTRY(@s8),AT(72,48,40,10),USE(mulj:JobNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                           PROMPT('I.M.E.I. Number'),AT(8,62),USE(?mulj:IMEINumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,62,124,10),USE(mulj:IMEINumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('I.M.E.I. Number'),TIP('I.M.E.I. Number'),UPR
                           PROMPT('M.S.N.'),AT(8,76),USE(?mulj:MSN:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,76,124,10),USE(mulj:MSN),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('M.S.N.'),TIP('M.S.N.'),UPR
                           PROMPT('AccountNumber'),AT(8,90),USE(?mulj:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,90,124,10),USE(mulj:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('AccountNumber'),TIP('AccountNumber'),UPR
                           PROMPT('Courier'),AT(8,104),USE(?mulj:Courier:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(72,104,124,10),USE(mulj:Courier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Courier'),TIP('Courier'),UPR
                           CHECK('Current'),AT(72,118,70,8),USE(mulj:Current),MSG('Current'),TIP('Current'),VALUE('1','0')
                         END
                       END
                       BUTTON('OK'),AT(106,134,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(155,134,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(155,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?mulj:RecordNumber:Prompt{prop:FontColor} = -1
    ?mulj:RecordNumber:Prompt{prop:Color} = 15066597
    If ?mulj:RecordNumber{prop:ReadOnly} = True
        ?mulj:RecordNumber{prop:FontColor} = 65793
        ?mulj:RecordNumber{prop:Color} = 15066597
    Elsif ?mulj:RecordNumber{prop:Req} = True
        ?mulj:RecordNumber{prop:FontColor} = 65793
        ?mulj:RecordNumber{prop:Color} = 8454143
    Else ! If ?mulj:RecordNumber{prop:Req} = True
        ?mulj:RecordNumber{prop:FontColor} = 65793
        ?mulj:RecordNumber{prop:Color} = 16777215
    End ! If ?mulj:RecordNumber{prop:Req} = True
    ?mulj:RecordNumber{prop:Trn} = 0
    ?mulj:RecordNumber{prop:FontStyle} = font:Bold
    ?mulj:RefNumber:Prompt{prop:FontColor} = -1
    ?mulj:RefNumber:Prompt{prop:Color} = 15066597
    If ?mulj:RefNumber{prop:ReadOnly} = True
        ?mulj:RefNumber{prop:FontColor} = 65793
        ?mulj:RefNumber{prop:Color} = 15066597
    Elsif ?mulj:RefNumber{prop:Req} = True
        ?mulj:RefNumber{prop:FontColor} = 65793
        ?mulj:RefNumber{prop:Color} = 8454143
    Else ! If ?mulj:RefNumber{prop:Req} = True
        ?mulj:RefNumber{prop:FontColor} = 65793
        ?mulj:RefNumber{prop:Color} = 16777215
    End ! If ?mulj:RefNumber{prop:Req} = True
    ?mulj:RefNumber{prop:Trn} = 0
    ?mulj:RefNumber{prop:FontStyle} = font:Bold
    ?mulj:JobNumber:Prompt{prop:FontColor} = -1
    ?mulj:JobNumber:Prompt{prop:Color} = 15066597
    If ?mulj:JobNumber{prop:ReadOnly} = True
        ?mulj:JobNumber{prop:FontColor} = 65793
        ?mulj:JobNumber{prop:Color} = 15066597
    Elsif ?mulj:JobNumber{prop:Req} = True
        ?mulj:JobNumber{prop:FontColor} = 65793
        ?mulj:JobNumber{prop:Color} = 8454143
    Else ! If ?mulj:JobNumber{prop:Req} = True
        ?mulj:JobNumber{prop:FontColor} = 65793
        ?mulj:JobNumber{prop:Color} = 16777215
    End ! If ?mulj:JobNumber{prop:Req} = True
    ?mulj:JobNumber{prop:Trn} = 0
    ?mulj:JobNumber{prop:FontStyle} = font:Bold
    ?mulj:IMEINumber:Prompt{prop:FontColor} = -1
    ?mulj:IMEINumber:Prompt{prop:Color} = 15066597
    If ?mulj:IMEINumber{prop:ReadOnly} = True
        ?mulj:IMEINumber{prop:FontColor} = 65793
        ?mulj:IMEINumber{prop:Color} = 15066597
    Elsif ?mulj:IMEINumber{prop:Req} = True
        ?mulj:IMEINumber{prop:FontColor} = 65793
        ?mulj:IMEINumber{prop:Color} = 8454143
    Else ! If ?mulj:IMEINumber{prop:Req} = True
        ?mulj:IMEINumber{prop:FontColor} = 65793
        ?mulj:IMEINumber{prop:Color} = 16777215
    End ! If ?mulj:IMEINumber{prop:Req} = True
    ?mulj:IMEINumber{prop:Trn} = 0
    ?mulj:IMEINumber{prop:FontStyle} = font:Bold
    ?mulj:MSN:Prompt{prop:FontColor} = -1
    ?mulj:MSN:Prompt{prop:Color} = 15066597
    If ?mulj:MSN{prop:ReadOnly} = True
        ?mulj:MSN{prop:FontColor} = 65793
        ?mulj:MSN{prop:Color} = 15066597
    Elsif ?mulj:MSN{prop:Req} = True
        ?mulj:MSN{prop:FontColor} = 65793
        ?mulj:MSN{prop:Color} = 8454143
    Else ! If ?mulj:MSN{prop:Req} = True
        ?mulj:MSN{prop:FontColor} = 65793
        ?mulj:MSN{prop:Color} = 16777215
    End ! If ?mulj:MSN{prop:Req} = True
    ?mulj:MSN{prop:Trn} = 0
    ?mulj:MSN{prop:FontStyle} = font:Bold
    ?mulj:AccountNumber:Prompt{prop:FontColor} = -1
    ?mulj:AccountNumber:Prompt{prop:Color} = 15066597
    If ?mulj:AccountNumber{prop:ReadOnly} = True
        ?mulj:AccountNumber{prop:FontColor} = 65793
        ?mulj:AccountNumber{prop:Color} = 15066597
    Elsif ?mulj:AccountNumber{prop:Req} = True
        ?mulj:AccountNumber{prop:FontColor} = 65793
        ?mulj:AccountNumber{prop:Color} = 8454143
    Else ! If ?mulj:AccountNumber{prop:Req} = True
        ?mulj:AccountNumber{prop:FontColor} = 65793
        ?mulj:AccountNumber{prop:Color} = 16777215
    End ! If ?mulj:AccountNumber{prop:Req} = True
    ?mulj:AccountNumber{prop:Trn} = 0
    ?mulj:AccountNumber{prop:FontStyle} = font:Bold
    ?mulj:Courier:Prompt{prop:FontColor} = -1
    ?mulj:Courier:Prompt{prop:Color} = 15066597
    If ?mulj:Courier{prop:ReadOnly} = True
        ?mulj:Courier{prop:FontColor} = 65793
        ?mulj:Courier{prop:Color} = 15066597
    Elsif ?mulj:Courier{prop:Req} = True
        ?mulj:Courier{prop:FontColor} = 65793
        ?mulj:Courier{prop:Color} = 8454143
    Else ! If ?mulj:Courier{prop:Req} = True
        ?mulj:Courier{prop:FontColor} = 65793
        ?mulj:Courier{prop:Color} = 16777215
    End ! If ?mulj:Courier{prop:Req} = True
    ?mulj:Courier{prop:Trn} = 0
    ?mulj:Courier{prop:FontStyle} = font:Bold
    ?mulj:Current{prop:Font,3} = -1
    ?mulj:Current{prop:Color} = 15066597
    ?mulj:Current{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMULDESPJ',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMULDESPJ',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMULDESPJ',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:RecordNumber:Prompt;  SolaceCtrlName = '?mulj:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:RecordNumber;  SolaceCtrlName = '?mulj:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:RefNumber:Prompt;  SolaceCtrlName = '?mulj:RefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:RefNumber;  SolaceCtrlName = '?mulj:RefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:JobNumber:Prompt;  SolaceCtrlName = '?mulj:JobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:JobNumber;  SolaceCtrlName = '?mulj:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:IMEINumber:Prompt;  SolaceCtrlName = '?mulj:IMEINumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:IMEINumber;  SolaceCtrlName = '?mulj:IMEINumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:MSN:Prompt;  SolaceCtrlName = '?mulj:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:MSN;  SolaceCtrlName = '?mulj:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:AccountNumber:Prompt;  SolaceCtrlName = '?mulj:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:AccountNumber;  SolaceCtrlName = '?mulj:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:Courier:Prompt;  SolaceCtrlName = '?mulj:Courier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:Courier;  SolaceCtrlName = '?mulj:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mulj:Current;  SolaceCtrlName = '?mulj:Current';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a MULDESPJ Record'
  OF ChangeRecord
    ActionMessage = 'Changing a MULDESPJ Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateMULDESPJ')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMULDESPJ')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?mulj:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mulj:Record,History::mulj:Record)
  SELF.AddHistoryField(?mulj:RecordNumber,1)
  SELF.AddHistoryField(?mulj:RefNumber,2)
  SELF.AddHistoryField(?mulj:JobNumber,3)
  SELF.AddHistoryField(?mulj:IMEINumber,4)
  SELF.AddHistoryField(?mulj:MSN,5)
  SELF.AddHistoryField(?mulj:AccountNumber,6)
  SELF.AddHistoryField(?mulj:Courier,7)
  SELF.AddHistoryField(?mulj:Current,8)
  SELF.AddUpdateFile(Access:MULDESPJ)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:MULDESPJ.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MULDESPJ
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MULDESPJ.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMULDESPJ',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMULDESPJ')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

