

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01016.INC'),ONCE        !Local module procedure declarations
                     END


Reconcile_Claim_Value PROCEDURE (f_Ref_number,f_value_claimed,f_courier,f_labour,f_parts,f_total_jobs) !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
FilesOpened          BYTE
Reference_Number_Temp STRING(30)
Value_Claimed_Temp   REAL
Labour_Temp          REAL
Parts_Temp           REAL
total_jobs_temp      REAL
Difference_Temp      REAL
courier_temp         REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Warranty Claim Financial Details'),AT(,,244,172),FONT('Arial',8,,),CENTER,IMM,ICON('Pc.ico'),TILED,TIMER(50),GRAY,DOUBLE
                       SHEET,AT(4,4,236,136),USE(?Sheet1),SPREAD
                         TAB('Financial Details'),USE(?Tab1)
                           PROMPT('Total Jobs Claimed'),AT(8,20),USE(?total_jobs:Prompt),TRN
                           ENTRY(@n6),AT(108,20,64,10),USE(total_jobs_temp),FONT('Arial',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Reference Number'),AT(8,36),USE(?Select_Global1:Prompt)
                           ENTRY(@s30),AT(108,36,124,10),USE(Reference_Number_Temp),FONT('Arial',8,,FONT:bold),REQ,UPR
                           PROMPT('Value Claimed'),AT(8,52),USE(?Select_Global2:Prompt)
                           ENTRY(@n14.2),AT(108,52,64,10),USE(Value_Claimed_Temp),FONT('Arial',8,,FONT:bold),REQ
                           BUTTON('Auto Valuate'),AT(176,80,56,16),USE(?Valuate_Claim:2),LEFT,ICON('Calc1.ico')
                           BUTTON('Valuate Claims'),AT(176,48,56,16),USE(?Valuate_Claim),LEFT,ICON('Calc1.ico')
                           PROMPT('Courier Cost ( Less V.A.T. )'),AT(8,68),USE(?courier_temp:Prompt)
                           ENTRY(@n14.2),AT(108,68,64,10),USE(courier_temp),LEFT,FONT(,,,FONT:bold),UPR
                           PROMPT('Labour Value ( Less V.A.T. )'),AT(8,84),USE(?Select_Global3:Prompt)
                           ENTRY(@n14.2),AT(108,84,64,10),USE(Labour_Temp),FONT(,8,,FONT:bold)
                           PROMPT('Parts Value ( Less V.A.T. )'),AT(8,100),USE(?Select_Global4:Prompt)
                           ENTRY(@n14.2),AT(108,100,64,10),USE(Parts_Temp),FONT(,8,,FONT:bold)
                           LINE,AT(108,116,64,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Difference Temp'),AT(8,124,94,10),USE(?Difference_Text),HIDE
                           ENTRY(@n-14.2),AT(108,124,64,10),USE(Difference_Temp),SKIP,HIDE,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),READONLY
                         END
                       END
                       PANEL,AT(4,144,236,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(124,148,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(180,148,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                       BUTTON('Export Claims'),AT(8,148,56,16),USE(?Export_Claims),LEFT,ICON('Disk.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?total_jobs:Prompt{prop:FontColor} = -1
    ?total_jobs:Prompt{prop:Color} = 15066597
    If ?total_jobs_temp{prop:ReadOnly} = True
        ?total_jobs_temp{prop:FontColor} = 65793
        ?total_jobs_temp{prop:Color} = 15066597
    Elsif ?total_jobs_temp{prop:Req} = True
        ?total_jobs_temp{prop:FontColor} = 65793
        ?total_jobs_temp{prop:Color} = 8454143
    Else ! If ?total_jobs_temp{prop:Req} = True
        ?total_jobs_temp{prop:FontColor} = 65793
        ?total_jobs_temp{prop:Color} = 16777215
    End ! If ?total_jobs_temp{prop:Req} = True
    ?total_jobs_temp{prop:Trn} = 0
    ?total_jobs_temp{prop:FontStyle} = font:Bold
    ?Select_Global1:Prompt{prop:FontColor} = -1
    ?Select_Global1:Prompt{prop:Color} = 15066597
    If ?Reference_Number_Temp{prop:ReadOnly} = True
        ?Reference_Number_Temp{prop:FontColor} = 65793
        ?Reference_Number_Temp{prop:Color} = 15066597
    Elsif ?Reference_Number_Temp{prop:Req} = True
        ?Reference_Number_Temp{prop:FontColor} = 65793
        ?Reference_Number_Temp{prop:Color} = 8454143
    Else ! If ?Reference_Number_Temp{prop:Req} = True
        ?Reference_Number_Temp{prop:FontColor} = 65793
        ?Reference_Number_Temp{prop:Color} = 16777215
    End ! If ?Reference_Number_Temp{prop:Req} = True
    ?Reference_Number_Temp{prop:Trn} = 0
    ?Reference_Number_Temp{prop:FontStyle} = font:Bold
    ?Select_Global2:Prompt{prop:FontColor} = -1
    ?Select_Global2:Prompt{prop:Color} = 15066597
    If ?Value_Claimed_Temp{prop:ReadOnly} = True
        ?Value_Claimed_Temp{prop:FontColor} = 65793
        ?Value_Claimed_Temp{prop:Color} = 15066597
    Elsif ?Value_Claimed_Temp{prop:Req} = True
        ?Value_Claimed_Temp{prop:FontColor} = 65793
        ?Value_Claimed_Temp{prop:Color} = 8454143
    Else ! If ?Value_Claimed_Temp{prop:Req} = True
        ?Value_Claimed_Temp{prop:FontColor} = 65793
        ?Value_Claimed_Temp{prop:Color} = 16777215
    End ! If ?Value_Claimed_Temp{prop:Req} = True
    ?Value_Claimed_Temp{prop:Trn} = 0
    ?Value_Claimed_Temp{prop:FontStyle} = font:Bold
    ?courier_temp:Prompt{prop:FontColor} = -1
    ?courier_temp:Prompt{prop:Color} = 15066597
    If ?courier_temp{prop:ReadOnly} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 15066597
    Elsif ?courier_temp{prop:Req} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 8454143
    Else ! If ?courier_temp{prop:Req} = True
        ?courier_temp{prop:FontColor} = 65793
        ?courier_temp{prop:Color} = 16777215
    End ! If ?courier_temp{prop:Req} = True
    ?courier_temp{prop:Trn} = 0
    ?courier_temp{prop:FontStyle} = font:Bold
    ?Select_Global3:Prompt{prop:FontColor} = -1
    ?Select_Global3:Prompt{prop:Color} = 15066597
    If ?Labour_Temp{prop:ReadOnly} = True
        ?Labour_Temp{prop:FontColor} = 65793
        ?Labour_Temp{prop:Color} = 15066597
    Elsif ?Labour_Temp{prop:Req} = True
        ?Labour_Temp{prop:FontColor} = 65793
        ?Labour_Temp{prop:Color} = 8454143
    Else ! If ?Labour_Temp{prop:Req} = True
        ?Labour_Temp{prop:FontColor} = 65793
        ?Labour_Temp{prop:Color} = 16777215
    End ! If ?Labour_Temp{prop:Req} = True
    ?Labour_Temp{prop:Trn} = 0
    ?Labour_Temp{prop:FontStyle} = font:Bold
    ?Select_Global4:Prompt{prop:FontColor} = -1
    ?Select_Global4:Prompt{prop:Color} = 15066597
    If ?Parts_Temp{prop:ReadOnly} = True
        ?Parts_Temp{prop:FontColor} = 65793
        ?Parts_Temp{prop:Color} = 15066597
    Elsif ?Parts_Temp{prop:Req} = True
        ?Parts_Temp{prop:FontColor} = 65793
        ?Parts_Temp{prop:Color} = 8454143
    Else ! If ?Parts_Temp{prop:Req} = True
        ?Parts_Temp{prop:FontColor} = 65793
        ?Parts_Temp{prop:Color} = 16777215
    End ! If ?Parts_Temp{prop:Req} = True
    ?Parts_Temp{prop:Trn} = 0
    ?Parts_Temp{prop:FontStyle} = font:Bold
    ?Difference_Text{prop:FontColor} = -1
    ?Difference_Text{prop:Color} = 15066597
    If ?Difference_Temp{prop:ReadOnly} = True
        ?Difference_Temp{prop:FontColor} = 65793
        ?Difference_Temp{prop:Color} = 15066597
    Elsif ?Difference_Temp{prop:Req} = True
        ?Difference_Temp{prop:FontColor} = 65793
        ?Difference_Temp{prop:Color} = 8454143
    Else ! If ?Difference_Temp{prop:Req} = True
        ?Difference_Temp{prop:FontColor} = 65793
        ?Difference_Temp{prop:Color} = 16777215
    End ! If ?Difference_Temp{prop:Req} = True
    ?Difference_Temp{prop:Trn} = 0
    ?Difference_Temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Reconcile_Claim_Value',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Reconcile_Claim_Value',1)
    SolaceViewVars('Reference_Number_Temp',Reference_Number_Temp,'Reconcile_Claim_Value',1)
    SolaceViewVars('Value_Claimed_Temp',Value_Claimed_Temp,'Reconcile_Claim_Value',1)
    SolaceViewVars('Labour_Temp',Labour_Temp,'Reconcile_Claim_Value',1)
    SolaceViewVars('Parts_Temp',Parts_Temp,'Reconcile_Claim_Value',1)
    SolaceViewVars('total_jobs_temp',total_jobs_temp,'Reconcile_Claim_Value',1)
    SolaceViewVars('Difference_Temp',Difference_Temp,'Reconcile_Claim_Value',1)
    SolaceViewVars('courier_temp',courier_temp,'Reconcile_Claim_Value',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_jobs:Prompt;  SolaceCtrlName = '?total_jobs:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_jobs_temp;  SolaceCtrlName = '?total_jobs_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global1:Prompt;  SolaceCtrlName = '?Select_Global1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reference_Number_Temp;  SolaceCtrlName = '?Reference_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global2:Prompt;  SolaceCtrlName = '?Select_Global2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Value_Claimed_Temp;  SolaceCtrlName = '?Value_Claimed_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Valuate_Claim:2;  SolaceCtrlName = '?Valuate_Claim:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Valuate_Claim;  SolaceCtrlName = '?Valuate_Claim';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?courier_temp:Prompt;  SolaceCtrlName = '?courier_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?courier_temp;  SolaceCtrlName = '?courier_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global3:Prompt;  SolaceCtrlName = '?Select_Global3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Labour_Temp;  SolaceCtrlName = '?Labour_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global4:Prompt;  SolaceCtrlName = '?Select_Global4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Parts_Temp;  SolaceCtrlName = '?Parts_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Difference_Text;  SolaceCtrlName = '?Difference_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Difference_Temp;  SolaceCtrlName = '?Difference_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Export_Claims;  SolaceCtrlName = '?Export_Claims';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Reconcile_Claim_Value')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Reconcile_Claim_Value')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?total_jobs:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Reconcile_Claim_Value',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Valuate_Claim:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim:2, Accepted)
      Case MessageEx('This is AUTO valuate the tagged claims.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              recordspercycle     = 25
              recordsprocessed    = 0
              percentprogress     = 0
              setcursor(cursor:wait)
              open(progresswindow)
              progress:thermometer    = 0
              ?progress:pcttext{prop:text} = '0% Completed'
      
              recordstoprocess    = Records(glo:queue)
              labour_temp = 0
              parts_temp  = 0
              courier_temp     = 0
              Loop x# = 1 To Records(glo:queue)
                  Get(glo:queue,x#)
                  Do GetNextRecord2
                  access:jobs.clearkey(job:ref_number_key)
                  job:ref_number = glo:pointer
                  if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      labour_temp += job:labour_cost_warranty
                      parts_temp  += job:parts_cost_warranty
                      courier_temp += job:courier_cost_warranty
                  end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
              End!Loop x# = 1 To Records(job_queue_global)
      
              setcursor()
              close(progresswindow)
              Display()
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim:2, Accepted)
    OF ?Valuate_Claim
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim, Accepted)
      x# = MessageEx('This will valuate the tagged claims.<13,10><13,10>Do you wish to: <13,10><13,10>1) Valuate the jobs without changing their current costs.<13,10><13,10>2) Re-value the tagged jobs using the current pricing structures.  (Warning! Job costs may change)','ServiceBase 2000',|
                     'Styles\question.ico','|&Valuate Jobs|&Re-value Jobs|&Cancel',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
      
      If x# <> 3
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          setcursor(cursor:wait)
          open(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
      
          recordstoprocess    = Records(glo:queue)
          value_claimed_temp = 0
          Loop x# = 1 To Records(glo:queue)
              Get(glo:queue,x#)
              Do GetNextRecord2
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number = glo:pointer
              if access:jobs.fetch(job:ref_number_key) = Level:Benign
                  If x# = 2
                      If job:ignore_warranty_charges <> 'YES'
                          Pricing_Routine('W',labour",parts",pass",a")
                          If pass" = True
                              job:labour_cost_warranty = Round(labour",.01)
                              job:parts_cost_warranty  = Round(parts",.01)
                              job:sub_total_warranty = Round(job:labour_cost_warranty,.01) + Round(job:parts_cost_warranty,.01) + Round(job:courier_cost_warranty,.01)
                              access:jobs.update()
                          End!If pass" = False
                      End!If job:ignore_warranty_charges <> 'YES'
                  End!If x# = 2
                  value_claimed_temp += (job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty)
              end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
          End!Loop x# = 1 To Records(job_queue_global)
      
          setcursor()
          close(progresswindow)
          Display(?value_claimed_temp)
      
      End!If x# <> 3
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Valuate_Claim, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If reference_number_temp = ''
          Case MessageEx('You must insert a Reference Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?reference_number_temp)
      Else!If reference_number_temp = ''
          Case MessageEx('You are about to Reconcile the selected claim.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
              
                  f_ref_number  = reference_number_temp
                  f_value_claimed = value_claimed_temp
                  f_courier       = courier_temp
                  f_labour        = labour_temp
                  f_parts         = parts_temp
                  f_total_jobs    = total_jobs_temp
                  Post(event:Closewindow)
              Of 2 ! &No Button
          End!Case MessageEx
      End!If reference_number_temp = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    OF ?Export_Claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export_Claims, Accepted)
      Case MessageEx('Are you sure you want to export the claim you are about to Reconcile?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              set(defaults)
              access:defaults.next()
              If def:exportpath <> ''
                  glo:file_name = Clip(def:exportpath) & '\WARR.CSV'
              Else!If def:exportpath <> ''
                  glo:file_name = 'C:\WARR.CSV'
              End!If def:exportpath <> ''
      
              if not filedialog ('Choose File',glo:file_name,'CSV Files|*.CSV|All Files|*.*', |
                          file:keepdir + file:noerror + file:longname)
                  !failed
              else!if not filedialog
                  !found
                  Remove(expgen)
                  access:expgen.open()
                  access:expgen.usefile()
      
                  Clear(gen:record)
                  gen:line1   = 'Warranty Valuation'
                  access:expgen.insert()
      
                  Clear(gen:record)
                  gen:line1   = ''
                  access:expgen.insert()
      
                  Clear(gen:record)
                  gen:line1   = 'Job Number,Account Number,Model Number,E.S.N. / I.M.E.I.,M.S.N.,Charge Type,Repair Type,Courier Cost,Labour Cost,Parts Cost,Engineer,Date Booked,Date Completed,Batch Number'
                  access:expgen.insert()
      
                  Clear(gen:record)
      
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
      
                  recordstoprocess    = Records(glo:queue)
                  value_claimed_temp = 0
                  Loop x# = 1 To Records(glo:queue)
                      Get(glo:queue,x#)
                      Do GetNextRecord2
                      access:jobs.clearkey(job:ref_number_key)
                      job:ref_number = glo:pointer
                      if access:jobs.fetch(job:ref_number_key) = Level:Benign
                          gen:line1   = CLip(job:ref_number)
                          gen:line1   = Clip(gen:line1) & ',' & CLip(job:account_number)
                          gen:line1   = Clip(gen:line1) & ',' & Clip(job:model_Number)
                          gen:line1   = Clip(gen:line1) & ',' & Clip(job:esn)
                          gen:line1   = Clip(gen:line1) & ',' & Clip(job:msn)
                          gen:line1   = Clip(gen:line1) & ',' & Clip(job:warranty_charge_type)
                          gen:line1   = Clip(gen:line1) & ',' & Clip(job:repair_type_warranty)
                          gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:courier_cost_warranty,@n14.2))
                          gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:labour_cost_warranty,@n14.2))
                          gen:line1   = Clip(gen:line1) & ',' & Clip(format(job:parts_cost_warranty,@n14.2))
                          gen:line1   = Clip(gen:line1) & ',' & CLip(job:engineer)
                          gen:line1   = Clip(gen:line1) & ',' & CLip(Format(job:date_booked,@d6))
                          gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:date_completed,@d6))
                          gen:line1   = Clip(gen:line1) & ',' & Clip(Format(job:edi_batch_number))
                      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
                      access:expgen.insert()
                  End!Loop x# = 1 To Records(job_queue_global)
      
                  setcursor()
                  close(progresswindow)
                  access:expgen.close()
      
              end!if not filedialog
      
      
      
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Export_Claims, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Reconcile_Claim_Value')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      total_Jobs_temp = Records(glo:queue)
      Display(?total_Jobs_temp)
      Select(?Reference_Number_Temp)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      Difference_temp = value_claimed_temp - labour_temp - parts_temp - courier_temp
      If difference_temp > 0
          Unhide(?difference_text)
          Unhide(?difference_temp)
          ?difference_text{prop:text} = 'Payment Short Fall'
      End
      If difference_Temp < 0
          unhide(?difference_text)
          Unhide(?difference_temp)
          ?difference_text{prop:text} = 'Overpaid By'
      End
      If difference_Temp = 0
          Hide(?difference_text)
          Hide(?difference_temp)
      End
      difference_temp = Abs(difference_temp)
      Display(?difference_temp)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

