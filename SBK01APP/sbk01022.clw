

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01022.INC'),ONCE        !Local module procedure declarations
                     END


despatch_job PROCEDURE (f_ref_number)                 !Generated from procedure template - Window

FilesOpened          BYTE
despatch_description_temp STRING(30)
Courier_Temp         STRING(30)
Despatch_Type_Temp   STRING(30)
Model_Number_Temp    STRING(30)
Manufacturer_temp    STRING(30)
ESN_Temp             STRING(30)
despatch_date_temp   DATE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Courier_Temp
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
window               WINDOW('Despatch Information'),AT(,,219,132),FONT('Tahoma',8,,),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,96),USE(?Sheet1),SPREAD
                         TAB('Despatch Information'),USE(?Tab1)
                           PROMPT('Despatch Description'),AT(8,20),USE(?despatch_description_temp:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(despatch_description_temp),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,36),USE(?Model_Number_Temp:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(Model_Number_Temp),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Manufacturer'),AT(8,52),USE(?Manufacturer_temp:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(Manufacturer_temp),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('E.S.N. / I.M.E.I.'),AT(8,68),USE(?ESN_Temp:Prompt),TRN
                           ENTRY(@s30),AT(84,68,124,10),USE(ESN_Temp),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Courier'),AT(8,84),USE(?Prompt1)
                           COMBO(@s30),AT(84,84,124,10),USE(Courier_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON('&OK'),AT(100,108,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,108,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,104,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?despatch_description_temp:Prompt{prop:FontColor} = -1
    ?despatch_description_temp:Prompt{prop:Color} = 15066597
    If ?despatch_description_temp{prop:ReadOnly} = True
        ?despatch_description_temp{prop:FontColor} = 65793
        ?despatch_description_temp{prop:Color} = 15066597
    Elsif ?despatch_description_temp{prop:Req} = True
        ?despatch_description_temp{prop:FontColor} = 65793
        ?despatch_description_temp{prop:Color} = 8454143
    Else ! If ?despatch_description_temp{prop:Req} = True
        ?despatch_description_temp{prop:FontColor} = 65793
        ?despatch_description_temp{prop:Color} = 16777215
    End ! If ?despatch_description_temp{prop:Req} = True
    ?despatch_description_temp{prop:Trn} = 0
    ?despatch_description_temp{prop:FontStyle} = font:Bold
    ?Model_Number_Temp:Prompt{prop:FontColor} = -1
    ?Model_Number_Temp:Prompt{prop:Color} = 15066597
    If ?Model_Number_Temp{prop:ReadOnly} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 15066597
    Elsif ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 8454143
    Else ! If ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 16777215
    End ! If ?Model_Number_Temp{prop:Req} = True
    ?Model_Number_Temp{prop:Trn} = 0
    ?Model_Number_Temp{prop:FontStyle} = font:Bold
    ?Manufacturer_temp:Prompt{prop:FontColor} = -1
    ?Manufacturer_temp:Prompt{prop:Color} = 15066597
    If ?Manufacturer_temp{prop:ReadOnly} = True
        ?Manufacturer_temp{prop:FontColor} = 65793
        ?Manufacturer_temp{prop:Color} = 15066597
    Elsif ?Manufacturer_temp{prop:Req} = True
        ?Manufacturer_temp{prop:FontColor} = 65793
        ?Manufacturer_temp{prop:Color} = 8454143
    Else ! If ?Manufacturer_temp{prop:Req} = True
        ?Manufacturer_temp{prop:FontColor} = 65793
        ?Manufacturer_temp{prop:Color} = 16777215
    End ! If ?Manufacturer_temp{prop:Req} = True
    ?Manufacturer_temp{prop:Trn} = 0
    ?Manufacturer_temp{prop:FontStyle} = font:Bold
    ?ESN_Temp:Prompt{prop:FontColor} = -1
    ?ESN_Temp:Prompt{prop:Color} = 15066597
    If ?ESN_Temp{prop:ReadOnly} = True
        ?ESN_Temp{prop:FontColor} = 65793
        ?ESN_Temp{prop:Color} = 15066597
    Elsif ?ESN_Temp{prop:Req} = True
        ?ESN_Temp{prop:FontColor} = 65793
        ?ESN_Temp{prop:Color} = 8454143
    Else ! If ?ESN_Temp{prop:Req} = True
        ?ESN_Temp{prop:FontColor} = 65793
        ?ESN_Temp{prop:Color} = 16777215
    End ! If ?ESN_Temp{prop:Req} = True
    ?ESN_Temp{prop:Trn} = 0
    ?ESN_Temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?Courier_Temp{prop:ReadOnly} = True
        ?Courier_Temp{prop:FontColor} = 65793
        ?Courier_Temp{prop:Color} = 15066597
    Elsif ?Courier_Temp{prop:Req} = True
        ?Courier_Temp{prop:FontColor} = 65793
        ?Courier_Temp{prop:Color} = 8454143
    Else ! If ?Courier_Temp{prop:Req} = True
        ?Courier_Temp{prop:FontColor} = 65793
        ?Courier_Temp{prop:Color} = 16777215
    End ! If ?Courier_Temp{prop:Req} = True
    ?Courier_Temp{prop:Trn} = 0
    ?Courier_Temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'despatch_job',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'despatch_job',1)
    SolaceViewVars('despatch_description_temp',despatch_description_temp,'despatch_job',1)
    SolaceViewVars('Courier_Temp',Courier_Temp,'despatch_job',1)
    SolaceViewVars('Despatch_Type_Temp',Despatch_Type_Temp,'despatch_job',1)
    SolaceViewVars('Model_Number_Temp',Model_Number_Temp,'despatch_job',1)
    SolaceViewVars('Manufacturer_temp',Manufacturer_temp,'despatch_job',1)
    SolaceViewVars('ESN_Temp',ESN_Temp,'despatch_job',1)
    SolaceViewVars('despatch_date_temp',despatch_date_temp,'despatch_job',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_description_temp:Prompt;  SolaceCtrlName = '?despatch_description_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_description_temp;  SolaceCtrlName = '?despatch_description_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Temp:Prompt;  SolaceCtrlName = '?Model_Number_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Temp;  SolaceCtrlName = '?Model_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer_temp:Prompt;  SolaceCtrlName = '?Manufacturer_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer_temp;  SolaceCtrlName = '?Manufacturer_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN_Temp:Prompt;  SolaceCtrlName = '?ESN_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN_Temp;  SolaceCtrlName = '?ESN_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Courier_Temp;  SolaceCtrlName = '?Courier_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('despatch_job')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'despatch_job')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?despatch_description_temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:STATUS.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  set(defaults)
  access:defaults.next()
  access:jobs.clearkey(job:ref_number_key)
  job:ref_number = f_ref_number
  If access:jobs.fetch(job:ref_number_key)
      Return(Level:Fatal)
  End!If access:jobs.fetch(job:ref_number_key)
  !Loan To Despatch
  If job:loan_unit_number <> '' And job:loan_despatched = '' And job:loan_status = 'AWAITING DESPATCH'
      despatch_type_temp = 'LOAN'
  End!If job:loan_unit_number <> '' And job:loan_despatched = ''
  !Exchange To Despatch
  If job:exchange_unit_number <> '' And job:exchange_despatched = '' And job:exchange_status = 'AWAITING DESPATCH'
      despatch_type_temp = 'EXCHANGE'
  End
  !Job To Despatch
  If job:date_completed <> '' And job:despatched <> 'YES'
      If def:qa_required = 'YES'
          If job:qa_passed = 'YES' Or job:qa_second_passed = 'YES'
              despatch_type_temp = 'JOB'
          End!If job:qa_passed = 'YES'
      Else
          despatch_type_temp = 'JOB'
      End!If def:qa_required = 'YES'
      
  End!If job:exchange_unit_number <> '' And job:exchange_despatched = ''
  If despatch_type_temp = ''
      Case MessageEx('The selected job is not ready to be despatched.','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
          Of 1 ! &OK Button
      End!Case MessageEx
      Return(Level:Fatal)
  End!If despatch_type_temp = ''
  
  Case despatch_type_temp
      Of 'LOAN'
          despatch_description_temp = 'LOAN UNIT' & Clip(Format(job:loan_unit_number,@p<<<<<<<#p))
          access:loan.clearkey(loa:ref_number_key)
          loa:ref_number = job:loan_unit_number
          if access:loan.fetch(loa:ref_number_key)
              !Failed
          Else!if access:loan.fetch(loa:ref_number_key)
              model_number_temp   = loa:model_number
              manufacturer_temp   = loa:manufacturer
              esn_temp            = loa:esn
          end!if access:loan.fetch(loa:ref_number_key)
  
      Of 'EXCHANGE'
          despatch_description_temp = 'EXCHANGE UNIT : ' & Clip(Format(job:exchange_unit_number,@p<<<<<<<#p))
          access:exchange.clearkey(xch:ref_number_key)
          xch:ref_number = job:exchange_unit_number
          if access:exchange.fetch(xch:ref_number_key)
             !failed
          Else!if access:exchange.fetch(xch:ref_number_key)
              model_number_temp   = xch:model_number
              manufacturer_temp   = xch:manufacturer
              esn_temp            = xch:esn
          end!if access:exchange.fetch(xch:ref_number_key)
  
      Of 'JOB'
          despatch_description_temp  = 'JOB NUMBER : ' & Clip(Format(job:ref_number,@p<<<<<<<#p))
          model_number_temp   = job:model_number
          manufacturer_temp   = job:manufacturer
          esn_temp            = job:esn
  
  End!Case despatch_type_temp
  
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB2.Init(Courier_Temp,?Courier_Temp,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(cou:Courier_Key)
  FDCB2.AddField(cou:Courier,FDCB2.Q.cou:Courier)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'despatch_job',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If courier_temp = ''
          Case MessageEx('You have not selected a Courier.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If courier_temp = ''
      
          If courier_temp = 'CITY LINK' Or courier_temp = 'BUSINESS POST'
              Case despatch_type_temp
                  Of 'LOAN'
                      job:loan_courier = courier_temp
                      job:loan_status = 'READY TO DESPATCH'
                  Of 'EXCHANGE'
                      job:exchange_courier = courier_temp
                      job:exchange_status = 'READY TO DESPATCH'
                  OF 'JOB'
                      job:courier = courier_temp
                      GetStatus(810,1,'JOB') !ready to despatch
              End!Case despatch_type_temp
          Else!!If courier_temp = 'CITY LINK' Or courier_temp = 'BUSINESS POST'
              Case despatch_type_temp
                  Of 'LOAN'
                      job:loan_courier = courier_temp
                      job:loan_status = 'DESPATCHED'
                      JOB:Loan_Despatched = Today()
                      access:users.clearkey(use:password_key)
                      use:password = glo:password
                      access:users.fetch(use:password_key)
                      job:loan_despatched_user = use:user_code
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'COURIER: ' & Clip(Courier_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          JOB:Loan_Despatched_User = aud:user
                          aud:action        = 'LOAN UNIT ' & Clip(job:loan_unit_number) & ' DESPATCHED'
                          aud:type          = 'LOA'
                          if access:audit.insert()
                              access:audit.cancelautoinc()
                          end
                      end!if access:audit.primerecord() = level:benign
                  Of 'EXCHANGE'
                      job:exchange_courier = courier_temp
                      job:exchange_status = 'DESPATCHED'
                      JOB:exchange_Despatched = Today()
                      access:users.clearkey(use:password_key)
                      use:password = glo:password
                      access:users.fetch(use:password_key)
                      job:exchange_despatched_user = use:user_code
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'COURIER: ' & Clip(Courier_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          JOB:Loan_Despatched_User = aud:user
                          aud:action        = 'EXCHANGE UNIT ' & Clip(job:Exchange_Unit_number) & ' DESPATCHED'
                          aud:type          = 'EXC'
                          if access:audit.insert()
                              access:audit.cancelautoinc()
                          end
                      end!if access:audit.primerecord() = level:benign
      
                  OF 'JOB'
                      job:courier = courier_temp
                      If job:paid = 'YES'
                          GetStatus(910,1,'JOB') !despatched unpaid
                      Else!If job:paid = 'YES'
                          GetStatus(905,1,'JOB') !despatched paid
      
                      End!If job:paid = 'YES'
                      job:despatched = 'YES'
                      job:date_despatched = Today()
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = 'COURIER: ' & Clip(Courier_temp)
                          aud:ref_number    = job:ref_number
                          aud:date          = Today()
                          aud:time          = Clock()
                          access:users.clearkey(use:password_key)
                          use:password = glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          aud:action        = 'UNIT DESPATCHED'
                          if access:audit.insert()
                              access:audit.cancelautoinc()
                          end
                      end!if access:audit.primerecord() = level:benign
      
              End!Case despatch_type_temp
      
          End!If courier_temp = 'CITY LINK' Or courier_temp = 'BUSINESS POST'
          access:jobs.update()
          glo:select1 = job:ref_number
          despatch_note
          glo:select1 = ''
      End!If courier_temp = ''
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'despatch_job')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

