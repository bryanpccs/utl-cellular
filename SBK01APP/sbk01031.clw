

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Vsa_fwiz.inc'),ONCE

                     MAP
                       INCLUDE('SBK01031.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBK01026.INC'),ONCE        !Req'd for module callout resolution
                     END


Exceptions_Invoice_Wizard PROCEDURE                   !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::4:TAGFLAG          BYTE(0)
DASBRW::4:TAGMOUSE         BYTE(0)
DASBRW::4:TAGDISPSTATUS    BYTE(0)
DASBRW::4:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ThisThreadActive BYTE
tag_temp             STRING(1)
save_job_id          USHORT,AUTO
save_tra_id          USHORT,AUTO
save_sub_id          USHORT,AUTO
tmp:type             STRING('MAI')
tmp:browsetype       STRING(4)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW3::View:Browse    VIEW(TRADETMP)
                       PROJECT(tratmp:Account_Number)
                       PROJECT(tratmp:Company_Name)
                       PROJECT(tratmp:RefNumber)
                       PROJECT(tratmp:Type)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tag_temp               LIKE(tag_temp)                 !List box control field - type derived from local data
tag_temp_Icon          LONG                           !Entry's icon ID
tratmp:Account_Number  LIKE(tratmp:Account_Number)    !List box control field - type derived from field
tratmp:Company_Name    LIKE(tratmp:Company_Name)      !List box control field - type derived from field
tmp:browsetype         LIKE(tmp:browsetype)           !List box control field - type derived from local data
tratmp:RefNumber       LIKE(tratmp:RefNumber)         !Primary key field - type derived from field
tratmp:Type            LIKE(tratmp:Type)              !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Create Invoice Wizard'),AT(,,340,332),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,332,296),USE(?Sheet1),SPREAD
                         TAB,USE(?Tab1)
                           PROMPT('Zero Valued Exceptions Wizard'),AT(8,8,124,12),USE(?Prompt1),FONT('Tahoma',12,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Invoice Exceptions'),AT(92,28),USE(?Prompt4),FONT(,,,FONT:bold)
                           BUTTON('View Exceptions'),AT(264,52,68,16),USE(?ViewExceptions),LEFT,ICON('Cancel.gif')
                           PROMPT('Trade Accounts'),AT(92,76),USE(?Prompt2),FONT(,8,,FONT:bold)
                           PROMPT('Tag the Trade Accounts you wish to check for exceptions'),AT(92,88),USE(?Prompt3)
                           PROMPT('Select this option to view ONLY jobs that have previously been marked as Excepti' &|
   'ons.'),AT(92,40,244,16),USE(?Prompt3:2),TRN
                           SHEET,AT(88,100,244,176),USE(?Sheet2),SPREAD
                             TAB('By Account Number'),USE(?Tab3)
                               ENTRY(@s15),AT(92,116,64,10),USE(tratmp:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                             END
                             TAB('By Company Name'),USE(?Tab4)
                               ENTRY(@s30),AT(92,116,124,10),USE(tratmp:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             END
                             TAB('By Type'),USE(?Tab5)
                               ENTRY(@s15),AT(92,116,64,10),USE(tratmp:Account_Number,,?TRATMP:Account_Number:2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                               OPTION,AT(232,108,96,32),USE(tmp:type)
                                 RADIO('Main'),AT(256,116),USE(?TRATMP:Type:Radio1),VALUE('MAI')
                                 RADIO('Sub'),AT(296,116),USE(?TRATMP:Type:Radio2),VALUE('SUB')
                               END
                             END
                           END
                           LIST,AT(92,132,236,140),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11LI@s1@63L(2)|M~Account Number~@s15@123L(2)|M~Company Name~@s30@16L(2)|M~Type~@' &|
   's4@'),FROM(Queue:Browse)
                           BUTTON('&Tag'),AT(92,280,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('T&ag All'),AT(144,280,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('&Untag All'),AT(196,280,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                           BUTTON('&Rev tags'),AT(143,141,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(143,161,70,13),USE(?DASSHOWTAG),HIDE
                         END
                       END
                       BUTTON('&Finish'),AT(220,308,56,16),USE(?OkButton),LEFT,ICON('thumbs.gif'),DEFAULT
                       BUTTON('Close'),AT(276,308,56,16),USE(?cancelButton),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,304,332,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Back'),AT(8,308,56,16),USE(?VSBackButton),LEFT,ICON(ICON:VCRrewind)
                       BUTTON('&Next'),AT(64,308,56,16),USE(?VSNextButton),LEFT,ICON(ICON:VCRfastforward)
                       IMAGE('wiz.gif'),AT(8,112),USE(?Image1)
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string('Building Trade Account List..'),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
Wizard5         CLASS(FormWizardClass)
TakeNewSelection        PROCEDURE,VIRTUAL
TakeBackEmbed           PROCEDURE,VIRTUAL
TakeNextEmbed           PROCEDURE,VIRTUAL
Validate                PROCEDURE(),LONG,VIRTUAL
                   END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW3                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW3::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW3::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 1
BRW3::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 2
BRW3::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 3
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?tratmp:Account_Number{prop:ReadOnly} = True
        ?tratmp:Account_Number{prop:FontColor} = 65793
        ?tratmp:Account_Number{prop:Color} = 15066597
    Elsif ?tratmp:Account_Number{prop:Req} = True
        ?tratmp:Account_Number{prop:FontColor} = 65793
        ?tratmp:Account_Number{prop:Color} = 8454143
    Else ! If ?tratmp:Account_Number{prop:Req} = True
        ?tratmp:Account_Number{prop:FontColor} = 65793
        ?tratmp:Account_Number{prop:Color} = 16777215
    End ! If ?tratmp:Account_Number{prop:Req} = True
    ?tratmp:Account_Number{prop:Trn} = 0
    ?tratmp:Account_Number{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?tratmp:Company_Name{prop:ReadOnly} = True
        ?tratmp:Company_Name{prop:FontColor} = 65793
        ?tratmp:Company_Name{prop:Color} = 15066597
    Elsif ?tratmp:Company_Name{prop:Req} = True
        ?tratmp:Company_Name{prop:FontColor} = 65793
        ?tratmp:Company_Name{prop:Color} = 8454143
    Else ! If ?tratmp:Company_Name{prop:Req} = True
        ?tratmp:Company_Name{prop:FontColor} = 65793
        ?tratmp:Company_Name{prop:Color} = 16777215
    End ! If ?tratmp:Company_Name{prop:Req} = True
    ?tratmp:Company_Name{prop:Trn} = 0
    ?tratmp:Company_Name{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    If ?TRATMP:Account_Number:2{prop:ReadOnly} = True
        ?TRATMP:Account_Number:2{prop:FontColor} = 65793
        ?TRATMP:Account_Number:2{prop:Color} = 15066597
    Elsif ?TRATMP:Account_Number:2{prop:Req} = True
        ?TRATMP:Account_Number:2{prop:FontColor} = 65793
        ?TRATMP:Account_Number:2{prop:Color} = 8454143
    Else ! If ?TRATMP:Account_Number:2{prop:Req} = True
        ?TRATMP:Account_Number:2{prop:FontColor} = 65793
        ?TRATMP:Account_Number:2{prop:Color} = 16777215
    End ! If ?TRATMP:Account_Number:2{prop:Req} = True
    ?TRATMP:Account_Number:2{prop:Trn} = 0
    ?TRATMP:Account_Number:2{prop:FontStyle} = font:Bold
    ?tmp:type{prop:Font,3} = -1
    ?tmp:type{prop:Color} = 15066597
    ?tmp:type{prop:Trn} = 0
    ?TRATMP:Type:Radio1{prop:Font,3} = -1
    ?TRATMP:Type:Radio1{prop:Color} = 15066597
    ?TRATMP:Type:Radio1{prop:Trn} = 0
    ?TRATMP:Type:Radio2{prop:Font,3} = -1
    ?TRATMP:Type:Radio2{prop:Color} = 15066597
    ?TRATMP:Type:Radio2{prop:Trn} = 0
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::4:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW3.UpdateBuffer
   GLO:Queue.Pointer = tratmp:RefNumber
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = tratmp:RefNumber
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    tag_temp = ''
  END
    Queue:Browse.tag_temp = tag_temp
  IF (tag_temp = '*')
    Queue:Browse.tag_temp_Icon = 2
  ELSE
    Queue:Browse.tag_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW3.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = tratmp:RefNumber
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW3.Reset
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::4:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::4:QUEUE = GLO:Queue
    ADD(DASBRW::4:QUEUE)
  END
  FREE(GLO:Queue)
  BRW3.Reset
  LOOP
    NEXT(BRW3::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::4:QUEUE.Pointer = tratmp:RefNumber
     GET(DASBRW::4:QUEUE,DASBRW::4:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = tratmp:RefNumber
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW3.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::4:DASSHOWTAG Routine
   CASE DASBRW::4:TAGDISPSTATUS
   OF 0
      DASBRW::4:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::4:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::4:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW3.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Exceptions_Invoice_Wizard',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tag_temp',tag_temp,'Exceptions_Invoice_Wizard',1)
    SolaceViewVars('save_job_id',save_job_id,'Exceptions_Invoice_Wizard',1)
    SolaceViewVars('save_tra_id',save_tra_id,'Exceptions_Invoice_Wizard',1)
    SolaceViewVars('save_sub_id',save_sub_id,'Exceptions_Invoice_Wizard',1)
    SolaceViewVars('tmp:type',tmp:type,'Exceptions_Invoice_Wizard',1)
    SolaceViewVars('tmp:browsetype',tmp:browsetype,'Exceptions_Invoice_Wizard',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ViewExceptions;  SolaceCtrlName = '?ViewExceptions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tratmp:Account_Number;  SolaceCtrlName = '?tratmp:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tratmp:Company_Name;  SolaceCtrlName = '?tratmp:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRATMP:Account_Number:2;  SolaceCtrlName = '?TRATMP:Account_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:type;  SolaceCtrlName = '?tmp:type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRATMP:Type:Radio1;  SolaceCtrlName = '?TRATMP:Type:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRATMP:Type:Radio2;  SolaceCtrlName = '?TRATMP:Type:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cancelButton;  SolaceCtrlName = '?cancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSBackButton;  SolaceCtrlName = '?VSBackButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?VSNextButton;  SolaceCtrlName = '?VSNextButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Image1;  SolaceCtrlName = '?Image1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Exceptions_Invoice_Wizard')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Exceptions_Invoice_Wizard')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Exceptions_Invoice_Wizard'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?cancelButton,RequestCancelled)
  Relate:CHARTYPE.Open
  Relate:DEFAULTS.Open
  Relate:TRADETMP.Open
  Relate:VATCODE.Open
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBS.UseFile
  Access:STDCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:SUBCHRGE.UseFile
  SELF.FilesOpened = True
  recordspercycle     = 25
  recordsprocessed    = 0
  percentprogress     = 0
  setcursor(cursor:wait)
  open(progresswindow)
  progress:thermometer    = 0
  ?progress:pcttext{prop:text} = '0% Completed'
  
  recordstoprocess    = Records(tradeacc)
  
  
  save_tra_id = access:tradeacc.savefile()
  set(tra:account_number_key)
  loop
      if access:tradeacc.next()
         break
      end !if
      Do GetNextRecord2
      If tra:invoice_sub_accounts <> 'YES'
          get(tradetmp,0)
          if access:tradetmp.primerecord() = Level:Benign
              tratmp:account_number = tra:account_number
              tratmp:company_name   = tra:company_name
              tratmp:type           = 'MAI'
              if access:tradetmp.insert()
                 access:tradetmp.cancelautoinc()
              end
          end!if access:tradetmp.primerecord() = Level:Benign
      Else!If tra:invoice_sub_accounts = 'YES'
          save_sub_id = access:subtracc.savefile()
          access:subtracc.clearkey(sub:main_account_key)
          sub:main_account_number = tra:account_number
          set(sub:main_account_key,sub:main_account_key)
          loop
              if access:subtracc.next()
                 break
              end !if
              if sub:main_account_number <> tra:account_number      |
                  then break.  ! end if
              get(tradetmp,0)
              if access:tradetmp.primerecord() = Level:Benign
                  tratmp:account_number = sub:account_number
                  tratmp:company_name   = sub:company_name
                  tratmp:type           = 'SUB'
                  if access:tradetmp.insert()
                     access:tradetmp.cancelautoinc()
                  end
              end!if access:tradetmp.primerecord() = Level:Benign
          end !loop
          access:subtracc.restorefile(save_sub_id)
      End!If tra:invoice_sub_accounts = 'YES'
  end !loop
  access:tradeacc.restorefile(save_tra_id)
  
  setcursor()
  close(progresswindow)
  BRW3.Init(?List,Queue:Browse.ViewPosition,BRW3::View:Browse,Queue:Browse,Relate:TRADETMP,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
    Wizard5.Init(?Sheet1, |                           ! Sheet
                     ?VSBackButton, |                 ! Back button
                     ?VSNextButton, |                 ! Next button
                     ?OkButton, |                     ! OK button
                     ?CancelButton, |                 ! Cancel button
                     1, |                             ! Skip hidden tabs
                     1, |                             ! OK and Next in same location
                     1)                               ! Validate before allowing Next button to be pressed
  ! support for CPCS
  BRW3.Q &= Queue:Browse
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,tratmp:AccountNoKey)
  BRW3.AddLocator(BRW3::Sort1:Locator)
  BRW3::Sort1:Locator.Init(?tratmp:Account_Number,tratmp:Account_Number,1,BRW3)
  BRW3.AddSortOrder(,tratmp:CompanyNameKey)
  BRW3.AddLocator(BRW3::Sort2:Locator)
  BRW3::Sort2:Locator.Init(?tratmp:Company_Name,tratmp:Company_Name,1,BRW3)
  BRW3.AddSortOrder(,tratmp:TypeKey)
  BRW3.AddRange(tratmp:Type,tmp:type)
  BRW3.AddLocator(BRW3::Sort3:Locator)
  BRW3::Sort3:Locator.Init(?TRATMP:Account_Number:2,tratmp:Account_Number,1,BRW3)
  BRW3.AddSortOrder(,tratmp:AccountNoKey)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,tratmp:Account_Number,1,BRW3)
  BIND('tag_temp',tag_temp)
  BIND('tmp:browsetype',tmp:browsetype)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW3.AddField(tag_temp,BRW3.Q.tag_temp)
  BRW3.AddField(tratmp:Account_Number,BRW3.Q.tratmp:Account_Number)
  BRW3.AddField(tratmp:Company_Name,BRW3.Q.tratmp:Company_Name)
  BRW3.AddField(tmp:browsetype,BRW3.Q.tmp:browsetype)
  BRW3.AddField(tratmp:RefNumber,BRW3.Q.tratmp:RefNumber)
  BRW3.AddField(tratmp:Type,BRW3.Q.tratmp:Type)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab3{PROP:TEXT} = 'By Account Number'
    ?Tab4{PROP:TEXT} = 'By Company Name'
    ?Tab5{PROP:TEXT} = 'By Type'
    ?List{PROP:FORMAT} ='11LI@s1@#1#63L(2)|M~Account Number~@s15@#3#123L(2)|M~Company Name~@s30@#4#16L(2)|M~Type~@s4@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:CHARTYPE.Close
    Relate:DEFAULTS.Close
    Relate:TRADETMP.Close
    Relate:VATCODE.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Exceptions_Invoice_Wizard'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  Remove(tradetmp)
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Exceptions_Invoice_Wizard',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
     IF Wizard5.Validate()
        DISABLE(Wizard5.NextControl())
     ELSE
        ENABLE(Wizard5.NextControl())
     END
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?ViewExceptions
      ThisWindow.Update
      START(Browse_Invoice_Exceptions, 25000)
      ThisWindow.Reset
    OF ?tmp:type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:type, Accepted)
      BRW3.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:type, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::4:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      recordspercycle     = 25
      recordsprocessed    = 0
      percentprogress     = 0
      setcursor(cursor:wait)
      open(progresswindow)
      progress:thermometer    = 0
      ?progress:pcttext{prop:text} = '0% Completed'
      ?progress:userstring{prop:text} = ''
      recordstoprocess    =  Records(glo:queue)
      
      Set(defaults)
      access:defaults.next()
      count# = 0
      Loop x# = 1 To Records(glo:queue)
          Get(glo:queue,x#)
          Do GetNextRecord2
      
          do_sub# = 0
          access:tradetmp.clearkey(tratmp:RefNumberKey)
          tratmp:RefNumber    = glo:pointer
          If access:tradetmp.tryfetch(tratmp:RefNumberKey) = Level:Benign
              Case tratmp:type
                  Of 'MAI'
                      access:tradeacc.clearkey(tra:account_number_key)
                      tra:account_number  = tratmp:Account_Number
                      access:tradeacc.tryfetch(tra:account_number_key)
                      IF tra:use_sub_accounts = 'YES'
                          do_sub# = 1
                      Else!IF tra:use_sub_accounts = 'YES'
                          do_sub# = 0
                      End!IF tra:use_sub_accounts = 'YES'
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:labour_vat_code
                      if access:vatcode.fetch(vat:vat_code_key)
                          If tralabcheck# = 0
                              setcursor()
                              Case MessageEx('Sub Account: '&Clip(tra:account_number)&'.<13,10><13,10>There is NO Labour V.A.T. Rate setup for this account.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',tralabcheck#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              setcursor(cursor:wait)
                              Cycle
                          End!If sublabcheck# = 0
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                      access:vatcode.clearkey(vat:vat_code_key)
                      vat:vat_code = tra:parts_vat_code
                      if access:vatcode.fetch(vat:vat_code_key)
                          If traparcheck# = 0
                              setcursor()
                              Case MessageEx('Sub Account: '&Clip(tra:account_number)&'.<13,10><13,10>There is NO Parts V.A.T. Rate setup for this account.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',traparcheck#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              setcursor(cursor:wait)
                              Cycle
                          End!If sublabcheck# = 0
                      end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
      
                  Of 'SUB'
                      access:subtracc.clearkey(sub:account_number_key)
                      sub:account_number  = tratmp:account_number
                      If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                          access:tradeacc.clearkey(tra:account_number_key)
                          tra:account_number  = tratmp:Account_Number
                          access:tradeacc.tryfetch(tra:account_number_key)
                          if tra:invoice_sub_accounts = 'YES'
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = sub:labour_vat_code
                              if access:vatcode.fetch(vat:vat_code_key)
                                  If sublabcheck# = 0
                                      setcursor()
                                      Case MessageEx('Sub Account: '&Clip(sub:account_number)&'.<13,10><13,10>There is NO Labour V.A.T. Rate setup for this account.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',sublabcheck#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      setcursor(cursor:wait)
                                      Cycle
                                  End!If sublabcheck# = 0
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = sub:parts_vat_code
                              if access:vatcode.fetch(vat:vat_code_key)
                                  If subparcheck# = 0
                                      setcursor()
                                      Case MessageEx('Sub Account: '&Clip(sub:account_number)&'.<13,10><13,10>There is NO Parts V.A.T. Rate setup for this account.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',subparcheck#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      setcursor(cursor:wait)
                                      Cycle
                                  End!If sublabcheck# = 0
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                          else!if tra:use_sub_accounts = 'YES'
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = tra:labour_vat_code
                              if access:vatcode.fetch(vat:vat_code_key)
                                  If tralabcheck# = 0
                                      setcursor()
                                      Case MessageEx('Sub Account: '&Clip(tra:account_number)&'.<13,10><13,10>There is NO Labour V.A.T. Rate setup for this account.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',tralabcheck#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      setcursor(cursor:wait)
                                      Cycle
                                  End!If sublabcheck# = 0
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                              access:vatcode.clearkey(vat:vat_code_key)
                              vat:vat_code = tra:parts_vat_code
                              if access:vatcode.fetch(vat:vat_code_key)
                                  If traparcheck# = 0
                                      setcursor()
                                      Case MessageEx('Sub Account: '&Clip(tra:account_number)&'.<13,10><13,10>There is NO Parts V.A.T. Rate setup for this account.','ServiceBase 2000',|
                                                     'Styles\stop.ico','|&OK',1,1,'Ignore Further Errors',traparcheck#,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                                          Of 1 ! &OK Button
                                      End!Case MessageEx
                                      setcursor(cursor:wait)
                                      Cycle
                                  End!If sublabcheck# = 0
                              end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
                          end!if tra:use_sub_accounts = 'YES'
      
                      End!If access:tradeacc.tryfetch(sub:account_number_key) = Level:Benign
              End!Case tratmp:type
          End!If access:tradetmp.tryfetch(tratmp:RefNumberKey) = Level:Benign
      
          
      
          If do_sub# = 1
              save_sub_id = access:subtracc.savefile()
              access:subtracc.clearkey(sub:main_account_key)
              sub:main_account_number = tratmp:account_number
              set(sub:main_account_key,sub:main_account_key)
              loop
                  if access:subtracc.next()
                     break
                  end !if
                  if sub:main_account_number <> tratmp:account_number      |
                      then break.  ! end if
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:chainvoicekey)
                  job:chargeable_job = 'YES'
                  job:account_number = sub:account_number
                  job:invoice_number = ''
                  set(job:chainvoicekey,job:chainvoicekey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:chargeable_job <> 'YES'      |
                      or job:account_number <> sub:account_number      |
                      or job:invoice_number <> ''      |
                          then break.  ! end if
      
                      Include('invexcep.inc')
                  end !loop
                  access:jobs.restorefile(save_job_id)
      
              end !loop
              access:subtracc.restorefile(save_sub_id)
          Else!If do_sub# = 1
              save_job_id = access:jobs.savefile()
              access:jobs.clearkey(job:chainvoicekey)
              job:chargeable_job = 'YES'
              job:account_number = tratmp:account_number
              job:invoice_number = ''
              set(job:chainvoicekey,job:chainvoicekey)
              loop
                  if access:jobs.next()
                     break
                  end !if
                  if job:chargeable_job <> 'YES'      |
                  or job:account_number <> tratmp:account_number      |
                  or job:invoice_number <> ''      |
                      then break.  ! end if
                  Include('invexcep.inc')
              end !loop
              access:jobs.restorefile(save_job_id)
      
          End!If do_sub# = 1
      
      End!Loop x# = 1 To Records(glo:queue)
      
      
      setcursor()
      close(progresswindow)
      
      If count# = 0
          Case MessageEx('There are no NEW Invoice Exceptions.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If count# = 0
          Browse_Invoice_Exceptions
      End!If count# = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?VSBackButton
      ThisWindow.Update
         Wizard5.TakeAccepted()
    OF ?VSNextButton
      ThisWindow.Update
         Wizard5.TakeAccepted()
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Exceptions_Invoice_Wizard')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet2
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet2)
        OF 1
          ?List{PROP:FORMAT} ='11LI@s1@#1#63L(2)|M~Account Number~@s15@#3#123L(2)|M~Company Name~@s30@#4#16L(2)|M~Type~@s4@#5#'
          ?Tab3{PROP:TEXT} = 'By Account Number'
        OF 2
          ?List{PROP:FORMAT} ='11LI@s1@#1#123L(2)|M~Company Name~@s30@#4#63L(2)|M~Account Number~@s15@#3#16L(2)|M~Type~@s4@#5#'
          ?Tab4{PROP:TEXT} = 'By Company Name'
        OF 3
          ?List{PROP:FORMAT} ='11LI@s1@#1#63L(2)|M~Account Number~@s15@#3#123L(2)|M~Company Name~@s30@#4#16L(2)|M~Type~@s4@#5#'
          ?Tab5{PROP:TEXT} = 'By Type'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::4:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?tratmp:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Account_Number, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Account_Number, Selected)
    OF ?tratmp:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Company_Name, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tratmp:Company_Name, Selected)
    OF ?TRATMP:Account_Number:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TRATMP:Account_Number:2, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TRATMP:Account_Number:2, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::4:DASUNTAGALL
      !thismakeover.setwindow(win:window)
      Select(?List)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW3.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet2) = 3
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW3.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = tratmp:RefNumber
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tag_temp = ''
    ELSE
      tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  CASE (tratmp:Type)
  OF 'MAI'
    tmp:browsetype = 'Main'
  ELSE
    tmp:browsetype = 'Sub'
  END
  PARENT.SetQueueRecord
  IF (tag_temp = '*')
    SELF.Q.tag_temp_Icon = 2
  ELSE
    SELF.Q.tag_temp_Icon = 1
  END
  SELF.Q.tmp:browsetype = tmp:browsetype              !Assign formula result to display queue


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW3::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW3::RecordStatus=ReturnValue
  IF BRW3::RecordStatus NOT=Record:OK THEN RETURN BRW3::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = tratmp:RefNumber
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::4:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW3::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW3::RecordStatus
  RETURN ReturnValue

Wizard5.TakeNewSelection PROCEDURE
   CODE
    PARENT.TakeNewSelection()

    IF NOT(BRW3.Q &= NULL) ! Has Browse Object been initialized?
       BRW3.ResetQueue(Reset:Queue)
    END

Wizard5.TakeBackEmbed PROCEDURE
   CODE

Wizard5.TakeNextEmbed PROCEDURE
   CODE

Wizard5.Validate PROCEDURE
   CODE
    ! Remember to check the {prop:visible} attribute before validating
    ! a field.
    RETURN False
