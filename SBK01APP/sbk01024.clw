

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01024.INC'),ONCE        !Local module procedure declarations
                     END


Validate_ESN PROCEDURE (f_old_esn,f_esn,f_validated)  !Generated from procedure template - Window

FilesOpened          BYTE
old_esn_temp         STRING(16)
esn_temp             STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Window               WINDOW('Validate E.S.N. / I.M.E.I.'),AT(,,242,88),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,236,52),USE(?Sheet1),SPREAD
                         TAB('Validate E.S.N. / I.M.E.I.'),USE(?Tab1)
                           PROMPT('E.S.N. / I.M.E.I. To Despatch'),AT(8,20),USE(?JOB:ESN:Prompt),TRN
                           ENTRY(@s30),AT(108,20,124,10),USE(old_esn_temp),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('E.S.N. / I.M.E.I. In Hand'),AT(8,36),USE(?esn_temp:Prompt),TRN
                           ENTRY(@s30),AT(108,36,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       PANEL,AT(4,60,236,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(124,64,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(180,64,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?JOB:ESN:Prompt{prop:FontColor} = -1
    ?JOB:ESN:Prompt{prop:Color} = 15066597
    If ?old_esn_temp{prop:ReadOnly} = True
        ?old_esn_temp{prop:FontColor} = 65793
        ?old_esn_temp{prop:Color} = 15066597
    Elsif ?old_esn_temp{prop:Req} = True
        ?old_esn_temp{prop:FontColor} = 65793
        ?old_esn_temp{prop:Color} = 8454143
    Else ! If ?old_esn_temp{prop:Req} = True
        ?old_esn_temp{prop:FontColor} = 65793
        ?old_esn_temp{prop:Color} = 16777215
    End ! If ?old_esn_temp{prop:Req} = True
    ?old_esn_temp{prop:Trn} = 0
    ?old_esn_temp{prop:FontStyle} = font:Bold
    ?esn_temp:Prompt{prop:FontColor} = -1
    ?esn_temp:Prompt{prop:Color} = 15066597
    If ?esn_temp{prop:ReadOnly} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 15066597
    Elsif ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 8454143
    Else ! If ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 16777215
    End ! If ?esn_temp{prop:Req} = True
    ?esn_temp{prop:Trn} = 0
    ?esn_temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Validate_ESN',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Validate_ESN',1)
    SolaceViewVars('old_esn_temp',old_esn_temp,'Validate_ESN',1)
    SolaceViewVars('esn_temp',esn_temp,'Validate_ESN',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:ESN:Prompt;  SolaceCtrlName = '?JOB:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?old_esn_temp;  SolaceCtrlName = '?old_esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp:Prompt;  SolaceCtrlName = '?esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp;  SolaceCtrlName = '?esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Validate_ESN')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Validate_ESN')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JOB:ESN:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  old_esn_temp    = f_old_esn
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Validate_ESN',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      f_esn   = esn_temp
      If esn_temp <> old_esn_temp
          f_validated = False
      Else!If esn_temp <> job:esn
          f_validated = True
      End!If esn_temp <> job:esn
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Validate_ESN')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

