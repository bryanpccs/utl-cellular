

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01042.INC'),ONCE        !Local module procedure declarations
                     END


ExternalReports PROCEDURE                             !Generated from procedure template - Window

MoveSeq:BRW4::MoveType BYTE
MoveSeq:BRW4::OutOfRange BYTE
MoveSeq:BRW4::OrigSequence LONG
MoveSeq:BRW4::TargetSequence LONG
MoveSeq:BRW4::SwapSequence LONG
MoveSeq:BRW4::SavePosition STRING(255)
MoveSeq:BRW4::SaveSequence LONG
MoveSeq:BRW4::InsertSequence LONG
MoveSeq:BRW4::DeleteSequence LONG
MoveSeq:BRW4::Section CSTRING(24)
MoveSeq:BRW4::DoNotCommit BYTE
MoveSeq:BRW4::DragSequence LONG
MoveSeq:BRW4::DropSequence LONG
ThisThreadActive BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
MS::MoveType         BYTE
MS::OutOfRange       BYTE
MS::OrigSequence     LONG
MS::TargetSequence   LONG
MS::SwapSequence     LONG
MS::SavePosition     STRING(255)
MS::SaveSequence     LONG
MS::InsertSequence   LONG
MS::DeleteSequence   LONG
MS::Section          CSTRING(24)
MS::DoNotCommit      BYTE
BRW4::View:Browse    VIEW(REPEXTTP)
                       PROJECT(rpt:ReportType)
                       PROJECT(rpt:RecordNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
rpt:ReportType         LIKE(rpt:ReportType)           !List box control field - type derived from field
rpt:RecordNumber       LIKE(rpt:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(REPEXTRP)
                       PROJECT(rex:ReportName)
                       PROJECT(rex:EXEName)
                       PROJECT(rex:RecordNumber)
                       PROJECT(rex:ReportType)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
rex:ReportName         LIKE(rex:ReportName)           !List box control field - type derived from field
rex:EXEName            LIKE(rex:EXEName)              !List box control field - type derived from field
rex:RecordNumber       LIKE(rex:RecordNumber)         !Primary key field - type derived from field
rex:ReportType         LIKE(rex:ReportType)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Custom Reports'),AT(,,391,243),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,156,236),USE(?Sheet1),WIZARD,SPREAD
                         TAB('By Report Type'),USE(?Tab1)
                           PROMPT('By Report Type'),AT(8,8),USE(?Prompt1:2)
                           BUTTON('&Insert'),AT(60,8,36,12),USE(?Insert),HIDE
                           BUTTON('&Change'),AT(96,8,36,12),USE(?Change),HIDE
                           BUTTON('&Delete'),AT(132,8,32,12),USE(?Delete),HIDE
                           BUTTON('Move Up'),AT(8,20,64,12),USE(?MoveUp),LEFT,TIP('Move record up one'),ICON('VSMOVEUP.ICO')
                           BUTTON,AT(48,76,16,14),USE(?MoveBegin),HIDE,TIP('Move record to the top'),ICON('VSMOVEBG.ICO')
                           BUTTON('Move Down'),AT(92,20,64,12),USE(?MoveDown),LEFT,TIP('Move record down one'),ICON('VSMOVEDN.ICO')
                           BUTTON,AT(68,76,16,14),USE(?MoveEnd),HIDE,TIP('Move record to the bottom'),ICON('VSMOVEND.ICO')
                           LIST,AT(8,36,148,200),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('320L(2)|M~Report Type~@s80@'),FROM(Queue:Browse)
                         END
                       END
                       SHEET,AT(164,4,224,236),USE(?Sheet2),WIZARD,SPREAD
                         TAB('By Report Name'),USE(?Tab2)
                           PROMPT('By Report Name'),AT(168,8),USE(?Prompt1)
                           BUTTON('Cancel'),AT(328,8,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                           PROMPT('Double-click the report to run'),AT(168,22),USE(?Prompt3),FONT(,,COLOR:White,FONT:bold)
                           BUTTON('&Insert'),AT(220,8,36,12),USE(?Insert:2),HIDE
                           BUTTON('&Change'),AT(256,8,36,12),USE(?Change:2),HIDE
                           BUTTON('&Delete'),AT(292,8,36,12),USE(?Delete:2),HIDE
                           LIST,AT(168,36,216,200),USE(?List:2),IMM,MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('320L(2)|M~Report Name~@s80@120L(2)|M~EXEName~@s30@'),FROM(Queue:Browse:1)
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ExternalReports',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('MS::MoveType',MS::MoveType,'ExternalReports',1)
    SolaceViewVars('MS::OutOfRange',MS::OutOfRange,'ExternalReports',1)
    SolaceViewVars('MS::OrigSequence',MS::OrigSequence,'ExternalReports',1)
    SolaceViewVars('MS::TargetSequence',MS::TargetSequence,'ExternalReports',1)
    SolaceViewVars('MS::SwapSequence',MS::SwapSequence,'ExternalReports',1)
    SolaceViewVars('MS::SavePosition',MS::SavePosition,'ExternalReports',1)
    SolaceViewVars('MS::SaveSequence',MS::SaveSequence,'ExternalReports',1)
    SolaceViewVars('MS::InsertSequence',MS::InsertSequence,'ExternalReports',1)
    SolaceViewVars('MS::DeleteSequence',MS::DeleteSequence,'ExternalReports',1)
    SolaceViewVars('MS::Section',MS::Section,'ExternalReports',1)
    SolaceViewVars('MS::DoNotCommit',MS::DoNotCommit,'ExternalReports',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveUp;  SolaceCtrlName = '?MoveUp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveBegin;  SolaceCtrlName = '?MoveBegin';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveDown;  SolaceCtrlName = '?MoveDown';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MoveEnd;  SolaceCtrlName = '?MoveEnd';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)


BRW4::SequencedMoveUp ROUTINE
  MoveSeq:BRW4::MoveType=1
  DO BRW4::SequencedMove

BRW4::SequencedMoveDown ROUTINE
  MoveSeq:BRW4::MoveType=2                            ! "Up" to the user is "down" sequentially
  DO BRW4::SequencedMove

BRW4::SequencedMoveBegin ROUTINE
  MoveSeq:BRW4::MoveType=3
  DO BRW4::SequencedMove

BRW4::SequencedMoveEnd ROUTINE
  MoveSeq:BRW4::MoveType=4
  DO BRW4::SequencedMove

BRW4::SequencedMoveDragAndDrop ROUTINE
  IF MoveSeq:BRW4::DragSequence AND MoveSeq:BRW4::DropSequence
     MoveSeq:BRW4::MoveType=CHOOSE(MoveSeq:BRW4::DropSequence<MoveSeq:BRW4::DragSequence,7,8)
     DO BRW4::SequencedMove
  END
  CLEAR(MoveSeq:BRW4::DragSequence)
  CLEAR(MoveSeq:BRW4::DropSequence)

BRW4::SequencedMove ROUTINE
  !--------------------- Initialization
  MoveSeq:BRW4::DoNotCommit=False
  MoveSeq:BRW4::OrigSequence=rpt:RecordNumber         ! Save original sequence
  LOGOUT(1,REPEXTTP)
  !--------------------- End:  Initialization
  !--------------------- Test if we can move
  DO MoveSeq:BRW4::SetToOriginalRecord
  DO MoveSeq:BRW4::GetNextSequencedRecord             ! Try to get next record
  IF MoveSeq:BRW4::OutOfRange                         ! If nothing to swap with,
     DO MoveSeq:BRW4::CleanUp                         ! quietly exit
     EXIT                                             ! No movement possible
  END
  MoveSeq:BRW4::TargetSequence=rpt:RecordNumber       ! Save target sequence value

  !--------------------- End:  Test if we can move
  !--------------------- Move current record to a temporary location
  DO MoveSeq:BRW4::PrimeFieldsHigh
  SET(rpt:RecordNumberKey,rpt:RecordNumberKey)
  MoveSeq:BRW4::OutOfRange=Access:REPEXTTP.Previous() ! Find last in sequence
  MoveSeq:BRW4::SwapSequence=rpt:RecordNumber+1       ! Save the sequence number of the last in this order

  DO MoveSeq:BRW4::PrimeFieldsLow
  rpt:RecordNumber=MoveSeq:BRW4::OrigSequence         ! Restore original record
  MoveSeq:BRW4::OutOfRange=Access:REPEXTTP.Fetch(rpt:RecordNumberKey)

  rpt:RecordNumber=MoveSeq:BRW4::SwapSequence         ! Place the current record in the new location
  IF Access:REPEXTTP.Update()
     MoveSeq:BRW4::Section='Put Swap Out'
     IF DUPLICATE(REPEXTTP) AND NOT(DUPLICATE(rpt:RecordNumberKey))
        MoveSeq:BRW4::Section='Put Swap Out (NKD)'
     END
     DO MoveSeq:BRW4::ErrorCheck
     DO MoveSeq:BRW4::CleanUp                         ! quietly exit
     EXIT
  END
  !--------------------- End:  Move current record to a temporary location

  !--------------------- Move all other records
  DO MoveSeq:BRW4::SetToOriginalRecord
  LOOP WHILE NOT(MoveSeq:BRW4::OutOfRange) AND rpt:RecordNumber<>MoveSeq:BRW4::SwapSequence
     MoveSeq:BRW4::TargetSequence=rpt:RecordNumber    ! Save next target
     rpt:RecordNumber=MoveSeq:BRW4::OrigSequence      ! Change to previous record's location
     IF Access:REPEXTTP.Update()                      ! Update the file
        MoveSeq:BRW4::Section='Shift Stage'
        DO MoveSeq:BRW4::ErrorCheck
     END
     IF MoveSeq:BRW4::MoveType<3                      ! Move, or finish
        BREAK                                         ! Move once, then done
     ELSE
        DO MoveSeq:BRW4::GetNextSequencedRecord
     END
     MoveSeq:BRW4::OrigSequence=MoveSeq:BRW4::TargetSequence
  END
  !--------------------- End:  Move all other records

  !--------------------- Move original record into final position
  IF rpt:RecordNumber<>MoveSeq:BRW4::SwapSequence OR MoveSeq:BRW4::OutOfRange
     DO MoveSeq:BRW4::PrimeFieldsLow
     rpt:RecordNumber=MoveSeq:BRW4::SwapSequence      ! Restore original record
     MoveSeq:BRW4::OutOfRange=Access:REPEXTTP.Fetch(rpt:RecordNumberKey)
     IF MoveSeq:BRW4::OutOfRange
        MoveSeq:BRW4::Section='Prepare for Final Move'
        DO MoveSeq:BRW4::ErrorCheck
     END
  END

  rpt:RecordNumber=MoveSeq:BRW4::TargetSequence
  IF Access:REPEXTTP.Update()                         ! Update the file
     MoveSeq:BRW4::Section='Final Move'
     DO MoveSeq:BRW4::ErrorCheck
  END
  !--------------------- End:  Move original record into final position

  DO MoveSeq:BRW4::CleanUp                            ! Logout, flush, etc.

  DO MoveSeq:BRW4::SetToTargetRecord
  BRW4.ResetFromBuffer()
  SELECT(?List)


MoveSeq:BRW4::ErrorCheck ROUTINE
  IF ERRORCODE()
     MoveSeq:BRW4::DoNotCommit=True
     MESSAGE('Record swap error: <13>' & ERROR() & '<13>REPEXTTP, rpt:RecordNumberKey' |
       & '<13>Key Field rpt:RecordNumber = ' & rpt:RecordNumber |
       & '<13>Section = ' & MoveSeq:BRW4::Section |
       & '<13>' ,'Error!')

  END

MoveSeq:BRW4::CleanUp ROUTINE
  IF MoveSeq:BRW4::DoNotCommit=True
     ROLLBACK()
  ELSE
     COMMIT()
  END

MoveSeq:BRW4::PrimeFieldsLow ROUTINE
   CLEAR(rpt:Record,-1)

MoveSeq:BRW4::PrimeFieldsHigh ROUTINE
   CLEAR(rpt:Record,1)

MoveSeq:BRW4::GetNextSequencedRecord ROUTINE
  MoveSeq:BRW4::OutOfRange=0
  EXECUTE (2 - MoveSeq:BRW4::MoveType % 2)
     MoveSeq:BRW4::OutOfRange=Access:REPEXTTP.TryPrevious()
     MoveSeq:BRW4::OutOfRange=Access:REPEXTTP.TryNext()
  END
  DO MoveSeq:BRW4::TestSequencedRange

  ! Extra drag and drop range check
  CASE MoveSeq:BRW4::MoveType
  OF 7
     IF rpt:RecordNumber<=MoveSeq:BRW4::DropSequence
        MoveSeq:BRW4::OutOfRange=1
     END
  OF 8
     IF rpt:RecordNumber>MoveSeq:BRW4::DropSequence
        MoveSeq:BRW4::OutOfRange=1
     END
  END

MoveSeq:BRW4::SetToOriginalRecord ROUTINE
  DO MoveSeq:BRW4::PrimeFieldsLow
  rpt:RecordNumber=MoveSeq:BRW4::OrigSequence         ! Start with the current record
  SET(rpt:RecordNumberKey,rpt:RecordNumberKey)
  DO MoveSeq:BRW4::GetNextSequencedRecord             ! Get current record

MoveSeq:BRW4::SetToTargetRecord ROUTINE
    DO MoveSeq:BRW4::PrimeFieldsLow
    rpt:RecordNumber=MoveSeq:BRW4::TargetSequence     ! Start with the current record
    SET(rpt:RecordNumberKey,rpt:RecordNumberKey)
    DO MoveSeq:BRW4::GetNextSequencedRecord           ! Get current record

MoveSeq:BRW4::TestSequencedRange ROUTINE
  !IF ERRORCODE()=33 |
  IF MoveSeq:BRW4::OutOfRange=1 |
     THEN
        MoveSeq:BRW4::OutOfRange=1
  END


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ExternalReports')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ExternalReports')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1:2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='ExternalReports'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:REPEXTRP.Open
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:REPEXTTP,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:REPEXTRP,SELF)
  OPEN(window)
  SELF.Opened=True
  If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
      ?Insert{prop:hide} = 0
      ?Change{prop:hide} = 0
      ?Delete{prop:hide} = 0
      ?Insert:2{prop:hide} = 0
      ?Change{prop:hide} = 0
      ?Delete{prop:hide} = 0
  End!If FullAccess(glo:Password,glo:PassAccount) = Level:Benign
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,rpt:RecordNumberKey)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,rpt:RecordNumber,1,BRW4)
  BRW4.AddField(rpt:ReportType,BRW4.Q.rpt:ReportType)
  BRW4.AddField(rpt:RecordNumber,BRW4.Q.rpt:RecordNumber)
  BRW5.Q &= Queue:Browse:1
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,rex:ReportNameKey)
  BRW5.AddRange(rex:ReportType,rpt:ReportType)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,rex:ReportName,1,BRW5)
  BRW5.AddField(rex:ReportName,BRW5.Q.rex:ReportName)
  BRW5.AddField(rex:EXEName,BRW5.Q.rex:EXEName)
  BRW5.AddField(rex:RecordNumber,BRW5.Q.rex:RecordNumber)
  BRW5.AddField(rex:ReportType,BRW5.Q.rex:ReportType)
  BRW4.AskProcedure = 1
  BRW5.AskProcedure = 2
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPEXTRP.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='ExternalReports'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ExternalReports',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  If KeyCode() = MouseLeft2 or FullAccess(glo:PassAccount,glo:Password)
      If Request = InsertRecord
          Case Number
              Of 1
                  Access:REPEXTTP.Cancelautoinc()
              Of 2
                  Access:REPEXTRP.Cancelautoinc()
          End!Case Number
      End!If Request = InsertRecord
      Return Level:Fatal
  End!If KeyCode() <> MouseLeft2
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateReportTypes
      UpdateReportNames
    END
    ReturnValue = GlobalResponse
  END
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?MoveUp
      ThisWindow.Update
      DO BRW4::SequencedMoveUp
    OF ?MoveBegin
      ThisWindow.Update
      DO BRW4::SequencedMoveBegin
    OF ?MoveDown
      ThisWindow.Update
      DO BRW4::SequencedMoveDown
    OF ?MoveEnd
      ThisWindow.Update
      DO BRW4::SequencedMoveEnd
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ExternalReports')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List:2
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
      If KeyCode() = MouseLeft2
          Run(Clip(Brw5.q.REX:EXEName) & ' %' & Clip(glo:Password))
      End!If KeyCode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

