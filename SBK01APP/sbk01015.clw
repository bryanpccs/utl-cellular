

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBK01015.INC'),ONCE        !Local module procedure declarations
                     END





Reconcile_Claims PROCEDURE                            !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
ThisThreadActive BYTE
CurrentTab           STRING(80)
save_inv_ali_id      USHORT,AUTO
invoice_number_temp  REAL
Manufacturer2_temp   STRING(30)
total_jobs_temp      STRING(20)
manufacturer3_temp   STRING(30)
reconcile_group      GROUP,PRE()
claim_reference_temp STRING(30)
Total_Claimed_temp   STRING(30)
Courier_Paid_Temp    STRING(30)
Labour_Paid_temp     STRING(30)
Parts_Paid_temp      STRING(30)
                     END
FilesOpened          BYTE
Manufacturer_Temp    STRING(30)
Batch_Number_Temp    LONG
jobs_tag             STRING(1)
jobs_tag_temp        STRING(1)
reconciled_temp      STRING('U')
Batch_Number2_Temp   REAL
Charge_Repair_Temp   STRING(60)
claim_total_temp     REAL
overpaid_by_temp     REAL
yes_temp             STRING('YES')
fin_temp             STRING('FIN')
Batch_total_temp     REAL
MarkOption           BYTE(1)
SelectAll            BYTE(0)
BrFilter1            STRING(300)
BrLocator1           STRING(50)
BrFilter2            STRING(300)
BrLocator2           STRING(30)
Query                STRING(500)
jfc1                 STRING(30)
jfc2                 STRING(30)
jfc3                 STRING(30)
jfc4                 STRING(30)
jfc5                 STRING(30)
jfc6                 STRING(30)
jfc7                 STRING(30)
jfc8                 STRING(30)
jfc9                 STRING(30)
jfc10                STRING(255)
jfc11                STRING(255)
jfc12                STRING(255)
tmp:BrowseString     STRING(1000)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Manufacturer_Temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Batch_Number_Temp
ebt:Batch_Number       LIKE(ebt:Batch_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?Manufacturer2_temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:3 QUEUE                           !Queue declaration for browse/combo box using ?Batch_Number2_Temp
ebt:Batch_Number       LIKE(ebt:Batch_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:4 QUEUE                           !Queue declaration for browse/combo box using ?manufacturer3_temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW9::View:Browse    VIEW(INVOICE)
                       PROJECT(inv:Invoice_Number)
                       PROJECT(inv:Date_Created)
                       PROJECT(inv:Total_Claimed)
                       PROJECT(inv:Claim_Reference)
                       PROJECT(inv:Manufacturer)
                       PROJECT(inv:Batch_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
inv:Invoice_Number     LIKE(inv:Invoice_Number)       !List box control field - type derived from field
inv:Date_Created       LIKE(inv:Date_Created)         !List box control field - type derived from field
Total_Claimed_temp     LIKE(Total_Claimed_temp)       !List box control field - type derived from local data
Batch_Number2_Temp     LIKE(Batch_Number2_Temp)       !Browse hot field - type derived from local data
inv:Total_Claimed      LIKE(inv:Total_Claimed)        !Browse hot field - type derived from field
inv:Claim_Reference    LIKE(inv:Claim_Reference)      !Browse hot field - type derived from field
inv:Manufacturer       LIKE(inv:Manufacturer)         !Browse key field - type derived from field
inv:Batch_Number       LIKE(inv:Batch_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:MSN)
                       PROJECT(job:Warranty_Charge_Type)
                       PROJECT(job:Repair_Type_Warranty)
                       PROJECT(job:Courier_Cost_Warranty)
                       PROJECT(job:Labour_Cost_Warranty)
                       PROJECT(job:Parts_Cost_Warranty)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Manufacturer)
                       PROJECT(job:EDI)
                       PROJECT(job:EDI_Batch_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
jobs_tag_temp          LIKE(jobs_tag_temp)            !List box control field - type derived from local data
jobs_tag_temp_Icon     LONG                           !Entry's icon ID
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:MSN                LIKE(job:MSN)                  !List box control field - type derived from field
job:Warranty_Charge_Type LIKE(job:Warranty_Charge_Type) !List box control field - type derived from field
job:Repair_Type_Warranty LIKE(job:Repair_Type_Warranty) !List box control field - type derived from field
job:Courier_Cost_Warranty LIKE(job:Courier_Cost_Warranty) !List box control field - type derived from field
job:Labour_Cost_Warranty LIKE(job:Labour_Cost_Warranty) !List box control field - type derived from field
job:Parts_Cost_Warranty LIKE(job:Parts_Cost_Warranty) !List box control field - type derived from field
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
jfc1                   LIKE(jfc1)                     !List box control field - type derived from local data
jfc2                   LIKE(jfc2)                     !List box control field - type derived from local data
jfc3                   LIKE(jfc3)                     !List box control field - type derived from local data
jfc4                   LIKE(jfc4)                     !List box control field - type derived from local data
jfc5                   LIKE(jfc5)                     !List box control field - type derived from local data
jfc6                   LIKE(jfc6)                     !List box control field - type derived from local data
jfc7                   LIKE(jfc7)                     !List box control field - type derived from local data
jfc8                   LIKE(jfc8)                     !List box control field - type derived from local data
jfc9                   LIKE(jfc9)                     !List box control field - type derived from local data
jfc10                  LIKE(jfc10)                    !List box control field - type derived from local data
jfc11                  LIKE(jfc11)                    !List box control field - type derived from local data
jfc12                  LIKE(jfc12)                    !List box control field - type derived from local data
job:Manufacturer       LIKE(job:Manufacturer)         !Browse key field - type derived from field
job:EDI                LIKE(job:EDI)                  !Browse key field - type derived from field
job:EDI_Batch_Number   LIKE(job:EDI_Batch_Number)     !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK30::job:EDI             LIKE(job:EDI)
HK30::job:EDI_Batch_Number LIKE(job:EDI_Batch_Number)
HK30::job:Manufacturer    LIKE(job:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
! ---------------------------------------- Higher Keys --------------------------------------- !
HK12::inv:Batch_Number    LIKE(inv:Batch_Number)
HK12::inv:Manufacturer    LIKE(inv:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB4::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB10::View:FileDropCombo VIEW(EDIBATCH)
                       PROJECT(ebt:Batch_Number)
                     END
FDCB19::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
FDCB20::View:FileDropCombo VIEW(EDIBATCH)
                       PROJECT(ebt:Batch_Number)
                     END
FDCB13::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,0)                 !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the EDI Claims'),AT(,,668,254),FONT('Arial',8,,FONT:regular),CENTER,IMM,ICON('pc.ico'),HLP('Reconcile_Claims'),SYSTEM,GRAY,DOUBLE
                       BUTTON('&Rev tags'),AT(211,135,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(223,163,70,13),USE(?DASSHOWTAG),HIDE
                       BUTTON('&Tag'),AT(8,232,56,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                       BUTTON('T&ag All'),AT(64,232,56,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                       BUTTON('&UnTag All'),AT(119,232,56,16),USE(?DASUNTAGALL),LEFT,ICON('UnTag.gif')
                       SHEET,AT(4,4,576,248),USE(?CurrentTab),SPREAD
                         TAB('Unreconciled Claims'),USE(?Tab1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?Prompt1),FONT(,,COLOR:White,,CHARSET:ANSI)
                           COMBO(@s30),AT(84,20,124,10),USE(Manufacturer_Temp),IMM,VSCROLL,FONT('Arial',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           BUTTON('EDI Claims'),AT(588,8,76,20),USE(?Redi_Claims),LEFT,ICON(ICON:NextPage)
                           PROMPT('Batch Number'),AT(8,36),USE(?Prompt2),FONT(,,COLOR:White,,CHARSET:ANSI)
                           COMBO(@s8b),AT(84,36,64,10),USE(Batch_Number_Temp),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('24L(2)@s8@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           BUTTON('Remove From Batch'),AT(588,44,76,20),USE(?Remove_Claims),LEFT,ICON('Delete.ico')
                           BUTTON('&Reconcile Claims'),AT(588,84,76,20),USE(?Process_Claims),LEFT,ICON('Invoice.gif')
                         END
                         TAB('Reconciled Claims'),USE(?Tab2)
                           GROUP,AT(416,16,156,48),USE(?Claim_Group)
                             PANEL,AT(428,20,144,44),USE(?claim_Panel),FILL(COLOR:Silver)
                             PROMPT('Claim Reference'),AT(436,24),USE(?Prompt5),TRN
                             STRING(@s15),AT(484,24,85,8),USE(inv:Claim_Reference),TRN,RIGHT,FONT(,,,FONT:bold)
                             PROMPT('Value Claimed (Ex V.A.T.)'),AT(436,36),USE(?Prompt5:2),TRN
                             STRING(@n14.2b),AT(516,36),USE(inv:Total_Claimed),TRN,RIGHT,FONT(,,,FONT:bold)
                             PROMPT('Claim Total (Ex V.A.T.)'),AT(436,48),USE(?Prompt5:3),TRN
                             STRING(@n14.2b),AT(516,48),USE(Total_Claimed_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                           END
                           PROMPT('Manufacturer'),AT(8,20),USE(?Prompt1:2),FONT(,,COLOR:White,,CHARSET:ANSI)
                           COMBO(@s30),AT(84,20,124,10),USE(Manufacturer2_temp),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           PROMPT('Invoices On Batch:'),AT(232,20),USE(?Prompt9),FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI)
                           LIST,AT(304,20,108,44),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('46L(2)|M~Invoice No~@s8@84L(2)|M~Date Created~@d6b@56L(2)|M~Total Claimed temp~@' &|
   'n14.2@'),FROM(Queue:Browse)
                           PROMPT('Batch Number'),AT(8,36),USE(?Prompt2:2),FONT(,,COLOR:White,,CHARSET:ANSI)
                           COMBO(@s8b),AT(84,36,64,10),USE(Batch_Number2_Temp),IMM,VSCROLL,FONT('Arial',8,,FONT:bold,CHARSET:ANSI),FORMAT('24L(2)|M@s8@'),DROP(10),FROM(Queue:FileDropCombo:3)
                           PANEL,AT(428,232,144,16),USE(?claim_Panel:2),FILL(COLOR:Silver)
                           PROMPT('Batch Total'),AT(436,236),USE(?Prompt5:4),TRN
                           STRING(@n14.2b),AT(516,236),USE(Batch_total_temp),TRN,RIGHT,FONT(,,,FONT:bold)
                         END
                         TAB('Rejected Claims'),USE(?Tab3)
                           PROMPT('Manufacturer'),AT(8,20),USE(?Prompt10),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           COMBO(@s30),AT(84,20,124,10),USE(manufacturer3_temp),IMM,VSCROLL,FORMAT('120L|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:4)
                           BUTTON('EDI Claims'),AT(588,180,76,20),USE(?edi_claims),LEFT,ICON(ICON:NextPage)
                         END
                       END
                       LIST,AT(8,68,568,160),USE(?Browse:1),IMM,HSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I~T~@s1@34R(2)|M~Job No~@s8@60L(2)|M~Account Number~@s15@64L(2)|M~Model Nu' &|
   'mber~@s30@64L(2)|M~I.M.E.I. Number~@s16@64L(2)|M~MSN~@s16@67L(2)|M~Charge Type~@' &|
   's30@66L(2)|M~Repair Type~@s30@43R(2)|M~Courier Cost~@n10.2@43R(2)|M~Labour Cost~' &|
   '@n10.2@42R(2)|M~Parts Cost~@n10.2@120L(2)|M~Order Number~R@s30@120L(2)|M~Fault C' &|
   'ode 1~R@s30@120L(2)|M~Fault Code 2~R@s30@120L(2)|M~Fault Code 3~R@s30@120L(2)|M~' &|
   'Fault Code 4~R@s30@120L(2)|M~Fault Code 5~R@s30@120L(2)|M~Fault Code 6~R@s30@120' &|
   'L(2)|M~Fault Code 7~R@s30@120L(2)|M~Fault Code 8~R@s30@120L(2)|M~Fault Code 9~R@' &|
   's30@120L(2)|M~Fault Code 10~R@s30@120L(2)|M~Fault Code 11~R@s30@120L(2)|M~Fault ' &|
   'Code 12~R@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&View Job'),AT(588,156,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('Close'),AT(588,232,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Processing Claims'),at(,,164,64),font('Arial',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Arial',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Arial',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW9::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1
BRW1::Sort2:Locator  StepLocatorClass                 !Conditional Locator - Choice(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

FDCB19               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

FDCB20               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:3         !Reference to browse queue type
                     END

FDCB13               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:4         !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_job_id   ushort,auto
pos           String(255)
WindowsDir      LPSTR(144)
SystemDir       WORD
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
DBH25:PopupText String(1000)
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?Manufacturer_Temp{prop:ReadOnly} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 15066597
    Elsif ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 8454143
    Else ! If ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 16777215
    End ! If ?Manufacturer_Temp{prop:Req} = True
    ?Manufacturer_Temp{prop:Trn} = 0
    ?Manufacturer_Temp{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?Batch_Number_Temp{prop:ReadOnly} = True
        ?Batch_Number_Temp{prop:FontColor} = 65793
        ?Batch_Number_Temp{prop:Color} = 15066597
    Elsif ?Batch_Number_Temp{prop:Req} = True
        ?Batch_Number_Temp{prop:FontColor} = 65793
        ?Batch_Number_Temp{prop:Color} = 8454143
    Else ! If ?Batch_Number_Temp{prop:Req} = True
        ?Batch_Number_Temp{prop:FontColor} = 65793
        ?Batch_Number_Temp{prop:Color} = 16777215
    End ! If ?Batch_Number_Temp{prop:Req} = True
    ?Batch_Number_Temp{prop:Trn} = 0
    ?Batch_Number_Temp{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?Claim_Group{prop:Font,3} = -1
    ?Claim_Group{prop:Color} = 15066597
    ?Claim_Group{prop:Trn} = 0
    ?claim_Panel{prop:Fill} = 15066597

    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?inv:Claim_Reference{prop:FontColor} = -1
    ?inv:Claim_Reference{prop:Color} = 15066597
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    ?inv:Total_Claimed{prop:FontColor} = -1
    ?inv:Total_Claimed{prop:Color} = 15066597
    ?Prompt5:3{prop:FontColor} = -1
    ?Prompt5:3{prop:Color} = 15066597
    ?Total_Claimed_temp{prop:FontColor} = -1
    ?Total_Claimed_temp{prop:Color} = 15066597
    ?Prompt1:2{prop:FontColor} = -1
    ?Prompt1:2{prop:Color} = 15066597
    If ?Manufacturer2_temp{prop:ReadOnly} = True
        ?Manufacturer2_temp{prop:FontColor} = 65793
        ?Manufacturer2_temp{prop:Color} = 15066597
    Elsif ?Manufacturer2_temp{prop:Req} = True
        ?Manufacturer2_temp{prop:FontColor} = 65793
        ?Manufacturer2_temp{prop:Color} = 8454143
    Else ! If ?Manufacturer2_temp{prop:Req} = True
        ?Manufacturer2_temp{prop:FontColor} = 65793
        ?Manufacturer2_temp{prop:Color} = 16777215
    End ! If ?Manufacturer2_temp{prop:Req} = True
    ?Manufacturer2_temp{prop:Trn} = 0
    ?Manufacturer2_temp{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Prompt2:2{prop:FontColor} = -1
    ?Prompt2:2{prop:Color} = 15066597
    If ?Batch_Number2_Temp{prop:ReadOnly} = True
        ?Batch_Number2_Temp{prop:FontColor} = 65793
        ?Batch_Number2_Temp{prop:Color} = 15066597
    Elsif ?Batch_Number2_Temp{prop:Req} = True
        ?Batch_Number2_Temp{prop:FontColor} = 65793
        ?Batch_Number2_Temp{prop:Color} = 8454143
    Else ! If ?Batch_Number2_Temp{prop:Req} = True
        ?Batch_Number2_Temp{prop:FontColor} = 65793
        ?Batch_Number2_Temp{prop:Color} = 16777215
    End ! If ?Batch_Number2_Temp{prop:Req} = True
    ?Batch_Number2_Temp{prop:Trn} = 0
    ?Batch_Number2_Temp{prop:FontStyle} = font:Bold
    ?claim_Panel:2{prop:Fill} = 15066597

    ?Prompt5:4{prop:FontColor} = -1
    ?Prompt5:4{prop:Color} = 15066597
    ?Batch_total_temp{prop:FontColor} = -1
    ?Batch_total_temp{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    If ?manufacturer3_temp{prop:ReadOnly} = True
        ?manufacturer3_temp{prop:FontColor} = 65793
        ?manufacturer3_temp{prop:Color} = 15066597
    Elsif ?manufacturer3_temp{prop:Req} = True
        ?manufacturer3_temp{prop:FontColor} = 65793
        ?manufacturer3_temp{prop:Color} = 8454143
    Else ! If ?manufacturer3_temp{prop:Req} = True
        ?manufacturer3_temp{prop:FontColor} = 65793
        ?manufacturer3_temp{prop:Color} = 16777215
    End ! If ?manufacturer3_temp{prop:Req} = True
    ?manufacturer3_temp{prop:Trn} = 0
    ?manufacturer3_temp{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:Queue.Pointer = job:Ref_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = job:Ref_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    jobs_tag_temp = '*'
  ELSE
    DELETE(GLO:Queue)
    jobs_tag_temp = ''
  END
    Queue:Browse:1.jobs_tag_temp = jobs_tag_temp
  IF (jobs_tag_temp = '*')
    Queue:Browse:1.jobs_tag_temp_Icon = 2
  ELSE
    Queue:Browse:1.jobs_tag_temp_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = job:Ref_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::5:QUEUE = GLO:Queue
    ADD(DASBRW::5:QUEUE)
  END
  FREE(GLO:Queue)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Pointer = job:Ref_Number
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = job:Ref_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Get_Invoice     Routine
    batch_total_temp = 0
    If manufacturer2_temp <> '' And batch_number2_temp > -1
        save_inv_ali_id = access:invoice_alias.savefile()
        access:invoice_alias.clearkey(inv_ali:batch_number_key)
        inv_ali:manufacturer = manufacturer2_temp
        inv_ali:batch_number = batch_number2_temp
        set(inv_ali:batch_number_key,inv_ali:batch_number_key)
        loop
            if access:invoice_alias.next()
               break
            end !if
            if inv_ali:manufacturer <> manufacturer2_temp
              BREAK
            END !End If
            IF inv_ali:batch_number <> batch_number2_temp      
              BREAK
            END  ! end if
            batch_total_temp += Round(INV_ali:Courier_Paid,.01) + Round(inv_ali:labour_paid,.01) + Round(inv_ali:parts_paid,.01)
        end !loop
        access:invoice_alias.restorefile(save_inv_ali_id)
    End!If manufactuer2_temp <> '' And batch_number2_temp > -1
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
ResortBrowse        Routine
      !5043 - Only show the fault codes that apply to the selected Manufacturer  (DBH: 02-12-2004)
      tmp:BrowseString = '11L(2)I~T~@s1@34R(2)|M~Job No~@s8@60L(2)|M~Account Number~@s15@64L(2)|M~Model Nu' &|
           'mber~@s30@64L(2)|M~I.M.E.I. Number~@s16@64L(2)|M~MSN~@s16@67L(2)|M~Charge Type~@' &|
           's30@66L(2)|M~Repair Type~@s30@43R(2)|M~Courier Cost~@n10.2@43R(2)|M~Labour Cost~' &|
           '@n10.2@42R(2)|M~Parts Cost~@n10.2@120L(2)|M~Order Number~R@s30@'
      
      Loop x# = 1 To 12
          Access:MANFAULT.ClearKey(maf:Field_Number_Key)
          Case Choice(?CurrentTab)
            Of 1
                maf:Manufacturer = Manufacturer_Temp
            Of 2
                maf:Manufacturer = Manufacturer2_Temp
            Of 3
                maf:Manufacturer = Manufacturer3_Temp
          End ! Case Choice(?CurrentTab)
          
          maf:Field_Number = x#
          If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
              !Found
              If x# = 11 Or x# = 12
                  tmp:BrowseString = Clip(tmp:BrowseString) & '120L(2)|M~Fault Code ' & x# & '~@s255@'
              Else ! If x# = 11 Or x# = 12
                  tmp:BrowseString = Clip(tmp:BrowseString) & '120L(2)|M~Fault Code ' & x# & '~@s30@'
              End ! If x# = 11 Or x# = 12
          Else !If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
              !Error
          End !If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
      End ! Loop x# = 1 To 12
      
      ?Browse:1{prop:Format} = tmp:BrowseString

!    Case Choice(?CurrentTab)
!    Of 1
!        ?Browse:1{prop:format} = '11L(2)I~T~@s1@#1#'&|
!                                '34R(2)|M~Job No~@s8@#3#'&|
!                                '60L(2)|M~Account Number~@s15@#4#'&|
!                                '64L(2)|M~Model Number~@s30@#5#'&|
!                                '64L(2)|M~I.M.E.I. Number~@s16@#6#'&|
!                                '64L(2)|M~MSN~@s16@#7#'&|
!                                '67L(2)|M~Charge Type~@s30@#8#'&|
!                                '66L(2)|M~Repair Type~@s30@#9#'&|
!                                '43R(2)|M~Courier Cost~@n10.2@#10#'&|
!                                '43R(2)|M~Labour Cost~@n10.2@#11#'&|
!                                '42R(2)|M~Parts Cost~@n10.2@#12#'
!    Of 2
!        If manufacturer2_temp = 'SAMSUNG'
!            ?Browse:1{prop:format} = '34R(2)|M~Job No~@s8@#3#'&|
!                                '60L(2)|M~Account Number~@s15@#4#'&|
!                                '64L(2)|M~Model Number~@s30@#5#'&|
!                                '64L(2)|M~I.M.E.I. Number~@s16@#6#'&|
!                                '64L(2)|M~MSN~@s16@#7#'&|
!                                '67L(2)|M~Claim Reference~@s30@#13#'&|
!                                '66L(2)|M~Repair Type~@s30@#9#'&|
!                                '43R(2)|M~Courier Cost~@n10.2@#10#'&|
!                                '43R(2)|M~Labour Cost~@n10.2@#11#'&|
!                                '42R(2)|M~Parts Cost~@n10.2@#12#'&|
!                                '67L(2)|M~Charge Type~@s30@#8#'
!                                
!
!        Else!If manufacturer_temp = 'SAMSUNG'
!            ?Browse:1{prop:format} = '34R(2)|M~Job No~@s8@#3#'&|
!                                '60L(2)|M~Account Number~@s15@#4#'&|
!                                '64L(2)|M~Model Number~@s30@#5#'&|
!                                '64L(2)|M~I.M.E.I. Number~@s16@#6#'&|
!                                '64L(2)|M~MSN~@s16@#7#'&|
!                                '67L(2)|M~Charge Type~@s30@#8#'&|
!                                '66L(2)|M~Repair Type~@s30@#9#'&|
!                                '43R(2)|M~Courier Cost~@n10.2@#10#'&|
!                                '43R(2)|M~Labour Cost~@n10.2@#11#'&|
!                                '42R(2)|M~Parts Cost~@n10.2@#12#'
!        End!If manufacturer_temp = 'SAMSUNG'
!    End!Case Choice(?CurrentTab)
!    thiswindow.reset(1)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Reconcile_Claims',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Reconcile_Claims',1)
    SolaceViewVars('save_inv_ali_id',save_inv_ali_id,'Reconcile_Claims',1)
    SolaceViewVars('invoice_number_temp',invoice_number_temp,'Reconcile_Claims',1)
    SolaceViewVars('Manufacturer2_temp',Manufacturer2_temp,'Reconcile_Claims',1)
    SolaceViewVars('total_jobs_temp',total_jobs_temp,'Reconcile_Claims',1)
    SolaceViewVars('manufacturer3_temp',manufacturer3_temp,'Reconcile_Claims',1)
    SolaceViewVars('reconcile_group:claim_reference_temp',reconcile_group:claim_reference_temp,'Reconcile_Claims',1)
    SolaceViewVars('reconcile_group:Total_Claimed_temp',reconcile_group:Total_Claimed_temp,'Reconcile_Claims',1)
    SolaceViewVars('reconcile_group:Courier_Paid_Temp',reconcile_group:Courier_Paid_Temp,'Reconcile_Claims',1)
    SolaceViewVars('reconcile_group:Labour_Paid_temp',reconcile_group:Labour_Paid_temp,'Reconcile_Claims',1)
    SolaceViewVars('reconcile_group:Parts_Paid_temp',reconcile_group:Parts_Paid_temp,'Reconcile_Claims',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Reconcile_Claims',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Reconcile_Claims',1)
    SolaceViewVars('Batch_Number_Temp',Batch_Number_Temp,'Reconcile_Claims',1)
    SolaceViewVars('jobs_tag',jobs_tag,'Reconcile_Claims',1)
    SolaceViewVars('jobs_tag_temp',jobs_tag_temp,'Reconcile_Claims',1)
    SolaceViewVars('reconciled_temp',reconciled_temp,'Reconcile_Claims',1)
    SolaceViewVars('Batch_Number2_Temp',Batch_Number2_Temp,'Reconcile_Claims',1)
    SolaceViewVars('Charge_Repair_Temp',Charge_Repair_Temp,'Reconcile_Claims',1)
    SolaceViewVars('claim_total_temp',claim_total_temp,'Reconcile_Claims',1)
    SolaceViewVars('overpaid_by_temp',overpaid_by_temp,'Reconcile_Claims',1)
    SolaceViewVars('yes_temp',yes_temp,'Reconcile_Claims',1)
    SolaceViewVars('fin_temp',fin_temp,'Reconcile_Claims',1)
    SolaceViewVars('Batch_total_temp',Batch_total_temp,'Reconcile_Claims',1)
    SolaceViewVars('MarkOption',MarkOption,'Reconcile_Claims',1)
    SolaceViewVars('SelectAll',SelectAll,'Reconcile_Claims',1)
    SolaceViewVars('BrFilter1',BrFilter1,'Reconcile_Claims',1)
    SolaceViewVars('BrLocator1',BrLocator1,'Reconcile_Claims',1)
    SolaceViewVars('BrFilter2',BrFilter2,'Reconcile_Claims',1)
    SolaceViewVars('BrLocator2',BrLocator2,'Reconcile_Claims',1)
    SolaceViewVars('Query',Query,'Reconcile_Claims',1)
    SolaceViewVars('jfc1',jfc1,'Reconcile_Claims',1)
    SolaceViewVars('jfc2',jfc2,'Reconcile_Claims',1)
    SolaceViewVars('jfc3',jfc3,'Reconcile_Claims',1)
    SolaceViewVars('jfc4',jfc4,'Reconcile_Claims',1)
    SolaceViewVars('jfc5',jfc5,'Reconcile_Claims',1)
    SolaceViewVars('jfc6',jfc6,'Reconcile_Claims',1)
    SolaceViewVars('jfc7',jfc7,'Reconcile_Claims',1)
    SolaceViewVars('jfc8',jfc8,'Reconcile_Claims',1)
    SolaceViewVars('jfc9',jfc9,'Reconcile_Claims',1)
    SolaceViewVars('jfc10',jfc10,'Reconcile_Claims',1)
    SolaceViewVars('jfc11',jfc11,'Reconcile_Claims',1)
    SolaceViewVars('jfc12',jfc12,'Reconcile_Claims',1)
    SolaceViewVars('tmp:BrowseString',tmp:BrowseString,'Reconcile_Claims',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer_Temp;  SolaceCtrlName = '?Manufacturer_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Redi_Claims;  SolaceCtrlName = '?Redi_Claims';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Batch_Number_Temp;  SolaceCtrlName = '?Batch_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Remove_Claims;  SolaceCtrlName = '?Remove_Claims';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Process_Claims;  SolaceCtrlName = '?Process_Claims';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Claim_Group;  SolaceCtrlName = '?Claim_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?claim_Panel;  SolaceCtrlName = '?claim_Panel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Claim_Reference;  SolaceCtrlName = '?inv:Claim_Reference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Total_Claimed;  SolaceCtrlName = '?inv:Total_Claimed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:3;  SolaceCtrlName = '?Prompt5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Total_Claimed_temp;  SolaceCtrlName = '?Total_Claimed_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:2;  SolaceCtrlName = '?Prompt1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer2_temp;  SolaceCtrlName = '?Manufacturer2_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2:2;  SolaceCtrlName = '?Prompt2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Batch_Number2_Temp;  SolaceCtrlName = '?Batch_Number2_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?claim_Panel:2;  SolaceCtrlName = '?claim_Panel:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:4;  SolaceCtrlName = '?Prompt5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Batch_total_temp;  SolaceCtrlName = '?Batch_total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?manufacturer3_temp;  SolaceCtrlName = '?manufacturer3_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?edi_claims;  SolaceCtrlName = '?edi_claims';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Reconcile_Claims','?Browse:1')   !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Reconcile_Claims')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Reconcile_Claims')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?DASREVTAG
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Reconcile_Claims'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EDIBATCH.Open
  Relate:INVOICE_ALIAS.Open
  Relate:STAHEAD.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:INVOICE.UseFile
  Access:MANUFACT.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:MANFAULT.UseFile
  SELF.FilesOpened = True
  ! --------------------------------------- Bound Fields --------------------------------------- !
  BIND('Manufacturer_Temp',Manufacturer_Temp)
  BIND('Manufacturer2_temp',Manufacturer2_temp)
  BIND('Batch_Number2_Temp',Batch_Number2_Temp)
  BIND('Batch_Number_Temp',Batch_Number_Temp)
  BIND('manufacturer3_temp',manufacturer3_temp)
  ! --------------------------------------- Bound Fields --------------------------------------- !
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:INVOICE,SELF)
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbk01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ! support for CPCS
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,inv:Batch_Number_Key)
  BRW9.AddRange(inv:Batch_Number)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(,inv:Batch_Number,1,BRW9)
  BIND('Total_Claimed_temp',Total_Claimed_temp)
  BIND('Batch_Number2_Temp',Batch_Number2_Temp)
  BRW9.AddField(inv:Invoice_Number,BRW9.Q.inv:Invoice_Number)
  BRW9.AddField(inv:Date_Created,BRW9.Q.inv:Date_Created)
  BRW9.AddField(Total_Claimed_temp,BRW9.Q.Total_Claimed_temp)
  BRW9.AddField(Batch_Number2_Temp,BRW9.Q.Batch_Number2_Temp)
  BRW9.AddField(inv:Total_Claimed,BRW9.Q.inv:Total_Claimed)
  BRW9.AddField(inv:Claim_Reference,BRW9.Q.inv:Claim_Reference)
  BRW9.AddField(inv:Manufacturer,BRW9.Q.inv:Manufacturer)
  BRW9.AddField(inv:Batch_Number,BRW9.Q.inv:Batch_Number)
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,job:EDI_Key)
  BRW1.AddRange(job:EDI_Batch_Number)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:EDI_Key)
  BRW1.AddRange(job:EDI_Batch_Number)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,job:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,job:EDI_Key)
  BRW1.AddRange(job:EDI_Batch_Number)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,job:Ref_Number,1,BRW1)
  BIND('jobs_tag_temp',jobs_tag_temp)
  BIND('jfc1',jfc1)
  BIND('jfc2',jfc2)
  BIND('jfc3',jfc3)
  BIND('jfc4',jfc4)
  BIND('jfc5',jfc5)
  BIND('jfc6',jfc6)
  BIND('jfc7',jfc7)
  BIND('jfc8',jfc8)
  BIND('jfc9',jfc9)
  BIND('jfc10',jfc10)
  BIND('jfc11',jfc11)
  BIND('jfc12',jfc12)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(jobs_tag_temp,BRW1.Q.jobs_tag_temp)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(job:MSN,BRW1.Q.job:MSN)
  BRW1.AddField(job:Warranty_Charge_Type,BRW1.Q.job:Warranty_Charge_Type)
  BRW1.AddField(job:Repair_Type_Warranty,BRW1.Q.job:Repair_Type_Warranty)
  BRW1.AddField(job:Courier_Cost_Warranty,BRW1.Q.job:Courier_Cost_Warranty)
  BRW1.AddField(job:Labour_Cost_Warranty,BRW1.Q.job:Labour_Cost_Warranty)
  BRW1.AddField(job:Parts_Cost_Warranty,BRW1.Q.job:Parts_Cost_Warranty)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(jfc1,BRW1.Q.jfc1)
  BRW1.AddField(jfc2,BRW1.Q.jfc2)
  BRW1.AddField(jfc3,BRW1.Q.jfc3)
  BRW1.AddField(jfc4,BRW1.Q.jfc4)
  BRW1.AddField(jfc5,BRW1.Q.jfc5)
  BRW1.AddField(jfc6,BRW1.Q.jfc6)
  BRW1.AddField(jfc7,BRW1.Q.jfc7)
  BRW1.AddField(jfc8,BRW1.Q.jfc8)
  BRW1.AddField(jfc9,BRW1.Q.jfc9)
  BRW1.AddField(jfc10,BRW1.Q.jfc10)
  BRW1.AddField(jfc11,BRW1.Q.jfc11)
  BRW1.AddField(jfc12,BRW1.Q.jfc12)
  BRW1.AddField(job:Manufacturer,BRW1.Q.job:Manufacturer)
  BRW1.AddField(job:EDI,BRW1.Q.job:EDI)
  BRW1.AddField(job:EDI_Batch_Number,BRW1.Q.job:EDI_Batch_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB4.Init(Manufacturer_Temp,?Manufacturer_Temp,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder(man:Manufacturer_Key)
  FDCB4.AddField(man:Manufacturer,FDCB4.Q.man:Manufacturer)
  FDCB4.AddField(man:RecordNumber,FDCB4.Q.man:RecordNumber)
  FDCB4.AddUpdateField(man:Manufacturer,Manufacturer_Temp)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB10.Init(Batch_Number_Temp,?Batch_Number_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo:1,Relate:EDIBATCH,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo:1
  FDCB10.AddSortOrder(ebt:Batch_Number_Key)
  FDCB10.AddRange(ebt:Manufacturer,Manufacturer_Temp)
  FDCB10.AddField(ebt:Batch_Number,FDCB10.Q.ebt:Batch_Number)
  FDCB10.AddUpdateField(ebt:Batch_Number,Batch_Number_Temp)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB19.Init(Manufacturer2_temp,?Manufacturer2_temp,Queue:FileDropCombo:2.ViewPosition,FDCB19::View:FileDropCombo,Queue:FileDropCombo:2,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB19.Q &= Queue:FileDropCombo:2
  FDCB19.AddSortOrder(man:Manufacturer_Key)
  FDCB19.AddField(man:Manufacturer,FDCB19.Q.man:Manufacturer)
  FDCB19.AddField(man:RecordNumber,FDCB19.Q.man:RecordNumber)
  FDCB19.AddUpdateField(man:Manufacturer,Manufacturer2_temp)
  ThisWindow.AddItem(FDCB19.WindowComponent)
  FDCB20.Init(Batch_Number2_Temp,?Batch_Number2_Temp,Queue:FileDropCombo:3.ViewPosition,FDCB20::View:FileDropCombo,Queue:FileDropCombo:3,Relate:EDIBATCH,ThisWindow,GlobalErrors,0,1,0)
  FDCB20.Q &= Queue:FileDropCombo:3
  FDCB20.AddSortOrder(ebt:Batch_Number_Key)
  FDCB20.AddRange(ebt:Manufacturer,Manufacturer2_temp)
  FDCB20.AddField(ebt:Batch_Number,FDCB20.Q.ebt:Batch_Number)
  FDCB20.AddUpdateField(ebt:Batch_Number,Batch_Number2_Temp)
  ThisWindow.AddItem(FDCB20.WindowComponent)
  FDCB13.Init(manufacturer3_temp,?manufacturer3_temp,Queue:FileDropCombo:4.ViewPosition,FDCB13::View:FileDropCombo,Queue:FileDropCombo:4,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB13.Q &= Queue:FileDropCombo:4
  FDCB13.AddSortOrder(man:Manufacturer_Key)
  FDCB13.AddField(man:Manufacturer,FDCB13.Q.man:Manufacturer)
  FDCB13.AddField(man:RecordNumber,FDCB13.Q.man:RecordNumber)
  FDCB13.AddUpdateField(man:Manufacturer,manufacturer3_temp)
  ThisWindow.AddItem(FDCB13.WindowComponent)
  BRW1.popup.additem('Tag','Popup2','',1)
  BRW1.popup.additemevent('Popup2',event:accepted,?DASTAG)
  BRW1.popup.seticon('Popup2','tag.gif')
  BRW1.popup.additem('Tag All','Popup3','Popup2',1)
  BRW1.popup.additemevent('Popup3',event:accepted,?DASTAGAll)
  BRW1.popup.seticon('Popup3','tagall.gif')
  BRW1.popup.additem('Untag All','Popup4','Popup3',1)
  BRW1.popup.additemevent('Popup4',event:accepted,?DASUNTAGALL)
  BRW1.popup.seticon('Popup4','untag.gif')
  BRW1.popup.additem('-','Popup5','Popup4',1)
  BRW1.popup.additem('EDI Claims','Popup6','Popup5',1)
  BRW1.popup.seticon('Popup6','point.ico')
  BRW1.popup.additem('Re-EDI Claims','SPopup61','Popup6',2)
  BRW1.popup.additemevent('SPopup61',event:accepted,?Redi_Claims)
  BRW1.popup.seticon('SPopup61','DITTO.ICO')
  BRW1.popup.additem('Remove From Batch','SPopup62','SPopup1',2)
  BRW1.popup.additemevent('SPopup62',event:accepted,?Remove_Claims)
  BRW1.popup.seticon('SPopup62','delete.gif')
  BRW1.popup.additem('-','SPopup63','SPopup2',2)
  BRW1.popup.additem('Reconcile Claims','SPopup64','SPopup3',2)
  BRW1.popup.additemevent('SPopup64',event:accepted,?Process_Claims)
  BRW1.popup.seticon('SPopup64','CALC2.ICO')
  BRW1.popup.additem('-','EPopup64','SPopup4',1)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'Unreconciled Claims'
    ?Tab2{PROP:TEXT} = 'Reconciled Claims'
    ?Tab3{PROP:TEXT} = 'Rejected Claims'
    ?Browse:1{PROP:FORMAT} ='11L(2)I~T~@s1@#1#34R(2)|M~Job No~@s8@#3#60L(2)|M~Account Number~@s15@#4#64L(2)|M~Model Number~@s30@#5#64L(2)|M~I.M.E.I. Number~@s16@#6#64L(2)|M~MSN~@s16@#7#67L(2)|M~Charge Type~@s30@#8#66L(2)|M~Repair Type~@s30@#9#43R(2)|M~Courier Cost~@n10.2@#10#43R(2)|M~Labour Cost~@n10.2@#11#42R(2)|M~Parts Cost~@n10.2@#12#120L(2)|M~Order Number~R@s30@#13#120L(2)|M~Fault Code 1~R@s30@#14#120L(2)|M~Fault Code 2~R@s30@#15#120L(2)|M~Fault Code 3~R@s30@#16#120L(2)|M~Fault Code 4~R@s30@#17#120L(2)|M~Fault Code 5~R@s30@#18#120L(2)|M~Fault Code 6~R@s30@#19#120L(2)|M~Fault Code 7~R@s30@#20#120L(2)|M~Fault Code 8~R@s30@#21#120L(2)|M~Fault Code 9~R@s30@#22#120L(2)|M~Fault Code 10~R@s30@#23#120L(2)|M~Fault Code 11~R@s30@#24#120L(2)|M~Fault Code 12~R@s30@#25#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EDIBATCH.Close
    Relate:INVOICE_ALIAS.Close
    Relate:STAHEAD.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Reconcile_Claims','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisThreadActive
    ThreadQ.Proc='Reconcile_Claims'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Reconcile_Claims',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  BRW1.Popup.SetItemEnable('Popup2',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('Popup3',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('Popup4',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('Popup5',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('Popup6',CHOOSE(NOT(~Records(Queue:Browse:1))))
  BRW1.Popup.SetItemEnable('SPopup61',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup62',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup63',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  BRW1.Popup.SetItemEnable('SPopup64',CHOOSE(NOT(~Records(Queue:Browse:1))))
  
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  If Keycode() = Mouseleft2
      do_update# = false
  Else
      case request
          of changerecord
              If SecurityCheck('JOBS - CHANGE')
                  case messageex('You do not have access to this option.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      of 1 ! &ok button
                  end!case messageex
                  do_update# = false
              end
      end !case request
  End!If Keycode() <> Mouseleft2
  If do_update# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJOBS
    ReturnValue = GlobalResponse
  END
  End!do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Xplore1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  Xplore1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  Xplore1.AddField(job:ESN,BRW1.Q.job:ESN)
  Xplore1.AddField(job:MSN,BRW1.Q.job:MSN)
  Xplore1.AddField(job:Warranty_Charge_Type,BRW1.Q.job:Warranty_Charge_Type)
  Xplore1.AddField(job:Repair_Type_Warranty,BRW1.Q.job:Repair_Type_Warranty)
  Xplore1.AddField(job:Courier_Cost_Warranty,BRW1.Q.job:Courier_Cost_Warranty)
  Xplore1.AddField(job:Labour_Cost_Warranty,BRW1.Q.job:Labour_Cost_Warranty)
  Xplore1.AddField(job:Parts_Cost_Warranty,BRW1.Q.job:Parts_Cost_Warranty)
  Xplore1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  Xplore1.AddField(job:Manufacturer,BRW1.Q.job:Manufacturer)
  Xplore1.AddField(job:EDI,BRW1.Q.job:EDI)
  Xplore1.AddField(job:EDI_Batch_Number,BRW1.Q.job:EDI_Batch_Number)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,job:EDI_Key) !Xplore Sort Order for File Sequence
  BRW1.AddRange(job:EDI_Batch_Number)                 !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
      ! Start Change 4141 BE(22/04/04)
      !batch_number_temp = man:batch_number - 1
      CLEAR(glo:queue)
      FREE(glo:queue)
      ! End Change 4141 BE(22/04/04)
      
      ! Start Change 2806 BE(18/07/03)
      !batch_number2_temp = man:batch_number
      !Display(?batch_number_temp)
      ! End Change 2806 BE(18/07/03)
      
      FDCB10.ResetQueue(1)
      ! Start Change 2806 BE(18/07/03)
      Display(?batch_number_temp)
      ! End Change 2806 BE(18/07/03)
      Do ResortBrowse
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    OF ?Redi_Claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Redi_Claims, Accepted)
      If Choice(?CurrentTab) = 1
          Case MessageEx('Do you wish to RE-EDI Batch Number '&clip(batch_number_temp)&'?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
              Of 1 ! &Yes Button
      
                  Access:MANUFACT.Clearkey(man:Manufacturer_Key)
                  man:Manufacturer    = Manufacturer_Temp
                  If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      ! Found
      
                  Else ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
                      ! Error
                  End ! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
      
                  Case man:EDIFileType
                  Of 'ALCATEL'
                      Alcatel_EDI(batch_number_temp)
                  Of 'BOSCH'
                      Bosch_EDI(batch_number_temp)
                  Of 'ERICSSON'
                      Ericsson_EDI(batch_number_temp)
                  Of 'MAXON'
                      Maxon_EDI(batch_number_temp)
                  Of 'MOTOROLA'
                      Motorola_EDI(batch_number_temp)
                  Of 'NEC'
                      NEC_EDI(batch_number_temp)
                  Of 'NOKIA'
                      Nokia_EDI(batch_number_temp)
                  Of 'SHARP'
                      Sharp_Excel_Edi(batch_number_temp)
                  Of 'SIEMENS'
                      Siemens_Edi(batch_number_temp)
                  Of 'SIEMENS DECT'
                      Siemens_Dect_Edi(batch_number_temp)
                  Of 'TELITAL'
                      Telital_EDI(batch_number_temp)
                  Of 'SAMSUNG'
                      Samsung_Edi(Batch_number_temp)
                  Of 'MITSUBISHI'
                      Mitsibushi_Edi(Batch_number_temp)
                  Of 'SAGEM'
                      Sagem_EDI(Batch_Number_Temp)
                  Of 'PANASONIC'
                      Panasonic_EDI(batch_Number_temp)
                  Of 'SONY'
                      Sony_EDI(batch_number_temp)
                  ! Start Change 3524 BE(14/01/04)
                  OF 'SENDO'
                      Sendo_EDI(batch_number_temp)
                  ! End Change 3524 BE(14/01/04)
                  ! Start Change 4018 BE(06/04/04)
                  OF 'VK'
                      VK_EDI(batch_number_temp)
                  ! End Change 4018 BE(06/04/04)
                  ! Start Change 3516 BE(27/09/04)
                  OF 'LG'
                      LG_EDI(batch_number_temp)
                  ! End Change 3516 BE(27/09/04)
                  Of 'STANDARD FORMAT'
                      ! Call ARC's Standard CSV - TrkBs: 5855 (DBH: 28-09-2005)
                      StandardFormat_EDI(Batch_Number_Temp,Manufacturer_Temp)
                  End
              Of 2 ! &No Button
          End!Case MessageEx
      
      End!If Choice(?CurrentTab) = 1
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Redi_Claims, Accepted)
    OF ?Batch_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Batch_Number_Temp, Accepted)
      ! Start Change 4141 BE(22/04/04)
      CLEAR(glo:queue)
      FREE(glo:queue)
      ! End Change 4141 BE(22/04/04)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Batch_Number_Temp, Accepted)
    OF ?Remove_Claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Claims, Accepted)
      If Choice(?CurrentTab) = 1
          Case MessageEx('All the tagged jobs will be removed from Batch Number '&Clip(batch_number_temp)&'.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\warn.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
              
                  setcursor(cursor:wait)
                  Loop x# = 1 To Records(glo:queue)
                      Get(glo:queue,x#)
                      access:jobs.clearkey(job:ref_number_key)
                      job:ref_number = glo:pointer
                      if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                          job:edi = 'NO'
                          job:edi_batch_number = ''
                          If access:jobs.update() = Level:Benign
                              If Access:AUDIT.PrimeRecord() = Level:Benign
                                  aud:Ref_Number    = job:ref_number
                                  aud:Date          = Today()
                                  aud:Time          = Clock()
                                  aud:Type          = 'JOB'
                                  Access:USERS.ClearKey(use:Password_Key)
                                  use:Password      = glo:Password
                                  Access:USERS.Fetch(use:Password_Key)
                                  aud:User          = use:User_Code
                                  aud:Action        = 'JOB REMOVED FROM EDI BATCH: ' & Batch_Number_Temp
                                  Access:AUDIT.Insert()
                              End!If Access:AUDIT.PrimeRecord() = Level:Benign
                          End !If access:jobs.update() = Level:Benign
                      End!if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                  End!Loop x$ = 1 To RecordsToProcess
                  setcursor()
                  Clear(glo:Queue)
                  Free(glo:Queue)
      
              Of 2 ! &No Button
          End!Case MessageEx
      End!If Choice(?CurrentTab) = 1
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Claims, Accepted)
    OF ?Process_Claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Claims, Accepted)
      If Choice(?CurrentTab) = 1
          If records(glo:queue)
              systemdir   = getsystemdirectory(windowsdir,size(windowsdir))
      
              Clear(reconcile_group)
              Reconcile_Claim_value(claim_reference_temp,Total_Claimed_temp,Courier_Paid_Temp,Labour_Paid_temp,Parts_Paid_temp,total_jobs_temp)
      
              If claim_reference_temp <> ''
                  skip_manufacturer# = 0
                  skip_vat# = 0
                  set(defaults)
                  access:defaults.next()
                  error# = 0
                  access:manufact.clearkey(man:manufacturer_key)
                  man:manufacturer = manufacturer_temp
                  if access:manufact.tryfetch(man:manufacturer_key) = level:benign
                      access:subtracc.clearkey(sub:account_number_key)
                      sub:account_number = man:trade_account
                      if access:subtracc.tryfetch(sub:account_number_key)
                          If skip_manufacturer# = 0
                              Case MessageEx('Error! <13,10><13,10>A Trade Account has not been allocated to this Manufacturer.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'&Skip Further Errors',skip_manufacturer#,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                          End!skip_manufacturer# = 0
                          error# = 1
                      Else!if access:subtracc.tryfetch(sub:account_number_key)
                          access:tradeacc.clearkey(tra:account_number_key) 
                          tra:account_number = sub:main_account_number
                          if access:tradeacc.tryfetch(tra:account_number_key) = level:benign
                              if tra:invoice_sub_accounts = 'YES'
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = sub:labour_vat_code
                                  if access:vatcode.tryfetch(vat:vat_code_key)
                                      If skip_vat# = 0
                                          Case MessageEx('Error! <13,10><13,10>V.A.T. Rates have not been setup for this Trade Account.','ServiceBase 2000',|
                                                         'Styles\stop.ico','|&OK',1,1,'&Skip Further Errors',skip_vat#,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                              Of 1 ! &OK Button
                                          End!Case MessageEx
                                      End!If skip_vat# = 0
                                      error# = 1
                                  else
                                      labour$ = vat:vat_rate
                                  end
                                  If error# = 0
                                      access:vatcode.clearkey(vat:vat_code_key)
                                      vat:vat_code = sub:parts_vat_code
                                      if access:vatcode.tryfetch(vat:vat_code_key)
                                          If skip_vat# = 0
                                              Case MessageEx('Error! <13,10><13,10>V.A.T. Rates have not been setup for this Trade Account.','ServiceBase 2000',|
                                                             'Styles\stop.ico','|&OK',1,1,'&Skip Further Errors',skip_vat#,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &OK Button
                                              End!Case MessageEx
                                          End!If skip_vat# = 0
                                          error# = 1
                                      else
                                          parts$ = vat:vat_rate
                                      end
                                  End!If error# = 0
                              else!if tra:use_sub_accounts = 'YES'
                                  access:vatcode.clearkey(vat:vat_code_key)
                                  vat:vat_code = tra:labour_vat_code
                                  if access:vatcode.tryfetch(vat:vat_code_key)
                                      If skip_vat# = 0
                                          Case MessageEx('Error! <13,10><13,10>V.A.T. Rates have not been setup for this Trade Account.','ServiceBase 2000',|
                                                         'Styles\stop.ico','|&OK',1,1,'&Skip Further Errors',skip_vat#,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                              Of 1 ! &OK Button
                                          End!Case MessageEx
                                      End!If skip_vat# = 0
                                      error# = 1
                                  else
                                      labour$ = vat:vat_rate
                                  end
                                  If error# = 0
                                      access:vatcode.clearkey(vat:vat_code_key)
                                      vat:vat_code = tra:parts_vat_code
                                      if access:vatcode.tryfetch(vat:vat_code_key)
                                          If skip_vat# = 0
                                              Case MessageEx('Error! <13,10><13,10>V.A.T. Rates have not been setup for this Trade Account.','ServiceBase 2000',|
                                                             'Styles\stop.ico','|&OK',1,1,'&Skip Further Errors',skip_vat#,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                  Of 1 ! &OK Button
                                              End!Case MessageEx
                                          End!If skip_vat# = 0
                                          error# = 1
                                      else
                                          parts$ = vat:vat_rate
                                      end
                                  End!If error# = 0
      
                              end!if tra:use_sub_accounts = 'YES'
                          end!if access:tradeacc.tryfetch(tra:account_number_key) = level:benign
      
                      End!if access:subtracc.tryfetch(sub:account_number_key)
                  End
                  IF error# = 0
          !SAGE
                      invoice_number_temp = 0
                      sage_error# = 0
                      If def:use_sage = 'YES'
                          file_error# = 0
                          glo:file_name = Clip(WindowsDir) & '\SAGEDATA.SPD'
                          Remove(paramss)
                          
                          access:paramss.open()
                          access:paramss.usefile()
          !LABOUR
                          get(paramss,0)
                          if access:paramss.primerecord() = Level:Benign
                              prm:account_ref       = Stripcomma(sub:Account_number)
                              If tra:invoice_sub_accounts = 'YES'
                                  prm:name              = Stripcomma(sub:company_name)
                                  prm:address_1         = Stripcomma(sub:address_line1)
                                  prm:address_2         = Stripcomma(sub:address_line2)
                                  prm:address_3         = Stripcomma(sub:address_line3)
                                  prm:address_4         = ''
                                  prm:address_5         = Stripcomma(sub:postcode)
                                  prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                                  prm:contact_name      = Stripcomma(sub:contact_name)
                              Else!If tra:invoice_sub_accounts = 'YES'
                                  prm:name              = Stripcomma(tra:company_name)
                                  prm:address_1         = Stripcomma(tra:address_line1)
                                  prm:address_2         = Stripcomma(tra:address_line2)
                                  prm:address_3         = Stripcomma(tra:address_line3)
                                  prm:address_4         = ''
                                  prm:address_5         = Stripcomma(tra:postcode)
                                  prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                                  prm:contact_name      = Stripcomma(tra:contact_name)
                              End!If tra:invoice_sub_accounts = 'YES'
                              prm:del_address_1     = ''
                              prm:del_address_2     = ''
                              prm:del_address_3     = ''
                              prm:del_address_4     = ''
                              prm:del_address_5     = ''
                              prm:notes_1           = ''
                              prm:notes_2           = ''
                              prm:notes_3           = ''
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.tryfetch(use:password_key)
                              prm:taken_by = use:user_code
                              prm:order_number      = Stripcomma(claim_reference_temp)
                              prm:cust_order_number = 'B' & Stripcomma(batch_number_temp)
                              prm:payment_ref       = ''
                              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                              prm:global_details    = ''
                              prm:items_net         = Stripcomma(labour_paid_temp + parts_paid_temp + courier_paid_temp)
                              prm:items_tax         = Stripcomma((Round(labour_paid_temp * (labour$/100),.01)) + (Round(parts_paid_temp * (parts$/100),.01)) + |
                                                              (Round(courier_paid_temp * (labour$/100),.01)))
                              prm:stock_code        = StripComma(DEF:Labour_Stock_Code)
                              prm:description       = StripComma(DEF:Labour_Description)
                              prm:nominal_code      = StripComma(DEF:Labour_Code)
                              prm:qty_order         = '1'
                              prm:unit_price        = '0'
                              prm:net_amount        = StripComma(labour_paid_temp)
                              prm:tax_amount        = StripComma(Round(labour_paid_temp * (labour$/100),.01))
                              prm:comment_1         = ''
                              prm:comment_2         = ''
                              prm:unit_of_sale      = '0'
                              prm:full_net_amount   = '0'
                              prm:invoice_date      = '*'
                              prm:data_filepath     = Clip(def:path_sage) & '\'
                              prm:user_name         = Clip(DEF:User_Name_Sage)
                              prm:password          = Clip(DEF:Password_Sage)
                              prm:set_invoice_number= '*'
                              prm:invoice_no        = '*'
                              access:paramss.insert()
                          end!if access:paramss.primerecord() = Level:Benign
          !PARTS
                          get(paramss,0)
                          if access:paramss.primerecord() = Level:Benign
                              prm:account_ref       = Stripcomma(sub:Account_number)
                              If tra:invoice_sub_accounts = 'YES'
                                  prm:name              = Stripcomma(sub:company_name)
                                  prm:address_1         = Stripcomma(sub:address_line1)
                                  prm:address_2         = Stripcomma(sub:address_line2)
                                  prm:address_3         = Stripcomma(sub:address_line3)
                                  prm:address_4         = ''
                                  prm:address_5         = Stripcomma(sub:postcode)
                                  prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                                  prm:contact_name      = Stripcomma(sub:contact_name)
                              Else!If tra:invoice_sub_accounts = 'YES'
                                  prm:name              = Stripcomma(tra:company_name)
                                  prm:address_1         = Stripcomma(tra:address_line1)
                                  prm:address_2         = Stripcomma(tra:address_line2)
                                  prm:address_3         = Stripcomma(tra:address_line3)
                                  prm:address_4         = ''
                                  prm:address_5         = Stripcomma(tra:postcode)
                                  prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                                  prm:contact_name      = Stripcomma(tra:contact_name)
                              End!If tra:invoice_sub_accounts = 'YES'
                              prm:del_address_1     = ''
                              prm:del_address_2     = ''
                              prm:del_address_3     = ''
                              prm:del_address_4     = ''
                              prm:del_address_5     = ''
                              prm:notes_1           = ''
                              prm:notes_2           = ''
                              prm:notes_3           = ''
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.tryfetch(use:password_key)
                              prm:taken_by = use:user_code
                              prm:order_number      = Stripcomma(claim_reference_temp)
                              prm:cust_order_number = 'B' & Stripcomma(batch_number_temp)
                              prm:payment_ref       = ''
                              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                              prm:global_details    = ''
                              prm:items_net         = Stripcomma(labour_paid_temp + parts_paid_temp + courier_paid_temp)
                              prm:items_tax         = Stripcomma((Round(labour_paid_temp * (labour$/100),.01)) + (Round(parts_paid_temp * (parts$/100),.01)) + |
                                                              (Round(courier_paid_temp * (labour$/100),.01)))
                              prm:stock_code        = Stripcomma(DEF:Parts_Stock_Code)
                              prm:description       = Stripcomma(DEF:Parts_Description)
                              prm:nominal_code      = Stripcomma(DEF:Parts_Code)
                              prm:qty_order         = '1'
                              prm:unit_price        = '0'
                              prm:net_amount        = Stripcomma(parts_paid_temp)
                              prm:tax_amount        = Stripcomma(Round(parts_paid_temp * (parts$/100),.01))
                              prm:comment_1         = ''
                              prm:comment_2         = ''
                              prm:unit_of_sale      = '0'
                              prm:full_net_amount   = '0'
                              prm:invoice_date      = '*'
                              prm:data_filepath     = Clip(def:path_sage) & '\'
                              prm:user_name         = Clip(DEF:User_Name_Sage)
                              prm:password          = Clip(DEF:Password_Sage)
                              prm:set_invoice_number= '*'
                              prm:invoice_no        = '*'
                              access:paramss.insert()
                          end!if access:paramss.primerecord() = Level:Benign
          !COURIER
                          get(paramss,0)
                          if access:paramss.primerecord() = Level:Benign
                              prm:account_ref       = Stripcomma(sub:Account_number)
                              If tra:invoice_sub_accounts = 'YES'
                                  prm:name              = Stripcomma(sub:company_name)
                                  prm:address_1         = Stripcomma(sub:address_line1)
                                  prm:address_2         = Stripcomma(sub:address_line2)
                                  prm:address_3         = Stripcomma(sub:address_line3)
                                  prm:address_4         = ''
                                  prm:address_5         = Stripcomma(sub:postcode)
                                  prm:cust_tel_number   = Stripcomma(sub:telephone_number)
                                  prm:contact_name      = Stripcomma(sub:contact_name)
                              Else!If tra:invoice_sub_accounts = 'YES'
                                  prm:name              = Stripcomma(tra:company_name)
                                  prm:address_1         = Stripcomma(tra:address_line1)
                                  prm:address_2         = Stripcomma(tra:address_line2)
                                  prm:address_3         = Stripcomma(tra:address_line3)
                                  prm:address_4         = ''
                                  prm:address_5         = Stripcomma(tra:postcode)
                                  prm:cust_tel_number   = Stripcomma(tra:telephone_number)
                                  prm:contact_name      = Stripcomma(tra:contact_name)
                              End!If tra:invoice_sub_accounts = 'YES'
                              prm:del_address_1     = ''
                              prm:del_address_2     = ''
                              prm:del_address_3     = ''
                              prm:del_address_4     = ''
                              prm:del_address_5     = ''
                              prm:notes_1           = ''
                              prm:notes_2           = ''
                              prm:notes_3           = ''
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.tryfetch(use:password_key)
                              prm:taken_by = use:user_code
                              prm:order_number      = Stripcomma(claim_reference_temp)
                              prm:cust_order_number = 'B' & Stripcomma(batch_number_temp)
                              prm:payment_ref       = ''
                              prm:global_nom_code   = Stripcomma(DEF:Global_Nominal_Code)
                              prm:global_details    = ''
                              prm:items_net         = Stripcomma(labour_paid_temp + parts_paid_temp + courier_paid_temp)
                              prm:items_tax         = Stripcomma((Round(labour_paid_temp * (labour$/100),.01)) + (Round(parts_paid_temp * (parts$/100),.01)) + |
                                                              (Round(courier_paid_temp * (labour$/100),.01)))
                              prm:stock_code        = Stripcomma(DEF:Courier_Stock_Code)
                              prm:description       = Stripcomma(DEF:Courier_Description)
                              prm:nominal_code      = Stripcomma(DEF:Courier_Code)
                              prm:qty_order         = '1'
                              prm:unit_price        = '0'
                              prm:net_amount        = Stripcomma(courier_paid_temp)
                              prm:tax_amount        = Stripcomma(Round(courier_paid_temp * (labour$/100),.01))
                              prm:comment_1         = ''
                              prm:comment_2         = ''
                              prm:unit_of_sale      = '0'
                              prm:full_net_amount   = '0'
                              prm:invoice_date      = '*'
                              prm:data_filepath     = Clip(def:path_sage) & '\'
                              prm:user_name         = Clip(DEF:User_Name_Sage)
                              prm:password          = Clip(DEF:Password_Sage)
                              prm:set_invoice_number= '*'
                              prm:invoice_no        = '*'
                              access:paramss.insert()
                          end!if access:paramss.primerecord() = Level:Benign
                          access:paramss.close()
                          Run(CLip(DEF:Path_Sage) & '\SAGEPROJ.EXE',1)
                          sage_error# = 0
                          access:paramss.open()
                          access:paramss.usefile()
                          Set(paramss,0)
                          If access:paramss.next()
                              sage_error# = 1
                          Else!If access:paramss.next()
                              If prm:invoice_no = -1 or prm:invoice_no = 0 or prm:invoice_no = ''
                                  sage_error# = 1
                              Else!If prm:invoice_no = '-1' or prm:invoice_no = '0'
                                  invoice_number_temp = prm:invoice_no
                                  If invoice_number_temp = 0
                                      sage_error# = 1
                                  End!If invoice_number_temp = 0
                              End!If prm:invoice_no = '-1'
                          End!If access:paramss.next()
                          access:paramss.close()
                      End!If def:use_sage = 'YES'
                      If sage_error# = 0
                          invoice_error# = 1
                          get(invoice,0)
                          if access:invoice.primerecord() = level:benign
                              If def:use_sage = 'YES'
                                  inv:invoice_number = invoice_number_temp
                              End!If def:use_sage = 'YES'
                              inv:job_number         = ''
                              inv:date_created       = Today()
                              inv:account_number     = man:trade_account
                              inv:accounttype        = 'MAI'
                              inv:total              = total_claimed_temp
                              inv:vat_rate_labour    = labour$
                              inv:vat_rate_parts     = parts$
                              inv:vat_rate_retail    = labour$
                              inv:vat_number         = def:vat_number
                              inv:invoice_vat_number = ''
                              inv:currency           = ''
                              inv:batch_number       = batch_number_temp
                              inv:manufacturer       = manufacturer_temp
                              inv:claim_reference    = claim_reference_temp
                              inv:total_claimed      = total_claimed_temp
                              inv:courier_paid       = courier_paid_temp
                              inv:labour_paid        = labour_paid_temp
                              inv:parts_paid         = parts_paid_temp
                              inv:reconciled_date    = Today()
                              inv:invoice_type       = 'WAR'
                              inv:jobs_count         = total_jobs_temp
                              invoice_error# = 0
                              if access:invoice.insert()
                                  invoice_error# = 1
                                  access:invoice.cancelautoinc()
                              end
      
                          end!if access:invoice.primerecord() = level:benign
      
                          If invoice_error# = 0
                              recordspercycle     = 25
                              recordsprocessed    = 0
                              percentprogress     = 0
                              open(progresswindow)
                              progress:thermometer    = 0
                              ?progress:pcttext{prop:text} = '0% Completed'
                              
                              RecordsToProcess = Records(glo:queue)
                              
                              setcursor(cursor:wait)
                              
                              Loop x$ = 1 To RecordsToProcess
                                Get(glo:queue,x$)
                                Do GetNextRecord2
                                access:jobs.clearkey(job:ref_number_key)
                                job:ref_number = glo:pointer
                                if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                                    job:edi = 'FIN'
                                    JOB:Invoice_Number_Warranty = inv:invoice_number
                                    JOB:WInvoice_Courier_Cost = JOB:Courier_Cost_Warranty
                                    JOB:WInvoice_Labour_Cost = JOB:Labour_Cost_Warranty
                                    JOB:WInvoice_Parts_Cost = JOB:Parts_Cost_Warranty
                                    JOB:WInvoice_Sub_Total = JOB:Sub_Total_Warranty
                                    JOB:Invoice_Date_Warranty = Today()
                                    job:paid_warranty = 'YES'
                                    If job:chargeable_job <> 'YES' Or (job:chargeable_job = 'YES' AND job:invoice_number <> '')
                                      If job:consignment_number <> ''
                                          Getstatus(910,0,'JOB')
                                      Else!If job:consignment_number <> ''
                                          If job:loan_unit_number <> ''
                                              Getstatus(811,0,'JOB')
                                          Else!If job:loan_unit_number <> ''
                                              If job:Exchange_Unit_Number = 0
                                                  Getstatus(810,0,'JOB')
                                              End !If job:Exchange_Unit_Number = 0
                                          End!If job:loan_unit_number <> ''
                                      End!If job:consignment_number <> ''
                                    End
                                    access:jobs.update()
                                      get(audit,0)
                                      if access:audit.primerecord() = level:benign
                                          aud:notes         = 'INVOICE NUMBER: ' & CLip(inv:invoice_number) & |
                                                              'BATCH NUMBER: ' & CLip(inv:batch_number)
                                          aud:ref_number    = job:ref_number
                                          aud:date          = today()
                                          aud:time          = clock()
                                          aud:type          = 'JOB'
                                          access:users.clearkey(use:password_key)
                                          use:password = glo:password
                                          access:users.fetch(use:password_key)
                                          aud:user = use:user_code
                                          aud:action        = 'WARRANTY CLAIM RECONCILED'
                                          access:audit.insert()
                                      end!�if access:audit.primerecord() = level:benign
      
                                end
                              End
                              
                              setcursor()
                              Close(ProgressWindow)
                              glo:select1 = inv:invoice_number
                              glo:select2 = batch_number_temp
                              glo:select3 = inv:account_number
                            
                              Summary_Warranty_Invoice
      
      
                              !Are there any exceptions?
      
                              count_exc# = 0
                              setcursor(cursor:wait)
                              save_job_id = access:jobs.savefile()
                              access:jobs.clearkey(job:edi_key)
                              job:manufacturer     = manufacturer_temp
                              job:edi              = 'YES'
                              job:edi_batch_number = batch_number_temp
                              set(job:edi_key,job:edi_key)
                              loop
                                  if access:jobs.next()
                                     break
                                  end !if
                                  if job:manufacturer     <> manufacturer_temp      |
                                  or job:edi              <> 'YES'      |
                                  or job:edi_batch_number <> batch_number_temp      |
                                      then break.  ! end if
                                  count_exc# = 1
                                  Break
                              end !loop
                              access:jobs.restorefile(save_job_id)
                              setcursor()
      
                              If count_exc# = 1
                                  exception# = 0
                                  Case MessageEx('There are jobs remaining on this claim.<13,10><13,10>Do you wish to mark these as exceptions?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                      Of 1 ! &Yes Button
                                          exception# = 1
                                      Of 2 ! &No Button
                                  End!Case MessageEx
      
                                  glo:select1 = manufacturer_temp
                                  glo:select2 = batch_number_temp
                                  glo:select3 = inv:account_number
      
                                      
                                  Exception_Report()
      
                                  recordspercycle     = 25
                                  recordsprocessed    = 0
                                  percentprogress     = 0
                                  open(progresswindow)
                                  progress:thermometer    = 0
                                  ?progress:pcttext{prop:text} = '0% Completed'
                                  
                                  RecordsToProcess = Records(glo:queue)
      
                                  setcursor(cursor:wait)
                                  save_job_id = access:jobs.savefile()
                                  access:jobs.clearkey(job:edi_key)
                                  job:manufacturer     = manufacturer_temp
                                  job:edi              = 'YES'
                                  job:edi_batch_number = batch_number_temp
                                  set(job:edi_key,job:edi_key)
                                  loop
                                      if access:jobs.next()
                                         break
                                      end !if
                                      if job:manufacturer     <> manufacturer_temp      |
                                      or job:edi              <> 'YES'      |
                                      or job:edi_batch_number <> batch_number_temp      |
                                          then break.  ! end if
                                      Do GetNextRecord2
                                      yldcnt# += 1
                                      if yldcnt# > 25
                                         yield() ; yldcnt# = 0
                                      end !if
                                      If exception# = 1
                                          pos = Position(job:edi_key)
                                          job:edi_batch_number = ''
                                          job:edi = 'EXC'
                                          access:jobs.update()
                                          SolaceViewVars(job:ref_number,job:ref_number,job:ref_number,6)
                                          get(audit,0)
                                          if access:audit.primerecord() = level:benign
                                              aud:notes         = 'EXCLUDED FROM BATCH NUMBER: ' & CLip(batch_Number_temp)
                                              aud:ref_number    = job:ref_number
                                              aud:date          = today()
                                              aud:time          = clock()
                                              access:users.clearkey(use:password_key)
                                              use:password = glo:password
                                              access:users.tryfetch(use:password_key)
                                              aud:user = use:user_code
                                              aud:action        = 'REPORT: WARRANTY CLAIMS EXCEPTIONS REPORT'
                                              access:audit.insert()
                                          end!�if access:audit.primerecord() = level:benign
                                          Reset(job:edi_key,pos)
                                      End!If exception# = 1
                                  end !loop
                                  access:jobs.restorefile(save_job_id)
                                  setcursor()
                                  Close(ProgressWindow)
                                  Clear(glo:Queue)
                              End!If count_exc# = 1
      
                          End!If invoice_error# = 0
                      End!If sage_error# = 1
                  End!IF error# = 0
              End !If claim_reference_temp <> ''
              Clear(glo:g_select1)            
          End!If records(jobs_number_queue)
      End!If Choice(?CurrentTab) = 1
      
      BRW1.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Process_Claims, Accepted)
    OF ?Manufacturer2_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer2_temp, Accepted)
      ! Start Change 2806 BE(18/07/03)
      !batch_number_temp = man:batch_number - 1
      !batch_number2_temp = man:batch_number
      !Display(?batch_number_temp)
      
      ! Start Change 4141 BE(22/04/04)
      !batch_number2_temp = man:batch_number - 1
      CLEAR(glo:queue)
      FREE(glo:queue)
      ! End Change 4141 BE(22/04/04)
      
      !Display(?batch_number2_temp)
      !Do get_invoice
      !Do resortbrowse
      ! End Change 2806 BE(18/07/03)
      
      FDCB20.ResetQueue(1)
      ! Start Change 2806 BE(18/07/03)
      Display(?batch_number2_temp)
      Do get_invoice
      Do resortbrowse
      ! End Change 2806 BE(18/07/03)
      
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer2_temp, Accepted)
    OF ?List
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Accepted)
      Do get_invoice
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Accepted)
    OF ?Batch_Number2_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Batch_Number2_Temp, Accepted)
      ! Start Change 4141 BE(22/04/04)
      CLEAR(glo:queue)
      FREE(glo:queue)
      ! End Change 4141 BE(22/04/04)
      
      Do get_invoice
      
      BRW9.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Batch_Number2_Temp, Accepted)
    OF ?manufacturer3_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manufacturer3_temp, Accepted)
      ! Start Change 2806 BE(23/07/03)
      CLEAR(glo:queue)
      FREE(glo:queue)
      ! End Change 2806 BE(23/07/03)
      Do ResortBrowse
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?manufacturer3_temp, Accepted)
    OF ?edi_claims
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?edi_claims, Accepted)
      ! Start Change 2806 BE(22/07/03)
      IF ((CHOICE(?CurrentTab) = 3) AND (RECORDS(glo:queue) > 0)) THEN
      
          BatchNo# = man:batch_number
      
      
          man:Batch_Number += 1
          Access:MANUFACT.Update()
      
      
          recordspercycle     = 25
          recordsprocessed    = 0
          percentprogress     = 0
          OPEN(progresswindow)
          progress:thermometer    = 0
          ?progress:pcttext{prop:text} = '0% Completed'
                              
          RecordsToProcess = RECORDS(glo:queue)
                              
          SETCURSOR(cursor:wait)
          LOOP x# = 1 TO RecordsToProcess
              GET(glo:queue,x#)
              DO GetNextRecord2
              access:jobs.clearkey(job:ref_number_key)
              job:ref_number = glo:pointer
              IF (access:jobs.tryfetch(job:ref_number_key) = Level:Benign) THEN
                  job:edi = 'YES'
                  job:edi_batch_number = BatchNo#
                  access:jobs.update()
              END
          END
          SETCURSOR()
          CLOSE(ProgressWindow)
          CLEAR(glo:queue)
          FREE(glo:queue)
      
          IF (Access:EDIBATCH.PrimeRecord() = Level:Benign)
              ebt:Batch_Number = BatchNo#
              ebt:Manufacturer = manufacturer3_temp
              Access:EDIBATCH.TryInsert()
          END
      
          BRW1.ResetSort(1)
      
          BatchNoString" = FORMAT(BatchNo#, @n8)
          glo:select1 = 'REJECTEDCLAIMS'
      
          CASE (manufacturer3_temp)
              OF 'ALCATEL'
                  Alcatel_EDI(BatchNoString")
              OF 'BOSCH'
                  Bosch_EDI(BatchNoString")
              OF 'ERICSSON'
                  Ericsson_EDI(BatchNoString")
              OF 'MAXON'
                  Maxon_EDI(BatchNoString")
              OF 'MOTOROLA'
                  Motorola_EDI(BatchNoString")
              OF 'NEC'
                  NEC_EDI(BatchNoString")
              OF 'NOKIA'
                  Nokia_EDI(BatchNoString")
              OF 'SHARP'
                  Sharp_Excel_Edi(BatchNoString")
              OF 'SIEMENS'
                  Siemens_Edi(BatchNoString")
              OF 'SIEMENS DECT'
                  Siemens_Dect_Edi(BatchNoString")
              OF 'TELITAL'
                  Telital_EDI(BatchNoString")
              OF 'SAMSUNG'
                  Samsung_Edi(BatchNoString")
              OF 'MITSUBISHI'
                  Mitsibushi_Edi(BatchNoString")
              OF 'SAGEM'
                  Sagem_EDI(BatchNoString")
              OF 'PANASONIC'
                  Panasonic_EDI(BatchNoString")
              OF 'SONY'
                  Sony_EDI(BatchNoString")
              ! Start Change 3524 BE(14/01/04)
              OF 'SENDO'
                  Sendo_EDI(BatchNoString")
              ! End Change 3524 BE(14/01/04)
              ! Start Change 4018 BE(06/04/04)
              OF 'VK'
                  VK_EDI(BatchNoString")
              ! End Change 4018 BE(06/04/04)
          END
      
          glo:select1 = ''
          FDCB13.ResetQueue(1)
      
      END
      ! End Change 2806 BE(22/07/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?edi_claims, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Reconcile_Claims')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?CurrentTab
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?CurrentTab)
    Do resortbrowse
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?CurrentTab)
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      IF Keycode() = MouseLeft2
          POST(EVENT:Accepted,?DASTAG)
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      ! Start Change 2806 BE(22/07/03)
      CASE (CHOICE(?CurrentTab))
          OF 1
              UNHIDE(?DASTAG)
              UNHIDE(?DASTAGALL)
              UNHIDE(?DASUNTAGALL)
              manufacturer_temp = man:manufacturer
              FDCB4.ResetQueue(1)
          OF 2
              HIDE(?DASTAG)
              HIDE(?DASTAGALL)
              HIDE(?DASUNTAGALL)
              manufacturer2_temp = man:manufacturer
              FDCB19.ResetQueue(1)
          OF 3
              UNHIDE(?DASTAG)
              UNHIDE(?DASTAGALL)
              UNHIDE(?DASUNTAGALL)
              manufacturer3_temp = man:manufacturer
              FDCB13.ResetQueue(1)
      END
      CLEAR(glo:queue)
      FREE(glo:queue)
      ! End Change 2806 BE(22/07/03)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='11L(2)I~T~@s1@#1#34R(2)|M~Job No~@s8@#3#60L(2)|M~Account Number~@s15@#4#64L(2)|M~Model Number~@s30@#5#64L(2)|M~I.M.E.I. Number~@s16@#6#64L(2)|M~MSN~@s16@#7#67L(2)|M~Charge Type~@s30@#8#66L(2)|M~Repair Type~@s30@#9#43R(2)|M~Courier Cost~@n10.2@#10#43R(2)|M~Labour Cost~@n10.2@#11#42R(2)|M~Parts Cost~@n10.2@#12#120L(2)|M~Order Number~R@s30@#13#120L(2)|M~Fault Code 1~R@s30@#14#120L(2)|M~Fault Code 2~R@s30@#15#120L(2)|M~Fault Code 3~R@s30@#16#120L(2)|M~Fault Code 4~R@s30@#17#120L(2)|M~Fault Code 5~R@s30@#18#120L(2)|M~Fault Code 6~R@s30@#19#120L(2)|M~Fault Code 7~R@s30@#20#120L(2)|M~Fault Code 8~R@s30@#21#120L(2)|M~Fault Code 9~R@s30@#22#120L(2)|M~Fault Code 10~R@s30@#23#120L(2)|M~Fault Code 11~R@s30@#24#120L(2)|M~Fault Code 12~R@s30@#25#'
          ?Tab1{PROP:TEXT} = 'Unreconciled Claims'
        OF 2
          ?Browse:1{PROP:FORMAT} ='34R(2)|M~Job No~@s8@#3#60L(2)|M~Account Number~@s15@#4#64L(2)|M~Model Number~@s30@#5#64L(2)|M~I.M.E.I. Number~@s16@#6#64L(2)|M~MSN~@s16@#7#67L(2)|M~Charge Type~@s30@#8#66L(2)|M~Repair Type~@s30@#9#43R(2)|M~Courier Cost~@n10.2@#10#43R(2)|M~Labour Cost~@n10.2@#11#42R(2)|M~Parts Cost~@n10.2@#12#120L(2)|M~Order Number~R@s30@#13#120L(2)|M~Fault Code 1~R@s30@#14#120L(2)|M~Fault Code 2~R@s30@#15#120L(2)|M~Fault Code 3~R@s30@#16#120L(2)|M~Fault Code 4~R@s30@#17#120L(2)|M~Fault Code 5~R@s30@#18#120L(2)|M~Fault Code 6~R@s30@#19#120L(2)|M~Fault Code 7~R@s30@#20#120L(2)|M~Fault Code 8~R@s30@#21#120L(2)|M~Fault Code 9~R@s30@#22#120L(2)|M~Fault Code 10~R@s30@#23#120L(2)|M~Fault Code 11~R@s30@#24#120L(2)|M~Fault Code 12~R@s30@#25#'
          ?Tab2{PROP:TEXT} = 'Reconciled Claims'
        OF 3
          ?Browse:1{PROP:FORMAT} ='11L(2)I~T~@s1@#1#34R(2)|M~Job No~@s8@#3#60L(2)|M~Account Number~@s15@#4#64L(2)|M~Model Number~@s30@#5#64L(2)|M~I.M.E.I. Number~@s16@#6#64L(2)|M~MSN~@s16@#7#67L(2)|M~Charge Type~@s30@#8#66L(2)|M~Repair Type~@s30@#9#43R(2)|M~Courier Cost~@n10.2@#10#43R(2)|M~Labour Cost~@n10.2@#11#42R(2)|M~Parts Cost~@n10.2@#12#120L(2)|M~Order Number~R@s30@#13#120L(2)|M~Fault Code 1~R@s30@#14#120L(2)|M~Fault Code 2~R@s30@#15#120L(2)|M~Fault Code 3~R@s30@#16#120L(2)|M~Fault Code 4~R@s30@#17#120L(2)|M~Fault Code 5~R@s30@#18#120L(2)|M~Fault Code 6~R@s30@#19#120L(2)|M~Fault Code 7~R@s30@#20#120L(2)|M~Fault Code 8~R@s30@#21#120L(2)|M~Fault Code 9~R@s30@#22#1020L(2)|M~Fault Code 10~R@s255@#23#1020L(2)|M~Fault Code 11~R@s255@#24#1020L(2)|M~Fault Code 12~R@s30@#25#'
          ?Tab3{PROP:TEXT} = 'Rejected Claims'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?Manufacturer_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Batch_Number_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Manufacturer2_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?List
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, NewSelection)
      Do get_invoice
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, NewSelection)
    OF ?Batch_Number2_Temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?manufacturer3_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::5:DASUNTAGALL
      Do resortbrowse
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB4.ResetQueue(1)
      FDCB10.ResetQueue(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW9.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = Manufacturer2_temp
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = Batch_Number2_Temp
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  Total_Claimed_temp = inv:Courier_Paid + inv:Parts_Paid + inv:Labour_Paid
  PARENT.SetQueueRecord
  SELF.Q.Total_Claimed_temp = Total_Claimed_temp      !Assign formula result to display queue


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Manufacturer_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'YES'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Batch_Number_Temp
  ELSIF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Manufacturer2_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'FIN'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Batch_Number2_Temp
  ELSIF Choice(?CurrentTab) = 3
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = manufacturer3_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'EXC'
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Manufacturer_Temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'YES'
     GET(SELF.Order.RangeList.List,3)
     Self.Order.RangeList.List.Right = Batch_Number_Temp
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF Choice(?CurrentTab) = 1
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = job:Ref_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      jobs_tag_temp = ''
    ELSE
      jobs_tag_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  Charge_Repair_Temp = CLIP(job:Warranty_Charge_Type) & ' / ' & CLIP(job:Repair_Type_Warranty)
  ! Start Change 4585 BE(04/10/2004)
  jfc1 = FormatJobFaultCode(job:Fault_Code1, 1, job:Manufacturer)
  jfc2 = FormatJobFaultCode(job:Fault_Code2, 2, job:Manufacturer)
  jfc3 = FormatJobFaultCode(job:Fault_Code3, 3, job:Manufacturer)
  jfc4 = FormatJobFaultCode(job:Fault_Code4, 4, job:Manufacturer)
  jfc5 = FormatJobFaultCode(job:Fault_Code5, 5, job:Manufacturer)
  jfc6 = FormatJobFaultCode(job:Fault_Code6, 6, job:Manufacturer)
  jfc7 = FormatJobFaultCode(job:Fault_Code7, 7, job:Manufacturer)
  jfc8 = FormatJobFaultCode(job:Fault_Code8, 8, job:Manufacturer)
  jfc9 = FormatJobFaultCode(job:Fault_Code9, 9, job:Manufacturer)
  jfc10 = FormatJobFaultCode(job:Fault_Code10, 10, job:Manufacturer)
  jfc11 = FormatJobFaultCode(job:Fault_Code11, 11, job:Manufacturer)
  jfc12 = FormatJobFaultCode(job:Fault_Code12, 12, job:Manufacturer)
  ! End Change 4585 BE(04/10/2004)
  PARENT.SetQueueRecord
  IF (jobs_tag_temp = '*')
    SELF.Q.jobs_tag_temp_Icon = 2
  ELSE
    SELF.Q.jobs_tag_temp_Icon = 1
  END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = job:Ref_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !jobs_tag_temp
  OF 3 !job:Ref_Number
  OF 4 !job:Account_Number
  OF 5 !job:Model_Number
  OF 6 !job:ESN
  OF 7 !job:MSN
  OF 8 !job:Warranty_Charge_Type
  OF 9 !job:Repair_Type_Warranty
  OF 10 !job:Courier_Cost_Warranty
  OF 11 !job:Labour_Cost_Warranty
  OF 12 !job:Parts_Cost_Warranty
  OF 13 !job:Order_Number
  OF 14 !jfc1
  OF 15 !jfc2
  OF 16 !jfc3
  OF 17 !jfc4
  OF 18 !jfc5
  OF 19 !jfc6
  OF 20 !jfc7
  OF 21 !jfc8
  OF 22 !jfc9
  OF 23 !jfc10
  OF 24 !jfc11
  OF 25 !jfc12
  OF 26 !job:Manufacturer
  OF 27 !job:EDI
  OF 28 !job:EDI_Batch_Number
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBS)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('jobs_tag_temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jobs_tag_temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jobs_tag_temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job No')                     !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job No')                     !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('I.M.E.I. Number')            !Header
                PSTRING('@s16')                       !Picture
                PSTRING('I.M.E.I. Number')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:MSN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('MSN')                        !Header
                PSTRING('@s16')                       !Picture
                PSTRING('MSN')                        !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Warranty_Charge_Type')   !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Charge Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Charge Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Repair_Type_Warranty')   !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Repair Type')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Repair Type')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Courier_Cost_Warranty')  !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Courier Cost')               !Header
                PSTRING('@n10.2')                     !Picture
                PSTRING('Courier Cost')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Labour_Cost_Warranty')   !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Labour Cost')                !Header
                PSTRING('@n10.2')                     !Picture
                PSTRING('Labour Cost')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Parts_Cost_Warranty')    !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Parts Cost')                 !Header
                PSTRING('@n10.2')                     !Picture
                PSTRING('Parts Cost')                 !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Order_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Order Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Order Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc1')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc1')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc1')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc2')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc2')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc2')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc3')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc3')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc3')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc4')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc4')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc4')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc5')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc5')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc5')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc6')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc6')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc6')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc7')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc7')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc7')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc8')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc8')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc8')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc9')                       !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc9')                       !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc9')                       !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc10')                      !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc10')                      !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc10')                      !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc11')                      !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc11')                      !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc11')                      !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('jfc12')                      !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('jfc12')                      !Header
                PSTRING('@S20')                       !Picture
                PSTRING('jfc12')                      !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Manufacturer')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Manufacturer')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Manufacturer')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:EDI')                    !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('EDI')                        !Header
                PSTRING('@s3')                        !Picture
                PSTRING('EDI')                        !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:EDI_Batch_Number')       !Field Name
                SHORT(24)                             !Default Column Width
                PSTRING('EDI Batch Number')           !Header
                PSTRING('@n6')                        !Picture
                PSTRING('EDI Batch Number')           !Description
                STRING('D')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                END
XpDim           SHORT(27)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

