

   MEMBER('sbk01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBK01045.INC'),ONCE        !Local module procedure declarations
                     END


UpdateOrderJobs PROCEDURE                             !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::orjtmp:Record LIKE(orjtmp:RECORD),STATIC
QuickWindow          WINDOW('Update the ORDJOBS File'),AT(,,196,124),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateOrderJobs'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,188,98),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?orjtmp:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,20,40,10),USE(orjtmp:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Order Number'),AT(8,34),USE(?orjtmp:OrderNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,34,40,10),USE(orjtmp:OrderNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Order Number'),TIP('Order Number'),UPR
                           PROMPT('Part Number'),AT(8,48),USE(?orjtmp:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,48,124,10),USE(orjtmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                           PROMPT('Job Number'),AT(8,62),USE(?orjtmp:JobNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,62,40,10),USE(orjtmp:JobNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                           CHECK('Chargeable/Warranty'),AT(64,76,88,8),USE(orjtmp:CharWarr),MSG('Chargeable/Warranty'),TIP('Chargeable/Warranty'),VALUE('1','0')
                           PROMPT('Quantity'),AT(8,88),USE(?orjtmp:Quantity:Prompt),TRN
                           SPIN(@n8),AT(64,88,51,10),USE(orjtmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity'),TIP('Quantity'),UPR,STEP(1)
                         END
                       END
                       BUTTON('OK'),AT(98,106,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(147,106,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(147,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?orjtmp:RecordNumber:Prompt{prop:FontColor} = -1
    ?orjtmp:RecordNumber:Prompt{prop:Color} = 15066597
    If ?orjtmp:RecordNumber{prop:ReadOnly} = True
        ?orjtmp:RecordNumber{prop:FontColor} = 65793
        ?orjtmp:RecordNumber{prop:Color} = 15066597
    Elsif ?orjtmp:RecordNumber{prop:Req} = True
        ?orjtmp:RecordNumber{prop:FontColor} = 65793
        ?orjtmp:RecordNumber{prop:Color} = 8454143
    Else ! If ?orjtmp:RecordNumber{prop:Req} = True
        ?orjtmp:RecordNumber{prop:FontColor} = 65793
        ?orjtmp:RecordNumber{prop:Color} = 16777215
    End ! If ?orjtmp:RecordNumber{prop:Req} = True
    ?orjtmp:RecordNumber{prop:Trn} = 0
    ?orjtmp:RecordNumber{prop:FontStyle} = font:Bold
    ?orjtmp:OrderNumber:Prompt{prop:FontColor} = -1
    ?orjtmp:OrderNumber:Prompt{prop:Color} = 15066597
    If ?orjtmp:OrderNumber{prop:ReadOnly} = True
        ?orjtmp:OrderNumber{prop:FontColor} = 65793
        ?orjtmp:OrderNumber{prop:Color} = 15066597
    Elsif ?orjtmp:OrderNumber{prop:Req} = True
        ?orjtmp:OrderNumber{prop:FontColor} = 65793
        ?orjtmp:OrderNumber{prop:Color} = 8454143
    Else ! If ?orjtmp:OrderNumber{prop:Req} = True
        ?orjtmp:OrderNumber{prop:FontColor} = 65793
        ?orjtmp:OrderNumber{prop:Color} = 16777215
    End ! If ?orjtmp:OrderNumber{prop:Req} = True
    ?orjtmp:OrderNumber{prop:Trn} = 0
    ?orjtmp:OrderNumber{prop:FontStyle} = font:Bold
    ?orjtmp:PartNumber:Prompt{prop:FontColor} = -1
    ?orjtmp:PartNumber:Prompt{prop:Color} = 15066597
    If ?orjtmp:PartNumber{prop:ReadOnly} = True
        ?orjtmp:PartNumber{prop:FontColor} = 65793
        ?orjtmp:PartNumber{prop:Color} = 15066597
    Elsif ?orjtmp:PartNumber{prop:Req} = True
        ?orjtmp:PartNumber{prop:FontColor} = 65793
        ?orjtmp:PartNumber{prop:Color} = 8454143
    Else ! If ?orjtmp:PartNumber{prop:Req} = True
        ?orjtmp:PartNumber{prop:FontColor} = 65793
        ?orjtmp:PartNumber{prop:Color} = 16777215
    End ! If ?orjtmp:PartNumber{prop:Req} = True
    ?orjtmp:PartNumber{prop:Trn} = 0
    ?orjtmp:PartNumber{prop:FontStyle} = font:Bold
    ?orjtmp:JobNumber:Prompt{prop:FontColor} = -1
    ?orjtmp:JobNumber:Prompt{prop:Color} = 15066597
    If ?orjtmp:JobNumber{prop:ReadOnly} = True
        ?orjtmp:JobNumber{prop:FontColor} = 65793
        ?orjtmp:JobNumber{prop:Color} = 15066597
    Elsif ?orjtmp:JobNumber{prop:Req} = True
        ?orjtmp:JobNumber{prop:FontColor} = 65793
        ?orjtmp:JobNumber{prop:Color} = 8454143
    Else ! If ?orjtmp:JobNumber{prop:Req} = True
        ?orjtmp:JobNumber{prop:FontColor} = 65793
        ?orjtmp:JobNumber{prop:Color} = 16777215
    End ! If ?orjtmp:JobNumber{prop:Req} = True
    ?orjtmp:JobNumber{prop:Trn} = 0
    ?orjtmp:JobNumber{prop:FontStyle} = font:Bold
    ?orjtmp:CharWarr{prop:Font,3} = -1
    ?orjtmp:CharWarr{prop:Color} = 15066597
    ?orjtmp:CharWarr{prop:Trn} = 0
    ?orjtmp:Quantity:Prompt{prop:FontColor} = -1
    ?orjtmp:Quantity:Prompt{prop:Color} = 15066597
    If ?orjtmp:Quantity{prop:ReadOnly} = True
        ?orjtmp:Quantity{prop:FontColor} = 65793
        ?orjtmp:Quantity{prop:Color} = 15066597
    Elsif ?orjtmp:Quantity{prop:Req} = True
        ?orjtmp:Quantity{prop:FontColor} = 65793
        ?orjtmp:Quantity{prop:Color} = 8454143
    Else ! If ?orjtmp:Quantity{prop:Req} = True
        ?orjtmp:Quantity{prop:FontColor} = 65793
        ?orjtmp:Quantity{prop:Color} = 16777215
    End ! If ?orjtmp:Quantity{prop:Req} = True
    ?orjtmp:Quantity{prop:Trn} = 0
    ?orjtmp:Quantity{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateOrderJobs',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateOrderJobs',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateOrderJobs',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:RecordNumber:Prompt;  SolaceCtrlName = '?orjtmp:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:RecordNumber;  SolaceCtrlName = '?orjtmp:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:OrderNumber:Prompt;  SolaceCtrlName = '?orjtmp:OrderNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:OrderNumber;  SolaceCtrlName = '?orjtmp:OrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:PartNumber:Prompt;  SolaceCtrlName = '?orjtmp:PartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:PartNumber;  SolaceCtrlName = '?orjtmp:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:JobNumber:Prompt;  SolaceCtrlName = '?orjtmp:JobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:JobNumber;  SolaceCtrlName = '?orjtmp:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:CharWarr;  SolaceCtrlName = '?orjtmp:CharWarr';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:Quantity:Prompt;  SolaceCtrlName = '?orjtmp:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:Quantity;  SolaceCtrlName = '?orjtmp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOrderJobs')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateOrderJobs')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orjtmp:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orjtmp:Record,History::orjtmp:Record)
  SELF.AddHistoryField(?orjtmp:RecordNumber,1)
  SELF.AddHistoryField(?orjtmp:OrderNumber,2)
  SELF.AddHistoryField(?orjtmp:PartNumber,3)
  SELF.AddHistoryField(?orjtmp:JobNumber,4)
  SELF.AddHistoryField(?orjtmp:CharWarr,6)
  SELF.AddHistoryField(?orjtmp:Quantity,7)
  SELF.AddUpdateFile(Access:ORDJOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDJOBS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDJOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDJOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateOrderJobs',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateOrderJobs')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

