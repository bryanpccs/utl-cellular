

   MEMBER('SBRSALVL.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBRSA003.INC'),ONCE        !Local module procedure declarations
                     END


GetCriteria PROCEDURE                                 !Generated from procedure template - Window

LOC:EndDate          DATE
LOC:StartDate        DATE
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
LOC:DayLeawayCount   LONG
LOC:Courier          STRING(30)
OPTION1              SHORT
LOC:TabNumber        BYTE(1)
LOC:MaxTabNumber     BYTE(2)
LOC:AccountNumber    STRING(15)
LOC:PartNumber       STRING(30)
LOC:JobNumber        LONG
LOC:UserCode         STRING(3)
LOC:UserName         STRING(100)
LOC:ProgramName      STRING(100)
LOC:SectionName      STRING(100)
LOC:FileName         STRING(255)
Excel                SIGNED !OLE Automation holder
LOC:SalesType        STRING(10)
LOC:PartType         STRING(10)
Accumulators         DECIMAL(7,2),DIM(3,3,3)
LOC:Text             STRING(255)
LOC:CommentText      STRING(100)
RecordCount          LONG
CONST:DateFormat     STRING('dd mmm yyyy')
LOC:Account_Name     STRING(100)
LOC:CompanyName      STRING(30)
Result               BYTE
CountOrders          LONG
CountLines           LONG
CountParts           LONG
CountDelivered       LONG
CancelPressed        BYTE(FALSE)
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
LOC:JobsExtendedInSync BYTE(false)
Progress:Text        STRING(100)
AccountQueue         QUEUE,PRE(aq)
AccountNumber        STRING(15)
AccountName          STRING(30)
NumberOfOrders       LONG
NumberOfLines        LONG
AccountAccumulators  DECIMAL(7,2),DIM(3,3,3)
                     END
Tally                GROUP,PRE(tally)
Quantity             LONG
QuantityInvoiced     LONG
QuantityBackOrdered  LONG
QuantityOrder        LONG
QuantityAccessory    LONG
QuantitySpareParts   LONG
ValueOfAccessories   DECIMAL(7,2),DIM(2)
ValueOfSpares        DECIMAL(7,2),DIM(3)
                     END
mo:SelectedTab::Sheet1    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Window
mo:SelectedField  Long
mo:WinType        equate(Win:Window)
mo:SelectedTab::Sheet1    Long  ! Makeover Template      LocalTreat = Default
mo:SelectedButton Long  ! ProcedureType = Window  WinType = Window
mo:SelectedField  Long
mo:WinType        equate(Win:Window)
window               WINDOW('Parts Ordered Report'),AT(,,147,112),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),CENTERED,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,140,76),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Engineer Performance Report Criteria'),USE(?Tab1)
                           STRING('Start Date'),AT(8,24,40,8),USE(?String2)
                           ENTRY(@D8),AT(52,24,64,10),USE(LOC:StartDate),IMM,FONT(,,,FONT:bold),TIP('Enter the earliest Order date'),REQ
                           BUTTON('...'),AT(120,24,10,10),USE(?StartPopCalendar),IMM,FLAT,LEFT,ICON('list3.ico')
                           STRING('End Date'),AT(8,40,40,8),USE(?String2:2)
                           ENTRY(@D8),AT(52,40,64,10),USE(LOC:EndDate),IMM,FONT(,,,FONT:bold),TIP('Enter the latest Order date'),REQ
                           BUTTON('...'),AT(120,40,10,10),USE(?EndPopCalendar),IMM,FLAT,LEFT,ICON('list3.ico')
                         END
                         TAB('Repair Categories'),USE(?Tab2)
                         END
                       END
                       STRING('Program name'),AT(8,8,132,12),USE(?TitleString),FONT(,,,FONT:regular,CHARSET:ANSI)
                       PANEL,AT(4,84,140,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Export To Excel'),AT(24,88,56,16),USE(?OkButton),FLAT,LEFT,TIP('Click to print report'),ICON('Excel.ico'),DEFAULT
                       BUTTON('Cancel'),AT(84,88,56,16),USE(?CancelButton),FLAT,LEFT,TIP('Click to close this form'),ICON('Cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              CLASS(ToolbarClass)
DisplayButtons         PROCEDURE(),DERIVED
                     END

! Dimensions for the Accumulators(3, 3, 3)

OrderDimension     EQUATE(1)
BackorderDimension EQUATE(2)
InvoiceDimension   EQUATE(3)

PartsDimension     EQUATE(1)
AccessoryDimension EQUATE(2)
TotalDimension     EQUATE(3)

CostDimension      EQUATE(1)
TradeDimension     EQUATE(2)
RetailDimension    EQUATE(3)

! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
! Excel EQUATES

!----------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
xlSolid      EQUATE(        1 ) ! Constants.
xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
xlRight      EQUATE(0FFFFEFC8h) ! Constants.
xlLastCell   EQUATE(       11 ) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.

xlCenter     EQUATE(0FFFFEFF4h) ! Excel.Constants
xlBottom     EQUATE(0FFFFEFF5h) ! Excel.Constants
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!----------------------------------------------------

! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    If ?LOC:StartDate{prop:ReadOnly} = True
        ?LOC:StartDate{prop:FontColor} = 65793
        ?LOC:StartDate{prop:Color} = 15066597
    Elsif ?LOC:StartDate{prop:Req} = True
        ?LOC:StartDate{prop:FontColor} = 65793
        ?LOC:StartDate{prop:Color} = 8454143
    Else ! If ?LOC:StartDate{prop:Req} = True
        ?LOC:StartDate{prop:FontColor} = 65793
        ?LOC:StartDate{prop:Color} = 16777215
    End ! If ?LOC:StartDate{prop:Req} = True
    ?LOC:StartDate{prop:Trn} = 0
    ?LOC:StartDate{prop:FontStyle} = font:Bold
    ?String2:2{prop:FontColor} = -1
    ?String2:2{prop:Color} = 15066597
    If ?LOC:EndDate{prop:ReadOnly} = True
        ?LOC:EndDate{prop:FontColor} = 65793
        ?LOC:EndDate{prop:Color} = 15066597
    Elsif ?LOC:EndDate{prop:Req} = True
        ?LOC:EndDate{prop:FontColor} = 65793
        ?LOC:EndDate{prop:Color} = 8454143
    Else ! If ?LOC:EndDate{prop:Req} = True
        ?LOC:EndDate{prop:FontColor} = 65793
        ?LOC:EndDate{prop:Color} = 16777215
    End ! If ?LOC:EndDate{prop:Req} = True
    ?LOC:EndDate{prop:Trn} = 0
    ?LOC:EndDate{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    ?TitleString{prop:FontColor} = -1
    ?TitleString{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
ExportSetup                             ROUTINE
    DATA
temp DATE
    CODE
        !-----------------------------------------------------------------
        SETCURSOR(CURSOR:Wait)
        CancelPressed = False

        IF LOC:StartDate > LOC:EndDate
            temp          = LOC:EndDate
            LOC:EndDate   = LOC:StartDate
            LOC:StartDate = temp
        END !IF
        !-----------------------------------------------------------------
        IF CLIP(LOC:FileName) = ''
            DO LoadFileDialog
        END !IF

        IF LOC:FileName = ''
            Case MessageEx('No filename chosen.<10,13>   Enter a filename then try again','ServiceBase 2000',|
                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button

            End!Case MessageEx

            SETCURSOR()

            EXIT
        END !IF LOC:FileName = ''

        IF LOWER(RIGHT(LOC:FileName, 4)) <> '.xls'
            LOC:FileName = CLIP(LOC:FileName) & '.xls'
        END !IF
        !-----------------------------------------------------------------
        DO ProgressBar_Setup
        !-----------------------------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.Calculation'}    = xlCalculationManual
        Excel{'Application.ScreenUpdating'} = False
        Excel{'Application.Visible'}        = False

        DO XL_AddWorkbook
        !-----------------------------------------------------------------
    EXIT
ExportBody                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        DO CreateTitleSheet
        DO CreateDataSheet
        !-----------------------------------------------------------------
    EXIT
ExportFinalize                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            Excel{'Application.ActiveWorkBook.Close()'}
            SETCURSOR()
            Excel{PROP:DEACTIVATE}

            EXIT
        END !IF
        !-----------------------------------------------------------------
        Excel{'Range("A1").Select'}

        Excel{'Application.Calculation'}    = xlCalculationAutomatic
        Excel{'Application.ScreenUpdating'} = False
        Excel{'Application.Visible'}        = False

        DO XL_SetWorksheetLandscape
        !-----------------------------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets
        ! Show summary on entry
        !
        Excel{'Application.DisplayAlerts'} = False  ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}
        Excel{'Sheets("Sheet3").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}
        Excel{'Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = 'ServiceBase 2000'

        Excel{'Application.ActiveWorkBook.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
        Excel{'Application.ActiveWorkBook.Close()'}

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------------------------
        DO ProgressBar_Finalise

        SETCURSOR()

        Case MessageEx('Export Completed.', |
                     'ServiceBase 2000',    |
                     'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
        Of 1 ! &OK Button
        End!Case MessageEx
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
CreateTitleSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet1").Select'}
        DO XL_SetWorksheetPortrait

        Excel{'ActiveSheet.Columns("a:a").ColumnWidth'} = 30
        Excel{'ActiveSheet.Columns("b:ae").ColumnWidth'} = 12

        Excel{'ActiveSheet.Name'} = 'Summary'
        !-----------------------------------------------------------------
        ! Title
        Excel{'ActiveCell.Formula'}  = 'Title'
            Excel{'Selection.Font.Bold'} = True

        Excel{'Range("A2").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Font.Size'} = 16
            Excel{'ActiveCell.Formula'}   = CLIP(LOC:ProgramName)

        Excel{'Range("A1:ae2").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Criteria
        Excel{'Range("A4").Select'}
            Excel{'ActiveCell.Formula'} = 'Criteria'
            DO XL_SetBold

        Excel{'Range("A4:ae4").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !
        Excel{'Range("A5").Select'}
            Excel{'ActiveCell.Formula'}          = 'Ordered Date From:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:StartDate, @D8))
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A6").Select'}
            Excel{'ActiveCell.Formula'}          = 'Ordered Date To:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:EndDate, @D8))
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A7").Select'}
            Excel{'ActiveCell.Formula'}     = 'Created By'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'} = LOC:UserName

        Excel{'Range("A8").Select'}
            Excel{'ActiveCell.Formula'}      = 'Date Created'
            DO XL_ColRight
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
                Excel{'ActiveCell.Formula'} = LEFT(FORMAT(TODAY(), @D8))

        Excel{'Range("A4:ae8").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A10").Select'}
            DO XL_SetBold
            Excel{'ActiveCell.Formula'} = 'Summary'
        Excel{'Range("A10:ae10").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------------------------
        ! Prepare For summary details
        !
        Excel{'Range("A11").Select'}
        !-----------------------------------------------------------------
    EXIT
CreateDataSheet                                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO XL_AddSheet
        DO XL_SetWorksheetLandscape

        LOC:SectionName = 'Detailed'
        LOC:CommentText = ''
        DO CreateSectionHeader

        DO SetColumns

        CountOrders    = 0
        CountLines     = 0
        CountParts     = 0
        CountDelivered = 0
        !------------------------------------------
        !
        Access:INVOICE.ClearKey(inv:Date_Created_Key)
        inv:Date_Created = LOC:StartDate
        SET(inv:Date_Created_Key, inv:Date_Created_Key)
        !------------------------------------------
        Progress:Text    = CLIP(LOC:SectionName)
        RecordsToProcess = RECORDS(INVOICE)
        RecordsProcessed = 0
        RecordCount      = 0

        DO ProgressBar_LoopPre
        !-----------------------------------------------------------------
        LOOP UNTIL Access:INVOICE.Next()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF inv:Date_Created > LOC:EndDate
                BREAK
            END !IF
            !-------------------------------------------------------------
            CountOrders += 1

            DO Print_RETSALES
            !-------------------------------------------------------------
        END !LOOP

        IF CancelPressed
            EXIT
        END !IF

        DO ProgressBar_LoopPost
        !-----------------------------------------------------------------
        DO WriteSummary
        !-----------------------------------------------------------------
    EXIT

!-----------------------------------------------
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment SIGNED
    CODE
        !-----------------------------------------------
        xlComment = Excel{'Selection.AddComment("' & CLIP(LOC:CommentText) & '")'}

        xlComment{'Shape.IncrementLeft'} = 127.50
        xlComment{'Shape.IncrementTop'}  =   8.25
        xlComment{'Shape.ScaleWidth'}    =   1.76
        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = CONST:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A11").Select'}
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("A11"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("A11").Select'}
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1 ! 2
        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT
XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
CreateSectionHeader                                        ROUTINE
    DATA
LastColumn STRING(2)
    CODE
        !-----------------------------------------------       
        Excel{'ActiveSheet.Name'} = CLIP(LOC:SectionName)
        !-----------------------------------------------
        LastColumn = 'AI'
        Excel{'Range("A1:' & LastColumn & '6").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("A8:' & LastColumn & '8").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------       
        Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Formula'}          = CLIP(LOC:ProgramName)
            Excel{'ActiveCell.Font.Size'}        = 14
            DO XL_SetBold

        Excel{'Range("A3").Select'}
            Excel{'ActiveCell.Formula'}          = 'Date From:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:StartDate, @D8))
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A4").Select'}
            Excel{'ActiveCell.Formula'}          = 'Date To:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(LOC:EndDate, @D8))
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A5").Select'}
            Excel{'ActiveCell.Formula'}          = 'Created By:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = LOC:UserName
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("A6").Select'}
            Excel{'ActiveCell.Formula'}          = 'Created Date:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = LEFT(FORMAT(TODAY(), @D8))
                DO XL_FormatDate
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------       
        Excel{'Range("A8").Select'}
            Excel{'ActiveCell.Formula'}     = 'Section Name:'
            DO XL_ColRight
                Excel{'ActiveCell.Formula'} = CLIP(LOC:SectionName)
                DO XL_SetBold
                DO XL_HorizontalAlignmentLeft

        Excel{'Range("E8").Select'}
            Excel{'ActiveCell.Formula'}     = 'Total Records:'
            DO XL_HorizontalAlignmentRight
            DO XL_ColRight
                Excel{'ActiveCell.Formula'} = 0
                DO XL_HorizontalAlignmentLeft
        !-----------------------------------------------
        Excel{'Range("A10").Select'}
        !-----------------------------------------------
    EXIT
Print_RETSALES                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !------------------------------------------
        !
        Access:RETSALES.ClearKey(ret:Invoice_Number_Key)
        ret:Invoice_Number = inv:Invoice_Number
        SET(ret:Invoice_Number_Key, ret:Invoice_Number_Key)
        !------------------------------------------
        Progress:Text = CLIP(LOC:SectionName) & ' Invoice(' & inv:Invoice_Number & ')'
        !-----------------------------------------------------------------
        LOOP UNTIL Access:RETSALES.Next()
            !-------------------------------------------------------------
            DO ProgressBar_Loop

            IF CancelPressed
                BREAK
            END !IF
            !-------------------------------------------------------------
            IF ret:Invoice_Number <> inv:Invoice_Number
                BREAK
            END !IF
            !-------------------------------------------------------------
            CountLines += 1

            DO Print_RETSTOCK
            !-------------------------------------------------------------
        END !LOOP

        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
    EXIT
Print_RETSTOCK                                                      ROUTINE
    DATA
Dimension1 BYTE
Dimension2 BYTE
    CODE
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        DO ClearAccumulators

        DO GetAccountFromQueue
        aq:NumberOfOrders += 1
        PUT(AccountQueue, +aq:AccountNumber, +aq:AccountName)
        !-----------------------------------------------------------------
        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
        res:Ref_Number = ret:Ref_Number
        SET(res:Part_Number_Key, res:Part_Number_Key)
        !-----------------------------------------------------------------
        LOOP UNTIL Access:RETSTOCK.Next()
            !-------------------------------------------------------------
            IF res:Ref_Number <> ret:Ref_Number
                BREAK
            END !IF
            !-------------------------------------------------------------
            CountParts       += res:Quantity
            tally:Quantity   += res:Quantity

            aq:NumberOfLines += 1
            PUT(AccountQueue, +aq:AccountNumber, +aq:AccountName)
            !-------------------------------------------------------------
            IF ret:Invoice_Number <> ''
                !LOC:SalesType = 'INVOICED'
                Dimension1                 = InvoiceDimension
                tally:QuantityInvoiced    += res:Quantity

            ELSIF res:despatched = 'YES'
                !LOC:SalesType = 'SALE'
                Dimension1                 = OrderDimension
                tally:QuantityOrder       += res:Quantity

            Else
                !LOC:SalesType = 'BACKORDER'
                Dimension1                 = BackorderDimension
                tally:QuantityBackOrdered += res:Quantity

            End!If res:despatched = 'YES'
            !-------------------------------------------------------------
            IF res:despatched = 'YES'
                CountDelivered += res:Quantity
            End!If res:despatched = 'YES'
            !-------------------------------------------------------------
            DO LoadSTOCK
!           IF Result <> True
!                EXIT
!           END !IF

            If sto:Accessory = 'YES'
                Dimension2 = AccessoryDimension
            Else
                Dimension2 = PartsDimension
            End !Case ret:Payment_Method
            !-------------------------------------------------------------
            Accumulators[Dimension1, Dimension2,     CostDimension]   += res:Quantity * res:Item_Cost
            Accumulators[Dimension1, Dimension2,     RetailDimension] += res:Quantity * res:Retail_Cost
            Accumulators[Dimension1, Dimension2,     TradeDimension]  += res:Quantity * res:Sale_Cost

            Accumulators[Dimension1, TotalDimension, CostDimension]   += res:Quantity * res:Item_Cost
            Accumulators[Dimension1, TotalDimension, RetailDimension] += res:Quantity * res:Retail_Cost
            Accumulators[Dimension1, TotalDimension, TradeDimension]  += res:Quantity * res:Sale_Cost
            !-------------------------------------------------------------
        END !LOOP
        !-----------------------------------------------------------------
        DO WriteColumns
        !-----------------------------------------------------------------
    EXIT
ClearAccumulators                                                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP X# = 1 TO 3
            LOOP Y# = 1 TO 3
                LOOP Z# = 1 TO 3
                    Accumulators[X#, Y#, Z#] = 0
                END !LOOP
            END !LOOP
        END !LOOP
        !-----------------------------------------------------------------
        tally:QuantityOrder       = 0
        tally:QuantityBackOrdered = 0
        tally:QuantityInvoiced    = 0
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
SetColumns                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range("a10:ai11").Select'}
            DO XL_SetTitle
            DO XL_SetBorder
        !-----------------------------------------------
        Excel{'Range("a10:j10").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("i10:q10").Select'}
            DO XL_SetTitle
            DO XL_SetGrid

        Excel{'Range("i10").Select'}
            Excel{'ActiveCell.Formula'} = 'Order Value Of Spare Part Sales'
        Excel{'Range("l10").Select'}
            Excel{'ActiveCell.Formula'} = 'Order Value Of Accessory Sales'
        Excel{'Range("o10").Select'}
            Excel{'ActiveCell.Formula'} = 'Total Order Value Of All Sales'

        Excel{'Range("r10").Select'}
            Excel{'ActiveCell.Formula'} = 'Backorder  Value Of Spare Part Sales'
        Excel{'Range("u10").Select'}
            Excel{'ActiveCell.Formula'} = 'Backorder Value Of Accessory Sales'
        Excel{'Range("x10").Select'}
            Excel{'ActiveCell.Formula'} = 'Total Backorder Value Of All Sales'

        Excel{'Range("aa10").Select'}
            Excel{'ActiveCell.Formula'} = 'Invoice Value Of Spare Part Sales'
        Excel{'Range("ad10").Select'}
            Excel{'ActiveCell.Formula'} = 'Invoice Value Of Accessory Sales'
        Excel{'Range("ag10").Select'}
            Excel{'ActiveCell.Formula'} = 'Total Invoice Value Of All Sales'
        !-----------------------------------------------
        Excel{'Range("i10:k10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("l10:n10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("o10:q10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("r10:t10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("u10:w10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("x10:z10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("aa10:ac10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("ad10:af10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("ag10:ai10").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        !-----------------------------------------------
        Excel{'Range("A11").Select'}
        excel:ColumnName  = 'Sale Number'
        excel:ColumnWidth = 12
        DO SetColumn

        excel:ColumnName  = 'Account Number'
        excel:ColumnWidth = 20
        DO SetColumn

        excel:ColumnName  = 'Account Name'
        DO SetColumn

        excel:ColumnName  = 'Part Number'
        DO SetColumn

        excel:ColumnName  = 'Description'
        DO SetColumn

        excel:ColumnName  = 'Order Qty'
        excel:ColumnWidth = 10
        DO SetColumn

        excel:ColumnName  = 'Back Order Qty'
        DO SetColumn

        excel:ColumnName  = 'Invoice Qty'
        DO SetColumn

        Excel{'Range("i11").Select'}
        LOOP x# = 1 TO 9
            excel:ColumnName  = 'Cost'
            DO SetColumn

            excel:ColumnName  = 'Trade'
            DO SetColumn

            excel:ColumnName  = 'Retail'
            DO SetColumn

        END !LOOP
        !-----------------------------------------------
        Excel{'Range("A12").Select'}
        Excel{'ActiveWindow.FreezePanes'} = True
        !-----------------------------------------------
    EXIT
SetColumn                               ROUTINE ! 
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = CLIP(excel:ColumnName)
        Excel{'ActiveCell.ColumnWidth'} = excel:ColumnWidth
        !-----------------------------------------------
        DO XL_HorizontalAlignmentLeft

        DO XL_SetBorder
        DO XL_SetWrapText
        DO XL_SetBold

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
SetColumnsNext                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentLeft

        DO XL_SetWrapText
        DO XL_SetBold

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
SetColumn_SalesType ROUTINE ! Sales Type
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Sales Type'
        Excel{'ActiveCell.ColumnWidth'} = 10

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_PartType ROUTINE ! Part Type
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Part Type'
        Excel{'ActiveCell.ColumnWidth'} = 10

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_Supplier                      ROUTINE ! Supplier
    DATA
    CODE
        !-----------------------------------------------
        ! TradeAccountName
        Excel{'ActiveCell.Formula'}     = 'Supplier'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_Overdue            ROUTINE ! Overdue
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Overdue'
        Excel{'ActiveCell.ColumnWidth'} = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_OrderNumber                          ROUTINE ! Order Number
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Order Number'
        Excel{'ActiveCell.ColumnWidth'} = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_OrderDate                             ROUTINE ! Order Date
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Order Date'
        Excel{'ActiveCell.ColumnWidth'} = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_OrderedBy             ROUTINE ! Ordered By
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Ordered By'
        Excel{'ActiveCell.ColumnWidth'} = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_OrderStatus             ROUTINE ! Order Status
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Order Received' ! 'Order Status'
        Excel{'ActiveCell.ColumnWidth'} = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_PartNumber                 ROUTINE ! Part Number
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Part Number'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_Description                ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Description'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_ReceivedDate   ROUTINE ! Date Received        
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Received Date'
        Excel{'ActiveCell.ColumnWidth'} = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_DeliveryNoteNumber                            ROUTINE ! Delivery Note Number
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Delivery Note Number'
        Excel{'ActiveCell.ColumnWidth'} = 11.00

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_Quantity                ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Quantity'
        Excel{'ActiveCell.ColumnWidth'} = 11.00

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_PurchaseCost                    ROUTINE ! Purchase Cost
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = 'Purchase Cost'
        Excel{'ActiveCell.ColumnWidth'}  = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_LineCost                 ROUTINE ! Line Cost
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}      = 'Line Cost'
        Excel{'ActiveCell.ColumnWidth'}  = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_Type                ROUTINE ! Type
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Type'
        Excel{'ActiveCell.ColumnWidth'} = 11
        DO XL_SetColumn_Date

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_SupplyPeriod                          ROUTINE ! Supply Period
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Supply Period'
        Excel{'ActiveCell.ColumnWidth'} = 11
        DO XL_SetColumn_Date

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_DaysOver                      ROUTINE ! Days Over
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Days Over'
        Excel{'ActiveCell.ColumnWidth'} = 11
        DO XL_HorizontalAlignmentLeft

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_DaysSinceOrder                  ROUTINE ! Days Since Order
    DATA
    CODE
        !-----------------------------------------------
        ! IMEI
        Excel{'ActiveCell.Formula'}     = 'Days Since Order'
        Excel{'ActiveCell.ColumnWidth'} = 11

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_ESN                           ROUTINE ! IMEI
    DATA
    CODE
        !-----------------------------------------------
        ! IMEI
        Excel{'ActiveCell.Formula'}     = 'IMEI'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_EstimateStatus                    ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Estimate Status'
        Excel{'ActiveCell.ColumnWidth'} = 10

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_HeadAccountName                   ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        ! Trade Account
        Excel{'ActiveCell.Formula'}     = 'Head Account Name'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_HeadAccountNumber             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Head Account No.
        Excel{'ActiveCell.Formula'}     = 'Head Account Number'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_IncomingCourier               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Incoming Courier'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_JobFaultDescription                       ROUTINE ! Fault Description
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Fault Description'
        Excel{'ActiveCell.ColumnWidth'} = 15

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_JobNumber                     ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Job Number'
        Excel{'ActiveCell.ColumnWidth'} = 10

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_JobSkillLevel                       ROUTINE ! Job Skill Level
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Job Skill Level'
        Excel{'ActiveCell.ColumnWidth'} = 10

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_JobType                       ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Job Type'
        Excel{'ActiveCell.ColumnWidth'} = 16.86

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_Manufacturer                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Make
        Excel{'ActiveCell.Formula'}     = 'Make'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_ModelNumber                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Model Number'
        Excel{'ActiveCell.ColumnWidth'} = 15

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_MSN                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! MSN
        Excel{'ActiveCell.Formula'}     = 'MSN'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_RowType                       ROUTINE ! Current Job | Previous Job
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Type'
        Excel{'ActiveCell.ColumnWidth'} = 15

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_SubAccountName               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Sub Account Name
        Excel{'ActiveCell.Formula'}     = 'Account Name'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_SubAccountNumber             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Sub Account Name
        Excel{'ActiveCell.Formula'}     = 'Sub Account Number'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_TransitType                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! Initial Transit Type
        Excel{'ActiveCell.Formula'}     = 'Initial Transit Type'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_UserForename                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Engineer Forename'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_UserSurname                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Engineer Surname'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_UnitType                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Unit Type'
        Excel{'ActiveCell.ColumnWidth'} = 15

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_WarrantyChargeType            ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Warranty Charge Type'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_WarrantyRepairType              ROUTINE ! Set values for data sheet column header cell
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'Warranty Repair Type'
        Excel{'ActiveCell.ColumnWidth'} = 20

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
SetColumn_WhoBooked                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Formula'}     = 'User Code'
        Excel{'ActiveCell.ColumnWidth'} = 12

        DO SetColumnsNext
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteColumns                                                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
!        If ret:Payment_Method = 'EXC'
!            EXIT
!        End !If ret:Payment_Method = 'EXC'
        !===============================================
        RecordCount += 1

        DO WriteColumn_SaleNumber
        DO WriteColumn_AccountNumber
        DO WriteColumn_AccountName
        DO WriteColumn_PartNumber
        DO WriteColumn_Description 
        DO WriteColumn_OrderQty
        DO WriteColumn_BackOrderQty
        DO WriteColumn_InvoiceQty

        LOOP X# = 1 TO 3
            LOOP Y# = 1 TO 3
                LOOP Z# = 1 TO 3
                    !-----------------------------------
                    aq:AccountAccumulators[X#, Y#, Z#] += Accumulators[X#, Y#, Z#]
                    !-----------------------------------
                    DO XL_HorizontalAlignmentRight
                    Excel{'ActiveCell.NumberFormat'} = '#,##0.00'
                    Excel{'ActiveCell.Formula'}      = Accumulators[X#, Y#, Z#]

                    DO XL_ColRight
                    !-----------------------------------
                END !LOOP
            END !LOOP
        END !LOOP

        PUT(AccountQueue, +aq:AccountNumber, +aq:AccountName)
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
WriteColumn_SaleNumber ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ret:Ref_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_AccountNumber ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ret:Account_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_AccountName ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ret:Contact_Name

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_PartNumber ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = res:Part_Number

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Description  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = res:Description

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_PartType ROUTINE ! Part Type
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = LOC:PartType

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_SalesType ROUTINE ! Sales Type
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = LOC:SalesType

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_OrderQty ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = tally:QuantityOrder

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_BackOrderQty ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = tally:QuantityBackOrdered

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_InvoiceQty ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = tally:QuantityInvoiced

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Backorder_Accessory_Cost ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = tally:QuantityBackOrdered * tally:QuantityAccessory * res:Item_Cost

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Backorder_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Backorder_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Invoice_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Invoice_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Invoice_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Order_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Order_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Cost_Order_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Retail_Backorder_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Retail_Backorder_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Retail_Backorder_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Retail_Invoice_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Retail_Invoice_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Retail_Invoice_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Retail_Order_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Retail_Order_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Retail_Order_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Trade_Backorder_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Trade_Backorder_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Trade_Backorder_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Trade_Invoice_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Trade_Invoice_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Trade_Invoice_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Trade_Order_Accessory ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
WriteColumn_Trade_Order_Parts ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT

WriteColumn_Trade_Order_Total ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_HorizontalAlignmentRight
        Excel{'ActiveCell.Formula'}  = ''

        DO XL_ColRight
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
WriteSummary                                              ROUTINE
    DATA
QueueIndex   LONG
ResultsCount LONG
FirstRow     LONG
LastRow      LONG
    CODE
        !-----------------------------------------------------------------
        ! summary details
        !
        Excel{'Range("F8").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'} = RecordCount

        Excel{'Range("G8").Select'}
            DO XL_HorizontalAlignmentRight
            Excel{'ActiveCell.Formula'} = 'Showing'

        Excel{'Range("H8").Select'}
            DO XL_HorizontalAlignmentLeft
            Excel{'ActiveCell.Formula'}  = '=SUBTOTAL(2, a12:a' & RecordCount+11 & ')'

        Excel{'Range("A11").Select'}
            DO XL_DataAutoFilter
        !-----------------------------------------------------------------
        ! summary sheet details
        !
        Excel{'ActiveWorkBook.Sheets("Summary").Select'}
        !-----------------------------------------------------------------
        ! Totals
        !
        DO SetSummaryColumns
        FirstRow = Excel{'ActiveCell.Row'}  ! first blank line
        FirstRow += -1                      ! headers row
        LastRow   = FirstRow + RECORDS(AccountQueue)

        DO XL_ColFirst
        SORT(AccountQueue, +aq:AccountNumber, +aq:AccountName)

        LOOP E# = 1 TO RECORDS(AccountQueue)
            !-------------------------------------------------------------
            GET(AccountQueue, E#)
            Excel{'ActiveCell.Formula'}          = aq:AccountNumber

            DO XL_ColRight
                Excel{'ActiveCell.Formula'}      = aq:AccountName

            DO XL_ColRight
                Excel{'ActiveCell.NumberFormat'} = '#,##0'
                Excel{'ActiveCell.Formula'}      = aq:NumberOfOrders

            DO XL_ColRight
                Excel{'ActiveCell.NumberFormat'} = '#,##0'
                Excel{'ActiveCell.Formula'}      = aq:NumberOfLines
            !-------------------------------------------------------------
            LOOP X# = 1 TO 3
                LOOP Y# = 1 TO 3
                    LOOP Z# = 1 TO 3
                        !-----------------------------------
                        DO XL_ColRight

                        DO XL_HorizontalAlignmentRight
                        Excel{'ActiveCell.NumberFormat'} = '#,##0.00'
                        Excel{'ActiveCell.Formula'}      = aq:AccountAccumulators[X#, Y#, Z#]
                        !-----------------------------------
                    END !LOOP
                END !LOOP
            END !LOOP
            !-------------------------------------------------------------
            DO XL_ColFirst
            DO XL_RowDown
            !-------------------------------------------------------------
        END !LOOP

        Excel{'Range("a' & FirstRow & ':ae' & LastRow & '").Select'}
            Excel{'Selection.AutoFilter'}
        !-----------------------------------------------------------------
    EXIT
SetSummaryColumns                                                     ROUTINE
    DATA
CurrentRow LONG
    CODE
        !-----------------------------------------------
        DO XL_ColFirst
            DO XL_ColFirst
            CurrentRow = Excel{'ActiveCell.Row'}

        Excel{'Range("a' & CurrentRow & ':d' & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetBorder

        Excel{'Range("e' & CurrentRow & ':ae' & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetGrid
        !-----------------------------------------------
        Excel{'Range("e' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Order Value Of Spare Part Sales'
        Excel{'Range("h' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Order Value Of Accessory Sales'
        Excel{'Range("k' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Total Order Value Of All Sales'

        Excel{'Range("e' & CurrentRow & ':g' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("h' & CurrentRow & ':j' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("k' & CurrentRow & ':m' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        !-----------------------------------------------
        Excel{'Range("n' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Backorder  Value Of Spare Part Sales'
        Excel{'Range("q' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Backorder Value Of Accessory Sales'
        Excel{'Range("t' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Total Backorder Value Of All Sales'

        Excel{'Range("n' & CurrentRow & ':p' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("q' & CurrentRow & ':s' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("t' & CurrentRow & ':v' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        !-----------------------------------------------
        Excel{'Range("w' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Invoice Value Of Spare Part Sales'
        Excel{'Range("z' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Invoice Value Of Accessory Sales'
        Excel{'Range("ac' & CurrentRow & '").Select'}
            Excel{'ActiveCell.Formula'} = 'Total Invoice Value Of All Sales'

        Excel{'Range("w' & CurrentRow & ':y' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("z' & CurrentRow & ':ab' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        Excel{'Range("ac' & CurrentRow & ':ae' & CurrentRow & '").Select'}
            DO XL_SetBold
            DO XL_SetBorder
            DO XL_MergeCells
        !-----------------------------------------------
        DO XL_RowDown
            DO XL_ColFirst
            CurrentRow = Excel{'ActiveCell.Row'}

        Excel{'Range("A' & CurrentRow & ':ae' & CurrentRow & '").Select'}
            DO XL_SetTitle
            DO XL_SetGrid

        Excel{'Range("A' & CurrentRow & '").Select'}

        excel:ColumnName  = 'Account Number'
            excel:ColumnWidth = 20
            DO SetColumn

        excel:ColumnName  = 'Account Name'
            excel:ColumnWidth = 20
            DO SetColumn

        excel:ColumnName  = 'Number Of Orders'
            excel:ColumnWidth = 10
            DO SetColumn

        excel:ColumnName  = 'Number Of Lines'
            DO SetColumn
        !-----------------------------------------------
        Excel{'Range("e' & CurrentRow & '").Select'}
        LOOP x# = 1 TO 9
            excel:ColumnName  = 'Cost'
            DO SetColumn

            excel:ColumnName  = 'Trade'
            DO SetColumn

            excel:ColumnName  = 'Retail'
            DO SetColumn

        END !LOOP
        !-----------------------------------------------
        DO XL_ColFirst
        DO XL_RowDown
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
ProgressBar_Setup                       ROUTINE
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
            recordspercycle      = 25
            recordsprocessed     = 0
            recordstoprocess     = 10 !***The Number Of Records, or at least a guess***
            percentprogress      = 0
            progress:thermometer = 0

            thiswindow.reset(1) !This may error, if so, remove this line.
            open(progresswindow)
            progresswindow{PROP:Timer} = 1
         
            ?progress:userstring{prop:text} = 'Running...'
            ?progress:pcttext{prop:text}    = '0% Completed'
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
ProgressBar_LoopPre          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text)
        !RecordsToProcess = RECORDS(JOBS) !***The Number Of Records, or at least a guess***
        !***Place code before you run your routine. It opens the progress window and sets how many records will be processed ***
        !-----------------------------------------------
    EXIT
ProgressBar_Loop             ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        Result            = False
        !RecordsProcessed += 1

        Do ProgressBar_GetNextRecord2 !Necessary
        cancelcheck# += 1
        If cancelcheck# > (RecordsToProcess/100)
            Do ProgressBar_CancelCheck
            If tmp:cancel = 1
                CancelPressed = True
                POST(Event:CloseWindow)
            End!If tmp:cancel = 1
            cancelcheck# = 0
        End!If cancelcheck# > 50
        !-----------------------------------------------
        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        !IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        !
            Display()
        !END !IF
        !***Place the following code inside the loop. The first line updates the progress bar, and the rest allows the user to cancel the routine. This can be removed if necessary. ****
        !-----------------------------------------------
    EXIT
ProgressBar_LoopPost         ROUTINE
    DATA
newPctText STRING(255)
    CODE
        !-----------------------------------------------
        ?Progress:UserString{PROP:Text} = CLIP(Progress:Text) & ' Done(' & RecordsProcessed & '/' & RecordCount & ')'

        newPctText = 'Checked(' & RecordsProcessed & '), Found(' & RecordCount & ')'

        IF ?progress:pcttext{prop:text} <> newPctText
            ?progress:pcttext{prop:text} = newPctText
        END !IF
        !-----------------------------------------------
    EXIT
ProgressBar_Finalise                        ROUTINE
        !**** End Of Loop ***
            Do ProgressBar_EndPrintRun
            close(progresswindow)
        !**** End Of Loop ***
        ! when in report procedure->LocalResponse = RequestCompleted
ProgressBar_GetNextRecord2                  routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        !?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
ProgressBar_CancelCheck                     routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
                CancelPressed = True
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1
ProgressBar_EndPrintRun                     routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF window{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = 'ServiceBase2000'
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', 'ServiceBase 2000 DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case MessageEx('Attempting to use Workshop Performance Report<10,13>' & |
                '   without using ServiceBase 2000.<10,13>'                       & |
                '   Start ServiceBase 2000 and run the report from there.<10,13>',  |
                'ServiceBase 2000',                                                 |
                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
            Of 1 ! &OK Button
            END!Case MessageEx

           POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Case MessageEx('Unable to find your logged in user details.', |
                    'ServiceBase 2000',                                   |
                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                Of 1 ! &OK Button
            End!Case MessageEx

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
!tmpDate STRING(8)
    CODE
        !-----------------------------------------------
        ! Generate default file name
        !
        !-----------------------------------------------
        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
        !
        IF CLIP(LOC:FileName) = ''
            !tmpDate = LEFT(CLIP(FORMAT(TODAY(), @D12)))
            LOC:FileName = CLIP(LOC:ProgramName) & ' ' & FORMAT(TODAY(), @D12) & '.xls'
        END !IF
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

        OriginalPath = PATH()
            IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)

                LOC:Filename = ''
            END !IF
        SETPATH(OriginalPath)

        UPDATE()
        DISPLAY()
        !-----------------------------------------------
    EXIT
CheckFileName                           ROUTINE ! Does LOC:FileName exist ?
    DATA
DirQ QUEUE(FILE:Queue),PRE(fil)
    END
    CODE
        !-----------------------------------------------
        DIRECTORY(DirQ, LOC:FileName, ff_:NORMAL)
        IF RECORDS(DirQ)
            DISABLE(?OKButton)

            EXIT
        END !IF
        !-----------------------------------------------
        ENABLE(?OKButton)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
GetAccountFromQueue                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------------------------
        ! on entry LOC:HeadAccountNumber = Account Number to look up and
        !   store the details in the queue
        !-----------------------------------------------------------------
        IF CancelPressed
            EXIT
        END !IF
        !-----------------------------------------------------------------
        aq:AccountNumber = ret:Account_Number
        aq:AccountName    = ret:Contact_Name
        GET(AccountQueue, +aq:AccountNumber, +aq:AccountName)
        IF ERRORCODE()
            aq:AccountNumber  = ret:Account_Number
            aq:AccountName    = ret:Contact_Name

            aq:NumberOfOrders = 0
            aq:NumberOfLines  = 0

            LOOP X# = 1 TO 3
                LOOP Y# = 1 TO 3
                    LOOP Z# = 1 TO 3
                        aq:AccountAccumulators[X#, Y#, Z#] = 0
                    END !LOOP
                END !LOOP
            END !LOOP

            ADD(AccountQueue, +aq:AccountNumber, +aq:AccountName)
        END !IF ERRORCODE()
        !-----------------------------------------------------------------
    EXIT
!-----------------------------------------------
LoadSTOCK                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! 
        !
        Access:STOCK.ClearKey(sto:Ref_Number_Key)
        sto:Ref_Number = res:Part_Ref_Number

        IF Access:STOCK.TryFetch(sto:Ref_Number_Key)
            Result = False
        ELSE
            Result = True
        END !IF
        !-----------------------------------------------
    EXIT
LoadSubAccount                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = LOC:AccountNumber
        !--------------------------------------------------------------------------------------
        IF Access:SUBTRACC.Fetch(sub:Account_Number_Key)
            LOC:Account_Name = '<Not Found>'
        ELSE
            LOC:Account_Name = CLIP(sub:Company_Name)
        END !IF
        !-----------------------------------------------
    EXIT
LoadSUPPLIER                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! 
        !
        Access:SUPPLIER.ClearKey(sup:Company_Name_Key)
        sup:Company_Name = LOC:CompanyName

        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
            Result = False
        ELSE
            Result = True
        END !IF
        !-----------------------------------------------
    EXIT
LoadRETSTOCK                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        ! 
        !
        Access:RETSTOCK.ClearKey(res:Part_Number_Key)
        res:Part_Number = ret:Ref_Number

        IF Access:RETSTOCK.TryFetch(res:Part_Number_Key)
            Result = False
        ELSE
            Result = True
        END !IF
        !-----------------------------------------------
    EXIT
LoadUSERS                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        !
        Access:USERS.ClearKey(use:User_Code_Key)
        use:User_Code = LOC:UserCode

        IF Access:USERS.TryFetch(use:User_Code_Key)
            Result = False
        ELSE
            Result = True
        END !IF
        !-----------------------------------------------
    EXIT
!-----------------------------------------------

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('GetCriteria')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String2
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:RETSALES.Open
  Access:USERS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:TRDBATCH.UseFile
  Access:RETSTOCK.UseFile
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
      LOC:StartDate      = DATE( MONTH(TODAY()), 1, YEAR(TODAY()) )
      LOC:EndDate        = TODAY() !
      LOC:DayLeawayCount = 90
  
      LOC:ProgramName = 'Retail Sales Valuation Report'
      ?TitleString{PROP:Text} = CLIP(LOC:ProgramName) & ' Criteria'
  
      DO GetUserName
  Do RecolourWindow
  ?LOC:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  ! support for CPCS
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  ThisMakeover.SetWindow(Win:Window)
  mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,?Sheet1)
  window{prop:buffer} = 1
  SELF.SetAlerts()
    ThisMakeover.Refresh()
    ThisMakeover.Refresh()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:RETSALES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    OF ?OkButton
          DO ExportSetup
          DO ExportBody
          DO ExportFinalize
      
          POST(Event:CloseWindow)
    OF ?CancelButton
       POST(Event:CloseWindow)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?StartPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:StartDate = TINCALENDARStyle1(LOC:StartDate)
          Display(?LOC:StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?EndPopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          LOC:EndDate = TINCALENDARStyle1(LOC:EndDate)
          Display(?LOC:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    ThisMakeover.TakeEvent(Win:Window,mo:SelectedButton,mo:SelectedField)
    ThisMakeover.TakeEvent(Win:Window,mo:SelectedButton,mo:SelectedField)
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?LOC:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?EndPopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,Field())
      mo:SelectedTab::Sheet1 = ThisMakeover.SheetColor(Win:Window,mo:SelectedTab::Sheet1,Field())
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Toolbar.DisplayButtons PROCEDURE

  CODE
  PARENT.DisplayButtons
    ThisMakeover.Refresh()
    ThisMakeover.Refresh()

