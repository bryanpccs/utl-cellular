

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05020.INC'),ONCE        !Local module procedure declarations
                     END


ForceFaultDescription PROCEDURE  (func:Type)          ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Fault Description
    If (def:Force_Fault_Description = 'B' and func:Type = 'B') Or |
        (def:Force_Fault_Description <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Fault_Description = 'B'
    Return Level:Benign
