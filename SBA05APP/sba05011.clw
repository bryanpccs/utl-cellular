

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05011.INC'),ONCE        !Local module procedure declarations
                     END


ForceColour          PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Colour!
    If (def:ForceCommonFault = 'B' And func:Type = 'B') Or |
        (def:ForceCommonFault <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Unit_Type = 'B'
    Return Level:Benign
