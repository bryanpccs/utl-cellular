

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05005.INC'),ONCE        !Local module procedure declarations
                     END


ForceTransitType     PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    !Initial Transit Type
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    If (def:Force_Initial_Transit_Type ='B' and func:Type = 'B') Or |
        (def:Force_Initial_Transit_Type <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End
    Return Level:Benign
