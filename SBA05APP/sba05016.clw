

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05016.INC'),ONCE        !Local module procedure declarations
                     END


ForceCustomerName    PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Customer Name
    If (def:Customer_Name = 'B' and func:Type = 'B') Or |
         (def:Customer_Name <> 'I' and func:Type = 'C')
         If CustomerNameRequired(func:AccountNumber) = Level:Benign
            Return Level:Fatal
         End!If CustomerNameRequired(job:Account_Number) = Level:Benign
    End!If def:Customer_Name = 'B'
    Return Level:Benign
