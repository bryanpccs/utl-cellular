

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05015.INC'),ONCE        !Local module procedure declarations
                     END


ForceOrderNumber     PROCEDURE  (func:AccountNumber,func:Type) ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Order Number
    Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
    sub:Account_Number  = func:AccountNumber
    If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Found
        Access:TRADEACC.Clearkey(tra:Account_Number_Key)
        tra:Account_Number  = sub:Main_Account_Number
        If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Found
            If tra:ForceOrderNumber
                Return Level:Fatal
            End !If tra:ForceOrderNumber
        Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
    Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
    ! Start Change 3960 BE(02/03/04)
    IF (DEF:Order_Number = 'B' And func:Type = 'B') OR |
        (DEF:Order_Number <> 'I' And func:Type = 'C')
        RETURN Level:Fatal
    END
    ! End Change 3960 BE(02/03/04)
    Return Level:Benign
