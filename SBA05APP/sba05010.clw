

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05010.INC'),ONCE        !Local module procedure declarations
                     END


ForceMobileNumber    PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Force Mobile Number
    If (def:Force_Mobile_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Mobile_Number <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Mobile_Number = 'B'
    Return Level:Benign
