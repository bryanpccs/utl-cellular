

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05009.INC'),ONCE        !Local module procedure declarations
                     END


ForceUnitType        PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Unit Type
    If (def:Force_Unit_Type = 'B' And func:Type = 'B') Or |
        (def:Force_Unit_Type <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Unit_Type = 'B'
    Return Level:Benign
