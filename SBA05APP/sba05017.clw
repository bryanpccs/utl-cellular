

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05017.INC'),ONCE        !Local module procedure declarations
                     END


ForceIncomingCourier PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Incoming Courier
    If (def:Force_Incoming_Courier = 'B' And func:Type = 'B') Or |
        (def:Force_Incoming_Courier <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Incoming_Courier = 'B'
    Return Level:Benign
