

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05022.INC'),ONCE        !Local module procedure declarations
                     END


ForceRepairType      PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Repair Type
    If (def:Force_Repair_Type = 'B' And func:Type = 'B') Or |
        (def:Force_Repair_Type <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Repair_Type = 'B'
    Return Level:Benign
