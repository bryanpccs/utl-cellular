

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05008.INC'),ONCE        !Local module procedure declarations
                     END


ForceModelNumber     PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Model Number
    If (def:Force_Model_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Model_Number <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Model_Number = 'B'
    Return Level:Benign
