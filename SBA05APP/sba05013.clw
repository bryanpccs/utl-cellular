

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05013.INC'),ONCE        !Local module procedure declarations
                     END


ForceLocation        PROCEDURE  (func:TransitType,func:Workshop,func:Type) ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Location
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        If trt:force_location = 'YES' and func:Workshop = 'YES'
            Return Level:Fatal
        End!If trt:force_location = 'YES' and job:location = ''
    end
    Return Level:Benign
