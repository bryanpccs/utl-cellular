

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05019.INC'),ONCE        !Local module procedure declarations
                     END


ForceDeliveryPostcode PROCEDURE  (func:Type)          ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Delivery Postcode
    If (def:ForceDelPostcode = 'B' And func:Type = 'B') Or |
        (def:ForceDelPostcode <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:ForceDelPostcode = 'B'
    Return Level:Benign
