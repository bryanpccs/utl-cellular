

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05014.INC'),ONCE        !Local module procedure declarations
                     END


ForceAuthorityNumber PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Authority Number
    If (def:Force_Authority_Number = 'B' And func:Type = 'B') Or |
        (def:Force_Authority_Number <> 'I' and func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_Authority_Number = 'B'
    Return Level:Benign
