

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05024.INC'),ONCE        !Local module procedure declarations
                     END


ForceNetwork         PROCEDURE  (func:Type)           ! Declare Procedure
  CODE
    !Network
    If (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI')= 'B' And func:Type = 'B') Or |
        (GETINI('COMPULSORY','Network',,CLIP(PATH())&'\SB2KDEF.INI') <> 'I' And func:Type = 'C')
        Return Level:Fatal
    End!If def:Force_ESN = 'B'
    Return Level:Benign
