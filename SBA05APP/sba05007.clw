

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05007.INC'),ONCE        !Local module procedure declarations
                     END


ForceMSN             PROCEDURE  (func:Manufacturer,func:Workshop,func:Type) ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !MSN
    If (def:Force_MSN = 'B' And func:Type = 'B') Or |
        (def:Force_MSN <> 'I' And func:Type = 'C')
        ! Start Change 2941 BE(06/11/03)
        !If MSNRequired(func:Manufacturer) = Level:Benign
        !    Return Level:Fatal
        !End!If MSNRequired(job:Manufacturer) = Level:Benign
        IF (func:Workshop <> 'YES' ) THEN
            DisableMsnCheck# = GETINI('VALIDATE','DisableMSNIfNotInWorkshop',0,CLIP(PATH())&'\SB2KDEF.INI')
            IF ((~DisableMsnCheck#) AND (MSNRequired(func:Manufacturer) = Level:Benign))THEN
                RETURN Level:Fatal
            END
        ELSE
            IF (MSNRequired(func:Manufacturer) = Level:Benign) THEN
                RETURN Level:Fatal
            END
        END
        ! Start Change 2941 BE(06/11/03)
    End!If def:Force_MSN = 'B'
    Return Level:Benign
