

   MEMBER('sba05app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA05012.INC'),ONCE        !Local module procedure declarations
                     END


ForceDOP             PROCEDURE  (func:TransitType,func:Manufacturer,func:WarrantyJob,func:Type) ! Declare Procedure
  CODE
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    !Date Of Purchase
    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = func:TransitType
    if access:trantype.fetch(trt:transit_type_key) = Level:Benign
        error# = 0
        If trt:force_dop = 'YES' And func:WarrantyJob = 'YES'
            Return Level:Fatal
        End !If trt:force_dop = 'YES' 
    end
    !Only check this if it hasn't already been forced by the Transit Type
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = func:Manufacturer
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        IF man:DOPCompulsory And func:WarrantyJob = 'YES'
            Return Level:Fatal
        End !IF man:DOPCompulsory and job:dop = '' and func:Type = 'C'
    Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
    Return Level:Benign
