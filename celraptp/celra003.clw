

   MEMBER('celraptp.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA003.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTRDBATCH PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?trb:Company_Name
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
History::trb:Record  LIKE(trb:RECORD),STATIC
QuickWindow          WINDOW('Update The Batch File'),AT(,,220,179),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('UpdateTRDBATCH'),SYSTEM,GRAY,DOUBLE,MDI,IMM
                       SHEET,AT(4,4,212,144),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Batch Number:'),AT(8,20),USE(?TRB:Batch_Number:Prompt)
                           ENTRY(@n-14),AT(84,20,64,10),USE(trb:Batch_Number),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('3rd Party Agent'),AT(8,36),USE(?TRB:Batch_Number:Prompt:2)
                           COMBO(@s30),AT(84,36,124,10),USE(trb:Company_Name),IMM,FONT(,,,FONT:bold),FORMAT('120L(2)|M@s30@'),DROP(5),FROM(Queue:FileDropCombo)
                           PROMPT('Job Number'),AT(8,52),USE(?TRB:Ref_Number:Prompt)
                           ENTRY(@s8),AT(84,52,64,10),USE(trb:Ref_Number),SKIP,LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR,READONLY
                           PROMPT('I.M.E.I. Number'),AT(8,68),USE(?TRB:ESN:Prompt),TRN
                           ENTRY(@s16),AT(84,68,124,10),USE(trb:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('M.S.N.'),AT(8,84),USE(?trb:MSN:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(trb:MSN),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('M.S.N.'),TIP('M.S.N.'),UPR
                           PROMPT('Status'),AT(8,100),USE(?TRB:ESN:Prompt:2),TRN
                           PROMPT('Return Date'),AT(8,132),USE(?TRB:Date:Prompt)
                           ENTRY(@d6b),AT(84,132,64,10),USE(trb:DateReturn),FONT(,,,FONT:bold),MSG('Date The Unit Was Returned')
                           BUTTON,AT(152,132,10,10),USE(?LookupDate),SKIP,ICON('Calenda2.ico')
                           OPTION('Status'),AT(84,96,124,28),USE(trb:Status),BOXED,MSG('Out or In')
                             RADIO('Out'),AT(104,108),USE(?Option1:Radio1),VALUE('OUT')
                             RADIO('In'),AT(168,108),USE(?Option1:Radio2),VALUE('IN')
                           END
                         END
                       END
                       BUTTON('&OK'),AT(100,156,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,156,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,152,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?TRB:Batch_Number:Prompt{prop:FontColor} = -1
    ?TRB:Batch_Number:Prompt{prop:Color} = 15066597
    If ?trb:Batch_Number{prop:ReadOnly} = True
        ?trb:Batch_Number{prop:FontColor} = 65793
        ?trb:Batch_Number{prop:Color} = 15066597
    Elsif ?trb:Batch_Number{prop:Req} = True
        ?trb:Batch_Number{prop:FontColor} = 65793
        ?trb:Batch_Number{prop:Color} = 8454143
    Else ! If ?trb:Batch_Number{prop:Req} = True
        ?trb:Batch_Number{prop:FontColor} = 65793
        ?trb:Batch_Number{prop:Color} = 16777215
    End ! If ?trb:Batch_Number{prop:Req} = True
    ?trb:Batch_Number{prop:Trn} = 0
    ?trb:Batch_Number{prop:FontStyle} = font:Bold
    ?TRB:Batch_Number:Prompt:2{prop:FontColor} = -1
    ?TRB:Batch_Number:Prompt:2{prop:Color} = 15066597
    If ?trb:Company_Name{prop:ReadOnly} = True
        ?trb:Company_Name{prop:FontColor} = 65793
        ?trb:Company_Name{prop:Color} = 15066597
    Elsif ?trb:Company_Name{prop:Req} = True
        ?trb:Company_Name{prop:FontColor} = 65793
        ?trb:Company_Name{prop:Color} = 8454143
    Else ! If ?trb:Company_Name{prop:Req} = True
        ?trb:Company_Name{prop:FontColor} = 65793
        ?trb:Company_Name{prop:Color} = 16777215
    End ! If ?trb:Company_Name{prop:Req} = True
    ?trb:Company_Name{prop:Trn} = 0
    ?trb:Company_Name{prop:FontStyle} = font:Bold
    ?TRB:Ref_Number:Prompt{prop:FontColor} = -1
    ?TRB:Ref_Number:Prompt{prop:Color} = 15066597
    If ?trb:Ref_Number{prop:ReadOnly} = True
        ?trb:Ref_Number{prop:FontColor} = 65793
        ?trb:Ref_Number{prop:Color} = 15066597
    Elsif ?trb:Ref_Number{prop:Req} = True
        ?trb:Ref_Number{prop:FontColor} = 65793
        ?trb:Ref_Number{prop:Color} = 8454143
    Else ! If ?trb:Ref_Number{prop:Req} = True
        ?trb:Ref_Number{prop:FontColor} = 65793
        ?trb:Ref_Number{prop:Color} = 16777215
    End ! If ?trb:Ref_Number{prop:Req} = True
    ?trb:Ref_Number{prop:Trn} = 0
    ?trb:Ref_Number{prop:FontStyle} = font:Bold
    ?TRB:ESN:Prompt{prop:FontColor} = -1
    ?TRB:ESN:Prompt{prop:Color} = 15066597
    If ?trb:ESN{prop:ReadOnly} = True
        ?trb:ESN{prop:FontColor} = 65793
        ?trb:ESN{prop:Color} = 15066597
    Elsif ?trb:ESN{prop:Req} = True
        ?trb:ESN{prop:FontColor} = 65793
        ?trb:ESN{prop:Color} = 8454143
    Else ! If ?trb:ESN{prop:Req} = True
        ?trb:ESN{prop:FontColor} = 65793
        ?trb:ESN{prop:Color} = 16777215
    End ! If ?trb:ESN{prop:Req} = True
    ?trb:ESN{prop:Trn} = 0
    ?trb:ESN{prop:FontStyle} = font:Bold
    ?trb:MSN:Prompt{prop:FontColor} = -1
    ?trb:MSN:Prompt{prop:Color} = 15066597
    If ?trb:MSN{prop:ReadOnly} = True
        ?trb:MSN{prop:FontColor} = 65793
        ?trb:MSN{prop:Color} = 15066597
    Elsif ?trb:MSN{prop:Req} = True
        ?trb:MSN{prop:FontColor} = 65793
        ?trb:MSN{prop:Color} = 8454143
    Else ! If ?trb:MSN{prop:Req} = True
        ?trb:MSN{prop:FontColor} = 65793
        ?trb:MSN{prop:Color} = 16777215
    End ! If ?trb:MSN{prop:Req} = True
    ?trb:MSN{prop:Trn} = 0
    ?trb:MSN{prop:FontStyle} = font:Bold
    ?TRB:ESN:Prompt:2{prop:FontColor} = -1
    ?TRB:ESN:Prompt:2{prop:Color} = 15066597
    ?TRB:Date:Prompt{prop:FontColor} = -1
    ?TRB:Date:Prompt{prop:Color} = 15066597
    If ?trb:DateReturn{prop:ReadOnly} = True
        ?trb:DateReturn{prop:FontColor} = 65793
        ?trb:DateReturn{prop:Color} = 15066597
    Elsif ?trb:DateReturn{prop:Req} = True
        ?trb:DateReturn{prop:FontColor} = 65793
        ?trb:DateReturn{prop:Color} = 8454143
    Else ! If ?trb:DateReturn{prop:Req} = True
        ?trb:DateReturn{prop:FontColor} = 65793
        ?trb:DateReturn{prop:Color} = 16777215
    End ! If ?trb:DateReturn{prop:Req} = True
    ?trb:DateReturn{prop:Trn} = 0
    ?trb:DateReturn{prop:FontStyle} = font:Bold
    ?trb:Status{prop:Font,3} = -1
    ?trb:Status{prop:Color} = 15066597
    ?trb:Status{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTRDBATCH',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTRDBATCH',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTRDBATCH',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRB:Batch_Number:Prompt;  SolaceCtrlName = '?TRB:Batch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:Batch_Number;  SolaceCtrlName = '?trb:Batch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRB:Batch_Number:Prompt:2;  SolaceCtrlName = '?TRB:Batch_Number:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:Company_Name;  SolaceCtrlName = '?trb:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRB:Ref_Number:Prompt;  SolaceCtrlName = '?TRB:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:Ref_Number;  SolaceCtrlName = '?trb:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRB:ESN:Prompt;  SolaceCtrlName = '?TRB:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:ESN;  SolaceCtrlName = '?trb:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:MSN:Prompt;  SolaceCtrlName = '?trb:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:MSN;  SolaceCtrlName = '?trb:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRB:ESN:Prompt:2;  SolaceCtrlName = '?TRB:ESN:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRB:Date:Prompt;  SolaceCtrlName = '?TRB:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:DateReturn;  SolaceCtrlName = '?trb:DateReturn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupDate;  SolaceCtrlName = '?LookupDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:Status;  SolaceCtrlName = '?trb:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Insert a 3rd Party Batch'
  OF ChangeRecord
    ActionMessage = 'Changing A 3rd Party Batch'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateTRDBATCH')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTRDBATCH')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRB:Batch_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(trb:Record,History::trb:Record)
  SELF.AddHistoryField(?trb:Batch_Number,2)
  SELF.AddHistoryField(?trb:Company_Name,3)
  SELF.AddHistoryField(?trb:Ref_Number,4)
  SELF.AddHistoryField(?trb:ESN,5)
  SELF.AddHistoryField(?trb:MSN,14)
  SELF.AddHistoryField(?trb:DateReturn,11)
  SELF.AddHistoryField(?trb:Status,6)
  SELF.AddUpdateFile(Access:TRDBATCH)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Access:JOBTHIRD.UseFile
  Access:MANUFACT.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRDBATCH
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  !Msn?
  Access:JOBS.ClearKey(job:Ref_Number_Key)
  job:Ref_Number = trb:Ref_Number
  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Found
      Access:MANUFACT.ClearKey(man:Manufacturer_Key)
      man:Manufacturer = job:Manufacturer
      If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If man:Use_MSN = 'YES'
              ?trb:MSN{Prop:Hide} = 0
              ?trb:MSN:Prompt{Prop:Hide}=0
          End !If man:Use_MSN = 'YES'
      Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
  Else!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
  Do RecolourWindow
  ?TRB:DateReturn{Prop:Alrt,255} = MouseLeft2
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.AddItem(ToolbarForm)
  FDCB8.Init(trb:Company_Name,?trb:Company_Name,Queue:FileDropCombo.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo
  FDCB8.AddSortOrder(trd:Company_Name_Key)
  FDCB8.AddField(trd:Company_Name,FDCB8.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTRDBATCH',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?trb:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
      IF LEN(CLIP(trb:ESN)) = 18
        !Ericsson IMEI!
        trb:ESN = SUB(trb:ESN,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
    OF ?LookupDate
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          trb:DateReturn = TINCALENDARStyle1(trb:DateReturn)
          Display(?TRB:DateReturn)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
  Access:JobThird.ClearKey(jot:ThirdPartyKey)
  jot:ThirdPartyNumber = trb:RecordNumber
  IF Access:JobThird.Fetch(jot:ThirdPartyKey)
    !Error!
  ELSE
    IF trb:Status = 'IN'
      jot:DateIn = trb:DateReturn
      jot:InIMEI = trb:ESN
    ELSIF trb:Status = 'OUT'
      jot:OutIMEI = trb:ESN
      jot:DateOut = trb:DateReturn
    END
    Access:JobThird.Update()
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTRDBATCH')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?trb:DateReturn
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?LookupDate)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

