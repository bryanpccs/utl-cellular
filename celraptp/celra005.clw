

   MEMBER('celraptp.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA008.INC'),ONCE        !Req'd for module callout resolution
                     END


Return_Units PROCEDURE                                !Generated from procedure template - Window

ESN_temp             STRING(30)
tmp:IMEIChanged      BYTE(0)
tmp:MSNChanged       BYTE(0)
save_aud_id          USHORT,AUTO
tmp:JobQueue         QUEUE,PRE(jobque)
JobNumber            LONG
Exchanged            STRING(3)
RecordNumber         LONG
                     END
tmp:JobNumber        LONG
tmp:ErrorText        STRING(60)
tmp:location         STRING(30)
tmp:status           STRING(30)
tmp:MSN              STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Returning 3rd Party Repairs'),AT(,,355,155),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),ALRT(F10Key),GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,220,120),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Returns'),USE(?Tab1)
                           PROMPT('Insert the Job Number and  I.M.E.I. Number of the Unit being Retured.'),AT(8,8,200,20),USE(?Prompt3),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Status'),AT(8,32),USE(?tmp:status:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,32,124,10),USE(tmp:status),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Status'),TIP('Status'),UPR
                           BUTTON,AT(212,32,10,10),USE(?LookupStatus),SKIP,ICON('List3.ico')
                           PROMPT('Location'),AT(8,48),USE(?tmp:location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,48,124,10),USE(tmp:location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(MouseLeft2),ALRT(EnterKey),UPR
                           BUTTON,AT(212,48,10,10),USE(?LookupLocation),SKIP,ICON('list3.ico')
                           PROMPT('Job Number'),AT(8,64),USE(?tmp:JobNumber:Prompt)
                           ENTRY(@s8),AT(84,64,64,10),USE(tmp:JobNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('I.M.E.I. Number'),AT(8,80),USE(?ESN:Prompt),TRN
                           ENTRY(@s30),AT(84,80,124,10),USE(ESN_temp),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('M.S.N.'),AT(8,96),USE(?tmp:MSN:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,96,124,10),USE(tmp:MSN),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('M.S.N.'),TIP('M.S.N.'),UPR
                           STRING(@s60),AT(8,112,200,12),USE(tmp:ErrorText),FONT(,10,COLOR:Navy,FONT:bold)
                         END
                       END
                       SHEET,AT(228,4,124,120),USE(?Sheet2),SPREAD
                         TAB('Jobs Updated Succesfully'),USE(?Tab2)
                           LIST,AT(232,20,116,100),USE(?List1),VSCROLL,FORMAT('56L(2)|M~Job Number~@s8@12L(2)|M~Exchanged~@s3@'),FROM(tmp:JobQueue)
                         END
                       END
                       PANEL,AT(4,128,348,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Finish [F10]'),AT(292,132,56,16),USE(?Cancel),LEFT,ICON('thumbs.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:tmp:status                Like(tmp:status)
look:tmp:location                Like(tmp:location)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?tmp:status:Prompt{prop:FontColor} = -1
    ?tmp:status:Prompt{prop:Color} = 15066597
    If ?tmp:status{prop:ReadOnly} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 15066597
    Elsif ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 8454143
    Else ! If ?tmp:status{prop:Req} = True
        ?tmp:status{prop:FontColor} = 65793
        ?tmp:status{prop:Color} = 16777215
    End ! If ?tmp:status{prop:Req} = True
    ?tmp:status{prop:Trn} = 0
    ?tmp:status{prop:FontStyle} = font:Bold
    ?tmp:location:Prompt{prop:FontColor} = -1
    ?tmp:location:Prompt{prop:Color} = 15066597
    If ?tmp:location{prop:ReadOnly} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 15066597
    Elsif ?tmp:location{prop:Req} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 8454143
    Else ! If ?tmp:location{prop:Req} = True
        ?tmp:location{prop:FontColor} = 65793
        ?tmp:location{prop:Color} = 16777215
    End ! If ?tmp:location{prop:Req} = True
    ?tmp:location{prop:Trn} = 0
    ?tmp:location{prop:FontStyle} = font:Bold
    ?tmp:JobNumber:Prompt{prop:FontColor} = -1
    ?tmp:JobNumber:Prompt{prop:Color} = 15066597
    If ?tmp:JobNumber{prop:ReadOnly} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 15066597
    Elsif ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 8454143
    Else ! If ?tmp:JobNumber{prop:Req} = True
        ?tmp:JobNumber{prop:FontColor} = 65793
        ?tmp:JobNumber{prop:Color} = 16777215
    End ! If ?tmp:JobNumber{prop:Req} = True
    ?tmp:JobNumber{prop:Trn} = 0
    ?tmp:JobNumber{prop:FontStyle} = font:Bold
    ?ESN:Prompt{prop:FontColor} = -1
    ?ESN:Prompt{prop:Color} = 15066597
    If ?ESN_temp{prop:ReadOnly} = True
        ?ESN_temp{prop:FontColor} = 65793
        ?ESN_temp{prop:Color} = 15066597
    Elsif ?ESN_temp{prop:Req} = True
        ?ESN_temp{prop:FontColor} = 65793
        ?ESN_temp{prop:Color} = 8454143
    Else ! If ?ESN_temp{prop:Req} = True
        ?ESN_temp{prop:FontColor} = 65793
        ?ESN_temp{prop:Color} = 16777215
    End ! If ?ESN_temp{prop:Req} = True
    ?ESN_temp{prop:Trn} = 0
    ?ESN_temp{prop:FontStyle} = font:Bold
    ?tmp:MSN:Prompt{prop:FontColor} = -1
    ?tmp:MSN:Prompt{prop:Color} = 15066597
    If ?tmp:MSN{prop:ReadOnly} = True
        ?tmp:MSN{prop:FontColor} = 65793
        ?tmp:MSN{prop:Color} = 15066597
    Elsif ?tmp:MSN{prop:Req} = True
        ?tmp:MSN{prop:FontColor} = 65793
        ?tmp:MSN{prop:Color} = 8454143
    Else ! If ?tmp:MSN{prop:Req} = True
        ?tmp:MSN{prop:FontColor} = 65793
        ?tmp:MSN{prop:Color} = 16777215
    End ! If ?tmp:MSN{prop:Req} = True
    ?tmp:MSN{prop:Trn} = 0
    ?tmp:MSN{prop:FontStyle} = font:Bold
    ?tmp:ErrorText{prop:FontColor} = -1
    ?tmp:ErrorText{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
FinalRoutine        Routine
    error# = 0
    access:trdbatch.clearkey(trb:jobstatuskey)
    trb:status     = 'OUT'
    trb:ref_number = tmp:JobNumber
    if access:trdbatch.tryfetch(trb:jobstatuskey)
        Case MessageEx('Error! The selected Job has not been despatched to a Third Party Agent.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        error# = 1
        Do fail_update
    Else!if access:trdbatch.tryfetch(trb:jobstatuskey)
        esn_change# = 0
        msn_change# = 0
        tmp:IMEIChanged = 0
        tmp:MSNCHanged  = 0

        If ESN_Temp <> trb:ESN
            tmp:IMEIChanged = 1
        End !If ESN_Temp <> trb:ESN

        If ?tmp:MSN{prop:hide} = 0
            If tmp:MSN <> trb:MSN
                tmp:MSNChanged = 1
            End !If tmp:MSN <> trb:MSN
        End !If ?tmp:MSN{prop:hide} = 0

        If tmp:IMEIChanged And tmp:MSNChanged
            Case MessageEx('The I.M.E.I. Number AND the M.S.N. are different to the unit that was initial despatched.'&|
              '<13,10>'&|
              '<13,10>Do you wish to change the I.M.E.I. Number and M.S.N., or re-enter the fields?','ServiceBase 2000',|
                           'Styles\warn.ico','|&Change|&Re-Enter',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                Of 1 ! &Change Button
                Of 2 ! &Re-Enter Button
                    Do Fail_Update
                    Error# = 1
                    ESN_Temp = ''
                    tmp:MSN = ''
                    Select(?ESN_Temp)
            End!Case MessageEx
        End !If tmp:IMEICHanged And tmp:MSNChanged

        If tmp:IMEIChanged AND ~tmp:MSNChanged
            Case MessageEx('The I.M.E.I. number of the unit is different to the unit that was initial despatched. But, note, the M.S.N. is the same.'&|
              '<13,10>'&|
              '<13,10>Is this correct? Do you want to change the I.M.E.I. Number or re-enter it?','ServiceBase 2000',|
                           'Styles\warn.ico','|&Change|&Re-Enter',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                Of 1 ! &Change Button
                Of 2 ! &Re-Enter Button
                    Do Fail_Update
                    Error# = 1
                    ESN_Temp = ''
                    Select(?ESN_Temp)
                    ?tmp:MSN{Prop:Touched} = 1
            End!Case MessageEx
        End !If tmp:IMEIChanged AND ~tmp:MSNChanged

        If ~tmp:IMEIChanged And tmp:MSNChanged
            Case MessageEx('The M.S.N. of the unit is different to the unit that was initial despatched. But, note, the I.M.E.I. Number is the same.'&|
              '<13,10>'&|
              '<13,10>Is this correct? Do you want to change the M.S.N. or re-enter it?','ServiceBase 2000',|
                           'Styles\warn.ico','|&Change|&Re-Enter',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                Of 1 ! &Change Button
                Of 2 ! &Re-Enter Button
                    Do Fail_Update
                    Error# = 1
                    tmp:MSN = ''
                    Select(?tmp:MSN)
            End!Case MessageEx
        End !If ~tmp:IMEIChanged And tmp:MSNChanged

        If error# = 0
            access:locinter.clearkey(loi:location_key)
            loi:location    = tmp:location
            If access:locinter.tryfetch(loi:location_key) = Level:Benign and tmp:location <> ''
                If loi:allocate_spaces = 'YES'
                    If loi:current_spaces < 1
                        Case MessageEx('There are no available locations. Please pick another.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        error# = 1
                        Do fail_update
                        Select(?tmp:location)
                    End!If loi:current_spaces < 1
                End!If loi:allocate_spaces = 'YES'
            End!If access:locinter(loi:location_key) = Level:Benign
        End!If error# = 0

        IF error# = 0
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number  = trb:ref_number
            If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                !Exchange Unit attached, or there will be an exchange unit at some time
                No_Audit# = 0
                ExchangeFound# = 0
                If trb:Exchanged = 'YES' and job:Exchange_Unit_Number = ''
                    !An Exchange Unit hasn't been attached yet, but
                    !should be at some point. There should already be an entry in the
                    !Exchange Stock for this. Now I've got to try and find it.
                    Access:EXCHANGE_ALIAS.ClearKey(xch_ali:ESN_Only_Key)
                    xch_ali:ESN = job:ESN
                    If Access:EXCHANGE_ALIAS.TryFetch(xch_ali:ESN_Only_Key) = Level:Benign
                        ExchangeFound# = 1
                        If xch_ali:audit_number <> ''
                            no_audit# = 0
                        Else!If xch_ali:audit_number <> ''
                            !Get Exchange Unit and mark as 'Return To Stock'
                            no_audit# = 1
                        End!If xch_ali:audit_number <> ''
                    End !If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                End !If trb:Exchanged = 'YES' and job:Exchange_Unit_Number = ''
                If (job:exchange_unit_number <> '' Or trb:exchanged = 'YES') And ExchangeFound# = 0
                    !Get Exchange Unit to see if it has an audit number attached
                    print_label# = 0
                    access:exchange_alias.clearkey(xch_ali:ref_number_key)
                    xch_ali:ref_number = job:exchange_unit_number
                    if access:exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign and job:exchange_unit_number <> ''
                        ExchangeFound# = 1
                        If xch_ali:audit_number <> ''
                            no_audit# = 0
                        Else!If xch_ali:audit_number <> ''
                            !Get Exchange Unit and mark as 'Return To Stock'
                            no_audit# = 1
                        End!If xch_ali:audit_number <> ''
                    Else!if access:exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
                        no_audit# = 1
                    End!if access:exchange_alias.tryfetch(xch_ali:ref_number_key) = Level:Benign
                End!If job:exchange_unit_number <> '' or trb:exchanged = 'YES'
                If ExchangeFound# = 1
                    If no_audit# = 0
                        access:excaudit.clearkey(exa:audit_number_key)
                        exa:stock_type  = xch_ali:stock_type
                        exa:audit_number    = xch_ali:audit_number
                        If access:excaudit.tryfetch(exa:audit_number_key) = Level:benign
                            !Remove Stock Unit From Audit Number
                            access:exchange.clearkey(xch:ref_number_key)
                            xch:ref_number  = exa:stock_unit_number
                            If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                                get(exchhist,0)
                                if access:exchhist.primerecord() = level:benign
                                    exh:ref_number      = xch:ref_number
                                    exh:date            = today()
                                    exh:time            = clock()
                                    access:users.clearkey(use:password_key)
                                    use:password        = glo:password
                                    access:users.fetch(use:password_key)
                                    exh:user = use:user_code
                                    exh:status          = 'REMOVED FROM AUDIT NUMBER: ' & Clip(xch:audit_number)
                                    access:exchhist.insert()
                                end!if access:exchhist.primerecord() = level:benign
                                xch:audit_number    = ''
                                access:exchange.update()
                            End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                            !Get Replacement Unit
                            access:exchange.clearkey(xch:ref_number_key)
                            xch:ref_number  = exa:replacement_unit_number
                            If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                                get(exchhist,0)
                                if access:exchhist.primerecord() = level:benign
                                    exh:ref_number      = xch:ref_number
                                    exh:date            = today()
                                    exh:time            = clock()
                                    access:users.clearkey(use:password_key)
                                    use:password        = glo:password
                                    access:users.fetch(use:password_key)
                                    exh:user = use:user_code
                                    exh:status          = 'UNIT RETURNED. (AUDIT:' & |
                                                            Clip(exa:audit_number)
                                    If tmp:IMEIChanged
                                        exh:status  = CLip(exh:status) & ' OLD I.M.E.I.: ' & Clip(xch:esn)
                                    End !If tmp:ESNChanged
                                    If tmp:MSNChanged
                                        exh:Status  = Clip(exh:Status) & ' OLD M.S.N.: ' & Clip(xch:MSN)
                                    End !If tmp:MSNChanged
                                    access:exchhist.insert()
                                end!if access:exchhist.primerecord() = level:benign
                                xch:audit_number    = exa:audit_number
                                xch:available       = 'RTS'
                                glo:select1         = xch:Ref_number
                                IF tmp:IMEIChanged
                                    xch:ESN         = ESN_Temp
                                End !IF tmp:IMEIChanged
                                If tmp:MSNChanged
                                    xch:MSN         = tmp:MSN
                                End !If tmp:MSNChanged
                                access:exchange.update()
                                print_label# = 1
                            End!If access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
                        End!If access:excaudit.tryfetch(exa:audit_number_key) = Level:benign
                    Else!If no_audit# = 0
                        !Get The Incoming Customer Unit and mark
                        Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                        xch:ESN = job:esn
                        If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:benign
                            get(exchhist,0)
                            if access:exchhist.primerecord() = level:benign
                                exh:ref_number      = xch:ref_number
                                exh:date            = today()
                                exh:time            = clock()
                                access:users.clearkey(use:password_key)
                                use:password        = glo:password
                                access:users.fetch(use:password_key)
                                exh:user = use:user_code
                                exh:status          = 'UNIT RETURNED.'
                                If tmp:IMEIChanged
                                    exh:status      = Clip(exh:status) & ' OLD I.M.E.I. : ' & Clip(xch:esn)
                                End!If esn_change# = 1
                                If tmp:MSNChanged
                                    exh:Status      = Clip(exh:Status) & ' OLD M.S.N. : ' & Clip(xch:msn)
                                End !If tmp:MSNChanged
                                access:exchhist.insert()
                            end!if access:exchhist.primerecord() = level:benign
                            xch:available   = 'RTS'
                            xch:job_number  = ''
                            glo:select1     = xch:ref_number
                            If tmp:IMEIChanged
                                xch:esn = esn_temp
                            End !If tmp:IMEIChanged
                            If tmp:MSNChanged
                                xch:MSN = tmp:MSN
                            End !If tmp:MSNChanged
                            access:exchange.update()
                            print_label# = 1

                        End !If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:benign
                    End!If no_audit# = 1

                    ! Start Change 1837 BE(11/0/03)
                    !If print_label# = 1
                    !    Set(defaults)
                    !    access:defaults.next()
                    !
                    !    If GETINI('PRINTING','ThirdPartyReturnsLabel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                    !        glo:select1 = job:ref_number
                    !        Thermal_Labels('3RD')
                    !        glo:select1 = ''
                    !    End!If def:use_job_label = 'YES'
                    !End!If print_label# = 1
                    ! End Change 1837 BE(11/0/03)

                    job:workshop = 'YES'
                    job:date_paid = Today()             !Use this field to Date Returned

                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        ! Start Change 2471 BE(14/04/03)
                        !jobe:InWorkshopDate = Today()
                        !jobe:InWorkshopTime = Clock()
                        !Access:JOBSE.Update()
                        lockdate# = GETINI('THIRDPARTY','LockInWorkshopDate',,CLIP(PATH())&'\SB2KDEF.INI')
                        IF ((lockdate# <> 1) OR ((lockdate# = 1) AND (jobe:InWorkshopDate = ''))) THEN
                            jobe:InWorkshopDate = Today()
                            jobe:InWorkshopTime = Clock()
                            Access:JOBSE.Update()
                        END
                        ! End Change 2471 BE(14/04/03)
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                    GetStatus(Sub(tmp:status,1,3),1,'JOB')
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:notes         = '3RD PARTY AGENT: ' & Clip(trb:company_name) & |
                                            '<13,10>BATCH NUMBER: ' & Clip(trb:batch_number)
                        If tmp:location <> ''
                            aud:notes   = Clip(aud:notes) & '<13,10>LOCATION: ' & Clip(tmp:location)
                        End!If tmp:location <> ''
                                            
                        If tmp:IMEIChanged
                            aud:notes       = Clip(aud:notes) & '<13,10,13,10>NEW I.M.E.I. NO.: ' & Clip(esn_temp) &|
                                            '<13,10>PREVIOUS I.M.E.I. NO.: ' & Clip(job:esn)
                        End!If esn_change# = 1
                        If tmp:MSNChanged
                            aud:Notes       = Clip(aud:Notes) & '<13,10,13,10>NEW M.S.N.: ' & Clip(tmp:MSN) &|
                                            '<13,10>PREVIOUS M.S.N.: ' & Clip(job:MSN)
                        End !If tmp:MSNChanged
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = 'JOB'
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = '3RD PARTY AGENT: UNIT RETURNED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign
                    If tmp:location <> ''
                        access:locinter.clearkey(loi:location_key)
                        loi:location    = tmp:location
                        If access:locinter.tryfetch(loi:location_key) = Level:Benign
                            If loi:allocate_spaces = 'YES'
                                loi:current_spaces -= 1
                                If loi:current_spaces < 1
                                    loi:current_spaces = 0
                                    loi:location_available = 'NO'
                                End!If loi:current_spaces < 1
                                access:locinter.update()
                            End!If loi:allocate_spaces = 'YES'
                        End!If access:locinter(loi:location_key) = Level:Benign
                        job:location    = tmp:location
                    End!If tmp:location <> ''

                    Do ThirdParty

                    If tmp:IMEIChanged
                        job:esn = esn_temp
                    End!If esn_change# = 1
                    If tmp:MSNChanged
                        job:MSN = tmp:MSN
                    End !If tmp:MSNChanged

                    access:jobs.update()

                    ! Start Change 1837 BE(11/0/03)
                    If print_label# = 1
                        Set(defaults)
                        access:defaults.next()

                        If GETINI('PRINTING','ThirdPartyReturnsLabel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                            glo:select1 = job:ref_number
                            Thermal_Labels('3RD')
                            glo:select1 = ''
                        End!If def:use_job_label = 'YES'
                    End!If print_label# = 1
                    ! End Change 1837 BE(11/0/03)

                    !Check to see if accessories where retained or kept
                    glo:select2 = 'DES'
                    setcursor(cursor:wait)
                    save_aud_id = access:audit.savefile()
                    access:audit.clearkey(aud:action_key)
                    aud:ref_number = job:ref_number
                    aud:action     = '3RD PARTY AGENT: UNIT SENT'
                    set(aud:action_key,aud:action_key)
                    loop
                        if access:audit.next()
                           break
                        end !if
                        if aud:ref_number <> job:ref_number      |
                        or aud:action     <> '3RD PARTY AGENT: UNIT SENT'      |
                            then break.  ! end if
                        If Instring('ACCESSORIES RETAINED',aud:notes,1,1)
                            glo:select2 = 'RET'
                            Break
                        End!If Instring('ACCESSORIES RETAINED',aud:notes,1,1)
                    end !loop
                    access:audit.restorefile(save_aud_id)
                    setcursor()
                    DO pass_update
                Else!If ExchangeFound# = 1
                    !Unit Not Exchanged
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:notes         = '3RD PARTY AGENT: ' & Clip(trb:company_name) & |
                                            '<13,10>BATCH NUMBER: ' & Clip(trb:batch_number)
                        If tmp:location <> ''
                            aud:notes   = Clip(aud:notes) & '<13,10>LOCATION: ' & Clip(tmp:location)
                        End!If tmp:location <> ''
                        If tmp:IMEIChanged
                            aud:notes       = Clip(aud:notes) & '<13,10,13,10>NEW I.M.E.I. NO.: ' & Clip(esn_temp) &|
                                            '<13,10>PREVIOUS I.M.E.I. NO.: ' & Clip(job:esn)
                        End!If esn_change# = 1
                        If tmp:MSNChanged
                            aud:Notes       = Clip(aud:Notes) & '<13,10,13,10>NEW M.S.N.: ' & Clip(tmp:MSN) &|
                                            '<13,10>PREVIOUS M.S.N.: ' & Clip(job:MSN)
                        End !If tmp:MSNChanged
                        aud:ref_number    = job:ref_number
                        aud:date          = today()
                        aud:time          = clock()
                        aud:type          = 'JOB'
                        access:users.clearkey(use:password_key)
                        use:password = glo:password
                        access:users.fetch(use:password_key)
                        aud:user = use:user_code
                        aud:action        = '3RD PARTY AGENT: UNIT RETURNED'
                        access:audit.insert()
                    end!�if access:audit.primerecord() = level:benign

                    Do ThirdParty

                    If tmp:IMEIChanged
                        job:esn = esn_temp
                    End!If esn_change# = 1
                    If tmp:MSNChanged
                        job:MSN = tmp:MSN
                    End !If tmp:MSNChanged
                    job:workshop = 'YES'
                    job:date_paid = Today()             !Use this field to Date Returned

                    Access:JOBSE.Clearkey(jobe:RefNumberKey)
                    jobe:RefNumber  = job:Ref_Number
                    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Found
                        ! Start Change 2471 BE(14/04/03)
                        !jobe:InWorkshopDate = Today()
                        !jobe:InWorkshopTime = Clock()
                        !Access:JOBSE.Update()
                        lockdate# = GETINI('THIRDPARTY','LockInWorkshopDate',,CLIP(PATH())&'\SB2KDEF.INI')
                        IF ((lockdate# <> 1) OR ((lockdate# = 1) AND (jobe:InWorkshopDate = ''))) THEN
                            jobe:InWorkshopDate = Today()
                            jobe:InWorkshopTime = Clock()
                            Access:JOBSE.Update()
                        END
                        ! End Change 2471 BE(14/04/03)
                    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                        !Error
                    End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                    GetStatus(Sub(tmp:status,1,3),1,'JOB') !QA Required

                    If tmp:location <> ''
                        access:locinter.clearkey(loi:location_key)
                        loi:location    = tmp:location
                        If access:locinter.tryfetch(loi:location_key) = Level:Benign
                            If loi:allocate_spaces = 'YES'
                                loi:current_spaces -= 1
                                If loi:current_spaces < 1
                                    loi:current_spaces = 0
                                    loi:location_available = 'NO'
                                End!If loi:current_spaces < 1
                                access:locinter.update()
                            End!If loi:allocate_spaces = 'YES'
                        End!If access:locinter(loi:location_key) = Level:Benign
                        job:location    = tmp:location
                    End!If tmp:location <> ''
                    access:jobs.update()

                    !Check to see if accessories where retained or kept
                    glo:select2 = 'DES'
                    setcursor(cursor:wait)
                    save_aud_id = access:audit.savefile()
                    access:audit.clearkey(aud:action_key)
                    aud:ref_number = job:ref_number
                    aud:action     = '3RD PARTY AGENT: UNIT SENT'
                    set(aud:action_key,aud:action_key)
                    loop
                        if access:audit.next()
                           break
                        end !if
                        if aud:ref_number <> job:ref_number      |
                        or aud:action     <> '3RD PARTY AGENT: UNIT SENT'      |
                            then break.  ! end if
                        If Instring('ACCESSORIES RETAINED',aud:notes,1,1)
                            glo:select2 = 'RET'
                            Break
                        End!If Instring('ACCESSORIES RETAINED',aud:notes,1,1)
                    end !loop
                    access:audit.restorefile(save_aud_id)
                    setcursor()

                    IF GETINI('PRINTING','ThirdPartyReturnsLabel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
                        glo:select1 = job:ref_number
                        Thermal_Labels('3RD')
                    End!IF def:use_job_label = 'YES'
                    glo:select1 = ''
                    glo:select2 = ''
                    Do pass_update
                End!If job:exchange_unit_number <> '' or trb:exchanged = 'YES'
            Else!!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
                Case MessageEx('Cannot find selected Job Number.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                Do fail_update
            End!If access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        End!IF error# = 0
    End!if access:trdbatch.tryfetch(trb:jobstatuskey)
       
pass_update     Routine
    jobque:RecordNumber += 1
    jobque:JobNumber    = tmp:JobNumber
    If job:exchange_unit_number
        jobque:Exchanged = 'YES'
    Else!If job:exchange_unit_number
        jobque:Exchanged = 'NO'
    End!If job:exchange_unit_number
    Add(tmp:JobQueue)
    Sort(tmp:JobQueue,-jobque:RecordNumber)
    trb:status = 'IN'
    trb:datereturn  = Today()
    trb:TimeReturn  = Clock()
    access:trdbatch.update()

    tmp:ErrorText   = 'Job Number: ' & Clip(tmp:JobNumber) & ' Updated Successfully.'
    ?tmp:ErrorText{prop:fontcolor} = color:navy
    tmp:jobNumber    = ''
    esn_temp        = ''
    tmp:MSN         = ''
    ?tmp:MSN{prop:Hide} = 1
    ?tmp:MSN:Prompt{prop:Hide} = 1
    Display()
    Select(?tmp:JobNumber)

fail_update     Routine
    tmp:ErrorText   = 'Job Number: ' & Clip(tmp:JobNumber) & ' Update Failed.'
    ?tmp:ErrorText{prop:fontcolor} = color:red
    Display()

ThirdParty      Routine
    access:jobthird.clearkey(jot:RefNumberKey)
    jot:RefNumber   = job:Ref_Number
    Set(jot:RefNumberKey,jot:RefnumberKey)
    If access:jobthird.previous()
        get(jobthird,0)
        if access:jobthird.primerecord() = Level:benign
            jot:refnumber    = job:Ref_Number
            jot:originalimei = job:esn
            jot:OriginalMSN  = job:MSN
            jot:outimei      = job:esn
            jot:OutMSN       = job:MSN
            jot:inimei       = esn_temp
            jot:inMSN        = tmp:MSN
            jot:dateout      = job:ThirdPartyDateDesp
            jot:datedespatched = job:ThirdPartyDateDesp
            jot:datein       = Today()
            if access:jobthird.insert()
                access:jobthird.cancelautoinc()
            end
        End!if access:jobthird.primerecord() = Level:benign
    Else!If access:jobthird.previous()
        If jot:RefNumber <> job:Ref_Number
            jot:refnumber    = job:Ref_Number
            jot:originalimei = job:esn
            jot:OriginalMSN  = job:MSN
            jot:outimei      = job:esn
            jot:OutMSN       = job:MSN
            jot:inimei       = esn_temp
            jot:inMSN        = tmp:MSN
            jot:dateout      = job:ThirdPartyDateDesp
            jot:datedespatched = job:ThirdPartyDateDesp
            jot:datein       = Today()
            if access:jobthird.insert()
                access:jobthird.cancelautoinc()
            end
        Else!If job:RefNumber <> jot:RefNumber
            jot:inimei  = esn_temp
            jot:inMSN   = tmp:MSN
            jot:datein  = Today()
            access:jobthird.update()
        End!If job:RefNumber <> jot:RefNumber
    End!If access:jobthird.previous()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Return_Units',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('ESN_temp',ESN_temp,'Return_Units',1)
    SolaceViewVars('tmp:IMEIChanged',tmp:IMEIChanged,'Return_Units',1)
    SolaceViewVars('tmp:MSNChanged',tmp:MSNChanged,'Return_Units',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Return_Units',1)
    SolaceViewVars('tmp:JobQueue:JobNumber',tmp:JobQueue:JobNumber,'Return_Units',1)
    SolaceViewVars('tmp:JobQueue:Exchanged',tmp:JobQueue:Exchanged,'Return_Units',1)
    SolaceViewVars('tmp:JobQueue:RecordNumber',tmp:JobQueue:RecordNumber,'Return_Units',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'Return_Units',1)
    SolaceViewVars('tmp:ErrorText',tmp:ErrorText,'Return_Units',1)
    SolaceViewVars('tmp:location',tmp:location,'Return_Units',1)
    SolaceViewVars('tmp:status',tmp:status,'Return_Units',1)
    SolaceViewVars('tmp:MSN',tmp:MSN,'Return_Units',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:status:Prompt;  SolaceCtrlName = '?tmp:status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:status;  SolaceCtrlName = '?tmp:status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStatus;  SolaceCtrlName = '?LookupStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location:Prompt;  SolaceCtrlName = '?tmp:location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:location;  SolaceCtrlName = '?tmp:location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber:Prompt;  SolaceCtrlName = '?tmp:JobNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobNumber;  SolaceCtrlName = '?tmp:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN:Prompt;  SolaceCtrlName = '?ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN_temp;  SolaceCtrlName = '?ESN_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:MSN:Prompt;  SolaceCtrlName = '?tmp:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:MSN;  SolaceCtrlName = '?tmp:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ErrorText;  SolaceCtrlName = '?tmp:ErrorText';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Return_Units')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Return_Units')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:EXCAUDIT.Open
  Relate:STAHEAD.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:TRDBATCH.UseFile
  Access:EXCHANGE_ALIAS.UseFile
  Access:EXCHHIST.UseFile
  Access:JOBTHIRD.UseFile
  Access:LOCATION.UseFile
  Access:LOCINTER.UseFile
  Access:MANUFACT.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  access:status.clearkey(sts:ref_number_only_key)
  sts:ref_number = 605
  if access:status.tryfetch(sts:ref_number_only_key) = Level:Benign
      tmp:status  = sts:status
  End!if access:status.tryfetch(sts:ref_number_only_key)
  OPEN(window)
  SELF.Opened=True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  If def:HideLocation
      ?tmp:Location{prop:Hide} = 1
      ?tmp:Location:Prompt{prop:Hide} =1
      ?LookupLocation{prop:Hide} = 1
  End !def:HideLocation
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  IF ?tmp:location{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?tmp:location{Prop:Tip}
  END
  IF ?tmp:location{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?tmp:location{Prop:Msg}
  END
  IF ?tmp:status{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?tmp:status{Prop:Tip}
  END
  IF ?tmp:status{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?tmp:status{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:EXCAUDIT.Close
    Relate:STAHEAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Return_Units',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      SelectStatus
      BrowseAvailLocation
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:status
      IF tmp:status OR ?tmp:status{Prop:Req}
        sts:Status = tmp:status
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:tmp:status        = tmp:status
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:status = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:status = look:tmp:status
            SELECT(?tmp:status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStatus
      ThisWindow.Update
      sts:Status = tmp:status
      GLO:Select1 = 'JOB'
      sts:Job = 'YES'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:status = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:status)
    OF ?tmp:location
      IF tmp:location OR ?tmp:location{Prop:Req}
        loi:Location = tmp:location
        !Save Lookup Field Incase Of error
        look:tmp:location        = tmp:location
        IF Access:LOCINTER.TryFetch(loi:Location_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:location = loi:Location
          ELSE
            !Restore Lookup On Error
            tmp:location = look:tmp:location
            SELECT(?tmp:location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = tmp:location
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:location = loi:Location
          Select(?+1)
      ELSE
          Select(?tmp:location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:location)
    OF ?tmp:JobNumber
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobNumber, Accepted)
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = tmp:JobNumber
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Found
              If man:Use_MSN = 'YES'
                  ?tmp:MSN{prop:hide} = 0
                  ?tmp:MSN:Prompt{Prop:hide} = 0
              Else
                  ?tmp:MSN{prop:Hide} = 1
                  ?tmp:MSN:Prompt{prop:Hide} = 1
              End !If man:Use_MSN = 'YES'
          Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      Else!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benig
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobNumber, Accepted)
    OF ?ESN_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Accepted)
      IF LEN(CLIP(ESN_temp)) = 18
        !Ericsson IMEI!
        ESN_temp = SUB(ESN_temp,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      If ?tmp:MSN{prop:hide} = 1
          Do FinalRoutine
      End !?tmp:MSN{prop:hide} = 1
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Accepted)
    OF ?tmp:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
      !Get Manufactuerere
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number = tmp:JobNumber
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          IF LEN(CLIP(tmp:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
            !Ericsson MSN!
            tmp:MSN = SUB(tmp:MSN,2,10)
            UPDATE()
          END
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      Do FinalRoutine
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Accepted)
    OF ?Cancel
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
      Case MessageEx('Do you wish to print a Returns Note?'&|
        '<13,10>','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
      Third_Party_Report_Criteria()
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Return_Units')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?tmp:location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, AlertKey)
      Post(Event:Accepted,?LookupLocation)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:location, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?ESN_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Selected)
      ?ESN_temp{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ESN_temp, Selected)
    OF ?tmp:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Selected)
      ?tmp:MSN{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:MSN, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F10Key
              Post(Event:Accepted,?Cancel)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

