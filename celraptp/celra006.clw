

   MEMBER('celraptp.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA006.INC'),ONCE        !Local module procedure declarations
                     END


Allocate_Authorisation_Number PROCEDURE               !Generated from procedure template - Window

tmp:BatchNo          LONG
save_trb_id          USHORT,AUTO
tmp:AuthorisationNo  STRING(30)
tmp:CompanyName      STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?tmp:CompanyName
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB4::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
window               WINDOW('Allocate Authorisation Number'),AT(,,220,120),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,84),USE(?Sheet1),WIZARD,SPREAD
                         TAB('sd'),USE(?Tab1)
                           PROMPT('The Authorisation Number will be allocated to ALL jobs in the selected Batch'),AT(8,8,204,20),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           COMBO(@s30),AT(84,36,124,10),USE(tmp:CompanyName),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Batch Number'),AT(8,52),USE(?tmp:BatchNumber:Prompt)
                           PROMPT('3rd Party Agent'),AT(8,36),USE(?tmp:BatchNumber:Prompt:2)
                           ENTRY(@s8),AT(84,52,64,10),USE(tmp:BatchNo),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Authorisation No'),AT(8,68),USE(?tmp:AuthorisationNo:Prompt)
                           ENTRY(@s30),AT(84,68,124,10),USE(tmp:AuthorisationNo),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Authorisation Number for the Despatched Batch'),TIP('Authorisation Number for the Despatched Batch'),REQ,UPR
                         END
                       END
                       PANEL,AT(4,92,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('All&ocate'),AT(8,96,56,16),USE(?OkButton),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,96,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB4                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?tmp:CompanyName{prop:ReadOnly} = True
        ?tmp:CompanyName{prop:FontColor} = 65793
        ?tmp:CompanyName{prop:Color} = 15066597
    Elsif ?tmp:CompanyName{prop:Req} = True
        ?tmp:CompanyName{prop:FontColor} = 65793
        ?tmp:CompanyName{prop:Color} = 8454143
    Else ! If ?tmp:CompanyName{prop:Req} = True
        ?tmp:CompanyName{prop:FontColor} = 65793
        ?tmp:CompanyName{prop:Color} = 16777215
    End ! If ?tmp:CompanyName{prop:Req} = True
    ?tmp:CompanyName{prop:Trn} = 0
    ?tmp:CompanyName{prop:FontStyle} = font:Bold
    ?tmp:BatchNumber:Prompt{prop:FontColor} = -1
    ?tmp:BatchNumber:Prompt{prop:Color} = 15066597
    ?tmp:BatchNumber:Prompt:2{prop:FontColor} = -1
    ?tmp:BatchNumber:Prompt:2{prop:Color} = 15066597
    If ?tmp:BatchNo{prop:ReadOnly} = True
        ?tmp:BatchNo{prop:FontColor} = 65793
        ?tmp:BatchNo{prop:Color} = 15066597
    Elsif ?tmp:BatchNo{prop:Req} = True
        ?tmp:BatchNo{prop:FontColor} = 65793
        ?tmp:BatchNo{prop:Color} = 8454143
    Else ! If ?tmp:BatchNo{prop:Req} = True
        ?tmp:BatchNo{prop:FontColor} = 65793
        ?tmp:BatchNo{prop:Color} = 16777215
    End ! If ?tmp:BatchNo{prop:Req} = True
    ?tmp:BatchNo{prop:Trn} = 0
    ?tmp:BatchNo{prop:FontStyle} = font:Bold
    ?tmp:AuthorisationNo:Prompt{prop:FontColor} = -1
    ?tmp:AuthorisationNo:Prompt{prop:Color} = 15066597
    If ?tmp:AuthorisationNo{prop:ReadOnly} = True
        ?tmp:AuthorisationNo{prop:FontColor} = 65793
        ?tmp:AuthorisationNo{prop:Color} = 15066597
    Elsif ?tmp:AuthorisationNo{prop:Req} = True
        ?tmp:AuthorisationNo{prop:FontColor} = 65793
        ?tmp:AuthorisationNo{prop:Color} = 8454143
    Else ! If ?tmp:AuthorisationNo{prop:Req} = True
        ?tmp:AuthorisationNo{prop:FontColor} = 65793
        ?tmp:AuthorisationNo{prop:Color} = 16777215
    End ! If ?tmp:AuthorisationNo{prop:Req} = True
    ?tmp:AuthorisationNo{prop:Trn} = 0
    ?tmp:AuthorisationNo{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Allocate_Authorisation_Number',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:BatchNo',tmp:BatchNo,'Allocate_Authorisation_Number',1)
    SolaceViewVars('save_trb_id',save_trb_id,'Allocate_Authorisation_Number',1)
    SolaceViewVars('tmp:AuthorisationNo',tmp:AuthorisationNo,'Allocate_Authorisation_Number',1)
    SolaceViewVars('tmp:CompanyName',tmp:CompanyName,'Allocate_Authorisation_Number',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CompanyName;  SolaceCtrlName = '?tmp:CompanyName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchNumber:Prompt;  SolaceCtrlName = '?tmp:BatchNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchNumber:Prompt:2;  SolaceCtrlName = '?tmp:BatchNumber:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:BatchNo;  SolaceCtrlName = '?tmp:BatchNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AuthorisationNo:Prompt;  SolaceCtrlName = '?tmp:AuthorisationNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:AuthorisationNo;  SolaceCtrlName = '?tmp:AuthorisationNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Allocate_Authorisation_Number')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Allocate_Authorisation_Number')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRDPARTY.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  FDCB4.Init(tmp:CompanyName,?tmp:CompanyName,Queue:FileDropCombo.ViewPosition,FDCB4::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB4.Q &= Queue:FileDropCombo
  FDCB4.AddSortOrder()
  FDCB4.AddField(trd:Company_Name,FDCB4.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB4.WindowComponent)
  FDCB4.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRDPARTY.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Allocate_Authorisation_Number',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      Case MessageEx('Are you sure you want to allocate the selected Authorisation Number to Batch '&Clip(tmp:BatchNo)&'?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
      
              count# = 0
              continue# = 0
              setcursor(cursor:wait)
              save_trb_id = access:trdbatch.savefile()
              access:trdbatch.clearkey(trb:batch_number_key)
              trb:company_name = tmp:companyName
              trb:batch_number = tmp:BatchNo
              set(trb:batch_number_key,trb:batch_number_key)
              loop
                  if access:trdbatch.next()
                     break
                  end !if
                  if trb:company_name <> tmp:CompanyName      |
                  or trb:batch_number <> tmp:BatchNo      |
                      then break.  ! end if
                  If trb:authorisationNo <> '' And continue# = 0
                      setcursor()
                      Case MessageEx('This Batch has already been allocated an Authorisation Number of '&clip(trb:authorisationno)&'.<13,10><13,10>Are you sure you want to OVERWRITE this?','ServiceBase 2000',|
                                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                          Of 1 ! &Yes Button
                              continue# = 1
                          Of 2 ! &No Button
                              count# = 2
                              Break
                      End!Case MessageEx
                      setcursor(cursor:wait)
                  End!If trb:authorisationNo <> '' And continue# = 0
                  count# = 1
                  trb:authorisationNo = tmp:AuthorisationNo
                  access:trdbatch.update()
              end !loop
              access:trdbatch.restorefile(save_trb_id)
              setcursor()
              Case count#
                  Of 0
                      Case MessageEx('Cannot find the selected Batch Number.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Of 1
                      Case MessageEx('Allocation Complete.','ServiceBase 2000',|
                                     'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
              End!IF count# = 0
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Allocate_Authorisation_Number')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

