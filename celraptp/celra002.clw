

   MEMBER('celraptp.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA003.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA004.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA005.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA006.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA007.INC'),ONCE        !Req'd for module callout resolution
                     END


Browse_Batch PROCEDURE                                !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
save_jot_id          USHORT,AUTO
save_trb_id          USHORT,AUTO
tmp:JobNumber        LONG
tmp:BatchNumber      LONG
tmp:CompanyName      STRING(30)
tmp:ThirdPartyNumber LONG
queue:filter         QUEUE,PRE()
                     END
Company_Name_temp    STRING(30)
status_temp          STRING('OUT')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Company_Name_temp
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(TRDBATCH)
                       PROJECT(trb:Batch_Number)
                       PROJECT(trb:Date)
                       PROJECT(trb:Company_Name)
                       PROJECT(trb:Ref_Number)
                       PROJECT(trb:ESN)
                       PROJECT(trb:MSN)
                       PROJECT(trb:AuthorisationNo)
                       PROJECT(trb:RecordNumber)
                       PROJECT(trb:Status)
                       JOIN(jot:ThirdPartyKey,trb:RecordNumber)
                         PROJECT(jot:DateDespatched)
                         PROJECT(jot:DateIn)
                       END
                       JOIN(job:Ref_Number_Key,trb:Ref_Number)
                         PROJECT(job:Account_Number)
                         PROJECT(job:Ref_Number)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
trb:Batch_Number       LIKE(trb:Batch_Number)         !List box control field - type derived from field
trb:Batch_Number_NormalFG LONG                        !Normal forground color
trb:Batch_Number_NormalBG LONG                        !Normal background color
trb:Batch_Number_SelectedFG LONG                      !Selected forground color
trb:Batch_Number_SelectedBG LONG                      !Selected background color
trb:Date               LIKE(trb:Date)                 !List box control field - type derived from field
trb:Date_NormalFG      LONG                           !Normal forground color
trb:Date_NormalBG      LONG                           !Normal background color
trb:Date_SelectedFG    LONG                           !Selected forground color
trb:Date_SelectedBG    LONG                           !Selected background color
trb:Company_Name       LIKE(trb:Company_Name)         !List box control field - type derived from field
trb:Company_Name_NormalFG LONG                        !Normal forground color
trb:Company_Name_NormalBG LONG                        !Normal background color
trb:Company_Name_SelectedFG LONG                      !Selected forground color
trb:Company_Name_SelectedBG LONG                      !Selected background color
trb:Ref_Number         LIKE(trb:Ref_Number)           !List box control field - type derived from field
trb:Ref_Number_NormalFG LONG                          !Normal forground color
trb:Ref_Number_NormalBG LONG                          !Normal background color
trb:Ref_Number_SelectedFG LONG                        !Selected forground color
trb:Ref_Number_SelectedBG LONG                        !Selected background color
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Account_Number_NormalFG LONG                      !Normal forground color
job:Account_Number_NormalBG LONG                      !Normal background color
job:Account_Number_SelectedFG LONG                    !Selected forground color
job:Account_Number_SelectedBG LONG                    !Selected background color
jot:DateDespatched     LIKE(jot:DateDespatched)       !List box control field - type derived from field
jot:DateDespatched_NormalFG LONG                      !Normal forground color
jot:DateDespatched_NormalBG LONG                      !Normal background color
jot:DateDespatched_SelectedFG LONG                    !Selected forground color
jot:DateDespatched_SelectedBG LONG                    !Selected background color
trb:ESN                LIKE(trb:ESN)                  !List box control field - type derived from field
trb:ESN_NormalFG       LONG                           !Normal forground color
trb:ESN_NormalBG       LONG                           !Normal background color
trb:ESN_SelectedFG     LONG                           !Selected forground color
trb:ESN_SelectedBG     LONG                           !Selected background color
trb:MSN                LIKE(trb:MSN)                  !List box control field - type derived from field
trb:MSN_NormalFG       LONG                           !Normal forground color
trb:MSN_NormalBG       LONG                           !Normal background color
trb:MSN_SelectedFG     LONG                           !Selected forground color
trb:MSN_SelectedBG     LONG                           !Selected background color
jot:DateIn             LIKE(jot:DateIn)               !List box control field - type derived from field
jot:DateIn_NormalFG    LONG                           !Normal forground color
jot:DateIn_NormalBG    LONG                           !Normal background color
jot:DateIn_SelectedFG  LONG                           !Selected forground color
jot:DateIn_SelectedBG  LONG                           !Selected background color
trb:AuthorisationNo    LIKE(trb:AuthorisationNo)      !List box control field - type derived from field
trb:AuthorisationNo_NormalFG LONG                     !Normal forground color
trb:AuthorisationNo_NormalBG LONG                     !Normal background color
trb:AuthorisationNo_SelectedFG LONG                   !Selected forground color
trb:AuthorisationNo_SelectedBG LONG                   !Selected background color
trb:RecordNumber       LIKE(trb:RecordNumber)         !Primary key field - type derived from field
trb:Status             LIKE(trb:Status)               !Browse key field - type derived from field
job:Ref_Number         LIKE(job:Ref_Number)           !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK19::trb:Company_Name    LIKE(trb:Company_Name)
HK19::trb:Status          LIKE(trb:Status)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB6::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
QuickWindow          WINDOW('Consignment Batch'),AT(,,662,262),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('Update_QA'),ALRT(F3Key),ALRT(F4Key),ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),SYSTEM,GRAY,DOUBLE,MDI
                       PROMPT('- Exchanged'),AT(20,248),USE(?Prompt1)
                       OPTION('Status'),AT(460,20,112,24),USE(status_temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                         RADIO('All'),AT(544,28),USE(?status_temp:Radio1),VALUE('ALL')
                         RADIO('Out'),AT(468,28),USE(?status_temp:Radio2),VALUE('OUT')
                         RADIO('In'),AT(508,28),USE(?status_temp:Radio3),VALUE('IN')
                       END
                       LIST,AT(8,48,564,188),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('37R(2)|M*~Batch No~L@s8@40R(2)|M*~Date~C(0)@d6@93L(2)|M*~Company Name~@s30@37R(2' &|
   ')|M*~Job No~@s8@65L(2)|M*~Account Number~@s15@44R(2)|M*~Despatched~@d6@69L(2)|M*' &|
   '~I.M.E.I. No~@s16@69L(2)|M*~M.S.N.~@s16@40R(2)|M*~Recvd~@d6@120L(2)|M*~Auth No~@' &|
   's30@'),FROM(Queue:Browse:1)
                       BUTTON('&Amend Single Entry'),AT(584,60,76,20),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('&Create New Batch [F3]'),AT(584,8,76,20),USE(?Insert),LEFT,ICON('proc_out.gif'),DEFAULT
                       BUTTON('&Add To Batch [F4]'),AT(584,32,76,20),USE(?Insert:2),LEFT,ICON('proc_out.gif'),DEFAULT
                       BUTTON('Allocate Auth. No [F5]'),AT(584,84,76,20),USE(?Allocate),LEFT,ICON('History.gif')
                       BUTTON('Print Routines [F6]'),AT(584,108,76,20),USE(?Print_Routines),LEFT,ICON(ICON:Print1)
                       BUTTON('Rapid Job Update [F7]'),AT(584,144,76,20),USE(?Rapid_Job_Update),LEFT,ICON('Clipbrd.gif')
                       BUTTON('Return Unit [F8]'),AT(584,188,76,20),USE(?returns),LEFT,ICON('proc_in.gif')
                       BUTTON('&Delete'),AT(584,212,76,20),USE(?Delete),LEFT,ICON('delete.ico')
                       BUTTON('Count Jobs'),AT(516,240,56,16),USE(?CountJobs),LEFT,ICON('CALC1.ICO')
                       SHEET,AT(4,2,572,258),USE(?CurrentTab),SPREAD
                         TAB('By Batch Number'),USE(?Tab:3)
                           ENTRY(@s8),AT(8,32,64,10),USE(trb:Batch_Number),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By 3rd Party Agent'),USE(?Tab2)
                           COMBO(@s30),AT(8,20,124,10),USE(Company_Name_temp),IMM,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           ENTRY(@s8),AT(8,32,64,10),USE(trb:Batch_Number,,?TRB:Batch_Number:2),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By I.M.E.I. Number'),USE(?Tab3)
                           ENTRY(@s16),AT(8,32,124,10),USE(trb:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('By Authorisation Number'),USE(?Tab4)
                           ENTRY(@s30),AT(8,32,124,10),USE(trb:AuthorisationNo),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Authorisation Number for the Despatched Batch'),TIP('Authorisation Number for the Despatched Batch'),UPR
                         END
                         TAB('By Job Number'),USE(?Tab5)
                           ENTRY(@s8),AT(8,32,64,10),USE(trb:Ref_Number),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Number'),TIP('Job Number'),UPR
                         END
                       END
                       BUTTON('Close'),AT(584,240,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       BOX,AT(8,248,8,8),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Red)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(status_temp) = 'ALL'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(status_temp) = 'IN'
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(status_temp) = 'OUT'
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(status_temp) = 'ALL'
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(status_temp) = 'IN'
BRW1::Sort10:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(status_temp) = 'OUT'
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(status_temp) = 'ALL'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(status_temp) = 'IN'
BRW1::Sort11:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(status_temp) = 'OUT'
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(status_temp) = 'ALL'
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(status_temp) = 'IN'
BRW1::Sort12:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(status_temp) = 'OUT'
BRW1::Sort13:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(status_temp) = 'ALL'
BRW1::Sort14:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(status_temp) = 'IN'
BRW1::Sort15:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(status_temp) = 'OUT'
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?status_temp{prop:Font,3} = -1
    ?status_temp{prop:Color} = 15066597
    ?status_temp{prop:Trn} = 0
    ?status_temp:Radio1{prop:Font,3} = -1
    ?status_temp:Radio1{prop:Color} = 15066597
    ?status_temp:Radio1{prop:Trn} = 0
    ?status_temp:Radio2{prop:Font,3} = -1
    ?status_temp:Radio2{prop:Color} = 15066597
    ?status_temp:Radio2{prop:Trn} = 0
    ?status_temp:Radio3{prop:Font,3} = -1
    ?status_temp:Radio3{prop:Color} = 15066597
    ?status_temp:Radio3{prop:Trn} = 0
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597
    If ?trb:Batch_Number{prop:ReadOnly} = True
        ?trb:Batch_Number{prop:FontColor} = 65793
        ?trb:Batch_Number{prop:Color} = 15066597
    Elsif ?trb:Batch_Number{prop:Req} = True
        ?trb:Batch_Number{prop:FontColor} = 65793
        ?trb:Batch_Number{prop:Color} = 8454143
    Else ! If ?trb:Batch_Number{prop:Req} = True
        ?trb:Batch_Number{prop:FontColor} = 65793
        ?trb:Batch_Number{prop:Color} = 16777215
    End ! If ?trb:Batch_Number{prop:Req} = True
    ?trb:Batch_Number{prop:Trn} = 0
    ?trb:Batch_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?Company_Name_temp{prop:ReadOnly} = True
        ?Company_Name_temp{prop:FontColor} = 65793
        ?Company_Name_temp{prop:Color} = 15066597
    Elsif ?Company_Name_temp{prop:Req} = True
        ?Company_Name_temp{prop:FontColor} = 65793
        ?Company_Name_temp{prop:Color} = 8454143
    Else ! If ?Company_Name_temp{prop:Req} = True
        ?Company_Name_temp{prop:FontColor} = 65793
        ?Company_Name_temp{prop:Color} = 16777215
    End ! If ?Company_Name_temp{prop:Req} = True
    ?Company_Name_temp{prop:Trn} = 0
    ?Company_Name_temp{prop:FontStyle} = font:Bold
    If ?TRB:Batch_Number:2{prop:ReadOnly} = True
        ?TRB:Batch_Number:2{prop:FontColor} = 65793
        ?TRB:Batch_Number:2{prop:Color} = 15066597
    Elsif ?TRB:Batch_Number:2{prop:Req} = True
        ?TRB:Batch_Number:2{prop:FontColor} = 65793
        ?TRB:Batch_Number:2{prop:Color} = 8454143
    Else ! If ?TRB:Batch_Number:2{prop:Req} = True
        ?TRB:Batch_Number:2{prop:FontColor} = 65793
        ?TRB:Batch_Number:2{prop:Color} = 16777215
    End ! If ?TRB:Batch_Number:2{prop:Req} = True
    ?TRB:Batch_Number:2{prop:Trn} = 0
    ?TRB:Batch_Number:2{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?trb:ESN{prop:ReadOnly} = True
        ?trb:ESN{prop:FontColor} = 65793
        ?trb:ESN{prop:Color} = 15066597
    Elsif ?trb:ESN{prop:Req} = True
        ?trb:ESN{prop:FontColor} = 65793
        ?trb:ESN{prop:Color} = 8454143
    Else ! If ?trb:ESN{prop:Req} = True
        ?trb:ESN{prop:FontColor} = 65793
        ?trb:ESN{prop:Color} = 16777215
    End ! If ?trb:ESN{prop:Req} = True
    ?trb:ESN{prop:Trn} = 0
    ?trb:ESN{prop:FontStyle} = font:Bold
    ?Tab4{prop:Color} = 15066597
    If ?trb:AuthorisationNo{prop:ReadOnly} = True
        ?trb:AuthorisationNo{prop:FontColor} = 65793
        ?trb:AuthorisationNo{prop:Color} = 15066597
    Elsif ?trb:AuthorisationNo{prop:Req} = True
        ?trb:AuthorisationNo{prop:FontColor} = 65793
        ?trb:AuthorisationNo{prop:Color} = 8454143
    Else ! If ?trb:AuthorisationNo{prop:Req} = True
        ?trb:AuthorisationNo{prop:FontColor} = 65793
        ?trb:AuthorisationNo{prop:Color} = 16777215
    End ! If ?trb:AuthorisationNo{prop:Req} = True
    ?trb:AuthorisationNo{prop:Trn} = 0
    ?trb:AuthorisationNo{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    If ?trb:Ref_Number{prop:ReadOnly} = True
        ?trb:Ref_Number{prop:FontColor} = 65793
        ?trb:Ref_Number{prop:Color} = 15066597
    Elsif ?trb:Ref_Number{prop:Req} = True
        ?trb:Ref_Number{prop:FontColor} = 65793
        ?trb:Ref_Number{prop:Color} = 8454143
    Else ! If ?trb:Ref_Number{prop:Req} = True
        ?trb:Ref_Number{prop:FontColor} = 65793
        ?trb:Ref_Number{prop:Color} = 16777215
    End ! If ?trb:Ref_Number{prop:Req} = True
    ?trb:Ref_Number{prop:Trn} = 0
    ?trb:Ref_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Batch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Batch',1)
    SolaceViewVars('save_jot_id',save_jot_id,'Browse_Batch',1)
    SolaceViewVars('save_trb_id',save_trb_id,'Browse_Batch',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'Browse_Batch',1)
    SolaceViewVars('tmp:BatchNumber',tmp:BatchNumber,'Browse_Batch',1)
    SolaceViewVars('tmp:CompanyName',tmp:CompanyName,'Browse_Batch',1)
    SolaceViewVars('tmp:ThirdPartyNumber',tmp:ThirdPartyNumber,'Browse_Batch',1)
    SolaceViewVars('Company_Name_temp',Company_Name_temp,'Browse_Batch',1)
    SolaceViewVars('status_temp',status_temp,'Browse_Batch',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp;  SolaceCtrlName = '?status_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp:Radio1;  SolaceCtrlName = '?status_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp:Radio2;  SolaceCtrlName = '?status_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?status_temp:Radio3;  SolaceCtrlName = '?status_temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Allocate;  SolaceCtrlName = '?Allocate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Routines;  SolaceCtrlName = '?Print_Routines';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Rapid_Job_Update;  SolaceCtrlName = '?Rapid_Job_Update';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?returns;  SolaceCtrlName = '?returns';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CountJobs;  SolaceCtrlName = '?CountJobs';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:Batch_Number;  SolaceCtrlName = '?trb:Batch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Company_Name_temp;  SolaceCtrlName = '?Company_Name_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRB:Batch_Number:2;  SolaceCtrlName = '?TRB:Batch_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:ESN;  SolaceCtrlName = '?trb:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:AuthorisationNo;  SolaceCtrlName = '?trb:AuthorisationNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trb:Ref_Number;  SolaceCtrlName = '?trb:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Batch')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Batch')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Batch'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Access:TRDPARTY.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:TRDBATCH,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,trb:BatchOnlyKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?TRB:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Number_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?TRB:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Number_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?TRB:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Number_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?TRB:Batch_Number:2,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Company_Status_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?TRB:Batch_Number:2,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:Batch_Company_Status_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?TRB:Batch_Number:2,trb:Batch_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:ESN_Only_Key)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?TRB:ESN,trb:ESN,1,BRW1)
  BRW1.AddSortOrder(,trb:ESN_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?TRB:ESN,trb:ESN,1,BRW1)
  BRW1.AddSortOrder(,trb:ESN_Status_Key)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?TRB:ESN,trb:ESN,1,BRW1)
  BRW1.AddSortOrder(,trb:AuthorisationKey)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?TRB:AuthorisationNo,trb:AuthorisationNo,1,BRW1)
  BRW1.AddSortOrder(,trb:StatusAuthKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?TRB:AuthorisationNo,trb:AuthorisationNo,1,BRW1)
  BRW1.AddSortOrder(,trb:StatusAuthKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?TRB:AuthorisationNo,trb:AuthorisationNo,1,BRW1)
  BRW1.AddSortOrder(,trb:JobNumberKey)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?trb:Ref_Number,trb:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:JobStatusKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?trb:Ref_Number,trb:Ref_Number,1,BRW1)
  BRW1.AddSortOrder(,trb:JobStatusKey)
  BRW1.AddRange(trb:Status)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?trb:Ref_Number,trb:Ref_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,trb:Batch_Number_Key)
  BRW1.AddRange(trb:Company_Name)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?TRB:Batch_Number,trb:Batch_Number,1,BRW1)
  BRW1.AddField(trb:Batch_Number,BRW1.Q.trb:Batch_Number)
  BRW1.AddField(trb:Date,BRW1.Q.trb:Date)
  BRW1.AddField(trb:Company_Name,BRW1.Q.trb:Company_Name)
  BRW1.AddField(trb:Ref_Number,BRW1.Q.trb:Ref_Number)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(jot:DateDespatched,BRW1.Q.jot:DateDespatched)
  BRW1.AddField(trb:ESN,BRW1.Q.trb:ESN)
  BRW1.AddField(trb:MSN,BRW1.Q.trb:MSN)
  BRW1.AddField(jot:DateIn,BRW1.Q.jot:DateIn)
  BRW1.AddField(trb:AuthorisationNo,BRW1.Q.trb:AuthorisationNo)
  BRW1.AddField(trb:RecordNumber,BRW1.Q.trb:RecordNumber)
  BRW1.AddField(trb:Status,BRW1.Q.trb:Status)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB6.Init(Company_Name_temp,?Company_Name_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder()
  FDCB6.AddField(trd:Company_Name,FDCB6.Q.trd:Company_Name)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:3{PROP:TEXT} = 'By Batch Number'
    ?Tab2{PROP:TEXT} = 'By 3rd Party Agent'
    ?Tab3{PROP:TEXT} = 'By I.M.E.I. Number'
    ?Tab4{PROP:TEXT} = 'By Authorisation Number'
    ?Tab5{PROP:TEXT} = 'By Job Number'
    ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Batch'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Batch',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = true
  
  case request
      of changerecord
          check_access('3RD PARTY DESPATCH - CHANGE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
      of deleterecord
          check_access('3RD PARTY DESPATCH - DELETE',x")
          if x" = false
              beep(beep:systemhand)  ;  yield()
              message('You do not have access to this option.', |
                      'ServiceBase 2000', icon:hand)
              do_update# = false
          end
          tmp:jobnumber   = TRB:Ref_Number
          tmp:BatchNumber = TRB:Batch_Number
          tmp:CompanyName = TRB:Company_Name
          tmp:ThirdPartyNumber    = trb:RecordNumber
  end !case request
  
  if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateTRDBATCH
    ReturnValue = GlobalResponse
  END
      If request  = Deleterecord And globalresponse = 1
          get(audit,0)
          if access:audit.primerecord() = level:benign
              aud:notes         = 'BATCH NUMBER: ' & Clip(tmp:BatchNumber) & |
                                  '<13,10>COMPANY NAME: ' & Clip(tmp:CompanyName)
              aud:ref_number    = tmp:jobNumber
              aud:date          = today()
              aud:time          = clock()
              aud:type          = 'JOB'
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              aud:user = use:user_code
              aud:action        = 'DELETED FROM 3RD PARTY BATCH'
              access:audit.insert()
          end!�if access:audit.primerecord() = level:benign
          !Delete the associated JOBTHIRD entry
          Save_jot_ID = Access:JOBTHIRD.SaveFile()
          Access:JOBTHIRD.ClearKey(jot:ThirdPartyKey)
          jot:ThirdPartyNumber = tmp:ThirdPartyNumber
          Set(jot:ThirdPartyKey,jot:ThirdPartyKey)
          Loop
              If Access:JOBTHIRD.NEXT()
                 Break
              End !If
              If jot:ThirdPartyNumber <> tmp:ThirdPartyNumber      |
                  Then Break.  ! End If
              Delete(JOBTHIRD)
          End !Loop
          Access:JOBTHIRD.RestoreFile(Save_jot_ID)
  
      End!If request  = Deleterecord And globalresponse = 1
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?trb:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
      IF LEN(CLIP(trb:ESN)) = 18
        !Ericsson IMEI!
        trb:ESN = SUB(trb:ESN,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:ESN, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?status_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
      thiswindow.reset(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?status_temp, Accepted)
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      Check_access('3RD PARTY DESPATCH',x")
      If x" = False
          Case MessageEx('You do not have access to this option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If x" = False
          Case MessageEx('Are you sure you want to Create a new batch of items to Despatch to a 3rd Party?','ServiceBase 2000','Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemquestion,msgex:samewidths,84,26,0)
              Of 1 ! &Yes Button
                  Despatch_Units('NEW')
      
              Of 2 ! &No Button
          End!Case MessageEx
      End!If x" = False
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    OF ?Insert:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert:2, Accepted)
      Check_access('3RD PARTY DESPATCH',x")
      If x" = False
          Case MessageEx('You do not have access to this option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If x" = False
          Despatch_Units('ADD')
      End!If x" = False
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert:2, Accepted)
    OF ?Allocate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate, Accepted)
      check_access('3RD PARTY DESPATCH - ALLOCATE',x")
      if x" = false
          case messageex('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              of 1 ! &ok button
          end!case messageex
      Else!if x" = false
          Allocate_Authorisation_Number
      end!if x" = false
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Allocate, Accepted)
    OF ?Print_Routines
      ThisWindow.Update
      Third_Party_Print_Routine
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Routines, Accepted)
    OF ?Rapid_Job_Update
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rapid_Job_Update, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number  = brw1.q.trb:ref_number
      If access:jobs.fetch(job:ref_number_key) = Level:Benign
          Globalrequest = ChangeRecord
          ! Start Change 1744 BE(15/04/03)
          glo:select20 = ''
          ! End Change 1744 BE(15/04/03)
          Update_Jobs_Rapid
          ! Start Change 1744 BE(15/04/03)
          glo:select20 = ''
          ! End Change 1744 BE(15/04/03)
      End!If access:jobs.fetch(job:ref_number_key) = Level:Benign
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Rapid_Job_Update, Accepted)
    OF ?returns
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?returns, Accepted)
      BRW1.ResetSort(1)
      Check_access('3RD PARTY DESPATCH - RETURNS',x")
      If x" = False
          Case MessageEx('You do not have access to this option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If x" = False
          Start(Return_units,250000)
      End!If x" = False
      
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?returns, Accepted)
    OF ?CountJobs
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CountJobs, Accepted)
      thiswindow.reset(1)
      count# = 0
      setcursor(cursor:wait)
      save_trb_id = access:trdbatch.savefile()
      Case status_temp
          Of 'ALL'
              Case Choice(?CurrentTab)
                  Of 1
                      set(trb:batchonlykey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
                  Of 2
                      access:trdbatch.clearkey(trb:batch_number_key)
                      trb:company_name = company_name_temp
                      set(trb:batch_number_key,trb:batch_number_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:company_name <> company_name_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 3
                      set(trb:esn_only_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
                  Of 4
                      set(trb:authorisationkey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
                  Of 5
                      set(trb:jobnumberkey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          count# += 1
                      end !loop
              End!Case Choice(?CurrentTab)
          Else
              Case Choice(?Currenttab)
                  Of 1
                      access:trdbatch.clearkey(trb:batch_number_status_key)
                      trb:status       = status_temp
                      set(trb:batch_number_status_key,trb:batch_number_status_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status       <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
      
                  Of 2
                      access:trdbatch.clearkey(trb:batch_company_status_key)
                      trb:status       = status_temp
                      trb:company_name = company_name_temp
                      set(trb:batch_company_status_key,trb:batch_company_status_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status       <> status_temp      |
                          or trb:company_name <> company_name_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 3
                      access:trdbatch.clearkey(trb:esn_status_key)
                      trb:status = status_temp
                      set(trb:esn_status_key,trb:esn_status_key)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 4
                      access:trdbatch.clearkey(trb:statusauthkey)
                      trb:status          = status_temp
                      set(trb:statusauthkey,trb:statusauthkey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status          <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
                  Of 5
                      access:trdbatch.clearkey(trb:jobstatuskey)
                      trb:status     = status_temp
                      set(trb:jobstatuskey,trb:jobstatuskey)
                      loop
                          if access:trdbatch.next()
                             break
                          end !if
                          if trb:status     <> status_temp      |
                              then break.  ! end if
                          count# += 1
                      end !loop
              End!Case Choice(?Currenttab)
      End!Case status_temp
      access:trdbatch.restorefile(save_trb_id)
      setcursor()
      
      Case MessageEx('There are '&Clip(count#)&' records.','ServiceBase 2000',|
                     'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
          Of 1 ! &OK Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CountJobs, Accepted)
    OF ?Company_Name_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Company_Name_temp, Accepted)
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Company_Name_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Batch')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab:3{PROP:TEXT} = 'By Batch Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab2{PROP:TEXT} = 'By 3rd Party Agent'
        OF 3
          ?Browse:1{PROP:FORMAT} ='69L(2)|M*~I.M.E.I. No~@s16@#31#37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab3{PROP:TEXT} = 'By I.M.E.I. Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='120L(2)|M*~Auth No~@s30@#46#37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#37R(2)|M*~Job No~@s8@#16#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#'
          ?Tab4{PROP:TEXT} = 'By Authorisation Number'
        OF 5
          ?Browse:1{PROP:FORMAT} ='37R(2)|M*~Job No~@s8@#16#37R(2)|M*~Batch No~L@s8@#1#40R(2)|M*~Date~C(0)@d6@#6#93L(2)|M*~Company Name~@s30@#11#65L(2)|M*~Account Number~@s15@#21#44R(2)|M*~Despatched~@d6@#26#69L(2)|M*~I.M.E.I. No~@s16@#31#69L(2)|M*~M.S.N.~@s16@#36#40R(2)|M*~Recvd~@d6@#41#120L(2)|M*~Auth No~@s30@#46#'
          ?Tab5{PROP:TEXT} = 'By Job Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?Company_Name_temp
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      Free(Queue:Browse:1)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?trb:Batch_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:Batch_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:Batch_Number, Selected)
    OF ?trb:AuthorisationNo
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:AuthorisationNo, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:AuthorisationNo, Selected)
    OF ?trb:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:Ref_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trb:Ref_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F3Key
              Post(Event:Accepted,?Insert)
          Of F4Key
              Post(Event:Accepted,?Insert:2)
          Of F5Key
              Post(Event:Accepted,?Allocate)
          Of F6Key
              Post(Event:Accepted,?Print_Routines)
          Of F7Key
              Post(Event:Accepted,?Rapid_Job_Update)
          Of F8Key
              Post(Event:Accepted,?Returns)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Company_Name_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Company_Name_temp
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = Company_Name_temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'ALL'
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'IN'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'OUT'
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = status_temp
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = Company_Name_temp
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'ALL'
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'IN'
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(status_temp) = 'OUT'
    RETURN SELF.SetSort(15,Force)
  ELSE
    RETURN SELF.SetSort(16,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.trb:Batch_Number_NormalFG = -1
  SELF.Q.trb:Batch_Number_NormalBG = -1
  SELF.Q.trb:Batch_Number_SelectedFG = -1
  SELF.Q.trb:Batch_Number_SelectedBG = -1
  SELF.Q.trb:Date_NormalFG = -1
  SELF.Q.trb:Date_NormalBG = -1
  SELF.Q.trb:Date_SelectedFG = -1
  SELF.Q.trb:Date_SelectedBG = -1
  SELF.Q.trb:Company_Name_NormalFG = -1
  SELF.Q.trb:Company_Name_NormalBG = -1
  SELF.Q.trb:Company_Name_SelectedFG = -1
  SELF.Q.trb:Company_Name_SelectedBG = -1
  SELF.Q.trb:Ref_Number_NormalFG = -1
  SELF.Q.trb:Ref_Number_NormalBG = -1
  SELF.Q.trb:Ref_Number_SelectedFG = -1
  SELF.Q.trb:Ref_Number_SelectedBG = -1
  SELF.Q.job:Account_Number_NormalFG = -1
  SELF.Q.job:Account_Number_NormalBG = -1
  SELF.Q.job:Account_Number_SelectedFG = -1
  SELF.Q.job:Account_Number_SelectedBG = -1
  SELF.Q.jot:DateDespatched_NormalFG = -1
  SELF.Q.jot:DateDespatched_NormalBG = -1
  SELF.Q.jot:DateDespatched_SelectedFG = -1
  SELF.Q.jot:DateDespatched_SelectedBG = -1
  SELF.Q.trb:ESN_NormalFG = -1
  SELF.Q.trb:ESN_NormalBG = -1
  SELF.Q.trb:ESN_SelectedFG = -1
  SELF.Q.trb:ESN_SelectedBG = -1
  SELF.Q.trb:MSN_NormalFG = -1
  SELF.Q.trb:MSN_NormalBG = -1
  SELF.Q.trb:MSN_SelectedFG = -1
  SELF.Q.trb:MSN_SelectedBG = -1
  SELF.Q.jot:DateIn_NormalFG = -1
  SELF.Q.jot:DateIn_NormalBG = -1
  SELF.Q.jot:DateIn_SelectedFG = -1
  SELF.Q.jot:DateIn_SelectedBG = -1
  SELF.Q.trb:AuthorisationNo_NormalFG = -1
  SELF.Q.trb:AuthorisationNo_NormalBG = -1
  SELF.Q.trb:AuthorisationNo_SelectedFG = -1
  SELF.Q.trb:AuthorisationNo_SelectedBG = -1
   
   
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Batch_Number_NormalFG = 255
     SELF.Q.trb:Batch_Number_NormalBG = 16777215
     SELF.Q.trb:Batch_Number_SelectedFG = 16777215
     SELF.Q.trb:Batch_Number_SelectedBG = 255
   ELSE
     SELF.Q.trb:Batch_Number_NormalFG = -1
     SELF.Q.trb:Batch_Number_NormalBG = -1
     SELF.Q.trb:Batch_Number_SelectedFG = -1
     SELF.Q.trb:Batch_Number_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Date_NormalFG = 255
     SELF.Q.trb:Date_NormalBG = 16777215
     SELF.Q.trb:Date_SelectedFG = 16777215
     SELF.Q.trb:Date_SelectedBG = 255
   ELSE
     SELF.Q.trb:Date_NormalFG = -1
     SELF.Q.trb:Date_NormalBG = -1
     SELF.Q.trb:Date_SelectedFG = -1
     SELF.Q.trb:Date_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Company_Name_NormalFG = 255
     SELF.Q.trb:Company_Name_NormalBG = 16777215
     SELF.Q.trb:Company_Name_SelectedFG = 16777215
     SELF.Q.trb:Company_Name_SelectedBG = 255
   ELSE
     SELF.Q.trb:Company_Name_NormalFG = -1
     SELF.Q.trb:Company_Name_NormalBG = -1
     SELF.Q.trb:Company_Name_SelectedFG = -1
     SELF.Q.trb:Company_Name_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:Ref_Number_NormalFG = 255
     SELF.Q.trb:Ref_Number_NormalBG = 16777215
     SELF.Q.trb:Ref_Number_SelectedFG = 16777215
     SELF.Q.trb:Ref_Number_SelectedBG = 255
   ELSE
     SELF.Q.trb:Ref_Number_NormalFG = -1
     SELF.Q.trb:Ref_Number_NormalBG = -1
     SELF.Q.trb:Ref_Number_SelectedFG = -1
     SELF.Q.trb:Ref_Number_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.job:Account_Number_NormalFG = 255
     SELF.Q.job:Account_Number_NormalBG = 16777215
     SELF.Q.job:Account_Number_SelectedFG = 16777215
     SELF.Q.job:Account_Number_SelectedBG = 255
   ELSE
     SELF.Q.job:Account_Number_NormalFG = -1
     SELF.Q.job:Account_Number_NormalBG = -1
     SELF.Q.job:Account_Number_SelectedFG = -1
     SELF.Q.job:Account_Number_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.jot:DateDespatched_NormalFG = 255
     SELF.Q.jot:DateDespatched_NormalBG = 16777215
     SELF.Q.jot:DateDespatched_SelectedFG = 16777215
     SELF.Q.jot:DateDespatched_SelectedBG = 255
   ELSE
     SELF.Q.jot:DateDespatched_NormalFG = -1
     SELF.Q.jot:DateDespatched_NormalBG = -1
     SELF.Q.jot:DateDespatched_SelectedFG = -1
     SELF.Q.jot:DateDespatched_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:ESN_NormalFG = 255
     SELF.Q.trb:ESN_NormalBG = 16777215
     SELF.Q.trb:ESN_SelectedFG = 16777215
     SELF.Q.trb:ESN_SelectedBG = 255
   ELSE
     SELF.Q.trb:ESN_NormalFG = -1
     SELF.Q.trb:ESN_NormalBG = -1
     SELF.Q.trb:ESN_SelectedFG = -1
     SELF.Q.trb:ESN_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:MSN_NormalFG = 255
     SELF.Q.trb:MSN_NormalBG = 16777215
     SELF.Q.trb:MSN_SelectedFG = 16777215
     SELF.Q.trb:MSN_SelectedBG = 255
   ELSE
     SELF.Q.trb:MSN_NormalFG = -1
     SELF.Q.trb:MSN_NormalBG = -1
     SELF.Q.trb:MSN_SelectedFG = -1
     SELF.Q.trb:MSN_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.jot:DateIn_NormalFG = 255
     SELF.Q.jot:DateIn_NormalBG = 16777215
     SELF.Q.jot:DateIn_SelectedFG = 16777215
     SELF.Q.jot:DateIn_SelectedBG = 255
   ELSE
     SELF.Q.jot:DateIn_NormalFG = -1
     SELF.Q.jot:DateIn_NormalBG = -1
     SELF.Q.jot:DateIn_SelectedFG = -1
     SELF.Q.jot:DateIn_SelectedBG = -1
   END
   IF (trb:Exchanged = 'YES')
     SELF.Q.trb:AuthorisationNo_NormalFG = 255
     SELF.Q.trb:AuthorisationNo_NormalBG = 16777215
     SELF.Q.trb:AuthorisationNo_SelectedFG = 16777215
     SELF.Q.trb:AuthorisationNo_SelectedBG = 255
   ELSE
     SELF.Q.trb:AuthorisationNo_NormalFG = -1
     SELF.Q.trb:AuthorisationNo_NormalBG = -1
     SELF.Q.trb:AuthorisationNo_SelectedFG = -1
     SELF.Q.trb:AuthorisationNo_SelectedBG = -1
   END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

