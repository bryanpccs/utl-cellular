

   MEMBER('celraptp.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA009.INC'),ONCE        !Req'd for module callout resolution
                     END


Despatch_Units PROCEDURE (f_type)                     !Generated from procedure template - Window

ThisThreadActive BYTE
Company_Name_temp    STRING(30)
tmp:type             STRING(3)
tmp:OriginalIMEI     STRING(30)
tmp:OriginalMSN      STRING(30)
save_trb_ali_id      USHORT,AUTO
save_trb_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
Job_Number_Temp      STRING(20)
esn_temp             STRING(30)
batch_number_temp    LONG
tmp:beginbatch       BYTE(0)
tmp:recordcount      LONG
tmp:jobsqueue        QUEUE,PRE(jobque)
job_number           LONG
RecordNumber         LONG
                     END
DisplayString        STRING(255)
tmp:JobNumber        LONG
tmp:passfail         STRING(40)
tmp:addbatch         BYTE(0)
tmp:itemsinbatch     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:CurrentBatchNumber LONG
tmp:Exchanged        BYTE(0)
tmp:msn              STRING(30)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Company_Name_temp
trd:Company_Name       LIKE(trd:Company_Name)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB1::View:FileDropCombo VIEW(TRDPARTY)
                       PROJECT(trd:Company_Name)
                     END
BRW5::View:Browse    VIEW(JOBACC)
                       PROJECT(jac:Accessory)
                       PROJECT(jac:Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
jac:Accessory          LIKE(jac:Accessory)            !List box control field - type derived from field
jac:Ref_Number         LIKE(jac:Ref_Number)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Inserting A 3rd Party Batch'),AT(,,351,259),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F10Key),GRAY,DOUBLE
                       PROMPT('3rd Party Agent'),AT(8,20),USE(?company_name_temp:prompt)
                       COMBO(@s30),AT(84,20,124,10),USE(Company_Name_temp),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       SHEET,AT(4,4,208,88),USE(?Sheet1),SPREAD
                         TAB('3rd Party Batch'),USE(?New_Tab),HIDE
                           PROMPT('Select a 3rd Party Agent and then tick ''Begin Batch''.'),AT(8,40,200,16),USE(?Prompt4),CENTER,FONT('Tahoma',8,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           CHECK('Begin Batch'),AT(84,64),USE(tmp:beginbatch),VALUE('1','0')
                         END
                         TAB('Add To 3rd Party Batch'),USE(?Add_Tab),HIDE
                           PROMPT('Batch Number'),AT(8,36),USE(?batch_number_temp:Prompt)
                           ENTRY(@s4),AT(84,36,64,10),USE(batch_number_temp),LEFT(1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Select a 3rd Party Agent and a Batch Number, then click ''Add To Batch'' to begin ' &|
   'adding to the selected Batch.'),AT(8,48,200,28),USE(?Prompt8),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           CHECK('Add To Batch'),AT(84,76),USE(tmp:addbatch),MSG('Add to Existing Batch'),TIP('Add to Existing Batch'),VALUE('1','0')
                         END
                       END
                       SHEET,AT(216,4,132,88),USE(?Sheet3),SPREAD
                         TAB('Accessories'),USE(?Tab3)
                           LIST,AT(220,20,124,68),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Accessory~@s30@'),FROM(Queue:Browse)
                         END
                       END
                       SHEET,AT(4,96,208,132),USE(?Sheet2),WIZARD,SPREAD
                         TAB('E.S.N. / I.M.E.I.'),USE(?Tab2)
                           PROMPT('Insert the Job Number and E.S.N. / I.M.E.I. of the Job being sent to the 3rd Par' &|
   'ty Agent, and then select one of the three options below.'),AT(8,100,196,24),USE(?Prompt5),FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Batch Number'),AT(8,128),USE(?Prompt6)
                           STRING(@s8),AT(84,128),USE(tmp:CurrentBatchNumber),HIDE,LEFT,FONT(,,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           STRING(@s8),AT(84,140),USE(tmp:itemsinbatch),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Batch Limit'),AT(8,152),USE(?Prompt10)
                           STRING(@s8),AT(84,152),USE(trd:BatchLimit),FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Total In Batch'),AT(8,140),USE(?Prompt6:2)
                           PROMPT('Job Number'),AT(8,164),USE(?Job_Number_Temp:Prompt)
                           ENTRY(@s20),AT(84,164,64,10),USE(Job_Number_Temp),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('I.M.E.I. Number'),AT(8,180),USE(?esn_temp:Prompt),TRN
                           ENTRY(@s30),AT(84,180,124,10),USE(esn_temp),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           PROMPT('M.S.N.'),AT(8,196),USE(?tmp:msn:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,196,124,10),USE(tmp:msn),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('M.S.N.'),TIP('M.S.N.'),REQ,UPR
                           STRING(@s40),AT(8,212,200,12),USE(tmp:passfail),FONT(,10,,FONT:bold)
                         END
                       END
                       SHEET,AT(216,96,132,132),USE(?Sheet4),SPREAD
                         TAB('Jobs Updated Successfully'),USE(?Tab4)
                           LIST,AT(220,112,124,112),USE(?List2),VSCROLL,FORMAT('56L(2)|M~Job Number~@s8@'),FROM(tmp:jobsqueue)
                         END
                       END
                       PANEL,AT(4,232,344,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&Finish [F10]'),AT(288,236,56,16),USE(?Finish),LEFT,ICON('thumbs.gif')
                       GROUP,AT(4,232,272,20),USE(?Button_Group),DISABLE
                         BUTTON('E&xchange [F5]'),AT(8,236,84,16),USE(?Exchange),LEFT,ICON('stock_br.gif')
                         BUTTON('&Retain ALL Accessories [F6]'),AT(96,236,84,16),USE(?accessories_retained),LEFT,ICON('stock_qu.gif')
                         BUTTON('&Despatched [F7]'),AT(184,236,84,16),USE(?Despatched),LEFT,ICON('desp_sm.gif')
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ld                   CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?company_name_temp:prompt{prop:FontColor} = -1
    ?company_name_temp:prompt{prop:Color} = 15066597
    If ?Company_Name_temp{prop:ReadOnly} = True
        ?Company_Name_temp{prop:FontColor} = 65793
        ?Company_Name_temp{prop:Color} = 15066597
    Elsif ?Company_Name_temp{prop:Req} = True
        ?Company_Name_temp{prop:FontColor} = 65793
        ?Company_Name_temp{prop:Color} = 8454143
    Else ! If ?Company_Name_temp{prop:Req} = True
        ?Company_Name_temp{prop:FontColor} = 65793
        ?Company_Name_temp{prop:Color} = 16777215
    End ! If ?Company_Name_temp{prop:Req} = True
    ?Company_Name_temp{prop:Trn} = 0
    ?Company_Name_temp{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?New_Tab{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?tmp:beginbatch{prop:Font,3} = -1
    ?tmp:beginbatch{prop:Color} = 15066597
    ?tmp:beginbatch{prop:Trn} = 0
    ?Add_Tab{prop:Color} = 15066597
    ?batch_number_temp:Prompt{prop:FontColor} = -1
    ?batch_number_temp:Prompt{prop:Color} = 15066597
    If ?batch_number_temp{prop:ReadOnly} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 15066597
    Elsif ?batch_number_temp{prop:Req} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 8454143
    Else ! If ?batch_number_temp{prop:Req} = True
        ?batch_number_temp{prop:FontColor} = 65793
        ?batch_number_temp{prop:Color} = 16777215
    End ! If ?batch_number_temp{prop:Req} = True
    ?batch_number_temp{prop:Trn} = 0
    ?batch_number_temp{prop:FontStyle} = font:Bold
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    ?tmp:addbatch{prop:Font,3} = -1
    ?tmp:addbatch{prop:Color} = 15066597
    ?tmp:addbatch{prop:Trn} = 0
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?tmp:CurrentBatchNumber{prop:FontColor} = -1
    ?tmp:CurrentBatchNumber{prop:Color} = 15066597
    ?tmp:itemsinbatch{prop:FontColor} = -1
    ?tmp:itemsinbatch{prop:Color} = 15066597
    ?Prompt10{prop:FontColor} = -1
    ?Prompt10{prop:Color} = 15066597
    ?trd:BatchLimit{prop:FontColor} = -1
    ?trd:BatchLimit{prop:Color} = 15066597
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?Job_Number_Temp:Prompt{prop:FontColor} = -1
    ?Job_Number_Temp:Prompt{prop:Color} = 15066597
    If ?Job_Number_Temp{prop:ReadOnly} = True
        ?Job_Number_Temp{prop:FontColor} = 65793
        ?Job_Number_Temp{prop:Color} = 15066597
    Elsif ?Job_Number_Temp{prop:Req} = True
        ?Job_Number_Temp{prop:FontColor} = 65793
        ?Job_Number_Temp{prop:Color} = 8454143
    Else ! If ?Job_Number_Temp{prop:Req} = True
        ?Job_Number_Temp{prop:FontColor} = 65793
        ?Job_Number_Temp{prop:Color} = 16777215
    End ! If ?Job_Number_Temp{prop:Req} = True
    ?Job_Number_Temp{prop:Trn} = 0
    ?Job_Number_Temp{prop:FontStyle} = font:Bold
    ?esn_temp:Prompt{prop:FontColor} = -1
    ?esn_temp:Prompt{prop:Color} = 15066597
    If ?esn_temp{prop:ReadOnly} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 15066597
    Elsif ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 8454143
    Else ! If ?esn_temp{prop:Req} = True
        ?esn_temp{prop:FontColor} = 65793
        ?esn_temp{prop:Color} = 16777215
    End ! If ?esn_temp{prop:Req} = True
    ?esn_temp{prop:Trn} = 0
    ?esn_temp{prop:FontStyle} = font:Bold
    ?tmp:msn:Prompt{prop:FontColor} = -1
    ?tmp:msn:Prompt{prop:Color} = 15066597
    If ?tmp:msn{prop:ReadOnly} = True
        ?tmp:msn{prop:FontColor} = 65793
        ?tmp:msn{prop:Color} = 15066597
    Elsif ?tmp:msn{prop:Req} = True
        ?tmp:msn{prop:FontColor} = 65793
        ?tmp:msn{prop:Color} = 8454143
    Else ! If ?tmp:msn{prop:Req} = True
        ?tmp:msn{prop:FontColor} = 65793
        ?tmp:msn{prop:Color} = 16777215
    End ! If ?tmp:msn{prop:Req} = True
    ?tmp:msn{prop:Trn} = 0
    ?tmp:msn{prop:FontStyle} = font:Bold
    ?tmp:passfail{prop:FontColor} = -1
    ?tmp:passfail{prop:Color} = 15066597
    ?Sheet4{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    ?List2{prop:FontColor} = 65793
    ?List2{prop:Color}= 16777215
    ?List2{prop:Color,2} = 16777215
    ?List2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?Button_Group{prop:Font,3} = -1
    ?Button_Group{prop:Color} = 15066597
    ?Button_Group{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
FinalValidation     Routine
    trb:exchanged = 'NO'
    error# = 0
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = job_number_temp
    if access:jobs.tryfetch(job:ref_number_key)
        error# = 1
        Case MessageEx('Unable to find selected Job Number.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    Else!if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
        If job:date_completed <> ''
            Case MessageEx('The selected Job Number has been Completed.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                Of 1 ! &OK Button
            End!Case MessageEx
            error# = 1
        End!If job:date_completed <> ''
        !Check if the trade account is allow to despatch to third party
        Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
        sub:Account_Number  = job:Account_Number
        If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Found
            Access:TRADEACC.Clearkey(tra:Account_Number_Key)
            tra:Account_Number  = sub:Main_Account_Number
            If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Found
                If tra:StopThirdParty
                    Case MessageEx('Error! Cannot send units to a Third Party for this job''s Trade Account.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End !If tra:StopThirdParty
            Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
            
        Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
        
        If error# = 0
            If job:esn <> esn_temp
                Access:EXCHANGE.ClearKey(xch:Esn_Only_Key)
                xch:esn = esn_temp
                If Access:EXCHANGE.TryFetch(xch:Esn_Only_Key) = Level:Benign
                    Case MessageEx('Error! You have entered the I.M.E.I. Number of the Exchange Unit attached to this job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                Else!if access:exchange.tryfetch(xch:esn_only_key) = Level:Benign
                    Access:Loan.clearkey(loa:esn_only_key)
                    loa:esn = esn_temp
                    if access:loan.tryfetch(loa:esn_only_key) = Level:Benign
                        Case MessageEx('Error! You have entered the I.M.E.I. Number of the Loan Unit attached to this job.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    Else!if access:loan.tryfetch(loa:esn_only_key) = Level:Benign
                        Case MessageEx('Error! Incorrect I.M.E.I. Number entered for this Job Number.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!if access:loan.tryfetch(loa:esn_only_key) = Level:Benign
                End!if access:exchange.tryfetch(xch:esn_only_key) = Level:Benign
            Else!If job:esn <> esn_temp
                If ?tmp:msn{prop:hide} = 0 And job:msn <> tmp:msn
                    Access:EXCHANGE.Clearkey(xch:MSN_Only_Key)
                    xch:msn = tmp:msn
                    If Access:EXCHANGE.TryFetch(xch:MSN_Only_Key) = Level:Benign
                        Case MessageEx('Error! You have entered the M.S.N. of the Exchange Unit attached to this job.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    Else !If Access:EXCHANGE.TryFetch(xch:MSN_Only_Key) = Level:Benign
                        Access:LOAN.Clearkey(loa:MSN_Only_Key)
                        loa:MSN = tmp:MSN
                        If Access:LOAN.TryFetch(loa:MSN_Only_Key) = Level:Benign
                            Case MessageEx('Error! You have entered the M.S.N. of the Loan Unit attached to this job.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            Error# = 1
                        Else !If Access:LOAN.TryFetch(loa:MSN_Only_Key) = Level:Benign
                            Case MessageEx('Incorrect M.S.N. entered for this Job Number.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            Error# = 1
                        End !If Access:LOAN.TryFetch(loa:MSN_Only_Key) = Level:Benign
                    End !If Access:EXCHANGE.TryFetch(xch:MSN_Only_Key) = Level:Benign
                Else !If ?tmp:msn{prop:hide} = 0 And job:msn <> tmp:msn
                    access:trdmodel.clearkey(trm:model_number_key)
                    trm:company_name = company_name_temp
                    trm:model_number = job:model_number
                    if access:trdmodel.tryfetch(trm:model_number_key)
                        Case MessageEx('The selected 3rd Party Agent has not been authorised to repair this Model Number.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        error# = 1
                    Else!if access:trdmodel.tryfetch(trm:model_number_key)
                        found# = 0
                        save_trb_ali_id = access:trdbatch_alias.savefile()
                        access:trdbatch_alias.clearkey(trb_ali:esn_only_key)
                        trb_ali:esn = esn_temp
                        set(trb_ali:esn_only_key,trb_ali:esn_only_key)
                        loop
                            if access:trdbatch_alias.next()
                               break
                            end !if
                            if trb_ali:esn <> esn_temp      |
                                then break.  ! end if
                            found# = 1
                            Break
        !                    If trb_ali:recordnumber <> trb:recordnumber
        !                        found# = 1
        !                        Break
        !                    End!If trb_ali:record_number <> trb:record_number
                        end !loop
                        access:trdbatch_alias.restorefile(save_trb_ali_id)

                        If found# = 1
                            Case MessageEx('This selected I.M.E.I. Number has already been previously entered before. <13,10><13,10>Do you want to continue, or re-enter the I.M.E.I. Number?','ServiceBase 2000',|
                                           'Styles\question.ico','|&Continue|&Re-Enter',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                Of 1 ! &Continue Button
                                Of 2 ! &Re-Enter Button
                                    error# = 1
                            End!Case MessageEx
                        End!If found# = 1
                        If error# = 0
                            Enable(?button_group)
                            tmp:JobNumber = job:ref_number
                            brw5.resetsort(1)
                        End!If found# = 1
                    End!if access:trdmodel.tryfetch(trm:model_number_key)
                End !If ?tmp:msn{prop:hide} = 0 And job:msn <> tmp:msn

            End!If job:esn <> esn_temp

        End!If error# = 0
        If error# = 1
            Select(?esn_temp)
            tmp:passfail = 'Job Number ' & Clip(job_number_temp) & ' Updated Failed'
            ?tmp:passfail{prop:fontcolor} = color:red

        End!If error# = 1
    End!if access:jobs.tryfetch(job:ref_number_key) = Level:Benign
       


Despatch        Routine
    If Access:TRDBATCH.Primerecord() = Level:Benign
!        Stop('After Prime: ' & trb:RecordNumber & '<13,10>Job Number: ' & job:Ref_Number)
        job:Workshop = 'NO'
        job:Third_Party_Site    = Company_Name_Temp
        GetStatus(410,1,'JOB') !Despatched To 3rd Party
        If tmp:Exchanged = 1
            trb:Exchanged = 'YES'
            If job:Exchange_Unit_Number = ''
                GetStatus(108,1,'EXC') !Exchange Unit Required
            End !If job:Exchange_Unit_Number <> ''
        Else !If tmp:Exchanged = 1
            trb:Exchanged = 'NO'
        End !If tmp:Exchanged = 1

        Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey)
            !If A Jobnotes entry doesn't not already exist, create one.
            If Access:JOBNOTES.Primerecord() = Level:Benign
                jbn:RefNumber   = job:Ref_Number
                If Access:JOBNOTES.Tryinsert()
                    Access:JOBNOTES.Cancelautoinc()
                End!If Access:JOBNOTES.Tryinsert()
                Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                jbn:RefNumber   = job:Ref_Number
                If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                    !Found
                End! If Access:.Tryfetch(jbn:RefNumberKey) = Level:Benign
          End!If Access:JOBNOTES.Primerecord() = Level:Benign
        End! If Access:.Tryfetch(jbn:RefNumberKey) = Level:Benign
        Clear(glo:Queue)
        Free(glo:Queue)
        Case tmp:type
            Of 'DES'
                If Records(Queue:Browse)
                    PickJobAccessories(job:Ref_Number)
                    If Records(Queue:Browse) <> Records(glo:Queue)
                        If jbn:Engineers_Notes = ''
                            jbn:Engineers_Notes = '<13,10>' & Records(glo:Queue) & ' ACCESSORIES DESPATCHED WITH UNIT'
                        Else!If job:engineers_notes = ''
                            jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>' & Records(glo:Queue) & ' ACCESSORIES DESPATCHED WITH UNIT'
                        End!If job:engineers_notes = ''

                    Else !If Records(Queue:Browse) <> Records(glo:Queue)
                        If jbn:Engineers_Notes = ''
                            jbn:Engineers_Notes = '<13,10>ACCESSORIES DESPATCHED WITH UNIT'
                        Else!If job:engineers_notes = ''
                            jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>ACCESSORIES DESPATCHED WITH UNIT'
                        End!If job:engineers_notes = ''

                    End !If Records(Queue:Browse) <> Records(glo:Queue)
                End !If Records(Queue:Browse)
                
            Of 'RET'
                If jbn:Engineers_Notes = ''
                    jbn:Engineers_Notes = '<13,10>ACCESSORIES RETAINED'
                Else!If job:engineers_notes = ''
                    jbn:Engineers_Notes = Clip(jbn:Engineers_Notes) & '<13,10>ACCESSORIES RETAINED'
                End!If job:engineers_notes = ''
        End!Case tmp:type

        access:JOBNOTES.Update()

        If Access:JOBS.TryUpdate() = Level:Benign
            tmp:OriginalIMEI    = job:Esn
            tmp:OriginalMSN     = job:MSN

            !See if a previous entry exists in the JOBTHIRD file for this job
            !If so, then take the Original IMEI Number. So every entry has the same
            !Original IMEI Number.
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT() = Level:Benign
                If jot:RefNumber = job:Ref_Number
                    error# = 0
                    tmp:OriginalIMEI    = jot:OriginalIMEI
                    tmp:OriginalMSN     = jot:OriginalMSN
                End!If jot:RefNumber = job:RefNumber
            End!If Access:JOBTHIRD.NEXT() = Level:Benign

            Access:JOBTHIRD.ClearKey(jot:ThirdPartyKey)
            jot:ThirdPartyNumber = trb:RecordNumber
            If Access:JOBTHIRD.TryFetch(jot:ThirdPartyKey) = Level:Benign
               ASSERT(0,'<13,10>About to create a duplicate JOBTHIRD value '&|
                        '<13,10>(Job Number: ' & Clip(job:Ref_Number) &')'& |
                        '<13,10>(Record Number: ' & Clip(trb:RecordNumber) & ')<13,10>')
            End

            !Create a new entry in the JOBTHIRD file for this despatch
            If Access:JOBTHIRD.Primerecord() = Level:Benign
                jot:RefNumber       = job:Ref_Number
                jot:OriginalIMEI    = tmp:OriginalIMEI
                jot:OriginalMSN     = tmp:OriginalMSN
                jot:OutIMEI         = ESN_Temp
                jot:OutMSN          = tmp:MSN
                jot:InIMEI          = ''
                jot:DateOut         = Today()
                jot:DateIn          = ''
                jot:ThirdPartyNumber    = trb:RecordNumber
                If Access:JOBTHIRD.Tryinsert()
                    Access:JOBTHIRD.Cancelautoinc()
                End!If Access:JOBTHIRD.Tryinsert()
            End!If Access:JOBTHIRD.Primerecord() = Level:Benign



            !Create an entry in the AUDIT trail
            If Access:AUDIT.PrimeRecord() = Level:Benign
                aud:Notes         = '3RD PARTY AGENT: ' & Clip(Company_Name_Temp) & '<13,10>BATCH NUMBER: ' & Clip(tmp:CurrentBatchNumber)
                If trb:Exchanged = 'YES'
                    aud:Notes = Clip(aud:Notes) & '<13,10,13,10>EXCHANGE UNIT REQUIRED'
                End!If trb:exchanged = 'YES'

                !Check if there are any accessories attached to this job
                !If so, make an entry in the AUDIT trail listing them.
                aud:notes = Clip(aud:notes) & '<13,10,13,10>ACCESSORIES RETAINED WITH UNIT:'
                acc_count# = 0
                Save_jac_ID = Access:JOBACC.SaveFile()
                Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = job:Ref_Number
                Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                Loop
                    If Access:JOBACC.NEXT()
                       Break
                    End !If
                    If jac:Ref_Number <> job:Ref_Number      |
                        Then Break.  ! End If
                        Case tmp:Type
                            Of 'RET'
                                aud:notes = Clip(aud:notes) & '<13,10>' & Clip(jac:accessory)
                                acc_count# += 1

                            Of 'DES'
                                Sort(glo:Queue,glo:Pointer)
                                glo:Pointer = jac:Accessory
                                Get(glo:Queue,glo:Pointer)
                                If Error()
                                    aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
                                    acc_count# += 1

                                End !If ~Error()>
                        End!Case tmp:Type

                End !Loop
                Access:JOBACC.RestoreFile(Save_jac_ID)

                If tmp:Type = 'DES'

                    aud:notes = Clip(aud:notes) & '<13,10,13,10>ACCESSORIES DESPATCHED WITH UNIT:'
                    Save_jac_ID = Access:JOBACC.SaveFile()
                    Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                    jac:Ref_Number = job:Ref_Number
                    Set(jac:Ref_Number_Key,jac:Ref_Number_Key)
                    Loop
                        If Access:JOBACC.NEXT()
                           Break
                        End !If
                        If jac:Ref_Number <> job:Ref_Number      |
                            Then Break.  ! End If
                        Sort(glo:Queue,glo:Pointer)
                        glo:Pointer = jac:Accessory
                        Get(glo:Queue,glo:Pointer)
                        If ~Error()
                            aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(jac:Accessory)
                        End !If ~Error()>

                    End !Loop
                    Access:JOBACC.RestoreFile(Save_jac_ID)
                End !If tmp:Type = 'DES'

                aud:Ref_Number    = job:Ref_Number
                aud:Date          = today()
                aud:Time          = clock()
                Access:USERS.ClearKey(use:Password_Key)
                use:Password = glo:Password
                Access:USERS.Fetch(use:Password_Key)
                aud:User = use:User_Code
                aud:Action        = '3RD PARTY AGENT: UNIT SENT'
                Access:AUDIT.Insert()
            end!�if access:audit.primerecord() = level:benign
            trb:Batch_Number    = tmp:CurrentBatchNumber
            trb:Ref_Number      = job_number_temp
            trb:Status          = 'OUT'
            trb:Company_Name    = Company_Name_Temp
            trb:ESN             = esn_temp
            trb:MSN             = tmp:MSN
            trb:Date            = Today()
            trb:Time            = Clock()
            If Access:TRDBATCH.Tryinsert()
                Access:TRDBATCH.Cancelautoinc()
            End!If Access:TRDBATCH.Tryinsert()
            !Write the entry into the Trade Batch File

            tmp:passfail = 'Job Number ' & Clip(job_number_temp) & ' Updated Successfully'
            ?tmp:passfail{prop:fontcolor} = color:green
            tmp:recordcount += 1
            jobque:job_number = job_number_temp
            jobque:RecordNumber = tmp:recordcount
            Add(tmp:jobsqueue)
            Sort(tmp:jobsqueue,-jobque:RecordNumber)

            !Print 3rd Party Despatch Note
            Set(DEFAULTS)
            access:DEFAULTS.next()
            Set(DEFAULT2)
            Access:DEFAULT2.Next()
            glo:select1 = job_number_temp
            glo:select2  = trb:batch_number
            glo:select3  = Company_Name_Temp
            If DEF:ThirdPartyNote = 'YES'
                Third_Party_Single_Despatch(tmp:type)
            End!If DEF:ThirdPartyNote = 'YES'
            If de2:PrintRetainedAccLabel
                If acc_count# <> 0
                    Retained_Accessories_Label
                End!If acc_count# <> 0
            End !If de2:PrintRetainedAccLabel
            glo:select1  = ''
            glo:select2  = ''
            glo:select3  = ''
            Clear(glo:Queue)
            Free(glo:Queue)

            Disable(?button_group)
            job_number_temp = ''
            esn_temp = ''
            tmp:msn = ''
            ?tmp:msn{prop:hide} = 1
            ?tmp:msn:prompt{prop:hide} = 1
            Select(?job_number_temp)
            tmp:jobnumber = ''
            brw5.resetsort(1)
            Display()


            
        End!If access:jobs.tryupdate() = Level:Benign
        Do count_batch
    End!If Access:TRDBATCH.Primerecord() = Level:Benign
Count_Batch         Routine
    Setcursor(cursor:wait)
    tmp:ItemsInBatch = 0
    Save_trb_ali_Id = Access:TRDBATCH_ALIAS.Savefile()
    Access:TRDBATCH_ALIAS.Clearkey(trb_ali:Company_Batch_Esn_Key)
    trb_ali:Company_Name = Company_Name_Temp
    trb_ali:Batch_Number = tmp:CurrentBatchNumber
    Set(trb_ali:Company_Batch_Esn_Key,trb_ali:Company_Batch_Esn_Key)
    Loop
        If Access:TRDBATCH_ALIAS.Next()
           Break
        End !if
        If trb_ali:Company_Name <> trb:Company_Name      |
        Or trb_ali:Batch_Number <> tmp:CurrentBatchNumber      |
            then Break.  ! end if
        tmp:ItemsInBatch += 1
    end !loop
    Access:TRDBATCH_ALIAS.Restorefile(Save_trb_ali_Id)
    Setcursor()
    If tmp:ItemsInBatch > trd:BatchLimit and trd:BatchLimit <> 0
        Case MessageEx('You have exceeded the batch limit for this Third Party Repairer.<13,10><13,10>Do you want to continue to add jobs to this batch?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
            Of 2 ! &No Button
                Post(event:accepted,?finish)
        End!Case MessageEx
    End!If tmp:itemsinbatch => trd:batchlimit


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Despatch_Units',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Company_Name_temp',Company_Name_temp,'Despatch_Units',1)
    SolaceViewVars('tmp:type',tmp:type,'Despatch_Units',1)
    SolaceViewVars('tmp:OriginalIMEI',tmp:OriginalIMEI,'Despatch_Units',1)
    SolaceViewVars('tmp:OriginalMSN',tmp:OriginalMSN,'Despatch_Units',1)
    SolaceViewVars('save_trb_ali_id',save_trb_ali_id,'Despatch_Units',1)
    SolaceViewVars('save_trb_id',save_trb_id,'Despatch_Units',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Despatch_Units',1)
    SolaceViewVars('Job_Number_Temp',Job_Number_Temp,'Despatch_Units',1)
    SolaceViewVars('esn_temp',esn_temp,'Despatch_Units',1)
    SolaceViewVars('batch_number_temp',batch_number_temp,'Despatch_Units',1)
    SolaceViewVars('tmp:beginbatch',tmp:beginbatch,'Despatch_Units',1)
    SolaceViewVars('tmp:recordcount',tmp:recordcount,'Despatch_Units',1)
    SolaceViewVars('tmp:jobsqueue:job_number',tmp:jobsqueue:job_number,'Despatch_Units',1)
    SolaceViewVars('tmp:jobsqueue:RecordNumber',tmp:jobsqueue:RecordNumber,'Despatch_Units',1)
    SolaceViewVars('DisplayString',DisplayString,'Despatch_Units',1)
    SolaceViewVars('tmp:JobNumber',tmp:JobNumber,'Despatch_Units',1)
    SolaceViewVars('tmp:passfail',tmp:passfail,'Despatch_Units',1)
    SolaceViewVars('tmp:addbatch',tmp:addbatch,'Despatch_Units',1)
    SolaceViewVars('tmp:itemsinbatch',tmp:itemsinbatch,'Despatch_Units',1)
    SolaceViewVars('tmp:CurrentBatchNumber',tmp:CurrentBatchNumber,'Despatch_Units',1)
    SolaceViewVars('tmp:Exchanged',tmp:Exchanged,'Despatch_Units',1)
    SolaceViewVars('tmp:msn',tmp:msn,'Despatch_Units',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?company_name_temp:prompt;  SolaceCtrlName = '?company_name_temp:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Company_Name_temp;  SolaceCtrlName = '?Company_Name_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?New_Tab;  SolaceCtrlName = '?New_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:beginbatch;  SolaceCtrlName = '?tmp:beginbatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Add_Tab;  SolaceCtrlName = '?Add_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?batch_number_temp:Prompt;  SolaceCtrlName = '?batch_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?batch_number_temp;  SolaceCtrlName = '?batch_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:addbatch;  SolaceCtrlName = '?tmp:addbatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:CurrentBatchNumber;  SolaceCtrlName = '?tmp:CurrentBatchNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:itemsinbatch;  SolaceCtrlName = '?tmp:itemsinbatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt10;  SolaceCtrlName = '?Prompt10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:BatchLimit;  SolaceCtrlName = '?trd:BatchLimit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:2;  SolaceCtrlName = '?Prompt6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job_Number_Temp:Prompt;  SolaceCtrlName = '?Job_Number_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job_Number_Temp;  SolaceCtrlName = '?Job_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp:Prompt;  SolaceCtrlName = '?esn_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?esn_temp;  SolaceCtrlName = '?esn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:msn:Prompt;  SolaceCtrlName = '?tmp:msn:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:msn;  SolaceCtrlName = '?tmp:msn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:passfail;  SolaceCtrlName = '?tmp:passfail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List2;  SolaceCtrlName = '?List2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button_Group;  SolaceCtrlName = '?Button_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange;  SolaceCtrlName = '?Exchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessories_retained;  SolaceCtrlName = '?accessories_retained';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Despatched;  SolaceCtrlName = '?Despatched';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Despatch_Units')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Despatch_Units')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?company_name_temp:prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Despatch_Units'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Relate:STAHEAD.Open
  Relate:TRDBATCH_ALIAS.Open
  Access:TRDBATCH.UseFile
  Access:JOBS.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:TRDPARTY.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBTHIRD.UseFile
  Access:MANUFACT.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  SELF.FilesOpened = True
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:JOBACC,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,jac:Ref_Number_Key)
  BRW5.AddRange(jac:Ref_Number,tmp:JobNumber)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,jac:Accessory,1,BRW5)
  BRW5.AddField(jac:Accessory,BRW5.Q.jac:Accessory)
  BRW5.AddField(jac:Ref_Number,BRW5.Q.jac:Ref_Number)
  IF ?tmp:beginbatch{Prop:Checked} = True
    UNHIDE(?tmp:CurrentBatchNumber)
    ENABLE(?Sheet3)
    ENABLE(?Sheet2)
    ENABLE(?Sheet4)
    DISABLE(?Sheet1)
    DISABLE(?company_name_temp:prompt)
    DISABLE(?Company_Name_temp)
  END
  IF ?tmp:beginbatch{Prop:Checked} = False
    DISABLE(?Sheet2)
    DISABLE(?Sheet3)
    DISABLE(?Sheet4)
  END
  IF ?tmp:addbatch{Prop:Checked} = True
    UNHIDE(?tmp:CurrentBatchNumber)
    ENABLE(?Sheet3)
    ENABLE(?Sheet2)
    ENABLE(?Sheet4)
    DISABLE(?Sheet1)
    DISABLE(?company_name_temp:prompt)
    DISABLE(?Company_Name_temp)
  END
  IF ?tmp:addbatch{Prop:Checked} = False
    DISABLE(?Sheet3)
    DISABLE(?Sheet4)
    DISABLE(?Sheet2)
  END
  ld.Init(Company_Name_temp,?Company_Name_temp,Queue:FileDropCombo.ViewPosition,FDCB1::View:FileDropCombo,Queue:FileDropCombo,Relate:TRDPARTY,ThisWindow,GlobalErrors,0,1,0)
  ld.Q &= Queue:FileDropCombo
  ld.AddSortOrder(trd:Company_Name_Key)
  ld.AddField(trd:Company_Name,ld.Q.trd:Company_Name)
  ThisWindow.AddItem(ld.WindowComponent)
  ld.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
    Relate:STAHEAD.Close
    Relate:TRDBATCH_ALIAS.Close
  END
  IF ThisThreadActive
    ThreadQ.Proc='Despatch_Units'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Despatch_Units',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tmp:beginbatch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:beginbatch, Accepted)
      IF tmp:beginbatch = 1
          Case MessageEx('You are about to create a NEW batch.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
      
                  Access:TRDPARTY.Clearkey(trd:Company_Name_Key)
                  trd:Company_Name    = Company_Name_Temp
                  If Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign
                      !Found
                      trd:Batch_Number += 1
                      tmp:CurrentBatchNumber = trd:Batch_Number
                      Do count_batch
                      Access:TRDPARTY.Update()
                  Else! If Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign
                      !Error
                      Case MessageEx('Cannot create a new Batch number for this Third Party Site.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      Post(Event:CloseWindow)
                  End! If Access:TRDPARTY.Tryfetch(trd:Company_Name_Key) = Level:Benign
      
      
              Of 2 ! &No Button
                  tmp:beginbatch = 0
          End!Case MessageEx
      End!IF tmp:beginbatch = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:beginbatch, Accepted)
    OF ?tmp:addbatch
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:addbatch, Accepted)
      If tmp:AddBatch = 1
          Case MessageEx('You are about to add an EXISTING batch.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  Error# = 0
                  Access:TRDBATCH.Clearkey(trb:Batch_Number_Key)
                  trb:Company_Name    = Company_Name_Temp
                  trb:Batch_Number    = Batch_Number_Temp
                  If Access:TRDBATCH.Tryfetch(trb:Batch_Number_Key) = Level:Benign
                      !Found
                      If trb:AuthorisationNo <> ''
                          Error# = 1
                          Case MessageEx('Cannot add to the selected Batch!<13,10><13,10>This Batch has already been Authorised.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      End!If trb:authorisationNo <> ''
      
                  Else! If Access:TRDBATCH.Tryfetch(trb:Batch_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'Fetch Error')
                      Case MessageEx('The specified Batch does not exist.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
      
                  End! If Access:TRDBATCH.Tryfetch(trb:Batch_Number_Key) = Level:Benign
      
      
                  If error# = 1
                      Select(?Batch_Number_Temp)
                      tmp:AddBatch = 0
                  Else!f error# = 1
                      tmp:CurrentBatchNumber  = Batch_Number_Temp
      
                      Do Count_Batch
                  End!If error# = 1
      
              Of 2 ! &No Button
                  tmp:AddBatch = 0
          End!Case MessageEx
      End!IF tmp:beginbatch = 1
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:addbatch, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:beginbatch
      IF ?tmp:beginbatch{Prop:Checked} = True
        UNHIDE(?tmp:CurrentBatchNumber)
        ENABLE(?Sheet3)
        ENABLE(?Sheet2)
        ENABLE(?Sheet4)
        DISABLE(?Sheet1)
        DISABLE(?company_name_temp:prompt)
        DISABLE(?Company_Name_temp)
      END
      IF ?tmp:beginbatch{Prop:Checked} = False
        DISABLE(?Sheet2)
        DISABLE(?Sheet3)
        DISABLE(?Sheet4)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:beginbatch, Accepted)
      !Thismakeover.setwindow(win:window)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:beginbatch, Accepted)
    OF ?tmp:addbatch
      IF ?tmp:addbatch{Prop:Checked} = True
        UNHIDE(?tmp:CurrentBatchNumber)
        ENABLE(?Sheet3)
        ENABLE(?Sheet2)
        ENABLE(?Sheet4)
        DISABLE(?Sheet1)
        DISABLE(?company_name_temp:prompt)
        DISABLE(?Company_Name_temp)
      END
      IF ?tmp:addbatch{Prop:Checked} = False
        DISABLE(?Sheet3)
        DISABLE(?Sheet4)
        DISABLE(?Sheet2)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:addbatch, Accepted)
      !Thismakeover.setwindow(win:window)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:addbatch, Accepted)
    OF ?Job_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Accepted)
      Access:JOBS.ClearKey(job:Ref_Number_Key)
      job:Ref_Number = Job_Number_Temp
      If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Found
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Found
              If man:Use_MSN = 'YES'
                  ?tmp:MSN{prop:hide} = 0
                  ?tmp:MSN:Prompt{Prop:hide} = 0
              Else !If man:Use_MSN = 'YES'
                  ?tmp:MSN{prop:Hide} = 1
                  ?tmp:MSN:Prompt{prop:Hide} = 1
              End !If man:Use_MSN = 'YES'
          Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      Else!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End!If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benig
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Accepted)
    OF ?esn_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Accepted)
      IF LEN(CLIP(ESN_Temp)) = 18
        !Ericsson IMEI!
        ESN_Temp = SUB(ESN_Temp,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      If ?tmp:Msn{prop:hide} = 1
          Do FinalValidation
      End !?tmp:Msn{prop:hide} = 1
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Accepted)
    OF ?tmp:msn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Accepted)
      !Get Manufactuerere
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number = Job_Number_Temp
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Found
          IF LEN(CLIP(tmp:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
            !Ericsson MSN!
            tmp:MSN = SUB(tmp:MSN,2,10)
            UPDATE()
          END
      Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      Do FinalValidation
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Accepted)
    OF ?Finish
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
      setcursor(cursor:wait)
      save_trb_id = access:trdbatch.savefile()
      access:trdbatch.clearkey(trb:batch_number_key)
      trb:company_name = ''
      trb:batch_number = 0
      set(trb:batch_number_key,trb:batch_number_key)
      loop
          if access:trdbatch.next()
             break
          end !if
          if trb:company_name <> ''      |
          or trb:batch_number <> 0      |
              then break.  ! end if
          Delete(trdbatch)
      end !loop
      access:trdbatch.restorefile(save_trb_id)
      setcursor()
      
      POst(event:closewindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Finish, Accepted)
    OF ?Exchange
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Exchange, Accepted)
      If job:Exchange_Unit_Number <> ''
          Case MessageEx('This job already has an Exchange Unit attached.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      End!If job:exchange_unit_number <> ''
      tmp:Exchanged = 1
      tmp:Type = 'DES'
      Do Despatch
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Exchange, Accepted)
    OF ?accessories_retained
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessories_retained, Accepted)
      If job:Exchange_Unit_Number <> ''
          Case MessageEx('Error! This job has an Exchange Unit attached.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If job:Exchange_Unit_Number <> ''
          tmp:Exchanged = 0
          tmp:type    = 'RET'
          Do Despatch
      End!If job:exchange_unit_number <> ''
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessories_retained, Accepted)
    OF ?Despatched
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Despatched, Accepted)
      If job:Exchange_Unit_Number <> ''
          Case MessageEx('Error! This job has an Exchange Unit attached.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If job:Exchange_Unit_Number <> ''
          tmp:Exchanged = 0
          tmp:Type = 'DES'
          Do Despatch
      End!If job:Exchange_Unit_Number <> ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Despatched, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Despatch_Units')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?Job_Number_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Selected)
      ?Job_Number_Temp{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Job_Number_Temp, Selected)
    OF ?esn_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Selected)
      ?ESN_temp{prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?esn_temp, Selected)
    OF ?tmp:msn
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Selected)
      ?tmp:MSN{Prop:Touched} = 1
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:msn, Selected)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:GainFocus
      IF window{PROP:Iconize}=TRUE
        window{PROP:Iconize}=FALSE
        IF window{PROP:Active}<>TRUE
           window{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              If ?Button_Group{prop:Disable} = 0
                  Post(Event:Accepted,?Exchange)
              End !If ?Exchange{prop:Hide} = 0
          Of F6Key
              If ?Button_Group{prop:Disable} = 0
                  Post(Event:Accepted,?Accessories_Retained)
              End !If ?Accessories_Retained{prop:Hide} = 0
          Of F7Key
              If ?Button_Group{prop:Disable} = 0
                  Post(Event:Accepted,?Despatched)
              End !If ?Despatched{prop:Hide} = 0
          Of F10Key
              Post(Event:Accepted,?Finish)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case f_Type
          Of 'NEW'
              Unhide(?new_tab)
              Post(Event:Accepted,?tmp:beginbatch)
          Of 'ADD'
              Unhide(?add_tab)
              Post(Event:accepted,?tmp:addbatch)
      End!Case f_Type
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

