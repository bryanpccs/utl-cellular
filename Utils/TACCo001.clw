

   MEMBER('TACCodeRoutine.clw')                            ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('TACCO001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                             ! Generated from procedure template - Window

tmp:SavePath         CSTRING(255)                          !
tmp:ExportFile       CSTRING(255),STATIC                   !
window               WINDOW('TAC Code Import/Export Routine'),AT(,,215,99),FONT('Tahoma',,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       BUTTON('Export Model Numbers From ServiceBase'),AT(8,6,84,60),USE(?Button:Export),ICON('export.gif')
                       BUTTON('Import TAC Codes INTO ServiceBase'),AT(124,6,84,60),USE(?Button:Import),ICON('import.gif')
                       BUTTON('Close'),AT(152,78,56,16),USE(?Close),LEFT,ICON('cancel.ico')
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('wizcncl.ico'),HIDE
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass
ImportFile    File,Driver('BASIC'),Pre(imp),Name(tmp:ExportFile),Create,Bindable,Thread
Record              Record
Manufacturer        String(30)
ModelNumber         String(30)
TACCode             String(8)
! UnitCost            String(30)
                    End
                End

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It`s called after the window open
!|
!---------------------------------------------------------------------------
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Button:Export
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  IF SELF.Request = SelectRecord
     SELF.AddItem(?Close,RequestCancelled)                 ! Add the close control to the window manger
  ELSE
     SELF.AddItem(?Close,RequestCompleted)                 ! Add the close control to the window manger
  END
  Relate:ESNMODEL.Open                                     ! File ESNMODEL used by this procedure, so make sure it's RelationManager is open
  Relate:MANUFACT.SetOpenRelated()
  Relate:MANUFACT.Open                                     ! File MANUFACT used by this procedure, so make sure it's RelationManager is open
  Access:MODELNUM.UseFile                                  ! File referenced in 'Other Files' so need to inform it's FileManager
  SELF.FilesOpened = True
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Main',window)                              ! Restore window settings from non-volatile store
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ESNMODEL.Close
    Relate:MANUFACT.Close
  END
  IF SELF.Opened
    INIMgr.Update('Main',window)                           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button:Export
      ThisWindow.Update
      tmp:SavePath = Path()
      If filedialog ('Choose Export Directory',tmp:ExportFile,'All Directories|*.*', |
                  file:save+file:keepdir + file:noerror + file:longname + file:directory)
          !Found
          SetPath(tmp:SavePath)
          If Sub(Clip(tmp:ExportFile),-1,1) = '\'
              tmp:ExportFile = Clip(tmp:ExportFile) & 'ModelNumberExport.csv'
          Else ! If Sub(Clip(_{The Path}_),-1,1) = '\'
              tmp:ExportFile = Clip(tmp:ExportFile) & '\ModelNumberExport.csv'
          End ! If Sub(Clip(_{The Path}_),-1,1) = '\'
      
          ! Remove any existing files (DBH: 23/10/2007)
          Remove(tmp:ExportFile)
      
          ! Set the title for the export file (DBH: 23/10/2007)
          ! LinePrint('Manufacturer,Model Number,TAC Code,Unit Cost',tmp:ExportFile)
          LinePrint('Manufacturer,Model Number,TAC Code',tmp:ExportFile)
      
          Do Prog:ProgressSetup
          Prog:TotalRecords = Records(MANUFACT)
          Prog:ShowPercentage = 1 !Show Percentage Figure
      
          Access:MANUFACT.Clearkey(man:Manufacturer_Key)
          Set(man:Manufacturer_Key)
          Accept
              Case Event()
                  Of Event:Timer
                      Loop 25 Times
                          !Inside Loop
                          If Access:MANUFACT.Next()
                              Prog:Exit = 1
                              Break
                          End ! If Access:ORDERS.Next()
      
                          Prog:RecordCount += 1
      
                          ?Prog:UserString{Prop:Text} = 'Manufacturer: ' & man:Manufacturer
      
                          Do Prog:UpdateScreen
      
                          ! Only include Vodafone models (DBH: 23/10/2007)
                          If ~man:IsVodafoneSpecific And ~man:IsCCentreSpecific
                              Cycle
                          End ! If ~man:IsVodafoneSpecific And ~man:IsCCenterSpecific
      
                          Access:MODELNUM.Clearkey(mod:Manufacturer_Key)
                          mod:Manufacturer = man:Manufacturer
                          Set(mod:Manufacturer_Key,mod:Manufacturer_Key)
                          Loop
                              If Access:MODELNUM.Next()
                                  Break
                              End ! If Access:MODELNUM.Next()
                              If mod:Manufacturer <> man:Manufacturer
                                  Break
                              End ! If mod:Manufacturer <> man:Manufacturer
      
                              ! Only include Vodafone models (DBH: 23/10/2007)
                              If ~mod:IsVodafoneSpecific And ~mod:IsCCentreSpecific
                                  Cycle
                              End ! If ~mod:IsVodafoneSpecific And ~mod:IsCCentreSpecific
                              !LinePrint(Clip(Upper(mod:Manufacturer)) & ',' & Clip(Upper(mod:Model_Number)) & ',,',tmp:ExportFile)
                              LinePrint(Clip(Upper(mod:Manufacturer)) & ',' & Clip(Upper(mod:Model_Number)) & ',',tmp:ExportFile)
                          End ! Loop
                      End ! Loop 25 Times
                  Of Event:CloseWindow
                      Prog:Exit = 1
                      Prog:Cancelled = 1
                      Break
                  Of Event:Accepted
                      If Field() = ?Prog:Cancel
                          Beep(Beep:SystemQuestion)  ;  Yield()
                          Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                         icon:Question,'&Yes|&No',2,2)
                              Of 1 ! &Yes Button
                                  Prog:Exit = 1
                                  Prog:Cancelled = 1
                                  Break
                              Of 2 ! &No Button
                          End!Case Message
                      End ! If Field() = ?ProgressCancel
              End ! Case Event()
              If Prog:Exit
                  Break
              End ! If Prog:Exit
          End ! Accept
          Do Prog:ProgressFinished
          Beep(Beep:SystemAsterisk)  ;  Yield()
          Case Message('Export complete.','Model Number Export',|
                         icon:Asterisk,'&OK',1,1) 
              Of 1 ! &OK Button
          End!Case Message
      Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Error
          SetPath(tmp:SavePath)
      End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
    OF ?Button:Import
      ThisWindow.Update
      tmp:SavePath = Path()
      tmp:ExportFile = 'ModelNumberExport.csv'
      If FileDialog('Select Import File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Found
          SetPath(tmp:SavePath)
      
          Open(ImportFile)
          If Error()
              Stop(Error())
          Else ! If Error()
      
              ! Clear TAC Code File First (DBH: 23/10/2007)
      
              SetCursor(cursor:Wait)
              Access:ESNMODEL.Clearkey(esn:Record_Number_Key)
              Set(esn:Record_Number_Key)
              Loop
                  If Access:ESNMODEL.Next()
                      Break
                  End ! If Access:ESNMODEL.Next()
                  Delete(ESNMODEL)
              End ! Loop
      
              Count# = 0
              Set(ImportFile)
              Loop
                  Next(ImportFile)
                  If Error()
                      Break
                  End ! If Error()
                  Count# += 1
              End ! Loop
      
              SetCursor()
      
              Do Prog:ProgressSetup
              Prog:TotalRecords = Count#
              Prog:ShowPercentage = 1 !Show Percentage Figure
      
              Set(ImportFile)
      
              Accept
                  Case Event()
                      Of Event:Timer
                          Loop 25 Times
                              !Inside Loop
                              Next(ImportFile)
                              If Error()
                                  Prog:Exit = 1
                                  Break
                              End ! If Error()
      
                              ! Don't import the header record (DBH: 23/10/2007)
                              If Upper(imp:Manufacturer) = 'MANUFACTURER'
                                  Cycle
                              End ! If Upper(imp:Manfacturer) = 'MANUFACTURER'
      
                              ! Make sure we have a valid entry (DBH: 23/10/2007)
                              If Clip(imp:Manufacturer) <> '' And Clip(imp:ModelNumber) <> '' ! And Clip(imp:TACCode) <> ''
      
                                  Access:ESNMODEL.ClearKey(esn:ESN_Key)
                                  esn:Model_Number = Clip(Upper(imp:ModelNumber))
                                  esn:ESN = Clip(Upper(imp:TACCode))
                                  If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
                                      !Found
      !                                If Clip(imp:UnitCost) <> ''
      !                                    Access:MODELNUM.Clearkey(mod:Model_Number_Key)
      !                                    mod:Model_Number = esn:Model_Number
      !                                    If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      !                                        mod:UnitCost = imp:UnitCost
      !                                        Access:MODELNUM.TryUpdate()
      !                                    End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      !                                End ! If Clip(imp:UnitCost) <> ''
                                      Cycle
                                  Else ! If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
                                      !Error
                                  End ! If Access:ESNMODEL.TryFetch(esn:ESN_Key) = Level:Benign
      
                                  If Access:ESNMODEL.PrimeRecord() = Level:Benign
                                      esn:Model_Number = Clip(Upper(imp:ModelNumber))
                                      esn:ESN = Clip(Upper(imp:TACCode))
                                      If Access:ESNMODEL.TryInsert() = Level:Benign
      !                                    If Clip(imp:UnitCost) <> ''
      !                                        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
      !                                        mod:Model_Number = esn:Model_Number
      !                                        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      !                                            mod:UnitCost = imp:UnitCost
      !                                            Access:MODELNUM.TryUpdate()
      !                                        End ! If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
      !                                    End ! If Clip(imp:UnitCost) <> ''
                                          Prog:RecordCount += 1
                                      Else ! If Access:ESNMODEL.TryInsert() = Level:Benign
                                          Access:ESNMODEL.CancelAutoInc()
                                      End ! If Access:ESNMODEL.TryInsert() = Level:Benign
                                  End ! If Access:ESNMODEL.PrimeRecord() = Level:Benign
                              End ! If imp:Manufacturer <> '' And imp:ModelNumber <> '' And imp:TACCode <> ''
      
                              ?Prog:UserString{Prop:Text} = 'Records Inserted: ' & Prog:RecordCount & '/' & Prog:TotalRecords
      
                              Do Prog:UpdateScreen
                          End ! Loop 25 Times
                      Of Event:CloseWindow
                          Prog:Exit = 1
                          Prog:Cancelled = 1
                          Break
                      Of Event:Accepted
                          If Field() = ?Prog:Cancel
                              Beep(Beep:SystemQuestion)  ;  Yield()
                              Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                             icon:Question,'&Yes|&No',2,2)
                                  Of 1 ! &Yes Button
                                      Prog:Exit = 1
                                      Prog:Cancelled = 1
                                      Break
                                  Of 2 ! &No Button
                              End!Case Message
                          End ! If Field() = ?ProgressCancel
                  End ! Case Event()
                  If Prog:Exit
                      Break
                  End ! If Prog:Exit
              End ! Accept
              Do Prog:ProgressFinished
              Close(ImportFile)
      
              Beep(Beep:SystemAsterisk)  ;  Yield()
              Case Message('Import complete.||Entries Inserted: ' & Prog:RecordCount,'TAC Code Import',|
                             icon:Asterisk,'&OK',1,1) 
                  Of 1 ! &OK Button
              End!Case Message
      
          End ! If Error()
      Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
          !Error
          SetPath(tmp:SavePath)
      End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

