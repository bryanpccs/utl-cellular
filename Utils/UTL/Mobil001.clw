

   MEMBER('mobilenumberextract.clw')                  ! This is a MEMBER module

                     MAP
                       INCLUDE('MOBIL001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
tmp:ImportFile       STRING(255),STATIC
tmp:ExportFile       STRING(255),STATIC
tmp:SavePath         STRING(255)
ImportFile    File,Driver('BASIC'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
SBJobNumber         String(30)
SIDJobNumber        String(30)
DateBooked          String(20)
d                   String(1)
BookingType         String(3)
f                   String(1)
g                   String(1)
h                   String(1)
i                   String(1)
j                   String(1)
k                   String(1)
MobileNumber        String(30)
m                   String(1)
n                   String(1)
o                   String(1)
p                   String(1)
q                   String(1)
r                   String(1)
s                   String(1)
t                   String(1)
u                   String(1)
v                   String(1)
w                   String(1)
x                   String(1)
y                   String(1)
z                   String(1)
aa                   String(1)
ab                   String(1)
ac                   String(1)
ad                   String(1)
ae                   String(1)
af                   String(1)
ag                   String(1)
ah                   String(1)
ai                   String(1)
aj                   String(1)
ak                   String(1)
al                   String(1)
am                   String(1)
an                   String(1)
ao                   String(1)
ap                   String(1)
aq                   String(1)
ar                   String(1)
as                   String(1)
at                   String(1)
au                   String(1)
av                   String(1)
aw                   String(1)
ax                   String(1)
ay                   String(1)
az                   String(1)
ba                   String(1)
bb                   String(1)
bc                   String(1)
bd                   String(1)
be                   String(1)
bf                   String(1)
bg                   String(1)
bh                   String(1)
bi                   String(1)
bj                   String(1)
bk                   String(1)
bl                   String(1)
bm                   String(1)
bn                   String(1)
bo                   String(1)
bp                   String(1)
bq                   String(1)
br                   String(1)
bs                   String(1)
bt                   String(1)
bu                   String(1)
bv                   String(1)
bw                   String(1)
bx                   String(1)
xby                   String(1)
bz                   String(1)
ca                   String(1)
cb                   String(1)
cc                   String(1)
cd                   String(1)
ce                   String(1)
cf                   String(1)
cg                   String(1)
ch                   String(1)
ci                   String(1)
cj                   String(1)
ck                   String(1)
cl                   String(1)
cm                   String(1)
cn                   String(1)
co                   String(1)
cp                   String(1)
cq                   String(1)
cr                   String(1)
cs                   String(1)
ct                   String(1)
cu                   String(1)
cv                   String(1)
cw                   String(1)
cx                   String(1)
cy                   String(1)
cz                   String(1)
da                   String(1)
db                   String(1)
dc                   String(1)
dd                   String(1)
de                   String(1)
df                   String(1)
dg                   String(1)
dh                   String(1)
di                   String(1)
dj                   String(1)
CurrentStatus       String(30)
                    End
                End

ExportFile    File,Driver('BASIC'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record              Record
Type                String(30)
MobileNumber        String(30)
BookingTYpe         String(30)
DateBooked          String(30)
CurrentStatus       String(30)
SBJobNumber         String(30)
SIDJobNumber        String(30)
                    End
              End
Prog        Class
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(),Byte
ProgressText       Procedure(String func:String)
ProgressFinish     Procedure()
            End
! moving bar window
prog:RecordsToProcess   Long,Auto
prog:RecordsPerCycle    Long,Auto
prog:RecordsProcessed   Long,Auto
prog:RecordsThisCycle   Long,Auto
prog:PercentProgress    Byte
prog:Cancel             Byte
prog:ProgressThermometer    Byte


prog:thermometer byte
prog:SkipRecords        Long
omit('***',ClarionetUsed=0)
CNprog:thermometer byte
CNprog:Text         String(60)
CNprog:pctText      String(60)
***z
progwindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(prog:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?prog:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?prog:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('cancel.gif')
     END

omit('***',ClarionetUsed=0)

!static webjob window
CN:progwindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(cnprog:Text),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?cnprog:Prompt),FONT(,14,,FONT:bold)
     END
***
  CODE
    Remove(MobileNumberExtract)
    
   Relate:MobileNumberExtract.Open
    tmp:SavePath = Path()
    If FileDialog('Choose File',tmp:ImportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        !Found
        SetPath(tmp:SavePath)

        Open(ImportFile)
        If Error()
            Stop(Error())
        End ! IF Error()

        Count# = 0
        Set(ImportFile,0)
        Loop
            Next(ImportFile)
            If Error()
                Break
            End ! If Error()
            Count# += 1
        End

        Stream(MobileNumberExtract)

        Prog.ProgressSetup(Count#)
        IgnoreFirst# = True
        Set(ImportFile,0)
        Loop
            Next(ImportFile)
            If Error()
                Break
            End ! If Error()
            LineCount# += 1
            Prog.ProgressText('Reading File (' & LineCount# & '/' & Count# & ')')
            If Prog.InsideLoop()
                Break
            End ! If Prog.InsideLoop()

            If IgnoreFirst# = True
                IgnoreFirst# = False
                Cycle
            End ! If IgnoreFirst# = True
            If Access:MOBILENUMBEREXTRACT.PrimeRecord() = Level:Benign
                mob:MobileNumber = impfil:MobileNumber
                mob:BookingType = impfil:BookingType
                mob:DateBooked  = Deformat(impfil:DateBooked,@d06)
                mob:CurrentStatus = impfil:CurrentStatus
                mob:SBJobNumber = impfil:SBJobNumber
                mob:SIDJobNumber = impfil:SIDJobNumber
                If Access:MOBILENUMBEREXTRACT.TryInsert() = Level:Benign
                    !Insert
                Else ! If Access:MOBILENUMBEREXTRACT.TryInsert() = Level:Benign
                    
                End ! If Access:MOBILENUMBEREXTRACT.TryInsert() = Level:Benign
            End ! If Access.MOBILENUMBEREXTRACT.PrimeRecord() = Level:Benign
        End ! Loop
        CLose(ImportFile)
        Flush(MobileNumberExtract)
        Prog.ProgressFinish()
        total# = Records(MobileNumberExtract)
        Prog.ProgressSetup(total#)
        Prog.ProgressText('Exporting..')

        tmp:ExportFile = 'Mobile Number Extract ' & Format(Today(),@d12) & FOrmat(CLock(),@t2) & '.csv'
        Create(ExportFile)
        Open(ExportFile)

        expfil:Type = 'Type'
        expfil:MobileNumber = 'Mobile Number'
        expfil:BookingType = 'Booking Type'
        expfil:DateBooked = 'Date Booked'
        expfil:CurrentStatus = 'Current Status'
        expfil:SBJobNumber = 'SB Job Number'
        expfil:SIDJobNumber = 'SID Job Number'
        Add(ExportFile)

        Stream(MobileNumberExtract)
        Mobile" = ''
        LineCount# = 0
        Access:MOBILENUMBEREXTRACT.Clearkey(mob:MobileNumberKey)
        mob:MobileNumber = ''
        Set(mob:MobileNumberKey,mob:MobileNumberKey)
        Loop ! Begin Loop
            If Access:MOBILENUMBEREXTRACT.Next()
                Break
            End ! If Access:MOBILENUMBEREXTRACT.Next()
            LineCount# += 1
            Prog.ProgressText('Writing New Export (' & LineCount# & '/' & total# & ')')
            If Prog.InsideLoop()
                Break
            End ! If Prog.InsideLoop()
            If mob:MobileNumber <> Mobile"
                expfil:Type = 'Original'
                Mobile" = mob:MobileNumber
            Else
                expfil:Type = 'Duplicate'
            End ! If mob:MobileNumber <> Mobile"
            expfil:MobileNumber = mob:MobileNumber
            expfil:BookingType = mob:BookingType
            expfil:DateBooked = Format(mob:DateBooked,@d06)
            expfil:CurrentStatus = mob:CurrentStatus
            expfil:SBJobNumber = mob:SBJobNumber
            expfil:SIDJobNumber = mob:SIDJobNumber
            Add(ExportFile)
        End ! Loop
        Prog.ProgressFinish()
        Close(ExportFile)
        Flush(MobileNumberExtract)
        Message('Finished')
    Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        !Error
        SetPath(tmp:SavePath)
    End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
   Relate:MobileNumberExtract.Close
    Remove(MobileNumberExtract)
        Halt()
Prog:nextrecord      routine
    Yield()
    prog:recordsprocessed += 1
    prog:recordsthiscycle += 1
    If prog:percentprogress < 100
        prog:percentprogress = (prog:recordsprocessed / prog:recordstoprocess)*100
        If prog:percentprogress > 100
            prog:percentprogress = 100
        End
        If prog:percentprogress <> prog:thermometer then
            prog:thermometer = prog:percentprogress

Omit('***',ClarionetUsed=0)
            If ClarionetServer:Active()
                cnprog:thermometer = prog:thermometer
            Else ! If ClarionetServer:Active()
***
                ?prog:pcttext{prop:text} = format(prog:percentprogress,@n3) & '% Completed'
Omit('***',ClarionetUsed=0)
            End ! If ClarionetServer:Active()
***
        End
    End
    Display()

prog:cancelcheck         routine
    cancel# = 0
    prog:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            prog:Cancel = 1
        Of Button:No
        End !CASE
    End!If cancel# = 1


prog:Finish         routine
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        CNprog:thermometer = 100
    Else ! If ClarionetServer:Active()
***
        prog:thermometer = 100
        ?prog:pcttext{prop:text} = '100% Completed'
        display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ProgressSetup        Procedure(Long func:Records)
Code
    prog:SkipRecords = 0

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(CN:ProgWindow)
    Else ! If ClarionetServer:Active()
***
        Open(ProgWindow)
        Prog.ResetProgress(func:Records)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    prog:recordspercycle         = 25
    prog:recordsprocessed        = 0
    prog:percentprogress         = 0
    prog:thermometer    = 0
    prog:recordstoprocess    = func:Records

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:pcttext{prop:text} = '0% Completed'
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.InsideLoop         Procedure()
Code

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Do Prog:NextRecord
        Do Prog:CancelCheck
        If Prog:Cancel = 1
            Return 1
        End !If Prog:Cancel = 1
        Return 0
Omit('***',ClarionetUsed=0)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:UserString{prop:Text} = Clip(func:String)
        Display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Do Prog:Finish
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(CN:progwindow)
    Else ! If ClarionetServer:Active()
***
        close(progwindow)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

