

   MEMBER('bercharg.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('BERCH001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
tmp:ImportFile       STRING(255),STATIC
tmp:LogFile          STRING(255),STATIC
tmp:SavePath         STRING(255)
tmp:ModelNumber      STRING(30)
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('cancel.ico'),HIDE
     END
ImportFile      File,Driver('BASIC', '/COMMA = 124'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
OracleCode                  String(60)
BERValue                    String(30)
                        End
                    End

LogFile      File,Driver('ASCII'),Pre(log),Name(tmp:LogFile),Create,Bindable,Thread
Record                  Record
Text                        String(255)
                        End
                    End
  CODE
   Relate:MODELNUM.Open
   Relate:CARISMA.Open
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Message('Routine to import BER charges.'&|
        '|'&|
        '|Click ''OK'' to select the file to import.','Maintenance',|
                   icon:Asterisk,'&OK|&Cancel',2,2)
        Of 1 ! &OK Button
            tmp:SavePath = Path()

            tmp:LogFile = Path() & '\BERChargeImport.log'

            tmp:ImportFile = ''
            If FileDialog('Choose File',tmp:ImportFile,'Data Files|*.*',file:KeepDir + file:NoError + File:LongName)
                !Found
                SetPath(tmp:SavePath)

                Create(LogFile)
                Open(LogFile)
                If Error()
                   Stop('Can''t open log file: ' & error())
                End

                Open(ImportFile)
                If Error()
                    Stop('Can''t open import file: ' & error())
                End ! If Error()

                Count# = 0
                Set(ImportFile,0)
                Loop
                    Next(ImportFile)
                    If Error()
                        Break
                    End ! If Error()
                    Count# += 1
                End ! Loop

                Do Prog:ProgressSetup
                Prog:TotalRecords = Count#
                Prog:ShowPercentage = 1 !Show Percentage Figure

                Updated# = 0
                Rejected# = 0

                Set(ImportFile,0)

                Accept
                    Case Event()
                        Of Event:Timer
                            Loop 25 Times
                                !Inside Loop
                                Next(ImportFile)
                                If Error()
                                    Prog:Exit = 1
                                    Break
                                End ! If Access:ORDERS.Next()

                                Prog:RecordCount += 1

                                ?Prog:UserString{Prop:Text} = 'Records Processed: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                                Do Prog:UpdateScreen
                                tmp:ModelNumber = ''

                                If Clip(impfil:OracleCode) <> ''

                                    Access:CARISMA.Clearkey(cma:ContractCodeKey)
                                    cma:ContractOracleCode = Clip(impfil:OracleCode)
                                    If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign
                                        ! Found
                                        tmp:ModelNumber = cma:ModelNo
                                    Else
                                        Access:CARISMA.Clearkey(cma:PAYTCodeKey)
                                        cma:PAYTOracleCode = Clip(impfil:OracleCode)
                                        If Access:CARISMA.TryFetch(cma:PAYTCodeKey) = Level:Benign
                                            ! Found
                                            tmp:ModelNumber = cma:ModelNo
                                        End
                                    End

                                    If Clip(tmp:ModelNumber) <> ''
                                        Access:MODELNUM.ClearKey(mod:Model_Number_Key)
                                        mod:Model_Number = Clip(tmp:ModelNumber)
                                        If Access:MODELNUM.TryFetch(mod:Model_Number_Key) = Level:Benign
                                            ! Found
                                            mod:BERCharge = Clip(impfil:BERValue)
                                            If Access:MODELNUM.TryUpdate() = Level:Benign
                                                Updated# += 1
                                            Else
                                                ! Error
                                                Rejected# += 1
                                                log:Text = 'Could not save BER charge of ' & Clip(impfil:BERValue) & ' to model number ' & Clip(mod:Model_Number)
                                                Add(LogFile)
                                            End
                                        End
                                    Else
                                        Rejected# += 1
                                        log:Text = 'Oracle Code: ' & Clip(impfil:OracleCode) & ' could not be found.'
                                        Add(LogFile)
                                    End

                                End ! If
                            End ! Loop 25 Times
                        Of Event:CloseWindow
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of Event:Accepted
                            If Field() = ?Prog:Cancel
                                Beep(Beep:SystemQuestion)  ;  Yield()
                                Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                               icon:Question,'&Yes|&No',2,2)
                                    Of 1 ! &Yes Button
                                        Prog:Exit = 1
                                        Prog:Cancelled = 1
                                        Break
                                    Of 2 ! &No Button
                                End!Case Message
                            End ! If Field() = ?ProgressCancel
                    End ! Case Event()
                    If Prog:Exit
                        Break
                    End ! If Prog:Exit
                End ! Accept
                Do Prog:ProgressFinished

                log:Text = 'Total records successfully updated: ' & Updated#
                Add(LogFile)
                log:Text = 'Total records rejected: ' & Rejected#
                Add(LogFile)

                Close(LogFile)
                Close(ImportFile)

                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Message('Routine finished.'&|
                    '|'&|
                    '|Records updated: ' & Clip(Updated#) & '|Records rejected: ' & Clip(Rejected#),'Maintenance',|
                               icon:Asterisk,'&OK',1,1) 
                    Of 1 ! &OK Button
                End!Case Message
            Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
                !Error
                SetPath(tmp:SavePath)
            End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        Of 2 ! &Cancel Button
    End!Case Message
   Relate:MODELNUM.Close
   Relate:CARISMA.Close
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
