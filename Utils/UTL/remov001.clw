

   MEMBER('removecancelhistory.clw')                  ! This is a MEMBER module

                     MAP
                       INCLUDE('REMOV001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
tmp:ImportFile       STRING(255),STATIC
INIFilePath          STRING(255)
ConnectionStr        STRING(255),STATIC
tmp:SavePath         STRING(255)
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING('Working ...'),AT(0,3,161,10),USE(?Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING('0% Completed'),AT(0,32,161,10),USE(?Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,),HIDE
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('cancel.ico'),HIDE
     END
ImportFile      File,Driver('BASIC', '/COMMA = 9' ),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
RefNumber               Long()
ConsignmentNumber       String(60)
Manufacturer            String(30)
ModelNumber             String(30)
ExchangeIMEINumber      String(30) ! Customer IMEI Number
StatusCode              String(30)
ResolutionCode          String(30)
StatusDate              String(10)
StatusTime              String(10)
IMEINumber              String(30)
IncomingConsignmentNumber   String(60)
                        End
                    End



WebJobEx        file, driver('Scalable'), oem, owner(ConnectionStr), name('WEBJOBEX'), pre(webex), bindable, thread
Record              record
SCAccountID             long, name('SCAccountID')
                    end
                end
  CODE
   Relate:JOBS.Open
   Relate:AUDSTATS.Open
   Relate:EXCHANGE.Open
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Message('Routine to FIX Corrupt Jobs.'&|
        '|'&|
        '|Click ''OK'' to select the file contaning the jobs to update.','Maintenance',|
                   icon:Asterisk,'&OK|&Cancel',2,2)
        Of 1 ! &OK Button
            tmp:SavePath = Path()
            tmp:ImportFile = ''
            If FileDialog('Choose File',tmp:ImportFile,'TAB Files|*.tab',file:KeepDir + file:NoError + File:LongName)
                !Found
                SetPath(tmp:SavePath)

                IniFilepath =  Clip(Path()) & '\Webimp.ini'
                ConnectionStr = GetIni('Defaults', 'Connection', '', Clip(IniFilepath))

                Open(WEBJOBEX)
                If Error()
                    Stop('Can''t open webjobex: ' & error())
                End ! If Error()

                Open(ImportFile)
                If Error()
                    Stop('Can''t open import file: ' & error())
                End ! If Error()

                Count# = 0
                Set(ImportFile,0)
                Loop
                    Next(ImportFile)
                    If Error()
                        Break
                    End ! If Error()
                    Count# += 1
                End ! Loop

                Do Prog:ProgressSetup
                Prog:TotalRecords = Count#
                Prog:ShowPercentage = 1 !Show Percentage Figure

                Updated# = 0
                Exchange# = 0

                Set(ImportFile,0)

                Accept
                    Case Event()
                        Of Event:Timer
                            Loop 25 Times
                                !Inside Loop
                                Next(ImportFile)
                                If Error()
                                    Prog:Exit = 1
                                    Break
                                End ! If Access:ORDERS.Next()

                                Prog:RecordCount += 1

                                ?Prog:UserString{Prop:Text} = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords

                                Do Prog:UpdateScreen


                                If impfil:RefNumber > 0
                                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                                    job:Ref_Number = impfil:RefNumber
                                    If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                        ! Found
                                        ! Remove cancelled status history (DBH: 25/04/2008)
                                        If impfil:StatusCode <> '799'
                                            Access:AUDSTATS.Clearkey(aus:NewStatusKey)
                                            aus:RefNumber = job:Ref_Number
                                            aus:Type = 'JOB'
                                            aus:NewStatus = '799 JOB CANCELLED'
                                            If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                                                ! Found
                                                Updated# += 1
                                                Delete(AUDSTATS)
                                            Else ! If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                                            End ! If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                                        End ! If impfil:StatusCode <> '799'

                                        ! Set SID Despatch Date to yesterday (DBH: 25/04/2008)
                                        !If impfil:StatusCode = '901'
                                        WebJobEx{Prop:SQL} = 'UPDATE WebJobEx SET DespatchDate = ''1900-01-01'' WHERE WebJobNo = ' & job:Order_Number
                                        !End ! If impfil:Status = '901'

                                        If impfil:ExchangeIMEINumber = '' Or (impfil:ExchangeIMEINumber = impfil:IMEINumber)
                                            If job:Exchange_Unit_Number > 0
                                                ! The job has been exchanged, but there is no valid exchange in the file (DBH: 25/04/2008)
                                                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                                                xch:Ref_Number = job:Exchange_Unit_Number
                                                If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                                    ! Found
!                                                    If Instring('+14',xch:ESN,1,1)
                                                        ! This is a corrupted IMEI Number (DBH: 25/04/2008)
                                                        job:Exchange_Unit_Number = 0
                                                        job:Exchange_User = ''
                                                        job:Exchange_Despatched = ''
                                                        job:Exchange_Despatched_User = ''
                                                        job:Exchange_Consignment_Number = ''
                                                        job:Exchange_Status = ''
                                                        Access:JOBS.Update()
                                                        Delete(EXCHANGE)
                                                        Exchange# += 1

                                                        Access:AUDSTATS.Clearkey(aus:NewStatusKey)
                                                        aus:RefNumber = job:Ref_Number
                                                        aus:Type = 'EXC'
                                                        aus:NewStatus = '125 EXCHANGE IMEI CONFIRMED'
                                                        If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                                                            ! Found
                                                            Delete(AUDSTATS)
                                                        Else ! If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
                                                        End ! If Access:AUDSTATS.TryFetch(aus:NewStatusKey) = Level:Benign
!                                                    End ! If Instring('+14',xch:ESN,1,1)
                                                Else ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                                End ! If Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign
                                            End ! If job:Exchange_Unit_Number > 0
                                        End ! If impfil:ExchangeIMEINumber = ''
                                    Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                    End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                                End ! If impfil:RefNumber >0
                            End ! Loop 25 Times
                        Of Event:CloseWindow
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of Event:Accepted
                            If Field() = ?Prog:Cancel
                                Beep(Beep:SystemQuestion)  ;  Yield()
                                Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                               icon:Question,'&Yes|&No',2,2)
                                    Of 1 ! &Yes Button
                                        Prog:Exit = 1
                                        Prog:Cancelled = 1
                                        Break
                                    Of 2 ! &No Button
                                End!Case Message
                            End ! If Field() = ?ProgressCancel
                    End ! Case Event()
                    If Prog:Exit
                        Break
                    End ! If Prog:Exit
                End ! Accept
                Do Prog:ProgressFinished

                Close(WEBJOBEX)
                Close(ImportFile)


                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Message('Routine finished.'&|
                    '|'&|
                    '|Jobs updated: ' & Clip(Updated#) & '|Exchanges Removed: ' & Clip(Exchange#),'Maintenance',|
                               icon:Asterisk,'&OK',1,1) 
                    Of 1 ! &OK Button
                End!Case Message
            Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
                !Error
                SetPath(tmp:SavePath)
            End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        Of 2 ! &Cancel Button
    End!Case Message
   Relate:JOBS.Close
   Relate:AUDSTATS.Close
   Relate:EXCHANGE.Close
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            ?Prog:PercentText{prop:Text}= Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    ?Prog:PercentText{prop:Text} = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
