  MEMBER('removecancelhistory.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
REMOVBC0:DctInit    PROCEDURE
REMOVBC0:DctKill    PROCEDURE
REMOVBC0:FilesInit  PROCEDURE
  END

Hide:Access:STDCHRGE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STDCHRGE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CARISMA  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CARISMA  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXCHAMF  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXCHAMF  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXCHAUI  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXCHAUI  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBSENG  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBSENG  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDSTAEX CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDSTAEX CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STMASAUD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STMASAUD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:AUDSTATS CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:AUDSTATS CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:JOBTHIRD CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:JOBTHIRD CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:PRODCODE CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:PRODCODE CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOAUDIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOAUDIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:BOUNCER  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:BOUNCER  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TEAMS    CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TEAMS    CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:EXCAUDIT CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:EXCAUDIT CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:MODELCOL CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:MODELCOL CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:CONTHIST CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:CONTHIST CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ACCESDEF CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:ACCESDEF CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:TRDSPEC  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:TRDSPEC  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:ORDPEND  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:ORDPEND  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

Hide:Access:STOHIST  CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END


Hide:Relate:STOHIST  CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

REMOVBC0:DctInit PROCEDURE
  CODE
  Relate:STDCHRGE &= Hide:Relate:STDCHRGE
  Relate:CARISMA &= Hide:Relate:CARISMA
  Relate:EXCHAMF &= Hide:Relate:EXCHAMF
  Relate:EXCHAUI &= Hide:Relate:EXCHAUI
  Relate:JOBSENG &= Hide:Relate:JOBSENG
  Relate:AUDSTAEX &= Hide:Relate:AUDSTAEX
  Relate:STMASAUD &= Hide:Relate:STMASAUD
  Relate:AUDSTATS &= Hide:Relate:AUDSTATS
  Relate:JOBTHIRD &= Hide:Relate:JOBTHIRD
  Relate:PRODCODE &= Hide:Relate:PRODCODE
  Relate:STOAUDIT &= Hide:Relate:STOAUDIT
  Relate:BOUNCER &= Hide:Relate:BOUNCER
  Relate:TEAMS &= Hide:Relate:TEAMS
  Relate:EXCAUDIT &= Hide:Relate:EXCAUDIT
  Relate:MODELCOL &= Hide:Relate:MODELCOL
  Relate:CONTHIST &= Hide:Relate:CONTHIST
  Relate:ACCESDEF &= Hide:Relate:ACCESDEF
  Relate:TRDSPEC &= Hide:Relate:TRDSPEC
  Relate:ORDPEND &= Hide:Relate:ORDPEND
  Relate:STOHIST &= Hide:Relate:STOHIST

REMOVBC0:FilesInit PROCEDURE
  CODE
  Hide:Relate:STDCHRGE.Init
  Hide:Relate:CARISMA.Init
  Hide:Relate:EXCHAMF.Init
  Hide:Relate:EXCHAUI.Init
  Hide:Relate:JOBSENG.Init
  Hide:Relate:AUDSTAEX.Init
  Hide:Relate:STMASAUD.Init
  Hide:Relate:AUDSTATS.Init
  Hide:Relate:JOBTHIRD.Init
  Hide:Relate:PRODCODE.Init
  Hide:Relate:STOAUDIT.Init
  Hide:Relate:BOUNCER.Init
  Hide:Relate:TEAMS.Init
  Hide:Relate:EXCAUDIT.Init
  Hide:Relate:MODELCOL.Init
  Hide:Relate:CONTHIST.Init
  Hide:Relate:ACCESDEF.Init
  Hide:Relate:TRDSPEC.Init
  Hide:Relate:ORDPEND.Init
  Hide:Relate:STOHIST.Init


REMOVBC0:DctKill PROCEDURE
  CODE
  Hide:Relate:STDCHRGE.Kill
  Hide:Relate:CARISMA.Kill
  Hide:Relate:EXCHAMF.Kill
  Hide:Relate:EXCHAUI.Kill
  Hide:Relate:JOBSENG.Kill
  Hide:Relate:AUDSTAEX.Kill
  Hide:Relate:STMASAUD.Kill
  Hide:Relate:AUDSTATS.Kill
  Hide:Relate:JOBTHIRD.Kill
  Hide:Relate:PRODCODE.Kill
  Hide:Relate:STOAUDIT.Kill
  Hide:Relate:BOUNCER.Kill
  Hide:Relate:TEAMS.Kill
  Hide:Relate:EXCAUDIT.Kill
  Hide:Relate:MODELCOL.Kill
  Hide:Relate:CONTHIST.Kill
  Hide:Relate:ACCESDEF.Kill
  Hide:Relate:TRDSPEC.Kill
  Hide:Relate:ORDPEND.Kill
  Hide:Relate:STOHIST.Kill


Hide:Access:STDCHRGE.Init PROCEDURE
  CODE
  SELF.Init(STDCHRGE,GlobalErrors)
  SELF.Buffer &= sta:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(sta:Model_Number_Charge_Key,'By Charge Type',0)
  SELF.AddKey(sta:Charge_Type_Key,'By Charge Type',0)
  SELF.AddKey(sta:Unit_Type_Key,'By Unit Type',0)
  SELF.AddKey(sta:Repair_Type_Key,'By Repair Type',0)
  SELF.AddKey(sta:Cost_Key,'By Cost',0)
  SELF.AddKey(sta:Charge_Type_Only_Key,'sta:Charge_Type_Only_Key',0)
  SELF.AddKey(sta:Repair_Type_Only_Key,'sta:Repair_Type_Only_Key',0)
  SELF.AddKey(sta:Unit_Type_Only_Key,'sta:Unit_Type_Only_Key',0)
  Access:STDCHRGE &= SELF


Hide:Relate:STDCHRGE.Init PROCEDURE
  CODE
  Hide:Access:STDCHRGE.Init
  SELF.Init(Access:STDCHRGE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:CHARTYPE)
  SELF.AddRelation(Relate:REPAIRTY)
  SELF.AddRelation(Relate:UNITTYPE)
  SELF.AddRelation(Relate:MANUFACT)


Hide:Access:STDCHRGE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STDCHRGE &= NULL


Hide:Relate:STDCHRGE.Kill PROCEDURE

  CODE
  Hide:Access:STDCHRGE.Kill
  PARENT.Kill
  Relate:STDCHRGE &= NULL


Hide:Access:CARISMA.Init PROCEDURE
  CODE
  SELF.Init(CARISMA,GlobalErrors)
  SELF.Buffer &= cma:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cma:RecordNumberKey,'cma:RecordNumberKey',1)
  SELF.AddKey(cma:ManufactModColourKey,'cma:ManufactModColourKey',0)
  SELF.AddKey(cma:ContractCodeKey,'cma:ContractCodeKey',0)
  SELF.AddKey(cma:PAYTCodeKey,'cma:PAYTCodeKey',0)
  Access:CARISMA &= SELF


Hide:Relate:CARISMA.Init PROCEDURE
  CODE
  Hide:Access:CARISMA.Init
  SELF.Init(Access:CARISMA,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:CARISMA.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CARISMA &= NULL


Hide:Relate:CARISMA.Kill PROCEDURE

  CODE
  Hide:Access:CARISMA.Kill
  PARENT.Kill
  Relate:CARISMA &= NULL


Hide:Access:EXCHAMF.Init PROCEDURE
  CODE
  SELF.Init(EXCHAMF,GlobalErrors)
  SELF.Buffer &= emf:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(emf:Audit_Number_Key,'emf:Audit_Number_Key',1)
  Access:EXCHAMF &= SELF


Hide:Relate:EXCHAMF.Init PROCEDURE
  CODE
  Hide:Access:EXCHAMF.Init
  SELF.Init(Access:EXCHAMF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:EXCHAUI,RI:CASCADE,RI:CASCADE,eau:Audit_Number_Key)
  SELF.AddRelationLink(emf:Audit_Number,eau:Audit_Number)


Hide:Access:EXCHAMF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXCHAMF &= NULL


Hide:Relate:EXCHAMF.Kill PROCEDURE

  CODE
  Hide:Access:EXCHAMF.Kill
  PARENT.Kill
  Relate:EXCHAMF &= NULL


Hide:Access:EXCHAUI.Init PROCEDURE
  CODE
  SELF.Init(EXCHAUI,GlobalErrors)
  SELF.Buffer &= eau:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(eau:Internal_No_Key,'eau:Internal_No_Key',1)
  SELF.AddKey(eau:Audit_Number_Key,'eau:Audit_Number_Key',0)
  SELF.AddKey(eau:Ref_Number_Key,'eau:Ref_Number_Key',0)
  SELF.AddKey(eau:Exch_Browse_Key,'eau:Exch_Browse_Key',0)
  Access:EXCHAUI &= SELF


Hide:Relate:EXCHAUI.Init PROCEDURE
  CODE
  Hide:Access:EXCHAUI.Init
  SELF.Init(Access:EXCHAUI,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:EXCHAMF)
  SELF.AddRelation(Relate:EXCHANGE)


Hide:Access:EXCHAUI.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXCHAUI &= NULL


Hide:Relate:EXCHAUI.Kill PROCEDURE

  CODE
  Hide:Access:EXCHAUI.Kill
  PARENT.Kill
  Relate:EXCHAUI &= NULL


Hide:Access:JOBSENG.Init PROCEDURE
  CODE
  SELF.Init(JOBSENG,GlobalErrors)
  SELF.Buffer &= joe:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(joe:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(joe:UserCodeKey,'By Date',0)
  SELF.AddKey(joe:UserCodeOnlyKey,'By Date',0)
  SELF.AddKey(joe:AllocatedKey,'By Date',0)
  SELF.AddKey(joe:JobNumberKey,'By Date',0)
  Access:JOBSENG &= SELF


Hide:Relate:JOBSENG.Init PROCEDURE
  CODE
  Hide:Access:JOBSENG.Init
  SELF.Init(Access:JOBSENG,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBSENG.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBSENG &= NULL


Hide:Relate:JOBSENG.Kill PROCEDURE

  CODE
  Hide:Access:JOBSENG.Kill
  PARENT.Kill
  Relate:JOBSENG &= NULL


Hide:Access:AUDSTAEX.Init PROCEDURE
  CODE
  SELF.Init(AUDSTAEX,GlobalErrors)
  SELF.Buffer &= aux:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aux:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(aux:RefNumberKey,'By Ref Number',0)
  Access:AUDSTAEX &= SELF


Hide:Relate:AUDSTAEX.Init PROCEDURE
  CODE
  Hide:Access:AUDSTAEX.Init
  SELF.Init(Access:AUDSTAEX,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:AUDSTATS)


Hide:Access:AUDSTAEX.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDSTAEX &= NULL


Hide:Relate:AUDSTAEX.Kill PROCEDURE

  CODE
  Hide:Access:AUDSTAEX.Kill
  PARENT.Kill
  Relate:AUDSTAEX &= NULL


Hide:Access:STMASAUD.Init PROCEDURE
  CODE
  SELF.Init(STMASAUD,GlobalErrors)
  SELF.Buffer &= stom:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stom:AutoIncrement_Key,'stom:AutoIncrement_Key',1)
  SELF.AddKey(stom:Compeleted_Key,'stom:Compeleted_Key',0)
  SELF.AddKey(stom:Sent_Key,'stom:Sent_Key',0)
  Access:STMASAUD &= SELF


Hide:Relate:STMASAUD.Init PROCEDURE
  CODE
  Hide:Access:STMASAUD.Init
  SELF.Init(Access:STMASAUD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOAUDIT,RI:CASCADE,RI:CASCADE,stoa:Audit_Ref_No_Key)
  SELF.AddRelationLink(stom:Audit_No,stoa:Audit_Ref_No)


Hide:Access:STMASAUD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STMASAUD &= NULL


Hide:Relate:STMASAUD.Kill PROCEDURE

  CODE
  Hide:Access:STMASAUD.Kill
  PARENT.Kill
  Relate:STMASAUD &= NULL


Hide:Access:AUDSTATS.Init PROCEDURE
  CODE
  SELF.Init(AUDSTATS,GlobalErrors)
  SELF.Buffer &= aus:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(aus:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(aus:DateChangedKey,'By Date Changed',0)
  SELF.AddKey(aus:NewStatusKey,'By New Status',0)
  Access:AUDSTATS &= SELF


Hide:Relate:AUDSTATS.Init PROCEDURE
  CODE
  Hide:Access:AUDSTATS.Init
  SELF.Init(Access:AUDSTATS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:AUDSTAEX,RI:CASCADE,RI:CASCADE,aux:RefNumberKey)
  SELF.AddRelationLink(aus:RecordNumber,aux:RefNumber)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:AUDSTATS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:AUDSTATS &= NULL


Hide:Relate:AUDSTATS.Kill PROCEDURE

  CODE
  Hide:Access:AUDSTATS.Kill
  PARENT.Kill
  Relate:AUDSTATS &= NULL


Hide:Access:JOBTHIRD.Init PROCEDURE
  CODE
  SELF.Init(JOBTHIRD,GlobalErrors)
  SELF.Buffer &= jot:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(jot:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(jot:RefNumberKey,'By Ref Number',0)
  SELF.AddKey(jot:OutIMEIKey,'By Outgoing IMEI Number',0)
  SELF.AddKey(jot:InIMEIKEy,'By Incoming IMEI Number',0)
  SELF.AddKey(jot:OutDateKey,'By Outgoing Date',0)
  SELF.AddKey(jot:ThirdPartyKey,'jot:ThirdPartyKey',0)
  SELF.AddKey(jot:OriginalIMEIKey,'By Original I.M.E.I. No',0)
  Access:JOBTHIRD &= SELF


Hide:Relate:JOBTHIRD.Init PROCEDURE
  CODE
  Hide:Access:JOBTHIRD.Init
  SELF.Init(Access:JOBTHIRD,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDBATCH)
  SELF.AddRelation(Relate:JOBS)


Hide:Access:JOBTHIRD.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:JOBTHIRD &= NULL


Hide:Relate:JOBTHIRD.Kill PROCEDURE

  CODE
  Hide:Access:JOBTHIRD.Kill
  PARENT.Kill
  Relate:JOBTHIRD &= NULL


Hide:Access:PRODCODE.Init PROCEDURE
  CODE
  SELF.Init(PRODCODE,GlobalErrors)
  SELF.Buffer &= prd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(prd:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(prd:ProductCodeKey,'By Product Code',0)
  SELF.AddKey(prd:ModelProductKey,'By Product Code',0)
  Access:PRODCODE &= SELF


Hide:Relate:PRODCODE.Init PROCEDURE
  CODE
  Hide:Access:PRODCODE.Init
  SELF.Init(Access:PRODCODE,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:PRODCODE.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:PRODCODE &= NULL


Hide:Relate:PRODCODE.Kill PROCEDURE

  CODE
  Hide:Access:PRODCODE.Kill
  PARENT.Kill
  Relate:PRODCODE &= NULL


Hide:Access:STOAUDIT.Init PROCEDURE
  CODE
  SELF.Init(STOAUDIT,GlobalErrors)
  SELF.Buffer &= stoa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(stoa:Internal_AutoNumber_Key,'stoa:Internal_AutoNumber_Key',1)
  SELF.AddKey(stoa:Audit_Ref_No_Key,'stoa:Audit_Ref_No_Key',0)
  SELF.AddKey(stoa:Cellular_Key,'stoa:Cellular_Key',0)
  SELF.AddKey(stoa:Stock_Ref_No_Key,'stoa:Stock_Ref_No_Key',0)
  SELF.AddKey(stoa:Stock_Site_Number_Key,'stoa:Stock_Site_Number_Key',0)
  SELF.AddKey(stoa:Lock_Down_Key,'stoa:Lock_Down_Key',0)
  Access:STOAUDIT &= SELF


Hide:Relate:STOAUDIT.Init PROCEDURE
  CODE
  Hide:Access:STOAUDIT.Init
  SELF.Init(Access:STOAUDIT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOCK)
  SELF.AddRelation(Relate:STMASAUD)


Hide:Access:STOAUDIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOAUDIT &= NULL


Hide:Relate:STOAUDIT.Kill PROCEDURE

  CODE
  Hide:Access:STOAUDIT.Kill
  PARENT.Kill
  Relate:STOAUDIT &= NULL


Hide:Access:BOUNCER.Init PROCEDURE
  CODE
  SELF.Init(BOUNCER,GlobalErrors)
  SELF.Buffer &= bou:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(bou:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(bou:Bouncer_Job_Number_Key,'By Job Number',0)
  SELF.AddKey(bou:Bouncer_Job_Only_Key,'By Job Number',0)
  Access:BOUNCER &= SELF


Hide:Relate:BOUNCER.Init PROCEDURE
  CODE
  Hide:Access:BOUNCER.Init
  SELF.Init(Access:BOUNCER,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS_ALIAS)


Hide:Access:BOUNCER.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:BOUNCER &= NULL


Hide:Relate:BOUNCER.Kill PROCEDURE

  CODE
  Hide:Access:BOUNCER.Kill
  PARENT.Kill
  Relate:BOUNCER &= NULL


Hide:Access:TEAMS.Init PROCEDURE
  CODE
  SELF.Init(TEAMS,GlobalErrors)
  SELF.Buffer &= tea:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tea:Record_Number_Key,'By Record Number',1)
  SELF.AddKey(tea:Team_Key,'By Team',0)
  Access:TEAMS &= SELF


Hide:Relate:TEAMS.Init PROCEDURE
  CODE
  Hide:Access:TEAMS.Init
  SELF.Init(Access:TEAMS,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:USERS,RI:CASCADE,RI:RESTRICT,use:Team_Surname)
  SELF.AddRelationLink(tea:Team,use:Team)


Hide:Access:TEAMS.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TEAMS &= NULL


Hide:Relate:TEAMS.Kill PROCEDURE

  CODE
  Hide:Access:TEAMS.Kill
  PARENT.Kill
  Relate:TEAMS &= NULL


Hide:Access:EXCAUDIT.Init PROCEDURE
  CODE
  SELF.Init(EXCAUDIT,GlobalErrors)
  SELF.Buffer &= exa:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(exa:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(exa:Audit_Number_Key,'By Audit Number',0)
  SELF.AddKey(exa:StockUnitNoKey,'exa:StockUnitNoKey',0)
  SELF.AddKey(exa:ReplaceUnitNoKey,'exa:ReplaceUnitNoKey',0)
  SELF.AddKey(exa:TypeStockNumber,'By Audit Number',0)
  Access:EXCAUDIT &= SELF


Hide:Relate:EXCAUDIT.Init PROCEDURE
  CODE
  Hide:Access:EXCAUDIT.Init
  SELF.Init(Access:EXCAUDIT,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:EXCHANGE_ALIAS)
  SELF.AddRelation(Relate:EXCHANGE)


Hide:Access:EXCAUDIT.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:EXCAUDIT &= NULL


Hide:Relate:EXCAUDIT.Kill PROCEDURE

  CODE
  Hide:Access:EXCAUDIT.Kill
  PARENT.Kill
  Relate:EXCAUDIT &= NULL


Hide:Access:MODELCOL.Init PROCEDURE
  CODE
  SELF.Init(MODELCOL,GlobalErrors)
  SELF.Buffer &= moc:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(moc:Record_Number_Key,'moc:Record_Number_Key',1)
  SELF.AddKey(moc:Colour_Key,'By Colour',0)
  Access:MODELCOL &= SELF


Hide:Relate:MODELCOL.Init PROCEDURE
  CODE
  Hide:Access:MODELCOL.Init
  SELF.Init(Access:MODELCOL,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:MODELNUM)


Hide:Access:MODELCOL.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:MODELCOL &= NULL


Hide:Relate:MODELCOL.Kill PROCEDURE

  CODE
  Hide:Access:MODELCOL.Kill
  PARENT.Kill
  Relate:MODELCOL &= NULL


Hide:Access:CONTHIST.Init PROCEDURE
  CODE
  SELF.Init(CONTHIST,GlobalErrors)
  SELF.Buffer &= cht:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(cht:Ref_Number_Key,'By Date',0)
  SELF.AddKey(cht:Action_Key,'By Action',0)
  SELF.AddKey(cht:User_Key,'By User',0)
  SELF.AddKey(cht:Record_Number_Key,'cht:Record_Number_Key',1)
  Access:CONTHIST &= SELF


Hide:Relate:CONTHIST.Init PROCEDURE
  CODE
  Hide:Access:CONTHIST.Init
  SELF.Init(Access:CONTHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:JOBS)


Hide:Access:CONTHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:CONTHIST &= NULL


Hide:Relate:CONTHIST.Kill PROCEDURE

  CODE
  Hide:Access:CONTHIST.Kill
  PARENT.Kill
  Relate:CONTHIST &= NULL


Hide:Access:ACCESDEF.Init PROCEDURE
  CODE
  SELF.Init(ACCESDEF,GlobalErrors)
  SELF.Buffer &= acd:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(acd:Accessory_Key,'By Accessory',0)
  Access:ACCESDEF &= SELF


Hide:Relate:ACCESDEF.Init PROCEDURE
  CODE
  Hide:Access:ACCESDEF.Init
  SELF.Init(Access:ACCESDEF,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:ACCESSOR,RI:CASCADE,RI:None,acr:Model_Number_Key)
  SELF.AddRelationLink(acd:Accessory,acr:Accessory)


Hide:Access:ACCESDEF.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ACCESDEF &= NULL


Hide:Relate:ACCESDEF.Kill PROCEDURE

  CODE
  Hide:Access:ACCESDEF.Kill
  PARENT.Kill
  Relate:ACCESDEF &= NULL


Hide:Access:TRDSPEC.Init PROCEDURE
  CODE
  SELF.Init(TRDSPEC,GlobalErrors)
  SELF.Buffer &= tsp:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(tsp:Short_Description_Key,'By Short Description',0)
  Access:TRDSPEC &= SELF


Hide:Relate:TRDSPEC.Init PROCEDURE
  CODE
  Hide:Access:TRDSPEC.Init
  SELF.Init(Access:TRDSPEC,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:TRDPARTY,RI:CASCADE,RI:RESTRICT,trd:Special_Instructions_Key)
  SELF.AddRelationLink(tsp:Short_Description,trd:Special_Instructions)


Hide:Access:TRDSPEC.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:TRDSPEC &= NULL


Hide:Relate:TRDSPEC.Kill PROCEDURE

  CODE
  Hide:Access:TRDSPEC.Kill
  PARENT.Kill
  Relate:TRDSPEC &= NULL


Hide:Access:ORDPEND.Init PROCEDURE
  CODE
  SELF.Init(ORDPEND,GlobalErrors)
  SELF.Buffer &= ope:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(ope:Ref_Number_Key,'By Ref Number',1)
  SELF.AddKey(ope:Supplier_Key,'By Supplier',0)
  SELF.AddKey(ope:DescriptionKey,'By Description',0)
  SELF.AddKey(ope:Supplier_Name_Key,'By Supplier',0)
  SELF.AddKey(ope:Part_Ref_Number_Key,'By Stock Ref Number',0)
  SELF.AddKey(ope:Awaiting_Supplier_Key,'By Part Number',0)
  SELF.AddKey(ope:PartRecordNumberKey,'By Record Number',0)
  SELF.AddKey(ope:Job_Number_Key,'By Job Number',0)
  SELF.AddKey(ope:Supplier_Job_Key,'ope:Supplier_Job_Key',0)
  Access:ORDPEND &= SELF


Hide:Relate:ORDPEND.Init PROCEDURE
  CODE
  Hide:Access:ORDPEND.Init
  SELF.Init(Access:ORDPEND,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:SUPPLIER)


Hide:Access:ORDPEND.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:ORDPEND &= NULL


Hide:Access:ORDPEND.PrimeFields PROCEDURE

  CODE
  ope:Awaiting_Stock = 'NO'
  PARENT.PrimeFields


Hide:Relate:ORDPEND.Kill PROCEDURE

  CODE
  Hide:Access:ORDPEND.Kill
  PARENT.Kill
  Relate:ORDPEND &= NULL


Hide:Access:STOHIST.Init PROCEDURE
  CODE
  SELF.Init(STOHIST,GlobalErrors)
  SELF.Buffer &= shi:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(shi:Ref_Number_Key,'By Ref_Number',0)
  SELF.AddKey(shi:record_number_key,'shi:record_number_key',1)
  SELF.AddKey(shi:Transaction_Type_Key,'By Transaction',0)
  SELF.AddKey(shi:DateKey,'By Date',0)
  Access:STOHIST &= SELF


Hide:Relate:STOHIST.Init PROCEDURE
  CODE
  Hide:Access:STOHIST.Init
  SELF.Init(Access:STOHIST,1)
  DO AddRelations_1

AddRelations_1 ROUTINE
  SELF.AddRelation(Relate:STOCK)


Hide:Access:STOHIST.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:STOHIST &= NULL


Hide:Relate:STOHIST.Kill PROCEDURE

  CODE
  Hide:Access:STOHIST.Kill
  PARENT.Kill
  Relate:STOHIST &= NULL

