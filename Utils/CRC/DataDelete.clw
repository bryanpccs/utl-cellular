   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('DATADBC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('DATAD001.CLW')
Main                   PROCEDURE   !
     END
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
   END

glo:FileName         STRING(255)
glo:TradeTmp         STRING(255)
glo:File_Name        STRING(255)
glo:File_Name2       STRING(255)
glo:File_Name3       STRING(255)
glo:File_Name4       STRING(255)
glo:RepairTypes      STRING(255)
SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'

STDCHRGE             FILE,DRIVER('Btrieve'),NAME('STDCHRGE.DAT'),PRE(sta),CREATE,BINDABLE,THREAD
Model_Number_Charge_Key  KEY(sta:Model_Number,sta:Charge_Type,sta:Unit_Type,sta:Repair_Type),NOCASE,PRIMARY
Charge_Type_Key          KEY(sta:Model_Number,sta:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(sta:Model_Number,sta:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(sta:Model_Number,sta:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(sta:Model_Number,sta:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(sta:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(sta:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(sta:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Model_Number                STRING(30)
Unit_Type                   STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
                         END
                     END                       

DEFEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('DEFEMAIL.DAT'),PRE(dem),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(dem:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
SMTPServer                  STRING(50)
SMTPPort                    STRING(3)
SMTPUserName                STRING(30)
SMTPPassword                STRING(30)
EmailFromAddress            STRING(255)
SMSToAddress                STRING(255)
EmailToAddress              STRING(255)
AdminToAddress              STRING(255)
DespFromSCText              STRING(160)
ArrivedAtRetailText         STRING(160)
                         END
                     END                       

CRCDEFS              FILE,DRIVER('Btrieve'),OEM,NAME('CRCDEFS.DAT'),PRE(crcdef),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(crcdef:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
FTPServer                   STRING(255)
FTPUserName                 STRING(60)
FTPPassword                 STRING(60)
FTPImportPath               STRING(255)
FTPExportPath               STRING(255)
EmailServer                 STRING(255)
EmailServerPort             STRING(3)
EmailFromAddress            STRING(255)
CRCEmailName                STRING(60)
CRCEmailAddress             STRING(255)
PCCSEmailName               STRING(60)
PCCSEmailAddress            STRING(255)
VodafoneEmailName           STRING(60)
VodafoneEmailAddress        STRING(255)
                         END
                     END                       

REGIONS              FILE,DRIVER('Btrieve'),OEM,NAME('REGIONS.DAT'),PRE(reg),CREATE,BINDABLE,THREAD
RegionIDKey              KEY(reg:RegionID),NOCASE,PRIMARY
RegionNameKey            KEY(reg:RegionName),NOCASE
Record                   RECORD,PRE()
RegionID                    LONG
RegionName                  STRING(30)
BankHolidaysFileName        STRING(100)
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                     END                       

ACCREG               FILE,DRIVER('Btrieve'),OEM,NAME('ACCREG.DAT'),PRE(acg),CREATE,BINDABLE,THREAD
AccRegIDKey              KEY(acg:AccRegID),NOCASE,PRIMARY
AccountNoKey             KEY(acg:AccountNo),NOCASE
RegionNameKey            KEY(acg:RegionName,acg:AccountNo),DUP,NOCASE
Record                   RECORD,PRE()
AccRegID                    LONG
AccountNo                   STRING(15)
RegionName                  STRING(30)
TransitDaysException        BYTE
TransitDaysToSC             LONG
TransitDaysToStore          LONG
                         END
                     END                       

XMLJOBS              FILE,DRIVER('Btrieve'),OEM,NAME('XMLJOBS.DAT'),PRE(XJB),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(XJB:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(XJB:TR_NO),DUP,NOCASE
JobNumberKey             KEY(XJB:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(XJB:RECORD_STATE,XJB:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(XJB:RECORD_STATE,XJB:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(XJB:RECORD_STATE,XJB:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(XJB:RECORD_STATE,XJB:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(XJB:RECORD_STATE,XJB:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
SYMPTOM1_DESC               STRING(40)
SYMPTOM2_DESC               STRING(40)
SYMPTOM3_DESC               STRING(40)
BP_NO                       STRING(10)
INQUIRY_TEXT                STRING(254)
                         END
                     END                       

EXCHAMF              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAMF.DAT'),PRE(emf),CREATE,BINDABLE,THREAD
Audit_Number_Key         KEY(emf:Audit_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Audit_Number                LONG
Date                        DATE
Time                        LONG
User                        STRING(3)
Status                      STRING(30)
Complete_Flag               BYTE
                         END
                     END                       

EXCHAUI              FILE,DRIVER('Btrieve'),OEM,NAME('EXCHAUI.DAT'),PRE(eau),CREATE,BINDABLE,THREAD
Internal_No_Key          KEY(eau:Internal_No),NOCASE,PRIMARY
Audit_Number_Key         KEY(eau:Audit_Number),DUP,NOCASE
Ref_Number_Key           KEY(eau:Ref_Number),DUP,NOCASE
Exch_Browse_Key          KEY(eau:Audit_Number,eau:Exists),DUP,NOCASE
Record                   RECORD,PRE()
Internal_No                 LONG
Audit_Number                LONG
Ref_Number                  LONG
Exists                      BYTE
New_IMEI                    BYTE
                         END
                     END                       

JOBSENG              FILE,DRIVER('Btrieve'),OEM,NAME('JOBSENG.DAT'),PRE(joe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(joe:RecordNumber),NOCASE,PRIMARY
UserCodeKey              KEY(joe:JobNumber,joe:UserCode,joe:DateAllocated),DUP,NOCASE
UserCodeOnlyKey          KEY(joe:UserCode,joe:DateAllocated),DUP,NOCASE
AllocatedKey             KEY(joe:JobNumber,joe:AllocatedBy,joe:DateAllocated),DUP,NOCASE
JobNumberKey             KEY(joe:JobNumber,-joe:DateAllocated,-joe:TimeAllocated),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
JobNumber                   LONG
UserCode                    STRING(3)
DateAllocated               DATE
TimeAllocated               STRING(20)
AllocatedBy                 STRING(3)
EngSkillLevel               LONG
JobSkillLevel               STRING(30)
                         END
                     END                       

MULDESPJ             FILE,DRIVER('Btrieve'),OEM,NAME('MULDESPJ.DAT'),PRE(mulj),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mulj:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(mulj:RefNumber,mulj:JobNumber),DUP,NOCASE
CurrentJobKey            KEY(mulj:RefNumber,mulj:Current,mulj:JobNumber),DUP,NOCASE
JobNumberOnlyKey         KEY(mulj:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobNumber                   LONG
IMEINumber                  STRING(30)
MSN                         STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
Current                     BYTE
                         END
                     END                       

MULDESP              FILE,DRIVER('Btrieve'),OEM,NAME('MULDESP.DAT'),PRE(muld),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(muld:RecordNumber),NOCASE,PRIMARY
BatchNumberKey           KEY(muld:BatchNumber),DUP,NOCASE
AccountNumberKey         KEY(muld:AccountNumber),DUP,NOCASE
CourierKey               KEY(muld:Courier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BatchNumber                 STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
BatchTotal                  LONG
                         END
                     END                       

INSMODELS            FILE,DRIVER('Btrieve'),OEM,PRE(INS3),CREATE,BINDABLE,THREAD
KeyModelNo               KEY(INS3:ModelNo),NOCASE,PRIMARY
KeyMakeNo                KEY(INS3:MakeNo),DUP,NOCASE
KeyBrwModelNumber        KEY(INS3:MakeNo,INS3:ModelNumber),NOCASE
KeyModelNumber           KEY(INS3:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
ModelNo                     LONG
MakeNo                      LONG
ModelNumber                 STRING(30)
                         END
                     END                       

STOCKLOG             FILE,DRIVER('Btrieve'),OEM,NAME('STOCKLOG.DAT'),PRE(sal),CREATE,BINDABLE,THREAD
Audit_Number_Key         KEY(sal:Audit_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Audit_Number                LONG
Date_created                DATE
Time_Created                TIME
User                        STRING(20)
Checked_Date                DATE
Checked_Time                TIME
Checked_By                  STRING(3)
Status                      STRING(10)
Scanned_Count               LONG
Stock_Status                STRING(10)
                         END
                     END                       

IMEILOG              FILE,DRIVER('Btrieve'),OEM,NAME('IMEILOG.DAT'),PRE(ime),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ime:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
Audit_Number                LONG
IMEI_Number                 STRING(30)
Stock_Type                  STRING(30)
Status_Code                 STRING(6)
Model_No                    STRING(30)
Manufacturer                STRING(30)
                         END
                     END                       

STMASAUD             FILE,DRIVER('Btrieve'),NAME('STMASAUD.DAT'),PRE(stom),CREATE,BINDABLE,THREAD
AutoIncrement_Key        KEY(-stom:Audit_No),NOCASE,PRIMARY
Compeleted_Key           KEY(stom:Complete),DUP,NOCASE
Sent_Key                 KEY(stom:Send),DUP,NOCASE
Record                   RECORD,PRE()
Audit_No                    LONG
branch                      STRING(30)
branch_id                   LONG
Audit_Date                  LONG
Audit_Time                  LONG
End_Date                    LONG
End_Time                    LONG
Audit_User                  STRING(3)
Complete                    STRING(1)
Send                        STRING(1)
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,NAME('AUDSTATS.DAT'),PRE(aus),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 STRING(20)
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END                       

IEXPDEFS             FILE,DRIVER('Btrieve'),OEM,PRE(iex),CREATE,BINDABLE,THREAD
KeyRecNo                 KEY(iex:RecNo),NOCASE,PRIMARY
KeyName                  KEY(iex:Name),NOCASE
KeyTypeName              KEY(iex:Type,iex:Name),DUP,NOCASE
Record                   RECORD,PRE()
RecNo                       LONG
Name                        STRING(50)
Type                        STRING(1)
                         END
                     END                       

RAPIDLST             FILE,DRIVER('Btrieve'),OEM,NAME('RAPIDLST.DAT'),PRE(RAP),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(RAP:RecordNumber),NOCASE,PRIMARY
OrderKey                 KEY(RAP:RapidOrder,RAP:ProgramName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RapidOrder                  LONG
ProgramName                 STRING(60)
Description                 STRING(255)
EXEPath                     STRING(255)
                         END
                     END                       

ICONTHIST            FILE,DRIVER('Btrieve'),OEM,PRE(ico),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(ico:RecordNumber),NOCASE,PRIMARY
KeyAction                KEY(ico:RefNumber,ico:Action,-ico:Date,-ico:Time),DUP,NOCASE
KeyUser                  KEY(ico:RefNumber,ico:User,-ico:Date,-ico:Time),DUP,NOCASE
KeyRefNumber             KEY(ico:RefNumber,-ico:Date,-ico:Time,ico:Action),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                REAL
RefNumber                   LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(1000)
Dummy                       STRING(1)
                         END
                     END                       

ORDTEMP              FILE,DRIVER('Btrieve'),NAME('ORDTEMP.DAT'),PRE(ort),CREATE,BINDABLE,THREAD
recordnumberkey          KEY(ort:recordnumber),NOCASE,PRIMARY
Keyordhno                KEY(ort:ordhno),DUP,NOCASE
Record                   RECORD,PRE()
recordnumber                LONG
ordhno                      LONG
manufact                    STRING(40)
partno                      STRING(20)
partdiscription             STRING(60)
qty                         LONG
itemcost                    REAL
totalcost                   REAL
thedate                     DATE
                         END
                     END                       

ORDITEMS             FILE,DRIVER('Btrieve'),NAME('ORDITEMS.DAT'),PRE(ori),CREATE,BINDABLE,THREAD
Keyordhno                KEY(ori:ordhno),DUP,NOCASE
recordnumberkey          KEY(ori:recordnumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
recordnumber                LONG
ordhno                      LONG
manufact                    STRING(40)
partno                      STRING(20)
partdiscription             STRING(60)
qty                         LONG
itemcost                    REAL
totalcost                   REAL
                         END
                     END                       

ORDHEAD              FILE,DRIVER('Btrieve'),NAME('ORDHEAD.DAT'),PRE(orh),CREATE,BINDABLE,THREAD
KeyOrder_no              KEY(orh:Order_no),NOCASE,PRIMARY
book_date_key            KEY(orh:thedate),DUP,NOCASE
pro_date_key             KEY(-orh:pro_date),DUP,NOCASE
pro_ord_no_key           KEY(orh:procesed,orh:thedate,orh:Order_no),DUP,NOCASE
pro_cust_name_key        KEY(orh:procesed,orh:pro_date,orh:CustName),DUP,NOCASE
KeyCustName              KEY(orh:CustName),DUP,NOCASE
AccountDateKey           KEY(orh:account_no,orh:thedate),DUP,NOCASE
SalesNumberKey           KEY(orh:SalesNumber),DUP,NOCASE
ProcessSaleNoKey         KEY(orh:procesed,orh:SalesNumber),DUP,NOCASE
DateDespatchedKey        KEY(orh:DateDespatched),DUP,NOCASE
Record                   RECORD,PRE()
Order_no                    LONG
account_no                  STRING(40)
CustName                    STRING(30)
CustAdd1                    STRING(30)
CustAdd2                    STRING(30)
CustAdd3                    STRING(30)
CustPostCode                STRING(10)
CustTel                     STRING(20)
CustFax                     STRING(20)
dName                       STRING(30)
dAdd1                       STRING(30)
dAdd2                       STRING(30)
dAdd3                       STRING(30)
dPostCode                   STRING(10)
dTel                        STRING(20)
dFax                        STRING(20)
thedate                     DATE
thetime                     TIME
procesed                    BYTE
pro_date                    DATE
WhoBooked                   STRING(30)
Department                  STRING(30)
CustOrderNumber             STRING(30)
DateDespatched              DATE
SalesNumber                 LONG
                         END
                     END                       

IMEISHIP             FILE,DRIVER('Btrieve'),OEM,NAME('IMEISHIP.DAT'),PRE(IMEI),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(IMEI:RecordNumber),NOCASE,PRIMARY
IMEIKey                  KEY(IMEI:IMEINumber),DUP,NOCASE
ShipDateKey              KEY(IMEI:ShipDate),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
IMEINumber                  STRING(30)
ShipDate                    DATE
                         END
                     END                       

IACTION              FILE,DRIVER('Btrieve'),OEM,PRE(iac),CREATE,BINDABLE,THREAD
KeyActionNo              KEY(iac:ActionNo),NOCASE,PRIMARY
KeyAction                KEY(iac:Action),NOCASE
Record                   RECORD,PRE()
ActionNo                    LONG
Action                      STRING(80)
                         END
                     END                       

JOBTHIRD             FILE,DRIVER('Btrieve'),OEM,NAME('JOBTHIRD.DAT'),PRE(jot),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jot:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jot:RefNumber),DUP,NOCASE
OutIMEIKey               KEY(jot:OutIMEI),DUP,NOCASE
InIMEIKEy                KEY(jot:InIMEI),DUP,NOCASE
OutDateKey               KEY(jot:RefNumber,jot:DateOut,jot:RecordNumber),DUP,NOCASE
ThirdPartyKey            KEY(jot:ThirdPartyNumber),DUP,NOCASE
OriginalIMEIKey          KEY(jot:OriginalIMEI),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
OriginalIMEI                STRING(30)
OutIMEI                     STRING(30)
InIMEI                      STRING(30)
DateOut                     DATE
DateDespatched              DATE
DateIn                      DATE
ThirdPartyNumber            LONG
OriginalMSN                 STRING(30)
OutMSN                      STRING(30)
InMSN                       STRING(30)
                         END
                     END                       

DEFAULTV             FILE,DRIVER('Btrieve'),OEM,NAME('DEFAULTV.DAT'),PRE(defv),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(defv:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
VersionNumber               STRING(30)
NagDate                     DATE
ReUpdate                    BYTE
                         END
                     END                       

LOGRETRN             FILE,DRIVER('Btrieve'),OEM,NAME('LOGRETRN.DAT'),PRE(lrtn),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lrtn:RefNumber),NOCASE,PRIMARY
ClubNokiaKey             KEY(lrtn:ClubNokiaSite,lrtn:Date),DUP,NOCASE
DateKey                  KEY(lrtn:Date),DUP,NOCASE
ReturnsNoKey             KEY(lrtn:ReturnsNumber,lrtn:Date),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
ClubNokiaSite               STRING(30)
ModelNumber                 STRING(30)
Quantity                    LONG
ReturnsNumber               STRING(30)
Date                        DATE
ReturnType                  STRING(3)
DummyField                  STRING(1)
To_Site                     STRING(30)
SalesCode                   STRING(30)
                         END
                     END                       

PRODCODE             FILE,DRIVER('Btrieve'),OEM,NAME('PRODCODE.DAT'),PRE(prd),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(prd:RecordNumber),NOCASE,PRIMARY
ProductCodeKey           KEY(prd:ProductCode),NOCASE
ModelProductKey          KEY(prd:ModelNumber,prd:ProductCode),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ModelNumber                 STRING(30)
ProductCode                 STRING(30)
DummyField                  STRING(2)
                         END
                     END                       

LOG2TEMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name3),PRE(lo2tmp),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lo2tmp:RefNumber),NOCASE,PRIMARY
IMEIKey                  KEY(lo2tmp:IMEI),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
IMEI                        STRING(30)
Marker                      BYTE
                         END
                     END                       

LOGSTOCK             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOCK.DAT'),PRE(logsto),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logsto:RefNumber),NOCASE,PRIMARY
SalesKey                 KEY(logsto:SalesCode),DUP,NOCASE
DescriptionKey           KEY(logsto:Description),DUP,NOCASE
SalesModelNoKey          KEY(logsto:SalesCode,logsto:ModelNumber),NOCASE
RefModelNoKey            KEY(logsto:RefNumber,logsto:ModelNumber),DUP,NOCASE
ModelNumberKey           KEY(logsto:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
ModelNumber                 STRING(30)
DummyField                  STRING(4)
                         END
                     END                       

LABLGTMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name2),PRE(lab),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lab:RefNumber),NOCASE,PRIMARY
DateKey                  KEY(lab:Date,lab:AccountNo,lab:Postcode),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
AccountNo                   STRING(15)
AccountName                 STRING(30)
Postcode                    STRING(15)
Date                        DATE
JobNumber                   LONG
ConsignmentNo               STRING(30)
Count                       LONG
                         END
                     END                       

LETTERS              FILE,DRIVER('Btrieve'),OEM,NAME('LETTERS.DAT'),PRE(let),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(let:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(let:Description),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(255)
Subject                     STRING(255)
TelephoneNumber             STRING(20)
FaxNumber                   STRING(20)
LetterText                  STRING(10000)
UseStatus                   STRING(3)
Status                      STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

EXPGEN               FILE,DRIVER('ASCII'),OEM,NAME(glo:file_name),PRE(gen),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Line1                       STRING(2000)
                         END
                     END                       

STOAUDIT             FILE,DRIVER('Btrieve'),NAME('STOAUDIT.DAT'),PRE(stoa),CREATE,BINDABLE,THREAD
Internal_AutoNumber_Key  KEY(stoa:Internal_AutoNumber),NOCASE,PRIMARY
Audit_Ref_No_Key         KEY(stoa:Audit_Ref_No),DUP,NOCASE
Cellular_Key             KEY(stoa:Site_Location,stoa:Stock_Ref_No),DUP,NOCASE
Stock_Ref_No_Key         KEY(stoa:Stock_Ref_No),DUP,NOCASE
Stock_Site_Number_Key    KEY(stoa:Audit_Ref_No,stoa:Site_Location),DUP,NOCASE
Lock_Down_Key            KEY(stoa:Audit_Ref_No,stoa:Stock_Ref_No),NOCASE
Record                   RECORD,PRE()
Internal_AutoNumber         LONG
Site_Location               STRING(30)
Stock_Ref_No                LONG
Original_Level              LONG
New_Level                   LONG
Audit_Reason                STRING(255)
Audit_Ref_No                LONG
Preliminary                 STRING(1)
Confirmed                   STRING(1)
                         END
                     END                       

DEFAULT2             FILE,DRIVER('Btrieve'),OEM,NAME('DEFAULT2.DAT'),PRE(de2),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(de2:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserSkillLevel              BYTE
Inv2CompanyName             STRING(30)
Inv2AddressLine1            STRING(30)
Inv2AddressLine2            STRING(30)
Inv2AddressLine3            STRING(30)
Inv2Postcode                STRING(15)
Inv2TelephoneNumber         STRING(15)
Inv2FaxNumber               STRING(15)
Inv2EmailAddress            STRING(255)
Inv2VATNumber               STRING(30)
CurrencySymbol              STRING(3)
UseRequisitionNumber        BYTE
PickEngStockOnly            BYTE
AllocateEngPassword         BYTE
PrintRetainedAccLabel       BYTE
                         END
                     END                       

ACTION               FILE,DRIVER('Btrieve'),OEM,NAME('ACTION.DAT'),PRE(act),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(act:Record_Number),NOCASE,PRIMARY
Action_Key               KEY(act:Action),NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Action                      STRING(30)
                         END
                     END                       

DEFRAPID             FILE,DRIVER('Btrieve'),OEM,NAME('DEFRAPID'),PRE(der),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(der:Record_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Record_Number               LONG
Print_Job_Card              STRING(20)
Print_Job_Label             STRING(3)
Use_Transit_Type            STRING(20)
Transit_Type                STRING(30)
Use_Workshop                STRING(20)
Workshop                    STRING(20)
                         END
                     END                       

RETSALES             FILE,DRIVER('Btrieve'),OEM,NAME('RETSALES.DAT'),PRE(ret),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ret:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(ret:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(ret:Despatched,ret:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(ret:Despatched,ret:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(ret:Despatched,ret:Account_Number,ret:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(ret:Account_Number,ret:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(ret:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(ret:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(ret:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(ret:Account_Number,ret:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(ret:Despatched,ret:Purchase_Order_Number,ret:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(ret:Despatch_Number,ret:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
                         END
                     END                       

BOUNCER              FILE,DRIVER('Btrieve'),OEM,NAME('BOUNCER.DAT'),PRE(bou),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(bou:Record_Number),NOCASE,PRIMARY
Bouncer_Job_Number_Key   KEY(bou:Original_Ref_Number,bou:Bouncer_Job_Number),NOCASE
Bouncer_Job_Only_Key     KEY(bou:Bouncer_Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Original_Ref_Number         REAL
Bouncer_Job_Number          REAL
                         END
                     END                       

TEAMS                FILE,DRIVER('Btrieve'),OEM,NAME('TEAMS.DAT'),PRE(tea),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(tea:Record_Number),NOCASE,PRIMARY
Team_Key                 KEY(tea:Team),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Team                        STRING(30)
Completed_Jobs              LONG
                         END
                     END                       

MERGE                FILE,DRIVER('Btrieve'),OEM,NAME('MERGE.DAT'),PRE(mer),BINDABLE,CREATE,THREAD
RefNumberKey             KEY(mer:RefNumber),NOCASE,PRIMARY
FieldNameKey             KEY(mer:FieldName),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
FieldName                   STRING(30)
FileName                    STRING(60)
Type                        STRING(3)
Description                 STRING(255)
Capitals                    BYTE
                         END
                     END                       

UPDDATA              FILE,DRIVER('Btrieve'),OEM,NAME('UPDDATA.DAT'),PRE(upd),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(upd:RefNumber),NOCASE,PRIMARY
TypeKey                  KEY(upd:Type,upd:DataField),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Type                        STRING(3)
DataField                   STRING(30)
DataField2                  STRING(30)
DataField3                  STRING(30)
DataField4                  STRING(30)
DataField5                  STRING(30)
DataField6                  STRING(30)
DataField7                  STRING(30)
Description                 STRING(500)
                         END
                     END                       

EXPSPARES            FILE,DRIVER('BASIC'),OEM,NAME(glo:file_name),PRE(expspa),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    STRING(30)
Sale_Cost                   STRING(20)
Total_Cost                  STRING(20)
                         END
                     END                       

WEBDEFLT             FILE,DRIVER('Btrieve'),OEM,NAME('WEBDEFLT.DAT'),PRE(web),CREATE,BINDABLE,THREAD
ordnokey                 KEY(web:ordno),NOCASE,PRIMARY
Record                   RECORD,PRE()
ordno                       LONG
                         END
                     END                       

EXCAUDIT             FILE,DRIVER('Btrieve'),OEM,NAME('EXCAUDIT.DAT'),PRE(exa),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(exa:Record_Number),NOCASE,PRIMARY
Audit_Number_Key         KEY(exa:Stock_Type,exa:Audit_Number),NOCASE
StockUnitNoKey           KEY(exa:Stock_Unit_Number),DUP,NOCASE
ReplaceUnitNoKey         KEY(exa:Replacement_Unit_Number),DUP,NOCASE
TypeStockNumber          KEY(exa:Stock_Type,exa:Stock_Unit_Number,exa:Audit_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Stock_Type                  STRING(30)
Audit_Number                REAL
Stock_Unit_Number           REAL
Replacement_Unit_Number     REAL
                         END
                     END                       

COMMONFA             FILE,DRIVER('Btrieve'),OEM,NAME('COMMONFA.DAT'),PRE(com),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(com:Ref_Number),NOCASE,PRIMARY
Description_Key          KEY(com:Model_Number,com:Category,com:Description),DUP,NOCASE
DescripOnlyKey           KEY(com:Model_Number,com:Description),DUP,NOCASE
Ref_Model_Key            KEY(com:Model_Number,com:Category,com:Ref_Number),DUP,NOCASE
RefOnlyKey               KEY(com:Model_Number,com:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Category                    STRING(30)
date_booked                 DATE
time_booked                 TIME
who_booked                  STRING(3)
Model_Number                STRING(30)
Description                 STRING(30)
Chargeable_Job              STRING(3)
Chargeable_Charge_Type      STRING(30)
Chargeable_Repair_Type      STRING(30)
Warranty_Job                STRING(3)
Warranty_Charge_Type        STRING(30)
Warranty_Repair_Type        STRING(30)
Auto_Complete               STRING(3)
Attach_Diagram              STRING(3)
Diagram_Setting             STRING(1)
Diagram_Path                STRING(255)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(255)
Fault_Code11                  STRING(255)
Fault_Code12                  STRING(255)
                            END
Invoice_Text                STRING(255)
Engineers_Notes             STRING(255)
DummyField                  STRING(2)
                         END
                     END                       

PARAMSS              FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(prm),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Account_Ref                 STRING(8)
Name                        STRING(60)
Address_1                   STRING(60)
Address_2                   STRING(60)
Address_3                   STRING(60)
Address_4                   STRING(60)
Address_5                   STRING(60)
Del_Address_1               STRING(60)
Del_Address_2               STRING(60)
Del_Address_3               STRING(60)
Del_Address_4               STRING(60)
Del_Address_5               STRING(60)
Cust_Tel_Number             STRING(30)
Contact_Name                STRING(30)
Notes_1                     STRING(60)
Notes_2                     STRING(60)
Notes_3                     STRING(60)
Taken_By                    STRING(60)
Order_Number                STRING(7)
Cust_Order_Number           STRING(30)
Payment_Ref                 STRING(8)
Global_Nom_Code             STRING(8)
Global_Details              STRING(60)
Items_Net                   STRING(8)
Items_Tax                   STRING(8)
Stock_Code                  STRING(30)
Description                 STRING(60)
Nominal_Code                STRING(8)
Qty_Order                   STRING(8)
Unit_Price                  STRING(8)
Net_Amount                  STRING(8)
Tax_Amount                  STRING(8)
Comment_1                   STRING(60)
Comment_2                   STRING(60)
Unit_Of_Sale                STRING(8)
Full_Net_Amount             STRING(8)
Invoice_Date                STRING(10)
Data_filepath               STRING(255)
User_Name                   STRING(30)
Password                    STRING(30)
Set_Invoice_Number          STRING(8)
Invoice_No                  STRING(8)
                         END
                     END                       

INSJOBS              FILE,DRIVER('Btrieve'),OEM,NAME('INSJOBS.DAT'),PRE(ijob),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(-ijob:RefNumber),NOCASE,PRIMARY
CompletedKey             KEY(ijob:EndDate,ijob:RefNumber),DUP,NOCASE
InvoiceNumberKey         KEY(ijob:InvoiceNumber,ijob:RefNumber),DUP,NOCASE
JobTypeKey               KEY(ijob:JobType),DUP,NOCASE
JobStatusKey             KEY(ijob:JobStatus),DUP,NOCASE
CompanyKey               KEY(ijob:AccountName),DUP,NOCASE
ContactKey               KEY(ijob:ContactName),DUP,NOCASE
PostCodeKey              KEY(ijob:Postcode),DUP,NOCASE
AllocJobKey              KEY(ijob:SubContractor,-ijob:RefNumber),DUP,NOCASE
AllocCompKey             KEY(ijob:SubContractor,ijob:AccountName),DUP,NOCASE
AllocPostCodeKey         KEY(ijob:SubContractor,ijob:Postcode),DUP,NOCASE
AllocStatusKey           KEY(ijob:SubContractor,ijob:JobStatus),DUP,NOCASE
AllocContactKey          KEY(ijob:SubContractor,ijob:ContactName),DUP,NOCASE
KeyClient                KEY(ijob:Client),DUP,NOCASE
KeyClientCoord           KEY(ijob:Client,ijob:WhoBooked),DUP,NOCASE
KeyAllocClient           KEY(ijob:SubContractor,ijob:WhoBooked,ijob:Client),DUP,NOCASE
KeyCoord                 KEY(ijob:WhoBooked),DUP,NOCASE
KeyAllocCoord            KEY(ijob:SubContractor,ijob:WhoBooked),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
DateBooked                  DATE
TimeBooked                  TIME
WhoBooked                   STRING(3)
AccountNumber               STRING(30)
AccountName                 STRING(30)
Postcode1                   STRING(4)
Postcode2                   STRING(4)
BuildingName                STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
MobileNumber                STRING(30)
OtherNumber                 STRING(30)
ContactName                 STRING(30)
EmailAddress                STRING(255)
OrderNumber                 STRING(30)
NoOfJobs                    LONG
EngineersOnSite             LONG
SubContractor               STRING(30)
StartDate                   DATE
StartTime                   TIME
EndDate                     DATE
EndTime                     TIME
Completed                   BYTE
SpecialInstruct             STRING(255)
InvoiceNumber               LONG
DummyField                  STRING(13)
AssStock1                   BYTE
AssStock2                   BYTE
AssStock3                   BYTE
AssStock4                   BYTE
AssStock5                   BYTE
AssStock6                   BYTE
AssStock7                   BYTE
AssStock8                   BYTE
AssStock9                   BYTE
AssStock10                  BYTE
AssStock11                  BYTE
AssStock12                  BYTE
AssStock13                  BYTE
AssStock14                  BYTE
AssStock15                  BYTE
AssStock16                  BYTE
JobType                     STRING(30)
JobStatus                   STRING(30)
Invoiced                    BYTE
Surveyed                    BYTE
EngineerInfo                STRING(255)
Client                      STRING(30)
DolphinRef                  STRING(20)
MainJobType                 BYTE
CityLinkRef                 STRING(30)
NoStoreys                   LONG
ApproxHeight                LONG
Brick                       BYTE
Glass                       BYTE
House                       CSTRING(20)
Portacabin                  BYTE
CladdingDesign              BYTE
POD                         STRING(100)
Postcode                    STRING(10)
Projected                   LONG
                         END
                     END                       

CONSIGN              FILE,DRIVER('Btrieve'),OEM,NAME('CONSIGN.DAT'),PRE(cns),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(cns:Record_Number),NOCASE,PRIMARY
Consignment_Number_Key   KEY(cns:Consignment_Note_Number,cns:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Consignment_Note_Number     STRING(30)
Ref_Number                  REAL
Type                        STRING(3)
                         END
                     END                       

JOBEXACC             FILE,DRIVER('Btrieve'),OEM,NAME('JOBEXACC.DAT'),PRE(jea),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jea:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(jea:Job_Ref_Number,jea:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Job_Ref_Number              REAL
Stock_Ref_Number            REAL
Part_Number                 STRING(30)
Description                 STRING(30)
                         END
                     END                       

MODELCOL             FILE,DRIVER('Btrieve'),OEM,NAME('MODELCOL.DAT'),PRE(moc),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(moc:Record_Number),NOCASE,PRIMARY
Colour_Key               KEY(moc:Model_Number,moc:Colour),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Colour                      STRING(30)
                         END
                     END                       

COMMCAT              FILE,DRIVER('Btrieve'),OEM,NAME('COMMCAT.DAT'),PRE(cmc),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(cmc:Record_Number),NOCASE,PRIMARY
Category_Key             KEY(cmc:Model_Number,cmc:Category),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Category                    STRING(30)
                         END
                     END                       

MESSAGES             FILE,DRIVER('Btrieve'),NAME('MESSAGES.DAT'),PRE(mes),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(mes:Record_Number),NOCASE,PRIMARY
Read_Key                 KEY(mes:Read,mes:Message_For,-mes:Date),DUP,NOCASE
Who_To_Key               KEY(mes:Message_For,-mes:Date),DUP,NOCASE
Who_From_Key             KEY(mes:Message_For,mes:Message_From),DUP,NOCASE
Read_Only_Key            KEY(mes:Read,-mes:Date),DUP,NOCASE
Date_Only_Key            KEY(-mes:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Date                        DATE
Time                        TIME
Message_For                 STRING(3)
Message_From                STRING(3)
Comment                     STRING(80)
Read                        STRING(3)
Message_Memo                STRING(1000)
                         END
                     END                       

LOGEXHE              FILE,DRIVER('Btrieve'),OEM,NAME('LOGEXHE.DAT'),PRE(log1),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(log1:Batch_No),NOCASE,PRIMARY
Processed_Key            KEY(log1:Processed,log1:Batch_No),DUP,NOCASE
Record                   RECORD,PRE()
Batch_No                    LONG
Processed                   STRING(1)
Date                        DATE
SMPF_No                     STRING(30)
ModelNumber                 STRING(30)
Qty                         LONG
                         END
                     END                       

DEFCRC               FILE,DRIVER('Btrieve'),OEM,NAME('DEFCRC.DAT'),PRE(dfc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(dfc:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
TransitType                 STRING(30)
Location                    STRING(30)
PAccountNumber              STRING(30)
PTransitType                STRING(30)
PLocation                   STRING(30)
EAccountNumber              STRING(30)
ETransitType                STRING(30)
ELocation                   STRING(30)
SAccountNumber              STRING(30)
STransitType                STRING(30)
STransitType2               STRING(30)
SLocation                   STRING(30)
                         END
                     END                       

PAYTYPES             FILE,DRIVER('Btrieve'),NAME('PAYTYPES.DAT'),PRE(pay),CREATE,BINDABLE,THREAD
Payment_Type_Key         KEY(pay:Payment_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Payment_Type                STRING(30)
Credit_Card                 STRING(3)
                         END
                     END                       

COMMONWP             FILE,DRIVER('Btrieve'),NAME('COMMONWP.DAT'),PRE(cwp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cwp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(cwp:Ref_Number),DUP,NOCASE
Description_Key          KEY(cwp:Ref_Number,cwp:Description),DUP,NOCASE
RefPartNumberKey         KEY(cwp:Ref_Number,cwp:Part_Number),DUP,NOCASE
PartNumberKey            KEY(cwp:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

LOGEXCH              FILE,DRIVER('Btrieve'),NAME('LOGEXCH.DAT'),PRE(xch1),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(xch1:Batch_Number),DUP,NOCASE
Ref_Number_Key           KEY(xch1:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(xch1:ESN),DUP,NOCASE
MSN_Only_Key             KEY(xch1:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(xch1:Stock_Type,xch1:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(xch1:Stock_Type,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(xch1:Stock_Type,xch1:ESN),DUP,NOCASE
MSN_Key                  KEY(xch1:Stock_Type,xch1:MSN),DUP,NOCASE
ESN_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:ESN),DUP,NOCASE
MSN_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:MSN),DUP,NOCASE
Ref_Available_Key        KEY(xch1:Available,xch1:Stock_Type,xch1:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(xch1:Available,xch1:Stock_Type,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(xch1:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(xch1:Available,xch1:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(xch1:Available,xch1:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(xch1:Available,xch1:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(xch1:Available,xch1:Model_Number,xch1:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
                         END
                     END                       

QAREASON             FILE,DRIVER('Btrieve'),OEM,NAME('QAREASON.DAT'),PRE(qar),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(qar:RecordNumber),NOCASE,PRIMARY
ReasonKey                KEY(qar:Reason),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Reason                      STRING(60)
                         END
                     END                       

COMMONCP             FILE,DRIVER('Btrieve'),NAME('COMMONCP.DAT'),PRE(ccp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ccp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ccp:Ref_Number),DUP,NOCASE
Description_Key          KEY(ccp:Ref_Number,ccp:Description),DUP,NOCASE
RefPartNumberKey         KEY(ccp:Ref_Number,ccp:Part_Number),DUP,NOCASE
PartNumberKey            KEY(ccp:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

ADDSEARCH            FILE,DRIVER('TOPSPEED'),OEM,NAME(glo:File_Name),PRE(addtmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(addtmp:RecordNumber),NOCASE,PRIMARY
AddressLine1Key          KEY(addtmp:AddressLine1),DUP,NOCASE
PostcodeKey              KEY(addtmp:Postcode),DUP,NOCASE
IncomingIMEIKey          KEY(addtmp:IncomingIMEI),DUP,NOCASE
ExchangedIMEIKey         KEY(addtmp:ExchangedIMEI),DUP,NOCASE
FinalIMEIKey             KEY(addtmp:FinalIMEI),DUP,NOCASE
JobNumberKey             KEY(addtmp:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
Postcode                    STRING(30)
JobNumber                   LONG
IncomingIMEI                STRING(30)
ExchangedIMEI               STRING(30)
FinalIMEI                   STRING(30)
Surname                     STRING(30)
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),NAME('CONTHIST.DAT'),PRE(cht),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE
Record_Number_Key        KEY(cht:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(1000)
                         END
                     END                       

CONTACTS             FILE,DRIVER('Btrieve'),NAME('CONTACTS.DAT'),PRE(con),CREATE,BINDABLE,THREAD
Name_Key                 KEY(con:Name),NOCASE,PRIMARY
Record                   RECORD,PRE()
Name                        STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name1               STRING(30)
Contact_Name2               STRING(30)
                         END
                     END                       

EXPJOBS              FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(expjob),BINDABLE,CREATE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Batch_Number                STRING(20)
who_booked                  STRING(20)
date_booked                 STRING(20)
time_booked                 STRING(20)
Current_Status              STRING(30)
Title                       STRING(20)
Initial                     STRING(20)
Surname                     STRING(30)
Account_Number              STRING(15)
Order_Number                STRING(30)
Chargeable_Job              STRING(20)
Charge_Type                 STRING(30)
Repair_Type                 STRING(30)
Warranty_Job                STRING(20)
Warranty_Charge_Type        STRING(30)
Repair_Type_Warranty        STRING(30)
Unit_Details_Group          GROUP
Model_Number                  STRING(30)
Manufacturer                  STRING(30)
ESN                           STRING(16)
MSN                           STRING(16)
Unit_Type                     STRING(30)
                            END
Mobile_Number               STRING(15)
Workshop                    STRING(20)
Location                    STRING(30)
Authority_Number            STRING(30)
DOP                         STRING(20)
Physical_Damage             STRING(20)
Intermittent_Fault          STRING(20)
Transit_Type                STRING(30)
Job_Priority                STRING(30)
Engineer                    STRING(20)
Address_Group               GROUP
Postcode                      STRING(20)
Company_Name                  STRING(30)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
Telephone_Number              STRING(15)
Fax_Number                    STRING(15)
                            END
Address_Collection_Group    GROUP
Postcode_Collection           STRING(20)
Company_Name_Collection       STRING(30)
Address_Line1_Collection      STRING(30)
Address_Line2_Collection      STRING(30)
Address_Line3_Collection      STRING(30)
Telephone_Collection          STRING(15)
                            END
Address_Delivery_Group      GROUP
Postcode_Delivery             STRING(20)
Company_Name_Delivery         STRING(30)
Address_Line1_Delivery        STRING(30)
Address_Line2_Delivery        STRING(30)
Address_Line3_Delivery        STRING(30)
Telephone_Delivery            STRING(15)
                            END
In_Repair_Group             GROUP
In_Repair                     STRING(20)
Date_In_Repair                STRING(20)
Time_In_Repair                STRING(20)
                            END
On_Test_Group               GROUP
On_Test                       STRING(20)
Date_On_Test                  STRING(20)
Time_On_Test                  STRING(20)
                            END
Completed_Group             GROUP
Date_Completed                STRING(20)
Time_Completed                STRING(20)
                            END
QA_Group                    GROUP
QA_Passed                     STRING(20)
Date_QA_Passed                STRING(20)
Time_QA_Passed                STRING(20)
QA_Rejected                   STRING(20)
Date_QA_Rejected              STRING(20)
Time_QA_Rejected              STRING(20)
QA_Second_Passed              STRING(20)
Date_QA_Second_Passed         STRING(20)
Time_QA_Second_Passed         STRING(20)
                            END
Estimate_Ready              STRING(20)
Estimate_Group              GROUP
Estimate                      STRING(20)
Estimate_If_Over              STRING(20)
Estimate_Accepted             STRING(20)
Estimate_Rejected             STRING(20)
                            END
Chargeable_Costs            GROUP
Courier_Cost                  STRING(20)
Labour_Cost                   STRING(20)
Parts_Cost                    STRING(20)
Sub_Total                     STRING(20)
                            END
Estimate_Costs              GROUP
Courier_Cost_Estimate         STRING(20)
Labour_Cost_Estimate          STRING(20)
Parts_Cost_Estimate           STRING(20)
Sub_Total_Estimate            STRING(20)
                            END
Warranty_Costs              GROUP
Courier_Cost_Warranty         STRING(20)
Labour_Cost_Warranty          STRING(20)
Parts_Cost_Warranty           STRING(20)
Sub_Total_Warranty            STRING(20)
                            END
Payment_Group               GROUP
Paid                          STRING(20)
Paid_Warranty                 STRING(20)
Date_Paid                     STRING(20)
Paid_User                     STRING(20)
                            END
Loan_Status                 STRING(30)
Loan_Issued_Date            STRING(20)
Loan_Unit_Number            STRING(20)
Loan_User                   STRING(20)
Loan_Despatched_Group       GROUP
Loan_Courier                  STRING(30)
Loan_Consignment_Number       STRING(30)
Loan_Despatched_User          STRING(20)
Loan_Despatch_Number          STRING(20)
                            END
Exchange_Status             STRING(30)
Exchange_Unit_Number        STRING(20)
Exchange_Issued_Date        STRING(20)
Exchange_User               STRING(20)
Exchange_Despatched_Group   GROUP
Exchange_Courier              STRING(30)
Exchange_Consignment_Number   STRING(30)
Exchange_Despatched_User      STRING(20)
Exchange_Despatch_Number      STRING(20)
                            END
Despatch_Group              GROUP
Date_Despatched               STRING(20)
Despatch_Number               STRING(20)
Despatch_User                 STRING(20)
Courier                       STRING(30)
Consignment_Number            STRING(30)
                            END
Incoming_Group              GROUP
Incoming_Courier              STRING(30)
Incoming_Consignment_Number   STRING(30)
Incoming_Date                 STRING(20)
                            END
Despatched                  STRING(20)
Third_Party_Site            STRING(30)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(255)
Fault_Code11                  STRING(255)
Fault_Code12                  STRING(255)
                            END
Special_Instructions        STRING(30)
EDI_Batch_Number            STRING(20)
Fault_Description           STRING(255)
Notes                       STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Fault_Report                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
Exchange_Reason             STRING(255)
                         END
                     END                       

EXPAUDIT             FILE,DRIVER('BASIC'),NAME(glo:file_name4),PRE(expaud),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Date                        STRING(20)
Time                        STRING(20)
User                        STRING(20)
Action                      STRING(80)
Notes                       STRING(10000)
                         END
                     END                       

JOBBATCH             FILE,DRIVER('Btrieve'),NAME('JOBBATCH.DAT'),PRE(jbt),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(jbt:Batch_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Batch_Number                REAL
Date                        DATE
Time                        TIME
                         END
                     END                       

DEFEDI2              FILE,DRIVER('Btrieve'),NAME('DEFEDI2.DAT'),PRE(ed2),CREATE,BINDABLE,THREAD
record_number_key        KEY(ed2:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
record_number               REAL
Country_Code                STRING(3)
                         END
                     END                       

DEFPRINT             FILE,DRIVER('Btrieve'),NAME('DEFPRINT.DAT'),PRE(dep),CREATE,BINDABLE,THREAD
Printer_Name_Key         KEY(dep:Printer_Name),NOCASE,PRIMARY
Record                   RECORD,PRE()
Printer_Name                STRING(60)
Printer_Path                STRING(255)
Background                  STRING(3)
Copies                      LONG
                         END
                     END                       

DEFWEB               FILE,DRIVER('Btrieve'),NAME('DEFWEB.DAT'),PRE(dew),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(dew:Record_Number),NOCASE,PRIMARY
Account_Number_Key       KEY(dew:Account_Number),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Account_Number              STRING(15)
Warranty_Charge_Type        STRING(30)
Location                    STRING(30)
Transit_Type                STRING(30)
Job_Priority                STRING(30)
Unit_Type                   STRING(30)
Status_Type                 STRING(30)
HandSet                     STRING(30)
Chargeable_Charge_Type      STRING(30)
Insurance_Charge_Type       STRING(30)
AllowJobBooking             BYTE
AllowJobProgress            BYTE
AllowOrderParts             BYTE
AllowOrderProgress          BYTE
Option5                     BYTE
Option6                     BYTE
Option7                     BYTE
Option8                     BYTE
Option9                     BYTE
Option10                    BYTE
                         END
                     END                       

EXPCITY              FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(epc),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Account_Number              STRING(8)
Ref_Number                  STRING(31)
Customer_Name               STRING(31)
Contact_Name                STRING(31)
Address_Line1               STRING(31)
Address_Line2               STRING(31)
Town                        STRING(31)
County                      STRING(31)
Postcode                    STRING(9)
City_Service                STRING(2)
City_Instructions           STRING(31)
Pudamt                      STRING(5)
Return_It                   STRING(2)
Saturday                    STRING(2)
Dog                         STRING(31)
Nol                         STRING(3)
                         END
                     END                       

PROCCODE             FILE,DRIVER('Btrieve'),NAME('PROCCODE.DAT'),PRE(pro),CREATE,BINDABLE,THREAD
Code_Number_Key          KEY(pro:Code_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Code_Number                 STRING(3)
Description                 STRING(30)
Allow_Loan                  STRING(3)
Accessory_Only              STRING(3)
                         END
                     END                       

COLOUR               FILE,DRIVER('Btrieve'),OEM,NAME('COLOUR.DAT'),PRE(col),CREATE,BINDABLE,THREAD
Colour_Key               KEY(col:Colour),NOCASE,PRIMARY
Record                   RECORD,PRE()
Colour                      STRING(30)
                         END
                     END                       

VODAIMP              FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(vdi),BINDABLE,THREAD
Record                   RECORD,PRE()
Line                        STRING(255)
                         END
                     END                       

JOBSVODA             FILE,DRIVER('Btrieve'),NAME('JOBSVODA.DAT'),PRE(jvf),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jvf:Ref_Number),NOCASE,PRIMARY
Ref_Pending_Key          KEY(jvf:Pending,jvf:Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(jvf:Pending,jvf:Order_Number),DUP,NOCASE
Process_Code_Key         KEY(jvf:Pending,jvf:Process_Code,jvf:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(jvf:Pending,jvf:Model_Number,jvf:Unit_Type),DUP,NOCASE
ESN_Key                  KEY(jvf:Pending,jvf:ESN),DUP,NOCASE
Job_Number_Key           KEY(jvf:Pending,jvf:Job_Number),DUP,NOCASE
Model_Ref_key            KEY(jvf:Pending,jvf:Model_Number,jvf:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Import_Query_Reason         STRING(1000)
Ref_Number                  REAL
Job_Number                  REAL
Status                      STRING(30)
Account_Number              STRING(15)
Account_Type                STRING(30)
Order_Number                STRING(30)
Process_Code                STRING(5)
Process_Description         STRING(30)
Job_Type                    STRING(30)
Customer_Name               STRING(60)
Contact_Number              STRING(15)
Mobile_Number               STRING(15)
Reported_Fault              STRING(2)
Delegation                  STRING(30)
Consignment_Number          STRING(30)
Date_Dispatch               DATE
Time_Dispatch               TIME
Accessory_String            STRING(255)
Pending_Reason              STRING(255)
Customer_Unit_Group         GROUP
ESN                           STRING(30)
Model_Number                  STRING(30)
Manufacturer                  STRING(30)
Unit_Type                     STRING(30)
Colour                        STRING(30)
                            END
Exchange_Unit_Group         GROUP
Exchange_Unit_Number          REAL
ESN_Exchange                  STRING(30)
Model_Number_Exchange         STRING(30)
Manufacturer_Exchange         STRING(30)
Unit_Type_Exchange            STRING(30)
                            END
Pending                     STRING(3)
Delivery_Address_Group      GROUP
Postcode_Delivery             STRING(10)
Address_Line1_Delivery        STRING(30)
Address_Line2_Delivery        STRING(30)
Address_Line3_Delivery        STRING(30)
                            END
Collection_Address_Group    GROUP
Postcode_Collection           STRING(10)
Address_Line1_Collection      STRING(30)
Address_Line2_Collection      STRING(30)
Address_Line3_Collection      STRING(30)
                            END
DummyField                  STRING(1)
                         END
                     END                       

XREPACT              FILE,DRIVER('BASIC'),NAME(glo:File_Name),PRE(xre),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Field1                      STRING(40)
Field2                      STRING(40)
Field3                      STRING(40)
Field4                      STRING(40)
                         END
                     END                       

ACCESDEF             FILE,DRIVER('Btrieve'),NAME('ACCESDEF.DAT'),PRE(acd),CREATE,BINDABLE,THREAD
Accessory_Key            KEY(acd:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Accessory                   STRING(30)
                         END
                     END                       

STANTEXT             FILE,DRIVER('Btrieve'),NAME('STANTEXT.DAT'),PRE(stt),CREATE,BINDABLE,THREAD
Description_Key          KEY(stt:Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
Description                 STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
DummyField                  STRING(1)
Text                        STRING(500)
                         END
                     END                       

NOTESENG             FILE,DRIVER('Btrieve'),NAME('NOTESENG.DAT'),PRE(noe),CREATE,BINDABLE,THREAD
Notes_Key                KEY(noe:Reference,noe:Notes),NOCASE,PRIMARY
Record                   RECORD,PRE()
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

IAUDIT               FILE,DRIVER('Btrieve'),OEM,PRE(iau),CREATE,BINDABLE,THREAD
KeyRecordNumber          KEY(iau:RecordNumber),NOCASE,PRIMARY
KeyRefNumber             KEY(iau:RefNumber,-iau:Date,-iau:Time,iau:Action),DUP,NOCASE
KeyAction                KEY(iau:RefNumber,iau:Action,-iau:Date,-iau:Time),DUP,NOCASE
KeyUser                  KEY(iau:RefNumber,iau:User,-iau:Date,-iau:Time),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                REAL
RefNumber                   LONG
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(255)
Dummy                       STRING(1)
                         END
                     END                       

IINVJOB              FILE,DRIVER('Btrieve'),OEM,PRE(IIN),CREATE,BINDABLE,THREAD
KeyInvNo                 KEY(IIN:InvoiceNo),NOCASE,PRIMARY
KeyPostedStatus          KEY(IIN:PostedStatus),DUP,NOCASE
KeySageNo                KEY(IIN:SageInvNo),DUP,NOCASE
Record                   RECORD,PRE()
InvoiceNo                   LONG
Customer                    STRING(30)
DateCreated                 DATE
PartCosts                   DECIMAL(12,2)
LabourCosts                 DECIMAL(12,2)
TotalNet                    DECIMAL(12,2)
PartVAT                     DECIMAL(12,2)
LabourVAT                   DECIMAL(12,2)
TotalVAT                    DECIMAL(12,2)
Gross                       DECIMAL(12,2)
SageInvNo                   LONG
PostedStatus                STRING(1)
RaisedInSage                STRING(1)
                         END
                     END                       

EXPLABG              FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(epg),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Account_Number              STRING(8)
Ref_Number                  STRING(31)
Customer_Name               STRING(31)
Contact_Name                STRING(31)
Address_Line1               STRING(31)
Address_Line2               STRING(31)
Address_Line3               STRING(31)
Address_Line4               STRING(31)
Postcode                    STRING(5)
City_Service                STRING(2)
City_Instructions           STRING(31)
Pudamt                      STRING(5)
Return_It                   STRING(2)
Saturday                    STRING(2)
Dog                         STRING(31)
Nol                         STRING(3)
JobNo                       STRING(9)
Weight                      STRING(6)
                         END
                     END                       

INSDEF               FILE,DRIVER('Btrieve'),OEM,NAME('INSDEF.DAT'),PRE(ind),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ind:RecordNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AssStock1                   STRING(30)
AssStock2                   STRING(30)
AssStock3                   STRING(30)
AssStock4                   STRING(30)
AssStock5                   STRING(30)
AssStock6                   STRING(30)
AssStock7                   STRING(30)
AssStock8                   STRING(30)
AssStock9                   STRING(30)
AssStock10                  STRING(30)
AssStock11                  STRING(30)
AssStock12                  STRING(30)
AssStock13                  STRING(30)
AssStock14                  STRING(30)
AssStock15                  STRING(30)
AssStock16                  STRING(30)
SMTPServer                  STRING(80)
SMTPPort                    USHORT
POP3Port                    USHORT
AskIfTooBig                 BYTE
SMTPPassword                STRING(80)
User                        STRING(80)
DeleteAfterDownload         BYTE
MaxKBytes                   LONG
DontSaveAttachments         BYTE
DontSaveEmbeds              BYTE
LocalAttachFolder           STRING(255)
POP3Server                  STRING(80)
UseDefaultLocation          BYTE
Location                    STRING(30)
EmailAddress                STRING(50)
DefExportPath               STRING(255)
Dummy                       STRING(1)
                         END
                     END                       

IEXPUSE              FILE,DRIVER('Btrieve'),OEM,PRE(iex1),CREATE,BINDABLE,THREAD
KeyRecNo                 KEY(iex1:RecNo),NOCASE,PRIMARY
KeyName                  KEY(iex1:Name),DUP,NOCASE
KeyExpNo                 KEY(iex1:ExpNo),DUP,NOCASE
KeyBrwExpNo              KEY(iex1:RecipientNo,iex1:ExpNo),NOCASE
KeyRecipientNo           KEY(iex1:RecipientNo),DUP,NOCASE
KeyBrowseName            KEY(iex1:RecipientNo,iex1:Name),DUP,NOCASE
Record                   RECORD,PRE()
RecNo                       LONG
RecipientNo                 LONG
ExpNo                       LONG
Name                        STRING(50)
Type                        STRING(1)
                         END
                     END                       

IRECEIVE             FILE,DRIVER('Btrieve'),OEM,PRE(ire),CREATE,BINDABLE,THREAD
KeyRecipientNo           KEY(ire:RecipientNo),NOCASE,PRIMARY
KeyCompany               KEY(ire:Company),DUP,NOCASE
KeyName                  KEY(ire:Name),DUP,NOCASE
Record                   RECORD,PRE()
RecipientNo                 LONG
Name                        STRING(30)
Company                     STRING(30)
EmailAddress                STRING(50)
                         END
                     END                       

IJOBSTATUS           FILE,DRIVER('Btrieve'),OEM,PRE(ij01),CREATE,BINDABLE,THREAD
JobStatusNoKey           KEY(ij01:JobStatusNo),NOCASE,PRIMARY
JobStatusKey             KEY(ij01:JobStatus),NOCASE
Record                   RECORD,PRE()
JobStatusNo                 LONG
JobStatus                   STRING(30)
                         END
                     END                       

ICLCOST              FILE,DRIVER('Btrieve'),OEM,PRE(ICL1),CREATE,BINDABLE,THREAD
KeyICLNo                 KEY(ICL1:ICLNo),NOCASE,PRIMARY
KeyStatusNo              KEY(ICL1:StatusNo),DUP,NOCASE
KeyBrwStatus             KEY(ICL1:ClientNo,ICL1:Status),NOCASE
KeyBrwStatusNo           KEY(ICL1:ClientNo,ICL1:StatusNo),DUP,NOCASE
KeyStatus                KEY(ICL1:Status),DUP,NOCASE
Record                   RECORD,PRE()
ICLNo                       LONG
ClientNo                    LONG
Status                      STRING(30)
StatusNo                    LONG
LabCost                     DECIMAL(7,2)
Dummy                       STRING(1)
                         END
                     END                       

IVEHDETS             FILE,DRIVER('Btrieve'),OEM,PRE(ive),CREATE,BINDABLE,THREAD
KeyVehDetNo              KEY(ive:VehDetNo),NOCASE,PRIMARY
KeyJobNo                 KEY(ive:JobNumber),DUP,NOCASE
BrwMakeKey               KEY(ive:JobNumber,ive:Make),DUP,NOCASE
KeyRegNo                 KEY(ive:Registration),DUP,NOCASE
KeyMobileNo              KEY(ive:MobileNo),DUP,NOCASE
KeyBrwMobileNo           KEY(ive:JobNumber,ive:MobileNo),DUP,NOCASE
MakeKey                  KEY(ive:Make),DUP,NOCASE
ModelKey                 KEY(ive:Model),DUP,NOCASE
BrwModelKey              KEY(ive:JobNumber,ive:Model),DUP,NOCASE
KeyFitted                KEY(ive:Fitted),DUP,NOCASE
KeyBrwFitted             KEY(ive:JobNumber,ive:Fitted),DUP,NOCASE
KeyBrwRegNo              KEY(ive:JobNumber,ive:Registration),DUP,NOCASE
KeyModelNo               KEY(ive:ModelNo),DUP,NOCASE
KeyMakeNo                KEY(ive:MakeNo),DUP,NOCASE
Record                   RECORD,PRE()
VehDetNo                    LONG
JobNumber                   LONG
Make                        STRING(30)
Model                       STRING(30)
Registration                STRING(10)
Dummy                       STRING(7)
Fitted                      STRING(1)
MobileNo                    STRING(30)
FittedEngineer              STRING(30)
FittedContractor            STRING(30)
FittedContractorNo          LONG
MakeNo                      LONG
ModelNo                     LONG
                         END
                     END                       

IAPPENG              FILE,DRIVER('Btrieve'),OEM,PRE(IAP1),CREATE,BINDABLE,THREAD
KeyAppEngNo              KEY(IAP1:AppEngNo),NOCASE,PRIMARY
KeyAppEngineer           KEY(IAP1:ApptNo,IAP1:AppEngineer),NOCASE
KeyApptNo                KEY(IAP1:ApptNo),DUP,NOCASE
KeyContractEng           KEY(IAP1:ContractorNo,IAP1:AppEngineer),DUP,NOCASE
Record                   RECORD,PRE()
AppEngNo                    LONG
ApptNo                      LONG
JobNo                       LONG
ContractorNo                LONG
AppEngineer                 STRING(30)
                         END
                     END                       

IAPPTYPE             FILE,DRIVER('Btrieve'),OEM,NAME('IAPPTYPE.DAT'),PRE(iap),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(iap:RecordNumber),NOCASE,PRIMARY
AppTypeKey               KEY(iap:AppType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AppType                     STRING(30)
                         END
                     END                       

INSMAKE              FILE,DRIVER('Btrieve'),OEM,PRE(INS2),CREATE,BINDABLE,THREAD
KeyMakeNo                KEY(INS2:MakeNo),NOCASE,PRIMARY
KeyMake                  KEY(INS2:Make),NOCASE
Record                   RECORD,PRE()
MakeNo                      LONG
Make                        STRING(30)
                         END
                     END                       

INSAPPS              FILE,DRIVER('Btrieve'),OEM,NAME('INSAPPS.DAT'),PRE(ins),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ins:RecordNumber),NOCASE,PRIMARY
AccountDateKey           KEY(ins:AccountNumber,ins:Date),DUP,NOCASE
DateKey                  KEY(ins:Date,ins:AccountNumber),DUP,NOCASE
JobDateKey               KEY(ins:RefNumber,ins:Date),DUP,NOCASE
KeyDayTime               KEY(ins:DayTime),DUP,NOCASE
KeyAppDate               KEY(ins:Date),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
RefNumber                   LONG
Date                        DATE
Time                        STRING(30)
DayTime                     STRING(30)
Type                        STRING(30)
Comment                     STRING(255)
Printed                     BYTE
DummyField                  STRING(4)
                         END
                     END                       

ISUBCONT             FILE,DRIVER('Btrieve'),OEM,NAME('ISUBCONT.DAT'),PRE(isub),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(isub:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(isub:AccountNumber),NOCASE
BusinessNameKey          KEY(isub:BusinessName,isub:AccountNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ContactName                 STRING(30)
AccountNumber               STRING(30)
BusinessName                STRING(30)
Postcode                    STRING(20)
BuildingName                STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
BusinessNumber              STRING(20)
FaxNumber                   STRING(20)
MobileNumber                STRING(20)
OtherNumber                 STRING(20)
EmailAddress                STRING(255)
DailyWorkLoad               LONG
Rating                      LONG
NumInstallers               LONG
ExpDatePubLiabIns           DATE
                         END
                     END                       

TRDSPEC              FILE,DRIVER('Btrieve'),NAME('TRDSPEC.DAT'),PRE(tsp),CREATE,BINDABLE,THREAD
Short_Description_Key    KEY(tsp:Short_Description),NOCASE,PRIMARY
Record                   RECORD,PRE()
Short_Description           STRING(30)
Long_Description            STRING(255)
                         END
                     END                       

ITIMES               FILE,DRIVER('Btrieve'),OEM,PRE(tim),CREATE,BINDABLE,THREAD
KeyTimeNo                KEY(tim:TimeNo),NOCASE,PRIMARY
KeyComputed              KEY(tim:Computed),NOCASE
Record                   RECORD,PRE()
TimeNo                      LONG
TimeFrom                    LONG
TimeTo                      LONG
Computed                    STRING(30)
                         END
                     END                       

IEQUIP               FILE,DRIVER('Btrieve'),OEM,NAME('IEQUIP.DAT'),PRE(iequ),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(iequ:RecordNumber),NOCASE,PRIMARY
JobPartNumberKey         KEY(iequ:RefNumber,iequ:PartNumber),DUP,NOCASE
RefPendRefKey            KEY(iequ:RefNumber,iequ:PendingRefNumber),DUP,NOCASE
ManufacturerKey          KEY(iequ:Manufacturer),DUP,NOCASE
BrwManufacturer          KEY(iequ:RefNumber,iequ:Manufacturer),DUP,NOCASE
KeyPartNumber            KEY(iequ:PartNumber),DUP,NOCASE
KeyDescription           KEY(iequ:Description),DUP,NOCASE
KeyBrwDescription        KEY(iequ:RefNumber,iequ:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
Supplier                    STRING(30)
Quantity                    LONG
ExcludeFromOrder            BYTE
DespatchNoteNumber          STRING(30)
PartRefNumber               LONG
DateOrdered                 DATE
PendingRefNumber            LONG
OrderNumber                 LONG
OrderPartNumber             LONG
DateReceived                DATE
Manufacturer                STRING(30)
Dummy                       STRING(3)
                         END
                     END                       

EPSCSV               FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(eps),BINDABLE,THREAD
Record                   RECORD,PRE()
LABEL1                      STRING(32)
LABEL2                      STRING(32)
LABEL3                      STRING(32)
LABEL4                      STRING(32)
LABEL5                      STRING(32)
LABEL6                      STRING(32)
LABEL7                      STRING(32)
LABEL8                      STRING(32)
LABEL9                      STRING(32)
LABEL10                     STRING(32)
LABEL11                     STRING(32)
LABEL12                     STRING(32)
LABEL13                     STRING(32)
LABEL14                     STRING(32)
LABEL15                     STRING(1000)
LABEL16                     STRING(32)
                         END
                     END                       

IENGINEERS           FILE,DRIVER('Btrieve'),OEM,PRE(ien),CREATE,BINDABLE,THREAD
KeyEngineerNo            KEY(ien:EngineerNo),NOCASE,PRIMARY
KeyEngineer              KEY(ien:Engineer),DUP,NOCASE
KeyBrwEngineer           KEY(ien:ContractorNo,ien:Engineer),NOCASE
KeyContractorNo          KEY(ien:ContractorNo),DUP,NOCASE
Record                   RECORD,PRE()
EngineerNo                  LONG
ContractorNo                LONG
Engineer                    STRING(30)
DatePassedCityGuilds        DATE
InstallPassExpiryDate       DATE
Passed                      STRING(1)
GotPass                     STRING(1)
                         END
                     END                       

ICLIENTS             FILE,DRIVER('Btrieve'),OEM,PRE(icl),CREATE,BINDABLE,THREAD
KeyClientNo              KEY(icl:ClientNo),NOCASE,PRIMARY
KeyClientName            KEY(icl:ClientName),NOCASE
Record                   RECORD,PRE()
ClientNo                    LONG
ClientName                  STRING(30)
LogoPath                    STRING(255)
                         END
                     END                       

ISUBPOST             FILE,DRIVER('Btrieve'),OEM,NAME('ISUBPOST.DAT'),PRE(ispt),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ispt:RecordNumber),NOCASE,PRIMARY
RefPostCodeKey           KEY(ispt:RefNumber,ispt:Postcode),DUP,NOCASE
PostcodeType             KEY(ispt:Postcode,ispt:Type,ispt:RefNumber),DUP,NOCASE
PostCodeRefKey           KEY(ispt:Postcode,ispt:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Postcode                    STRING(4)
Type                        BYTE
DummyField                  STRING(2)
                         END
                     END                       

IJOBTYPE             FILE,DRIVER('Btrieve'),OEM,PRE(ijo),CREATE,BINDABLE,THREAD
JobTypeNoKey             KEY(ijo:JobTypeNo),NOCASE,PRIMARY
JobTypeKey               KEY(ijo:JobType),NOCASE
Record                   RECORD,PRE()
JobTypeNo                   LONG
JobType                     STRING(30)
LabourCost                  DECIMAL(7,2)
DummyField                  STRING(1)
                         END
                     END                       

EXPGENDM             FILE,DRIVER('BASIC'),OEM,NAME(glo:file_name),PRE(exp),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
line1                       STRING(60),DIM(50)
                         END
                     END                       

EDIBATCH             FILE,DRIVER('Btrieve'),NAME('EDIBATCH.DAT'),PRE(ebt),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(ebt:Manufacturer,ebt:Batch_Number),NOCASE
Record                   RECORD,PRE()
Batch_Number                REAL
Manufacturer                STRING(30)
Date                        DATE
Time                        TIME
                         END
                     END                       

ORDPEND              FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ope),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ope:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ope:Supplier,ope:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ope:Supplier,ope:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ope:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ope:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ope:Awaiting_Stock,ope:Supplier,ope:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ope:PartRecordNumber),DUP,NOCASE
Job_Number_Key           KEY(ope:Job_Number),DUP,NOCASE
Supplier_Job_Key         KEY(ope:Supplier,ope:Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                     END                       

STOHIST              FILE,DRIVER('Btrieve'),NAME('STOHIST.DAT'),PRE(shi),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(shi:Ref_Number,shi:Date),DUP,NOCASE
record_number_key        KEY(shi:Record_Number),NOCASE,PRIMARY
Transaction_Type_Key     KEY(shi:Ref_Number,shi:Transaction_Type,shi:Date),DUP,NOCASE
DateKey                  KEY(shi:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  LONG
User                        STRING(3)
Transaction_Type            STRING(3)
Despatch_Note_Number        STRING(30)
Job_Number                  LONG
Sales_Number                LONG
Quantity                    REAL
Date                        DATE
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Notes                       STRING(255)
Information                 STRING(255)
                         END
                     END                       

NEWFEAT              FILE,DRIVER('Btrieve'),NAME('NEWFEAT.DAT'),PRE(fea),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(fea:Record_Number),NOCASE,PRIMARY
date_key                 KEY(-fea:date,fea:description),DUP,NOCASE
description_Key          KEY(fea:description,fea:date),DUP,NOCASE
DateTypeKey              KEY(fea:ReportType,-fea:date,fea:description),DUP,NOCASE
DescriptionTypeKey       KEY(fea:ReportType,fea:description),DUP,NOCASE
DescriptionOnlyKey       KEY(fea:description),DUP,NOCASE
RefNoKey                 KEY(fea:RefNumber),DUP,NOCASE
RefNoTypeKey             KEY(fea:ReportType,fea:RefNumber),DUP,NOCASE
Date_Key_Ascending       KEY(fea:date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
date                        DATE
description                 STRING(60)
Text                        STRING(1000)
ReportType                  BYTE
RefNumber                   STRING(30)
DocumentPath                STRING(255)
                         END
                     END                       

EXREASON             FILE,DRIVER('Btrieve'),OEM,NAME('EXREASON.DAT'),PRE(exr),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(exr:RecordNumber),NOCASE,PRIMARY
ReasonKey                KEY(exr:ReasonType,exr:Reason),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReasonType                  BYTE
Reason                      STRING(80)
                         END
                     END                       

REPTYDEF             FILE,DRIVER('Btrieve'),NAME('REPTYDEF.DAT'),PRE(rtd),CREATE,BINDABLE,THREAD
Repair_Type_Key          KEY(rtd:Repair_Type),NOCASE,PRIMARY
Chargeable_Key           KEY(rtd:Chargeable,rtd:Repair_Type),DUP,NOCASE
Warranty_Key             KEY(rtd:Warranty,rtd:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
WarrantyCode                STRING(5)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
BER                         BYTE
                         END
                     END                       

EPSIMP               FILE,DRIVER('Btrieve'),NAME(glo:file_name2),PRE(epi),BINDABLE,CREATE,THREAD
Record_Number_Key        KEY(epi:Record_Number),NOCASE,PRIMARY
Fault_Description           MEMO(1000)
Record                   RECORD,PRE()
Record_Number               REAL
Model_Number                STRING(30)
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Telephone_Collection        STRING(15)
ESN                         STRING(30)
Fault_Code1                 STRING(30)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Postcode_Collection         STRING(10)
Order_Number                STRING(30)
Passed                      STRING(3)
                         END
                     END                       

RETACCOUNTSLIST      FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name),PRE(retacc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(retacc:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(retacc:RefNumber,retacc:AccountNumber),DUP,NOCASE
DateOrderedKey           KEY(retacc:RefNumber,retacc:DateOrdered,retacc:AccountNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccountNumber               STRING(30)
CompanyName                 STRING(30)
DateOrdered                 DATE
QuantityOrdered             LONG
QuantityToShip              LONG
SaleNumber                  LONG
OrigRecordNumber            LONG
SaleType                    STRING(4)
                         END
                     END                       

PRIORITY             FILE,DRIVER('Btrieve'),NAME('PRIORITY.DAT'),PRE(pri),CREATE,BINDABLE,THREAD
Priority_Type_Key        KEY(pri:Priority_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Priority_Type               STRING(30)
Time                        REAL
Book_Before                 TIME
                         END
                     END                       

DEFEPS               FILE,DRIVER('Btrieve'),NAME('DEFEPS.DAT'),PRE(dee),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(dee:Record_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Record_Number               REAL
Account_Number              STRING(15)
Warranty_Charge_Type        STRING(30)
Location                    STRING(30)
Transit_Type                STRING(30)
Job_Priority                STRING(30)
Unit_Type                   STRING(30)
Status_Type                 STRING(30)
Delivery_Text               STRING(1000)
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(jobe:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(jobe:CompleteRepairDate,jobe:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
                         END
                     END                       

REPEXTTP             FILE,DRIVER('Btrieve'),OEM,NAME('REPEXTTP.DAT'),PRE(rpt),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rpt:RecordNumber),NOCASE,PRIMARY
ReportTypeKey            KEY(rpt:ReportType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportType                  STRING(80)
                         END
                     END                       

MANFAULT             FILE,DRIVER('Btrieve'),NAME('MANFAULT.DAT'),PRE(maf),CREATE,BINDABLE,THREAD
Field_Number_Key         KEY(maf:Manufacturer,maf:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
                         END
                     END                       

INVPARTS             FILE,DRIVER('Btrieve'),OEM,NAME('INVPARTS.DAT'),PRE(ivp),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(ivp:RecordNumber),NOCASE,PRIMARY
InvoiceNoKey             KEY(ivp:InvoiceNumber,ivp:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
InvoiceNumber               LONG
RetstockNumber              LONG
CreditQuantity              LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
JobNumber                   LONG
                         END
                     END                       

JOBSEARC             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(jobser),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobser:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobser:RefNumber),DUP,NOCASE
ConsignmentNoKey         KEY(jobser:ConsignmentNumber,jobser:RefNumber),DUP,NOCASE
AccountNoKey             KEY(jobser:AccountNumber),DUP,NOCASE
CourierKey               KEY(jobser:Courier),DUP,NOCASE
IMEIKey                  KEY(jobser:IMEI),DUP,NOCASE
JobTypeKey               KEY(jobser:JobType,jobser:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ConsignmentNumber           STRING(30)
JobType                     STRING(30)
IMEI                        STRING(30)
Courier                     STRING(30)
AccountNumber               STRING(30)
                         END
                     END                       

INVPATMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(ivptmp),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(ivptmp:RecordNumber),NOCASE,PRIMARY
InvoiceNoKey             KEY(ivptmp:InvoiceNumber,ivptmp:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
InvoiceNumber               LONG
RetstockNumber              LONG
CreditQuantity              LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
RetailCost                  REAL
JobNumber                   LONG
                         END
                     END                       

TRACHAR              FILE,DRIVER('Btrieve'),NAME('TRACHAR.DAT'),PRE(tch),CREATE,BINDABLE,THREAD
Account_Number_Key       KEY(tch:Account_Number,tch:Charge_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
                         END
                     END                       

CITYSERV             FILE,DRIVER('Btrieve'),OEM,NAME('CITYSERV.DAT'),PRE(cit),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(cit:RefNumber),NOCASE,PRIMARY
ServiceKey               KEY(cit:Service),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Service                     STRING(1)
Description                 STRING(30)
                         END
                     END                       

PICKNOTE             FILE,DRIVER('Btrieve'),OEM,PRE(pnt),CREATE,BINDABLE,THREAD
keypicknote              KEY(pnt:PickNoteRef),NOCASE,PRIMARY
keyonlocation            KEY(pnt:Location),DUP,NOCASE
keyonjobno               KEY(pnt:JobReference),DUP,NOCASE
Record                   RECORD,PRE()
PickNoteRef                 LONG
JobReference                LONG
PickNoteNumber              LONG
EngineerCode                STRING(3)
Location                    STRING(30)
IsPrinted                   BYTE
PrintedBy                   STRING(3)
PrintedDate                 DATE
PrintedTime                 TIME
IsConfirmed                 BYTE
ConfirmedBy                 STRING(3)
ConfirmedDate               DATE
ConfirmedTime               TIME
ConfirmedNotes              STRING(255)
Usercode                    STRING(3)
                         END
                     END                       

DISCOUNT             FILE,DRIVER('Btrieve'),NAME('DISCOUNT.DAT'),PRE(dis),CREATE,BINDABLE,THREAD
Discount_Code_Key        KEY(dis:Discount_Code),NOCASE,PRIMARY
Record                   RECORD,PRE()
Discount_Code               STRING(2)
Discount_Rate               REAL
                         END
                     END                       

LOCVALUE             FILE,DRIVER('Btrieve'),OEM,NAME('LOCVALUE.DAT'),PRE(lov),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lov:RecordNumber),NOCASE,PRIMARY
DateKey                  KEY(lov:Location,lov:TheDate),DUP,NOCASE
DateOnly                 KEY(lov:TheDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
TheDate                     DATE
PurchaseTotal               REAL
SaleCostTotal               REAL
RetailCostTotal             REAL
QuantityTotal               LONG
                         END
                     END                       

STOCKTYP             FILE,DRIVER('Btrieve'),NAME('STOCKTYP.DAT'),PRE(stp),CREATE,BINDABLE,THREAD
Stock_Type_Key           KEY(stp:Stock_Type),NOCASE,PRIMARY
Use_Loan_Key             KEY(stp:Use_Loan,stp:Stock_Type),DUP,NOCASE
Use_Exchange_Key         KEY(stp:Use_Exchange,stp:Stock_Type),DUP,NOCASE
Record                   RECORD,PRE()
Use_Loan                    STRING(3)
Use_Exchange                STRING(3)
Stock_Type                  STRING(30)
Available                   BYTE
                         END
                     END                       

IEMAILS              FILE,DRIVER('Btrieve'),OEM,PRE(iem),CREATE,BINDABLE,THREAD
KeyEmailNo               KEY(iem:EmailNo),NOCASE,PRIMARY
KeyDescription           KEY(iem:Description),DUP,NOCASE
KeySubject               KEY(iem:Subject),DUP,NOCASE
KeyStatus                KEY(iem:Status),DUP,NOCASE
Record                   RECORD,PRE()
EmailNo                     LONG
Description                 STRING(255)
Subject                     STRING(255)
Status                      STRING(30)
BodyText                    STRING(10000)
ReturnAddress               STRING(50)
Dummy                       STRING(2)
                         END
                     END                       

COURIER              FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(cou),CREATE,BINDABLE,THREAD
Courier_Key              KEY(cou:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(cou:Courier_Type,cou:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
                         END
                     END                       

TRDPARTY             FILE,DRIVER('Btrieve'),NAME('TRDPARTY.DAT'),PRE(trd),CREATE,BINDABLE,THREAD
Company_Name_Key         KEY(trd:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(trd:Account_Number),NOCASE
Special_Instructions_Key KEY(trd:Special_Instructions),DUP,NOCASE
Courier_Only_Key         KEY(trd:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Account_Number              STRING(30)
Contact_Name                STRING(60)
Address_Group               GROUP
Postcode                      STRING(15)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
Telephone_Number              STRING(15)
Fax_Number                    STRING(15)
                            END
Turnaround_Time             REAL
Courier_Direct              STRING(3)
Courier                     STRING(30)
Print_Order                 STRING(15)
Special_Instructions        STRING(30)
Batch_Number                LONG
BatchLimit                  LONG
                         END
                     END                       

JOBSTMP              FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(jobpre),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(jobpre:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
TransitType                 STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
ModelNumber                 STRING(30)
Manufacturer                STRING(30)
UnitType                    STRING(30)
Colour                      STRING(30)
DOP                         DATE
MobileNumber                STRING(30)
Location                    STRING(30)
AccountNumber               STRING(30)
OrderNumber                 STRING(30)
ChargeableJob               STRING(3)
WarrantyJob                 STRING(3)
CChargeType                 STRING(30)
WChargeType                 STRING(30)
Intermittent                STRING(3)
InCourier                   STRING(30)
InConsignNo                 STRING(30)
InDate                      DATE
Title                       STRING(5)
Initial                     STRING(1)
Surname                     STRING(30)
CompanyName                 STRING(30)
Postcode                    STRING(15)
Address1                    STRING(30)
Address2                    STRING(30)
Address3                    STRING(30)
Telephone                   STRING(30)
Fax                         STRING(30)
CCompanyName                STRING(30)
CPostcode                   STRING(15)
CAddress1                   STRING(30)
CAddress2                   STRING(30)
CAddress3                   STRING(30)
CTelephone                  STRING(30)
DCompanyName                STRING(30)
DPostcode                   STRING(15)
DAddress1                   STRING(30)
DAddress2                   STRING(30)
DAddress3                   STRING(30)
DTelephone                  STRING(30)
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(255)
FaultCode11                 STRING(255)
FaultCode12                 STRING(255)
FaultText                   STRING(255)
EngineerText                STRING(255)
                         END
                     END                       

LOGALLOC             FILE,DRIVER('Btrieve'),OEM,NAME('LOGALLOC.DAT'),PRE(LOG2),CREATE,BINDABLE,THREAD
AutoNumber_Key           KEY(LOG2:RefNumber),NOCASE,PRIMARY
Browse_Key               KEY(LOG2:Done_Flag,LOG2:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Date                        DATE
UserCode                    STRING(3)
ModelNumber                 STRING(30)
Location                    STRING(30)
SMPFNumber                  STRING(30)
DespatchNo                  LONG
SalesCode                   STRING(30)
Description                 STRING(30)
From                        STRING(30)
To_Site                     STRING(30)
Qty                         REAL
Done_Flag                   BYTE
                         END
                     END                       

JOBNOTES             FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(jbn:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
Fault_Description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
ColContatName               STRING(30)
ColDepartment               STRING(30)
DelContactName              STRING(30)
DelDepartment               STRING(30)
                         END
                     END                       

PROINV               FILE,DRIVER('Btrieve'),OEM,NAME('PROINV.DAT'),PRE(prv),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(prv:RefNumber),NOCASE,PRIMARY
AccountKey               KEY(prv:Invoiced,prv:AccountNumber,prv:BatchNumber),DUP,NOCASE
AccStatusKey             KEY(prv:Status,prv:AccountNumber,prv:BatchNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
AccountNumber               STRING(15)
AccountName                 STRING(30)
AccountType                 STRING(3)
BatchNumber                 LONG
DateCreated                 DATE
User                        STRING(3)
BatchValue                  REAL
NoOfJobs                    LONG
Status                      STRING(3)
Invoiced                    STRING(3)
                         END
                     END                       

EXPBUS               FILE,DRIVER('ASCII'),NAME(glo:file_name),PRE(exb),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Job_Date                    STRING(9)
Customer_Name               STRING(31)
Address_Line1               STRING(31)
Address_Line2               STRING(31)
Town                        STRING(31)
Postcode                    STRING(31)
Contact_Name                STRING(31)
Telephone_Number            STRING(21)
Business_Service            STRING(5)
Ref_Number                  STRING(21)
Alt_Ref                     STRING(21)
Items                       STRING(11)
Weight                      STRING(11)
Spec_1                      STRING(51)
Spec_2                      STRING(51)
Comp_No                     STRING(2)
                         END
                     END                       

IMPCITY              FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(imc),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
ref_number                  STRING(30)
consignment_number          STRING(30)
date                        STRING(20)
                         END
                     END                       

JOBACTMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(jactmp),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jactmp:Record_Number),NOCASE,PRIMARY
Accessory_Key            KEY(jactmp:Accessory),NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Accessory                   STRING(30)
                         END
                     END                       

RETPARTSLIST         FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name2),PRE(retpar),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(retpar:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(retpar:Processed,retpar:PartNumber),DUP,NOCASE
LocPartNumberKey         KEY(retpar:Location,retpar:Processed,retpar:PartNumber),DUP,NOCASE
LocDescriptionKey        KEY(retpar:Location,retpar:Processed,retpar:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartRefNumber               LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Location                    STRING(30)
QuantityStock               LONG
QuantityBackOrder           LONG
QuantityOnOrder             LONG
Processed                   BYTE
                         END
                     END                       

TRANTYPE             FILE,DRIVER('Btrieve'),NAME('TRANTYPE.DAT'),PRE(trt),CREATE,BINDABLE,THREAD
Transit_Type_Key         KEY(trt:Transit_Type),NOCASE,PRIMARY
Record                   RECORD,PRE()
Transit_Type                STRING(30)
Courier_Collection_Report   STRING(3)
Collection_Address          STRING(3)
Delivery_Address            STRING(3)
Loan_Unit                   STRING(3)
Exchange_Unit               STRING(3)
Force_DOP                   STRING(3)
Force_Location              STRING(3)
Skip_Workshop               STRING(3)
HideLocation                BYTE
InternalLocation            STRING(30)
Workshop_Label              STRING(3)
Job_Card                    STRING(3)
JobReceipt                  STRING(3)
Initial_Status              STRING(30)
Initial_Priority            STRING(30)
ExchangeStatus              STRING(30)
LoanStatus                  STRING(30)
Location                    STRING(3)
ANCCollNote                 STRING(3)
InWorkshop                  STRING(3)
DummyField                  STRING(1)
                         END
                     END                       

DESBATCH             FILE,DRIVER('Btrieve'),NAME('DESBATCH.DAT'),PRE(dbt),CREATE,BINDABLE,THREAD
Batch_Number_Key         KEY(dbt:Batch_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Batch_Number                REAL
Date                        DATE
Time                        TIME
                         END
                     END                       

JOBVODAC             FILE,DRIVER('Btrieve'),NAME('JOBVODAC.DAT'),PRE(jva),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jva:Ref_Number,jva:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
                         END
                     END                       

ESNMODEL             FILE,DRIVER('Btrieve'),NAME('ESNMODEL.DAT'),PRE(esn),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(esn:Record_Number),NOCASE,PRIMARY
ESN_Key                  KEY(esn:ESN,esn:Model_Number),DUP,NOCASE
ESN_Only_Key             KEY(esn:ESN),DUP,NOCASE
Model_Number_Key         KEY(esn:Model_Number,esn:ESN),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
ESN                         STRING(6)
Model_Number                STRING(30)
                         END
                     END                       

JOBPAYMT             FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jpt:Record_Number),NOCASE,PRIMARY
All_Date_Key             KEY(jpt:Ref_Number,jpt:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
                         END
                     END                       

LOGASSSTTEMP         FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name4),PRE(logasttmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logasttmp:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(logasttmp:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
PartRefNumber               LONG
Location                    STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

STOCK                FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(sto),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(sto:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(sto:Sundry_Item),DUP,NOCASE
Description_Key          KEY(sto:Location,sto:Description),DUP,NOCASE
Description_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Description),DUP,NOCASE
Shelf_Location_Key       KEY(sto:Location,sto:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(sto:Location,sto:Accessory,sto:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Supplier_Key             KEY(sto:Location,sto:Supplier),DUP,NOCASE
Location_Key             KEY(sto:Location,sto:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(sto:Location,sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(sto:Location,sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(sto:Location,sto:Accessory,sto:Manufacturer,sto:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(sto:Location,sto:Part_Number,sto:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE
SecondLocKey             KEY(sto:Location,sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE
RequestedKey             KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
AllowDuplicate              BYTE
ExcludeFromEDI              BYTE
RF_Board                    BYTE
                         END
                     END                       

LOGGED               FILE,DRIVER('Btrieve'),NAME('LOGGED.DAT'),PRE(log),CREATE,BINDABLE,THREAD
Surname_Key              KEY(log:Surname,log:Date),DUP,NOCASE
record_number_key        KEY(log:record_number),NOCASE,PRIMARY
User_Code_Key            KEY(log:User_Code,log:Time),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
record_number               REAL
Surname                     STRING(30)
Forename                    STRING(30)
Date                        DATE
Time                        TIME
                         END
                     END                       

LOGRTHIS             FILE,DRIVER('Btrieve'),OEM,NAME('LOGRTHIS.DAT'),PRE(lsrh),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(lsrh:RefNumber),DUP,NOCASE
DateIMEIKey              KEY(lsrh:Date,lsrh:IMEI),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
IMEI                        STRING(30)
Quantity                    LONG
Date                        DATE
ModelNumber                 STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

LOGSTHIS             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTHIS.DAT'),PRE(logsth),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logsth:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logsth:RefNumber),DUP,NOCASE
DateKey                  KEY(logsth:RefNumber,logsth:Date),DUP,NOCASE
StatusDateKey            KEY(logsth:RefNumber,logsth:Status,logsth:Date),DUP,NOCASE
DateStaClubKey           KEY(logsth:Date,logsth:Status,logsth:ModelNumber,logsth:Location,logsth:SMPFNumber,logsth:DespatchNoteNo,logsth:ClubNokia),DUP,NOCASE
RefModelNoKey            KEY(logsth:RefNumber,logsth:ModelNumber),DUP,NOCASE
DespatchNoKey            KEY(logsth:DespatchNo),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Date                        DATE
UserCode                    STRING(3)
StockIn                     LONG
StockOut                    LONG
Status                      STRING(10)
ClubNokia                   STRING(30)
SMPFNumber                  STRING(30)
DespatchNoteNo              STRING(30)
ModelNumber                 STRING(30)
Location                    STRING(30)
OrderRefNo                  STRING(30)
DespatchNo                  LONG
Deletion_Reason             STRING(40)
DummyField                  STRING(6)
                         END
                     END                       

LOGASSST             FILE,DRIVER('Btrieve'),OEM,NAME('LOGASSST.DAT'),PRE(logast),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logast:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logast:RefNumber,logast:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PartRefNumber               LONG
Location                    STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

LOGSTOLC             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOLC.DAT'),PRE(logstl),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logstl:RefNumber),NOCASE,PRIMARY
LocationKey              KEY(logstl:Location),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Location                    STRING(30)
Mark_Returns_Available      BYTE
                         END
                     END                       

LOGTEMP              FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name2),PRE(logtmp),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logtmp:RefNumber),NOCASE,PRIMARY
IMEIKey                  KEY(logtmp:IMEI),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
IMEI                        STRING(30)
Marker                      BYTE
                         END
                     END                       

LOGSERST             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSERST.DAT'),PRE(logser),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logser:RecordNumber),NOCASE,PRIMARY
ESNKey                   KEY(logser:ESN),DUP,NOCASE
RefNumberKey             KEY(logser:RefNumber,logser:ESN),DUP,NOCASE
ESNStatusKey             KEY(logser:RefNumber,logser:Status,logser:ESN),DUP,NOCASE
NokiaStatusKey           KEY(logser:RefNumber,logser:Status,logser:ClubNokia,logser:ESN),DUP,NOCASE
AllocNo_Key              KEY(logser:AllocNo,logser:Status,logser:ESN),DUP,NOCASE
Status_Alloc_Key         KEY(logser:RefNumber,logser:ClubNokia,logser:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ESN                         STRING(30)
Status                      STRING(10)
ClubNokia                   STRING(30)
AllocNo                     LONG
DummyField                  STRING(4)
                         END
                     END                       

LOGDEFLT             FILE,DRIVER('Btrieve'),OEM,NAME('LOGDEFLT.DAT'),PRE(ldef),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ldef:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
                         END
                     END                       

LOGSTLOC             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTLOC.DAT'),PRE(lstl),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(lstl:RecordNumber),NOCASE,PRIMARY
RefLocationKey           KEY(lstl:RefNumber,lstl:Location),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Location                    STRING(30)
AvlQuantity                 LONG
DesQuantity                 LONG
allocquantity               LONG
DummyField                  STRING(1)
                         END
                     END                       

MULTIDEF             FILE,DRIVER('Btrieve'),OEM,NAME('MULTIDEF.DAT'),PRE(mul),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mul:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(mul:Description),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(255)
ModelNumber                 STRING(30)
UnitType                    STRING(30)
Accessories                 STRING(30)
OrderNumber                 STRING(30)
InternalLocation            STRING(30)
FaultDescription            STRING(255)
DOP                         DATE
ChaChargeType               STRING(30)
ChaRepairType               STRING(30)
WarChargeType               STRING(30)
WarRepairType               STRING(30)
                         END
                     END                       

TRAFAULO             FILE,DRIVER('Btrieve'),NAME('TRAFAULO.DAT'),PRE(tfo),CREATE,BINDABLE,THREAD
Field_Key                KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Field,tfo:Description),NOCASE,PRIMARY
DescriptionKey           KEY(tfo:AccountNumber,tfo:Field_Number,tfo:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

TRAFAULT             FILE,DRIVER('Btrieve'),NAME('TRAFAULT.DAT'),PRE(taf),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(taf:RecordNumber),NOCASE,PRIMARY
Field_Number_Key         KEY(taf:AccountNumber,taf:Field_Number),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
AccountNumber               STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
Compulsory_At_Booking       STRING(3)
ReplicateFault              STRING(3)
ReplicateInvoice            STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
                         END
                     END                       

EXMINLEV             FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name),PRE(exm),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(exm:RecordNumber),NOCASE,PRIMARY
ManufacturerKey          KEY(exm:Manufacturer,exm:ModelNumber),DUP,NOCASE
ModelNumberKey           KEY(exm:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
ModelNumber                 STRING(30)
MinimumLevel                LONG
Available                   LONG
                         END
                     END                       

LOGSTHII             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTHII.DAT'),PRE(logsti),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logsti:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logsti:RefNumber,logsti:IMEI),DUP,NOCASE
IMEIRefNoKey             KEY(logsti:IMEI,logsti:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
IMEI                        STRING(30)
DummyField                  STRING(2)
                         END
                     END                       

LOGCLSTE             FILE,DRIVER('Btrieve'),OEM,NAME('LOGCLSTE.DAT'),PRE(logclu),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logclu:RecordNumber),NOCASE,PRIMARY
ClubNokiaKey             KEY(logclu:ClubNokia),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ClubNokia                   STRING(30)
Postcode                    STRING(15)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

STATCRIT             FILE,DRIVER('Btrieve'),OEM,NAME('STATCRIT.DAT'),PRE(stac),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(stac:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(stac:Description),DUP,NOCASE
DescriptionOptionKey     KEY(stac:Description,stac:OptionType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
OptionType                  STRING(30)
FieldValue                  STRING(40)
                         END
                     END                       

NETWORKS             FILE,DRIVER('Btrieve'),OEM,NAME('NETWORKS.DAT'),PRE(net),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(net:RecordNumber),NOCASE,PRIMARY
NetworkKey               KEY(net:Network),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Network                     STRING(30)
                         END
                     END                       

LOGDESNO             FILE,DRIVER('Btrieve'),OEM,NAME('LOGDESNO.DAT'),PRE(ldes),CREATE,BINDABLE,THREAD
DespatchNoKey            KEY(ldes:DespatchNo),NOCASE,PRIMARY
Record                   RECORD,PRE()
DespatchNo                  LONG
Date                        DATE
Time                        TIME
                         END
                     END                       

CPNDPRTS             FILE,DRIVER('Btrieve'),OEM,NAME('CPNDPRTS.DAT'),PRE(tmppen),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tmppen:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(tmppen:UserID,tmppen:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
UserID                      STRING(30)
JobNumber                   LONG
DateBooked                  DATE
AccountNumber               STRING(30)
ModelNumber                 STRING(30)
PartNumber                  STRING(30)
Description                 STRING(30)
Quantity                    LONG
DateOrdered                 DATE
Location                    STRING(30)
Status                      STRING(30)
                         END
                     END                       

LOGSALCD             FILE,DRIVER('Btrieve'),OEM,NAME('LOGSALCD.DAT'),PRE(logsal),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logsal:RefNumber),NOCASE,PRIMARY
SalesCodeKey             KEY(logsal:SalesCode),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

DEFSTOCK             FILE,DRIVER('Btrieve'),NAME('DEFSTOCK.DAT'),PRE(dst),CREATE,BINDABLE,THREAD
record_number_key        KEY(dst:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Use_Site_Location           STRING(3)
record_number               REAL
Site_Location               STRING(30)
                         END
                     END                       

JOBLOHIS             FILE,DRIVER('Btrieve'),NAME('JOBLOHIS.DAT'),PRE(jlh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jlh:Ref_Number,jlh:Date,jlh:Time),DUP,NOCASE
record_number_key        KEY(jlh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

LOAN                 FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(loa),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(loa:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(loa:ESN),DUP,NOCASE
MSN_Only_Key             KEY(loa:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(loa:Stock_Type,loa:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(loa:Stock_Type,loa:ESN),DUP,NOCASE
MSN_Key                  KEY(loa:Stock_Type,loa:MSN),DUP,NOCASE
ESN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:ESN),DUP,NOCASE
MSN_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:MSN),DUP,NOCASE
Ref_Available_Key        KEY(loa:Available,loa:Stock_Type,loa:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(loa:Available,loa:Stock_Type,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(loa:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(loa:Model_Number,loa:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(loa:Available,loa:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(loa:Available,loa:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(loa:Available,loa:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(loa:Available,loa:Model_Number,loa:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

NOTESCON             FILE,DRIVER('Btrieve'),NAME('NOTESCON.DAT'),PRE(noc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(noc:RecordNumber),NOCASE,PRIMARY
Notes_Key                KEY(noc:Reference,noc:Notes),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

VATCODE              FILE,DRIVER('Btrieve'),NAME('VATCODE.DAT'),PRE(vat),CREATE,BINDABLE,THREAD
Vat_code_Key             KEY(vat:VAT_Code),NOCASE,PRIMARY
Record                   RECORD,PRE()
VAT_Code                    STRING(2)
VAT_Rate                    REAL
                         END
                     END                       

LOANHIST             FILE,DRIVER('Btrieve'),NAME('LOANHIST.DAT'),PRE(loh),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(loh:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(loh:Ref_Number,-loh:Date,-loh:Time),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

TURNARND             FILE,DRIVER('Btrieve'),NAME('TURNARND.DAT'),PRE(tur),CREATE,BINDABLE,THREAD
Turnaround_Time_Key      KEY(tur:Turnaround_Time),NOCASE,PRIMARY
Record                   RECORD,PRE()
Turnaround_Time             STRING(30)
Days                        REAL
Hours                       REAL
                         END
                     END                       

ORDJOBS              FILE,DRIVER('Btrieve'),OEM,NAME(glo:file_name),PRE(orjtmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(orjtmp:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(orjtmp:PartNumber,orjtmp:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
OrderNumber                 LONG
PartNumber                  STRING(30)
JobNumber                   LONG
RefNumber                   LONG
CharWarr                    STRING(1)
Quantity                    LONG
                         END
                     END                       

MERGETXT             FILE,DRIVER('Btrieve'),OEM,NAME('MERGETXT'),PRE(mrt),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(mrt:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Letter                      STRING(10000)
                         END
                     END                       

MERGELET             FILE,DRIVER('Btrieve'),NAME('MERGELET.DAT'),PRE(mrg),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(mrg:RefNumber),NOCASE,PRIMARY
LetterNameKey            KEY(mrg:LetterName),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
LetterName                  STRING(30)
UseStatus                   STRING(3)
Status                      STRING(30)
                         END
                     END                       

ORDSTOCK             FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_name2),PRE(orstmp),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(orstmp:RecordNumber),NOCASE,PRIMARY
LocationKey              KEY(orstmp:PartNumber,orstmp:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
OrderNumber                 LONG
PartNumber                  STRING(30)
Location                    STRING(30)
Quantity                    LONG
                         END
                     END                       

EXCHHIST             FILE,DRIVER('Btrieve'),NAME('EXCHHIST.DAT'),PRE(exh),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(exh:record_number),NOCASE,PRIMARY
Ref_Number_Key           KEY(exh:Ref_Number,-exh:Date,-exh:Time),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

ACCESSOR             FILE,DRIVER('Btrieve'),NAME('ACCESSOR.DAT'),PRE(acr),CREATE,BINDABLE,THREAD
Accesory_Key             KEY(acr:Model_Number,acr:Accessory),NOCASE,PRIMARY
Model_Number_Key         KEY(acr:Accessory,acr:Model_Number),NOCASE
AccessOnlyKey            KEY(acr:Accessory),DUP,NOCASE
Record                   RECORD,PRE()
Accessory                   STRING(30)
Model_Number                STRING(30)
                         END
                     END                       

SUBACCAD             FILE,DRIVER('Btrieve'),OEM,NAME('SUBACCAD.DAT'),PRE(sua),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sua:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(sua:RefNumber,sua:AccountNumber),DUP,NOCASE
CompanyNameKey           KEY(sua:RefNumber,sua:CompanyName),DUP,NOCASE
AccountNumberOnlyKey     KEY(sua:AccountNumber),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AccountNumber               STRING(15)
CompanyName                 STRING(30)
Postcode                    STRING(15)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(15)
FaxNumber                   STRING(15)
EmailAddress                STRING(255)
ContactName                 STRING(30)
                         END
                     END                       

NOTESDEL             FILE,DRIVER('Btrieve'),OEM,PRE(nod),CREATE,BINDABLE,THREAD
keyonnotesdel            KEY(nod:ReasonCode),NOCASE,PRIMARY
Record                   RECORD,PRE()
ReasonCode                  STRING(30)
ReasonText                  STRING(80)
                         END
                     END                       

PICKDET              FILE,DRIVER('Btrieve'),OEM,PRE(pdt),CREATE,BINDABLE,THREAD
keypickdet               KEY(pdt:PickDetailRef),NOCASE,PRIMARY
keyonpicknote            KEY(pdt:PickNoteRef),DUP,NOCASE
Record                   RECORD,PRE()
PickDetailRef               LONG
PickNoteRef                 LONG
PartRefNumber               LONG
Quantity                    LONG
IsChargeable                BYTE
RequestDate                 DATE
RequestTime                 TIME
EngineerCode                STRING(3)
IsInStock                   BYTE
IsPrinted                   BYTE
IsDelete                    BYTE
DeleteReason                STRING(255)
Usercode                    STRING(3)
StockPartRefNumber          LONG
PartNumber                  STRING(30)
                         END
                     END                       

WARPARTS             FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(wpr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(wpr:Ref_Number,wpr:Part_Number),DUP,NOCASE
RecordNumberKey          KEY(wpr:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(wpr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wpr:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(wpr:Ref_Number,wpr:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(wpr:Ref_Number,wpr:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wpr:Ref_Number,wpr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wpr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(wpr:Ref_Number,wpr:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(wpr:Supplier,wpr:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(wpr:Supplier,wpr:Date_Received),DUP,NOCASE
RequestedKey             KEY(wpr:Requested,wpr:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           REAL
Date_Received               DATE
Status_Date                 DATE
Main_Part                   STRING(3)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END                       

PARTS                FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(par:Ref_Number,par:Part_Number),DUP,NOCASE
recordnumberkey          KEY(par:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(par:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(par:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(par:Ref_Number,par:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(par:Ref_Number,par:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(par:Ref_Number,par:Order_Number),DUP,NOCASE
Supplier_Key             KEY(par:Supplier),DUP,NOCASE
Order_Part_Key           KEY(par:Ref_Number,par:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(par:Supplier,par:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(par:Supplier,par:Date_Received),DUP,NOCASE
RequestedKey             KEY(par:Requested,par:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           LONG
Date_Received               DATE
Status_Date                 DATE
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END                       

JOBACC               FILE,DRIVER('Btrieve'),NAME('JOBACC.DAT'),PRE(jac),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jac:Ref_Number,jac:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
                         END
                     END                       

USELEVEL             FILE,DRIVER('Btrieve'),NAME('USELEVEL.DAT'),PRE(lev),CREATE,BINDABLE,THREAD
User_Level_Key           KEY(lev:User_Level),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Level                  STRING(30)
                         END
                     END                       

ALLLEVEL             FILE,DRIVER('Btrieve'),NAME('ALLLEVEL.DAT'),PRE(all),CREATE,BINDABLE,THREAD
Access_Level_Key         KEY(all:Access_Level),NOCASE,PRIMARY
Record                   RECORD,PRE()
Access_Level                STRING(30)
Description                 STRING(500)
                         END
                     END                       

QAPARTSTEMP          FILE,DRIVER('Btrieve'),OEM,NAME(glo:FileName),PRE(qap),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(qap:RecordNumber),NOCASE,PRIMARY
PartNumberKey            KEY(qap:PartNumber),DUP,NOCASE
DescriptionKey           KEY(qap:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PartRecordNumber            LONG
PartType                    STRING(3)
                         END
                     END                       

ACCAREAS             FILE,DRIVER('Btrieve'),NAME('ACCAREAS.DAT'),PRE(acc),CREATE,BINDABLE,THREAD
Access_level_key         KEY(acc:User_Level,acc:Access_Area),NOCASE,PRIMARY
AccessOnlyKey            KEY(acc:Access_Area),DUP,NOCASE
Record                   RECORD,PRE()
Access_Area                 STRING(30)
User_Level                  STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(aud),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
DummyField                  STRING(2)
Notes                       STRING(255)
Type                        STRING(3)
                         END
                     END                       

USERS                FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(use),CREATE,BINDABLE,THREAD
User_Code_Key            KEY(use:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(use:User_Type,use:Active,use:Surname),DUP,NOCASE
Surname_Active_Key       KEY(use:Active,use:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(use:Active,use:User_Code),DUP,NOCASE
User_Type_Key            KEY(use:User_Type,use:Surname),DUP,NOCASE
surname_key              KEY(use:Surname),DUP,NOCASE
password_key             KEY(use:Password),NOCASE
Logged_In_Key            KEY(use:Logged_In,use:Surname),DUP,NOCASE
Team_Surname             KEY(use:Team,use:Surname),DUP,NOCASE
Active_Team_Surname      KEY(use:Team,use:Active,use:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
                         END
                     END                       

LOCSHELF             FILE,DRIVER('Btrieve'),NAME('LOCSHELF.DAT'),PRE(los),CREATE,BINDABLE,THREAD
Shelf_Location_Key       KEY(los:Site_Location,los:Shelf_Location),NOCASE,PRIMARY
Record                   RECORD,PRE()
Site_Location               STRING(30)
Shelf_Location              STRING(30)
                         END
                     END                       

DEFAULTS             FILE,DRIVER('Btrieve'),NAME('DEFAULTS.DAT'),PRE(def),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(def:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Name                   STRING(30)
record_number               REAL
Version_Number              STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Postcode                    STRING(15)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
VAT_Number                  STRING(30)
Use_Invoice_Address         STRING(3)
Use_For_Order               STRING(3)
Invoice_Company_Name        STRING(30)
Invoice_Address_Line1       STRING(30)
Invoice_Address_Line2       STRING(30)
Invoice_Address_Line3       STRING(30)
Invoice_Postcode            STRING(15)
Invoice_Telephone_Number    STRING(15)
Invoice_Fax_Number          STRING(15)
InvoiceEmailAddress         STRING(255)
Invoice_VAT_Number          STRING(30)
OrderCompanyName            STRING(30)
OrderAddressLine1           STRING(30)
OrderAddressLine2           STRING(30)
OrderAddressLine3           STRING(30)
OrderPostcode               STRING(30)
OrderTelephoneNumber        STRING(30)
OrderFaxNumber              STRING(30)
OrderEmailAddress           STRING(255)
Use_Postcode                STRING(3)
Postcode_Path               STRING(255)
PostcodeDll                 STRING(3)
Vat_Rate_Labour             STRING(2)
Vat_Rate_Parts              STRING(2)
License_Number              STRING(10)
Maximum_Users               REAL
Warranty_Period             REAL
Automatic_Replicate         STRING(3)
Automatic_Replicate_Field   STRING(15)
Estimate_If_Over            REAL
Start_Work_Hours            TIME
End_Work_Hours              TIME
Include_Saturday            STRING(3)
Include_Sunday              STRING(3)
Show_Mobile_Number          STRING(3)
Show_Loan_Exchange_Details  STRING(3)
Use_Credit_Limit            STRING(3)
Hide_Physical_Damage        STRING(3)
Hide_Insurance              STRING(3)
Hide_Authority_Number       STRING(3)
Hide_Advance_Payment        STRING(3)
HideColour                  STRING(3)
HideInCourier               STRING(3)
HideIntFault                STRING(3)
HideProduct                 STRING(3)
HideLocation                BYTE
Allow_Bouncer               STRING(3)
Job_Batch_Number            REAL
QA_Required                 STRING(3)
QA_Before_Complete          STRING(3)
QAPreliminary               STRING(3)
QAExchLoan                  STRING(3)
CompleteAtQA                BYTE
RapidQA                     STRING(3)
ValidateESN                 STRING(3)
ValidateDesp                STRING(3)
Force_Accessory_Check       STRING(3)
Force_Initial_Transit_Type  STRING(1)
Force_Mobile_Number         STRING(1)
Force_Model_Number          STRING(1)
Force_Unit_Type             STRING(1)
Force_Fault_Description     STRING(1)
Force_ESN                   STRING(1)
Force_MSN                   STRING(1)
Force_Job_Type              STRING(1)
Force_Engineer              STRING(1)
Force_Invoice_Text          STRING(1)
Force_Repair_Type           STRING(1)
Force_Authority_Number      STRING(1)
Force_Fault_Coding          STRING(1)
Force_Spares                STRING(1)
Force_Incoming_Courier      STRING(1)
Force_Outoing_Courier       STRING(1)
Force_DOP                   STRING(1)
Order_Number                STRING(1)
Customer_Name               STRING(1)
ForcePostcode               STRING(1)
ForceDelPostcode            STRING(1)
ForceCommonFault            STRING(1)
Remove_Backgrounds          STRING(3)
Use_Loan_Exchange_Label     STRING(3)
Use_Job_Label               STRING(3)
UseSmallLabel               BYTE
Job_Label                   STRING(255)
add_stock_label             STRING(3)
receive_stock_label         STRING(3)
QA_Failed_Label             STRING(3)
QA_Failed_Report            STRING(3)
QAPassLabel                 BYTE
ThirdPartyNote              STRING(3)
Vodafone_Import_Path        STRING(255)
Label_Printer_Type          STRING(30)
Browse_Option               STRING(3)
ANCCollectionNo             LONG
Use_Sage                    STRING(3)
Global_Nominal_Code         STRING(30)
Parts_Stock_Code            STRING(30)
Labour_Stock_Code           STRING(30)
Courier_Stock_Code          STRING(30)
Parts_Description           STRING(60)
Labour_Description          STRING(60)
Courier_Description         STRING(60)
Parts_Code                  STRING(30)
Labour_Code                 STRING(30)
Courier_Code                STRING(30)
User_Name_Sage              STRING(30)
Password_Sage               STRING(30)
Company_Number_Sage         STRING(30)
Path_Sage                   STRING(255)
BouncerTime                 LONG
ExportPath                  STRING(255)
PrintBarcode                BYTE
ChaChargeType               STRING(30)
WarChargeType               STRING(30)
RetBackOrders               BYTE
RemoveWorkshopDespatch      BYTE
SummaryOrders               BYTE
EuroRate                    REAL
IrishVatRate                REAL
EmailServerAddress          STRING(255)
EmailServerPort             LONG
Job_Label_Accessories       STRING(3)
ShowRepTypeCosts            BYTE
PickNoteNormal              BYTE
PickNoteMainStore           BYTE
PickNoteChangeStatus        BYTE
PickNoteJobStatus           STRING(30)
TeamPerformanceTicker       BYTE
TickerRefreshRate           SHORT
UseOriginalBookingTemplate  BYTE
                         END
                     END                       

REPTYCAT             FILE,DRIVER('Btrieve'),OEM,NAME('REPTYCAT.DAT'),PRE(repc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(repc:RecordNumber),NOCASE,PRIMARY
RepairTypeKey            KEY(repc:RepairType),DUP,NOCASE
CategoryKey              KEY(repc:Category,repc:RepairType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RepairType                  STRING(30)
Category                    STRING(30)
                         END
                     END                       

REPTYPETEMP          FILE,DRIVER('Btrieve'),OEM,NAME(glo:RepairTypes),PRE(rept),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rept:RecordNumber),NOCASE,PRIMARY
RepairTypeKey            KEY(rept:RepairType),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RepairType                  STRING(30)
Cost                        REAL
                         END
                     END                       

MANFPALO             FILE,DRIVER('Btrieve'),NAME('MANFPALO.DAT'),PRE(mfp),CREATE,BINDABLE,THREAD
Field_Key                KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Field),NOCASE,PRIMARY
DescriptionKey           KEY(mfp:Manufacturer,mfp:Field_Number,mfp:Description),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

JOBSTAGE             FILE,DRIVER('Btrieve'),NAME('JOBSTAGE.DAT'),PRE(jst),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jst:Ref_Number,-jst:Date),DUP,NOCASE
Job_Stage_Key            KEY(jst:Ref_Number,jst:Job_Stage),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Job_Stage                   STRING(30)
Date                        DATE
Time                        TIME
User                        STRING(3)
                         END
                     END                       

MANFAUPA             FILE,DRIVER('Btrieve'),NAME('MANFAUPA.DAT'),PRE(map),CREATE,BINDABLE,THREAD
Field_Number_Key         KEY(map:Manufacturer,map:Field_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field_Name                  STRING(30)
Field_Type                  STRING(30)
Lookup                      STRING(3)
Force_Lookup                STRING(3)
Compulsory                  STRING(3)
RestrictLength              BYTE
LengthFrom                  LONG
LengthTo                    LONG
ForceFormat                 BYTE
FieldFormat                 STRING(30)
DateType                    STRING(30)
                         END
                     END                       

EXPPARTS             FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(exppar),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 STRING(20)
Sale_Cost                     STRING(20)
                            END
Quantity                    STRING(20)
Exclude_From_Order          STRING(20)
Despatch_Note_Number        STRING(30)
Date_Ordered                STRING(20)
Order_Number                STRING(20)
Date_Received               STRING(20)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

REPEXTRP             FILE,DRIVER('Btrieve'),OEM,NAME('REPEXTRP.DAT'),PRE(rex),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rex:RecordNumber),NOCASE,PRIMARY
ReportNameKey            KEY(rex:ReportType,rex:ReportName),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ReportType                  STRING(80)
ReportName                  STRING(80)
EXEName                     STRING(30)
                         END
                     END                       

GENSHORT             FILE,DRIVER('Btrieve'),NAME('GENSHORT.DAT'),PRE(gens),CREATE,BINDABLE,THREAD
AutoNumber_Key           KEY(gens:Autonumber_Field),NOCASE,PRIMARY
Lock_Down_Key            KEY(gens:Stock_Ref_No,gens:Audit_No),DUP,NOCASE
Record                   RECORD,PRE()
Autonumber_Field            LONG
branch_id                   LONG
Audit_No                    LONG
Stock_Ref_No                LONG
Stock_Qty                   LONG
                         END
                     END                       

LOCINTER             FILE,DRIVER('Btrieve'),NAME('LOCINTER.DAT'),PRE(loi),CREATE,BINDABLE,THREAD
Location_Key             KEY(loi:Location),NOCASE,PRIMARY
Location_Available_Key   KEY(loi:Location_Available,loi:Location),DUP,NOCASE
Record                   RECORD,PRE()
Location                    STRING(30)
Location_Available          STRING(3)
Allocate_Spaces             STRING(3)
Total_Spaces                REAL
Current_Spaces              STRING(6)
                         END
                     END                       

STAHEAD              FILE,DRIVER('Btrieve'),NAME('STAHEAD.DAT'),PRE(sth),CREATE,BINDABLE,THREAD
Heading_Key              KEY(sth:Ref_Number,sth:Heading),NOCASE,PRIMARY
Ref_Number_Key           KEY(sth:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Heading                     STRING(30)
                         END
                     END                       

NOTESINV             FILE,DRIVER('Btrieve'),NAME('NOTESINV.DAT'),PRE(noi),CREATE,BINDABLE,THREAD
Notes_Key                KEY(noi:Reference,noi:Notes),NOCASE,PRIMARY
Record                   RECORD,PRE()
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

NOTESFAU             FILE,DRIVER('Btrieve'),NAME('NOTESFAU.DAT'),PRE(nof),CREATE,BINDABLE,THREAD
Notes_Key                KEY(nof:Manufacturer,nof:Reference,nof:Notes),NOCASE,PRIMARY
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Reference                   STRING(30)
Notes                       STRING(80)
                         END
                     END                       

MANFAULO             FILE,DRIVER('Btrieve'),NAME('MANFAULO.DAT'),PRE(mfo),CREATE,BINDABLE,THREAD
Field_Key                KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Field),NOCASE,PRIMARY
DescriptionKey           KEY(mfo:Manufacturer,mfo:Field_Number,mfo:Description),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Field_Number                REAL
Field                       STRING(30)
Description                 STRING(60)
                         END
                     END                       

STATREP              FILE,DRIVER('Btrieve'),OEM,NAME('STATREP.DAT'),PRE(star),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(star:RecordNumber),NOCASE,PRIMARY
DescriptionKey           KEY(star:Description),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Description                 STRING(60)
                         END
                     END                       

JOBEXHIS             FILE,DRIVER('Btrieve'),NAME('JOBEXHIS.DAT'),PRE(jxh),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(jxh:Ref_Number,jxh:Date,jxh:Time),DUP,NOCASE
record_number_key        KEY(jxh:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Loan_Unit_Number            REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Status                      STRING(60)
                         END
                     END                       

STARECIP             FILE,DRIVER('Btrieve'),OEM,NAME('STARECIP.DAT'),PRE(str),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(str:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(str:RefNumber,str:RecipientType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
                         END
                     END                       

RETSTOCK             FILE,DRIVER('Btrieve'),OEM,NAME('RETSTOCK.DAT'),PRE(res),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(res:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(res:Ref_Number,res:Part_Number),DUP,NOCASE
Despatched_Key           KEY(res:Ref_Number,res:Despatched,res:Despatch_Note_Number,res:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(res:Ref_Number,res:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(res:Ref_Number,res:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(res:Ref_Number,res:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(res:Ref_Number,res:Order_Number),DUP,NOCASE
DespatchedKey            KEY(res:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(res:Ref_Number,res:Despatched,res:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
                         END
                     END                       

RETPAY               FILE,DRIVER('Btrieve'),OEM,NAME('RETPAY.DAT'),PRE(rtp),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(rtp:Record_Number),NOCASE,PRIMARY
Date_Key                 KEY(rtp:Ref_Number,rtp:Date,rtp:Payment_Type),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
Authorisation_Code          STRING(30)
Card_Holder                 STRING(30)
                         END
                     END                       

RETDESNO             FILE,DRIVER('Btrieve'),OEM,NAME('RETDESNO.DAT'),PRE(rdn),CREATE,BINDABLE,THREAD
Despatch_Number_Key      KEY(rdn:Despatch_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Despatch_Number             LONG
Date                        DATE
Time                        TIME
Consignment_Number          STRING(30)
Courier                     STRING(30)
Sale_Number                 LONG
                         END
                     END                       

ORDPARTS             FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(orp),CREATE,BINDABLE,THREAD
Order_Number_Key         KEY(orp:Order_Number,orp:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(orp:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(orp:Part_Ref_Number,orp:Part_Number,orp:Description),DUP,NOCASE
Part_Number_Key          KEY(orp:Order_Number,orp:Part_Number,orp:Description),DUP,NOCASE
Description_Key          KEY(orp:Order_Number,orp:Description,orp:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(orp:All_Received,orp:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(orp:Account_Number,orp:Part_Type,orp:All_Received,orp:Part_Number),DUP,NOCASE
Allocated_Key            KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(orp:Part_Type,orp:All_Received,orp:Allocated_To_Sale,orp:Account_Number,orp:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(orp:Order_Number,orp:Part_Type,orp:All_Received,orp:Part_Number,orp:Description),DUP,NOCASE
PartRecordNumberKey      KEY(orp:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                     END                       

STOCKMIN             FILE,DRIVER('Btrieve'),OEM,NAME(glo:File_Name),PRE(smin),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(smin:RecordNumber),NOCASE,PRIMARY
LocationPartNoKey        KEY(smin:Location,smin:PartNumber),DUP,NOCASE
LocationDescriptionKey   KEY(smin:Location,smin:Description),DUP,NOCASE
LocationSuppPartKey      KEY(smin:Location,smin:Supplier,smin:PartNumber),DUP,NOCASE
LocationSuppDescKey      KEY(smin:Location,smin:Supplier,smin:Description),DUP,NOCASE
PartRefNumberKey         KEY(smin:PartRefNumber),DUP,NOCASE
PartNumberKey            KEY(smin:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
PartNumber                  STRING(30)
Description                 STRING(20)
Supplier                    STRING(30)
Usage                       LONG
MinLevel                    LONG
QuantityOnOrder             LONG
QuantityInStock             LONG
QuantityRequired            LONG
PartRefNumber               LONG
QuantityJobsPending         LONG
                         END
                     END                       

LOCATION             FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(loc),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(loc:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(loc:Location),NOCASE
Main_Store_Key           KEY(loc:Main_Store,loc:Location),DUP,NOCASE
ActiveLocationKey        KEY(loc:Active,loc:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(loc:Active,loc:Main_Store,loc:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
PickNoteEnable              BYTE
                         END
                     END                       

WPARTTMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name2),PRE(wartmp),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(wartmp:Ref_Number,wartmp:Part_Number),DUP,NOCASE
record_number_key        KEY(wartmp:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(wartmp:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wartmp:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(wartmp:Ref_Number,wartmp:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(wartmp:Ref_Number,wartmp:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wartmp:Ref_Number,wartmp:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wartmp:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Main_Part                   STRING(3)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

PARTSTMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(partmp),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(partmp:Ref_Number,partmp:Part_Number),DUP,NOCASE
record_number_key        KEY(partmp:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(partmp:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(partmp:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(partmp:Ref_Number,partmp:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(partmp:Ref_Number,partmp:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(partmp:Ref_Number,partmp:Order_Number),DUP,NOCASE
Supplier_Key             KEY(partmp:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

EXCHANGE             FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(xch:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(xch:ESN),DUP,NOCASE
MSN_Only_Key             KEY(xch:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(xch:Stock_Type,xch:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(xch:Stock_Type,xch:ESN),DUP,NOCASE
MSN_Key                  KEY(xch:Stock_Type,xch:MSN),DUP,NOCASE
ESN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:ESN),DUP,NOCASE
MSN_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:MSN),DUP,NOCASE
Ref_Available_Key        KEY(xch:Available,xch:Stock_Type,xch:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(xch:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(xch:Model_Number,xch:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(xch:Available,xch:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(xch:Available,xch:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(xch:Available,xch:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(xch:Available,xch:Model_Number,xch:Ref_Number),DUP,NOCASE
StockBookedKey           KEY(xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE
AvailBookedKey           KEY(xch:Available,xch:Stock_Type,xch:Model_Number,xch:Date_Booked,xch:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(xch:Date_Booked),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
Audit_Number                REAL
DummyField                  STRING(1)
                         END
                     END                       

STOESN               FILE,DRIVER('Btrieve'),OEM,NAME('STOESN.DAT'),PRE(ste),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(ste:Record_Number),NOCASE,PRIMARY
Sold_Key                 KEY(ste:Ref_Number,ste:Sold,ste:Serial_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Serial_Number               STRING(16)
Sold                        STRING(3)
                         END
                     END                       

LOANACC              FILE,DRIVER('Btrieve'),NAME('LOANACC.DAT'),PRE(lac),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(lac:Ref_Number,lac:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
                         END
                     END                       

SUPPLIER             FILE,DRIVER('Btrieve'),NAME('SUPPLIER.DAT'),PRE(sup),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sup:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(sup:Account_Number),DUP,NOCASE
Company_Name_Key         KEY(sup:Company_Name),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Company_Name                STRING(30)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
HistoryUsage                LONG
Factor                      REAL
Notes                       STRING(255)
                         END
                     END                       

EXCHACC              FILE,DRIVER('Btrieve'),NAME('EXCHACC.DAT'),PRE(xca),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(xca:Ref_Number,xca:Accessory),NOCASE,PRIMARY
Record                   RECORD,PRE()
Ref_Number                  REAL
Accessory                   STRING(30)
                         END
                     END                       

RECIPTYP             FILE,DRIVER('Btrieve'),OEM,NAME('RECIPTYP.DAT'),PRE(rec),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(rec:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(rec:RecipientType),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RecipientType               STRING(30)
                         END
                     END                       

SUPVALA              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALA.DAT'),PRE(suva),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(suva:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(suva:Supplier,suva:RunDate),DUP,NOCASE
DateOnly                 KEY(suva:RunDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
OldBackOrder                DATE
OldOutOrder                 DATE
                         END
                     END                       

ESTPARTS             FILE,DRIVER('Btrieve'),NAME('ESTPARTS.DAT'),PRE(epr),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(epr:Ref_Number,epr:Part_Number),DUP,NOCASE
record_number_key        KEY(epr:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(epr:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(epr:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(epr:Ref_Number,epr:Part_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(epr:Ref_Number,epr:Order_Number),DUP,NOCASE
Supplier_Key             KEY(epr:Supplier),DUP,NOCASE
Order_Part_Key           KEY(epr:Ref_Number,epr:Order_Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Date_Received               DATE
Order_Part_Number           REAL
Status_Date                 DATE
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

TRAEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('TRAEMAIL.DAT'),PRE(tre),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tre:RecordNumber),NOCASE,PRIMARY
RecipientKey             KEY(tre:RefNumber,tre:RecipientType),DUP,NOCASE
ContactNameKey           KEY(tre:RefNumber,tre:ContactName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
ContactName                 STRING(60)
EmailAddress                STRING(255)
                         END
                     END                       

SUPVALB              FILE,DRIVER('Btrieve'),OEM,NAME('SUPVALB.DAT'),PRE(suvb),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(suvb:RecordNumber),NOCASE,PRIMARY
RunDateKey               KEY(suvb:Supplier,suvb:RunDate),DUP,NOCASE
LocationKey              KEY(suvb:Supplier,suvb:RunDate,suvb:Location),DUP,NOCASE
DateOnly                 KEY(suvb:RunDate),DUP,NOCASE,OPT
Record                   RECORD,PRE()
RecordNumber                LONG
Supplier                    STRING(30)
RunDate                     DATE
Location                    STRING(30)
BackOrderValue              REAL
                         END
                     END                       

JOBS                 FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(job:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job:Model_Number,job:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job:Engineer,job:Completed,job:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job:Engineer,job:Workshop,job:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job:Surname),DUP,NOCASE
MobileNumberKey          KEY(job:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job:ESN),DUP,NOCASE
MSN_Key                  KEY(job:MSN),DUP,NOCASE
AccountNumberKey         KEY(job:Account_Number,job:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job:Account_Number,job:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job:Engineer,job:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job:Model_Number,job:Date_Completed),DUP,NOCASE
By_Status                KEY(job:Current_Status,job:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job:Current_Status,job:Location,job:Ref_Number),DUP,NOCASE
Location_Key             KEY(job:Location,job:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job:Third_Party_Site,job:Third_Party_Printed,job:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job:Third_Party_Site,job:Third_Party_Printed,job:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job:Job_Priority,job:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job:Manufacturer,job:EDI,job:EDI_Batch_Number,job:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job:Batch_Number,job:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job:Batch_Number,job:Current_Status,job:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job:Batch_Number,job:Model_Number,job:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job:Batch_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job:Batch_Number,job:Completed,job:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job:Chargeable_Job,job:Account_Number,job:Invoice_Number,job:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job:Invoice_Exception,job:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job:Despatched,job:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job:Despatched,job:Account_Number,job:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job:Despatched,job:Current_Courier,job:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job:Despatched,job:Account_Number,job:Current_Courier,job:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job:Despatch_Number,job:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job:Courier,job:Date_Despatched,job:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job:Loan_Courier,job:Loan_Despatched,job:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job:Exchange_Courier,job:Exchange_Despatched,job:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job:Bouncer,job:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job:Engineer,job:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job:Exchange_Status,job:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job:Loan_Status,job:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job:Exchange_Status,job:Location,job:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job:Loan_Status,job:Location,job:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job:InvoiceAccount,job:InvoiceBatch,job:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job:InvoiceAccount,job:InvoiceBatch,job:InvoiceStatus,job:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

MODELNUM             FILE,DRIVER('Btrieve'),NAME('MODELNUM.DAT'),PRE(mod),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(mod:Model_Number),NOCASE,PRIMARY
Manufacturer_Key         KEY(mod:Manufacturer,mod:Model_Number),DUP,NOCASE
Manufacturer_Unit_Type_Key KEY(mod:Manufacturer,mod:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Model_Number                STRING(30)
Manufacturer                STRING(30)
Specify_Unit_Type           STRING(3)
Product_Type                STRING(30)
Unit_Type                   STRING(30)
Nokia_Unit_Type             STRING(30)
ESN_Length_From             REAL
ESN_Length_To               REAL
MSN_Length_From             REAL
MSN_Length_To               REAL
ExchangeUnitMinLevel        LONG
IsVodafoneSpecific          BYTE
IsCCentreSpecific           BYTE
                         END
                     END                       

SUBEMAIL             FILE,DRIVER('Btrieve'),OEM,NAME('SUBEMAIL.DAT'),PRE(sue),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sue:RecordNumber),NOCASE,PRIMARY
RecipientTypeKey         KEY(sue:RefNumber,sue:RecipientType),DUP,NOCASE
ContactNameKey           KEY(sue:RefNumber,sue:ContactName),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
RecipientType               STRING(30)
ContactName                 STRING(60)
EmailAddress                STRING(255)
                         END
                     END                       

SUPPTEMP             FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(suptmp),CREATE,BINDABLE,THREAD
Company_Name_Key         KEY(suptmp:Company_Name),NOCASE,PRIMARY
Account_Number_Key       KEY(suptmp:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Address_Group               GROUP
Postcode                      STRING(10)
Address_Line1                 STRING(30)
Address_Line2                 STRING(30)
Address_Line3                 STRING(30)
                            END
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Account_Number              STRING(15)
Order_Period                REAL
Normal_Supply_Period        REAL
Minimum_Order_Value         REAL
Notes                       STRING(1000)
                         END
                     END                       

ORDERS               FILE,DRIVER('Btrieve'),NAME('ORDERS.DAT'),PRE(ord),CREATE,BINDABLE,THREAD
Order_Number_Key         KEY(ord:Order_Number),NOCASE,PRIMARY
Printed_Key              KEY(ord:Printed,ord:Order_Number),DUP,NOCASE
Supplier_Printed_Key     KEY(ord:Printed,ord:Supplier,ord:Order_Number),DUP,NOCASE
Received_Key             KEY(ord:All_Received,ord:Order_Number),DUP,NOCASE
Supplier_Key             KEY(ord:Supplier,ord:Order_Number),DUP,NOCASE
Supplier_Received_Key    KEY(ord:Supplier,ord:All_Received,ord:Order_Number),DUP,NOCASE
DateKey                  KEY(ord:Date),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                REAL
Supplier                    STRING(30)
Date                        DATE
Printed                     STRING(3)
All_Received                STRING(3)
User                        STRING(3)
Dummy_Field                 STRING(1)
                         END
                     END                       

UNITTYPE             FILE,DRIVER('Btrieve'),NAME('UNITTYPE.DAT'),PRE(uni),CREATE,BINDABLE,THREAD
Unit_Type_Key            KEY(uni:Unit_Type),NOCASE,PRIMARY
ActiveKey                KEY(uni:Active,uni:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Unit_Type                   STRING(30)
Active                      BYTE
                         END
                     END                       

STATUS               FILE,DRIVER('Btrieve'),NAME('STATUS.DAT'),PRE(sts),CREATE,BINDABLE,THREAD
Ref_Number_Only_Key      KEY(sts:Ref_Number),NOCASE,PRIMARY
Status_Key               KEY(sts:Status),DUP,NOCASE
Heading_Key              KEY(sts:Heading_Ref_Number,sts:Status),NOCASE
Ref_Number_Key           KEY(sts:Heading_Ref_Number,sts:Ref_Number),NOCASE
LoanKey                  KEY(sts:Loan,sts:Status),DUP,NOCASE
ExchangeKey              KEY(sts:Exchange,sts:Status),DUP,NOCASE
JobKey                   KEY(sts:Job,sts:Status),DUP,NOCASE
TurnJobKey               KEY(sts:Use_Turnaround_Time,sts:Job,sts:Status),DUP,NOCASE
Record                   RECORD,PRE()
Heading_Ref_Number          REAL
Status                      STRING(30)
Ref_Number                  REAL
Use_Turnaround_Time         STRING(3)
Use_Turnaround_Days         STRING(3)
Turnaround_Days             LONG
Use_Turnaround_Hours        STRING(3)
Turnaround_Hours            LONG
Notes                       STRING(1000)
Loan                        STRING(3)
Exchange                    STRING(3)
Job                         STRING(3)
SystemStatus                STRING(3)
EngineerStatus              BYTE
EnableEmail                 BYTE
SenderEmailAddress          STRING(255)
EmailSubject                STRING(255)
EmailBody                   STRING(500)
PickNoteEnable              BYTE
                         END
                     END                       

REPAIRTY             FILE,DRIVER('Btrieve'),NAME('REPAIRTY.DAT'),PRE(rep),CREATE,BINDABLE,THREAD
Repair_Type_Key          KEY(rep:Manufacturer,rep:Model_Number,rep:Repair_Type),NOCASE,PRIMARY
Manufacturer_Key         KEY(rep:Manufacturer,rep:Repair_Type),DUP,NOCASE
Model_Number_Key         KEY(rep:Model_Number,rep:Repair_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(rep:Repair_Type),DUP,NOCASE
Model_Chargeable_Key     KEY(rep:Model_Number,rep:Chargeable,rep:Repair_Type),DUP,NOCASE
Model_Warranty_Key       KEY(rep:Model_Number,rep:Warranty,rep:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Chargeable                  STRING(3)
Warranty                    STRING(3)
CompFaultCoding             BYTE
ExcludeFromEDI              BYTE
ExcludeFromInvoicing        BYTE
                         END
                     END                       

TRADETMP             FILE,DRIVER('Btrieve'),OEM,NAME(glo:tradetmp),PRE(tratmp),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(tratmp:RefNumber),NOCASE,PRIMARY
AccountNoKey             KEY(tratmp:Account_Number),DUP,NOCASE
CompanyNameKey           KEY(tratmp:Company_Name),DUP,NOCASE
TypeKey                  KEY(tratmp:Type,tratmp:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Account_Number              STRING(15)
Company_Name                STRING(30)
Type                        STRING(3)
                         END
                     END                       

EXCCHRGE             FILE,DRIVER('Btrieve'),NAME('EXCCHRGE.DAT'),PRE(exc),CREATE,BINDABLE,THREAD
Manufacturer_Key         KEY(exc:Manufacturer,exc:Charge_Type,exc:Model_Number,exc:Unit_Type,exc:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Manufacturer                STRING(30)
Charge_Type                 STRING(30)
Model_Number                STRING(30)
Unit_Type                   STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
                         END
                     END                       

INVOICE              FILE,DRIVER('Btrieve'),NAME('INVOICE.DAT'),PRE(inv),CREATE,BINDABLE,THREAD
Invoice_Number_Key       KEY(inv:Invoice_Number),NOCASE,PRIMARY
Invoice_Type_Key         KEY(inv:Invoice_Type,inv:Invoice_Number),DUP,NOCASE
Account_Number_Type_Key  KEY(inv:Invoice_Type,inv:Account_Number,inv:Invoice_Number),DUP,NOCASE
Account_Number_Key       KEY(inv:Account_Number,inv:Invoice_Number),DUP,NOCASE
Date_Created_Key         KEY(inv:Date_Created),DUP,NOCASE
Batch_Number_Key         KEY(inv:Manufacturer,inv:Batch_Number),DUP,NOCASE
Record                   RECORD,PRE()
Invoice_Number              REAL
Invoice_Type                STRING(3)
Job_Number                  REAL
Date_Created                DATE
Account_Number              STRING(15)
AccountType                 STRING(3)
Total                       REAL
Vat_Rate_Labour             REAL
Vat_Rate_Parts              REAL
Vat_Rate_Retail             REAL
VAT_Number                  STRING(30)
Invoice_VAT_Number          STRING(30)
Currency                    STRING(30)
Batch_Number                REAL
Manufacturer                STRING(30)
Claim_Reference             STRING(30)
Total_Claimed               REAL
Courier_Paid                REAL
Labour_Paid                 REAL
Parts_Paid                  REAL
Reconciled_Date             DATE
jobs_count                  REAL
PrevInvoiceNo               LONG
InvoiceCredit               STRING(3)
UseAlternativeAddress       BYTE
EuroExhangeRate             REAL
                         END
                     END                       

TRDBATCH             FILE,DRIVER('Btrieve'),OEM,NAME('TRDBATCH.DAT'),PRE(trb),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(trb:RecordNumber),NOCASE,PRIMARY
Batch_Number_Key         KEY(trb:Company_Name,trb:Batch_Number),DUP,NOCASE
Batch_Number_Status_Key  KEY(trb:Status,trb:Batch_Number),DUP,NOCASE
BatchOnlyKey             KEY(trb:Batch_Number),DUP,NOCASE
Batch_Company_Status_Key KEY(trb:Status,trb:Company_Name,trb:Batch_Number),DUP,NOCASE
ESN_Only_Key             KEY(trb:ESN),DUP,NOCASE
ESN_Status_Key           KEY(trb:Status,trb:ESN),DUP,NOCASE
Company_Batch_ESN_Key    KEY(trb:Company_Name,trb:Batch_Number,trb:ESN),DUP,NOCASE
AuthorisationKey         KEY(trb:AuthorisationNo),DUP,NOCASE
StatusAuthKey            KEY(trb:Status,trb:AuthorisationNo),DUP,NOCASE
CompanyDateKey           KEY(trb:Company_Name,trb:Status,trb:DateReturn),DUP,NOCASE
JobStatusKey             KEY(trb:Status,trb:Ref_Number),DUP,NOCASE
JobNumberKey             KEY(trb:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Batch_Number                LONG
Company_Name                STRING(30)
Ref_Number                  LONG
ESN                         STRING(20)
Status                      STRING(3)
Date                        DATE
Time                        TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
                         END
                     END                       

TRACHRGE             FILE,DRIVER('Btrieve'),NAME('TRACHRGE.DAT'),PRE(trc),CREATE,BINDABLE,THREAD
Account_Charge_Key       KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type,trc:Unit_Type,trc:Repair_Type),NOCASE,PRIMARY
Model_Repair_Key         KEY(trc:Model_Number,trc:Repair_Type),DUP,NOCASE
Charge_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(trc:Account_Number,trc:Model_Number,trc:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(trc:Account_Number,trc:Model_Number,trc:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(trc:Account_Number,trc:Model_Number,trc:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(trc:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(trc:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(trc:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
                         END
                     END                       

TRDMODEL             FILE,DRIVER('Btrieve'),NAME('TRDMODEL.DAT'),PRE(trm),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(trm:Company_Name,trm:Model_Number),NOCASE,PRIMARY
Model_Number_Only_Key    KEY(trm:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Company_Name                STRING(30)
Model_Number                STRING(30)
                         END
                     END                       

STOMODEL             FILE,DRIVER('Btrieve'),NAME('STOMODEL.DAT'),PRE(stm),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(stm:Ref_Number,stm:Manufacturer,stm:Model_Number),NOCASE,PRIMARY
Model_Part_Number_Key    KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE
Description_Key          KEY(stm:Accessory,stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE
Manufacturer_Key         KEY(stm:Manufacturer,stm:Model_Number),DUP,NOCASE
Ref_Part_Description     KEY(stm:Ref_Number,stm:Part_Number,stm:Location,stm:Description),DUP,NOCASE
Location_Part_Number_Key KEY(stm:Model_Number,stm:Location,stm:Part_Number),DUP,NOCASE
Location_Description_Key KEY(stm:Model_Number,stm:Location,stm:Description),DUP,NOCASE
Mode_Number_Only_Key     KEY(stm:Ref_Number,stm:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Manufacturer                STRING(30)
Model_Number                STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Location                    STRING(30)
Accessory                   STRING(3)
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(30)
FaultCode11                 STRING(30)
FaultCode12                 STRING(30)
                         END
                     END                       

TRADEACC             FILE,DRIVER('Btrieve'),NAME('TRADEACC.DAT'),PRE(tra),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(tra:RecordNumber),NOCASE,PRIMARY
Account_Number_Key       KEY(tra:Account_Number),NOCASE
Company_Name_Key         KEY(tra:Company_Name),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Contact_Name                STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Retail_VAT_Code             STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Use_Sub_Accounts            STRING(3)
Invoice_Sub_Accounts        STRING(3)
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Price_Despatch              STRING(3)
Price_Retail_Despatch       STRING(3)
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Standard_Repair_Type        STRING(15)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
Use_Contact_Name            STRING(3)
VAT_Number                  STRING(30)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Allow_Loan                  STRING(3)
Allow_Exchange              STRING(3)
Force_Fault_Codes           STRING(3)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Skip_Despatch               STRING(3)
IgnoreDespatch              STRING(3)
Summary_Despatch_Notes      STRING(3)
Exchange_Stock_Type         STRING(30)
Loan_Stock_Type             STRING(30)
Courier_Cost                REAL
Force_Estimate              STRING(3)
Estimate_If_Over            REAL
Turnaround_Time             STRING(30)
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
RefurbCharge                STRING(3)
ChargeType                  STRING(30)
WarChargeType               STRING(30)
ExchangeAcc                 STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
ExcludeBouncer              BYTE
RetailZeroParts             BYTE
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
ShowRepairTypes             BYTE
WebMemo                     STRING(255)
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
StopThirdParty              BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
MakeSplitJob                BYTE
SplitJobStatus              STRING(30)
SplitExchangeStatus         STRING(30)
SplitCChargeType            STRING(30)
SplitWChargeType            STRING(30)
CheckChargeablePartsCost    BYTE
MaxChargeablePartsCost      REAL
UseAlternateDespatchNote    BYTE
                         END
                     END                       

QUERYREA             FILE,DRIVER('Btrieve'),OEM,NAME('QUERYREA.DAT'),PRE(que),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(que:RefNumber),NOCASE,PRIMARY
QueryKey                 KEY(que:Query),NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
Query                       STRING(60)
                         END
                     END                       

SUBCHRGE             FILE,DRIVER('Btrieve'),NAME('SUBCHRGE.DAT'),PRE(suc),CREATE,BINDABLE,THREAD
Model_Repair_Type_Key    KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type,suc:Unit_Type,suc:Repair_Type),NOCASE,PRIMARY
Account_Charge_Key       KEY(suc:Account_Number,suc:Model_Number,suc:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(suc:Account_Number,suc:Model_Number,suc:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(suc:Account_Number,suc:Model_Number,suc:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(suc:Account_Number,suc:Model_Number,suc:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(suc:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(suc:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(suc:Unit_Type),DUP,NOCASE
Model_Repair_Key         KEY(suc:Model_Number,suc:Repair_Type),DUP,NOCASE
Record                   RECORD,PRE()
Account_Number              STRING(15)
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
                         END
                     END                       

DEFCHRGE             FILE,DRIVER('Btrieve'),NAME('DEFCHRGE.DAT'),PRE(dec),CREATE,BINDABLE,THREAD
Charge_Type_Key          KEY(dec:Charge_Type),NOCASE,PRIMARY
Repair_Type_Only_Key     KEY(dec:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(dec:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Unit_Type                   STRING(30)
Model_Number                STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
                         END
                     END                       

SUBTRACC             FILE,DRIVER('Btrieve'),NAME('SUBTRACC.DAT'),PRE(sub),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(sub:RecordNumber),NOCASE,PRIMARY
Main_Account_Key         KEY(sub:Main_Account_Number,sub:Account_Number),NOCASE
Main_Name_Key            KEY(sub:Main_Account_Number,sub:Company_Name),DUP,NOCASE
Account_Number_Key       KEY(sub:Account_Number),NOCASE
Branch_Key               KEY(sub:Branch),DUP,NOCASE
Main_Branch_Key          KEY(sub:Main_Account_Number,sub:Branch),DUP,NOCASE
Company_Name_Key         KEY(sub:Company_Name),DUP,NOCASE
Contact_Centre_Key       KEY(sub:IsContactCentre,sub:Account_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Main_Account_Number         STRING(15)
Account_Number              STRING(15)
Postcode                    STRING(15)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Branch                      STRING(30)
Contact_Name                STRING(30)
Enquiry_Source              STRING(30)
Labour_Discount_Code        STRING(2)
Retail_Discount_Code        STRING(2)
Parts_Discount_Code         STRING(2)
Labour_VAT_Code             STRING(2)
Retail_VAT_Code             STRING(2)
Parts_VAT_Code              STRING(2)
Account_Type                STRING(6)
Credit_Limit                REAL
Account_Balance             REAL
Stop_Account                STRING(3)
Allow_Cash_Sales            STRING(3)
Use_Delivery_Address        STRING(3)
Use_Collection_Address      STRING(3)
UseCustDespAdd              STRING(3)
Courier_Incoming            STRING(30)
Courier_Outgoing            STRING(30)
VAT_Number                  STRING(30)
Despatch_Invoiced_Jobs      STRING(3)
Despatch_Paid_Jobs          STRING(3)
Print_Despatch_Notes        STRING(3)
PriceDespatchNotes          BYTE
Print_Despatch_Complete     STRING(3)
Print_Despatch_Despatch     STRING(3)
Print_Retail_Despatch_Note  STRING(3)
Print_Retail_Picking_Note   STRING(3)
Despatch_Note_Per_Item      STRING(3)
Summary_Despatch_Notes      STRING(3)
Courier_Cost                REAL
Password                    STRING(20)
Retail_Payment_Type         STRING(3)
Retail_Price_Structure      STRING(3)
Use_Customer_Address        STRING(3)
Invoice_Customer_Address    STRING(3)
ZeroChargeable              STRING(3)
MultiInvoice                STRING(3)
EDIInvoice                  STRING(3)
ExportPath                  STRING(255)
ImportPath                  STRING(255)
BatchNumber                 LONG
HideDespAdd                 BYTE
EuroApplies                 BYTE
ForceCommonFault            BYTE
ForceOrderNumber            BYTE
WebPassword1                STRING(30)
WebPassword2                STRING(30)
InvoiceAtDespatch           BYTE
InvoiceType                 BYTE
IndividualSummary           BYTE
UseTradeContactNo           BYTE
E1                          BYTE
E2                          BYTE
E3                          BYTE
UseDespatchDetails          BYTE
AltTelephoneNumber          STRING(30)
AltFaxNumber                STRING(30)
AltEmailAddress             STRING(255)
UseAlternativeAdd           BYTE
AutoSendStatusEmails        BYTE
EmailRecipientList          BYTE
EmailEndUser                BYTE
IsContactCentre             BYTE
                         END
                     END                       

CHARTYPE             FILE,DRIVER('Btrieve'),NAME('CHARTYPE.DAT'),PRE(cha),CREATE,BINDABLE,THREAD
Charge_Type_Key          KEY(cha:Charge_Type),NOCASE,PRIMARY
Ref_Number_Key           KEY(cha:Ref_Number,cha:Charge_Type),DUP,NOCASE
Physical_Damage_Key      KEY(cha:Allow_Physical_Damage,cha:Charge_Type),DUP,NOCASE
Warranty_Key             KEY(cha:Warranty,cha:Charge_Type),DUP,NOCASE
Warranty_Ref_Number_Key  KEY(cha:Warranty,cha:Ref_Number,cha:Charge_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Force_Warranty              STRING(3)
Allow_Physical_Damage       STRING(3)
Allow_Estimate              STRING(3)
Force_Estimate              STRING(3)
Invoice_Customer            STRING(3)
Invoice_Trade_Customer      STRING(3)
Invoice_Manufacturer        STRING(3)
No_Charge                   STRING(3)
Zero_Parts                  STRING(3)
Warranty                    STRING(3)
Ref_Number                  REAL
Exclude_EDI                 STRING(3)
ExcludeInvoice              BYTE
                         END
                     END                       

MANUFACT             FILE,DRIVER('Btrieve'),NAME('MANUFACT.DAT'),PRE(man),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(man:RecordNumber),NOCASE,PRIMARY
Manufacturer_Key         KEY(man:Manufacturer),NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Manufacturer                STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(15)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
EmailAddress                STRING(255)
Use_MSN                     STRING(3)
Contact_Name1               STRING(60)
Contact_Name2               STRING(60)
Head_Office_Telephone       STRING(15)
Head_Office_Fax             STRING(15)
Technical_Support_Telephone STRING(15)
Technical_Support_Fax       STRING(15)
Technical_Support_Hours     STRING(30)
Batch_Number                REAL
EDI_Account_Number          STRING(30)
EDI_Path                    STRING(255)
Trade_Account               STRING(30)
Supplier                    STRING(30)
Warranty_Period             REAL
SamsungCount                LONG
IncludeAdjustment           STRING(3)
AdjustPart                  BYTE
ForceParts                  BYTE
NokiaType                   STRING(20)
RemAccCosts                 BYTE
SiemensNewEDI               BYTE
DOPCompulsory               BYTE
Notes                       STRING(255)
UseQA                       BYTE
UseElectronicQA             BYTE
QALoanExchange              BYTE
QAAtCompletion              BYTE
SiemensNumber               LONG
SiemensDate                 DATE
UseProductCode              BYTE
QAParts                     BYTE
QANetwork                   BYTE
DisallowNA                  BYTE
IsVodafoneSpecific          BYTE
MSNFieldMask                STRING(30)
IsCCentreSpecific           BYTE
EDIFileType                 STRING(30)
                         END
                     END                       

USMASSIG             FILE,DRIVER('Btrieve'),NAME('USMASSIG.DAT'),PRE(usm),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(usm:User_Code,usm:Model_Number),NOCASE,PRIMARY
Record                   RECORD,PRE()
User_Code                   STRING(3)
Model_Number                STRING(30)
                         END
                     END                       

EXPWARPARTS          FILE,DRIVER('BASIC'),NAME(glo:file_name),PRE(expwar),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
Ref_Number                  STRING(20)
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 STRING(20)
Sale_Cost                     STRING(20)
                            END
Quantity                    STRING(20)
Exclude_From_Order          STRING(20)
Despatch_Note_Number        STRING(30)
Date_Ordered                STRING(20)
Order_Number                STRING(20)
Date_Received               STRING(20)
UnitType                    STRING(30)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
FirstJob                    STRING(1)
                         END
                     END                       

USUASSIG             FILE,DRIVER('Btrieve'),NAME('USUASSIG.DAT'),PRE(usu),CREATE,BINDABLE,THREAD
Unit_Type_Key            KEY(usu:User_Code,usu:Unit_Type),NOCASE,PRIMARY
Unit_Type_Only_Key       KEY(usu:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Unit_Type                   STRING(30)
                         END
                     END                       

XMLTRADE             FILE,DRIVER('Btrieve'),OEM,NAME('XMLTRADE.DAT'),PRE(XTR),CREATE,BINDABLE,THREAD
AccountNumberKey         KEY(XTR:AccountNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
AccountNumber               STRING(15)
                         END
                     END                       

XMLJOBQ              FILE,DRIVER('Btrieve'),OEM,NAME('XMLJOBQ'),PRE(XJQ),CREATE,BINDABLE,THREAD
JobNumberKey             KEY(XJQ:JobNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
JobNumber                   LONG
                         END
                     END                       

XMLDEFS              FILE,DRIVER('Btrieve'),OEM,NAME('XMLDEFS.DAT'),PRE(XDF),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(XDF:RecordNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RecordNumber                LONG
UserCode                    STRING(3)
AccountNumber               STRING(15)
ChargeType                  STRING(30)
WarrantyChargeType          STRING(30)
TransitType                 STRING(30)
JobTurnaroundTime           STRING(30)
UnitType                    STRING(30)
CollectionStatus            STRING(30)
DropOffStatus               STRING(30)
DeliveryText                STRING(255)
DropPointStatus             STRING(30)
DropPointLocation           STRING(30)
                         END
                     END                       

XMLSB                FILE,DRIVER('Btrieve'),OEM,NAME('XMLSB.DAT'),PRE(XSB),CREATE,BINDABLE,THREAD
SBStatusKey              KEY(XSB:SBStatus),NOCASE,PRIMARY
FKStatusCodeKey          KEY(XSB:FKStatusCode),DUP,NOCASE
Record                   RECORD,PRE()
SBStatus                    STRING(30)
FKStatusCode                STRING(4)
                         END
                     END                       

XMLSAM               FILE,DRIVER('Btrieve'),OEM,NAME('XMLSAM.DAT'),PRE(XSA),CREATE,BINDABLE,THREAD
StatusCodeKey            KEY(XSA:StatusCode),NOCASE,PRIMARY
Record                   RECORD,PRE()
StatusCode                  STRING(4)
                         END
                     END                       

XMLDOCS              FILE,DRIVER('Btrieve'),OEM,NAME('XMLDOCS'),PRE(XMD),CREATE,BINDABLE,THREAD
RecordNoKey              KEY(XMD:RECORD_NO),NOCASE,PRIMARY
TypeKey                  KEY(XMD:TYPE),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NO                   LONG
ENTRY_DATE                  DATE
ENTRY_TIME                  TIME
TYPE                        BYTE
PATHNAME                    STRING(255)
                         END
                     END                       

DEFEDI               FILE,DRIVER('Btrieve'),NAME('DEFEDI.DAT'),PRE(edi),CREATE,BINDABLE,THREAD
record_number_key        KEY(edi:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
Supplier_Code               STRING(8)
record_number               REAL
Country_Code                STRING(3)
Dealer_ID                   STRING(3)
                         END
                     END                       

XMLJOBS_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('XMLJOBS.DAT'),PRE(xjb_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(xjb_ali:RECORD_NUMBER),NOCASE,PRIMARY
TR_NO_KEY                KEY(xjb_ali:TR_NO),DUP,NOCASE
JobNumberKey             KEY(xjb_ali:JOB_NO),DUP,NOCASE
State_Job_Number_Key     KEY(xjb_ali:RECORD_STATE,xjb_ali:JOB_NO),DUP,NOCASE
State_Surname_Key        KEY(xjb_ali:RECORD_STATE,xjb_ali:CONSUMER_LAST_NAME),DUP,NOCASE
State_Postcode_Key       KEY(xjb_ali:RECORD_STATE,xjb_ali:CONSUMER_POSTCODE),DUP,NOCASE
State_Model_Key          KEY(xjb_ali:RECORD_STATE,xjb_ali:MODEL_CODE),DUP,NOCASE
State_Serial_No_Key      KEY(xjb_ali:RECORD_STATE,xjb_ali:SERIAL_NO),DUP,NOCASE
Record                   RECORD,PRE()
RECORD_NUMBER               LONG
RECORD_STATE                BYTE
EXCEPTION_CODE              LONG
EXCEPTION_DESC              STRING(255)
JOB_NO                      LONG
COMPANY                     STRING(4)
TR_NO                       STRING(30)
ASC_CODE                    STRING(10)
MODEL_CODE                  STRING(40)
SERIAL_NO                   STRING(16)
PURCHASE_DATE               DATE
SERVICE_TYPE                STRING(2)
IN_OUT_WARRANTY             STRING(1)
REQUEST_DATE                DATE
REQUEST_TIME                TIME
REPAIR_ETD_DATE             DATE
REPAIR_ETD_TIME             TIME
TR_STATUS                   STRING(2)
TR_REASON                   STRING(2)
CONSUMER_TITLE              STRING(20)
CONSUMER_FIRST_NAME         STRING(40)
CONSUMER_LAST_NAME          STRING(40)
CONSUMER_TEL_NUMBER1        STRING(30)
CONSUMER_FAX_NUMBER         STRING(30)
CONSUMER_EMAIL              STRING(241)
CONSUMER_COUNTRY            STRING(20)
CONSUMER_REGION             STRING(20)
CONSUMER_POSTCODE           STRING(10)
CONSUMER_CITY               STRING(40)
CONSUMER_STREET             STRING(60)
CONSUMER_HOUSE_NUMBER       STRING(20)
REPAIR_COMPLETE_DATE        DATE
REPAIR_COMPLETE_TIME        TIME
GOODS_DELIVERY_DATE         DATE
GOODS_DELIVERY_TIME         TIME
SYMPTOM1_DESC               STRING(40)
SYMPTOM2_DESC               STRING(40)
SYMPTOM3_DESC               STRING(40)
BP_NO                       STRING(10)
INQUIRY_TEXT                STRING(254)
                         END
                     END                       

AUDIT_ALIAS          FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(audali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(audali:Ref_Number,-audali:Date,-audali:Time,audali:Action),DUP,NOCASE
Action_Key               KEY(audali:Ref_Number,audali:Action,-audali:Date),DUP,NOCASE
User_Key                 KEY(audali:Ref_Number,audali:User,-audali:Date),DUP,NOCASE
Record_Number_Key        KEY(audali:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(audali:Action,audali:Date),DUP,NOCASE
TypeRefKey               KEY(audali:Ref_Number,audali:Type,-audali:Date,-audali:Time,audali:Action),DUP,NOCASE
TypeActionKey            KEY(audali:Ref_Number,audali:Type,audali:Action,-audali:Date),DUP,NOCASE
TypeUserKey              KEY(audali:Ref_Number,audali:Type,audali:User,-audali:Date),DUP,NOCASE
DateActionJobKey         KEY(audali:Date,audali:Action,audali:Ref_Number),DUP,NOCASE
DateJobKey               KEY(audali:Date,audali:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(audali:Date,audali:Type,audali:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(audali:Date,audali:Type,audali:Action,audali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
DummyField                  STRING(2)
Notes                       STRING(255)
Type                        STRING(3)
                         END
                     END                       

XMLSAM_ALIAS         FILE,DRIVER('Btrieve'),OEM,NAME('XMLSAM.DAT'),PRE(xsa_ali),CREATE,BINDABLE,THREAD
StatusCodeKey            KEY(xsa_ali:StatusCode),NOCASE,PRIMARY
Record                   RECORD,PRE()
StatusCode                  STRING(4)
                         END
                     END                       

MULDESP_ALIAS        FILE,DRIVER('Btrieve'),OEM,NAME('MULDESP.DAT'),PRE(muld_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(muld_ali:RecordNumber),NOCASE,PRIMARY
BatchNumberKey           KEY(muld_ali:BatchNumber),DUP,NOCASE
AccountNumberKey         KEY(muld_ali:AccountNumber),DUP,NOCASE
CourierKey               KEY(muld_ali:Courier),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
BatchNumber                 STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
BatchTotal                  LONG
                         END
                     END                       

MULDESPJ_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('MULDESPJ.DAT'),PRE(mulj_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(mulj_ali:RecordNumber),NOCASE,PRIMARY
JobNumberKey             KEY(mulj_ali:RefNumber,mulj_ali:JobNumber),DUP,NOCASE
CurrentJobKey            KEY(mulj_ali:RefNumber,mulj_ali:Current,mulj_ali:JobNumber),DUP,NOCASE
JobNumberOnlyKey         KEY(mulj_ali:JobNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobNumber                   LONG
IMEINumber                  STRING(30)
MSN                         STRING(30)
AccountNumber               STRING(30)
Courier                     STRING(30)
Current                     BYTE
                         END
                     END                       

SUBCONTALIAS         FILE,DRIVER('Btrieve'),OEM,NAME('ISUBCONT.DAT'),PRE(SUB1),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(SUB1:RecordNumber),NOCASE,PRIMARY
AccountNumberKey         KEY(SUB1:AccountNumber),NOCASE
BusinessNameKey          KEY(SUB1:BusinessName,SUB1:AccountNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
ContactName                 STRING(30)
AccountNumber               STRING(30)
BusinessName                STRING(30)
Postcode                    STRING(20)
BuildingName                STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
BusinessNumber              STRING(20)
FaxNumber                   STRING(20)
MobileNumber                STRING(20)
OtherNumber                 STRING(20)
EmailAddress                STRING(255)
DailyWorkLoad               LONG
Rating                      LONG
NumInstallers               LONG
ExpDatePubLiabIns           DATE
                         END
                     END                       

INSJOBSALIAS         FILE,DRIVER('Btrieve'),OEM,NAME('INSJOBS.DAT'),PRE(ins1),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(-ins1:RefNumber),NOCASE,PRIMARY
CompletedKey             KEY(ins1:EndDate,ins1:RefNumber),DUP,NOCASE
InvoiceNumberKey         KEY(ins1:InvoiceNumber,ins1:RefNumber),DUP,NOCASE
JobTypeKey               KEY(ins1:JobType),DUP,NOCASE
JobStatusKey             KEY(ins1:JobStatus),DUP,NOCASE
CompanyKey               KEY(ins1:AccountName),DUP,NOCASE
ContactKey               KEY(ins1:ContactName),DUP,NOCASE
PostCodeKey              KEY(ins1:Postcode),DUP,NOCASE
AllocJobKey              KEY(ins1:SubContractor,-ins1:RefNumber),DUP,NOCASE
AllocCompKey             KEY(ins1:SubContractor,ins1:AccountName),DUP,NOCASE
AllocPostCodeKey         KEY(ins1:SubContractor,ins1:Postcode),DUP,NOCASE
AllocStatusKey           KEY(ins1:SubContractor,ins1:JobStatus),DUP,NOCASE
AllocContactKey          KEY(ins1:SubContractor,ins1:ContactName),DUP,NOCASE
KeyClient                KEY(ins1:Client),DUP,NOCASE
KeyClientCoord           KEY(ins1:Client,ins1:WhoBooked),DUP,NOCASE
KeyAllocClient           KEY(ins1:SubContractor,ins1:WhoBooked,ins1:Client),DUP,NOCASE
KeyCoord                 KEY(ins1:WhoBooked),DUP,NOCASE
KeyAllocCoord            KEY(ins1:SubContractor,ins1:WhoBooked),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
DateBooked                  DATE
TimeBooked                  TIME
WhoBooked                   STRING(3)
AccountNumber               STRING(30)
AccountName                 STRING(30)
Postcode1                   STRING(4)
Postcode2                   STRING(4)
BuildingName                STRING(30)
AddressLine1                STRING(30)
AddressLine2                STRING(30)
AddressLine3                STRING(30)
TelephoneNumber             STRING(30)
FaxNumber                   STRING(30)
MobileNumber                STRING(30)
OtherNumber                 STRING(30)
ContactName                 STRING(30)
EmailAddress                STRING(255)
OrderNumber                 STRING(30)
NoOfJobs                    LONG
EngineersOnSite             LONG
SubContractor               STRING(30)
StartDate                   DATE
StartTime                   TIME
EndDate                     DATE
EndTime                     TIME
Completed                   BYTE
SpecialInstruct             STRING(255)
InvoiceNumber               LONG
DummyField                  STRING(13)
AssStock1                   BYTE
AssStock2                   BYTE
AssStock3                   BYTE
AssStock4                   BYTE
AssStock5                   BYTE
AssStock6                   BYTE
AssStock7                   BYTE
AssStock8                   BYTE
AssStock9                   BYTE
AssStock10                  BYTE
AssStock11                  BYTE
AssStock12                  BYTE
AssStock13                  BYTE
AssStock14                  BYTE
AssStock15                  BYTE
AssStock16                  BYTE
JobType                     STRING(30)
JobStatus                   STRING(30)
Invoiced                    BYTE
Surveyed                    BYTE
EngineerInfo                STRING(255)
Client                      STRING(30)
DolphinRef                  STRING(20)
MainJobType                 BYTE
CityLinkRef                 STRING(30)
NoStoreys                   LONG
ApproxHeight                LONG
Brick                       BYTE
Glass                       BYTE
House                       CSTRING(20)
Portacabin                  BYTE
CladdingDesign              BYTE
POD                         STRING(100)
Postcode                    STRING(10)
Projected                   LONG
                         END
                     END                       

ESTPARTS_ALIAS       FILE,DRIVER('Btrieve'),NAME('ESTPARTS.DAT'),PRE(epr_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(epr_ali:Ref_Number,epr_ali:Part_Number),DUP,NOCASE
record_number_key        KEY(epr_ali:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(epr_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(epr_ali:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(epr_ali:Ref_Number,epr_ali:Part_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(epr_ali:Ref_Number,epr_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(epr_ali:Supplier),DUP,NOCASE
Order_Part_Key           KEY(epr_ali:Ref_Number,epr_ali:Order_Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Date_Received               DATE
Order_Part_Number           REAL
Status_Date                 DATE
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

IEQUIP_ALIAS         FILE,DRIVER('Btrieve'),OEM,NAME('IEQUIP.DAT'),PRE(iequ_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(iequ_ali:RecordNumber),NOCASE,PRIMARY
JobPartNumberKey         KEY(iequ_ali:RefNumber,iequ_ali:PartNumber),DUP,NOCASE
RefPendRefKey            KEY(iequ_ali:RefNumber,iequ_ali:PendingRefNumber),DUP,NOCASE
ManufacturerKey          KEY(iequ_ali:Manufacturer),DUP,NOCASE
BrwManufacturer          KEY(iequ_ali:RefNumber,iequ_ali:Manufacturer),DUP,NOCASE
KeyPartNumber            KEY(iequ_ali:PartNumber),DUP,NOCASE
KeyDescription           KEY(iequ_ali:Description),DUP,NOCASE
KeyBrwDescription        KEY(iequ_ali:RefNumber,iequ_ali:Description),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PurchaseCost                REAL
SaleCost                    REAL
Supplier                    STRING(30)
Quantity                    LONG
ExcludeFromOrder            BYTE
DespatchNoteNumber          STRING(30)
PartRefNumber               LONG
DateOrdered                 DATE
PendingRefNumber            LONG
OrderNumber                 LONG
OrderPartNumber             LONG
DateReceived                DATE
Manufacturer                STRING(30)
Dummy                       STRING(3)
                         END
                     END                       

JOBS2_ALIAS          FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job2),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(job2:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job2:Model_Number,job2:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job2:Engineer,job2:Completed,job2:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job2:Engineer,job2:Workshop,job2:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job2:Surname),DUP,NOCASE
MobileNumberKey          KEY(job2:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job2:ESN),DUP,NOCASE
MSN_Key                  KEY(job2:MSN),DUP,NOCASE
AccountNumberKey         KEY(job2:Account_Number,job2:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job2:Account_Number,job2:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job2:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job2:Engineer,job2:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job2:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job2:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job2:Model_Number,job2:Date_Completed),DUP,NOCASE
By_Status                KEY(job2:Current_Status,job2:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job2:Current_Status,job2:Location,job2:Ref_Number),DUP,NOCASE
Location_Key             KEY(job2:Location,job2:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job2:Third_Party_Site,job2:Third_Party_Printed,job2:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job2:Job_Priority,job2:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job2:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job2:Manufacturer,job2:EDI,job2:EDI_Batch_Number,job2:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job2:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job2:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job2:Batch_Number,job2:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job2:Batch_Number,job2:Current_Status,job2:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job2:Batch_Number,job2:Model_Number,job2:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job2:Batch_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job2:Batch_Number,job2:Completed,job2:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job2:Chargeable_Job,job2:Account_Number,job2:Invoice_Number,job2:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job2:Invoice_Exception,job2:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job2:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job2:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job2:Despatched,job2:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job2:Despatched,job2:Account_Number,job2:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job2:Despatched,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job2:Despatched,job2:Account_Number,job2:Current_Courier,job2:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job2:Despatch_Number,job2:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job2:Courier,job2:Date_Despatched,job2:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job2:Loan_Courier,job2:Loan_Despatched,job2:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job2:Exchange_Courier,job2:Exchange_Despatched,job2:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job2:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job2:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job2:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job2:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job2:Bouncer,job2:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job2:Engineer,job2:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job2:Exchange_Status,job2:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job2:Loan_Status,job2:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job2:Exchange_Status,job2:Location,job2:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job2:Loan_Status,job2:Location,job2:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job2:InvoiceAccount,job2:InvoiceBatch,job2:InvoiceStatus,job2:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

LOGASSST_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('LOGASSST.DAT'),PRE(logast_alias),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logast_alias:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(logast_alias:RefNumber,logast_alias:PartNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
PartNumber                  STRING(30)
Description                 STRING(30)
PartRefNumber               LONG
Location                    STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

LOGSTOCK_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('LOGSTOCK.DAT'),PRE(logsto_ali),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(logsto_ali:RefNumber),NOCASE,PRIMARY
SalesKey                 KEY(logsto_ali:SalesCode),DUP,NOCASE
DescriptionKey           KEY(logsto_ali:Description),DUP,NOCASE
SalesModelNoKey          KEY(logsto_ali:SalesCode,logsto_ali:ModelNumber),NOCASE
RefModelNoKey            KEY(logsto_ali:RefNumber,logsto_ali:ModelNumber),DUP,NOCASE
ModelNumberKey           KEY(logsto_ali:ModelNumber),DUP,NOCASE
Record                   RECORD,PRE()
RefNumber                   LONG
SalesCode                   STRING(30)
Description                 STRING(30)
ModelNumber                 STRING(30)
DummyField                  STRING(4)
                         END
                     END                       

LOGSERST_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('LOGSERST.DAT'),PRE(logser_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(logser_ali:RecordNumber),NOCASE,PRIMARY
ESNKey                   KEY(logser_ali:ESN),DUP,NOCASE
RefNumberKey             KEY(logser_ali:RefNumber,logser_ali:ESN),DUP,NOCASE
ESNStatusKey             KEY(logser_ali:RefNumber,logser_ali:Status,logser_ali:ESN),DUP,NOCASE
NokiaStatusKey           KEY(logser_ali:RefNumber,logser_ali:Status,logser_ali:ClubNokia,logser_ali:ESN),DUP,NOCASE
AllocNo_Key              KEY(logser_ali:AllocNo,logser_ali:Status,logser_ali:ESN),DUP,NOCASE
Status_Alloc_Key         KEY(logser_ali:RefNumber,logser_ali:ClubNokia,logser_ali:Status),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
ESN                         STRING(30)
Status                      STRING(10)
ClubNokia                   STRING(30)
AllocNo                     LONG
DummyField                  STRING(4)
                         END
                     END                       

COURIER_ALIAS        FILE,DRIVER('Btrieve'),NAME('COURIER.DAT'),PRE(cou_ali),CREATE,BINDABLE,THREAD
Courier_Key              KEY(cou_ali:Courier),NOCASE,PRIMARY
Courier_Type_Key         KEY(cou_ali:Courier_Type,cou_ali:Courier),DUP,NOCASE
Record                   RECORD,PRE()
Courier                     STRING(30)
Account_Number              STRING(15)
Postcode                    STRING(10)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Contact_Name                STRING(60)
Export_Path                 STRING(255)
Include_Estimate            STRING(3)
Include_Chargeable          STRING(3)
Include_Warranty            STRING(3)
Service                     STRING(15)
Import_Path                 STRING(255)
Courier_Type                STRING(30)
Last_Despatch_Time          TIME
ICOLSuffix                  STRING(3)
ContractNumber              STRING(20)
ANCPath                     STRING(255)
ANCCount                    LONG
DespatchClose               STRING(3)
LabGOptions                 STRING(60)
CustomerCollection          BYTE
NewStatus                   STRING(30)
NoOfDespatchNotes           LONG
AutoConsignmentNo           BYTE
LastConsignmentNo           LONG
PrintLabel                  BYTE
                         END
                     END                       

JOBNOTES_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn_ali),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(jbn_ali:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
Fault_Description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
ColContatName               STRING(30)
ColDepartment               STRING(30)
DelContactName              STRING(30)
DelDepartment               STRING(30)
                         END
                     END                       

TRDBATCH_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('TRDBATCH.DAT'),PRE(trb_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(trb_ali:RecordNumber),NOCASE,PRIMARY
Batch_Number_Key         KEY(trb_ali:Company_Name,trb_ali:Batch_Number),DUP,NOCASE
Batch_Number_Status_Key  KEY(trb_ali:Status,trb_ali:Batch_Number),DUP,NOCASE
BatchOnlyKey             KEY(trb_ali:Batch_Number),DUP,NOCASE
Batch_Company_Status_Key KEY(trb_ali:Status,trb_ali:Company_Name,trb_ali:Batch_Number),DUP,NOCASE
ESN_Only_Key             KEY(trb_ali:ESN),DUP,NOCASE
ESN_Status_Key           KEY(trb_ali:Status,trb_ali:ESN),DUP,NOCASE
Company_Batch_ESN_Key    KEY(trb_ali:Company_Name,trb_ali:Batch_Number,trb_ali:ESN),DUP,NOCASE
AuthorisationKey         KEY(trb_ali:AuthorisationNo),DUP,NOCASE
StatusAuthKey            KEY(trb_ali:Status,trb_ali:AuthorisationNo),DUP,NOCASE
CompanyDateKey           KEY(trb_ali:Company_Name,trb_ali:Status,trb_ali:DateReturn),DUP,NOCASE
JobStatusKey             KEY(trb_ali:Status,trb_ali:Ref_Number),DUP,NOCASE
JobNumberKey             KEY(trb_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Batch_Number                LONG
Company_Name                STRING(30)
Ref_Number                  LONG
ESN                         STRING(20)
Status                      STRING(3)
Date                        DATE
Time                        TIME
AuthorisationNo             STRING(30)
Exchanged                   STRING(3)
DateReturn                  DATE
TimeReturn                  TIME
TurnTime                    LONG
MSN                         STRING(30)
                         END
                     END                       

INVOICE_ALIAS        FILE,DRIVER('Btrieve'),NAME('INVOICE.DAT'),PRE(inv_ali),CREATE,BINDABLE,THREAD
Invoice_Number_Key       KEY(inv_ali:Invoice_Number),NOCASE,PRIMARY
Invoice_Type_Key         KEY(inv_ali:Invoice_Type,inv_ali:Invoice_Number),DUP,NOCASE
Account_Number_Type_Key  KEY(inv_ali:Invoice_Type,inv_ali:Account_Number,inv_ali:Invoice_Number),DUP,NOCASE
Account_Number_Key       KEY(inv_ali:Account_Number,inv_ali:Invoice_Number),DUP,NOCASE
Date_Created_Key         KEY(inv_ali:Date_Created),DUP,NOCASE
Batch_Number_Key         KEY(inv_ali:Manufacturer,inv_ali:Batch_Number),DUP,NOCASE
Record                   RECORD,PRE()
Invoice_Number              REAL
Invoice_Type                STRING(3)
Job_Number                  REAL
Date_Created                DATE
Account_Number              STRING(15)
AccountType                 STRING(3)
Total                       REAL
Vat_Rate_Labour             REAL
Vat_Rate_Parts              REAL
Vat_Rate_Retail             REAL
VAT_Number                  STRING(30)
Invoice_VAT_Number          STRING(30)
Currency                    STRING(30)
Batch_Number                REAL
Manufacturer                STRING(30)
Claim_Reference             STRING(30)
Total_Claimed               REAL
Courier_Paid                REAL
Labour_Paid                 REAL
Parts_Paid                  REAL
Reconciled_Date             DATE
jobs_count                  REAL
PrevInvoiceNo               LONG
InvoiceCredit               STRING(3)
UseAlternativeAddress       BYTE
EuroExhangeRate             REAL
                         END
                     END                       

ORDPEND_ALIAS        FILE,DRIVER('Btrieve'),NAME('ORDPEND.DAT'),PRE(ope_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(ope_ali:Ref_Number),NOCASE,PRIMARY
Supplier_Key             KEY(ope_ali:Supplier,ope_ali:Part_Number),DUP,NOCASE
DescriptionKey           KEY(ope_ali:Supplier,ope_ali:Description),DUP,NOCASE
Supplier_Name_Key        KEY(ope_ali:Supplier),DUP,NOCASE
Part_Ref_Number_Key      KEY(ope_ali:Part_Ref_Number),DUP,NOCASE
Awaiting_Supplier_Key    KEY(ope_ali:Awaiting_Stock,ope_ali:Supplier,ope_ali:Part_Number),DUP,NOCASE
PartRecordNumberKey      KEY(ope_ali:PartRecordNumber),DUP,NOCASE
Job_Number_Key           KEY(ope_ali:Job_Number),DUP,NOCASE
Supplier_Job_Key         KEY(ope_ali:Supplier,ope_ali:Job_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Part_Ref_Number             LONG
Job_Number                  LONG
Part_Type                   STRING(3)
Supplier                    STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Quantity                    LONG
Account_Number              STRING(15)
Awaiting_Stock              STRING(3)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                     END                       

RETSALES_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('RETSALES.DAT'),PRE(res_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(res_ali:Ref_Number),NOCASE,PRIMARY
Invoice_Number_Key       KEY(res_ali:Invoice_Number),DUP,NOCASE
Despatched_Purchase_Key  KEY(res_ali:Despatched,res_ali:Purchase_Order_Number),DUP,NOCASE
Despatched_Ref_Key       KEY(res_ali:Despatched,res_ali:Ref_Number),DUP,NOCASE
Despatched_Account_Key   KEY(res_ali:Despatched,res_ali:Account_Number,res_ali:Purchase_Order_Number),DUP,NOCASE
Account_Number_Key       KEY(res_ali:Account_Number,res_ali:Ref_Number),DUP,NOCASE
Purchase_Number_Key      KEY(res_ali:Purchase_Order_Number),DUP,NOCASE
Despatch_Number_Key      KEY(res_ali:Despatch_Number),DUP,NOCASE
Date_Booked_Key          KEY(res_ali:date_booked),DUP,NOCASE
AccountInvoiceKey        KEY(res_ali:Account_Number,res_ali:Invoice_Number),DUP,NOCASE
DespatchedPurchDateKey   KEY(res_ali:Despatched,res_ali:Purchase_Order_Number,res_ali:date_booked),DUP,NOCASE
DespatchNoRefNoKey       KEY(res_ali:Despatch_Number,res_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Account_Number              STRING(15)
Contact_Name                STRING(30)
Purchase_Order_Number       STRING(30)
Delivery_Collection         STRING(3)
Payment_Method              STRING(3)
Courier_Cost                REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier                     STRING(30)
Consignment_Number          STRING(30)
Date_Despatched             DATE
Despatched                  STRING(3)
Despatch_Number             LONG
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Courier_Cost        REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
WebOrderNumber              LONG
WebDateCreated              DATE
WebTimeCreated              STRING(20)
WebCreatedUser              STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Building_Name               STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Postcode_Delivery           STRING(10)
Company_Name_Delivery       STRING(30)
Building_Name_Delivery      STRING(30)
Address_Line1_Delivery      STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Fax_Number_Delivery         STRING(15)
Delivery_Text               STRING(255)
Invoice_Text                STRING(255)
                         END
                     END                       

RETSTOCK_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('RETSTOCK.DAT'),PRE(ret_ali),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(ret_ali:Record_Number),NOCASE,PRIMARY
Part_Number_Key          KEY(ret_ali:Ref_Number,ret_ali:Part_Number),DUP,NOCASE
Despatched_Key           KEY(ret_ali:Ref_Number,ret_ali:Despatched,ret_ali:Despatch_Note_Number,ret_ali:Part_Number),DUP,NOCASE
Despatched_Only_Key      KEY(ret_ali:Ref_Number,ret_ali:Despatched),DUP,NOCASE
Pending_Ref_Number_Key   KEY(ret_ali:Ref_Number,ret_ali:Pending_Ref_Number),DUP,NOCASE
Order_Part_Key           KEY(ret_ali:Ref_Number,ret_ali:Order_Part_Number),DUP,NOCASE
Order_Number_Key         KEY(ret_ali:Ref_Number,ret_ali:Order_Number),DUP,NOCASE
DespatchedKey            KEY(ret_ali:Despatched),DUP,NOCASE
DespatchedPartKey        KEY(ret_ali:Ref_Number,ret_ali:Despatched,ret_ali:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               LONG
Ref_Number                  REAL
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Item_Cost                   REAL
Quantity                    REAL
Despatched                  STRING(20)
Order_Number                REAL
Date_Ordered                DATE
Date_Received               DATE
Despatch_Note_Number        REAL
Despatch_Date               DATE
Part_Ref_Number             REAL
Pending_Ref_Number          REAL
Order_Part_Number           REAL
Purchase_Order_Number       STRING(30)
Previous_Sale_Number        LONG
InvoicePart                 LONG
Credit                      STRING(3)
                         END
                     END                       

LOCATION_ALIAS       FILE,DRIVER('Btrieve'),NAME('LOCATION.DAT'),PRE(loc_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(loc_ali:RecordNumber),NOCASE,PRIMARY
Location_Key             KEY(loc_ali:Location),NOCASE
Main_Store_Key           KEY(loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE
ActiveLocationKey        KEY(loc_ali:Active,loc_ali:Location),DUP,NOCASE
ActiveMainStoreKey       KEY(loc_ali:Active,loc_ali:Main_Store,loc_ali:Location),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
Location                    STRING(30)
Main_Store                  STRING(3)
Active                      BYTE
PickNoteEnable              BYTE
                         END
                     END                       

COMMONFA_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('COMMONFA.DAT'),PRE(com_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(com_ali:Ref_Number),NOCASE,PRIMARY
Description_Key          KEY(com_ali:Model_Number,com_ali:Category,com_ali:Description),DUP,NOCASE
DescripOnlyKey           KEY(com_ali:Model_Number,com_ali:Description),DUP,NOCASE
Ref_Model_Key            KEY(com_ali:Model_Number,com_ali:Category,com_ali:Ref_Number),DUP,NOCASE
RefOnlyKey               KEY(com_ali:Model_Number,com_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
Category                    STRING(30)
date_booked                 DATE
time_booked                 TIME
who_booked                  STRING(3)
Model_Number                STRING(30)
Description                 STRING(30)
Chargeable_Job              STRING(3)
Chargeable_Charge_Type      STRING(30)
Chargeable_Repair_Type      STRING(30)
Warranty_Job                STRING(3)
Warranty_Charge_Type        STRING(30)
Warranty_Repair_Type        STRING(30)
Auto_Complete               STRING(3)
Attach_Diagram              STRING(3)
Diagram_Setting             STRING(1)
Diagram_Path                STRING(255)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(255)
Fault_Code11                  STRING(255)
Fault_Code12                  STRING(255)
                            END
Invoice_Text                STRING(255)
Engineers_Notes             STRING(255)
DummyField                  STRING(2)
                         END
                     END                       

COMMONCP_ALIAS       FILE,DRIVER('Btrieve'),NAME('COMMONCP.DAT'),PRE(ccp_ali),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(ccp_ali:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(ccp_ali:Ref_Number),DUP,NOCASE
Description_Key          KEY(ccp_ali:Ref_Number,ccp_ali:Description),DUP,NOCASE
RefPartNumberKey         KEY(ccp_ali:Ref_Number,ccp_ali:Part_Number),DUP,NOCASE
PartNumberKey            KEY(ccp_ali:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

COMMONWP_ALIAS       FILE,DRIVER('Btrieve'),NAME('COMMONWP.DAT'),PRE(cwp_alias),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(cwp_alias:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(cwp_alias:Ref_Number),DUP,NOCASE
Description_Key          KEY(cwp_alias:Ref_Number,cwp_alias:Description),DUP,NOCASE
RefPartNumberKey         KEY(cwp_alias:Ref_Number,cwp_alias:Part_Number),DUP,NOCASE
PartNumberKey            KEY(cwp_alias:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Quantity                    REAL
Part_Ref_Number             REAL
Adjustment                  STRING(3)
Exclude_From_Order          STRING(3)
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

STOMODEL_ALIAS       FILE,DRIVER('Btrieve'),NAME('STOMODEL.DAT'),PRE(stm_ali),CREATE,BINDABLE,THREAD
Model_Number_Key         KEY(stm_ali:Ref_Number,stm_ali:Manufacturer,stm_ali:Model_Number),NOCASE,PRIMARY
Model_Part_Number_Key    KEY(stm_ali:Accessory,stm_ali:Model_Number,stm_ali:Location,stm_ali:Part_Number),DUP,NOCASE
Description_Key          KEY(stm_ali:Accessory,stm_ali:Model_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE
Manufacturer_Key         KEY(stm_ali:Manufacturer,stm_ali:Model_Number),DUP,NOCASE
Ref_Part_Description     KEY(stm_ali:Ref_Number,stm_ali:Part_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE
Location_Part_Number_Key KEY(stm_ali:Model_Number,stm_ali:Location,stm_ali:Part_Number),DUP,NOCASE
Location_Description_Key KEY(stm_ali:Model_Number,stm_ali:Location,stm_ali:Description),DUP,NOCASE
Mode_Number_Only_Key     KEY(stm_ali:Ref_Number,stm_ali:Model_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Manufacturer                STRING(30)
Model_Number                STRING(30)
Part_Number                 STRING(30)
Description                 STRING(30)
Location                    STRING(30)
Accessory                   STRING(3)
FaultCode1                  STRING(30)
FaultCode2                  STRING(30)
FaultCode3                  STRING(30)
FaultCode4                  STRING(30)
FaultCode5                  STRING(30)
FaultCode6                  STRING(30)
FaultCode7                  STRING(30)
FaultCode8                  STRING(30)
FaultCode9                  STRING(30)
FaultCode10                 STRING(30)
FaultCode11                 STRING(30)
FaultCode12                 STRING(30)
                         END
                     END                       

JOBPAYMT_ALIAS       FILE,DRIVER('Btrieve'),NAME('JOBPAYMT.DAT'),PRE(jpt_ali),CREATE,BINDABLE,THREAD
Record_Number_Key        KEY(jpt_ali:Record_Number),NOCASE,PRIMARY
All_Date_Key             KEY(jpt_ali:Ref_Number,jpt_ali:Date),DUP,NOCASE
Record                   RECORD,PRE()
Record_Number               REAL
Ref_Number                  REAL
Date                        DATE
Payment_Type                STRING(30)
Credit_Card_Number          STRING(20)
Expiry_Date                 STRING(5)
Issue_Number                STRING(5)
Amount                      REAL
User_Code                   STRING(3)
                         END
                     END                       

PARTSTMP_ALIAS       FILE,DRIVER('Btrieve'),NAME(glo:file_name),PRE(partmp_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(partmp_ali:Ref_Number,partmp_ali:Part_Number),DUP,NOCASE
record_number_key        KEY(partmp_ali:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(partmp_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(partmp_ali:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(partmp_ali:Ref_Number,partmp_ali:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(partmp_ali:Ref_Number,partmp_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(partmp_ali:Ref_Number,partmp_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(partmp_ali:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

WPARTTMP_ALIAS       FILE,DRIVER('Btrieve'),NAME(glo:file_name2),PRE(wartmp_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(wartmp_ali:Ref_Number,wartmp_ali:Part_Number),DUP,NOCASE
record_number_key        KEY(wartmp_ali:record_number),NOCASE,PRIMARY
Part_Ref_Number2_Key     KEY(wartmp_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(wartmp_ali:Date_Ordered),DUP,NOCASE
Part_Ref_Number_Key      KEY(wartmp_ali:Ref_Number,wartmp_ali:Part_Ref_Number),DUP,NOCASE
Pending_Ref_Number_Key   KEY(wartmp_ali:Ref_Number,wartmp_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(wartmp_ali:Ref_Number,wartmp_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(wartmp_ali:Supplier),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  REAL
record_number               REAL
Adjustment                  STRING(3)
Part_Ref_Number             REAL
Part_Details_Group          GROUP
Part_Number                   STRING(30)
Description                   STRING(30)
Supplier                      STRING(30)
Purchase_Cost                 REAL
Sale_Cost                     REAL
Retail_Cost                   REAL
                            END
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          REAL
Order_Number                REAL
Main_Part                   STRING(3)
Fault_Codes1                GROUP
Fault_Code1                   STRING(30)
Fault_Code2                   STRING(30)
Fault_Code3                   STRING(30)
Fault_Code4                   STRING(30)
Fault_Code5                   STRING(30)
Fault_Code6                   STRING(30)
                            END
Fault_Codes2                GROUP
Fault_Code7                   STRING(30)
Fault_Code8                   STRING(30)
Fault_Code9                   STRING(30)
Fault_Code10                  STRING(30)
Fault_Code11                  STRING(30)
Fault_Code12                  STRING(30)
                            END
                         END
                     END                       

LOAN_ALIAS           FILE,DRIVER('Btrieve'),NAME('LOAN.DAT'),PRE(loa_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(loa_ali:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(loa_ali:ESN),DUP,NOCASE
MSN_Only_Key             KEY(loa_ali:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE
MSN_Key                  KEY(loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE
ESN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:ESN),DUP,NOCASE
MSN_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:MSN),DUP,NOCASE
Ref_Available_Key        KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(loa_ali:Available,loa_ali:Stock_Type,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(loa_ali:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(loa_ali:Available,loa_ali:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(loa_ali:Available,loa_ali:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(loa_ali:Available,loa_ali:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(loa_ali:Available,loa_ali:Model_Number,loa_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

EXCHANGE_ALIAS       FILE,DRIVER('Btrieve'),NAME('EXCHANGE.DAT'),PRE(xch_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(xch_ali:Ref_Number),NOCASE,PRIMARY
ESN_Only_Key             KEY(xch_ali:ESN),DUP,NOCASE
MSN_Only_Key             KEY(xch_ali:MSN),DUP,NOCASE
Ref_Number_Stock_Key     KEY(xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE
Model_Number_Key         KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
ESN_Key                  KEY(xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE
MSN_Key                  KEY(xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE
ESN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:ESN),DUP,NOCASE
MSN_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:MSN),DUP,NOCASE
Ref_Available_Key        KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Ref_Number),DUP,NOCASE
Model_Available_Key      KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
Stock_Type_Key           KEY(xch_ali:Stock_Type),DUP,NOCASE
ModelRefNoKey            KEY(xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
AvailIMEIOnlyKey         KEY(xch_ali:Available,xch_ali:ESN),DUP,NOCASE
AvailMSNOnlyKey          KEY(xch_ali:Available,xch_ali:MSN),DUP,NOCASE
AvailRefOnlyKey          KEY(xch_ali:Available,xch_ali:Ref_Number),DUP,NOCASE
AvailModOnlyKey          KEY(xch_ali:Available,xch_ali:Model_Number,xch_ali:Ref_Number),DUP,NOCASE
StockBookedKey           KEY(xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE
AvailBookedKey           KEY(xch_ali:Available,xch_ali:Stock_Type,xch_ali:Model_Number,xch_ali:Date_Booked,xch_ali:Ref_Number),DUP,NOCASE
DateBookedKey            KEY(xch_ali:Date_Booked),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(30)
MSN                         STRING(30)
Colour                      STRING(30)
Location                    STRING(30)
Shelf_Location              STRING(30)
Date_Booked                 DATE
Times_Issued                REAL
Available                   STRING(3)
Job_Number                  LONG
Stock_Type                  STRING(30)
Audit_Number                REAL
DummyField                  STRING(1)
                         END
                     END                       

ACCAREAS_ALIAS       FILE,DRIVER('Btrieve'),NAME('ACCAREAS.DAT'),PRE(acc_ali),CREATE,BINDABLE,THREAD
Access_level_key         KEY(acc_ali:User_Level,acc_ali:Access_Area),NOCASE,PRIMARY
AccessOnlyKey            KEY(acc_ali:Access_Area),DUP,NOCASE
Record                   RECORD,PRE()
Access_Area                 STRING(30)
User_Level                  STRING(30)
DummyField                  STRING(1)
                         END
                     END                       

USERS_ALIAS          FILE,DRIVER('Btrieve'),NAME('USERS.DAT'),PRE(use_ali),CREATE,BINDABLE,THREAD
User_Code_Key            KEY(use_ali:User_Code),NOCASE,PRIMARY
User_Type_Active_Key     KEY(use_ali:User_Type,use_ali:Active,use_ali:Surname),DUP,NOCASE
Surname_Active_Key       KEY(use_ali:Active,use_ali:Surname),DUP,NOCASE
User_Code_Active_Key     KEY(use_ali:Active,use_ali:User_Code),DUP,NOCASE
User_Type_Key            KEY(use_ali:User_Type,use_ali:Surname),DUP,NOCASE
surname_key              KEY(use_ali:Surname),DUP,NOCASE
password_key             KEY(use_ali:Password),NOCASE
Logged_In_Key            KEY(use_ali:Logged_In,use_ali:Surname),DUP,NOCASE
Team_Surname             KEY(use_ali:Team,use_ali:Surname),DUP,NOCASE
Active_Team_Surname      KEY(use_ali:Team,use_ali:Active,use_ali:Surname),DUP,NOCASE
Record                   RECORD,PRE()
User_Code                   STRING(3)
Forename                    STRING(30)
Surname                     STRING(30)
Password                    STRING(20)
User_Type                   STRING(15)
Job_Assignment              STRING(15)
Supervisor                  STRING(3)
User_Level                  STRING(30)
Logged_In                   STRING(1)
Logged_in_date              DATE
Logged_In_Time              TIME
Restrict_Logins             STRING(3)
Concurrent_Logins           REAL
Active                      STRING(3)
Team                        STRING(30)
Location                    STRING(30)
Repair_Target               REAL
SkillLevel                  LONG
StockFromLocationOnly       BYTE
EmailAddress                STRING(255)
                         END
                     END                       

WARPARTS_ALIAS       FILE,DRIVER('Btrieve'),NAME('WARPARTS.DAT'),PRE(war_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(war_ali:Ref_Number,war_ali:Part_Number),DUP,NOCASE
RecordNumberKey          KEY(war_ali:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(war_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(war_ali:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(war_ali:Ref_Number,war_ali:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(war_ali:Ref_Number,war_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(war_ali:Ref_Number,war_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(war_ali:Supplier),DUP,NOCASE
Order_Part_Key           KEY(war_ali:Ref_Number,war_ali:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(war_ali:Supplier,war_ali:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(war_ali:Supplier,war_ali:Date_Received),DUP,NOCASE
RequestedKey             KEY(war_ali:Requested,war_ali:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           REAL
Date_Received               DATE
Status_Date                 DATE
Main_Part                   STRING(3)
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END                       

ORDPARTS_ALIAS       FILE,DRIVER('Btrieve'),NAME('ORDPARTS.DAT'),PRE(orp_ali),CREATE,BINDABLE,THREAD
Order_Number_Key         KEY(orp_ali:Order_Number,orp_ali:Part_Ref_Number),DUP,NOCASE
record_number_key        KEY(orp_ali:Record_Number),NOCASE,PRIMARY
Ref_Number_Key           KEY(orp_ali:Part_Ref_Number,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE
Part_Number_Key          KEY(orp_ali:Order_Number,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE
Description_Key          KEY(orp_ali:Order_Number,orp_ali:Description,orp_ali:Part_Number),DUP,NOCASE
Received_Part_Number_Key KEY(orp_ali:All_Received,orp_ali:Part_Number),DUP,NOCASE
Account_Number_Key       KEY(orp_ali:Account_Number,orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Part_Number),DUP,NOCASE
Allocated_Key            KEY(orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Allocated_To_Sale,orp_ali:Part_Number),DUP,NOCASE
Allocated_Account_Key    KEY(orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Allocated_To_Sale,orp_ali:Account_Number,orp_ali:Part_Number),DUP,NOCASE
Order_Type_Received      KEY(orp_ali:Order_Number,orp_ali:Part_Type,orp_ali:All_Received,orp_ali:Part_Number,orp_ali:Description),DUP,NOCASE
PartRecordNumberKey      KEY(orp_ali:PartRecordNumber),DUP,NOCASE
Record                   RECORD,PRE()
Order_Number                LONG
Record_Number               LONG
Part_Ref_Number             LONG
Quantity                    LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Job_Number                  LONG
Part_Type                   STRING(3)
Number_Received             LONG
Date_Received               DATE
All_Received                STRING(3)
Allocated_To_Sale           STRING(3)
Account_Number              STRING(15)
DespatchNoteNumber          STRING(30)
Reason                      STRING(255)
PartRecordNumber            LONG
                         END
                     END                       

PARTS_ALIAS          FILE,DRIVER('Btrieve'),NAME('PARTS.DAT'),PRE(par_ali),CREATE,BINDABLE,THREAD
Part_Number_Key          KEY(par_ali:Ref_Number,par_ali:Part_Number),DUP,NOCASE
recordnumberkey          KEY(par_ali:Record_Number),NOCASE,PRIMARY
PartRefNoKey             KEY(par_ali:Part_Ref_Number),DUP,NOCASE
Date_Ordered_Key         KEY(par_ali:Date_Ordered),DUP,NOCASE
RefPartRefNoKey          KEY(par_ali:Ref_Number,par_ali:Part_Ref_Number),DUP,NOCASE
PendingRefNoKey          KEY(par_ali:Ref_Number,par_ali:Pending_Ref_Number),DUP,NOCASE
Order_Number_Key         KEY(par_ali:Ref_Number,par_ali:Order_Number),DUP,NOCASE
Supplier_Key             KEY(par_ali:Supplier),DUP,NOCASE
Order_Part_Key           KEY(par_ali:Ref_Number,par_ali:Order_Part_Number),DUP,NOCASE
SuppDateOrdKey           KEY(par_ali:Supplier,par_ali:Date_Ordered),DUP,NOCASE
SuppDateRecKey           KEY(par_ali:Supplier,par_ali:Date_Received),DUP,NOCASE
RequestedKey             KEY(par_ali:Requested,par_ali:Part_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Record_Number               LONG
Adjustment                  STRING(3)
Part_Ref_Number             LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
Quantity                    REAL
Warranty_Part               STRING(3)
Exclude_From_Order          STRING(3)
Despatch_Note_Number        STRING(30)
Date_Ordered                DATE
Pending_Ref_Number          LONG
Order_Number                LONG
Order_Part_Number           LONG
Date_Received               DATE
Status_Date                 DATE
Fault_Codes_Checked         STRING(3)
InvoicePart                 LONG
Credit                      STRING(3)
Requested                   BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
                         END
                     END                       

STOCK_ALIAS          FILE,DRIVER('Btrieve'),NAME('STOCK.DAT'),PRE(sto_ali),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(sto_ali:Ref_Number),NOCASE,PRIMARY
Sundry_Item_Key          KEY(sto_ali:Sundry_Item),DUP,NOCASE
Description_Key          KEY(sto_ali:Location,sto_ali:Description),DUP,NOCASE
Description_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Description),DUP,NOCASE
Shelf_Location_Key       KEY(sto_ali:Location,sto_ali:Shelf_Location),DUP,NOCASE
Shelf_Location_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE
Supplier_Accessory_Key   KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Shelf_Location),DUP,NOCASE
Manufacturer_Key         KEY(sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE
Supplier_Key             KEY(sto_ali:Location,sto_ali:Supplier),DUP,NOCASE
Location_Key             KEY(sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE
Part_Number_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Part_Number),DUP,NOCASE
Ref_Part_Description_Key KEY(sto_ali:Location,sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE
Location_Manufacturer_Key KEY(sto_ali:Location,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE
Manufacturer_Accessory_Key KEY(sto_ali:Location,sto_ali:Accessory,sto_ali:Manufacturer,sto_ali:Part_Number),DUP,NOCASE
Location_Part_Description_Key KEY(sto_ali:Location,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE
Ref_Part_Description2_Key KEY(sto_ali:Ref_Number,sto_ali:Part_Number,sto_ali:Description),DUP,NOCASE
Minimum_Part_Number_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE
Minimum_Description_Key  KEY(sto_ali:Minimum_Stock,sto_ali:Location,sto_ali:Description),DUP,NOCASE
SecondLocKey             KEY(sto_ali:Location,sto_ali:Shelf_Location,sto_ali:Second_Location,sto_ali:Part_Number),DUP,NOCASE
RequestedKey             KEY(sto_ali:QuantityRequested,sto_ali:Part_Number),DUP,NOCASE
ExchangeAccPartKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Part_Number),DUP,NOCASE
ExchangeAccDescKey       KEY(sto_ali:ExchangeUnit,sto_ali:Location,sto_ali:Description),DUP,NOCASE
Record                   RECORD,PRE()
Sundry_Item                 STRING(3)
Ref_Number                  LONG
Part_Number                 STRING(30)
Description                 STRING(30)
Supplier                    STRING(30)
Purchase_Cost               REAL
Sale_Cost                   REAL
Retail_Cost                 REAL
AccessoryCost               REAL
Percentage_Mark_Up          REAL
Shelf_Location              STRING(30)
Manufacturer                STRING(30)
Location                    STRING(30)
Second_Location             STRING(30)
Quantity_Stock              REAL
Quantity_To_Order           REAL
Quantity_On_Order           REAL
Minimum_Level               REAL
Reorder_Level               REAL
Use_VAT_Code                STRING(3)
Service_VAT_Code            STRING(2)
Retail_VAT_Code             STRING(2)
Superceeded                 STRING(3)
Superceeded_Ref_Number      REAL
Pending_Ref_Number          REAL
Accessory                   STRING(3)
Minimum_Stock               STRING(3)
Assign_Fault_Codes          STRING(3)
Individual_Serial_Numbers   STRING(3)
ExchangeUnit                STRING(3)
QuantityRequested           LONG
Suspend                     BYTE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(30)
Fault_Code11                STRING(30)
Fault_Code12                STRING(30)
E1                          BYTE
E2                          BYTE
E3                          BYTE
AllowDuplicate              BYTE
ExcludeFromEDI              BYTE
RF_Board                    BYTE
                         END
                     END                       

JOBS_ALIAS           FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(job_ali),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(job_ali:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(job_ali:Model_Number,job_ali:Unit_Type),DUP,NOCASE
EngCompKey               KEY(job_ali:Engineer,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(job_ali:Engineer,job_ali:Workshop,job_ali:Ref_Number),DUP,NOCASE
Surname_Key              KEY(job_ali:Surname),DUP,NOCASE
MobileNumberKey          KEY(job_ali:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(job_ali:ESN),DUP,NOCASE
MSN_Key                  KEY(job_ali:MSN),DUP,NOCASE
AccountNumberKey         KEY(job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(job_ali:Account_Number,job_ali:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(job_ali:Model_Number),DUP,NOCASE
Engineer_Key             KEY(job_ali:Engineer,job_ali:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(job_ali:date_booked),DUP,NOCASE
DateCompletedKey         KEY(job_ali:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(job_ali:Model_Number,job_ali:Date_Completed),DUP,NOCASE
By_Status                KEY(job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(job_ali:Current_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
Location_Key             KEY(job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:ESN),DUP,NOCASE
ThirdMsnKey              KEY(job_ali:Third_Party_Site,job_ali:Third_Party_Printed,job_ali:MSN),DUP,NOCASE
PriorityTypeKey          KEY(job_ali:Job_Priority,job_ali:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(job_ali:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(job_ali:Manufacturer,job_ali:EDI,job_ali:EDI_Batch_Number,job_ali:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(job_ali:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(job_ali:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(job_ali:Batch_Number,job_ali:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(job_ali:Batch_Number,job_ali:Current_Status,job_ali:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(job_ali:Batch_Number,job_ali:Model_Number,job_ali:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(job_ali:Batch_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(job_ali:Batch_Number,job_ali:Completed,job_ali:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(job_ali:Chargeable_Job,job_ali:Account_Number,job_ali:Invoice_Number,job_ali:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(job_ali:Invoice_Exception,job_ali:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(job_ali:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(job_ali:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(job_ali:Despatched,job_ali:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(job_ali:Despatched,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(job_ali:Despatched,job_ali:Account_Number,job_ali:Current_Courier,job_ali:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(job_ali:Despatch_Number,job_ali:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(job_ali:Courier,job_ali:Date_Despatched,job_ali:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(job_ali:Loan_Courier,job_ali:Loan_Despatched,job_ali:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(job_ali:Exchange_Courier,job_ali:Exchange_Despatched,job_ali:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(job_ali:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(job_ali:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(job_ali:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(job_ali:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(job_ali:Bouncer,job_ali:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(job_ali:Engineer,job_ali:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(job_ali:Exchange_Status,job_ali:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(job_ali:Loan_Status,job_ali:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(job_ali:Exchange_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(job_ali:Loan_Status,job_ali:Location,job_ali:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(job_ali:InvoiceAccount,job_ali:InvoiceBatch,job_ali:InvoiceStatus,job_ali:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

XMLTRADE_ALIAS       FILE,DRIVER('Btrieve'),OEM,NAME('XMLTRADE.DAT'),PRE(xtr_ali),CREATE,BINDABLE,THREAD
AccountNumberKey         KEY(xtr_ali:AccountNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
AccountNumber               STRING(15)
                         END
                     END                       

XMLSB_ALIAS          FILE,DRIVER('Btrieve'),OEM,NAME('XMLSB.DAT'),PRE(xsb_ali),CREATE,BINDABLE,THREAD
SBStatusKey              KEY(xsb_ali:SBStatus),NOCASE,PRIMARY
FKStatusCodeKey          KEY(xsb_ali:FKStatusCode),DUP,NOCASE
Record                   RECORD,PRE()
SBStatus                    STRING(30)
FKStatusCode                STRING(4)
                         END
                     END                       

STDCHRGE_ALIAS       FILE,DRIVER('Btrieve'),NAME('STDCHRGE.DAT'),PRE(sta_ali),CREATE,BINDABLE,THREAD
Model_Number_Charge_Key  KEY(sta_ali:Model_Number,sta_ali:Charge_Type,sta_ali:Unit_Type,sta_ali:Repair_Type),NOCASE,PRIMARY
Charge_Type_Key          KEY(sta_ali:Model_Number,sta_ali:Charge_Type),DUP,NOCASE
Unit_Type_Key            KEY(sta_ali:Model_Number,sta_ali:Unit_Type),DUP,NOCASE
Repair_Type_Key          KEY(sta_ali:Model_Number,sta_ali:Repair_Type),DUP,NOCASE
Cost_Key                 KEY(sta_ali:Model_Number,sta_ali:Cost),DUP,NOCASE
Charge_Type_Only_Key     KEY(sta_ali:Charge_Type),DUP,NOCASE
Repair_Type_Only_Key     KEY(sta_ali:Repair_Type),DUP,NOCASE
Unit_Type_Only_Key       KEY(sta_ali:Unit_Type),DUP,NOCASE
Record                   RECORD,PRE()
Charge_Type                 STRING(30)
Model_Number                STRING(30)
Unit_Type                   STRING(30)
Repair_Type                 STRING(30)
Cost                        REAL
dummy                       BYTE
                         END
                     END                       


glo:ErrorText        STRING(1000)
glo:owner            STRING(20)
glo:DateModify       DATE
glo:TimeModify       TIME
glo:Notes_Global     STRING(1000)
glo:Q_Invoice        QUEUE,PRE(glo)
Invoice_Number         REAL
Account_Number         STRING(15)
                     END
glo:afe_path         STRING(255)
glo:Password         STRING(20)
glo:PassAccount      STRING(30)
glo:Preview          STRING('True')
glo:q_JobNumber      QUEUE,PRE(GLO)
Job_Number_Pointer     REAL
                     END
glo:Q_PartsOrder     QUEUE,PRE(GLO)
Q_Order_Number         REAL
Q_Part_Number          STRING(30)
Q_Description          STRING(30)
Q_Supplier             STRING(30)
Q_Quantity             LONG
Q_Purchase_Cost        REAL
Q_Sale_Cost            REAL
                     END
glo:G_Select1        GROUP,PRE(GLO)
Select1                STRING(30)
Select2                STRING(40)
Select3                STRING(40)
Select4                STRING(40)
Select5                STRING(40)
Select6                STRING(40)
Select7                STRING(40)
Select8                STRING(40)
Select9                STRING(40)
Select10               STRING(40)
Select11               STRING(40)
Select12               STRING(40)
Select13               STRING(40)
Select14               STRING(40)
Select15               STRING(40)
Select16               STRING(40)
Select17               STRING(40)
Select18               STRING(40)
Select19               STRING(40)
Select20               STRING(40)
Select21               STRING(40)
Select22               STRING(40)
Select23               STRING(40)
Select24               STRING(40)
Select25               STRING(40)
Select26               STRING(40)
Select27               STRING(40)
Select28               STRING(40)
Select29               STRING(40)
Select30               STRING(40)
Select31               STRING(40)
Select32               STRING(40)
Select33               STRING(40)
Select34               STRING(40)
Select35               STRING(40)
Select36               STRING(40)
Select37               STRING(40)
Select38               STRING(40)
Select39               STRING(40)
Select40               STRING(40)
Select41               STRING(40)
Select42               STRING(40)
Select43               STRING(40)
Select44               STRING(40)
Select45               STRING(40)
Select46               STRING(40)
Select47               STRING(40)
Select48               STRING(40)
Select49               STRING(40)
Select50               STRING(40)
                     END
glo:q_RepairType     QUEUE,PRE(GLO)
Repair_Type_Pointer    STRING(30)
                     END
glo:Q_UnitType       QUEUE,PRE(GLO)
Unit_Type_Pointer      STRING(30)
                     END
glo:Q_ChargeType     QUEUE,PRE(GLO)
Charge_Type_Pointer    STRING(30)
                     END
glo:Q_ModelNumber    QUEUE,PRE(GLO)
Model_Number_Pointer   STRING(30)
                     END
glo:Q_Accessory      QUEUE,PRE(GLO)
Accessory_Pointer      STRING(30)
                     END
glo:Q_AccessLevel    QUEUE,PRE(GLO)
Access_Level_Pointer   STRING(30)
                     END
glo:Q_AccessAreas    QUEUE,PRE(GLO)
Access_Areas_Pointer   STRING(30)
                     END
glo:Q_Notes          QUEUE,PRE(GLO)
notes_pointer          STRING(80)
                     END
glo:EnableFlag       STRING(3)
glo:TimeLogged       TIME
glo:GLO:ApplicationName STRING(8)
glo:GLO:ApplicationCreationDate LONG
glo:GLO:ApplicationCreationTime LONG
glo:GLO:ApplicationChangeDate LONG
glo:GLO:ApplicationChangeTime LONG
glo:GLO:Compiled32   BYTE
glo:GLO:AppINIFile   STRING(255)
glo:Queue            QUEUE,PRE(GLO)
Pointer                STRING(40)
                     END
glo:Queue2           QUEUE,PRE(GLO)
Pointer2               STRING(40)
                     END
glo:Queue3           QUEUE,PRE(GLO)
Pointer3               STRING(40)
                     END
glo:Queue4           QUEUE,PRE(GLO)
Pointer4               STRING(40)
                     END
glo:Queue5           QUEUE,PRE(GLO)
Pointer5               STRING(40)
                     END
glo:Queue6           QUEUE,PRE(GLO)
Pointer6               STRING(40)
                     END
glo:Queue7           QUEUE,PRE(GLO)
Pointer7               STRING(40)
                     END
glo:Queue8           QUEUE,PRE(GLO)
Pointer8               STRING(40)
                     END
glo:Queue9           QUEUE,PRE(GLO)
Pointer9               STRING(40)
                     END
glo:Queue10          QUEUE,PRE(GLO)
Pointer10              STRING(40)
                     END
glo:Queue11          QUEUE,PRE(GLO)
Pointer11              STRING(40)
                     END
glo:Queue12          QUEUE,PRE(GLO)
Pointer12              STRING(40)
                     END
glo:Queue13          QUEUE,PRE(GLO)
Pointer13              STRING(40)
                     END
glo:Queue14          QUEUE,PRE(GLO)
Pointer14              STRING(40)
                     END
glo:Queue15          QUEUE,PRE(GLO)
Pointer15              STRING(40)
                     END
glo:Queue16          QUEUE,PRE(GLO)
Pointer16              STRING(40)
                     END
glo:Queue17          QUEUE,PRE(GLO)
Pointer17              STRING(40)
                     END
glo:Queue18          QUEUE,PRE(GLO)
Pointer18              STRING(40)
                     END
glo:Queue19          QUEUE,PRE(GLO)
Pointer19              STRING(40)
                     END
glo:Queue20          QUEUE,PRE(GLO)
Pointer20              STRING(40)
                     END
Access:STDCHRGE      &FileManager,THREAD                   ! FileManager for STDCHRGE
Relate:STDCHRGE      &RelationManager,THREAD               ! RelationManager for STDCHRGE
Access:DEFEMAIL      &FileManager,THREAD                   ! FileManager for DEFEMAIL
Relate:DEFEMAIL      &RelationManager,THREAD               ! RelationManager for DEFEMAIL
Access:CRCDEFS       &FileManager,THREAD                   ! FileManager for CRCDEFS
Relate:CRCDEFS       &RelationManager,THREAD               ! RelationManager for CRCDEFS
Access:REGIONS       &FileManager,THREAD                   ! FileManager for REGIONS
Relate:REGIONS       &RelationManager,THREAD               ! RelationManager for REGIONS
Access:ACCREG        &FileManager,THREAD                   ! FileManager for ACCREG
Relate:ACCREG        &RelationManager,THREAD               ! RelationManager for ACCREG
Access:XMLJOBS       &FileManager,THREAD                   ! FileManager for XMLJOBS
Relate:XMLJOBS       &RelationManager,THREAD               ! RelationManager for XMLJOBS
Access:EXCHAMF       &FileManager,THREAD                   ! FileManager for EXCHAMF
Relate:EXCHAMF       &RelationManager,THREAD               ! RelationManager for EXCHAMF
Access:EXCHAUI       &FileManager,THREAD                   ! FileManager for EXCHAUI
Relate:EXCHAUI       &RelationManager,THREAD               ! RelationManager for EXCHAUI
Access:JOBSENG       &FileManager,THREAD                   ! FileManager for JOBSENG
Relate:JOBSENG       &RelationManager,THREAD               ! RelationManager for JOBSENG
Access:MULDESPJ      &FileManager,THREAD                   ! FileManager for MULDESPJ
Relate:MULDESPJ      &RelationManager,THREAD               ! RelationManager for MULDESPJ
Access:MULDESP       &FileManager,THREAD                   ! FileManager for MULDESP
Relate:MULDESP       &RelationManager,THREAD               ! RelationManager for MULDESP
Access:INSMODELS     &FileManager,THREAD                   ! FileManager for INSMODELS
Relate:INSMODELS     &RelationManager,THREAD               ! RelationManager for INSMODELS
Access:STOCKLOG      &FileManager,THREAD                   ! FileManager for STOCKLOG
Relate:STOCKLOG      &RelationManager,THREAD               ! RelationManager for STOCKLOG
Access:IMEILOG       &FileManager,THREAD                   ! FileManager for IMEILOG
Relate:IMEILOG       &RelationManager,THREAD               ! RelationManager for IMEILOG
Access:STMASAUD      &FileManager,THREAD                   ! FileManager for STMASAUD
Relate:STMASAUD      &RelationManager,THREAD               ! RelationManager for STMASAUD
Access:AUDSTATS      &FileManager,THREAD                   ! FileManager for AUDSTATS
Relate:AUDSTATS      &RelationManager,THREAD               ! RelationManager for AUDSTATS
Access:IEXPDEFS      &FileManager,THREAD                   ! FileManager for IEXPDEFS
Relate:IEXPDEFS      &RelationManager,THREAD               ! RelationManager for IEXPDEFS
Access:RAPIDLST      &FileManager,THREAD                   ! FileManager for RAPIDLST
Relate:RAPIDLST      &RelationManager,THREAD               ! RelationManager for RAPIDLST
Access:ICONTHIST     &FileManager,THREAD                   ! FileManager for ICONTHIST
Relate:ICONTHIST     &RelationManager,THREAD               ! RelationManager for ICONTHIST
Access:ORDTEMP       &FileManager,THREAD                   ! FileManager for ORDTEMP
Relate:ORDTEMP       &RelationManager,THREAD               ! RelationManager for ORDTEMP
Access:ORDITEMS      &FileManager,THREAD                   ! FileManager for ORDITEMS
Relate:ORDITEMS      &RelationManager,THREAD               ! RelationManager for ORDITEMS
Access:ORDHEAD       &FileManager,THREAD                   ! FileManager for ORDHEAD
Relate:ORDHEAD       &RelationManager,THREAD               ! RelationManager for ORDHEAD
Access:IMEISHIP      &FileManager,THREAD                   ! FileManager for IMEISHIP
Relate:IMEISHIP      &RelationManager,THREAD               ! RelationManager for IMEISHIP
Access:IACTION       &FileManager,THREAD                   ! FileManager for IACTION
Relate:IACTION       &RelationManager,THREAD               ! RelationManager for IACTION
Access:JOBTHIRD      &FileManager,THREAD                   ! FileManager for JOBTHIRD
Relate:JOBTHIRD      &RelationManager,THREAD               ! RelationManager for JOBTHIRD
Access:DEFAULTV      &FileManager,THREAD                   ! FileManager for DEFAULTV
Relate:DEFAULTV      &RelationManager,THREAD               ! RelationManager for DEFAULTV
Access:LOGRETRN      &FileManager,THREAD                   ! FileManager for LOGRETRN
Relate:LOGRETRN      &RelationManager,THREAD               ! RelationManager for LOGRETRN
Access:PRODCODE      &FileManager,THREAD                   ! FileManager for PRODCODE
Relate:PRODCODE      &RelationManager,THREAD               ! RelationManager for PRODCODE
Access:LOG2TEMP      &FileManager,THREAD                   ! FileManager for LOG2TEMP
Relate:LOG2TEMP      &RelationManager,THREAD               ! RelationManager for LOG2TEMP
Access:LOGSTOCK      &FileManager,THREAD                   ! FileManager for LOGSTOCK
Relate:LOGSTOCK      &RelationManager,THREAD               ! RelationManager for LOGSTOCK
Access:LABLGTMP      &FileManager,THREAD                   ! FileManager for LABLGTMP
Relate:LABLGTMP      &RelationManager,THREAD               ! RelationManager for LABLGTMP
Access:LETTERS       &FileManager,THREAD                   ! FileManager for LETTERS
Relate:LETTERS       &RelationManager,THREAD               ! RelationManager for LETTERS
Access:EXPGEN        &FileManager,THREAD                   ! FileManager for EXPGEN
Relate:EXPGEN        &RelationManager,THREAD               ! RelationManager for EXPGEN
Access:STOAUDIT      &FileManager,THREAD                   ! FileManager for STOAUDIT
Relate:STOAUDIT      &RelationManager,THREAD               ! RelationManager for STOAUDIT
Access:DEFAULT2      &FileManager,THREAD                   ! FileManager for DEFAULT2
Relate:DEFAULT2      &RelationManager,THREAD               ! RelationManager for DEFAULT2
Access:ACTION        &FileManager,THREAD                   ! FileManager for ACTION
Relate:ACTION        &RelationManager,THREAD               ! RelationManager for ACTION
Access:DEFRAPID      &FileManager,THREAD                   ! FileManager for DEFRAPID
Relate:DEFRAPID      &RelationManager,THREAD               ! RelationManager for DEFRAPID
Access:RETSALES      &FileManager,THREAD                   ! FileManager for RETSALES
Relate:RETSALES      &RelationManager,THREAD               ! RelationManager for RETSALES
Access:BOUNCER       &FileManager,THREAD                   ! FileManager for BOUNCER
Relate:BOUNCER       &RelationManager,THREAD               ! RelationManager for BOUNCER
Access:TEAMS         &FileManager,THREAD                   ! FileManager for TEAMS
Relate:TEAMS         &RelationManager,THREAD               ! RelationManager for TEAMS
Access:MERGE         &FileManager,THREAD                   ! FileManager for MERGE
Relate:MERGE         &RelationManager,THREAD               ! RelationManager for MERGE
Access:UPDDATA       &FileManager,THREAD                   ! FileManager for UPDDATA
Relate:UPDDATA       &RelationManager,THREAD               ! RelationManager for UPDDATA
Access:EXPSPARES     &FileManager,THREAD                   ! FileManager for EXPSPARES
Relate:EXPSPARES     &RelationManager,THREAD               ! RelationManager for EXPSPARES
Access:WEBDEFLT      &FileManager,THREAD                   ! FileManager for WEBDEFLT
Relate:WEBDEFLT      &RelationManager,THREAD               ! RelationManager for WEBDEFLT
Access:EXCAUDIT      &FileManager,THREAD                   ! FileManager for EXCAUDIT
Relate:EXCAUDIT      &RelationManager,THREAD               ! RelationManager for EXCAUDIT
Access:COMMONFA      &FileManager,THREAD                   ! FileManager for COMMONFA
Relate:COMMONFA      &RelationManager,THREAD               ! RelationManager for COMMONFA
Access:PARAMSS       &FileManager,THREAD                   ! FileManager for PARAMSS
Relate:PARAMSS       &RelationManager,THREAD               ! RelationManager for PARAMSS
Access:INSJOBS       &FileManager,THREAD                   ! FileManager for INSJOBS
Relate:INSJOBS       &RelationManager,THREAD               ! RelationManager for INSJOBS
Access:CONSIGN       &FileManager,THREAD                   ! FileManager for CONSIGN
Relate:CONSIGN       &RelationManager,THREAD               ! RelationManager for CONSIGN
Access:JOBEXACC      &FileManager,THREAD                   ! FileManager for JOBEXACC
Relate:JOBEXACC      &RelationManager,THREAD               ! RelationManager for JOBEXACC
Access:MODELCOL      &FileManager,THREAD                   ! FileManager for MODELCOL
Relate:MODELCOL      &RelationManager,THREAD               ! RelationManager for MODELCOL
Access:COMMCAT       &FileManager,THREAD                   ! FileManager for COMMCAT
Relate:COMMCAT       &RelationManager,THREAD               ! RelationManager for COMMCAT
Access:MESSAGES      &FileManager,THREAD                   ! FileManager for MESSAGES
Relate:MESSAGES      &RelationManager,THREAD               ! RelationManager for MESSAGES
Access:LOGEXHE       &FileManager,THREAD                   ! FileManager for LOGEXHE
Relate:LOGEXHE       &RelationManager,THREAD               ! RelationManager for LOGEXHE
Access:DEFCRC        &FileManager,THREAD                   ! FileManager for DEFCRC
Relate:DEFCRC        &RelationManager,THREAD               ! RelationManager for DEFCRC
Access:PAYTYPES      &FileManager,THREAD                   ! FileManager for PAYTYPES
Relate:PAYTYPES      &RelationManager,THREAD               ! RelationManager for PAYTYPES
Access:COMMONWP      &FileManager,THREAD                   ! FileManager for COMMONWP
Relate:COMMONWP      &RelationManager,THREAD               ! RelationManager for COMMONWP
Access:LOGEXCH       &FileManager,THREAD                   ! FileManager for LOGEXCH
Relate:LOGEXCH       &RelationManager,THREAD               ! RelationManager for LOGEXCH
Access:QAREASON      &FileManager,THREAD                   ! FileManager for QAREASON
Relate:QAREASON      &RelationManager,THREAD               ! RelationManager for QAREASON
Access:COMMONCP      &FileManager,THREAD                   ! FileManager for COMMONCP
Relate:COMMONCP      &RelationManager,THREAD               ! RelationManager for COMMONCP
Access:ADDSEARCH     &FileManager,THREAD                   ! FileManager for ADDSEARCH
Relate:ADDSEARCH     &RelationManager,THREAD               ! RelationManager for ADDSEARCH
Access:CONTHIST      &FileManager,THREAD                   ! FileManager for CONTHIST
Relate:CONTHIST      &RelationManager,THREAD               ! RelationManager for CONTHIST
Access:CONTACTS      &FileManager,THREAD                   ! FileManager for CONTACTS
Relate:CONTACTS      &RelationManager,THREAD               ! RelationManager for CONTACTS
Access:EXPJOBS       &FileManager,THREAD                   ! FileManager for EXPJOBS
Relate:EXPJOBS       &RelationManager,THREAD               ! RelationManager for EXPJOBS
Access:EXPAUDIT      &FileManager,THREAD                   ! FileManager for EXPAUDIT
Relate:EXPAUDIT      &RelationManager,THREAD               ! RelationManager for EXPAUDIT
Access:JOBBATCH      &FileManager,THREAD                   ! FileManager for JOBBATCH
Relate:JOBBATCH      &RelationManager,THREAD               ! RelationManager for JOBBATCH
Access:DEFEDI2       &FileManager,THREAD                   ! FileManager for DEFEDI2
Relate:DEFEDI2       &RelationManager,THREAD               ! RelationManager for DEFEDI2
Access:DEFPRINT      &FileManager,THREAD                   ! FileManager for DEFPRINT
Relate:DEFPRINT      &RelationManager,THREAD               ! RelationManager for DEFPRINT
Access:DEFWEB        &FileManager,THREAD                   ! FileManager for DEFWEB
Relate:DEFWEB        &RelationManager,THREAD               ! RelationManager for DEFWEB
Access:EXPCITY       &FileManager,THREAD                   ! FileManager for EXPCITY
Relate:EXPCITY       &RelationManager,THREAD               ! RelationManager for EXPCITY
Access:PROCCODE      &FileManager,THREAD                   ! FileManager for PROCCODE
Relate:PROCCODE      &RelationManager,THREAD               ! RelationManager for PROCCODE
Access:COLOUR        &FileManager,THREAD                   ! FileManager for COLOUR
Relate:COLOUR        &RelationManager,THREAD               ! RelationManager for COLOUR
Access:VODAIMP       &FileManager,THREAD                   ! FileManager for VODAIMP
Relate:VODAIMP       &RelationManager,THREAD               ! RelationManager for VODAIMP
Access:JOBSVODA      &FileManager,THREAD                   ! FileManager for JOBSVODA
Relate:JOBSVODA      &RelationManager,THREAD               ! RelationManager for JOBSVODA
Access:XREPACT       &FileManager,THREAD                   ! FileManager for XREPACT
Relate:XREPACT       &RelationManager,THREAD               ! RelationManager for XREPACT
Access:ACCESDEF      &FileManager,THREAD                   ! FileManager for ACCESDEF
Relate:ACCESDEF      &RelationManager,THREAD               ! RelationManager for ACCESDEF
Access:STANTEXT      &FileManager,THREAD                   ! FileManager for STANTEXT
Relate:STANTEXT      &RelationManager,THREAD               ! RelationManager for STANTEXT
Access:NOTESENG      &FileManager,THREAD                   ! FileManager for NOTESENG
Relate:NOTESENG      &RelationManager,THREAD               ! RelationManager for NOTESENG
Access:IAUDIT        &FileManager,THREAD                   ! FileManager for IAUDIT
Relate:IAUDIT        &RelationManager,THREAD               ! RelationManager for IAUDIT
Access:IINVJOB       &FileManager,THREAD                   ! FileManager for IINVJOB
Relate:IINVJOB       &RelationManager,THREAD               ! RelationManager for IINVJOB
Access:EXPLABG       &FileManager,THREAD                   ! FileManager for EXPLABG
Relate:EXPLABG       &RelationManager,THREAD               ! RelationManager for EXPLABG
Access:INSDEF        &FileManager,THREAD                   ! FileManager for INSDEF
Relate:INSDEF        &RelationManager,THREAD               ! RelationManager for INSDEF
Access:IEXPUSE       &FileManager,THREAD                   ! FileManager for IEXPUSE
Relate:IEXPUSE       &RelationManager,THREAD               ! RelationManager for IEXPUSE
Access:IRECEIVE      &FileManager,THREAD                   ! FileManager for IRECEIVE
Relate:IRECEIVE      &RelationManager,THREAD               ! RelationManager for IRECEIVE
Access:IJOBSTATUS    &FileManager,THREAD                   ! FileManager for IJOBSTATUS
Relate:IJOBSTATUS    &RelationManager,THREAD               ! RelationManager for IJOBSTATUS
Access:ICLCOST       &FileManager,THREAD                   ! FileManager for ICLCOST
Relate:ICLCOST       &RelationManager,THREAD               ! RelationManager for ICLCOST
Access:IVEHDETS      &FileManager,THREAD                   ! FileManager for IVEHDETS
Relate:IVEHDETS      &RelationManager,THREAD               ! RelationManager for IVEHDETS
Access:IAPPENG       &FileManager,THREAD                   ! FileManager for IAPPENG
Relate:IAPPENG       &RelationManager,THREAD               ! RelationManager for IAPPENG
Access:IAPPTYPE      &FileManager,THREAD                   ! FileManager for IAPPTYPE
Relate:IAPPTYPE      &RelationManager,THREAD               ! RelationManager for IAPPTYPE
Access:INSMAKE       &FileManager,THREAD                   ! FileManager for INSMAKE
Relate:INSMAKE       &RelationManager,THREAD               ! RelationManager for INSMAKE
Access:INSAPPS       &FileManager,THREAD                   ! FileManager for INSAPPS
Relate:INSAPPS       &RelationManager,THREAD               ! RelationManager for INSAPPS
Access:ISUBCONT      &FileManager,THREAD                   ! FileManager for ISUBCONT
Relate:ISUBCONT      &RelationManager,THREAD               ! RelationManager for ISUBCONT
Access:TRDSPEC       &FileManager,THREAD                   ! FileManager for TRDSPEC
Relate:TRDSPEC       &RelationManager,THREAD               ! RelationManager for TRDSPEC
Access:ITIMES        &FileManager,THREAD                   ! FileManager for ITIMES
Relate:ITIMES        &RelationManager,THREAD               ! RelationManager for ITIMES
Access:IEQUIP        &FileManager,THREAD                   ! FileManager for IEQUIP
Relate:IEQUIP        &RelationManager,THREAD               ! RelationManager for IEQUIP
Access:EPSCSV        &FileManager,THREAD                   ! FileManager for EPSCSV
Relate:EPSCSV        &RelationManager,THREAD               ! RelationManager for EPSCSV
Access:IENGINEERS    &FileManager,THREAD                   ! FileManager for IENGINEERS
Relate:IENGINEERS    &RelationManager,THREAD               ! RelationManager for IENGINEERS
Access:ICLIENTS      &FileManager,THREAD                   ! FileManager for ICLIENTS
Relate:ICLIENTS      &RelationManager,THREAD               ! RelationManager for ICLIENTS
Access:ISUBPOST      &FileManager,THREAD                   ! FileManager for ISUBPOST
Relate:ISUBPOST      &RelationManager,THREAD               ! RelationManager for ISUBPOST
Access:IJOBTYPE      &FileManager,THREAD                   ! FileManager for IJOBTYPE
Relate:IJOBTYPE      &RelationManager,THREAD               ! RelationManager for IJOBTYPE
Access:EXPGENDM      &FileManager,THREAD                   ! FileManager for EXPGENDM
Relate:EXPGENDM      &RelationManager,THREAD               ! RelationManager for EXPGENDM
Access:EDIBATCH      &FileManager,THREAD                   ! FileManager for EDIBATCH
Relate:EDIBATCH      &RelationManager,THREAD               ! RelationManager for EDIBATCH
Access:ORDPEND       &FileManager,THREAD                   ! FileManager for ORDPEND
Relate:ORDPEND       &RelationManager,THREAD               ! RelationManager for ORDPEND
Access:STOHIST       &FileManager,THREAD                   ! FileManager for STOHIST
Relate:STOHIST       &RelationManager,THREAD               ! RelationManager for STOHIST
Access:NEWFEAT       &FileManager,THREAD                   ! FileManager for NEWFEAT
Relate:NEWFEAT       &RelationManager,THREAD               ! RelationManager for NEWFEAT
Access:EXREASON      &FileManager,THREAD                   ! FileManager for EXREASON
Relate:EXREASON      &RelationManager,THREAD               ! RelationManager for EXREASON
Access:REPTYDEF      &FileManager,THREAD                   ! FileManager for REPTYDEF
Relate:REPTYDEF      &RelationManager,THREAD               ! RelationManager for REPTYDEF
Access:EPSIMP        &FileManager,THREAD                   ! FileManager for EPSIMP
Relate:EPSIMP        &RelationManager,THREAD               ! RelationManager for EPSIMP
Access:RETACCOUNTSLIST &FileManager,THREAD                 ! FileManager for RETACCOUNTSLIST
Relate:RETACCOUNTSLIST &RelationManager,THREAD             ! RelationManager for RETACCOUNTSLIST
Access:PRIORITY      &FileManager,THREAD                   ! FileManager for PRIORITY
Relate:PRIORITY      &RelationManager,THREAD               ! RelationManager for PRIORITY
Access:DEFEPS        &FileManager,THREAD                   ! FileManager for DEFEPS
Relate:DEFEPS        &RelationManager,THREAD               ! RelationManager for DEFEPS
Access:JOBSE         &FileManager,THREAD                   ! FileManager for JOBSE
Relate:JOBSE         &RelationManager,THREAD               ! RelationManager for JOBSE
Access:REPEXTTP      &FileManager,THREAD                   ! FileManager for REPEXTTP
Relate:REPEXTTP      &RelationManager,THREAD               ! RelationManager for REPEXTTP
Access:MANFAULT      &FileManager,THREAD                   ! FileManager for MANFAULT
Relate:MANFAULT      &RelationManager,THREAD               ! RelationManager for MANFAULT
Access:INVPARTS      &FileManager,THREAD                   ! FileManager for INVPARTS
Relate:INVPARTS      &RelationManager,THREAD               ! RelationManager for INVPARTS
Access:JOBSEARC      &FileManager,THREAD                   ! FileManager for JOBSEARC
Relate:JOBSEARC      &RelationManager,THREAD               ! RelationManager for JOBSEARC
Access:INVPATMP      &FileManager,THREAD                   ! FileManager for INVPATMP
Relate:INVPATMP      &RelationManager,THREAD               ! RelationManager for INVPATMP
Access:TRACHAR       &FileManager,THREAD                   ! FileManager for TRACHAR
Relate:TRACHAR       &RelationManager,THREAD               ! RelationManager for TRACHAR
Access:CITYSERV      &FileManager,THREAD                   ! FileManager for CITYSERV
Relate:CITYSERV      &RelationManager,THREAD               ! RelationManager for CITYSERV
Access:PICKNOTE      &FileManager,THREAD                   ! FileManager for PICKNOTE
Relate:PICKNOTE      &RelationManager,THREAD               ! RelationManager for PICKNOTE
Access:DISCOUNT      &FileManager,THREAD                   ! FileManager for DISCOUNT
Relate:DISCOUNT      &RelationManager,THREAD               ! RelationManager for DISCOUNT
Access:LOCVALUE      &FileManager,THREAD                   ! FileManager for LOCVALUE
Relate:LOCVALUE      &RelationManager,THREAD               ! RelationManager for LOCVALUE
Access:STOCKTYP      &FileManager,THREAD                   ! FileManager for STOCKTYP
Relate:STOCKTYP      &RelationManager,THREAD               ! RelationManager for STOCKTYP
Access:IEMAILS       &FileManager,THREAD                   ! FileManager for IEMAILS
Relate:IEMAILS       &RelationManager,THREAD               ! RelationManager for IEMAILS
Access:COURIER       &FileManager,THREAD                   ! FileManager for COURIER
Relate:COURIER       &RelationManager,THREAD               ! RelationManager for COURIER
Access:TRDPARTY      &FileManager,THREAD                   ! FileManager for TRDPARTY
Relate:TRDPARTY      &RelationManager,THREAD               ! RelationManager for TRDPARTY
Access:JOBSTMP       &FileManager,THREAD                   ! FileManager for JOBSTMP
Relate:JOBSTMP       &RelationManager,THREAD               ! RelationManager for JOBSTMP
Access:LOGALLOC      &FileManager,THREAD                   ! FileManager for LOGALLOC
Relate:LOGALLOC      &RelationManager,THREAD               ! RelationManager for LOGALLOC
Access:JOBNOTES      &FileManager,THREAD                   ! FileManager for JOBNOTES
Relate:JOBNOTES      &RelationManager,THREAD               ! RelationManager for JOBNOTES
Access:PROINV        &FileManager,THREAD                   ! FileManager for PROINV
Relate:PROINV        &RelationManager,THREAD               ! RelationManager for PROINV
Access:EXPBUS        &FileManager,THREAD                   ! FileManager for EXPBUS
Relate:EXPBUS        &RelationManager,THREAD               ! RelationManager for EXPBUS
Access:IMPCITY       &FileManager,THREAD                   ! FileManager for IMPCITY
Relate:IMPCITY       &RelationManager,THREAD               ! RelationManager for IMPCITY
Access:JOBACTMP      &FileManager,THREAD                   ! FileManager for JOBACTMP
Relate:JOBACTMP      &RelationManager,THREAD               ! RelationManager for JOBACTMP
Access:RETPARTSLIST  &FileManager,THREAD                   ! FileManager for RETPARTSLIST
Relate:RETPARTSLIST  &RelationManager,THREAD               ! RelationManager for RETPARTSLIST
Access:TRANTYPE      &FileManager,THREAD                   ! FileManager for TRANTYPE
Relate:TRANTYPE      &RelationManager,THREAD               ! RelationManager for TRANTYPE
Access:DESBATCH      &FileManager,THREAD                   ! FileManager for DESBATCH
Relate:DESBATCH      &RelationManager,THREAD               ! RelationManager for DESBATCH
Access:JOBVODAC      &FileManager,THREAD                   ! FileManager for JOBVODAC
Relate:JOBVODAC      &RelationManager,THREAD               ! RelationManager for JOBVODAC
Access:ESNMODEL      &FileManager,THREAD                   ! FileManager for ESNMODEL
Relate:ESNMODEL      &RelationManager,THREAD               ! RelationManager for ESNMODEL
Access:JOBPAYMT      &FileManager,THREAD                   ! FileManager for JOBPAYMT
Relate:JOBPAYMT      &RelationManager,THREAD               ! RelationManager for JOBPAYMT
Access:LOGASSSTTEMP  &FileManager,THREAD                   ! FileManager for LOGASSSTTEMP
Relate:LOGASSSTTEMP  &RelationManager,THREAD               ! RelationManager for LOGASSSTTEMP
Access:STOCK         &FileManager,THREAD                   ! FileManager for STOCK
Relate:STOCK         &RelationManager,THREAD               ! RelationManager for STOCK
Access:LOGGED        &FileManager,THREAD                   ! FileManager for LOGGED
Relate:LOGGED        &RelationManager,THREAD               ! RelationManager for LOGGED
Access:LOGRTHIS      &FileManager,THREAD                   ! FileManager for LOGRTHIS
Relate:LOGRTHIS      &RelationManager,THREAD               ! RelationManager for LOGRTHIS
Access:LOGSTHIS      &FileManager,THREAD                   ! FileManager for LOGSTHIS
Relate:LOGSTHIS      &RelationManager,THREAD               ! RelationManager for LOGSTHIS
Access:LOGASSST      &FileManager,THREAD                   ! FileManager for LOGASSST
Relate:LOGASSST      &RelationManager,THREAD               ! RelationManager for LOGASSST
Access:LOGSTOLC      &FileManager,THREAD                   ! FileManager for LOGSTOLC
Relate:LOGSTOLC      &RelationManager,THREAD               ! RelationManager for LOGSTOLC
Access:LOGTEMP       &FileManager,THREAD                   ! FileManager for LOGTEMP
Relate:LOGTEMP       &RelationManager,THREAD               ! RelationManager for LOGTEMP
Access:LOGSERST      &FileManager,THREAD                   ! FileManager for LOGSERST
Relate:LOGSERST      &RelationManager,THREAD               ! RelationManager for LOGSERST
Access:LOGDEFLT      &FileManager,THREAD                   ! FileManager for LOGDEFLT
Relate:LOGDEFLT      &RelationManager,THREAD               ! RelationManager for LOGDEFLT
Access:LOGSTLOC      &FileManager,THREAD                   ! FileManager for LOGSTLOC
Relate:LOGSTLOC      &RelationManager,THREAD               ! RelationManager for LOGSTLOC
Access:MULTIDEF      &FileManager,THREAD                   ! FileManager for MULTIDEF
Relate:MULTIDEF      &RelationManager,THREAD               ! RelationManager for MULTIDEF
Access:TRAFAULO      &FileManager,THREAD                   ! FileManager for TRAFAULO
Relate:TRAFAULO      &RelationManager,THREAD               ! RelationManager for TRAFAULO
Access:TRAFAULT      &FileManager,THREAD                   ! FileManager for TRAFAULT
Relate:TRAFAULT      &RelationManager,THREAD               ! RelationManager for TRAFAULT
Access:EXMINLEV      &FileManager,THREAD                   ! FileManager for EXMINLEV
Relate:EXMINLEV      &RelationManager,THREAD               ! RelationManager for EXMINLEV
Access:LOGSTHII      &FileManager,THREAD                   ! FileManager for LOGSTHII
Relate:LOGSTHII      &RelationManager,THREAD               ! RelationManager for LOGSTHII
Access:LOGCLSTE      &FileManager,THREAD                   ! FileManager for LOGCLSTE
Relate:LOGCLSTE      &RelationManager,THREAD               ! RelationManager for LOGCLSTE
Access:STATCRIT      &FileManager,THREAD                   ! FileManager for STATCRIT
Relate:STATCRIT      &RelationManager,THREAD               ! RelationManager for STATCRIT
Access:NETWORKS      &FileManager,THREAD                   ! FileManager for NETWORKS
Relate:NETWORKS      &RelationManager,THREAD               ! RelationManager for NETWORKS
Access:LOGDESNO      &FileManager,THREAD                   ! FileManager for LOGDESNO
Relate:LOGDESNO      &RelationManager,THREAD               ! RelationManager for LOGDESNO
Access:CPNDPRTS      &FileManager,THREAD                   ! FileManager for CPNDPRTS
Relate:CPNDPRTS      &RelationManager,THREAD               ! RelationManager for CPNDPRTS
Access:LOGSALCD      &FileManager,THREAD                   ! FileManager for LOGSALCD
Relate:LOGSALCD      &RelationManager,THREAD               ! RelationManager for LOGSALCD
Access:DEFSTOCK      &FileManager,THREAD                   ! FileManager for DEFSTOCK
Relate:DEFSTOCK      &RelationManager,THREAD               ! RelationManager for DEFSTOCK
Access:JOBLOHIS      &FileManager,THREAD                   ! FileManager for JOBLOHIS
Relate:JOBLOHIS      &RelationManager,THREAD               ! RelationManager for JOBLOHIS
Access:LOAN          &FileManager,THREAD                   ! FileManager for LOAN
Relate:LOAN          &RelationManager,THREAD               ! RelationManager for LOAN
Access:NOTESCON      &FileManager,THREAD                   ! FileManager for NOTESCON
Relate:NOTESCON      &RelationManager,THREAD               ! RelationManager for NOTESCON
Access:VATCODE       &FileManager,THREAD                   ! FileManager for VATCODE
Relate:VATCODE       &RelationManager,THREAD               ! RelationManager for VATCODE
Access:LOANHIST      &FileManager,THREAD                   ! FileManager for LOANHIST
Relate:LOANHIST      &RelationManager,THREAD               ! RelationManager for LOANHIST
Access:TURNARND      &FileManager,THREAD                   ! FileManager for TURNARND
Relate:TURNARND      &RelationManager,THREAD               ! RelationManager for TURNARND
Access:ORDJOBS       &FileManager,THREAD                   ! FileManager for ORDJOBS
Relate:ORDJOBS       &RelationManager,THREAD               ! RelationManager for ORDJOBS
Access:MERGETXT      &FileManager,THREAD                   ! FileManager for MERGETXT
Relate:MERGETXT      &RelationManager,THREAD               ! RelationManager for MERGETXT
Access:MERGELET      &FileManager,THREAD                   ! FileManager for MERGELET
Relate:MERGELET      &RelationManager,THREAD               ! RelationManager for MERGELET
Access:ORDSTOCK      &FileManager,THREAD                   ! FileManager for ORDSTOCK
Relate:ORDSTOCK      &RelationManager,THREAD               ! RelationManager for ORDSTOCK
Access:EXCHHIST      &FileManager,THREAD                   ! FileManager for EXCHHIST
Relate:EXCHHIST      &RelationManager,THREAD               ! RelationManager for EXCHHIST
Access:ACCESSOR      &FileManager,THREAD                   ! FileManager for ACCESSOR
Relate:ACCESSOR      &RelationManager,THREAD               ! RelationManager for ACCESSOR
Access:SUBACCAD      &FileManager,THREAD                   ! FileManager for SUBACCAD
Relate:SUBACCAD      &RelationManager,THREAD               ! RelationManager for SUBACCAD
Access:NOTESDEL      &FileManager,THREAD                   ! FileManager for NOTESDEL
Relate:NOTESDEL      &RelationManager,THREAD               ! RelationManager for NOTESDEL
Access:PICKDET       &FileManager,THREAD                   ! FileManager for PICKDET
Relate:PICKDET       &RelationManager,THREAD               ! RelationManager for PICKDET
Access:WARPARTS      &FileManager,THREAD                   ! FileManager for WARPARTS
Relate:WARPARTS      &RelationManager,THREAD               ! RelationManager for WARPARTS
Access:PARTS         &FileManager,THREAD                   ! FileManager for PARTS
Relate:PARTS         &RelationManager,THREAD               ! RelationManager for PARTS
Access:JOBACC        &FileManager,THREAD                   ! FileManager for JOBACC
Relate:JOBACC        &RelationManager,THREAD               ! RelationManager for JOBACC
Access:USELEVEL      &FileManager,THREAD                   ! FileManager for USELEVEL
Relate:USELEVEL      &RelationManager,THREAD               ! RelationManager for USELEVEL
Access:ALLLEVEL      &FileManager,THREAD                   ! FileManager for ALLLEVEL
Relate:ALLLEVEL      &RelationManager,THREAD               ! RelationManager for ALLLEVEL
Access:QAPARTSTEMP   &FileManager,THREAD                   ! FileManager for QAPARTSTEMP
Relate:QAPARTSTEMP   &RelationManager,THREAD               ! RelationManager for QAPARTSTEMP
Access:ACCAREAS      &FileManager,THREAD                   ! FileManager for ACCAREAS
Relate:ACCAREAS      &RelationManager,THREAD               ! RelationManager for ACCAREAS
Access:AUDIT         &FileManager,THREAD                   ! FileManager for AUDIT
Relate:AUDIT         &RelationManager,THREAD               ! RelationManager for AUDIT
Access:USERS         &FileManager,THREAD                   ! FileManager for USERS
Relate:USERS         &RelationManager,THREAD               ! RelationManager for USERS
Access:LOCSHELF      &FileManager,THREAD                   ! FileManager for LOCSHELF
Relate:LOCSHELF      &RelationManager,THREAD               ! RelationManager for LOCSHELF
Access:DEFAULTS      &FileManager,THREAD                   ! FileManager for DEFAULTS
Relate:DEFAULTS      &RelationManager,THREAD               ! RelationManager for DEFAULTS
Access:REPTYCAT      &FileManager,THREAD                   ! FileManager for REPTYCAT
Relate:REPTYCAT      &RelationManager,THREAD               ! RelationManager for REPTYCAT
Access:REPTYPETEMP   &FileManager,THREAD                   ! FileManager for REPTYPETEMP
Relate:REPTYPETEMP   &RelationManager,THREAD               ! RelationManager for REPTYPETEMP
Access:MANFPALO      &FileManager,THREAD                   ! FileManager for MANFPALO
Relate:MANFPALO      &RelationManager,THREAD               ! RelationManager for MANFPALO
Access:JOBSTAGE      &FileManager,THREAD                   ! FileManager for JOBSTAGE
Relate:JOBSTAGE      &RelationManager,THREAD               ! RelationManager for JOBSTAGE
Access:MANFAUPA      &FileManager,THREAD                   ! FileManager for MANFAUPA
Relate:MANFAUPA      &RelationManager,THREAD               ! RelationManager for MANFAUPA
Access:EXPPARTS      &FileManager,THREAD                   ! FileManager for EXPPARTS
Relate:EXPPARTS      &RelationManager,THREAD               ! RelationManager for EXPPARTS
Access:REPEXTRP      &FileManager,THREAD                   ! FileManager for REPEXTRP
Relate:REPEXTRP      &RelationManager,THREAD               ! RelationManager for REPEXTRP
Access:GENSHORT      &FileManager,THREAD                   ! FileManager for GENSHORT
Relate:GENSHORT      &RelationManager,THREAD               ! RelationManager for GENSHORT
Access:LOCINTER      &FileManager,THREAD                   ! FileManager for LOCINTER
Relate:LOCINTER      &RelationManager,THREAD               ! RelationManager for LOCINTER
Access:STAHEAD       &FileManager,THREAD                   ! FileManager for STAHEAD
Relate:STAHEAD       &RelationManager,THREAD               ! RelationManager for STAHEAD
Access:NOTESINV      &FileManager,THREAD                   ! FileManager for NOTESINV
Relate:NOTESINV      &RelationManager,THREAD               ! RelationManager for NOTESINV
Access:NOTESFAU      &FileManager,THREAD                   ! FileManager for NOTESFAU
Relate:NOTESFAU      &RelationManager,THREAD               ! RelationManager for NOTESFAU
Access:MANFAULO      &FileManager,THREAD                   ! FileManager for MANFAULO
Relate:MANFAULO      &RelationManager,THREAD               ! RelationManager for MANFAULO
Access:STATREP       &FileManager,THREAD                   ! FileManager for STATREP
Relate:STATREP       &RelationManager,THREAD               ! RelationManager for STATREP
Access:JOBEXHIS      &FileManager,THREAD                   ! FileManager for JOBEXHIS
Relate:JOBEXHIS      &RelationManager,THREAD               ! RelationManager for JOBEXHIS
Access:STARECIP      &FileManager,THREAD                   ! FileManager for STARECIP
Relate:STARECIP      &RelationManager,THREAD               ! RelationManager for STARECIP
Access:RETSTOCK      &FileManager,THREAD                   ! FileManager for RETSTOCK
Relate:RETSTOCK      &RelationManager,THREAD               ! RelationManager for RETSTOCK
Access:RETPAY        &FileManager,THREAD                   ! FileManager for RETPAY
Relate:RETPAY        &RelationManager,THREAD               ! RelationManager for RETPAY
Access:RETDESNO      &FileManager,THREAD                   ! FileManager for RETDESNO
Relate:RETDESNO      &RelationManager,THREAD               ! RelationManager for RETDESNO
Access:ORDPARTS      &FileManager,THREAD                   ! FileManager for ORDPARTS
Relate:ORDPARTS      &RelationManager,THREAD               ! RelationManager for ORDPARTS
Access:STOCKMIN      &FileManager,THREAD                   ! FileManager for STOCKMIN
Relate:STOCKMIN      &RelationManager,THREAD               ! RelationManager for STOCKMIN
Access:LOCATION      &FileManager,THREAD                   ! FileManager for LOCATION
Relate:LOCATION      &RelationManager,THREAD               ! RelationManager for LOCATION
Access:WPARTTMP      &FileManager,THREAD                   ! FileManager for WPARTTMP
Relate:WPARTTMP      &RelationManager,THREAD               ! RelationManager for WPARTTMP
Access:PARTSTMP      &FileManager,THREAD                   ! FileManager for PARTSTMP
Relate:PARTSTMP      &RelationManager,THREAD               ! RelationManager for PARTSTMP
Access:EXCHANGE      &FileManager,THREAD                   ! FileManager for EXCHANGE
Relate:EXCHANGE      &RelationManager,THREAD               ! RelationManager for EXCHANGE
Access:STOESN        &FileManager,THREAD                   ! FileManager for STOESN
Relate:STOESN        &RelationManager,THREAD               ! RelationManager for STOESN
Access:LOANACC       &FileManager,THREAD                   ! FileManager for LOANACC
Relate:LOANACC       &RelationManager,THREAD               ! RelationManager for LOANACC
Access:SUPPLIER      &FileManager,THREAD                   ! FileManager for SUPPLIER
Relate:SUPPLIER      &RelationManager,THREAD               ! RelationManager for SUPPLIER
Access:EXCHACC       &FileManager,THREAD                   ! FileManager for EXCHACC
Relate:EXCHACC       &RelationManager,THREAD               ! RelationManager for EXCHACC
Access:RECIPTYP      &FileManager,THREAD                   ! FileManager for RECIPTYP
Relate:RECIPTYP      &RelationManager,THREAD               ! RelationManager for RECIPTYP
Access:SUPVALA       &FileManager,THREAD                   ! FileManager for SUPVALA
Relate:SUPVALA       &RelationManager,THREAD               ! RelationManager for SUPVALA
Access:ESTPARTS      &FileManager,THREAD                   ! FileManager for ESTPARTS
Relate:ESTPARTS      &RelationManager,THREAD               ! RelationManager for ESTPARTS
Access:TRAEMAIL      &FileManager,THREAD                   ! FileManager for TRAEMAIL
Relate:TRAEMAIL      &RelationManager,THREAD               ! RelationManager for TRAEMAIL
Access:SUPVALB       &FileManager,THREAD                   ! FileManager for SUPVALB
Relate:SUPVALB       &RelationManager,THREAD               ! RelationManager for SUPVALB
Access:JOBS          &FileManager,THREAD                   ! FileManager for JOBS
Relate:JOBS          &RelationManager,THREAD               ! RelationManager for JOBS
Access:MODELNUM      &FileManager,THREAD                   ! FileManager for MODELNUM
Relate:MODELNUM      &RelationManager,THREAD               ! RelationManager for MODELNUM
Access:SUBEMAIL      &FileManager,THREAD                   ! FileManager for SUBEMAIL
Relate:SUBEMAIL      &RelationManager,THREAD               ! RelationManager for SUBEMAIL
Access:SUPPTEMP      &FileManager,THREAD                   ! FileManager for SUPPTEMP
Relate:SUPPTEMP      &RelationManager,THREAD               ! RelationManager for SUPPTEMP
Access:ORDERS        &FileManager,THREAD                   ! FileManager for ORDERS
Relate:ORDERS        &RelationManager,THREAD               ! RelationManager for ORDERS
Access:UNITTYPE      &FileManager,THREAD                   ! FileManager for UNITTYPE
Relate:UNITTYPE      &RelationManager,THREAD               ! RelationManager for UNITTYPE
Access:STATUS        &FileManager,THREAD                   ! FileManager for STATUS
Relate:STATUS        &RelationManager,THREAD               ! RelationManager for STATUS
Access:REPAIRTY      &FileManager,THREAD                   ! FileManager for REPAIRTY
Relate:REPAIRTY      &RelationManager,THREAD               ! RelationManager for REPAIRTY
Access:TRADETMP      &FileManager,THREAD                   ! FileManager for TRADETMP
Relate:TRADETMP      &RelationManager,THREAD               ! RelationManager for TRADETMP
Access:EXCCHRGE      &FileManager,THREAD                   ! FileManager for EXCCHRGE
Relate:EXCCHRGE      &RelationManager,THREAD               ! RelationManager for EXCCHRGE
Access:INVOICE       &FileManager,THREAD                   ! FileManager for INVOICE
Relate:INVOICE       &RelationManager,THREAD               ! RelationManager for INVOICE
Access:TRDBATCH      &FileManager,THREAD                   ! FileManager for TRDBATCH
Relate:TRDBATCH      &RelationManager,THREAD               ! RelationManager for TRDBATCH
Access:TRACHRGE      &FileManager,THREAD                   ! FileManager for TRACHRGE
Relate:TRACHRGE      &RelationManager,THREAD               ! RelationManager for TRACHRGE
Access:TRDMODEL      &FileManager,THREAD                   ! FileManager for TRDMODEL
Relate:TRDMODEL      &RelationManager,THREAD               ! RelationManager for TRDMODEL
Access:STOMODEL      &FileManager,THREAD                   ! FileManager for STOMODEL
Relate:STOMODEL      &RelationManager,THREAD               ! RelationManager for STOMODEL
Access:TRADEACC      &FileManager,THREAD                   ! FileManager for TRADEACC
Relate:TRADEACC      &RelationManager,THREAD               ! RelationManager for TRADEACC
Access:QUERYREA      &FileManager,THREAD                   ! FileManager for QUERYREA
Relate:QUERYREA      &RelationManager,THREAD               ! RelationManager for QUERYREA
Access:SUBCHRGE      &FileManager,THREAD                   ! FileManager for SUBCHRGE
Relate:SUBCHRGE      &RelationManager,THREAD               ! RelationManager for SUBCHRGE
Access:DEFCHRGE      &FileManager,THREAD                   ! FileManager for DEFCHRGE
Relate:DEFCHRGE      &RelationManager,THREAD               ! RelationManager for DEFCHRGE
Access:SUBTRACC      &FileManager,THREAD                   ! FileManager for SUBTRACC
Relate:SUBTRACC      &RelationManager,THREAD               ! RelationManager for SUBTRACC
Access:CHARTYPE      &FileManager,THREAD                   ! FileManager for CHARTYPE
Relate:CHARTYPE      &RelationManager,THREAD               ! RelationManager for CHARTYPE
Access:MANUFACT      &FileManager,THREAD                   ! FileManager for MANUFACT
Relate:MANUFACT      &RelationManager,THREAD               ! RelationManager for MANUFACT
Access:USMASSIG      &FileManager,THREAD                   ! FileManager for USMASSIG
Relate:USMASSIG      &RelationManager,THREAD               ! RelationManager for USMASSIG
Access:EXPWARPARTS   &FileManager,THREAD                   ! FileManager for EXPWARPARTS
Relate:EXPWARPARTS   &RelationManager,THREAD               ! RelationManager for EXPWARPARTS
Access:USUASSIG      &FileManager,THREAD                   ! FileManager for USUASSIG
Relate:USUASSIG      &RelationManager,THREAD               ! RelationManager for USUASSIG
Access:XMLTRADE      &FileManager,THREAD                   ! FileManager for XMLTRADE
Relate:XMLTRADE      &RelationManager,THREAD               ! RelationManager for XMLTRADE
Access:XMLJOBQ       &FileManager,THREAD                   ! FileManager for XMLJOBQ
Relate:XMLJOBQ       &RelationManager,THREAD               ! RelationManager for XMLJOBQ
Access:XMLDEFS       &FileManager,THREAD                   ! FileManager for XMLDEFS
Relate:XMLDEFS       &RelationManager,THREAD               ! RelationManager for XMLDEFS
Access:XMLSB         &FileManager,THREAD                   ! FileManager for XMLSB
Relate:XMLSB         &RelationManager,THREAD               ! RelationManager for XMLSB
Access:XMLSAM        &FileManager,THREAD                   ! FileManager for XMLSAM
Relate:XMLSAM        &RelationManager,THREAD               ! RelationManager for XMLSAM
Access:XMLDOCS       &FileManager,THREAD                   ! FileManager for XMLDOCS
Relate:XMLDOCS       &RelationManager,THREAD               ! RelationManager for XMLDOCS
Access:DEFEDI        &FileManager,THREAD                   ! FileManager for DEFEDI
Relate:DEFEDI        &RelationManager,THREAD               ! RelationManager for DEFEDI
Access:XMLJOBS_ALIAS &FileManager,THREAD                   ! FileManager for XMLJOBS_ALIAS
Relate:XMLJOBS_ALIAS &RelationManager,THREAD               ! RelationManager for XMLJOBS_ALIAS
Access:AUDIT_ALIAS   &FileManager,THREAD                   ! FileManager for AUDIT_ALIAS
Relate:AUDIT_ALIAS   &RelationManager,THREAD               ! RelationManager for AUDIT_ALIAS
Access:XMLSAM_ALIAS  &FileManager,THREAD                   ! FileManager for XMLSAM_ALIAS
Relate:XMLSAM_ALIAS  &RelationManager,THREAD               ! RelationManager for XMLSAM_ALIAS
Access:MULDESP_ALIAS &FileManager,THREAD                   ! FileManager for MULDESP_ALIAS
Relate:MULDESP_ALIAS &RelationManager,THREAD               ! RelationManager for MULDESP_ALIAS
Access:MULDESPJ_ALIAS &FileManager,THREAD                  ! FileManager for MULDESPJ_ALIAS
Relate:MULDESPJ_ALIAS &RelationManager,THREAD              ! RelationManager for MULDESPJ_ALIAS
Access:SUBCONTALIAS  &FileManager,THREAD                   ! FileManager for SUBCONTALIAS
Relate:SUBCONTALIAS  &RelationManager,THREAD               ! RelationManager for SUBCONTALIAS
Access:INSJOBSALIAS  &FileManager,THREAD                   ! FileManager for INSJOBSALIAS
Relate:INSJOBSALIAS  &RelationManager,THREAD               ! RelationManager for INSJOBSALIAS
Access:ESTPARTS_ALIAS &FileManager,THREAD                  ! FileManager for ESTPARTS_ALIAS
Relate:ESTPARTS_ALIAS &RelationManager,THREAD              ! RelationManager for ESTPARTS_ALIAS
Access:IEQUIP_ALIAS  &FileManager,THREAD                   ! FileManager for IEQUIP_ALIAS
Relate:IEQUIP_ALIAS  &RelationManager,THREAD               ! RelationManager for IEQUIP_ALIAS
Access:JOBS2_ALIAS   &FileManager,THREAD                   ! FileManager for JOBS2_ALIAS
Relate:JOBS2_ALIAS   &RelationManager,THREAD               ! RelationManager for JOBS2_ALIAS
Access:LOGASSST_ALIAS &FileManager,THREAD                  ! FileManager for LOGASSST_ALIAS
Relate:LOGASSST_ALIAS &RelationManager,THREAD              ! RelationManager for LOGASSST_ALIAS
Access:LOGSTOCK_ALIAS &FileManager,THREAD                  ! FileManager for LOGSTOCK_ALIAS
Relate:LOGSTOCK_ALIAS &RelationManager,THREAD              ! RelationManager for LOGSTOCK_ALIAS
Access:LOGSERST_ALIAS &FileManager,THREAD                  ! FileManager for LOGSERST_ALIAS
Relate:LOGSERST_ALIAS &RelationManager,THREAD              ! RelationManager for LOGSERST_ALIAS
Access:COURIER_ALIAS &FileManager,THREAD                   ! FileManager for COURIER_ALIAS
Relate:COURIER_ALIAS &RelationManager,THREAD               ! RelationManager for COURIER_ALIAS
Access:JOBNOTES_ALIAS &FileManager,THREAD                  ! FileManager for JOBNOTES_ALIAS
Relate:JOBNOTES_ALIAS &RelationManager,THREAD              ! RelationManager for JOBNOTES_ALIAS
Access:TRDBATCH_ALIAS &FileManager,THREAD                  ! FileManager for TRDBATCH_ALIAS
Relate:TRDBATCH_ALIAS &RelationManager,THREAD              ! RelationManager for TRDBATCH_ALIAS
Access:INVOICE_ALIAS &FileManager,THREAD                   ! FileManager for INVOICE_ALIAS
Relate:INVOICE_ALIAS &RelationManager,THREAD               ! RelationManager for INVOICE_ALIAS
Access:ORDPEND_ALIAS &FileManager,THREAD                   ! FileManager for ORDPEND_ALIAS
Relate:ORDPEND_ALIAS &RelationManager,THREAD               ! RelationManager for ORDPEND_ALIAS
Access:RETSALES_ALIAS &FileManager,THREAD                  ! FileManager for RETSALES_ALIAS
Relate:RETSALES_ALIAS &RelationManager,THREAD              ! RelationManager for RETSALES_ALIAS
Access:RETSTOCK_ALIAS &FileManager,THREAD                  ! FileManager for RETSTOCK_ALIAS
Relate:RETSTOCK_ALIAS &RelationManager,THREAD              ! RelationManager for RETSTOCK_ALIAS
Access:LOCATION_ALIAS &FileManager,THREAD                  ! FileManager for LOCATION_ALIAS
Relate:LOCATION_ALIAS &RelationManager,THREAD              ! RelationManager for LOCATION_ALIAS
Access:COMMONFA_ALIAS &FileManager,THREAD                  ! FileManager for COMMONFA_ALIAS
Relate:COMMONFA_ALIAS &RelationManager,THREAD              ! RelationManager for COMMONFA_ALIAS
Access:COMMONCP_ALIAS &FileManager,THREAD                  ! FileManager for COMMONCP_ALIAS
Relate:COMMONCP_ALIAS &RelationManager,THREAD              ! RelationManager for COMMONCP_ALIAS
Access:COMMONWP_ALIAS &FileManager,THREAD                  ! FileManager for COMMONWP_ALIAS
Relate:COMMONWP_ALIAS &RelationManager,THREAD              ! RelationManager for COMMONWP_ALIAS
Access:STOMODEL_ALIAS &FileManager,THREAD                  ! FileManager for STOMODEL_ALIAS
Relate:STOMODEL_ALIAS &RelationManager,THREAD              ! RelationManager for STOMODEL_ALIAS
Access:JOBPAYMT_ALIAS &FileManager,THREAD                  ! FileManager for JOBPAYMT_ALIAS
Relate:JOBPAYMT_ALIAS &RelationManager,THREAD              ! RelationManager for JOBPAYMT_ALIAS
Access:PARTSTMP_ALIAS &FileManager,THREAD                  ! FileManager for PARTSTMP_ALIAS
Relate:PARTSTMP_ALIAS &RelationManager,THREAD              ! RelationManager for PARTSTMP_ALIAS
Access:WPARTTMP_ALIAS &FileManager,THREAD                  ! FileManager for WPARTTMP_ALIAS
Relate:WPARTTMP_ALIAS &RelationManager,THREAD              ! RelationManager for WPARTTMP_ALIAS
Access:LOAN_ALIAS    &FileManager,THREAD                   ! FileManager for LOAN_ALIAS
Relate:LOAN_ALIAS    &RelationManager,THREAD               ! RelationManager for LOAN_ALIAS
Access:EXCHANGE_ALIAS &FileManager,THREAD                  ! FileManager for EXCHANGE_ALIAS
Relate:EXCHANGE_ALIAS &RelationManager,THREAD              ! RelationManager for EXCHANGE_ALIAS
Access:ACCAREAS_ALIAS &FileManager,THREAD                  ! FileManager for ACCAREAS_ALIAS
Relate:ACCAREAS_ALIAS &RelationManager,THREAD              ! RelationManager for ACCAREAS_ALIAS
Access:USERS_ALIAS   &FileManager,THREAD                   ! FileManager for USERS_ALIAS
Relate:USERS_ALIAS   &RelationManager,THREAD               ! RelationManager for USERS_ALIAS
Access:WARPARTS_ALIAS &FileManager,THREAD                  ! FileManager for WARPARTS_ALIAS
Relate:WARPARTS_ALIAS &RelationManager,THREAD              ! RelationManager for WARPARTS_ALIAS
Access:ORDPARTS_ALIAS &FileManager,THREAD                  ! FileManager for ORDPARTS_ALIAS
Relate:ORDPARTS_ALIAS &RelationManager,THREAD              ! RelationManager for ORDPARTS_ALIAS
Access:PARTS_ALIAS   &FileManager,THREAD                   ! FileManager for PARTS_ALIAS
Relate:PARTS_ALIAS   &RelationManager,THREAD               ! RelationManager for PARTS_ALIAS
Access:STOCK_ALIAS   &FileManager,THREAD                   ! FileManager for STOCK_ALIAS
Relate:STOCK_ALIAS   &RelationManager,THREAD               ! RelationManager for STOCK_ALIAS
Access:JOBS_ALIAS    &FileManager,THREAD                   ! FileManager for JOBS_ALIAS
Relate:JOBS_ALIAS    &RelationManager,THREAD               ! RelationManager for JOBS_ALIAS
Access:XMLTRADE_ALIAS &FileManager,THREAD                  ! FileManager for XMLTRADE_ALIAS
Relate:XMLTRADE_ALIAS &RelationManager,THREAD              ! RelationManager for XMLTRADE_ALIAS
Access:XMLSB_ALIAS   &FileManager,THREAD                   ! FileManager for XMLSB_ALIAS
Relate:XMLSB_ALIAS   &RelationManager,THREAD               ! RelationManager for XMLSB_ALIAS
Access:STDCHRGE_ALIAS &FileManager,THREAD                  ! FileManager for STDCHRGE_ALIAS
Relate:STDCHRGE_ALIAS &RelationManager,THREAD              ! RelationManager for STDCHRGE_ALIAS

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrorStatus    ErrorStatusClass,THREAD
GlobalErrors         ErrorClass                            ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END

lCurrentFDSetting    LONG                                  ! Used by window frame dragging
lAdjFDSetting        LONG                                  ! ditto

  CODE
  GlobalErrors.Init(GlobalErrorStatus)
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('DataDelete.INI', NVD_INI)                   ! Configure INIManager to use INI file
  DctInit
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)       ! Configure frame dragging
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher
    

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


Dictionary.Construct PROCEDURE

  CODE
  IF THREAD()<>1
     DctInit()
  END


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

