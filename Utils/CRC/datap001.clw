

   MEMBER('datapurge.clw')                            ! This is a MEMBER module

                     MAP
                       INCLUDE('DATAP001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
tmp:StartDate        DATE
tmp:StartTime        TIME
Prog        Class
ProgressSetup      Procedure(Long func:Records)
ResetProgress      Procedure(Long func:Records)
InsideLoop         Procedure(),Byte
ProgressText       Procedure(String func:String)
ProgressFinish     Procedure()
            End
! moving bar window
prog:RecordsToProcess   Long,Auto
prog:RecordsPerCycle    Long,Auto
prog:RecordsProcessed   Long,Auto
prog:RecordsThisCycle   Long,Auto
prog:PercentProgress    Byte
prog:Cancel             Byte
prog:ProgressThermometer    Byte


prog:thermometer byte
prog:SkipRecords        Long
omit('***',ClarionetUsed=0)
CNprog:thermometer byte
CNprog:Text         String(60)
CNprog:pctText      String(60)
***z
progwindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(prog:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?prog:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?prog:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),LEFT,ICON('cancel.gif')
     END

omit('***',ClarionetUsed=0)

!static webjob window
CN:progwindow WINDOW('Working...'),AT(,,164,41),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,GRAY,DOUBLE
       STRING(@s60),AT(4,2,156,10),USE(cnprog:Text),CENTER
       PROMPT('Working, please wait...'),AT(8,16),USE(?cnprog:Prompt),FONT(,14,,FONT:bold)
     END
***
  CODE
   Relate:AUDIT.Open
   Relate:AUDSTATS.Open
   Relate:AUDSTATS1.Open
   Relate:CONTHIST.Open
   Relate:CONTHIST1.Open
   Relate:ESTPARTS.Open
   Relate:ESTPARTS1.Open
   Relate:JOBACC.Open
   Relate:JOBACC1.Open
   Relate:JOBEXHIS.Open
   Relate:JOBEXHIS1.Open
   Relate:JOBLOHIS.Open
   Relate:JOBLOHIS1.Open
   Relate:JOBNOTES.Open
   Relate:JOBNOTES1.Open
   Relate:JOBS.Open
   Relate:JOBS1.Open
   Relate:JOBSE.Open
   Relate:JOBSE1.Open
   Relate:JOBSENG.Open
   Relate:JOBSENG1.Open
   Relate:JOBSTAGE.Open
   Relate:JOBSTAGE1.Open
   Relate:JOBTHIRD.Open
   Relate:JOBTHIRD1.Open
   Relate:PARTS.Open
   Relate:PARTS1.Open
   Relate:WARPARTS.Open
   Relate:AUDIT1.Open
   Relate:WARPARTS1.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Message('Click ''OK'' to begin.', |
            'Data Purge', Icon:Asterisk, |
             Button:OK+Button:Cancel, Button:Cancel, 0)
    Of Button:OK
        tmp:StartDate = Today()
        tmp:StartTime = Clock()
    Of Button:Cancel
        Return
    End !CASE

    OK# = 0
    FAIL# = 0
    BREAK# = 0

    Prog.ProgressSetup(RECORDS(JOBS))

    Access:TRADEACC.ClearKey(tra:RecordNumberKey)
    tra:RecordNumber = 0
    Set(tra:RecordNumberKey,tra:RecordNumberKey)
    Loop
        If Access:TRADEACC.NEXT()
           Break
        End !If
        If tra:Account_Number <> 'VR (HEAD)' And tra:Account_Number <> 'VODA CONNECT'
            Cycle
        End ! If tra:Account_Number <> 'VR (HEAD)' And tra:Account_Number <> 'VODA CONNECT'
        Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
        sub:Main_Account_Number = tra:Account_Number
        Set(sub:Main_Account_Key,sub:Main_Account_Key)
        Loop
            If Access:SUBTRACC.NEXT()
               Break
            End !If
            If sub:Main_Account_Number <> tra:Account_Number      |
                Then Break.  ! End If
            Access:JOBS1.ClearKey(job1:AccountNumberKey)
            job1:Account_Number = sub:Account_Number
            Set(job1:AccountNumberKey,job1:AccountNumberKey)
            Loop
                If Access:JOBS1.NEXT()
                   Break
                End !If
                If job1:Account_Number <> sub:Account_Number      |
                    Then Break.  ! End If
                If Prog.InsideLoop()
                    BREAK# = 1
                    Break
                End ! If Prog.InsideLoop()
                If Access:JOBS.PrimeRecord() = Level:Benign
                    job:Record :=: job1:Record
                    If Access:JOBS.TryInsert() = Level:Benign
                        ! Insert Successful
                        OK# += 1
                        Prog.ProgressText('Jobs: ' & OK# & '. Failed: ' & FAIL#)
                    Else ! If Access:JOBS.TryInsert() = Level:Benign
                        ! Insert Failed
                        FAIL# += 1
                        Prog.ProgressText('Jobs: ' & OK# & '. Failed: ' & FAIL#)
                        Cycle
                        Access:JOBS.CancelAutoInc()
                    End ! If Access:JOBS.TryInsert() = Level:Benign
                End !If Access:JOBS.PrimeRecord() = Level:Benign

                Access:AUDIT1.ClearKey(aud1:Ref_Number_Key)
                aud1:Ref_Number = job1:Ref_Number
                Set(aud1:Ref_Number_Key,aud1:Ref_Number_Key)
                Loop
                    If Access:AUDIT1.NEXT()
                       Break
                    End !If
                    If aud1:Ref_Number <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:AUDIT.PrimeRecord() = Level:Benign
                        aud:Record :=: aud1:Record
                        If Access:AUDIT.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:AUDIT.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:AUDIT.CancelAutoInc()
                        End ! If Access:AUDIT.TryInsert() = Level:Benign
                    End !If Access:AUDIT.PrimeRecord() = Level:Benign
                End !Loop

                Access:AUDSTATS1.ClearKey(aus1:DateChangedKey)
                aus1:RefNumber   = job1:Ref_Number
                Set(aus1:DateChangedKey,aus1:DateChangedKey)
                Loop
                    If Access:AUDSTATS1.NEXT()
                       Break
                    End !If
                    If aus1:RefNumber   <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:AUDSTATS.PrimeRecord() = Level:Benign
                        aus:Record :=: aus1:Record
                        If Access:AUDSTATS.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:AUDSTATS.CancelAutoInc()
                        End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                    End !If Access:AUDSTATS.PrimeRecord() = Level:Benign
                End !Loop

                Access:CONTHIST1.ClearKey(cht1:Ref_Number_Key)
                cht1:Ref_Number = job1:Ref_Number
                Set(cht1:Ref_Number_Key,cht1:Ref_Number_Key)
                Loop
                    If Access:CONTHIST1.NEXT()
                       Break
                    End !If
                    If cht1:Ref_Number <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:CONTHIST.PrimeRecord() = Level:Benign
                        cht:Record :=: cht1:Record
                        If Access:CONTHIST.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:CONTHIST.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:CONTHIST.CancelAutoInc()
                        End ! If Access:CONTHIST.TryInsert() = Level:Benign
                    End !If Access:CONTHIST.PrimeRecord() = Level:Benign
                End !Loop

                Access:ESTPARTS1.ClearKey(epr1:Part_Number_Key)
                epr1:Ref_Number  = job1:Ref_Number
                Set(epr1:Part_Number_Key,epr1:Part_Number_Key)
                Loop
                    If Access:ESTPARTS1.NEXT()
                       Break
                    End !If
                    If epr1:Ref_Number  <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:ESTPARTS.PrimeRecord() = Level:Benign
                        epr:Record :=: epr1:Record
                        If Access:ESTPARTS.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:ESTPARTS.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:ESTPARTS.CancelAutoInc()
                        End ! If Access:ESTPARTS.TryInsert() = Level:Benign
                    End !If Access:ESTPARTS.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBACC1.ClearKey(jac1:Ref_Number_Key)
                jac1:Ref_Number = job1:Ref_Number
                Set(jac1:Ref_Number_Key,jac1:Ref_Number_Key)
                Loop
                    If Access:JOBACC1.NEXT()
                       Break
                    End !If
                    If jac1:Ref_Number <> job1:Ref_Number      |
                        Then Break.  ! End If

                    If Access:JOBACC.PrimeRecord() = Level:Benign
                        jac:Record :=: jac1:Record
                        If Access:JOBACC.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBACC.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBACC.CancelAutoInc()
                        End ! If Access:JOBACC.TryInsert() = Level:Benign
                    End !If Access:JOBACC.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBEXHIS1.ClearKey(jxh1:Ref_Number_Key)
                jxh1:Ref_Number = job1:Ref_Number
                Set(jxh1:Ref_Number_Key,jxh1:Ref_Number_Key)
                Loop
                    If Access:JOBEXHIS1.NEXT()
                       Break
                    End !If
                    If jxh1:Ref_Number <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:JOBEXHIS1.PrimeRecord() = Level:Benign
                        jxh:Record :=: jxh1:Record
                        If Access:JOBEXHIS1.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBEXHIS1.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBEXHIS1.CancelAutoInc()
                        End ! If Access:JOBEXHIS1.TryInsert() = Level:Benign
                    End !If Access:JOBEXHIS1.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBLOHIS1.ClearKey(jlh1:Ref_Number_Key)
                jlh1:Ref_Number = job1:Ref_Number
                Set(jlh1:Ref_Number_Key,jlh1:Ref_Number_Key)
                Loop
                    If Access:JOBLOHIS1.NEXT()
                       Break
                    End !If
                    If jlh1:Ref_Number <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:JOBLOHIS.PrimeRecord() = Level:Benign
                        jlh:Record :=: jlh1:Record
                        If Access:JOBLOHIS.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBLOHIS.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBLOHIS.CancelAutoInc()
                        End ! If Access:JOBLOHIS.TryInsert() = Level:Benign
                    End !If Access:JOBLOHIS.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBNOTES1.ClearKey(jbn1:RefNumberKey)
                jbn1:RefNumber = job1:Ref_Number
                Set(jbn1:RefNumberKey,jbn1:RefNumberKey)
                Loop
                    If Access:JOBNOTES1.NEXT()
                       Break
                    End !If
                    If jbn1:RefNumber <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:JOBNOTES.PrimeRecord() = Level:Benign
                        jbn:Record :=: jbn1:Record
                        If Access:JOBNOTES.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBNOTES.CancelAutoInc()
                        End ! If Access:JOBNOTES.TryInsert() = Level:Benign
                    End !If Access:JOBNOTES.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBSE1.ClearKey(jobe1:RefNumberKey)
                jobe1:RefNumber = job1:Ref_Number
                Set(jobe1:RefNumberKey,jobe1:RefNumberKey)
                Loop
                    If Access:JOBSE1.NEXT()
                       Break
                    End !If
                    If jobe1:RefNumber <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:JOBSE.PrimeRecord() = Level:Benign
                        jobe:Record :=: jobe1:Record
                        If Access:JOBSE.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBSE.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBSE.CancelAutoInc()
                        End ! If Access:JOBSE.TryInsert() = Level:Benign
                    End !If Access:JOBSE.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBSENG1.ClearKey(joe1:UserCodeKey)
                joe1:JobNumber     = job1:Ref_Number
                Set(joe1:UserCodeKey,joe1:UserCodeKey)
                Loop
                    If Access:JOBSENG1.NEXT()
                       Break
                    End !If
                    If joe1:JobNumber     <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:JOBSENG.PrimeRecord() = Level:Benign
                        joe:Record :=: joe1:Record
                        If Access:JOBSENG.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBSENG.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBSENG.CancelAutoInc()
                        End ! If Access:JOBSENG.TryInsert() = Level:Benign
                    End !If Access:JOBSENG.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBSTAGE1.ClearKey(jst1:Ref_Number_Key)
                jst1:Ref_Number = job1:Ref_Number
                Set(jst1:Ref_Number_Key,jst1:Ref_Number_Key)
                Loop
                    If Access:JOBSTAGE1.NEXT()
                       Break
                    End !If
                    If jst1:Ref_Number <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                        jst:Record :=: jst1:Record
                        If Access:JOBSTAGE.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBSTAGE.CancelAutoInc()
                        End ! If Access:JOBSTAGE.TryInsert() = Level:Benign
                    End !If Access:JOBSTAGE.PrimeRecord() = Level:Benign
                End !Loop

                Access:JOBTHIRD1.ClearKey(jot1:RefNumberKey)
                jot1:RefNumber = job1:Ref_Number
                Set(jot1:RefNumberKey,jot1:RefNumberKey)
                Loop
                    If Access:JOBTHIRD1.NEXT()
                       Break
                    End !If
                    If jot1:RefNumber <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:JOBTHIRD.PrimeRecord() = Level:Benign
                        jot:Record :=: jot1:Record
                        If Access:JOBTHIRD.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:JOBTHIRD.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:JOBTHIRD.CancelAutoInc()
                        End ! If Access:JOBTHIRD.TryInsert() = Level:Benign
                    End !If Access:JOBTHIRD.PrimeRecord() = Level:Benign
                End !Loop

                Access:PARTS1.ClearKey(par1:Part_Number_Key)
                par1:Ref_Number  = job1:Ref_Number
                Set(par1:Part_Number_Key,par1:Part_Number_Key)
                Loop
                    If Access:PARTS1.NEXT()
                       Break
                    End !If
                    If par1:Ref_Number  <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:PARTS.PrimeRecord() = Level:Benign
                        par:Record :=: par1:Record
                        If Access:PARTS.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:PARTS.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:PARTS.CancelAutoInc()
                        End ! If Access:PARTS.TryInsert() = Level:Benign
                    End !If Access:PARTS.PrimeRecord() = Level:Benign
                End !Loop

                Access:WARPARTS1.ClearKey(wpr1:Part_Number_Key)
                wpr1:Ref_Number  = job1:Ref_Number
                Set(wpr1:Part_Number_Key,wpr1:Part_Number_Key)
                Loop
                    If Access:WARPARTS1.NEXT()
                       Break
                    End !If
                    If wpr1:Ref_Number  <> job1:Ref_Number      |
                        Then Break.  ! End If
                    If Access:WARPARTS.PrimeRecord() = Level:Benign
                        wpr:Record :=: wpr1:Record
                        If Access:WARPARTS.TryInsert() = Level:Benign
                            ! Insert Successful

                        Else ! If Access:WARPARTS.TryInsert() = Level:Benign
                            ! Insert Failed
                            Access:WARPARTS.CancelAutoInc()
                        End ! If Access:WARPARTS.TryInsert() = Level:Benign
                    End !If Access:WARPARTS.PrimeRecord() = Level:Benign
                End !Loop
            End !Loop
            If BREAK# = 1
                Break
            End ! If BREAK# = 1
        End !Loop
        If BREAK# = 1
            Break
        End ! If BREAK# = 1
    End !Loop

   Relate:AUDIT.Close
   Relate:AUDSTATS.Close
   Relate:AUDSTATS1.Close
   Relate:CONTHIST.Close
   Relate:CONTHIST1.Close
   Relate:ESTPARTS.Close
   Relate:ESTPARTS1.Close
   Relate:JOBACC.Close
   Relate:JOBACC1.Close
   Relate:JOBEXHIS.Close
   Relate:JOBEXHIS1.Close
   Relate:JOBLOHIS.Close
   Relate:JOBLOHIS1.Close
   Relate:JOBNOTES.Close
   Relate:JOBNOTES1.Close
   Relate:JOBS.Close
   Relate:JOBS1.Close
   Relate:JOBSE.Close
   Relate:JOBSE1.Close
   Relate:JOBSENG.Close
   Relate:JOBSENG1.Close
   Relate:JOBSTAGE.Close
   Relate:JOBSTAGE1.Close
   Relate:JOBTHIRD.Close
   Relate:JOBTHIRD1.Close
   Relate:PARTS.Close
   Relate:PARTS1.Close
   Relate:WARPARTS.Close
   Relate:AUDIT1.Close
   Relate:WARPARTS1.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
    If Break# <> 1
        Prog.ProgressText('Removing/Renaming Files')

        !Rename Files

        Remove('AUDIT.DAT')
        Rename('AUDIT1.DAT','AUDIT.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('AUDSTATS.DAT')
        Rename('AUDSTATS1.DAT','AUDSTATS.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('CONTHIST.DAT')
        Rename('CONTHIST1.DAT','CONTHIST.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('ESTPARTS.DAT')
        Rename('ESTPARTS1.DAT','ESTPARTS.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBACC.DAT')
        Rename('JOBACC1.DAT','JOBACC.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBEXHIS.DAT')
        Rename('JOBEXHIS1.DAT','JOBEXHIS.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBLOHIS.DAT')
        Rename('JOBLOHIS1.DAT','JOBLOHIS.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBNOTES.DAT')
        Rename('JOBNOTES.DAT1','JOBNOTES.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBS.DAT')
        Rename('JOBS1.DAT','JOBS.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBSE.DAT')
        Rename('JOBSE1.DAT','JOBSE.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBSENG.DAT')
        Rename('JOBSENG1.DAT','JOBSENG.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBSTAGE.DAT')
        Rename('JOBSTAGE1.DAT','JOBSTAGE.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('JOBTHIRD.DAT')
        Rename('JOBTHIRD1.DAT','JOBTHIRD.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('PARTS.DAT')
        Rename('PARTS1.DAT','PARTS.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()

        Remove('WARPARTS.DAT')
        Rename('WARPARTS1.DAT','WARPARTS.DAT')
        If Error()
            Stop(Error())
        End ! IF Error()
    End ! If Break# <> 1

    Prog.ProgressFinish()
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Message('Started: '&Format(tmp:StartDate,@d6)&' '&|
            Format(tmp:StartTime,@t1)&|
            '||Finished: '&Format(Today(),@d6)&' '&Format(Clock(),@t1) &|
            '||Jobs Created: ' & OK# &|
            '||Failed: ' & FAIL#, |
            'Data Purge', Icon:Asterisk, |
             Button:OK, Button:OK, 0)
    Of Button:OK
    End !CASE
Prog:nextrecord      routine
    Yield()
    prog:recordsprocessed += 1
    prog:recordsthiscycle += 1
    If prog:percentprogress < 100
        prog:percentprogress = (prog:recordsprocessed / prog:recordstoprocess)*100
        If prog:percentprogress > 100
            prog:percentprogress = 100
        End
        If prog:percentprogress <> prog:thermometer then
            prog:thermometer = prog:percentprogress

Omit('***',ClarionetUsed=0)
            If ClarionetServer:Active()
                cnprog:thermometer = prog:thermometer
            Else ! If ClarionetServer:Active()
***
                ?prog:pcttext{prop:text} = format(prog:percentprogress,@n3) & '% Completed'
Omit('***',ClarionetUsed=0)
            End ! If ClarionetServer:Active()
***
        End
    End
    Display()

prog:cancelcheck         routine
    cancel# = 0
    prog:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Prog:Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Beep(Beep:SystemQuestion)  ;  Yield()
        Case Message('Are you sure you want to cancel?', |
                'Cancel Pressed', Icon:Question, |
                 Button:Yes+Button:No, Button:No, 0)
        Of Button:Yes
            prog:Cancel = 1
        Of Button:No
        End !CASE
    End!If cancel# = 1


prog:Finish         routine
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        CNprog:thermometer = 100
    Else ! If ClarionetServer:Active()
***
        prog:thermometer = 100
        ?prog:pcttext{prop:text} = '100% Completed'
        display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ProgressSetup        Procedure(Long func:Records)
Code
    prog:SkipRecords = 0

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Prog.ResetProgress(func:Records)
        Clarionet:OpenPushWindow(CN:ProgWindow)
    Else ! If ClarionetServer:Active()
***
        Open(ProgWindow)
        Prog.ResetProgress(func:Records)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ResetProgress      Procedure(Long func:Records)
Code
    prog:recordspercycle         = 25
    prog:recordsprocessed        = 0
    prog:percentprogress         = 0
    prog:thermometer    = 0
    prog:recordstoprocess    = func:Records

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:pcttext{prop:text} = '0% Completed'
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.InsideLoop         Procedure()
Code

Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        Return 0
    Else ! ClarionetServer:Active()
***
        Do Prog:NextRecord
        Do Prog:CancelCheck
        If Prog:Cancel = 1
            Return 1
        End !If Prog:Cancel = 1
        Return 0
Omit('***',ClarionetUsed=0)
    End ! ClarionetServer:Active()
***

Prog.ProgressText        Procedure(String    func:String)
Code
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
    Else ! If ClarionetServer:Active()
***
        ?prog:UserString{prop:Text} = Clip(func:String)
        Display()
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

Prog.ProgressFinish     Procedure()
Code
    Do Prog:Finish
Omit('***',ClarionetUsed=0)
    If ClarionetServer:Active()
        ClarioNet:ClosePushWindow(CN:progwindow)
    Else ! If ClarionetServer:Active()
***
        close(progwindow)
Omit('***',ClarionetUsed=0)
    End ! If ClarionetServer:Active()
***

