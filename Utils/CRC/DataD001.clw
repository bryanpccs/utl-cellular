

   MEMBER('DataDelete.clw')                                ! This is a MEMBER module

                     MAP
                       INCLUDE('DATAD001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                             ! Declare Procedure
tmp:StartDate        DATE                                  !Start Date
tmp:StartTime        TIME                                  !Start Time
pos                  STRING(255)                           !
  CODE
        tmp:StartDate = Today()
        tmp:StartTime = Clock()
        LinePrint('DataDelete Started: ' & Format(tmp:StartDate,@d6) & ' @ ' & Format(tmp:StartTime,@t1),'DATADELETE.LOG')

        Relate:TRADEACC.Open()
        Relate:JOBS.Open()
        Relate:JOBS_ALIAS.Open()

        Break# = 0
        Count# = 0
        Access:TRADEACC.ClearKey(tra:RecordNumberKey)
        tra:RecordNumber = 1
        Set(tra:RecordNumberKey,tra:RecordNumberKey)
        Loop
            If Access:TRADEACC.NEXT()
               Break
            End !If

            If tra:Account_Number = 'VR (HEAD)' Or |
                tra:Account_Number = 'VODA CONNECT'
                Cycle
            End ! tra:Account_Number = 'VODA CONNECT'

            CountJobs# = 0

            Access:SUBTRACC.ClearKey(sub:Main_Account_Key)
            sub:Main_Account_Number = tra:Account_Number
            Set(sub:Main_Account_Key,sub:Main_Account_Key)
            Loop
                If Access:SUBTRACC.NEXT()
                   Break
                End !If
                If sub:Main_Account_Number <> tra:Account_Number      |
                    Then Break.  ! End If

                Access:JOBS_ALIAS.ClearKey(job_ali:AccountNumberKey)
                job_ali:Account_Number = sub:Account_Number
                Set(job_ali:AccountNumberKey,job_ali:AccountNumberKey)
                Loop
                    If Access:JOBS_ALIAS.NEXT()
                       Break
                    End !If
                    If job_ali:Account_Number <> sub:Account_Number      |
                        Then Break.  ! End If
                    Access:JOBS.Clearkey(job:Ref_Number_Key)
                    job:Ref_Number  = job_ali:Ref_Number
                    If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        ! Found
                        If Relate:JOBS.Delete(0) = Level:Benign
                            Count# += 1
                            CountJobs# += 1
                            ! Has 55 mins passed? (DBH: 12-10-2005)
                            If Command () > ''
                                If Clock() - tmp:StartTime > Command() * 6000
                                    Break# = 1
                                    Break
                                End ! If Clock() - tmp:StartTime > 330000
                            Else ! If Command () > ''
                                If Clock() - tmp:StartTime > 330000
                                    Break# = 1
                                    Break
                                End ! If Clock() - tmp:StartTime > 330000
                            End ! If Command () > ''
                        End ! If Relate:JOBS.Delete(0) = Level:Benign
                    Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                        ! Error
                    End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                End !Loop
                If Break# = 1
                    Break
                End ! If Break# = 1
            End !Loop
            If Break# = 1
                Break
            End ! If Break# = 1

            If CountJobs# = 0
                LinePrint('No Jobs Found For Account: ' & Clip(tra:Account_Number),'DATADELETE.LOG')
            End ! If CountJobs# = 0
        End !Loop

        Relate:JOBS.CLose()
        Relate:JOBS_ALIAS.Close()

        If Count# = 0
            Set(TRADEACC,0)
            Loop
                If Access:TRADEACC.NEXT()
                   Break
                End !If
                If tra:Account_Number <> 'VR (HEAD)' And |
                    tra:Account_Number <> 'VODA CONNECT'
!                    pos = Position(tra:RecordNumberKey)
                    LinePrint('Deleted Account Number: ' & Clip(tra:Account_Number),'DATADELETE.LOG')
                    Relate:TRADEACC.Delete(0)
!                    Reset(tra:RecordNumberKey,pos)
                End ! tra:Account_Number <> 'VODACONNECT'
            End !Loop
        End ! If Count# = 0

        Relate:TRADEACC.Close()

        LinePrint('Jobs Deleted: ' & Count#,'DATADELETE.LOG')
        LinePrint('DataDelete Finished: ' & Format(Today(),@d6) & ' @ ' & Format(Clock(),@t1),'DATADELETE.LOG')
        LinePrint('===================','DATADELETE.LOG')

