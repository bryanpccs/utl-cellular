

   MEMBER('NewIMEIShip.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('NEWIM001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:SkipCount          Byte(0)
Prog:SkipNumber         Long()
Prog:PercentText        String(100)
Prog:UserString         String(100)
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING(@s100),AT(0,3,161,10),USE(Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(0,32,161,10),USE(Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),HIDE
     END
  CODE
   Relate:IMEISHIP.Open
   Relate:IMEISHIPnew.Open
    Do Prog:ProgressSetup
    Prog:SkipNumber = 250
    Prog:TotalRecords = RECORDS(IMEISHIP) / 8
    Prog:ShowPercentage = 1 !Show Percentage Figure
    Prog:UserString = 'Searching for records...'

    Access:IMEISHIP.Clearkey(imei:shipDateKey)
    imei:shipDate    = deformat('01/01/2005',@d06)
    set(imei:shipDateKey,imei:shipDateKey)

    Accept
        Case Event()
            Of Event:Timer
                Loop 25 Times
                    !Inside Loop
                    If Access:IMEISHIP.Next()
                        Prog:Exit = 1
                        Break
                    End ! If Access:ORDERS.Next()

                    if (imei:shipDate    < deformat('01/01/2005',@d06))
                        prog:Exit = 1
                        Break
                    end ! if (imei:shipDate    <> deformat('01/01/2005',@d06))
                    if (Access:IMEISHIPNEW.PrimeRecord() = Level:Benign)
                        imei1:record :=: imei:record
                        if (Access:IMEISHIPNEW.TryInsert() = Level:Benign)
                            ! Inserted
                        else ! if (Access:IMEISHIPNEW.TryInsert() = Level:Benign)
                            ! Error
                        end ! if (Access:IMEISHIPNEW.TryInsert() = Level:Benign)
                    end ! if (Access:IMEISHIPNEW.PrimeRecord() = Level:Benign)

                    Prog:RecordCount += 1

                    Prog:UserString = 'Records Added ' & Prog:RecordCount

                    Do Prog:UpdateScreen
                End ! Loop 25 Times
            Of Event:CloseWindow
                Prog:Exit = 1
                Prog:Cancelled = 1
                Break
            Of Event:Accepted
                If Field() = ?Prog:Cancel
                    Beep(Beep:SystemQuestion)  ;  Yield()
                    Case Message('Are you sure you want to cancel?','Cancel Pressed',|
                                   icon:Question,'&Yes|&No',2,2)
                        Of 1 ! &Yes Button
                            Prog:Exit = 1
                            Prog:Cancelled = 1
                            Break
                        Of 2 ! &No Button
                    End!Case Message
                End ! If Field() = ?ProgressCancel
        End ! Case Event()
        If Prog:Exit
            Break
        End ! If Prog:Exit
    End ! Accept
    Do Prog:ProgressFinished
   Relate:IMEISHIP.Close
   Relate:IMEISHIPnew.Close
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            Prog:PercentText = Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    Prog:PercentText = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
