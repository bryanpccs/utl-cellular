  MEMBER('ShippingDateLocator.clw')

  INCLUDE('ABFILE.INC'),ONCE

  MAP
SHIPPBC0:DctInit    PROCEDURE
SHIPPBC0:DctKill    PROCEDURE
SHIPPBC0:FilesInit  PROCEDURE
  END

Hide:Access:IMEISHIP CLASS(FileManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
                     END


Hide:Relate:IMEISHIP CLASS(RelationManager)
Init                   PROCEDURE
Kill                   PROCEDURE(),DERIVED
                     END

SHIPPBC0:DctInit PROCEDURE
  CODE
  Relate:IMEISHIP &= Hide:Relate:IMEISHIP

SHIPPBC0:FilesInit PROCEDURE
  CODE
  Hide:Relate:IMEISHIP.Init


SHIPPBC0:DctKill PROCEDURE
  CODE
  Hide:Relate:IMEISHIP.Kill


Hide:Access:IMEISHIP.Init PROCEDURE
  CODE
  SELF.Init(IMEISHIP,GlobalErrors)
  SELF.Buffer &= IMEI:Record
  SELF.Create = 1
  SELF.LockRecover = 10
  SELF.AddKey(IMEI:RecordNumberKey,'By Record Number',1)
  SELF.AddKey(IMEI:IMEIKey,'By I.M.E.I. Number',0)
  SELF.AddKey(IMEI:ShipDateKey,'By Shipment Date',0)
  Access:IMEISHIP &= SELF


Hide:Relate:IMEISHIP.Init PROCEDURE
  CODE
  Hide:Access:IMEISHIP.Init
  SELF.Init(Access:IMEISHIP,1)


Hide:Access:IMEISHIP.Kill PROCEDURE

  CODE
  PARENT.Kill
  Access:IMEISHIP &= NULL


Hide:Access:IMEISHIP.PrimeFields PROCEDURE

  CODE
  IMEI:BER = 0
  IMEI:ManualEntry = 0
  PARENT.PrimeFields


Hide:Relate:IMEISHIP.Kill PROCEDURE

  CODE
  Hide:Access:IMEISHIP.Kill
  PARENT.Kill
  Relate:IMEISHIP &= NULL

