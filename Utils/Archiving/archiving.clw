   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFILE.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('EQUATES.CLW'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

!* * * * Line Print Template Generated Code * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

OFSTRUCT    GROUP,TYPE                                                                  
cBytes       BYTE                                                                       ! Specifies the length, in bytes, of the structure
cFixedDisk   BYTE                                                                       ! Specifies whether the file is on a hard (fixed) disk. This member is nonzero if the file is on a hard disk
nErrCode     SIGNED                                                                     ! Specifies the MS-DOS error code if the OpenFile function failed
Reserved1    SIGNED                                                                     ! Reserved, Don't use
Reserved2    SIGNED                                                                     ! Reserved, Don't use
szPathName   BYTE,DIM(128)                                                              ! Specifies the path and filename of the file
            END

LF                  CSTRING(CHR(10))                                                    ! Line Feed
FF                  CSTRING(CHR(12))                                                    ! Form Feed
CR                  CSTRING(CHR(13))                                                    ! Carriage Return

lpszFilename        CSTRING(144)                                                        ! File Name
fnAttribute         SIGNED                                                              ! Attribute
hf                  SIGNED                                                              ! File Handle
hpvBuffer           STRING(500)                                                        ! String To Write
cbBuffer            SIGNED                                                              ! Characters to Write
BytesWritten        SIGNED                                                              ! Characters Written

Succeeded           EQUATE(0)                                                           ! Function Succeeded
OpenError           EQUATE(1)                                                           ! Can Not Open File or Device
WriteError          EQUATE(2)                                                           ! Can not Write to File or Device
CloseError          EQUATE(3)                                                           ! Can not Close File or Device
SeekError           EQUATE(4)                                                           ! Can not Seek File or Device

ATTR_Normal         EQUATE(0)                                                           ! File Attribute Normal
ATTR_ReadOnly       EQUATE(1)                                                           ! File Attribute Read Only
ATTR_Hidden         EQUATE(2)                                                           ! File Attribute Hidden
ATTR_System         EQUATE(3)                                                           ! File Attribute System

OF_READ             EQUATE(0000h)                                                       ! Opens the file for reading only
OF_WRITE            EQUATE(0001h)                                                       ! Opens the file for writing only
OF_READWRITE        EQUATE(0002h)                                                       ! Opens the file for reading and writing
OF_SHARE_COMPAT     EQUATE(0000h)                                                       ! Opens the file with compatibility mode, allowing any program on a given machine to open the file any number of times.
OF_SHARE_EXCLUSIVE  EQUATE(0010h)                                                       ! Opens the file with exclusive mode, denying other programs both read and write access to the file
OF_SHARE_DENY_WRITE EQUATE(0020h)                                                       ! Opens the file and denies other programs write access to the file
OF_SHARE_DENY_READ  EQUATE(0030h)                                                       ! Opens the file and denies other programs read access to the file
OF_SHARE_DENY_NONE  EQUATE(0040h)                                                       ! Opens the file without denying other programs read or write access to the file
OF_PARSE            EQUATE(0100h)                                                       ! Fills the OFSTRUCT structure but carries out no other action
OF_DELETE           EQUATE(0200h)                                                       ! Deletes the file
OF_VERIFY           EQUATE(0400h)                                                       ! Compares the time and date in the OF_STRUCT with the time and date of the specified file
OF_SEARCH           EQUATE(0400h)                                                       ! Windows searches in directories even when the file name includes a full path
OF_CANCEL           EQUATE(0800h)                                                       ! Adds a Cancel button to the OF_PROMPT dialog box. Pressing the Cancel button directs OpenFile to return a file-not-found error message
OF_CREATE           EQUATE(1000h)                                                       ! Creates a new file. If the file already exists, it is truncated to zero length
OF_PROMPT           EQUATE(2000h)                                                       ! Displays a dialog box if the requested file does not exist.
OF_EXIST            EQUATE(4000h)                                                       ! Opens the file, and then closes it. This value is used to test for file existence
OF_REOPEN           EQUATE(8000h)                                                       ! Opens the file using information in the reopen buffer

OF_STRUCT           LIKE(OFSTRUCT)                                                      ! Will Be loaded with File information. See OFSTRUCT above

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('ARCHIBC.CLW')
DctInit     PROCEDURE
DctKill     PROCEDURE
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('ARCHI001.CLW')
Main                   PROCEDURE   !
     END
     
     !* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
          LinePrint(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>),BYTE,PROC          ! Declare LinePrint Function
          DeleteFile(STRING FileToDelete),BYTE,PROC                                          ! Declare DeleteFile Function
            MODULE('')                                                                       ! MODULE Start
             OpenFile(*CSTRING,*OFSTRUCT,SIGNED),SIGNED,PASCAL,RAW                           ! Open/Create/Delete File
             _llseek(SIGNED,LONG,SIGNED),LONG,PASCAL                                         ! Control File Pointer
             _lwrite(SIGNED,*STRING,UNSIGNED),UNSIGNED,PASCAL,RAW                           ! Write to File
             _lclose(SIGNED),SIGNED,PASCAL                                                   ! Close File
            END                                                                              ! Terminate Module
     
     !* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
     
   END

glo:oldaudit       STRING(255)
glo:oldaudstaex    STRING(255)
glo:oldaudstats    STRING(255)
glo:oldconthist    STRING(255)
glo:oldjobnotes    STRING(255)
glo:oldjobs        STRING(255)
glo:oldjobse       STRING(255)
SilentRunning        BYTE(0)                         !Set true when application is running in silent mode

JOBS                 FILE,DRIVER('Btrieve'),NAME('JOBS.DAT'),PRE(JOB),BINDABLE,CREATE,THREAD
Ref_Number_Key           KEY(JOB:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(JOB:Model_Number,JOB:Unit_Type),DUP,NOCASE
EngCompKey               KEY(JOB:Engineer,JOB:Completed,JOB:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(JOB:Engineer,JOB:Workshop,JOB:Ref_Number),DUP,NOCASE
Surname_Key              KEY(JOB:Surname),DUP,NOCASE
MobileNumberKey          KEY(JOB:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(JOB:ESN),DUP,NOCASE
MSN_Key                  KEY(JOB:MSN),DUP,NOCASE
AccountNumberKey         KEY(JOB:Account_Number,JOB:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(JOB:Account_Number,JOB:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(JOB:Model_Number),DUP,NOCASE
Engineer_Key             KEY(JOB:Engineer,JOB:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(JOB:date_booked),DUP,NOCASE
DateCompletedKey         KEY(JOB:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(JOB:Model_Number,JOB:Date_Completed),DUP,NOCASE
By_Status                KEY(JOB:Current_Status,JOB:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(JOB:Current_Status,JOB:Location,JOB:Ref_Number),DUP,NOCASE
Location_Key             KEY(JOB:Location,JOB:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(JOB:Third_Party_Site,JOB:Third_Party_Printed,JOB:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(JOB:Third_Party_Site,JOB:Third_Party_Printed,JOB:ESN),DUP,NOCASE
ThirdMsnKey              KEY(JOB:Third_Party_Site,JOB:Third_Party_Printed,JOB:MSN),DUP,NOCASE
PriorityTypeKey          KEY(JOB:Job_Priority,JOB:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(JOB:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(JOB:Manufacturer,JOB:EDI,JOB:EDI_Batch_Number,JOB:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(JOB:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(JOB:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(JOB:Batch_Number,JOB:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(JOB:Batch_Number,JOB:Current_Status,JOB:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(JOB:Batch_Number,JOB:Model_Number,JOB:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(JOB:Batch_Number,JOB:Invoice_Number,JOB:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(JOB:Batch_Number,JOB:Completed,JOB:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(JOB:Chargeable_Job,JOB:Account_Number,JOB:Invoice_Number,JOB:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(JOB:Invoice_Exception,JOB:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(JOB:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(JOB:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(JOB:Despatched,JOB:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(JOB:Despatched,JOB:Account_Number,JOB:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(JOB:Despatched,JOB:Current_Courier,JOB:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(JOB:Despatched,JOB:Account_Number,JOB:Current_Courier,JOB:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(JOB:Despatch_Number,JOB:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(JOB:Courier,JOB:Date_Despatched,JOB:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(JOB:Loan_Courier,JOB:Loan_Despatched,JOB:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(JOB:Exchange_Courier,JOB:Exchange_Despatched,JOB:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(JOB:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(JOB:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(JOB:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(JOB:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(JOB:Bouncer,JOB:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(JOB:Engineer,JOB:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(JOB:Exchange_Status,JOB:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(JOB:Loan_Status,JOB:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(JOB:Exchange_Status,JOB:Location,JOB:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(JOB:Loan_Status,JOB:Location,JOB:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(JOB:InvoiceAccount,JOB:InvoiceBatch,JOB:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(JOB:InvoiceAccount,JOB:InvoiceBatch,JOB:InvoiceStatus,JOB:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

oldAUDIT             FILE,DRIVER('Btrieve'),NAME(glo:oldaudit),PRE(oldaud),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(oldaud:Ref_Number,-oldaud:Date,-oldaud:Time,oldaud:Action),DUP,NOCASE
Action_Key               KEY(oldaud:Ref_Number,oldaud:Action,-oldaud:Date),DUP,NOCASE
User_Key                 KEY(oldaud:Ref_Number,oldaud:User,-oldaud:Date),DUP,NOCASE
Record_Number_Key        KEY(oldaud:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(oldaud:Action,oldaud:Date),DUP,NOCASE
TypeRefKey               KEY(oldaud:Ref_Number,oldaud:Type,-oldaud:Date,-oldaud:Time,oldaud:Action),DUP,NOCASE
TypeActionKey            KEY(oldaud:Ref_Number,oldaud:Type,oldaud:Action,-oldaud:Date),DUP,NOCASE
TypeUserKey              KEY(oldaud:Ref_Number,oldaud:Type,oldaud:User,-oldaud:Date),DUP,NOCASE
DateActionJobKey         KEY(oldaud:Date,oldaud:Action,oldaud:Ref_Number),DUP,NOCASE
DateJobKey               KEY(oldaud:Date,oldaud:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(oldaud:Date,oldaud:Type,oldaud:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(oldaud:Date,oldaud:Type,oldaud:Action,oldaud:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
DummyField                  STRING(2)
Notes                       STRING(255)
Type                        STRING(3)
                         END
                     END                       

oldAUDSTAEX          FILE,DRIVER('Btrieve'),OEM,NAME(glo:oldaudstaex),PRE(oldaux),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(oldaux:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(oldaux:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AgentName                   STRING(30)
                         END
                     END                       

oldAUDSTATS          FILE,DRIVER('Btrieve'),OEM,NAME(glo:oldaudstats),PRE(oldaus),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(oldaus:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(oldaus:RefNumber,oldaus:Type,oldaus:DateChanged),DUP,NOCASE
NewStatusKey             KEY(oldaus:RefNumber,oldaus:Type,oldaus:NewStatus),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 STRING(20)
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END                       

oldCONTHIST          FILE,DRIVER('Btrieve'),NAME(glo:oldconthist),PRE(oldcht),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(oldcht:Ref_Number,-oldcht:Date,-oldcht:Time,oldcht:Action),DUP,NOCASE
Action_Key               KEY(oldcht:Ref_Number,oldcht:Action,-oldcht:Date),DUP,NOCASE
User_Key                 KEY(oldcht:Ref_Number,oldcht:User,-oldcht:Date),DUP,NOCASE
Record_Number_Key        KEY(oldcht:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(1000)
                         END
                     END                       

oldJOBNOTES          FILE,DRIVER('Btrieve'),OEM,NAME(glo:oldjobnotes),PRE(oldjbn),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(oldjbn:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
Fault_Description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
ColContatName               STRING(30)
ColDepartment               STRING(30)
DelContactName              STRING(30)
DelDepartment               STRING(30)
                         END
                     END                       

oldJOBS              FILE,DRIVER('Btrieve'),NAME(glo:oldjobs),PRE(oldjob),BINDABLE,THREAD
Ref_Number_Key           KEY(oldjob:Ref_Number),NOCASE,PRIMARY
Model_Unit_Key           KEY(oldjob:Model_Number,oldjob:Unit_Type),DUP,NOCASE
EngCompKey               KEY(oldjob:Engineer,oldjob:Completed,oldjob:Ref_Number),DUP,NOCASE
EngWorkKey               KEY(oldjob:Engineer,oldjob:Workshop,oldjob:Ref_Number),DUP,NOCASE
Surname_Key              KEY(oldjob:Surname),DUP,NOCASE
MobileNumberKey          KEY(oldjob:Mobile_Number),DUP,NOCASE
ESN_Key                  KEY(oldjob:ESN),DUP,NOCASE
MSN_Key                  KEY(oldjob:MSN),DUP,NOCASE
AccountNumberKey         KEY(oldjob:Account_Number,oldjob:Ref_Number),DUP,NOCASE
AccOrdNoKey              KEY(oldjob:Account_Number,oldjob:Order_Number),DUP,NOCASE
Model_Number_Key         KEY(oldjob:Model_Number),DUP,NOCASE
Engineer_Key             KEY(oldjob:Engineer,oldjob:Model_Number),DUP,NOCASE
Date_Booked_Key          KEY(oldjob:date_booked),DUP,NOCASE
DateCompletedKey         KEY(oldjob:Date_Completed),DUP,NOCASE
ModelCompKey             KEY(oldjob:Model_Number,oldjob:Date_Completed),DUP,NOCASE
By_Status                KEY(oldjob:Current_Status,oldjob:Ref_Number),DUP,NOCASE
StatusLocKey             KEY(oldjob:Current_Status,oldjob:Location,oldjob:Ref_Number),DUP,NOCASE
Location_Key             KEY(oldjob:Location,oldjob:Ref_Number),DUP,NOCASE
Third_Party_Key          KEY(oldjob:Third_Party_Site,oldjob:Third_Party_Printed,oldjob:Ref_Number),DUP,NOCASE
ThirdEsnKey              KEY(oldjob:Third_Party_Site,oldjob:Third_Party_Printed,oldjob:ESN),DUP,NOCASE
ThirdMsnKey              KEY(oldjob:Third_Party_Site,oldjob:Third_Party_Printed,oldjob:MSN),DUP,NOCASE
PriorityTypeKey          KEY(oldjob:Job_Priority,oldjob:Ref_Number),DUP,NOCASE
Unit_Type_Key            KEY(oldjob:Unit_Type),DUP,NOCASE
EDI_Key                  KEY(oldjob:Manufacturer,oldjob:EDI,oldjob:EDI_Batch_Number,oldjob:Ref_Number),DUP,NOCASE
InvoiceNumberKey         KEY(oldjob:Invoice_Number),DUP,NOCASE
WarInvoiceNoKey          KEY(oldjob:Invoice_Number_Warranty),DUP,NOCASE
Batch_Number_Key         KEY(oldjob:Batch_Number,oldjob:Ref_Number),DUP,NOCASE
Batch_Status_Key         KEY(oldjob:Batch_Number,oldjob:Current_Status,oldjob:Ref_Number),DUP,NOCASE
BatchModelNoKey          KEY(oldjob:Batch_Number,oldjob:Model_Number,oldjob:Ref_Number),DUP,NOCASE
BatchInvoicedKey         KEY(oldjob:Batch_Number,oldjob:Invoice_Number,oldjob:Ref_Number),DUP,NOCASE
BatchCompKey             KEY(oldjob:Batch_Number,oldjob:Completed,oldjob:Ref_Number),DUP,NOCASE
ChaInvoiceKey            KEY(oldjob:Chargeable_Job,oldjob:Account_Number,oldjob:Invoice_Number,oldjob:Ref_Number),DUP,NOCASE
InvoiceExceptKey         KEY(oldjob:Invoice_Exception,oldjob:Ref_Number),DUP,NOCASE
ConsignmentNoKey         KEY(oldjob:Consignment_Number),DUP,NOCASE
InConsignKey             KEY(oldjob:Incoming_Consignment_Number),DUP,NOCASE
ReadyToDespKey           KEY(oldjob:Despatched,oldjob:Ref_Number),DUP,NOCASE
ReadyToTradeKey          KEY(oldjob:Despatched,oldjob:Account_Number,oldjob:Ref_Number),DUP,NOCASE
ReadyToCouKey            KEY(oldjob:Despatched,oldjob:Current_Courier,oldjob:Ref_Number),DUP,NOCASE
ReadyToAllKey            KEY(oldjob:Despatched,oldjob:Account_Number,oldjob:Current_Courier,oldjob:Ref_Number),DUP,NOCASE
DespJobNumberKey         KEY(oldjob:Despatch_Number,oldjob:Ref_Number),DUP,NOCASE
DateDespatchKey          KEY(oldjob:Courier,oldjob:Date_Despatched,oldjob:Ref_Number),DUP,NOCASE
DateDespLoaKey           KEY(oldjob:Loan_Courier,oldjob:Loan_Despatched,oldjob:Ref_Number),DUP,NOCASE
DateDespExcKey           KEY(oldjob:Exchange_Courier,oldjob:Exchange_Despatched,oldjob:Ref_Number),DUP,NOCASE
ChaRepTypeKey            KEY(oldjob:Repair_Type),DUP,NOCASE
WarRepTypeKey            KEY(oldjob:Repair_Type_Warranty),DUP,NOCASE
ChaTypeKey               KEY(oldjob:Charge_Type),DUP,NOCASE
WarChaTypeKey            KEY(oldjob:Warranty_Charge_Type),DUP,NOCASE
Bouncer_Key              KEY(oldjob:Bouncer,oldjob:Ref_Number),DUP,NOCASE
EngDateCompKey           KEY(oldjob:Engineer,oldjob:Date_Completed),DUP,NOCASE
ExcStatusKey             KEY(oldjob:Exchange_Status,oldjob:Ref_Number),DUP,NOCASE
LoanStatusKey            KEY(oldjob:Loan_Status,oldjob:Ref_Number),DUP,NOCASE
ExchangeLocKey           KEY(oldjob:Exchange_Status,oldjob:Location,oldjob:Ref_Number),DUP,NOCASE
LoanLocKey               KEY(oldjob:Loan_Status,oldjob:Location,oldjob:Ref_Number),DUP,NOCASE
BatchJobKey              KEY(oldjob:InvoiceAccount,oldjob:InvoiceBatch,oldjob:Ref_Number),DUP,NOCASE
BatchStatusKey           KEY(oldjob:InvoiceAccount,oldjob:InvoiceBatch,oldjob:InvoiceStatus,oldjob:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
Ref_Number                  LONG
Batch_Number                LONG
Internal_Status             STRING(10)
Auto_Search                 STRING(30)
who_booked                  STRING(3)
date_booked                 DATE
time_booked                 TIME
Cancelled                   STRING(3)
Bouncer                     STRING(8)
Bouncer_Type                STRING(3)
Web_Type                    STRING(3)
Warranty_Job                STRING(3)
Chargeable_Job              STRING(3)
Model_Number                STRING(30)
Manufacturer                STRING(30)
ESN                         STRING(20)
MSN                         STRING(20)
ProductCode                 STRING(30)
Unit_Type                   STRING(30)
Colour                      STRING(30)
Location_Type               STRING(10)
Phone_Lock                  STRING(30)
Workshop                    STRING(3)
Location                    STRING(30)
Authority_Number            STRING(30)
Insurance_Reference_Number  STRING(30)
DOP                         DATE
Insurance                   STRING(3)
Insurance_Type              STRING(30)
Transit_Type                STRING(30)
Physical_Damage             STRING(3)
Intermittent_Fault          STRING(3)
Loan_Status                 STRING(30)
Exchange_Status             STRING(30)
Job_Priority                STRING(30)
Charge_Type                 STRING(30)
Warranty_Charge_Type        STRING(30)
Current_Status              STRING(30)
Account_Number              STRING(15)
Trade_Account_Name          STRING(30)
Department_Name             STRING(30)
Order_Number                STRING(30)
POP                         STRING(3)
In_Repair                   STRING(3)
Date_In_Repair              DATE
Time_In_Repair              TIME
On_Test                     STRING(3)
Date_On_Test                DATE
Time_On_Test                TIME
Estimate_Ready              STRING(3)
QA_Passed                   STRING(3)
Date_QA_Passed              DATE
Time_QA_Passed              TIME
QA_Rejected                 STRING(3)
Date_QA_Rejected            DATE
Time_QA_Rejected            TIME
QA_Second_Passed            STRING(3)
Date_QA_Second_Passed       DATE
Time_QA_Second_Passed       TIME
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Postcode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Telephone_Number            STRING(15)
Fax_Number                  STRING(15)
Mobile_Number               STRING(15)
Postcode_Collection         STRING(10)
Company_Name_Collection     STRING(30)
Address_Line1_Collection    STRING(30)
Address_Line2_Collection    STRING(30)
Address_Line3_Collection    STRING(30)
Telephone_Collection        STRING(15)
Postcode_Delivery           STRING(10)
Address_Line1_Delivery      STRING(30)
Company_Name_Delivery       STRING(30)
Address_Line2_Delivery      STRING(30)
Address_Line3_Delivery      STRING(30)
Telephone_Delivery          STRING(15)
Date_Completed              DATE
Time_Completed              TIME
Completed                   STRING(3)
Paid                        STRING(3)
Paid_Warranty               STRING(3)
Date_Paid                   DATE
Repair_Type                 STRING(30)
Repair_Type_Warranty        STRING(30)
Engineer                    STRING(3)
Ignore_Chargeable_Charges   STRING(3)
Ignore_Warranty_Charges     STRING(3)
Ignore_Estimate_Charges     STRING(3)
Courier_Cost                REAL
Advance_Payment             REAL
Labour_Cost                 REAL
Parts_Cost                  REAL
Sub_Total                   REAL
Courier_Cost_Estimate       REAL
Labour_Cost_Estimate        REAL
Parts_Cost_Estimate         REAL
Sub_Total_Estimate          REAL
Courier_Cost_Warranty       REAL
Labour_Cost_Warranty        REAL
Parts_Cost_Warranty         REAL
Sub_Total_Warranty          REAL
Loan_Issued_Date            DATE
Loan_Unit_Number            LONG
Loan_accessory              STRING(3)
Loan_User                   STRING(3)
Loan_Courier                STRING(30)
Loan_Consignment_Number     STRING(30)
Loan_Despatched             DATE
Loan_Despatched_User        STRING(3)
Loan_Despatch_Number        LONG
LoaService                  STRING(1)
Exchange_Unit_Number        LONG
Exchange_Authorised         STRING(3)
Loan_Authorised             STRING(3)
Exchange_Accessory          STRING(3)
Exchange_Issued_Date        DATE
Exchange_User               STRING(3)
Exchange_Courier            STRING(30)
Exchange_Consignment_Number STRING(30)
Exchange_Despatched         DATE
Exchange_Despatched_User    STRING(3)
Exchange_Despatch_Number    LONG
ExcService                  STRING(1)
Date_Despatched             DATE
Despatch_Number             LONG
Despatch_User               STRING(3)
Courier                     STRING(30)
Consignment_Number          STRING(30)
Incoming_Courier            STRING(30)
Incoming_Consignment_Number STRING(30)
Incoming_Date               DATE
Despatched                  STRING(3)
Despatch_Type               STRING(3)
Current_Courier             STRING(30)
JobService                  STRING(1)
Last_Repair_Days            REAL
Third_Party_Site            STRING(30)
Estimate                    STRING(3)
Estimate_If_Over            REAL
Estimate_Accepted           STRING(3)
Estimate_Rejected           STRING(3)
Third_Party_Printed         STRING(3)
ThirdPartyDateDesp          DATE
Fault_Code1                 STRING(30)
Fault_Code2                 STRING(30)
Fault_Code3                 STRING(30)
Fault_Code4                 STRING(30)
Fault_Code5                 STRING(30)
Fault_Code6                 STRING(30)
Fault_Code7                 STRING(30)
Fault_Code8                 STRING(30)
Fault_Code9                 STRING(30)
Fault_Code10                STRING(255)
Fault_Code11                STRING(255)
Fault_Code12                STRING(255)
PreviousStatus              STRING(30)
StatusUser                  STRING(3)
Status_End_Date             DATE
Status_End_Time             TIME
Turnaround_End_Date         DATE
Turnaround_End_Time         TIME
Turnaround_Time             STRING(30)
Special_Instructions        STRING(30)
InvoiceAccount              STRING(15)
InvoiceStatus               STRING(3)
InvoiceBatch                LONG
InvoiceQuery                STRING(255)
EDI                         STRING(3)
EDI_Batch_Number            REAL
Invoice_Exception           STRING(3)
Invoice_Failure_Reason      STRING(80)
Invoice_Number              LONG
Invoice_Date                DATE
Invoice_Date_Warranty       DATE
Invoice_Courier_Cost        REAL
Invoice_Labour_Cost         REAL
Invoice_Parts_Cost          REAL
Invoice_Sub_Total           REAL
Invoice_Number_Warranty     LONG
WInvoice_Courier_Cost       REAL
WInvoice_Labour_Cost        REAL
WInvoice_Parts_Cost         REAL
WInvoice_Sub_Total          REAL
                         END
                     END                       

oldJOBSE             FILE,DRIVER('Btrieve'),OEM,NAME(glo:oldjobse),PRE(oldjobe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(oldjobe:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(oldjobe:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(oldjobe:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(oldjobe:CompleteRepairDate,oldjobe:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

JOBNOTES             FILE,DRIVER('Btrieve'),OEM,NAME('JOBNOTES.DAT'),PRE(jbn),CREATE,BINDABLE,THREAD
RefNumberKey             KEY(jbn:RefNumber),NOCASE,PRIMARY
Record                   RECORD,PRE()
RefNumber                   LONG
Fault_Description           STRING(255)
Engineers_Notes             STRING(255)
Invoice_Text                STRING(255)
Collection_Text             STRING(255)
Delivery_Text               STRING(255)
ColContatName               STRING(30)
ColDepartment               STRING(30)
DelContactName              STRING(30)
DelDepartment               STRING(30)
                         END
                     END                       

CONTHIST             FILE,DRIVER('Btrieve'),NAME('CONTHIST.DAT'),PRE(cht),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(cht:Ref_Number,-cht:Date,-cht:Time,cht:Action),DUP,NOCASE
Action_Key               KEY(cht:Ref_Number,cht:Action,-cht:Date),DUP,NOCASE
User_Key                 KEY(cht:Ref_Number,cht:User,-cht:Date),DUP,NOCASE
Record_Number_Key        KEY(cht:record_number),NOCASE,PRIMARY
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
Notes                       STRING(1000)
                         END
                     END                       

JOBSE                FILE,DRIVER('Btrieve'),OEM,NAME('JOBSE.DAT'),PRE(jobe),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(jobe:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(jobe:RefNumber),DUP,NOCASE
InWorkshopDateKey        KEY(jobe:InWorkshopDate),DUP,NOCASE
CompleteRepairKey        KEY(jobe:CompleteRepairDate,jobe:CompleteRepairTime),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
JobMark                     BYTE
TraFaultCode1               STRING(30)
TraFaultCode2               STRING(30)
TraFaultCode3               STRING(30)
TraFaultCode4               STRING(30)
TraFaultCode5               STRING(30)
TraFaultCode6               STRING(30)
TraFaultCode7               STRING(30)
TraFaultCode8               STRING(30)
TraFaultCode9               STRING(30)
TraFaultCode10              STRING(30)
TraFaultCode11              STRING(30)
TraFaultCode12              STRING(30)
SIMNumber                   STRING(30)
JobReceived                 BYTE
SkillLevel                  LONG
UPSFlagCode                 STRING(1)
FailedDelivery              BYTE
CConfirmSecondEntry         STRING(3)
WConfirmSecondEntry         STRING(3)
EndUserEmailAddress         STRING(255)
Network                     STRING(30)
ExchangeReason              STRING(255)
LoanReason                  STRING(255)
InWorkshopDate              DATE
InWorkshopTime              TIME
Pre_RF_Board_IMEI           STRING(20)
CompleteRepairType          BYTE
CompleteRepairDate          DATE
CompleteRepairTime          TIME
CustomerCollectionDate      DATE
CustomerCollectionTime      TIME
EstimatedDespatchDate       DATE
EstimatedDespatchTime       TIME
FaultCode13                 STRING(30)
FaultCode14                 STRING(30)
FaultCode15                 STRING(30)
FaultCode16                 STRING(30)
FaultCode17                 STRING(30)
FaultCode18                 STRING(30)
FaultCode19                 STRING(30)
FaultCode20                 STRING(30)
                         END
                     END                       

AUDSTATS             FILE,DRIVER('Btrieve'),OEM,NAME('AUDSTATS.DAT'),PRE(aus),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aus:RecordNumber),NOCASE,PRIMARY
DateChangedKey           KEY(aus:RefNumber,aus:Type,aus:DateChanged),DUP,NOCASE
NewStatusKey             KEY(aus:RefNumber,aus:Type,aus:NewStatus),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
Type                        STRING(3)
DateChanged                 DATE
TimeChanged                 STRING(20)
OldStatus                   STRING(30)
NewStatus                   STRING(30)
UserCode                    STRING(3)
                         END
                     END                       

AUDSTAEX             FILE,DRIVER('Btrieve'),OEM,NAME('AUDSTAEX.DAT'),PRE(aux),CREATE,BINDABLE,THREAD
RecordNumberKey          KEY(aux:RecordNumber),NOCASE,PRIMARY
RefNumberKey             KEY(aux:RefNumber),DUP,NOCASE
Record                   RECORD,PRE()
RecordNumber                LONG
RefNumber                   LONG
AgentName                   STRING(30)
                         END
                     END                       

AUDIT                FILE,DRIVER('Btrieve'),NAME('AUDIT.DAT'),PRE(aud),CREATE,BINDABLE,THREAD
Ref_Number_Key           KEY(aud:Ref_Number,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
Action_Key               KEY(aud:Ref_Number,aud:Action,-aud:Date),DUP,NOCASE
User_Key                 KEY(aud:Ref_Number,aud:User,-aud:Date),DUP,NOCASE
Record_Number_Key        KEY(aud:record_number),NOCASE,PRIMARY
ActionOnlyKey            KEY(aud:Action,aud:Date),DUP,NOCASE
TypeRefKey               KEY(aud:Ref_Number,aud:Type,-aud:Date,-aud:Time,aud:Action),DUP,NOCASE
TypeActionKey            KEY(aud:Ref_Number,aud:Type,aud:Action,-aud:Date),DUP,NOCASE
TypeUserKey              KEY(aud:Ref_Number,aud:Type,aud:User,-aud:Date),DUP,NOCASE
DateActionJobKey         KEY(aud:Date,aud:Action,aud:Ref_Number),DUP,NOCASE
DateJobKey               KEY(aud:Date,aud:Ref_Number),DUP,NOCASE
DateTypeJobKey           KEY(aud:Date,aud:Type,aud:Ref_Number),DUP,NOCASE
DateTypeActionKey        KEY(aud:Date,aud:Type,aud:Action,aud:Ref_Number),DUP,NOCASE
Record                   RECORD,PRE()
record_number               REAL
Ref_Number                  REAL
Date                        DATE
Time                        TIME
User                        STRING(3)
Action                      STRING(80)
DummyField                  STRING(2)
Notes                       STRING(255)
Type                        STRING(3)
                         END
                     END                       



Access:JOBS          &FileManager
Relate:JOBS          &RelationManager
Access:oldAUDIT      &FileManager
Relate:oldAUDIT      &RelationManager
Access:oldAUDSTAEX   &FileManager
Relate:oldAUDSTAEX   &RelationManager
Access:oldAUDSTATS   &FileManager
Relate:oldAUDSTATS   &RelationManager
Access:oldCONTHIST   &FileManager
Relate:oldCONTHIST   &RelationManager
Access:oldJOBNOTES   &FileManager
Relate:oldJOBNOTES   &RelationManager
Access:oldJOBS       &FileManager
Relate:oldJOBS       &RelationManager
Access:oldJOBSE      &FileManager
Relate:oldJOBSE      &RelationManager
Access:JOBNOTES      &FileManager
Relate:JOBNOTES      &RelationManager
Access:CONTHIST      &FileManager
Relate:CONTHIST      &RelationManager
Access:JOBSE         &FileManager
Relate:JOBSE         &RelationManager
Access:AUDSTATS      &FileManager
Relate:AUDSTATS      &RelationManager
Access:AUDSTAEX      &FileManager
Relate:AUDSTAEX      &RelationManager
Access:AUDIT         &FileManager
Relate:AUDIT         &RelationManager
FuzzyMatcher         FuzzyClass
GlobalErrors         ErrorClass
INIMgr               INIClass
GlobalRequest        BYTE(0),THREAD
GlobalResponse       BYTE(0),THREAD
VCRRequest           LONG(0),THREAD
lCurrentFDSetting    LONG
lAdjFDSetting        LONG

  CODE
  GlobalErrors.Init
  DctInit
  FuzzyMatcher.Init
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)
  INIMgr.Init('archiving.INI')
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill
  FuzzyMatcher.Kill
  DctKill
  GlobalErrors.Kill
    

!* * * * Line Print Template Generated Code (Start) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

LinePrint FUNCTION(STRING StringToPrint,<STRING DeviceName>,<BYTE CRLF>)                ! LinePrint Function

   CODE                                                                                 ! Fuction Code Starts Here
    IF OMITTED(2)                                                                       ! If Device Name is Omitted
       IF SUB(PRINTER{07B29H},1,2) = '\\' 
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})))               ! Use Default Device
       ELSE
         lpszFilename = SUB(PRINTER{07B29H},1,LEN(CLIP(PRINTER{07B29H})) - 1)           ! Use Default Device
       END 
    ELSE                                                                                ! Otherwise
       lpszFilename = CLIP(DeviceName)                                                  ! Use passed Device Name
    END                                                                                 ! Terminate IF

    IF (OMITTED(3) OR CRLF = True) AND CLIP(StringToPrint) <> FF                        ! If CRLF parameter is set or omitted and user did not pass FF
        hpvBuffer = StringToPrint & CR & LF                                             ! Print String with CR & LF
    ELSE                                                                                ! Otherwise
       hpvBuffer = StringToPrint                                                        ! Print Text As Is
    END                                                                                 ! Terminate IF

    cbBuffer = LEN(CLIP(hpvBuffer))                                                     ! Check Length of the Data to be Printed

     hf = OpenFile(lpszFilename,OF_STRUCT,OF_WRITE)                                     ! Open file and obtain file handle
     IF hf = -1                                                                         ! If File does not exist
      hf = OpenFile(lpszFilename,OF_STRUCT,OF_CREATE)                                   ! Create file and obtain file handle
      IF hf = -1 THEN RETURN(OpenError).                                                ! If Error then return OpenError
     END                                                                                ! Terminate IF

    IF SUB(lpszFilename,1,3) <> 'COM' AND |                                             ! If user prints to a file
       SUB(lpszFilename,1,3) <> 'LPT' AND |                                            
       SUB(lpszFilename,1,2) <> '\\'
       IF _llseek(hf,0,2) = -1 THEN RETURN(4).                                          ! Set file pointer to the end of the file (Append lines)
    END                                                                                 ! Terminate IF

    BytesWritten = _lwrite(hf,hpvBuffer,cbBuffer)                                       ! Write to the file
    IF BytesWritten < cbBuffer THEN RETURN(WriteError).                                 ! IF Writing to a device or file is not possible, return Write Error
    IF _lclose(hf) THEN RETURN(CloseError) ELSE RETURN(Succeeded).                      ! If error in closing device or a file, return CloseError otherwise return Succeeded


DeleteFile FUNCTION(STRING FileToDelete)                                                ! DeleteFile Function

    CODE                                                                                ! Function Code Starts Here
    lpszFilename = CLIP(FileToDelete)                                                   ! Put file name in the buffer
    hf = OpenFile(lpszFilename,OF_STRUCT,OF_DELETE)                                     ! and delete it
     IF hf = -1 THEN RETURN(OpenError) ELSE RETURN(Succeeded).                          ! If error, return OpenError otherwise Return Succeeded

!* * * * Line Print Template Generated Code (End) * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 


