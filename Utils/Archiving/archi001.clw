

   MEMBER('archiving.clw')                            ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ARCHI001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

tmp:OldPath          STRING(255)
tmp:CurrentPath      STRING(255)
tmp:StartDate        DATE
tmp:EndDate          DATE
tmp:StartRunningDate DATE
tmp:StartRunningTime TIME
window               WINDOW('Archive Process'),AT(,,327,150),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       GROUP('Location Of Old Data Files'),AT(4,4,320,32),USE(?Group1),BOXED
                         PROMPT('Data File Path'),AT(8,18),USE(?Prompt1),TRN
                         ENTRY(@s255),AT(64,18,240,10),USE(tmp:OldPath),FONT('MS Sans Serif',,,)
                         BUTTON('...'),AT(308,18,12,10),USE(?LookupFile)
                       END
                       GROUP('Current Location Of Live Data Files'),AT(4,46,320,32),USE(?Group2),BOXED,TRN
                         STRING(@s255),AT(8,59,312,11),USE(tmp:CurrentPath),TRN,CENTER,FONT(,,0404080H,,CHARSET:ANSI)
                       END
                       GROUP('Booking Date Range'),AT(4,82,320,44),USE(?Group3),BOXED
                         PROMPT('Start Date'),AT(9,92),USE(?tmp:StartDate:Prompt)
                         ENTRY(@D06b),AT(64,92,64,10),USE(tmp:StartDate),FONT('MS Sans Serif',,,)
                         PROMPT('End Date'),AT(8,108),USE(?tmp:EndDate:Prompt)
                         ENTRY(@d06b),AT(64,110,64,10),USE(tmp:EndDate),FONT('MS Sans Serif',,,)
                       END
                       BUTTON('Cancel'),AT(268,130,56,16),USE(?Cancel)
                       PROMPT(''),AT(4,132),USE(?Prompt:Status)
                       BUTTON('Begin Routine'),AT(208,130,56,16),USE(?Button:BeginRoutine)
                     END

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:SkipCount          Byte(0)
Prog:SkipNumber         Long()
Prog:PercentText        String(100)
Prog:UserString         String(100)
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING(@s100),AT(0,3,161,10),USE(Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(0,32,161,10),USE(Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),HIDE,LEFT,ICON('cancel.ico')
     END
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FileLookup1          SelectFileClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Prog:SkipCount = 0
    Prog:PercentText = '< 1% Completed'
    Prog:UserString = 'Working...'
    Prog:SkipNumber = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            Prog:PercentText = Format(Prog:PercentProgress,@n3) & '% Completed'
        Else
            Prog:PercentText = ''
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer

    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    Prog:PercentText = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  tmp:CurrentPath = Path()
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  OPEN(window)
  SELF.Opened=True
  FileLookup1.Init
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:LongName)
  FileLookup1.Flags=BOR(FileLookup1.Flags,FILE:Directory)
  FileLookup1.SetMask('All Files','*.*')
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupFile
      ThisWindow.Update
      tmp:OldPath = Upper(FileLookup1.Ask(1)  )
      DISPLAY
    OF ?Button:BeginRoutine
      ThisWindow.Update
      If Clip(tmp:OldPath) = ''
          Select(?tmp:OldPath)
          Cycle
      End ! If Clip(tmp:OldPath) = ''
      If tmp:StartDate = ''
          Select(?tmp:StartDate)
          Cycle
      End ! If tmp:StartDate = ''
      If tmp:EndDate = ''
          Select(?tmp:EndDate)
          Cycle
      End ! If tmp:EndDate = ''
      
      Beep(Beep:SystemQuestion)  ;  Yield()
      Case Message('The routine will copy date from '&|
          '||' & Clip(tmp:OldPath) & ''&|
          '|'&|
          '|to '&|
          '|'&|
          '|' & Clip(tmp:CurrentPath) & ''&|
          '|'&|
          '|Are you sure you want to continue?','Archive',|
                     Icon:Question,'&Yes|&No',2) 
          Of 1 ! &Yes Button
          Of 2 ! &No Button
              Cycle
      End!Case Message
      
      tmp:StartRunningDate = Today()
      tmp:StartRunningTime = Clock()
      
      
      Relate:JOBS.Open()
      Relate:AUDIT.Open()
      Relate:AUDSTAEX.Open()
      Relate:AUDSTATS.Open()
      Relate:CONTHIST.Open()
      Relate:JOBSE.Open()
      Relate:JOBNOTES.Open()
      
      If Sub(Clip(tmp:OldPath),-1,1) <> '\'
          tmp:OldPath = Clip(tmp:OldPath) & '\'
      End ! If Sub(tmp:OldPath,-1,1) = '\'
      
      glo:oldaudit        = Clip(tmp:OldPath) & 'AUDIT.DAT'
      glo:oldaudstaex     = Clip(tmp:OldPath) & 'AUDSTAEX.DAT'
      glo:oldaudstats     = Clip(tmp:OldPath) & 'AUDSTATS.DAT'
      glo:oldconthist     = Clip(tmp:OldPath) & 'CONTHIST.DAT'
      glo:oldjobnotes     = Clip(tmp:OldPath) & 'JOBNOTES.DAT'
      glo:oldjobs         = Clip(tmp:OldPath) & 'JOBS.DAT'
      glo:oldjobse        = Clip(tmp:OldPath) & 'JOBSE.DAT'
      
      Relate:oldJOBS.Open()
      Relate:oldAUDIT.Open()
      Relate:oldAUDSTAEX.Open()
      Relate:oldAUDSTATS.Open()
      Relate:oldCONTHIST.Open()
      Relate:oldJOBSE.Open()
      Relate:oldJOBNOTES.Open()
      
      Stream(JOBS)
      Stream(JOBSE)
      Stream(JOBNOTES)
      Stream(AUDSTATS)
      Stream(CONTHIST)
      Stream(AUDSTAEX)
      Stream(AUDIT)
      Stream(oldJOBS)
      Stream(oldJOBSE)
      Stream(oldJOBNOTES)
      Stream(oldAUDSTATS)
      Stream(oldCONTHIST)
      Stream(oldAUDSTAEX)
      Stream(oldAUDIT)
      
      
      Do Prog:ProgressSetup
      Prog:TotalRecords = Records(OLDJOBS)
      Prog:ShowPercentage = 1 !Show Percentage Figure
      Prog:SkipNumber = 100
      Access:oldJOBS.Clearkey(oldjob:Date_Booked_Key)
      oldjob:Date_Booked = tmp:StartDate
      Set(oldjob:Date_Booked_Key,oldjob:Date_Booked_Key)
      Accept
          Case Event()
          Of Event:Timer
              Loop 100 Times
                  !Inside Loop
                  If Access:oldJOBS.Next()
                      Prog:Exit = 1
                      Break
                  End ! If Access:JOBS.Next()
                  If oldjob:Date_Booked > tmp:EndDate
                      Prog:Exit = 1
                      Break
                  End ! If job:Date_Completed > tmp:EndDate
      
                  Do Prog:UpdateScreen
                  Prog:UserString = 'Records Updated: ' & Prog:RecordCount & ' / ' & Prog:TotalRecords
      
                  Access:JOBS.ClearKey(job:Ref_Number_Key)
                  job:Ref_Number = oldjob:Ref_Number
                  If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      Cycle
                  Else ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                  End ! If Access:JOBS.TryFetch(job:Ref_Number_Key) = Level:Benign
      
                  Prog:RecordCount += 1
      
                  If Access:JOBS.PrimeRecord() = Level:Benign
                      job:Record :=: oldjob:Record
                      If Access:JOBS.TryInsert() = Level:Benign
                          !Insert
      !                    LinePrint(job:Ref_Number,'JobsInserted.log')
                      Else ! If Access:JOBS.TryInsert() = Level:Benign
      !                    LinePrint(job:Ref_Number & ' *FAILED*','JobsInserted.log')
                          Access:JOBS.CancelAutoInc()
                      End ! If Access:JOBS.TryInsert() = Level:Benign
                  End ! If Access.JOBS.PrimeRecord() = Level:Benign
      
                  Access:OLDAUDIT.Clearkey(oldaud:Ref_Number_Key)
                  oldaud:Ref_Number = oldjob:Ref_Number
                  Set(oldaud:Ref_Number_Key,oldaud:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:OLDAUDIT.Next()
                          Break
                      End ! If Access:OLDAUDIT.Next()
                      If oldaud:Ref_Number <> oldjob:Ref_Number
                          Break
                      End ! If oldaud:Ref_Number <> oldjob:Ref_Number
                      If Access:AUDIT.PrimeRecord() = Level:Benign
                          aud:Record :=: oldaud:Record
                          If Access:AUDIT.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:AUDIT.TryInsert() = Level:Benign
                              Access:AUDIT.CancelAutoInc()
                          End ! If Access:AUDIT.TryInsert() = Level:Benign
                      End ! If Access.AUDIT.PrimeRecord() = Level:Benign
                  End ! Loop
      
                  Access:OLDAUDSTAEX.Clearkey(oldaux:RefNumberKey)
                  oldaux:RefNumber = oldjob:Ref_Number
                  Set(oldaux:RefNumberKey,oldaux:RefNumberKey)
                  Loop ! Begin Loop
                      If Access:OLDAUDSTAEX.Next()
                          Break
                      End ! If Access:OLDAUDSTAEX.Next()
                      If oldaux:RefNumber <> oldjob:Ref_Number
                          Break
                      End ! If oldaux:RefNumber <> oldjob:Ref_Number
                      If Access:AUDSTAEX.PrimeRecord() = Level:Benign
                          aux:Record :=: oldaux:Record
                          If Access:AUDSTAEX.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:AUDSTAEX.TryInsert() = Level:Benign
                              Access:AUDSTAEX.CancelAutoInc()
                          End ! If Access:AUDSTAEX.TryInsert() = Level:Benign
                      End ! If Access.AUDSTAEX.PrimeRecord() = Level:Benign
                  End ! Loop
      
                  Access:OLDAUDSTATS.Clearkey(oldaus:DateChangedKey)
                  oldaus:RefNumber = oldjob:Ref_Number
                  Set(oldaus:DateChangedKey,oldaus:DateChangedKey)
                  Loop ! Begin Loop
                      If Access:OLDAUDSTATS.Next()
                          Break
                      End ! If Access:OLDAUDSTATS.Next()
                      If oldaus:RefNumber <> oldjob:Ref_Number
                          Break
                      End ! If oldaus:RefNumber <> oldjob:Ref_Number
                      If Access:AUDSTATS.PrimeRecord() = Level:Benign
                          aus:Record :=: oldaus:Record
                          If Access:AUDSTATS.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:AUDSTATS.TryInsert() = Level:Benign
                              Access:AUDSTATS.CancelAutoInc()
                          End ! If Access:AUDSTATS.TryInsert() = Level:Benign
                      End ! If Access.AUDSTATS.PrimeRecord() = Level:Benign
                  End ! Loop
      
                  Access:OLDCONTHIST.Clearkey(oldcht:Ref_Number_Key)
                  oldcht:Ref_Number = oldjob:Ref_Number
                  Set(oldcht:Ref_Number_Key,oldcht:Ref_Number_Key)
                  Loop ! Begin Loop
                      If Access:OLDCONTHIST.Next()
                          Break
                      End ! If Access:OLDCONTHIST.Next()
                      If oldcht:Ref_Number <> oldjob:Ref_Number
                          Break
                      End ! If oldcht:Ref_Number <> oldjob:Ref_Number
                      If Access:CONTHIST.PrimeRecord() = Level:Benign
                          cht:Record :=: oldcht:Record
                          If Access:CONTHIST.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:CONTHIST.TryInsert() = Level:Benign
                              Access:CONTHIST.CancelAutoInc()
                          End ! If Access:CONTHIST.TryInsert() = Level:Benign
                      End ! If Access.CONTHIST.PrimeRecord() = Level:Benign
                  End ! Loop
      
                  Access:OLDJOBNOTES.Clearkey(oldjbn:RefNumberKey)
                  oldjbn:RefNumber = oldjob:Ref_Number
                  Set(oldjbn:RefNumberKey,oldjbn:RefNumberKey)
                  Loop ! Begin Loop
                      If Access:OLDJOBNOTES.Next()
                          Break
                      End ! If Access:OLDJOBNOTES.Next()
                      If oldjbn:RefNumber <> oldjob:Ref_Number
                          Break
                      End ! If oldjbn:RefNumber <> oldjob:Ref_Number
                      If Access:JOBNOTES.PrimeRecord() = Level:Benign
                          jbn:Record :=: oldjbn:Record
                          If Access:JOBNOTES.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:JOBNOTES.TryInsert() = Level:Benign
                              Access:JOBNOTES.CancelAutoInc()
                          End ! If Access:JOBNOTES.TryInsert() = Level:Benign
                      End ! If Access.JOBNOTES.PrimeRecord() = Level:Benign
                  End ! Loop
      
                  Access:oldJOBSE.Clearkey(oldjobe:RefNumberKey)
                  oldjobe:RefNumber = oldjob:Ref_Number
                  Set(oldjobe:RefNumberKey,oldjobe:RefNumberKey)
                  Loop ! Begin Loop
                      If Access:oldJOBSE.Next()
                          Break
                      End ! If Access:JOBSE.Next()
                      If oldjobe:RefNumber <> oldjob:Ref_Number
                          Break
                      End ! If oldjobe:RefNumber <> oldjob:Ref_Number
                      If Access:JOBSE.PrimeRecord() = Level:Benign
                          jobe:Record :=: oldjobe:Record
                          If Access:JOBSE.TryInsert() = Level:Benign
                              !Insert
                          Else ! If Access:JOBSE.TryInsert() = Level:Benign
                              Access:JOBSE.CancelAutoInc()
                          End ! If Access:JOBSE.TryInsert() = Level:Benign
                      End ! If Access.JOBSE.PrimeRecord() = Level:Benign
                  End ! Loop
      
              End ! Loop 25 Times
          Of Event:CloseWindow
              Prog:Exit = 1
              Prog:Cancelled = 1
              Break
          Of Event:Accepted
              If Field() = ?Prog:Cancel
                  Beep(Beep:SystemQuestion)  ;  Yield()
                  Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                  Of 1 ! Yes
                      Prog:Exit = 1
                      Prog:Cancelled = 1
                      Break
                  Of 2 ! No
                  End ! Case Message
              End! If FIeld()
          End ! Case Event()
          If Prog:Exit
              Break
          End ! If Prog:Exit
      End ! Accept
      Do Prog:ProgressFinished
      
      
      
      Flush(JOBS)
      Flush(JOBSE)
      Flush(JOBNOTES)
      Flush(AUDSTATS)
      Flush(CONTHIST)
      Flush(AUDSTAEX)
      Flush(AUDIT)
      Flush(oldJOBS)
      Flush(oldJOBSE)
      Flush(oldJOBNOTES)
      Flush(oldAUDSTATS)
      Flush(oldCONTHIST)
      Flush(oldAUDSTAEX)
      Flush(oldAUDIT)
      
      Relate:JOBS.Close()
      Relate:AUDIT.Close()
      Relate:AUDSTAEX.Close()
      Relate:AUDSTATS.Close()
      Relate:CONTHIST.Close()
      Relate:JOBSE.Close()
      Relate:JOBNOTES.Close()
      Relate:oldJOBS.Close()
      Relate:oldAUDIT.Close()
      Relate:oldAUDSTAEX.Close()
      Relate:oldAUDSTATS.Close()
      Relate:oldCONTHIST.Close()
      Relate:oldJOBSE.Close()
      Relate:oldJOBNOTES.Close()
      
      Beep(Beep:SystemAsterisk)  ;  Yield()
      Case Message('Finished.'&|
          '|'&|
          '|Started: ' & Clip(Format(tmp:StartRunningDate,@d06) & ' ' & Format(tmp:StartRunningTime,@t01)) & ''&|
          '|'&|
          '|Finished: ' & Clip(Format(Today(),@d06) & ' ' & Format(Clock(),@t01)) & ''&|
          '|'&|
          '|Jobs Copied: ' & Clip(Prog:RecordCount) & '','Archive',|
                     Icon:Asterisk,'&OK',1) 
          Of 1 ! &OK Button
      End!Case Message
      
      Post(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

