

   MEMBER('archiving.clw')                            ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('ARCHI002.INC'),ONCE        !Local module procedure declarations
                     END


XFiles PROCEDURE                                      !Generated from procedure template - Window

window               WINDOW('Caption'),AT(,,260,100),GRAY,DOUBLE
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('XFiles')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:AUDIT.Open
  Relate:AUDSTAEX.Open
  Relate:AUDSTATS.Open
  Relate:CONTHIST.Open
  Relate:JOBNOTES.Open
  Relate:JOBS.Open
  Relate:JOBSE.Open
  Relate:oldAUDIT.Open
  Relate:oldAUDSTAEX.Open
  Relate:oldAUDSTATS.Open
  Relate:oldCONTHIST.Open
  Relate:oldJOBNOTES.Open
  Relate:oldJOBS.Open
  Relate:oldJOBSE.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:AUDSTAEX.Close
    Relate:AUDSTATS.Close
    Relate:CONTHIST.Close
    Relate:JOBNOTES.Close
    Relate:JOBS.Close
    Relate:JOBSE.Close
    Relate:oldAUDIT.Close
    Relate:oldAUDSTAEX.Close
    Relate:oldAUDSTATS.Close
    Relate:oldCONTHIST.Close
    Relate:oldJOBNOTES.Close
    Relate:oldJOBS.Close
    Relate:oldJOBSE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

