

   MEMBER('FixJobNotes.clw')                          ! This is a MEMBER module

                     MAP
                       INCLUDE('FIXJO001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
ConnectionStr        STRING(255),STATIC
iniFilePath         string(255)

WEBJOB               FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(WEB),BINDABLE,THREAD
UK_Ref_Number            KEY(WEB:Ref_Number),NAME('UK_Ref_Number'),PRIMARY
EXPORTEDKEY              KEY(WEB:Exported), DUP
SBJOBNOKEY               KEY(WEB:SBJobNo), DUP
PROCESSIDKEY             KEY(WEB:ProcessID), DUP
RETSTOREBOOKKEY          KEY(WEB:RetStoreCode, WEB:BookDate), DUP
ACCOUNTNOSTATUSKEY       KEY(WEB:AccountNo, WEB:StatusChange), DUP
BOOKTYPEDATEKEY          KEY(WEB:BookingType, WEB:BookDate), DUP
TYPERETSTOREBOOKKEY      KEY(WEB:BookingType, WEB:RetStoreCode, WEB:BookDate), DUP
TYPEACCNOSTATUSKEY       KEY(WEB:BookingType, WEB:AccountNo, WEB:StatusChange), DUP
TYPERETURNBOOKKEY        KEY(WEB:BookingType, WEB:UnitReturned, WEB:BookDate), DUP
TYPERETURNDATEKEY        KEY(WEB:BookingType, WEB:UnitReturned, WEB:DateUnitReturned), DUP
ESNKEY                   KEY(WEB:ESN), DUP
CPIMEIKEY                KEY(WEB:CPIMEI, WEB:BookDate), DUP
SMSALERTNOKEY            KEY(WEB:SMSAlertNo), DUP
CUSTCOLLECTDATEKEY       KEY(WEB:CustCollectionDate), DUP
SENDSMSKEY               KEY(WEB:SMSType, WEB:SMSReq, WEB:SMSSent, WEB:Ref_Number), DUP
SENDEMAILKEY             KEY(WEB:SMSType, WEB:EmailReq, WEB:EmailSent, WEB:Ref_Number), DUP
MOBILENOKEY              KEY(WEB:Mobile_Number), DUP
BOOKDATEKEY              KEY(WEB:BookDate), DUP
CPALLOCBOOKKEY           KEY(WEB:CPAllocated, WEB:BookDate), DUP
CPONLYALLOCBOOKKEY       KEY(WEB:CPOnly, WEB:CPAllocated, WEB:BookDate), DUP
COMPCUSTCOLLDATEKEY      KEY(WEB:Completed, WEB:CustCollectionDate), DUP
Record                   RECORD,PRE()
Ref_Number                  LONG
BookTime                    TIME
BookDate                    DATE
ESN                         STRING(20)
Title                       STRING(4)
Initial                     STRING(1)
Surname                     STRING(30)
Telephone_Number            STRING(15)
PostCode                    STRING(10)
Company_Name                STRING(30)
Address_Line1               STRING(30)
Address_Line2               STRING(30)
Address_Line3               STRING(30)
Mobile_Number               STRING(15)
Manufacturer                STRING(30)
Model_Number                STRING(30)
Colour                      STRING(30)
Phone_Lock                  STRING(30)
PrimaryFault                STRING(255)
FreeTextFault               STRING(255)
Intermittent_Fault          BYTE
Battery                     BYTE
Charger                     BYTE
BatteryCover                BYTE
None                        BYTE
LoanESN                     STRING(20)
LoanMake                    STRING(30)
LoanModel                   STRING(30)
LoanBattery                 BYTE
LoanCharger                 BYTE
ExportTime                  TIME
ExportDate                  DATE
Exported                    BYTE
RepairType                  STRING(2)
DateOfPurchase              DATE
InsuranceRefNo              STRING(30)
AccountNo                   STRING(15)
BookedBy                    STRING(30)
SBJobNo                     LONG
CompanyNameCollect          STRING(30)
AddressL1Collect            STRING(30)
AddressL2Collect            STRING(30)
AddressL3Collect            STRING(30)
PostcodeCollect             STRING(10)
TelNoCollect                STRING(15)
CompanyNameDelivery         STRING(30)
AddressL1Delivery           STRING(30)
AddressL2Delivery           STRING(30)
AddressL3Delivery           STRING(30)
PostcodeDelivery            STRING(10)
TelNoDelivery               STRING(15)
UnitCondition               STRING(255)
StoreNotes                  STRING(255)
StatusChange                STRING(30)
ProcessID                   LONG
ContractPeriod              LONG
Reason                      STRING(255)
FaxNo                       STRING(15)
TitleCollect                STRING(4)
InitialCollect              STRING(1)
SurnameCollect              STRING(30)
TitleDelivery               STRING(4)
InitialDelivery             STRING(1)
SurnameDelivery             STRING(30)
EmailAddress                STRING(255)
RetStoreCode                STRING(15)
SMSReq                      LONG
SMSSent                     LONG
LoanBoxNumber               STRING(20)
BookingType                 STRING(1)
CollectionMadeToday         BYTE
UseCustCollectDate          BYTE
SysCollectionDate           DATE
CustCollectionDate          DATE
SMSType                     STRING(1)
CPOnly                      BYTE
CPIssueReason               STRING(30)
CPExplanation               STRING(255)
CPAllocated                 STRING(1)
CPIMEI                      STRING(20)
CPAccBattery                STRING(1)
CPAccCharger                STRING(1)
CPAccHeadset                STRING(1)
CPAccGuide                  STRING(1)
CPAccPackaging              STRING(1)
MemoryCard                  BYTE
LoanManual                  BYTE
UnitReturned                BYTE
DateUnitReturned            DATE
DeviceCondition             STRING(40)
CPReturnDate                DATE
CPDepositValue              PDECIMAL(8, 2)
CPDepositSKUCode            STRING(30)
CPDeposit                   STRING(1)
CPReturnDateReason          STRING(255)
EmailReq                    BYTE
ValueSegment                STRING(20)
Contract                    BYTE
Country                     STRING(20)
CountryCollect              STRING(20)
CountryDelivery             STRING(20)
StatusAgentName             STRING(30)
SMSAlertNo                  STRING(15)
EmailSent                   BYTE
Completed                   BYTE
                         END
                     END  
  CODE
   Relate:JOBNOTES.Open
    iniFilePath =  clip(path()) & '\Webimp.ini'
    ConnectionStr = getini('Defaults', 'Connection', '', clip(iniFilePath))

    open(WEBJOB)
    if (errorcode())
        if errorcode() = 90
            message('Error Opening WEBJOB : ' & fileerrorcode() & ' - ' & fileerror())
        else
            message('Error Opening WEBJOB : ' & error())
        end
        Return
    end

    count# = 0
    Loop x# = 4732148  To 4732242
        web:SBJobNo = x#
        get(WEBJOB, web:SBJobNoKey)
        if error() then cycle.

        Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
        jbn:RefNumber    = x#
        if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
            ! Found
        else ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)
            ! Error
            Cycle
        end ! if (Access:JOBNOTES.TryFetch(jbn:RefNumberKey) = Level:Benign)

        jbn:ColContatName   = clip(web:TitleCollect)
        if clip(web:InitialCollect) <> ''
            if clip(jbn:ColContatName) <> ''
                jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:InitialCollect
            else
                jbn:ColContatName = web:InitialCollect
            end
        end
        if clip(web:SurnameCollect) <> ''
            if clip(jbn:ColContatName) <> ''
                jbn:ColContatName = clip(jbn:ColContatName) & ' ' & web:SurnameCollect
            else
                jbn:ColContatName = web:SurnameCollect
            end
        end

        jbn:DelContactName  = clip(web:TitleDelivery)
        if clip(web:InitialDelivery) <> ''
            if clip(jbn:DelContactName) <> ''
                jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:InitialDelivery
            else
                jbn:DelContactName = web:InitialDelivery
            end
        end
        if clip(web:SurnameDelivery) <> ''
            if clip(jbn:DelContactName) <> ''
                jbn:DelContactName = clip(jbn:DelContactName) & ' ' & web:SurnameDelivery
            else
                jbn:DelContactName = web:SurnameDelivery
            end
        end

        !IF CLIP(UPPER(web:PrimaryFault)) <> '25 OTHER - SEE COMMENTS'
        jbn:Fault_Description = web:PrimaryFault
        !END

        if job:Intermittent_Fault = 'YES'
            if jbn:Fault_Description = ''
                jbn:Fault_Description = '(INTERMITTENT FAULT)'
            else
                jbn:Fault_Description = clip(jbn:Fault_Description) & ' (INTERMITTENT FAULT)'
            end
        end

        !jbn:Engineers_Notes = web:FreeTextFault
        ! Change o' spec 29 July 2004
        if web:FreeTextFault <> ''
            if jbn:Fault_Description = ''
                jbn:Fault_Description = clip(web:FreeTextFault)
            else
                jbn:Fault_Description = clip(jbn:Fault_Description) & ' ' & clip(web:FreeTextFault)
            end
        end

        If Access:JOBNOTES.TryUpdate() = Level:Benign
            count# += 1
        end ! If Access:JOBNOTES.TryUpdate() = Level:Benign


    end ! 4733671

    close(WebJob)

    Message('Updated: ' & count#)
   Relate:JOBNOTES.Close
