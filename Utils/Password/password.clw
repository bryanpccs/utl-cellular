   PROGRAM



   INCLUDE('ABERROR.INC'),ONCE
   INCLUDE('ABFUZZY.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ERRORS.CLW'),ONCE
   INCLUDE('KEYCODES.CLW'),ONCE

   MAP
     MODULE('Windows API')
SystemParametersInfo PROCEDURE (LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
     END
     MODULE('PASSWBC.CLW')
DctInit     PROCEDURE                                      ! Initializes the dictionary definition module
DctKill     PROCEDURE                                      ! Kills the dictionary definition module
     END
!--- Application Global and Exported Procedure Definitions --------------------------------------------
     MODULE('PASSW001.CLW')
Main                   PROCEDURE   !
     END
         module('Win32')
             GetSystemMetrics(long nIndex), long, pascal
             GetWindowLong(long hWnd, long nIndex), long, pascal, name('GetWindowLongA')
             SetWindowLong(long hWnd, long nIndex, long dwNewLong), long, pascal, proc, name('SetWindowLongA')
             SetWindowPos(long hWnd, long hWndInsertAfter, long X, long Y, long cx, long cy, long uFlags), bool, pascal, proc
             SystemParametersInfo(unsigned uiAction, unsigned uiParam, long pvParam, unsigned fWinIni), bool, pascal, proc, raw, name('SystemParametersInfoA')
             GetWindowRect(long hWnd, long pvParam), long, pascal
             CreateSemaphore(<*SECURITY_ATTRIBUTES lpSemaphoreAttributes>, LONG lInitialCount, LONG lMaximumCount, |
                              *CSTRING lpName), LONG, RAW, PASCAL, NAME('CreateSemaphoreA')
             GetLastError(),LONG,PASCAL
         end
   END

SilentRunning        BYTE(0)                               ! Set true when application is running in 'silent mode'


! Program equates

! System metrics
SM_CXSCREEN      equate(0)
SM_CYSCREEN      equate(1)

HWND_BOTTOM         EQUATE(1)   !Put the window at the bottom of the Z-order.
HWND_NOTOPMOST      EQUATE(-2)  !Put the window below all topmost windows and above all non-topmost windows.
HWND_TOP            EQUATE(0)   !Put the window at the top of the Z-order.
HWND_TOPMOST        EQUATE(-1)  !Make the window topmost (above all other windows) permanently.

SWP_NOREPOSITION    EQUATE(0200h)
SWP_DRAWFRAME       EQUATE(020h)  ! Same as SWP_FRAMECHANGED.
SWP_FRAMECHANGED    EQUATE(020h)  ! Fully redraw the window in its new position.
SWP_HIDEWINDOW      EQUATE(080h)  ! Hide the window from the screen.
SWP_NOACTIVATE      EQUATE(010h)  ! Do not make the window active after moving it unless it was already the active window.
SWP_NOCOPYBITS      EQUATE(0100h) ! Do not redraw anything drawn on the window after it is moved.
SWP_NOMOVE          EQUATE(02h)   ! Do not move the window.
SWP_NOSIZE          EQUATE(01h)   ! Do not resize the window.
SWP_NOREDRAW        EQUATE(08h)   ! Do not remove the image of the window in its former position, effectively leaving a ghost image on the screen.
SWP_NOZORDER        EQUATE(04h)   ! Do not change the window's position in the Z-order.
SWP_SHOWWINDOW      EQUATE(040h)  ! Show the window if it is hidden.

! Window extended styles
GWL_EXSTYLE      equate(-20)

WS_EX_TOOLWINDOW equate(080h)

! System parameters
SPI_SETWORKAREA  equate(47)
SPI_GETWORKAREA  equate(48)

SPIF_SENDCHANGE  equate(02h)



SECURITY_ATTRIBUTES     GROUP,TYPE
nLength                   LONG
lpSecurityDescriptor      LONG
bInheritHandle            LONG
                        END

!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------
TinToolsViewRecord  EQUATE(15)
TinToolsCopyRecord  EQUATE(16)
!--------------------------------------------------------------------------
! -----------Tintools-------------
!--------------------------------------------------------------------------

FuzzyMatcher         FuzzyClass                            ! Global fuzzy matcher
GlobalErrorStatus    ErrorStatusClass,THREAD
GlobalErrors         ErrorClass                            ! Global error manager
INIMgr               INIClass                              ! Global non-volatile storage manager
GlobalRequest        BYTE(0),THREAD                        ! Set when a browse calls a form, to let it know action to perform
GlobalResponse       BYTE(0),THREAD                        ! Set to the response from the form
VCRRequest           LONG(0),THREAD                        ! Set to the request from the VCR buttons

Dictionary           CLASS,THREAD
Construct              PROCEDURE
Destruct               PROCEDURE
                     END

lCurrentFDSetting    LONG                                  ! Used by window frame dragging
lAdjFDSetting        LONG                                  ! ditto

  CODE
  GlobalErrors.Init(GlobalErrorStatus)
  FuzzyMatcher.Init                                        ! Initilaize the browse 'fuzzy matcher'
  FuzzyMatcher.SetOption(MatchOption:NoCase, 1)            ! Configure case matching
  FuzzyMatcher.SetOption(MatchOption:WordOnly, 0)          ! Configure 'word only' matching
  INIMgr.Init('password.INI', NVD_INI)                     ! Configure INIManager to use INI file
  DctInit
  SystemParametersInfo (38, 0, lCurrentFDSetting, 0)       ! Configure frame dragging
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 0, lAdjFDSetting, 3)
  END
  Main
  INIMgr.Update
  IF lCurrentFDSetting = 1
    SystemParametersInfo (37, 1, lAdjFDSetting, 3)
  END
  INIMgr.Kill                                              ! Destroy INI manager
  FuzzyMatcher.Kill                                        ! Destroy fuzzy matcher


Dictionary.Construct PROCEDURE

  CODE
  IF THREAD()<>1
     DctInit()
  END


Dictionary.Destruct PROCEDURE

  CODE
  DctKill()

