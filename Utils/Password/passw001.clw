

   MEMBER('password.clw')                                  ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('PASSW001.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                             ! Generated from procedure template - Window

tmp:AccessNo         STRING(20)                            !
tmp:Password         STRING(20)                            !
SBversion43          BYTE                                  !
window               WINDOW('Password'),AT(,,179,91),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),ICON('KEY.ICO'),WALLPAPER('wall2.jpg'),DOUBLE,IMM
                       PROMPT('SB2000 No.:'),AT(4,32),USE(?Prompt1),TRN,FONT(,10,,)
                       PROMPT('Password:'),AT(4,46),USE(?Prompt2),TRN,FONT(,10,,)
                       TEXT,AT(56,32,112,12),USE(tmp:AccessNo),BOXED,TRN,FONT('Tahoma',10,COLOR:Black,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),SINGLE
                       TEXT,AT(56,46,112,12),USE(tmp:Password),BOXED,TRN,FONT('Tahoma',10,COLOR:Red,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),SINGLE,READONLY
                       PROMPT(''),AT(56,60,112,10),USE(?Prompt:Status),TRN,CENTER,FONT(,,COLOR:Gray,FONT:italic)
                       CHECK('Vodacom / UK Cellular (Pre 4.4)'),AT(56,22),USE(SBversion43),TRN,FONT(,,COLOR:Black,,CHARSET:ANSI),VALUE('1','0')
                       BUTTON('Get SB2000 No from Clipboard'),AT(52,4,120,14),USE(?PasteButton),TRN,FLAT,LEFT,FONT(,,COLOR:Navy,FONT:underline),ICON('wok.ico'),DEFAULT
                       BUTTON('Clear Fields'),AT(64,74,64,14),USE(?ClearButton),TRN,FLAT,LEFT,FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI),ICON('wdelete.ico')
                       BUTTON('Close'),AT(132,74,43,15),USE(?Close),TRN,FLAT,LEFT,FONT(,,COLOR:Navy,FONT:underline,CHARSET:ANSI),ICON('wclose.ico')
                       BOX,AT(0,72,180,20),USE(?Box:BottomBar),COLOR(0F0F0F0H),FILL(0F0F0F0H)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
Kill                   PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED       ! Method added to host embed code
                     END

Toolbar              ToolbarClass

  CODE
  GlobalResponse = ThisWindow.Run()                        ! Opens the window and starts an Accept Loop

!---------------------------------------------------------------------------
DefineListboxStyle ROUTINE
!|
!| This routine create all the styles to be shared in this window
!| It's called after the window open
!|
!---------------------------------------------------------------------------
SetPassword    ROUTINE
    IF (tmp:AccessNo <> '') THEN
        day#    = Sub(tmp:AccessNo,1,5)
        time#   = Sub(tmp:AccessNo,6,20)

        ! Start Change 4780 BE(26/10/2004)
        IF (SBVersion43) THEN
            ! Pre Version 4.4
            tmp:Password = INT(((day# * 3)/23) + ((time# * 2)/7))
        ELSE
            ! Version 4.4 +
            tmp:Password = INT(((day# * 3)/19) + ((time# * 2)/9))
        END
        ! End Change 4780 BE(26/10/2004)
        SetClipboard(tmp:Password)
        ?Prompt:Status{prop:Text} = 'Password Saved To Clipboard'
    END

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest                             ! Store the incoming request
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors                              ! Set this windows ErrorManager to the global ErrorManager
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)                                     ! Clear GlobalRequest after storing locally
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)                    ! Add the close control to the window amanger
  SELF.Open(window)                                        ! Open window
  Do DefineListboxStyle
  INIMgr.Fetch('Main',window)                              ! Restore window settings from non-volatile store
  ! Set window to stay on top
  res# = SetWindowPos(0{Prop:Handle}, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE + SWP_NOSIZE)
  
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.Opened
    INIMgr.Update('Main',window)                           ! Save window data to non-volatile store
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receive all EVENT:Accepted's
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:AccessNo
      DO SetPassword
      SELECT(?tmp:Password)
      
    OF ?SBversion43
      DO SetPassword
      DISPLAY
    OF ?PasteButton
      ThisWindow.Update
      tmp:AccessNo = CLIPBOARD()
      DO SetPassword
      SETCLIPBOARD(tmp:Password)
      DISPLAY
    OF ?ClearButton
      ThisWindow.Update
      tmp:AccessNo = ''
      tmp:Password = ''
      SELECT(?tmp:AccessNo)
      DISPLAY
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP                                                     ! This method receives all Selected events
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeSelected()
    CASE FIELD()
    OF ?tmp:AccessNo
      ?Prompt:Status{prop:Text} = ''
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

