

   MEMBER('ShippingDateLocator.clw')                  ! This is a MEMBER module

                     MAP
                       INCLUDE('SHIPP001.INC'),ONCE        !Local module procedure declarations
                     END


Main                 PROCEDURE                        ! Declare Procedure
tmp:SavePath      CString(255)
tmp:ImportFile    Cstring(255),Static
tmp:ExportFile    CString(255),Static
ImportFile    File,Driver('ASCII'),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record              Record
IMEINumber          String(30)
                    End
                End
ExportFile    File,Driver('BASIC'),Pre(expfil),Name(tmp:ExportFile),Create,Bindable,Thread
Record              Record
IMEINumber          String(30)
ShipDate            String(30)
                    End
                End

! ** Progress Window Declaration **
Prog:TotalRecords       Long,Auto
Prog:RecordsProcessed   Long(0)
Prog:PercentProgress    Byte(0)
Prog:Thermometer        Byte(0)
Prog:Exit               Byte,Auto
Prog:Cancelled          Byte,Auto
Prog:ShowPercentage     Byte,Auto
Prog:RecordCount        Long,Auto
Prog:SkipCount          Byte(0)
Prog:SkipNumber         Long()
Prog:PercentText        String(100)
Prog:UserString         String(100)
Prog:ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1), |
         GRAY,DOUBLE
       PROGRESS,USE(Prog:Thermometer),AT(4,16,152,12),RANGE(0,100)
       STRING(@s100),AT(0,3,161,10),USE(Prog:UserString),CENTER,FONT('Tahoma',8,,)
       STRING(@s100),AT(0,32,161,10),USE(Prog:PercentText),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?Prog:Cancel),HIDE,LEFT,ICON('cancel.ico')
     END
  CODE
   Relate:IMEISHIP.Open
    Beep(Beep:SystemAsterisk)  ;  Yield()
    Case Message('Routine to locate Shipping Date in IMEISHIP file.'&|
        '|'&|
        '|Click ''OK'' to pick CSV file.','Shipping Date Locator',|
                   Icon:Asterisk,'&OK|&Cancel',2) 
        Of 1 ! &OK Button
            
            tmp:SavePath = Path()
            tmp:ImportFile = ''
            If FileDialog('Choose File',tmp:ImportFile,'CSV Files|*.csv',file:KeepDir + file:NoError + File:LongName)
                !Found
                tmp:ExportFile = Clip(tmp:ImportFile) & '_new.csv'
                Remove(tmp:ExportFile)
                Create(ExportFile)
                Open(ExportFile)

                SetPath(tmp:SavePath)

                Open(ImportFile)
                Set(ImportFile,0)

                Do Prog:ProgressSetup
                Prog:TotalRecords = Records(ImportFILE)
                Prog:ShowPercentage = 1 !Show Percentage Figure
                Prog:SkipNumber = 0

                Accept
                    Case Event()
                    Of Event:Timer
                        Loop 25 Times
                            !Inside Loop
                            Next(ImportFile)
                            If Error()
                                Prog:Exit = 1
                                Break
                            End ! If Access:EXPORTFILE.Next()
                            Do Prog:UpdateScreen
                            Prog:RecordCount += 1
                            Prog:UserString = 'Records Updated: ' & Prog:RecordCount & '/' & Prog:TotalRecords
                            Access:IMEISHIP.Clearkey(imei:IMEIKey)
                            imei:IMEINumber    = impfil:IMEINumber
                            If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
                                ! Found
                                expfil:IMEINumber = impfil:IMEINumber
                                
                                If imei:ShipDate > 0
                                    expfil:ShipDate = Format(imei:ShipDate,@d06)
                                Else ! If imei:ShipDate > 0
                                    expfil:ShipDate = 'SHIPPING DATE NOT FOUND'
                                End ! If imei:ShipDate > 0
                                Add(ExportFile)
                            Else ! If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign
                                ! Error
                                expfil:IMEINumber = impfil:IMEINumber
                                expfil:ShipDate = 'IMEI RECORD NOT FOUND'
                                Add(ExportFile)
                            End !If Access:IMEISHIP.TryFetch(imei:IMEIKey) = Level:Benign


                        End ! Loop 25 Times
                    Of Event:CloseWindow
                        Prog:Exit = 1
                        Prog:Cancelled = 1
                        Break
                    Of Event:Accepted
                        If Field() = ?Prog:Cancel
                            Beep(Beep:SystemQuestion)  ;  Yield()
                            Case Message('Are you sure you want to cancel?','Cancel Pressed',icon:Question,'&Yes|&No',2,2)
                            Of 1 ! Yes
                                Prog:Exit = 1
                                Prog:Cancelled = 1
                                Break
                            Of 2 ! No
                            End ! Case Message
                        End! If FIeld()
                    End ! Case Event()
                    If Prog:Exit
                        Break
                    End ! If Prog:Exit
                End ! Accept
                Do Prog:ProgressFinished

                Close(ExportFile)
                Close(IMportFile)
                Beep(Beep:SystemAsterisk)  ;  Yield()
                Case Message('New file created.'&|
                    '|'&|
                    '|' & Clip(tmp:ExportFile) & '.','Finished',|
                               Icon:Asterisk,'&OK',1) 
                    Of 1 ! &OK Button
                End!Case Message
            Else ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
                !Error
                SetPath(tmp:SavePath)
            End ! If FileDialog('Choose File',tmp:ExportFile,'CSV Files|*.csv|All Files|*.*',file:KeepDir + file:NoError + File:LongName)
        Of 2 ! &Cancel Button
            
    End!Case Message
   Relate:IMEISHIP.Close
! ** Progress Window Setup / Update / Finish Routine **
Prog:ProgressSetup      Routine
    Prog:Exit = 0
    Prog:Cancelled = 0
    Prog:RecordCount = 0
    Open(Prog:ProgressWindow)
    ?Prog:Cancel{prop:Hide} = 0
    0{prop:Timer} = 1

Prog:UpdateScreen       Routine
    Prog:RecordsProcessed += 1

    Prog:PercentProgress = (Prog:RecordsProcessed / Prog:TotalRecords) * 100

    IF Prog:PercentProgress > 100 Or Prog:PercentProgress < 0
        Prog:RecordsProcessed = 0
    End ! IF Prog:PercentProgress > 100

    IF Prog:PercentProgress <> Prog:Thermometer
        Prog:Thermometer = Prog:PercentProgress
        If Prog:ShowPercentage
            ?Prog:PercentText{prop:Hide} = False
            Prog:PercentText = Format(Prog:PercentProgress,@n3) & '% Completed'
        End ! If Prog:ShowPercentage
    End ! IF Prog.PercentProgress <> Prog:Thermometer
    Display()

Prog:ProgressFinished   Routine
    Prog:Thermometer = 100
    ?Prog:PercentText{prop:Hide} = False
    Prog:PercentText = 'Finished.'
    Close(Prog:ProgressWindow)
    Display()
