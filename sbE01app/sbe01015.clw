

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01015.INC'),ONCE        !Local module procedure declarations
                     END


UpdateCHARTYPE PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
pos                  STRING(255)
Charge_Type_Temp     STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::cha:Record  LIKE(cha:RECORD),STATIC
QuickWindow          WINDOW('Update the CHARTYPE File'),AT(,,220,196),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateCHARTYPE'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,160),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Charge Type'),AT(8,20),USE(?CHA:Charge_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(cha:Charge_Type),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Ref Number'),AT(8,36),USE(?CHA:Ref_Number:Prompt),TRN
                           ENTRY(@n3),AT(84,36,64,10),USE(cha:Ref_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,CAP
                           CHECK('Warranty Charge Type'),AT(84,52,89,12),USE(cha:Warranty),VALUE('YES','NO')
                           CHECK('Compulsory Fault Coding'),AT(84,64,108,12),USE(cha:Force_Warranty),VALUE('YES','NO')
                           CHECK('Exclude From EDI Process'),AT(84,76,100,12),USE(cha:Exclude_EDI),VALUE('YES','NO')
                           CHECK('Exclude From Invoicing'),AT(84,88),USE(cha:ExcludeInvoice),MSG('Exclude From Invoicing'),TIP('Exclude From Invoicing'),VALUE('1','0')
                           CHECK('Allow Physical Damage'),AT(84,104,91,12),USE(cha:Allow_Physical_Damage),VALUE('YES','NO')
                           CHECK('Allow Estimate'),AT(84,120,63,12),USE(cha:Allow_Estimate),VALUE('YES','NO')
                           CHECK('Force Estimate'),AT(84,132,63,12),USE(cha:Force_Estimate),VALUE('YES','NO')
                           CHECK('Zero Value Parts'),AT(84,148,,12),USE(cha:Zero_Parts),VALUE('YES','NO')
                         END
                       END
                       BUTTON('&OK'),AT(100,172,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,172,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,168,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_job_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?CHA:Charge_Type:Prompt{prop:FontColor} = -1
    ?CHA:Charge_Type:Prompt{prop:Color} = 15066597
    If ?cha:Charge_Type{prop:ReadOnly} = True
        ?cha:Charge_Type{prop:FontColor} = 65793
        ?cha:Charge_Type{prop:Color} = 15066597
    Elsif ?cha:Charge_Type{prop:Req} = True
        ?cha:Charge_Type{prop:FontColor} = 65793
        ?cha:Charge_Type{prop:Color} = 8454143
    Else ! If ?cha:Charge_Type{prop:Req} = True
        ?cha:Charge_Type{prop:FontColor} = 65793
        ?cha:Charge_Type{prop:Color} = 16777215
    End ! If ?cha:Charge_Type{prop:Req} = True
    ?cha:Charge_Type{prop:Trn} = 0
    ?cha:Charge_Type{prop:FontStyle} = font:Bold
    ?CHA:Ref_Number:Prompt{prop:FontColor} = -1
    ?CHA:Ref_Number:Prompt{prop:Color} = 15066597
    If ?cha:Ref_Number{prop:ReadOnly} = True
        ?cha:Ref_Number{prop:FontColor} = 65793
        ?cha:Ref_Number{prop:Color} = 15066597
    Elsif ?cha:Ref_Number{prop:Req} = True
        ?cha:Ref_Number{prop:FontColor} = 65793
        ?cha:Ref_Number{prop:Color} = 8454143
    Else ! If ?cha:Ref_Number{prop:Req} = True
        ?cha:Ref_Number{prop:FontColor} = 65793
        ?cha:Ref_Number{prop:Color} = 16777215
    End ! If ?cha:Ref_Number{prop:Req} = True
    ?cha:Ref_Number{prop:Trn} = 0
    ?cha:Ref_Number{prop:FontStyle} = font:Bold
    ?cha:Warranty{prop:Font,3} = -1
    ?cha:Warranty{prop:Color} = 15066597
    ?cha:Warranty{prop:Trn} = 0
    ?cha:Force_Warranty{prop:Font,3} = -1
    ?cha:Force_Warranty{prop:Color} = 15066597
    ?cha:Force_Warranty{prop:Trn} = 0
    ?cha:Exclude_EDI{prop:Font,3} = -1
    ?cha:Exclude_EDI{prop:Color} = 15066597
    ?cha:Exclude_EDI{prop:Trn} = 0
    ?cha:ExcludeInvoice{prop:Font,3} = -1
    ?cha:ExcludeInvoice{prop:Color} = 15066597
    ?cha:ExcludeInvoice{prop:Trn} = 0
    ?cha:Allow_Physical_Damage{prop:Font,3} = -1
    ?cha:Allow_Physical_Damage{prop:Color} = 15066597
    ?cha:Allow_Physical_Damage{prop:Trn} = 0
    ?cha:Allow_Estimate{prop:Font,3} = -1
    ?cha:Allow_Estimate{prop:Color} = 15066597
    ?cha:Allow_Estimate{prop:Trn} = 0
    ?cha:Force_Estimate{prop:Font,3} = -1
    ?cha:Force_Estimate{prop:Color} = 15066597
    ?cha:Force_Estimate{prop:Trn} = 0
    ?cha:Zero_Parts{prop:Font,3} = -1
    ?cha:Zero_Parts{prop:Color} = 15066597
    ?cha:Zero_Parts{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateCHARTYPE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateCHARTYPE',1)
    SolaceViewVars('pos',pos,'UpdateCHARTYPE',1)
    SolaceViewVars('Charge_Type_Temp',Charge_Type_Temp,'UpdateCHARTYPE',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateCHARTYPE',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateCHARTYPE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateCHARTYPE',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateCHARTYPE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CHA:Charge_Type:Prompt;  SolaceCtrlName = '?CHA:Charge_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Charge_Type;  SolaceCtrlName = '?cha:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CHA:Ref_Number:Prompt;  SolaceCtrlName = '?CHA:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Ref_Number;  SolaceCtrlName = '?cha:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Warranty;  SolaceCtrlName = '?cha:Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Force_Warranty;  SolaceCtrlName = '?cha:Force_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Exclude_EDI;  SolaceCtrlName = '?cha:Exclude_EDI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:ExcludeInvoice;  SolaceCtrlName = '?cha:ExcludeInvoice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Allow_Physical_Damage;  SolaceCtrlName = '?cha:Allow_Physical_Damage';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Allow_Estimate;  SolaceCtrlName = '?cha:Allow_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Force_Estimate;  SolaceCtrlName = '?cha:Force_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cha:Zero_Parts;  SolaceCtrlName = '?cha:Zero_Parts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Charge Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Charge Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateCHARTYPE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateCHARTYPE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CHA:Charge_Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(cha:Record,History::cha:Record)
  SELF.AddHistoryField(?cha:Charge_Type,1)
  SELF.AddHistoryField(?cha:Ref_Number,12)
  SELF.AddHistoryField(?cha:Warranty,11)
  SELF.AddHistoryField(?cha:Force_Warranty,2)
  SELF.AddHistoryField(?cha:Exclude_EDI,13)
  SELF.AddHistoryField(?cha:ExcludeInvoice,14)
  SELF.AddHistoryField(?cha:Allow_Physical_Damage,3)
  SELF.AddHistoryField(?cha:Allow_Estimate,4)
  SELF.AddHistoryField(?cha:Force_Estimate,5)
  SELF.AddHistoryField(?cha:Zero_Parts,10)
  SELF.AddUpdateFile(Access:CHARTYPE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CHARTYPE.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:CHARTYPE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?cha:Warranty{Prop:Checked} = True
    ENABLE(?CHA:Force_Warranty)
    ENABLE(?CHA:Exclude_EDI)
  END
  IF ?cha:Warranty{Prop:Checked} = False
    cha:Force_Warranty = 'NO'
    DISABLE(?CHA:Force_Warranty)
    DISABLE(?CHA:Exclude_EDI)
  END
  IF ?cha:Allow_Estimate{Prop:Checked} = True
    ENABLE(?CHA:Force_Estimate)
  END
  IF ?cha:Allow_Estimate{Prop:Checked} = False
    cha:Force_Estimate = 'NO'
    DISABLE(?CHA:Force_Estimate)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateCHARTYPE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    cha:Force_Warranty = 'NO'
    cha:Allow_Physical_Damage = 'NO'
    cha:Warranty = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?cha:Warranty
      IF ?cha:Warranty{Prop:Checked} = True
        ENABLE(?CHA:Force_Warranty)
        ENABLE(?CHA:Exclude_EDI)
      END
      IF ?cha:Warranty{Prop:Checked} = False
        cha:Force_Warranty = 'NO'
        DISABLE(?CHA:Force_Warranty)
        DISABLE(?CHA:Exclude_EDI)
      END
      ThisWindow.Reset
    OF ?cha:Allow_Estimate
      IF ?cha:Allow_Estimate{Prop:Checked} = True
        ENABLE(?CHA:Force_Estimate)
      END
      IF ?cha:Allow_Estimate{Prop:Checked} = False
        cha:Force_Estimate = 'NO'
        DISABLE(?CHA:Force_Estimate)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If thiswindow.request <> Insertrecord
      If cha:charge_type <> charge_type_temp
          Case MessageEx('Are you sure you want to change this Charge Type?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  setcursor(cursor:wait)
                  
                  save_job_id = access:jobs.savefile()
                  access:jobs.clearkey(job:chatypekey)
                  job:charge_type = charge_type_temp
                  set(job:chatypekey,job:chatypekey)
                  loop
                      if access:jobs.next()
                         break
                      end !if
                      if job:charge_type <> charge_type_temp      |
                          then break.  ! end if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      pos = Position(job:chatypekey)
                      job:charge_type = cha:charge_type
                      access:jobs.update()
                      Reset(job:chatypekey,pos)
      
                  end !loop
                  access:jobs.restorefile(save_job_id)
                  setcursor()
              Of 2 ! &No Button
                  cha:charge_type = charge_type_temp
                  Select(?cha:charge_type)
                  Cycle
          End!Case MessageEx
      End!If cha:charge_type <> charge_type_temp
  End!If thiswindow.request <> Insertrecord
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateCHARTYPE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      charge_type_temp = cha:charge_type
      Post(Event:Accepted,?cha:warranty)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

