

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01068.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBE01066.INC'),ONCE        !Req'd for module callout resolution
                     END


Browse_Users PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
user_type_temp       STRING(15),DIM(2)
LocalRequest         LONG
FilesOpened          BYTE
active_temp          STRING('YES')
Team_Temp            STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Team_Temp
tea:Team               LIKE(tea:Team)                 !List box control field - type derived from field
tea:Record_Number      LIKE(tea:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:User_Type)
                       PROJECT(use:User_Code)
                       PROJECT(use:Active)
                       PROJECT(use:Team)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:User_Type          LIKE(use:User_Type)            !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
use:Active             LIKE(use:Active)               !Browse key field - type derived from field
use:Team               LIKE(use:Team)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB10::View:FileDropCombo VIEW(TEAMS)
                       PROJECT(tea:Team)
                       PROJECT(tea:Record_Number)
                     END
QuickWindow          WINDOW('Browse the Users File'),AT(,,440,207),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Users'),SYSTEM,GRAY,MAX,RESIZE
                       OPTION,AT(8,16,104,20),USE(active_temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI)
                         RADIO('Active Only'),AT(16,24),USE(?active_temp:Radio1),TRN,VALUE('YES')
                         RADIO('All'),AT(72,24),USE(?active_temp:Radio2),TRN,VALUE('ALL')
                       END
                       ENTRY(@s30),AT(8,40,,10),USE(use:Surname),FONT('Tahoma',8,,FONT:bold),UPR
                       LIST,AT(8,56,340,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('125L(2)|M~Surname~@s30@123L(2)|M~Forename~@s30@77L(2)|M~User Type~@s15@77L(2)|M~' &|
   'User Code~@s3@'),FROM(Queue:Browse:1)
                       STRING('Surname'),AT(140,40),USE(?String1),TRN,FONT(,,COLOR:White,)
                       BUTTON('&Select'),AT(360,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       BUTTON('&Insert'),AT(360,108,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(360,132,76,20),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(360,156,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,348,200),USE(?CurrentTab),SPREAD
                         TAB('All Users'),USE(?Tab:2)
                         END
                         TAB('Engineers Only'),USE(?Tab2)
                         END
                         TAB('Administrators Only'),USE(?Tab3)
                         END
                         TAB('By Team'),USE(?Tab4)
                           COMBO(@s30),AT(200,40,124,10),USE(Team_Temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Team'),AT(328,40),USE(?Prompt1),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                         END
                         TAB('By User Code'),USE(?Tab5)
                           PROMPT('User Code'),AT(140,40),USE(?use:User_Code:Prompt),TRN,FONT(,,COLOR:White,,CHARSET:ANSI)
                           ENTRY(@s3),AT(8,40,128,10),USE(use:User_Code),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('Close'),AT(360,184,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(active_temp) = 'ALL'
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(active_temp) = 'YES'
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'ALL'
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'YES'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'ALL'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'YES'
BRW1::Sort7:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(active_temp) = 'ALL'
BRW1::Sort8:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(active_temp) = 'YES'
BRW1::Sort9:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(active_temp) = 'ALL'
BRW1::Sort10:Locator IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(active_temp) = 'YES'
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'ALL'
BRW1::Sort5:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(active_temp) = 'YES'
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'ALL'
BRW1::Sort6:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(active_temp) = 'YES'
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort8:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(active_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB10               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?active_temp{prop:Font,3} = -1
    ?active_temp{prop:Color} = 15066597
    ?active_temp{prop:Trn} = 0
    ?active_temp:Radio1{prop:Font,3} = -1
    ?active_temp:Radio1{prop:Color} = 15066597
    ?active_temp:Radio1{prop:Trn} = 0
    ?active_temp:Radio2{prop:Font,3} = -1
    ?active_temp:Radio2{prop:Color} = 15066597
    ?active_temp:Radio2{prop:Trn} = 0
    If ?use:Surname{prop:ReadOnly} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 15066597
    Elsif ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 8454143
    Else ! If ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 16777215
    End ! If ?use:Surname{prop:Req} = True
    ?use:Surname{prop:Trn} = 0
    ?use:Surname{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    If ?Team_Temp{prop:ReadOnly} = True
        ?Team_Temp{prop:FontColor} = 65793
        ?Team_Temp{prop:Color} = 15066597
    Elsif ?Team_Temp{prop:Req} = True
        ?Team_Temp{prop:FontColor} = 65793
        ?Team_Temp{prop:Color} = 8454143
    Else ! If ?Team_Temp{prop:Req} = True
        ?Team_Temp{prop:FontColor} = 65793
        ?Team_Temp{prop:Color} = 16777215
    End ! If ?Team_Temp{prop:Req} = True
    ?Team_Temp{prop:Trn} = 0
    ?Team_Temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    ?use:User_Code:Prompt{prop:FontColor} = -1
    ?use:User_Code:Prompt{prop:Color} = 15066597
    If ?use:User_Code{prop:ReadOnly} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 15066597
    Elsif ?use:User_Code{prop:Req} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 8454143
    Else ! If ?use:User_Code{prop:Req} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 16777215
    End ! If ?use:User_Code{prop:Req} = True
    ?use:User_Code{prop:Trn} = 0
    ?use:User_Code{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Refresh_Browse      Routine
    thiswindow.reset(1)
    If active_temp = 'YES'
        If Choice(?CurrentTab) = 4
            use:team    = team_temp
        End!If Choice(?CurrentTab) = 4
        brw1.addrange(use:active,active_temp)
        brw1.applyrange 
        thiswindow.reset(1)
    End!If active_temp = 'YES'


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Users',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Users',1)
    
      Loop SolaceDim1# = 1 to 2
        SolaceFieldName" = 'user_type_temp' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",user_type_temp[SolaceDim1#],'Browse_Users',1)
      End
    
    
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Users',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Users',1)
    SolaceViewVars('active_temp',active_temp,'Browse_Users',1)
    SolaceViewVars('Team_Temp',Team_Temp,'Browse_Users',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?active_temp;  SolaceCtrlName = '?active_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?active_temp:Radio1;  SolaceCtrlName = '?active_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?active_temp:Radio2;  SolaceCtrlName = '?active_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Surname;  SolaceCtrlName = '?use:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Team_Temp;  SolaceCtrlName = '?Team_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:User_Code:Prompt;  SolaceCtrlName = '?use:User_Code:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:User_Code;  SolaceCtrlName = '?use:User_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Users')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Users')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?active_temp:Radio1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:TEAMS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:USERS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  user_type_temp[1] = 'ENGINEER'
  user_type_temp[2] = 'ADMINISTRATOR'
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,use:surname_key)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:Surname_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,use:User_Type_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?USE:Surname,use:User_Type,1,BRW1)
  BRW1.SetFilter('(Upper(use:user_type) = ''ENGINEER'')')
  BRW1::Sort5:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort5:StepClass,use:User_Type_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddResetField(active_temp)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,use:User_Type_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?USE:Surname,use:User_Type,1,BRW1)
  BRW1.SetFilter('(Upper(use:user_type) = ''ADMINISTRATOR'')')
  BRW1::Sort6:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Alpha)
  BRW1.AddSortOrder(BRW1::Sort6:StepClass,use:User_Type_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:Team_Surname)
  BRW1.AddRange(use:Team,Team_Temp)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1::Sort8:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort8:StepClass,use:Active_Team_Surname)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddSortOrder(,use:User_Code_Key)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?use:User_Code,use:User_Code,1,BRW1)
  BRW1.AddSortOrder(,use:User_Code_Active_Key)
  BRW1.AddRange(use:Active,active_temp)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?use:User_Code,use:User_Code,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,use:surname_key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?use:Surname,use:Surname,1,BRW1)
  BRW1.AddResetField(Team_Temp)
  BRW1.AddResetField(active_temp)
  BIND('active_temp',active_temp)
  BIND('user_type_temp_1',user_type_temp[1])
  BIND('user_type_temp_2',user_type_temp[2])
  BIND('Team_Temp',Team_Temp)
  BRW1.AddField(use:Surname,BRW1.Q.use:Surname)
  BRW1.AddField(use:Forename,BRW1.Q.use:Forename)
  BRW1.AddField(use:User_Type,BRW1.Q.use:User_Type)
  BRW1.AddField(use:User_Code,BRW1.Q.use:User_Code)
  BRW1.AddField(use:Active,BRW1.Q.use:Active)
  BRW1.AddField(use:Team,BRW1.Q.use:Team)
  QuickWindow{PROP:MinWidth}=440
  QuickWindow{PROP:MinHeight}=192
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB10.Init(Team_Temp,?Team_Temp,Queue:FileDropCombo.ViewPosition,FDCB10::View:FileDropCombo,Queue:FileDropCombo,Relate:TEAMS,ThisWindow,GlobalErrors,0,1,0)
  FDCB10.Q &= Queue:FileDropCombo
  FDCB10.AddSortOrder(tea:Team_Key)
  FDCB10.AddField(tea:Team,FDCB10.Q.tea:Team)
  FDCB10.AddField(tea:Record_Number,FDCB10.Q.tea:Record_Number)
  ThisWindow.AddItem(FDCB10.WindowComponent)
  FDCB10.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'All Users'
    ?Tab2{PROP:TEXT} = 'Engineers Only'
    ?Tab3{PROP:TEXT} = 'Administrators Only'
    ?Tab4{PROP:TEXT} = 'By Team'
    ?Tab5{PROP:TEXT} = 'By User Code'
    ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TEAMS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Users',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('USERS - INSERT',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            check_access('USERS - CHANGE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            check_access('USERS - DELETE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateUSERS
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?active_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?active_temp, Accepted)
      Do refresh_browse
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?active_temp, Accepted)
    OF ?Team_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Team_Temp, Accepted)
      Do refresh_browse
      Select(?Browse:1)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Team_Temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Users')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      Do refresh_browse
      Select(?Browse:1)
      IF CHOICE(?CurrentTab) = 5
        HIDE(?USE:Surname)
        HIDE(?String1)
        UNHIDE(?USE:User_Code)
        UNHIDE(?use:User_Code:Prompt)
      ELSE
        HIDE(?USE:User_Code)
        HIDE(?use:User_Code:Prompt)
        UNHIDE(?USE:Surname)
        UNHIDE(?String1)
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab:2{PROP:TEXT} = 'All Users'
        OF 2
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab2{PROP:TEXT} = 'Engineers Only'
        OF 3
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab3{PROP:TEXT} = 'Administrators Only'
        OF 4
          ?Browse:1{PROP:FORMAT} ='125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#77L(2)|M~User Code~@s3@#4#'
          ?Tab4{PROP:TEXT} = 'By Team'
        OF 5
          ?Browse:1{PROP:FORMAT} ='77L(2)|M~User Code~@s3@#4#125L(2)|M~Surname~@s30@#1#123L(2)|M~Forename~@s30@#2#77L(2)|M~User Type~@s15@#3#'
          ?Tab5{PROP:TEXT} = 'By User Code'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?use:Surname
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Surname, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Surname, Selected)
    OF ?use:User_Code
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(active_temp) = 'ALL'
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(active_temp) = 'YES'
    RETURN SELF.SetSort(10,Force)
  ELSE
    RETURN SELF.SetSort(11,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort5:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 5, Init, (BYTE Controls,BYTE Mode))
  use:user_type   = 'ENGINEER'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 5, Init, (BYTE Controls,BYTE Mode))


BRW1::Sort6:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 6, Init, (BYTE Controls,BYTE Mode))
  use:user_type = 'ADMINISTRATOR'
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 6, Init, (BYTE Controls,BYTE Mode))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?USE:Surname, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?String1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?active_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?active_temp:Radio1, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?active_temp:Radio2, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

