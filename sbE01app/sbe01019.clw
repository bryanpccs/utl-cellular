

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01019.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTRANTYPE PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::trt:Record  LIKE(trt:RECORD),STATIC
QuickWindow          WINDOW('Update the TRANTYPE File'),AT(,,451,315),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateTRANTYPE'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,444,280),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Transit Type'),AT(8,20),USE(?TRT:Transit_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(trt:Transit_Type),FONT('Tahoma',8,,FONT:bold),UPR
                           GROUP('Required Fields'),AT(8,128,216,112),USE(?Group2),BOXED
                             CHECK('Collection Address Required'),AT(84,140,125,6),USE(trt:Collection_Address),VALUE('YES','NO')
                             CHECK('Delivery Address Required'),AT(84,152,125,8),USE(trt:Delivery_Address),VALUE('YES','NO')
                             CHECK('Force Entry Of D.O.P.'),AT(84,164,125,6),USE(trt:Force_DOP),VALUE('YES','NO')
                             CHECK('Location Required'),AT(84,180),USE(trt:Location),VALUE('YES','NO')
                             CHECK('Force Entry Of Location'),AT(84,192,125,8),USE(trt:Force_Location),VALUE('YES','NO')
                             CHECK('Loan Unit To Be Issued'),AT(84,212,125,8),USE(trt:Loan_Unit),VALUE('YES','NO')
                             CHECK('Exchange Unit To Be Issued'),AT(84,224,125,8),USE(trt:Exchange_Unit),VALUE('YES','NO')
                           END
                           GROUP('Workshop Defaults'),AT(8,240,216,40),USE(?Group4),BOXED
                             CHECK('Skip Workshop On Initial Booking'),AT(84,252),USE(trt:Skip_Workshop),VALUE('YES','NO')
                             CHECK('In Workshop'),AT(84,264),USE(trt:InWorkshop),RIGHT,VALUE('YES','NO')
                           END
                           GROUP('Auto Fill Fields'),AT(8,32,216,92),USE(?Group1),BOXED
                             PROMPT('Job Status'),AT(12,44),USE(?trt:Initial_Status:Prompt),TRN
                             ENTRY(@s30),AT(84,44,124,10),USE(trt:Initial_Status),FONT('Tahoma',8,,FONT:bold),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                             BUTTON,AT(212,44,10,10),USE(?LookupJobStatus),SKIP,ICON('List3.ico')
                             PROMPT('Exchange Status'),AT(12,60),USE(?trt:ExchangeStatus:Prompt)
                             ENTRY(@s30),AT(84,60,124,10),USE(trt:ExchangeStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                             BUTTON,AT(212,60,10,10),USE(?LookupExchangeStatus),SKIP,ICON('List3.ico')
                             PROMPT('Loan Status'),AT(12,76),USE(?trt:LoanStatus:Prompt)
                             ENTRY(@s30),AT(84,76,124,10),USE(trt:LoanStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                             BUTTON,AT(212,76,10,10),USE(?LookupLoanStatus),SKIP,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('List3.ico')
                             PROMPT('Job Turnaround Time'),AT(12,92),USE(?trt:Initial_Priority:Prompt),TRN
                             ENTRY(@s30),AT(84,92,124,10),USE(trt:Initial_Priority),FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR
                             BUTTON,AT(212,92,10,10),USE(?LookupTurnaroundTime),SKIP,ICON('List3.ico')
                             PROMPT('Location'),AT(12,108),USE(?trt:InternalLocation:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(84,108,124,10),USE(trt:InternalLocation),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR
                             BUTTON,AT(212,108,10,10),USE(?LookupLocation),SKIP,ICON('List3.ico')
                           END
                           GROUP('Printing Defaults'),AT(228,32,216,96),USE(?Group3),BOXED
                             CHECK('Include In Courier Collection Report'),AT(304,44,125,6),USE(trt:Courier_Collection_Report),VALUE('YES','NO')
                             CHECK('Print Workshop Label'),AT(304,64,125,8),USE(trt:Workshop_Label),VALUE('YES','NO')
                             CHECK('Print Job Card'),AT(304,76),USE(trt:Job_Card),VALUE('YES','NO')
                             CHECK('Print Job Receipt'),AT(304,88),USE(trt:JobReceipt),RIGHT,MSG('Print Job Receipt'),TIP('Print Job Receipt'),VALUE('YES','NO')
                             CHECK('Print ANC Collection Note'),AT(304,100),USE(trt:ANCCollNote),MSG('Print ANC Collection Note'),TIP('Print ANC Collection Note'),VALUE('YES','NO')
                           END
                         END
                       END
                       PANEL,AT(4,288,444,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(332,292,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(388,292,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
look:trt:Initial_Status                Like(trt:Initial_Status)
look:trt:ExchangeStatus                Like(trt:ExchangeStatus)
look:trt:LoanStatus                Like(trt:LoanStatus)
look:trt:Initial_Priority                Like(trt:Initial_Priority)
look:trt:InternalLocation                Like(trt:InternalLocation)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?TRT:Transit_Type:Prompt{prop:FontColor} = -1
    ?TRT:Transit_Type:Prompt{prop:Color} = 15066597
    If ?trt:Transit_Type{prop:ReadOnly} = True
        ?trt:Transit_Type{prop:FontColor} = 65793
        ?trt:Transit_Type{prop:Color} = 15066597
    Elsif ?trt:Transit_Type{prop:Req} = True
        ?trt:Transit_Type{prop:FontColor} = 65793
        ?trt:Transit_Type{prop:Color} = 8454143
    Else ! If ?trt:Transit_Type{prop:Req} = True
        ?trt:Transit_Type{prop:FontColor} = 65793
        ?trt:Transit_Type{prop:Color} = 16777215
    End ! If ?trt:Transit_Type{prop:Req} = True
    ?trt:Transit_Type{prop:Trn} = 0
    ?trt:Transit_Type{prop:FontStyle} = font:Bold
    ?Group2{prop:Font,3} = -1
    ?Group2{prop:Color} = 15066597
    ?Group2{prop:Trn} = 0
    ?trt:Collection_Address{prop:Font,3} = -1
    ?trt:Collection_Address{prop:Color} = 15066597
    ?trt:Collection_Address{prop:Trn} = 0
    ?trt:Delivery_Address{prop:Font,3} = -1
    ?trt:Delivery_Address{prop:Color} = 15066597
    ?trt:Delivery_Address{prop:Trn} = 0
    ?trt:Force_DOP{prop:Font,3} = -1
    ?trt:Force_DOP{prop:Color} = 15066597
    ?trt:Force_DOP{prop:Trn} = 0
    ?trt:Location{prop:Font,3} = -1
    ?trt:Location{prop:Color} = 15066597
    ?trt:Location{prop:Trn} = 0
    ?trt:Force_Location{prop:Font,3} = -1
    ?trt:Force_Location{prop:Color} = 15066597
    ?trt:Force_Location{prop:Trn} = 0
    ?trt:Loan_Unit{prop:Font,3} = -1
    ?trt:Loan_Unit{prop:Color} = 15066597
    ?trt:Loan_Unit{prop:Trn} = 0
    ?trt:Exchange_Unit{prop:Font,3} = -1
    ?trt:Exchange_Unit{prop:Color} = 15066597
    ?trt:Exchange_Unit{prop:Trn} = 0
    ?Group4{prop:Font,3} = -1
    ?Group4{prop:Color} = 15066597
    ?Group4{prop:Trn} = 0
    ?trt:Skip_Workshop{prop:Font,3} = -1
    ?trt:Skip_Workshop{prop:Color} = 15066597
    ?trt:Skip_Workshop{prop:Trn} = 0
    ?trt:InWorkshop{prop:Font,3} = -1
    ?trt:InWorkshop{prop:Color} = 15066597
    ?trt:InWorkshop{prop:Trn} = 0
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?trt:Initial_Status:Prompt{prop:FontColor} = -1
    ?trt:Initial_Status:Prompt{prop:Color} = 15066597
    If ?trt:Initial_Status{prop:ReadOnly} = True
        ?trt:Initial_Status{prop:FontColor} = 65793
        ?trt:Initial_Status{prop:Color} = 15066597
    Elsif ?trt:Initial_Status{prop:Req} = True
        ?trt:Initial_Status{prop:FontColor} = 65793
        ?trt:Initial_Status{prop:Color} = 8454143
    Else ! If ?trt:Initial_Status{prop:Req} = True
        ?trt:Initial_Status{prop:FontColor} = 65793
        ?trt:Initial_Status{prop:Color} = 16777215
    End ! If ?trt:Initial_Status{prop:Req} = True
    ?trt:Initial_Status{prop:Trn} = 0
    ?trt:Initial_Status{prop:FontStyle} = font:Bold
    ?trt:ExchangeStatus:Prompt{prop:FontColor} = -1
    ?trt:ExchangeStatus:Prompt{prop:Color} = 15066597
    If ?trt:ExchangeStatus{prop:ReadOnly} = True
        ?trt:ExchangeStatus{prop:FontColor} = 65793
        ?trt:ExchangeStatus{prop:Color} = 15066597
    Elsif ?trt:ExchangeStatus{prop:Req} = True
        ?trt:ExchangeStatus{prop:FontColor} = 65793
        ?trt:ExchangeStatus{prop:Color} = 8454143
    Else ! If ?trt:ExchangeStatus{prop:Req} = True
        ?trt:ExchangeStatus{prop:FontColor} = 65793
        ?trt:ExchangeStatus{prop:Color} = 16777215
    End ! If ?trt:ExchangeStatus{prop:Req} = True
    ?trt:ExchangeStatus{prop:Trn} = 0
    ?trt:ExchangeStatus{prop:FontStyle} = font:Bold
    ?trt:LoanStatus:Prompt{prop:FontColor} = -1
    ?trt:LoanStatus:Prompt{prop:Color} = 15066597
    If ?trt:LoanStatus{prop:ReadOnly} = True
        ?trt:LoanStatus{prop:FontColor} = 65793
        ?trt:LoanStatus{prop:Color} = 15066597
    Elsif ?trt:LoanStatus{prop:Req} = True
        ?trt:LoanStatus{prop:FontColor} = 65793
        ?trt:LoanStatus{prop:Color} = 8454143
    Else ! If ?trt:LoanStatus{prop:Req} = True
        ?trt:LoanStatus{prop:FontColor} = 65793
        ?trt:LoanStatus{prop:Color} = 16777215
    End ! If ?trt:LoanStatus{prop:Req} = True
    ?trt:LoanStatus{prop:Trn} = 0
    ?trt:LoanStatus{prop:FontStyle} = font:Bold
    ?trt:Initial_Priority:Prompt{prop:FontColor} = -1
    ?trt:Initial_Priority:Prompt{prop:Color} = 15066597
    If ?trt:Initial_Priority{prop:ReadOnly} = True
        ?trt:Initial_Priority{prop:FontColor} = 65793
        ?trt:Initial_Priority{prop:Color} = 15066597
    Elsif ?trt:Initial_Priority{prop:Req} = True
        ?trt:Initial_Priority{prop:FontColor} = 65793
        ?trt:Initial_Priority{prop:Color} = 8454143
    Else ! If ?trt:Initial_Priority{prop:Req} = True
        ?trt:Initial_Priority{prop:FontColor} = 65793
        ?trt:Initial_Priority{prop:Color} = 16777215
    End ! If ?trt:Initial_Priority{prop:Req} = True
    ?trt:Initial_Priority{prop:Trn} = 0
    ?trt:Initial_Priority{prop:FontStyle} = font:Bold
    ?trt:InternalLocation:Prompt{prop:FontColor} = -1
    ?trt:InternalLocation:Prompt{prop:Color} = 15066597
    If ?trt:InternalLocation{prop:ReadOnly} = True
        ?trt:InternalLocation{prop:FontColor} = 65793
        ?trt:InternalLocation{prop:Color} = 15066597
    Elsif ?trt:InternalLocation{prop:Req} = True
        ?trt:InternalLocation{prop:FontColor} = 65793
        ?trt:InternalLocation{prop:Color} = 8454143
    Else ! If ?trt:InternalLocation{prop:Req} = True
        ?trt:InternalLocation{prop:FontColor} = 65793
        ?trt:InternalLocation{prop:Color} = 16777215
    End ! If ?trt:InternalLocation{prop:Req} = True
    ?trt:InternalLocation{prop:Trn} = 0
    ?trt:InternalLocation{prop:FontStyle} = font:Bold
    ?Group3{prop:Font,3} = -1
    ?Group3{prop:Color} = 15066597
    ?Group3{prop:Trn} = 0
    ?trt:Courier_Collection_Report{prop:Font,3} = -1
    ?trt:Courier_Collection_Report{prop:Color} = 15066597
    ?trt:Courier_Collection_Report{prop:Trn} = 0
    ?trt:Workshop_Label{prop:Font,3} = -1
    ?trt:Workshop_Label{prop:Color} = 15066597
    ?trt:Workshop_Label{prop:Trn} = 0
    ?trt:Job_Card{prop:Font,3} = -1
    ?trt:Job_Card{prop:Color} = 15066597
    ?trt:Job_Card{prop:Trn} = 0
    ?trt:JobReceipt{prop:Font,3} = -1
    ?trt:JobReceipt{prop:Color} = 15066597
    ?trt:JobReceipt{prop:Trn} = 0
    ?trt:ANCCollNote{prop:Font,3} = -1
    ?trt:ANCCollNote{prop:Color} = 15066597
    ?trt:ANCCollNote{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTRANTYPE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTRANTYPE',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateTRANTYPE',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateTRANTYPE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTRANTYPE',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateTRANTYPE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRT:Transit_Type:Prompt;  SolaceCtrlName = '?TRT:Transit_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Transit_Type;  SolaceCtrlName = '?trt:Transit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group2;  SolaceCtrlName = '?Group2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Collection_Address;  SolaceCtrlName = '?trt:Collection_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Delivery_Address;  SolaceCtrlName = '?trt:Delivery_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Force_DOP;  SolaceCtrlName = '?trt:Force_DOP';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Location;  SolaceCtrlName = '?trt:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Force_Location;  SolaceCtrlName = '?trt:Force_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Loan_Unit;  SolaceCtrlName = '?trt:Loan_Unit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Exchange_Unit;  SolaceCtrlName = '?trt:Exchange_Unit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group4;  SolaceCtrlName = '?Group4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Skip_Workshop;  SolaceCtrlName = '?trt:Skip_Workshop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:InWorkshop;  SolaceCtrlName = '?trt:InWorkshop';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Initial_Status:Prompt;  SolaceCtrlName = '?trt:Initial_Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Initial_Status;  SolaceCtrlName = '?trt:Initial_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupJobStatus;  SolaceCtrlName = '?LookupJobStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:ExchangeStatus:Prompt;  SolaceCtrlName = '?trt:ExchangeStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:ExchangeStatus;  SolaceCtrlName = '?trt:ExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupExchangeStatus;  SolaceCtrlName = '?LookupExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:LoanStatus:Prompt;  SolaceCtrlName = '?trt:LoanStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:LoanStatus;  SolaceCtrlName = '?trt:LoanStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLoanStatus;  SolaceCtrlName = '?LookupLoanStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Initial_Priority:Prompt;  SolaceCtrlName = '?trt:Initial_Priority:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Initial_Priority;  SolaceCtrlName = '?trt:Initial_Priority';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupTurnaroundTime;  SolaceCtrlName = '?LookupTurnaroundTime';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:InternalLocation:Prompt;  SolaceCtrlName = '?trt:InternalLocation:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:InternalLocation;  SolaceCtrlName = '?trt:InternalLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLocation;  SolaceCtrlName = '?LookupLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group3;  SolaceCtrlName = '?Group3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Courier_Collection_Report;  SolaceCtrlName = '?trt:Courier_Collection_Report';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Workshop_Label;  SolaceCtrlName = '?trt:Workshop_Label';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:Job_Card;  SolaceCtrlName = '?trt:Job_Card';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:JobReceipt;  SolaceCtrlName = '?trt:JobReceipt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trt:ANCCollNote;  SolaceCtrlName = '?trt:ANCCollNote';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Transit Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Transit Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTRANTYPE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTRANTYPE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRT:Transit_Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(trt:Record,History::trt:Record)
  SELF.AddHistoryField(?trt:Transit_Type,1)
  SELF.AddHistoryField(?trt:Collection_Address,3)
  SELF.AddHistoryField(?trt:Delivery_Address,4)
  SELF.AddHistoryField(?trt:Force_DOP,7)
  SELF.AddHistoryField(?trt:Location,19)
  SELF.AddHistoryField(?trt:Force_Location,8)
  SELF.AddHistoryField(?trt:Loan_Unit,5)
  SELF.AddHistoryField(?trt:Exchange_Unit,6)
  SELF.AddHistoryField(?trt:Skip_Workshop,9)
  SELF.AddHistoryField(?trt:InWorkshop,21)
  SELF.AddHistoryField(?trt:Initial_Status,15)
  SELF.AddHistoryField(?trt:ExchangeStatus,17)
  SELF.AddHistoryField(?trt:LoanStatus,18)
  SELF.AddHistoryField(?trt:Initial_Priority,16)
  SELF.AddHistoryField(?trt:InternalLocation,11)
  SELF.AddHistoryField(?trt:Courier_Collection_Report,2)
  SELF.AddHistoryField(?trt:Workshop_Label,12)
  SELF.AddHistoryField(?trt:Job_Card,13)
  SELF.AddHistoryField(?trt:JobReceipt,14)
  SELF.AddHistoryField(?trt:ANCCollNote,20)
  SELF.AddUpdateFile(Access:TRANTYPE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOCINTER.Open
  Relate:STATUS.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRANTYPE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?trt:Initial_Status{Prop:Tip} AND ~?LookupJobStatus{Prop:Tip}
     ?LookupJobStatus{Prop:Tip} = 'Select ' & ?trt:Initial_Status{Prop:Tip}
  END
  IF ?trt:Initial_Status{Prop:Msg} AND ~?LookupJobStatus{Prop:Msg}
     ?LookupJobStatus{Prop:Msg} = 'Select ' & ?trt:Initial_Status{Prop:Msg}
  END
  IF ?trt:ExchangeStatus{Prop:Tip} AND ~?LookupExchangeStatus{Prop:Tip}
     ?LookupExchangeStatus{Prop:Tip} = 'Select ' & ?trt:ExchangeStatus{Prop:Tip}
  END
  IF ?trt:ExchangeStatus{Prop:Msg} AND ~?LookupExchangeStatus{Prop:Msg}
     ?LookupExchangeStatus{Prop:Msg} = 'Select ' & ?trt:ExchangeStatus{Prop:Msg}
  END
  IF ?trt:LoanStatus{Prop:Tip} AND ~?LookupLoanStatus{Prop:Tip}
     ?LookupLoanStatus{Prop:Tip} = 'Select ' & ?trt:LoanStatus{Prop:Tip}
  END
  IF ?trt:LoanStatus{Prop:Msg} AND ~?LookupLoanStatus{Prop:Msg}
     ?LookupLoanStatus{Prop:Msg} = 'Select ' & ?trt:LoanStatus{Prop:Msg}
  END
  IF ?trt:Initial_Priority{Prop:Tip} AND ~?LookupTurnaroundTime{Prop:Tip}
     ?LookupTurnaroundTime{Prop:Tip} = 'Select ' & ?trt:Initial_Priority{Prop:Tip}
  END
  IF ?trt:Initial_Priority{Prop:Msg} AND ~?LookupTurnaroundTime{Prop:Msg}
     ?LookupTurnaroundTime{Prop:Msg} = 'Select ' & ?trt:Initial_Priority{Prop:Msg}
  END
  IF ?trt:InternalLocation{Prop:Tip} AND ~?LookupLocation{Prop:Tip}
     ?LookupLocation{Prop:Tip} = 'Select ' & ?trt:InternalLocation{Prop:Tip}
  END
  IF ?trt:InternalLocation{Prop:Msg} AND ~?LookupLocation{Prop:Msg}
     ?LookupLocation{Prop:Msg} = 'Select ' & ?trt:InternalLocation{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?trt:Location{Prop:Checked} = True
    UNHIDE(?TRT:Force_Location)
  END
  IF ?trt:Location{Prop:Checked} = False
    trt:Force_Location = 'NO'
    HIDE(?TRT:Force_Location)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !Clear Globals
  glo:Select1 = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCINTER.Close
    Relate:STATUS.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTRANTYPE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      Browse_Status
      Browse_Status
      Browse_Status
      BrowseTurnaroundTimes
      Browse_Locations
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?trt:Location
      IF ?trt:Location{Prop:Checked} = True
        UNHIDE(?TRT:Force_Location)
      END
      IF ?trt:Location{Prop:Checked} = False
        trt:Force_Location = 'NO'
        HIDE(?TRT:Force_Location)
      END
      ThisWindow.Reset
    OF ?trt:Initial_Status
      IF trt:Initial_Status OR ?trt:Initial_Status{Prop:Req}
        sts:Status = trt:Initial_Status
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:trt:Initial_Status        = trt:Initial_Status
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            trt:Initial_Status = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            trt:Initial_Status = look:trt:Initial_Status
            SELECT(?trt:Initial_Status)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupJobStatus
      ThisWindow.Update
      sts:Status = trt:Initial_Status
      GLO:Select1 = 'JOB'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          trt:Initial_Status = sts:Status
          Select(?+1)
      ELSE
          Select(?trt:Initial_Status)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?trt:Initial_Status)
    OF ?trt:ExchangeStatus
      IF trt:ExchangeStatus OR ?trt:ExchangeStatus{Prop:Req}
        sts:Status = trt:ExchangeStatus
        sts:Exchange = 'YES'
        GLO:Select1 = 'EXC'
        !Save Lookup Field Incase Of error
        look:trt:ExchangeStatus        = trt:ExchangeStatus
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            trt:ExchangeStatus = sts:Status
          ELSE
            CLEAR(sts:Exchange)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            trt:ExchangeStatus = look:trt:ExchangeStatus
            SELECT(?trt:ExchangeStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupExchangeStatus
      ThisWindow.Update
      sts:Status = trt:ExchangeStatus
      GLO:Select1 = 'EXC'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          trt:ExchangeStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?trt:ExchangeStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?trt:ExchangeStatus)
    OF ?trt:LoanStatus
      IF trt:LoanStatus OR ?trt:LoanStatus{Prop:Req}
        sts:Status = trt:LoanStatus
        GLO:Select1 = 'LOA'
        sts:Loan = 'YES'
        !Save Lookup Field Incase Of error
        look:trt:LoanStatus        = trt:LoanStatus
        IF Access:STATUS.TryFetch(sts:LoanKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            trt:LoanStatus = sts:Status
          ELSE
            CLEAR(GLO:Select1)
            CLEAR(sts:Loan)
            !Restore Lookup On Error
            trt:LoanStatus = look:trt:LoanStatus
            SELECT(?trt:LoanStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLoanStatus
      ThisWindow.Update
      sts:Status = trt:LoanStatus
      GLO:Select1 = 'LOA'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          trt:LoanStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?trt:LoanStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?trt:LoanStatus)
    OF ?trt:Initial_Priority
      IF trt:Initial_Priority OR ?trt:Initial_Priority{Prop:Req}
        tur:Turnaround_Time = trt:Initial_Priority
        !Save Lookup Field Incase Of error
        look:trt:Initial_Priority        = trt:Initial_Priority
        IF Access:TURNARND.TryFetch(tur:Turnaround_Time_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            trt:Initial_Priority = tur:Turnaround_Time
          ELSE
            !Restore Lookup On Error
            trt:Initial_Priority = look:trt:Initial_Priority
            SELECT(?trt:Initial_Priority)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupTurnaroundTime
      ThisWindow.Update
      tur:Turnaround_Time = trt:Initial_Priority
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          trt:Initial_Priority = tur:Turnaround_Time
          Select(?+1)
      ELSE
          Select(?trt:Initial_Priority)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?trt:Initial_Priority)
    OF ?trt:InternalLocation
      IF trt:InternalLocation OR ?trt:InternalLocation{Prop:Req}
        loi:Location = trt:InternalLocation
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:trt:InternalLocation        = trt:InternalLocation
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            trt:InternalLocation = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            trt:InternalLocation = look:trt:InternalLocation
            SELECT(?trt:InternalLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLocation
      ThisWindow.Update
      loi:Location = trt:InternalLocation
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          trt:InternalLocation = loi:Location
          Select(?+1)
      ELSE
          Select(?trt:InternalLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?trt:InternalLocation)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTRANTYPE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?trt:Initial_Status
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:Initial_Status, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:Initial_Status, AlertKey)
    END
  OF ?trt:ExchangeStatus
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:ExchangeStatus, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:ExchangeStatus, AlertKey)
    END
  OF ?trt:LoanStatus
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:LoanStatus, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:LoanStatus, AlertKey)
    END
  OF ?trt:Initial_Priority
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:Initial_Priority, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:Initial_Priority, AlertKey)
    END
  OF ?trt:InternalLocation
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:InternalLocation, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trt:InternalLocation, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

