

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01091.INC'),ONCE        !Local module procedure declarations
                     END


Rapid_Accessory_Insert PROCEDURE                      !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::1:TAGFLAG          BYTE(0)
DASBRW::1:TAGMOUSE         BYTE(0)
DASBRW::1:TAGDISPSTATUS    BYTE(0)
DASBRW::1:QUEUE           QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
accessory_tag_temp   STRING(1)
accessory_tag2_temp  STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW9::View:Browse    VIEW(ACCESSOR)
                       PROJECT(acr:Accessory)
                       PROJECT(acr:Model_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
accessory_tag2_temp    LIKE(accessory_tag2_temp)      !List box control field - type derived from local data
accessory_tag2_temp_Icon LONG                         !Entry's icon ID
acr:Accessory          LIKE(acr:Accessory)            !List box control field - type derived from field
acr:Model_Number       LIKE(acr:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse The Accessory File'),AT(,,254,212),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Loan_Accessories'),ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F10Key),ALRT(SpaceKey),SYSTEM,GRAY,DOUBLE
                       BUTTON('&Save And Close [F10]'),AT(176,36,76,20),USE(?AddAccessory),LEFT,ICON('select2.ico')
                       SHEET,AT(4,4,164,204),USE(?Sheet2),SPREAD
                         TAB('All Accessories'),USE(?Tab2)
                           ENTRY(@s30),AT(8,20,124,10),USE(acr:Accessory),FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(8,36,156,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),ALRT(SpaceKey),FORMAT('10L(2)I@s1@120L(2)~Accessory~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Rev tags'),AT(73,82,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(65,106,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&UnTag All [F7]'),AT(112,188,52,16),USE(?DASUNTAGALL),LEFT,ICON('UnTag.gif')
                           BUTTON('&Tag [F5]'),AT(8,188,52,16),USE(?DASTAG),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All [F6]'),AT(60,188,52,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                         END
                       END
                       BUTTON('Close'),AT(176,188,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW9                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW9::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?acr:Accessory{prop:ReadOnly} = True
        ?acr:Accessory{prop:FontColor} = 65793
        ?acr:Accessory{prop:Color} = 15066597
    Elsif ?acr:Accessory{prop:Req} = True
        ?acr:Accessory{prop:FontColor} = 65793
        ?acr:Accessory{prop:Color} = 8454143
    Else ! If ?acr:Accessory{prop:Req} = True
        ?acr:Accessory{prop:FontColor} = 65793
        ?acr:Accessory{prop:Color} = 16777215
    End ! If ?acr:Accessory{prop:Req} = True
    ?acr:Accessory{prop:Trn} = 0
    ?acr:Accessory{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::1:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW9.UpdateBuffer
   GLO:Queue2.Pointer2 = acr:Accessory
   GET(GLO:Queue2,GLO:Queue2.Pointer2)
  IF ERRORCODE()
     GLO:Queue2.Pointer2 = acr:Accessory
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    accessory_tag2_temp = '*'
  ELSE
    DELETE(GLO:Queue2)
    accessory_tag2_temp = ''
  END
    Queue:Browse.accessory_tag2_temp = accessory_tag2_temp
  IF (accessory_tag2_temp = '*')
    Queue:Browse.accessory_tag2_temp_Icon = 2
  ELSE
    Queue:Browse.accessory_tag2_temp_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::1:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW9.Reset
  FREE(GLO:Queue2)
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue2.Pointer2 = acr:Accessory
     ADD(GLO:Queue2,GLO:Queue2.Pointer2)
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::1:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue2)
  BRW9.Reset
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::1:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::1:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue2)
    GET(GLO:Queue2,QR#)
    DASBRW::1:QUEUE = GLO:Queue2
    ADD(DASBRW::1:QUEUE)
  END
  FREE(GLO:Queue2)
  BRW9.Reset
  LOOP
    NEXT(BRW9::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::1:QUEUE.Pointer2 = acr:Accessory
     GET(DASBRW::1:QUEUE,DASBRW::1:QUEUE.Pointer2)
    IF ERRORCODE()
       GLO:Queue2.Pointer2 = acr:Accessory
       ADD(GLO:Queue2,GLO:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW9.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::1:DASSHOWTAG Routine
   CASE DASBRW::1:TAGDISPSTATUS
   OF 0
      DASBRW::1:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::1:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::1:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW9.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Rapid_Accessory_Insert',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Rapid_Accessory_Insert',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Rapid_Accessory_Insert',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Rapid_Accessory_Insert',1)
    SolaceViewVars('accessory_tag_temp',accessory_tag_temp,'Rapid_Accessory_Insert',1)
    SolaceViewVars('accessory_tag2_temp',accessory_tag2_temp,'Rapid_Accessory_Insert',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?AddAccessory;  SolaceCtrlName = '?AddAccessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?acr:Accessory;  SolaceCtrlName = '?acr:Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Rapid_Accessory_Insert')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Rapid_Accessory_Insert')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?AddAccessory
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCESSOR.Open
  Relate:LOANACC.Open
  Access:JOBS.UseFile
  SELF.FilesOpened = True
  BRW9.Init(?List,Queue:Browse.ViewPosition,BRW9::View:Browse,Queue:Browse,Relate:ACCESSOR,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW9.Q &= Queue:Browse
  BRW9.RetainRow = 0
  BRW9.AddSortOrder(,acr:Accesory_Key)
  BRW9.AddRange(acr:Model_Number,GLO:Select1)
  BRW9.AddLocator(BRW9::Sort0:Locator)
  BRW9::Sort0:Locator.Init(?acr:Accessory,acr:Accessory,1,BRW9)
  BIND('accessory_tag2_temp',accessory_tag2_temp)
  BIND('GLO:Select1',GLO:Select1)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW9.AddField(accessory_tag2_temp,BRW9.Q.accessory_tag2_temp)
  BRW9.AddField(acr:Accessory,BRW9.Q.acr:Accessory)
  BRW9.AddField(acr:Model_Number,BRW9.Q.acr:Model_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCESSOR.Close
    Relate:LOANACC.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Rapid_Accessory_Insert',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AddAccessory
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddAccessory, Accepted)
      If Records(glo:Queue2) <> ''
          Loop x# = 1 To Records(glo:Queue2)
              Get(glo:Queue2,x#)
              jac:ref_number  = glo:select2
              jac:accessory   = glo:pointer2
              Access:jobacc.tryinsert()
          End
      End
      
      Post(event:closewindow)
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AddAccessory, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::1:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::1:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::1:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::1:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::1:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Rapid_Accessory_Insert')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If Keycode() = MouseLeft2
         Post(Event:Accepted,?DASTAG)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::1:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?acr:Accessory
      Select(?List)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key OrOf SpaceKey
              Post(Event:Accepted,?Dastag)
          Of F6Key
              Post(Event:Accepted,?Dastagall)
          Of F7Key
              Post(Event:Accepted,?Dasuntagall)
          Of F10Key
              Post(Event:Accepted,?AddAccessory)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::1:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW9.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW9.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = acr:Accessory
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    IF ERRORCODE()
      accessory_tag2_temp = ''
    ELSE
      accessory_tag2_temp = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (accessory_tag2_temp = '*')
    SELF.Q.accessory_tag2_temp_Icon = 2
  ELSE
    SELF.Q.accessory_tag2_temp_Icon = 1
  END


BRW9.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW9.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW9::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW9::RecordStatus=ReturnValue
  IF BRW9::RecordStatus NOT=Record:OK THEN RETURN BRW9::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue2.Pointer2 = acr:Accessory
     GET(GLO:Queue2,GLO:Queue2.Pointer2)
    EXECUTE DASBRW::1:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW9::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW9::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

