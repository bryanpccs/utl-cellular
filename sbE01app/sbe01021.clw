

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01021.INC'),ONCE        !Local module procedure declarations
                     END


UpdateLOCINTER PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
total_spaces_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Disable_Location     BYTE
History::loi:Record  LIKE(loi:RECORD),STATIC
QuickWindow          WINDOW('Update the LOCINTER File'),AT(,,220,140),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateLOCINTER'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,104),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Location'),AT(8,20),USE(?LOI:Location:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(loi:Location),FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK('Disable Location'),AT(84,40),USE(Disable_Location),TRN
                           CHECK('Allocate Available Spaces'),AT(84,56,112,8),USE(loi:Allocate_Spaces),TRN,VALUE('YES','NO')
                           PROMPT('Total Spaces'),AT(8,72),USE(?LOI:Total_Spaces:Prompt),TRN
                           SPIN(@p<<<<<#p),AT(84,72,64,10),USE(loi:Total_Spaces),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR,RANGE(1,999999),STEP(1)
                           PROMPT('Current Free Spaces'),AT(8,88),USE(?LOI:Current_Spaces:Prompt),TRN
                           ENTRY(@p<<<<<#p),AT(84,88,64,10),USE(loi:Current_Spaces),SKIP,RIGHT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                         END
                       END
                       PANEL,AT(4,112,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,116,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,116,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?LOI:Location:Prompt{prop:FontColor} = -1
    ?LOI:Location:Prompt{prop:Color} = 15066597
    If ?loi:Location{prop:ReadOnly} = True
        ?loi:Location{prop:FontColor} = 65793
        ?loi:Location{prop:Color} = 15066597
    Elsif ?loi:Location{prop:Req} = True
        ?loi:Location{prop:FontColor} = 65793
        ?loi:Location{prop:Color} = 8454143
    Else ! If ?loi:Location{prop:Req} = True
        ?loi:Location{prop:FontColor} = 65793
        ?loi:Location{prop:Color} = 16777215
    End ! If ?loi:Location{prop:Req} = True
    ?loi:Location{prop:Trn} = 0
    ?loi:Location{prop:FontStyle} = font:Bold
    ?Disable_Location{prop:Font,3} = -1
    ?Disable_Location{prop:Color} = 15066597
    ?Disable_Location{prop:Trn} = 0
    ?loi:Allocate_Spaces{prop:Font,3} = -1
    ?loi:Allocate_Spaces{prop:Color} = 15066597
    ?loi:Allocate_Spaces{prop:Trn} = 0
    ?LOI:Total_Spaces:Prompt{prop:FontColor} = -1
    ?LOI:Total_Spaces:Prompt{prop:Color} = 15066597
    If ?loi:Total_Spaces{prop:ReadOnly} = True
        ?loi:Total_Spaces{prop:FontColor} = 65793
        ?loi:Total_Spaces{prop:Color} = 15066597
    Elsif ?loi:Total_Spaces{prop:Req} = True
        ?loi:Total_Spaces{prop:FontColor} = 65793
        ?loi:Total_Spaces{prop:Color} = 8454143
    Else ! If ?loi:Total_Spaces{prop:Req} = True
        ?loi:Total_Spaces{prop:FontColor} = 65793
        ?loi:Total_Spaces{prop:Color} = 16777215
    End ! If ?loi:Total_Spaces{prop:Req} = True
    ?loi:Total_Spaces{prop:Trn} = 0
    ?loi:Total_Spaces{prop:FontStyle} = font:Bold
    ?LOI:Current_Spaces:Prompt{prop:FontColor} = -1
    ?LOI:Current_Spaces:Prompt{prop:Color} = 15066597
    If ?loi:Current_Spaces{prop:ReadOnly} = True
        ?loi:Current_Spaces{prop:FontColor} = 65793
        ?loi:Current_Spaces{prop:Color} = 15066597
    Elsif ?loi:Current_Spaces{prop:Req} = True
        ?loi:Current_Spaces{prop:FontColor} = 65793
        ?loi:Current_Spaces{prop:Color} = 8454143
    Else ! If ?loi:Current_Spaces{prop:Req} = True
        ?loi:Current_Spaces{prop:FontColor} = 65793
        ?loi:Current_Spaces{prop:Color} = 16777215
    End ! If ?loi:Current_Spaces{prop:Req} = True
    ?loi:Current_Spaces{prop:Trn} = 0
    ?loi:Current_Spaces{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Hide_Fields     Routine
    ! Start Change 2866 BE(06/11/03)
    !If loi:allocate_spaces <> 'YES'
    !    Disable(?loi:total_spaces)
    !    Disable(?loi:total_spaces:prompt)
    !    Disable(?loi:current_spaces)
    !    Disable(?loi:current_spaces:prompt)
    !Else
    !    Enable(?loi:total_spaces)
    !   Enable(?loi:total_spaces:prompt)
    !   Enable(?loi:current_spaces)
    !    Enable(?loi:current_spaces:prompt)
    !End
    IF (Disable_Location) THEN
        Disable(?loi:allocate_spaces)
        Disable(?loi:total_spaces)
        Disable(?loi:total_spaces:prompt)
        Disable(?loi:current_spaces)
        Disable(?loi:current_spaces:prompt)
    ELSE
        Enable(?loi:allocate_spaces)
        If loi:allocate_spaces <> 'YES'
            Disable(?loi:total_spaces)
            Disable(?loi:total_spaces:prompt)
            Disable(?loi:current_spaces)
            Disable(?loi:current_spaces:prompt)
        Else
            Enable(?loi:total_spaces)
            Enable(?loi:total_spaces:prompt)
            Enable(?loi:current_spaces)
            Enable(?loi:current_spaces:prompt)
        End
    END
    ! End Change 2866 BE(06/11/03)
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOCINTER',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOCINTER',1)
    SolaceViewVars('total_spaces_temp',total_spaces_temp,'UpdateLOCINTER',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateLOCINTER',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateLOCINTER',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOCINTER',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateLOCINTER',1)
    SolaceViewVars('Disable_Location',Disable_Location,'UpdateLOCINTER',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOI:Location:Prompt;  SolaceCtrlName = '?LOI:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loi:Location;  SolaceCtrlName = '?loi:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Disable_Location;  SolaceCtrlName = '?Disable_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loi:Allocate_Spaces;  SolaceCtrlName = '?loi:Allocate_Spaces';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOI:Total_Spaces:Prompt;  SolaceCtrlName = '?LOI:Total_Spaces:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loi:Total_Spaces;  SolaceCtrlName = '?loi:Total_Spaces';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOI:Current_Spaces:Prompt;  SolaceCtrlName = '?LOI:Current_Spaces:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loi:Current_Spaces;  SolaceCtrlName = '?loi:Current_Spaces';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Internal Location'
  OF ChangeRecord
    ActionMessage = 'Changing An Internal Location'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLOCINTER')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOCINTER')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOI:Location:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(loi:Record,History::loi:Record)
  SELF.AddHistoryField(?loi:Location,1)
  SELF.AddHistoryField(?loi:Allocate_Spaces,3)
  SELF.AddHistoryField(?loi:Total_Spaces,4)
  SELF.AddHistoryField(?loi:Current_Spaces,5)
  SELF.AddUpdateFile(Access:LOCINTER)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOCINTER.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOCINTER
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCINTER.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOCINTER',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    loi:Allocate_Spaces = 'NO'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Disable_Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Disable_Location, Accepted)
      ! Start Change 2866 BE(06/11/03)
      Do Hide_Fields
      ! End Change 2866 BE(06/11/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Disable_Location, Accepted)
    OF ?loi:Allocate_Spaces
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loi:Allocate_Spaces, Accepted)
      Do Hide_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loi:Allocate_Spaces, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ! Check Spaces
      If ThisWindow.Request = Insertrecord
          loi:current_spaces = loi:total_spaces
      Else!End!If ThisWindow.Request = Insertrecord
          If loi:total_spaces <> total_spaces_temp
              If loi:total_spaces < (total_spaces_temp - loi:current_spaces)
                  Case MessageEx('There are currently '&clip(total_spaces_temp - loi:current_spaces)&' being used.<13,10><13,10>You cannot set your total spaces to less than this value.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  loi:total_spaces = total_spaces_temp
                  Select(?loi:total_spaces)
                  Cycle
              End
              loi:current_spaces += (loi:total_spaces - total_spaces_temp)
          End
      End!If ThisWindow.Request = Insertrecord
  
      ! Start Change 2866 BE(06/11/03)
      !If loi:allocate_spaces <> 'YES'
      IF (Disable_Location) THEN
          loi:allocate_spaces = 'NO'
          loi:location_available = 'NO'
          loi:current_spaces  = 'N/A'
      ELSIF (loi:allocate_spaces <> 'YES') THEN
      ! End Change 2866 BE(06/11/03)
          loi:location_available = 'YES'
          loi:current_spaces  = 'N/A'
      Else
          If loi:current_spaces < 1
              loi:location_available = 'NO'
          Else
              loi:location_available = 'YES'
          End
      End
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOCINTER')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! Start Change 2866
      IF ((loi:Allocate_Spaces = 'NO') AND (loi:Location_Available = 'NO')) THEN
          Disable_Location = 1
      ELSE
          Disable_Location = 0
      END
      ! End Change 2866
      Do Hide_Fields
      total_spaces_temp = loi:total_spaces
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

