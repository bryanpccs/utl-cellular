

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01011.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMODELNUM PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ContractOracleCode STRING(30)
tmp:PAYTOracleCode   STRING(30)
tmp:SavedColour      STRING(30)
tmp:TierName         STRING(10)
retailRepairCharge   STRING(10)
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?mod:Unit_Type
uni:Unit_Type          LIKE(uni:Unit_Type)            !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?mod:OEMManufacturer
man_ali:Manufacturer   LIKE(man_ali:Manufacturer)     !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?rrct:Tier
rrct:Tier              LIKE(rrct:Tier)                !List box control field - type derived from field
rrct:RecordNumber      LIKE(rrct:RecordNumber)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB8::View:FileDropCombo VIEW(UNITTYPE)
                       PROJECT(uni:Unit_Type)
                     END
FDCB18::View:FileDropCombo VIEW(MANUFACT_ALIAS)
                       PROJECT(man_ali:Manufacturer)
                     END
FDCB19::View:FileDropCombo VIEW(SIDRRCT)
                       PROJECT(rrct:Tier)
                       PROJECT(rrct:RecordNumber)
                     END
BRW7::View:Browse    VIEW(AccSKU)
                       PROJECT(ASKU:AccessoryName)
                       PROJECT(ASKU:OracleCode)
                       PROJECT(ASKU:AccessorySKUCode)
                       PROJECT(ASKU:AccessSKUID)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
ASKU:AccessoryName     LIKE(ASKU:AccessoryName)       !List box control field - type derived from field
ASKU:OracleCode        LIKE(ASKU:OracleCode)          !List box control field - type derived from field
ASKU:AccessorySKUCode  LIKE(ASKU:AccessorySKUCode)    !List box control field - type derived from field
ASKU:AccessSKUID       LIKE(ASKU:AccessSKUID)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW10::View:Browse   VIEW(MODELCOL)
                       PROJECT(moc:Colour)
                       PROJECT(moc:Record_Number)
                       PROJECT(moc:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
moc:Colour             LIKE(moc:Colour)               !List box control field - type derived from field
tmp:ContractOracleCode LIKE(tmp:ContractOracleCode)   !List box control field - type derived from local data
tmp:PAYTOracleCode     LIKE(tmp:PAYTOracleCode)       !List box control field - type derived from local data
moc:Record_Number      LIKE(moc:Record_Number)        !Primary key field - type derived from field
moc:Model_Number       LIKE(moc:Model_Number)         !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW15::View:Browse   VIEW(PRODCODE)
                       PROJECT(prd:ProductCode)
                       PROJECT(prd:WarrantyPeriod)
                       PROJECT(prd:RecordNumber)
                       PROJECT(prd:ModelNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
prd:ProductCode        LIKE(prd:ProductCode)          !List box control field - type derived from field
prd:WarrantyPeriod     LIKE(prd:WarrantyPeriod)       !List box control field - type derived from field
prd:RecordNumber       LIKE(prd:RecordNumber)         !Primary key field - type derived from field
prd:ModelNumber        LIKE(prd:ModelNumber)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::mod:Record  LIKE(mod:RECORD),STATIC
QuickWindow          WINDOW('Update the MODELNUM File'),AT(,,684,404),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateMODELNUM'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,220,372),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           GROUP,AT(8,298,189,10),USE(?GroupAccessoryProduct),HIDE
                             CHECK('Accessory Repair'),AT(8,298),USE(mod:AccessoryRepair),RIGHT(1),VALUE('1','0')
                             CHECK('Accessory Return'),AT(124,298),USE(mod:AccessoryReturn),RIGHT(1),VALUE('1','0')
                           END
                           PROMPT('Manufacturer'),AT(8,20),USE(?MOD:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(mod:Manufacturer),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,33),USE(?MOD:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,33,124,10),USE(mod:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           CHECK('OEM'),AT(10,49),USE(mod:OEM),TRN,MSG('OEM'),TIP('OEM'),VALUE('1','0')
                           COMBO(@s20),AT(84,49,124,10),USE(mod:OEMManufacturer),IMM,REQ,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo),MSG('Manufacturer')
                           CHECK('Specify Unit Type'),AT(84,62),USE(mod:Specify_Unit_Type),VALUE('YES','NO')
                           COMBO(@s30),AT(84,73,124,10),USE(mod:Unit_Type),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                           PROMPT('Product Type'),AT(8,89),USE(?MOD:Product_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,89,124,10),USE(mod:Product_Type),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Exchange Unit Min Level'),AT(8,102,64,16),USE(?mod:ExchangeUnitMinLevel:Prompt),TRN
                           ENTRY(@s8),AT(84,102,64,10),USE(mod:ExchangeUnitMinLevel),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Exchange Unit Min Level'),TIP('Exchange Unit Min Level'),UPR
                           PROMPT('Unit Type'),AT(8,73),USE(?mod:unit_type:prompt)
                           PROMPT('IMEI Length From'),AT(8,116),USE(?MOD:ESN_Length:Prompt),TRN
                           ENTRY(@s2),AT(84,116,15,10),USE(mod:ESN_Length_From),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('To'),AT(112,116),USE(?MOD:ESN_Length_To:Prompt),TRN
                           ENTRY(@s2),AT(132,116,15,10),USE(mod:ESN_Length_To),FONT('Tahoma',8,,FONT:bold),UPR
                           GROUP,AT(8,124,152,20),USE(?Msn_Group)
                             PROMPT('M.S.N. Length From'),AT(8,129),USE(?MOD:MSN_Length:Prompt),TRN
                             ENTRY(@s2),AT(84,129,15,10),USE(mod:MSN_Length_From),FONT('Tahoma',8,,FONT:bold),UPR
                             PROMPT('To'),AT(112,129),USE(?MOD:MSN_Length_To:Prompt),TRN
                             ENTRY(@s2),AT(132,129,15,10),USE(mod:MSN_Length_To),FONT('Tahoma',8,,FONT:bold),UPR
                           END
                           PROMPT('Retail Repair Charge Tier'),AT(8,145),USE(?RetailRepairChargeTier:Prompt),TRN,FONT(,7,,,CHARSET:ANSI)
                           COMBO(@n2),AT(84,145,64,10),USE(rrct:Tier),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('20L|M@n2@'),DROP(10,64),FROM(Queue:FileDropCombo:2)
                           STRING(@s10),AT(156,145),USE(retailRepairCharge),TRN,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Exchange Charge (<163>)'),AT(8,158),USE(?mod:ExchangeCharge:Prompt),TRN
                           ENTRY(@n8.2),AT(84,158,64,10),USE(mod:ExchangeCharge),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Postal Repair Charge (<163>)'),AT(8,172),USE(?mod:PostalRepairCharge:Prompt),TRN,FONT(,7,,,CHARSET:ANSI)
                           ENTRY(@n8.2),AT(84,172,64,10),USE(mod:PostalRepairCharge),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('BER Charge (<163>)'),AT(8,185),USE(?mod:BERCharge:Prompt)
                           ENTRY(@n8.2),AT(84,185,64,10),USE(mod:BERCharge),DECIMAL(12),FONT(,,,FONT:bold)
                           CHECK('Vodafone Retail Repair'),AT(8,201),USE(mod:IsVodafoneSpecific),VALUE('1','0')
                           CHECK('Vodafone Retail Exchange'),AT(8,212),USE(mod:RetailExchange),MSG('Vodafone Retail Exchange'),TIP('Vodafone Retail Exchange'),VALUE('1','0')
                           CHECK('Vodafone CBU Postal'),AT(8,225),USE(mod:CBUPostal),MSG('Vodafone CBU Postal'),TIP('Vodafone CBU Postal'),VALUE('1','0')
                           CHECK('Vodafone CBU Exchange'),AT(8,236),USE(mod:CBUExchange),MSG('Vodafone CBU Exchange'),TIP('Vodafone CBU Exchange'),VALUE('1','0')
                           CHECK('Vodafone ISP Postal'),AT(124,201),USE(mod:ISPPostal),MSG('Vodafone ISP Postal'),TIP('Vodafone ISP Postal'),VALUE('1','0')
                           CHECK('Vodafone ISP Exchange'),AT(124,212),USE(mod:ISPExchange),MSG('Vodafone ISP Exchange'),TIP('Vodafone ISP Exchange'),VALUE('1','0')
                           CHECK('Vodafone EBU Postal'),AT(124,225),USE(mod:EBUPostal),MSG('Vodafone EBU Postal'),TIP('Vodafone EBU Postal'),VALUE('1','0')
                           CHECK('Vodafone EBU Exchange'),AT(124,236),USE(mod:EBUExchange),MSG('Vodafone EBU Exchange'),TIP('Vodafone EBU Exchange'),VALUE('1','0')
                           CHECK('Vodafone Known Issue Display'),AT(8,249),USE(mod:KnownIssueDisplay),MSG('Vodafone Known Issue Display'),TIP('Vodafone Known Issue Display'),VALUE('1','0')
                           CHECK('Tablet Device'),AT(124,249),USE(mod:TabletDevice),FONT(,,,,CHARSET:ANSI),VALUE('1','0'),MSG('Tablet Device')
                           CHECK('7 Day Replacement'),AT(8,260),USE(mod:Replacement28Day),FONT(,,,,CHARSET:ANSI),MSG('28 Day Replacement'),TIP('28 Day Replacement'),VALUE('1','0')
                           CHECK('Router'),AT(124,260),USE(mod:Router),RIGHT(1),VALUE('1','0')
                           CHECK('7 Day Exchange (Contact Centre)'),AT(8,273),USE(mod:CCExchange28Day),MSG('28 Day Exchange (Contact Centre)'),TIP('28 Day Exchange (Contact Centre)'),VALUE('1','0')
                           CHECK('Accessory Product'),AT(8,286),USE(mod:AccessoryProduct),RIGHT(1),VALUE('1','0')
                           PROMPT('1st Model Specific URL'),AT(8,314),USE(?mod:ModelSpecificURL1:Prompt)
                           ENTRY(@s250),AT(84,314,124,10),USE(mod:ModelSpecificURL1),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           CHECK('Serialised Product'),AT(124,286),USE(mod:SerialisedProduct),RIGHT(1),VALUE('1','0')
                           STRING('e.g http://www........'),AT(84,324),USE(?URLHelper),TRN
                           PROMPT('2nd Model Specific URL'),AT(8,332),USE(?mod:ModelSpecificURL2:Prompt)
                           ENTRY(@s250),AT(84,332,124,10),USE(mod:ModelSpecificURL2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('3rd Model Specific URL'),AT(8,348),USE(?mod:ModelSpecificURL3:Prompt)
                           ENTRY(@s250),AT(84,348,124,10),USE(mod:ModelSpecificURL3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Return Period (Days)'),AT(8,364),USE(?mod:ReturnPeriod:Prompt)
                           ENTRY(@n_8),AT(84,364,64,10),USE(mod:ReturnPeriod),RIGHT(1),FONT(,8,,FONT:bold),MSG('Return Period (Days)'),TIP('Return Period (Days)')
                         END
                       END
                       SHEET,AT(430,4,252,148),USE(?Sheet2),SPREAD
                         TAB('By Accessory'),USE(?Tab2)
                           LIST,AT(432,20,184,128),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M~Accessory~@s30@50L(2)|M~Oracle Code~@s30@60L(2)|M~SKU Code~@s22@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(620,20,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(620,74,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(620,132,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                         END
                       END
                       SHEET,AT(430,156,254,220),USE(?Sheet2:2),SPREAD
                         TAB('By Colour / Oracle Code'),USE(?Tab3)
                           LIST,AT(432,174,184,174),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('60L(2)|M~Colour~@s30@60L(2)|M~Contract O/C~@s30@60L(2)|M~PAYT O/C~@s30@'),FROM(Queue:Browse:1)
                           BUTTON('&Insert'),AT(620,172,56,16),USE(?Insert:2),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(620,250,56,16),USE(?Change:2),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(620,332,56,16),USE(?Delete:2),LEFT,ICON('delete.gif')
                         END
                       END
                       SHEET,AT(228,4,200,372),USE(?Sheet4),SPREAD
                         TAB('By Product Code'),USE(?ProductCodeTab)
                           LIST,AT(232,20,188,308),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('100L(2)|M~Product Code~@s30@32L|M~Warranty Exception~L(2)@n4B@'),FROM(Queue:Browse:2)
                           BUTTON('&Insert'),AT(232,332,56,16),USE(?Insert:3),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(298,332,56,16),USE(?Change:3),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(364,332,56,16),USE(?Delete:3),LEFT,ICON('delete.gif')
                         END
                       END
                       PANEL,AT(4,378,678,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(566,382,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(622,382,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
TakeAccepted           PROCEDURE(),DERIVED
                     END

FDCB18               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB19               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

BRW7                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW10                CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW10::Sort0:Locator IncrementalLocatorClass          !Default Locator
BRW15                CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW15::Sort0:Locator StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
saveReturnPeriod        LIKE(mod:ReturnPeriod)
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?GroupAccessoryProduct{prop:Font,3} = -1
    ?GroupAccessoryProduct{prop:Color} = 15066597
    ?GroupAccessoryProduct{prop:Trn} = 0
    ?mod:AccessoryRepair{prop:Font,3} = -1
    ?mod:AccessoryRepair{prop:Color} = 15066597
    ?mod:AccessoryRepair{prop:Trn} = 0
    ?mod:AccessoryReturn{prop:Font,3} = -1
    ?mod:AccessoryReturn{prop:Color} = 15066597
    ?mod:AccessoryReturn{prop:Trn} = 0
    ?MOD:Manufacturer:Prompt{prop:FontColor} = -1
    ?MOD:Manufacturer:Prompt{prop:Color} = 15066597
    If ?mod:Manufacturer{prop:ReadOnly} = True
        ?mod:Manufacturer{prop:FontColor} = 65793
        ?mod:Manufacturer{prop:Color} = 15066597
    Elsif ?mod:Manufacturer{prop:Req} = True
        ?mod:Manufacturer{prop:FontColor} = 65793
        ?mod:Manufacturer{prop:Color} = 8454143
    Else ! If ?mod:Manufacturer{prop:Req} = True
        ?mod:Manufacturer{prop:FontColor} = 65793
        ?mod:Manufacturer{prop:Color} = 16777215
    End ! If ?mod:Manufacturer{prop:Req} = True
    ?mod:Manufacturer{prop:Trn} = 0
    ?mod:Manufacturer{prop:FontStyle} = font:Bold
    ?MOD:Model_Number:Prompt{prop:FontColor} = -1
    ?MOD:Model_Number:Prompt{prop:Color} = 15066597
    If ?mod:Model_Number{prop:ReadOnly} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 15066597
    Elsif ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 8454143
    Else ! If ?mod:Model_Number{prop:Req} = True
        ?mod:Model_Number{prop:FontColor} = 65793
        ?mod:Model_Number{prop:Color} = 16777215
    End ! If ?mod:Model_Number{prop:Req} = True
    ?mod:Model_Number{prop:Trn} = 0
    ?mod:Model_Number{prop:FontStyle} = font:Bold
    ?mod:OEM{prop:Font,3} = -1
    ?mod:OEM{prop:Color} = 15066597
    ?mod:OEM{prop:Trn} = 0
    If ?mod:OEMManufacturer{prop:ReadOnly} = True
        ?mod:OEMManufacturer{prop:FontColor} = 65793
        ?mod:OEMManufacturer{prop:Color} = 15066597
    Elsif ?mod:OEMManufacturer{prop:Req} = True
        ?mod:OEMManufacturer{prop:FontColor} = 65793
        ?mod:OEMManufacturer{prop:Color} = 8454143
    Else ! If ?mod:OEMManufacturer{prop:Req} = True
        ?mod:OEMManufacturer{prop:FontColor} = 65793
        ?mod:OEMManufacturer{prop:Color} = 16777215
    End ! If ?mod:OEMManufacturer{prop:Req} = True
    ?mod:OEMManufacturer{prop:Trn} = 0
    ?mod:OEMManufacturer{prop:FontStyle} = font:Bold
    ?mod:Specify_Unit_Type{prop:Font,3} = -1
    ?mod:Specify_Unit_Type{prop:Color} = 15066597
    ?mod:Specify_Unit_Type{prop:Trn} = 0
    If ?mod:Unit_Type{prop:ReadOnly} = True
        ?mod:Unit_Type{prop:FontColor} = 65793
        ?mod:Unit_Type{prop:Color} = 15066597
    Elsif ?mod:Unit_Type{prop:Req} = True
        ?mod:Unit_Type{prop:FontColor} = 65793
        ?mod:Unit_Type{prop:Color} = 8454143
    Else ! If ?mod:Unit_Type{prop:Req} = True
        ?mod:Unit_Type{prop:FontColor} = 65793
        ?mod:Unit_Type{prop:Color} = 16777215
    End ! If ?mod:Unit_Type{prop:Req} = True
    ?mod:Unit_Type{prop:Trn} = 0
    ?mod:Unit_Type{prop:FontStyle} = font:Bold
    ?MOD:Product_Type:Prompt{prop:FontColor} = -1
    ?MOD:Product_Type:Prompt{prop:Color} = 15066597
    If ?mod:Product_Type{prop:ReadOnly} = True
        ?mod:Product_Type{prop:FontColor} = 65793
        ?mod:Product_Type{prop:Color} = 15066597
    Elsif ?mod:Product_Type{prop:Req} = True
        ?mod:Product_Type{prop:FontColor} = 65793
        ?mod:Product_Type{prop:Color} = 8454143
    Else ! If ?mod:Product_Type{prop:Req} = True
        ?mod:Product_Type{prop:FontColor} = 65793
        ?mod:Product_Type{prop:Color} = 16777215
    End ! If ?mod:Product_Type{prop:Req} = True
    ?mod:Product_Type{prop:Trn} = 0
    ?mod:Product_Type{prop:FontStyle} = font:Bold
    ?mod:ExchangeUnitMinLevel:Prompt{prop:FontColor} = -1
    ?mod:ExchangeUnitMinLevel:Prompt{prop:Color} = 15066597
    If ?mod:ExchangeUnitMinLevel{prop:ReadOnly} = True
        ?mod:ExchangeUnitMinLevel{prop:FontColor} = 65793
        ?mod:ExchangeUnitMinLevel{prop:Color} = 15066597
    Elsif ?mod:ExchangeUnitMinLevel{prop:Req} = True
        ?mod:ExchangeUnitMinLevel{prop:FontColor} = 65793
        ?mod:ExchangeUnitMinLevel{prop:Color} = 8454143
    Else ! If ?mod:ExchangeUnitMinLevel{prop:Req} = True
        ?mod:ExchangeUnitMinLevel{prop:FontColor} = 65793
        ?mod:ExchangeUnitMinLevel{prop:Color} = 16777215
    End ! If ?mod:ExchangeUnitMinLevel{prop:Req} = True
    ?mod:ExchangeUnitMinLevel{prop:Trn} = 0
    ?mod:ExchangeUnitMinLevel{prop:FontStyle} = font:Bold
    ?mod:unit_type:prompt{prop:FontColor} = -1
    ?mod:unit_type:prompt{prop:Color} = 15066597
    ?MOD:ESN_Length:Prompt{prop:FontColor} = -1
    ?MOD:ESN_Length:Prompt{prop:Color} = 15066597
    If ?mod:ESN_Length_From{prop:ReadOnly} = True
        ?mod:ESN_Length_From{prop:FontColor} = 65793
        ?mod:ESN_Length_From{prop:Color} = 15066597
    Elsif ?mod:ESN_Length_From{prop:Req} = True
        ?mod:ESN_Length_From{prop:FontColor} = 65793
        ?mod:ESN_Length_From{prop:Color} = 8454143
    Else ! If ?mod:ESN_Length_From{prop:Req} = True
        ?mod:ESN_Length_From{prop:FontColor} = 65793
        ?mod:ESN_Length_From{prop:Color} = 16777215
    End ! If ?mod:ESN_Length_From{prop:Req} = True
    ?mod:ESN_Length_From{prop:Trn} = 0
    ?mod:ESN_Length_From{prop:FontStyle} = font:Bold
    ?MOD:ESN_Length_To:Prompt{prop:FontColor} = -1
    ?MOD:ESN_Length_To:Prompt{prop:Color} = 15066597
    If ?mod:ESN_Length_To{prop:ReadOnly} = True
        ?mod:ESN_Length_To{prop:FontColor} = 65793
        ?mod:ESN_Length_To{prop:Color} = 15066597
    Elsif ?mod:ESN_Length_To{prop:Req} = True
        ?mod:ESN_Length_To{prop:FontColor} = 65793
        ?mod:ESN_Length_To{prop:Color} = 8454143
    Else ! If ?mod:ESN_Length_To{prop:Req} = True
        ?mod:ESN_Length_To{prop:FontColor} = 65793
        ?mod:ESN_Length_To{prop:Color} = 16777215
    End ! If ?mod:ESN_Length_To{prop:Req} = True
    ?mod:ESN_Length_To{prop:Trn} = 0
    ?mod:ESN_Length_To{prop:FontStyle} = font:Bold
    ?Msn_Group{prop:Font,3} = -1
    ?Msn_Group{prop:Color} = 15066597
    ?Msn_Group{prop:Trn} = 0
    ?MOD:MSN_Length:Prompt{prop:FontColor} = -1
    ?MOD:MSN_Length:Prompt{prop:Color} = 15066597
    If ?mod:MSN_Length_From{prop:ReadOnly} = True
        ?mod:MSN_Length_From{prop:FontColor} = 65793
        ?mod:MSN_Length_From{prop:Color} = 15066597
    Elsif ?mod:MSN_Length_From{prop:Req} = True
        ?mod:MSN_Length_From{prop:FontColor} = 65793
        ?mod:MSN_Length_From{prop:Color} = 8454143
    Else ! If ?mod:MSN_Length_From{prop:Req} = True
        ?mod:MSN_Length_From{prop:FontColor} = 65793
        ?mod:MSN_Length_From{prop:Color} = 16777215
    End ! If ?mod:MSN_Length_From{prop:Req} = True
    ?mod:MSN_Length_From{prop:Trn} = 0
    ?mod:MSN_Length_From{prop:FontStyle} = font:Bold
    ?MOD:MSN_Length_To:Prompt{prop:FontColor} = -1
    ?MOD:MSN_Length_To:Prompt{prop:Color} = 15066597
    If ?mod:MSN_Length_To{prop:ReadOnly} = True
        ?mod:MSN_Length_To{prop:FontColor} = 65793
        ?mod:MSN_Length_To{prop:Color} = 15066597
    Elsif ?mod:MSN_Length_To{prop:Req} = True
        ?mod:MSN_Length_To{prop:FontColor} = 65793
        ?mod:MSN_Length_To{prop:Color} = 8454143
    Else ! If ?mod:MSN_Length_To{prop:Req} = True
        ?mod:MSN_Length_To{prop:FontColor} = 65793
        ?mod:MSN_Length_To{prop:Color} = 16777215
    End ! If ?mod:MSN_Length_To{prop:Req} = True
    ?mod:MSN_Length_To{prop:Trn} = 0
    ?mod:MSN_Length_To{prop:FontStyle} = font:Bold
    ?RetailRepairChargeTier:Prompt{prop:FontColor} = -1
    ?RetailRepairChargeTier:Prompt{prop:Color} = 15066597
    If ?rrct:Tier{prop:ReadOnly} = True
        ?rrct:Tier{prop:FontColor} = 65793
        ?rrct:Tier{prop:Color} = 15066597
    Elsif ?rrct:Tier{prop:Req} = True
        ?rrct:Tier{prop:FontColor} = 65793
        ?rrct:Tier{prop:Color} = 8454143
    Else ! If ?rrct:Tier{prop:Req} = True
        ?rrct:Tier{prop:FontColor} = 65793
        ?rrct:Tier{prop:Color} = 16777215
    End ! If ?rrct:Tier{prop:Req} = True
    ?rrct:Tier{prop:Trn} = 0
    ?rrct:Tier{prop:FontStyle} = font:Bold
    ?retailRepairCharge{prop:FontColor} = -1
    ?retailRepairCharge{prop:Color} = 15066597
    ?mod:ExchangeCharge:Prompt{prop:FontColor} = -1
    ?mod:ExchangeCharge:Prompt{prop:Color} = 15066597
    If ?mod:ExchangeCharge{prop:ReadOnly} = True
        ?mod:ExchangeCharge{prop:FontColor} = 65793
        ?mod:ExchangeCharge{prop:Color} = 15066597
    Elsif ?mod:ExchangeCharge{prop:Req} = True
        ?mod:ExchangeCharge{prop:FontColor} = 65793
        ?mod:ExchangeCharge{prop:Color} = 8454143
    Else ! If ?mod:ExchangeCharge{prop:Req} = True
        ?mod:ExchangeCharge{prop:FontColor} = 65793
        ?mod:ExchangeCharge{prop:Color} = 16777215
    End ! If ?mod:ExchangeCharge{prop:Req} = True
    ?mod:ExchangeCharge{prop:Trn} = 0
    ?mod:ExchangeCharge{prop:FontStyle} = font:Bold
    ?mod:PostalRepairCharge:Prompt{prop:FontColor} = -1
    ?mod:PostalRepairCharge:Prompt{prop:Color} = 15066597
    If ?mod:PostalRepairCharge{prop:ReadOnly} = True
        ?mod:PostalRepairCharge{prop:FontColor} = 65793
        ?mod:PostalRepairCharge{prop:Color} = 15066597
    Elsif ?mod:PostalRepairCharge{prop:Req} = True
        ?mod:PostalRepairCharge{prop:FontColor} = 65793
        ?mod:PostalRepairCharge{prop:Color} = 8454143
    Else ! If ?mod:PostalRepairCharge{prop:Req} = True
        ?mod:PostalRepairCharge{prop:FontColor} = 65793
        ?mod:PostalRepairCharge{prop:Color} = 16777215
    End ! If ?mod:PostalRepairCharge{prop:Req} = True
    ?mod:PostalRepairCharge{prop:Trn} = 0
    ?mod:PostalRepairCharge{prop:FontStyle} = font:Bold
    ?mod:BERCharge:Prompt{prop:FontColor} = -1
    ?mod:BERCharge:Prompt{prop:Color} = 15066597
    If ?mod:BERCharge{prop:ReadOnly} = True
        ?mod:BERCharge{prop:FontColor} = 65793
        ?mod:BERCharge{prop:Color} = 15066597
    Elsif ?mod:BERCharge{prop:Req} = True
        ?mod:BERCharge{prop:FontColor} = 65793
        ?mod:BERCharge{prop:Color} = 8454143
    Else ! If ?mod:BERCharge{prop:Req} = True
        ?mod:BERCharge{prop:FontColor} = 65793
        ?mod:BERCharge{prop:Color} = 16777215
    End ! If ?mod:BERCharge{prop:Req} = True
    ?mod:BERCharge{prop:Trn} = 0
    ?mod:BERCharge{prop:FontStyle} = font:Bold
    ?mod:IsVodafoneSpecific{prop:Font,3} = -1
    ?mod:IsVodafoneSpecific{prop:Color} = 15066597
    ?mod:IsVodafoneSpecific{prop:Trn} = 0
    ?mod:RetailExchange{prop:Font,3} = -1
    ?mod:RetailExchange{prop:Color} = 15066597
    ?mod:RetailExchange{prop:Trn} = 0
    ?mod:CBUPostal{prop:Font,3} = -1
    ?mod:CBUPostal{prop:Color} = 15066597
    ?mod:CBUPostal{prop:Trn} = 0
    ?mod:CBUExchange{prop:Font,3} = -1
    ?mod:CBUExchange{prop:Color} = 15066597
    ?mod:CBUExchange{prop:Trn} = 0
    ?mod:ISPPostal{prop:Font,3} = -1
    ?mod:ISPPostal{prop:Color} = 15066597
    ?mod:ISPPostal{prop:Trn} = 0
    ?mod:ISPExchange{prop:Font,3} = -1
    ?mod:ISPExchange{prop:Color} = 15066597
    ?mod:ISPExchange{prop:Trn} = 0
    ?mod:EBUPostal{prop:Font,3} = -1
    ?mod:EBUPostal{prop:Color} = 15066597
    ?mod:EBUPostal{prop:Trn} = 0
    ?mod:EBUExchange{prop:Font,3} = -1
    ?mod:EBUExchange{prop:Color} = 15066597
    ?mod:EBUExchange{prop:Trn} = 0
    ?mod:KnownIssueDisplay{prop:Font,3} = -1
    ?mod:KnownIssueDisplay{prop:Color} = 15066597
    ?mod:KnownIssueDisplay{prop:Trn} = 0
    ?mod:TabletDevice{prop:Font,3} = -1
    ?mod:TabletDevice{prop:Color} = 15066597
    ?mod:TabletDevice{prop:Trn} = 0
    ?mod:Replacement28Day{prop:Font,3} = -1
    ?mod:Replacement28Day{prop:Color} = 15066597
    ?mod:Replacement28Day{prop:Trn} = 0
    ?mod:Router{prop:Font,3} = -1
    ?mod:Router{prop:Color} = 15066597
    ?mod:Router{prop:Trn} = 0
    ?mod:CCExchange28Day{prop:Font,3} = -1
    ?mod:CCExchange28Day{prop:Color} = 15066597
    ?mod:CCExchange28Day{prop:Trn} = 0
    ?mod:AccessoryProduct{prop:Font,3} = -1
    ?mod:AccessoryProduct{prop:Color} = 15066597
    ?mod:AccessoryProduct{prop:Trn} = 0
    ?mod:ModelSpecificURL1:Prompt{prop:FontColor} = -1
    ?mod:ModelSpecificURL1:Prompt{prop:Color} = 15066597
    If ?mod:ModelSpecificURL1{prop:ReadOnly} = True
        ?mod:ModelSpecificURL1{prop:FontColor} = 65793
        ?mod:ModelSpecificURL1{prop:Color} = 15066597
    Elsif ?mod:ModelSpecificURL1{prop:Req} = True
        ?mod:ModelSpecificURL1{prop:FontColor} = 65793
        ?mod:ModelSpecificURL1{prop:Color} = 8454143
    Else ! If ?mod:ModelSpecificURL1{prop:Req} = True
        ?mod:ModelSpecificURL1{prop:FontColor} = 65793
        ?mod:ModelSpecificURL1{prop:Color} = 16777215
    End ! If ?mod:ModelSpecificURL1{prop:Req} = True
    ?mod:ModelSpecificURL1{prop:Trn} = 0
    ?mod:ModelSpecificURL1{prop:FontStyle} = font:Bold
    ?mod:SerialisedProduct{prop:Font,3} = -1
    ?mod:SerialisedProduct{prop:Color} = 15066597
    ?mod:SerialisedProduct{prop:Trn} = 0
    ?URLHelper{prop:FontColor} = -1
    ?URLHelper{prop:Color} = 15066597
    ?mod:ModelSpecificURL2:Prompt{prop:FontColor} = -1
    ?mod:ModelSpecificURL2:Prompt{prop:Color} = 15066597
    If ?mod:ModelSpecificURL2{prop:ReadOnly} = True
        ?mod:ModelSpecificURL2{prop:FontColor} = 65793
        ?mod:ModelSpecificURL2{prop:Color} = 15066597
    Elsif ?mod:ModelSpecificURL2{prop:Req} = True
        ?mod:ModelSpecificURL2{prop:FontColor} = 65793
        ?mod:ModelSpecificURL2{prop:Color} = 8454143
    Else ! If ?mod:ModelSpecificURL2{prop:Req} = True
        ?mod:ModelSpecificURL2{prop:FontColor} = 65793
        ?mod:ModelSpecificURL2{prop:Color} = 16777215
    End ! If ?mod:ModelSpecificURL2{prop:Req} = True
    ?mod:ModelSpecificURL2{prop:Trn} = 0
    ?mod:ModelSpecificURL2{prop:FontStyle} = font:Bold
    ?mod:ModelSpecificURL3:Prompt{prop:FontColor} = -1
    ?mod:ModelSpecificURL3:Prompt{prop:Color} = 15066597
    If ?mod:ModelSpecificURL3{prop:ReadOnly} = True
        ?mod:ModelSpecificURL3{prop:FontColor} = 65793
        ?mod:ModelSpecificURL3{prop:Color} = 15066597
    Elsif ?mod:ModelSpecificURL3{prop:Req} = True
        ?mod:ModelSpecificURL3{prop:FontColor} = 65793
        ?mod:ModelSpecificURL3{prop:Color} = 8454143
    Else ! If ?mod:ModelSpecificURL3{prop:Req} = True
        ?mod:ModelSpecificURL3{prop:FontColor} = 65793
        ?mod:ModelSpecificURL3{prop:Color} = 16777215
    End ! If ?mod:ModelSpecificURL3{prop:Req} = True
    ?mod:ModelSpecificURL3{prop:Trn} = 0
    ?mod:ModelSpecificURL3{prop:FontStyle} = font:Bold
    ?mod:ReturnPeriod:Prompt{prop:FontColor} = -1
    ?mod:ReturnPeriod:Prompt{prop:Color} = 15066597
    If ?mod:ReturnPeriod{prop:ReadOnly} = True
        ?mod:ReturnPeriod{prop:FontColor} = 65793
        ?mod:ReturnPeriod{prop:Color} = 15066597
    Elsif ?mod:ReturnPeriod{prop:Req} = True
        ?mod:ReturnPeriod{prop:FontColor} = 65793
        ?mod:ReturnPeriod{prop:Color} = 8454143
    Else ! If ?mod:ReturnPeriod{prop:Req} = True
        ?mod:ReturnPeriod{prop:FontColor} = 65793
        ?mod:ReturnPeriod{prop:Color} = 16777215
    End ! If ?mod:ReturnPeriod{prop:Req} = True
    ?mod:ReturnPeriod{prop:Trn} = 0
    ?mod:ReturnPeriod{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet2:2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Sheet4{prop:Color} = 15066597
    ?ProductCodeTab{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Hide_fields     Routine
    If mod:specify_unit_type <> 'YES'
        Disable(?mod:unit_type)
        Disable(?mod:unit_type:prompt)
    Else
        Enable(?mod:unit_type)
        Enable(?mod:unit_type:prompt)
    End
DisplayRetailRepairCharge routine

    Access:SIDRRCT.ClearKey(rrct:TierKey)
    rrct:Tier = mod:RetailRepairTier
    if Access:SIDRRCT.Fetch(rrct:TierKey) = Level:Benign
        retailRepairCharge = '�' & clip(left(format(rrct:ChargeValue, @n9.2)))
    end

    display()
    
ShowGroupAccessoryProduct   ROUTINE
    ?GroupAccessoryProduct{prop:Hide} = 1
    IF (mod:AccessoryProduct = 1)
        ?GroupAccessoryProduct{Prop:Hide} = 0
    END ! IF
    DISPLAY()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMODELNUM',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMODELNUM',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateMODELNUM',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateMODELNUM',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMODELNUM',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateMODELNUM',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'UpdateMODELNUM',1)
    SolaceViewVars('tmp:ContractOracleCode',tmp:ContractOracleCode,'UpdateMODELNUM',1)
    SolaceViewVars('tmp:PAYTOracleCode',tmp:PAYTOracleCode,'UpdateMODELNUM',1)
    SolaceViewVars('tmp:SavedColour',tmp:SavedColour,'UpdateMODELNUM',1)
    SolaceViewVars('tmp:TierName',tmp:TierName,'UpdateMODELNUM',1)
    SolaceViewVars('retailRepairCharge',retailRepairCharge,'UpdateMODELNUM',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GroupAccessoryProduct;  SolaceCtrlName = '?GroupAccessoryProduct';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:AccessoryRepair;  SolaceCtrlName = '?mod:AccessoryRepair';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:AccessoryReturn;  SolaceCtrlName = '?mod:AccessoryReturn';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MOD:Manufacturer:Prompt;  SolaceCtrlName = '?MOD:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Manufacturer;  SolaceCtrlName = '?mod:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MOD:Model_Number:Prompt;  SolaceCtrlName = '?MOD:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Model_Number;  SolaceCtrlName = '?mod:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:OEM;  SolaceCtrlName = '?mod:OEM';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:OEMManufacturer;  SolaceCtrlName = '?mod:OEMManufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Specify_Unit_Type;  SolaceCtrlName = '?mod:Specify_Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Unit_Type;  SolaceCtrlName = '?mod:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MOD:Product_Type:Prompt;  SolaceCtrlName = '?MOD:Product_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Product_Type;  SolaceCtrlName = '?mod:Product_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ExchangeUnitMinLevel:Prompt;  SolaceCtrlName = '?mod:ExchangeUnitMinLevel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ExchangeUnitMinLevel;  SolaceCtrlName = '?mod:ExchangeUnitMinLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:unit_type:prompt;  SolaceCtrlName = '?mod:unit_type:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MOD:ESN_Length:Prompt;  SolaceCtrlName = '?MOD:ESN_Length:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ESN_Length_From;  SolaceCtrlName = '?mod:ESN_Length_From';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MOD:ESN_Length_To:Prompt;  SolaceCtrlName = '?MOD:ESN_Length_To:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ESN_Length_To;  SolaceCtrlName = '?mod:ESN_Length_To';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Msn_Group;  SolaceCtrlName = '?Msn_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MOD:MSN_Length:Prompt;  SolaceCtrlName = '?MOD:MSN_Length:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:MSN_Length_From;  SolaceCtrlName = '?mod:MSN_Length_From';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MOD:MSN_Length_To:Prompt;  SolaceCtrlName = '?MOD:MSN_Length_To:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:MSN_Length_To;  SolaceCtrlName = '?mod:MSN_Length_To';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RetailRepairChargeTier:Prompt;  SolaceCtrlName = '?RetailRepairChargeTier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rrct:Tier;  SolaceCtrlName = '?rrct:Tier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retailRepairCharge;  SolaceCtrlName = '?retailRepairCharge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ExchangeCharge:Prompt;  SolaceCtrlName = '?mod:ExchangeCharge:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ExchangeCharge;  SolaceCtrlName = '?mod:ExchangeCharge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:PostalRepairCharge:Prompt;  SolaceCtrlName = '?mod:PostalRepairCharge:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:PostalRepairCharge;  SolaceCtrlName = '?mod:PostalRepairCharge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:BERCharge:Prompt;  SolaceCtrlName = '?mod:BERCharge:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:BERCharge;  SolaceCtrlName = '?mod:BERCharge';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:IsVodafoneSpecific;  SolaceCtrlName = '?mod:IsVodafoneSpecific';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:RetailExchange;  SolaceCtrlName = '?mod:RetailExchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:CBUPostal;  SolaceCtrlName = '?mod:CBUPostal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:CBUExchange;  SolaceCtrlName = '?mod:CBUExchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ISPPostal;  SolaceCtrlName = '?mod:ISPPostal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ISPExchange;  SolaceCtrlName = '?mod:ISPExchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:EBUPostal;  SolaceCtrlName = '?mod:EBUPostal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:EBUExchange;  SolaceCtrlName = '?mod:EBUExchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:KnownIssueDisplay;  SolaceCtrlName = '?mod:KnownIssueDisplay';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:TabletDevice;  SolaceCtrlName = '?mod:TabletDevice';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Replacement28Day;  SolaceCtrlName = '?mod:Replacement28Day';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:Router;  SolaceCtrlName = '?mod:Router';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:CCExchange28Day;  SolaceCtrlName = '?mod:CCExchange28Day';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:AccessoryProduct;  SolaceCtrlName = '?mod:AccessoryProduct';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ModelSpecificURL1:Prompt;  SolaceCtrlName = '?mod:ModelSpecificURL1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ModelSpecificURL1;  SolaceCtrlName = '?mod:ModelSpecificURL1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:SerialisedProduct;  SolaceCtrlName = '?mod:SerialisedProduct';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?URLHelper;  SolaceCtrlName = '?URLHelper';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ModelSpecificURL2:Prompt;  SolaceCtrlName = '?mod:ModelSpecificURL2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ModelSpecificURL2;  SolaceCtrlName = '?mod:ModelSpecificURL2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ModelSpecificURL3:Prompt;  SolaceCtrlName = '?mod:ModelSpecificURL3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ModelSpecificURL3;  SolaceCtrlName = '?mod:ModelSpecificURL3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ReturnPeriod:Prompt;  SolaceCtrlName = '?mod:ReturnPeriod:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?mod:ReturnPeriod;  SolaceCtrlName = '?mod:ReturnPeriod';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2:2;  SolaceCtrlName = '?Sheet2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProductCodeTab;  SolaceCtrlName = '?ProductCodeTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Model Number'
  OF ChangeRecord
    ActionMessage = 'Changing A Model Number'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMODELNUM')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMODELNUM')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?mod:AccessoryRepair
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(mod:Record,History::mod:Record)
  SELF.AddHistoryField(?mod:AccessoryRepair,41)
  SELF.AddHistoryField(?mod:AccessoryReturn,42)
  SELF.AddHistoryField(?mod:Manufacturer,2)
  SELF.AddHistoryField(?mod:Model_Number,1)
  SELF.AddHistoryField(?mod:OEM,17)
  SELF.AddHistoryField(?mod:OEMManufacturer,18)
  SELF.AddHistoryField(?mod:Specify_Unit_Type,3)
  SELF.AddHistoryField(?mod:Unit_Type,5)
  SELF.AddHistoryField(?mod:Product_Type,4)
  SELF.AddHistoryField(?mod:ExchangeUnitMinLevel,11)
  SELF.AddHistoryField(?mod:ESN_Length_From,7)
  SELF.AddHistoryField(?mod:ESN_Length_To,8)
  SELF.AddHistoryField(?mod:MSN_Length_From,9)
  SELF.AddHistoryField(?mod:MSN_Length_To,10)
  SELF.AddHistoryField(?mod:ExchangeCharge,32)
  SELF.AddHistoryField(?mod:PostalRepairCharge,33)
  SELF.AddHistoryField(?mod:BERCharge,37)
  SELF.AddHistoryField(?mod:IsVodafoneSpecific,12)
  SELF.AddHistoryField(?mod:RetailExchange,20)
  SELF.AddHistoryField(?mod:CBUPostal,21)
  SELF.AddHistoryField(?mod:CBUExchange,22)
  SELF.AddHistoryField(?mod:ISPPostal,23)
  SELF.AddHistoryField(?mod:ISPExchange,24)
  SELF.AddHistoryField(?mod:EBUPostal,25)
  SELF.AddHistoryField(?mod:EBUExchange,26)
  SELF.AddHistoryField(?mod:KnownIssueDisplay,19)
  SELF.AddHistoryField(?mod:TabletDevice,35)
  SELF.AddHistoryField(?mod:Replacement28Day,34)
  SELF.AddHistoryField(?mod:Router,38)
  SELF.AddHistoryField(?mod:CCExchange28Day,30)
  SELF.AddHistoryField(?mod:AccessoryProduct,39)
  SELF.AddHistoryField(?mod:ModelSpecificURL1,27)
  SELF.AddHistoryField(?mod:SerialisedProduct,40)
  SELF.AddHistoryField(?mod:ModelSpecificURL2,28)
  SELF.AddHistoryField(?mod:ModelSpecificURL3,29)
  SELF.AddHistoryField(?mod:ReturnPeriod,36)
  SELF.AddUpdateFile(Access:MODELNUM)
  SELF.AddItem(?Cancel,RequestCancelled)
  glo:WebJobConnection = getini('Connections', 'WebJob', '', clip(path()) & '\SQLAccess.ini')
  Relate:AccSKU.Open
  Relate:CARISMA.Open
  Relate:MANUFACT_ALIAS.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MODELNUM
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW7.Init(?List,Queue:Browse.ViewPosition,BRW7::View:Browse,Queue:Browse,Relate:AccSKU,SELF)
  BRW10.Init(?List:2,Queue:Browse:1.ViewPosition,BRW10::View:Browse,Queue:Browse:1,Relate:MODELCOL,SELF)
  BRW15.Init(?List:3,Queue:Browse:2.ViewPosition,BRW15::View:Browse,Queue:Browse:2,Relate:PRODCODE,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  saveReturnPeriod = mod:ReturnPeriod ! #12955
  DO ShowGroupAccessoryProduct
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW7.Q &= Queue:Browse
  BRW7.RetainRow = 0
  BRW7.AddSortOrder(,ASKU:UK_AccessSKUID)
  BRW7.AddField(ASKU:AccessoryName,BRW7.Q.ASKU:AccessoryName)
  BRW7.AddField(ASKU:OracleCode,BRW7.Q.ASKU:OracleCode)
  BRW7.AddField(ASKU:AccessorySKUCode,BRW7.Q.ASKU:AccessorySKUCode)
  BRW7.AddField(ASKU:AccessSKUID,BRW7.Q.ASKU:AccessSKUID)
  BRW10.Q &= Queue:Browse:1
  BRW10.RetainRow = 0
  BRW10.AddSortOrder(,moc:Colour_Key)
  BRW10.AddRange(moc:Model_Number,Relate:MODELCOL,Relate:MODELNUM)
  BRW10.AddLocator(BRW10::Sort0:Locator)
  BRW10::Sort0:Locator.Init(,moc:Colour,1,BRW10)
  BIND('tmp:ContractOracleCode',tmp:ContractOracleCode)
  BIND('tmp:PAYTOracleCode',tmp:PAYTOracleCode)
  BRW10.AddField(moc:Colour,BRW10.Q.moc:Colour)
  BRW10.AddField(tmp:ContractOracleCode,BRW10.Q.tmp:ContractOracleCode)
  BRW10.AddField(tmp:PAYTOracleCode,BRW10.Q.tmp:PAYTOracleCode)
  BRW10.AddField(moc:Record_Number,BRW10.Q.moc:Record_Number)
  BRW10.AddField(moc:Model_Number,BRW10.Q.moc:Model_Number)
  BRW15.Q &= Queue:Browse:2
  BRW15.RetainRow = 0
  BRW15.AddSortOrder(,prd:ModelProductKey)
  BRW15.AddRange(prd:ModelNumber,Relate:PRODCODE,Relate:MODELNUM)
  BRW15.AddLocator(BRW15::Sort0:Locator)
  BRW15::Sort0:Locator.Init(,prd:ProductCode,1,BRW15)
  BRW15.AddField(prd:ProductCode,BRW15.Q.prd:ProductCode)
  BRW15.AddField(prd:WarrantyPeriod,BRW15.Q.prd:WarrantyPeriod)
  BRW15.AddField(prd:RecordNumber,BRW15.Q.prd:RecordNumber)
  BRW15.AddField(prd:ModelNumber,BRW15.Q.prd:ModelNumber)
  IF ?mod:OEM{Prop:Checked} = True
    UNHIDE(?mod:OEMManufacturer)
  END
  IF ?mod:OEM{Prop:Checked} = False
    HIDE(?mod:OEMManufacturer)
  END
  FDCB8.Init(mod:Unit_Type,?mod:Unit_Type,Queue:FileDropCombo:1.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:1,Relate:UNITTYPE,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:1
  FDCB8.AddSortOrder(uni:Unit_Type_Key)
  FDCB8.AddField(uni:Unit_Type,FDCB8.Q.uni:Unit_Type)
  FDCB8.AddUpdateField(uni:Unit_Type,mod:Unit_Type)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  FDCB18.Init(mod:OEMManufacturer,?mod:OEMManufacturer,Queue:FileDropCombo.ViewPosition,FDCB18::View:FileDropCombo,Queue:FileDropCombo,Relate:MANUFACT_ALIAS,ThisWindow,GlobalErrors,0,1,0)
  FDCB18.Q &= Queue:FileDropCombo
  FDCB18.AddSortOrder(man_ali:Manufacturer_Key)
  FDCB18.AddField(man_ali:Manufacturer,FDCB18.Q.man_ali:Manufacturer)
  ThisWindow.AddItem(FDCB18.WindowComponent)
  FDCB18.DefaultFill = 0
  FDCB19.Init(rrct:Tier,?rrct:Tier,Queue:FileDropCombo:2.ViewPosition,FDCB19::View:FileDropCombo,Queue:FileDropCombo:2,Relate:SIDRRCT,ThisWindow,GlobalErrors,0,1,0)
  FDCB19.Q &= Queue:FileDropCombo:2
  FDCB19.AddSortOrder(rrct:TierKey)
  FDCB19.AddField(rrct:Tier,FDCB19.Q.rrct:Tier)
  FDCB19.AddField(rrct:RecordNumber,FDCB19.Q.rrct:RecordNumber)
  FDCB19.AddUpdateField(rrct:Tier,mod:RetailRepairTier)
  ThisWindow.AddItem(FDCB19.WindowComponent)
  FDCB19.DefaultFill = 0
  BRW10.AskProcedure = 1
  BRW7.AskProcedure = 2
  BRW15.AskProcedure = 3
  SELF.SetAlerts()
  BRW7::View:Browse{PROP:SQLFilter} = 'OracleCode = ''' & clip(tmp:ContractOracleCode) & ''' OR OracleCode = ''' & clip(tmp:PAYTOracleCode) & ''''
  
  
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AccSKU.Close
    Relate:CARISMA.Close
    Relate:MANUFACT_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMODELNUM',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    mod:Manufacturer = glo:select1
    mod:ESN_Length_To = 16
    mod:MSN_Length_To = 10
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_insert# = 1
  tmp:SavedColour = ''
  If request = Insertrecord
  !    Case number
  !        Of 1
  !            access:accessor.cancelautoinc()
  !
  !            do_insert# = 0
  !            If mod:model_number = ''
  !                Case MessageEx('You have not entered a Model Number.','ServiceBase 2000',|
  !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                    Of 1 ! &OK Button
  !                End!Case MessageEx
  !            Else!If mod:model_number = ''
  !                Pick_Accessories_Only
  !        
  !                If Records(glo:Q_Accessory)
  !                    Loop x$ = 1 To Records(glo:Q_Accessory)
  !                        Get(glo:Q_Accessory,x$)
  !                        get(accessor,0)
  !                        if access:accessor.primerecord() = level:benign
  !                            acr:accessory    = glo:accessory_pointer
  !                            acr:model_number = mod:model_number
  !                            if access:accessor.tryinsert()
  !                                access:accessor.cancelautoinc()
  !                            end
  !                        end!if access:accessor.primerecord() = level:benign
  !                    End!Loop x$ = 1 To Records(glo:Q_Accessory)
  !                End!If Records(glo:Q_Accessory)
  !            End !If mod:model_number = ''
  !        Of 2
  !            access:modelcol.cancelautoinc()
  !            do_insert# = 0
  !            If mod:model_number = ''
  !                Case MessageEx('You have not entered a Model Number.','ServiceBase 2000',|
  !                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  !                    Of 1 ! &OK Button
  !                End!Case MessageEx
  !            Else!If mod:model_number = ''
  !                Pick_Colour
  !        
  !                If Records(glo:Queue)
  !                    Loop x$ = 1 To Records(glo:Queue)
  !                        Get(glo:Queue,x$)
  !                        get(modelcol,0)
  !                        if access:modelcol.primerecord() = level:benign
  !                          moc:model_number  = mod:model_number
  !                          moc:colour        = glo:pointer
  !                          if access:modelcol.tryinsert()
  !                              access:modelcol.cancelautoinc()
  !                          end
  !                        end!if access:modelcol.primerecord() = level:benign
  !                    End!Loop x$ = 1 To Records(glo:Q_Accessory)
  !                End!If Records(glo:Q_Accessory)
  !            End !If mod:model_number = ''
  !    End!Case number
  Else
      if request = DeleteRecord
          BRW10.UpdateBuffer()
          tmp:SavedColour = moc:Colour
      end
  End !If request = Insertrecord
  
  If do_insert# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateMODELCOL(mod:Manufacturer)
      UpdateAccessory(tmp:ContractOracleCode, tmp:PAYTOracleCode)
      UpdateProductCode
    END
    ReturnValue = GlobalResponse
  END
  if Number = 2
      if Request = DeleteRecord and ReturnValue = RequestCompleted
          Access:CARISMA.ClearKey(cma:ManufactModColourKey)
          cma:Manufacturer = mod:Manufacturer
          cma:ModelNo = mod:Model_Number
          cma:Colour = tmp:SavedColour
          if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
              Relate:CARISMA.Delete(0)
          end
      end
  end
  
  End !do_insert# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?mod:OEM
      IF ?mod:OEM{Prop:Checked} = True
        UNHIDE(?mod:OEMManufacturer)
      END
      IF ?mod:OEM{Prop:Checked} = False
        HIDE(?mod:OEMManufacturer)
      END
      ThisWindow.Reset
    OF ?mod:Specify_Unit_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Specify_Unit_Type, Accepted)
      Do Hide_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Specify_Unit_Type, Accepted)
    OF ?mod:Unit_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Unit_Type, Accepted)
      If ~0{prop:acceptall}
          If mod:specify_unit_type = 'YES'
              access:unittype.clearkey(uni:unit_type_key)
              uni:unit_type = mod:unit_type
              if access:unittype.fetch(uni:unit_type_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browseunittype
                  if globalresponse = requestcompleted
                      mod:unit_type = uni:unit_type
                      display()
                  end
                  globalrequest     = saverequest#
              end !if access:unittype.fetch(uni:unit_type_key)
          End !If mod:specify_unit_type = 'YES'
      End !If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:Unit_Type, Accepted)
    OF ?rrct:Tier
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rrct:Tier, Accepted)
      do DisplayRetailRepairCharge
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rrct:Tier, Accepted)
    OF ?mod:AccessoryProduct
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:AccessoryProduct, Accepted)
      DO ShowGroupAccessoryProduct
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?mod:AccessoryProduct, Accepted)
    OF ?List:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, Accepted)
      BRW7::View:Browse{PROP:SQLFilter} = 'OracleCode = ''' & clip(tmp:ContractOracleCode) & ''' OR OracleCode = ''' & clip(tmp:PAYTOracleCode) & ''''
      BRW7.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF (mod:ReturnPeriod <> saveReturnPeriod) ! #12955
      Access:Users.Clearkey(use:Password_Key)
      use:Password = glo:Password
      IF (Access:Users.TryFetch(use:Password_Key))
      END  ! IF
      LINEPRINT(FORMAT(TODAY(),@d06) & ',' & |
                  CLIP(use:User_Code) & ',' & |
                  CLIP(mod:Manufacturer) & ',' & |
                  CLIP(mod:Model_Number) & ',' & |
                  mod:ReturnPeriod,'ReturnPeriod.log')
  END ! IF
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMODELNUM')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List:2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, NewSelection)
      BRW7::View:Browse{PROP:SQLFilter} = 'OracleCode = ''' & clip(tmp:ContractOracleCode) & ''' OR OracleCode = ''' & clip(tmp:PAYTOracleCode) & ''''
      BRW7.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List:2, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If glo:select2 <> 'YES'
          Hide(?msn_group)
      Else
          Unhide(?msn_group)
      End
      Do Hide_Fields
      !If mod:manufacturer = 'NOKIA'
      !    Unhide(?nokia_group)
      !Else!If mod:manufacturer = 'NOKIA'
      !    Hide(?nokia_group)
      !End!If mod:manufacturer = 'NOKIA'
      
      do DisplayRetailRepairCharge
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW7.ResetSort(1)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB8.TakeAccepted PROCEDURE

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, TakeAccepted, ())
  If mod:specify_unit_type = 'YES'
  
  PARENT.TakeAccepted
  End
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(8, TakeAccepted, ())


BRW7.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW7.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, Init, (SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM))
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.DeleteControl=?Delete:2
  END
  if WM.Request <> ViewRecord
      self.ChangeControl = ?Change:2
  end
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, Init, (SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM))


BRW10.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ResetSort, (BYTE Force),BYTE)
  ReturnValue = PARENT.ResetSort(Force)
  if (records(Queue:Browse:1) > 0)
      ?Insert{PROP:Disable} = 0
  else
      ?Insert{PROP:Disable} = 1
  end
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ResetSort, (BYTE Force),BYTE)
  RETURN ReturnValue


BRW10.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW10.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW10::RecordStatus  BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  BRW10::RecordStatus=ReturnValue
  IF BRW10::RecordStatus NOT=Record:OK THEN RETURN BRW10::RecordStatus.
  ! Get the Oracle Code
  tmp:ContractOracleCode = ''
  tmp:PAYTOracleCode = ''
  
  Access:CARISMA.ClearKey(cma:ManufactModColourKey)
  cma:Manufacturer = mod:Manufacturer
  cma:ModelNo = moc:Model_Number
  cma:Colour = moc:Colour
  if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
      tmp:ContractOracleCode = cma:ContractOracleCode
      tmp:PAYTOracleCode = cma:PAYTOracleCode
  end
  
  ReturnValue=BRW10::RecordStatus
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(10, ValidateRecord, (),BYTE)
  RETURN ReturnValue


BRW15.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW15.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

