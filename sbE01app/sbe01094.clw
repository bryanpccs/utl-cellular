

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01094.INC'),ONCE        !Local module procedure declarations
                     END


UpdateStatusRecipients PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::str:Record  LIKE(str:RECORD),STATIC
QuickWindow          WINDOW('Update the STARECIP File'),AT(,,200,84),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateStatusRecipients'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,192,58),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?str:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(68,20,40,10),USE(str:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Ref Number'),AT(8,34),USE(?str:RefNumber:Prompt),TRN
                           ENTRY(@s8),AT(68,34,40,10),USE(str:RefNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Ref Number'),TIP('Ref Number'),UPR
                           PROMPT('Recipient Type'),AT(8,48),USE(?str:RecipientType:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(68,48,124,10),USE(str:RecipientType),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Recipient Type'),TIP('Recipient Type'),UPR
                         END
                       END
                       BUTTON('OK'),AT(102,66,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(151,66,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(151,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?str:RecordNumber:Prompt{prop:FontColor} = -1
    ?str:RecordNumber:Prompt{prop:Color} = 15066597
    If ?str:RecordNumber{prop:ReadOnly} = True
        ?str:RecordNumber{prop:FontColor} = 65793
        ?str:RecordNumber{prop:Color} = 15066597
    Elsif ?str:RecordNumber{prop:Req} = True
        ?str:RecordNumber{prop:FontColor} = 65793
        ?str:RecordNumber{prop:Color} = 8454143
    Else ! If ?str:RecordNumber{prop:Req} = True
        ?str:RecordNumber{prop:FontColor} = 65793
        ?str:RecordNumber{prop:Color} = 16777215
    End ! If ?str:RecordNumber{prop:Req} = True
    ?str:RecordNumber{prop:Trn} = 0
    ?str:RecordNumber{prop:FontStyle} = font:Bold
    ?str:RefNumber:Prompt{prop:FontColor} = -1
    ?str:RefNumber:Prompt{prop:Color} = 15066597
    If ?str:RefNumber{prop:ReadOnly} = True
        ?str:RefNumber{prop:FontColor} = 65793
        ?str:RefNumber{prop:Color} = 15066597
    Elsif ?str:RefNumber{prop:Req} = True
        ?str:RefNumber{prop:FontColor} = 65793
        ?str:RefNumber{prop:Color} = 8454143
    Else ! If ?str:RefNumber{prop:Req} = True
        ?str:RefNumber{prop:FontColor} = 65793
        ?str:RefNumber{prop:Color} = 16777215
    End ! If ?str:RefNumber{prop:Req} = True
    ?str:RefNumber{prop:Trn} = 0
    ?str:RefNumber{prop:FontStyle} = font:Bold
    ?str:RecipientType:Prompt{prop:FontColor} = -1
    ?str:RecipientType:Prompt{prop:Color} = 15066597
    If ?str:RecipientType{prop:ReadOnly} = True
        ?str:RecipientType{prop:FontColor} = 65793
        ?str:RecipientType{prop:Color} = 15066597
    Elsif ?str:RecipientType{prop:Req} = True
        ?str:RecipientType{prop:FontColor} = 65793
        ?str:RecipientType{prop:Color} = 8454143
    Else ! If ?str:RecipientType{prop:Req} = True
        ?str:RecipientType{prop:FontColor} = 65793
        ?str:RecipientType{prop:Color} = 16777215
    End ! If ?str:RecipientType{prop:Req} = True
    ?str:RecipientType{prop:Trn} = 0
    ?str:RecipientType{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateStatusRecipients',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateStatusRecipients',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateStatusRecipients',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?str:RecordNumber:Prompt;  SolaceCtrlName = '?str:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?str:RecordNumber;  SolaceCtrlName = '?str:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?str:RefNumber:Prompt;  SolaceCtrlName = '?str:RefNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?str:RefNumber;  SolaceCtrlName = '?str:RefNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?str:RecipientType:Prompt;  SolaceCtrlName = '?str:RecipientType:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?str:RecipientType;  SolaceCtrlName = '?str:RecipientType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateStatusRecipients')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateStatusRecipients')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?str:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(str:Record,History::str:Record)
  SELF.AddHistoryField(?str:RecordNumber,1)
  SELF.AddHistoryField(?str:RefNumber,2)
  SELF.AddHistoryField(?str:RecipientType,3)
  SELF.AddUpdateFile(Access:STARECIP)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STARECIP.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STARECIP
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STARECIP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateStatusRecipients',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateStatusRecipients')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

UpdateAccessory PROCEDURE (contractOracleCode, paytOracleCode) !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
oracleCodeQ          QUEUE,PRE()
oracleCode           CSTRING(31)
                     END
tmp:OracleCode       CSTRING(31)
History::ASKU:Record LIKE(ASKU:RECORD),STATIC
QuickWindow          WINDOW('Update the AccessSKU File'),AT(,,220,104),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateAccessory'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,68),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Oracle Code'),AT(8,20),USE(?OracleCode:Prompt)
                           LIST,AT(84,20,124,10),USE(tmp:OracleCode),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('120L(2)|M@s30@'),DROP(10),FROM(oracleCodeQ)
                           PROMPT('Accessory Name'),AT(8,36),USE(?ASKU:AccessoryName:Prompt)
                           ENTRY(@s30),AT(84,36,124,10),USE(ASKU:AccessoryName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ
                           PROMPT('Accessory SKU Code'),AT(8,52),USE(?ASKU:AccessorySKUCode:Prompt)
                           ENTRY(@s22),AT(84,52,92,10),USE(ASKU:AccessorySKUCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                         END
                       END
                       BUTTON('OK'),AT(100,80,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,80,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,76,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?OracleCode:Prompt{prop:FontColor} = -1
    ?OracleCode:Prompt{prop:Color} = 15066597
    ?tmp:OracleCode{prop:FontColor} = 65793
    ?tmp:OracleCode{prop:Color}= 16777215
    ?tmp:OracleCode{prop:Color,2} = 16777215
    ?tmp:OracleCode{prop:Color,3} = 12937777
    ?ASKU:AccessoryName:Prompt{prop:FontColor} = -1
    ?ASKU:AccessoryName:Prompt{prop:Color} = 15066597
    If ?ASKU:AccessoryName{prop:ReadOnly} = True
        ?ASKU:AccessoryName{prop:FontColor} = 65793
        ?ASKU:AccessoryName{prop:Color} = 15066597
    Elsif ?ASKU:AccessoryName{prop:Req} = True
        ?ASKU:AccessoryName{prop:FontColor} = 65793
        ?ASKU:AccessoryName{prop:Color} = 8454143
    Else ! If ?ASKU:AccessoryName{prop:Req} = True
        ?ASKU:AccessoryName{prop:FontColor} = 65793
        ?ASKU:AccessoryName{prop:Color} = 16777215
    End ! If ?ASKU:AccessoryName{prop:Req} = True
    ?ASKU:AccessoryName{prop:Trn} = 0
    ?ASKU:AccessoryName{prop:FontStyle} = font:Bold
    ?ASKU:AccessorySKUCode:Prompt{prop:FontColor} = -1
    ?ASKU:AccessorySKUCode:Prompt{prop:Color} = 15066597
    If ?ASKU:AccessorySKUCode{prop:ReadOnly} = True
        ?ASKU:AccessorySKUCode{prop:FontColor} = 65793
        ?ASKU:AccessorySKUCode{prop:Color} = 15066597
    Elsif ?ASKU:AccessorySKUCode{prop:Req} = True
        ?ASKU:AccessorySKUCode{prop:FontColor} = 65793
        ?ASKU:AccessorySKUCode{prop:Color} = 8454143
    Else ! If ?ASKU:AccessorySKUCode{prop:Req} = True
        ?ASKU:AccessorySKUCode{prop:FontColor} = 65793
        ?ASKU:AccessorySKUCode{prop:Color} = 16777215
    End ! If ?ASKU:AccessorySKUCode{prop:Req} = True
    ?ASKU:AccessorySKUCode{prop:Trn} = 0
    ?ASKU:AccessorySKUCode{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateAccessory',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateAccessory',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateAccessory',1)
    SolaceViewVars('oracleCodeQ:oracleCode',oracleCodeQ:oracleCode,'UpdateAccessory',1)
    SolaceViewVars('tmp:OracleCode',tmp:OracleCode,'UpdateAccessory',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OracleCode:Prompt;  SolaceCtrlName = '?OracleCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:OracleCode;  SolaceCtrlName = '?tmp:OracleCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ASKU:AccessoryName:Prompt;  SolaceCtrlName = '?ASKU:AccessoryName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ASKU:AccessoryName;  SolaceCtrlName = '?ASKU:AccessoryName';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ASKU:AccessorySKUCode:Prompt;  SolaceCtrlName = '?ASKU:AccessorySKUCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ASKU:AccessorySKUCode;  SolaceCtrlName = '?ASKU:AccessorySKUCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Accessory'
  OF ChangeRecord
    ActionMessage = 'Change An Accessory'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateAccessory')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateAccessory')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?OracleCode:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(ASKU:Record,History::ASKU:Record)
  SELF.AddHistoryField(?ASKU:AccessoryName,3)
  SELF.AddHistoryField(?ASKU:AccessorySKUCode,4)
  SELF.AddUpdateFile(Access:AccSKU)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AccSKU.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:AccSKU
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  if len(clip(contractOracleCode)) > 0
      oracleCodeQ.oracleCode = clip(contractOracleCode)
      add(oracleCodeQ)
  end
  
  if len(clip(paytOracleCode)) > 0
      oracleCodeQ.oracleCode = clip(paytOracleCode)
      add(oracleCodeQ)
  end
  
  if (self.Request = InsertRecord)
      if len(clip(contractOracleCode)) > 0 and len(clip(paytOracleCode)) = 0
          tmp:OracleCode = contractOracleCode
      else
          if len(clip(paytOracleCode)) > 0 and len(clip(contractOracleCode)) = 0
              tmp:OracleCode = paytOracleCode
          end
      end
  else
      tmp:OracleCode = clip(ASKU:OracleCode)
  end
  
  Do RecolourWindow
  ?tmp:OracleCode{prop:vcr} = TRUE
  ?tmp:OracleCode{prop:vcr} = False
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AccSKU.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateAccessory',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      if len(clip(tmp:OracleCode)) = 0
          select(?tmp:OracleCode)
          cycle
      end
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ASKU:OracleCode = clip(tmp:OracleCode)
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateAccessory')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

