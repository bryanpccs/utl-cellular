

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01033.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Audit PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:StatusType       STRING('JOB')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:User             STRING(50)
BRW1::View:Browse    VIEW(AUDIT)
                       PROJECT(aud:Date)
                       PROJECT(aud:Time)
                       PROJECT(aud:Action)
                       PROJECT(aud:User)
                       PROJECT(aud:Notes)
                       PROJECT(aud:record_number)
                       PROJECT(aud:Ref_Number)
                       PROJECT(aud:Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
aud:Date               LIKE(aud:Date)                 !List box control field - type derived from field
aud:Time               LIKE(aud:Time)                 !List box control field - type derived from field
tmp:User               LIKE(tmp:User)                 !List box control field - type derived from local data
aud:Action             LIKE(aud:Action)               !List box control field - type derived from field
aud:User               LIKE(aud:User)                 !List box control field - type derived from field
aud:Notes              LIKE(aud:Notes)                !Browse hot field - type derived from field
aud:record_number      LIKE(aud:record_number)        !Primary key field - type derived from field
aud:Ref_Number         LIKE(aud:Ref_Number)           !Browse key field - type derived from field
aud:Type               LIKE(aud:Type)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK2::aud:Ref_Number       LIKE(aud:Ref_Number)
HK2::aud:Type             LIKE(aud:Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Audit File'),AT(,,610,272),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Audit'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,48,400,216),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('50R(2)|M~Date~L@d6b@26R(2)|M~Time~L@t1@125L(2)|M~User~@s30@272L(2)|M~Action~@s80' &|
   '@12L(2)|M~User~@s3@'),FROM(Queue:Browse:1)
                       OPTION('Status Type'),AT(252,16,156,28),USE(tmp:StatusType),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                         RADIO('Job'),AT(260,28),USE(?Option1:Radio1),VALUE('JOB')
                         RADIO('Exchange'),AT(291,28),USE(?Option1:Radio2),VALUE('EXC')
                         RADIO('Loan'),AT(343,28),USE(?Option1:Radio3),VALUE('LOA')
                         RADIO('All'),AT(379,28),USE(?Option1:Radio4),VALUE('ALL')
                       END
                       PROMPT('Notes'),AT(420,8),USE(?AUD:Notes:Prompt),TRN,FONT('Tahoma',8,COLOR:Navy,FONT:bold),COLOR(COLOR:Silver)
                       TEXT,AT(420,20,184,120),USE(aud:Notes),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                       PANEL,AT(416,4,192,140),USE(?Panel1)
                       SHEET,AT(4,4,408,264),USE(?CurrentTab),SPREAD
                         TAB('By Date'),USE(?Tab:2)
                         END
                         TAB('By Action'),USE(?Tab2)
                           ENTRY(@s30),AT(8,20,124,10),USE(aud:Action),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                         TAB('By User'),USE(?Tab3)
                           ENTRY(@s2),AT(8,20,64,10),USE(aud:User),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                       END
                       BUTTON('Close'),AT(532,248,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort3:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(tmp:StatusType) = 'ALL'
BRW1::Sort4:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(tmp:StatusType) <> 'ALL'
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(tmp:StatusType) = 'ALL'
BRW1::Sort5:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(tmp:StatusType) <> 'ALL'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(tmp:StatusType) = 'ALL'
BRW1::Sort6:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(tmp:StatusType) <> 'ALL'
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?tmp:StatusType{prop:Font,3} = -1
    ?tmp:StatusType{prop:Color} = 15066597
    ?tmp:StatusType{prop:Trn} = 0
    ?Option1:Radio1{prop:Font,3} = -1
    ?Option1:Radio1{prop:Color} = 15066597
    ?Option1:Radio1{prop:Trn} = 0
    ?Option1:Radio2{prop:Font,3} = -1
    ?Option1:Radio2{prop:Color} = 15066597
    ?Option1:Radio2{prop:Trn} = 0
    ?Option1:Radio3{prop:Font,3} = -1
    ?Option1:Radio3{prop:Color} = 15066597
    ?Option1:Radio3{prop:Trn} = 0
    ?Option1:Radio4{prop:Font,3} = -1
    ?Option1:Radio4{prop:Color} = 15066597
    ?Option1:Radio4{prop:Trn} = 0
    ?AUD:Notes:Prompt{prop:FontColor} = -1
    ?AUD:Notes:Prompt{prop:Color} = 15066597
    If ?aud:Notes{prop:ReadOnly} = True
        ?aud:Notes{prop:FontColor} = 65793
        ?aud:Notes{prop:Color} = 15066597
    Elsif ?aud:Notes{prop:Req} = True
        ?aud:Notes{prop:FontColor} = 65793
        ?aud:Notes{prop:Color} = 8454143
    Else ! If ?aud:Notes{prop:Req} = True
        ?aud:Notes{prop:FontColor} = 65793
        ?aud:Notes{prop:Color} = 16777215
    End ! If ?aud:Notes{prop:Req} = True
    ?aud:Notes{prop:Trn} = 0
    ?aud:Notes{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597

    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?aud:Action{prop:ReadOnly} = True
        ?aud:Action{prop:FontColor} = 65793
        ?aud:Action{prop:Color} = 15066597
    Elsif ?aud:Action{prop:Req} = True
        ?aud:Action{prop:FontColor} = 65793
        ?aud:Action{prop:Color} = 8454143
    Else ! If ?aud:Action{prop:Req} = True
        ?aud:Action{prop:FontColor} = 65793
        ?aud:Action{prop:Color} = 16777215
    End ! If ?aud:Action{prop:Req} = True
    ?aud:Action{prop:Trn} = 0
    ?aud:Action{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?aud:User{prop:ReadOnly} = True
        ?aud:User{prop:FontColor} = 65793
        ?aud:User{prop:Color} = 15066597
    Elsif ?aud:User{prop:Req} = True
        ?aud:User{prop:FontColor} = 65793
        ?aud:User{prop:Color} = 8454143
    Else ! If ?aud:User{prop:Req} = True
        ?aud:User{prop:FontColor} = 65793
        ?aud:User{prop:Color} = 16777215
    End ! If ?aud:User{prop:Req} = True
    ?aud:User{prop:Trn} = 0
    ?aud:User{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Audit',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Audit',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Audit',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Audit',1)
    SolaceViewVars('tmp:StatusType',tmp:StatusType,'Browse_Audit',1)
    SolaceViewVars('tmp:User',tmp:User,'Browse_Audit',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StatusType;  SolaceCtrlName = '?tmp:StatusType';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio1;  SolaceCtrlName = '?Option1:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio2;  SolaceCtrlName = '?Option1:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio3;  SolaceCtrlName = '?Option1:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Option1:Radio4;  SolaceCtrlName = '?Option1:Radio4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AUD:Notes:Prompt;  SolaceCtrlName = '?AUD:Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?aud:Notes;  SolaceCtrlName = '?aud:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?aud:Action;  SolaceCtrlName = '?aud:Action';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?aud:User;  SolaceCtrlName = '?aud:User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Audit')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Audit')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:AUDIT,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,aud:Ref_Number_Key)
  BRW1.AddRange(aud:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(,aud:Date,1,BRW1)
  BRW1.AddSortOrder(,aud:TypeRefKey)
  BRW1.AddRange(aud:Type,tmp:StatusType)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(,aud:Date,1,BRW1)
  BRW1.AddSortOrder(,aud:Action_Key)
  BRW1.AddRange(aud:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?aud:Action,aud:Action,1,BRW1)
  BRW1.AddSortOrder(,aud:TypeActionKey)
  BRW1.AddRange(aud:Type,tmp:StatusType)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?aud:Action,aud:Action,1,BRW1)
  BRW1.AddSortOrder(,aud:User_Key)
  BRW1.AddRange(aud:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?aud:User,aud:User,1,BRW1)
  BRW1.AddSortOrder(,aud:TypeUserKey)
  BRW1.AddRange(aud:Type,tmp:StatusType)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?aud:User,aud:User,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,aud:Ref_Number_Key)
  BRW1.AddRange(aud:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,aud:Date,1,BRW1)
  BIND('tmp:User',tmp:User)
  BRW1.AddField(aud:Date,BRW1.Q.aud:Date)
  BRW1.AddField(aud:Time,BRW1.Q.aud:Time)
  BRW1.AddField(tmp:User,BRW1.Q.tmp:User)
  BRW1.AddField(aud:Action,BRW1.Q.aud:Action)
  BRW1.AddField(aud:User,BRW1.Q.aud:User)
  BRW1.AddField(aud:Notes,BRW1.Q.aud:Notes)
  BRW1.AddField(aud:record_number,BRW1.Q.aud:record_number)
  BRW1.AddField(aud:Ref_Number,BRW1.Q.aud:Ref_Number)
  BRW1.AddField(aud:Type,BRW1.Q.aud:Type)
  QuickWindow{PROP:MinWidth}=529
  QuickWindow{PROP:MinHeight}=272
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Audit',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:StatusType
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
      thiswindow.reset(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:StatusType, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Audit')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?aud:Action
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?aud:Action, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?aud:Action, Selected)
    OF ?aud:User
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?aud:User, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?aud:User, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2 And Upper(tmp:StatusType) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
  ELSIF Choice(?CurrentTab) = 3 And Upper(tmp:StatusType) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
  ELSIF Choice(?CurrentTab) = 1 And Upper(tmp:StatusType) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
  ELSIF Choice(?CurrentTab) = 1 And Upper(tmp:StatusType) <> 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = tmp:StatusType
     end
  ELSIF Choice(?CurrentTab) = 2 And Upper(tmp:StatusType) <> 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = tmp:StatusType
     end
  ELSIF Choice(?CurrentTab) = 3 And Upper(tmp:StatusType) <> 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = glo:select12
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = tmp:StatusType
     end
  ELSE
  END
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(tmp:StatusType) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(tmp:StatusType) <> 'ALL'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(tmp:StatusType) = 'ALL'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(tmp:StatusType) <> 'ALL'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(tmp:StatusType) = 'ALL'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(tmp:StatusType) <> 'ALL'
    RETURN SELF.SetSort(6,Force)
  ELSE
    RETURN SELF.SetSort(7,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  access:users.clearkey(use:user_code_key)
  use:user_code   = aud:user
  If access:users.tryfetch(use:user_code_key) = Level:Benign
      !Found
      tmp:user    = Clip(use:Forename) & ' ' & Clip(use:Surname)
  Else! If access:users.tryfetch(use:user_code_key) = Level:Benign
      !Error
      tmp:user    = '* ERROR - USER CODE: ' & Clip(aud:User) & ' *'
  End! If access:users.tryfetch(use:user_code_key) = Level:Benign
  PARENT.SetQueueRecord
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?Panel1, Resize:FixRight+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?AUD:Notes, Resize:FixRight+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?AUD:Notes:Prompt, Resize:FixRight+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?AUD:User, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?AUD:Action, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

