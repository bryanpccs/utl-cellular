

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01026.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Accessories PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
Model_Number_Temp    STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Model_Number_Temp
mod:Model_Number       LIKE(mod:Model_Number)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(ACCESSOR)
                       PROJECT(acr:Accessory)
                       PROJECT(acr:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
acr:Accessory          LIKE(acr:Accessory)            !List box control field - type derived from field
acr:Model_Number       LIKE(acr:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(MODELNUM)
                       PROJECT(mod:Model_Number)
                     END
QuickWindow          WINDOW('Browse the Accessories File'),AT(,,248,204),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Accessories'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,52,148,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M~Accessory~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(168,104,76,20),USE(?Insert:2),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(168,128,76,20),USE(?Change:2),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(168,152,76,20),USE(?Delete:2),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,156,196),USE(?CurrentTab),SPREAD
                         TAB('By Accessory'),USE(?Tab:2)
                           COMBO(@s30),AT(8,20,100,10),USE(Model_Number_Temp),IMM,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Model Number'),AT(112,21),USE(?Prompt1),FONT(,,COLOR:White,)
                           ENTRY(@s30),AT(8,36,124,10),USE(acr:Accessory),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(168,180,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?Model_Number_Temp{prop:ReadOnly} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 15066597
    Elsif ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 8454143
    Else ! If ?Model_Number_Temp{prop:Req} = True
        ?Model_Number_Temp{prop:FontColor} = 65793
        ?Model_Number_Temp{prop:Color} = 16777215
    End ! If ?Model_Number_Temp{prop:Req} = True
    ?Model_Number_Temp{prop:Trn} = 0
    ?Model_Number_Temp{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?acr:Accessory{prop:ReadOnly} = True
        ?acr:Accessory{prop:FontColor} = 65793
        ?acr:Accessory{prop:Color} = 15066597
    Elsif ?acr:Accessory{prop:Req} = True
        ?acr:Accessory{prop:FontColor} = 65793
        ?acr:Accessory{prop:Color} = 8454143
    Else ! If ?acr:Accessory{prop:Req} = True
        ?acr:Accessory{prop:FontColor} = 65793
        ?acr:Accessory{prop:Color} = 16777215
    End ! If ?acr:Accessory{prop:Req} = True
    ?acr:Accessory{prop:Trn} = 0
    ?acr:Accessory{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Accessories',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Accessories',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Accessories',1)
    SolaceViewVars('Model_Number_Temp',Model_Number_Temp,'Browse_Accessories',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:2;  SolaceCtrlName = '?Change:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Temp;  SolaceCtrlName = '?Model_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?acr:Accessory;  SolaceCtrlName = '?acr:Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Accessories')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Accessories')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCESSOR.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ACCESSOR,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,acr:Accesory_Key)
  BRW1.AddRange(acr:Model_Number,Model_Number_Temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?acr:Accessory,acr:Accessory,1,BRW1)
  BIND('Model_Number_Temp',Model_Number_Temp)
  BRW1.AddField(acr:Accessory,BRW1.Q.acr:Accessory)
  BRW1.AddField(acr:Model_Number,BRW1.Q.acr:Model_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB5.Init(Model_Number_Temp,?Model_Number_Temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:MODELNUM,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(mod:Model_Number_Key)
  FDCB5.AddField(mod:Model_Number,FDCB5.Q.mod:Model_Number)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCESSOR.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Accessories',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateACCESSOR
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Accessories')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB5.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

