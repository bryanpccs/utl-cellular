

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01086.INC'),ONCE        !Local module procedure declarations
                     END


Change_Status PROCEDURE (f_ref_number)                !Generated from procedure template - Window

FilesOpened          BYTE
despatch_description_temp STRING(30)
Courier_Temp         STRING(30)
Despatch_Type_Temp   STRING(30)
Model_Number_Temp    STRING(30)
Manufacturer_temp    STRING(30)
ESN_Temp             STRING(30)
current_status_temp  STRING(30)
new_status_temp      STRING(30)
tmp:JobStatus        BYTE
tmp:ExchangeStatus   BYTE
tmp:LoanStatus       BYTE
tmp:ExchangeCurrentStatus STRING(30)
tmp:ExchangeNewStatus STRING(30)
tmp:LoanCurrentStatus STRING(30)
tmp:LoanNewStatus    STRING(30)
tmp:reason           STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Change Status'),AT(,,231,219),FONT('Tahoma',8,,),CENTER,ICON('Pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,36),USE(?Sheet2),SPREAD
                         TAB('Select Status Type To Change'),USE(?Tab2)
                           CHECK('Job Status'),AT(10,20),USE(tmp:JobStatus),VALUE('1','0')
                           CHECK('Exchange Status'),AT(80,20),USE(tmp:ExchangeStatus),VALUE('1','0')
                           CHECK('Loan Status'),AT(168,20),USE(tmp:LoanStatus),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,44,224,144),USE(?Panel2),FILL(COLOR:Silver)
                       SHEET,AT(4,44,224,52),USE(?Sheet1),SPREAD
                         TAB('Job Status'),USE(?Job),HIDE
                           PROMPT('Current Status'),AT(8,60),USE(?current_status_temp:Prompt),TRN
                           ENTRY(@s30),AT(84,60,124,10),USE(current_status_temp),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('New Status'),AT(8,76),USE(?Prompt2),TRN
                           ENTRY(@s30),AT(84,76,124,10),USE(new_status_temp),FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),REQ,UPR
                           BUTTON,AT(212,76,10,10),USE(?LookupJobStatus),SKIP,ICON('List3.ico')
                         END
                         TAB('Exchange Status'),USE(?Exchange),HIDE
                           PROMPT('Current Status'),AT(8,60),USE(?tmp:ExchangeCurrentStatus:Prompt)
                           ENTRY(@s30),AT(84,60,124,10),USE(tmp:ExchangeCurrentStatus),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('New Status'),AT(8,76),USE(?tmp:ExchangeCurrentStatus:Prompt:2)
                           ENTRY(@s30),AT(84,76,124,10),USE(tmp:ExchangeNewStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),REQ,UPR
                           BUTTON,AT(212,76,10,10),USE(?LookupExchangeStatus),SKIP,ICON('List3.ico')
                         END
                         TAB('Loan Status'),USE(?Loan),HIDE
                           PROMPT('Current Status'),AT(8,60),USE(?tmp:LoanCurrentStatus:Prompt)
                           ENTRY(@s30),AT(84,60,124,10),USE(tmp:LoanCurrentStatus),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           ENTRY(@s30),AT(84,76,124,10),USE(tmp:LoanNewStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),REQ,UPR
                           BUTTON,AT(212,76,10,10),USE(?LookupLoanStatus),SKIP,ICON('List3.ico')
                           PROMPT('New Status'),AT(8,76),USE(?tmp:LoanCurrentStatus:Prompt:2)
                         END
                       END
                       SHEET,AT(4,100,224,88),USE(?Sheet3),HIDE,SPREAD
                         TAB('Reason For Status Change'),USE(?Tab5)
                           TEXT,AT(8,116,216,68),USE(tmp:reason),VSCROLL,FONT(,,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&OK'),AT(112,196,56,16),USE(?OkButton),LEFT,ICON('OK.gif'),DEFAULT
                       BUTTON('Cancel'),AT(168,196,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,192,224,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:new_status_temp                Like(new_status_temp)
look:tmp:ExchangeNewStatus                Like(tmp:ExchangeNewStatus)
look:tmp:LoanNewStatus                Like(tmp:LoanNewStatus)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?tmp:JobStatus{prop:Font,3} = -1
    ?tmp:JobStatus{prop:Color} = 15066597
    ?tmp:JobStatus{prop:Trn} = 0
    ?tmp:ExchangeStatus{prop:Font,3} = -1
    ?tmp:ExchangeStatus{prop:Color} = 15066597
    ?tmp:ExchangeStatus{prop:Trn} = 0
    ?tmp:LoanStatus{prop:Font,3} = -1
    ?tmp:LoanStatus{prop:Color} = 15066597
    ?tmp:LoanStatus{prop:Trn} = 0
    ?Panel2{prop:Fill} = 15066597

    ?Sheet1{prop:Color} = 15066597
    ?Job{prop:Color} = 15066597
    ?current_status_temp:Prompt{prop:FontColor} = -1
    ?current_status_temp:Prompt{prop:Color} = 15066597
    If ?current_status_temp{prop:ReadOnly} = True
        ?current_status_temp{prop:FontColor} = 65793
        ?current_status_temp{prop:Color} = 15066597
    Elsif ?current_status_temp{prop:Req} = True
        ?current_status_temp{prop:FontColor} = 65793
        ?current_status_temp{prop:Color} = 8454143
    Else ! If ?current_status_temp{prop:Req} = True
        ?current_status_temp{prop:FontColor} = 65793
        ?current_status_temp{prop:Color} = 16777215
    End ! If ?current_status_temp{prop:Req} = True
    ?current_status_temp{prop:Trn} = 0
    ?current_status_temp{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?new_status_temp{prop:ReadOnly} = True
        ?new_status_temp{prop:FontColor} = 65793
        ?new_status_temp{prop:Color} = 15066597
    Elsif ?new_status_temp{prop:Req} = True
        ?new_status_temp{prop:FontColor} = 65793
        ?new_status_temp{prop:Color} = 8454143
    Else ! If ?new_status_temp{prop:Req} = True
        ?new_status_temp{prop:FontColor} = 65793
        ?new_status_temp{prop:Color} = 16777215
    End ! If ?new_status_temp{prop:Req} = True
    ?new_status_temp{prop:Trn} = 0
    ?new_status_temp{prop:FontStyle} = font:Bold
    ?Exchange{prop:Color} = 15066597
    ?tmp:ExchangeCurrentStatus:Prompt{prop:FontColor} = -1
    ?tmp:ExchangeCurrentStatus:Prompt{prop:Color} = 15066597
    If ?tmp:ExchangeCurrentStatus{prop:ReadOnly} = True
        ?tmp:ExchangeCurrentStatus{prop:FontColor} = 65793
        ?tmp:ExchangeCurrentStatus{prop:Color} = 15066597
    Elsif ?tmp:ExchangeCurrentStatus{prop:Req} = True
        ?tmp:ExchangeCurrentStatus{prop:FontColor} = 65793
        ?tmp:ExchangeCurrentStatus{prop:Color} = 8454143
    Else ! If ?tmp:ExchangeCurrentStatus{prop:Req} = True
        ?tmp:ExchangeCurrentStatus{prop:FontColor} = 65793
        ?tmp:ExchangeCurrentStatus{prop:Color} = 16777215
    End ! If ?tmp:ExchangeCurrentStatus{prop:Req} = True
    ?tmp:ExchangeCurrentStatus{prop:Trn} = 0
    ?tmp:ExchangeCurrentStatus{prop:FontStyle} = font:Bold
    ?tmp:ExchangeCurrentStatus:Prompt:2{prop:FontColor} = -1
    ?tmp:ExchangeCurrentStatus:Prompt:2{prop:Color} = 15066597
    If ?tmp:ExchangeNewStatus{prop:ReadOnly} = True
        ?tmp:ExchangeNewStatus{prop:FontColor} = 65793
        ?tmp:ExchangeNewStatus{prop:Color} = 15066597
    Elsif ?tmp:ExchangeNewStatus{prop:Req} = True
        ?tmp:ExchangeNewStatus{prop:FontColor} = 65793
        ?tmp:ExchangeNewStatus{prop:Color} = 8454143
    Else ! If ?tmp:ExchangeNewStatus{prop:Req} = True
        ?tmp:ExchangeNewStatus{prop:FontColor} = 65793
        ?tmp:ExchangeNewStatus{prop:Color} = 16777215
    End ! If ?tmp:ExchangeNewStatus{prop:Req} = True
    ?tmp:ExchangeNewStatus{prop:Trn} = 0
    ?tmp:ExchangeNewStatus{prop:FontStyle} = font:Bold
    ?Loan{prop:Color} = 15066597
    ?tmp:LoanCurrentStatus:Prompt{prop:FontColor} = -1
    ?tmp:LoanCurrentStatus:Prompt{prop:Color} = 15066597
    If ?tmp:LoanCurrentStatus{prop:ReadOnly} = True
        ?tmp:LoanCurrentStatus{prop:FontColor} = 65793
        ?tmp:LoanCurrentStatus{prop:Color} = 15066597
    Elsif ?tmp:LoanCurrentStatus{prop:Req} = True
        ?tmp:LoanCurrentStatus{prop:FontColor} = 65793
        ?tmp:LoanCurrentStatus{prop:Color} = 8454143
    Else ! If ?tmp:LoanCurrentStatus{prop:Req} = True
        ?tmp:LoanCurrentStatus{prop:FontColor} = 65793
        ?tmp:LoanCurrentStatus{prop:Color} = 16777215
    End ! If ?tmp:LoanCurrentStatus{prop:Req} = True
    ?tmp:LoanCurrentStatus{prop:Trn} = 0
    ?tmp:LoanCurrentStatus{prop:FontStyle} = font:Bold
    If ?tmp:LoanNewStatus{prop:ReadOnly} = True
        ?tmp:LoanNewStatus{prop:FontColor} = 65793
        ?tmp:LoanNewStatus{prop:Color} = 15066597
    Elsif ?tmp:LoanNewStatus{prop:Req} = True
        ?tmp:LoanNewStatus{prop:FontColor} = 65793
        ?tmp:LoanNewStatus{prop:Color} = 8454143
    Else ! If ?tmp:LoanNewStatus{prop:Req} = True
        ?tmp:LoanNewStatus{prop:FontColor} = 65793
        ?tmp:LoanNewStatus{prop:Color} = 16777215
    End ! If ?tmp:LoanNewStatus{prop:Req} = True
    ?tmp:LoanNewStatus{prop:Trn} = 0
    ?tmp:LoanNewStatus{prop:FontStyle} = font:Bold
    ?tmp:LoanCurrentStatus:Prompt:2{prop:FontColor} = -1
    ?tmp:LoanCurrentStatus:Prompt:2{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    If ?tmp:reason{prop:ReadOnly} = True
        ?tmp:reason{prop:FontColor} = 65793
        ?tmp:reason{prop:Color} = 15066597
    Elsif ?tmp:reason{prop:Req} = True
        ?tmp:reason{prop:FontColor} = 65793
        ?tmp:reason{prop:Color} = 8454143
    Else ! If ?tmp:reason{prop:Req} = True
        ?tmp:reason{prop:FontColor} = 65793
        ?tmp:reason{prop:Color} = 16777215
    End ! If ?tmp:reason{prop:Req} = True
    ?tmp:reason{prop:Trn} = 0
    ?tmp:reason{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
UnHideReason        Routine
    If tmp:JobStatus Or tmp:ExchangeStatus Or tmp:LoanStatus
        ?Sheet3{prop:Hide} = 0
        ?Panel2{prop:Hide} = 1
    Else !If tmp:JobStatus Or tmp:ExchangeStatus Or tmp:LoanStatus
        ?Sheet3{prop:Hide} = 1
        ?Panel2{prop:Hide} = 0
    End !If tmp:JobStatus Or tmp:ExchangeStatus Or tmp:LoanStatus


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Change_Status',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Change_Status',1)
    SolaceViewVars('despatch_description_temp',despatch_description_temp,'Change_Status',1)
    SolaceViewVars('Courier_Temp',Courier_Temp,'Change_Status',1)
    SolaceViewVars('Despatch_Type_Temp',Despatch_Type_Temp,'Change_Status',1)
    SolaceViewVars('Model_Number_Temp',Model_Number_Temp,'Change_Status',1)
    SolaceViewVars('Manufacturer_temp',Manufacturer_temp,'Change_Status',1)
    SolaceViewVars('ESN_Temp',ESN_Temp,'Change_Status',1)
    SolaceViewVars('current_status_temp',current_status_temp,'Change_Status',1)
    SolaceViewVars('new_status_temp',new_status_temp,'Change_Status',1)
    SolaceViewVars('tmp:JobStatus',tmp:JobStatus,'Change_Status',1)
    SolaceViewVars('tmp:ExchangeStatus',tmp:ExchangeStatus,'Change_Status',1)
    SolaceViewVars('tmp:LoanStatus',tmp:LoanStatus,'Change_Status',1)
    SolaceViewVars('tmp:ExchangeCurrentStatus',tmp:ExchangeCurrentStatus,'Change_Status',1)
    SolaceViewVars('tmp:ExchangeNewStatus',tmp:ExchangeNewStatus,'Change_Status',1)
    SolaceViewVars('tmp:LoanCurrentStatus',tmp:LoanCurrentStatus,'Change_Status',1)
    SolaceViewVars('tmp:LoanNewStatus',tmp:LoanNewStatus,'Change_Status',1)
    SolaceViewVars('tmp:reason',tmp:reason,'Change_Status',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobStatus;  SolaceCtrlName = '?tmp:JobStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeStatus;  SolaceCtrlName = '?tmp:ExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanStatus;  SolaceCtrlName = '?tmp:LoanStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job;  SolaceCtrlName = '?Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?current_status_temp:Prompt;  SolaceCtrlName = '?current_status_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?current_status_temp;  SolaceCtrlName = '?current_status_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?new_status_temp;  SolaceCtrlName = '?new_status_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupJobStatus;  SolaceCtrlName = '?LookupJobStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Exchange;  SolaceCtrlName = '?Exchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeCurrentStatus:Prompt;  SolaceCtrlName = '?tmp:ExchangeCurrentStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeCurrentStatus;  SolaceCtrlName = '?tmp:ExchangeCurrentStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeCurrentStatus:Prompt:2;  SolaceCtrlName = '?tmp:ExchangeCurrentStatus:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ExchangeNewStatus;  SolaceCtrlName = '?tmp:ExchangeNewStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupExchangeStatus;  SolaceCtrlName = '?LookupExchangeStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Loan;  SolaceCtrlName = '?Loan';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanCurrentStatus:Prompt;  SolaceCtrlName = '?tmp:LoanCurrentStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanCurrentStatus;  SolaceCtrlName = '?tmp:LoanCurrentStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanNewStatus;  SolaceCtrlName = '?tmp:LoanNewStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupLoanStatus;  SolaceCtrlName = '?LookupLoanStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LoanCurrentStatus:Prompt:2;  SolaceCtrlName = '?tmp:LoanCurrentStatus:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:reason;  SolaceCtrlName = '?tmp:reason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Change_Status')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Change_Status')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?tmp:JobStatus
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:ACCAREAS.Open
  Relate:AUDIT.Open
  Relate:DEFAULTS.Open
  Relate:STARECIP.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:USELEVEL.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:SUBEMAIL.UseFile
  Access:TRAEMAIL.UseFile
  Access:STATUS.UseFile
  SELF.FilesOpened = True
  access:jobs.clearkey(job:ref_number_key)
  job:ref_number = f_ref_number
  If access:jobs.fetch(job:ref_number_key)
      Thiswindow.kill
  End!If access:jobs.fetch(job:ref_number_key)
  current_status_temp = job:current_status
  tmp:ExchangeCurrentStatus = job:exchange_status
  tmp:LoanCurrentStatus = job:loan_status
  
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?new_status_temp{Prop:Tip} AND ~?LookupJobStatus{Prop:Tip}
     ?LookupJobStatus{Prop:Tip} = 'Select ' & ?new_status_temp{Prop:Tip}
  END
  IF ?new_status_temp{Prop:Msg} AND ~?LookupJobStatus{Prop:Msg}
     ?LookupJobStatus{Prop:Msg} = 'Select ' & ?new_status_temp{Prop:Msg}
  END
  IF ?tmp:ExchangeNewStatus{Prop:Tip} AND ~?LookupExchangeStatus{Prop:Tip}
     ?LookupExchangeStatus{Prop:Tip} = 'Select ' & ?tmp:ExchangeNewStatus{Prop:Tip}
  END
  IF ?tmp:ExchangeNewStatus{Prop:Msg} AND ~?LookupExchangeStatus{Prop:Msg}
     ?LookupExchangeStatus{Prop:Msg} = 'Select ' & ?tmp:ExchangeNewStatus{Prop:Msg}
  END
  IF ?tmp:LoanNewStatus{Prop:Tip} AND ~?LookupLoanStatus{Prop:Tip}
     ?LookupLoanStatus{Prop:Tip} = 'Select ' & ?tmp:LoanNewStatus{Prop:Tip}
  END
  IF ?tmp:LoanNewStatus{Prop:Msg} AND ~?LookupLoanStatus{Prop:Msg}
     ?LookupLoanStatus{Prop:Msg} = 'Select ' & ?tmp:LoanNewStatus{Prop:Msg}
  END
  IF ?tmp:JobStatus{Prop:Checked} = True
    tmp:ExchangeStatus = 0
    tmp:LoanStatus = 0
    UNHIDE(?Job)
    HIDE(?Exchange)
    HIDE(?Loan)
  END
  IF ?tmp:JobStatus{Prop:Checked} = False
    HIDE(?Job)
  END
  IF ?tmp:ExchangeStatus{Prop:Checked} = True
    tmp:JobStatus = 0
    tmp:LoanStatus = 0
    UNHIDE(?Exchange)
    HIDE(?Job)
    HIDE(?Loan)
  END
  IF ?tmp:ExchangeStatus{Prop:Checked} = False
    HIDE(?Exchange)
  END
  IF ?tmp:LoanStatus{Prop:Checked} = True
    tmp:ExchangeStatus = 0
    tmp:JobStatus = 0
    HIDE(?Job)
    HIDE(?Exchange)
    UNHIDE(?Loan)
  END
  IF ?tmp:LoanStatus{Prop:Checked} = False
    HIDE(?Loan)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS.Close
    Relate:AUDIT.Close
    Relate:DEFAULTS.Close
    Relate:STARECIP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Change_Status',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickJobStatus
      PickExchangeStatus
      PickLoanStatus
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:JobStatus
      IF ?tmp:JobStatus{Prop:Checked} = True
        tmp:ExchangeStatus = 0
        tmp:LoanStatus = 0
        UNHIDE(?Job)
        HIDE(?Exchange)
        HIDE(?Loan)
      END
      IF ?tmp:JobStatus{Prop:Checked} = False
        HIDE(?Job)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobStatus, Accepted)
      Do UnhideReason
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:JobStatus, Accepted)
    OF ?tmp:ExchangeStatus
      IF ?tmp:ExchangeStatus{Prop:Checked} = True
        tmp:JobStatus = 0
        tmp:LoanStatus = 0
        UNHIDE(?Exchange)
        HIDE(?Job)
        HIDE(?Loan)
      END
      IF ?tmp:ExchangeStatus{Prop:Checked} = False
        HIDE(?Exchange)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeStatus, Accepted)
      Do UnhideReason
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeStatus, Accepted)
    OF ?tmp:LoanStatus
      IF ?tmp:LoanStatus{Prop:Checked} = True
        tmp:ExchangeStatus = 0
        tmp:JobStatus = 0
        HIDE(?Job)
        HIDE(?Exchange)
        UNHIDE(?Loan)
      END
      IF ?tmp:LoanStatus{Prop:Checked} = False
        HIDE(?Loan)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LoanStatus, Accepted)
      Do UnhideReason
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LoanStatus, Accepted)
    OF ?new_status_temp
      IF new_status_temp OR ?new_status_temp{Prop:Req}
        sts:Status = new_status_temp
        sts:Job = 'YES'
        !Save Lookup Field Incase Of error
        look:new_status_temp        = new_status_temp
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            new_status_temp = sts:Status
          ELSE
            CLEAR(sts:Job)
            !Restore Lookup On Error
            new_status_temp = look:new_status_temp
            SELECT(?new_status_temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupJobStatus
      ThisWindow.Update
      sts:Status = new_status_temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          new_status_temp = sts:Status
          Select(?+1)
      ELSE
          Select(?new_status_temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?new_status_temp)
    OF ?tmp:ExchangeNewStatus
      IF tmp:ExchangeNewStatus OR ?tmp:ExchangeNewStatus{Prop:Req}
        sts:Status = tmp:ExchangeNewStatus
        sts:Exchange = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:ExchangeNewStatus        = tmp:ExchangeNewStatus
        IF Access:STATUS.TryFetch(sts:ExchangeKey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            tmp:ExchangeNewStatus = sts:Status
          ELSE
            CLEAR(sts:Exchange)
            !Restore Lookup On Error
            tmp:ExchangeNewStatus = look:tmp:ExchangeNewStatus
            SELECT(?tmp:ExchangeNewStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupExchangeStatus
      ThisWindow.Update
      sts:Status = tmp:ExchangeNewStatus
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          tmp:ExchangeNewStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:ExchangeNewStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ExchangeNewStatus)
    OF ?tmp:LoanNewStatus
      IF tmp:LoanNewStatus OR ?tmp:LoanNewStatus{Prop:Req}
        sts:Status = tmp:LoanNewStatus
        sts:Loan = 'YES'
        !Save Lookup Field Incase Of error
        look:tmp:LoanNewStatus        = tmp:LoanNewStatus
        IF Access:STATUS.TryFetch(sts:LoanKey)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            tmp:LoanNewStatus = sts:Status
          ELSE
            CLEAR(sts:Loan)
            !Restore Lookup On Error
            tmp:LoanNewStatus = look:tmp:LoanNewStatus
            SELECT(?tmp:LoanNewStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupLoanStatus
      ThisWindow.Update
      sts:Status = tmp:LoanNewStatus
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          tmp:LoanNewStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?tmp:LoanNewStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:LoanNewStatus)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    If (tmp:JobStatus And new_status_temp = '') Or |
        (tmp:ExchangeStatus And tmp:ExchangeNewStatus = '') Or |
        (tmp:LoanStatus And tmp:LoanNewStatus = '')
        Case MessageEx('You must select a status.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    Else !tmp:LoanStatus And tmp:LoanNewStatus = ''

        If tmp:reason = ''
            Case MessageEx('You must insert a reason for the status change.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!If tmp:reason = ''
             If tmp:JobStatus = 1
                cont# = 1
                If job:Date_Completed <> ''
                    If SecurityCheck('CHANGE COMPLETED STATUS')
                        Case MessageEx('This job has been completed.<13,10><13,10>You do not have access to change the status.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        cont# = 0
                    Else!If SecurityCheck('CHANGE COMPLETED STATUS')
                        Case MessageEx('Warning! This job has been completed.<13,10><13,10>Are you sure you want to change the status?','ServiceBase 2000',|
                                       'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                            Of 1 ! &Yes Button
                            Of 2 ! &No Button
                                cont# = 0
                        End!Case MessageEx
                    End!If SecurityCheck('CHANGE COMPLETED STATUS')
                End!If job:Date_Completed <> ''

                If cont# = 1
                    If job:Date_Despatched <> ''
                        If SecurityCheck('CHANGE DESPATCHED STATUS')
                            Case MessageEx('This job has been despatched.<13,10><13,10>You do not have access to change the status.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            cont# = 0
                        Else!If SecurityCheck('CHANGE DESPATCHED STATUS')
                            Case MessageEx('Warning! This job has been despatched.<13,10><13,10>Are you sure you want to change the status?','ServiceBase 2000',|
                                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                Of 1 ! &Yes Button
                                Of 2 ! &No Button
                                    cont# = 0
                            End!Case MessageEx
                        End!If SecurityCheck('CHANGE DESPATCHED STATUS')

                    End!If job:Date_Despatched <> ''
                End!If cont# = 1
                If cont# = 1
                    error# = 0
                    access:status.clearkey(sts:status_key)
                    sts:status = new_status_temp
                    if access:status.fetch(sts:status_key)
?                       Assert(0,'<13,10>Cannot find the status.<13,10>')
                    Else!if access:status.fetch(sts:status_key)
                        GetStatus(sts:ref_number,1,'JOB')

                        If sts:ref_number    = 799
                            If SecurityCheck('JOBS - CANCELLING')
                                Case MessageEx('You do not have a sufficient access level to cancel a job.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                new_status_temp = current_status_temp
                                Display(?new_status_temp)
                                error# = 1
                            Else!if x" = false
                                Case MessageEx('Are you sure you want to cancel the selected job?','ServiceBase 2000',|
                                               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                    Of 1 ! &Yes Button
                                        job:cancelled = 'YES'
                                        access:jobs.update()
                                    Of 2 ! &No Button
                                        error# = 1
                                        new_status_temp = current_status_temp
                                        Display(?new_status_temp)
                                End!Case MessageEx
                            End!if x" = false
                        Else!If sts:ref_number    = 799
                            access:jobs.update()
                        End!If sts:ref_number    = 799
                        If error# = 0
            Compile('***',Debug=1)
                message('Current Status: ' & current_status_temp & |
                         'New Status: ' & new_status_temp,'Debug Message', icon:exclamation)
            ***
                            If current_status_temp <> new_status_temp
                                get(audit,0)
                                if access:audit.primerecord() = level:benign
                                    aud:notes         = 'PREVIOUS STATUS: ' & clip(current_status_temp) & '<13,10>NEW STATUS: ' & clip(new_status_temp) &|
                                                        '<13,10,13,10>REASON:<13,10>' & Clip(tmp:reason)
                                    aud:ref_number    = JOB:REF_NUMBER
                                    aud:date          = tODAY()
                                    aud:time          = CLOCK()
                                    access:users.clearkey(use:password_key)
                                    use:password = glo:password
                                    access:users.fetch(use:password_key)
                                    aud:user = use:user_code
                                    aud:action        = 'MANUAL STATUS CHANGE TO: ' & clip(new_status_temp)
                                    if access:audit.insert()
                                        access:audit.cancelautoinc()
                                    end
                                End!if access:audit.primerecord() = level:benign
                            End!If current_status_temp <> new_status_temp

                            Post(Event:CloseWindow)
                        End!If error# = 0
                    end!if access:status.fetch(sts:status_key)
                End!If cont# = 1

            End!If tmp:JobStatus = 1

            If tmp:exchangestatus = 1
                access:status.clearkey(sts:status_key)
                sts:status = tmp:ExchangeNewStatus
                if access:status.fetch(sts:status_key) = Level:Benign
                    GetStatus(sts:Ref_Number,1,'EXC')
                    access:jobs.update()
                    If tmp:exchangecurrentstatus <> tmp:exchangenewstatus

                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:notes         = 'PREVIOUS STATUS: ' & clip(tmp:ExchangeCurrentStatus) & '<13,10>NEW STATUS: ' & clip(tmp:ExchangeNewStatus) &|
                                                   '<13,10,13,10>REASON:<13,10>' & Clip(tmp:reason)
                            aud:ref_number    = JOB:REF_NUMBER
                            aud:date          = tODAY()
                            aud:time          = CLOCK()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            aud:action        = 'MANUAL EXCHANGE STATUS CHANGE TO: ' & clip(tmp:exchangenewstatus)
                            aud:type          = 'EXC'
                            if access:audit.insert()
                                access:audit.cancelautoinc()
                            end
                        End!if access:audit.primerecord() = level:benign
                    End!If current_status_temp <> new_status_temp

                    Post(Event:CloseWindow)
                end!if access:status.fetch(sts:status_key)
            End!If tmp:exchange_status
            If tmp:loanstatus = 1
                access:status.clearkey(sts:status_key)
                sts:status = tmp:LoanNewStatus
                if access:status.fetch(sts:status_key) = Level:Benign
                    GetStatus(sts:Ref_number,1,'LOA')
                    access:jobs.update()
                    If tmp:loancurrentstatus <> tmp:loannewstatus

                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:notes         = 'PREVIOUS STATUS: ' & clip(tmp:LoanCurrentStatus) & '<13,10>NEW STATUS: ' & clip(tmp:LoanNewStatus) &|
                                                    '<13,10,13,10>REASON:<13,10>' & Clip(tmp:reason)
                            aud:ref_number    = JOB:REF_NUMBER
                            aud:date          = tODAY()
                            aud:time          = CLOCK()
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            aud:action        = 'MANUAL LOAN STATUS CHANGE TO: ' & clip(tmp:loannewstatus)
                            aud:type          = 'LOA'
                            if access:audit.insert()
                                access:audit.cancelautoinc()
                            end
                        End!if access:audit.primerecord() = level:benign
                    End!If current_status_temp <> new_status_temp

                    Post(Event:CloseWindow)
                end!if access:status.fetch(sts:status_key)
            End!If tmp:loanstatus = 1
        End!If tmp:reason = ''
    End !tmp:LoanStatus And tmp:LoanNewStatus = ''

      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Change_Status')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?new_status_temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?new_status_temp, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?new_status_temp, AlertKey)
    END
  OF ?tmp:ExchangeNewStatus
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeNewStatus, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ExchangeNewStatus, AlertKey)
    END
  OF ?tmp:LoanNewStatus
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LoanNewStatus, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:LoanNewStatus, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

