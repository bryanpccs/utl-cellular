

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01029.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTRDPARTY PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?trd:Courier
cou:Courier            LIKE(cou:Courier)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW11::View:Browse   VIEW(TRDMODEL)
                       PROJECT(trm:Model_Number)
                       PROJECT(trm:Company_Name)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
trm:Model_Number       LIKE(trm:Model_Number)         !List box control field - type derived from field
trm:Company_Name       LIKE(trm:Company_Name)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB25::View:FileDropCombo VIEW(COURIER)
                       PROJECT(cou:Courier)
                     END
History::trd:Record  LIKE(trd:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Update the TRDPARTY File'),AT(,,394,243),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateTRDPARTY'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,224,208),USE(?Sheet2),SPREAD
                         TAB('General'),USE(?Tab2)
                           PROMPT('Company Name'),AT(8,20),USE(?TRD:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(trd:Company_Name),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Account Number'),AT(8,36),USE(?TRD:Account_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(trd:Account_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Contact Name'),AT(8,52),USE(?TRD:Contact_Name:Prompt),TRN
                           ENTRY(@s60),AT(84,52,124,10),USE(trd:Contact_Name),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Postcode'),AT(8,68),USE(?TRD:Postcode:Prompt),TRN
                           ENTRY(@s15),AT(84,68,64,10),USE(trd:Postcode),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Clear'),AT(152,68,56,10),USE(?Clear_Address),SKIP,LEFT,ICON(ICON:Cut)
                           PROMPT('Address'),AT(8,80),USE(?TRD:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,80,124,10),USE(trd:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,92,124,10),USE(trd:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,104,124,10),USE(trd:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(8,120),USE(?TRD:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,120,64,10),USE(trd:Telephone_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fax Number'),AT(8,136),USE(?TRD:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,136,64,10),USE(trd:Fax_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Repairs Completed Within'),AT(8,152,74,18),USE(?TRD:Turnaround_Time:Prompt),TRN
                           ENTRY(@n4),AT(84,152,64,10),USE(trd:Turnaround_Time),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Days'),AT(152,152),USE(?Prompt9)
                           PROMPT('Batch Limit'),AT(8,172),USE(?TRD:BatchLimit:Prompt)
                           ENTRY(@s8),AT(84,172,64,10),USE(trd:BatchLimit),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Courier'),AT(8,188),USE(?Prompt11)
                           COMBO(@s30),AT(84,188,124,10),USE(trd:Courier),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                         END
                       END
                       PANEL,AT(4,216,388,24),USE(?Panel1:2),FILL(COLOR:Silver)
                       SHEET,AT(232,4,160,208),USE(?Sheet1),SPREAD
                         TAB('Model Numbers'),USE(?Tab1)
                           ENTRY(@s30),AT(236,20,124,10),USE(trm:Model_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(236,36,152,148),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)~Model Number~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(236,192,56,16),USE(?Button7),LEFT,ICON('insert.gif')
                           BUTTON('&Delete'),AT(332,192,56,16),USE(?Delete:2),LEFT,ICON('delete.gif')
                         END
                       END
                       BUTTON('&OK'),AT(276,220,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(332,220,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW11                CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW11::Sort0:Locator IncrementalLocatorClass          !Default Locator
FDCB25               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
TakeAccepted           PROCEDURE(),DERIVED
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_trm_id   ushort,auto
save_trr_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?TRD:Company_Name:Prompt{prop:FontColor} = -1
    ?TRD:Company_Name:Prompt{prop:Color} = 15066597
    If ?trd:Company_Name{prop:ReadOnly} = True
        ?trd:Company_Name{prop:FontColor} = 65793
        ?trd:Company_Name{prop:Color} = 15066597
    Elsif ?trd:Company_Name{prop:Req} = True
        ?trd:Company_Name{prop:FontColor} = 65793
        ?trd:Company_Name{prop:Color} = 8454143
    Else ! If ?trd:Company_Name{prop:Req} = True
        ?trd:Company_Name{prop:FontColor} = 65793
        ?trd:Company_Name{prop:Color} = 16777215
    End ! If ?trd:Company_Name{prop:Req} = True
    ?trd:Company_Name{prop:Trn} = 0
    ?trd:Company_Name{prop:FontStyle} = font:Bold
    ?TRD:Account_Number:Prompt{prop:FontColor} = -1
    ?TRD:Account_Number:Prompt{prop:Color} = 15066597
    If ?trd:Account_Number{prop:ReadOnly} = True
        ?trd:Account_Number{prop:FontColor} = 65793
        ?trd:Account_Number{prop:Color} = 15066597
    Elsif ?trd:Account_Number{prop:Req} = True
        ?trd:Account_Number{prop:FontColor} = 65793
        ?trd:Account_Number{prop:Color} = 8454143
    Else ! If ?trd:Account_Number{prop:Req} = True
        ?trd:Account_Number{prop:FontColor} = 65793
        ?trd:Account_Number{prop:Color} = 16777215
    End ! If ?trd:Account_Number{prop:Req} = True
    ?trd:Account_Number{prop:Trn} = 0
    ?trd:Account_Number{prop:FontStyle} = font:Bold
    ?TRD:Contact_Name:Prompt{prop:FontColor} = -1
    ?TRD:Contact_Name:Prompt{prop:Color} = 15066597
    If ?trd:Contact_Name{prop:ReadOnly} = True
        ?trd:Contact_Name{prop:FontColor} = 65793
        ?trd:Contact_Name{prop:Color} = 15066597
    Elsif ?trd:Contact_Name{prop:Req} = True
        ?trd:Contact_Name{prop:FontColor} = 65793
        ?trd:Contact_Name{prop:Color} = 8454143
    Else ! If ?trd:Contact_Name{prop:Req} = True
        ?trd:Contact_Name{prop:FontColor} = 65793
        ?trd:Contact_Name{prop:Color} = 16777215
    End ! If ?trd:Contact_Name{prop:Req} = True
    ?trd:Contact_Name{prop:Trn} = 0
    ?trd:Contact_Name{prop:FontStyle} = font:Bold
    ?TRD:Postcode:Prompt{prop:FontColor} = -1
    ?TRD:Postcode:Prompt{prop:Color} = 15066597
    If ?trd:Postcode{prop:ReadOnly} = True
        ?trd:Postcode{prop:FontColor} = 65793
        ?trd:Postcode{prop:Color} = 15066597
    Elsif ?trd:Postcode{prop:Req} = True
        ?trd:Postcode{prop:FontColor} = 65793
        ?trd:Postcode{prop:Color} = 8454143
    Else ! If ?trd:Postcode{prop:Req} = True
        ?trd:Postcode{prop:FontColor} = 65793
        ?trd:Postcode{prop:Color} = 16777215
    End ! If ?trd:Postcode{prop:Req} = True
    ?trd:Postcode{prop:Trn} = 0
    ?trd:Postcode{prop:FontStyle} = font:Bold
    ?TRD:Address_Line1:Prompt{prop:FontColor} = -1
    ?TRD:Address_Line1:Prompt{prop:Color} = 15066597
    If ?trd:Address_Line1{prop:ReadOnly} = True
        ?trd:Address_Line1{prop:FontColor} = 65793
        ?trd:Address_Line1{prop:Color} = 15066597
    Elsif ?trd:Address_Line1{prop:Req} = True
        ?trd:Address_Line1{prop:FontColor} = 65793
        ?trd:Address_Line1{prop:Color} = 8454143
    Else ! If ?trd:Address_Line1{prop:Req} = True
        ?trd:Address_Line1{prop:FontColor} = 65793
        ?trd:Address_Line1{prop:Color} = 16777215
    End ! If ?trd:Address_Line1{prop:Req} = True
    ?trd:Address_Line1{prop:Trn} = 0
    ?trd:Address_Line1{prop:FontStyle} = font:Bold
    If ?trd:Address_Line2{prop:ReadOnly} = True
        ?trd:Address_Line2{prop:FontColor} = 65793
        ?trd:Address_Line2{prop:Color} = 15066597
    Elsif ?trd:Address_Line2{prop:Req} = True
        ?trd:Address_Line2{prop:FontColor} = 65793
        ?trd:Address_Line2{prop:Color} = 8454143
    Else ! If ?trd:Address_Line2{prop:Req} = True
        ?trd:Address_Line2{prop:FontColor} = 65793
        ?trd:Address_Line2{prop:Color} = 16777215
    End ! If ?trd:Address_Line2{prop:Req} = True
    ?trd:Address_Line2{prop:Trn} = 0
    ?trd:Address_Line2{prop:FontStyle} = font:Bold
    If ?trd:Address_Line3{prop:ReadOnly} = True
        ?trd:Address_Line3{prop:FontColor} = 65793
        ?trd:Address_Line3{prop:Color} = 15066597
    Elsif ?trd:Address_Line3{prop:Req} = True
        ?trd:Address_Line3{prop:FontColor} = 65793
        ?trd:Address_Line3{prop:Color} = 8454143
    Else ! If ?trd:Address_Line3{prop:Req} = True
        ?trd:Address_Line3{prop:FontColor} = 65793
        ?trd:Address_Line3{prop:Color} = 16777215
    End ! If ?trd:Address_Line3{prop:Req} = True
    ?trd:Address_Line3{prop:Trn} = 0
    ?trd:Address_Line3{prop:FontStyle} = font:Bold
    ?TRD:Telephone_Number:Prompt{prop:FontColor} = -1
    ?TRD:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?trd:Telephone_Number{prop:ReadOnly} = True
        ?trd:Telephone_Number{prop:FontColor} = 65793
        ?trd:Telephone_Number{prop:Color} = 15066597
    Elsif ?trd:Telephone_Number{prop:Req} = True
        ?trd:Telephone_Number{prop:FontColor} = 65793
        ?trd:Telephone_Number{prop:Color} = 8454143
    Else ! If ?trd:Telephone_Number{prop:Req} = True
        ?trd:Telephone_Number{prop:FontColor} = 65793
        ?trd:Telephone_Number{prop:Color} = 16777215
    End ! If ?trd:Telephone_Number{prop:Req} = True
    ?trd:Telephone_Number{prop:Trn} = 0
    ?trd:Telephone_Number{prop:FontStyle} = font:Bold
    ?TRD:Fax_Number:Prompt{prop:FontColor} = -1
    ?TRD:Fax_Number:Prompt{prop:Color} = 15066597
    If ?trd:Fax_Number{prop:ReadOnly} = True
        ?trd:Fax_Number{prop:FontColor} = 65793
        ?trd:Fax_Number{prop:Color} = 15066597
    Elsif ?trd:Fax_Number{prop:Req} = True
        ?trd:Fax_Number{prop:FontColor} = 65793
        ?trd:Fax_Number{prop:Color} = 8454143
    Else ! If ?trd:Fax_Number{prop:Req} = True
        ?trd:Fax_Number{prop:FontColor} = 65793
        ?trd:Fax_Number{prop:Color} = 16777215
    End ! If ?trd:Fax_Number{prop:Req} = True
    ?trd:Fax_Number{prop:Trn} = 0
    ?trd:Fax_Number{prop:FontStyle} = font:Bold
    ?TRD:Turnaround_Time:Prompt{prop:FontColor} = -1
    ?TRD:Turnaround_Time:Prompt{prop:Color} = 15066597
    If ?trd:Turnaround_Time{prop:ReadOnly} = True
        ?trd:Turnaround_Time{prop:FontColor} = 65793
        ?trd:Turnaround_Time{prop:Color} = 15066597
    Elsif ?trd:Turnaround_Time{prop:Req} = True
        ?trd:Turnaround_Time{prop:FontColor} = 65793
        ?trd:Turnaround_Time{prop:Color} = 8454143
    Else ! If ?trd:Turnaround_Time{prop:Req} = True
        ?trd:Turnaround_Time{prop:FontColor} = 65793
        ?trd:Turnaround_Time{prop:Color} = 16777215
    End ! If ?trd:Turnaround_Time{prop:Req} = True
    ?trd:Turnaround_Time{prop:Trn} = 0
    ?trd:Turnaround_Time{prop:FontStyle} = font:Bold
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?TRD:BatchLimit:Prompt{prop:FontColor} = -1
    ?TRD:BatchLimit:Prompt{prop:Color} = 15066597
    If ?trd:BatchLimit{prop:ReadOnly} = True
        ?trd:BatchLimit{prop:FontColor} = 65793
        ?trd:BatchLimit{prop:Color} = 15066597
    Elsif ?trd:BatchLimit{prop:Req} = True
        ?trd:BatchLimit{prop:FontColor} = 65793
        ?trd:BatchLimit{prop:Color} = 8454143
    Else ! If ?trd:BatchLimit{prop:Req} = True
        ?trd:BatchLimit{prop:FontColor} = 65793
        ?trd:BatchLimit{prop:Color} = 16777215
    End ! If ?trd:BatchLimit{prop:Req} = True
    ?trd:BatchLimit{prop:Trn} = 0
    ?trd:BatchLimit{prop:FontStyle} = font:Bold
    ?Prompt11{prop:FontColor} = -1
    ?Prompt11{prop:Color} = 15066597
    If ?trd:Courier{prop:ReadOnly} = True
        ?trd:Courier{prop:FontColor} = 65793
        ?trd:Courier{prop:Color} = 15066597
    Elsif ?trd:Courier{prop:Req} = True
        ?trd:Courier{prop:FontColor} = 65793
        ?trd:Courier{prop:Color} = 8454143
    Else ! If ?trd:Courier{prop:Req} = True
        ?trd:Courier{prop:FontColor} = 65793
        ?trd:Courier{prop:Color} = 16777215
    End ! If ?trd:Courier{prop:Req} = True
    ?trd:Courier{prop:Trn} = 0
    ?trd:Courier{prop:FontStyle} = font:Bold
    ?Panel1:2{prop:Fill} = 15066597

    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    If ?trm:Model_Number{prop:ReadOnly} = True
        ?trm:Model_Number{prop:FontColor} = 65793
        ?trm:Model_Number{prop:Color} = 15066597
    Elsif ?trm:Model_Number{prop:Req} = True
        ?trm:Model_Number{prop:FontColor} = 65793
        ?trm:Model_Number{prop:Color} = 8454143
    Else ! If ?trm:Model_Number{prop:Req} = True
        ?trm:Model_Number{prop:FontColor} = 65793
        ?trm:Model_Number{prop:Color} = 16777215
    End ! If ?trm:Model_Number{prop:Req} = True
    ?trm:Model_Number{prop:Trn} = 0
    ?trm:Model_Number{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTRDPARTY',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTRDPARTY',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateTRDPARTY',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateTRDPARTY',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTRDPARTY',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateTRDPARTY',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Company_Name:Prompt;  SolaceCtrlName = '?TRD:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Company_Name;  SolaceCtrlName = '?trd:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Account_Number:Prompt;  SolaceCtrlName = '?TRD:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Account_Number;  SolaceCtrlName = '?trd:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Contact_Name:Prompt;  SolaceCtrlName = '?TRD:Contact_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Contact_Name;  SolaceCtrlName = '?trd:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Postcode:Prompt;  SolaceCtrlName = '?TRD:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Postcode;  SolaceCtrlName = '?trd:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Address;  SolaceCtrlName = '?Clear_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Address_Line1:Prompt;  SolaceCtrlName = '?TRD:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Address_Line1;  SolaceCtrlName = '?trd:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Address_Line2;  SolaceCtrlName = '?trd:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Address_Line3;  SolaceCtrlName = '?trd:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Telephone_Number:Prompt;  SolaceCtrlName = '?TRD:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Telephone_Number;  SolaceCtrlName = '?trd:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Fax_Number:Prompt;  SolaceCtrlName = '?TRD:Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Fax_Number;  SolaceCtrlName = '?trd:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:Turnaround_Time:Prompt;  SolaceCtrlName = '?TRD:Turnaround_Time:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Turnaround_Time;  SolaceCtrlName = '?trd:Turnaround_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRD:BatchLimit:Prompt;  SolaceCtrlName = '?TRD:BatchLimit:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:BatchLimit;  SolaceCtrlName = '?trd:BatchLimit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt11;  SolaceCtrlName = '?Prompt11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trd:Courier;  SolaceCtrlName = '?trd:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1:2;  SolaceCtrlName = '?Panel1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trm:Model_Number;  SolaceCtrlName = '?trm:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button7;  SolaceCtrlName = '?Button7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:2;  SolaceCtrlName = '?Delete:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A 3rd Party Repairer'
  OF ChangeRecord
    ActionMessage = 'Changing A 3rd Party Repairer'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTRDPARTY')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTRDPARTY')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRD:Company_Name:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(trd:Record,History::trd:Record)
  SELF.AddHistoryField(?trd:Company_Name,1)
  SELF.AddHistoryField(?trd:Account_Number,2)
  SELF.AddHistoryField(?trd:Contact_Name,3)
  SELF.AddHistoryField(?trd:Postcode,5)
  SELF.AddHistoryField(?trd:Address_Line1,6)
  SELF.AddHistoryField(?trd:Address_Line2,7)
  SELF.AddHistoryField(?trd:Address_Line3,8)
  SELF.AddHistoryField(?trd:Telephone_Number,9)
  SELF.AddHistoryField(?trd:Fax_Number,10)
  SELF.AddHistoryField(?trd:Turnaround_Time,11)
  SELF.AddHistoryField(?trd:BatchLimit,17)
  SELF.AddHistoryField(?trd:Courier,13)
  SELF.AddUpdateFile(Access:TRDPARTY)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRDPARTY
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW11.Init(?List,Queue:Browse.ViewPosition,BRW11::View:Browse,Queue:Browse,Relate:TRDMODEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW11.Q &= Queue:Browse
  BRW11.RetainRow = 0
  BRW11.AddSortOrder(,trm:Model_Number_Key)
  BRW11.AddRange(trm:Company_Name,Relate:TRDMODEL,Relate:TRDPARTY)
  BRW11.AddLocator(BRW11::Sort0:Locator)
  BRW11::Sort0:Locator.Init(?trm:Model_Number,trm:Model_Number,1,BRW11)
  BRW11.AddField(trm:Model_Number,BRW11.Q.trm:Model_Number)
  BRW11.AddField(trm:Company_Name,BRW11.Q.trm:Company_Name)
  BRW11.AskProcedure = 1
  FDCB25.Init(trd:Courier,?trd:Courier,Queue:FileDropCombo.ViewPosition,FDCB25::View:FileDropCombo,Queue:FileDropCombo,Relate:COURIER,ThisWindow,GlobalErrors,0,0,0)
  FDCB25.EntryCompletion=False
  FDCB25.Q &= Queue:FileDropCombo
  FDCB25.AddSortOrder(cou:Courier_Key)
  FDCB25.AddField(cou:Courier,FDCB25.Q.cou:Courier)
  FDCB25.AddUpdateField(cou:Courier,trd:Courier)
  ThisWindow.AddItem(FDCB25.WindowComponent)
  FDCB25.DefaultFill = 0
  BRW11.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTRDPARTY',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = True
  
    If trd:company_name = ''
          Case MessageEx('You have not entered a Company Name.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          do_update# = False
    End !If trd:company_name = ''
  
    If do_update# = True
        If request  = Insertrecord
            If number = 2
                brw11.resetsort(1)
                If trm:model_number = ''
                      Case MessageEx('You must select a Model Number before you can insert Repair Types.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                    do_update# = False
                End !If trm:model_number = ''
            End !If number = 2
        End !If request  = Insertrecord
    End !If do_update# = True
  
    If do_update# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Third_Party_Models
    ReturnValue = GlobalResponse
  END
    End !If do_update# = True
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Button7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
      If trd:company_name = ''
          Case MessageEx('You have not entered a Company Name.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else
          Pick_Model_Number
          Setcursor(cursor:wait)
          If Records(glo:Q_ModelNumber)
              Loop x# = 1 To Records(glo:Q_ModelNumber)
                  Get(glo:Q_ModelNumber,x#)
      
                  trm:company_name = trd:company_name
                  trm:model_number = glo:model_number_pointer
                  access:trdmodel.tryinsert()
              End
          End
          Setcursor()
      End !If trd:company_name = ''
      BRW11.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button7, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?trd:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trd:Postcode, Accepted)
      If ~quickwindow{prop:acceptall}
          Postcode_Routine (TRD:Postcode,TRD:Address_Line1,TRD:Address_Line2,TRD:Address_Line3)
          Select(?trd:address_line1,1)
          Display()
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trd:Postcode, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      Case MessageEx('Are you sure you want to clear the address?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              trd:postcode      = ''
              trd:address_line1 = ''
              trd:address_line2 = ''
              trd:address_line3 = ''
              Select(?trd:postcode)
              Display()
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?trd:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trd:Telephone_Number, Accepted)
      
       temp_string = Clip(left(trd:Telephone_Number))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       trd:Telephone_Number = temp_string
       Display(?trd:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trd:Telephone_Number, Accepted)
    OF ?trd:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trd:Fax_Number, Accepted)
      
       temp_string = Clip(left(trd:Fax_Number))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       trd:Fax_Number = temp_string
       Display(?trd:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trd:Fax_Number, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTRDPARTY')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?trm:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trm:Model_Number, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?trm:Model_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW11.ResetSort(1)
      FDCB25.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW11.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete:2
  END


BRW11.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


FDCB25.TakeAccepted PROCEDURE

  CODE
  ! Before Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(25, TakeAccepted, ())
  If quickwindow{prop:acceptall} <> 1
  PARENT.TakeAccepted
  End
  ! After Embed Point: %FileDropComboMethodCodeSection) DESC(FileDropCombo Method Executable Code Section) ARG(25, TakeAccepted, ())

