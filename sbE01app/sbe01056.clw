

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBE01056.INC'),ONCE        !Local module procedure declarations
                     END


Turnaround_Routine   PROCEDURE  (f_days,f_hours,f_end_days,f_end_hours) ! Declare Procedure
clock_temp           TIME
start_time_temp      TIME
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Turnaround_Routine')      !Add Procedure to Log
  end


    Set(Defaults)
    access:defaults.next()
    f_end_days = Today()
    f_end_hours = Clock()
    x# = 0
    count# = 0
    If f_days <> 0
        Loop
            count# += 1
            day_number# = (Today() + count#) % 7
            If def:include_saturday <> 'YES'
                If day_number# = 6
                    f_end_days += 1
                    Cycle
                End
            End
            If def:include_sunday <> 'YES'
                If day_number# = 0
                    f_end_days += 1
                    Cycle
                End
            End
            f_end_days += 1
            x# += 1
            If x# >= f_days
                Break
            End!If x# >= sts:turnaround_days
        End!Loop
    End!If f_days <> ''
    If f_hours <> 0
        start_time_temp = Clock()
        new_day# = 0
        Loop x# = 1 To f_hours
            clock_temp = start_time_temp + (x# * 360000)
            If def:start_work_hours <> '' And def:end_work_hours <> ''
                If clock_temp < def:start_work_hours Or clock_temp > def:end_work_hours
                    f_end_hours = def:start_work_hours
                    start_time_temp = def:start_work_hours
                    f_end_days += 1
                End!If clock_temp > def:start_work_hours And clock_temp < def:end_work_hours
            End!If def:start_work_hours <> '' And def:end_work_hours <> ''
            f_end_hours += 360000
!Compile('***',Debug=1)
!    message('x#: ' & x# & '|clock_temp: ' & Format(clock_temp,@t1) & |
!            '|f_end_hours: ' & Format(f_end_hours,@t1) & '|f_end_days: ' & Format(f_end_days,@d6),'Debug Message', icon:exclamation)
!***
        End!Loop x# = 1 To Abs(sts:turnaround_hours/6000)
    End!If f_hours <> ''



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Turnaround_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('clock_temp',clock_temp,'Turnaround_Routine',1)
    SolaceViewVars('start_time_temp',start_time_temp,'Turnaround_Routine',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
