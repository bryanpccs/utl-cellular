

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01093.INC'),ONCE        !Local module procedure declarations
                     END


StatusEmailDefaults PROCEDURE                         !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW4::View:Browse    VIEW(STARECIP)
                       PROJECT(str:RecipientType)
                       PROJECT(str:RecordNumber)
                       PROJECT(str:RefNumber)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
str:RecipientType      LIKE(str:RecipientType)        !List box control field - type derived from field
str:RecordNumber       LIKE(str:RecordNumber)         !Primary key field - type derived from field
str:RefNumber          LIKE(str:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Email Defaults'),AT(,,399,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,392,312),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Sender Email Address'),AT(8,8),USE(?sts:SenderEmailAddress:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,8,124,10),USE(sts:SenderEmailAddress),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Sender Email Address'),TIP('Sender Email Address')
                           PROMPT('Email Subject'),AT(8,24),USE(?sts:EmailSubject:Prompt),TRN,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s255),AT(84,24,184,10),USE(sts:EmailSubject),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Email Subject'),TIP('Email Subject')
                           LIST,AT(84,40,148,112),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('120L(2)|M~Recipient Type~@s30@'),FROM(Queue:Browse)
                           GROUP('Reference Fields To Include On Email'),AT(240,44,140,108),USE(?Group1),BOXED
                           END
                           PROMPT('Email Footer'),AT(8,256),USE(?Prompt3)
                           PROMPT('Email Body'),AT(8,176),USE(?Prompt3:2)
                           BUTTON('&Change'),AT(140,156,42,12),USE(?Change)
                           BUTTON('&Delete'),AT(192,156,42,12),USE(?Delete)
                           BUTTON('Insert'),AT(84,156,42,12),USE(?Insert)
                           TEXT,AT(84,176,304,76),USE(sts:EmailBody),VSCROLL,FONT(,,,FONT:bold),MSG('Email Body')
                         END
                       END
                       PANEL,AT(4,320,392,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(336,324,56,16),USE(?OK),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?sts:SenderEmailAddress:Prompt{prop:FontColor} = -1
    ?sts:SenderEmailAddress:Prompt{prop:Color} = 15066597
    If ?sts:SenderEmailAddress{prop:ReadOnly} = True
        ?sts:SenderEmailAddress{prop:FontColor} = 65793
        ?sts:SenderEmailAddress{prop:Color} = 15066597
    Elsif ?sts:SenderEmailAddress{prop:Req} = True
        ?sts:SenderEmailAddress{prop:FontColor} = 65793
        ?sts:SenderEmailAddress{prop:Color} = 8454143
    Else ! If ?sts:SenderEmailAddress{prop:Req} = True
        ?sts:SenderEmailAddress{prop:FontColor} = 65793
        ?sts:SenderEmailAddress{prop:Color} = 16777215
    End ! If ?sts:SenderEmailAddress{prop:Req} = True
    ?sts:SenderEmailAddress{prop:Trn} = 0
    ?sts:SenderEmailAddress{prop:FontStyle} = font:Bold
    ?sts:EmailSubject:Prompt{prop:FontColor} = -1
    ?sts:EmailSubject:Prompt{prop:Color} = 15066597
    If ?sts:EmailSubject{prop:ReadOnly} = True
        ?sts:EmailSubject{prop:FontColor} = 65793
        ?sts:EmailSubject{prop:Color} = 15066597
    Elsif ?sts:EmailSubject{prop:Req} = True
        ?sts:EmailSubject{prop:FontColor} = 65793
        ?sts:EmailSubject{prop:Color} = 8454143
    Else ! If ?sts:EmailSubject{prop:Req} = True
        ?sts:EmailSubject{prop:FontColor} = 65793
        ?sts:EmailSubject{prop:Color} = 16777215
    End ! If ?sts:EmailSubject{prop:Req} = True
    ?sts:EmailSubject{prop:Trn} = 0
    ?sts:EmailSubject{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Prompt3:2{prop:FontColor} = -1
    ?Prompt3:2{prop:Color} = 15066597
    If ?sts:EmailBody{prop:ReadOnly} = True
        ?sts:EmailBody{prop:FontColor} = 65793
        ?sts:EmailBody{prop:Color} = 15066597
    Elsif ?sts:EmailBody{prop:Req} = True
        ?sts:EmailBody{prop:FontColor} = 65793
        ?sts:EmailBody{prop:Color} = 8454143
    Else ! If ?sts:EmailBody{prop:Req} = True
        ?sts:EmailBody{prop:FontColor} = 65793
        ?sts:EmailBody{prop:Color} = 16777215
    End ! If ?sts:EmailBody{prop:Req} = True
    ?sts:EmailBody{prop:Trn} = 0
    ?sts:EmailBody{prop:FontStyle} = font:Bold
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StatusEmailDefaults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:SenderEmailAddress:Prompt;  SolaceCtrlName = '?sts:SenderEmailAddress:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:SenderEmailAddress;  SolaceCtrlName = '?sts:SenderEmailAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:EmailSubject:Prompt;  SolaceCtrlName = '?sts:EmailSubject:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:EmailSubject;  SolaceCtrlName = '?sts:EmailSubject';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3:2;  SolaceCtrlName = '?Prompt3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:EmailBody;  SolaceCtrlName = '?sts:EmailBody';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('StatusEmailDefaults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'StatusEmailDefaults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?sts:SenderEmailAddress:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?OK,RequestCancelled)
  Relate:RECIPTYP.Open
  Relate:STARECIP.Open
  Access:STATUS.UseFile
  SELF.FilesOpened = True
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:STARECIP,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.RetainRow = 0
  BRW4.AddSortOrder(,str:RecipientTypeKey)
  BRW4.AddRange(str:RefNumber,sts:Ref_Number)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,str:RecipientType,1,BRW4)
  BRW4.AddField(str:RecipientType,BRW4.Q.str:RecipientType)
  BRW4.AddField(str:RecordNumber,BRW4.Q.str:RecordNumber)
  BRW4.AddField(str:RefNumber,BRW4.Q.str:RefNumber)
  BRW4.AskProcedure = 1
  BRW4.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:RECIPTYP.Close
    Relate:STARECIP.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'StatusEmailDefaults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateStatusRecipients
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Insert
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
      SaveRequest#      = GlobalRequest
      GlobalResponse    = RequestCancelled
      GlobalRequest     = SelectRecord
      PickRecipientTypes
      If Globalresponse = RequestCompleted
          If Access:STARECIP.PrimeRecord() = Level:Benign
              str:RefNumber   = sts:Ref_Number
              str:RecipientType   = rec:RecipientType
              If Access:STARECIP.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:STARECIP.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:STARECIP.TryInsert() = Level:Benign
          End !If Access:STARECIP.PrimeRecord() = Level:Benign
      End
      GlobalRequest     = SaveRequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Insert, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'StatusEmailDefaults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

