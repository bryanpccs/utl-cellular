

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01041.INC'),ONCE        !Local module procedure declarations
                     END


Auto_Search_Window PROCEDURE                          !Generated from procedure template - Window

LocalRequest         LONG
FilesOpened          BYTE
Search_Field_Temp    STRING(30)
Search_By_Temp       BYTE
Search_Result_Temp   STRING(30)
Trade_Customer_Temp  STRING(46)
Customer_Name_Temp   STRING(70)
Days_Since_Booking_Temp LONG
total_temp           REAL
balance_temp         REAL
vat_total_warranty_temp REAL
total_warranty_temp  REAL
vat_total_estimate_temp REAL
total_estimate_temp  REAL
vat_total_temp       REAL
msn_temp             STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(JOBS_ALIAS)
                       PROJECT(job_ali:Ref_Number)
                       PROJECT(job_ali:Account_Number)
                       PROJECT(job_ali:Surname)
                       PROJECT(job_ali:Mobile_Number)
                       PROJECT(job_ali:ESN)
                       PROJECT(job_ali:MSN)
                       PROJECT(job_ali:Postcode)
                       PROJECT(job_ali:Charge_Type)
                       PROJECT(job_ali:Date_Completed)
                       PROJECT(job_ali:Model_Number)
                       PROJECT(job_ali:Manufacturer)
                       PROJECT(job_ali:Unit_Type)
                       PROJECT(job_ali:Loan_Status)
                       PROJECT(job_ali:Exchange_Status)
                       PROJECT(job_ali:Company_Name)
                       PROJECT(job_ali:Telephone_Delivery)
                       PROJECT(job_ali:Address_Line3_Delivery)
                       PROJECT(job_ali:Address_Line2_Delivery)
                       PROJECT(job_ali:Company_Name_Delivery)
                       PROJECT(job_ali:Address_Line1_Delivery)
                       PROJECT(job_ali:Postcode_Delivery)
                       PROJECT(job_ali:Telephone_Collection)
                       PROJECT(job_ali:Address_Line3_Collection)
                       PROJECT(job_ali:Address_Line2_Collection)
                       PROJECT(job_ali:Address_Line1_Collection)
                       PROJECT(job_ali:Company_Name_Collection)
                       PROJECT(job_ali:Postcode_Collection)
                       PROJECT(job_ali:Fax_Number)
                       PROJECT(job_ali:Address_Line3)
                       PROJECT(job_ali:Telephone_Number)
                       PROJECT(job_ali:Address_Line2)
                       PROJECT(job_ali:Address_Line1)
                       PROJECT(job_ali:Courier_Cost)
                       PROJECT(job_ali:Advance_Payment)
                       PROJECT(job_ali:Labour_Cost)
                       PROJECT(job_ali:Parts_Cost)
                       PROJECT(job_ali:Sub_Total)
                       PROJECT(job_ali:Courier_Cost_Estimate)
                       PROJECT(job_ali:Labour_Cost_Estimate)
                       PROJECT(job_ali:Parts_Cost_Estimate)
                       PROJECT(job_ali:Sub_Total_Estimate)
                       PROJECT(job_ali:Courier_Cost_Warranty)
                       PROJECT(job_ali:Labour_Cost_Warranty)
                       PROJECT(job_ali:Parts_Cost_Warranty)
                       PROJECT(job_ali:Sub_Total_Warranty)
                       JOIN(jbn_ali:RefNumberKey,job_ali:Ref_Number)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
job_ali:Ref_Number     LIKE(job_ali:Ref_Number)       !List box control field - type derived from field
job_ali:Account_Number LIKE(job_ali:Account_Number)   !List box control field - type derived from field
job_ali:Surname        LIKE(job_ali:Surname)          !List box control field - type derived from field
job_ali:Mobile_Number  LIKE(job_ali:Mobile_Number)    !List box control field - type derived from field
job_ali:ESN            LIKE(job_ali:ESN)              !List box control field - type derived from field
job_ali:MSN            LIKE(job_ali:MSN)              !List box control field - type derived from field
job_ali:Postcode       LIKE(job_ali:Postcode)         !List box control field - type derived from field
job_ali:Charge_Type    LIKE(job_ali:Charge_Type)      !List box control field - type derived from field
job_ali:Date_Completed LIKE(job_ali:Date_Completed)   !List box control field - type derived from field
Days_Since_Booking_Temp LIKE(Days_Since_Booking_Temp) !List box control field - type derived from local data
job_ali:Model_Number   LIKE(job_ali:Model_Number)     !Browse hot field - type derived from field
job_ali:Manufacturer   LIKE(job_ali:Manufacturer)     !Browse hot field - type derived from field
job_ali:Unit_Type      LIKE(job_ali:Unit_Type)        !Browse hot field - type derived from field
job_ali:Loan_Status    LIKE(job_ali:Loan_Status)      !Browse hot field - type derived from field
job_ali:Exchange_Status LIKE(job_ali:Exchange_Status) !Browse hot field - type derived from field
msn_temp               LIKE(msn_temp)                 !Browse hot field - type derived from local data
job_ali:Company_Name   LIKE(job_ali:Company_Name)     !Browse hot field - type derived from field
job_ali:Telephone_Delivery LIKE(job_ali:Telephone_Delivery) !Browse hot field - type derived from field
job_ali:Address_Line3_Delivery LIKE(job_ali:Address_Line3_Delivery) !Browse hot field - type derived from field
job_ali:Address_Line2_Delivery LIKE(job_ali:Address_Line2_Delivery) !Browse hot field - type derived from field
job_ali:Company_Name_Delivery LIKE(job_ali:Company_Name_Delivery) !Browse hot field - type derived from field
job_ali:Address_Line1_Delivery LIKE(job_ali:Address_Line1_Delivery) !Browse hot field - type derived from field
job_ali:Postcode_Delivery LIKE(job_ali:Postcode_Delivery) !Browse hot field - type derived from field
job_ali:Telephone_Collection LIKE(job_ali:Telephone_Collection) !Browse hot field - type derived from field
job_ali:Address_Line3_Collection LIKE(job_ali:Address_Line3_Collection) !Browse hot field - type derived from field
job_ali:Address_Line2_Collection LIKE(job_ali:Address_Line2_Collection) !Browse hot field - type derived from field
job_ali:Address_Line1_Collection LIKE(job_ali:Address_Line1_Collection) !Browse hot field - type derived from field
job_ali:Company_Name_Collection LIKE(job_ali:Company_Name_Collection) !Browse hot field - type derived from field
job_ali:Postcode_Collection LIKE(job_ali:Postcode_Collection) !Browse hot field - type derived from field
job_ali:Fax_Number     LIKE(job_ali:Fax_Number)       !Browse hot field - type derived from field
job_ali:Address_Line3  LIKE(job_ali:Address_Line3)    !Browse hot field - type derived from field
job_ali:Telephone_Number LIKE(job_ali:Telephone_Number) !Browse hot field - type derived from field
job_ali:Address_Line2  LIKE(job_ali:Address_Line2)    !Browse hot field - type derived from field
job_ali:Address_Line1  LIKE(job_ali:Address_Line1)    !Browse hot field - type derived from field
job_ali:Courier_Cost   LIKE(job_ali:Courier_Cost)     !Browse hot field - type derived from field
job_ali:Advance_Payment LIKE(job_ali:Advance_Payment) !Browse hot field - type derived from field
job_ali:Labour_Cost    LIKE(job_ali:Labour_Cost)      !Browse hot field - type derived from field
job_ali:Parts_Cost     LIKE(job_ali:Parts_Cost)       !Browse hot field - type derived from field
job_ali:Sub_Total      LIKE(job_ali:Sub_Total)        !Browse hot field - type derived from field
job_ali:Courier_Cost_Estimate LIKE(job_ali:Courier_Cost_Estimate) !Browse hot field - type derived from field
job_ali:Labour_Cost_Estimate LIKE(job_ali:Labour_Cost_Estimate) !Browse hot field - type derived from field
job_ali:Parts_Cost_Estimate LIKE(job_ali:Parts_Cost_Estimate) !Browse hot field - type derived from field
job_ali:Sub_Total_Estimate LIKE(job_ali:Sub_Total_Estimate) !Browse hot field - type derived from field
job_ali:Courier_Cost_Warranty LIKE(job_ali:Courier_Cost_Warranty) !Browse hot field - type derived from field
job_ali:Labour_Cost_Warranty LIKE(job_ali:Labour_Cost_Warranty) !Browse hot field - type derived from field
job_ali:Parts_Cost_Warranty LIKE(job_ali:Parts_Cost_Warranty) !Browse hot field - type derived from field
job_ali:Sub_Total_Warranty LIKE(job_ali:Sub_Total_Warranty) !Browse hot field - type derived from field
total_temp             LIKE(total_temp)               !Browse hot field - type derived from local data
balance_temp           LIKE(balance_temp)             !Browse hot field - type derived from local data
vat_total_warranty_temp LIKE(vat_total_warranty_temp) !Browse hot field - type derived from local data
total_warranty_temp    LIKE(total_warranty_temp)      !Browse hot field - type derived from local data
vat_total_estimate_temp LIKE(vat_total_estimate_temp) !Browse hot field - type derived from local data
total_estimate_temp    LIKE(total_estimate_temp)      !Browse hot field - type derived from local data
vat_total_temp         LIKE(vat_total_temp)           !Browse hot field - type derived from local data
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW3::View:Browse    VIEW(PARTS)
                       PROJECT(par:Quantity)
                       PROJECT(par:Description)
                       PROJECT(par:Record_Number)
                       PROJECT(par:Ref_Number)
                       PROJECT(par:Part_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
par:Quantity           LIKE(par:Quantity)             !List box control field - type derived from field
par:Description        LIKE(par:Description)          !List box control field - type derived from field
par:Record_Number      LIKE(par:Record_Number)        !Primary key field - type derived from field
par:Ref_Number         LIKE(par:Ref_Number)           !Browse key field - type derived from field
par:Part_Number        LIKE(par:Part_Number)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(WARPARTS)
                       PROJECT(wpr:Quantity)
                       PROJECT(wpr:Part_Number)
                       PROJECT(wpr:Record_Number)
                       PROJECT(wpr:Ref_Number)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
wpr:Quantity           LIKE(wpr:Quantity)             !List box control field - type derived from field
wpr:Part_Number        LIKE(wpr:Part_Number)          !List box control field - type derived from field
wpr:Record_Number      LIKE(wpr:Record_Number)        !Primary key field - type derived from field
wpr:Ref_Number         LIKE(wpr:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Auto Search Facility'),AT(,,666,348),FONT('Tahoma',8,,),CENTER,IMM,ICON('spy.ico'),TILED,TIMER(100),GRAY,DOUBLE
                       SHEET,AT(4,28,660,168),USE(?Sheet1),SPREAD
                         TAB('By Surname'),USE(?Surname_Tab)
                         END
                         TAB('By Mobile Number'),USE(?Mobile_Number_Tab)
                         END
                         TAB('By E.S.N. / I.M.E.I.'),USE(?ESN_Tab)
                         END
                         TAB('By M.S.N.'),USE(?MSN_Tab)
                         END
                       END
                       LIST,AT(8,44,652,148),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('36R(2)|M~Job No~@p<<<<<<<<<<<<#p@64L(2)|M~Account Number~@s15@79L(2)|M~Surname~@s30@63' &|
   'L(2)|M~Mobile Number~@s15@69L(2)|M~ESN/IMEI~@s16@45L(2)|M~MSN~@s10@47L(2)|M~Post' &|
   'code~@s10@124L|M~Charge Type~L(2)@s30@66R(2)|M~Date Completed~L@D6b@24L(2)|M~Day' &|
   's~@n6@'),FROM(Queue:Browse)
                       SHEET,AT(516,200,148,115),USE(?Sheet3),SPREAD
                         TAB('Chargeable'),USE(?Tab10)
                           STRING('Courier'),AT(520,216),USE(?String57)
                           STRING(@n14.2),AT(604,216),USE(job_ali:Courier_Cost),RIGHT
                           STRING('Labour'),AT(520,228),USE(?String25),TRN
                           STRING(@n14.2),AT(605,228),USE(job_ali:Labour_Cost),RIGHT
                           STRING('Parts'),AT(520,240),USE(?String26),TRN
                           STRING(@n14.2),AT(605,240),USE(job_ali:Parts_Cost),RIGHT
                           STRING('Sub Total'),AT(520,252),USE(?String58)
                           STRING(@n14.2),AT(605,252),USE(job_ali:Sub_Total),RIGHT
                           PANEL,AT(597,264,60,2),USE(?Panel5)
                           PANEL,AT(597,288,60,2),USE(?Panel5:2)
                           STRING('Advance Payment'),AT(520,292),USE(?String61)
                           STRING(@n14.2),AT(605,292),USE(job_ali:Advance_Payment),RIGHT
                           STRING('Balance'),AT(520,304),USE(?String63),FONT(,,,FONT:bold)
                           STRING(@n-14.2),AT(605,304),USE(balance_temp),RIGHT,FONT(,,,FONT:bold)
                           STRING('V.A.T.'),AT(520,268),USE(?String28),TRN
                           STRING(@n14.2),AT(605,268),USE(vat_total_temp),TRN,RIGHT
                           STRING('Total'),AT(520,276),USE(?String29),TRN,FONT(,,,FONT:bold)
                           STRING(@n-14.2),AT(605,276),USE(total_temp),RIGHT,FONT(,,,FONT:bold)
                         END
                         TAB('Estimate'),USE(?Tab9)
                           STRING('Courier'),AT(520,216),USE(?String47)
                           STRING(@n14.2),AT(604,216),USE(job_ali:Courier_Cost_Estimate),RIGHT
                           STRING('Labour'),AT(520,228),USE(?String48)
                           STRING(@n14.2),AT(604,228),USE(job_ali:Labour_Cost_Estimate),RIGHT
                           STRING('Parts'),AT(520,240),USE(?String49)
                           STRING(@n14.2),AT(604,240),USE(job_ali:Parts_Cost_Estimate),RIGHT
                           STRING('Sub Total'),AT(520,252),USE(?String50)
                           STRING(@n14.2),AT(604,252),USE(job_ali:Sub_Total_Estimate),RIGHT
                           PANEL,AT(597,264,60,2),USE(?Panel5:3)
                           STRING('V.A.T.'),AT(520,268),USE(?String51),TRN
                           STRING(@n14.2),AT(604,268),USE(vat_total_estimate_temp),RIGHT
                           PANEL,AT(597,288,60,2),USE(?Panel5:4)
                           STRING('Total'),AT(520,276),USE(?String52),FONT(,,,FONT:bold)
                           STRING(@n14.2),AT(604,276),USE(total_estimate_temp),RIGHT,FONT(,,,FONT:bold)
                         END
                         TAB('Warranty'),USE(?Tab11)
                           STRING('Courier'),AT(520,216),USE(?String57:2)
                           STRING(@n14.2),AT(604,216),USE(job_ali:Courier_Cost_Warranty),RIGHT
                           STRING('Labour'),AT(520,228),USE(?String25:2),TRN
                           STRING(@n14.2),AT(604,228),USE(job_ali:Labour_Cost_Warranty),RIGHT
                           STRING('Parts'),AT(520,240),USE(?String26:2),TRN
                           STRING('Sub Total'),AT(520,252),USE(?String58:2),TRN
                           STRING(@n14.2),AT(604,252),USE(job_ali:Sub_Total_Warranty),RIGHT
                           STRING(@n14.2),AT(604,240),USE(job_ali:Parts_Cost_Warranty),RIGHT
                           PANEL,AT(597,264,60,2),USE(?Panel5:5)
                           STRING('V.A.T.'),AT(520,268),USE(?String28:2),TRN
                           STRING(@n-14.2),AT(605,268),USE(vat_total_warranty_temp),TRN,RIGHT
                           STRING('Total'),AT(520,276),USE(?String29:2),TRN,FONT(,,,FONT:bold)
                           STRING(@n14.2),AT(604,276),USE(total_warranty_temp),RIGHT,FONT(,,,FONT:bold)
                           PANEL,AT(597,288,60,2),USE(?Panel5:6)
                         END
                       END
                       PANEL,AT(4,320,660,24),USE(?Panel1),FILL(COLOR:Silver)
                       SHEET,AT(176,200,168,115),USE(?Sheet4),SPREAD
                         TAB('Unit Details'),USE(?Unit_Details_Tab)
                           STRING('I.M.E.I. / M.S.N.'),AT(180,216),USE(?String9),TRN
                           STRING(@s30),AT(236,216,103,10),USE(job_ali:ESN),FONT(,,,FONT:bold)
                           STRING('M.S.N.'),AT(180,228),USE(?String10),TRN
                           STRING(@s30),AT(236,228,103,10),USE(msn_temp),FONT(,,,FONT:bold)
                           STRING('Make'),AT(180,240),USE(?String11),TRN
                           STRING(@s30),AT(236,240,103,10),USE(job_ali:Manufacturer),FONT(,,,FONT:bold)
                           STRING('Model'),AT(180,252),USE(?String12),TRN
                           STRING(@s30),AT(236,252,103,10),USE(job_ali:Model_Number),FONT(,,,FONT:bold)
                           STRING('Mobile Number'),AT(180,264),USE(?String13),TRN
                           STRING(@s15),AT(236,264),USE(job_ali:Mobile_Number),FONT(,,,FONT:bold)
                           STRING('Unit Type'),AT(180,276),USE(?String14),TRN
                           STRING(@s25),AT(236,276),USE(job_ali:Unit_Type),FONT(,,,FONT:bold)
                           STRING('Loan Status'),AT(180,288),USE(?String15),TRN
                           STRING(@s30),AT(236,288,103,10),USE(job_ali:Loan_Status),FONT(,,,FONT:bold)
                           STRING('Exchange Status'),AT(180,300),USE(?String16),TRN
                           STRING(@s30),AT(236,300,103,10),USE(job_ali:Exchange_Status),FONT(,,,FONT:bold)
                         END
                       END
                       SHEET,AT(348,200,164,116),USE(?Sheet5),SPREAD
                         TAB('Chargeable Parts'),USE(?Tab12)
                           LIST,AT(352,216,156,96),USE(?List:2),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('21R(2)|M~Qty~@n4@120L(2)|M~Description~@s30@'),FROM(Queue:Browse:1)
                         END
                         TAB('Warranty Parts'),USE(?Tab13)
                           LIST,AT(352,216,156,96),USE(?List:3),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('24R(2)|M~Qty~@s8@120L(2)|M~Part Number~@s30@'),FROM(Queue:Browse:2)
                         END
                       END
                       SHEET,AT(4,200,168,115),USE(?Sheet2),SPREAD
                         TAB('Invoice'),USE(?Invoice_Tab)
                           STRING(@s30),AT(8,216),USE(job_ali:Company_Name),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,228),USE(job_ali:Address_Line1),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,240),USE(job_ali:Address_Line2),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,252),USE(job_ali:Address_Line3),FONT(,,,FONT:bold)
                           STRING(@s10),AT(8,264),USE(job_ali:Postcode),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,276),USE(job_ali:Telephone_Number),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,288),USE(job_ali:Fax_Number),FONT(,,,FONT:bold)
                         END
                         TAB('Collection'),USE(?Collection_Tab)
                           STRING(@s30),AT(8,216),USE(job_ali:Company_Name_Collection),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,228),USE(job_ali:Address_Line1_Collection),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,240),USE(job_ali:Address_Line2_Collection),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,252),USE(job_ali:Address_Line3_Collection),FONT(,,,FONT:bold)
                           STRING(@s10),AT(8,264),USE(job_ali:Postcode_Collection),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,276),USE(job_ali:Telephone_Collection),FONT(,,,FONT:bold)
                         END
                         TAB('Delivery'),USE(?delivery_tab)
                           STRING(@s30),AT(8,216),USE(job_ali:Company_Name_Delivery),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,228),USE(job_ali:Address_Line1_Delivery),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,240),USE(job_ali:Address_Line2_Delivery),FONT(,,,FONT:bold)
                           STRING(@s30),AT(8,252),USE(job_ali:Address_Line3_Delivery),FONT(,,,FONT:bold)
                           STRING(@s10),AT(8,264),USE(job_ali:Postcode_Delivery),FONT(,,,FONT:bold)
                           STRING(@s15),AT(8,276),USE(job_ali:Telephone_Delivery),FONT(,,,FONT:bold)
                         END
                       END
                       BUTTON('&Create Job'),AT(8,324,76,16),USE(?Create_Job_Button),SKIP,LEFT,ICON('insert.gif')
                       BUTTON('Audit Trail'),AT(92,324,76,16),USE(?audit_Trail),LEFT,ICON('audit.gif')
                       BUTTON('Invoice Text'),AT(256,324,76,16),USE(?Button6),LEFT,ICON('history.gif')
                       BUTTON('Cancel'),AT(604,324,56,16),USE(?CancelButton),SKIP,LEFT,ICON('cancel.gif'),STD(STD:Close)
                       BUTTON('Fault Description'),AT(176,324,76,16),USE(?Button6:2),LEFT,ICON('history.gif')
                       BUTTON('Print Bouncer History'),AT(500,324,76,16),USE(?Print_Job_Card:2),LEFT,ICON(ICON:Print1)
                       BUTTON('Print Job Card'),AT(420,324,76,16),USE(?Print_Job_Card),LEFT,ICON(ICON:Print1)
                       PANEL,AT(4,4,660,20),USE(?Panel1:2),FILL(COLOR:Silver)
                       STRING('Title'),AT(8,8),USE(?Match_Found_Text),TRN,FONT('Tahoma',12,COLOR:Navy,FONT:bold)
                       BUTTON('Engineer Notes'),AT(336,324,76,16),USE(?Button7),LEFT,ICON('history.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 2
BRW1::Sort2:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 3
BRW1::Sort3:Locator  StepLocatorClass                 !Conditional Locator - Choice(?Sheet1) = 4
BRW3                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW3::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Surname_Tab{prop:Color} = 15066597
    ?Mobile_Number_Tab{prop:Color} = 15066597
    ?ESN_Tab{prop:Color} = 15066597
    ?MSN_Tab{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet3{prop:Color} = 15066597
    ?Tab10{prop:Color} = 15066597
    ?String57{prop:FontColor} = -1
    ?String57{prop:Color} = 15066597
    ?job_ali:Courier_Cost{prop:FontColor} = -1
    ?job_ali:Courier_Cost{prop:Color} = 15066597
    ?String25{prop:FontColor} = -1
    ?String25{prop:Color} = 15066597
    ?job_ali:Labour_Cost{prop:FontColor} = -1
    ?job_ali:Labour_Cost{prop:Color} = 15066597
    ?String26{prop:FontColor} = -1
    ?String26{prop:Color} = 15066597
    ?job_ali:Parts_Cost{prop:FontColor} = -1
    ?job_ali:Parts_Cost{prop:Color} = 15066597
    ?String58{prop:FontColor} = -1
    ?String58{prop:Color} = 15066597
    ?job_ali:Sub_Total{prop:FontColor} = -1
    ?job_ali:Sub_Total{prop:Color} = 15066597
    ?Panel5{prop:Fill} = 15066597

    ?Panel5:2{prop:Fill} = 15066597

    ?String61{prop:FontColor} = -1
    ?String61{prop:Color} = 15066597
    ?job_ali:Advance_Payment{prop:FontColor} = -1
    ?job_ali:Advance_Payment{prop:Color} = 15066597
    ?String63{prop:FontColor} = -1
    ?String63{prop:Color} = 15066597
    ?balance_temp{prop:FontColor} = -1
    ?balance_temp{prop:Color} = 15066597
    ?String28{prop:FontColor} = -1
    ?String28{prop:Color} = 15066597
    ?vat_total_temp{prop:FontColor} = -1
    ?vat_total_temp{prop:Color} = 15066597
    ?String29{prop:FontColor} = -1
    ?String29{prop:Color} = 15066597
    ?total_temp{prop:FontColor} = -1
    ?total_temp{prop:Color} = 15066597
    ?Tab9{prop:Color} = 15066597
    ?String47{prop:FontColor} = -1
    ?String47{prop:Color} = 15066597
    ?job_ali:Courier_Cost_Estimate{prop:FontColor} = -1
    ?job_ali:Courier_Cost_Estimate{prop:Color} = 15066597
    ?String48{prop:FontColor} = -1
    ?String48{prop:Color} = 15066597
    ?job_ali:Labour_Cost_Estimate{prop:FontColor} = -1
    ?job_ali:Labour_Cost_Estimate{prop:Color} = 15066597
    ?String49{prop:FontColor} = -1
    ?String49{prop:Color} = 15066597
    ?job_ali:Parts_Cost_Estimate{prop:FontColor} = -1
    ?job_ali:Parts_Cost_Estimate{prop:Color} = 15066597
    ?String50{prop:FontColor} = -1
    ?String50{prop:Color} = 15066597
    ?job_ali:Sub_Total_Estimate{prop:FontColor} = -1
    ?job_ali:Sub_Total_Estimate{prop:Color} = 15066597
    ?Panel5:3{prop:Fill} = 15066597

    ?String51{prop:FontColor} = -1
    ?String51{prop:Color} = 15066597
    ?vat_total_estimate_temp{prop:FontColor} = -1
    ?vat_total_estimate_temp{prop:Color} = 15066597
    ?Panel5:4{prop:Fill} = 15066597

    ?String52{prop:FontColor} = -1
    ?String52{prop:Color} = 15066597
    ?total_estimate_temp{prop:FontColor} = -1
    ?total_estimate_temp{prop:Color} = 15066597
    ?Tab11{prop:Color} = 15066597
    ?String57:2{prop:FontColor} = -1
    ?String57:2{prop:Color} = 15066597
    ?job_ali:Courier_Cost_Warranty{prop:FontColor} = -1
    ?job_ali:Courier_Cost_Warranty{prop:Color} = 15066597
    ?String25:2{prop:FontColor} = -1
    ?String25:2{prop:Color} = 15066597
    ?job_ali:Labour_Cost_Warranty{prop:FontColor} = -1
    ?job_ali:Labour_Cost_Warranty{prop:Color} = 15066597
    ?String26:2{prop:FontColor} = -1
    ?String26:2{prop:Color} = 15066597
    ?String58:2{prop:FontColor} = -1
    ?String58:2{prop:Color} = 15066597
    ?job_ali:Sub_Total_Warranty{prop:FontColor} = -1
    ?job_ali:Sub_Total_Warranty{prop:Color} = 15066597
    ?job_ali:Parts_Cost_Warranty{prop:FontColor} = -1
    ?job_ali:Parts_Cost_Warranty{prop:Color} = 15066597
    ?Panel5:5{prop:Fill} = 15066597

    ?String28:2{prop:FontColor} = -1
    ?String28:2{prop:Color} = 15066597
    ?vat_total_warranty_temp{prop:FontColor} = -1
    ?vat_total_warranty_temp{prop:Color} = 15066597
    ?String29:2{prop:FontColor} = -1
    ?String29:2{prop:Color} = 15066597
    ?total_warranty_temp{prop:FontColor} = -1
    ?total_warranty_temp{prop:Color} = 15066597
    ?Panel5:6{prop:Fill} = 15066597

    ?Panel1{prop:Fill} = 15066597

    ?Sheet4{prop:Color} = 15066597
    ?Unit_Details_Tab{prop:Color} = 15066597
    ?String9{prop:FontColor} = -1
    ?String9{prop:Color} = 15066597
    ?job_ali:ESN{prop:FontColor} = -1
    ?job_ali:ESN{prop:Color} = 15066597
    ?String10{prop:FontColor} = -1
    ?String10{prop:Color} = 15066597
    ?msn_temp{prop:FontColor} = -1
    ?msn_temp{prop:Color} = 15066597
    ?String11{prop:FontColor} = -1
    ?String11{prop:Color} = 15066597
    ?job_ali:Manufacturer{prop:FontColor} = -1
    ?job_ali:Manufacturer{prop:Color} = 15066597
    ?String12{prop:FontColor} = -1
    ?String12{prop:Color} = 15066597
    ?job_ali:Model_Number{prop:FontColor} = -1
    ?job_ali:Model_Number{prop:Color} = 15066597
    ?String13{prop:FontColor} = -1
    ?String13{prop:Color} = 15066597
    ?job_ali:Mobile_Number{prop:FontColor} = -1
    ?job_ali:Mobile_Number{prop:Color} = 15066597
    ?String14{prop:FontColor} = -1
    ?String14{prop:Color} = 15066597
    ?job_ali:Unit_Type{prop:FontColor} = -1
    ?job_ali:Unit_Type{prop:Color} = 15066597
    ?String15{prop:FontColor} = -1
    ?String15{prop:Color} = 15066597
    ?job_ali:Loan_Status{prop:FontColor} = -1
    ?job_ali:Loan_Status{prop:Color} = 15066597
    ?String16{prop:FontColor} = -1
    ?String16{prop:Color} = 15066597
    ?job_ali:Exchange_Status{prop:FontColor} = -1
    ?job_ali:Exchange_Status{prop:Color} = 15066597
    ?Sheet5{prop:Color} = 15066597
    ?Tab12{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Tab13{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Invoice_Tab{prop:Color} = 15066597
    ?job_ali:Company_Name{prop:FontColor} = -1
    ?job_ali:Company_Name{prop:Color} = 15066597
    ?job_ali:Address_Line1{prop:FontColor} = -1
    ?job_ali:Address_Line1{prop:Color} = 15066597
    ?job_ali:Address_Line2{prop:FontColor} = -1
    ?job_ali:Address_Line2{prop:Color} = 15066597
    ?job_ali:Address_Line3{prop:FontColor} = -1
    ?job_ali:Address_Line3{prop:Color} = 15066597
    ?job_ali:Postcode{prop:FontColor} = -1
    ?job_ali:Postcode{prop:Color} = 15066597
    ?job_ali:Telephone_Number{prop:FontColor} = -1
    ?job_ali:Telephone_Number{prop:Color} = 15066597
    ?job_ali:Fax_Number{prop:FontColor} = -1
    ?job_ali:Fax_Number{prop:Color} = 15066597
    ?Collection_Tab{prop:Color} = 15066597
    ?job_ali:Company_Name_Collection{prop:FontColor} = -1
    ?job_ali:Company_Name_Collection{prop:Color} = 15066597
    ?job_ali:Address_Line1_Collection{prop:FontColor} = -1
    ?job_ali:Address_Line1_Collection{prop:Color} = 15066597
    ?job_ali:Address_Line2_Collection{prop:FontColor} = -1
    ?job_ali:Address_Line2_Collection{prop:Color} = 15066597
    ?job_ali:Address_Line3_Collection{prop:FontColor} = -1
    ?job_ali:Address_Line3_Collection{prop:Color} = 15066597
    ?job_ali:Postcode_Collection{prop:FontColor} = -1
    ?job_ali:Postcode_Collection{prop:Color} = 15066597
    ?job_ali:Telephone_Collection{prop:FontColor} = -1
    ?job_ali:Telephone_Collection{prop:Color} = 15066597
    ?delivery_tab{prop:Color} = 15066597
    ?job_ali:Company_Name_Delivery{prop:FontColor} = -1
    ?job_ali:Company_Name_Delivery{prop:Color} = 15066597
    ?job_ali:Address_Line1_Delivery{prop:FontColor} = -1
    ?job_ali:Address_Line1_Delivery{prop:Color} = 15066597
    ?job_ali:Address_Line2_Delivery{prop:FontColor} = -1
    ?job_ali:Address_Line2_Delivery{prop:Color} = 15066597
    ?job_ali:Address_Line3_Delivery{prop:FontColor} = -1
    ?job_ali:Address_Line3_Delivery{prop:Color} = 15066597
    ?job_ali:Postcode_Delivery{prop:FontColor} = -1
    ?job_ali:Postcode_Delivery{prop:Color} = 15066597
    ?job_ali:Telephone_Delivery{prop:FontColor} = -1
    ?job_ali:Telephone_Delivery{prop:Color} = 15066597
    ?Panel1:2{prop:Fill} = 15066597

    ?Match_Found_Text{prop:FontColor} = -1
    ?Match_Found_Text{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
The_totals      Routine
    !Figure out what the labour rate etc is!

    Auto_Total_Price('C',vat",total",balance")
!    job:sub_total = job:labour_cost + job:parts_cost + job:courier_cost
!    total_temp = total"
    vat_total_temp = vat"
!    balance_due_temp = balance"

    Auto_Total_Price('E',vat",total",balance")
!    job:sub_total_estimate = job:labour_cost_estimate + job:parts_cost_estimate + job:courier_cost_estimate
    vat_total_estimate_temp = vat"
!    vat_estimate_temp = vat"

    Auto_Total_Price('W',vat",total",balance")
!    job:sub_total_warranty  = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
    vat_total_warranty_temp = vat"
!    total_warranty_temp = total"
!    balance_due_warranty_temp = balance"

    !vat_total_temp = job_ali:sub_total * (def:vat_rate_labour/100)
    total_temp  = job_ali:sub_total + vat_total_temp
    balance_temp = total_temp - job_ali:advance_payment
    !vat_total_warranty_temp = job_ali:sub_total_warranty * (def:vat_rate_labour/100)
    total_warranty_temp     = job_ali:sub_total_warranty + vat_total_warranty_temp
    !vat_total_estimate_temp = job_ali:sub_total_estimate * (def:vat_rate_labour/100)
    total_estimate_temp     = job_ali:sub_total_estimate + vat_total_estimate_temp


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Auto_Search_Window',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Auto_Search_Window',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Auto_Search_Window',1)
    SolaceViewVars('Search_Field_Temp',Search_Field_Temp,'Auto_Search_Window',1)
    SolaceViewVars('Search_By_Temp',Search_By_Temp,'Auto_Search_Window',1)
    SolaceViewVars('Search_Result_Temp',Search_Result_Temp,'Auto_Search_Window',1)
    SolaceViewVars('Trade_Customer_Temp',Trade_Customer_Temp,'Auto_Search_Window',1)
    SolaceViewVars('Customer_Name_Temp',Customer_Name_Temp,'Auto_Search_Window',1)
    SolaceViewVars('Days_Since_Booking_Temp',Days_Since_Booking_Temp,'Auto_Search_Window',1)
    SolaceViewVars('total_temp',total_temp,'Auto_Search_Window',1)
    SolaceViewVars('balance_temp',balance_temp,'Auto_Search_Window',1)
    SolaceViewVars('vat_total_warranty_temp',vat_total_warranty_temp,'Auto_Search_Window',1)
    SolaceViewVars('total_warranty_temp',total_warranty_temp,'Auto_Search_Window',1)
    SolaceViewVars('vat_total_estimate_temp',vat_total_estimate_temp,'Auto_Search_Window',1)
    SolaceViewVars('total_estimate_temp',total_estimate_temp,'Auto_Search_Window',1)
    SolaceViewVars('vat_total_temp',vat_total_temp,'Auto_Search_Window',1)
    SolaceViewVars('msn_temp',msn_temp,'Auto_Search_Window',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Surname_Tab;  SolaceCtrlName = '?Surname_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Mobile_Number_Tab;  SolaceCtrlName = '?Mobile_Number_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ESN_Tab;  SolaceCtrlName = '?ESN_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MSN_Tab;  SolaceCtrlName = '?MSN_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab10;  SolaceCtrlName = '?Tab10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String57;  SolaceCtrlName = '?String57';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Courier_Cost;  SolaceCtrlName = '?job_ali:Courier_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String25;  SolaceCtrlName = '?String25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Labour_Cost;  SolaceCtrlName = '?job_ali:Labour_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String26;  SolaceCtrlName = '?String26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Parts_Cost;  SolaceCtrlName = '?job_ali:Parts_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String58;  SolaceCtrlName = '?String58';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Sub_Total;  SolaceCtrlName = '?job_ali:Sub_Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel5;  SolaceCtrlName = '?Panel5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel5:2;  SolaceCtrlName = '?Panel5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String61;  SolaceCtrlName = '?String61';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Advance_Payment;  SolaceCtrlName = '?job_ali:Advance_Payment';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String63;  SolaceCtrlName = '?String63';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?balance_temp;  SolaceCtrlName = '?balance_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String28;  SolaceCtrlName = '?String28';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_total_temp;  SolaceCtrlName = '?vat_total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String29;  SolaceCtrlName = '?String29';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_temp;  SolaceCtrlName = '?total_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab9;  SolaceCtrlName = '?Tab9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String47;  SolaceCtrlName = '?String47';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Courier_Cost_Estimate;  SolaceCtrlName = '?job_ali:Courier_Cost_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String48;  SolaceCtrlName = '?String48';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Labour_Cost_Estimate;  SolaceCtrlName = '?job_ali:Labour_Cost_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String49;  SolaceCtrlName = '?String49';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Parts_Cost_Estimate;  SolaceCtrlName = '?job_ali:Parts_Cost_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String50;  SolaceCtrlName = '?String50';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Sub_Total_Estimate;  SolaceCtrlName = '?job_ali:Sub_Total_Estimate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel5:3;  SolaceCtrlName = '?Panel5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String51;  SolaceCtrlName = '?String51';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_total_estimate_temp;  SolaceCtrlName = '?vat_total_estimate_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel5:4;  SolaceCtrlName = '?Panel5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String52;  SolaceCtrlName = '?String52';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_estimate_temp;  SolaceCtrlName = '?total_estimate_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab11;  SolaceCtrlName = '?Tab11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String57:2;  SolaceCtrlName = '?String57:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Courier_Cost_Warranty;  SolaceCtrlName = '?job_ali:Courier_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String25:2;  SolaceCtrlName = '?String25:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Labour_Cost_Warranty;  SolaceCtrlName = '?job_ali:Labour_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String26:2;  SolaceCtrlName = '?String26:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String58:2;  SolaceCtrlName = '?String58:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Sub_Total_Warranty;  SolaceCtrlName = '?job_ali:Sub_Total_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Parts_Cost_Warranty;  SolaceCtrlName = '?job_ali:Parts_Cost_Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel5:5;  SolaceCtrlName = '?Panel5:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String28:2;  SolaceCtrlName = '?String28:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?vat_total_warranty_temp;  SolaceCtrlName = '?vat_total_warranty_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String29:2;  SolaceCtrlName = '?String29:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?total_warranty_temp;  SolaceCtrlName = '?total_warranty_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel5:6;  SolaceCtrlName = '?Panel5:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Unit_Details_Tab;  SolaceCtrlName = '?Unit_Details_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String9;  SolaceCtrlName = '?String9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:ESN;  SolaceCtrlName = '?job_ali:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String10;  SolaceCtrlName = '?String10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?msn_temp;  SolaceCtrlName = '?msn_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String11;  SolaceCtrlName = '?String11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Manufacturer;  SolaceCtrlName = '?job_ali:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String12;  SolaceCtrlName = '?String12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Model_Number;  SolaceCtrlName = '?job_ali:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String13;  SolaceCtrlName = '?String13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Mobile_Number;  SolaceCtrlName = '?job_ali:Mobile_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String14;  SolaceCtrlName = '?String14';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Unit_Type;  SolaceCtrlName = '?job_ali:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String15;  SolaceCtrlName = '?String15';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Loan_Status;  SolaceCtrlName = '?job_ali:Loan_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String16;  SolaceCtrlName = '?String16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Exchange_Status;  SolaceCtrlName = '?job_ali:Exchange_Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet5;  SolaceCtrlName = '?Sheet5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab12;  SolaceCtrlName = '?Tab12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab13;  SolaceCtrlName = '?Tab13';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoice_Tab;  SolaceCtrlName = '?Invoice_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Company_Name;  SolaceCtrlName = '?job_ali:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line1;  SolaceCtrlName = '?job_ali:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line2;  SolaceCtrlName = '?job_ali:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line3;  SolaceCtrlName = '?job_ali:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Postcode;  SolaceCtrlName = '?job_ali:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Telephone_Number;  SolaceCtrlName = '?job_ali:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Fax_Number;  SolaceCtrlName = '?job_ali:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Collection_Tab;  SolaceCtrlName = '?Collection_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Company_Name_Collection;  SolaceCtrlName = '?job_ali:Company_Name_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line1_Collection;  SolaceCtrlName = '?job_ali:Address_Line1_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line2_Collection;  SolaceCtrlName = '?job_ali:Address_Line2_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line3_Collection;  SolaceCtrlName = '?job_ali:Address_Line3_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Postcode_Collection;  SolaceCtrlName = '?job_ali:Postcode_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Telephone_Collection;  SolaceCtrlName = '?job_ali:Telephone_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?delivery_tab;  SolaceCtrlName = '?delivery_tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Company_Name_Delivery;  SolaceCtrlName = '?job_ali:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line1_Delivery;  SolaceCtrlName = '?job_ali:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line2_Delivery;  SolaceCtrlName = '?job_ali:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Address_Line3_Delivery;  SolaceCtrlName = '?job_ali:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Postcode_Delivery;  SolaceCtrlName = '?job_ali:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job_ali:Telephone_Delivery;  SolaceCtrlName = '?job_ali:Telephone_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Create_Job_Button;  SolaceCtrlName = '?Create_Job_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?audit_Trail;  SolaceCtrlName = '?audit_Trail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6;  SolaceCtrlName = '?Button6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button6:2;  SolaceCtrlName = '?Button6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Job_Card:2;  SolaceCtrlName = '?Print_Job_Card:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Print_Job_Card;  SolaceCtrlName = '?Print_Job_Card';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1:2;  SolaceCtrlName = '?Panel1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Match_Found_Text;  SolaceCtrlName = '?Match_Found_Text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button7;  SolaceCtrlName = '?Button7';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Auto_Search_Window')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Auto_Search_Window')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:INVOICE.Open
  Relate:VATCODE.Open
  Access:JOBS_ALIAS.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?List,Queue:Browse.ViewPosition,BRW1::View:Browse,Queue:Browse,Relate:JOBS_ALIAS,SELF)
  BRW3.Init(?List:2,Queue:Browse:1.ViewPosition,BRW3::View:Browse,Queue:Browse:1,Relate:PARTS,SELF)
  BRW5.Init(?List:3,Queue:Browse:2.ViewPosition,BRW5::View:Browse,Queue:Browse:2,Relate:WARPARTS,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,job_ali:MobileNumberKey)
  BRW1.AddRange(job_ali:Mobile_Number,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(,job_ali:Mobile_Number,1,BRW1)
  BRW1.SetFilter('(job_ali:ref_number <<> glo:select11)')
  BRW1.AddSortOrder(,job_ali:ESN_Key)
  BRW1.AddRange(job_ali:ESN,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(,job_ali:ESN,1,BRW1)
  BRW1.SetFilter('(job_ali:ref_number <<> glo:select11)')
  BRW1.AddSortOrder(,job_ali:MSN_Key)
  BRW1.AddRange(job_ali:MSN,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(,job_ali:MSN,1,BRW1)
  BRW1.SetFilter('(job_ali:ref_number <<> glo:select11)')
  BRW1.AddSortOrder(,job_ali:Surname_Key)
  BRW1.AddRange(job_ali:Surname,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,job_ali:Surname,1,BRW1)
  BRW1.SetFilter('(job_ali:ref_number <<> glo:select11)')
  BIND('Days_Since_Booking_Temp',Days_Since_Booking_Temp)
  BIND('msn_temp',msn_temp)
  BIND('total_temp',total_temp)
  BIND('balance_temp',balance_temp)
  BIND('vat_total_warranty_temp',vat_total_warranty_temp)
  BIND('total_warranty_temp',total_warranty_temp)
  BIND('vat_total_estimate_temp',vat_total_estimate_temp)
  BIND('total_estimate_temp',total_estimate_temp)
  BIND('vat_total_temp',vat_total_temp)
  BIND('GLO:Select2',GLO:Select2)
  BIND('GLO:Select11',GLO:Select11)
  BRW1.AddField(job_ali:Ref_Number,BRW1.Q.job_ali:Ref_Number)
  BRW1.AddField(job_ali:Account_Number,BRW1.Q.job_ali:Account_Number)
  BRW1.AddField(job_ali:Surname,BRW1.Q.job_ali:Surname)
  BRW1.AddField(job_ali:Mobile_Number,BRW1.Q.job_ali:Mobile_Number)
  BRW1.AddField(job_ali:ESN,BRW1.Q.job_ali:ESN)
  BRW1.AddField(job_ali:MSN,BRW1.Q.job_ali:MSN)
  BRW1.AddField(job_ali:Postcode,BRW1.Q.job_ali:Postcode)
  BRW1.AddField(job_ali:Charge_Type,BRW1.Q.job_ali:Charge_Type)
  BRW1.AddField(job_ali:Date_Completed,BRW1.Q.job_ali:Date_Completed)
  BRW1.AddField(Days_Since_Booking_Temp,BRW1.Q.Days_Since_Booking_Temp)
  BRW1.AddField(job_ali:Model_Number,BRW1.Q.job_ali:Model_Number)
  BRW1.AddField(job_ali:Manufacturer,BRW1.Q.job_ali:Manufacturer)
  BRW1.AddField(job_ali:Unit_Type,BRW1.Q.job_ali:Unit_Type)
  BRW1.AddField(job_ali:Loan_Status,BRW1.Q.job_ali:Loan_Status)
  BRW1.AddField(job_ali:Exchange_Status,BRW1.Q.job_ali:Exchange_Status)
  BRW1.AddField(msn_temp,BRW1.Q.msn_temp)
  BRW1.AddField(job_ali:Company_Name,BRW1.Q.job_ali:Company_Name)
  BRW1.AddField(job_ali:Telephone_Delivery,BRW1.Q.job_ali:Telephone_Delivery)
  BRW1.AddField(job_ali:Address_Line3_Delivery,BRW1.Q.job_ali:Address_Line3_Delivery)
  BRW1.AddField(job_ali:Address_Line2_Delivery,BRW1.Q.job_ali:Address_Line2_Delivery)
  BRW1.AddField(job_ali:Company_Name_Delivery,BRW1.Q.job_ali:Company_Name_Delivery)
  BRW1.AddField(job_ali:Address_Line1_Delivery,BRW1.Q.job_ali:Address_Line1_Delivery)
  BRW1.AddField(job_ali:Postcode_Delivery,BRW1.Q.job_ali:Postcode_Delivery)
  BRW1.AddField(job_ali:Telephone_Collection,BRW1.Q.job_ali:Telephone_Collection)
  BRW1.AddField(job_ali:Address_Line3_Collection,BRW1.Q.job_ali:Address_Line3_Collection)
  BRW1.AddField(job_ali:Address_Line2_Collection,BRW1.Q.job_ali:Address_Line2_Collection)
  BRW1.AddField(job_ali:Address_Line1_Collection,BRW1.Q.job_ali:Address_Line1_Collection)
  BRW1.AddField(job_ali:Company_Name_Collection,BRW1.Q.job_ali:Company_Name_Collection)
  BRW1.AddField(job_ali:Postcode_Collection,BRW1.Q.job_ali:Postcode_Collection)
  BRW1.AddField(job_ali:Fax_Number,BRW1.Q.job_ali:Fax_Number)
  BRW1.AddField(job_ali:Address_Line3,BRW1.Q.job_ali:Address_Line3)
  BRW1.AddField(job_ali:Telephone_Number,BRW1.Q.job_ali:Telephone_Number)
  BRW1.AddField(job_ali:Address_Line2,BRW1.Q.job_ali:Address_Line2)
  BRW1.AddField(job_ali:Address_Line1,BRW1.Q.job_ali:Address_Line1)
  BRW1.AddField(job_ali:Courier_Cost,BRW1.Q.job_ali:Courier_Cost)
  BRW1.AddField(job_ali:Advance_Payment,BRW1.Q.job_ali:Advance_Payment)
  BRW1.AddField(job_ali:Labour_Cost,BRW1.Q.job_ali:Labour_Cost)
  BRW1.AddField(job_ali:Parts_Cost,BRW1.Q.job_ali:Parts_Cost)
  BRW1.AddField(job_ali:Sub_Total,BRW1.Q.job_ali:Sub_Total)
  BRW1.AddField(job_ali:Courier_Cost_Estimate,BRW1.Q.job_ali:Courier_Cost_Estimate)
  BRW1.AddField(job_ali:Labour_Cost_Estimate,BRW1.Q.job_ali:Labour_Cost_Estimate)
  BRW1.AddField(job_ali:Parts_Cost_Estimate,BRW1.Q.job_ali:Parts_Cost_Estimate)
  BRW1.AddField(job_ali:Sub_Total_Estimate,BRW1.Q.job_ali:Sub_Total_Estimate)
  BRW1.AddField(job_ali:Courier_Cost_Warranty,BRW1.Q.job_ali:Courier_Cost_Warranty)
  BRW1.AddField(job_ali:Labour_Cost_Warranty,BRW1.Q.job_ali:Labour_Cost_Warranty)
  BRW1.AddField(job_ali:Parts_Cost_Warranty,BRW1.Q.job_ali:Parts_Cost_Warranty)
  BRW1.AddField(job_ali:Sub_Total_Warranty,BRW1.Q.job_ali:Sub_Total_Warranty)
  BRW1.AddField(total_temp,BRW1.Q.total_temp)
  BRW1.AddField(balance_temp,BRW1.Q.balance_temp)
  BRW1.AddField(vat_total_warranty_temp,BRW1.Q.vat_total_warranty_temp)
  BRW1.AddField(total_warranty_temp,BRW1.Q.total_warranty_temp)
  BRW1.AddField(vat_total_estimate_temp,BRW1.Q.vat_total_estimate_temp)
  BRW1.AddField(total_estimate_temp,BRW1.Q.total_estimate_temp)
  BRW1.AddField(vat_total_temp,BRW1.Q.vat_total_temp)
  BRW3.Q &= Queue:Browse:1
  BRW3.RetainRow = 0
  BRW3.AddSortOrder(,par:Part_Number_Key)
  BRW3.AddRange(par:Ref_Number,job_ali:Ref_Number)
  BRW3.AddLocator(BRW3::Sort0:Locator)
  BRW3::Sort0:Locator.Init(,par:Part_Number,1,BRW3)
  BRW3.AddField(par:Quantity,BRW3.Q.par:Quantity)
  BRW3.AddField(par:Description,BRW3.Q.par:Description)
  BRW3.AddField(par:Record_Number,BRW3.Q.par:Record_Number)
  BRW3.AddField(par:Ref_Number,BRW3.Q.par:Ref_Number)
  BRW3.AddField(par:Part_Number,BRW3.Q.par:Part_Number)
  BRW5.Q &= Queue:Browse:2
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,wpr:Part_Number_Key)
  BRW5.AddRange(wpr:Ref_Number,job_ali:Ref_Number)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,wpr:Part_Number,1,BRW5)
  BRW5.AddField(wpr:Quantity,BRW5.Q.wpr:Quantity)
  BRW5.AddField(wpr:Part_Number,BRW5.Q.wpr:Part_Number)
  BRW5.AddField(wpr:Record_Number,BRW5.Q.wpr:Record_Number)
  BRW5.AddField(wpr:Ref_Number,BRW5.Q.wpr:Ref_Number)
  BRW3.AddToolbarTarget(Toolbar)
  BRW5.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
    Relate:VATCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Auto_Search_Window',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Create_Job_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Job_Button, Accepted)
      ThisWindow.reset
      do_job# = 1
      If job_ali:date_completed = ''
          Set(defaults)
          access:defaults.next()
          If def:allow_bouncer <> 'YES'
              Case MessageEx('Cannot replicate and incomplete job.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              do_job# = 0
              glo:select3 = ''
          Else
              do_job# = 1
          End
      End!If job_ali:date_completed = ''
      If do_job# = 1
          glo:select3 = job_ali:ref_number
          New_Job_Screen
          Post(event:closewindow)
      end !If do_job# = 1
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Create_Job_Button, Accepted)
    OF ?audit_Trail
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?audit_Trail, Accepted)
      glo:select12 = JOB_ALI:Ref_Number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?audit_Trail, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?audit_Trail
      ThisWindow.Update
      Browse_Audit
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?audit_Trail, Accepted)
      glo:select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?audit_Trail, Accepted)
    OF ?Button6
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      ShowText(brw1.q.job_ali:ref_number,'INVOICE')
      ThisWindow.Reset
    OF ?Button6:2
      ThisWindow.Update
      ShowText(brw1.q.job_ali:ref_number,'FAULT')
      ThisWindow.Reset
    OF ?Print_Job_Card:2
      ThisWindow.Update
      Bouncer_History
      ThisWindow.Reset
    OF ?Print_Job_Card
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Job_Card, Accepted)
      thiswindow.reset(1)
      glo:select1  = job_ali:ref_number
      Job_Card
      glo:select1  = 'ESN'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Print_Job_Card, Accepted)
    OF ?Button7
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      ShowText(brw1.q.job_ali:ref_number,'NOTES')
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Auto_Search_Window')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case glo:select1
          Of 'SURNAME'
              unhide(?surname_tab)
              Hide(?mobile_number_tab)
              Hide(?esn_tab)
              Hide(?msn_tab)
              ?match_found_text{prop:text} = 'Surname Match Found: ' & Clip(glo:select2)
          Of 'MOBILE'
              hide(?surname_tab)
              unHide(?mobile_number_tab)
              Hide(?esn_tab)
              Hide(?msn_tab)
              ?match_found_text{prop:text} = 'Mobile Number Match Found: ' & Clip(glo:select2)
          Of 'ESN'
              hide(?surname_tab)
              Hide(?mobile_number_tab)
              unHide(?esn_tab)
              Hide(?msn_tab)
              ?match_found_text{prop:text} = 'I.M.E.I. / E.S.N. Match Found: ' & Clip(glo:select2)
          Of 'MSN'
              hide(?surname_tab)
              Hide(?mobile_number_tab)
              Hide(?esn_tab)
              unHide(?msn_tab)
              ?match_found_text{prop:text} = 'M.S.N. Match Found: ' & Clip(glo:select2)
      End
      If glo:select12 = 'VIEW ONLY'
          Hide(?Create_Job_Button)
      End
      BRW1.ResetSort(1)
      BRW3.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      do the_totals
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet1) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?Sheet1) = 3
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?Sheet1) = 4
    RETURN SELF.SetSort(3,Force)
  ELSE
    RETURN SELF.SetSort(4,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())
  If job_ali:title = ''
      If job_ali:initial = ''
          customer_name_temp = Clip(job_ali:surname)
      Else
          customer_name_temp = Clip(job_ali:initial) & ' ' & Clip(job_ali:surname)
      End
  Else
      If job_ali:initial = ''
          customer_name_temp = Clip(job_ali:title) & ' ' & Clip(job_ali:surname)
      Else
          customer_name_temp = Clip(job_ali:title) & ' ' & Clip(job_ali:initial) & ' ' & Clip(job_ali:surname)
      End
  End
  
  Trade_Customer_Temp = CLIP(job_ali:Account_Number) & ' - ' & CLIP(job_ali:Trade_Account_Name)
  Days_Since_Booking_Temp = TODAY() - job_ali:date_booked
  IF (job_ali:MSN <> '')
    msn_temp = job_ali:MSN
  ELSE
    msn_temp = 'N/A'
  END
  PARENT.SetQueueRecord
  SELF.Q.Days_Since_Booking_Temp = Days_Since_Booking_Temp !Assign formula result to display queue
  SELF.Q.msn_temp = msn_temp                          !Assign formula result to display queue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, SetQueueRecord, ())


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW3.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

