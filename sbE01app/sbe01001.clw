

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01001.INC'),ONCE        !Local module procedure declarations
                     END


Updateuser_levels PROCEDURE                           !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::12:TAGFLAG         BYTE(0)
DASBRW::12:TAGMOUSE        BYTE(0)
DASBRW::12:TAGDISPSTATUS   BYTE(0)
DASBRW::12:QUEUE          QUEUE
Access_Areas_Pointer          LIKE(GLO:Access_Areas_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::13:TAGFLAG         BYTE(0)
DASBRW::13:TAGMOUSE        BYTE(0)
DASBRW::13:TAGDISPSTATUS   BYTE(0)
DASBRW::13:QUEUE          QUEUE
Access_Level_Pointer          LIKE(GLO:Access_Level_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
save_all_id          USHORT,AUTO
CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
areas_tag            STRING(1)
level_tag            STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AvailableQueue       QUEUE,PRE(avlque)
AccessLevel          STRING(30)
                     END
tmp:Show             BYTE(0)
BRW5::View:Browse    VIEW(ACCAREAS)
                       PROJECT(acc:Access_Area)
                       PROJECT(acc:User_Level)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
areas_tag              LIKE(areas_tag)                !List box control field - type derived from local data
areas_tag_Icon         LONG                           !Entry's icon ID
acc:Access_Area        LIKE(acc:Access_Area)          !List box control field - type derived from field
acc:User_Level         LIKE(acc:User_Level)           !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ALLLEVEL)
                       PROJECT(all:Access_Level)
                       PROJECT(all:Description)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
level_tag              LIKE(level_tag)                !List box control field - type derived from local data
level_tag_NormalFG     LONG                           !Normal forground color
level_tag_NormalBG     LONG                           !Normal background color
level_tag_SelectedFG   LONG                           !Selected forground color
level_tag_SelectedBG   LONG                           !Selected background color
level_tag_Icon         LONG                           !Entry's icon ID
all:Access_Level       LIKE(all:Access_Level)         !List box control field - type derived from field
all:Access_Level_NormalFG LONG                        !Normal forground color
all:Access_Level_NormalBG LONG                        !Normal background color
all:Access_Level_SelectedFG LONG                      !Selected forground color
all:Access_Level_SelectedBG LONG                      !Selected background color
tmp:Show               LIKE(tmp:Show)                 !List box control field - type derived from local data
tmp:Show_NormalFG      LONG                           !Normal forground color
tmp:Show_NormalBG      LONG                           !Normal background color
tmp:Show_SelectedFG    LONG                           !Selected forground color
tmp:Show_SelectedBG    LONG                           !Selected background color
all:Description        LIKE(all:Description)          !Browse hot field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::lev:Record  LIKE(lev:RECORD),STATIC
QuickWindow          WINDOW('Update the Access Levels'),AT(,,668,261),FONT('Tahoma',8,,),CENTER,IMM,HLP('Updateuser_levels'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,232,224),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Access Level'),AT(8,20),USE(?lev:user_level:Prompt)
                           ENTRY(@s25),AT(84,20,124,10),USE(lev:User_Level),FONT(,,,FONT:bold),REQ,UPR
                           PROMPT('Description'),AT(8,36),USE(?ALL:Description:Prompt)
                           TEXT,AT(84,36,148,60),USE(all:Description),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           BUTTON('&Insert'),AT(188,104,42,12),USE(?Insert),HIDE
                           BUTTON('&Change'),AT(188,120,42,12),USE(?Change),HIDE
                           BUTTON('&Delete'),AT(188,136,42,12),USE(?Delete),HIDE
                         END
                       END
                       SHEET,AT(504,4,160,224),USE(?CurrentTab:2),SPREAD
                         TAB('Access Areas For Access Level'),USE(?Tab3)
                           LIST,AT(508,20,152,184),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10L(2)I@s1@100L(2)|~Access Area~@s40@'),FROM(Queue:Browse)
                           BUTTON('T&ag'),AT(508,208,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('Tag All'),AT(560,208,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('Untag All'),AT(612,208,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                       END
                       PANEL,AT(404,4,96,224),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&Add Tagged Access Levels'),AT(408,20,88,20),USE(?Add_Areas),FLAT,RIGHT,ICON('Select2.ico')
                       PROMPT(' - Access Level Not Allocated'),AT(420,52,66,22),USE(?Prompt3)
                       BOX,AT(408,52,8,8),USE(?Box1),COLOR(COLOR:Black),FILL(COLOR:Red)
                       BUTTON('Remove Tagged Access Areas'),AT(408,156,88,20),USE(?Remove_Areas),FLAT,LEFT,ICON('Select.ico')
                       SHEET,AT(240,4,160,224),USE(?Sheet2),SPREAD
                         TAB('All Access Area'),USE(?Tab2)
                           LIST,AT(244,20,152,184),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('10L(2)*I@s1@100L(2)|*~Access Level~@s40@4L(2)|*~Show~@n1@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(304,48,32,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(304,84,32,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Rev tags'),AT(296,108,32,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(284,132,32,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('&Tag'),AT(244,208,48,16),USE(?DASTAG:2),LEFT,ICON('tag.gif')
                           BUTTON('Tag All'),AT(296,208,48,16),USE(?DASTAGAll:2),LEFT,ICON('tagall.gif')
                           BUTTON('Untag All'),AT(348,208,48,16),USE(?DASUNTAGALL:2),LEFT,ICON('untag.gif')
                         END
                       END
                       PANEL,AT(4,232,660,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(548,236,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(604,236,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW5                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  IncrementalLocatorClass          !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_acc_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?lev:user_level:Prompt{prop:FontColor} = -1
    ?lev:user_level:Prompt{prop:Color} = 15066597
    If ?lev:User_Level{prop:ReadOnly} = True
        ?lev:User_Level{prop:FontColor} = 65793
        ?lev:User_Level{prop:Color} = 15066597
    Elsif ?lev:User_Level{prop:Req} = True
        ?lev:User_Level{prop:FontColor} = 65793
        ?lev:User_Level{prop:Color} = 8454143
    Else ! If ?lev:User_Level{prop:Req} = True
        ?lev:User_Level{prop:FontColor} = 65793
        ?lev:User_Level{prop:Color} = 16777215
    End ! If ?lev:User_Level{prop:Req} = True
    ?lev:User_Level{prop:Trn} = 0
    ?lev:User_Level{prop:FontStyle} = font:Bold
    ?ALL:Description:Prompt{prop:FontColor} = -1
    ?ALL:Description:Prompt{prop:Color} = 15066597
    If ?all:Description{prop:ReadOnly} = True
        ?all:Description{prop:FontColor} = 65793
        ?all:Description{prop:Color} = 15066597
    Elsif ?all:Description{prop:Req} = True
        ?all:Description{prop:FontColor} = 65793
        ?all:Description{prop:Color} = 8454143
    Else ! If ?all:Description{prop:Req} = True
        ?all:Description{prop:FontColor} = 65793
        ?all:Description{prop:Color} = 16777215
    End ! If ?all:Description{prop:Req} = True
    ?all:Description{prop:Trn} = 0
    ?all:Description{prop:FontStyle} = font:Bold
    ?CurrentTab:2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel2{prop:Fill} = 15066597

    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::12:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW5.UpdateBuffer
   GLO:Q_AccessAreas.Access_Areas_Pointer = acc:Access_Area
   GET(GLO:Q_AccessAreas,GLO:Q_AccessAreas.Access_Areas_Pointer)
  IF ERRORCODE()
     GLO:Q_AccessAreas.Access_Areas_Pointer = acc:Access_Area
     ADD(GLO:Q_AccessAreas,GLO:Q_AccessAreas.Access_Areas_Pointer)
    areas_tag = '*'
  ELSE
    DELETE(GLO:Q_AccessAreas)
    areas_tag = ''
  END
    Queue:Browse.areas_tag = areas_tag
  IF (areas_tag = '*')
    Queue:Browse.areas_tag_Icon = 2
  ELSE
    Queue:Browse.areas_tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(GLO:Q_AccessAreas)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_AccessAreas.Access_Areas_Pointer = acc:Access_Area
     ADD(GLO:Q_AccessAreas,GLO:Q_AccessAreas.Access_Areas_Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_AccessAreas)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::12:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_AccessAreas)
    GET(GLO:Q_AccessAreas,QR#)
    DASBRW::12:QUEUE = GLO:Q_AccessAreas
    ADD(DASBRW::12:QUEUE)
  END
  FREE(GLO:Q_AccessAreas)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::12:QUEUE.Access_Areas_Pointer = acc:Access_Area
     GET(DASBRW::12:QUEUE,DASBRW::12:QUEUE.Access_Areas_Pointer)
    IF ERRORCODE()
       GLO:Q_AccessAreas.Access_Areas_Pointer = acc:Access_Area
       ADD(GLO:Q_AccessAreas,GLO:Q_AccessAreas.Access_Areas_Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::12:DASSHOWTAG Routine
   CASE DASBRW::12:TAGDISPSTATUS
   OF 0
      DASBRW::12:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::12:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::12:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::13:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW6.UpdateBuffer
   GLO:Q_AccessLevel.Access_Level_Pointer = all:Access_Level
   GET(GLO:Q_AccessLevel,GLO:Q_AccessLevel.Access_Level_Pointer)
  IF ERRORCODE()
     GLO:Q_AccessLevel.Access_Level_Pointer = all:Access_Level
     ADD(GLO:Q_AccessLevel,GLO:Q_AccessLevel.Access_Level_Pointer)
    level_tag = '*'
  ELSE
    DELETE(GLO:Q_AccessLevel)
    level_tag = ''
  END
    Queue:Browse:1.level_tag = level_tag
  Queue:Browse:1.level_tag_NormalFG = -1
  Queue:Browse:1.level_tag_NormalBG = -1
  Queue:Browse:1.level_tag_SelectedFG = -1
  Queue:Browse:1.level_tag_SelectedBG = -1
  IF (level_tag = '*')
    Queue:Browse:1.level_tag_Icon = 2
  ELSE
    Queue:Browse:1.level_tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(GLO:Q_AccessLevel)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Q_AccessLevel.Access_Level_Pointer = all:Access_Level
     ADD(GLO:Q_AccessLevel,GLO:Q_AccessLevel.Access_Level_Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Q_AccessLevel)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::13:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Q_AccessLevel)
    GET(GLO:Q_AccessLevel,QR#)
    DASBRW::13:QUEUE = GLO:Q_AccessLevel
    ADD(DASBRW::13:QUEUE)
  END
  FREE(GLO:Q_AccessLevel)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::13:QUEUE.Access_Level_Pointer = all:Access_Level
     GET(DASBRW::13:QUEUE,DASBRW::13:QUEUE.Access_Level_Pointer)
    IF ERRORCODE()
       GLO:Q_AccessLevel.Access_Level_Pointer = all:Access_Level
       ADD(GLO:Q_AccessLevel,GLO:Q_AccessLevel.Access_Level_Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::13:DASSHOWTAG Routine
   CASE DASBRW::13:TAGDISPSTATUS
   OF 0
      DASBRW::13:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::13:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::13:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Updateuser_levels',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_all_id',save_all_id,'Updateuser_levels',1)
    SolaceViewVars('CurrentTab',CurrentTab,'Updateuser_levels',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Updateuser_levels',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Updateuser_levels',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Updateuser_levels',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Updateuser_levels',1)
    SolaceViewVars('areas_tag',areas_tag,'Updateuser_levels',1)
    SolaceViewVars('level_tag',level_tag,'Updateuser_levels',1)
    SolaceViewVars('AvailableQueue:AccessLevel',AvailableQueue:AccessLevel,'Updateuser_levels',1)
    SolaceViewVars('tmp:Show',tmp:Show,'Updateuser_levels',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lev:user_level:Prompt;  SolaceCtrlName = '?lev:user_level:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lev:User_Level;  SolaceCtrlName = '?lev:User_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ALL:Description:Prompt;  SolaceCtrlName = '?ALL:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?all:Description;  SolaceCtrlName = '?all:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab:2;  SolaceCtrlName = '?CurrentTab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Add_Areas;  SolaceCtrlName = '?Add_Areas';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Remove_Areas;  SolaceCtrlName = '?Remove_Areas';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting An Access Level'
  OF ChangeRecord
    ActionMessage = 'Changing An Access Level'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Updateuser_levels')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Updateuser_levels')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?lev:user_level:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(lev:Record,History::lev:Record)
  SELF.AddHistoryField(?lev:User_Level,1)
  SELF.AddUpdateFile(Access:USELEVEL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:USELEVEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW5.Init(?List,Queue:Browse.ViewPosition,BRW5::View:Browse,Queue:Browse,Relate:ACCAREAS,SELF)
  BRW6.Init(?List:2,Queue:Browse:1.ViewPosition,BRW6::View:Browse,Queue:Browse:1,Relate:ALLLEVEL,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
      Unhide(?Insert)
      Unhide(?Change)
      Unhide(?Delete)
  End
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW5.Q &= Queue:Browse
  BRW5.RetainRow = 0
  BRW5.AddSortOrder(,acc:Access_level_key)
  BRW5.AddRange(acc:User_Level,lev:User_Level)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,acc:Access_Area,1,BRW5)
  BIND('areas_tag',areas_tag)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW5.AddField(areas_tag,BRW5.Q.areas_tag)
  BRW5.AddField(acc:Access_Area,BRW5.Q.acc:Access_Area)
  BRW5.AddField(acc:User_Level,BRW5.Q.acc:User_Level)
  BRW6.Q &= Queue:Browse:1
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,all:Access_Level_Key)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,all:Access_Level,1,BRW6)
  BIND('level_tag',level_tag)
  BIND('tmp:Show',tmp:Show)
  ?List:2{PROP:IconList,1} = '~notick1.ico'
  ?List:2{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(level_tag,BRW6.Q.level_tag)
  BRW6.AddField(all:Access_Level,BRW6.Q.all:Access_Level)
  BRW6.AddField(tmp:Show,BRW6.Q.tmp:Show)
  BRW6.AddField(all:Description,BRW6.Q.all:Description)
  BRW6.AskProcedure = 1
  BRW5.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_AccessAreas)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Q_AccessLevel)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  ?List:2{Prop:Alrt,239} = SpaceKey
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab2{PROP:TEXT} = 'All Access Area'
    ?List:2{PROP:FORMAT} ='10L(2)*I@s1@#1#100L(2)|*~Access Level~@s40@#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Q_AccessAreas)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Q_AccessLevel)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Updateuser_levels',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 0
  If ?insert{prop:visible} = 1
      do_update# = 1
  End!If password = 'BRX' or password = 'JOB:ENTER'
  If do_update# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_All_Levels
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?Add_Areas
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Areas, Accepted)
      If lev:user_level = ''
          Case MessageEx('You must insert an Access Level before you can continue.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If lev:user_level = ''
      
      
          Case MessageEx('This will add all the Tagged Access Levels to this User Level.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
          
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
          
                  recordstoprocess    = Records(glo:Q_AccessLevel)
          
                  Loop x$ = 1 To Records(glo:Q_AccessLevel)
                      Get(glo:Q_AccessLevel,x$)
                      Do GetNextRecord2
                      acc:user_level  = lev:user_level
                      acc:access_area = glo:access_level_pointer
                      If access:accareas.tryinsert()
                          Cycle
                      End
                  End
          
          
                  setcursor()
                  close(progresswindow)
                  Do DASBRW::12:DASUNTAGALL
          
              Of 2 ! &No Button
          End!Case MessageEx
      End!If lev:user_level = ''  
      BRW5.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Areas, Accepted)
    OF ?Remove_Areas
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Areas, Accepted)
      Case MessageEx('This will remove all the Tagged Access Areas from this User Level.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              recordspercycle     = 25
              recordsprocessed    = 0
              percentprogress     = 0
              setcursor(cursor:wait)
              open(progresswindow)
              progress:thermometer    = 0
              ?progress:pcttext{prop:text} = '0% Completed'
          
              recordstoprocess    = Records(glo:Q_AccessAreas)
          
              Loop x$ = 1 To Records(glo:Q_AccessAreas)
                  Get(glo:Q_AccessAreas,x$)
                  Do GetNextRecord2
                  
                  save_acc_id = access:accareas.savefile()
                  access:accareas.clearkey(acc:access_level_key)
                  acc:user_level  = lev:user_level
                  acc:access_area = glo:access_areas_pointer
                  set(acc:access_level_key,acc:access_level_key)
                  loop
                      if access:accareas.next()
                         break
                      end !if
                      if acc:user_level  <> lev:user_level       |
                      or acc:access_area <> glo:access_areas_pointer      |
                          then break.  ! end if
                      Delete(accareas)
                  end !loop
                  access:accareas.restorefile(save_acc_id)
              End
              setcursor()
              close(progresswindow)
      
          Of 2 ! &No Button
      End!Case MessageEx
      BRW5.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Remove_Areas, Accepted)
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::12:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::13:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Updateuser_levels')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  OF ?List:2
    CASE EVENT()
    OF EVENT:PreAlertKey
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG)
         CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF Keycode() = SpaceKey
         POST(EVENT:Accepted,?DASTAG:2)
         CYCLE
      END
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::12:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?Sheet2
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet2)
        OF 1
          ?List:2{PROP:FORMAT} ='10L(2)*I@s1@#1#100L(2)|*~Access Level~@s40@#7#'
          ?Tab2{PROP:TEXT} = 'All Access Area'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::13:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::12:DASUNTAGALL
      Do DASBRW::13:DASUNTAGALL
      ?List:2{prop:Filter} = 'tmp:Show = 1'
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_AccessAreas.Access_Areas_Pointer = acc:Access_Area
     GET(GLO:Q_AccessAreas,GLO:Q_AccessAreas.Access_Areas_Pointer)
    IF ERRORCODE()
      areas_tag = ''
    ELSE
      areas_tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (areas_tag = '*')
    SELF.Q.areas_tag_Icon = 2
  ELSE
    SELF.Q.areas_tag_Icon = 1
  END


BRW5.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_AccessAreas.Access_Areas_Pointer = acc:Access_Area
     GET(GLO:Q_AccessAreas,GLO:Q_AccessAreas.Access_Areas_Pointer)
    EXECUTE DASBRW::12:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW6.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_AccessLevel.Access_Level_Pointer = all:Access_Level
     GET(GLO:Q_AccessLevel,GLO:Q_AccessLevel.Access_Level_Pointer)
    IF ERRORCODE()
      level_tag = ''
    ELSE
      level_tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  Access:ACCAREAS.ClearKey(acc:Access_level_key)
  acc:User_Level  = lev:User_Level
  acc:Access_Area = all:Access_Level
  If Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign
      !Found
      tmp:Show = 0
  Else!If Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      tmp:Show = 1
  End!If Access:ACCAREAS.TryFetch(acc:Access_level_key) = Level:Benign
  PARENT.SetQueueRecord
  SELF.Q.level_tag_NormalFG = -1
  SELF.Q.level_tag_NormalBG = -1
  SELF.Q.level_tag_SelectedFG = -1
  SELF.Q.level_tag_SelectedBG = -1
  IF (level_tag = '*')
    SELF.Q.level_tag_Icon = 2
  ELSE
    SELF.Q.level_tag_Icon = 1
  END
  SELF.Q.all:Access_Level_NormalFG = -1
  SELF.Q.all:Access_Level_NormalBG = -1
  SELF.Q.all:Access_Level_SelectedFG = -1
  SELF.Q.all:Access_Level_SelectedBG = -1
  SELF.Q.tmp:Show_NormalFG = -1
  SELF.Q.tmp:Show_NormalBG = -1
  SELF.Q.tmp:Show_SelectedFG = -1
  SELF.Q.tmp:Show_SelectedBG = -1
   
   
   IF (tmp:Show = 0)
     SELF.Q.level_tag_NormalFG = 8421504
     SELF.Q.level_tag_NormalBG = 16777215
     SELF.Q.level_tag_SelectedFG = 16777215
     SELF.Q.level_tag_SelectedBG = 8421504
   ELSE
     SELF.Q.level_tag_NormalFG = 255
     SELF.Q.level_tag_NormalBG = 16777215
     SELF.Q.level_tag_SelectedFG = 16777215
     SELF.Q.level_tag_SelectedBG = 255
   END
   IF (tmp:Show = 0)
     SELF.Q.all:Access_Level_NormalFG = 8421504
     SELF.Q.all:Access_Level_NormalBG = 16777215
     SELF.Q.all:Access_Level_SelectedFG = 16777215
     SELF.Q.all:Access_Level_SelectedBG = 8421504
   ELSE
     SELF.Q.all:Access_Level_NormalFG = 255
     SELF.Q.all:Access_Level_NormalBG = 16777215
     SELF.Q.all:Access_Level_SelectedFG = 16777215
     SELF.Q.all:Access_Level_SelectedBG = 255
   END
   IF (tmp:Show = 0)
     SELF.Q.tmp:Show_NormalFG = 8421504
     SELF.Q.tmp:Show_NormalBG = 16777215
     SELF.Q.tmp:Show_SelectedFG = 16777215
     SELF.Q.tmp:Show_SelectedBG = 8421504
   ELSE
     SELF.Q.tmp:Show_NormalFG = 255
     SELF.Q.tmp:Show_NormalBG = 16777215
     SELF.Q.tmp:Show_SelectedFG = 16777215
     SELF.Q.tmp:Show_SelectedBG = 255
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())


BRW6.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  IF Keycode() = SpaceKey
    RETURN ReturnValue
  END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Q_AccessLevel.Access_Level_Pointer = all:Access_Level
     GET(GLO:Q_AccessLevel,GLO:Q_AccessLevel.Access_Level_Pointer)
    EXECUTE DASBRW::13:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue

