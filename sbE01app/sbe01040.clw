

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBE01040.INC'),ONCE        !Local module procedure declarations
                     END


Add_Access_Routine   PROCEDURE  (f_field_name)        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Add_Access_Routine')      !Add Procedure to Log
  end


   Relate:ACCAREAS.Open
! Check Access
    Case MessageEx('The security area name for this field is:<13,10><13,10>'&Clip(f_field_name)&'<13,10><13,10>Do you with to add this to the security levels?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
    GlobalRequest = SelectRecord                      ! Set Action for Lookup
    Pick_Access_Level                                 ! Call the Lookup Procedure
    GlobalResponse = RequestCancelled                 ! Clear Result
            If Records(glo:Q_AccessLevel)
                Loop x# = 1 To Records(glo:Q_AccessLevel)
                    Get(glo:Q_AccessLevel,x#)
                    clear(acc:record, -1)
                    acc:user_level  = glo:access_level_pointer
                    acc:access_area = f_field_name
                    add(accareas)
                end !loop
            End
        Of 2 ! &No Button
    End!Case MessageEx
   Relate:ACCAREAS.Close


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Add_Access_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
