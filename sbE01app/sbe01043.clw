

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01043.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Users_Job_Assignment PROCEDURE                 !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
tmp:engineer         STRING('ENGINEER {22}')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:Team)
                       PROJECT(use:SkillLevel)
                       PROJECT(use:User_Code)
                       PROJECT(use:Active)
                       PROJECT(use:User_Type)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:Team               LIKE(use:Team)                 !List box control field - type derived from field
use:SkillLevel         LIKE(use:SkillLevel)           !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !List box control field - type derived from field
use:Active             LIKE(use:Active)               !Browse key field - type derived from field
use:User_Type          LIKE(use:User_Type)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK10::use:Active          LIKE(use:Active)
HK10::use:User_Type       LIKE(use:User_Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Select An Engineer'),AT(,,474,227),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Users_Job_Assignment'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,372,184),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('124L(2)|M~Surname~@s30@75L(2)|M~Forename~@s30@120L(2)|M~Team~@s30@118L(2)|M~Skil' &|
   'l Level~@s8@41L(2)|M~User Code~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(392,20,76,20),USE(?Select:2),LEFT,ICON('select.ico'),DEFAULT
                       SHEET,AT(4,4,380,220),USE(?CurrentTab),SPREAD
                         TAB('By Surname'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(use:Surname),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By User Code'),USE(?Tab2)
                           ENTRY(@s3),AT(8,20,124,10),USE(use:User_Code),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('Close'),AT(392,204,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?use:Surname{prop:ReadOnly} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 15066597
    Elsif ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 8454143
    Else ! If ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 16777215
    End ! If ?use:Surname{prop:Req} = True
    ?use:Surname{prop:Trn} = 0
    ?use:Surname{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?use:User_Code{prop:ReadOnly} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 15066597
    Elsif ?use:User_Code{prop:Req} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 8454143
    Else ! If ?use:User_Code{prop:Req} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 16777215
    End ! If ?use:User_Code{prop:Req} = True
    ?use:User_Code{prop:Trn} = 0
    ?use:User_Code{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Users_Job_Assignment',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Users_Job_Assignment',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Users_Job_Assignment',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Users_Job_Assignment',1)
    SolaceViewVars('tmp:engineer',tmp:engineer,'Browse_Users_Job_Assignment',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Surname;  SolaceCtrlName = '?use:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:User_Code;  SolaceCtrlName = '?use:User_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Users_Job_Assignment')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Users_Job_Assignment')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:USERS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:USERS,SELF)
  Brw1.SelectWholeRecord=True
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,use:User_Code_Active_Key)
  BRW1.AddRange(use:Active)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?use:User_Code,use:User_Code,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,use:User_Type_Active_Key)
  BRW1.AddRange(use:Active)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?USE:Surname,use:Surname,1,BRW1)
  BRW1.AddField(use:Surname,BRW1.Q.use:Surname)
  BRW1.AddField(use:Forename,BRW1.Q.use:Forename)
  BRW1.AddField(use:Team,BRW1.Q.use:Team)
  BRW1.AddField(use:SkillLevel,BRW1.Q.use:SkillLevel)
  BRW1.AddField(use:User_Code,BRW1.Q.use:User_Code)
  BRW1.AddField(use:Active,BRW1.Q.use:Active)
  BRW1.AddField(use:User_Type,BRW1.Q.use:User_Type)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Surname'
    ?Tab2{PROP:TEXT} = 'By User Code'
    ?Browse:1{PROP:FORMAT} ='124L(2)|M~Surname~@s30@#1#75L(2)|M~Forename~@s30@#2#120L(2)|M~Team~@s30@#3#118L(2)|M~Skill Level~@s8@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:USERS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Users_Job_Assignment',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Users_Job_Assignment')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Surname~@s30@#1#75L(2)|M~Forename~@s30@#2#120L(2)|M~Team~@s30@#3#118L(2)|M~Skill Level~@s8@#4#'
          ?Tab:2{PROP:TEXT} = 'By Surname'
        OF 2
          ?Browse:1{PROP:FORMAT} ='41L(2)|M~User Code~@s3@#5#124L(2)|M~Surname~@s30@#1#75L(2)|M~Forename~@s30@#2#120L(2)|M~Team~@s30@#3#'
          ?Tab2{PROP:TEXT} = 'By User Code'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?use:Surname
      ! Before Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?use:Surname, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlPreEventHandling) DESC(Legacy: Control Event Handling, before generated code) ARG(?use:Surname, Selected)
    OF ?use:User_Code
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = 'YES'
  ELSE
     GET(SELF.Order.RangeList.List,1)
     Self.Order.RangeList.List.Right = tmp:engineer
     GET(SELF.Order.RangeList.List,2)
     Self.Order.RangeList.List.Right = 'YES'
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  ReturnValue = PARENT.ValidateRecord()
  ! Start Change 4913 BE(28/10/2004)
  IF (use:user_type <> tmp:Engineer) THEN
      ReturnValue = Record:Filtered
  END
  ! End Change 4913 BE(28/10/2004)
  BRW1::RecordStatus=ReturnValue
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ValidateRecord, (),BYTE)
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

