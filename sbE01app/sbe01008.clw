

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01008.INC'),ONCE        !Local module procedure declarations
                     END


UpdateUNITTYPE PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::uni:Record  LIKE(uni:RECORD),STATIC
QuickWindow          WINDOW('Update the UNITTYPE File'),AT(,,220,87),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateUNITTYPE'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,52),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Unit Type'),AT(8,20),USE(?UNI:Unit_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(uni:Unit_Type),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           CHECK('Active'),AT(84,36),USE(uni:Active),MSG('Active'),TIP('Active'),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,60,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,64,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,64,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?UNI:Unit_Type:Prompt{prop:FontColor} = -1
    ?UNI:Unit_Type:Prompt{prop:Color} = 15066597
    If ?uni:Unit_Type{prop:ReadOnly} = True
        ?uni:Unit_Type{prop:FontColor} = 65793
        ?uni:Unit_Type{prop:Color} = 15066597
    Elsif ?uni:Unit_Type{prop:Req} = True
        ?uni:Unit_Type{prop:FontColor} = 65793
        ?uni:Unit_Type{prop:Color} = 8454143
    Else ! If ?uni:Unit_Type{prop:Req} = True
        ?uni:Unit_Type{prop:FontColor} = 65793
        ?uni:Unit_Type{prop:Color} = 16777215
    End ! If ?uni:Unit_Type{prop:Req} = True
    ?uni:Unit_Type{prop:Trn} = 0
    ?uni:Unit_Type{prop:FontStyle} = font:Bold
    ?uni:Active{prop:Font,3} = -1
    ?uni:Active{prop:Color} = 15066597
    ?uni:Active{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateUNITTYPE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateUNITTYPE',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateUNITTYPE',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateUNITTYPE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateUNITTYPE',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateUNITTYPE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UNI:Unit_Type:Prompt;  SolaceCtrlName = '?UNI:Unit_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?uni:Unit_Type;  SolaceCtrlName = '?uni:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?uni:Active;  SolaceCtrlName = '?uni:Active';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Unit Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Unit Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateUNITTYPE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateUNITTYPE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?UNI:Unit_Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(uni:Record,History::uni:Record)
  SELF.AddHistoryField(?uni:Unit_Type,1)
  SELF.AddHistoryField(?uni:Active,2)
  SELF.AddUpdateFile(Access:UNITTYPE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:UNITTYPE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:UNITTYPE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:UNITTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateUNITTYPE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateUNITTYPE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

