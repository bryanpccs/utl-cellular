

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01049.INC'),ONCE        !Local module procedure declarations
                     END


UpdateNOTESFAU PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::nof:Record  LIKE(nof:RECORD),STATIC
QuickWindow          WINDOW('Update the NOTESFAU File'),AT(,,292,104),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateNOTESFAU'),TILED,SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,284,68),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?NOF:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(nof:Manufacturer),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Reference'),AT(7,35),USE(?NOF:Reference:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(nof:Reference),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Notes'),AT(8,52),USE(?NOF:Notes:Prompt),TRN
                           ENTRY(@s80),AT(84,52,200,10),USE(nof:Notes),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('&OK'),AT(172,80,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(228,80,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,76,284,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?NOF:Manufacturer:Prompt{prop:FontColor} = -1
    ?NOF:Manufacturer:Prompt{prop:Color} = 15066597
    If ?nof:Manufacturer{prop:ReadOnly} = True
        ?nof:Manufacturer{prop:FontColor} = 65793
        ?nof:Manufacturer{prop:Color} = 15066597
    Elsif ?nof:Manufacturer{prop:Req} = True
        ?nof:Manufacturer{prop:FontColor} = 65793
        ?nof:Manufacturer{prop:Color} = 8454143
    Else ! If ?nof:Manufacturer{prop:Req} = True
        ?nof:Manufacturer{prop:FontColor} = 65793
        ?nof:Manufacturer{prop:Color} = 16777215
    End ! If ?nof:Manufacturer{prop:Req} = True
    ?nof:Manufacturer{prop:Trn} = 0
    ?nof:Manufacturer{prop:FontStyle} = font:Bold
    ?NOF:Reference:Prompt{prop:FontColor} = -1
    ?NOF:Reference:Prompt{prop:Color} = 15066597
    If ?nof:Reference{prop:ReadOnly} = True
        ?nof:Reference{prop:FontColor} = 65793
        ?nof:Reference{prop:Color} = 15066597
    Elsif ?nof:Reference{prop:Req} = True
        ?nof:Reference{prop:FontColor} = 65793
        ?nof:Reference{prop:Color} = 8454143
    Else ! If ?nof:Reference{prop:Req} = True
        ?nof:Reference{prop:FontColor} = 65793
        ?nof:Reference{prop:Color} = 16777215
    End ! If ?nof:Reference{prop:Req} = True
    ?nof:Reference{prop:Trn} = 0
    ?nof:Reference{prop:FontStyle} = font:Bold
    ?NOF:Notes:Prompt{prop:FontColor} = -1
    ?NOF:Notes:Prompt{prop:Color} = 15066597
    If ?nof:Notes{prop:ReadOnly} = True
        ?nof:Notes{prop:FontColor} = 65793
        ?nof:Notes{prop:Color} = 15066597
    Elsif ?nof:Notes{prop:Req} = True
        ?nof:Notes{prop:FontColor} = 65793
        ?nof:Notes{prop:Color} = 8454143
    Else ! If ?nof:Notes{prop:Req} = True
        ?nof:Notes{prop:FontColor} = 65793
        ?nof:Notes{prop:Color} = 16777215
    End ! If ?nof:Notes{prop:Req} = True
    ?nof:Notes{prop:Trn} = 0
    ?nof:Notes{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateNOTESFAU',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateNOTESFAU',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateNOTESFAU',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateNOTESFAU',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NOF:Manufacturer:Prompt;  SolaceCtrlName = '?NOF:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?nof:Manufacturer;  SolaceCtrlName = '?nof:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NOF:Reference:Prompt;  SolaceCtrlName = '?NOF:Reference:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?nof:Reference;  SolaceCtrlName = '?nof:Reference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NOF:Notes:Prompt;  SolaceCtrlName = '?NOF:Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?nof:Notes;  SolaceCtrlName = '?nof:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Fault Description Text'
  OF ChangeRecord
    ActionMessage = 'Changing A Fault Description Text'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateNOTESFAU')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateNOTESFAU')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?NOF:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(nof:Record,History::nof:Record)
  SELF.AddHistoryField(?nof:Manufacturer,1)
  SELF.AddHistoryField(?nof:Reference,2)
  SELF.AddHistoryField(?nof:Notes,3)
  SELF.AddUpdateFile(Access:NOTESFAU)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:NOTESFAU.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:NOTESFAU
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:NOTESFAU.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateNOTESFAU',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateNOTESFAU')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

