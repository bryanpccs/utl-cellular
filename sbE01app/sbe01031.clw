

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01031.INC'),ONCE        !Local module procedure declarations
                     END


Pick_Repair_Type PROCEDURE                            !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::5:TAGFLAG          BYTE(0)
DASBRW::5:TAGMOUSE         BYTE(0)
DASBRW::5:TAGDISPSTATUS    BYTE(0)
DASBRW::5:QUEUE           QUEUE
Repair_Type_Pointer           LIKE(GLO:Repair_Type_Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
FilesOpened          BYTE
Repair_Type_Tag      STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(REPAIRTY)
                       PROJECT(rep:Repair_Type)
                       PROJECT(rep:Manufacturer)
                       PROJECT(rep:Model_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
Repair_Type_Tag        LIKE(Repair_Type_Tag)          !List box control field - type derived from local data
Repair_Type_Tag_Icon   LONG                           !Entry's icon ID
rep:Repair_Type        LIKE(rep:Repair_Type)          !List box control field - type derived from field
rep:Manufacturer       LIKE(rep:Manufacturer)         !Primary key field - type derived from field
rep:Model_Number       LIKE(rep:Model_Number)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the REPAIRTY File'),AT(,,252,208),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Pick_Repair_Type'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,148,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('12L(2)J@s1@80L(2)|M~Repair Type~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('&Rev tags'),AT(180,100,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(180,84,70,13),USE(?DASSHOWTAG),HIDE
                       SHEET,AT(4,4,156,200),USE(?CurrentTab),SPREAD
                         TAB('By Repair Type'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(rep:Repair_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('&Tag'),AT(8,184,48,16),USE(?DASTAG),LEFT,ICON('tag.gif')
                           BUTTON('Tag All'),AT(56,184,48,16),USE(?DASTAGAll),LEFT,ICON('tagall.gif')
                           BUTTON('Untag All'),AT(104,184,48,16),USE(?DASUNTAGALL),LEFT,ICON('untag.gif')
                         END
                       END
                       BUTTON('Select'),AT(168,20,76,20),USE(?Select),LEFT,ICON('select.ico'),STD(STD:Close)
                       BUTTON('Close'),AT(168,184,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?rep:Repair_Type{prop:ReadOnly} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 15066597
    Elsif ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 8454143
    Else ! If ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 16777215
    End ! If ?rep:Repair_Type{prop:Req} = True
    ?rep:Repair_Type{prop:Trn} = 0
    ?rep:Repair_Type{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::5:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?Browse:1))
  BRW1.UpdateBuffer
   GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
   GET(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
  IF ERRORCODE()
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     ADD(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    Repair_Type_Tag = '*'
  ELSE
    DELETE(GLO:q_RepairType)
    Repair_Type_Tag = ''
  END
    Queue:Browse:1.Repair_Type_Tag = Repair_Type_Tag
  IF (repair_type_tag = '*')
    Queue:Browse:1.Repair_Type_Tag_Icon = 2
  ELSE
    Queue:Browse:1.Repair_Type_Tag_Icon = 1
  END
  PUT(Queue:Browse:1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW1.Reset
  FREE(GLO:q_RepairType)
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     ADD(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASUNTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:q_RepairType)
  BRW1.Reset
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASREVTAGALL Routine
  ?Browse:1{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::5:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:q_RepairType)
    GET(GLO:q_RepairType,QR#)
    DASBRW::5:QUEUE = GLO:q_RepairType
    ADD(DASBRW::5:QUEUE)
  END
  FREE(GLO:q_RepairType)
  BRW1.Reset
  LOOP
    NEXT(BRW1::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::5:QUEUE.Repair_Type_Pointer = rep:Repair_Type
     GET(DASBRW::5:QUEUE,DASBRW::5:QUEUE.Repair_Type_Pointer)
    IF ERRORCODE()
       GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
       ADD(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    END
  END
  SETCURSOR
  BRW1.ResetSort(1)
  SELECT(?Browse:1,CHOICE(?Browse:1))
DASBRW::5:DASSHOWTAG Routine
   CASE DASBRW::5:TAGDISPSTATUS
   OF 0
      DASBRW::5:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::5:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::5:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW1.ResetSort(1)
   SELECT(?Browse:1,CHOICE(?Browse:1))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Pick_Repair_Type',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Pick_Repair_Type',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Pick_Repair_Type',1)
    SolaceViewVars('Repair_Type_Tag',Repair_Type_Tag,'Pick_Repair_Type',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rep:Repair_Type;  SolaceCtrlName = '?rep:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select;  SolaceCtrlName = '?Select';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Pick_Repair_Type')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Pick_Repair_Type')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:REPAIRTY.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:REPAIRTY,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,rep:Model_Number_Key)
  BRW1.AddRange(rep:Model_Number,GLO:Select2)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?rep:Repair_Type,rep:Repair_Type,1,BRW1)
  BIND('Repair_Type_Tag',Repair_Type_Tag)
  ?Browse:1{PROP:IconList,1} = '~notick1.ico'
  ?Browse:1{PROP:IconList,2} = '~tick1.ico'
  BRW1.AddField(Repair_Type_Tag,BRW1.Q.Repair_Type_Tag)
  BRW1.AddField(rep:Repair_Type,BRW1.Q.rep:Repair_Type)
  BRW1.AddField(rep:Manufacturer,BRW1.Q.rep:Manufacturer)
  BRW1.AddField(rep:Model_Number,BRW1.Q.rep:Model_Number)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:q_RepairType)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:REPAIRTY.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Pick_Repair_Type',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Close
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
      DO DASBRW::5:DASUNTAGALL
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Close, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::5:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Pick_Repair_Type')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Browse:1
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?Browse:1{PROPLIST:MouseDownRow} > 0) 
        CASE ?Browse:1{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::5:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?Browse:1{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      DO DASBRW::5:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     GET(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    IF ERRORCODE()
      Repair_Type_Tag = ''
    ELSE
      Repair_Type_Tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (repair_type_tag = '*')
    SELF.Q.Repair_Type_Tag_Icon = 2
  ELSE
    SELF.Q.Repair_Type_Tag_Icon = 1
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW1::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW1::RecordStatus=ReturnValue
  IF BRW1::RecordStatus NOT=Record:OK THEN RETURN BRW1::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:q_RepairType.Repair_Type_Pointer = rep:Repair_Type
     GET(GLO:q_RepairType,GLO:q_RepairType.Repair_Type_Pointer)
    EXECUTE DASBRW::5:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW1::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW1::RecordStatus
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

