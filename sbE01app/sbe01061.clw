

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01061.INC'),ONCE        !Local module procedure declarations
                     END


UpdateREPTYDEF PROCEDURE                              !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
save_sta_id          USHORT,AUTO
save_trc_id          USHORT,AUTO
pos                  STRING(255)
sav:RepairType       STRING(30)
save_job_id          USHORT,AUTO
save_suc_id          USHORT,AUTO
save_tuc_id          USHORT,AUTO
sav:Chargeable       STRING('''YES''')
sav:Warranty         STRING('''YES''')
save_man_id          USHORT,AUTO
save_mod_id          USHORT,AUTO
tmp:replicate        BYTE(0)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::rtd:Record  LIKE(rtd:RECORD),STATIC
QuickWindow          WINDOW('Update the REPTYDEF File'),AT(,,220,175),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateREPTYDEF'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,140),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Repair Type'),AT(8,20),USE(?RTD:Repair_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(rtd:Repair_Type),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK('Chargeable'),AT(84,36),USE(rtd:Chargeable),VALUE('YES','NO')
                           CHECK('Warranty'),AT(84,48),USE(rtd:Warranty),VALUE('YES','NO')
                           PROMPT('Warranty Code'),AT(8,64),USE(?RTD:WarrantyCode:Prompt)
                           ENTRY(@s5),AT(84,64,64,10),USE(rtd:WarrantyCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Code For Warranty (Sagem)'),TIP('Code For Warranty (Sagem)'),UPR
                           CHECK('Compulsory Fault Coding'),AT(84,80),USE(rtd:CompFaultCoding),MSG('Compulsory Fault Coding'),TIP('Compulsory Fault Coding'),VALUE('1','0')
                           CHECK('Exclude From EDI'),AT(84,96),USE(rtd:ExcludeFromEDI),MSG('Exclude From EDI'),TIP('Exclude From EDI'),VALUE('1','0')
                           CHECK('Exclude From Invoicing'),AT(84,112),USE(rtd:ExcludeFromInvoicing),MSG('Exclude From Invoicing'),TIP('Exclude From Invoicing'),VALUE('1','0')
                           CHECK('B.E.R. Repair Type'),AT(84,128),USE(rtd:BER),MSG('B.E.R.'),TIP('B.E.R.'),VALUE('1','0')
                         END
                       END
                       BUTTON('&OK'),AT(100,152,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,152,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,148,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Replicate'),AT(8,152,56,16),USE(?Replicate),HIDE,LEFT,ICON('CardStak.gif')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?RTD:Repair_Type:Prompt{prop:FontColor} = -1
    ?RTD:Repair_Type:Prompt{prop:Color} = 15066597
    If ?rtd:Repair_Type{prop:ReadOnly} = True
        ?rtd:Repair_Type{prop:FontColor} = 65793
        ?rtd:Repair_Type{prop:Color} = 15066597
    Elsif ?rtd:Repair_Type{prop:Req} = True
        ?rtd:Repair_Type{prop:FontColor} = 65793
        ?rtd:Repair_Type{prop:Color} = 8454143
    Else ! If ?rtd:Repair_Type{prop:Req} = True
        ?rtd:Repair_Type{prop:FontColor} = 65793
        ?rtd:Repair_Type{prop:Color} = 16777215
    End ! If ?rtd:Repair_Type{prop:Req} = True
    ?rtd:Repair_Type{prop:Trn} = 0
    ?rtd:Repair_Type{prop:FontStyle} = font:Bold
    ?rtd:Chargeable{prop:Font,3} = -1
    ?rtd:Chargeable{prop:Color} = 15066597
    ?rtd:Chargeable{prop:Trn} = 0
    ?rtd:Warranty{prop:Font,3} = -1
    ?rtd:Warranty{prop:Color} = 15066597
    ?rtd:Warranty{prop:Trn} = 0
    ?RTD:WarrantyCode:Prompt{prop:FontColor} = -1
    ?RTD:WarrantyCode:Prompt{prop:Color} = 15066597
    If ?rtd:WarrantyCode{prop:ReadOnly} = True
        ?rtd:WarrantyCode{prop:FontColor} = 65793
        ?rtd:WarrantyCode{prop:Color} = 15066597
    Elsif ?rtd:WarrantyCode{prop:Req} = True
        ?rtd:WarrantyCode{prop:FontColor} = 65793
        ?rtd:WarrantyCode{prop:Color} = 8454143
    Else ! If ?rtd:WarrantyCode{prop:Req} = True
        ?rtd:WarrantyCode{prop:FontColor} = 65793
        ?rtd:WarrantyCode{prop:Color} = 16777215
    End ! If ?rtd:WarrantyCode{prop:Req} = True
    ?rtd:WarrantyCode{prop:Trn} = 0
    ?rtd:WarrantyCode{prop:FontStyle} = font:Bold
    ?rtd:CompFaultCoding{prop:Font,3} = -1
    ?rtd:CompFaultCoding{prop:Color} = 15066597
    ?rtd:CompFaultCoding{prop:Trn} = 0
    ?rtd:ExcludeFromEDI{prop:Font,3} = -1
    ?rtd:ExcludeFromEDI{prop:Color} = 15066597
    ?rtd:ExcludeFromEDI{prop:Trn} = 0
    ?rtd:ExcludeFromInvoicing{prop:Font,3} = -1
    ?rtd:ExcludeFromInvoicing{prop:Color} = 15066597
    ?rtd:ExcludeFromInvoicing{prop:Trn} = 0
    ?rtd:BER{prop:Font,3} = -1
    ?rtd:BER{prop:Color} = 15066597
    ?rtd:BER{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateREPTYDEF',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateREPTYDEF',1)
    SolaceViewVars('save_sta_id',save_sta_id,'UpdateREPTYDEF',1)
    SolaceViewVars('save_trc_id',save_trc_id,'UpdateREPTYDEF',1)
    SolaceViewVars('pos',pos,'UpdateREPTYDEF',1)
    SolaceViewVars('sav:RepairType',sav:RepairType,'UpdateREPTYDEF',1)
    SolaceViewVars('save_job_id',save_job_id,'UpdateREPTYDEF',1)
    SolaceViewVars('save_suc_id',save_suc_id,'UpdateREPTYDEF',1)
    SolaceViewVars('save_tuc_id',save_tuc_id,'UpdateREPTYDEF',1)
    SolaceViewVars('sav:Chargeable',sav:Chargeable,'UpdateREPTYDEF',1)
    SolaceViewVars('sav:Warranty',sav:Warranty,'UpdateREPTYDEF',1)
    SolaceViewVars('save_man_id',save_man_id,'UpdateREPTYDEF',1)
    SolaceViewVars('save_mod_id',save_mod_id,'UpdateREPTYDEF',1)
    SolaceViewVars('tmp:replicate',tmp:replicate,'UpdateREPTYDEF',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateREPTYDEF',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateREPTYDEF',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RTD:Repair_Type:Prompt;  SolaceCtrlName = '?RTD:Repair_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:Repair_Type;  SolaceCtrlName = '?rtd:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:Chargeable;  SolaceCtrlName = '?rtd:Chargeable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:Warranty;  SolaceCtrlName = '?rtd:Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RTD:WarrantyCode:Prompt;  SolaceCtrlName = '?RTD:WarrantyCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:WarrantyCode;  SolaceCtrlName = '?rtd:WarrantyCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:CompFaultCoding;  SolaceCtrlName = '?rtd:CompFaultCoding';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:ExcludeFromEDI;  SolaceCtrlName = '?rtd:ExcludeFromEDI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:ExcludeFromInvoicing;  SolaceCtrlName = '?rtd:ExcludeFromInvoicing';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rtd:BER;  SolaceCtrlName = '?rtd:BER';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Replicate;  SolaceCtrlName = '?Replicate';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Repair Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Repair Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateREPTYDEF')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateREPTYDEF')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?RTD:Repair_Type:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rtd:Record,History::rtd:Record)
  SELF.AddHistoryField(?rtd:Repair_Type,1)
  SELF.AddHistoryField(?rtd:Chargeable,2)
  SELF.AddHistoryField(?rtd:Warranty,3)
  SELF.AddHistoryField(?rtd:WarrantyCode,4)
  SELF.AddHistoryField(?rtd:CompFaultCoding,5)
  SELF.AddHistoryField(?rtd:ExcludeFromEDI,6)
  SELF.AddHistoryField(?rtd:ExcludeFromInvoicing,7)
  SELF.AddHistoryField(?rtd:BER,8)
  SELF.AddUpdateFile(Access:REPTYDEF)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:JOBS.Open
  Relate:REPTYDEF.Open
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:REPAIRTY.UseFile
  Access:STDCHRGE.UseFile
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REPTYDEF
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?rtd:Warranty{Prop:Checked} = True
    ENABLE(?RTD:WarrantyCode)
  END
  IF ?rtd:Warranty{Prop:Checked} = False
    rtd:WarrantyCode = ''
    DISABLE(?RTD:WarrantyCode)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
    Relate:REPTYDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateREPTYDEF',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?rtd:Warranty
      IF ?rtd:Warranty{Prop:Checked} = True
        ENABLE(?RTD:WarrantyCode)
      END
      IF ?rtd:Warranty{Prop:Checked} = False
        rtd:WarrantyCode = ''
        DISABLE(?RTD:WarrantyCode)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?Replicate
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
      tmp:replicate   = 0
      Case MessageEx('Do you wish to attach this Repair Type to ALL Model Numbers?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              tmp:replicate   = 1
              Case MessageEx('This repair type will be replicated when you have <13,10>completed this screen.<13,10><13,10>(This message will close automatically)','ServiceBase 2000',|
                             'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,500) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If thiswindow.request <> Insertrecord
      If sav:RepairType  <> rtd:repair_Type Or sav:Chargeable  <> rtd:chargeable Or sav:Warranty    <> rtd:warranty
          Case MessageEx('You have changed this Repair Type. Do you want to replicate this change across all other occurances of this Repair Type for each Manufacturer?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
  
                  recordstoprocess    = Records(manufact) + Records(Jobs)
  
                  save_man_id = access:manufact.savefile()
                  set(man:manufacturer_key)
                  loop
                      if access:manufact.next()
                         break
                      end !if
  
                      Do GetNextRecord2
                      save_mod_id = access:modelnum.savefile()
                      access:modelnum.clearkey(mod:manufacturer_key)
                      mod:manufacturer = man:manufacturer
                      set(mod:manufacturer_key,mod:manufacturer_key)
                      loop
                          if access:modelnum.next()
                             break
                          end !if
                          if mod:manufacturer <> man:manufacturer      |
                              then break.  ! end if
                          access:repairty.clearkey(rep:repair_type_key)
                          rep:manufacturer = mod:manufacturer
                          rep:model_number = mod:model_number
                          rep:repair_type  = sav:RepairType
                          if access:repairty.tryfetch(rep:repair_type_key) = Level:Benign
                              rep:repair_Type = rtd:repair_Type
                              rep:chargeable  = rtd:chargeable
                              rep:warranty    = rtd:warranty
                              access:repairty.tryupdate()
                          End!if access:repairty.tryfetch(rep:repair_type_key) = Level:Benign
  
                          If sav:RepairType <> rtd:Repair_Type
  
                          
                              save_sta_id = access:stdchrge.savefile()
                              access:stdchrge.clearkey(sta:repair_type_key)
                              sta:model_number = mod:model_number
                              sta:repair_type  = sav:RepairType
                              set(sta:repair_type_key,sta:repair_type_key)
                              loop
                                  if access:stdchrge.next()
                                     break
                                  end !if
                                  if sta:model_number <> mod:model_number      |
                                  or sta:repair_type  <> sav:RepairType      |
                                      then break.  ! end if
                                  Pos = Position(sta:repair_type_key)
                                  sta:repair_type =rtd:repair_Type
                                  If access:stdchrge.tryupdate() = Level:benign
                                      Reset(sta:repair_type_key,pos)
                                  End !If access:stdchrge.tryupdate() = Level:benign
                                  
                              end !loop
                              access:stdchrge.restorefile(save_sta_id)
  
                              save_suc_id = access:subchrge.savefile()
                              access:subchrge.clearkey(suc:model_repair_key)
                              suc:model_number = mod:model_number
                              suc:repair_type  = sav:RepairType
                              set(suc:model_repair_key,suc:model_repair_key)
                              loop
                                  if access:subchrge.next()
                                     break
                                  end !if
                                  if suc:model_number <> mod:model_number      |
                                  or suc:repair_type  <> sav:RepairType      |
                                      then break.  ! end if
                                  Pos = Position(suc:model_repair_key)
                                  suc:repair_type = rtd:repair_Type
                                  If access:subchrge.tryupdate() = Level:Benign
                                      Reset(suc:model_repair_key,pos)
                                  End !If access:subchrge.tryupdate() = Level:Benign
                              end !loop
                              access:subchrge.restorefile(save_suc_id)
  
                              save_trc_id = access:trachrge.savefile()
                              access:trachrge.clearkey(trc:model_repair_key)
                              trc:model_number = mod:model_number
                              trc:repair_type  = sav:RepairType
                              set(trc:model_repair_key,trc:model_repair_key)
                              loop
                                  if access:trachrge.next()
                                     break
                                  end !if
                                  if trc:model_number <> mod:model_number      |
                                  or trc:repair_type  <> sav:RepairType      |
                                      then break.  ! end if
                                  Pos = Position(trc:model_repair_key)
                                  trc:repair_type = rtd:repair_Type
                                  If access:trachrge.tryupdate() = Level:Benign
                                      Reset(trc:model_repair_key,pos)
                                  End !If access:trachrge.tryupdate() = Level:Benign
                              end !loop
                              access:trachrge.restorefile(save_trc_id)
                          End !If sav:RepairType <> rts:Repair_Type
                      end !loop
                      access:modelnum.restorefile(save_mod_id)
  
                  end !loop
                  If sav:RepairType <> rtd:Repair_Type
  
                  
                      access:manufact.restorefile(save_man_id)
                      save_job_id = access:jobs.savefile()
                      access:jobs.clearkey(job:chareptypekey)
                      job:repair_type = sav:RepairType
                      set(job:chareptypekey,job:chareptypekey)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          if job:repair_type <> sav:RepairType      |
                              then break.  ! end if
                          Do GetNextRecord2
                          pos = Position(job:chareptypekey)
                          job:repair_type = rtd:repair_Type
                          If access:jobs.tryupdate() = Level:Benign
                              Reset(job:chareptypekey,pos)
                          End !If access:jobs.tryupdate() = Level:Benign
                      end !loop
                      access:jobs.restorefile(save_job_id)
  
                      save_job_id = access:jobs.savefile()
                      access:jobs.clearkey(job:warreptypekey)
                      job:repair_type_warranty = sav:repairType
                      set(job:warreptypekey,job:warreptypekey)
                      loop
                          if access:jobs.next()
                             break
                          end !if
                          if job:repair_type_warranty <> sav:RepairType      |
                              then break.  ! end if
                          Do GetNextRecord2
                          pos = Position(job:warreptypekey)
                          job:repair_type_warranty = rtd:repair_Type
                          If access:jobs.tryupdate() = Level:Benign
                              Reset(job:warreptypekey,pos)
                          End !If access:jobs.tryupdate() = Level:Benign
                      end !loop
                      access:jobs.restorefile(save_job_id)
                  End !If sav:RepairType <> rtd:Repair_Type
                  setcursor()
                  close(progresswindow)
  
              Of 2 ! &No Button
          End!Case MessageEx
      End
  End!If thiswindow.request <> Insertrecord
  ReturnValue = PARENT.TakeCompleted()
  If tmp:replicate = 1
  
      recordspercycle     = 25
      recordsprocessed    = 0
      percentprogress     = 0
      setcursor(cursor:wait)
      open(progresswindow)
      progress:thermometer    = 0
      ?progress:pcttext{prop:text} = '0% Completed'
  
      recordstoprocess    = Records(manufact)
  
      save_man_id = access:manufact.savefile()
      set(man:manufacturer_key)
      loop
          if access:manufact.next()
             break
          end !if
          Do GetNextRecord2
          save_mod_id = access:modelnum.savefile()
          access:modelnum.clearkey(mod:manufacturer_key)
          mod:manufacturer = man:manufacturer
          set(mod:manufacturer_key,mod:manufacturer_key)
          loop
              if access:modelnum.next()
                 break
              end !if
              if mod:manufacturer <> man:manufacturer      |
                  then break.  ! end if
              access:repairty.clearkey(rep:repair_type_key)
              rep:manufacturer = mod:manufacturer
              rep:model_number = mod:model_number
              rep:repair_type  = RTD:Repair_Type
              if access:repairty.tryfetch(rep:repair_type_key)
                  get(repairty,0)
                  if access:repairty.primerecord() = Level:benign
                      rep:manufacturer = mod:manufacturer
                      rep:model_number = mod:model_number
                      rep:repair_type  = rtd:repair_type
                      rep:chargeable   = rtd:chargeable
                      rep:warranty     = rtd:warranty
                      if access:repairty.insert()
                         access:repairty.cancelautoinc()
                      end
                  end!if access:repairty.primerecord() = Level:benign
              end
          end !loop
          access:modelnum.restorefile(save_mod_id)
  
      end !loop
      access:manufact.restorefile(save_man_id)
      setcursor()
      close(progresswindow)
  
  End!If tmp:replicate = 1
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateREPTYDEF')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      If thiswindow.request   = INsertrecord
          Unhide(?replicate)
      Else!If thiswindow.request   = INsertrecord
          sav:RepairType  = rtd:repair_Type
          sav:Chargeable  = rtd:chargeable
          sav:Warranty    = rtd:warranty
      End!If thiswindow.request   = INsertrecord
      Post(Event:Accepted,?rtd:WarrantyCode)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

