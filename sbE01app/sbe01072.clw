

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01072.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Default_Colours PROCEDURE                      !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
save_col_id          USHORT,AUTO
save_moc_id          USHORT,AUTO
save_mod_id          USHORT,AUTO
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(COLOUR)
                       PROJECT(col:Colour)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
col:Colour             LIKE(col:Colour)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Default Colour File'),AT(,,248,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Default_Colours'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,148,144),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('80L(2)|M~Colour~@s30@'),FROM(Queue:Browse:1)
                       BUTTON('Assign To All Models'),AT(168,44,76,20),USE(?AssignColours),LEFT,ICON('finger.gif')
                       BUTTON('&Select'),AT(168,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       BUTTON('&Insert'),AT(168,88,76,20),USE(?Insert:3),LEFT,ICON('insert.ico')
                       BUTTON('&Change'),AT(168,112,76,20),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(168,136,76,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                       SHEET,AT(4,4,156,180),USE(?CurrentTab),SPREAD
                         TAB('By Colour'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(col:Colour),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('Close'),AT(168,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?col:Colour{prop:ReadOnly} = True
        ?col:Colour{prop:FontColor} = 65793
        ?col:Colour{prop:Color} = 15066597
    Elsif ?col:Colour{prop:Req} = True
        ?col:Colour{prop:FontColor} = 65793
        ?col:Colour{prop:Color} = 8454143
    Else ! If ?col:Colour{prop:Req} = True
        ?col:Colour{prop:FontColor} = 65793
        ?col:Colour{prop:Color} = 16777215
    End ! If ?col:Colour{prop:Req} = True
    ?col:Colour{prop:Trn} = 0
    ?col:Colour{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Default_Colours',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Default_Colours',1)
    SolaceViewVars('save_col_id',save_col_id,'Browse_Default_Colours',1)
    SolaceViewVars('save_moc_id',save_moc_id,'Browse_Default_Colours',1)
    SolaceViewVars('save_mod_id',save_mod_id,'Browse_Default_Colours',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Default_Colours',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AssignColours;  SolaceCtrlName = '?AssignColours';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?col:Colour;  SolaceCtrlName = '?col:Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Default_Colours')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Default_Colours')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:COLOUR.Open
  Relate:MODELCOL.Open
  Access:MODELNUM.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:COLOUR,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  If Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      0{prop:Text} = 'Browse The Default ' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
      ?Tab:2{prop:Text} = 'By ' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
      ?Browse:1{prop:Format} = '80L(2)|M~' & Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & '~@s30@'
  End !Clip(GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,col:Colour_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?col:Colour,col:Colour,1,BRW1)
  BRW1.AddField(col:Colour,BRW1.Q.col:Colour)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COLOUR.Close
    Relate:MODELCOL.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Default_Colours',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateCOLOUR
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?AssignColours
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AssignColours, Accepted)
      colours" = 'Colours'
      If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
          colours"    = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI')) & 's'
      End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      
      
      Case MessageEx('If you continue ALL the ' & Clip(Colours") & ' currently assigned to Model Numbers will be REMOVED and replaced by the Default List.'&|
        '<13,10>'&|
        '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                     'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Case MessageEx('Replace all ' & Colours" & '.'&|
                '<13,10>'&|
                '<13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      !Loop through all the model numbers
                      !Removed the colours that are already attached.
                      !All the current default list.
                      !This is probably not the best of quickest,
                      !but this needs to be done in a rush.
      
                      recordspercycle         = 25
                      recordsprocessed        = 0
                      percentprogress         = 0
                      progress:thermometer    = 0
                      thiswindow.reset(1)
                      open(progresswindow)
      
                      ?progress:userstring{prop:text} = 'Running...'
                      ?progress:pcttext{prop:text} = '0% Completed'
      
                      recordstoprocess    = Records(MODELNUM)
      
                      Setcursor(Cursor:Wait)
                      Save_mod_ID = Access:MODELNUM.SaveFile()
                      Set(mod:Model_Number_Key)
                      Loop
                          If Access:MODELNUM.NEXT()
                             Break
                          End !If
                          Do GetNextRecord2
                          cancelcheck# += 1
                          If cancelcheck# > (RecordsToProcess/100)
                              Do cancelcheck
                              If tmp:cancel = 1
                                  Break
                              End!If tmp:cancel = 1
                              cancelcheck# = 0
                          End!If cancelcheck# > 50
      
                          Save_moc_ID = Access:MODELCOL.SaveFile()
                          Access:MODELCOL.ClearKey(moc:Colour_Key)
                          moc:Model_Number = mod:Model_Number
                          Set(moc:Colour_Key,moc:Colour_Key)
                          Loop
                              If Access:MODELCOL.NEXT()
                                 Break
                              End !If
                              If moc:Model_Number <> mod:Model_Number|
                                  Then Break.  ! End If
                              Delete(MODELCOL)
                          End !Loop
                          Access:MODELCOL.RestoreFile(Save_moc_ID)
      
                          Save_col_ID = Access:COLOUR.SaveFile()
                          Set(col:Colour_Key)
                          Loop
                              If Access:COLOUR.NEXT()
                                 Break
                              End !If
                              If Access:MODELCOL.PrimeRecord() = Level:Benign
                                  moc:Model_Number    = mod:Model_Number
                                  moc:Colour          = col:Colour
                                  If Access:MODELCOL.TryInsert() = Level:Benign
                                      !Insert Successful
                                  Else !If Access:MODELCOL.TryInsert() = Level:Benign
                                      !Insert Failed
                                  End !If Access:MODELCOL.TryInsert() = Level:Benign
                              End !If Access:MODELCOL.PrimeRecord() = Level:Benign
                          End !Loop
                          Access:COLOUR.RestoreFile(Save_col_ID)
                      End !Loop
                      Access:MODELNUM.RestoreFile(Save_mod_ID)
                      Setcursor()
      
                      Do EndPrintRun
                      close(progresswindow)
      
                  Of 2 ! &No Button
              End!Case MessageEx
          Of 2 ! &No Button
      End! Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?AssignColours, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Default_Colours')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?col:Colour
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?col:Colour, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?col:Colour, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

