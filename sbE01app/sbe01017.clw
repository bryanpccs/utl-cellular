

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01017.INC'),ONCE        !Local module procedure declarations
                     END


UpdateREPAIRTY PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::rep:Record  LIKE(rep:RECORD),STATIC
QuickWindow          WINDOW('Update the REPAIRTY File'),AT(,,220,131),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateREPAIRTY'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,96),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Manufacturer'),AT(8,20),USE(?REP:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(rep:Manufacturer),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Model Number'),AT(8,36),USE(?REP:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(rep:Model_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Repair Type'),AT(8,52),USE(?REP:Repair_Type:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(rep:Repair_Type),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           CHECK('Chargeable'),AT(84,68),USE(rep:Chargeable),VALUE('YES','NO')
                           CHECK('Warranty'),AT(84,84),USE(rep:Warranty),VALUE('YES','NO')
                         END
                       END
                       BUTTON('&OK'),AT(100,108,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(156,108,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,104,212,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?REP:Manufacturer:Prompt{prop:FontColor} = -1
    ?REP:Manufacturer:Prompt{prop:Color} = 15066597
    If ?rep:Manufacturer{prop:ReadOnly} = True
        ?rep:Manufacturer{prop:FontColor} = 65793
        ?rep:Manufacturer{prop:Color} = 15066597
    Elsif ?rep:Manufacturer{prop:Req} = True
        ?rep:Manufacturer{prop:FontColor} = 65793
        ?rep:Manufacturer{prop:Color} = 8454143
    Else ! If ?rep:Manufacturer{prop:Req} = True
        ?rep:Manufacturer{prop:FontColor} = 65793
        ?rep:Manufacturer{prop:Color} = 16777215
    End ! If ?rep:Manufacturer{prop:Req} = True
    ?rep:Manufacturer{prop:Trn} = 0
    ?rep:Manufacturer{prop:FontStyle} = font:Bold
    ?REP:Model_Number:Prompt{prop:FontColor} = -1
    ?REP:Model_Number:Prompt{prop:Color} = 15066597
    If ?rep:Model_Number{prop:ReadOnly} = True
        ?rep:Model_Number{prop:FontColor} = 65793
        ?rep:Model_Number{prop:Color} = 15066597
    Elsif ?rep:Model_Number{prop:Req} = True
        ?rep:Model_Number{prop:FontColor} = 65793
        ?rep:Model_Number{prop:Color} = 8454143
    Else ! If ?rep:Model_Number{prop:Req} = True
        ?rep:Model_Number{prop:FontColor} = 65793
        ?rep:Model_Number{prop:Color} = 16777215
    End ! If ?rep:Model_Number{prop:Req} = True
    ?rep:Model_Number{prop:Trn} = 0
    ?rep:Model_Number{prop:FontStyle} = font:Bold
    ?REP:Repair_Type:Prompt{prop:FontColor} = -1
    ?REP:Repair_Type:Prompt{prop:Color} = 15066597
    If ?rep:Repair_Type{prop:ReadOnly} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 15066597
    Elsif ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 8454143
    Else ! If ?rep:Repair_Type{prop:Req} = True
        ?rep:Repair_Type{prop:FontColor} = 65793
        ?rep:Repair_Type{prop:Color} = 16777215
    End ! If ?rep:Repair_Type{prop:Req} = True
    ?rep:Repair_Type{prop:Trn} = 0
    ?rep:Repair_Type{prop:FontStyle} = font:Bold
    ?rep:Chargeable{prop:Font,3} = -1
    ?rep:Chargeable{prop:Color} = 15066597
    ?rep:Chargeable{prop:Trn} = 0
    ?rep:Warranty{prop:Font,3} = -1
    ?rep:Warranty{prop:Color} = 15066597
    ?rep:Warranty{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateREPAIRTY',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateREPAIRTY',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateREPAIRTY',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateREPAIRTY',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateREPAIRTY',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateREPAIRTY',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?REP:Manufacturer:Prompt;  SolaceCtrlName = '?REP:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rep:Manufacturer;  SolaceCtrlName = '?rep:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?REP:Model_Number:Prompt;  SolaceCtrlName = '?REP:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rep:Model_Number;  SolaceCtrlName = '?rep:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?REP:Repair_Type:Prompt;  SolaceCtrlName = '?REP:Repair_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rep:Repair_Type;  SolaceCtrlName = '?rep:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rep:Chargeable;  SolaceCtrlName = '?rep:Chargeable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rep:Warranty;  SolaceCtrlName = '?rep:Warranty';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Repair Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Repair Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateREPAIRTY')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateREPAIRTY')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?REP:Manufacturer:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(rep:Record,History::rep:Record)
  SELF.AddHistoryField(?rep:Manufacturer,1)
  SELF.AddHistoryField(?rep:Model_Number,2)
  SELF.AddHistoryField(?rep:Repair_Type,3)
  SELF.AddHistoryField(?rep:Chargeable,4)
  SELF.AddHistoryField(?rep:Warranty,5)
  SELF.AddUpdateFile(Access:REPAIRTY)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:REPAIRTY.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:REPAIRTY
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:REPAIRTY.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateREPAIRTY',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    rep:Manufacturer = glo:select1
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateREPAIRTY')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

