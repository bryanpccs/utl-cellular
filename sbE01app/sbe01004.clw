

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABUTIL.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01004.INC'),ONCE        !Local module procedure declarations
                     END


UpdateCOURIER PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
DirRetCode           SHORT
DirDialogHeader      CSTRING(40)
DirTargetVariable    CSTRING(280)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::cou:Record  LIKE(cou:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Update the COURIER File'),AT(,,459,267),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateCOURIER'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,224,232),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Courier'),AT(8,20),USE(?COU:Courier:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(cou:Courier),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Account Number'),AT(8,36),USE(?COU:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,36,64,10),USE(cou:Account_Number),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Postcode'),AT(8,52),USE(?COU:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(84,52,64,10),USE(cou:Postcode),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Clear'),AT(152,52,56,10),USE(?Button3),SKIP,FLAT,LEFT,ICON(ICON:Cut)
                           PROMPT('Address'),AT(8,64),USE(?COU:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,64,124,10),USE(cou:Address_Line1),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,76,124,10),USE(cou:Address_Line2),FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s30),AT(84,88,124,10),USE(cou:Address_Line3),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Telephone Number'),AT(8,104),USE(?COU:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,104,64,10),USE(cou:Telephone_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Fax Number'),AT(8,120),USE(?COU:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,120,64,10),USE(cou:Fax_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Contact Name'),AT(8,136),USE(?COU:Contact_Name:Prompt),TRN
                           ENTRY(@s60),AT(84,136,124,10),USE(cou:Contact_Name),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Courier Type'),AT(8,152),USE(?COU:Courier_Type:Prompt)
                           LIST,AT(84,152,124,10),USE(cou:Courier_Type),VSCROLL,LEFT(2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),DROP(10,124),FROM('N/A|CITY LINK|LABEL G|BUSINESS POST|ANC|PARCELINE|PARCELINE LASERNET|ROYAL MAIL|SDS|TOTE|UPS|UPS ALTERNATIVE|TOTE PARCELINE')
                           CHECK('Despatch At Closing'),AT(84,168),USE(cou:DespatchClose),RIGHT,MSG('Despatch At Closing'),TIP('Despatch At Closing'),VALUE('YES','NO')
                           CHECK('Customer Collection'),AT(84,184),USE(cou:CustomerCollection),MSG('Customer Collection'),TIP('Customer Collection'),VALUE('1','0')
                           GROUP,AT(4,192,216,40),USE(?CustomerCollectionGroup),DISABLE
                             PROMPT('On Despatch Change Status To'),AT(8,196,72,16),USE(?cou:NewStatus:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                             ENTRY(@s30),AT(84,200,124,10),USE(cou:NewStatus),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('On Despatch Change Status To'),TIP('On Despatch Change Status To'),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                             BUTTON,AT(212,200,10,10),USE(?LookupStatus),SKIP,FONT('Tahoma',8,,,CHARSET:ANSI),ICON('List3.ico')
                             PROMPT('No Of Despatch Notes'),AT(8,216),USE(?cou:NoOfDespatchNotes:Prompt),TRN
                             SPIN(@n8),AT(84,216,64,10),USE(cou:NoOfDespatchNotes),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('No Of Despatch Notes'),TIP('No Of Despatch Notes'),UPR,RANGE(1,10),STEP(1)
                           END
                         END
                       END
                       SHEET,AT(232,4,224,232),USE(?Sheet2),BELOW,SPREAD
                         TAB('City'),USE(?CityLinkTab),HIDE
                           PROMPT('Export Path'),AT(236,20),USE(?cou:Export_Path:Prompt:2),TRN
                           ENTRY(@s255),AT(312,20,124,10),USE(cou:Export_Path,,?cou:Export_Path:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(440,20,10,10),USE(?LookupCityExportPath),SKIP,ICON('List3.ico')
                           PROMPT('Default Service'),AT(236,36),USE(?COU:Service:Prompt),TRN
                           ENTRY(@s15),AT(312,36,64,10),USE(cou:Service),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('ANC'),USE(?ANCTab),HIDE
                           PROMPT('Export Path'),AT(236,20),USE(?cou:Export_Path:Prompt:3),TRN
                           ENTRY(@s255),AT(312,20,124,10),USE(cou:Export_Path,,?cou:Export_Path:3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(440,20,10,10),USE(?LookupANCExportPath),SKIP,ICON('List3.ico')
                           PROMPT('Import Path'),AT(236,36),USE(?COU:Import_Path:Prompt),TRN
                           ENTRY(@s255),AT(312,36,124,10),USE(cou:Import_Path),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(440,36,10,10),USE(?LookupFile:3),ICON('list3.ico')
                           PROMPT('Last Despatch By'),AT(236,52),USE(?COU:Last_Despatch_Time:Prompt)
                           ENTRY(@t1),AT(312,52,64,10),USE(cou:Last_Despatch_Time),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('(HH:MM) (24hr)'),AT(384,52),USE(?Prompt16)
                           PROMPT('ICOL File Suffix'),AT(236,68),USE(?COU:ICOLSuffix:Prompt)
                           ENTRY(@s3),AT(312,68,64,10),USE(cou:ICOLSuffix),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('ANC ICOL File Suffix'),TIP('ANC ICOL File Suffix'),UPR
                           PROMPT('Contract Number'),AT(236,84),USE(?COU:ContractNumber:Prompt)
                           ENTRY(@s20),AT(312,84,124,10),USE(cou:ContractNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('ANC Contract Number'),TIP('ANC Contract Number'),UPR
                           PROMPT('ANC Label Path'),AT(236,100),USE(?COU:ANCPath:Prompt)
                           ENTRY(@s255),AT(312,100,124,10),USE(cou:ANCPath),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Path to ANCPAPER.EXE  (I.e. s:\apps\service)'),TIP('Path to ANCPAPER.EXE  (I.e. s:\apps\service)'),UPR
                           BUTTON,AT(440,100,10,10),USE(?LookupFile:4),ICON('list3.ico')
                         END
                         TAB('GenDef'),USE(?GeneralDefaults)
                           CHECK('Auto Allocate Consignment Number'),AT(236,20),USE(cou:AutoConsignmentNo),MSG('Auto Allocate Consignment Number'),TIP('Auto Allocate Consignment Number'),VALUE('1','0')
                           PROMPT('Last Allocated Consignment Number'),AT(236,32,76,16),USE(?cou:LastConsignmentNo:Prompt),TRN
                           ENTRY(@s8),AT(316,32,64,10),USE(cou:LastConsignmentNo),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Last Allocated Consignment Number'),TIP('Last Allocated Consignment Number'),UPR
                           CHECK('Print Label'),AT(236,52),USE(cou:PrintLabel),MSG('Print Label'),TIP('Print Label'),VALUE('1','0')
                         END
                         TAB('G'),USE(?LabelGTab),HIDE
                           PROMPT('Default Service'),AT(236,36),USE(?COU:Service:Prompt:2),TRN
                           ENTRY(@s15),AT(312,36,64,10),USE(cou:Service,,?COU:Service:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           PROMPT('Label G Command Options'),AT(236,48,72,16),USE(?COU:LabGOptions:Prompt)
                           ENTRY(@s60),AT(312,52,124,10),USE(cou:LabGOptions),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Label G Command Line Options'),TIP('Label G Command Line Options'),UPR
                           PROMPT('Last Despatch By'),AT(236,68),USE(?cou:Last_Despatch_Time:Prompt:5)
                           ENTRY(@t1),AT(312,68,64,10),USE(cou:Last_Despatch_Time,,?cou:Last_Despatch_Time:5),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('(HH:MM) (24hr)'),AT(380,68),USE(?Prompt16:5)
                           PROMPT('Export Path'),AT(236,20),USE(?COU:Export_Path:Prompt),TRN
                           ENTRY(@s255),AT(312,20,124,10),USE(cou:Export_Path),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(440,20,10,10),USE(?LookupFile),ICON('list3.ico')
                         END
                         TAB('RM'),USE(?RoyalMailTab)
                           PROMPT('Export Path'),AT(236,20),USE(?cou:Export_Path:Prompt:4),TRN
                           ENTRY(@s255),AT(312,20,124,10),USE(cou:Export_Path,,?cou:Export_Path:4),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(440,20,10,10),USE(?LookupRoyalExportPath),SKIP,ICON('List3.ico')
                           PROMPT('Import Path'),AT(236,36),USE(?cou:Import_Path:Prompt:2),TRN
                           ENTRY(@s255),AT(312,36,124,10),USE(cou:Import_Path,,?cou:Import_Path:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(440,36,10,10),USE(?LookupRoyalImportPath),SKIP,ICON('List3.ico')
                           PROMPT('Last Despatch By'),AT(235,52),USE(?cou:Last_Despatch_Time:Prompt:2)
                           ENTRY(@t1),AT(312,52,64,10),USE(cou:Last_Despatch_Time,,?cou:Last_Despatch_Time:2),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('(HH:MM) (24hr)'),AT(380,52),USE(?Prompt16:2)
                         END
                         TAB('PL'),USE(?ParcelLineTab)
                           PROMPT('Export Path'),AT(236,20),USE(?cou:Export_Path:Prompt:5),TRN
                           ENTRY(@s255),AT(312,20,124,10),USE(cou:Export_Path,,?cou:Export_Path:5),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(440,20,10,10),USE(?LookupParcelExportPath),SKIP,ICON('List3.ico')
                           PROMPT('Last Despatch By'),AT(236,36),USE(?cou:Last_Despatch_Time:Prompt:3)
                           ENTRY(@t1),AT(312,36,64,10),USE(cou:Last_Despatch_Time,,?cou:Last_Despatch_Time:3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('(HH:MM) (24hr)'),AT(380,36),USE(?Prompt16:3)
                         END
                         TAB('UPS'),USE(?UPSTab)
                           PROMPT('Export Path'),AT(236,20),USE(?cou:Export_Path:Prompt:6),TRN
                           ENTRY(@s255),AT(312,20,124,10),USE(cou:Export_Path,,?cou:Export_Path:6),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                           BUTTON,AT(440,20,10,10),USE(?LookupUPSExportPath),SKIP,ICON('List3.ico')
                           PROMPT('Last Despatch By'),AT(236,36),USE(?cou:Last_Despatch_Time:Prompt:4)
                           ENTRY(@t1),AT(312,36,64,10),USE(cou:Last_Despatch_Time,,?cou:Last_Despatch_Time:4),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('(HH:MM) (24hr)'),AT(380,36),USE(?Prompt16:4)
                         END
                         TAB('TOTE'),USE(?ToteTab)
                           PROMPT('Last Despatch By'),AT(236,20),USE(?cou:Last_Despatch_Time:Prompt:6)
                           ENTRY(@t1),AT(312,20,64,10),USE(cou:Last_Despatch_Time,,?cou:Last_Despatch_Time:6),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('(HH:MM) (24hr)'),AT(380,20),USE(?Prompt16:6)
                           CHECK('Print Label'),AT(239,39),USE(cou:PrintLabel,,?cou:PrintLabel:2),MSG('Print Label'),TIP('Print Label'),VALUE('1','0')
                         END
                         TAB('TPL'),USE(?ToteParcelineTab)
                           PROMPT('Last Despatch By'),AT(236,36),USE(?cou:Last_Despatch_Time:Prompt:7)
                           ENTRY(@t1),AT(312,36,64,10),USE(cou:Last_Despatch_Time,,?cou:Last_Despatch_Time:7),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('(HH:MM) (24hr)'),AT(380,36),USE(?Prompt16:7)
                           PROMPT('Default Service'),AT(236,52),USE(?COU:Service:Prompt:3),TRN
                           ENTRY(@s15),AT(312,52,64,10),USE(cou:Service,,?cou:Service:3),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           CHECK('Print Label'),AT(239,69),USE(cou:PrintLabel,,?cou:PrintLabel:3),MSG('Print Label'),TIP('Print Label'),VALUE('1','0')
                           BUTTON('...'),AT(440,20,10,10),USE(?LookupFile:2),LEFT,ICON('list3.ico')
                           PROMPT('Export Path'),AT(236,20),USE(?cou:Export_Path:Prompt:7),TRN
                           ENTRY(@s255),AT(312,20,124,10),USE(cou:Export_Path,,?cou:Export_Path:7),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       PANEL,AT(4,240,452,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(340,244,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(396,244,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FileLookup12         SelectFileClass
FileLookup11         SelectFileClass
FileLookup14         SelectFileClass
FileLookup18         SelectFileClass
FileLookup19         SelectFileClass
FileLookup20         SelectFileClass
FileLookup21         SelectFileClass
FileLookup22         SelectFileClass
FileLookup23         SelectFileClass
FileLookup10         SelectFileClass
!Save Entry Fields Incase Of Lookup
look:cou:NewStatus                Like(cou:NewStatus)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?COU:Courier:Prompt{prop:FontColor} = -1
    ?COU:Courier:Prompt{prop:Color} = 15066597
    If ?cou:Courier{prop:ReadOnly} = True
        ?cou:Courier{prop:FontColor} = 65793
        ?cou:Courier{prop:Color} = 15066597
    Elsif ?cou:Courier{prop:Req} = True
        ?cou:Courier{prop:FontColor} = 65793
        ?cou:Courier{prop:Color} = 8454143
    Else ! If ?cou:Courier{prop:Req} = True
        ?cou:Courier{prop:FontColor} = 65793
        ?cou:Courier{prop:Color} = 16777215
    End ! If ?cou:Courier{prop:Req} = True
    ?cou:Courier{prop:Trn} = 0
    ?cou:Courier{prop:FontStyle} = font:Bold
    ?COU:Account_Number:Prompt{prop:FontColor} = -1
    ?COU:Account_Number:Prompt{prop:Color} = 15066597
    If ?cou:Account_Number{prop:ReadOnly} = True
        ?cou:Account_Number{prop:FontColor} = 65793
        ?cou:Account_Number{prop:Color} = 15066597
    Elsif ?cou:Account_Number{prop:Req} = True
        ?cou:Account_Number{prop:FontColor} = 65793
        ?cou:Account_Number{prop:Color} = 8454143
    Else ! If ?cou:Account_Number{prop:Req} = True
        ?cou:Account_Number{prop:FontColor} = 65793
        ?cou:Account_Number{prop:Color} = 16777215
    End ! If ?cou:Account_Number{prop:Req} = True
    ?cou:Account_Number{prop:Trn} = 0
    ?cou:Account_Number{prop:FontStyle} = font:Bold
    ?COU:Postcode:Prompt{prop:FontColor} = -1
    ?COU:Postcode:Prompt{prop:Color} = 15066597
    If ?cou:Postcode{prop:ReadOnly} = True
        ?cou:Postcode{prop:FontColor} = 65793
        ?cou:Postcode{prop:Color} = 15066597
    Elsif ?cou:Postcode{prop:Req} = True
        ?cou:Postcode{prop:FontColor} = 65793
        ?cou:Postcode{prop:Color} = 8454143
    Else ! If ?cou:Postcode{prop:Req} = True
        ?cou:Postcode{prop:FontColor} = 65793
        ?cou:Postcode{prop:Color} = 16777215
    End ! If ?cou:Postcode{prop:Req} = True
    ?cou:Postcode{prop:Trn} = 0
    ?cou:Postcode{prop:FontStyle} = font:Bold
    ?COU:Address_Line1:Prompt{prop:FontColor} = -1
    ?COU:Address_Line1:Prompt{prop:Color} = 15066597
    If ?cou:Address_Line1{prop:ReadOnly} = True
        ?cou:Address_Line1{prop:FontColor} = 65793
        ?cou:Address_Line1{prop:Color} = 15066597
    Elsif ?cou:Address_Line1{prop:Req} = True
        ?cou:Address_Line1{prop:FontColor} = 65793
        ?cou:Address_Line1{prop:Color} = 8454143
    Else ! If ?cou:Address_Line1{prop:Req} = True
        ?cou:Address_Line1{prop:FontColor} = 65793
        ?cou:Address_Line1{prop:Color} = 16777215
    End ! If ?cou:Address_Line1{prop:Req} = True
    ?cou:Address_Line1{prop:Trn} = 0
    ?cou:Address_Line1{prop:FontStyle} = font:Bold
    If ?cou:Address_Line2{prop:ReadOnly} = True
        ?cou:Address_Line2{prop:FontColor} = 65793
        ?cou:Address_Line2{prop:Color} = 15066597
    Elsif ?cou:Address_Line2{prop:Req} = True
        ?cou:Address_Line2{prop:FontColor} = 65793
        ?cou:Address_Line2{prop:Color} = 8454143
    Else ! If ?cou:Address_Line2{prop:Req} = True
        ?cou:Address_Line2{prop:FontColor} = 65793
        ?cou:Address_Line2{prop:Color} = 16777215
    End ! If ?cou:Address_Line2{prop:Req} = True
    ?cou:Address_Line2{prop:Trn} = 0
    ?cou:Address_Line2{prop:FontStyle} = font:Bold
    If ?cou:Address_Line3{prop:ReadOnly} = True
        ?cou:Address_Line3{prop:FontColor} = 65793
        ?cou:Address_Line3{prop:Color} = 15066597
    Elsif ?cou:Address_Line3{prop:Req} = True
        ?cou:Address_Line3{prop:FontColor} = 65793
        ?cou:Address_Line3{prop:Color} = 8454143
    Else ! If ?cou:Address_Line3{prop:Req} = True
        ?cou:Address_Line3{prop:FontColor} = 65793
        ?cou:Address_Line3{prop:Color} = 16777215
    End ! If ?cou:Address_Line3{prop:Req} = True
    ?cou:Address_Line3{prop:Trn} = 0
    ?cou:Address_Line3{prop:FontStyle} = font:Bold
    ?COU:Telephone_Number:Prompt{prop:FontColor} = -1
    ?COU:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?cou:Telephone_Number{prop:ReadOnly} = True
        ?cou:Telephone_Number{prop:FontColor} = 65793
        ?cou:Telephone_Number{prop:Color} = 15066597
    Elsif ?cou:Telephone_Number{prop:Req} = True
        ?cou:Telephone_Number{prop:FontColor} = 65793
        ?cou:Telephone_Number{prop:Color} = 8454143
    Else ! If ?cou:Telephone_Number{prop:Req} = True
        ?cou:Telephone_Number{prop:FontColor} = 65793
        ?cou:Telephone_Number{prop:Color} = 16777215
    End ! If ?cou:Telephone_Number{prop:Req} = True
    ?cou:Telephone_Number{prop:Trn} = 0
    ?cou:Telephone_Number{prop:FontStyle} = font:Bold
    ?COU:Fax_Number:Prompt{prop:FontColor} = -1
    ?COU:Fax_Number:Prompt{prop:Color} = 15066597
    If ?cou:Fax_Number{prop:ReadOnly} = True
        ?cou:Fax_Number{prop:FontColor} = 65793
        ?cou:Fax_Number{prop:Color} = 15066597
    Elsif ?cou:Fax_Number{prop:Req} = True
        ?cou:Fax_Number{prop:FontColor} = 65793
        ?cou:Fax_Number{prop:Color} = 8454143
    Else ! If ?cou:Fax_Number{prop:Req} = True
        ?cou:Fax_Number{prop:FontColor} = 65793
        ?cou:Fax_Number{prop:Color} = 16777215
    End ! If ?cou:Fax_Number{prop:Req} = True
    ?cou:Fax_Number{prop:Trn} = 0
    ?cou:Fax_Number{prop:FontStyle} = font:Bold
    ?COU:Contact_Name:Prompt{prop:FontColor} = -1
    ?COU:Contact_Name:Prompt{prop:Color} = 15066597
    If ?cou:Contact_Name{prop:ReadOnly} = True
        ?cou:Contact_Name{prop:FontColor} = 65793
        ?cou:Contact_Name{prop:Color} = 15066597
    Elsif ?cou:Contact_Name{prop:Req} = True
        ?cou:Contact_Name{prop:FontColor} = 65793
        ?cou:Contact_Name{prop:Color} = 8454143
    Else ! If ?cou:Contact_Name{prop:Req} = True
        ?cou:Contact_Name{prop:FontColor} = 65793
        ?cou:Contact_Name{prop:Color} = 16777215
    End ! If ?cou:Contact_Name{prop:Req} = True
    ?cou:Contact_Name{prop:Trn} = 0
    ?cou:Contact_Name{prop:FontStyle} = font:Bold
    ?COU:Courier_Type:Prompt{prop:FontColor} = -1
    ?COU:Courier_Type:Prompt{prop:Color} = 15066597
    ?cou:Courier_Type{prop:FontColor} = 65793
    ?cou:Courier_Type{prop:Color}= 16777215
    ?cou:Courier_Type{prop:Color,2} = 16777215
    ?cou:Courier_Type{prop:Color,3} = 12937777
    ?cou:DespatchClose{prop:Font,3} = -1
    ?cou:DespatchClose{prop:Color} = 15066597
    ?cou:DespatchClose{prop:Trn} = 0
    ?cou:CustomerCollection{prop:Font,3} = -1
    ?cou:CustomerCollection{prop:Color} = 15066597
    ?cou:CustomerCollection{prop:Trn} = 0
    ?CustomerCollectionGroup{prop:Font,3} = -1
    ?CustomerCollectionGroup{prop:Color} = 15066597
    ?CustomerCollectionGroup{prop:Trn} = 0
    ?cou:NewStatus:Prompt{prop:FontColor} = -1
    ?cou:NewStatus:Prompt{prop:Color} = 15066597
    If ?cou:NewStatus{prop:ReadOnly} = True
        ?cou:NewStatus{prop:FontColor} = 65793
        ?cou:NewStatus{prop:Color} = 15066597
    Elsif ?cou:NewStatus{prop:Req} = True
        ?cou:NewStatus{prop:FontColor} = 65793
        ?cou:NewStatus{prop:Color} = 8454143
    Else ! If ?cou:NewStatus{prop:Req} = True
        ?cou:NewStatus{prop:FontColor} = 65793
        ?cou:NewStatus{prop:Color} = 16777215
    End ! If ?cou:NewStatus{prop:Req} = True
    ?cou:NewStatus{prop:Trn} = 0
    ?cou:NewStatus{prop:FontStyle} = font:Bold
    ?cou:NoOfDespatchNotes:Prompt{prop:FontColor} = -1
    ?cou:NoOfDespatchNotes:Prompt{prop:Color} = 15066597
    If ?cou:NoOfDespatchNotes{prop:ReadOnly} = True
        ?cou:NoOfDespatchNotes{prop:FontColor} = 65793
        ?cou:NoOfDespatchNotes{prop:Color} = 15066597
    Elsif ?cou:NoOfDespatchNotes{prop:Req} = True
        ?cou:NoOfDespatchNotes{prop:FontColor} = 65793
        ?cou:NoOfDespatchNotes{prop:Color} = 8454143
    Else ! If ?cou:NoOfDespatchNotes{prop:Req} = True
        ?cou:NoOfDespatchNotes{prop:FontColor} = 65793
        ?cou:NoOfDespatchNotes{prop:Color} = 16777215
    End ! If ?cou:NoOfDespatchNotes{prop:Req} = True
    ?cou:NoOfDespatchNotes{prop:Trn} = 0
    ?cou:NoOfDespatchNotes{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?CityLinkTab{prop:Color} = 15066597
    ?cou:Export_Path:Prompt:2{prop:FontColor} = -1
    ?cou:Export_Path:Prompt:2{prop:Color} = 15066597
    If ?cou:Export_Path:2{prop:ReadOnly} = True
        ?cou:Export_Path:2{prop:FontColor} = 65793
        ?cou:Export_Path:2{prop:Color} = 15066597
    Elsif ?cou:Export_Path:2{prop:Req} = True
        ?cou:Export_Path:2{prop:FontColor} = 65793
        ?cou:Export_Path:2{prop:Color} = 8454143
    Else ! If ?cou:Export_Path:2{prop:Req} = True
        ?cou:Export_Path:2{prop:FontColor} = 65793
        ?cou:Export_Path:2{prop:Color} = 16777215
    End ! If ?cou:Export_Path:2{prop:Req} = True
    ?cou:Export_Path:2{prop:Trn} = 0
    ?cou:Export_Path:2{prop:FontStyle} = font:Bold
    ?COU:Service:Prompt{prop:FontColor} = -1
    ?COU:Service:Prompt{prop:Color} = 15066597
    If ?cou:Service{prop:ReadOnly} = True
        ?cou:Service{prop:FontColor} = 65793
        ?cou:Service{prop:Color} = 15066597
    Elsif ?cou:Service{prop:Req} = True
        ?cou:Service{prop:FontColor} = 65793
        ?cou:Service{prop:Color} = 8454143
    Else ! If ?cou:Service{prop:Req} = True
        ?cou:Service{prop:FontColor} = 65793
        ?cou:Service{prop:Color} = 16777215
    End ! If ?cou:Service{prop:Req} = True
    ?cou:Service{prop:Trn} = 0
    ?cou:Service{prop:FontStyle} = font:Bold
    ?ANCTab{prop:Color} = 15066597
    ?cou:Export_Path:Prompt:3{prop:FontColor} = -1
    ?cou:Export_Path:Prompt:3{prop:Color} = 15066597
    If ?cou:Export_Path:3{prop:ReadOnly} = True
        ?cou:Export_Path:3{prop:FontColor} = 65793
        ?cou:Export_Path:3{prop:Color} = 15066597
    Elsif ?cou:Export_Path:3{prop:Req} = True
        ?cou:Export_Path:3{prop:FontColor} = 65793
        ?cou:Export_Path:3{prop:Color} = 8454143
    Else ! If ?cou:Export_Path:3{prop:Req} = True
        ?cou:Export_Path:3{prop:FontColor} = 65793
        ?cou:Export_Path:3{prop:Color} = 16777215
    End ! If ?cou:Export_Path:3{prop:Req} = True
    ?cou:Export_Path:3{prop:Trn} = 0
    ?cou:Export_Path:3{prop:FontStyle} = font:Bold
    ?COU:Import_Path:Prompt{prop:FontColor} = -1
    ?COU:Import_Path:Prompt{prop:Color} = 15066597
    If ?cou:Import_Path{prop:ReadOnly} = True
        ?cou:Import_Path{prop:FontColor} = 65793
        ?cou:Import_Path{prop:Color} = 15066597
    Elsif ?cou:Import_Path{prop:Req} = True
        ?cou:Import_Path{prop:FontColor} = 65793
        ?cou:Import_Path{prop:Color} = 8454143
    Else ! If ?cou:Import_Path{prop:Req} = True
        ?cou:Import_Path{prop:FontColor} = 65793
        ?cou:Import_Path{prop:Color} = 16777215
    End ! If ?cou:Import_Path{prop:Req} = True
    ?cou:Import_Path{prop:Trn} = 0
    ?cou:Import_Path{prop:FontStyle} = font:Bold
    ?COU:Last_Despatch_Time:Prompt{prop:FontColor} = -1
    ?COU:Last_Despatch_Time:Prompt{prop:Color} = 15066597
    If ?cou:Last_Despatch_Time{prop:ReadOnly} = True
        ?cou:Last_Despatch_Time{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time{prop:Color} = 15066597
    Elsif ?cou:Last_Despatch_Time{prop:Req} = True
        ?cou:Last_Despatch_Time{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time{prop:Color} = 8454143
    Else ! If ?cou:Last_Despatch_Time{prop:Req} = True
        ?cou:Last_Despatch_Time{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time{prop:Color} = 16777215
    End ! If ?cou:Last_Despatch_Time{prop:Req} = True
    ?cou:Last_Despatch_Time{prop:Trn} = 0
    ?cou:Last_Despatch_Time{prop:FontStyle} = font:Bold
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?COU:ICOLSuffix:Prompt{prop:FontColor} = -1
    ?COU:ICOLSuffix:Prompt{prop:Color} = 15066597
    If ?cou:ICOLSuffix{prop:ReadOnly} = True
        ?cou:ICOLSuffix{prop:FontColor} = 65793
        ?cou:ICOLSuffix{prop:Color} = 15066597
    Elsif ?cou:ICOLSuffix{prop:Req} = True
        ?cou:ICOLSuffix{prop:FontColor} = 65793
        ?cou:ICOLSuffix{prop:Color} = 8454143
    Else ! If ?cou:ICOLSuffix{prop:Req} = True
        ?cou:ICOLSuffix{prop:FontColor} = 65793
        ?cou:ICOLSuffix{prop:Color} = 16777215
    End ! If ?cou:ICOLSuffix{prop:Req} = True
    ?cou:ICOLSuffix{prop:Trn} = 0
    ?cou:ICOLSuffix{prop:FontStyle} = font:Bold
    ?COU:ContractNumber:Prompt{prop:FontColor} = -1
    ?COU:ContractNumber:Prompt{prop:Color} = 15066597
    If ?cou:ContractNumber{prop:ReadOnly} = True
        ?cou:ContractNumber{prop:FontColor} = 65793
        ?cou:ContractNumber{prop:Color} = 15066597
    Elsif ?cou:ContractNumber{prop:Req} = True
        ?cou:ContractNumber{prop:FontColor} = 65793
        ?cou:ContractNumber{prop:Color} = 8454143
    Else ! If ?cou:ContractNumber{prop:Req} = True
        ?cou:ContractNumber{prop:FontColor} = 65793
        ?cou:ContractNumber{prop:Color} = 16777215
    End ! If ?cou:ContractNumber{prop:Req} = True
    ?cou:ContractNumber{prop:Trn} = 0
    ?cou:ContractNumber{prop:FontStyle} = font:Bold
    ?COU:ANCPath:Prompt{prop:FontColor} = -1
    ?COU:ANCPath:Prompt{prop:Color} = 15066597
    If ?cou:ANCPath{prop:ReadOnly} = True
        ?cou:ANCPath{prop:FontColor} = 65793
        ?cou:ANCPath{prop:Color} = 15066597
    Elsif ?cou:ANCPath{prop:Req} = True
        ?cou:ANCPath{prop:FontColor} = 65793
        ?cou:ANCPath{prop:Color} = 8454143
    Else ! If ?cou:ANCPath{prop:Req} = True
        ?cou:ANCPath{prop:FontColor} = 65793
        ?cou:ANCPath{prop:Color} = 16777215
    End ! If ?cou:ANCPath{prop:Req} = True
    ?cou:ANCPath{prop:Trn} = 0
    ?cou:ANCPath{prop:FontStyle} = font:Bold
    ?GeneralDefaults{prop:Color} = 15066597
    ?cou:AutoConsignmentNo{prop:Font,3} = -1
    ?cou:AutoConsignmentNo{prop:Color} = 15066597
    ?cou:AutoConsignmentNo{prop:Trn} = 0
    ?cou:LastConsignmentNo:Prompt{prop:FontColor} = -1
    ?cou:LastConsignmentNo:Prompt{prop:Color} = 15066597
    If ?cou:LastConsignmentNo{prop:ReadOnly} = True
        ?cou:LastConsignmentNo{prop:FontColor} = 65793
        ?cou:LastConsignmentNo{prop:Color} = 15066597
    Elsif ?cou:LastConsignmentNo{prop:Req} = True
        ?cou:LastConsignmentNo{prop:FontColor} = 65793
        ?cou:LastConsignmentNo{prop:Color} = 8454143
    Else ! If ?cou:LastConsignmentNo{prop:Req} = True
        ?cou:LastConsignmentNo{prop:FontColor} = 65793
        ?cou:LastConsignmentNo{prop:Color} = 16777215
    End ! If ?cou:LastConsignmentNo{prop:Req} = True
    ?cou:LastConsignmentNo{prop:Trn} = 0
    ?cou:LastConsignmentNo{prop:FontStyle} = font:Bold
    ?cou:PrintLabel{prop:Font,3} = -1
    ?cou:PrintLabel{prop:Color} = 15066597
    ?cou:PrintLabel{prop:Trn} = 0
    ?LabelGTab{prop:Color} = 15066597
    ?COU:Service:Prompt:2{prop:FontColor} = -1
    ?COU:Service:Prompt:2{prop:Color} = 15066597
    If ?COU:Service:2{prop:ReadOnly} = True
        ?COU:Service:2{prop:FontColor} = 65793
        ?COU:Service:2{prop:Color} = 15066597
    Elsif ?COU:Service:2{prop:Req} = True
        ?COU:Service:2{prop:FontColor} = 65793
        ?COU:Service:2{prop:Color} = 8454143
    Else ! If ?COU:Service:2{prop:Req} = True
        ?COU:Service:2{prop:FontColor} = 65793
        ?COU:Service:2{prop:Color} = 16777215
    End ! If ?COU:Service:2{prop:Req} = True
    ?COU:Service:2{prop:Trn} = 0
    ?COU:Service:2{prop:FontStyle} = font:Bold
    ?COU:LabGOptions:Prompt{prop:FontColor} = -1
    ?COU:LabGOptions:Prompt{prop:Color} = 15066597
    If ?cou:LabGOptions{prop:ReadOnly} = True
        ?cou:LabGOptions{prop:FontColor} = 65793
        ?cou:LabGOptions{prop:Color} = 15066597
    Elsif ?cou:LabGOptions{prop:Req} = True
        ?cou:LabGOptions{prop:FontColor} = 65793
        ?cou:LabGOptions{prop:Color} = 8454143
    Else ! If ?cou:LabGOptions{prop:Req} = True
        ?cou:LabGOptions{prop:FontColor} = 65793
        ?cou:LabGOptions{prop:Color} = 16777215
    End ! If ?cou:LabGOptions{prop:Req} = True
    ?cou:LabGOptions{prop:Trn} = 0
    ?cou:LabGOptions{prop:FontStyle} = font:Bold
    ?cou:Last_Despatch_Time:Prompt:5{prop:FontColor} = -1
    ?cou:Last_Despatch_Time:Prompt:5{prop:Color} = 15066597
    If ?cou:Last_Despatch_Time:5{prop:ReadOnly} = True
        ?cou:Last_Despatch_Time:5{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:5{prop:Color} = 15066597
    Elsif ?cou:Last_Despatch_Time:5{prop:Req} = True
        ?cou:Last_Despatch_Time:5{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:5{prop:Color} = 8454143
    Else ! If ?cou:Last_Despatch_Time:5{prop:Req} = True
        ?cou:Last_Despatch_Time:5{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:5{prop:Color} = 16777215
    End ! If ?cou:Last_Despatch_Time:5{prop:Req} = True
    ?cou:Last_Despatch_Time:5{prop:Trn} = 0
    ?cou:Last_Despatch_Time:5{prop:FontStyle} = font:Bold
    ?Prompt16:5{prop:FontColor} = -1
    ?Prompt16:5{prop:Color} = 15066597
    ?COU:Export_Path:Prompt{prop:FontColor} = -1
    ?COU:Export_Path:Prompt{prop:Color} = 15066597
    If ?cou:Export_Path{prop:ReadOnly} = True
        ?cou:Export_Path{prop:FontColor} = 65793
        ?cou:Export_Path{prop:Color} = 15066597
    Elsif ?cou:Export_Path{prop:Req} = True
        ?cou:Export_Path{prop:FontColor} = 65793
        ?cou:Export_Path{prop:Color} = 8454143
    Else ! If ?cou:Export_Path{prop:Req} = True
        ?cou:Export_Path{prop:FontColor} = 65793
        ?cou:Export_Path{prop:Color} = 16777215
    End ! If ?cou:Export_Path{prop:Req} = True
    ?cou:Export_Path{prop:Trn} = 0
    ?cou:Export_Path{prop:FontStyle} = font:Bold
    ?RoyalMailTab{prop:Color} = 15066597
    ?cou:Export_Path:Prompt:4{prop:FontColor} = -1
    ?cou:Export_Path:Prompt:4{prop:Color} = 15066597
    If ?cou:Export_Path:4{prop:ReadOnly} = True
        ?cou:Export_Path:4{prop:FontColor} = 65793
        ?cou:Export_Path:4{prop:Color} = 15066597
    Elsif ?cou:Export_Path:4{prop:Req} = True
        ?cou:Export_Path:4{prop:FontColor} = 65793
        ?cou:Export_Path:4{prop:Color} = 8454143
    Else ! If ?cou:Export_Path:4{prop:Req} = True
        ?cou:Export_Path:4{prop:FontColor} = 65793
        ?cou:Export_Path:4{prop:Color} = 16777215
    End ! If ?cou:Export_Path:4{prop:Req} = True
    ?cou:Export_Path:4{prop:Trn} = 0
    ?cou:Export_Path:4{prop:FontStyle} = font:Bold
    ?cou:Import_Path:Prompt:2{prop:FontColor} = -1
    ?cou:Import_Path:Prompt:2{prop:Color} = 15066597
    If ?cou:Import_Path:2{prop:ReadOnly} = True
        ?cou:Import_Path:2{prop:FontColor} = 65793
        ?cou:Import_Path:2{prop:Color} = 15066597
    Elsif ?cou:Import_Path:2{prop:Req} = True
        ?cou:Import_Path:2{prop:FontColor} = 65793
        ?cou:Import_Path:2{prop:Color} = 8454143
    Else ! If ?cou:Import_Path:2{prop:Req} = True
        ?cou:Import_Path:2{prop:FontColor} = 65793
        ?cou:Import_Path:2{prop:Color} = 16777215
    End ! If ?cou:Import_Path:2{prop:Req} = True
    ?cou:Import_Path:2{prop:Trn} = 0
    ?cou:Import_Path:2{prop:FontStyle} = font:Bold
    ?cou:Last_Despatch_Time:Prompt:2{prop:FontColor} = -1
    ?cou:Last_Despatch_Time:Prompt:2{prop:Color} = 15066597
    If ?cou:Last_Despatch_Time:2{prop:ReadOnly} = True
        ?cou:Last_Despatch_Time:2{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:2{prop:Color} = 15066597
    Elsif ?cou:Last_Despatch_Time:2{prop:Req} = True
        ?cou:Last_Despatch_Time:2{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:2{prop:Color} = 8454143
    Else ! If ?cou:Last_Despatch_Time:2{prop:Req} = True
        ?cou:Last_Despatch_Time:2{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:2{prop:Color} = 16777215
    End ! If ?cou:Last_Despatch_Time:2{prop:Req} = True
    ?cou:Last_Despatch_Time:2{prop:Trn} = 0
    ?cou:Last_Despatch_Time:2{prop:FontStyle} = font:Bold
    ?Prompt16:2{prop:FontColor} = -1
    ?Prompt16:2{prop:Color} = 15066597
    ?ParcelLineTab{prop:Color} = 15066597
    ?cou:Export_Path:Prompt:5{prop:FontColor} = -1
    ?cou:Export_Path:Prompt:5{prop:Color} = 15066597
    If ?cou:Export_Path:5{prop:ReadOnly} = True
        ?cou:Export_Path:5{prop:FontColor} = 65793
        ?cou:Export_Path:5{prop:Color} = 15066597
    Elsif ?cou:Export_Path:5{prop:Req} = True
        ?cou:Export_Path:5{prop:FontColor} = 65793
        ?cou:Export_Path:5{prop:Color} = 8454143
    Else ! If ?cou:Export_Path:5{prop:Req} = True
        ?cou:Export_Path:5{prop:FontColor} = 65793
        ?cou:Export_Path:5{prop:Color} = 16777215
    End ! If ?cou:Export_Path:5{prop:Req} = True
    ?cou:Export_Path:5{prop:Trn} = 0
    ?cou:Export_Path:5{prop:FontStyle} = font:Bold
    ?cou:Last_Despatch_Time:Prompt:3{prop:FontColor} = -1
    ?cou:Last_Despatch_Time:Prompt:3{prop:Color} = 15066597
    If ?cou:Last_Despatch_Time:3{prop:ReadOnly} = True
        ?cou:Last_Despatch_Time:3{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:3{prop:Color} = 15066597
    Elsif ?cou:Last_Despatch_Time:3{prop:Req} = True
        ?cou:Last_Despatch_Time:3{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:3{prop:Color} = 8454143
    Else ! If ?cou:Last_Despatch_Time:3{prop:Req} = True
        ?cou:Last_Despatch_Time:3{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:3{prop:Color} = 16777215
    End ! If ?cou:Last_Despatch_Time:3{prop:Req} = True
    ?cou:Last_Despatch_Time:3{prop:Trn} = 0
    ?cou:Last_Despatch_Time:3{prop:FontStyle} = font:Bold
    ?Prompt16:3{prop:FontColor} = -1
    ?Prompt16:3{prop:Color} = 15066597
    ?UPSTab{prop:Color} = 15066597
    ?cou:Export_Path:Prompt:6{prop:FontColor} = -1
    ?cou:Export_Path:Prompt:6{prop:Color} = 15066597
    If ?cou:Export_Path:6{prop:ReadOnly} = True
        ?cou:Export_Path:6{prop:FontColor} = 65793
        ?cou:Export_Path:6{prop:Color} = 15066597
    Elsif ?cou:Export_Path:6{prop:Req} = True
        ?cou:Export_Path:6{prop:FontColor} = 65793
        ?cou:Export_Path:6{prop:Color} = 8454143
    Else ! If ?cou:Export_Path:6{prop:Req} = True
        ?cou:Export_Path:6{prop:FontColor} = 65793
        ?cou:Export_Path:6{prop:Color} = 16777215
    End ! If ?cou:Export_Path:6{prop:Req} = True
    ?cou:Export_Path:6{prop:Trn} = 0
    ?cou:Export_Path:6{prop:FontStyle} = font:Bold
    ?cou:Last_Despatch_Time:Prompt:4{prop:FontColor} = -1
    ?cou:Last_Despatch_Time:Prompt:4{prop:Color} = 15066597
    If ?cou:Last_Despatch_Time:4{prop:ReadOnly} = True
        ?cou:Last_Despatch_Time:4{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:4{prop:Color} = 15066597
    Elsif ?cou:Last_Despatch_Time:4{prop:Req} = True
        ?cou:Last_Despatch_Time:4{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:4{prop:Color} = 8454143
    Else ! If ?cou:Last_Despatch_Time:4{prop:Req} = True
        ?cou:Last_Despatch_Time:4{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:4{prop:Color} = 16777215
    End ! If ?cou:Last_Despatch_Time:4{prop:Req} = True
    ?cou:Last_Despatch_Time:4{prop:Trn} = 0
    ?cou:Last_Despatch_Time:4{prop:FontStyle} = font:Bold
    ?Prompt16:4{prop:FontColor} = -1
    ?Prompt16:4{prop:Color} = 15066597
    ?ToteTab{prop:Color} = 15066597
    ?cou:Last_Despatch_Time:Prompt:6{prop:FontColor} = -1
    ?cou:Last_Despatch_Time:Prompt:6{prop:Color} = 15066597
    If ?cou:Last_Despatch_Time:6{prop:ReadOnly} = True
        ?cou:Last_Despatch_Time:6{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:6{prop:Color} = 15066597
    Elsif ?cou:Last_Despatch_Time:6{prop:Req} = True
        ?cou:Last_Despatch_Time:6{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:6{prop:Color} = 8454143
    Else ! If ?cou:Last_Despatch_Time:6{prop:Req} = True
        ?cou:Last_Despatch_Time:6{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:6{prop:Color} = 16777215
    End ! If ?cou:Last_Despatch_Time:6{prop:Req} = True
    ?cou:Last_Despatch_Time:6{prop:Trn} = 0
    ?cou:Last_Despatch_Time:6{prop:FontStyle} = font:Bold
    ?Prompt16:6{prop:FontColor} = -1
    ?Prompt16:6{prop:Color} = 15066597
    ?cou:PrintLabel:2{prop:Font,3} = -1
    ?cou:PrintLabel:2{prop:Color} = 15066597
    ?cou:PrintLabel:2{prop:Trn} = 0
    ?ToteParcelineTab{prop:Color} = 15066597
    ?cou:Last_Despatch_Time:Prompt:7{prop:FontColor} = -1
    ?cou:Last_Despatch_Time:Prompt:7{prop:Color} = 15066597
    If ?cou:Last_Despatch_Time:7{prop:ReadOnly} = True
        ?cou:Last_Despatch_Time:7{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:7{prop:Color} = 15066597
    Elsif ?cou:Last_Despatch_Time:7{prop:Req} = True
        ?cou:Last_Despatch_Time:7{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:7{prop:Color} = 8454143
    Else ! If ?cou:Last_Despatch_Time:7{prop:Req} = True
        ?cou:Last_Despatch_Time:7{prop:FontColor} = 65793
        ?cou:Last_Despatch_Time:7{prop:Color} = 16777215
    End ! If ?cou:Last_Despatch_Time:7{prop:Req} = True
    ?cou:Last_Despatch_Time:7{prop:Trn} = 0
    ?cou:Last_Despatch_Time:7{prop:FontStyle} = font:Bold
    ?Prompt16:7{prop:FontColor} = -1
    ?Prompt16:7{prop:Color} = 15066597
    ?COU:Service:Prompt:3{prop:FontColor} = -1
    ?COU:Service:Prompt:3{prop:Color} = 15066597
    If ?cou:Service:3{prop:ReadOnly} = True
        ?cou:Service:3{prop:FontColor} = 65793
        ?cou:Service:3{prop:Color} = 15066597
    Elsif ?cou:Service:3{prop:Req} = True
        ?cou:Service:3{prop:FontColor} = 65793
        ?cou:Service:3{prop:Color} = 8454143
    Else ! If ?cou:Service:3{prop:Req} = True
        ?cou:Service:3{prop:FontColor} = 65793
        ?cou:Service:3{prop:Color} = 16777215
    End ! If ?cou:Service:3{prop:Req} = True
    ?cou:Service:3{prop:Trn} = 0
    ?cou:Service:3{prop:FontStyle} = font:Bold
    ?cou:PrintLabel:3{prop:Font,3} = -1
    ?cou:PrintLabel:3{prop:Color} = 15066597
    ?cou:PrintLabel:3{prop:Trn} = 0
    ?cou:Export_Path:Prompt:7{prop:FontColor} = -1
    ?cou:Export_Path:Prompt:7{prop:Color} = 15066597
    If ?cou:Export_Path:7{prop:ReadOnly} = True
        ?cou:Export_Path:7{prop:FontColor} = 65793
        ?cou:Export_Path:7{prop:Color} = 15066597
    Elsif ?cou:Export_Path:7{prop:Req} = True
        ?cou:Export_Path:7{prop:FontColor} = 65793
        ?cou:Export_Path:7{prop:Color} = 8454143
    Else ! If ?cou:Export_Path:7{prop:Req} = True
        ?cou:Export_Path:7{prop:FontColor} = 65793
        ?cou:Export_Path:7{prop:Color} = 16777215
    End ! If ?cou:Export_Path:7{prop:Req} = True
    ?cou:Export_Path:7{prop:Trn} = 0
    ?cou:Export_Path:7{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateCOURIER',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateCOURIER',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateCOURIER',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateCOURIER',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateCOURIER',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateCOURIER',1)
    SolaceViewVars('DirRetCode',DirRetCode,'UpdateCOURIER',1)
    SolaceViewVars('DirDialogHeader',DirDialogHeader,'UpdateCOURIER',1)
    SolaceViewVars('DirTargetVariable',DirTargetVariable,'UpdateCOURIER',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Courier:Prompt;  SolaceCtrlName = '?COU:Courier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Courier;  SolaceCtrlName = '?cou:Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Account_Number:Prompt;  SolaceCtrlName = '?COU:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Account_Number;  SolaceCtrlName = '?cou:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Postcode:Prompt;  SolaceCtrlName = '?COU:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Postcode;  SolaceCtrlName = '?cou:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button3;  SolaceCtrlName = '?Button3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Address_Line1:Prompt;  SolaceCtrlName = '?COU:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Address_Line1;  SolaceCtrlName = '?cou:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Address_Line2;  SolaceCtrlName = '?cou:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Address_Line3;  SolaceCtrlName = '?cou:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Telephone_Number:Prompt;  SolaceCtrlName = '?COU:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Telephone_Number;  SolaceCtrlName = '?cou:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Fax_Number:Prompt;  SolaceCtrlName = '?COU:Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Fax_Number;  SolaceCtrlName = '?cou:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Contact_Name:Prompt;  SolaceCtrlName = '?COU:Contact_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Contact_Name;  SolaceCtrlName = '?cou:Contact_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Courier_Type:Prompt;  SolaceCtrlName = '?COU:Courier_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Courier_Type;  SolaceCtrlName = '?cou:Courier_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:DespatchClose;  SolaceCtrlName = '?cou:DespatchClose';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:CustomerCollection;  SolaceCtrlName = '?cou:CustomerCollection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CustomerCollectionGroup;  SolaceCtrlName = '?CustomerCollectionGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:NewStatus:Prompt;  SolaceCtrlName = '?cou:NewStatus:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:NewStatus;  SolaceCtrlName = '?cou:NewStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupStatus;  SolaceCtrlName = '?LookupStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:NoOfDespatchNotes:Prompt;  SolaceCtrlName = '?cou:NoOfDespatchNotes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:NoOfDespatchNotes;  SolaceCtrlName = '?cou:NoOfDespatchNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CityLinkTab;  SolaceCtrlName = '?CityLinkTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:Prompt:2;  SolaceCtrlName = '?cou:Export_Path:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:2;  SolaceCtrlName = '?cou:Export_Path:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupCityExportPath;  SolaceCtrlName = '?LookupCityExportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Service:Prompt;  SolaceCtrlName = '?COU:Service:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Service;  SolaceCtrlName = '?cou:Service';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ANCTab;  SolaceCtrlName = '?ANCTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:Prompt:3;  SolaceCtrlName = '?cou:Export_Path:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:3;  SolaceCtrlName = '?cou:Export_Path:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupANCExportPath;  SolaceCtrlName = '?LookupANCExportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Import_Path:Prompt;  SolaceCtrlName = '?COU:Import_Path:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Import_Path;  SolaceCtrlName = '?cou:Import_Path';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFile:3;  SolaceCtrlName = '?LookupFile:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Last_Despatch_Time:Prompt;  SolaceCtrlName = '?COU:Last_Despatch_Time:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time;  SolaceCtrlName = '?cou:Last_Despatch_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16;  SolaceCtrlName = '?Prompt16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:ICOLSuffix:Prompt;  SolaceCtrlName = '?COU:ICOLSuffix:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:ICOLSuffix;  SolaceCtrlName = '?cou:ICOLSuffix';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:ContractNumber:Prompt;  SolaceCtrlName = '?COU:ContractNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:ContractNumber;  SolaceCtrlName = '?cou:ContractNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:ANCPath:Prompt;  SolaceCtrlName = '?COU:ANCPath:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:ANCPath;  SolaceCtrlName = '?cou:ANCPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFile:4;  SolaceCtrlName = '?LookupFile:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?GeneralDefaults;  SolaceCtrlName = '?GeneralDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:AutoConsignmentNo;  SolaceCtrlName = '?cou:AutoConsignmentNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:LastConsignmentNo:Prompt;  SolaceCtrlName = '?cou:LastConsignmentNo:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:LastConsignmentNo;  SolaceCtrlName = '?cou:LastConsignmentNo';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:PrintLabel;  SolaceCtrlName = '?cou:PrintLabel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LabelGTab;  SolaceCtrlName = '?LabelGTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Service:Prompt:2;  SolaceCtrlName = '?COU:Service:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Service:2;  SolaceCtrlName = '?COU:Service:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:LabGOptions:Prompt;  SolaceCtrlName = '?COU:LabGOptions:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:LabGOptions;  SolaceCtrlName = '?cou:LabGOptions';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:Prompt:5;  SolaceCtrlName = '?cou:Last_Despatch_Time:Prompt:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:5;  SolaceCtrlName = '?cou:Last_Despatch_Time:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:5;  SolaceCtrlName = '?Prompt16:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Export_Path:Prompt;  SolaceCtrlName = '?COU:Export_Path:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path;  SolaceCtrlName = '?cou:Export_Path';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFile;  SolaceCtrlName = '?LookupFile';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RoyalMailTab;  SolaceCtrlName = '?RoyalMailTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:Prompt:4;  SolaceCtrlName = '?cou:Export_Path:Prompt:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:4;  SolaceCtrlName = '?cou:Export_Path:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupRoyalExportPath;  SolaceCtrlName = '?LookupRoyalExportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Import_Path:Prompt:2;  SolaceCtrlName = '?cou:Import_Path:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Import_Path:2;  SolaceCtrlName = '?cou:Import_Path:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupRoyalImportPath;  SolaceCtrlName = '?LookupRoyalImportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:Prompt:2;  SolaceCtrlName = '?cou:Last_Despatch_Time:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:2;  SolaceCtrlName = '?cou:Last_Despatch_Time:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:2;  SolaceCtrlName = '?Prompt16:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ParcelLineTab;  SolaceCtrlName = '?ParcelLineTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:Prompt:5;  SolaceCtrlName = '?cou:Export_Path:Prompt:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:5;  SolaceCtrlName = '?cou:Export_Path:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupParcelExportPath;  SolaceCtrlName = '?LookupParcelExportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:Prompt:3;  SolaceCtrlName = '?cou:Last_Despatch_Time:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:3;  SolaceCtrlName = '?cou:Last_Despatch_Time:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:3;  SolaceCtrlName = '?Prompt16:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UPSTab;  SolaceCtrlName = '?UPSTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:Prompt:6;  SolaceCtrlName = '?cou:Export_Path:Prompt:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:6;  SolaceCtrlName = '?cou:Export_Path:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupUPSExportPath;  SolaceCtrlName = '?LookupUPSExportPath';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:Prompt:4;  SolaceCtrlName = '?cou:Last_Despatch_Time:Prompt:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:4;  SolaceCtrlName = '?cou:Last_Despatch_Time:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:4;  SolaceCtrlName = '?Prompt16:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ToteTab;  SolaceCtrlName = '?ToteTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:Prompt:6;  SolaceCtrlName = '?cou:Last_Despatch_Time:Prompt:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:6;  SolaceCtrlName = '?cou:Last_Despatch_Time:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:6;  SolaceCtrlName = '?Prompt16:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:PrintLabel:2;  SolaceCtrlName = '?cou:PrintLabel:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ToteParcelineTab;  SolaceCtrlName = '?ToteParcelineTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:Prompt:7;  SolaceCtrlName = '?cou:Last_Despatch_Time:Prompt:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Last_Despatch_Time:7;  SolaceCtrlName = '?cou:Last_Despatch_Time:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:7;  SolaceCtrlName = '?Prompt16:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?COU:Service:Prompt:3;  SolaceCtrlName = '?COU:Service:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Service:3;  SolaceCtrlName = '?cou:Service:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:PrintLabel:3;  SolaceCtrlName = '?cou:PrintLabel:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupFile:2;  SolaceCtrlName = '?LookupFile:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:Prompt:7;  SolaceCtrlName = '?cou:Export_Path:Prompt:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cou:Export_Path:7;  SolaceCtrlName = '?cou:Export_Path:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Courier'
  OF ChangeRecord
    ActionMessage = 'Changing A Courier'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateCOURIER')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateCOURIER')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?COU:Courier:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(cou:Record,History::cou:Record)
  SELF.AddHistoryField(?cou:Courier,1)
  SELF.AddHistoryField(?cou:Account_Number,2)
  SELF.AddHistoryField(?cou:Postcode,3)
  SELF.AddHistoryField(?cou:Address_Line1,4)
  SELF.AddHistoryField(?cou:Address_Line2,5)
  SELF.AddHistoryField(?cou:Address_Line3,6)
  SELF.AddHistoryField(?cou:Telephone_Number,7)
  SELF.AddHistoryField(?cou:Fax_Number,8)
  SELF.AddHistoryField(?cou:Contact_Name,9)
  SELF.AddHistoryField(?cou:Courier_Type,16)
  SELF.AddHistoryField(?cou:DespatchClose,22)
  SELF.AddHistoryField(?cou:CustomerCollection,24)
  SELF.AddHistoryField(?cou:NewStatus,25)
  SELF.AddHistoryField(?cou:NoOfDespatchNotes,26)
  SELF.AddHistoryField(?cou:Export_Path:2,10)
  SELF.AddHistoryField(?cou:Service,14)
  SELF.AddHistoryField(?cou:Export_Path:3,10)
  SELF.AddHistoryField(?cou:Import_Path,15)
  SELF.AddHistoryField(?cou:Last_Despatch_Time,17)
  SELF.AddHistoryField(?cou:ICOLSuffix,18)
  SELF.AddHistoryField(?cou:ContractNumber,19)
  SELF.AddHistoryField(?cou:ANCPath,20)
  SELF.AddHistoryField(?cou:AutoConsignmentNo,27)
  SELF.AddHistoryField(?cou:LastConsignmentNo,28)
  SELF.AddHistoryField(?cou:PrintLabel,29)
  SELF.AddHistoryField(?COU:Service:2,14)
  SELF.AddHistoryField(?cou:LabGOptions,23)
  SELF.AddHistoryField(?cou:Last_Despatch_Time:5,17)
  SELF.AddHistoryField(?cou:Export_Path,10)
  SELF.AddHistoryField(?cou:Export_Path:4,10)
  SELF.AddHistoryField(?cou:Import_Path:2,15)
  SELF.AddHistoryField(?cou:Last_Despatch_Time:2,17)
  SELF.AddHistoryField(?cou:Export_Path:5,10)
  SELF.AddHistoryField(?cou:Last_Despatch_Time:3,17)
  SELF.AddHistoryField(?cou:Export_Path:6,10)
  SELF.AddHistoryField(?cou:Last_Despatch_Time:4,17)
  SELF.AddHistoryField(?cou:Last_Despatch_Time:6,17)
  SELF.AddHistoryField(?cou:PrintLabel:2,29)
  SELF.AddHistoryField(?cou:Last_Despatch_Time:7,17)
  SELF.AddHistoryField(?cou:Service:3,14)
  SELF.AddHistoryField(?cou:PrintLabel:3,29)
  SELF.AddHistoryField(?cou:Export_Path:7,10)
  SELF.AddUpdateFile(Access:COURIER)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:COURIER.Open
  Relate:DEFAULTS.Open
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:COURIER
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?cou:Courier_Type{prop:vcr} = TRUE
  ?cou:Courier_Type{prop:vcr} = False
  ! support for CPCS
  IF ?cou:NewStatus{Prop:Tip} AND ~?LookupStatus{Prop:Tip}
     ?LookupStatus{Prop:Tip} = 'Select ' & ?cou:NewStatus{Prop:Tip}
  END
  IF ?cou:NewStatus{Prop:Msg} AND ~?LookupStatus{Prop:Msg}
     ?LookupStatus{Prop:Msg} = 'Select ' & ?cou:NewStatus{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?cou:CustomerCollection{Prop:Checked} = True
    ENABLE(?CustomerCollectionGroup)
  END
  IF ?cou:CustomerCollection{Prop:Checked} = False
    DISABLE(?CustomerCollectionGroup)
  END
  IF ?cou:AutoConsignmentNo{Prop:Checked} = True
    ENABLE(?cou:LastConsignmentNo:Prompt)
    ENABLE(?cou:LastConsignmentNo)
  END
  IF ?cou:AutoConsignmentNo{Prop:Checked} = False
    DISABLE(?cou:LastConsignmentNo:Prompt)
    DISABLE(?cou:LastConsignmentNo)
  END
  FileLookup12.Init
  FileLookup12.Flags=BOR(FileLookup12.Flags,FILE:LongName)
  FileLookup12.Flags=BOR(FileLookup12.Flags,FILE:Directory)
  FileLookup12.SetMask('All Files','*.*')
  FileLookup12.WindowTitle='Export Path'
  FileLookup11.Init
  FileLookup11.Flags=BOR(FileLookup11.Flags,FILE:LongName)
  FileLookup11.Flags=BOR(FileLookup11.Flags,FILE:Directory)
  FileLookup11.SetMask('All Files','*.*')
  FileLookup11.WindowTitle='Import Path'
  FileLookup14.Init
  FileLookup14.Flags=BOR(FileLookup14.Flags,FILE:LongName)
  FileLookup14.Flags=BOR(FileLookup14.Flags,FILE:Directory)
  FileLookup14.SetMask('All Files','*.*')
  FileLookup14.WindowTitle='ANC Label Path'
  FileLookup18.Init
  FileLookup18.Flags=BOR(FileLookup18.Flags,FILE:LongName)
  FileLookup18.Flags=BOR(FileLookup18.Flags,FILE:Directory)
  FileLookup18.SetMask('All Files','*.*')
  FileLookup18.WindowTitle='Export Path'
  FileLookup19.Init
  FileLookup19.Flags=BOR(FileLookup19.Flags,FILE:LongName)
  FileLookup19.Flags=BOR(FileLookup19.Flags,FILE:Directory)
  FileLookup19.SetMask('All Files','*.*')
  FileLookup19.WindowTitle='Export Path'
  FileLookup20.Init
  FileLookup20.Flags=BOR(FileLookup20.Flags,FILE:LongName)
  FileLookup20.Flags=BOR(FileLookup20.Flags,FILE:Directory)
  FileLookup20.SetMask('All Files','*.*')
  FileLookup20.WindowTitle='Export Path'
  FileLookup21.Init
  FileLookup21.Flags=BOR(FileLookup21.Flags,FILE:LongName)
  FileLookup21.Flags=BOR(FileLookup21.Flags,FILE:Directory)
  FileLookup21.SetMask('All Files','*.*')
  FileLookup21.WindowTitle='Import Path'
  FileLookup22.Init
  FileLookup22.Flags=BOR(FileLookup22.Flags,FILE:LongName)
  FileLookup22.Flags=BOR(FileLookup22.Flags,FILE:Directory)
  FileLookup22.SetMask('All Files','*.*')
  FileLookup22.WindowTitle='Export Path'
  FileLookup23.Init
  FileLookup23.Flags=BOR(FileLookup23.Flags,FILE:LongName)
  FileLookup23.Flags=BOR(FileLookup23.Flags,FILE:Directory)
  FileLookup23.SetMask('All Files','*.*')
  FileLookup23.WindowTitle='Export Path'
  FileLookup10.Init
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:LongName)
  FileLookup10.Flags=BOR(FileLookup10.Flags,FILE:Directory)
  FileLookup10.SetMask('All Files','*.*')
  FileLookup10.WindowTitle='Export Path'
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COURIER.Close
    Relate:DEFAULTS.Close
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateCOURIER',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Status
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?cou:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Postcode, Accepted)
      If ~0{prop:acceptall}
          Postcode_Routine(cou:postcode,cou:address_line1,cou:address_line2,cou:address_line3)
          Select(?cou:address_line1,1)
          Display()
      End
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Postcode, Accepted)
    OF ?Button3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
      Case MessageEx('Are you sure you want to clear the Address?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          
              cou:postcode      = ''
              cou:address_line1 = ''
              cou:address_line2 = ''
              cou:address_line3 = ''
              Display()
              Select(?cou:postcode)
          Of 2 ! &No Button
      End!Case MessageEx
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Button3, Accepted)
    OF ?cou:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Telephone_Number, Accepted)
      
       temp_string = Clip(left(cou:Telephone_Number))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       cou:Telephone_Number = temp_string
       Display(?cou:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Telephone_Number, Accepted)
    OF ?cou:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Fax_Number, Accepted)
      
       temp_string = Clip(left(cou:Fax_Number))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       cou:Fax_Number = temp_string
       Display(?cou:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Fax_Number, Accepted)
    OF ?cou:Courier_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Courier_Type, Accepted)
      If ~0{prop:acceptall}
          Case cou:courier_type
              Of 'CITY LINK'
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  UnHide(?CityLinkTab)
                  hide(?RoyalMailTab)
                  Hide(?ParcelLineTab)
                  Hide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?CityLinkTab{prop:Text} = 'City Link Defaults'
              Of 'PARCELINE LASERNET'
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  UnHide(?CityLinkTab)
                  hide(?RoyalMailTab)
                  Hide(?ParcelLineTab)
                  Hide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?CityLinkTab{prop:Text} = 'Lasernet Defaults'
              Of 'ANC'
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Unhide(?ANCTab)
                  Hide(?CityLinkTab)
                  hide(?RoyalMailTab)
                  Hide(?ParcelLineTab)
                  Hide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?ANCTab{prop:text} = 'ANC Defaults'
              Of 'LABEL G'
                  hide(?RoyalMailTab)
                  Unhide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  Hide(?CityLinkTab)
                  Hide(?ParcelLineTab)
                  Hide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?LabelgTab{prop:text} = 'LABEL G Defaults'
              Of 'ROYAL MAIL'
                  Unhide(?RoyalMailTab)
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  Hide(?CityLinkTab)
                  Hide(?ParcelLineTab)
                  Hide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?RoyalMailTab{prop:text} = 'Royal Mail Defaults'
              Of 'PARCELINE' 
                  hide(?RoyalMailTab)
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  Hide(?CityLinkTab)
                  UnHide(?ParcelLineTab)
                  Hide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?ParcelLineTab{Prop:text} = 'Parceline Defaults'
              Of 'UPS' Orof 'UPS ALTERNATIVE'
                  hide(?RoyalMailTab)
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  Hide(?CityLinkTab)
                  Hide(?ParcelLineTab)
                  UnHide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?UPSTab{prop:text} = 'UPS Defaults'
              Of 'TOTE'
                  hide(?RoyalMailTab)
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  Hide(?CityLinkTab)
                  Hide(?ParcelLineTab)
                  Hide(?UPSTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  UNHIDE(?ToteTab)
                  ?ToteTab{prop:text} = 'TOTE Defaults'
              ! Start Change 3813 BE919/01/04)
              Of 'TOTE PARCELINE'
                  hide(?RoyalMailTab)
                  hide(?LabelgTab)
                  Hide(?GeneralDefaults)
                  Hide(?ANCTab)
                  Hide(?CityLinkTab)
                  Hide(?ParcelLineTab)
                  Hide(?ToteTab)
                  Hide(?UPSTab)
                  UNHIDE(?ToteParcelineTab)
                  ?ToteParcelineTab{prop:text} = 'TOTE PARCELINE Defaults'
              ! End Change 3813 BE919/01/04)
              Else
                  hide(?LabelgTab)
                  UnHide(?GeneralDefaults)
                  Hide(?ANCTab)
                  Hide(?CityLinkTab)
                  hide(?RoyalMailTab)
                  Hide(?ParcelLineTab)
                  Hide(?UPSTab)
                  HIDE(?ToteTab)
                  ! Start Change 3813 BE919/01/04)
                  Hide(?ToteParcelineTab)
                  ! End Change 3813 BE919/01/04)
                  ?GeneralDefaults{prop:text} = 'General Defaults'
          End!Case cou:courier_type
          !thismakeover.setwindow(win:form)
      End!If ~0{prop:acceptall}
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:Courier_Type, Accepted)
    OF ?cou:CustomerCollection
      IF ?cou:CustomerCollection{Prop:Checked} = True
        ENABLE(?CustomerCollectionGroup)
      END
      IF ?cou:CustomerCollection{Prop:Checked} = False
        DISABLE(?CustomerCollectionGroup)
      END
      ThisWindow.Reset
    OF ?cou:NewStatus
      IF cou:NewStatus OR ?cou:NewStatus{Prop:Req}
        sts:Status = cou:NewStatus
        sts:Job = 'YES'
        GLO:Select1 = 'JOB'
        !Save Lookup Field Incase Of error
        look:cou:NewStatus        = cou:NewStatus
        IF Access:STATUS.TryFetch(sts:JobKey)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            cou:NewStatus = sts:Status
          ELSE
            CLEAR(sts:Job)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            cou:NewStatus = look:cou:NewStatus
            SELECT(?cou:NewStatus)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupStatus
      ThisWindow.Update
      sts:Status = cou:NewStatus
      GLO:Select1 = 'JOB'
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          cou:NewStatus = sts:Status
          Select(?+1)
      ELSE
          Select(?cou:NewStatus)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?cou:NewStatus)
    OF ?LookupCityExportPath
      ThisWindow.Update
      cou:Export_Path = Upper(FileLookup18.Ask(1)  )
      DISPLAY
    OF ?LookupANCExportPath
      ThisWindow.Update
      cou:Export_Path = Upper(FileLookup19.Ask(1)  )
      DISPLAY
    OF ?LookupFile:3
      ThisWindow.Update
      cou:Import_Path = Upper(FileLookup11.Ask(1)  )
      DISPLAY
    OF ?LookupFile:4
      ThisWindow.Update
      cou:ANCPath = Upper(FileLookup14.Ask(1)  )
      DISPLAY
    OF ?cou:AutoConsignmentNo
      IF ?cou:AutoConsignmentNo{Prop:Checked} = True
        ENABLE(?cou:LastConsignmentNo:Prompt)
        ENABLE(?cou:LastConsignmentNo)
      END
      IF ?cou:AutoConsignmentNo{Prop:Checked} = False
        DISABLE(?cou:LastConsignmentNo:Prompt)
        DISABLE(?cou:LastConsignmentNo)
      END
      ThisWindow.Reset
    OF ?LookupFile
      ThisWindow.Update
      cou:Export_Path = Upper(FileLookup12.Ask(1)  )
      DISPLAY
    OF ?LookupRoyalExportPath
      ThisWindow.Update
      cou:Export_Path = Upper(FileLookup20.Ask(1)  )
      DISPLAY
    OF ?LookupRoyalImportPath
      ThisWindow.Update
      cou:Import_Path = Upper(FileLookup21.Ask(1)  )
      DISPLAY
    OF ?LookupParcelExportPath
      ThisWindow.Update
      cou:Export_Path = Upper(FileLookup22.Ask(1)  )
      DISPLAY
    OF ?LookupUPSExportPath
      ThisWindow.Update
      cou:Export_Path = Upper(FileLookup23.Ask(1)  )
      DISPLAY
    OF ?LookupFile:2
      ThisWindow.Update
      cou:Export_Path = Upper(FileLookup10.Ask(1)  )
      DISPLAY
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateCOURIER')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?cou:NewStatus
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:NewStatus, AlertKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cou:NewStatus, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      ! Start Change 3813 BE919/01/04)
      ?Sheet2{PROP:ABOVE} = 1
      ! End Change 3813 BE919/01/04)
      Post(Event:accepted,?cou:courier_type)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

