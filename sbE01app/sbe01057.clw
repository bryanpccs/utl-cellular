

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01057.INC'),ONCE        !Local module procedure declarations
                     END


Update_Status_Only PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::sts:Record  LIKE(sts:RECORD),STATIC
QuickWindow          WINDOW('Update the STATUS File'),AT(,,224,108),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateSTATUS'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,216,72),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Status'),AT(8,20),USE(?STS:Status:Prompt),TRN
                           ENTRY(@s30),AT(84,20,127,10),USE(sts:Status),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           CHECK('Use Turnaround Time'),AT(84,36,,12),USE(sts:Use_Turnaround_Time),VALUE('YES','NO')
                           GROUP,AT(4,48,168,24),USE(?turnaround_time_group)
                             PROMPT('Days'),AT(84,50),USE(?sts:turnaround_days:prompt),TRN,FONT(,7,,)
                             PROMPT('Hours'),AT(120,50),USE(?sts:turnaround_hours:prompt),TRN,FONT(,7,,)
                             PROMPT('Turnaround Time'),AT(8,58),USE(?sts:use_turnaround_time:2)
                             SPIN(@n4b),AT(84,58,32,10),USE(sts:Turnaround_Days),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),RANGE(0,9999),STEP(1)
                             SPIN(@n2b),AT(120,58,32,10),USE(sts:Turnaround_Hours),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),RANGE(0,23),STEP(1)
                           END
                         END
                       END
                       BUTTON('&OK'),AT(104,84,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(160,84,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,80,216,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?STS:Status:Prompt{prop:FontColor} = -1
    ?STS:Status:Prompt{prop:Color} = 15066597
    If ?sts:Status{prop:ReadOnly} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 15066597
    Elsif ?sts:Status{prop:Req} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 8454143
    Else ! If ?sts:Status{prop:Req} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 16777215
    End ! If ?sts:Status{prop:Req} = True
    ?sts:Status{prop:Trn} = 0
    ?sts:Status{prop:FontStyle} = font:Bold
    ?sts:Use_Turnaround_Time{prop:Font,3} = -1
    ?sts:Use_Turnaround_Time{prop:Color} = 15066597
    ?sts:Use_Turnaround_Time{prop:Trn} = 0
    ?turnaround_time_group{prop:Font,3} = -1
    ?turnaround_time_group{prop:Color} = 15066597
    ?turnaround_time_group{prop:Trn} = 0
    ?sts:turnaround_days:prompt{prop:FontColor} = -1
    ?sts:turnaround_days:prompt{prop:Color} = 15066597
    ?sts:turnaround_hours:prompt{prop:FontColor} = -1
    ?sts:turnaround_hours:prompt{prop:Color} = 15066597
    ?sts:use_turnaround_time:2{prop:FontColor} = -1
    ?sts:use_turnaround_time:2{prop:Color} = 15066597
    If ?sts:Turnaround_Days{prop:ReadOnly} = True
        ?sts:Turnaround_Days{prop:FontColor} = 65793
        ?sts:Turnaround_Days{prop:Color} = 15066597
    Elsif ?sts:Turnaround_Days{prop:Req} = True
        ?sts:Turnaround_Days{prop:FontColor} = 65793
        ?sts:Turnaround_Days{prop:Color} = 8454143
    Else ! If ?sts:Turnaround_Days{prop:Req} = True
        ?sts:Turnaround_Days{prop:FontColor} = 65793
        ?sts:Turnaround_Days{prop:Color} = 16777215
    End ! If ?sts:Turnaround_Days{prop:Req} = True
    ?sts:Turnaround_Days{prop:Trn} = 0
    ?sts:Turnaround_Days{prop:FontStyle} = font:Bold
    If ?sts:Turnaround_Hours{prop:ReadOnly} = True
        ?sts:Turnaround_Hours{prop:FontColor} = 65793
        ?sts:Turnaround_Hours{prop:Color} = 15066597
    Elsif ?sts:Turnaround_Hours{prop:Req} = True
        ?sts:Turnaround_Hours{prop:FontColor} = 65793
        ?sts:Turnaround_Hours{prop:Color} = 8454143
    Else ! If ?sts:Turnaround_Hours{prop:Req} = True
        ?sts:Turnaround_Hours{prop:FontColor} = 65793
        ?sts:Turnaround_Hours{prop:Color} = 16777215
    End ! If ?sts:Turnaround_Hours{prop:Req} = True
    ?sts:Turnaround_Hours{prop:Trn} = 0
    ?sts:Turnaround_Hours{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Status_Only',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Status_Only',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Update_Status_Only',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Status_Only',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Status_Only',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Update_Status_Only',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STS:Status:Prompt;  SolaceCtrlName = '?STS:Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Status;  SolaceCtrlName = '?sts:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Use_Turnaround_Time;  SolaceCtrlName = '?sts:Use_Turnaround_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?turnaround_time_group;  SolaceCtrlName = '?turnaround_time_group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:turnaround_days:prompt;  SolaceCtrlName = '?sts:turnaround_days:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:turnaround_hours:prompt;  SolaceCtrlName = '?sts:turnaround_hours:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:use_turnaround_time:2;  SolaceCtrlName = '?sts:use_turnaround_time:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Turnaround_Days;  SolaceCtrlName = '?sts:Turnaround_Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Turnaround_Hours;  SolaceCtrlName = '?sts:Turnaround_Hours';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Status Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Status Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Status_Only')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Status_Only')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STS:Status:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sts:Record,History::sts:Record)
  SELF.AddHistoryField(?sts:Status,2)
  SELF.AddHistoryField(?sts:Use_Turnaround_Time,4)
  SELF.AddHistoryField(?sts:Turnaround_Days,6)
  SELF.AddHistoryField(?sts:Turnaround_Hours,8)
  SELF.AddUpdateFile(Access:STATUS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STATUS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?sts:Use_Turnaround_Time{Prop:Checked} = True
    ENABLE(?turnaround_time_group)
  END
  IF ?sts:Use_Turnaround_Time{Prop:Checked} = False
    DISABLE(?turnaround_time_group)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Status_Only',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sts:Use_Turnaround_Time
      IF ?sts:Use_Turnaround_Time{Prop:Checked} = True
        ENABLE(?turnaround_time_group)
      END
      IF ?sts:Use_Turnaround_Time{Prop:Checked} = False
        DISABLE(?turnaround_time_group)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Status_Only')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

