

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01052.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Engineers_Notes PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
select_temp          STRING('''N''')
FilesOpened          BYTE
manufacturer_temp    STRING(30)
notes_tag            STRING(1)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW8::View:Browse    VIEW(NOTESENG)
                       PROJECT(noe:Reference)
                       PROJECT(noe:Notes)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
noe:Reference          LIKE(noe:Reference)            !List box control field - type derived from field
noe:Notes              LIKE(noe:Notes)                !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Standard Engineers Notes'),AT(,,528,228),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Fault_Description_Text'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,268,220),USE(?CurrentTab),SPREAD
                         TAB('By Notes'),USE(?Tab:2)
                           LIST,AT(8,36,260,160),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('72L(2)|M~Reference~@s30@320L(2)|M~Notes~@s80@'),FROM(Queue:Browse),DRAGID('FromList')
                           ENTRY(@s30),AT(8,20,72,10),USE(noe:Reference),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON('&Insert'),AT(8,200,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Change'),AT(68,200,56,16),USE(?Change),LEFT,ICON('edit.gif')
                           BUTTON('&Delete'),AT(128,200,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                           BUTTON('Add Selected &Text To List'),AT(192,200,76,16),USE(?add),LEFT,ICON('select2.ico')
                         END
                       END
                       SHEET,AT(276,4,168,220),USE(?Sheet2),SPREAD
                         TAB('Text To Appear On Job'),USE(?Tab2)
                           LIST,AT(280,36,160,160),USE(?List2),VSCROLL,FORMAT('320L(2)~Notes~@s80@'),FROM(glo:Q_Notes),DROPID('FromList')
                           BUTTON('&Remove Text From List'),AT(280,200,76,16),USE(?remove_text),LEFT,ICON('select.ico')
                         END
                       END
                       BUTTON('&Select'),AT(448,20,76,20),USE(?Select_Button),LEFT,ICON('select.ico')
                       BUTTON('Close'),AT(448,204,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    If ?noe:Reference{prop:ReadOnly} = True
        ?noe:Reference{prop:FontColor} = 65793
        ?noe:Reference{prop:Color} = 15066597
    Elsif ?noe:Reference{prop:Req} = True
        ?noe:Reference{prop:FontColor} = 65793
        ?noe:Reference{prop:Color} = 8454143
    Else ! If ?noe:Reference{prop:Req} = True
        ?noe:Reference{prop:FontColor} = 65793
        ?noe:Reference{prop:Color} = 16777215
    End ! If ?noe:Reference{prop:Req} = True
    ?noe:Reference{prop:Trn} = 0
    ?noe:Reference{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List2{prop:FontColor} = 65793
    ?List2{prop:Color}= 16777215
    ?List2{prop:Color,2} = 16777215
    ?List2{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Engineers_Notes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Engineers_Notes',1)
    SolaceViewVars('select_temp',select_temp,'Browse_Engineers_Notes',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Engineers_Notes',1)
    SolaceViewVars('manufacturer_temp',manufacturer_temp,'Browse_Engineers_Notes',1)
    SolaceViewVars('notes_tag',notes_tag,'Browse_Engineers_Notes',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?noe:Reference;  SolaceCtrlName = '?noe:Reference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?add;  SolaceCtrlName = '?add';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List2;  SolaceCtrlName = '?List2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?remove_text;  SolaceCtrlName = '?remove_text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Button;  SolaceCtrlName = '?Select_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Engineers_Notes')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Engineers_Notes')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?List
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:MANUFACT.Open
  Relate:NOTESENG.Open
  SELF.FilesOpened = True
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:NOTESENG,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  ! support for CPCS
  Clear(glo:Q_Notes)
  Free(glo:Q_Notes)
  BRW8.Q &= Queue:Browse
  BRW8.RetainRow = 0
  BRW8.AddSortOrder(,noe:Notes_Key)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(?noe:Reference,noe:Reference,1,BRW8)
  BRW8.AddField(noe:Reference,BRW8.Q.noe:Reference)
  BRW8.AddField(noe:Notes,BRW8.Q.noe:Notes)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.AskProcedure = 1
  BRW8.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:MANUFACT.Close
    Relate:NOTESENG.Close
  If select_Temp <> 'Y'
      Clear(glo:Q_Notes)
      Free(glo:Q_Notes)
  End
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Engineers_Notes',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('ENGINEERS NOTES - INSERT',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            check_access('ENGINEERS NOTES - CHANGE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            check_access('ENGINEERS NOTES - DELETE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateNOTESENG
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?add
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?add, Accepted)
      glo:notes_pointer = noe:notes
      Add(glo:Q_Notes)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?add, Accepted)
    OF ?remove_text
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?remove_text, Accepted)
      glo:notes_pointer = noe:notes
      Get(glo:Q_Notes,glo:notes_pointer)
      Delete(glo:Q_Notes)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?remove_text, Accepted)
    OF ?Select_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select_Button, Accepted)
      select_temp = 'Y'
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select_Button, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Engineers_Notes')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:Drag
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Drag)
      get(queue:browse,noe:notes)
      if dragid()
          setdropid(brw8.q.noe:notes)
      end!if dragid()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, Drag)
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List2
    CASE EVENT()
    OF EVENT:Drop
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List2, Drop)
      if dropid()
          glo:notes_pointer = noe:notes
          Add(glo:Q_Notes)
          Select(?List)
      end!if dropid()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List2, Drop)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?noe:Reference
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?noe:Reference, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?noe:Reference, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW8.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

