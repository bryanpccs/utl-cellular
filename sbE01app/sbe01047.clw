

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01047.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Model_Stock PROCEDURE (f_model_number,f_user_code) !Generated from procedure template - Window

CurrentTab           STRING(80)
model_number_temp    STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
location_temp        STRING(30)
no_temp              STRING('NO')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:one              BYTE(1)
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?location_temp
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Part_Number)
                       PROJECT(stm:Description)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:Manufacturer)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:Location)
                       JOIN(sto:Ref_Part_Description_Key,stm:Location,stm:Ref_Number,stm:Part_Number,stm:Description)
                         PROJECT(sto:Purchase_Cost)
                         PROJECT(sto:Sale_Cost)
                         PROJECT(sto:Quantity_Stock)
                         PROJECT(sto:Location)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
stm:Part_Number        LIKE(stm:Part_Number)          !List box control field - type derived from field
stm:Description        LIKE(stm:Description)          !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !List box control field - type derived from field
stm:Manufacturer       LIKE(stm:Manufacturer)         !Primary key field - type derived from field
stm:Model_Number       LIKE(stm:Model_Number)         !Primary key field - type derived from field
stm:Location           LIKE(stm:Location)             !Browse key field - type derived from field
sto:Location           LIKE(sto:Location)             !Related join file key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB6::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
QuickWindow          WINDOW('Browse the Stock File'),AT(,,476,210),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Model_Stock'),SYSTEM,GRAY,MAX,RESIZE
                       PANEL,AT(4,4,384,20),USE(?Panel1),FILL(COLOR:Silver)
                       PROMPT('Site Location'),AT(8,8),USE(?Prompt1)
                       COMBO(@s30),AT(64,8,124,10),USE(location_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),FORMAT('120L(2)*@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                       LIST,AT(8,60,376,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('124L(2)|M~Part Number~@s30@124L(2)|M~Description~@s30@[38R(2)|M~Purchase~L@n7.2@' &|
   '33R(2)|M~Sale~L@n7.2@]|M~Costs~47R(2)|M~Qty In Stock~L@N8@48L(2)|M~Ref Number~@n' &|
   '012@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(396,32,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       SHEET,AT(4,28,384,180),USE(?CurrentTab),SPREAD
                         TAB('By Part Number'),USE(?Tab:6)
                           ENTRY(@s30),AT(8,44,124,10),USE(stm:Part_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Description'),USE(?Tab2)
                           ENTRY(@s30),AT(8,44,124,10),USE(stm:Description),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(396,188,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass CLASS(StepStringClass)          !Default Step Manager
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

BRW1::Sort1:StepClass CLASS(StepStringClass)          !Conditional Step Manager - Choice(?CurrentTab) = 2
Init                   PROCEDURE(BYTE Controls,BYTE Mode)
                     END

Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB6                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel1{prop:Fill} = 15066597

    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?location_temp{prop:ReadOnly} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 15066597
    Elsif ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 8454143
    Else ! If ?location_temp{prop:Req} = True
        ?location_temp{prop:FontColor} = 65793
        ?location_temp{prop:Color} = 16777215
    End ! If ?location_temp{prop:Req} = True
    ?location_temp{prop:Trn} = 0
    ?location_temp{prop:FontStyle} = font:Bold
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:6{prop:Color} = 15066597
    If ?stm:Part_Number{prop:ReadOnly} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 15066597
    Elsif ?stm:Part_Number{prop:Req} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 8454143
    Else ! If ?stm:Part_Number{prop:Req} = True
        ?stm:Part_Number{prop:FontColor} = 65793
        ?stm:Part_Number{prop:Color} = 16777215
    End ! If ?stm:Part_Number{prop:Req} = True
    ?stm:Part_Number{prop:Trn} = 0
    ?stm:Part_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?stm:Description{prop:ReadOnly} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 15066597
    Elsif ?stm:Description{prop:Req} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 8454143
    Else ! If ?stm:Description{prop:Req} = True
        ?stm:Description{prop:FontColor} = 65793
        ?stm:Description{prop:Color} = 16777215
    End ! If ?stm:Description{prop:Req} = True
    ?stm:Description{prop:Trn} = 0
    ?stm:Description{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Model_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Model_Stock',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Browse_Model_Stock',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Model_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Model_Stock',1)
    SolaceViewVars('location_temp',location_temp,'Browse_Model_Stock',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Model_Stock',1)
    SolaceViewVars('tmp:one',tmp:one,'Browse_Model_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?location_temp;  SolaceCtrlName = '?location_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:6;  SolaceCtrlName = '?Tab:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Part_Number;  SolaceCtrlName = '?stm:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Description;  SolaceCtrlName = '?stm:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Model_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Model_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Panel1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Access:MODELNUM.UseFile
  Access:STOCK.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  error# = 1
  If f_user_code <> ''
      access:users.clearkey(use:user_code_key)
      use:user_code = f_user_code
      if access:users.fetch(use:user_code_key) = Level:Benign
          If use:location <> ''
              error# = 0
              location_temp = use:location
          End!If use:location <> ''
      End!if access:users.fetch(use:user_code_key) = Level:Benign
  End!If glo:select11 <> ''
  If error# = 1
      Set(defstock)
      Access:defstock.next()
      If dst:site_location <> ''
          location_temp = dst:site_location
      End
  End!If error# = 1
  
  Access:LOCATION.ClearKey(loc:ActiveLocationKey)
  loc:Active   = 1
  loc:Location = Location_Temp
  If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Found
  
  Else!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      Location_Temp = ''
  End!If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign
  
  stm:model_number = Upper(f_model_number)
  
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOMODEL,SELF)
  brw1.SelectWholeRecord = True
  OPEN(QuickWindow)
  SELF.Opened=True
  !Have already got the user
  If use:StockFromLocationOnly
      ?Location_Temp{prop:Disable} = 1
  End !de2:PickEngStockOnly
  
  Select(?Browse:1)
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,stm:Location_Description_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STM:Description,stm:Description,1,BRW1)
  BRW1.AddResetField(location_temp)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,stm:Location_Part_Number_Key)
  BRW1.AddRange(stm:Location,location_temp)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?STM:Part_Number,stm:Part_Number,1,BRW1)
  BRW1.AddResetField(location_temp)
  BIND('GLO:Select12',GLO:Select12)
  BIND('location_temp',location_temp)
  BIND('no_temp',no_temp)
  BIND('model_number_temp',model_number_temp)
  BRW1.AddField(stm:Part_Number,BRW1.Q.stm:Part_Number)
  BRW1.AddField(stm:Description,BRW1.Q.stm:Description)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(stm:Ref_Number,BRW1.Q.stm:Ref_Number)
  BRW1.AddField(stm:Manufacturer,BRW1.Q.stm:Manufacturer)
  BRW1.AddField(stm:Model_Number,BRW1.Q.stm:Model_Number)
  BRW1.AddField(stm:Location,BRW1.Q.stm:Location)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=584
  QuickWindow{PROP:MinHeight}=210
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  FDCB6.Init(location_temp,?location_temp,Queue:FileDropCombo.ViewPosition,FDCB6::View:FileDropCombo,Queue:FileDropCombo,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB6.Q &= Queue:FileDropCombo
  FDCB6.AddSortOrder(loc:ActiveLocationKey)
  FDCB6.AddRange(loc:Active,tmp:one)
  FDCB6.AddField(loc:Location,FDCB6.Q.loc:Location)
  FDCB6.AddField(loc:RecordNumber,FDCB6.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB6.WindowComponent)
  FDCB6.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:6{PROP:TEXT} = 'By Part Number'
    ?Tab2{PROP:TEXT} = 'By Description'
    ?Browse:1{PROP:FORMAT} ='124L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#38R(2)|M~Purchase~L@n7.2@#3#33R(2)|M~Sale~L@n7.2@#4#47R(2)|M~Qty In Stock~L@N8@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Model_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?location_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?location_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?location_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Model_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Part Number~@s30@#1#124L(2)|M~Description~@s30@#2#38R(2)|M~Purchase~L@n7.2@#3#33R(2)|M~Sale~L@n7.2@#4#47R(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab:6{PROP:TEXT} = 'By Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Description~@s30@#2#124L(2)|M~Part Number~@s30@#1#38R(2)|M~Purchase~L@n7.2@#3#33R(2)|M~Sale~L@n7.2@#4#47R(2)|M~Qty In Stock~L@N8@#5#'
          ?Tab2{PROP:TEXT} = 'By Description'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      QuickWindow{prop:text} = 'Available Stock For Model: ' & Clip(f_model_number)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      FDCB6.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW1::Sort0:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Method Executable Code Section) ARG(1, , Init, (BYTE Controls,BYTE Mode))
  stm:model_number = Upper(f_model_number)
  
  
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Method Executable Code Section) ARG(1, , Init, (BYTE Controls,BYTE Mode))


BRW1::Sort1:StepClass.Init PROCEDURE(BYTE Controls,BYTE Mode)

  CODE
  ! Before Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))
  stm:model_number = Upper(f_model_number)
  
  
  PARENT.Init(Controls,Mode)
  ! After Embed Point: %StepManagerMethodCodeSection) DESC(Step Manager Executable Code Section) ARG(1, 1, Init, (BYTE Controls,BYTE Mode))


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Panel1, Resize:FixLeft+Resize:FixTop, Resize:LockHeight)
  SELF.SetStrategy(?location_temp, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?STM:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?STM:Description, Resize:FixLeft+Resize:FixTop, Resize:LockSize)


FDCB6.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

