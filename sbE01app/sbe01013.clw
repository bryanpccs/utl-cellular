

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01013.INC'),ONCE        !Local module procedure declarations
                     END


UpdateProductCode PROCEDURE                           !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::prd:Record  LIKE(prd:RECORD),STATIC
QuickWindow          WINDOW('Inserting A Product Code'),AT(,,220,87),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateProductCode'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,52),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Product Code'),AT(8,20),USE(?PRD:ProductCode:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(prd:ProductCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Warranty Period'),AT(8,36),USE(?prd:WarrantyPeriod:Prompt),TRN,FONT('Tahoma',,,,CHARSET:ANSI)
                           ENTRY(@n4),AT(84,36,64,10),USE(prd:WarrantyPeriod),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Warranty Period - Product Exception'),TIP('Warranty Period - Product Exception'),UPR
                           STRING('(Months)'),AT(154,36),USE(?String1),TRN
                         END
                       END
                       PANEL,AT(4,60,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,64,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(156,64,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?PRD:ProductCode:Prompt{prop:FontColor} = -1
    ?PRD:ProductCode:Prompt{prop:Color} = 15066597
    If ?prd:ProductCode{prop:ReadOnly} = True
        ?prd:ProductCode{prop:FontColor} = 65793
        ?prd:ProductCode{prop:Color} = 15066597
    Elsif ?prd:ProductCode{prop:Req} = True
        ?prd:ProductCode{prop:FontColor} = 65793
        ?prd:ProductCode{prop:Color} = 8454143
    Else ! If ?prd:ProductCode{prop:Req} = True
        ?prd:ProductCode{prop:FontColor} = 65793
        ?prd:ProductCode{prop:Color} = 16777215
    End ! If ?prd:ProductCode{prop:Req} = True
    ?prd:ProductCode{prop:Trn} = 0
    ?prd:ProductCode{prop:FontStyle} = font:Bold
    ?prd:WarrantyPeriod:Prompt{prop:FontColor} = -1
    ?prd:WarrantyPeriod:Prompt{prop:Color} = 15066597
    If ?prd:WarrantyPeriod{prop:ReadOnly} = True
        ?prd:WarrantyPeriod{prop:FontColor} = 65793
        ?prd:WarrantyPeriod{prop:Color} = 15066597
    Elsif ?prd:WarrantyPeriod{prop:Req} = True
        ?prd:WarrantyPeriod{prop:FontColor} = 65793
        ?prd:WarrantyPeriod{prop:Color} = 8454143
    Else ! If ?prd:WarrantyPeriod{prop:Req} = True
        ?prd:WarrantyPeriod{prop:FontColor} = 65793
        ?prd:WarrantyPeriod{prop:Color} = 16777215
    End ! If ?prd:WarrantyPeriod{prop:Req} = True
    ?prd:WarrantyPeriod{prop:Trn} = 0
    ?prd:WarrantyPeriod{prop:FontStyle} = font:Bold
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateProductCode',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateProductCode',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateProductCode',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PRD:ProductCode:Prompt;  SolaceCtrlName = '?PRD:ProductCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?prd:ProductCode;  SolaceCtrlName = '?prd:ProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?prd:WarrantyPeriod:Prompt;  SolaceCtrlName = '?prd:WarrantyPeriod:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?prd:WarrantyPeriod;  SolaceCtrlName = '?prd:WarrantyPeriod';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Product Code'
  OF ChangeRecord
    ActionMessage = 'Changing A Product Code'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateProductCode')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateProductCode')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?PRD:ProductCode:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(prd:Record,History::prd:Record)
  SELF.AddHistoryField(?prd:ProductCode,3)
  SELF.AddHistoryField(?prd:WarrantyPeriod,4)
  SELF.AddUpdateFile(Access:PRODCODE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PRODCODE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PRODCODE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PRODCODE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateProductCode',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateProductCode')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

