

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01023.INC'),ONCE        !Local module procedure declarations
                     END


UpdateSTATUS PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::sts:Record  LIKE(sts:RECORD),STATIC
QuickWindow          WINDOW('Update the STATUS File'),AT(,,328,227),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('UpdateSTATUS'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,320,192),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Heading Ref Number'),AT(8,20),USE(?STS:Heading:Prompt),TRN
                           ENTRY(@n3),AT(84,20,64,10),USE(sts:Heading_Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR
                           CHECK('System Status'),AT(252,20),USE(sts:SystemStatus),LEFT,MSG('This Status Cannot Be Changed Or Deleted'),TIP('This Status Cannot Be Changed Or Deleted'),VALUE('YES','NO')
                           PROMPT('Ref Number'),AT(8,36),USE(?STS:Ref_Number:Prompt),TRN
                           ENTRY(@s3),AT(84,36,64,10),USE(sts:Ref_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Status'),AT(8,52),USE(?STS:Status:Prompt),TRN
                           ENTRY(@s30),AT(84,52,127,10),USE(sts:Status),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Notes'),AT(8,68),USE(?STS:Notes:Prompt),TRN
                           TEXT,AT(84,68,232,52),USE(sts:Notes),FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK('Use Turnaround Time'),AT(84,148,,12),USE(sts:Use_Turnaround_Time),VALUE('YES','NO')
                           CHECK('Enable Pick Notes'),AT(172,149),USE(sts:PickNoteEnable),VALUE('1','0')
                           CHECK('Loan Status'),AT(84,124),USE(sts:Loan),VALUE('YES','NO')
                           GROUP,AT(4,164,168,24),USE(?turnaround_time_group)
                             PROMPT('Days'),AT(84,164),USE(?sts:turnaround_days:prompt),TRN,FONT(,7,,)
                             PROMPT('Hours'),AT(120,164),USE(?sts:turnaround_hours:prompt),TRN,FONT(,7,,)
                             PROMPT('Turnaround Time'),AT(8,172),USE(?sts:use_turnaround_time:2)
                             ENTRY(@n3b),AT(84,172,28,10),USE(sts:Turnaround_Days),DECIMAL(14),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             ENTRY(@n2b),AT(120,172,28,10),USE(sts:Turnaround_Hours),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           END
                           CHECK('Exchange Status'),AT(148,124),USE(sts:Exchange),VALUE('YES','NO')
                           CHECK('Job Status'),AT(224,124),USE(sts:Job),VALUE('YES','NO')
                           CHECK('Display In Engineer''s Browse'),AT(84,136),USE(sts:EngineerStatus),MSG('Display In Engineer''s Browse'),TIP('Display In Engineer''s Browse'),VALUE('1','0')
                         END
                       END
                       BUTTON('&OK'),AT(208,204,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(264,204,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,200,320,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?STS:Heading:Prompt{prop:FontColor} = -1
    ?STS:Heading:Prompt{prop:Color} = 15066597
    If ?sts:Heading_Ref_Number{prop:ReadOnly} = True
        ?sts:Heading_Ref_Number{prop:FontColor} = 65793
        ?sts:Heading_Ref_Number{prop:Color} = 15066597
    Elsif ?sts:Heading_Ref_Number{prop:Req} = True
        ?sts:Heading_Ref_Number{prop:FontColor} = 65793
        ?sts:Heading_Ref_Number{prop:Color} = 8454143
    Else ! If ?sts:Heading_Ref_Number{prop:Req} = True
        ?sts:Heading_Ref_Number{prop:FontColor} = 65793
        ?sts:Heading_Ref_Number{prop:Color} = 16777215
    End ! If ?sts:Heading_Ref_Number{prop:Req} = True
    ?sts:Heading_Ref_Number{prop:Trn} = 0
    ?sts:Heading_Ref_Number{prop:FontStyle} = font:Bold
    ?sts:SystemStatus{prop:Font,3} = -1
    ?sts:SystemStatus{prop:Color} = 15066597
    ?sts:SystemStatus{prop:Trn} = 0
    ?STS:Ref_Number:Prompt{prop:FontColor} = -1
    ?STS:Ref_Number:Prompt{prop:Color} = 15066597
    If ?sts:Ref_Number{prop:ReadOnly} = True
        ?sts:Ref_Number{prop:FontColor} = 65793
        ?sts:Ref_Number{prop:Color} = 15066597
    Elsif ?sts:Ref_Number{prop:Req} = True
        ?sts:Ref_Number{prop:FontColor} = 65793
        ?sts:Ref_Number{prop:Color} = 8454143
    Else ! If ?sts:Ref_Number{prop:Req} = True
        ?sts:Ref_Number{prop:FontColor} = 65793
        ?sts:Ref_Number{prop:Color} = 16777215
    End ! If ?sts:Ref_Number{prop:Req} = True
    ?sts:Ref_Number{prop:Trn} = 0
    ?sts:Ref_Number{prop:FontStyle} = font:Bold
    ?STS:Status:Prompt{prop:FontColor} = -1
    ?STS:Status:Prompt{prop:Color} = 15066597
    If ?sts:Status{prop:ReadOnly} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 15066597
    Elsif ?sts:Status{prop:Req} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 8454143
    Else ! If ?sts:Status{prop:Req} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 16777215
    End ! If ?sts:Status{prop:Req} = True
    ?sts:Status{prop:Trn} = 0
    ?sts:Status{prop:FontStyle} = font:Bold
    ?STS:Notes:Prompt{prop:FontColor} = -1
    ?STS:Notes:Prompt{prop:Color} = 15066597
    If ?sts:Notes{prop:ReadOnly} = True
        ?sts:Notes{prop:FontColor} = 65793
        ?sts:Notes{prop:Color} = 15066597
    Elsif ?sts:Notes{prop:Req} = True
        ?sts:Notes{prop:FontColor} = 65793
        ?sts:Notes{prop:Color} = 8454143
    Else ! If ?sts:Notes{prop:Req} = True
        ?sts:Notes{prop:FontColor} = 65793
        ?sts:Notes{prop:Color} = 16777215
    End ! If ?sts:Notes{prop:Req} = True
    ?sts:Notes{prop:Trn} = 0
    ?sts:Notes{prop:FontStyle} = font:Bold
    ?sts:Use_Turnaround_Time{prop:Font,3} = -1
    ?sts:Use_Turnaround_Time{prop:Color} = 15066597
    ?sts:Use_Turnaround_Time{prop:Trn} = 0
    ?sts:PickNoteEnable{prop:Font,3} = -1
    ?sts:PickNoteEnable{prop:Color} = 15066597
    ?sts:PickNoteEnable{prop:Trn} = 0
    ?sts:Loan{prop:Font,3} = -1
    ?sts:Loan{prop:Color} = 15066597
    ?sts:Loan{prop:Trn} = 0
    ?turnaround_time_group{prop:Font,3} = -1
    ?turnaround_time_group{prop:Color} = 15066597
    ?turnaround_time_group{prop:Trn} = 0
    ?sts:turnaround_days:prompt{prop:FontColor} = -1
    ?sts:turnaround_days:prompt{prop:Color} = 15066597
    ?sts:turnaround_hours:prompt{prop:FontColor} = -1
    ?sts:turnaround_hours:prompt{prop:Color} = 15066597
    ?sts:use_turnaround_time:2{prop:FontColor} = -1
    ?sts:use_turnaround_time:2{prop:Color} = 15066597
    If ?sts:Turnaround_Days{prop:ReadOnly} = True
        ?sts:Turnaround_Days{prop:FontColor} = 65793
        ?sts:Turnaround_Days{prop:Color} = 15066597
    Elsif ?sts:Turnaround_Days{prop:Req} = True
        ?sts:Turnaround_Days{prop:FontColor} = 65793
        ?sts:Turnaround_Days{prop:Color} = 8454143
    Else ! If ?sts:Turnaround_Days{prop:Req} = True
        ?sts:Turnaround_Days{prop:FontColor} = 65793
        ?sts:Turnaround_Days{prop:Color} = 16777215
    End ! If ?sts:Turnaround_Days{prop:Req} = True
    ?sts:Turnaround_Days{prop:Trn} = 0
    ?sts:Turnaround_Days{prop:FontStyle} = font:Bold
    If ?sts:Turnaround_Hours{prop:ReadOnly} = True
        ?sts:Turnaround_Hours{prop:FontColor} = 65793
        ?sts:Turnaround_Hours{prop:Color} = 15066597
    Elsif ?sts:Turnaround_Hours{prop:Req} = True
        ?sts:Turnaround_Hours{prop:FontColor} = 65793
        ?sts:Turnaround_Hours{prop:Color} = 8454143
    Else ! If ?sts:Turnaround_Hours{prop:Req} = True
        ?sts:Turnaround_Hours{prop:FontColor} = 65793
        ?sts:Turnaround_Hours{prop:Color} = 16777215
    End ! If ?sts:Turnaround_Hours{prop:Req} = True
    ?sts:Turnaround_Hours{prop:Trn} = 0
    ?sts:Turnaround_Hours{prop:FontStyle} = font:Bold
    ?sts:Exchange{prop:Font,3} = -1
    ?sts:Exchange{prop:Color} = 15066597
    ?sts:Exchange{prop:Trn} = 0
    ?sts:Job{prop:Font,3} = -1
    ?sts:Job{prop:Color} = 15066597
    ?sts:Job{prop:Trn} = 0
    ?sts:EngineerStatus{prop:Font,3} = -1
    ?sts:EngineerStatus{prop:Color} = 15066597
    ?sts:EngineerStatus{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSTATUS',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateSTATUS',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateSTATUS',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateSTATUS',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateSTATUS',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateSTATUS',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STS:Heading:Prompt;  SolaceCtrlName = '?STS:Heading:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Heading_Ref_Number;  SolaceCtrlName = '?sts:Heading_Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:SystemStatus;  SolaceCtrlName = '?sts:SystemStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STS:Ref_Number:Prompt;  SolaceCtrlName = '?STS:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Ref_Number;  SolaceCtrlName = '?sts:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STS:Status:Prompt;  SolaceCtrlName = '?STS:Status:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Status;  SolaceCtrlName = '?sts:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STS:Notes:Prompt;  SolaceCtrlName = '?STS:Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Notes;  SolaceCtrlName = '?sts:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Use_Turnaround_Time;  SolaceCtrlName = '?sts:Use_Turnaround_Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:PickNoteEnable;  SolaceCtrlName = '?sts:PickNoteEnable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Loan;  SolaceCtrlName = '?sts:Loan';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?turnaround_time_group;  SolaceCtrlName = '?turnaround_time_group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:turnaround_days:prompt;  SolaceCtrlName = '?sts:turnaround_days:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:turnaround_hours:prompt;  SolaceCtrlName = '?sts:turnaround_hours:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:use_turnaround_time:2;  SolaceCtrlName = '?sts:use_turnaround_time:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Turnaround_Days;  SolaceCtrlName = '?sts:Turnaround_Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Turnaround_Hours;  SolaceCtrlName = '?sts:Turnaround_Hours';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Exchange;  SolaceCtrlName = '?sts:Exchange';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Job;  SolaceCtrlName = '?sts:Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:EngineerStatus;  SolaceCtrlName = '?sts:EngineerStatus';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Status Type'
  OF ChangeRecord
    ActionMessage = 'Changing A Status Type'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateSTATUS')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSTATUS')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STS:Heading:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sts:Record,History::sts:Record)
  SELF.AddHistoryField(?sts:Heading_Ref_Number,1)
  SELF.AddHistoryField(?sts:SystemStatus,13)
  SELF.AddHistoryField(?sts:Ref_Number,3)
  SELF.AddHistoryField(?sts:Status,2)
  SELF.AddHistoryField(?sts:Notes,9)
  SELF.AddHistoryField(?sts:Use_Turnaround_Time,4)
  SELF.AddHistoryField(?sts:PickNoteEnable,19)
  SELF.AddHistoryField(?sts:Loan,10)
  SELF.AddHistoryField(?sts:Turnaround_Days,6)
  SELF.AddHistoryField(?sts:Turnaround_Hours,8)
  SELF.AddHistoryField(?sts:Exchange,11)
  SELF.AddHistoryField(?sts:Job,12)
  SELF.AddHistoryField(?sts:EngineerStatus,14)
  SELF.AddUpdateFile(Access:STATUS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STATUS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STATUS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?sts:Use_Turnaround_Time{Prop:Checked} = True
    ENABLE(?turnaround_time_group)
  END
  IF ?sts:Use_Turnaround_Time{Prop:Checked} = False
    DISABLE(?turnaround_time_group)
  END
  IF ?sts:Job{Prop:Checked} = True
    UNHIDE(?sts:EngineerStatus)
  END
  IF ?sts:Job{Prop:Checked} = False
    HIDE(?sts:EngineerStatus)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STATUS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSTATUS',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    sts:Heading_Ref_Number = glo:select2
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sts:Use_Turnaround_Time
      IF ?sts:Use_Turnaround_Time{Prop:Checked} = True
        ENABLE(?turnaround_time_group)
      END
      IF ?sts:Use_Turnaround_Time{Prop:Checked} = False
        DISABLE(?turnaround_time_group)
      END
      ThisWindow.Reset
    OF ?sts:Turnaround_Hours
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sts:Turnaround_Hours, Accepted)
      If sts:Turnaround_Hours < 0 or sts:Turnaround_Hours> 23
          Case MessageEx('The Hours value must be between 0 and 23.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Select(?sts:turnaround_hours)
      End !sts:Turnaround_Hours < 0 or sts:Turnaround_Hours> 23
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sts:Turnaround_Hours, Accepted)
    OF ?sts:Job
      IF ?sts:Job{Prop:Checked} = True
        UNHIDE(?sts:EngineerStatus)
      END
      IF ?sts:Job{Prop:Checked} = False
        HIDE(?sts:EngineerStatus)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSTATUS')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      if securitycheck('PICK PART DEFAULTS') = level:benign
          enable(?sts:PickNoteEnable)
      else!if securitycheck('PICK PART DEFAULTS') = level:benign
          disable(?sts:PickNoteEnable)
      end!if securitycheck('PICK PART DEFAULTS') = level:benign
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

