

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01080.INC'),ONCE        !Local module procedure declarations
                     END


Update_Contact_History PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?cht:Action
act:Action             LIKE(act:Action)               !List box control field - type derived from field
act:Record_Number      LIKE(act:Record_Number)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(ACTION)
                       PROJECT(act:Action)
                       PROJECT(act:Record_Number)
                     END
History::cht:Record  LIKE(cht:RECORD),STATIC
QuickWindow          WINDOW('Update the CONTHIST File'),AT(,,358,204),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Update_Contact_History'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,352,168),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:2)
                           PROMPT('Ref Number'),AT(8,20),USE(?CHT:Ref_Number:Prompt),TRN
                           ENTRY(@s9),AT(84,20,64,10),USE(cht:Ref_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Date'),AT(8,36),USE(?CHT:Date:Prompt),TRN
                           ENTRY(@d6b),AT(84,36,64,10),USE(cht:Date),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           ENTRY(@t1),AT(148,36,64,10),USE(cht:Time),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('User'),AT(8,52),USE(?CHT:User:Prompt),TRN
                           ENTRY(@s3),AT(84,52,40,10),USE(cht:User),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Action'),AT(8,68),USE(?cht:action:prompt)
                           COMBO(@s80),AT(84,68,124,10),USE(cht:Action),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Notes'),AT(8,84),USE(?CHT:Notes:Prompt),TRN
                           TEXT,AT(84,84,268,80),USE(cht:Notes),VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           BUTTON,AT(68,84,10,10),USE(?LookupNotes),SKIP,ICON('List3.ico')
                         END
                       END
                       BUTTON('&OK'),AT(232,180,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(292,180,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,176,352,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?CHT:Ref_Number:Prompt{prop:FontColor} = -1
    ?CHT:Ref_Number:Prompt{prop:Color} = 15066597
    If ?cht:Ref_Number{prop:ReadOnly} = True
        ?cht:Ref_Number{prop:FontColor} = 65793
        ?cht:Ref_Number{prop:Color} = 15066597
    Elsif ?cht:Ref_Number{prop:Req} = True
        ?cht:Ref_Number{prop:FontColor} = 65793
        ?cht:Ref_Number{prop:Color} = 8454143
    Else ! If ?cht:Ref_Number{prop:Req} = True
        ?cht:Ref_Number{prop:FontColor} = 65793
        ?cht:Ref_Number{prop:Color} = 16777215
    End ! If ?cht:Ref_Number{prop:Req} = True
    ?cht:Ref_Number{prop:Trn} = 0
    ?cht:Ref_Number{prop:FontStyle} = font:Bold
    ?CHT:Date:Prompt{prop:FontColor} = -1
    ?CHT:Date:Prompt{prop:Color} = 15066597
    If ?cht:Date{prop:ReadOnly} = True
        ?cht:Date{prop:FontColor} = 65793
        ?cht:Date{prop:Color} = 15066597
    Elsif ?cht:Date{prop:Req} = True
        ?cht:Date{prop:FontColor} = 65793
        ?cht:Date{prop:Color} = 8454143
    Else ! If ?cht:Date{prop:Req} = True
        ?cht:Date{prop:FontColor} = 65793
        ?cht:Date{prop:Color} = 16777215
    End ! If ?cht:Date{prop:Req} = True
    ?cht:Date{prop:Trn} = 0
    ?cht:Date{prop:FontStyle} = font:Bold
    If ?cht:Time{prop:ReadOnly} = True
        ?cht:Time{prop:FontColor} = 65793
        ?cht:Time{prop:Color} = 15066597
    Elsif ?cht:Time{prop:Req} = True
        ?cht:Time{prop:FontColor} = 65793
        ?cht:Time{prop:Color} = 8454143
    Else ! If ?cht:Time{prop:Req} = True
        ?cht:Time{prop:FontColor} = 65793
        ?cht:Time{prop:Color} = 16777215
    End ! If ?cht:Time{prop:Req} = True
    ?cht:Time{prop:Trn} = 0
    ?cht:Time{prop:FontStyle} = font:Bold
    ?CHT:User:Prompt{prop:FontColor} = -1
    ?CHT:User:Prompt{prop:Color} = 15066597
    If ?cht:User{prop:ReadOnly} = True
        ?cht:User{prop:FontColor} = 65793
        ?cht:User{prop:Color} = 15066597
    Elsif ?cht:User{prop:Req} = True
        ?cht:User{prop:FontColor} = 65793
        ?cht:User{prop:Color} = 8454143
    Else ! If ?cht:User{prop:Req} = True
        ?cht:User{prop:FontColor} = 65793
        ?cht:User{prop:Color} = 16777215
    End ! If ?cht:User{prop:Req} = True
    ?cht:User{prop:Trn} = 0
    ?cht:User{prop:FontStyle} = font:Bold
    ?cht:action:prompt{prop:FontColor} = -1
    ?cht:action:prompt{prop:Color} = 15066597
    If ?cht:Action{prop:ReadOnly} = True
        ?cht:Action{prop:FontColor} = 65793
        ?cht:Action{prop:Color} = 15066597
    Elsif ?cht:Action{prop:Req} = True
        ?cht:Action{prop:FontColor} = 65793
        ?cht:Action{prop:Color} = 8454143
    Else ! If ?cht:Action{prop:Req} = True
        ?cht:Action{prop:FontColor} = 65793
        ?cht:Action{prop:Color} = 16777215
    End ! If ?cht:Action{prop:Req} = True
    ?cht:Action{prop:Trn} = 0
    ?cht:Action{prop:FontStyle} = font:Bold
    ?CHT:Notes:Prompt{prop:FontColor} = -1
    ?CHT:Notes:Prompt{prop:Color} = 15066597
    If ?cht:Notes{prop:ReadOnly} = True
        ?cht:Notes{prop:FontColor} = 65793
        ?cht:Notes{prop:Color} = 15066597
    Elsif ?cht:Notes{prop:Req} = True
        ?cht:Notes{prop:FontColor} = 65793
        ?cht:Notes{prop:Color} = 8454143
    Else ! If ?cht:Notes{prop:Req} = True
        ?cht:Notes{prop:FontColor} = 65793
        ?cht:Notes{prop:Color} = 16777215
    End ! If ?cht:Notes{prop:Req} = True
    ?cht:Notes{prop:Trn} = 0
    ?cht:Notes{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Contact_History',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Contact_History',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Contact_History',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Contact_History',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CHT:Ref_Number:Prompt;  SolaceCtrlName = '?CHT:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:Ref_Number;  SolaceCtrlName = '?cht:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CHT:Date:Prompt;  SolaceCtrlName = '?CHT:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:Date;  SolaceCtrlName = '?cht:Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:Time;  SolaceCtrlName = '?cht:Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CHT:User:Prompt;  SolaceCtrlName = '?CHT:User:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:User;  SolaceCtrlName = '?cht:User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:action:prompt;  SolaceCtrlName = '?cht:action:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:Action;  SolaceCtrlName = '?cht:Action';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CHT:Notes:Prompt;  SolaceCtrlName = '?CHT:Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:Notes;  SolaceCtrlName = '?cht:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupNotes;  SolaceCtrlName = '?LookupNotes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Contact'
  OF ChangeRecord
    ActionMessage = 'Changing A Contact'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_Contact_History')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Contact_History')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CHT:Ref_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(cht:Record,History::cht:Record)
  SELF.AddHistoryField(?cht:Ref_Number,2)
  SELF.AddHistoryField(?cht:Date,3)
  SELF.AddHistoryField(?cht:Time,4)
  SELF.AddHistoryField(?cht:User,5)
  SELF.AddHistoryField(?cht:Action,6)
  SELF.AddHistoryField(?cht:Notes,7)
  SELF.AddUpdateFile(Access:CONTHIST)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACTION.Open
  Relate:CONTHIST.Open
  Access:USERS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:CONTHIST
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  If thiswindow.request = Insertrecord
      access:users.clearkey(use:password_key)
      use:password =glo:password
      access:users.fetch(use:password_key)
      cht:user = use:user_code
  End!If thiswindow.request = Insertrecord
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB7.Init(cht:Action,?cht:Action,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:ACTION,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(act:Action_Key)
  FDCB7.AddField(act:Action,FDCB7.Q.act:Action)
  FDCB7.AddField(act:Record_Number,FDCB7.Q.act:Record_Number)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACTION.Close
    Relate:CONTHIST.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Contact_History',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    cht:Date = Today()
    cht:Time = Clock()
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?LookupNotes
      ThisWindow.Update
      GlobalRequest = SelectRecord
      BrowseContactHistoryNotes
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupNotes, Accepted)
      If Records(glo:Q_Notes)
          Loop x# = 1 To Records(glo:Q_Notes)
              Get(glo:Q_Notes,x#)
              If cht:Notes = ''
                  cht:Notes = glo:Notes_Pointer
              Else!If con:Notes = ''
                  cht:Notes = Clip(cht:Notes) & '<13,10>' & glo:Notes_Pointer
              End!If con:Notes = ''
          End!Loop x# = 1 To Records(glo:Q_Notes).
      End!If Records(glo:Q_Notes)
      Display(?cht:Notes)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?LookupNotes, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Contact_History')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

