

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01012.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMODELCOL PROCEDURE (func:Manufacturer)          !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
tmp:ContractOracleCode STRING(30)
tmp:VerifyContractOC STRING(30)
tmp:SaveContractOC   STRING(30)
tmp:PAYTOracleCode   STRING(30)
tmp:VerifyPAYTOC     STRING(30)
tmp:SavePAYTOC       STRING(30)
tmp:ContractActive   BYTE(0)
tmp:PAYTActive       BYTE(0)
tmp:SaveContractActive BYTE(0)
tmp:SavePAYTActive   BYTE(0)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?moc:Colour
col:Colour             LIKE(col:Colour)               !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB9::View:FileDropCombo VIEW(COLOUR)
                       PROJECT(col:Colour)
                     END
History::moc:Record  LIKE(moc:RECORD),STATIC
QuickWindow          WINDOW('Update the MODELCOL File'),AT(,,228,198),FONT('Tahoma',8,,,CHARSET:ANSI),CENTER,IMM,HLP('UpdateMODELCOL'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,220,162),USE(?CurrentTab),SPREAD
                         TAB('Colour Details'),USE(?Tab:1)
                           STRING('Model'),AT(8,20),USE(?String1),TRN
                           STRING(@s30),AT(76,20),USE(moc:Model_Number),TRN,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Colour'),AT(8,34),USE(?Prompt1),TRN
                           COMBO(@s30),AT(76,34,120,10),USE(moc:Colour),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M@s30@'),DROP(10,120),FROM(Queue:FileDropCombo)
                           GROUP('Contract'),AT(8,48,212,54),USE(?ContractGroup),BOXED,TRN
                             CHECK('Active'),AT(76,56),USE(tmp:ContractActive),TRN,MSG('Active'),TIP('Active'),VALUE('1','0')
                           END
                           PROMPT('Oracle Code'),AT(16,70),USE(?tmp:ContractOracleCode:Prompt),TRN
                           ENTRY(@s30),AT(76,70,120,10),USE(tmp:ContractOracleCode),FONT(,,,FONT:bold,CHARSET:ANSI),REQ,UPR
                           PROMPT('Oracle Code'),AT(16,86),USE(?tmp:VerifyContractOC:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(76,86,120,10),USE(tmp:VerifyContractOC),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),UPR,PASSWORD
                           GROUP('PAYT'),AT(8,102,212,54),USE(?PAYTGroup),BOXED,TRN
                             CHECK('Active'),AT(76,111),USE(tmp:PAYTActive),TRN,MSG('Active'),TIP('Active'),VALUE('1','0')
                             PROMPT('Oracle Code'),AT(16,124),USE(?tmp:PAYTOracleCode:Prompt),TRN
                             ENTRY(@s30),AT(76,124,120,10),USE(tmp:PAYTOracleCode),FONT(,,,FONT:bold,CHARSET:ANSI),REQ,UPR
                             PROMPT('Oracle Code'),AT(16,140),USE(?tmp:VerifyPAYTOC:Prompt),TRN,HIDE
                             ENTRY(@s30),AT(76,140,120,10),USE(tmp:VerifyPAYTOC),HIDE,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(080FFFFH),UPR,PASSWORD
                           END
                         END
                       END
                       PANEL,AT(4,170,220,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('OK'),AT(108,174,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(164,174,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB9                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?moc:Model_Number{prop:FontColor} = -1
    ?moc:Model_Number{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?moc:Colour{prop:ReadOnly} = True
        ?moc:Colour{prop:FontColor} = 65793
        ?moc:Colour{prop:Color} = 15066597
    Elsif ?moc:Colour{prop:Req} = True
        ?moc:Colour{prop:FontColor} = 65793
        ?moc:Colour{prop:Color} = 8454143
    Else ! If ?moc:Colour{prop:Req} = True
        ?moc:Colour{prop:FontColor} = 65793
        ?moc:Colour{prop:Color} = 16777215
    End ! If ?moc:Colour{prop:Req} = True
    ?moc:Colour{prop:Trn} = 0
    ?moc:Colour{prop:FontStyle} = font:Bold
    ?ContractGroup{prop:Font,3} = -1
    ?ContractGroup{prop:Color} = 15066597
    ?ContractGroup{prop:Trn} = 0
    ?tmp:ContractActive{prop:Font,3} = -1
    ?tmp:ContractActive{prop:Color} = 15066597
    ?tmp:ContractActive{prop:Trn} = 0
    ?tmp:ContractOracleCode:Prompt{prop:FontColor} = -1
    ?tmp:ContractOracleCode:Prompt{prop:Color} = 15066597
    If ?tmp:ContractOracleCode{prop:ReadOnly} = True
        ?tmp:ContractOracleCode{prop:FontColor} = 65793
        ?tmp:ContractOracleCode{prop:Color} = 15066597
    Elsif ?tmp:ContractOracleCode{prop:Req} = True
        ?tmp:ContractOracleCode{prop:FontColor} = 65793
        ?tmp:ContractOracleCode{prop:Color} = 8454143
    Else ! If ?tmp:ContractOracleCode{prop:Req} = True
        ?tmp:ContractOracleCode{prop:FontColor} = 65793
        ?tmp:ContractOracleCode{prop:Color} = 16777215
    End ! If ?tmp:ContractOracleCode{prop:Req} = True
    ?tmp:ContractOracleCode{prop:Trn} = 0
    ?tmp:ContractOracleCode{prop:FontStyle} = font:Bold
    ?tmp:VerifyContractOC:Prompt{prop:FontColor} = -1
    ?tmp:VerifyContractOC:Prompt{prop:Color} = 15066597
    If ?tmp:VerifyContractOC{prop:ReadOnly} = True
        ?tmp:VerifyContractOC{prop:FontColor} = 65793
        ?tmp:VerifyContractOC{prop:Color} = 15066597
    Elsif ?tmp:VerifyContractOC{prop:Req} = True
        ?tmp:VerifyContractOC{prop:FontColor} = 65793
        ?tmp:VerifyContractOC{prop:Color} = 8454143
    Else ! If ?tmp:VerifyContractOC{prop:Req} = True
        ?tmp:VerifyContractOC{prop:FontColor} = 65793
        ?tmp:VerifyContractOC{prop:Color} = 16777215
    End ! If ?tmp:VerifyContractOC{prop:Req} = True
    ?tmp:VerifyContractOC{prop:Trn} = 0
    ?tmp:VerifyContractOC{prop:FontStyle} = font:Bold
    ?PAYTGroup{prop:Font,3} = -1
    ?PAYTGroup{prop:Color} = 15066597
    ?PAYTGroup{prop:Trn} = 0
    ?tmp:PAYTActive{prop:Font,3} = -1
    ?tmp:PAYTActive{prop:Color} = 15066597
    ?tmp:PAYTActive{prop:Trn} = 0
    ?tmp:PAYTOracleCode:Prompt{prop:FontColor} = -1
    ?tmp:PAYTOracleCode:Prompt{prop:Color} = 15066597
    If ?tmp:PAYTOracleCode{prop:ReadOnly} = True
        ?tmp:PAYTOracleCode{prop:FontColor} = 65793
        ?tmp:PAYTOracleCode{prop:Color} = 15066597
    Elsif ?tmp:PAYTOracleCode{prop:Req} = True
        ?tmp:PAYTOracleCode{prop:FontColor} = 65793
        ?tmp:PAYTOracleCode{prop:Color} = 8454143
    Else ! If ?tmp:PAYTOracleCode{prop:Req} = True
        ?tmp:PAYTOracleCode{prop:FontColor} = 65793
        ?tmp:PAYTOracleCode{prop:Color} = 16777215
    End ! If ?tmp:PAYTOracleCode{prop:Req} = True
    ?tmp:PAYTOracleCode{prop:Trn} = 0
    ?tmp:PAYTOracleCode{prop:FontStyle} = font:Bold
    ?tmp:VerifyPAYTOC:Prompt{prop:FontColor} = -1
    ?tmp:VerifyPAYTOC:Prompt{prop:Color} = 15066597
    If ?tmp:VerifyPAYTOC{prop:ReadOnly} = True
        ?tmp:VerifyPAYTOC{prop:FontColor} = 65793
        ?tmp:VerifyPAYTOC{prop:Color} = 15066597
    Elsif ?tmp:VerifyPAYTOC{prop:Req} = True
        ?tmp:VerifyPAYTOC{prop:FontColor} = 65793
        ?tmp:VerifyPAYTOC{prop:Color} = 8454143
    Else ! If ?tmp:VerifyPAYTOC{prop:Req} = True
        ?tmp:VerifyPAYTOC{prop:FontColor} = 65793
        ?tmp:VerifyPAYTOC{prop:Color} = 16777215
    End ! If ?tmp:VerifyPAYTOC{prop:Req} = True
    ?tmp:VerifyPAYTOC{prop:Trn} = 0
    ?tmp:VerifyPAYTOC{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMODELCOL',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMODELCOL',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateMODELCOL',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:ContractOracleCode',tmp:ContractOracleCode,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:VerifyContractOC',tmp:VerifyContractOC,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:SaveContractOC',tmp:SaveContractOC,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:PAYTOracleCode',tmp:PAYTOracleCode,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:VerifyPAYTOC',tmp:VerifyPAYTOC,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:SavePAYTOC',tmp:SavePAYTOC,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:ContractActive',tmp:ContractActive,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:PAYTActive',tmp:PAYTActive,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:SaveContractActive',tmp:SaveContractActive,'UpdateMODELCOL',1)
    SolaceViewVars('tmp:SavePAYTActive',tmp:SavePAYTActive,'UpdateMODELCOL',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?moc:Model_Number;  SolaceCtrlName = '?moc:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?moc:Colour;  SolaceCtrlName = '?moc:Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ContractGroup;  SolaceCtrlName = '?ContractGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ContractActive;  SolaceCtrlName = '?tmp:ContractActive';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ContractOracleCode:Prompt;  SolaceCtrlName = '?tmp:ContractOracleCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ContractOracleCode;  SolaceCtrlName = '?tmp:ContractOracleCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:VerifyContractOC:Prompt;  SolaceCtrlName = '?tmp:VerifyContractOC:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:VerifyContractOC;  SolaceCtrlName = '?tmp:VerifyContractOC';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PAYTGroup;  SolaceCtrlName = '?PAYTGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PAYTActive;  SolaceCtrlName = '?tmp:PAYTActive';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PAYTOracleCode:Prompt;  SolaceCtrlName = '?tmp:PAYTOracleCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PAYTOracleCode;  SolaceCtrlName = '?tmp:PAYTOracleCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:VerifyPAYTOC:Prompt;  SolaceCtrlName = '?tmp:VerifyPAYTOC:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:VerifyPAYTOC;  SolaceCtrlName = '?tmp:VerifyPAYTOC';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Model Specific Colour'
  OF ChangeRecord
    ActionMessage = 'Changing A Model Specific Colour'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMODELCOL')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMODELCOL')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(moc:Record,History::moc:Record)
  SELF.AddHistoryField(?moc:Model_Number,2)
  SELF.AddHistoryField(?moc:Colour,3)
  SELF.AddUpdateFile(Access:MODELCOL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:CARISMA.Open
  Relate:COLOUR.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:MODELCOL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Access:CARISMA.ClearKey(cma:ManufactModColourKey)
  cma:Manufacturer = func:Manufacturer
  cma:ModelNo = moc:Model_Number
  cma:Colour = moc:Colour
  if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
      tmp:ContractOracleCode = cma:ContractOracleCode
      tmp:PAYTOracleCode = cma:PAYTOracleCode
      tmp:ContractActive = cma:ContractActive
      tmp:PAYTActive = cma:PAYTActive
  end
  
  tmp:SaveContractOC = tmp:ContractOracleCode
  tmp:SavePAYTOC = tmp:PAYTOracleCode
  tmp:SaveContractActive = tmp:ContractActive
  tmp:SavePAYTActive = tmp:PAYTActive
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?tmp:ContractActive{Prop:Checked} = True
    UNHIDE(?tmp:ContractOracleCode:Prompt)
    UNHIDE(?tmp:ContractOracleCode)
  END
  IF ?tmp:ContractActive{Prop:Checked} = False
    HIDE(?tmp:ContractOracleCode:Prompt)
    HIDE(?tmp:ContractOracleCode)
    HIDE(?tmp:VerifyContractOC:Prompt)
    HIDE(?tmp:VerifyContractOC)
  END
  IF ?tmp:PAYTActive{Prop:Checked} = True
    UNHIDE(?tmp:PAYTOracleCode:Prompt)
    UNHIDE(?tmp:PAYTOracleCode)
  END
  IF ?tmp:PAYTActive{Prop:Checked} = False
    HIDE(?tmp:PAYTOracleCode:Prompt)
    HIDE(?tmp:PAYTOracleCode)
    HIDE(?tmp:VerifyPAYTOC:Prompt)
    HIDE(?tmp:VerifyPAYTOC)
  END
  SELF.AddItem(ToolbarForm)
  FDCB9.Init(moc:Colour,?moc:Colour,Queue:FileDropCombo.ViewPosition,FDCB9::View:FileDropCombo,Queue:FileDropCombo,Relate:COLOUR,ThisWindow,GlobalErrors,0,1,0)
  FDCB9.Q &= Queue:FileDropCombo
  FDCB9.AddSortOrder(col:Colour_Key)
  FDCB9.AddField(col:Colour,FDCB9.Q.col:Colour)
  FDCB9.AddUpdateField(col:Colour,moc:Colour)
  ThisWindow.AddItem(FDCB9.WindowComponent)
  FDCB9.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CARISMA.Close
    Relate:COLOUR.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMODELCOL',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?tmp:ContractOracleCode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ContractOracleCode, Accepted)
      If tmp:ContractOracleCode <> tmp:SaveContractOC
          Access:CARISMA.ClearKey(cma:ContractCodeKey)
          cma:ContractOracleCode = tmp:ContractOracleCode
          If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign
              !Found
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('The entered Contract Oracle Code has already been applied to the follow Model:'&|
                  '|' & Clip(cma:Manufacturer) & ''&|
                  '|' & Clip(cma:ModelNo) & ''&|
                  '|' & Clip(cma:Colour) & |
                  '|' & 'CONTRACT','Error',|
                             icon:Hand,'&OK',1,1) 
                  Of 1 ! &OK Button
              End!Case Message
              tmp:ContractOracleCode = ''
              select(?tmp:ContractOracleCode)
              Display()
              Cycle
          Else ! If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign
              !Error - oracle code must be unique across both Contract/PAYT
              Access:CARISMA.ClearKey(cma:PAYTCodeKey)
              cma:PAYTOracleCode = tmp:ContractOracleCode
              If Access:CARISMA.TryFetch(cma:PAYTCodeKey) = Level:Benign
                  !Found
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Message('The entered Contract Oracle Code has already been applied to the follow Model:'&|
                      '|' & Clip(cma:Manufacturer) & ''&|
                      '|' & Clip(cma:ModelNo) & ''&|
                      '|' & Clip(cma:Colour) & |
                      '|' & 'PAYT','Error',|
                                 icon:Hand,'&OK',1,1)
                      Of 1 ! &OK Button
                  End!Case Message
                  tmp:ContractOracleCode = ''
                  select(?tmp:ContractOracleCode)
                  Display()
                  Cycle
              End
          End ! If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign
      
          if tmp:ContractOracleCode = tmp:PAYTOracleCode
              !Found
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('The entered Contract Oracle Code has already been applied to this model.', 'Error' ,|
                           icon:Hand,'&OK',1,1)
                  Of 1 ! &OK Button
              End!Case Message
              tmp:ContractOracleCode = ''
              select(?tmp:ContractOracleCode)
              Display()
              Cycle
          end
      
          ?tmp:VerifyContractOC{prop:Hide} = False
          ?tmp:VerifyContractOC:Prompt{prop:Hide} = False
          Select(?tmp:VerifyContractOC)
      End ! If tmp:ContractOracleCode <> tmp:SaveContractOC
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ContractOracleCode, Accepted)
    OF ?tmp:VerifyContractOC
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:VerifyContractOC, Accepted)
      If (?tmp:VerifyContractOC{Prop:Hide} = false) and (tmp:VerifyContractOC <> tmp:ContractOracleCode)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('The entered Contract Oracle codes do not match.','Error',|
                         icon:Hand,'&OK',1,1) 
              Of 1 ! &OK Button
          End!Case Message
          tmp:VerifyContractOC = ''
          Select(?tmp:ContractOracleCode)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:VerifyContractOC, Accepted)
    OF ?tmp:PAYTOracleCode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PAYTOracleCode, Accepted)
      If tmp:PAYTOracleCode <> tmp:SavePAYTOC
          Access:CARISMA.ClearKey(cma:PAYTCodeKey)
          cma:PAYTOracleCode = tmp:PAYTOracleCode
          If Access:CARISMA.TryFetch(cma:PAYTCodeKey) = Level:Benign
              !Found
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('The entered PAYT Oracle Code has already been applied to the follow Model:'&|
                  '|' & Clip(cma:Manufacturer) & ''&|
                  '|' & Clip(cma:ModelNo) & ''&|
                  '|' & Clip(cma:Colour) & |
                  '|' & 'PAYT','Error',|
                             icon:Hand,'&OK',1,1) 
                  Of 1 ! &OK Button
              End!Case Message
              tmp:PAYTOracleCode = ''
              select(?tmp:PAYTOracleCode)
              Display()
              Cycle
          Else ! If Access:CARISMA.TryFetch(cma:PAYTCodeKey) = Level:Benign
              !Error - oracle code must be unique across both Contract/PAYT
              Access:CARISMA.ClearKey(cma:ContractCodeKey)
              cma:ContractOracleCode = tmp:PAYTOracleCode
              If Access:CARISMA.TryFetch(cma:ContractCodeKey) = Level:Benign
                  !Found
                  Beep(Beep:SystemHand)  ;  Yield()
                  Case Message('The entered PAYT Oracle Code has already been applied to the follow Model:'&|
                      '|' & Clip(cma:Manufacturer) & ''&|
                      '|' & Clip(cma:ModelNo) & ''&|
                      '|' & Clip(cma:Colour) & |
                      '|' & 'CONTRACT','Error',|
                                 icon:Hand,'&OK',1,1)
                      Of 1 ! &OK Button
                  End!Case Message
                  tmp:PAYTOracleCode = ''
                  select(?tmp:PAYTOracleCode)
                  Display()
                  Cycle
              End
          End ! If Access:CARISMA.TryFetch(cma:PAYTCodeKey) = Level:Benign
      
          if tmp:ContractOracleCode = tmp:PAYTOracleCode
              !Found
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('The entered PAYT Oracle Code has already been applied to this model.', 'Error' ,|
                           icon:Hand,'&OK',1,1)
                  Of 1 ! &OK Button
              End!Case Message
              tmp:PAYTOracleCode = ''
              select(?tmp:PAYTOracleCode)
              Display()
              Cycle
          end
      
          ?tmp:VerifyPAYTOC{prop:Hide} = False
          ?tmp:VerifyPAYTOC:Prompt{prop:Hide} = False
          Select(?tmp:VerifyPAYTOC)
      End ! If tmp:PAYTOracleCode <> tmp:SavePAYTOC
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PAYTOracleCode, Accepted)
    OF ?tmp:VerifyPAYTOC
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:VerifyPAYTOC, Accepted)
      If (?tmp:VerifyPAYTOC{Prop:Hide} = false) and (tmp:VerifyPAYTOC <> tmp:PAYTOracleCode)
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('The entered PAYT Oracle codes do not match.','Error',|
                         icon:Hand,'&OK',1,1) 
              Of 1 ! &OK Button
          End!Case Message
          tmp:VerifyPAYTOC = ''
          Select(?tmp:PAYTOracleCode)
          Display()
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:VerifyPAYTOC, Accepted)
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      ! OK Button
      
      if moc:Colour = ''
          select(?moc:Colour)
          cycle
      end
      
      If (tmp:ContractActive = 0 And tmp:PAYTActive = 0) Or (tmp:ContractOracleCode = '' And tmp:PAYTOracleCode = '')
          Beep(Beep:SystemHand)  ;  Yield()
          Case Message('An Oracle Code must be configured against this device / colour combination.'&|
              '|'&|
              '|Please configure either the Contract or PAYT Oracle codes before saving this record.','ServiceBase',|
                         Icon:Hand,'&OK',1) 
              Of 1 ! &OK Button
          End!Case Message
          Cycle
      End ! If tmp:ContractOracleCode = '' And tmp:PAYTOracleCode = ''
      
      if tmp:ContractOracleCode = '' And tmp:ContractActive
          select(?tmp:ContractOracleCode)
          cycle
      end
      
      if tmp:PAYTOracleCode = '' And tmp:PAYTActive
          select(?tmp:PAYTOracleCode)
          cycle
      end
      
      ! Inserting (DBH 09/07/2008) # 100003 - Blank fields if not active
      If tmp:ContractActive = 0
          tmp:ContractOracleCode = ''
          tmp:SaveContractOC = ''
      End ! If tmp:ContractActive = 0
      If tmp:PAYTActive = 0
          tmp:PAYTOracleCode = ''
          tmp:SavePAYTOC = ''
      End ! If tmp:PAYTActive = 0
      ! End (DBH 09/07/2008) #100003
      
      
      If tmp:ContractOracleCode <> tmp:SaveContractOC
          If tmp:VerifyContractOC = ''
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('Please verify the Contract Oracle Code.','Error',|
                            icon:Hand,'&OK',1,1)
                  Of 1 ! &OK Button
              End !Case Message
              Select(?tmp:VerifyContractOC)
              Display()
              Cycle
          End
      End
      
      If tmp:PAYTOracleCode <> tmp:SavePAYTOC
          If tmp:VerifyPAYTOC = ''
              Beep(Beep:SystemHand)  ;  Yield()
              Case Message('Please verify the PAYT Oracle Code.','Error',|
                            icon:Hand,'&OK',1,1)
                  Of 1 ! &OK Button
              End !Case Message
              Select(?tmp:VerifyPAYTOC)
              Display()
              Cycle
          End
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:ContractActive
      IF ?tmp:ContractActive{Prop:Checked} = True
        UNHIDE(?tmp:ContractOracleCode:Prompt)
        UNHIDE(?tmp:ContractOracleCode)
      END
      IF ?tmp:ContractActive{Prop:Checked} = False
        HIDE(?tmp:ContractOracleCode:Prompt)
        HIDE(?tmp:ContractOracleCode)
        HIDE(?tmp:VerifyContractOC:Prompt)
        HIDE(?tmp:VerifyContractOC)
      END
      ThisWindow.Reset
    OF ?tmp:PAYTActive
      IF ?tmp:PAYTActive{Prop:Checked} = True
        UNHIDE(?tmp:PAYTOracleCode:Prompt)
        UNHIDE(?tmp:PAYTOracleCode)
      END
      IF ?tmp:PAYTActive{Prop:Checked} = False
        HIDE(?tmp:PAYTOracleCode:Prompt)
        HIDE(?tmp:PAYTOracleCode)
        HIDE(?tmp:VerifyPAYTOC:Prompt)
        HIDE(?tmp:VerifyPAYTOC)
      END
      ThisWindow.Reset
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeCompleted()
  if ReturnValue = Level:Benign
      ! This prevents a record being created if a duplicate key error (same colour) is encountered
      Access:CARISMA.ClearKey(cma:ManufactModColourKey)
      cma:Manufacturer = func:Manufacturer
      cma:ModelNo = moc:Model_Number
      cma:Colour = moc:Colour
      if Access:CARISMA.Fetch(cma:ManufactModColourKey) = Level:Benign
          cma:ContractOracleCode  = tmp:ContractOracleCode
          cma:PAYTOracleCode      = tmp:PAYTOracleCode
          cma:ContractActive      = tmp:ContractActive
          cma:PAYTActive          = tmp:PAYTActive
          Access:CARISMA.Update()
      else
          if Access:CARISMA.PrimeRecord() = Level:Benign
              cma:Manufacturer        = func:Manufacturer
              cma:ModelNo             = moc:Model_Number
              cma:Colour              = moc:Colour
              cma:ContractOracleCode  = tmp:ContractOracleCode
              cma:PAYTOracleCode      = tmp:PAYTOracleCode
              cma:ContractActive      = tmp:ContractActive
              cma:PAYTActive          = tmp:PAYTActive
              if Access:CARISMA.Update() <> Level:Benign
                  Access:CARISMA.CancelAutoInc()
              end
          end
      end
  end
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMODELCOL')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

