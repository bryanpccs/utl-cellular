

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01087.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTRAFAULO PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::tfo:Record  LIKE(tfo:RECORD),STATIC
QuickWindow          WINDOW('Update the TRAFAULO File'),AT(,,320,112),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateTRAFAULO'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,312,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?TFO:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(68,20,40,10),USE(tfo:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Account Number'),AT(8,34),USE(?TFO:AccountNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(68,34,124,10),USE(tfo:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Account Number'),TIP('Account Number'),UPR
                           PROMPT('Field Number'),AT(8,48),USE(?TFO:Field_Number:Prompt),TRN
                           ENTRY(@n2),AT(68,48,40,10),USE(tfo:Field_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Field'),AT(8,62),USE(?TFO:Field:Prompt),TRN
                           ENTRY(@s30),AT(68,62,124,10),USE(tfo:Field),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Description'),AT(8,76),USE(?TFO:Description:Prompt),TRN
                           ENTRY(@s60),AT(68,76,244,10),USE(tfo:Description),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('OK'),AT(173,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(222,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(271,94,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?TFO:RecordNumber:Prompt{prop:FontColor} = -1
    ?TFO:RecordNumber:Prompt{prop:Color} = 15066597
    If ?tfo:RecordNumber{prop:ReadOnly} = True
        ?tfo:RecordNumber{prop:FontColor} = 65793
        ?tfo:RecordNumber{prop:Color} = 15066597
    Elsif ?tfo:RecordNumber{prop:Req} = True
        ?tfo:RecordNumber{prop:FontColor} = 65793
        ?tfo:RecordNumber{prop:Color} = 8454143
    Else ! If ?tfo:RecordNumber{prop:Req} = True
        ?tfo:RecordNumber{prop:FontColor} = 65793
        ?tfo:RecordNumber{prop:Color} = 16777215
    End ! If ?tfo:RecordNumber{prop:Req} = True
    ?tfo:RecordNumber{prop:Trn} = 0
    ?tfo:RecordNumber{prop:FontStyle} = font:Bold
    ?TFO:AccountNumber:Prompt{prop:FontColor} = -1
    ?TFO:AccountNumber:Prompt{prop:Color} = 15066597
    If ?tfo:AccountNumber{prop:ReadOnly} = True
        ?tfo:AccountNumber{prop:FontColor} = 65793
        ?tfo:AccountNumber{prop:Color} = 15066597
    Elsif ?tfo:AccountNumber{prop:Req} = True
        ?tfo:AccountNumber{prop:FontColor} = 65793
        ?tfo:AccountNumber{prop:Color} = 8454143
    Else ! If ?tfo:AccountNumber{prop:Req} = True
        ?tfo:AccountNumber{prop:FontColor} = 65793
        ?tfo:AccountNumber{prop:Color} = 16777215
    End ! If ?tfo:AccountNumber{prop:Req} = True
    ?tfo:AccountNumber{prop:Trn} = 0
    ?tfo:AccountNumber{prop:FontStyle} = font:Bold
    ?TFO:Field_Number:Prompt{prop:FontColor} = -1
    ?TFO:Field_Number:Prompt{prop:Color} = 15066597
    If ?tfo:Field_Number{prop:ReadOnly} = True
        ?tfo:Field_Number{prop:FontColor} = 65793
        ?tfo:Field_Number{prop:Color} = 15066597
    Elsif ?tfo:Field_Number{prop:Req} = True
        ?tfo:Field_Number{prop:FontColor} = 65793
        ?tfo:Field_Number{prop:Color} = 8454143
    Else ! If ?tfo:Field_Number{prop:Req} = True
        ?tfo:Field_Number{prop:FontColor} = 65793
        ?tfo:Field_Number{prop:Color} = 16777215
    End ! If ?tfo:Field_Number{prop:Req} = True
    ?tfo:Field_Number{prop:Trn} = 0
    ?tfo:Field_Number{prop:FontStyle} = font:Bold
    ?TFO:Field:Prompt{prop:FontColor} = -1
    ?TFO:Field:Prompt{prop:Color} = 15066597
    If ?tfo:Field{prop:ReadOnly} = True
        ?tfo:Field{prop:FontColor} = 65793
        ?tfo:Field{prop:Color} = 15066597
    Elsif ?tfo:Field{prop:Req} = True
        ?tfo:Field{prop:FontColor} = 65793
        ?tfo:Field{prop:Color} = 8454143
    Else ! If ?tfo:Field{prop:Req} = True
        ?tfo:Field{prop:FontColor} = 65793
        ?tfo:Field{prop:Color} = 16777215
    End ! If ?tfo:Field{prop:Req} = True
    ?tfo:Field{prop:Trn} = 0
    ?tfo:Field{prop:FontStyle} = font:Bold
    ?TFO:Description:Prompt{prop:FontColor} = -1
    ?TFO:Description:Prompt{prop:Color} = 15066597
    If ?tfo:Description{prop:ReadOnly} = True
        ?tfo:Description{prop:FontColor} = 65793
        ?tfo:Description{prop:Color} = 15066597
    Elsif ?tfo:Description{prop:Req} = True
        ?tfo:Description{prop:FontColor} = 65793
        ?tfo:Description{prop:Color} = 8454143
    Else ! If ?tfo:Description{prop:Req} = True
        ?tfo:Description{prop:FontColor} = 65793
        ?tfo:Description{prop:Color} = 16777215
    End ! If ?tfo:Description{prop:Req} = True
    ?tfo:Description{prop:Trn} = 0
    ?tfo:Description{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTRAFAULO',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTRAFAULO',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTRAFAULO',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFO:RecordNumber:Prompt;  SolaceCtrlName = '?TFO:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfo:RecordNumber;  SolaceCtrlName = '?tfo:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFO:AccountNumber:Prompt;  SolaceCtrlName = '?TFO:AccountNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfo:AccountNumber;  SolaceCtrlName = '?tfo:AccountNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFO:Field_Number:Prompt;  SolaceCtrlName = '?TFO:Field_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfo:Field_Number;  SolaceCtrlName = '?tfo:Field_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFO:Field:Prompt;  SolaceCtrlName = '?TFO:Field:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfo:Field;  SolaceCtrlName = '?tfo:Field';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TFO:Description:Prompt;  SolaceCtrlName = '?TFO:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tfo:Description;  SolaceCtrlName = '?tfo:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a TRAFAULO Record'
  OF ChangeRecord
    ActionMessage = 'Changing a TRAFAULO Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTRAFAULO')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTRAFAULO')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TFO:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(tfo:Record,History::tfo:Record)
  SELF.AddHistoryField(?tfo:RecordNumber,1)
  SELF.AddHistoryField(?tfo:AccountNumber,2)
  SELF.AddHistoryField(?tfo:Field_Number,3)
  SELF.AddHistoryField(?tfo:Field,4)
  SELF.AddHistoryField(?tfo:Description,5)
  SELF.AddUpdateFile(Access:TRAFAULO)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRAFAULO.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRAFAULO
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRAFAULO.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTRAFAULO',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTRAFAULO')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

