

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01081.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Contact_History PROCEDURE                      !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(CONTHIST)
                       PROJECT(cht:Date)
                       PROJECT(cht:Time)
                       PROJECT(cht:User)
                       PROJECT(cht:Action)
                       PROJECT(cht:Notes)
                       PROJECT(cht:record_number)
                       PROJECT(cht:Ref_Number)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
cht:Date               LIKE(cht:Date)                 !List box control field - type derived from field
cht:Time               LIKE(cht:Time)                 !List box control field - type derived from field
cht:User               LIKE(cht:User)                 !List box control field - type derived from field
cht:Action             LIKE(cht:Action)               !List box control field - type derived from field
cht:Notes              LIKE(cht:Notes)                !Browse hot field - type derived from field
cht:record_number      LIKE(cht:record_number)        !Primary key field - type derived from field
cht:Ref_Number         LIKE(cht:Ref_Number)           !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Contact History File'),AT(,,417,254),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Audit'),SYSTEM,GRAY,MAX,RESIZE
                       LIST,AT(8,36,320,116),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('50R(2)|M~Date~L@d6b@26R(2)|M~Time~@t1@23L(2)|M~User~@s3@80L(2)|M~Action~@s80@'),FROM(Queue:Browse:1)
                       PROMPT('Notes'),AT(8,156),USE(?AUD:Notes:Prompt),TRN,FONT('Tahoma',8,COLOR:Navy,FONT:bold)
                       BUTTON('&Insert'),AT(336,156,76,20),USE(?Insert),LEFT,ICON('Insert.ico')
                       TEXT,AT(8,168,320,80),USE(cht:Notes),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                       BUTTON('&Change'),AT(336,180,76,20),USE(?Change),LEFT,ICON('Edit.ico')
                       BUTTON('&Delete'),AT(336,204,76,20),USE(?Delete),LEFT,ICON('Delete.ico')
                       SHEET,AT(4,4,328,248),USE(?CurrentTab),SPREAD
                         TAB('By Date'),USE(?Tab:2)
                         END
                         TAB('By Action'),USE(?Tab2)
                           ENTRY(@s80),AT(8,20,124,10),USE(cht:Action),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                         TAB('By User'),USE(?Tab3)
                           ENTRY(@s3),AT(8,20,64,10),USE(cht:User),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                       END
                       BUTTON('Close'),AT(336,232,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?AUD:Notes:Prompt{prop:FontColor} = -1
    ?AUD:Notes:Prompt{prop:Color} = 15066597
    If ?cht:Notes{prop:ReadOnly} = True
        ?cht:Notes{prop:FontColor} = 65793
        ?cht:Notes{prop:Color} = 15066597
    Elsif ?cht:Notes{prop:Req} = True
        ?cht:Notes{prop:FontColor} = 65793
        ?cht:Notes{prop:Color} = 8454143
    Else ! If ?cht:Notes{prop:Req} = True
        ?cht:Notes{prop:FontColor} = 65793
        ?cht:Notes{prop:Color} = 16777215
    End ! If ?cht:Notes{prop:Req} = True
    ?cht:Notes{prop:Trn} = 0
    ?cht:Notes{prop:FontStyle} = font:Bold
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?cht:Action{prop:ReadOnly} = True
        ?cht:Action{prop:FontColor} = 65793
        ?cht:Action{prop:Color} = 15066597
    Elsif ?cht:Action{prop:Req} = True
        ?cht:Action{prop:FontColor} = 65793
        ?cht:Action{prop:Color} = 8454143
    Else ! If ?cht:Action{prop:Req} = True
        ?cht:Action{prop:FontColor} = 65793
        ?cht:Action{prop:Color} = 16777215
    End ! If ?cht:Action{prop:Req} = True
    ?cht:Action{prop:Trn} = 0
    ?cht:Action{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?cht:User{prop:ReadOnly} = True
        ?cht:User{prop:FontColor} = 65793
        ?cht:User{prop:Color} = 15066597
    Elsif ?cht:User{prop:Req} = True
        ?cht:User{prop:FontColor} = 65793
        ?cht:User{prop:Color} = 8454143
    Else ! If ?cht:User{prop:Req} = True
        ?cht:User{prop:FontColor} = 65793
        ?cht:User{prop:Color} = 16777215
    End ! If ?cht:User{prop:Req} = True
    ?cht:User{prop:Trn} = 0
    ?cht:User{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Contact_History',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Contact_History',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Contact_History',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Contact_History',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AUD:Notes:Prompt;  SolaceCtrlName = '?AUD:Notes:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:Notes;  SolaceCtrlName = '?cht:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:Action;  SolaceCtrlName = '?cht:Action';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?cht:User;  SolaceCtrlName = '?cht:User';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Contact_History')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Contact_History')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:CONTHIST.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:CONTHIST,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,cht:Action_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?cht:Action,cht:Action,1,BRW1)
  BRW1.AddSortOrder(,cht:User_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?cht:User,cht:User,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha+ScrollSort:Descending)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,cht:Ref_Number_Key)
  BRW1.AddRange(cht:Ref_Number,GLO:Select12)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,cht:Date,1,BRW1)
  BRW1.AddField(cht:Date,BRW1.Q.cht:Date)
  BRW1.AddField(cht:Time,BRW1.Q.cht:Time)
  BRW1.AddField(cht:User,BRW1.Q.cht:User)
  BRW1.AddField(cht:Action,BRW1.Q.cht:Action)
  BRW1.AddField(cht:Notes,BRW1.Q.cht:Notes)
  BRW1.AddField(cht:record_number,BRW1.Q.cht:record_number)
  BRW1.AddField(cht:Ref_Number,BRW1.Q.cht:Ref_Number)
  QuickWindow{PROP:MinWidth}=529
  QuickWindow{PROP:MinHeight}=272
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Date'
    ?Tab2{PROP:TEXT} = 'By Action'
    ?Tab3{PROP:TEXT} = 'By User'
    ?Browse:1{PROP:FORMAT} ='50R(2)|M~Date~L@d6b@#1#26R(2)|M~Time~@t1@#2#23L(2)|M~User~@s3@#3#80L(2)|M~Action~@s80@#4#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CONTHIST.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Contact_History',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            check_access('CONTACT HISTORY - INSERT',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            check_access('CONTACT HISTORY - CHANGE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            check_access('CONTACT HISTORY - DELETE',x")
            if x" = false
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  
  
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Contact_History
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Contact_History')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='50R(2)|M~Date~L@d6b@#1#26R(2)|M~Time~@t1@#2#23L(2)|M~User~@s3@#3#80L(2)|M~Action~@s80@#4#'
          ?Tab:2{PROP:TEXT} = 'By Date'
        OF 2
          ?Browse:1{PROP:FORMAT} ='80L(2)|M~Action~@s80@#4#50R(2)|M~Date~L@d6b@#1#26R(2)|M~Time~@t1@#2#23L(2)|M~User~@s3@#3#'
          ?Tab2{PROP:TEXT} = 'By Action'
        OF 3
          ?Browse:1{PROP:FORMAT} ='23L(2)|M~User~@s3@#3#50R(2)|M~Date~L@d6b@#1#26R(2)|M~Time~@t1@#2#80L(2)|M~Action~@s80@#4#'
          ?Tab3{PROP:TEXT} = 'By User'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?cht:Action
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cht:Action, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cht:Action, Selected)
    OF ?cht:User
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cht:User, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?cht:User, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?AUD:Notes:Prompt, Resize:FixRight+Resize:FixTop, Resize:LockSize)

