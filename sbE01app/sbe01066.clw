

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01066.INC'),ONCE        !Local module procedure declarations
                     END


UpdateUSERS PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
job_assignment_temp  STRING(15)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
FrameTitle           CSTRING(128)
CPCSExecutePopUp     BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?use:User_Level
lev:User_Level         LIKE(lev:User_Level)           !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?use:Location
loc:Location           LIKE(loc:Location)             !List box control field - type derived from field
loc:Location_NormalFG  LONG                           !Normal forground color
loc:Location_NormalBG  LONG                           !Normal background color
loc:Location_SelectedFG LONG                          !Selected forground color
loc:Location_SelectedBG LONG                          !Selected background color
loc:RecordNumber       LIKE(loc:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(USELEVEL)
                       PROJECT(lev:User_Level)
                     END
FDCB8::View:FileDropCombo VIEW(LOCATION)
                       PROJECT(loc:Location)
                       PROJECT(loc:RecordNumber)
                     END
History::use:Record  LIKE(use:RECORD),STATIC
QuickWindow          WINDOW('Update the USERS File'),AT(,,231,307),FONT('Tahoma',8,,),CENTER,IMM,HLP('UpdateUSERS'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,224,188),USE(?Sheet1),SPREAD
                         TAB('General Details'),USE(?Tab1)
                           PROMPT('User Code'),AT(8,20),USE(?USE:User_Code:Prompt),TRN
                           ENTRY(@s3),AT(84,20,64,10),USE(use:User_Code),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Forename'),AT(8,36),USE(?USE:Forename:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(use:Forename),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Surname'),AT(8,52),USE(?USE:Surname:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(use:Surname),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           PROMPT('Password'),AT(8,68),USE(?USE:Password:Prompt),TRN
                           ENTRY(@s20),AT(84,68,124,10),USE(use:Password),LEFT,FONT('Tahoma',8,,FONT:bold),REQ,UPR,PASSWORD
                           PROMPT('Job Type'),AT(8,84),USE(?USE:User_Type:Prompt),TRN
                           COMBO(@s15),AT(84,84,124,10),USE(use:User_Type),LEFT(2),FONT('Tahoma',8,,FONT:bold),REQ,UPR,DROP(10),FROM('ENGINEER|ADMINISTRATOR')
                           GROUP('Engineer Defaults'),AT(8,96,216,92),USE(?EngineerDefaults),DISABLE,BOXED
                             PROMPT('Skill Level'),AT(13,110),USE(?use:SkillLevel:prompt),TRN
                             SPIN(@s8),AT(85,110,64,10),USE(use:SkillLevel),FONT(,,,FONT:bold),UPR,RANGE(0,10),STEP(1),MSG('Skill Level')
                             PROMPT('Team'),AT(13,126),USE(?USE:Team:Prompt:2),TRN
                             ENTRY(@s30),AT(85,126,124,10),USE(use:Team),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),REQ,UPR
                             BUTTON,AT(213,126,10,10),USE(?LookupTeams),SKIP,ICON('List3.ico')
                             PROMPT('Primary Stock Location'),AT(13,142),USE(?use:location:prompt)
                             COMBO(@s30),AT(85,142,124,10),USE(use:Location),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),REQ,UPR,FORMAT('120L(2)|M*@s30@'),DROP(10,124),FROM(Queue:FileDropCombo:2)
                             PROMPT('Daily Repair Target'),AT(13,158),USE(?USE:Repair_Target:Prompt)
                             SPIN(@s8),AT(85,158,64,10),USE(use:Repair_Target),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(0,999999),STEP(1)
                             CHECK('Pick Stock From Eng Location Only'),AT(84,176),USE(use:StockFromLocationOnly),MSG('Pick Stock From Users Location Only'),TIP('Pick Stock From Users Location Only'),VALUE('1','0')
                           END
                           CHECK('Active User'),AT(152,20),USE(use:Active),VALUE('YES','NO')
                         END
                       END
                       SHEET,AT(4,196,224,72),USE(?Sheet2),SPREAD
                         TAB('Security Details'),USE(?Tab2)
                           PROMPT('Access Level'),AT(8,212),USE(?Prompt8),TRN
                           COMBO(@s30),AT(84,212,124,10),USE(use:User_Level),VSCROLL,LEFT(2),FONT('Tahoma',8,,FONT:bold),REQ,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           CHECK('Restrict Logins'),AT(84,228),USE(use:Restrict_Logins),VALUE('YES','NO')
                           PROMPT('Concurrent Logins'),AT(8,244),USE(?use:concurrent_logins:prompt),TRN
                           SPIN(@n4b),AT(84,244,64,10),USE(use:Concurrent_Logins),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),RANGE(1,9999),STEP(1)
                         END
                       END
                       PANEL,AT(4,272,224,24),USE(?Panel3),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(112,276,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(168,276,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

FDCB8                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
SetQueueRecord         PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
look:use:Team                Like(use:Team)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?USE:User_Code:Prompt{prop:FontColor} = -1
    ?USE:User_Code:Prompt{prop:Color} = 15066597
    If ?use:User_Code{prop:ReadOnly} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 15066597
    Elsif ?use:User_Code{prop:Req} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 8454143
    Else ! If ?use:User_Code{prop:Req} = True
        ?use:User_Code{prop:FontColor} = 65793
        ?use:User_Code{prop:Color} = 16777215
    End ! If ?use:User_Code{prop:Req} = True
    ?use:User_Code{prop:Trn} = 0
    ?use:User_Code{prop:FontStyle} = font:Bold
    ?USE:Forename:Prompt{prop:FontColor} = -1
    ?USE:Forename:Prompt{prop:Color} = 15066597
    If ?use:Forename{prop:ReadOnly} = True
        ?use:Forename{prop:FontColor} = 65793
        ?use:Forename{prop:Color} = 15066597
    Elsif ?use:Forename{prop:Req} = True
        ?use:Forename{prop:FontColor} = 65793
        ?use:Forename{prop:Color} = 8454143
    Else ! If ?use:Forename{prop:Req} = True
        ?use:Forename{prop:FontColor} = 65793
        ?use:Forename{prop:Color} = 16777215
    End ! If ?use:Forename{prop:Req} = True
    ?use:Forename{prop:Trn} = 0
    ?use:Forename{prop:FontStyle} = font:Bold
    ?USE:Surname:Prompt{prop:FontColor} = -1
    ?USE:Surname:Prompt{prop:Color} = 15066597
    If ?use:Surname{prop:ReadOnly} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 15066597
    Elsif ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 8454143
    Else ! If ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 16777215
    End ! If ?use:Surname{prop:Req} = True
    ?use:Surname{prop:Trn} = 0
    ?use:Surname{prop:FontStyle} = font:Bold
    ?USE:Password:Prompt{prop:FontColor} = -1
    ?USE:Password:Prompt{prop:Color} = 15066597
    If ?use:Password{prop:ReadOnly} = True
        ?use:Password{prop:FontColor} = 65793
        ?use:Password{prop:Color} = 15066597
    Elsif ?use:Password{prop:Req} = True
        ?use:Password{prop:FontColor} = 65793
        ?use:Password{prop:Color} = 8454143
    Else ! If ?use:Password{prop:Req} = True
        ?use:Password{prop:FontColor} = 65793
        ?use:Password{prop:Color} = 16777215
    End ! If ?use:Password{prop:Req} = True
    ?use:Password{prop:Trn} = 0
    ?use:Password{prop:FontStyle} = font:Bold
    ?USE:User_Type:Prompt{prop:FontColor} = -1
    ?USE:User_Type:Prompt{prop:Color} = 15066597
    If ?use:User_Type{prop:ReadOnly} = True
        ?use:User_Type{prop:FontColor} = 65793
        ?use:User_Type{prop:Color} = 15066597
    Elsif ?use:User_Type{prop:Req} = True
        ?use:User_Type{prop:FontColor} = 65793
        ?use:User_Type{prop:Color} = 8454143
    Else ! If ?use:User_Type{prop:Req} = True
        ?use:User_Type{prop:FontColor} = 65793
        ?use:User_Type{prop:Color} = 16777215
    End ! If ?use:User_Type{prop:Req} = True
    ?use:User_Type{prop:Trn} = 0
    ?use:User_Type{prop:FontStyle} = font:Bold
    ?EngineerDefaults{prop:Font,3} = -1
    ?EngineerDefaults{prop:Color} = 15066597
    ?EngineerDefaults{prop:Trn} = 0
    ?use:SkillLevel:prompt{prop:FontColor} = -1
    ?use:SkillLevel:prompt{prop:Color} = 15066597
    If ?use:SkillLevel{prop:ReadOnly} = True
        ?use:SkillLevel{prop:FontColor} = 65793
        ?use:SkillLevel{prop:Color} = 15066597
    Elsif ?use:SkillLevel{prop:Req} = True
        ?use:SkillLevel{prop:FontColor} = 65793
        ?use:SkillLevel{prop:Color} = 8454143
    Else ! If ?use:SkillLevel{prop:Req} = True
        ?use:SkillLevel{prop:FontColor} = 65793
        ?use:SkillLevel{prop:Color} = 16777215
    End ! If ?use:SkillLevel{prop:Req} = True
    ?use:SkillLevel{prop:Trn} = 0
    ?use:SkillLevel{prop:FontStyle} = font:Bold
    ?USE:Team:Prompt:2{prop:FontColor} = -1
    ?USE:Team:Prompt:2{prop:Color} = 15066597
    If ?use:Team{prop:ReadOnly} = True
        ?use:Team{prop:FontColor} = 65793
        ?use:Team{prop:Color} = 15066597
    Elsif ?use:Team{prop:Req} = True
        ?use:Team{prop:FontColor} = 65793
        ?use:Team{prop:Color} = 8454143
    Else ! If ?use:Team{prop:Req} = True
        ?use:Team{prop:FontColor} = 65793
        ?use:Team{prop:Color} = 16777215
    End ! If ?use:Team{prop:Req} = True
    ?use:Team{prop:Trn} = 0
    ?use:Team{prop:FontStyle} = font:Bold
    ?use:location:prompt{prop:FontColor} = -1
    ?use:location:prompt{prop:Color} = 15066597
    If ?use:Location{prop:ReadOnly} = True
        ?use:Location{prop:FontColor} = 65793
        ?use:Location{prop:Color} = 15066597
    Elsif ?use:Location{prop:Req} = True
        ?use:Location{prop:FontColor} = 65793
        ?use:Location{prop:Color} = 8454143
    Else ! If ?use:Location{prop:Req} = True
        ?use:Location{prop:FontColor} = 65793
        ?use:Location{prop:Color} = 16777215
    End ! If ?use:Location{prop:Req} = True
    ?use:Location{prop:Trn} = 0
    ?use:Location{prop:FontStyle} = font:Bold
    ?USE:Repair_Target:Prompt{prop:FontColor} = -1
    ?USE:Repair_Target:Prompt{prop:Color} = 15066597
    If ?use:Repair_Target{prop:ReadOnly} = True
        ?use:Repair_Target{prop:FontColor} = 65793
        ?use:Repair_Target{prop:Color} = 15066597
    Elsif ?use:Repair_Target{prop:Req} = True
        ?use:Repair_Target{prop:FontColor} = 65793
        ?use:Repair_Target{prop:Color} = 8454143
    Else ! If ?use:Repair_Target{prop:Req} = True
        ?use:Repair_Target{prop:FontColor} = 65793
        ?use:Repair_Target{prop:Color} = 16777215
    End ! If ?use:Repair_Target{prop:Req} = True
    ?use:Repair_Target{prop:Trn} = 0
    ?use:Repair_Target{prop:FontStyle} = font:Bold
    ?use:StockFromLocationOnly{prop:Font,3} = -1
    ?use:StockFromLocationOnly{prop:Color} = 15066597
    ?use:StockFromLocationOnly{prop:Trn} = 0
    ?use:Active{prop:Font,3} = -1
    ?use:Active{prop:Color} = 15066597
    ?use:Active{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt8{prop:FontColor} = -1
    ?Prompt8{prop:Color} = 15066597
    If ?use:User_Level{prop:ReadOnly} = True
        ?use:User_Level{prop:FontColor} = 65793
        ?use:User_Level{prop:Color} = 15066597
    Elsif ?use:User_Level{prop:Req} = True
        ?use:User_Level{prop:FontColor} = 65793
        ?use:User_Level{prop:Color} = 8454143
    Else ! If ?use:User_Level{prop:Req} = True
        ?use:User_Level{prop:FontColor} = 65793
        ?use:User_Level{prop:Color} = 16777215
    End ! If ?use:User_Level{prop:Req} = True
    ?use:User_Level{prop:Trn} = 0
    ?use:User_Level{prop:FontStyle} = font:Bold
    ?use:Restrict_Logins{prop:Font,3} = -1
    ?use:Restrict_Logins{prop:Color} = 15066597
    ?use:Restrict_Logins{prop:Trn} = 0
    ?use:concurrent_logins:prompt{prop:FontColor} = -1
    ?use:concurrent_logins:prompt{prop:Color} = 15066597
    If ?use:Concurrent_Logins{prop:ReadOnly} = True
        ?use:Concurrent_Logins{prop:FontColor} = 65793
        ?use:Concurrent_Logins{prop:Color} = 15066597
    Elsif ?use:Concurrent_Logins{prop:Req} = True
        ?use:Concurrent_Logins{prop:FontColor} = 65793
        ?use:Concurrent_Logins{prop:Color} = 8454143
    Else ! If ?use:Concurrent_Logins{prop:Req} = True
        ?use:Concurrent_Logins{prop:FontColor} = 65793
        ?use:Concurrent_Logins{prop:Color} = 16777215
    End ! If ?use:Concurrent_Logins{prop:Req} = True
    ?use:Concurrent_Logins{prop:Trn} = 0
    ?use:Concurrent_Logins{prop:FontStyle} = font:Bold
    ?Panel3{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Check_Tabs      Routine
!    If use:user_type = 'ENGINEER'
!        Enable(?List)
!        Enable(?List:2)
!        Enable(?Insert)
!        Enable(?Delete)
!        Enable(?Insert:2)
!        Enable(?Delete:2)
!        Enable(?use:job_assignment)
!    Else
!        Disable(?List)
!        Disable(?List:2)
!        Disable(?Insert)
!        Disable(?Delete)
!        Disable(?Insert:2)
!        Disable(?Delete:2)
!        Disable(?use:job_assignment)
!    End
!        If use:job_assignment   = '' Or use:job_assignment = 'MODEL NUMBER'
!            Hide(?Unit_Tab)
!            Unhide(?Model_Tab)
!        End
!        If use:job_assignment   = 'UNIT TYPE'
!            Hide(?Model_Tab)
!            Unhide(?Unit_Tab)
!        End
!    Display()
Count_Job_Assignments       Routine
    found_job_assignments# = 0
    If job_assignment_temp    = 'MODEL NUMBER'
        setcursor(cursor:wait)
        clear(usm:record, -1)
        usm:user_code    = use:user_code
        set(usm:model_number_key,usm:model_number_key)
        loop
            next(usmassig)
            if errorcode()                  |
               or usm:user_code    <> use:user_code      |
               then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            found_job_assignments# = 1
            Break
        end !loop
        setcursor()
    End
    If job_assignment_temp   = 'UNIT TYPE'
        setcursor(cursor:wait)
        clear(usu:record, -1)
        usu:user_code = use:user_code
        set(usu:unit_type_key,usu:unit_type_key)
        loop
            next(usuassig)
            if errorcode()               |
               or usu:user_code <> use:user_code      |
               then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            found_job_assignments# = 1
            Break
        end !loop
        setcursor()
    End
Delete_Job_Assignments       Routine
    If job_assignment_temp = 'MODEL NUMBER'
        setcursor(cursor:wait)
        logout(2,usmassig)
        if errorcode()
           Stop(Error())
        end
        clear(usm:record, -1)
        usm:user_code    = use:user_code
        set(usm:model_number_key,usm:model_number_key)
        loop
            next(usmassig)
            if errorcode()                  |
               or usm:user_code    <> use:user_code      |
               then break.  ! end if
            delete(usmassig)
            case errorcode()
            of noerror
            else
                stop('DELETE '&clip(errorfile())&' Error: '&error() )
            end
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        end !loop
        commit()
        if errorcode()
           Stop(Error())
        end
        setcursor()
    End
    If job_assignment_temp   = 'UNIT TYPE'
        setcursor(cursor:wait)
        logout(2,usuassig)
        if errorcode()
           Stop(Error())
        end
        clear(usu:record, -1)
        usu:user_code = use:user_code
        set(usu:unit_type_key,usu:unit_type_key)
        loop
            next(usuassig)
            if errorcode()               |
               or usu:user_code <> use:user_code      |
               then break.  ! end if
            delete(usuassig)
            case errorcode()
            of noerror
            else
                stop('DELETE '&clip(errorfile())&' Error: '&error() )
            end
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
        end !loop
        commit()
        if errorcode()
           Stop(Error())
        end
        setcursor()
    End
Concurrent_Field        Routine
    If use:restrict_logins <> 'YES'
        Disable(?use:concurrent_logins)
        Disable(?use:concurrent_logins:prompt)
    Else
        Enable(?use:concurrent_logins)
        Enable(?use:concurrent_logins:prompt)
    End
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateUSERS',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateUSERS',1)
    SolaceViewVars('job_assignment_temp',job_assignment_temp,'UpdateUSERS',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateUSERS',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateUSERS',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateUSERS',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateUSERS',1)
    SolaceViewVars('FrameTitle',FrameTitle,'UpdateUSERS',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'UpdateUSERS',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?USE:User_Code:Prompt;  SolaceCtrlName = '?USE:User_Code:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:User_Code;  SolaceCtrlName = '?use:User_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?USE:Forename:Prompt;  SolaceCtrlName = '?USE:Forename:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Forename;  SolaceCtrlName = '?use:Forename';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?USE:Surname:Prompt;  SolaceCtrlName = '?USE:Surname:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Surname;  SolaceCtrlName = '?use:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?USE:Password:Prompt;  SolaceCtrlName = '?USE:Password:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Password;  SolaceCtrlName = '?use:Password';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?USE:User_Type:Prompt;  SolaceCtrlName = '?USE:User_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:User_Type;  SolaceCtrlName = '?use:User_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EngineerDefaults;  SolaceCtrlName = '?EngineerDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:SkillLevel:prompt;  SolaceCtrlName = '?use:SkillLevel:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:SkillLevel;  SolaceCtrlName = '?use:SkillLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?USE:Team:Prompt:2;  SolaceCtrlName = '?USE:Team:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Team;  SolaceCtrlName = '?use:Team';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupTeams;  SolaceCtrlName = '?LookupTeams';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:location:prompt;  SolaceCtrlName = '?use:location:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Location;  SolaceCtrlName = '?use:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?USE:Repair_Target:Prompt;  SolaceCtrlName = '?USE:Repair_Target:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Repair_Target;  SolaceCtrlName = '?use:Repair_Target';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:StockFromLocationOnly;  SolaceCtrlName = '?use:StockFromLocationOnly';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Active;  SolaceCtrlName = '?use:Active';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt8;  SolaceCtrlName = '?Prompt8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:User_Level;  SolaceCtrlName = '?use:User_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Restrict_Logins;  SolaceCtrlName = '?use:Restrict_Logins';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:concurrent_logins:prompt;  SolaceCtrlName = '?use:concurrent_logins:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Concurrent_Logins;  SolaceCtrlName = '?use:Concurrent_Logins';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel3;  SolaceCtrlName = '?Panel3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A User'
  OF ChangeRecord
    ActionMessage = 'Changing A User'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateUSERS')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateUSERS')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?USE:User_Code:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(use:Record,History::use:Record)
  SELF.AddHistoryField(?use:User_Code,1)
  SELF.AddHistoryField(?use:Forename,2)
  SELF.AddHistoryField(?use:Surname,3)
  SELF.AddHistoryField(?use:Password,4)
  SELF.AddHistoryField(?use:User_Type,5)
  SELF.AddHistoryField(?use:SkillLevel,18)
  SELF.AddHistoryField(?use:Team,15)
  SELF.AddHistoryField(?use:Location,16)
  SELF.AddHistoryField(?use:Repair_Target,17)
  SELF.AddHistoryField(?use:StockFromLocationOnly,19)
  SELF.AddHistoryField(?use:Active,14)
  SELF.AddHistoryField(?use:User_Level,8)
  SELF.AddHistoryField(?use:Restrict_Logins,12)
  SELF.AddHistoryField(?use:Concurrent_Logins,13)
  SELF.AddUpdateFile(Access:USERS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFAULT2.Open
  Relate:LOCATION.Open
  Relate:USELEVEL.Open
  Access:TEAMS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:USERS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?use:Team{Prop:Tip} AND ~?LookupTeams{Prop:Tip}
     ?LookupTeams{Prop:Tip} = 'Select ' & ?use:Team{Prop:Tip}
  END
  IF ?use:Team{Prop:Msg} AND ~?LookupTeams{Prop:Msg}
     ?LookupTeams{Prop:Msg} = 'Select ' & ?use:Team{Prop:Msg}
  END
  Set(DEFAULT2)
  Access:DEFAULT2.Next()
  
  If DE2:UserSkillLevel
      ?use:SkillLevel{prop:req} = 1
  Else !DE2:UserSkillLevel
      ?use:SkillLevel{prop:req} = 0
  End !DE2:UserSkillLevel
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  FDCB5.Init(use:User_Level,?use:User_Level,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:USELEVEL,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(lev:User_Level_Key)
  FDCB5.AddField(lev:User_Level,FDCB5.Q.lev:User_Level)
  FDCB5.AddUpdateField(lev:User_Level,use:User_Level)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  FDCB8.Init(use:Location,?use:Location,Queue:FileDropCombo:2.ViewPosition,FDCB8::View:FileDropCombo,Queue:FileDropCombo:2,Relate:LOCATION,ThisWindow,GlobalErrors,0,1,0)
  FDCB8.Q &= Queue:FileDropCombo:2
  FDCB8.AddSortOrder(loc:Location_Key)
  FDCB8.AddField(loc:Location,FDCB8.Q.loc:Location)
  FDCB8.AddField(loc:RecordNumber,FDCB8.Q.loc:RecordNumber)
  ThisWindow.AddItem(FDCB8.WindowComponent)
  FDCB8.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULT2.Close
    Relate:LOCATION.Close
    Relate:USELEVEL.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateUSERS',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    use:User_Type = 'ENGINEER'
    use:Job_Assignment = 'MODEL NUMBER'
    use:Supervisor = 'NO'
    use:User_Level = '** UNKNOWN **'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Browse_Teams
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?use:User_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Type, Accepted)
      If use:User_Type = 'ENGINEER'
          Enable(?EngineerDefaults)
      Else !use:User_Type = 'ENGINEER'
          Disable(?EngineerDefaults)
      End !use:User_Type = 'ENGINEER'
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Type, Accepted)
    OF ?use:Team
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Team, Accepted)
      If ?EngineerDefaults{prop:Disable} = 0
      IF use:Team OR ?use:Team{Prop:Req}
        tea:Team = use:Team
        !Save Lookup Field Incase Of error
        look:use:Team        = use:Team
        IF Access:TEAMS.TryFetch(tea:Team_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            use:Team = tea:Team
          ELSE
            !Restore Lookup On Error
            use:Team = look:use:Team
            SELECT(?use:Team)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      End !If ~0{prop:AcceptAll}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Team, Accepted)
    OF ?LookupTeams
      ThisWindow.Update
      tea:Team = use:Team
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          use:Team = tea:Team
          Select(?+1)
      ELSE
          Select(?use:Team)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?use:Team)
    OF ?use:User_Level
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Level, Accepted)
      access:uselevel.clearkey(lev:user_level_key)
      lev:user_level = use:user_level
      if access:uselevel.fetch(lev:user_level_key)
          saverequest#      = globalrequest
          globalresponse    = requestcancelled
          globalrequest     = selectrecord
          browse_access_levels
          if globalresponse = requestcompleted
              use:user_level = lev:user_level
          Else
              use:user_level = '**UNKNOWN**'
          end
          globalrequest     = saverequest#
          Display(?use:user_level)
      end!if access:uselevel.fetch(lev:user_level_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:User_Level, Accepted)
    OF ?use:Restrict_Logins
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Restrict_Logins, Accepted)
      Do Concurrent_Field
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?use:Restrict_Logins, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateUSERS')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Concurrent_Field
      Do Check_Tabs
      Post(Event:Accepted,?use:User_Type)
      job_assignment_temp = use:job_assignment
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      FDCB5.ResetQueue(1)
      FDCB8.ResetQueue(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


FDCB8.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.loc:Location_NormalFG = -1
  SELF.Q.loc:Location_NormalBG = -1
  SELF.Q.loc:Location_SelectedFG = -1
  SELF.Q.loc:Location_SelectedBG = -1

