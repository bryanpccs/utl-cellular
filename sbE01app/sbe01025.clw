

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBE01025.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Header PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STAHEAD)
                       PROJECT(sth:Ref_Number)
                       PROJECT(sth:Heading)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sth:Ref_Number         LIKE(sth:Ref_Number)           !List box control field - type derived from field
sth:Heading            LIKE(sth:Heading)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(STATUS)
                       PROJECT(sts:Ref_Number)
                       PROJECT(sts:Status)
                       PROJECT(sts:Notes)
                       PROJECT(sts:Heading_Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
sts:Ref_Number         LIKE(sts:Ref_Number)           !List box control field - type derived from field
sts:Status             LIKE(sts:Status)               !List box control field - type derived from field
sts:Notes              LIKE(sts:Notes)                !Browse hot field - type derived from field
sts:Heading_Ref_Number LIKE(sts:Heading_Ref_Number)   !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Current Status File'),AT(,,576,214),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Header'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,152,128),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('31L(2)|M~Ref No~@n3@80L(2)|M~Heading~@s30@'),FROM(Queue:Browse:1)
                       SHEET,AT(4,4,160,180),USE(?CurrentTab),SPREAD
                         TAB('By &Ref Number'),USE(?Tab5)
                           ENTRY(@n3b),AT(8,20,44,10),USE(sth:Ref_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By &Heading'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(sth:Heading),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       SHEET,AT(168,4,160,180),USE(?Sheet2),SPREAD
                         TAB('By Ref &Number'),USE(?Tab4)
                           ENTRY(@n3b),AT(172,20,44,10),USE(sts:Ref_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By &Status Type'),USE(?Tab2)
                           ENTRY(@s30),AT(172,20,124,10),USE(sts:Status),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       SHEET,AT(332,4,240,180),USE(?Sheet3),SPREAD
                         TAB('Notes'),USE(?Tab3)
                           TEXT,AT(336,36,232,64),USE(sts:Notes),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                         END
                       END
                       BUTTON('Close'),AT(512,192,56,16),USE(?Close),FLAT,LEFT,ICON('cancel.gif')
                       BUTTON('&Insert'),AT(8,168,48,12),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(60,168,48,12),USE(?Change:3),LEFT,ICON('edit.ico'),DEFAULT
                       BUTTON('&Delete'),AT(112,168,48,12),USE(?Delete:3),LEFT,ICON('delete.ico')
                       LIST,AT(172,36,152,128),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('32L(2)|M~Ref No~@n3@107L(2)|M~Status~@s30@'),FROM(Queue:Browse)
                       BUTTON('&Insert'),AT(172,168,48,12),USE(?Insert),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(224,168,48,12),USE(?Change),LEFT,ICON('edit.ico')
                       BUTTON('&Delete'),AT(276,168,48,12),USE(?Delete),LEFT,ICON('delete.ico')
                       PANEL,AT(4,188,568,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW6::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?Sheet2) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    If ?sth:Ref_Number{prop:ReadOnly} = True
        ?sth:Ref_Number{prop:FontColor} = 65793
        ?sth:Ref_Number{prop:Color} = 15066597
    Elsif ?sth:Ref_Number{prop:Req} = True
        ?sth:Ref_Number{prop:FontColor} = 65793
        ?sth:Ref_Number{prop:Color} = 8454143
    Else ! If ?sth:Ref_Number{prop:Req} = True
        ?sth:Ref_Number{prop:FontColor} = 65793
        ?sth:Ref_Number{prop:Color} = 16777215
    End ! If ?sth:Ref_Number{prop:Req} = True
    ?sth:Ref_Number{prop:Trn} = 0
    ?sth:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    If ?sth:Heading{prop:ReadOnly} = True
        ?sth:Heading{prop:FontColor} = 65793
        ?sth:Heading{prop:Color} = 15066597
    Elsif ?sth:Heading{prop:Req} = True
        ?sth:Heading{prop:FontColor} = 65793
        ?sth:Heading{prop:Color} = 8454143
    Else ! If ?sth:Heading{prop:Req} = True
        ?sth:Heading{prop:FontColor} = 65793
        ?sth:Heading{prop:Color} = 16777215
    End ! If ?sth:Heading{prop:Req} = True
    ?sth:Heading{prop:Trn} = 0
    ?sth:Heading{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab4{prop:Color} = 15066597
    If ?sts:Ref_Number{prop:ReadOnly} = True
        ?sts:Ref_Number{prop:FontColor} = 65793
        ?sts:Ref_Number{prop:Color} = 15066597
    Elsif ?sts:Ref_Number{prop:Req} = True
        ?sts:Ref_Number{prop:FontColor} = 65793
        ?sts:Ref_Number{prop:Color} = 8454143
    Else ! If ?sts:Ref_Number{prop:Req} = True
        ?sts:Ref_Number{prop:FontColor} = 65793
        ?sts:Ref_Number{prop:Color} = 16777215
    End ! If ?sts:Ref_Number{prop:Req} = True
    ?sts:Ref_Number{prop:Trn} = 0
    ?sts:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?sts:Status{prop:ReadOnly} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 15066597
    Elsif ?sts:Status{prop:Req} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 8454143
    Else ! If ?sts:Status{prop:Req} = True
        ?sts:Status{prop:FontColor} = 65793
        ?sts:Status{prop:Color} = 16777215
    End ! If ?sts:Status{prop:Req} = True
    ?sts:Status{prop:Trn} = 0
    ?sts:Status{prop:FontStyle} = font:Bold
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    If ?sts:Notes{prop:ReadOnly} = True
        ?sts:Notes{prop:FontColor} = 65793
        ?sts:Notes{prop:Color} = 15066597
    Elsif ?sts:Notes{prop:Req} = True
        ?sts:Notes{prop:FontColor} = 65793
        ?sts:Notes{prop:Color} = 8454143
    Else ! If ?sts:Notes{prop:Req} = True
        ?sts:Notes{prop:FontColor} = 65793
        ?sts:Notes{prop:Color} = 16777215
    End ! If ?sts:Notes{prop:Req} = True
    ?sts:Notes{prop:Trn} = 0
    ?sts:Notes{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Header',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Header',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Header',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Header',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sth:Ref_Number;  SolaceCtrlName = '?sth:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sth:Heading;  SolaceCtrlName = '?sth:Heading';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab4;  SolaceCtrlName = '?Tab4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Ref_Number;  SolaceCtrlName = '?sts:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Status;  SolaceCtrlName = '?sts:Status';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sts:Notes;  SolaceCtrlName = '?sts:Notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Header')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Header')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:STAHEAD.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STAHEAD,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:STATUS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,sth:Heading_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?STH:Heading,sth:Ref_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sth:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sth:Ref_Number,sth:Ref_Number,1,BRW1)
  BRW1.AddField(sth:Ref_Number,BRW1.Q.sth:Ref_Number)
  BRW1.AddField(sth:Heading,BRW1.Q.sth:Heading)
  BRW6.Q &= Queue:Browse
  BRW6.RetainRow = 0
  BRW6.AddSortOrder(,sts:Heading_Key)
  BRW6.AddRange(sts:Heading_Ref_Number,sth:Ref_Number)
  BRW6.AddLocator(BRW6::Sort1:Locator)
  BRW6::Sort1:Locator.Init(?STS:Status,sts:Status,1,BRW6)
  BRW6.AddSortOrder(,sts:Ref_Number_Key)
  BRW6.AddRange(sts:Heading_Ref_Number,sth:Ref_Number)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?STS:Ref_Number,sts:Ref_Number,1,BRW6)
  BRW6.AddField(sts:Ref_Number,BRW6.Q.sts:Ref_Number)
  BRW6.AddField(sts:Status,BRW6.Q.sts:Status)
  BRW6.AddField(sts:Notes,BRW6.Q.sts:Notes)
  BRW6.AddField(sts:Heading_Ref_Number,BRW6.Q.sts:Heading_Ref_Number)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  BRW6.AskProcedure = 2
  BRW6.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  brw1.popup.kill
  brw6.popup.kill
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab5{PROP:TEXT} = 'By &Ref Number'
    ?Tab:2{PROP:TEXT} = 'By &Heading'
    ?Tab4{PROP:TEXT} = 'By Ref &Number'
    ?Tab2{PROP:TEXT} = 'By &Status Type'
    ?Tab3{PROP:TEXT} = 'Notes'
    ?Browse:1{PROP:FORMAT} ='31L(2)|M~Ref No~@n3@#1#80L(2)|M~Heading~@s30@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab5{PROP:TEXT} = 'By &Ref Number'
    ?Tab:2{PROP:TEXT} = 'By &Heading'
    ?Tab4{PROP:TEXT} = 'By Ref &Number'
    ?Tab2{PROP:TEXT} = 'By &Status Type'
    ?Tab3{PROP:TEXT} = 'Notes'
    ?List{PROP:FORMAT} ='32L(2)|M~Ref No~@n3@#1#107L(2)|M~Status~@s30@#2#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STAHEAD.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Header',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  glo:select2  = Brw1.q.sth:ref_number
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      UpdateSTAHEAD
      UpdateSTATUS
    END
    ReturnValue = GlobalResponse
  END
  glo:select2 = ''
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Header')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
        SELECT(?Browse:1)                   ! Reselect list box after tab change
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet3
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet3)
        OF 1
          ?Browse:1{PROP:FORMAT} ='31L(2)|M~Ref No~@n3@#1#80L(2)|M~Heading~@s30@#2#'
          ?Tab5{PROP:TEXT} = 'By &Ref Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='80L(2)|M~Heading~@s30@#2#31L(2)|M~Ref No~@n3@#1#'
          ?Tab:2{PROP:TEXT} = 'By &Heading'
        OF 3
          ?Browse:1{PROP:FORMAT} ='31L(2)|M~Ref No~@n3@#1#80L(2)|M~Heading~@s30@#2#'
          ?Tab4{PROP:TEXT} = 'By Ref &Number'
        OF 4
          ?Browse:1{PROP:FORMAT} ='31L(2)|M~Ref No~@n3@#1#80L(2)|M~Heading~@s30@#2#'
          ?Tab2{PROP:TEXT} = 'By &Status Type'
        OF 5
          ?Browse:1{PROP:FORMAT} ='31L(2)|M~Ref No~@n3@#1#80L(2)|M~Heading~@s30@#2#'
          ?Tab3{PROP:TEXT} = 'Notes'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet3)
        OF 1
          ?List{PROP:FORMAT} ='32L(2)|M~Ref No~@n3@#1#107L(2)|M~Status~@s30@#2#'
          ?Tab5{PROP:TEXT} = 'By &Ref Number'
        OF 2
          ?List{PROP:FORMAT} ='32L(2)|M~Ref No~@n3@#1#107L(2)|M~Status~@s30@#2#'
          ?Tab:2{PROP:TEXT} = 'By &Heading'
        OF 3
          ?List{PROP:FORMAT} ='32L(2)|M~Ref No~@n3@#1#107L(2)|M~Status~@s30@#2#'
          ?Tab4{PROP:TEXT} = 'By Ref &Number'
        OF 4
          ?List{PROP:FORMAT} ='107L(2)|M~Status~@s30@#2#32L(2)|M~Ref No~@n3@#1#'
          ?Tab2{PROP:TEXT} = 'By &Status Type'
        OF 5
          ?List{PROP:FORMAT} ='32L(2)|M~Ref No~@n3@#1#107L(2)|M~Status~@s30@#2#'
          ?Tab3{PROP:TEXT} = 'Notes'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW6.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?Sheet2) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

