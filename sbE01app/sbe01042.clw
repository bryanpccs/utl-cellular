

   MEMBER('sbe01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBE01042.INC'),ONCE        !Local module procedure declarations
                     END


Status_Routine       PROCEDURE  (f_status_number,f_current_status,f_end_date,f_end_time,f_audit) ! Declare Procedure
old_status_temp      STRING(30)
start_time_temp      TIME
clock_temp           TIME
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Status_Routine')      !Add Procedure to Log
  end


    error# = 0
    If job:cancelled = 'YES'
        error# = 1
    End!If job:cancelled = 'YES'
    access:status.clearkey(sts:status_key)
    sts:status = job:current_status
    if access:status.fetch(sts:status_key) = Level:Benign
        If sts:ref_number > 500 And sts:ref_number < 600
            If job:estimate = 'YES'
                If job:estimate_accepted <> 'YES' and job:estimate_rejected <> 'YES'
                    error# = 1
                End!If job:estimate_accepted <> 'YES' and job:estimate_rejected <> 'YES'
            End!If job:estimate = 'YES'
        End!If sts:ref_number > 500 And sts:ref_number < 600

    End!if access:status.fetch(sts:status_key) = Level:Benign

    If error# = 0
        old_status_temp = job:current_status
        access:status.clearkey(sts:ref_number_only_key)
        sts:ref_number  = f_status_number
        If access:status.fetch(sts:ref_number_only_key)
            f_current_status = 'ERROR'
        Else!If access:status.fetch(sts:ref_number_only_key)
            f_current_status = Clip(sts:status)
            If f_status_number <> 0
                If old_status_temp <> sts:status And old_status_temp <> '' And f_audit <> 1
                    access:users.clearkey(use:password_key)
                    use:password    =glo:password
                    access:users.fetch(use:password_key)
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:notes   = 'PREVIOUS STATUS: ' & Clip(old_status_temp) & |
                                        '<13,10>NEW STATUS: ' & Clip(sts:status)
                        aud:ref_number  = job:ref_number
                        aud:date        = Today()
                        aud:time        = Clock()
                        aud:user        = use:user_code
                        aud:action      = 'STATUS CHANGE TO: ' & Clip(sts:status)
                        if access:audit.tryinsert()
                            access:audit.cancelautoinc()
                        end
                    end!if access:audit.primerecord() = level:benign
                End!If old_status_temp <> sts:status
            End!If f_status_number = 0
        End!If access:status.fetch(sts:ref_number_only_key)

        access:stahead.clearkey(sth:ref_number_key)
        sth:ref_number  = sts:heading_ref_number
        If access:stahead.fetch(sth:ref_number_key) = Level:Benign
            access:jobstage.clearkey(jst:job_stage_key)
            jst:ref_number = job:ref_number
            jst:job_stage  = sth:heading
            if access:jobstage.fetch(jst:job_stage_key) 
                get(jobstage,0)
                jst:ref_number = job:ref_number
                jst:job_stage  = sth:heading
                jst:date       = Today()
                jst:time       = Clock()
                access:users.clearkey(use:password_key)
                use:password    =glo:password
                access:users.fetch(use:password_key)
                jst:user       = use:user_code
                access:jobstage.tryinsert()
                If f_audit <> 1
                    get(audit,0)
                    if access:audit.primerecord() = level:benign
                        aud:notes      = ''
                        aud:ref_number = job:ref_number
                        aud:date       = Today()
                        aud:time       = Clock()
                        aud:user       = jst:user
                        aud:action     = 'JOB STAGE CHANGED TO: ' & Clip(sth:heading)
                        if access:audit.tryinsert()
                          access:audit.cancelautoinc()
                        end
                    end!if access:audit.primerecord() = level:benign
                End!End!If f_audit = 'Y'
            end
        End !If access:stahead.fetch(sth:heading_key) = Level:Benign

        If sts:use_turnaround_time = 'YES'
            Turnaround_Routine(sts:turnaround_days,sts:turnaround_hours,f_end_date,f_end_time)
        Else!If sts:use_turnaround_time = 'YES'
            f_end_date = Deformat('1/1/2050',@d6)
            f_end_time = Clock()
        End!If sts:use_turnaround_days = 'YES'
    End!If error# = 0


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Status_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('old_status_temp',old_status_temp,'Status_Routine',1)
    SolaceViewVars('start_time_temp',start_time_temp,'Status_Routine',1)
    SolaceViewVars('clock_temp',clock_temp,'Status_Routine',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
