

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01021.INC'),ONCE        !Local module procedure declarations
                     END


FindManufacturer     PROCEDURE  (func:Manufacturer)   ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
sav:Path             STRING(255)
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'FindManufacturer')      !Add Procedure to Log
  end


        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = func:Manufacturer
        If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Found
            If man:EDI_Path = ''
                Case MessageEx('You have not setup the EDI Path for this Manuafacturer.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                Return Level:Fatal
            Else !If man:EDI_Path = ''
                sav:Path    = Path()
                SetPath(Clip(man:EDI_Path))
                If Error()
                    Case MessageEx('The EDI Path for this Manufacturer does not exist.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return Level:Fatal
                End !If Error()
                SetPath(sav:Path)
            End !If man:EDI_Path = ''

            If man:EDI_Account_Number = ''
                Case MessageEx('An EDI Account Number has not been setup for this Manaufacturer.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                Return Level:Fatal
            End !If man:EDI_Account_Number = ''

            If man:nokiatype = '' and func:Manufacturer = 'NOKIA'
                Case MessageEx('You must select who Processes the Warranty Claim.<13,10>This is found in the Manufacturer''s defaults.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                Return Level:Fatal
            End!If man:nokiatype = ''

        Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Case MessageEx('Cannot find the Manufacturer "' & Clip(func:Manufacturer) & '". '&|
              '<13,10>'&|
              '<13,10>Ensure that a Manufacturer of this name has been setup.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            Return Level:Fatal
        End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

        Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'FindManufacturer',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('sav:Path',sav:Path,'FindManufacturer',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
