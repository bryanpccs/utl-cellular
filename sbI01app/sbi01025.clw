

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01025.INC'),ONCE        !Local module procedure declarations
                     END


Siemens_DECT_EDI     PROCEDURE  (f_batch_number)      ! Declare Procedure
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
save_job_id          USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
pos                  STRING(255)
thefilename          STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(248)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
lso_ref      STRING(10)
cust_class   STRING(4)
date_in      STRING(9)
date_out     STRING(9)
model        STRING(6)
type         STRING(6)
imei_no      STRING(17)
date_code    STRING(3)
war_until    STRING(6)
fault_code   STRING(5)
l_0code      STRING(6)
l_1code      STRING(6)
l_2code      STRING(6)
l_25code      STRING(6)
qual_mod     STRING(6)
wty_acc      STRING(4)
spare1       STRING(17)
spare2       STRING(17)
spare3       STRING(17)
spare4       STRING(17)
spare5       STRING(17)
spare6       STRING(17)
refurb       STRING(4)
carriage     STRING(11)
labour       STRING(11)
parts        STRING(11)           .

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL
parts_temp         REAL
labour_temp        REAL
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Siemens_DECT_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:JOBSTAGE.Open
   Relate:SUBTRACC.Open
   Relate:CHARTYPE.Open
   Relate:DISCOUNT.Open
   Relate:VATCODE.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:JOBSE.Open
   Relate:STOCK.Open
!**Variable
    tmp:ManufacturerName = 'SIEMENS DECT'
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
            !Always use the new version of the EDI
            man:SiemensNewEDI = 1
!**Variable
            If man:SiemensNewEDI
                If Month(Today()) <> Month(man:SiemensDate)
                    man:SiemensNumber = 100
                    man:SiemensDate = Today()
                Else !If Month(Today()) <> Month(man:SiemensDate)
                    man:SiemensNumber += 1
                    man:SiemensDate = Today()
                End !If Month(Today()) <> Month(man:SiemensDate)
                glo:File_Name   = Clip(Clip(man:EDI_Path) & '\' & Clip(man:EDI_Account_Number) & '_' & Format(Year(Today()),@n04) & Format(Month(Today()),@n02) & Format(man:SiemensNumber,@n03) & '.wsr')
                Access:MANUFACT.TryUpdate()
!                IF f_batch_number = 0
!                  glo:file_name = CLIP(CLIP(MAN:EDI_Path)&'\SI'&CLIP(Format(MAN:Batch_number,@n05))&'.TXT')
!                ELSE
!                  glo:file_name = CLIP(CLIP(MAN:EDI_Path)&'\SI'&CLIP(Format(f_Batch_number,@n05))&'.TXT')
!                END
                Access:EXPGEN.Open()
                Access:EXPGEN.Usefile()
            Else !If man:SiemensNewEDI
                IF f_batch_number = 0
                  filename = CLIP(CLIP(MAN:EDI_Path)&'\SIE'&CLIP(Format(MAN:Batch_number,@n05))&'.CSV')
                ELSE
                  filename = CLIP(CLIP(MAN:EDI_Path)&'\SIE'&CLIP(Format(f_Batch_number,@n05))&'.CSV')
                END
                Open(Out_File)                           ! Open The Output File
                If Errorcode()                           ! If Error
                  Create(Out_File)                       ! Create A New File
                    If Errorcode()
                        Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!If Errorcode() 
                  Open(Out_File)        ! If Still Error Then Stop
                Else
                  Open(Out_File)
                  Empty(Out_File)
                End

            End !If man:SiemensNewEDI
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If

                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'

                        If man:SiemensNewEDI
                            Do NewExport
                        Else !If man:SiemensNewEDI
                            Do Export
                        End !If man:SiemensNewEDI

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        !Found
                        pos = Position(job:EDI_Key)
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)
                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50
                        If man:SiemensNewEDI
                            Do NewExport
                        Else !If man:SiemensNewEDI
                            Do Export
                        End !If man:SiemensNewEDI
                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    ! Start Change 2806 BE(22/07/03)
                    !If man:SiemensNewEDI
                    !    Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !      '<13,10>'&|
                    !      '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(glo:file_name) & '.','ServiceBase 2000',|
                    !                   'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !        Of 1 ! &OK Button
                    !    End!Case MessageEx
                    !
                    !Else !If man:SiemensNewEDI
                    !    Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !      '<13,10>'&|
                    !      '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                    !                   'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !        Of 1 ! &OK Button
                    !    End!Case MessageEx
                    !
                    !End !If man:SiemensNewEDI
                    IF (man:SiemensNewEDI) THEN
                        TheFilename = CLIP(glo:file_name)
                    ELSE
                        TheFilename = CLIP(filename)
                    END
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(TheFilename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(TheFilename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)

                End!If f_batch_number = 0
                If man:SiemensNewEDI
                    Access:EXPGEN.Close()
                Else!If man:SiemensNewEDI
                    Close(Out_File)
                End!"If man:SiemensNewEDI
            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:JOBSTAGE.Close
   Relate:SUBTRACC.Close
   Relate:CHARTYPE.Close
   Relate:DISCOUNT.Close
   Relate:VATCODE.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:JOBSE.Close
   Relate:STOCK.Close
Export      Routine
                Clear(Ouf:Record)
                Clear(Mt_Total)
                L1:Lso_Ref = Sub(job:Ref_Number,1,10)
                L1:Cust_Class = ',' & Sub(job:Fault_Code6,1,3)
                L1:Date_In = ',' & Format(Day(job:Date_Booked),@N02)&'.'&Format(Month(job:Date_Booked),@N02)&'.'&Format(Sub(Year(job:Date_Booked),3,2),@N02)
                L1:Date_Out = ',' & Format(Day(job:Date_Completed),@N02)&'.'&Format(Month(job:Date_Completed),@N02)&'.'&Format(Sub(Year(job:Date_Completed),3,2),@N02)
                L1:Model = ',' & Sub(job:Model_Number,1,5)
                L1:Type = ',' & Sub(job:Fault_Code1,1,5)
                IMEIError# = 0
                If job:Third_Party_Site <> ''
                    Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                    jot:RefNumber = job:Ref_Number
                    Set(jot:RefNumberKey,jot:RefNumberKey)
                    If Access:JOBTHIRD.NEXT()
                        IMEIError# = 1
                    Else !If Access:JOBTHIRD.NEXT()
                        If jot:RefNumber <> job:Ref_Number            
                            IMEIError# = 1
                        Else !If jot:RefNumber <> job:Ref_Number            
                            IMEIError# = 0                
                        End !If jot:RefNumber <> job:Ref_Number            
                    End !If Access:JOBTHIRD.NEXT()
                Else !job:Third_Party_Site <> ''
                    IMEIError# = 1    
                End !job:Third_Party_Site <> ''

                If IMEIError# = 1
                    L1:Imei_No = ',' & Sub(job:Esn,1,15)
                Else !IMEIError# = 1
                    L1:Imei_No = ',' & Sub(Jot:OriginalIMEI,1,15)
                End !IMEIError# = 1
                
                L1:Date_Code = ',' & Sub(job:Fault_Code3,1,2)
                L1:War_Until = ',' & Format(Month(job:Fault_Code5),@N02)&'.'&Format(Sub(Year(job:Fault_Code5),3,2),@N02)
                L1:Fault_Code = ',' & Sub(job:Fault_Code2,1,4)
                l1:l_0code = ',' & Sub(job:fault_code7,1,5)
                L1:L_1Code = ',' & Sub(job:Fault_Code8,1,5)
                l1:l_2code = ',' & sub(job:fault_code9,1,5)
                l1:l_25code = ',' & Sub(job:fault_code10,1,5)
                L1:Qual_Mod = ',' & Sub(job:Fault_Code11,1,5)
                L1:Wty_Acc = ',' & Sub('YES',1,3)

                xp# = 0
!                parts_temp = 0
                setcursor(cursor:wait)
                save_wpr_id = access:warparts.savefile()
                access:warparts.clearkey(wpr:part_number_key)
                wpr:ref_number  = job:ref_number
                set(wpr:part_number_key,wpr:part_number_key)
                loop
                    if access:warparts.next()
                       break
                    end !if
                    if wpr:ref_number  <> job:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
!                    parts_temp +=(wpr:purchase_cost * wpr:quantity)
                    If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                        Cycle
                    End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)

                    xp# += 1
                    CASE xp#
                        OF 1
                              L1:Spare1 = ',' & SUB(wpr:part_number,1,16)
                        OF 2
                              L1:Spare2 = ',' & SUB(wpr:part_number,1,16)
                        OF 3
                              L1:Spare3 = ',' & SUB(wpr:part_number,1,16)
                        OF 4
                              L1:Spare4 = ',' & SUB(wpr:part_number,1,16)
                        OF 5
                              L1:Spare5 = ',' & SUB(wpr:part_number,1,16)
                        OF 6
                              L1:Spare6 = ',' & SUB(wpr:part_number,1,16)
                    END
                end !loop
                access:warparts.restorefile(save_wpr_id)
                setcursor()    

                IF l1:spare1 = ''
                    L1:Spare1 = ',' & Sub('',1,16)
                END
                IF l1:spare2 = ''
                    L1:Spare2 = ',' & Sub('',1,16)
                END
                IF l1:spare3 = ''
                    L1:Spare3 = ',' & Sub('',1,16)
                END
                IF l1:spare4 = ''
                    L1:Spare4 = ',' & Sub('',1,16)
                END
                IF l1:spare5 = ''
                    L1:Spare5 = ',' & Sub('',1,16)
                END
                IF l1:spare6 = ''
                    L1:Spare6 = ',' & Sub('',1,16)
                END
                    
                L1:Refurb = ',' & Sub(job:Fault_Code4,1,3)
                l1:carriage = ',' & SUB(FORMAT(job:courier_cost_warranty,@n8.2),1,10)
                l1:labour = ',' & SUB(FORMAT(job:labour_cost_warranty,@n8.2),1,10)
                l1:parts = ',' & SUB(FORMAT(job:parts_cost_warranty,@n8.2),1,10)
                Add(Out_File)
                Count+=1
                Clear(Ouf:Record)
NewExport       Routine !From 15th October 2001
    Clear(gen:Record)
    gen:Line1   = 'R'
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(man:edi_Account_Number)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Ref_Number)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Order_Number)
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code4)
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Model_Number)
    gen:Line1   = Clip(gen:Line1) & ';' & CLIP(job:Fault_Code12)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_code1)
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:esn)                !Allow for Third Party
    Else !IMEIError# = 1
        gen:Line1   = Clip(gen:Line1) & ';' & Clip(jot:OriginalIMEI)                !Allow for Third Party
    End !IMEIError# = 1
    
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    !Get Jobse!
    Access:Jobse.ClearKey(jobe:RefNumberKey)
    jobe:RefNumber = job:Ref_Number
    Access:Jobse.Fetch(jobe:RefNumberKey)
    gen:Line1   = Clip(gen:Line1) & ';' & CLIP(jobe:Network)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code3)
    gen:Line1   = Clip(gen:Line1) & ';' & Format(job:DOP,@d12)
    gen:Line1   = Clip(gen:Line1) & ';' & Format(job:Date_Booked,@d12)
    If job:Exchange_Unit_Number <> ''
        If job:Exchange_Despatched <> ''
            gen:Line1   = Clip(gen:Line1) & ';' & Format(job:Exchange_Despatched,@d12)
        Else !If job:Exchange_Despatched <> ''
            If job:Date_Despatched <> ''
                gen:Line1   = Clip(gen:Line1) & ';' & Format(job:Date_Despatched,@d12)
            Else !If job:Date_Despatched <> ''
                gen:Line1   = Clip(gen:Line1) & ';' & ''
            End !If job:Date_Despatched <> ''
        End !If job:Exchange_Despatched <> ''
    Else !If job:Exchange_Unit_Number <> ''
        If job:Date_Despatched <> ''
            gen:Line1   = Clip(gen:Line1) & ';' & Format(job:Date_Despatched,@d12)
        Else !If job:Date_Despatched <> ''
            gen:Line1   = Clip(gen:Line1) & ';' & ''
        End !If job:Date_Despatched <> ''
    End !If job:Exchange_Unit_Number <> ''

    gen:Line1   = Clip(gen:Line1) & ';' & Format(job:Date_Completed,@d12)
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code5)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code6)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code2)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code7)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code8)
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code9)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code10)
    gen:Line1   = Clip(gen:Line1) & ';' & Clip(job:Fault_Code11)
    gen:Line1   = Clip(gen:Line1) & ';' & ''
    count+= 1
    Access:EXPGEN.Insert()

    Clear(gen:Record)
    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)
        gen:Line1   = 'M'
        gen:Line1   = Clip(gen:Line1) & ';' & Clip(wpr:Part_Number)
        gen:Line1   = Clip(gen:Line1) & ';' & ''
        gen:Line1   = Clip(gen:Line1) & ';' & Clip(wpr:Fault_Code1)
        gen:Line1   = Clip(gen:Line1) & ';' & Clip(wpr:Fault_Code2)
        Access:EXPGEN.Insert()
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)


    
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Siemens_DECT_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Siemens_DECT_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Siemens_DECT_EDI',1)
    SolaceViewVars('save_job_id',save_job_id,'Siemens_DECT_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Siemens_DECT_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Siemens_DECT_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Siemens_DECT_EDI',1)
    SolaceViewVars('pos',pos,'Siemens_DECT_EDI',1)
    SolaceViewVars('thefilename',thefilename,'Siemens_DECT_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
