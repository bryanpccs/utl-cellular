

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01027.INC'),ONCE        !Local module procedure declarations
                     END


ExcludeFromEDI       PROCEDURE  (func:PartNumber)     ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ExcludeFromEDI')      !Add Procedure to Log
  end


    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = func:PartNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        If sto:ExcludeFromEDI
            Return Level:Fatal
        End !If sto:ExcludeFromEDI
    Else ! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Error
    End !If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ExcludeFromEDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
