

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01033.INC'),ONCE        !Local module procedure declarations
                     END


Motorola_EDI         PROCEDURE  (f_batch_number)      ! Declare Procedure
save_job_Ali_id      USHORT,AUTO
save_job_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_mod_id          USHORT,AUTO
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
sav:Path             STRING(255)
tmp:ManufacturerName STRING(30)
tmp:totalClaims      LONG
tmp:TotalLines       LONG
pos                  STRING(255)
EDIVersion           LONG
TempDate             DATE
tmp:PartNo           STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(250)
          . . .

Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
record_id      STRING(3)
dealer_code    STRING(3)
supp_code      STRING(8)
country        STRING(3)
warr_claim     STRING(12)
exch_code      STRING(2)
prod_code      STRING(4)
msn_in         STRING(14)
msn_out        STRING(14)
fault_code     STRING(2)
repair_code    STRING(2)
esn_in         STRING(18)
esn_out        STRING(18)
bill_acc       STRING(6)
rep_date       STRING(8)
rep_time       STRING(8)
cycle_time     STRING(8)
war_flag       STRING(1)
dop            STRING(8)
add_code       String(2)
res1           String(20)
res2           String(20)
res3           String(20)
res4           String(20)
res5           String(20)
          .

Out_Part   GROUP,OVER(Ouf:Out_Group),PRE(L2)
record2_id     STRING(3)
supp_code2     STRING(8)
war_code2      STRING(12)
part_no        STRING(20)
qty_rep        STRING(2)
qty_exch       STRING(2)
usage          String(1)
res6           String(20)
res7           String(20)
         .

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count    LONG
NewOutFile FILE,DRIVER('ASCII'),PRE(NOUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
NewOutGroup     GROUP
Line1           STRING(3000)
          . . .

HeaderLine1 GROUP,OVER(nouf:NewOutGroup),PRE(H1)
RecordIdentifier        String(4)
FileType                String(6)
DateGenerated           String(16)
MASCCode                String(8)
SiteCode                String(3)
Reserved1               String(50)
            End

HeaderLine2 GROUP,OVER(nouf:NewOutGroup),PRE(H2)
RecordIdentifier        String(4)
DateSent                String(16)
PackageVersion          String(12)
DataVersion             String(12)
Reserved1               String(50)
            End

HeaderLine3 Group,Over(nouf:NewOutGroup),Pre(h3)
RecordIdentifier        String(4)
Reserved1               String(20)
Reserved2               String(50)
Reserved3               String(50)
            End

ClaimDetail Group,Over(nouf:NewOutGroup),Pre(cd)
RecordIdentifier        String(3)
ConsumerTitle           String(3)
ConsumerFirstName       String(40)
ConsumerSurname         String(40)
Address1                String(50)
Address2                String(50)
Address3                String(50)
Address4                String(50)
Address5                String(50)
Address6                String(40)
Address7                String(40)
Address8                String(10)
ConsumerPhoneNumber     String(30)
ConsumerEmailAddress    String(100)
ConsumerType            String(6)
ConsumerGender          String(1)
ConsumerAgeGroup        String(2)
CountryCode             String(3)
CourierTrackingNumberIn String(20)
CourierTrackingNumberOut    String(20)
WarrantyClaim           String(12)
CallCentreNumber        String(20)
BillingAccountNumber    String(6)
TransactionCode         String(3)
ProductCode             String(4)
TransceiverCode         String(15)
ModelCode               String(15)
CIMNumber               String(10)
MechSerialNumberIn      String(14)
MechSerialNumberOut        String(14)
ElecSerialNumberIn      String(18)
ElecSerialNumberOut     String(18)
FaultCode               String(3)
ComsumerPerceivedFaultCode  String(3)
RepairCode              String(3)
RepairStatus            String(3)
PartsNotAvailable1      String(20)
PartsNotAvailable2      String(20)
DateReceived            String(10)
TimeReceived            String(5)
ExpectedShipDate        String(10)
ExpectedShipTime        String(5)
DateDespatched          String(10)
TimeDespatched          String(5)
RepairDate              String(10)
RepairTime              String(8)
RepairCycleTime         String(8)
ConsumerCycleTime       String(8)
POPWarrantyClaim        String(1)
DateOfPurchase          String(10)
Reserved1               String(50)
Reserved2               String(50)
Reserved3               String(50)
Reserved4               String(20)
Reserved5               String(20)
Reserved6               String(20)
Reserved7               String(20)
Reserved8               String(20)
Reserved9               String(20)
Reserved10              String(20)
Reserved11              String(10)
Reserved12              String(10)
Reserved13              String(10)
Reserved14              String(10)
Reserved15              String(10)
                        End

ComponentDetail  Group,Over(nouf:NewOutGroup),Pre(cdd)
RecordIdentifier        String(3)
MASCCode                String(8)
WarrantyClaimNumber     String(12)
PartReplaced            String(20)
QuantityReplaced        String(2)
QuantityExchanged       String(2)
RepairOrRefurbish       String(1)
Reserved1               String(50)
Reserved2               String(50)
Reserved3               String(50)
Reserved4               String(20)
Reserved5               String(10)
                End

TrailerRecord   Group,Over(nouf:NewOutGroup),Pre(tr)
RecordIdentifier        String(3)
NumberOfClaims          String(20)
NumberOfCompenentsLines String(20)
Reserved1               String(20)
                End

! Start Change 2894 BE(09/07/03)
V2OutFile   FILE,DRIVER('ASCII'),PRE(V2OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD          RECORD
V2OutGroup          GROUP
Line1                   STRING(3000)
                    END
                END
            END

V2HeaderLine1 GROUP,OVER(v2ouf:V2OutGroup),PRE(V2H1)
RecordIdentifier                    STRING(4)
FileType                            STRING(6)
DateGenerated                       STRING(16)
MASCCode                            STRING(8)
SiteCode                            STRING(3)
Reserved1                           STRING(50)
            END

V2HeaderLine2 GROUP,OVER(v2ouf:V2OutGroup),PRE(V2H2)
RecordIdentifier                    STRING(4)
DateSent                            STRING(16)
PackageVersion                      STRING(12)
DataVersion                         STRING(12)
MASCFilename                        STRING(50)
            END

V2HeaderLine3 GROUP,OVER(v2ouf:V2OutGroup),PRE(V2H3)
RecordIdentifier                    STRING(4)
Reserved1                           STRING(20)
Reserved2                           STRING(50)
Reserved3                           STRING(50)
            END

V2ClaimDetail GROUP,OVER(v2ouf:V2OutGroup),PRE(V2CD)
RecordIdentifier                    STRING(3)
ConsumerTitle                       STRING(3)
ConsumerFirstName                   STRING(40)
ConsumerSurname                     STRING(40)
Address1                            STRING(50)
Address2                            STRING(50)
Address3                            STRING(50)
Address4                            STRING(50)
Address5                            STRING(50)
Address6                            STRING(40)
Address7                            STRING(40)
Address8                            STRING(10)
ConsumerPhoneNumber                 STRING(30)
ConsumerEmailAddress                STRING(100)
FieldBulletinNumber                 STRING(6)
ConsumerGender                      STRING(1)
ConsumerAgeGroup                    STRING(2)
CountryCode                         STRING(3)
CourierTrackingNumberIn             STRING(20)
CourierTrackingNumberOut            STRING(20)
WarrantyClaim                       STRING(12)
CallCentreNumber                    STRING(20)
AirtimeCarrierCode                  STRING(6)
TransactionCode                     STRING(3)
ProductCode                         STRING(4)
TransceiverCode                     STRING(15)
ModelCode                           STRING(15)
FactoryCode                         STRING(10)
MSNIn                               STRING(14)
MSNOut                              STRING(14)
IMEIIn                              STRING(18)
IMEIOut                             STRING(18)
FaultCode                           STRING(3)
ComsumerPerceivedFaultCode          STRING(3)
RepairCode                          STRING(3)
RepairStatus                        STRING(3)
PartsNotAvailable1                  STRING(20)
PartsNotAvailable2                  STRING(20)
DateReceived                        STRING(10)
TimeReceived                        STRING(5)
ExpectedShipDate                    STRING(10)
ExpectedShipTime                    STRING(5)
DateDespatched                      STRING(10)
TimeDespatched                      STRING(5)
RepairDate                          STRING(10)
RepairTime                          STRING(8)
RepairCycleTime                     STRING(8)
ConsumerCycleTime                   STRING(8)
POPWarrantyClaim                    STRING(1)
DateOfPurchase                      STRING(10)
Reserved1                          STRING(11)
Reserved2                          STRING(11)
SoftwareVersionIn                   STRING(10)
SoftwareVersionOut                  STRING(10)
GlobalCustomerComplaintCode         STRING(8)
Reserved3                           STRING(9)
GlobalPrimaryProblemFound           STRING(8)
GlobalSecondaryProblemFound         STRING(8)
GlobalPrimaryRepairCode             STRING(8)
GlobalSecondaryRepairCode           STRING(8)
Reserved4                           STRING(11)
Airtime                             STRING(20)
Reserved5                           STRING(10)
SpecialProjectNumber                STRING(10)
BillingAccountNumber                STRING(20)
Reserved6                           STRING(10)
Reserved7                           STRING(10)
Reserved8                           STRING(15)
AccessoryDateCode                   STRING(5)
MobileNumber                        STRING(12)
Reserved9                           STRING(10)
CustomerContactDate                 STRING(10)
CustomerContactTime                 STRING(5)
Reserved10                          STRING(23)
Reserved11                          STRING(10)
Reserved12                          STRING(2)
Reserved13                          STRING(8)
Reserved14                          STRING(10)
Reserved15                          STRING(10)
Reserved16                          STRING(10)
Reserved17                          STRING(10)
Reserved18                          STRING(10)
                END

V2ComponentDetail  GROUP,OVER(v2ouf:V2OutGroup),PRE(V2CDD)
RecordIdentifier                    STRING(3)
MASCCode                            STRING(8)
WarrantyClaimNumber                 STRING(12)
PartReplaced                        STRING(20)
QuantityReplaced                    STRING(2)
QuantityExchanged                   STRING(2)
RepairOrRefurbish                   STRING(1)
ReferenceDesignator                 STRING(6)
ReferenceDesignatorNumber           STRING(6)
PartFailureCode                     STRING(5)
Reserved1                           STRING(5)
Reserved2                           STRING(28)
Reserved3                           STRING(50)
Reserved4                           STRING(50)
Reserved5                           STRING(20)
Reserved6                           STRING(10)
                END

V2TrailerRecord   GROUP,OVER(v2ouf:V2OutGroup),PRE(V2TR)
RecordIdentifier                    STRING(3)
NumberOfClaims                      STRING(20)
NumberOfCompenentsLines             STRING(20)
Reserved1                           STRING(20)
                END
! End Change 2894 BE(09/07/03)

! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Motorola_EDI')      !Add Procedure to Log
  end


   Relate:EXCHANGE.Open
   Relate:MANUFACT.Open
   Relate:JOBS.Open
   Relate:WARPARTS.Open
   Relate:DEFEDI.Open
   Relate:EDIBATCH.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
   Relate:JOBSTAGE.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:PARTS.Open
   Relate:MODELNUM.Open
   Relate:STOCK.Open
   Relate:JOBSE.Open
   Relate:DEFAULTS.Open
!**Variable
    tmp:ManufacturerName = 'MOTOROLA'
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Benign
            Set(DEFEDI)
            If Access:DEFEDI.Next()
                If Access:DEFEDI.PrimeRecord() = Level:Benign
                    edi:Dealer_ID  = 'N/A'
                    If Access:DEFEDI.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:DEFEDI.TryInsert() = Level:Benign
                        !Insert Failed
                    End!If Access:DEFEDI.TryInsert() = Level:Benign
                End !If Access:DEFEDI.PrimeRecord() = Level:Benign
                Case MessageEx('Motorola EDI Defaults have not been setup.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                ManError# = 1
            End !If Access:DEFEDI.Next()
        Else!!If FindManufacturer(tmp:ManufacturerName) = Level:Benign
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            ! Start Change 2894 BE(09/07/03)
            MaxVersion# = 2
            IF (man:SiemensNewEDI <= MaxVersion#) THEN
                EDIVersion =  man:SiemensNewEDI
            ELSE
                EDIVersion =  MaxVersion# - 1
            END
            ! End Change 2894 BE(09/07/03)

            Error# = 0
!**Variable
            ! Start Change 2894 BE(09/07/03)
            !If man:SiemensNewEDI
            IF (EDIVersion = 1) THEN
            ! End Change 2894 BE(09/07/03)
                filename = CLIP(CLIP(MAN:EDI_Path)&'\MEFSIN')
                OPEN(NewOutFile)                           ! Open the output file
                IF ERRORCODE()                           ! If error
                    CREATE(NewOutFile)                       ! create a new file
                    If Errorcode()
                        Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!If Errorcode() 
                    OPEN(NewOutfile)        ! If still error then stop
                ELSE
                    OPEN(NewOutfile)
                    EMPTY(NewOutFile)
                END
            ! Start Change 2894 BE(09/07/03)
            ELSIF (EDIVersion = 2) THEN
                filename = CLIP(CLIP(MAN:EDI_Path)&'\MEFSIN')
                OPEN(V2OutFile)                           ! Open the output file
                IF ERRORCODE()                           ! If error
                    CREATE(V2OutFile)                       ! create a new file
                    If Errorcode()
                        Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!If Errorcode() 
                    OPEN(V2OutFile)        ! If still error then stop
                ELSE
                    OPEN(V2OutFile)
                    EMPTY(V2OutFile)
                END
            ! End Change 2894 BE(09/07/03)
            Else !If man:SiemensNewEDI
                filename = CLIP(CLIP(MAN:EDI_Path)&'\MEFSIN')
                OPEN(Out_File)                           ! Open the output file
                IF ERRORCODE()                           ! If error
                    CREATE(Out_File)                       ! create a new file
                    If Errorcode()
                        Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!If Errorcode() 
                    OPEN(out_file)        ! If still error then stop
                ELSE
                    OPEN(out_file)
                    EMPTY(Out_file)
                END

            End !If man:SiemensNewEDI
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    ! Start Change 2894 BE(09/07/03)
                    !If man:SiemensNewEDI
                    IF (EDIVersion = 1) THEN
                    ! End Change 2894 BE(09/07/03)
                        Do NewHeader
                    ! Start Change 2894 BE(09/07/03)
                    ELSIF (EDIVersion = 2) THEN
                        Do V2Header
                    ! End Change 2894 BE(09/07/03)
                    End !If man:SiemensNewEDI

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        If tmp:cancel = 1
                            Break
                        End!If tmp:cancel = 1

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate

                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'

                        ! Start Change 2894 BE(09/07/03)
                        !If man:SiemensNewEDI
                        IF (EDIVersion = 1) THEN
                        ! End Change 2894 BE(09/07/03)
                            Do NewExport
                        ! Start Change 2894 BE(09/07/03)
                        ELSIF (EDIVersion = 2) THEN
                            Do V2Export
                        ! End Change 2894 BE(09/07/03)
                        Else !If man:SiemensNewEDI
                            Do Export
                        End !If man:SiemensNewEDI

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        pos = Position(job:EDI_Key)
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                        ! Start Change 2894 BE(09/07/03)
                        !If man:SiemensNewEDI
                        !Else !If man:SiemensNewEDI
                        IF (EDIVersion = 0) THEN
                        ! End Change 2894 BE(09/07/03)
                            Do Export_Spares
                        End !If man:SiemensNewEDI
                        
                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    !Now go through the incomplete jobs and export
                    ! Start Change 2894 BE(09/07/03)
                    !If man:SiemensNewEDI
                    IF (EDIVersion > 0) THEN
                    ! End Change 2894 BE(09/07/03)
                        RecordsPerCycle = 25
                        RecordsProcessed = 0
                        PercentProgress = 0
                        OPEN(ProgressWindow)
                        Progress:Thermometer = 0
                        RecordsToProcess    = Records(MODELNUM)
                        ?Progress:PctText{Prop:Text} = '0% Completed'
                        ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                        Save_mod_ID = Access:MODELNUM.SaveFile()
                        Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                        mod:Manufacturer = 'MOTOROLA'
                        Set(mod:Manufacturer_Key,mod:Manufacturer_Key)
                        Loop
                            If Access:MODELNUM.NEXT()
                               Break
                            End !If
                            If mod:Manufacturer <> 'MOTOROLA'|
                                Then Break.  ! End If
                            Do GetNextRecord2
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1

                            Save_job_ID = Access:JOBS.SaveFile()
                            Access:JOBS.ClearKey(job:ModelCompKey)
                            job:Model_Number   = mod:Model_Number
                            job:Date_Completed = 0
                            Set(job:ModelCompKey,job:ModelCompKey)
                            Loop
                                If Access:JOBS.NEXT()
                                   Break
                                End !If
                                If job:Model_Number   <> mod:Model_Number      |
                                Or job:Date_Completed <> 0      |
                                    Then Break.  ! End If
                                If job:Warranty_Job <> 'YES'
                                    Cycle
                                End !If job_ali:Warranty_Job <> 'YES'

                                ! Start Change 4267 BE(10/05/04)
                                IF ((job:workshop <> 'YES') OR |
                                    (job:warranty_charge_type = '')) THEN
                                    CYCLE
                                END
                                ! End Change 4267 BE(10/05/04)

                                ! Start Change 2894 BE(09/07/03)
                                !Do NewExport
                                IF (EDIVersion = 1) THEN
                                    DO NewExport
                                ELSE
                                    DO V2Export
                                END
                                ! End Change 2894 BE(09/07/03)

                            End !Loop
                            Access:JOBS.RestoreFile(Save_job_ID)

                        End !Loop
                        Access:MODELNUM.RestoreFile(Save_mod_ID)

                        ! Start Change 2894 BE(09/07/03)
                        !Do NewTrailer
                        IF (EDIVersion = 1) THEN
                            Do NewTrailer
                        ELSE
                            DO V2Trailer
                        END
                        ! End Change 2894 BE(09/07/03)
                        Close(ProgressWindow)
                    End !If man:SiemensNewEDI

                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(CLIP(MAN:EDI_Path)&'\MEFSIN') & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx

                        glo:select1 = ebt:Batch_Number
                        glo:Select3 = tmp:ManufacturerName
                        Nokia_Accessories_Claim_Report
                        glo:Select1 = ''
                        glo:Select3 = ''
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    ! Start Change 2894 BE(09/07/03)
                    !If man:SiemensNewEDI
                    !    Do NewHeader
                    IF (EDIVersion = 1) THEN
                        Do NewHeader
                    ELSIF (EDIVersion = 2) THEN
                        Do V2Header
                    ! End Change 2894 BE(09/07/03)
                    End !If man:SiemensNewEDI

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        If tmp:cancel = 1
                            Break
                        End!If tmp:cancel = 1

                        ! Start Change 2894 BE(09/07/03)
                        !If man:SiemensNewEDI
                        !    Do NewExport
                        IF (EDIVersion = 1) THEN
                            DO NewExport
                        ELSIF (EDIVersion = 2) THEN
                            DO V2Export
                        ! End Change 2894 BE(09/07/03)
                        Else !If man:SiemensNewEDI
                            Do Export
                        End !If man:SiemensNewEDI

                        ! Start Change 2894 BE(09/07/03)
                        !If man:SiemensNewEDI
                        !Else !If man:SiemensNewEDI
                        IF (EDIVersion = 0) THEN
                        ! End Change 2894 BE(09/07/03)
                            Do Export_Spares
                        End !If man:SiemensNewEDI
                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    !Now go through the incomplete jobs and export
                    ! Start Change 2894 BE(09/07/03)
                    !If man:SiemensNewEDI
                    IF (EDIVersion > 0) THEN
                    ! End Change 2894 BE(09/07/03)
                        RecordsPerCycle = 25
                        RecordsProcessed = 0
                        PercentProgress = 0
                        OPEN(ProgressWindow)
                        Progress:Thermometer = 0
                        RecordsToProcess    = Records(MODELNUM)
                        ?Progress:PctText{Prop:Text} = '0% Completed'
                        ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                        Save_mod_ID = Access:MODELNUM.SaveFile()
                        Access:MODELNUM.ClearKey(mod:Manufacturer_Key)
                        mod:Manufacturer = 'MOTOROLA'
                        Set(mod:Manufacturer_Key,mod:Manufacturer_Key)
                        Loop
                            If Access:MODELNUM.NEXT()
                               Break
                            End !If
                            If mod:Manufacturer <> 'MOTOROLA'|
                                Then Break.  ! End If
                            Do GetNextRecord2
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1

                            Save_job_ID = Access:JOBS.SaveFile()
                            Access:JOBS.ClearKey(job:ModelCompKey)
                            job:Model_Number   = mod:Model_Number
                            job:Date_Completed = 0
                            Set(job:ModelCompKey,job:ModelCompKey)
                            Loop
                                If Access:JOBS.NEXT()
                                   Break
                                End !If
                                If job:Model_Number   <> mod:Model_Number      |
                                Or job:Date_Completed <> 0      |
                                    Then Break.  ! End If

                                ! Start Change 4267 BE(10/05/04)
                                IF ((job:workshop <> 'YES') OR |
                                    (job:warranty_charge_type = '')) THEN
                                    CYCLE
                                END
                                ! End Change 4267 BE(10/05/04)

                                If job:Warranty_Job <> 'YES'
                                    Cycle
                                End !If job_ali:Warranty_Job <> 'YES'

                                ! Start Change 2894 BE(09/07/03)
                                !Do NewExport
                                IF (EDIVersion = 1) THEN
                                    DO NewExport
                                ELSE
                                    DO V2Export
                                END
                                ! End Change 2894 BE(09/07/03)

                            End !Loop
                            Access:JOBS.RestoreFile(Save_job_ID)

                        End !Loop
                        Access:MODELNUM.RestoreFile(Save_mod_ID)

                        ! Start Change 2894 BE(09/07/03)
                        !Do NewTrailer
                        IF (EDIVersion = 1) THEN
                            DO NewTrailer
                        ELSE
                            DO V2Trailer
                        END
                        ! End Change 2894 BE(09/07/03)
                        Close(ProgressWindow)
                    End !If man:SiemensNewEDI

                    ! Start Change 2806 BE(22/07/03)
                    !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !  '<13,10>'&|
                    !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(CLIP(MAN:EDI_Path)&'\MEFSIN') & '.','ServiceBase 2000',|
                    !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !    Of 1 ! &OK Button
                    !End!Case MessageEx
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(CLIP(MAN:EDI_Path)&'\MEFSIN') & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(CLIP(MAN:EDI_Path)&'\MEFSIN') & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)

                    glo:select1 = f_batch_number
                    glo:select3 = tmp:ManufacturerName
                    Nokia_Accessories_Claim_Report
                    glo:select1 = ''
                    glo:Select3 = ''

                End!If f_batch_number = 0

                ! Start Change 2894 BE(09/07/03)
                !If man:SiemensNewEDI
                !    Close(NewOutFile)
                IF (EDIVersion = 1) THEN
                    CLOSE(NewOutFile)
                ELSIF (EDIVersion = 2) THEN
                    CLOSE(V2OutFile)
                ! End Change 2894 BE(09/07/03)
                Else !If man:SiemensNewEDI
                    CLOSE(out_file)

                End !If man:SiemensNewEDI

            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:EXCHANGE.Close
   Relate:MANUFACT.Close
   Relate:JOBS.Close
   Relate:WARPARTS.Close
   Relate:DEFEDI.Close
   Relate:EDIBATCH.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
   Relate:JOBSTAGE.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:PARTS.Close
   Relate:MODELNUM.Close
   Relate:STOCK.Close
   Relate:JOBSE.Close
   Relate:DEFAULTS.Close
Export      Routine

    CLEAR(ouf:RECORD)
    CLEAR(mt_total)
    l1:record_id   = 'HDR'
    l1:dealer_code = EDI:Dealer_ID
    l1:supp_code   = EDI:Supplier_Code
    l1:country     = EDI:Country_Code
    l1:warr_claim  = job:Ref_Number
    l1:exch_code   = job:Fault_Code4
    l1:prod_code   = FORMAT(job:Fault_Code1,@s4)
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        l1:esn_in  = job:ESN
        l1:msn_in   = job:MSN
    Else !IMEIError# = 1
        l1:esn_in  = jot:OriginalIMEI
        l1:msn_in   = jot:OriginalMSN
    End !IMEIError# = 1

    IF job:Exchange_Unit_Number <> ''
        access:exchange.clearkey(xch:ref_number_key)
        XCH:Ref_Number = job:Exchange_Unit_Number
        If access:exchange.fetch(xch:ref_number_key)
              l1:msn_out = job:MSN
              l1:esn_out = job:ESN
        Else!If access:exchange.fetch(xch:ref_number_key)
            l1:msn_out = XCH:MSN
              l1:esn_out = XCH:ESN
        End!If access:exchange.fetch(xch:ref_number_key)
    ELSE!IF job:Exchange_Unit_Number <> ''
        l1:msn_out = job:MSN
        l1:esn_out = job:ESN
    END!IF job:Exchange_Unit_Number <> ''
    l1:fault_code = FORMAT(job:Fault_Code2,@n02)
    l1:repair_code = FORMAT(job:Fault_Code3,@n02)
    l1:bill_acc    = MAN:EDI_Account_Number
    l1:rep_date    = FORMAT(DAY(job:Date_Completed),@n02)&'/'&FORMAT(MONTH(job:Date_Completed),@n02)&'/'&FORMAT(SUB(YEAR(job:Date_Completed),3,2),@n02)
    !---How do we work out the cycle time?!
    l1:rep_time = FORMAT(days,@n02)&':'&FORMAT(time,@t01)
    l1:cycle_time = FORMAT(days,@n02)&':'&FORMAT(time,@t01)
    !---These 2 are needed!
    l1:war_flag    = 'Y'
    l1:dop         = FORMAT(DAY(job:DOP),@n02)&'/'&FORMAT(MONTH(job:DOP),@n02)&'/'&FORMAT(SUB(YEAR(job:DOP),3,2),@n02)
    count# += 1
    ADD(out_file)
    CLEAR(ouf:RECORD)
Export_Spares       Routine
    setcursor(cursor:wait)
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)

        l2:record2_id = 'DET'
        l2:supp_code2 = EDI:Supplier_Code
        l2:war_code2  = job:Ref_Number
        l2:part_no    = WPR:Part_Number
        l2:qty_rep    = FORMAT(WPR:Quantity,@N02)
        l2:qty_exch   = '0'
        l2:usage      = ' '
        ADD(out_file)
        CLEAR(ouf:RECORD)

    end !loop
    access:warparts.restorefile(save_wpr_id)
    setcursor()
NewExport       Routine
    Clear(nouf:Record)
    cd:RecordIdentifier = 'CLM'
    cd:ConsumerTitle    = job:Title
    cd:ConsumerFirstName   = job:Initial
    cd:ConsumerSurname  = job:Surname
    cd:Address1         = job:Address_Line1
    cd:Address2         = job:Address_Line2
    cd:Address3         = job:Address_Line3
    cd:Address4         = ''
    cd:Address5         = ''
    cd:Address6         = ''
    cd:Address7         = job:Postcode
    cd:Address8         = ''
    cd:ConsumerPhoneNumber  = job:Telephone_Number
    cd:ConsumerEmailAddress = ''
    cd:ConsumerType     = ''
    cd:ConsumerGender   = ''
    cd:ConsumerAgeGroup = ''
    cd:CountryCode      = edi:Country_Code
    cd:CourierTrackingNumberIn  = job:Incoming_Consignment_Number
    cd:CourierTrackingNumberOut = job:Consignment_Number
    cd:WarrantyClaim    = job:Ref_Number
    cd:CallCentreNumber = ''
    cd:BillingAccountNumber = ''
    cd:TransactionCode  = job:Fault_Code4
    cd:ProductCode      = job:Fault_Code1
    cd:TransceiverCode  = ''
    cd:ModelCode        = ''
    cd:CIMNumber        = ''

    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        cd:MechSerialNumberIn   = job:MSN
        cd:ElecSerialNumberIn   = job:ESN
    Else !IMEIError# = 1
        cd:ElecSerialNumberIn   = jot:OriginalIMEI
        cd:MechSerialNumberIn   =  jot:OriginalMSN
    End !IMEIError# = 1

    cd:MechSerialNumberOut  = job:MSN
    cd:ElecSerialNumberOut  = job:ESN
    cd:FaultCode            = job:Fault_Code2
    cd:ComsumerPerceivedFaultCode   = job:Fault_Code5
    cd:RepairCode                   = job:Fault_Code3

    If job:Date_Completed <> ''
        cd:RepairStatus = 'DES'
    Else !If job:Date_Completed <> ''
        If job:Cancelled = 'YES'
            cd:RepairStatus = 'CAN'
        Else !If job:Cancelled = 'YES'
            If CheckParts('W')
                cd:RepairStatus         = 'AWP'
            Else !If CheckParts('W')
                If job:Engineer <> ''
                    cd:RepairStatus     = 'INR'
                Else !If job:Engineer <> ''
                    cd:RepairStatus     = 'AWR'
                End !If job:Engineer <> ''
            End !If CheckParts('W')
        End !If job:Cancelled = 'YES'
    End !If job:Date_Completed <> ''

    cd:PartsNotAvailable1   = ''
    cd:PartsNotAvailable2   = ''
    ! Start Change 2387 BE(01/05/03)
    ! this code change was not carried forward from V3.1!!
    !cd:DateReceived         = Format(job:Date_Booked,@d06)
    cd:DateReceived         = Format(job:Date_Booked,@d06b)
    ! End Change 2387 BE(01/05/03)
    cd:TimeReceived         = format(job:Time_Booked,@t01)
    cd:ExpectedShipDate     = ''
    cd:ExpectedShipTime     = ''
    ! Start Change 2387 BE(01/05/03)
    ! this code change was not carried forward from V3.1!!
    !cd:DateDespatched       = Format(job:Date_Completed,@d06)
    cd:DateDespatched       = Format(job:Date_Completed,@d06b)
    ! End Change 2387 BE(01/05/03)
    cd:TimeDespatched       = Format(job:Time_Completed,@t01)
    ! Start Change 2387 BE(01/05/03)
    ! this code change was not carried forward from V3.1!!
    !cd:RepairDate           = Format(job:Date_Completed,@d06)
    cd:RepairDate           = Format(job:Date_Completed,@d06b)
    ! End Change 2387 BE(01/05/03)
    count# = 0
    loop
        count# += 1
        date$   = job:date_booked + count#
        If def:include_saturday <> 'YES'
            If date$    % 7 = 6
                Cycle
            End!If date$    % 7 - 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If date$ % 7 = 0
                Cycle
            End!If date$ % 7 = 0
        End!If def:include_sunday <> 'YES'
        If date$ >= job:date_completed
            Break
        End!If date$ >= job:date_completed
    End!loop
!    If job:time_completed > job:time_booked
!        cd:RepairTime      = Format(count#,@n02) & ':' & Format(job:time_completed - job:time_booked,@t01) !Leadtime
!    End!If job:time_completed > job:time_booked
!    If job:time_completed < job:time_booked
!        time$   = def:end_work_hours - job:time_booked + job:time_completed - def:start_work_hours
!        cd:RepairTime      = Format(count#-1,@n02) & ':' & Format(time$,@t1)                              !Leadtime
!    End!If job:time_completed < job:time_booked
!    If job:time_completed = job:time_booked
     cd:RepairTime      = Format(count#-1,@n02) & ':00:00'                                             !Leadtime
!    End!If job:time_completed - job:time_booked

    loop
        count# += 1
        date$   = job:date_booked + count#
        If def:include_saturday <> 'YES'
            If date$    % 7 = 6
                Cycle
            End!If date$    % 7 - 6
        End!If def:include_saturday <> 'YES'
        If def:include_sunday <> 'YES'
            If date$ % 7 = 0
                Cycle
            End!If date$ % 7 = 0
        End!If def:include_sunday <> 'YES'
        If job:Exchange_Unit_Number
            !Job has been exchanged
            If job:Exchange_Despatched = ''
                !If job has not been despatched
                Count# = 1
                Break
            Else !If job:Exchange_Despatched = ''
                If date$ >= job:Exchange_Despatched
                    Break
                End!If date$ >= job:date_completed
            End !If job:Exchange_Despatched = ''
        Else !If job:Exchange_Unit_Number
            If job:Date_Despatched = ''
                !If job has not been despatched
                Count# = 1
                Break
            Else !If job:Date_Despatched = ''
                If date$ >= job:Date_Despatched
                    Break
                End!If date$ >= job:date_completed

            End !If job:Date_Despatched = ''
        End !If job:Exchange_Unit_Number
    End!loop

!    If job:time_completed > job:time_booked
!        cd:RepairTime      = Format(count#,@n02) & ':' & Format(job:time_completed - job:time_booked,@t01) !Leadtime
!    End!If job:time_completed > job:time_booked
!    If job:time_completed < job:time_booked
!        time$   = def:end_work_hours - job:time_booked + job:time_completed - def:start_work_hours
!        cd:RepairTime      = Format(count#-1,@n02) & ':' & Format(time$,@t1)                              !Leadtime
!    End!If job:time_completed < job:time_booked
!    If job:time_completed = job:time_booked
    cd:RepairCycleTime      = Format(count#-1,@n02) & ':00:00'                                             !Leadtime
!    End!If job:time_completed - job:time_booked

    cd:ConsumerCycleTime    = ''
    If job:DOP <> ''
        cd:POPWarrantyClaim = 'Y'
    Else !If job:DOP <> ''
        cd:POPWarrantyClaim = 'N'
    End !If job:DOP <> ''
    ! Start Change 2387 BE(01/05/03)
    ! this code change was not carried forward from V3.1!!
    !cd:DateOfPurchase   = Format(job:DOP,@d06)
    cd:DateOfPurchase   = Format(job:DOP,@d06b)
    ! End Change 2387 BE(01/05/03)

    cd:Reserved1 = ''
    cd:Reserved2 = ''
    cd:Reserved3 = ''
    cd:Reserved4 = ''
    cd:Reserved5 = ''
    cd:Reserved6 = ''
    cd:Reserved7 = ''
    cd:Reserved8 = ''
    cd:Reserved9 = ''
    cd:Reserved10 = ''
    cd:Reserved11 = ''
    cd:Reserved12 = ''
    cd:Reserved13 = ''
    cd:Reserved14 = ''
    cd:Reserved15 = ''

    Add(NewOutFile)

    tmp:TotalClaims += 1

    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)
        Clear(nouf:Record)
        cdd:RecordIdentifier    = 'CMP'
        cdd:MASCCode            = man:EDI_Account_Number
        cdd:WarrantyClaimNumber = job:Ref_Number
        cdd:PartReplaced        = wpr:Part_Number
        cdd:QuantityReplaced    = wpr:Quantity
        cdd:QuantityExchanged = '0'
        cdd:RepairOrRefurbish = 'R'
        cdd:Reserved1           = ''
        cdd:Reserved2           = ''
        cdd:Reserved3           = ''
        cdd:Reserved4           = ''
        cdd:Reserved5           = ''
        Add(NewOutFile)
        tmp:TotalLines += 1
    End !Loop
    Access:WARPARTS.RestoreFile(Save_wpr_ID)


    
    


NewHeader       Routine
    Clear(nouf:Record)
    h1:RecordIdentifier = 'HDR'
    h1:FileType         = 'CLMCRT'
    h1:DateGenerated    = Format(Today(),@d06) & Format(Clock(),@t01)
    h1:MASCCode         = man:EDI_Account_Number
    h1:SiteCode         = man:Account_number
    h1:Reserved1         = ''
    Add(NewOutFile)

    Clear(nouf:Record)
    h2:RecordIdentifier = 'HDR2'
    h2:DateSent         = ''
    h2:PackageVersion   = ''
    h2:DataVersion      = ''
    h2:Reserved1        = ''
    Add(NewOutFile)

    Clear(nouf:Record)
    h3:RecordIdentifier = 'HDR3'
    h3:Reserved1        = ''
    h3:Reserved2        = ''
    h3:Reserved3        = ''
    Add(NewOutFile)
NewTrailer      Routine
    Clear(nouf:Record)
    tr:RecordIdentifier = 'TRA'
    tr:NumberOfClaims   = tmp:TotalClaims
    tr:NumberOfCompenentsLines = tmp:TotalLines
    tr:Reserved1 = ''
    Add(NewOutFile)

V2Export       ROUTINE    ! Start Change 2894 BE(09/07/03)
    CLEAR(v2ouf:Record)
    V2CD:RecordIdentifier = 'CLM'
    V2CD:ConsumerTitle    = job:Title
    V2CD:ConsumerFirstName   = job:Initial
    V2CD:ConsumerSurname  = job:Surname
    V2CD:Address1         = job:Address_Line1
    V2CD:Address2         = job:Address_Line2
    V2CD:Address3         = job:Address_Line3
    V2CD:Address4         = ''
    V2CD:Address5         = ''
    V2CD:Address6         = ''
    V2CD:Address7         = job:Postcode
    V2CD:Address8         = ''
    V2CD:ConsumerPhoneNumber  = job:Telephone_Number
    V2CD:ConsumerEmailAddress = ''
    V2CD:FieldBulletinNumber  = ''
    V2CD:ConsumerGender   = ''
    V2CD:ConsumerAgeGroup = ''
    V2CD:CountryCode      = edi:Country_Code
    V2CD:CourierTrackingNumberIn  = job:Incoming_Consignment_Number
    V2CD:CourierTrackingNumberOut = job:Consignment_Number
    V2CD:WarrantyClaim    = job:Ref_Number
    V2CD:CallCentreNumber = ''
    V2CD:AirtimeCarrierCode = ''
    V2CD:TransactionCode  = job:Fault_Code4
    V2CD:ProductCode      = job:Fault_Code1
    V2CD:TransceiverCode  = ''
    V2CD:ModelCode        = ''
    V2CD:FactoryCode      = ''

    IMEIError# = 0
    IF (job:Third_Party_Site <> '') THEN
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        SET(jot:RefNumberKey,jot:RefNumberKey)
        IF (Access:JOBTHIRD.NEXT() <> Level:benign) THEN
            IMEIError# = 1
        ELSE
            IF (jot:RefNumber <> job:Ref_Number) THEN
                IMEIError# = 1
            ELSE
                IMEIError# = 0                
            END
        END
    ELSE
        IMEIError# = 1    
    END

    IF (IMEIError# = 1) THEN
        V2CD:MSNIn   = job:MSN
        access:JOBSE.clearkey(jobe:RefNumberKey)
        jobe:RefNumber  = job:Ref_Number
        IF ((access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) AND (jobe:Pre_RF_Board_IMEI <> '')) THEN
            V2CD:IMEIIn = jobe:Pre_RF_Board_IMEI
        ELSE
            V2CD:IMEIIn = job:ESN
        END
    ELSE
        V2CD:IMEIIn   = jot:OriginalIMEI
        V2CD:MSNIn   =  jot:OriginalMSN
    END

! Start Change 4641 BE(08/09/2004)
!    V2CD:MSNOut  = job:MSN
!    V2CD:IMEIOut  = job:ESN

    IF (job:exchange_unit_number = '') THEN
        V2CD:IMEIOut   = job:ESN
        V2CD:MSNOut   =  job:MSN
    ELSE
        access:exchange.clearkey(xch:ref_number_key)
        xch:ref_number = job:exchange_unit_number
        IF (access:exchange.fetch(xch:ref_number_key) = Level:Benign) THEN
            V2CD:IMEIOut = xch:esn
            V2CD:MSNOut  = xch:msn
        ELSE
            V2CD:IMEIOut   = job:ESN
            V2CD:MSNOut   =  job:MSN
        END
    END
! End Change 4641 BE(08/09/2004)

    !V2CD:FaultCode   =  job:Fault_Code2
    V2CD:FaultCode   =  ''
    !V2CD:ComsumerPerceivedFaultCode   = job:Fault_Code5
    V2CD:ComsumerPerceivedFaultCode   = ''
    !V2CD:RepairCode                   = job:Fault_Code3
    V2CD:RepairCode                   = ''

    IF (job:Date_Completed <> '') THEN
        V2CD:RepairStatus = 'DES'
    ELSIF (job:Cancelled = 'YES') THEN
        V2CD:RepairStatus = 'CAN'
    ELSE
        DO CheckAwaitingParts
        IF (tmp:PartNo <> '') THEN
            V2CD:RepairStatus = 'AWP'
        ELSIF (job:Engineer <> '') THEN
            V2CD:RepairStatus = 'INR'
        ELSE
            V2CD:RepairStatus = 'AWR'
        END
    END

    IF (V2CD:RepairStatus = 'AWP') THEN
        V2CD:PartsNotAvailable1   = tmp:PartNo
    ELSE
        V2CD:PartsNotAvailable1   = ''
    END
    V2CD:PartsNotAvailable2   = ''

! Start Change 4641 BE(08/09/2004)
!    V2CD:DateReceived         = FORMAT(job:Date_Booked,@d06b)
!    V2CD:TimeReceived         = FORMAT(job:Time_Booked,@t01)
    access:JOBSE.clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    IF ((access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) AND (jobe:InWorkshopDate <> '')) THEN
        V2CD:DateReceived = FORMAT(jobe:InWorkshopDate,@d06b)
        V2CD:TimeReceived = FORMAT(jobe:InWorkshopTime,@t01)
    ELSE
        V2CD:DateReceived = FORMAT(job:Date_Booked,@d06b)
        V2CD:TimeReceived = FORMAT(job:Time_Booked,@t01)
    END
! End Change 4641 BE(08/09/2004)

    V2CD:ExpectedShipDate     = ''
    V2CD:ExpectedShipTime     = ''

! Start Change 4641 BE(08/09/2004)
!    V2CD:DateDespatched       = FORMAT(job:Date_Completed,@d06b)
!    V2CD:TimeDespatched       = FORMAT(job:Time_Completed,@t01)
    IF (job:exchange_unit_number <> '') THEN
        IF (job:exchange_despatched = '') THEN
            ! Start Change xxx BE(05/10/2004)
            !V2CD:DateDespatched = ''
            !V2CD:TimeDespatched = ''
            V2CD:DateDespatched = '00/00/0000'
            V2CD:TimeDespatched = '00:00:00'
            ! End Change xxx BE(05/10/2004)
        ELSE
            V2CD:DateDespatched = FORMAT(job:exchange_despatched,@d06b)
            ! Start Change xxx BE(05/10/2004)
            V2CD:TimeDespatched = '00:00:00'
            ! End Change xxx BE(05/10/2004)
            access:audit.clearkey(aud:TypeRefKey)
            aud:Ref_Number = job:ref_number
            aud:Type       = 'EXC'
            SET(aud:TypeRefKey,aud:TypeRefKey)
            LOOP
                IF ((access:audit.next() <> Level:Benign) OR |
                    (aud:Ref_Number <> job:ref_number) OR |
                    (aud:Type <> 'EXC')) THEN
                    BREAK
                END
                IF (aud:Action[1 : 24] = 'EXCHANGE UNIT DESPATCHED') THEN
                    V2CD:TimeDespatched = FORMAT(aud:Time, @t01)
                    BREAK
                END
            END
        END
    ELSE
        IF (job:date_despatched = '') THEN
            ! Start Change xxx BE(05/10/2004)
            !V2CD:DateDespatched = ''
            !V2CD:TimeDespatched = ''
            V2CD:DateDespatched = '00/00/0000'
            V2CD:TimeDespatched = '00:00:00'
            ! End Change xxx BE(05/10/2004)
        ELSE
            V2CD:DateDespatched = FORMAT(job:date_despatched,@d06b)
            ! Start Change xxx BE(05/10/2004)
            !V2CD:TimeDespatched = ''
            V2CD:TimeDespatched = '00:00:00'
            ! End Change xxx BE(05/10/2004)
            access:audit.clearkey(aud:TypeRefKey)
            aud:Ref_Number = job:ref_number
            aud:Type       = 'JOB'
            SET(aud:TypeRefKey,aud:TypeRefKey)
            LOOP
                IF ((access:audit.next() <> Level:Benign) OR |
                    (aud:Ref_Number <> job:ref_number) OR |
                    (aud:Type <> 'JOB')) THEN
                    BREAK
                END
                IF (aud:Action[1 : 14] = 'JOB DESPATCHED') THEN
                    V2CD:TimeDespatched = FORMAT(aud:Time, @t01)
                    BREAK
                END
            END
        END
    END
! End Change 4641 BE(08/09/2004)

    V2CD:RepairDate           = FORMAT(job:Date_Completed,@d06b)

! Start Change 4641 BE(08/09/2004)
    st_date# = job:date_booked
    access:JOBSE.clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    IF ((access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) AND (jobe:InWorkshopDate <> '')) THEN
        st_date# = jobe:InWorkshopDate
    END
! End Change 4641 BE(08/09/2004)

    count# = 0
    ! Start Change 4641 BE(08/09/2004)
    daycount# = 0
    ! End Change 4641 BE(08/09/2004)
    LOOP
        ! Start Change 4641 BE(08/09/2004)
        !count# += 1
        !TempDate   = job:date_booked + count#
        TempDate   = st_date# + count#
        count# += 1
        ! End Change 4641 BE(08/09/2004)
        DayNo# = TempDate % 7
        IF (def:include_saturday <> 'YES') THEN
            IF (DayNo# = 6) THEN
                CYCLE
            END
        END
        IF (def:include_sunday <> 'YES') THEN
            IF (DayNo# = 0) THEN
                CYCLE
            END
        END

        ! Start Change 4641 BE(08/09/2004)
        daycount# += 1
        ! End Change 4641 BE(08/09/2004)

        IF (TempDate >= job:date_completed) THEN
            BREAK
        END
    END
    ! Start Change 4641 BE(08/09/2004)
    !V2CD:RepairTime      = Format(count#-1,@n02) & ':00:00'       !Leadtime
    V2CD:RepairTime      = Format(daycount#-1,@n02) & ':00:00'       !Leadtime
    ! End Change 4641 BE(08/09/2004)

! Start Change 4641 BE(08/09/2004)
    count# = 0
    daycount# = 0
! End Change 4641 BE(08/09/2004)

! Start Change 4641 BE(08/09/2004)
    IF ((job:Exchange_Unit_Number <> '') AND (job:Exchange_Despatched = '')) OR |
        ((job:Exchange_Unit_Number = '') AND (job:Date_Despatched = '')) THEN
        ! Start Change xxx BE(05/10/2004)
        !V2CD:RepairCycleTime = ''
        V2CD:RepairCycleTime = '00:00:00'
        ! Start Change xxx BE(05/10/2004)
    ELSE
! End Change 4641 BE(08/09/2004)
        LOOP
            ! Start Change 4641 BE(08/09/2004)
            !count# += 1
            !TempDate   = job:date_booked + count#
            TempDate   = st_date# + count#
            count# += 1
            ! End Change 4641 BE(08/09/2004)
            DayNo# = TempDate % 7
            IF (def:include_saturday <> 'YES') THEN
                IF (DayNo# = 6) THEN
                    CYCLE
                END
            END
            IF (def:include_sunday <> 'YES') THEN
                IF (DayNo# = 0) THEN
                    CYCLE
                END
            END

            ! Start Change 4641 BE(08/09/2004)
            daycount# += 1
            ! End Change 4641 BE(08/09/2004)

            IF (job:Exchange_Unit_Number) THEN
                !Job has been exchanged
                IF (job:Exchange_Despatched = '') THEN
                    !If job has not been despatched
                    ! Start Change 4641 BE(08/09/2004)
                    !Count# = 1
                    daycount# = 1
                    ! End Change 4641 BE(08/09/2004)
                    BREAK
                ELSE
                    IF (TempDate >= job:Exchange_Despatched) THEN
                        BREAK
                    END
                END
            ELSE
                IF (job:Date_Despatched = '')
                    !If job has not been despatched
                    ! Start Change 4641 BE(08/09/2004)
                    !Count# = 1
                    daycount# = 1
                    ! End Change 4641 BE(08/09/2004)
                    BREAK
                ELSE
                    IF (TempDate >= job:Date_Despatched) THEN
                        BREAK
                    END
                END
            END
        END

        ! Start Change 4641 BE(08/09/2004)
        !V2CD:RepairCycleTime      = Format(count#-1,@n02) & ':00:00'   !Leadtime
        V2CD:RepairCycleTime      = Format(daycount#-1,@n02) & ':00:00'   !Leadtime
        ! End Change 4641 BE(08/09/2004)

! Start Change 4641 BE(08/09/2004)
    END
! End Change 4641 BE(08/09/2004)

    V2CD:ConsumerCycleTime    = ''

    IF (job:DOP <> '') THEN
        V2CD:POPWarrantyClaim = 'Y'
    ELSE
        V2CD:POPWarrantyClaim = 'N'
    END

    V2CD:DateOfPurchase   = Format(job:DOP,@d06b)
    V2CD:Reserved1 = ''
    V2CD:Reserved2 = ''
    V2CD:SoftwareVersionIn = job:Fault_Code6
    V2CD:SoftwareVersionOut = job:Fault_Code7
    V2CD:GlobalCustomerComplaintCode = job:Fault_Code5
    V2CD:Reserved3 = ''
    V2CD:GlobalPrimaryProblemFound = job:Fault_Code2
    V2CD:GlobalSecondaryProblemFound = job:Fault_Code9
    V2CD:GlobalPrimaryRepairCode = job:Fault_Code3
    V2CD:GlobalSecondaryRepairCode = job:Fault_Code10
    V2CD:Reserved4 = ''
    V2CD:Airtime = job:Fault_Code8
    V2CD:Reserved5 = ''
    V2CD:SpecialProjectNumber = ''
    V2CD:BillingAccountNumber = ''
    V2CD:Reserved6 = ''
    V2CD:Reserved7 = ''
    V2CD:Reserved8 = ''
    V2CD:AccessoryDateCode = FORMAT(job:Fault_Code11, @d13b)
    IF (job:Mobile_Number = '') THEN
        V2CD:MobileNumber = 'NA'
    ELSE
        V2CD:MobileNumber = job:Mobile_Number
    END
    V2CD:Reserved9 = ''
    V2CD:CustomerContactDate = FORMAT(job:Date_Booked, @d06b)
    V2CD:CustomerContactTime = FORMAT(job:Time_Booked, @t01)
    V2CD:Reserved10 = ''
    V2CD:Reserved11 = ''
    V2CD:Reserved12 = ''
    V2CD:Reserved13 = ''
    V2CD:Reserved14 = ''
    V2CD:Reserved15 = ''
    V2CD:Reserved16 = ''
    V2CD:Reserved17 = ''
    V2CD:Reserved18 = ''

    ADD(V2OutFile)

    tmp:TotalClaims += 1

    Save_wpr_ID = Access:WARPARTS.SaveFile()
    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    SET(wpr:Part_Number_Key,wpr:Part_Number_Key)
    LOOP
        IF (Access:WARPARTS.NEXT() <> Level:benign) OR (wpr:Ref_Number  <> job:Ref_Number) THEN
           BREAK
        END

        IF (((man:includeadjustment <> 'YES') AND (wpr:part_number = 'ADJUSTMENT')) OR |
            (ExcludeFromEDI(wpr:Part_Ref_Number))) THEN
            CYCLE
        END

        CLEAR(v2ouf:Record)
        V2CDD:RecordIdentifier    = 'CMP'
        V2CDD:MASCCode            = man:EDI_Account_Number
        V2CDD:WarrantyClaimNumber = job:Ref_Number
        V2CDD:PartReplaced        = wpr:Part_Number
        V2CDD:QuantityReplaced    = wpr:Quantity
        V2CDD:QuantityExchanged = '0'
        V2CDD:RepairOrRefurbish = 'R'
        V2CDD:ReferenceDesignator = wpr:Fault_Code1
        V2CDD:ReferenceDesignatorNumber = wpr:Fault_code2
        V2CDD:partFailureCode = wpr:Fault_Code3
        V2CDD:Reserved1           = ''
        V2CDD:Reserved2           = ''
        V2CDD:Reserved3           = ''
        V2CDD:Reserved4           = ''
        V2CDD:Reserved5           = ''
        V2CDD:Reserved6           = ''
        ADD(V2OutFile)
        tmp:TotalLines += 1
    END
    Access:WARPARTS.RestoreFile(Save_wpr_ID)

! End Change 2894 BE(09/07/03)


    
    


V2Header       ROUTINE   ! Start Change 2894 BE(09/07/03)
    CLEAR(v2ouf:Record)
    V2H1:RecordIdentifier = 'HDR'
    V2H1:FileType         = 'CLMCRT'
    ! Start Change 3167 BE(02/09/03)
    !V2H1:DateGenerated    = Format(Today(),@d06) & Format(Clock(),@t01)
    V2H1:DateGenerated    = Format(Today(),@d06) & ' ' & Format(Clock(),@t01)
    ! Start Change 3167 BE(02/09/03)
    ! Start Change 3441 BE(21/10/03)
    V2H1:MASCCode         = man:EDI_Account_Number
    ! End Change 3441 BE(21/10/03)
    V2H1:SiteCode         = man:Account_number
    V2H1:Reserved1         = ''
    ADD(V2OutFile)

    CLEAR(v2ouf:Record)
    V2H2:RecordIdentifier = 'HDR2'
    V2H2:DateSent         = ''
    V2H2:PackageVersion   = ''
    V2H2:DataVersion      = ''
    V2H2:MASCFilename     = ''
    ADD(V2OutFile)

    CLEAR(v2ouf:Record)
    V2H3:RecordIdentifier = 'HDR3'
    V2H3:Reserved1        = ''
    V2H3:Reserved2        = ''
    V2H3:Reserved3        = ''
    ADD(V2OutFile)

! End Change 2894 BE(09/07/03)
V2Trailer      ROUTINE         ! Start Change 2894 BE(09/07/03)
    CLEAR(v2ouf:Record)
    V2TR:RecordIdentifier = 'TRA'
    V2TR:NumberOfClaims   = tmp:TotalClaims
    V2TR:NumberOfCompenentsLines = tmp:TotalLines
    V2TR:Reserved1 = ''
    ADD(V2OutFile)

! End Change 2894 BE(09/07/03)
CheckAwaitingParts ROUTINE ! Start Change 2894 BE(09/07/03)

    tmp:PartNo = ''
    save_wpr_id = access:WARPARTS.savefile()
    access:WARPARTS.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    SET(wpr:part_number_key,wpr:part_number_key)
    LOOP
        IF ((access:warparts.next() <> Level:benign) OR (wpr:ref_number  <> job:ref_number)) THEN
            BREAK
        END
        IF ((wpr:pending_ref_number <> '' and wpr:Order_Number = '') OR |
            (wpr:order_number <> '' AND wpr:date_received = '')) THEN
            tmp:PartNo = wpr:Part_Number
            BREAK
        END
    END
    access:WARPARTS.restorefile(save_wpr_id)

! End Change 2894 BE(09/07/03)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Motorola_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_Ali_id',save_job_Ali_id,'Motorola_EDI',1)
    SolaceViewVars('save_job_id',save_job_id,'Motorola_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Motorola_EDI',1)
    SolaceViewVars('save_mod_id',save_mod_id,'Motorola_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Motorola_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Motorola_EDI',1)
    SolaceViewVars('sav:Path',sav:Path,'Motorola_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Motorola_EDI',1)
    SolaceViewVars('tmp:totalClaims',tmp:totalClaims,'Motorola_EDI',1)
    SolaceViewVars('tmp:TotalLines',tmp:TotalLines,'Motorola_EDI',1)
    SolaceViewVars('pos',pos,'Motorola_EDI',1)
    SolaceViewVars('EDIVersion',EDIVersion,'Motorola_EDI',1)
    SolaceViewVars('TempDate',TempDate,'Motorola_EDI',1)
    SolaceViewVars('tmp:PartNo',tmp:PartNo,'Motorola_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
