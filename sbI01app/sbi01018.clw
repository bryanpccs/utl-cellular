

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01018.INC'),ONCE        !Local module procedure declarations
                     END


AlcatelExport        PROCEDURE  (f_batch_number)      ! Declare Procedure
save_job_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
savepath             STRING(255)
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! Moving Bar Window
RejectRecord         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO

Progress:Thermometer BYTE
ProgressWindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER,FONT('Arial',8,,)
     END

x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(2000)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
serv_code     STRING(4)
month         STRING(3)
repair_date   STRING(9)
serial_no     STRING(17)
total_labour  STRING(14)
total_parts   STRING(14)
currency      STRING(11)
user          STRING(20)
model         String(30)
repairtype    String(30)
FaultCode     String(30)
AccountNo     String(30)

           .

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL
parts1        REAL
labour        REAL
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'AlcatelExport')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:WARPARTS.Open
   Relate:EXCHANGE.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:STATUS.Open
   Relate:AUDIT.Open
   Relate:STAHEAD.Open
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:JOBSTAGE.Open
   Relate:JOBTHIRD.Open
    savepath = path()
    filename = 'ALC' & FOrmat(f_batch_number,@n05) & '.DAT'
    if not filedialog ('Choose File',filename,'DAT Files|*.DAT|All Files|*.*', |
                file:keepdir + file:noerror + file:longname)
        !failed
        setpath(savepath)
    else!if not filedialog
        !found

        YIELD()
        RecordsPerCycle = 25
        RecordsProcessed = 0
        PercentProgress = 0
        OPEN(ProgressWindow)
        Progress:Thermometer = 0
        ?Progress:PctText{Prop:Text} = '0% Completed'
        ProgressWindow{Prop:Text} = 'Alcatel EDI Export'
        ?Progress:UserString{Prop:Text}=''
        setcursor(cursor:wait)
        DISPLAY()
        OPEN(Out_File)                           ! Open the output file
        IF ERRORCODE()                           ! If error
            CREATE(Out_File)                       ! create a new file
            If Errorcode()
                Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                Do Exit_Proc
            End!If Errorcode() 
            OPEN(out_file)
            EMPTY(Out_file)

        ELSE
            OPEN(out_file)
            EMPTY(Out_file)
        End
        count = 1

!Count records
            count_records# = 0
            setcursor(cursor:wait)
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:edi_key)
            job:manufacturer    = 'ALCATEL'
            job:edi             = 'YES'
            job:edi_batch_number = f_batch_number
            Set(job:edi_key,job:edi_key)
            Loop
                If access:jobs.next()
                    Break
                End!If access:jobs.next()
                If job:manufacturer <> 'ALCATEL'  |
                    or job:edi      <> 'YES'         |
                    or job:edi_batch_number <> f_batch_number   |
                    then Break.
                count_records#  += 1
            End!Loop
            access:jobs.restorefile(save_job_id)
            setcursor()
        
            Recordstoprocess = count_records#

            setcursor(cursor:wait)
            save_job_id = access:jobs.savefile()
            access:jobs.clearkey(job:edi_key)
            job:manufacturer     = 'ALCATEL'
            job:edi              = 'YES'
            job:edi_batch_number = f_batch_number
            set(job:edi_key,job:edi_key)
            loop
                !omemo(jobs)
                if access:jobs.next()
                   break
                end !if
                Do GetNextRecord2
                if job:manufacturer     <> 'ALCATEL'      |
                or job:edi              <> 'YES'      |
                or job:edi_batch_number <> f_batch_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                Do Export
            end !loop
            access:jobs.restorefile(save_job_id)
            setcursor()
            Case MessageEx('Export Complete.','ServiceBase 2000',|
                           'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Close(out_file)
        Do exit_proc

        setpath(savepath)
    end!if not filedialog
exit_proc    ROUTINE
  SETCURSOR()
   Relate:JOBS.Close
   Relate:WARPARTS.Close
   Relate:EXCHANGE.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:STATUS.Close
   Relate:AUDIT.Close
   Relate:STAHEAD.Close
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:JOBSTAGE.Close
   Relate:JOBTHIRD.Close
  RETURN

GetNextRecord2      Routine
  RecordsProcessed += 1
  RecordsThisCycle += 1
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
    IF PercentProgress <> Progress:Thermometer THEN
      Progress:Thermometer = PercentProgress
      ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & '% Completed'
      DISPLAY()
    END
  END
EndPrintRun         Routine
    Progress:Thermometer = 100
    ?Progress:PctText{Prop:Text} = '100% Completed'
    Close(ProgressWindow)
    DISPLAY()
Export        Routine
      l1:serv_code = SUB(MAN:EDI_Account_Number,1,4)&'!'
      l1:month = FORMAT(MONTH(today()),@n02)&'!'
      l1:repair_date = FORMAT(YEAR(JOB:Date_Completed),@n04)&FORMAT(MONTH(JOB:Date_Completed),@n02)&FORMAT(DAY(JOB:Date_Completed),@n02)&'!'

        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 0                
                End !If jot:RefNumber <> job:Ref_Number            
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            l1:serial_no = SUB(JOB:ESN,1,15)&'!'
        Else !IMEIError# = 1
            l1:serial_no = SUB(jot:OriginalIMEI,1,15)&'!'
        End !IMEIError# = 1
      
      l1:total_labour = SUB(FORMAT(job:labour_cost_warranty,@n013`3),1,12)&'!'
      l1:total_parts      = SUB(FORMAT(job:parts_cost_warranty,@n013`3),1,12)&'!'
      l1:currency = 'STERLING!'
      l1:user = Format(SUB(JOB:Ref_Number,1,10),@s10) &'!'
      l1:model  = Format(job:model_number,@s29) & '!'
      l1:repairtype = Format(job:repair_Type_warranty,@s29) & '!'
      l1:faultcode  = Format(job:fault_code1,@s29) & '!'
      l1:AccountNo  = Format(job:account_number,@s29)
      ADD(out_file)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AlcatelExport',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'AlcatelExport',1)
    SolaceViewVars('savepath',savepath,'AlcatelExport',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
