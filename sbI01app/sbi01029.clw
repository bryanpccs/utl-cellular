

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01029.INC'),ONCE        !Local module procedure declarations
                     END


Sony_Ericcson_EDI    PROCEDURE  (f_batch_number,func:Manufacturer) ! Declare Procedure
save_job_id          USHORT,AUTO
save_aud_id          USHORT
pos                  STRING(255)
count                REAL
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
x            SHORT

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(255)
Line2           STRING(229)
          . . .

Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
job_no          STRING(10)
scid            STRING(11)
cust_type       STRING(2)
Country         STRING(4)
cust_title      STRING(5)
Cust_fname      STRING(16)
Cust_Sname      STRING(21)
Comp_name       STRING(21)
Add1            STRING(31)
Add2            STRING(31)
Add3            STRING(31)
P_Code          STRING(11)
Tel             STRING(31)
Date_booked     STRING(9)
Time_Started    STRING(6)
date_finish     STRING(9)
Time_Finish     STRING(6)
War_proof       STRING(2)
Prod_Type       STRING(2)
Model_ID        STRING(16)
Prod_Code       STRING(23)
Prod_Out        STRING(23)
IMEI_IN         STRING(19)
IMEI_OUT        STRING(19)
Msn_In          STRING(14)
Msn_Out         STRING(14)
Hard_In         STRING(8)
Soft_In         STRING(23)
Man_Year_In     STRING(3)
Man_Week_In     STRING(3)
Man_Year_Out    STRING(3)
Man_Week_Out    STRING(3)
Fault_Code      STRING(5)
Action_Code     STRING(5)
Part_Qty        STRING(4)
Part_No         STRING(23)
Symptom_code    STRING(5)
Remark          STRING(5)
           .
table     queue,pre(tbl)
part_no   STRING(25)
price     REAL
done      STRING(1)
        .
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Sony_Ericcson_EDI')      !Add Procedure to Log
  end


   Relate:DEFEDI2.Open
   Relate:DEFAULTS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:WARPARTS.Open
   Relate:EXCHANGE.Open
   Relate:CHARTYPE.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
   Relate:TRACHRGE.Open
   Relate:SUBCHRGE.Open
   Relate:STDCHRGE.Open
   Relate:DISCOUNT.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:AUDIT.Open
   Relate:JOBS.Open
   Relate:JOBSTAGE.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:STOCK.Open
   Relate:JOBSE.Open
!**Variable
    Set(Defaults)
    Access:DEFAULTS.Next()
    tmp:ManufacturerName = func:Manufacturer
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Benign
            set(defedi2)
            If access:defedi2.next()
                ed2:country_code = 'N/A'
                access:defedi2.insert()
                Case MessageEx('The EDI Defaults have not been setup for this Manufacturer.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                ManError# = 1
            End!If access:defedi.next()
        Else
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
!**Variable
            IF f_batch_number = 0
                filename = CLIP(CLIP(MAN:EDI_Path)&'\FI1'&CLIP(Format(MAN:Batch_number,@n05))&'.PRT')
                filename2 = CLIP(CLIP(MAN:EDI_Path)&'\FI2'&CLIP(Format(MAN:Batch_number,@n05))&'.PRT')
        
            ELSE
                filename = CLIP(CLIP(MAN:EDI_Path)&'\FI1'&CLIP(Format(f_Batch_number,@n05))&'.PRT')
                filename2 = CLIP(CLIP(MAN:EDI_Path)&'\FI2'&CLIP(Format(f_Batch_number,@n05))&'.PRT')
            END
            OPEN(Out_File)                           ! Open the output file
            IF ERRORCODE()                           ! If error
                CREATE(Out_File)                       ! create a new file
                If Errorcode()
                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End!If Errorcode() 
                OPEN(out_file)        ! If still error then stop
            ELSE
                OPEN(out_file)
                EMPTY(Out_file)
            END
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate

                        !Get the jobse number
                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                        jobe:RefNumber  = job:Ref_Number
                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Found

                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign

                        DO Export2

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        pos = Position(job:EDI_Key)
                        !Found
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        !Get the jobse number
                        Access:JOBSE.Clearkey(jobe:RefNumberKey)
                        jobe:RefNumber  = job:Ref_Number
                        If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Found

                        Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                            !Error
                        End !If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign


                        DO Export2

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    ! Start Change 2806 BE(22/07/03)
                    !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !  '<13,10>'&|
                    !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                    !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !    Of 1 ! &OK Button
                    !End!Case MessageEx
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)

                End!If f_batch_number = 0
                CLOSE(out_file)
            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:DEFEDI2.Close
   Relate:DEFAULTS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:WARPARTS.Close
   Relate:EXCHANGE.Close
   Relate:CHARTYPE.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
   Relate:TRACHRGE.Close
   Relate:SUBCHRGE.Close
   Relate:STDCHRGE.Close
   Relate:DISCOUNT.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:AUDIT.Close
   Relate:JOBS.Close
   Relate:JOBSTAGE.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:STOCK.Close
   Relate:JOBSE.Close
Export         Routine
    CLEAR(ouf:RECORD)
    CLEAR(mt_total)
    setcursor(cursor:wait)
    count_spares# = 0
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:Ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:Ref_number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)

        count_spares# += 1
        l1:job_no   = Format(job:ref_number,@s10)
        l1:scid     = '<9>'& Format(Clip(man:edi_account_number),@s10)
        l1:cust_type    = '<9>'& 'D'
        l1:country      = '<9>'& Format(ed2:country_code,@s3)
        l1:Cust_title   = '<9>'& Format(job:title,@s4)
        l1:cust_fname   = '<9>'& Format(job:initial,@s15)
        l1:cust_sname   = '<9>'& Format(job:surname,@s20)
        l1:comp_name    = '<9>'& Format(job:company_name,@s20)
        l1:add1         = '<9>'& Format(job:address_line1,@s30)
        l1:add2         = '<9>'& Format(job:address_line2,@s30)
        l1:add3         = '<9>'& Format(job:address_line3,@s30)
        l1:p_code       = '<9>'& Format(job:postcode,@s10)
        l1:tel          = '<9>'& Format(job:telephone_number,@s30)
        l1:date_booked  = '<9>'& Format(Day(job:Date_Booked),@n02) & '/' & Format(Month(job:Date_Booked),@n02) & '/' & |
                                Sub(Format(Year(job:Date_Booked),@n04),3,2)
        l1:time_started = '<9>'& Format(Format(job:time_booked,@t1),@s5)
        l1:date_finish  = '<9>'& Format(Day(job:Date_Completed),@n02) & '/' & Format(Month(job:Date_Completed),@n02) & '/' & |
                                Sub(Format(Year(job:Date_Completed),@n04),3,2)
        l1:time_finish  = '<9>'& Format(Format(job:time_completed,@t1),@s5)
        l1:war_proof    = '<9>'& Format(job:fault_code3,@s1)
        l1:prod_type    = '<9>'& Format(job:fault_code4,@s1)
        l1:model_id     = '<9>'& Format(job:model_number,@s15)
        l1:prod_code    = '<9>'& Format(job:fault_code1,@s22)
        l1:prod_out     = '<9>'& Format(job:fault_code2,@s22)

!        l1:msn_in   = '<9>'& Format(job:msn,@s13)

        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 0                
                End !If jot:RefNumber <> job:Ref_Number            
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            l1:imei_in  = '<9>'& Format(job:esn,@s18)
            l1:msn_in   = '<9>'& Format(job:msn,@s13)
        Else !IMEIError# = 1
            l1:imei_in  = '<9>'& Format(jot:OriginalIMEI,@s18)
            l1:msn_in   = '<9>'& Format(jot:OriginalMSN,@s13)
        End !IMEIError# = 1
        l1:imei_out  = '<9>'& Format(job:esn,@s18)
        !Neil Added This!
        l1:msn_out   = '<9>'& FORMAT(job:msn,@s13)
        !Why did you make it blank Neil. Change it to use job:msn
        !
        IF job:exchange_unit_number <> ''
            access:exchange.clearkey(xch:ref_number_key)
            xch:ref_number = job:exchange_unit_number
            if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                l1:imei_out = '<9>'& Format(xch:esn,@s18)
                l1:msn_out  = '<9>'& Format(xch:msn,@s13)
            End!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
        End!IF job:exchange_unit_number <> ''

        l1:hard_in  = '<9>'& Format(job:fault_code5,@s7)
        l1:soft_in  = '<9>'& Format(job:fault_code6,@s22)
        l1:man_year_in  = '<9>'& Format(job:fault_code7,@s2)
        l1:man_week_in  = '<9>'& Format(job:fault_code8,@s2)
        l1:man_year_out = '<9>'& Format(job:fault_code9,@s2)
        l1:man_week_out  = '<9>'& Format(job:fault_code10,@s2)
        l1:fault_code   = '<9>'& Format(wpr:fault_code2,@s4)
        l1:action_code  = '<9>'& Format(wpr:fault_code3,@s4)
        l1:part_qty     = '<9>'& Format(Format(wpr:quantity,@n03),@s3)
        l1:part_no      = '<9>'& Format(wpr:part_number,@s22)
        If wpr:Fault_Code4 = ''
            l1:symptom_code = '<9>'& Format(job:fault_code11,@s4)
        Else!If wpr:Fault_Code4 = ''
            l1:symptom_code = '<9>'& Format(wpr:fault_code4,@s4)
        End!If wpr:Fault_Code4 = ''
        
        l1:remark       = '<9>'& 'N/A' 
        ADD(out_file)
        If error()
            stop(error())
        End!If error()
        CLEAR(ouf:RECORD)
    end !loop
    access:warparts.restorefile(save_wpr_id)
    setcursor()

    If count_spares# = 0
        l1:job_no   = Format(job:ref_number,@s10)
        l1:scid     = '<9>'& Format(Clip(man:edi_account_number),@s10)
        l1:cust_type    = '<9>'& 'D'
        l1:country      = '<9>'& Format(ed2:country_code,@s3)
        l1:Cust_title   = '<9>'& Format(job:title,@s4)
        l1:cust_fname   = '<9>'& Format(job:initial,@s15)
        l1:cust_sname   = '<9>'& Format(job:surname,@s20)
        l1:comp_name    = '<9>'& Format(job:company_name,@s20)
        l1:add1         = '<9>'& Format(job:address_line1,@s30)
        l1:add2         = '<9>'& Format(job:address_line2,@s30)
        l1:add3         = '<9>'& Format(job:address_line3,@s30)
        l1:p_code       = '<9>'& Format(job:postcode,@s10)
        l1:tel          = '<9>'& Format(job:telephone_number,@s30)
        l1:date_booked  = '<9>'& Format(Day(job:Date_Booked),@n02) & '/' & Format(Month(job:Date_Booked),@n02) & '/' & |
                                Sub(Format(Year(job:Date_Booked),@n04),3,2)
        l1:time_started = '<9>'& Format(Format(job:time_booked,@t1),@s5)
        l1:date_finish  = '<9>'& Format(Day(job:Date_Completed),@n02) & '/' & Format(Month(job:Date_Completed),@n02) & '/' & |
                                Sub(Format(Year(job:Date_Completed),@n04),3,2)
        l1:time_finish  = '<9>'& Format(Format(job:time_completed,@t1),@s5)
        l1:war_proof    = '<9>'& Format(job:fault_code3,@s1)
        l1:prod_type    = '<9>'& Format(job:fault_code4,@s1)
        l1:model_id     = '<9>'& Format(job:model_number,@s15)
        l1:prod_code    = '<9>'& Format(job:fault_code1,@s22)
        l1:prod_out     = '<9>'& Format(job:fault_code2,@s22)
        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 0                
                End !If jot:RefNumber <> job:Ref_Number            
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            l1:imei_in  = '<9>'& Format(job:esn,@s18)
            l1:msn_in   = '<9>'& Format(job:msn,@s13)
        Else !IMEIError# = 1
            l1:imei_in  = '<9>'& Format(jot:OriginalIMEI,@s18)
            l1:msn_in   = '<9>'& Format(jot:OriginalMSN,@s13)
        End !IMEIError# = 1
        l1:imei_out  = '<9>'& Format(job:esn,@s18)
        !Neil Added This!
        l1:msn_out   = '<9>'& FORMAT(job:msn,@s13)
        !Why did you make it blank Neil. Change it to use job:msn
        !
        IF job:exchange_unit_number <> ''
            access:exchange.clearkey(xch:ref_number_key)
            xch:ref_number = job:exchange_unit_number
            if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                l1:imei_out = '<9>'& Format(xch:esn,@s18)
                l1:msn_out  = '<9>'& Format(xch:msn,@s13)
            End!if access:exchange.fetch(xch:ref_number_key) = Level:Benign
        End!IF job:exchange_unit_number <> ''
        l1:hard_in  = '<9>'& Format(job:fault_code5,@s7)
        l1:soft_in  = '<9>'& Format(job:fault_code6,@s22)
        l1:man_year_in  = '<9>'& Format(job:fault_code7,@s2)
        l1:man_week_in  = '<9>'& Format(job:fault_code8,@s2)
        l1:man_year_out = '<9>'& Format(job:fault_code9,@s2)
        l1:man_week_out  = '<9>'& Format(job:fault_code10,@s2)
        l1:fault_code   = '<9>'& Format('',@s4)
        l1:action_code  = '<9>'& Format('',@s4)
        l1:part_qty     = '<9>'& Format('000',@s3)
        l1:part_no      = '<9>'& Format('',@s22)
        If wpr:Fault_Code4 = ''
            l1:symptom_code = '<9>'& Format(job:fault_code11,@s4)
        Else!If wpr:Fault_Code4 = ''
            l1:symptom_code = '<9>'& Format(wpr:fault_code4,@s4)
        End!If wpr:Fault_Code4 = ''
        l1:remark       = '<9>N/A'
        ADD(out_file)
        CLEAR(ouf:RECORD)
    End!If count_spares# = 0

Export2         Routine             !  Change 2410 BE(24/03/03)
                                    ! NEW FORMAT  imported from South African Version of ServiceBase (Version ??.??)
    CLEAR(ouf:RECORD)
    CLEAR(mt_total)
    setcursor(cursor:wait)
    count_spares# = 0
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:Ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:Ref_number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
        If wpr:Part_Number = 'EXCH'
            Cycle
        End !If wpr:Part_Number = 'EXCH'
        count_spares# += 1
        !Work Order Number
        ouf:Line1   = Format(job:Ref_Number,@s20)
        !Service Location ID
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(man:EDI_Account_Number,@s8)
        !POP Date
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:DOP,@d12)
        !POP Supplier
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code5,@s50)

        !Intec want to use the In Workshop Date/Time instead of booking date/time (BB:2410)
        If Instring('INTEC',def:User_Name,1,1)
            !Date Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(jobe:InWorkshopDate,@d12)
            !Time Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(jobe:InWorkshopTime,@t1)
        Else !If Substring('INTEC',def:User_Name,1,1)
            !Date Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Date_Booked,@d12)
            !Time Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Time_Booked,@t1)
        End !If Substring('INTEC',def:User_Name,1,1)
        !Repair Completion Date
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Date_Completed,@d12)
        !Repair Completion Time
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Time_Completed,@t1)
        !Phone Returned Date
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Date_Despatched,@d12)
        !Phone Returned Time
        Found# = 0
        Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.ClearKey(aud:TypeRefKey)
        aud:Ref_Number = job:Ref_Number
        aud:Type       = 'JOB'
        aud:Date       = Today()
        Set(aud:TypeRefKey,aud:TypeRefKey)
        Loop
            If Access:AUDIT.NEXT()
               Break
            End !If
            If aud:Ref_Number <> job:Ref_Number       |
            Or aud:Type       <> 'JOB'       |
                Then Break.  ! End If
            If Instring('JOB DESPATCHED',aud:Action,1,1)
                Found# = 1
                ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(aud:Time,@t1)
            End !If Instring('JOB DESPATCHED',aud:Action,1,1)
        End !Loop
        Access:AUDIT.RestoreFile(Save_aud_ID)
        If Found# = 0
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
        End !If Found# = 0
        !Product Type
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code4,@s1)
        !Model Number
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Model_Number,@s20)
        !SSN In
        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 0                
                End !If jot:RefNumber <> job:Ref_Number            
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:ESN,@s30)
        Else !IMEIError# = 1
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(jot:OriginalIMEI,@s30)
        End !IMEIError# = 1
        !SSN Out
        If job:Exchange_Unit_Number <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(xch:ESN,@s30)
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        Else !If job:Exchange_Unit_Number <> 0
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
        End !If job:Exchange_Unit_Number <> 0
        !Software Revision State
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code6,@s10)
        !Fault Codes ID
        !Fault Codes ID
        ! Start Change 2410  BE(10/04/2003)
        !ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_code3,@s2)
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(wpr:Fault_code2,@s2)
        ! End Change 2410  BE(10/04/2003)
        !Action Code ID
        ! Start Change 2410  BE(10/04/2003)
        !ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(wpr:Fault_Code2,@s2)
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(wpr:Fault_Code3,@s2)
        ! End Change 2410  BE(10/04/2003)
        !Material Quantity
        ! Start Change 2410 BE(24/03/03)
        ! Changed because @s3 by default includes decimal point for integer value
        ! and reduces effective field size to just 2 characters (e.g. 99. instead of 999)
        !ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(wpr:Quantity,@s3)
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(wpr:Quantity,@n3)
        ! Start Change 2410 BE(24/03/03)
        !Material Number
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(wpr:Part_Number,@s18)
        !Reused Indicator
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & '0'
        !Symptom Code
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code11,@s30)
        !Remarks
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & 'N/A'

        ADD(out_file)
        If error()
            stop(error())
        End!If error()
        CLEAR(ouf:RECORD)
    end !loop
    access:warparts.restorefile(save_wpr_id)
    setcursor()

    If count_spares# = 0
        !Work Order Number
        ouf:Line1   = Format(job:Ref_Number,@s20)
        !Service Location ID
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(man:EDI_Account_Number,@s8)
        !POP Date
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:DOP,@d12)
        !POP Supplier
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code5,@s50)
        !Intec want to use the In Workshop Date/Time instead of booking date/time (BB:2410)
        If Instring('INTEC',def:User_Name,1,1)
            !Date Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(jobe:InWorkshopDate,@d12)
            !Time Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(jobe:InWorkshopTime,@t1)
        Else !If Substring('INTEC',def:User_Name,1,1)
            !Date Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Date_Booked,@d12)
            !Time Received
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Time_Booked,@t1)
        End !If Substring('INTEC',def:User_Name,1,1)
        !Repair Completion Date
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Date_Completed,@d12)
        !Repair Completion Time
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Time_Completed,@t1)
        !Phone Returned Date
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Date_Despatched,@d12)
        !Phone Returned Time
        Found# = 0
        Save_aud_ID = Access:AUDIT.SaveFile()
        Access:AUDIT.ClearKey(aud:TypeRefKey)
        aud:Ref_Number = job:Ref_Number
        aud:Type       = 'JOB'
        aud:Date       = Today()
        Set(aud:TypeRefKey,aud:TypeRefKey)
        Loop
            If Access:AUDIT.NEXT()
               Break
            End !If
            If aud:Ref_Number <> job:Ref_Number       |
            Or aud:Type       <> 'JOB'       |
                Then Break.  ! End If
            If Instring('JOB DESPATCHED',aud:Action,1,1)
                Found# = 1
                ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(aud:Time,@t1)
            End !If Instring('JOB DESPATCHED',aud:Action,1,1)
        End !Loop
        Access:AUDIT.RestoreFile(Save_aud_ID)
        If Found# = 0
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
        End !If Found# = 0
        !Product Type
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code4,@s1)
        !Model Number
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Model_Number,@s20)
        !SSN In
        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 0                
                End !If jot:RefNumber <> job:Ref_Number            
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:ESN,@s30)
        Else !IMEIError# = 1
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(jot:OriginalIMEI,@s30)
        End !IMEIError# = 1
        !SSN Out
        If job:Exchange_Unit_Number <> 0
            Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
            xch:Ref_Number  = job:Exchange_Unit_Number
            If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Found
                ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(xch:ESN,@s30)
            Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                !Error
                ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
            End !If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
        Else !If job:Exchange_Unit_Number <> 0
            ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
        End !If job:Exchange_Unit_Number <> 0
        !Software Revision State
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code6,@s10)
        !Fault Codes ID
        ! Start Change 2410  BE(10/04/2003)
        !ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_code3,@s2)
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(wpr:Fault_code2,@s2)
        ! End Change 2410  BE(10/04/2003)
        !Action Code ID
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
        !Material Quantity
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
        !Material Number
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & ''
        !Reused Indicator
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & '0'
        !Symptom Code
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & Format(job:Fault_Code11,@s30)
        !Remarks
        ouf:Line1   = Clip(ouf:Line1) & '<9>' & 'N/A'

        ADD(out_file)
        CLEAR(ouf:RECORD)
    End!If count_spares# = 0
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Sony_Ericcson_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'Sony_Ericcson_EDI',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Sony_Ericcson_EDI',1)
    SolaceViewVars('pos',pos,'Sony_Ericcson_EDI',1)
    SolaceViewVars('count',count,'Sony_Ericcson_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Sony_Ericcson_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Sony_Ericcson_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Sony_Ericcson_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Sony_Ericcson_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Sony_Ericcson_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
