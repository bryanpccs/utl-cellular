

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01012.INC'),ONCE        !Local module procedure declarations
                     END


Samsung_EDI          PROCEDURE  (f_batch_number)      ! Declare Procedure
pos                  STRING(255)
save_job_ali_id      USHORT,AUTO
save_job_id          USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
tmp:ClaimNumber      LONG
save_wpr_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group       Group
Line1           STRING(2000)
          . ..

Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
type         STRING(2)        !1
claim_no     STRING(8)
acc_no       STRING(10)
reg_no       STRING(14)       !4 5
cust_no      STRING(20)       !6
auth_no      STRING(10)
rep_type     STRING(1)
model        STRING(18)
serial_no    STRING(14)       !10
purchased    STRING(1)
purch_date   STRING(8)
book_date    STRING(8)
compl_date   STRING(8)
lab_time     STRING(6)
internal1    String(16)
oth_amount   STRING(8)
internal2    String(8)
oth_reason   STRING(10)
cond_code1   String(1)        !20
symp_code1   STRING(3)
cond_code2   String(1)
symp_code2   STRING(3)
defect_1     STRING(60)
defect_2     STRING(60)
defect_3     STRING(60)
defect_4     STRING(60)
repair_1     STRING(60)
repair_2     STRING(60)
repair_3     STRING(60)       !30
repair_4     STRING(60)
internal3    String(40)
cust_name    STRING(30)
cust_add1    STRING(30)
cust_add2    STRING(30)
cust_town    STRING(20)
cust_cnty    STRING(30)
cust_pcod    STRING(10)
cust_tel     STRING(15)       !39
!deal_name    STRING(30)
!deal_add1    STRING(30)
!deal_add2    STRING(30)
!deal_town    STRING(20)
!deal_cnty    STRING(30)
!deal_pcod    STRING(10)
!deal_tel     STRING(15)
          .
!out_fil2    DOS,PRE(ou2),NAME(filename)
Out_Part   GROUP,OVER(Ouf:Out_Group),PRE(L2)
type1        STRING(2)
claim_no1    STRING(8)
part_no      STRING(18)
qty          STRING(6)
inv_no       STRING(10)
circuit_r    STRING(6)
sect_code    STRING(3)
defect       STRING(1)
repair       STRING(1)
part_fill1   STRING(12)
         .

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL
parts_temp         REAL
labour_temp        REAL
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Samsung_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:JOBSTAGE.Open
   Relate:SUBTRACC.Open
   Relate:CHARTYPE.Open
   Relate:DISCOUNT.Open
   Relate:VATCODE.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBNOTES.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:STOCK.Open
!**Variable
    tmp:ManufacturerName = 'SAMSUNG'
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
!**Variable
            IF f_batch_number = 0
                filename = CLIP(CLIP(MAN:EDI_Path)&'\SAM'&CLIP(Format(MAN:Batch_number,@n05))&'.' & Clip(Sub(man:edi_account_number,1,2) & '_'))
            ELSE
                filename = CLIP(CLIP(MAN:EDI_Path)&'\SAM'&CLIP(Format(f_Batch_number,@n05))&'.' & Clip(Sub(man:edi_account_number,1,2) & '_'))
            END
            OPEN(Out_File)                           ! Open the output file
            IF ERRORCODE()                           ! If error
                CREATE(Out_File)                       ! create a new file
                If Errorcode()
                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End!If Errorcode() 
                OPEN(out_file)        ! If still error then stop
            ELSE
                OPEN(out_file)
                EMPTY(Out_file)
            END
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                tmp:ClaimNumber = man:samsungcount

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'

                        tmp:ClaimNumber += 1
                        Do Export

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        !Found
                        pos = Position(job:EDI_Key)
                        job:Fault_Code12    = tmp:ClaimNumber
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)
                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:SamsungCount = tmp:ClaimNumber
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        Do Export
                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    ! Start Change 2806 BE(22/07/03)
                    !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !  '<13,10>'&|
                    !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                    !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !    Of 1 ! &OK Button
                    !End!Case MessageEx
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)

                End!If f_batch_number = 0
                CLOSE(out_file)
            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:JOBSTAGE.Close
   Relate:SUBTRACC.Close
   Relate:CHARTYPE.Close
   Relate:DISCOUNT.Close
   Relate:VATCODE.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBNOTES.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:STOCK.Close
Export      Routine
    Clear(Ouf:Record)
    Clear(Mt_Total)
    l1:type    = 'A1'
    If f_Batch_Number = 0
        l1:claim_no    =  Format(Format(man:edi_account_number,@s2) & Format(tmp:ClaimNumber,@n06),@s8) !Claim Number
    Else !If f_Batch_Number = 0
        l1:claim_no    =  Format(Format(man:edi_account_number,@s2) & Format(job:fault_code12,@n06),@s8) !Claim Number
    End !If f_Batch_Number = 0
    
    l1:acc_no    =  Format(man:account_number,@s8)                           !Account Number
    l1:reg_no    = Format('',@s14)                                         !Reg_No
    l1:cust_no    =  Format(Format(job:ref_number,@n08),@s20)                 !Cust_no
    l1:auth_no    =  Format(job:authority_number,@s10)                        !Auth_no
    If job:workshop = 'YES'
        l1:rep_type    =  'C'                                                  !Rep_Type
    Else!If job:workshop = 'YES'
        l1:rep_type    =  'H'                                                  !Rep_Type
    End!If job:workshop = 'YES'
    l1:model    =  Format(job:model_number,@s18)                            !Model

    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        l1:serial_no    =  Format(job:msn,@s18)                             !Serial_No
    Else !IMEIError# = 1
        l1:serial_no    =  Format(jot:OriginalMSN,@s18)                             !Serial_No
    End !IMEIError# = 1

    If job:dop <> ''
        l1:purchased    =  'Y'                                                  !Purchased
        l1:purch_date    =  Format(Format(Year(job:dop),@n04) & Format(Month(job:dop),@n02) & Format(Day(job:dop),@n02),@s8)
    Else!If job:dop <> ''
        l1:purchased    =  'N'                                                  !Purchased
        l1:purch_date    =  Format('',@s8)
    End!If job:dop <> ''

    access:jobstage.clearkey(jst:job_stage_key)
    jst:ref_number = job:ref_number
    jst:job_stage  = '300 IN WORKSHOP'
    if access:jobstage.tryfetch(jst:job_stage_key) = Level:Benign
        l1:book_date    =  Format(Format(Year(JST:Date),@n04) & Format(Month(JST:Date),@n02) & Format(Day(JST:Date),@n02),@s8)
    Else!if access:jobstage.tryfetch(jst:job_stage_key) = Level:Benign
        l1:book_date    =  Format(Format(Year(job:date_booked),@n04) & Format(Month(job:date_booked),@n02) & Format(Day(job:date_booked),@n02),@s8)
    End!if access:jobstage.tryfetch(jst:job_stage_key) = Level:Benign

    ! CHANGE 2263 BE 25/02/03
    !l1:compl_date    =  Format(Format(Year(job:date_booked),@n04) & Format(Month(job:date_booked),@n02) & Format(Day(job:date_booked),@n02),@s8)
    l1:compl_date = l1:book_date  ! Completed Date should always equal Booked Date (Samsung Requirement)
    ! CHANGE 2263 BE 25/02/03

    If job:time_completed > job:Time_booked
        the_time$ = Format((count# * 24) + ((job:time_completed - job:time_booked)/360000),@n09.2)
    End!If job:time_completed > job:Time_booked
    If job:time_completed < job:time_booked
        time$    = def:end_work_hours - job:time_booked + job:time_completed - def:start_work_hours
        the_time$ = Format((count# * 24) + (time$/360000),@n09.2)
    End!If job:time_compelted < job:time_booked
    If job:time_completed = job:time_booked
        the_time$    = Format((count# * 24),@n09.2)
    End!Format((count# * 24) + ((job:time_completed - job:time_booked)/360000)@n6.2)
    
    temp"     = Format(the_time$,@n09.2)
    temp1"    = ''
    temp2"    = ''
    Loop x# = 1 To 9     
        temp1"    = Sub(Clip(temp"),x#,1)
        If clip(left(temp1")) = '.'
            Cycle
        End!If clip(left(temp1")) = '.'
        temp2"    = Clip(temp2") & Temp1"
    End!Loop x# = 1 To 9

    l1:lab_time    =  Format(temp2",@s6)                                       !Lab_Time
    l1:internal1    = Format('',@s16)

!    temp"    = Format(job:labour_cost_warranty,@n09.2)
!    temp1"    = ''
!    temp2"    = ''
!    Loop x# = 1 To 9     
!        temp1"    = Sub(Clip(temp"),x#,1)
!        If clip(left(temp1")) = '.'
!            Cycle
!        End!If clip(left(temp1")) = '.'
!        temp2"    = Clip(temp2") & Temp1"
!    End!Loop x# = 1 To 9
!
!    l1:lab_amount    =  Format(temp2",@s8)                                       !Lab_Amount
!
!    access:subtracc.clearkey(sub:account_number_key)
!    sub:account_number = job:account_number
!    if access:subtracc.fetch(sub:account_number_key) = level:benign
!        access:tradeacc.clearkey(tra:account_number_key) 
!        tra:account_number = sub:main_account_number
!        if access:tradeacc.fetch(tra:account_number_key) = level:benign
!            if tra:use_sub_accounts = 'YES'
!                access:vatcode.clearkey(vat:vat_code_key)
!                vat:vat_code = sub:labour_vat_code
!                if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
!                    labour_rate$ = vat:vat_rate
!                end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
!            else!if tra:use_sub_accounts = 'YES'
!                access:vatcode.clearkey(vat:vat_code_key)
!                vat:vat_code = tra:labour_vat_code
!                if access:vatcode.tryfetch(vat:vat_code_key) = level:benign
!                    labour_rate$ = vat:vat_rate
!                end!if access:vatcode.clearkey(vat:vat_code_key) = level:benign
!            end!if tra:use_sub_accounts = 'YES'
!        end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
!    end!if access:subtracc.fetch(sub:account_number_key) = level:benign    
!
!    temp"    = Format((job:labour_cost_warranty/100) * labour_rate$,@n09.2)
!    temp1"    = ''
!    temp2"    = ''
!    Loop x# = 1 To 9     
!        temp1"    = Sub(Clip(temp"),x#,1)
!        If clip(left(temp1")) = '.'
!            Cycle
!        End!If clip(left(temp1")) = '.'
!        temp2"    = Clip(temp2") & Temp1"
!    End!Loop x# = 1 To 9
!
!    l1:lab_tax    =  Format(temp2",@s8)                                       !Lab_tax
    l1:oth_amount    =  Format(job:Fault_code6,@s8)                                               !Oth_Amount
    l1:internal2    = Format(Format(Year(job:date_Completed),@n04) & Format(Month(job:date_Completed),@n02) & Format(Day(job:date_Completed),@n02),@s8)
!    l1:oth_tax    =  '00000000'                                               !Oth_Tax
    l1:oth_Reason    =  Format(job:Fault_Code7,@s10) 
    l1:cond_code1    =  Format(job:fault_code1,@s1)
    l1:symp_code1    =  Format(job:fault_code2,@s3)                              !Symp_Code1
    l1:cond_code2    =  Format(job:Fault_Code8,@s1)
    l1:symp_code2    =  Format(job:fault_code5,@s3)                                                   !Symp_Code2
    access:jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:ref_number
    access:jobnotes.tryfetch(jbn:RefNumberKey)

    l1:defect_1    =  Format(Stripcomma(stripreturn(jbn:fault_description)),@s60)  !Defect_1
    l1:defect_2    =  '                                                            '  !Defect_2
    l1:defect_3    =  '                                                            '  !Defect_3
    l1:defect_4    =  '                                                            '  !Defect_4
    l1:repair_1    =  Format(Stripcomma(stripreturn(jbn:invoice_text)),@s60)   !Repair_1
    l1:repair_2    =  '                                                            '  !Repair_2
    l1:repair_3    =  '                                                            '  !Repair_3
    l1:repair_4    =  '                                                            '  !Repair_4

    ! Start Change BE029 BE(20/05/2004)                                                            
    !If job:title = ''
    !    ! Start Change 2287 BE(07/03/03)
    !    !l1:cust_name    = Format(job:company_name,@s3)
    !    l1:cust_name    = Format(job:company_name,@s30)
    !    ! End Change 2287 BE(07/03/03)
    !Else!If job:title = ''
    !    l1:cust_name    =  Format(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),@s30) !Cust_Name
    !End!If job:title = ''
    l1:cust_name = job:title

    IF (l1:cust_name = '') THEN
        l1:cust_name  =  job:initial
    ELSE
        l1:cust_name = CLIP(l1:cust_name) & ' ' & job:initial
    END

    IF (l1:cust_name = '') THEN
        l1:cust_name  =  FORMAT(job:surname, @s30)
    ELSE
        l1:cust_name = FORMAT(CLIP(l1:cust_name) & ' ' & job:surname, @s30)
    END

    IF (l1:cust_name = '') THEN
        l1:cust_name  =  FORMAT(job:company_name, @s30)
    END
    ! End Change BE029 BE(20/05/2004)

    l1:cust_add1    =  Format(job:address_line1,@s30)                           !Cust_Add1
    l1:cust_add2    =  Format(job:address_line2,@s30)                           !Cust_Add2
    If job:Address_Line3 = ''
        l1:cust_town    =  Format(job:address_line2,@s20)                           !Cust_Add3
    Else !If job:Address_Line3 = ''
        l1:cust_town    =  Format(job:address_line3,@s20)                           !Cust_Add3
    End !If job:Address_Line3 = ''
    
    l1:cust_cnty    =  Format('',@s30)                         !Cust_Cnty
    l1:cust_pcod    =  Format(job:postcode,@s10)                                !Cust_PCod
    l1:cust_tel    =  Format(job:telephone_number,@s15)                        !Cust_Tel
!    l1:deal_name    =  Format(job:fault_code4,@s30)                             !Deal_Name
!    l1:deal_add1    =  Format('',@s30)                         !Deal_Add1
!    l1:deal_add2    =  Format('',@s30)                         !Deal_Add2
!    l1:deal_town    =  Format('',@s20)                                   !Deal_Add3
!    l1:deal_cnty    =  Format('',@s30)                         !Deal_Cnty
!    l1:deal_pcod    =  Format('',@s10)                                             !Deal_PCod
!    l1:deal_tel    =  Format('',@s15)                                        !Deal_Tel
    Add(out_file)
    count_parts# = 0

    Clear(ouf:Record)
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)

        l2:type1    = 'A2'

        If f_Batch_Number = 0
            l2:claim_no1    =  Format(Format(man:edi_account_number,@s2) & Format(tmp:ClaimNumber,@n06),@s8) !Claim Number
        Else !If f_Batch_Number = 0
            l2:claim_no1    =  Format(Format(man:edi_account_number,@s2) & Format(job:fault_code12,@n06),@s8) !Claim Number
        End !If f_Batch_Number = 0

        If wpr:part_number = 'ADJUSTMENT'
            l2:part_no  = Format('',@s18)
            l2:qty    =  Format('000000',@n06)
        Else!If wpr:part_number = 'ADJUSTMENT'
            l2:part_no    =  Format(wpr:part_number,@s18)
            l2:qty    =  Format(wpr:quantity,@n06)
        End!If wpr:part_number = 'ADJUSTMENT'
        l2:inv_no    =  Format(wpr:despatch_note_number,@s10)
        l2:circuit_r    =  Format(wpr:Fault_Code1,@s6)
        l2:sect_code    =  Format(wpr:Fault_Code2,@s3)
        l2:defect    =  Format(wpr:Fault_Code4,@s1)
        l2:repair    =  Format(wpr:Fault_Code3,@s1)
        count_parts# += 1
        Add(out_file)
    end !loop
    access:warparts.restorefile(save_wpr_id)
    If count_parts# = 0
        l2:type1    = 'A2'
        If f_Batch_Number = 0
            l2:claim_no1    =  Format(Format(man:edi_account_number,@s2) & Format(tmp:ClaimNumber,@n06),@s8) !Claim Number
        Else !If f_Batch_Number = 0
            l2:claim_no1    =  Format(Format(man:edi_account_number,@s2) & Format(job:fault_code12,@n06),@s8) !Claim Number
        End !If f_Batch_Number = 0
        l2:part_no    =  Format('***NO PART***',@s18)
        l2:qty    =  Format('',@n06)
        l2:inv_no    =  Format('',@s10)
        l2:circuit_r    =  Format('',@s6)
        l2:sect_code    =  Format('',@s3)
        l2:defect    =  Format('',@s1)
        l2:repair    =  Format('',@s1)
        Add(out_file)
    End!If count_parts# = 0
    count += 1
    Clear(Ouf:Record)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Samsung_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('pos',pos,'Samsung_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Samsung_EDI',1)
    SolaceViewVars('save_job_id',save_job_id,'Samsung_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Samsung_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Samsung_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Samsung_EDI',1)
    SolaceViewVars('tmp:ClaimNumber',tmp:ClaimNumber,'Samsung_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Samsung_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
