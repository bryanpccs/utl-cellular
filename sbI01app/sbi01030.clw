

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01030.INC'),ONCE        !Local module procedure declarations
                     END


Sendo_EDI            PROCEDURE  (f_batch_number)      ! Declare Procedure
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
save_job_id          USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
pos                  STRING(255)
ManEdiAccount        STRING(30)
WarrantyPartsCost    REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
EdiFile     FILE,DRIVER('BASIC'),PRE(edi),NAME(Filename),CREATE,BINDABLE,THREAD
EdiRecord     RECORD,PRE(edirec)
JobNumber           STRING(8)
DateBooked          STRING(10)
Country             STRING(2)
ModelNumber         STRING(30)
OriginalIMEI        STRING(20)
DateCode            STRING(10)
DOP                 STRING(10)
UnitType            STRING(30)
NewIMEI             STRING(20)
ServiceCentre       STRING(30)
DateCompleted       STRING(10)
DateDespatched      STRING(10)
CustomerFaultCode   STRING(30)
ActualFaultCode     STRING(30)
CircuitRef          STRING(30)
RepairCode          STRING(30)
PartRef_1           STRING(30)
PartRef_2           STRING(30)
PartRef_3           STRING(30)
SparePartCost       STRING(10)
RepairLevel         STRING(30)
LabourCost          STRING(10)
TotalRepairCost     STRING(10)
            END
         END

EdiFileV2   FILE,DRIVER('BASIC'),PRE(ediv2),NAME(Filename),CREATE,BINDABLE,THREAD
EdiRecordV2     RECORD,PRE(edirecv2)
JobNumber             STRING(8)
DateBooked            STRING(10)
Country               STRING(2)     ! = UK
ModelNumber           STRING(30)
OriginalIMEI          STRING(20)
DateCode              STRING(10)
DOP                   STRING(10)
UnitType              STRING(30)    ! Handset = Transceiver
NewIMEI               STRING(20)
ServiceCentre         STRING(30)
ServiceCentreLocation STRING(30)    ! JFC 6
InWorkshopDate        STRING(10)
DateCompleted         STRING(10)
DateDespatched        STRING(10)
CustomerFaultCode     STRING(30)
ActualFaultCode       STRING(30)
CircuitRefGroup     GROUP, DIM(12)
CircuitRef              STRING(30)  ! Warranty PArt 1-12 PFC1
END
RepairCode            STRING(30)
PartRefGroup        GROUP,DIM(12)
PartRef                 STRING(30)  ! Warranty Part Number 1-12
END
SparePartCost         STRING(10)
RepairLevel           STRING(30)    ! System Admin\Financial Defaults\Repair Type\Warranty Code
LabourCost            STRING(10)
TotalRepairCost       STRING(10)
ClaimCreated          STRING(6)     ! YYYYMM
Currency              STRING(30)    ! JFC 7
                END
            END



! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Sendo_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:JOBSTAGE.Open
   Relate:SUBTRACC.Open
   Relate:CHARTYPE.Open
   Relate:DISCOUNT.Open
   Relate:VATCODE.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:STOCK.Open
   Relate:JOBSE.Open
   Relate:EXCHANGE.Open
   Relate:REPTYDEF.Open
    tmp:ManufacturerName = 'SENDO'
    continue# = 0
    IF (f_batch_number = 0) THEN
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        IF (tmp:ProcessBeforeDate <> '') THEN
            continue# = 1
        END
    ELSE
        continue# = 1
    END

    IF ((continue# = 1) AND (FindManufacturer(tmp:ManufacturerName) <> Level:Fatal)) THEN
        ! Start Change 4010 BE(12/03/04)
!        IF f_batch_number = 0
!            filename = CLIP(CLIP(MAN:EDI_Path)&'\SENDO'&CLIP(Format(MAN:Batch_number,@n03))&'.CSV')
!        ELSE
!            filename = CLIP(CLIP(MAN:EDI_Path)&'\SENDO'&CLIP(Format(f_Batch_number,@n03))&'.CSV')
!        END
!        IF (~EXISTS(CLIP(filename))) THEN
!            CREATE(EdiFile)
!        END
!        OPEN(EdiFile, 12h)  ! Mode:  Read/Write Deny All
!        IF (ERRORCODE()) THEN
        ManEdiAccount = man:Edi_Account_Number
        error# = 0
        IF (man:SiemensNewEdi = 0) THEN
            IF f_batch_number = 0
                filename = CLIP(CLIP(MAN:EDI_Path)&'\SENDO'&CLIP(Format(MAN:Batch_number,@n03))&'.CSV')
            ELSE
                filename = CLIP(CLIP(MAN:EDI_Path)&'\SENDO'&CLIP(Format(f_Batch_number,@n03))&'.CSV')
            END
            IF (~EXISTS(CLIP(filename))) THEN
                CREATE(EdiFile)
            END
            OPEN(EdiFile, 12h)  ! Mode:  Read/Write Deny All
            IF (ERRORCODE()) THEN
            END
        ELSE
            filename = CLIP(CLIP(MAN:EDI_Path)&'\UK_' & CLIP(manEdiAccount) & '_' & FORMAT(YEAR(TODAY()), @n04) & |
                                                                     '-' & FORMAT(MONTH(TODAY()), @n02) &'.CSV')
            IF (~EXISTS(CLIP(filename))) THEN
                CREATE(EdiFileV2)
            END
            OPEN(EdiFileV2, 12h)  ! Mode:  Read/Write Deny All
            IF (ERRORCODE()) THEN
            END
        END
        IF (error# = 1) THEN
        ! End Change 4010 BE(12/03/04)
            MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.',|
                      'ServiceBase 2000',|
                      'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                      beep:systemhand,msgex:samewidths,84,26,0)
        ELSE
            ! Start Change 4010 BE(12/03/04)
            !EMPTY(EdiFile)
            !ManEdiAccount = man:Edi_Account_Number
            IF (man:SiemensNewEdi = 0) THEN
                EMPTY(EdiFile)
            ELSE
                EMPTY(EdiFileV2)
            END
            ! End Change 4010 BE(12/03/04)
            RecordsPerCycle = 25
            RecordsProcessed = 0
            PercentProgress = 0

            OPEN(ProgressWindow)
            Progress:Thermometer = 0
            ?Progress:PctText{Prop:Text} = '0% Completed'
            ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

            IF (f_batch_number = 0) THEN
                tmp:CountRecords = 0
                Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                Save_job_ID = Access:JOBS.SaveFile()
                Access:JOBS.ClearKey(job:EDI_Key)
                job:Manufacturer     = tmp:ManufacturerName
                job:EDI              = 'NO'
                SET(job:EDI_Key,job:EDI_Key)
                LOOP
                    IF ((Access:JOBS.NEXT() <> Level:benign) OR |
                        (job:Manufacturer <> tmp:ManufacturerName) OR |
                        (job:EDI <> 'NO')) THEN
                        BREAK
                    END
                    DO GetNextRecord2
                    cancelcheck# += 1
                    IF (cancelcheck# > (RecordsToProcess/100)) THEN
                        DO cancelcheck
                        IF (tmp:cancel = 1)
                            BREAK
                        END
                        cancelcheck# = 0
                    END

                    IF ((job:Date_Completed > tmp:ProcessBeforeDate) OR |
                         (job:Warranty_Job <> 'YES')) THEN
                        CYCLE
                    END

                    IF (job:ignore_warranty_charges <> 'YES') THEN
                        Pricing_Routine('W',labour",parts",pass",a")
                        IF (pass" = True) THEN
                            job:labour_cost_warranty = labour"
                            job:parts_cost_warranty  = parts"
                            job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                        END
                    END

                    !Calculate Total Parts Cost using Trade Price
                    access:chartype.clearkey(cha:charge_type_key)
                    cha:charge_type = job:warranty_charge_type
                    WarrantyPartsCost = 0.0
                    IF ((access:chartype.fetch(cha:charge_type_key) = Level:benign) AND |
                        (cha:no_charge <> 'YES') AND |
                        (cha:zero_parts <> 'YES')) THEN
                        save_wpr_id = access:warparts.savefile()
                        access:warparts.clearkey(wpr:part_number_key)
                        wpr:ref_number  = job:ref_number
                        SET(wpr:part_number_key,wpr:part_number_key)
                        LOOP
                            IF ((access:warparts.next() <> Level:benign) OR |
                                (wpr:ref_number  <> job:ref_number )) THEN
                                BREAK
                            END
                            ! Start Change 4690 BE(12/03/04)
                            !WarrantyPartsCost += wpr:sale_cost * wpr:quantity
                            IF (man:SiemensNewEdi = 0) THEN
                                WarrantyPartsCost += wpr:sale_cost * wpr:quantity
                            ELSE
                                WarrantyPartsCost += wpr:purchase_cost * wpr:quantity
                            END
                            ! End Change 4690 BE(12/03/04)
                        END
                        access:warparts.restorefile(save_wpr_id)
                    END

                    ! Start Change 4010 BE(12/03/04)
                    !DO Export
                    IF (man:SiemensNewEdi = 0) THEN
                        DO Export
                    ELSE
                        DO ExportV2
                    END
                    ! End Change 4010 BE(12/03/04)

                    pos = POSITION(job:EDI_Key)
                    job:EDI = 'YES'
                    job:EDI_Batch_Number = Man:Batch_Number
                    tmp:CountRecords += 1
                    AddToAudit
                    Access:JOBS.Update()
                    RESET(job:EDI_Key,pos)

                END
                Access:JOBS.RestoreFile(Save_job_ID)
                CLOSE(ProgressWindow)
                IF (tmp:CountRecords) THEN
                    IF (Access:EDIBATCH.PrimeRecord() = Level:Benign) THEN
                        ebt:Batch_Number    = man:Batch_Number
                        ebt:Manufacturer    = tmp:ManufacturerName
                        Access:EDIBATCH.Insert()
                        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                        man:Manufacturer = tmp:ManufacturerName
                        IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign) THEN
                            man:Batch_Number += 1
                            Access:MANUFACT.Update()
                        END
                    END
                    MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                         '<13,10>'&|
                         '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                         'Styles\idea.ico','&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                         beep:systemasterisk,msgex:samewidths,84,26,0)
                END
            ELSE
                tmp:CountRecords = 0
                Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)
                Save_job_ID = Access:JOBS.SaveFile()
                Access:JOBS.ClearKey(job:EDI_Key)
                job:Manufacturer     = tmp:ManufacturerName
                job:EDI              = 'YES'
                job:EDI_Batch_Number = f_Batch_Number
                SET(job:EDI_Key,job:EDI_Key)
                LOOP
                    IF ((Access:JOBS.NEXT() <> Level:Benign) OR |
                        (job:Manufacturer <> tmp:ManufacturerName ) OR |
                        (job:EDI <> 'YES' ) OR |
                        (job:EDI_Batch_Number <> f_Batch_Number)) THEN
                       BREAK
                    END
                    DO GetNextRecord2
                    cancelcheck# += 1
                    IF (cancelcheck# > (RecordsToProcess/100)) THEN
                        DO cancelcheck
                        IF (tmp:cancel = 1)
                            BREAK
                        END
                        cancelcheck# = 0
                    END

                    !Calculate Total Parts Cost using Trade Price
                    access:chartype.clearkey(cha:charge_type_key)
                    cha:charge_type = job:warranty_charge_type
                    WarrantyPartsCost = 0.0
                    IF ((access:chartype.fetch(cha:charge_type_key) = Level:benign) AND |
                        (cha:no_charge <> 'YES') AND |
                        (cha:zero_parts <> 'YES')) THEN
                        save_wpr_id = access:warparts.savefile()
                        access:warparts.clearkey(wpr:part_number_key)
                        wpr:ref_number  = job:ref_number
                        SET(wpr:part_number_key,wpr:part_number_key)
                        LOOP
                            IF ((access:warparts.next() <> Level:benign) OR |
                                (wpr:ref_number  <> job:ref_number )) THEN
                                BREAK
                            END
                            ! Start Change 4690 BE(12/03/04)
                            !WarrantyPartsCost += wpr:sale_cost * wpr:quantity
                            IF (man:SiemensNewEdi = 0) THEN
                                WarrantyPartsCost += wpr:sale_cost * wpr:quantity
                            ELSE
                                WarrantyPartsCost += wpr:purchase_cost * wpr:quantity
                            END
                            ! End Change 4690 BE(12/03/04)
                        END
                        access:warparts.restorefile(save_wpr_id)
                    END

                    ! Start Change 4010 BE(12/03/04)
                    !DO Export
                    IF (man:SiemensNewEdi = 0) THEN
                        DO Export
                    ELSE
                        DO ExportV2
                    END
                    ! End Change 4010 BE(12/03/04)
                END
                Access:JOBS.RestoreFile(Save_job_ID)
                CLOSE(ProgressWindow)

                IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                END

            END

            ! Start Change 4010 BE(12/03/04)
            !CLOSE(EdiFile)
            IF (man:SiemensNewEdi = 0) THEN
                CLOSE(EdiFile)
            ELSE
                CLOSE(EdiFileV2)
            END
            ! End Change 4010 BE(12/03/04)
        END
    END
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:JOBSTAGE.Close
   Relate:SUBTRACC.Close
   Relate:CHARTYPE.Close
   Relate:DISCOUNT.Close
   Relate:VATCODE.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:STOCK.Close
   Relate:JOBSE.Close
   Relate:EXCHANGE.Close
   Relate:REPTYDEF.Close
Export      ROUTINE
    CLEAR(EdiFile)
    edirec:JobNumber            = FORMAT(job:ref_number, @s8)
    edirec:DateBooked           = FORMAT(job:date_booked, @d06)  ! dd/mm/yyyy
    edirec:Country              = 'UK'
    edirec:ModelNumber          = CLIP(job:model_number)

    access:JOBSE.clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    IF ((access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign)  AND |
        (jobe:Pre_RF_Board_IMEI <> ''))THEN
        edirec:OriginalIMEI         = CLIP(jobe:Pre_RF_Board_IMEI)
        edirec:NewIMEI              = CLIP(job:ESN)
    ELSE
        edirec:OriginalIMEI         = CLIP(job:ESN)
    END

    edirec:DateCode             = CLIP(job:fault_code1)
    edirec:DOP                  = FORMAT(job:DOP, @d06)          ! dd/mm/yyyy

    IF (CLIP(job:unit_type) = 'HANDSET') THEN
        edirec:UnitType             = 'Transceiver'
    ELSE
        edirec:UnitType             = CLIP(job:unit_type)
    END

    IF (job:Exchange_Unit_Number <> 0) THEN
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign) THEN
            edirec:NewIMEI     = CLIP(xch:ESN)
        END
    END

    edirec:ServiceCentre        = CLIP(ManEdiAccount)
    edirec:DateCompleted        = FORMAT(job:date_completed, @d06)  ! dd/mm/yyyy
    edirec:DateDespatched       = FORMAT(job:date_completed, @d06)  ! dd/mm/yyyy
    edirec:CustomerFaultCode    = CLIP(job:fault_code2)
    edirec:ActualFaultCode      = CLIP(job:fault_code3)
    edirec:CircuitRef           = CLIP(job:fault_code4)
    edirec:RepairCode           = CLIP(job:fault_code5)

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    SET(wpr:part_number_key,wpr:part_number_key)
    count# = 1
    LOOP
        IF ((access:warparts.next() <> Level:Benign) OR |
            (wpr:ref_number <> job:ref_number)) THEN
           BREAK
        END
        IF (ExcludeFromEDI(wpr:Part_Ref_Number)) THEN
            CYCLE
        END
        !Start - Use "Alternative Part Number" which is stored in Fault Code 12 - TrkBs: 4981 (DBH: 08-12-2004)
        CASE count#
            OF 1
                edirec:PartRef_1 = CLIP(wpr:Fault_Code12)
            OF 2
                edirec:PartRef_2 = CLIP(wpr:Fault_Code12)
            OF 3
                edirec:PartRef_3 = CLIP(wpr:Fault_Code12)
            ELSE
                BREAK
        END
        !End   - Use "Alternative Part Number" which is stored in Fault Code 12 - TrkBs: 4981 (DBH: 08-12-2004)
        count# += 1
    END
    access:warparts.restorefile(save_wpr_id)

    edirec:SparePartCost        = LEFT(FORMAT(WarrantyPartsCost, @n6.2))
    edirec:RepairLevel          = CLIP(job:repair_type_warranty)
    edirec:LabourCost           = LEFT(FORMAT(job:Labour_Cost_warranty, @n6.2))
    edirec:TotalRepairCost      = LEFT(FORMAT((WarrantyPartsCost + job:Labour_Cost_Warranty), @n6.2))

    ADD(EdiFile)
ExportV2    ROUTINE
    CLEAR(EdiFileV2)
    edirecV2:JobNumber            = FORMAT(job:ref_number, @s8)
    edirecV2:DateBooked           = FORMAT(job:date_booked, @d06)  ! dd/mm/yyyy
    edirecV2:Country              = 'UK'
    edirecV2:ModelNumber          = CLIP(job:model_number)

    access:JOBSE.clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    IF ((access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign)  AND |
        (jobe:Pre_RF_Board_IMEI <> ''))THEN
        edirecV2:OriginalIMEI         = CLIP(jobe:Pre_RF_Board_IMEI)
        edirecV2:NewIMEI              = CLIP(job:ESN)
    ELSE
        edirecV2:OriginalIMEI         = CLIP(job:ESN)
    END

    edirecV2:DateCode             = CLIP(job:fault_code1)
    edirecV2:DOP                  = FORMAT(job:DOP, @d06)          ! dd/mm/yyyy

    IF (CLIP(job:unit_type) = 'HANDSET') THEN
        edirecV2:UnitType             = 'Transceiver'
    ELSE
        edirecV2:UnitType             = CLIP(job:unit_type)
    END

    IF (job:Exchange_Unit_Number <> 0) THEN
        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
        xch:Ref_Number = job:Exchange_Unit_Number
        IF (Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = Level:Benign) THEN
            edirecV2:NewIMEI     = CLIP(xch:ESN)
        END
    END

    edirecV2:ServiceCentre         = CLIP(ManEdiAccount)
    edirecV2:ServiceCentreLocation = CLIP(job:fault_code6)
    edirecV2:InWorkshopDate        = FORMAT(jobe:InWorkshopDate, @d06)  ! dd/mm/yyyy
    edirecV2:DateCompleted         = FORMAT(job:date_completed, @d06)  ! dd/mm/yyyy
    edirecV2:DateDespatched        = FORMAT(job:date_completed, @d06)  ! dd/mm/yyyy
    edirecV2:CustomerFaultCode     = CLIP(job:fault_code2)
    edirecV2:ActualFaultCode       = CLIP(job:fault_code3)

    edirecV2:RepairCode            = CLIP(job:fault_code5)

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    SET(wpr:part_number_key,wpr:part_number_key)
    count# = 1
    LOOP
        IF ((access:warparts.next() <> Level:Benign) OR |
            (wpr:ref_number <> job:ref_number) OR |
            (count# > 12)) THEN
           BREAK
        END
        IF (ExcludeFromEDI(wpr:Part_Ref_Number)) THEN
            CYCLE
        END
        edirecV2:CircuitRefGroup[count#].CircuitRef = CLIP(wpr:Fault_Code1)
        !Use "Alternative Part Number", which is stored in Fault Code 12 - TrkBs: 4981 (DBH: 08-12-2004)
        edirecV2:PartRefGroup[count#].PartRef = CLIP(wpr:Fault_Code12)
        count# += 1
    END
    access:warparts.restorefile(save_wpr_id)

    edirecV2:SparePartCost        = LEFT(FORMAT(WarrantyPartsCost, @n6.2))

    access:reptydef.clearkey(rtd:warranty_key)
    rtd:warranty    = 'YES'
    rtd:repair_type = job:repair_type_warranty
    IF (access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign) THEN
        edirecV2:RepairLevel = CLIP(rtd:WarrantyCode)
    END

    edirecV2:LabourCost      = LEFT(FORMAT(job:Labour_Cost_warranty, @n6.2))
    edirecV2:TotalRepairCost = LEFT(FORMAT((WarrantyPartsCost + job:Labour_Cost_Warranty), @n6.2))
    edirecV2:ClaimCreated    = FORMAT(YEAR(TODAY()), @n04) & FORMAT(MONTH(TODAY()), @n02)
    edirecV2:Currency        = CLIP(job:fault_code7)

    ADD(EdiFileV2)

getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Sendo_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Sendo_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Sendo_EDI',1)
    SolaceViewVars('save_job_id',save_job_id,'Sendo_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Sendo_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Sendo_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Sendo_EDI',1)
    SolaceViewVars('pos',pos,'Sendo_EDI',1)
    SolaceViewVars('ManEdiAccount',ManEdiAccount,'Sendo_EDI',1)
    SolaceViewVars('WarrantyPartsCost',WarrantyPartsCost,'Sendo_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
