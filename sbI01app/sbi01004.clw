

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01004.INC'),ONCE        !Local module procedure declarations
                     END


NEC_EDI              PROCEDURE  (f_batch_number)      ! Declare Procedure
save_job_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
pos                  STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(255)
line2           STRING(255)
line3           STRING(2)
          . . .
Out_File2 FILE,DRIVER('ASCII'),PRE(OU2),NAME(Filename2),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group2     GROUP
Line1           STRING(76)
          . . .

Out_Part GROUP,OVER(Ou2:Out_Group2),PRE(L2)
job_number       String(8)
description      String(30)
part_number      String(30)
retail_cost       String(8)
            .
!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
serial_no         STRING(18)
exch_sn           STRING(18)
eqpt_type         STRING(4)
eqpt_no           STRING(18)
damage1           STRING(4)
damage2           STRING(4)
damage3           STRING(4)
damage4           STRING(4)
damage5           STRING(4)
Activity1         STRING(4)
Activity2         STRING(4)
Activity3         STRING(4)
Activity4         STRING(4)
Activity5         STRING(4)
fault             STRING(40)
job_no            STRING(20)
date_in           STRING(8)
date_out          STRING(8)
material1         STRING(18)
material2         STRING(18)
material3         STRING(18)
material4         STRING(18)
material5         STRING(18)
qty1              STRING(13)
qty2              STRING(13)
qty3              STRING(13)
qty4              STRING(13)
qty5              STRING(13)
circ_ref1         STRING(30)
circ_ref2         STRING(30)
circ_ref3         STRING(30)
circ_ref4         STRING(30)
circ_ref5         STRING(30)
eng_surn          STRING(30)
eng_code          STRING(3)
           .


mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL
labour        REAL

! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'NEC_EDI')      !Add Procedure to Log
  end


   Relate:EXCHANGE.Open
   Relate:JOBS.Open
   Relate:WARPARTS.Open
   Relate:MANUFACT.Open
   Relate:USERS.Open
   Relate:EDIBATCH.Open
   Relate:JOBSTAGE.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:AUDIT.Open
   Relate:CHARTYPE.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:JOBNOTES.Open
   Relate:STOCK.Open
!**Variable
    tmp:ManufacturerName = 'NEC'
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
!**Variable
            filename = CLIP(CLIP(MAN:EDI_Path)&'\'&FORMAT(today(),@d11)&CLIP(MAN:EDI_Account_Number))
            If f_batch_number = 0
                filename2 = clip(man:edi_path) & '\NEC' & CLip(format(man:batch_number,@n05)) & '.CSV'
            Else!If f_batch_number = 0
                filename2 = clip(man:edi_path)&'\NEC'&Clip(format(f_batch_number,@n05)) & '.CSV'
            End!If f_batch_number = 0
            OPEN(Out_File2)                           ! Open the output file
            IF ERRORCODE()                           ! If error
                CREATE(Out_File2)                       ! create a new file
                If Errorcode()
                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End!If Errorcode() 
                OPEN(out_file2)        ! If still error then stop
            ELSE
                OPEN(out_file2)
                EMPTY(Out_file2)
            END
            If Error# = 0
                OPEN(Out_File)                           ! Open the output file
                IF ERRORCODE()                           ! If error
                    CREATE(Out_File)                       ! create a new file
                    IF ERRORCODE()
                        Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!IF ERRORCODE()
                    OPEN(out_file)
                    EMPTY(Out_file)

                ELSE
                    OPEN(out_file)
                    EMPTY(Out_file)
                End

            End !If Error# = 0
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        Do Export

                        !Found
                        pos = Position(job:EDI_Key)
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)
                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        Do Export
                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    ! Start Change 2806 BE(22/07/03)
                    !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !  '<13,10>'&|
                    !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                    !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !    Of 1 ! &OK Button
                    !End!Case MessageEx
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! END Change 2806 BE(22/07/03)

                End!If f_batch_number = 0
                CLOSE(out_file)
            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:EXCHANGE.Close
   Relate:JOBS.Close
   Relate:WARPARTS.Close
   Relate:MANUFACT.Close
   Relate:USERS.Close
   Relate:EDIBATCH.Close
   Relate:JOBSTAGE.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:AUDIT.Close
   Relate:CHARTYPE.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:JOBNOTES.Close
   Relate:STOCK.Close
Export        Routine
      CLEAR(ouf:RECORD)
      CLEAR(mt_total)
      Clear(ou2:record)
        IMEIError# = 0
        If job:Third_Party_Site <> ''
            Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
            jot:RefNumber = job:Ref_Number
            Set(jot:RefNumberKey,jot:RefNumberKey)
            If Access:JOBTHIRD.NEXT()
                IMEIError# = 1
            Else !If Access:JOBTHIRD.NEXT()
                If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 1
                Else !If jot:RefNumber <> job:Ref_Number            
                    IMEIError# = 0                
                End !If jot:RefNumber <> job:Ref_Number            
            End !If Access:JOBTHIRD.NEXT()
        Else !job:Third_Party_Site <> ''
            IMEIError# = 1    
        End !job:Third_Party_Site <> ''

        If IMEIError# = 1
            l1:serial_no = UPPER(stripcomma(stripreturn(job:ESN)))
        Else !IMEIError# = 1
            l1:serial_no = UPPER(stripcomma(stripreturn(JOt:OriginalIMEI)))
        End !IMEIError# = 1
      
      IF job:Exchange_Unit_Number <> ''
          
          access:exchange.clearkey(job:ref_number_key)
          XCH:Ref_Number = job:Exchange_Unit_Number
          If access:exchange.fetch(job:ref_number_key) = Level:Benign
              l1:exch_sn   = UPPER(stripcomma(stripreturn(XCH:ESN)))
          ELSE!If access:exchange.fetch(job:ref_number_key) = Level:Benign
              l1:exch_sn   = ''
          END!If access:exchange.fetch(job:ref_number_key) = Level:Benign
      ELSE
          l1:exch_sn   = ''
      END
      l1:eqpt_type   = UPPER(stripcomma(stripreturn(job:Fault_Code2)))
      IF job:Authority_Number = ''
          l1:eqpt_no   = UPPER(stripcomma(stripreturn(job:Fault_Code1)))
      ELSE
          l1:eqpt_no   = UPPER(stripcomma(stripreturn(job:Authority_Number)))
      .
      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
      jbn:RefNumber = job:Ref_Number
      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found

      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
      

      l1:fault       = UPPER(stripcomma(stripreturn(JBN:Fault_Description)))
      l1:job_no      = FORMAT(job:Ref_Number,@n_20)
      l1:date_in     = FORMAT(job:date_booked,@d12)
      l1:date_out    = FORMAT(job:Date_Completed,@d12)
      l1:eng_code    = UPPER(SUB(job:Engineer,1,1))
      access:users.clearkey(use:user_code_key)
      USE:User_Code = job:engineer
      access:users.fetch(use:user_code_key)
      l1:eng_surn    = UPPER(stripcomma(stripreturn(USE:Surname)))

      found_main# = 0
      setcursor(cursor:wait)
      save_wpr_id = access:warparts.savefile()
      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job:ref_number
      set(wpr:part_number_key,wpr:part_number_key)
      loop
          if access:warparts.next()
             break
          end !if
          if wpr:ref_number  <> job:ref_number      |
              then break.  ! end if
          If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
              Cycle
          End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)

          If wpr:main_part = 'YES'                                  
              l1:damage1     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
              l1:Activity1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
              l1:material1   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
              l1:qty1        = FORMAT(WPR:Quantity,@n_13)
              l1:circ_ref1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
              found_main# = 1
              Break
          End!          If wpr:main_part = 'YES'
      end !loop
      access:warparts.restorefile(save_wpr_id)
      setcursor()

      spa_cnt#=0

      setcursor(cursor:wait)
      save_wpr_id = access:warparts.savefile()
      access:warparts.clearkey(wpr:part_number_key)
      wpr:ref_number  = job:ref_number
      set(wpr:part_number_key,wpr:part_number_key)
      loop
          if access:warparts.next()
             break
          end !if
          if wpr:ref_number  <> job:ref_number      |
              then break.  ! end if

          If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
              Cycle
          End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)

          If found_main# = 0
              spa_cnt#+=1
               If spa_cnt# > 5
                    l2:job_number   = stripcomma(stripreturn(job:ref_number))
                    l2:description  = ',' & stripcomma(stripreturn(wpr:description))
                    l2:part_number  = ',' & stripcomma(stripreturn(wpr:part_number))
                    l2:retail_cost   = ',' & Format(wpr:sale_cost,@n10.2)
                    Add(out_file2)
               End!If spa_cnt# > 5

              CASE spa_cnt#
                OF 1
                  l1:damage1     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material1   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty1        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref1   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 2
                  l1:damage2     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material2   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty2        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
               OF 3
                  l1:damage3     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity3   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material3   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty3        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref3   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 4
                  l1:damage4     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity4   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material4   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty4        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref4   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 5
                  l1:damage5     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity5   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material5   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty5        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref5   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
               End!CASE spa_cnt#
          Else!If found_main# = 0
              If wpr:main_part = 'YES'
                Cycle
              End!If wpr:main_part = 'YES'
              spa_cnt#+=1
               If spa_cnt# > 4
                    l2:job_number   = stripcomma(stripreturn(job:ref_number))
                    l2:description  = ',' & stripcomma(stripreturn(wpr:description))
                    l2:part_number  = ',' & stripcomma(stripreturn(wpr:part_number))
                    l2:retail_cost   = ',' & Format(wpr:sale_cost,@n10.2)
                    Add(out_file2)
               End!If spa_cnt# > 5

              CASE spa_cnt#
                OF 1
                  l1:damage2     = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material2   = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty2        = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref2   = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 2
                  l1:damage3    = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity3  = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material3  = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty3       = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref3  = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 3
                  l1:damage4    = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity4  = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material4  = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty4       = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref4  = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
                OF 4
                  l1:damage5    = UPPER(stripcomma(stripreturn(WPR:Fault_Code2)))
                  l1:Activity5  = UPPER(stripcomma(stripreturn(WPR:Fault_Code3)))
                  l1:material5  = UPPER(stripcomma(stripreturn(WPR:Part_Number)))
                  l1:qty5       = FORMAT(WPR:Quantity,@n_13)
                  l1:circ_ref5  = UPPER(stripcomma(stripreturn(WPR:Fault_Code1)))
               End!CASE spa_cnt#
          End!If found_main# = 0
      end !loop
      access:warparts.restorefile(save_wpr_id)
      setcursor()
      ADD(out_file)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'NEC_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'NEC_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'NEC_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'NEC_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'NEC_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'NEC_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'NEC_EDI',1)
    SolaceViewVars('pos',pos,'NEC_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
