

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01016.INC'),ONCE        !Local module procedure declarations
                     END


Sony_EDI             PROCEDURE  (f_batch_number)      ! Declare Procedure
tmp:FirstPage        BYTE
save_wpr_id          USHORT,AUTO
save_job_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
pos                  STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Line1           STRING(2000)
           ..


mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL
parts_temp         REAL
labour_temp        REAL
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Sony_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:JOBSTAGE.Open
   Relate:SUBTRACC.Open
   Relate:CHARTYPE.Open
   Relate:DISCOUNT.Open
   Relate:VATCODE.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
!**Variable
    tmp:ManufacturerName = 'SONY'

    !Check the flag to see if should be using the Sony Ericcson export instead
    !BB 2696
    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
    man:Manufacturer = 'SONY'
    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:SiemensNewEDI
            Sony_Ericcson_EDI(f_Batch_Number,'SONY')
            Return
        End !If man:SiemensNewEDI
    Else !If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
        !Error
    End !If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign

    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
!**Variable
            IF f_batch_number = 0
                filename = CLIP(CLIP(MAN:EDI_Path)&'\SON'&CLIP(Format(MAN:Batch_number,@n05))&'.CSV')
            ELSE
                filename = CLIP(CLIP(MAN:EDI_Path)&'\SON'&CLIP(Format(f_Batch_number,@n05))&'.CSV')
            END
            OPEN(Out_File)                           ! Open the output file
            IF ERRORCODE()                           ! If error
                CREATE(Out_File)                       ! create a new file
                If Errorcode()
                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End!If Errorcode() 
                OPEN(out_file)        ! If still error then stop
            ELSE
                OPEN(out_file)
                EMPTY(Out_file)
            END
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'
                tmp:FirstPage = 1

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'

                        Do Export

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        !Found
                        pos = Position(job:EDI_Key)
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)
                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        Do Export
                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    ! Start Change 2806 BE(22/07/03)
                    !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !  '<13,10>'&|
                    !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                    !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !    Of 1 ! &OK Button
                    !End!Case MessageEx
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)


                End!If f_batch_number = 0
                CLOSE(out_file)
            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:JOBSTAGE.Close
   Relate:SUBTRACC.Close
   Relate:CHARTYPE.Close
   Relate:DISCOUNT.Close
   Relate:VATCODE.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
Export      Routine
    If tmp:FirstPage = 1
        Clear(ouf:record)
        ouf:line1   = 'RegionCode,CustomerID,RepairNo,ModelNo,IMEI,DatePurchase,DateReceived,DateShipped,' &|
                        'WarrantyCode,ConditionCode,SymptomCode,DefectCode,1stPartNo,1stPartQty,' &|
                        '2ndPartNo,2ndPartQty,3rdPartNo,3rdPartQty,4thPartNo,4thPartQty,' &|
                        '5thPartNo,5thPartQty,LabourClaim,PartsClaim,TotalClaim'
        Add(out_file)
        tmp:firstpage = 0
    End!If tmp:FirstPage = 1
    Clear(Ouf:Record)
    ouf:line1   = 'IP1,'
    ouf:line1   = Clip(ouf:line1) & Clip(man:edi_account_number) & ','
    ouf:line1   = Clip(ouf:line1) & Clip(job:ref_number) & ','
    ouf:line1   = Clip(ouf:line1) & Clip(job:model_number) & ','
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        ouf:line1   = Clip(ouf:line1) & Clip(job:esn) & ','
    Else !IMEIError# = 1
        ouf:line1   = Clip(ouf:line1) & Clip(jot:OriginalIMEI) & ','
    End !IMEIError# = 1
    
    If job:dop <> ''
        ouf:line1   = Clip(ouf:line1) & Format(Day(job:dop),@n02) & '.' & Format(Month(job:dop),@n02) &|
                                        '.' & Format(Year(job:dop),@n04) & ','

    Else!If job:dop <> ''
        ouf:line1   = Clip(ouf:Line1) & ','
    End!If job:dop <> ''
    ouf:line1   = Clip(ouf:line1) & Format(Day(job:date_booked),@n02) & '.' & Format(Month(job:date_booked),@n02) &|
                                    '.' & Format(Year(job:date_booked),@n04) & ','
    If job:exchange_unit_number <> ''
        If job:exchange_despatched <> ''
            ouf:line1   = Clip(ouf:line1) & Format(Day(job:Exchange_Despatched),@n02) & '.' & Format(Month(job:Exchange_Despatched),@n02) &|
                                        '.' & Format(Year(job:Exchange_Despatched),@n04) & ','
        Else!If job:exchange_despatched <> ''
            ouf:line1   = Clip(ouf:line1) & ','
        End!If job:exchange_despatched <> ''
    Else!If job:exchange_unit_number <> ''
        If job:date_despatched <> ''
            ouf:line1   = Clip(ouf:line1) & Format(Day(job:Date_Despatched),@n02) & '.' & Format(Month(job:Date_Despatched),@n02) &|
                                        '.' & Format(Year(job:Date_Despatched),@n04) & ','
        Else!If job:date_despatched <> ''
            ouf:line1   = Clip(ouf:line1) & ','
        End!If job:date_despatched <> ''
    End!If job:exchange_unit_number <> ''

    ouf:line1   = Clip(ouf:line1) & Clip(job:fault_code1) & ','     !Warranty Code
    ouf:line1   = Clip(ouf:line1) & Clip(job:fault_code2) & ','     !Condition Code
    ouf:line1   = Clip(ouf:line1) & Clip(job:fault_code3) & ','     !Symptom Code
    ouf:line1   = Clip(ouf:line1) & Clip(job:fault_code4) & ','     !Defect Code

    count_parts# = 0
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)
        count_parts# += 1
        ouf:line1   = Clip(ouf:line1) & Clip(wpr:part_number) & ','
        ouf:line1   = Clip(ouf:line1) & Clip(wpr:quantity) & ','
        If count_parts# => 5
            Break
        End!If count_parts# => 5
    end !loop
    access:warparts.restorefile(save_wpr_id)
    count_parts# += 1
    Loop x# = count_parts# To 5
        ouf:line1   = Clip(ouf:line1) & ','
        ouf:line1   = Clip(ouf:line1) & ','
    End!Loop x# = count_parts# To 5

    ouf:line1   = Clip(ouf:line1) & Format(job:labour_cost_warranty,@n10.2) & ','
    ouf:line1   = Clip(ouf:line1) & Format(job:parts_cost_warranty,@n10.2) & ','
    ouf:line1   = Clip(ouf:line1) & Format(job:sub_total_warranty,@n10.2) & ','
    Add(out_file)
    count += 1
    Clear(Ouf:Record)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Sony_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('tmp:FirstPage',tmp:FirstPage,'Sony_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Sony_EDI',1)
    SolaceViewVars('save_job_id',save_job_id,'Sony_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Sony_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Sony_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Sony_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Sony_EDI',1)
    SolaceViewVars('pos',pos,'Sony_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
