

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01036.INC'),ONCE        !Local module procedure declarations
                     END


Panasonic_EDI        PROCEDURE  (f_batch_number)      ! Declare Procedure
EdiVersion           LONG
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
save_job_id          USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
pos                  STRING(255)
tmp:NewFileName      STRING(255)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group       Group
Line1           STRING(2000)
          . ..

Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
type         STRING(2)
date1        String(4)
date3        String(2)
date4        String(2)
! Start Change 2290 BE(14/03/03)
!jobno        String(6)
jobno        String(8)
! End Change 2290 BE(14/03/03)
accountno    String(4)
filler       String(48)
modelno      String(14)
! Start Change 2852 BE(01/07/03)
!serialno     String(14)
serialno     String(15)
! End Change 2852 BE(01/07/03)
! Start Change 3020 BE(05/08/03)
!servcode     String(1)
! End Change 3020 BE(05/08/03)
dop          String(6)
faultdate    String(6)
compdate     String(6)
filler2      String(124)
faulttext    String(70)
repairtext   String(70)
filler3      String(10)
condcode     String(1)
symptom1     String(1)
symptom2     String(1)
extencode    String(1)
filler4      String(26)
cust_name    STRING(30)
cust_add1    STRING(30)
cust_add2    STRING(30)
cust_town    STRING(30)
cust_cnty    STRING(20)
cust_pcod    STRING(10)
cust_tel     STRING(15)
DealerName   String(30)
filler5      String(60)
DealerCity   String(20)
filler6      String(54)
fillerx      String(1)
          .
!out_fil2    DOS,PRE(ou2),NAME(filename)
Out_Part   GROUP,OVER(Ouf:Out_Group),PRE(L2)
type1        STRING(2)
! Start Change 2290 BE(14/03/03)
!jobno1       String(6)
jobno1       String(8)
! End Change 2290 BE(14/03/03)
part_no      STRING(14)
qty          STRING(6)
filler1      String(14)
sect_code    STRING(3)
defect       STRING(1)
repair       STRING(1)
PanaInvoice  String(9)
Filler2      String(32)
Fillery      String(1)
         .

! Start Change 4502 BE(06/09/2004)
ClaimFile FILE,DRIVER('DOS'),PRE(CLF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group       GROUP
Line1            STRING(2000)
         END
           END
                END

R00_Part   GROUP,OVER(CLF:Out_Group),PRE(R00)
RecordType           STRING(2)        ! 00
VersionNumber        STRING(2)        ! 20
BatchName            STRING(10)       ! Batch Creation TimeStamp YYMMDDhhmm
ClientNumber         STRING(13)       ! Panaonic Account Number
BatchCreationDate    STRING(8)        ! Batch Creation Date  YYYYMMDD
ClaimAddresssing     STRING(35)       ! Company Name in Licence Address
Dummy                STRING(3)        ! Blank
CompanyId            STRING(9)        ! Blank
Country              STRING(4)        ! Blank
BranchCode           STRING(4)        ! Blank
AccountNumber        STRING(13)       ! Blank
BatchDocumentType    STRING(3)        ! CLA
BatchDirection       STRING(1)        ! C
EOR                  STRING(2)
           END

R01_Part   GROUP,OVER(CLF:Out_Group),PRE(R01)
RecordType              STRING(2)        ! 00
VersionNumber           STRING(2)        ! 20
BatchName               STRING(10)       ! Batch Creation TimeStamp YYMMDDhhmm
ClientNumber            STRING(13)       ! Panaonic Account Number
WorkSheetNumber         STRING(15)       ! ServiceBase Batch No
ActivityClaimedFor      STRING(1)        ! Job Fault Code ??
GuaranteeCode           STRING(2)        ! Job Fault Code ??
DateRepairReceived      STRING(8)        ! InWorkshop Date
DateRepairDespatched    STRING(8)        ! Repair Completed Date
Workshop                STRING(1)        ! W
RepairStartDate         STRING(8)        ! Blank
StockIndicator          STRING(1)        ! D
ConsumerName            STRING(35)       ! Customer Address - Company Name
ConsumerAddress1        STRING(35)       ! Customer Address - Address Line 1
ConsumerAddress2        STRING(35)       ! Customer Address - Address Line 2
ConsumerPhoneNumber     STRING(15)       ! Customer Address - Telephone Number
ConsumerPostCode        STRING(9)        ! Customer Address - Postcode
ConsumerCity            STRING(35)       ! Customer Address - Address Line 3
ConsumerCountryCode     STRING(3)        ! Blank
AuthorisationNumber     STRING(9)        ! Blank
BrandName               STRING(35)       ! Blank
SerialNumber            STRING(35)       ! IMEI
ModelNumber             STRING(35)       ! Model No
PurchaseDate            STRING(8)        ! DoP
ExtendedGuaranteeNumber STRING(9)        ! Blank
DealerName              STRING(35)       ! Service Base Licence Address - Company Name
DealerAddress1          STRING(35)       ! Service Base Licence Address - Address Line 1
DealerAddress2          STRING(35)       ! Service Base Licence Address - Address Line 2
DealerPostcode          STRING(9)        ! Service Base Licence Address - Postcode
DealerCity              STRING(35)       ! Service Base Licence Address - AddressLine 3
DealerVATNumber         STRING(35)       ! ServiceBase VAT No (Licence Address Screen)
DealerCountryCode       STRING(3)        ! GBR
Currency                STRING(3)        ! GBP
ConsumerDataConsent     STRING(1)        ! U
SecondSerialNumber      STRING(20)       ! Blank
ArticleNumber           STRING(20)       ! Blank
ClientNumber2           STRING(13)       ! Blank
ClaimId2                STRING(15)       ! Blank
NotReporting            STRING(1)        ! Blank
CountryOfSalesOrigin    STRING(3)        ! Blank
EOR                     STRING(2)
           END

R02_Part   GROUP,OVER(CLF:Out_Group),PRE(R02)
RecordType              STRING(2)        ! 00
VersionNumber           STRING(2)        ! 20
BatchName               STRING(10)       ! Batch Creation TimeStamp YYMMDDhhmm
ClientNumber            STRING(13)       ! Panaonic Account Number
WorkSheetNumber         STRING(15)       ! ServiceBase Batch No
PositionNumber          STRING(9)        ! Blank
ConditionCode           STRING(1)        ! Job Fault Code 1
SymptomCode             STRING(3)        ! Job Fault Code 2
Flag                    STRING(1)        ! Part Fault Code 1
DefectCode              STRING(2)        ! Part Fault Code 3
RepairCode              STRING(2)        ! Part Fault Code 4
SectionCode             STRING(3)        ! Part Fault Code 2
PCBDescription          STRING(7)        ! Blank
PartOrderNumber         STRING(20)       ! Part Number unless sundry item (eg software upgrade)
InvoiceNumber           STRING(20)       ! Blank
PriceOfPart             STRING(10)       ! Blank
QuantityParts           STRING(2)        ! Part Quantity
Currency                STRING(3)        ! GBP
MessageSequenceNumber   STRING(3)        ! Line No per part on job (eg 001, 002 , 003)
FOCParts                STRING(20)       ! Blank
TracedPartId            STRING(22)       ! Blank
InitialFailure          STRING(1)        ! Blank
ProductionFailure       STRING(1)        ! Blank
PartClaimPricePercent   STRING(3)        ! Blank
InvoiceTypePart         STRING(1)        ! Blank
DeliveryNoteNumber      STRING(20)       ! Blank
EOR                     STRING(2)
           END

R05_Part   GROUP,OVER(CLF:Out_Group),PRE(R05)
RecordType           STRING(2)        ! 00
VersionNumber        STRING(2)        ! 20
BatchName            STRING(10)       ! Batch Creation TimeStamp YYMMDDhhmm
ClientNumber         STRING(13)       ! Panaonic Account Number
WorkSheetNumber      STRING(15)       ! ServiceBase Batch No
MessageSeqNumber     STRING(2)        ! Blank
MessageCode          STRING(1)        ! Blank
MemoField            STRING(70)       ! ServiceBase Fault Description
EOR                  STRING(2)
           END

R99_Part   GROUP,OVER(CLF:Out_Group),PRE(R99)
RecordType           STRING(2)        ! 00
VersionNumber        STRING(2)        ! 20
BatchName            STRING(10)       ! Batch Creation TimeStamp YYMMDDhhmm
ClientNumber         STRING(13)       ! Panaonic Account Number
NumberRecords        STRING(10)       ! Number of Transmitted data lines (records)
                                      ! including header and trailer lines
EOR                  STRING(2)
           END
! End Change 4502 BE(06/09/2004)


mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL
parts_temp         REAL
labour_temp        REAL
! V2 Params   Start Change 4052 BE(07/09/2004)
V2Params    GROUP,PRE(V2)
VersionNumber   STRING(2)
BatchName       STRING(10)
ClientNumber    STRING(13)
RecordCount     LONG
            END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Panasonic_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:JOBSTAGE.Open
   Relate:SUBTRACC.Open
   Relate:CHARTYPE.Open
   Relate:DISCOUNT.Open
   Relate:VATCODE.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:JOBNOTES.Open
   Relate:STOCK.Open
   Relate:DEFAULTS.Open
   Relate:JOBSE.Open
   Relate:MANFAULT.Open
!**Variable
    ! Start Change 4502 BE(07/09/2004)
    SET(Defaults)
    access:defaults.next()
    ! End Change 4502 BE(07/09/2004)

    tmp:ManufacturerName = 'PANASONIC'
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            ! Start Change 4502 BE(06/09/2004)
            EdiVersion = man:SiemensNewEDI
            ! End Change 4502 BE(06/09/2004)

            Error# = 0
!**Variable
            IF f_batch_number = 0
                ! Start Change 4502 BE(06/09/2004)
                !filename = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.DAT')
                IF (EdiVersion = 2) THEN
                    filename = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.TXT')
                ELSE
                    filename = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.DAT')
                END
                ! End Change 4502 BE(06/09/2004)
            ELSE
                ! Start Change 4502 BE(06/09/2004)
                !filename = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.DAT')
                IF (EdiVersion = 2) THEN
                    filename = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.TXT')
                ELSE
                    filename = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.DAT')
                END
                ! End Change 4502 BE(06/09/2004)
            END

            IF (EdiVersion = 2) THEN
                OPEN(ClaimFile)                           ! Open the output file
                IF ERRORCODE()                           ! If error
                    CREATE(ClaimFile)                       ! create a new file
                    If Errorcode()
                        Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!If Errorcode() 
                    OPEN(ClaimFile)        ! If still error then stop
                ELSE
                    OPEN(ClaimFile)
                    EMPTY(ClaimFile)
                END
            ELSE
                OPEN(Out_File)                           ! Open the output file
                IF ERRORCODE()                           ! If error
                    CREATE(Out_File)                       ! create a new file
                    If Errorcode()
                        Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        Error# = 1
                    End!If Errorcode() 
                    OPEN(out_file)        ! If still error then stop
                ELSE
                    OPEN(out_file)
                    EMPTY(Out_file)
                END
            END
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                ! Start Change 4502 BE(06/09/2004)
                IF (EdiVersion = 2) THEN
                    DO V2Header
                END
                ! End Change 4502 BE(06/09/2004)

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> ''

                        ! Start Change 4502 BE(06/09/2004)
                        !Do Export
                        IF (EdiVersion = 2) THEN
                            DO V2Export
                        ELSE
                            Do Export
                        END
                        ! End Change 4502 BE(06/09/2004)

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        pos = Position(job:EDI_Key)
                        !Found
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    !Start - Moved "close file" code to here. Should be before messages and need the batch number - TrkBs: 5228 (DBH: 18-01-2005)
                    ! Start Change 4502 BE(06/09/2004)
                    !CLOSE(out_file)
                    IF (EdiVersion = 2) THEN
                        DO V2Trailer
                        CLOSE(ClaimFile)
                    ELSE
                        CLOSE(out_file)
                    END
                    ! End Change 4502 BE(06/09/2004)
                    !End   - Moved "close file" code to here. Should be before messages and need the batch number - TrkBs: 5228 (DBH: 18-01-2005)

                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign

                        !Start - Rename the file to it's new format - TrkBs: 5228 (DBH: 18-01-2005)
                        tmp:NewFileName = Clip(man:EDI_Path) & '\IN_' & Format(Day(TOday()),@n02) & Format(Month(Today()),@n02) & Format(Sub(Year(Today()),-2,2),@n02) & Format(Sub(ebt:Batch_Number,-2,2),@n02) & '.txt'
                        Remove(Clip(tmp:NewFilename))
                        Rename(Clip(Filename),Clip(tmp:NewFilename))
                        !End   - Rename the file to it's new format - TrkBs: 5228 (DBH: 18-01-2005)

                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(tmp:NewFileName) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        ! Start Change 4502 BE(06/09/2004)
                        !Do Export
                        IF (EdiVersion = 2) THEN
                            DO V2Export
                        ELSE
                            Do Export
                        END
                        ! End Change 4502 BE(06/09/2004)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    !Start - Moved "close file" code to there. Makes more sense to have it before the messages - TrkBs: 5228 (DBH: 18-01-2005)
                    ! Start Change 4502 BE(06/09/2004)
                    !CLOSE(out_file)
                    IF (EdiVersion = 2) THEN
                        DO V2Trailer
                        CLOSE(ClaimFile)
                    ELSE
                        CLOSE(out_file)
                    END
                    ! End Change 4502 BE(06/09/2004)
                    !End   - Moved "close file" code to there. Makes more sense to have it before the messages - TrkBs: 5228 (DBH: 18-01-2005)

                    ! Start Change 2806 BE(22/07/03)

                    !Start - Rename the file to it's new format - TrkBs: 5228 (DBH: 18-01-2005)
                    tmp:NewFileName = Clip(man:EDI_Path) & '\IN_' & Format(Day(TOday()),@n02) & Format(Month(Today()),@n02) & Format(Sub(Year(Today()),-2,2),@n02) & Format(Sub(f_Batch_Number,-2,2),@n02) & '.txt'
                    Remove(Clip(tmp:NewFilename))
                    Rename(Clip(Filename),Clip(tmp:NewFilename))
                    !End   - Rename the file to it's new format - TrkBs: 5228 (DBH: 18-01-2005)

                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(tmp:NewFileName) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(tmp:NewFileName) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)

                End!If f_batch_number = 0

            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:JOBSTAGE.Close
   Relate:SUBTRACC.Close
   Relate:CHARTYPE.Close
   Relate:DISCOUNT.Close
   Relate:VATCODE.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:JOBNOTES.Close
   Relate:STOCK.Close
   Relate:DEFAULTS.Close
   Relate:JOBSE.Close
   Relate:MANFAULT.Close
Export      Routine
    Clear(Ouf:Record)
    Clear(Mt_Total)
    l1:type    = 'A1'
    l1:date1    = Format(Year(Today()),@n04)
    l1:date3    = Format(month(Today()),@n02)
    l1:date4    = Format(day(Today()),@n02)
    ! Start Change 2290 BE(14/03/03)
    !l1:jobno    = Format(job:ref_number,@n06)
    l1:jobno    = Format(job:ref_number,@n08)
    ! End Change 2290 BE(14/03/03)
    l1:AccountNo    = Format(man:EDI_Account_Number,@s4)
    l1:filler   = Format('',@s48)
    l1:modelno  = Format(job:model_number,@s14)
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        ! Start Change 2852 BE(01/07/03)
        !l1:serialno = Format(job:esn,@s14)
        l1:serialno = Format(job:esn,@s15)
        ! End Change 2852 BE(01/07/03)
    Else !IMEIError# = 1
        ! Start Change 2852 BE(01/07/03)
        !l1:serialno = Format(jot:OriginalIMEI,@s14)
        l1:serialno = Format(jot:OriginalIMEI,@s15)
        ! End Change 2852 BE(01/07/03)
    End !IMEIError# = 1

    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        !Found

    Else! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
    
    ! Start Change 3020 BE(05/08/03)
    !l1:servcode = Format(' ',@s1)
    ! Start Change 3020 BE(05/08/03)
    l1:dop      = Format(job:dop,@d11)
    l1:faultdate = Format(job:date_booked,@d11)
    l1:compdate = Format(job:date_completed,@d11)
    l1:filler2  = Format('',@s124)
    l1:faulttext = Format(Stripcomma(Stripreturn(jbn:fault_description)),@s70)
    l1:repairtext = Format(Stripcomma(Stripreturn(jbn:invoice_Text)),@s70)
    l1:filler3  = Format('',@s10)
    ! Start Change 1673 BE(08/08/03)
    !l1:condcode = Format(job:fault_code1,@s1)
    !l1:symptom1 = Format(job:fault_code2,@s1)
    !l1:symptom2 = Format(job:fault_code3,@s1)
    !l1:extencode = Format(job:fault_code4,@s1)
    IF (man:SiemensNewEdi = 1) THEN
        tempfc" =  Format(job:fault_code2,@s3)
        l1:condcode = Format(job:fault_code1,@s1)
        l1:symptom1 = tempfc"[1 : 1]
        l1:symptom2 = tempfc"[2 : 2]
        l1:extencode = tempfc"[3 : 3]
    ELSE
        l1:condcode = Format(job:fault_code1,@s1)
        l1:symptom1 = Format(job:fault_code2,@s1)
        l1:symptom2 = Format(job:fault_code3,@s1)
        l1:extencode = Format(job:fault_code4,@s1)
    END
    ! End Change 1673 BE(08/08/03)
    l1:filler4  = Format('',@s26)
    l1:cust_name    =  Format(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),@s30) !Cust_Name
    l1:cust_add1    =  Format(job:address_line1,@s30)                           !Cust_Add1
    l1:cust_add2    =  Format(job:address_line2,@s30)                           !Cust_Add2
    l1:cust_town    =  Format(job:address_line3,@s30)                           !Cust_Add3
    l1:cust_cnty    =  Format('',@s20)                         !Cust_Cnty
    l1:cust_pcod    =  Format(job:postcode,@s10)                                !Cust_PCod
    l1:cust_tel    =  Format(job:telephone_number,@s15)                        !Cust_Tel
    l1:DealerName   = Format('',@s30)
    l1:Filler5      = Format('',@s60)
    l1:DealerCity   = Format('',@s20)
    l1:Filler6      = Format('',@s54)
    l1:Fillerx      = 'X'
    Add(out_file)

    Clear(ouf:Record)

    ! Start Change 2421 BE(13/05/03)
    count_parts# = 0
    count_adjust# = 0
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    SET(wpr:part_number_key,wpr:part_number_key)
    LOOP
        IF (access:warparts.next() <> Level:Benign) THEN
           BREAK
        END
        IF  (wpr:ref_number  <> job:ref_number) THEN      |
            BREAK
        END
        IF (man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT') THEN
            CYCLE
        END
        !Exclude part from EDI?
        IF (ExcludeFromEDI(wpr:Part_Ref_Number)) THEN
            CYCLE
        END
        count_parts# += 1
        IF (wpr:part_number = 'ADJUSTMENT') THEN
            count_adjust# += 1
        END
    END

    IF ((count_parts# = 0) OR ((count_parts# = 1) AND (count_adjust# = 1))) THEN
        l2:type1        = 'A2'
        l2:jobno1       =  Format(job:ref_number, @n08)
        l2:part_no      =  FORMAT('No Parts', @s14)
        l2:qty          =  Format(1, @n06)
        l2:filler1      =  Format('', @s14)
        l2:sect_code    =  Format('', @s3)
        l2:defect       =  Format('', @s1)
        l2:repair       =  Format('', @s1)
        l2:PanaInvoice  =  Format('', @s9)
        l2:Filler2      =  Format('', @s32)
        l2:Fillery      =  'Y'
        Add(out_file)
    ELSE
    !save_wpr_id = access:warparts.savefile()
    ! End Change 2421 BE(13/05/03)

        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
                break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                Cycle
            End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            !Exclude part from EDI?
            If ExcludeFromEDI(wpr:Part_Ref_Number)
                Cycle
            End !If ExcludeFromEDI(wpr:Part_Ref_Number)

            l2:type1    = 'A2'
            ! Start Change 2290 BE(14/03/03)
            !l2:jobno1    = Format(job:ref_number,@n06)
            l2:jobno1    = Format(job:ref_number,@n08)
            ! End Change 2290 BE(14/03/03)
            l2:part_no    =  Format(wpr:part_number,@s14)
            l2:qty    =  Format(wpr:quantity,@n06)
            l2:filler1  = Format('',@s14)
            l2:sect_code    =  Format(wpr:Fault_Code2,@s3)
            l2:defect    =  Format(wpr:Fault_Code3,@s1)
            l2:repair    =  Format(wpr:Fault_Code4,@s1)
            l2:PanaInvoice  = Format('',@s9)
            l2:Filler2      = Format('',@s32)
            l2:Fillery      = 'Y'
            Add(out_file)
        end !loop

    ! Start Change 2421 BE(13/05/03)
    END
    ! End Change 2421 BE(13/05/03)

    access:warparts.restorefile(save_wpr_id)
    count += 1
    Clear(Ouf:Record)

V2Header      Routine          ! Start Change 4052 BE(06/09/2004)
    CLEAR(V2Params)
    V2:VersionNumber = '20'
    V2:BatchName     = FORMAT(TODAY(), @d011) & FORMAT(CLOCK(), @t02)  ! Batch Creation TimeStamp YYMMDDhhmm
    V2:ClientNumber  = man:EDI_Account_Number                          ! Panasonic Account Number

    Clear(CLF:Record)
    R00:RecordType           = '00'
    R00:VersionNumber        = V2:VersionNumber
    R00:BatchName            = V2:BatchName
    R00:ClientNumber         = V2:ClientNumber

    R00:BatchCreationDate    = FORMAT(TODAY(), @d012)        ! Batch Creation Date  YYYYMMDD
    R00:ClaimAddresssing     = def:User_Name                 ! Company Name in Licence Address
    R00:BatchDocumentType    = 'CLA'
    R00:BatchDirection       = 'C'

    !Add(out_file)
    R00:EOR = '<13,10>'
    Add(ClaimFile, SIZE(R00_Part))
    V2:RecordCount += 1
V2Export      Routine          ! Start Change 4052 BE(06/09/2004)

    Clear(CLF:Record)
    R01:RecordType           = '01'
    R01:VersionNumber        = V2:VersionNumber
    R01:BatchName            = V2:BatchName
    R01:ClientNumber         = V2:ClientNumber

    R01:WorkSheetNumber      = FORMAT(job:ref_number, @s15)

    R01:ActivityClaimedFor       = job:Fault_Code5        ! Job Fault Code ??
    R01:GuaranteeCode            = job:Fault_Code6        ! Job Fault Code ??

    access:jobse.clearkey(jobe:RefNumberKey)
    jobe:RefNumber = job:ref_number
    IF (access:jobse.fetch(jobe:RefNumberKey) = Level:Benign) THEN
        !Use booking date if no workshop date - TrkBs: 5180 (DBH: 23-12-2004)
        If jobe:InWorkshopDate = ''
            R01:DateRepairReceived      = FORMAT(job:Date_Booked, @d012)        ! InWorkshop Date
        Else ! If jobe:InWorkshopDate = ''
            R01:DateRepairReceived      = FORMAT(jobe:InWorkshopDate, @d012)        ! InWorkshop Date
        End ! If jobe:InWorkshopDate = ''
        !End   - Use booking date if no workshop date - TrkBs: 5180 (DBH: 23-12-2004)

        !Only Intec use the "Repair Completed" date, so use Date Completed instead - TrkBs: 4980 (DBH: 10-12-2004)
        If jobe:CompleteRepairDate = ''
            R01:DateRepairDespatched    = FORMAT(job:Date_Completed, @d012)    ! Repair Completed Date
        Else ! If jobe:CompleteRepairDate = ''
            R01:DateRepairDespatched    = FORMAT(jobe:CompleteRepairDate, @d012)    ! Repair Completed Date
        End ! If jobe:CompleteRepairDate = ''
        !End   - Only Intec use the "Repair Completed" date, so use Date Completed instead - TrkBs: 4980 (DBH: 10-12-2004)
        
    END

    R01:Workshop                = 'W'

    !Start - If fault code 8 is set-up for Panasonic, then use job fault code 8.  - TrkBs: 5357 (DBH: 11-02-2005)
    Access:MANFAULT.ClearKey(maf:Field_Number_Key)
    maf:Manufacturer = job:Manufacturer
    maf:Field_Number = 8
    If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
        !Found
        R01:StockIndicator          = job:Fault_Code8
    Else !If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
        !Error
        !No fault code set-up, default to "D" - TrkBs: 5357 (DBH: 11-02-2005)
        R01:StockIndicator          = 'D'
    End !If Access:MANFAULT.TryFetch(maf:Field_Number_Key) = Level:Benign
    !End   - If fault code 8 is set-up for Panasonic, then use job fault code 8.  - TrkBs: 5357 (DBH: 11-02-2005)

    R01:ConsumerName            = job:Company_Name             ! Customer Address - Company Name
    R01:ConsumerAddress1        = job:Address_Line1            ! Customer Address - Address Line 1
    R01:ConsumerAddress2        = job:Address_Line2            ! Customer Address - Address Line 2
    R01:ConsumerPhoneNumber     = job:Telephone_Number         ! Customer Address - Telephone Number
    R01:ConsumerPostCode        = job:Postcode                 ! Customer Address - Postcode
    R01:ConsumerCity            = job:Address_Line3            ! Customer Address - Address Line 3
    !Start - Authorisation now required - TrkBs: 5316 (DBH: 31-01-2005)
    R01:AuthorisationNumber     = job:Fault_Code7
    !End   - Authorisation now required - TrkBs: 5316 (DBH: 31-01-2005)

    IF (job:Third_Party_Site <> '') THEN
        Access:JOBTHIRD.clearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        IF (Access:JOBTHIRD.fetch(jot:RefNumberKey) = Level:Benign) THEN
            R01:SerialNumber      = jot:OriginalIMEI           ! IMEI
        ELSE
            R01:SerialNumber      = job:ESN                    ! IMEI
        END
    ELSE
        R01:SerialNumber      = job:ESN                        ! IMEI
    END

    ! Show the Incoming IMEI. This if changed if RF Swap - TrkBs: 6457 (DBH: 29-09-2005)
    If jobe:Pre_RF_Board_IMEI <> ''
        R01:SerialNumber = jobe:Pre_RF_Board_IMEI
    End ! If jobe:Pre_RF_Board_IMEI <> ''

    R01:ModelNumber             = job:Model_Number             ! Model No
    R01:PurchaseDate            = FORMAT(job:Dop, @d012)       ! DoP
    !The delear fields are not required - TrkBs: 4980 (DBH: 07-12-2004)
    R01:DealerName              = '' !def:User_Name                ! Service Base Licence Address - Company Name
    R01:DealerAddress1          = '' !def:Address_Line1            ! Service Base Licence Address - Address Line 1
    R01:DealerAddress2          = '' !def:Address_line2            ! Service Base Licence Address - Address Line 2
    R01:DealerPostcode          = '' !def:Postcode                 ! Service Base Licence Address - Postcode
    R01:DealerCity              = '' !def:Address_Line3            ! Service Base Licence Address - AddressLine 3
    R01:DealerVATNumber         = '' !def:VAT_Number               ! ServiceBase VAT No (Licence Address Screen)
    R01:DealerCountryCode       = '' !'GBR'
    R01:Currency                = 'GBP'
    R01:ConsumerDataConsent     = 'U'

    !Add(out_file)
    R01:EOR = '<13,10>'
    Add(ClaimFile, SIZE(R01_Part))
    V2:RecordCount += 1

    seq# = 0
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    SET(wpr:part_number_key,wpr:part_number_key)
    LOOP
        IF ((access:warparts.next() <> Level:Benign) OR |
            (wpr:ref_number  <> job:ref_number)) THEN
            BREAK
        END
        IF (man:includeadjustment <> 'YES' AND wpr:part_number = 'ADJUSTMENT') THEN
            CYCLE
        END
        IF (ExcludeFromEDI(wpr:Part_Ref_Number))
            CYCLE
        END

        seq# += 1

        Clear(CLF:Record)
        R02:RecordType            = '02'
        R02:VersionNumber         = V2:VersionNumber
        R02:BatchName             = V2:BatchName
        R02:ClientNumber          = V2:ClientNumber

        R02:WorkSheetNumber       = FORMAT(job:ref_number, @s15)

        R02:ConditionCode         = job:Fault_Code1                  ! Job Fault Code 1
        ! Start Change 4935 BE(3/11/2004)
        !R02:SymptomCode           = job:Fault_Code2                  ! Job Fault Code 2
        R02:SymptomCode           = job:Fault_Code2[1:1] & job:Fault_Code3[1:1] & job:Fault_Code4[1:1]
        ! End Change 4935 BE(3/11/2004)
        R02:Flag                  = wpr:Fault_Code1                  ! Part Fault Code 1
        R02:DefectCode            = wpr:Fault_Code3                  ! Part Fault Code 3
        R02:RepairCode            = wpr:Fault_Code4                  ! Part Fault Code 4
        R02:SectionCode           = wpr:Fault_Code2                  ! Part Fault Code 2

        access:stock.clearkey(sto:Ref_Number_Key)
        sto:Ref_Number = wpr:part_Ref_Number
        IF (access:stock.fetch(sto:Ref_Number_Key) = Level:Benign) THEN
            IF (sto:Sundry_Item <> 'YES') THEN
                R02:PartOrderNumber   = wpr:Part_Number          ! Part Number unless sundry item (eg software upgrade)
            END
        ELSE
            R02:PartOrderNumber       = wpr:Part_Number
        END

        R02:QuantityParts         = FORMAT(wpr:Quantity, @n02)       ! Part Quantity
        R02:Currency              = 'GBP'
        R02:MessageSequenceNumber = FORMAT(seq#, @n03)               ! Line No per part on job (eg 001, 002 , 003)

        !Add(out_file)
        R02:EOR = '<13,10>'
        Add(ClaimFile, SIZE(R02_Part))
        V2:RecordCount += 1

    END

    access:jobnotes.clearkey(jbn:RefNumberKey)
    jbn:RefNumber = job:ref_number
    IF ((access:jobnotes.fetch(jbn:RefNumberKey) = Level:Benign) AND |
        (jbn:Fault_Description <> '')) THEN
        Clear(CLF:Record)
        R05:RecordType           = '05'
        R05:VersionNumber        = V2:VersionNumber
        R05:BatchName            = V2:BatchName
        R05:ClientNumber         = V2:ClientNumber

        R05:WorkSheetNumber      = FORMAT(job:ref_number, @s15)

        R05:MessageCode          = 'M'
        R05:MemoField            = jbn:Fault_Description       ! ServiceBase Fault Description
        len# = LEN(CLIP(R05:MemoField))
        LOOP ix# = 1 TO len#
            IF (NOT ISALPHA(R05:MemoField[ix# : ix#]) AND NOT NUMERIC(R05:MemoField[ix# : ix#])) THEN
                R05:MemoField[ix# : ix#] = ' '
            END
        END

        !Add(out_file)
        R05:EOR = '<13,10>'
        Add(ClaimFile, SIZE(R05_Part))
        V2:RecordCount += 1
    END
V2Trailer      Routine          ! Start Change 4052 BE(06/09/2004)

    Clear(CLF:Record)
    R99:RecordType           = '99'
    R99:VersionNumber        = V2:VersionNumber
    R99:BatchName            = V2:BatchName
    R99:ClientNumber         = V2:ClientNumber

    V2:RecordCount += 1
    R99:NumberRecords        = FORMAT(V2:RecordCount, @n010)

    !Add(out_file)
    R99:EOR = '<13,10>'
    Add(ClaimFile, SIZE(R99_Part))

getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Panasonic_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('EdiVersion',EdiVersion,'Panasonic_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Panasonic_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Panasonic_EDI',1)
    SolaceViewVars('save_job_id',save_job_id,'Panasonic_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Panasonic_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Panasonic_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Panasonic_EDI',1)
    SolaceViewVars('pos',pos,'Panasonic_EDI',1)
    SolaceViewVars('tmp:NewFileName',tmp:NewFileName,'Panasonic_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
