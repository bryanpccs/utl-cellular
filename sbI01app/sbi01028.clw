

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBI01028.INC'),ONCE        !Local module procedure declarations
                     END


CRC_Nokia_EDI_XML_Export PROCEDURE (func:BatchNumber,func:Accessory) !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
save_wpr_id          USHORT,AUTO
save_aud_id          USHORT,AUTO
Parameter_Group      GROUP,PRE(param)
StartRecordNumber    LONG
ENDRecordNumber      LONG
MessageID            STRING(30)
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(100)
EmailTo              STRING(100)
MessageNumber        LONG
RecordNumber         LONG
StartDate            DATE
EndDate              DATE
                     END
Local_Group          GROUP,PRE(LOC)
Account_Name         STRING(100)
AccountNumber        STRING(15)
ApplicationName      STRING('ServiceBase 2000')
ASCNumber            STRING('999_100880')
CommentText          STRING(100)
DesktopPath          STRING(255)
EmailSubject         STRING(100)
FileName             STRING(255)
HTMLFilename         STRING(255)
InitialRecordNumber  LONG
KeepTrackOfRecordNumber LONG
LastRecordNumber     LONG
MaxHTMLSize          LONG
Path                 STRING(255)
ProgramName          STRING(100)
SectionName          STRING(100)
Text                 STRING(255)
UserCode             STRING(3)
UserName             STRING(100)
                     END
XML_Group            GROUP,PRE(xml)
Filename             STRING(256)
TotalCharacters      LONG
Depth                LONG(0)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED !OLE Automation holder
excel:ColumnName     STRING(100)
excel:ColumnWidth    REAL
excel:DateFormat     STRING('dd mmm yyyy')
excel:OperatingSystem REAL
Excel:Visible        BYTE(0)
                     END
Misc_Group           GROUP,PRE()
AccountValue         STRING(15)
AccountTag           STRING(1)
AccountCount         LONG
AccountChange        BYTE
OPTION1              SHORT
RecordCount          LONG
Result               BYTE
Save_Job_ID          ULONG,AUTO
SAVEPATH             STRING(255)
TotalCharacters      LONG
                     END
Progress_Group       GROUP,PRE()
progress:Text        STRING(100)
progress:NextCancelCheck TIME
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('E')
DataLastCol          STRING('S')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
Clipboard_Group      GROUP,PRE(clip)
OriginalValue        STRING(255)
Saved                BYTE
Value                CSTRING(2048)
                     END
HTML_Queue           QUEUE,PRE(html)
LINE                 STRING(1024)
                     END
Email_Group          GROUP,PRE()
EmailServer          STRING(80)
EmailPort            USHORT
EmailFrom            STRING(255)
EmailTo              STRING(1024)
EmailSubject         STRING(255)
EmailCC              STRING(1024)
EmailBCC             STRING(1024)
EmailFileList        STRING(1024)
EmailMessageText     STRING(16384)
EmailMessageHTML     STRING(1048576)
                     END
XML_Queue            QUEUE,PRE(xml)
LINE                 STRING(2048)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
WarrantyPart_Queue   QUEUE,PRE(wprQ)
PartNumber           LIKE(wpr:Part_Number)
RepairSymptomCode    LIKE(wpr:Fault_Code4)
FaultCode            LIKE(wpr:Fault_Code3)
KeyRepair            LIKE(wpr:Fault_Code5)
CCTReferenceNumber   LIKE(wpr:Fault_Code1)
RepairModule         LIKE(wpr:Fault_Code2)
PartReplaced         LIKE(wpr:Fault_Code6)
Cost                 REAL
Qty                  LONG
FaultCode6           STRING(30)
                     END
ReceiveItem_Queue    QUEUE,PRE(recQ)
ReceiveItem          STRING(2)
                     END
XMLElement_Queue     QUEUE,PRE(elementQ)
Code                 STRING(4)
Value                STRING(100)
                     END
pos                  STRING(255)
tmp:ReturnedBatchNumber LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
window               WINDOW('Process Claims Before Date'),AT(,,209,98),FONT('Arial',8,,),CENTER,ICON('PC.ICO'),CENTERED,GRAY,DOUBLE,IMM
                       SHEET,AT(3,2,201,66),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Criteria'),USE(?Tab1)
                           PROMPT('Only Jobs completed Up To the selected date will be Claimed.'),AT(8,8,196,20),USE(?Prompt1),FONT(,,COLOR:Navy,FONT:bold)
                           STRING('Date'),AT(8,36,76,8),USE(?String2:2)
                           ENTRY(@D8),AT(92,36,64,10),USE(param:EndDate),IMM,FONT(,,,FONT:bold),TIP('Enter the latest Job Completed Date'),REQ
                           BUTTON('...'),AT(160,36,10,10),USE(?PopCalendar:2),IMM,FLAT,LEFT,ICON('CALENDA2.ICO')
                           CHECK('Show Excel'),AT(148,52,52,12),USE(Excel:Visible),HIDE,LEFT,FONT(,,,FONT:bold),TIP('Tick To Show Excel During Report Creation.<13,10>Only Tick if you are experiencing pr' &|
   'oblems <13,10>with Excel reports.'),VALUE('1','0')
                         END
                       END
                       PANEL,AT(4,72,200,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(92,76,56,16),USE(?OK),FLAT,LEFT,TIP('Click to create the data file.'),ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(144,76,56,16),USE(?Cancel),FLAT,LEFT,TIP('Press to close the criteria form.'),ICON('cancel.gif')
                     END

JobsExport     File,Driver('BASIC','/COMMA=9 /ALWAYSQUOTE=OFF'),Pre(jobexp),Name(csv:Filename),Create,Bindable,Thread
Record                  Record
HeaderBlock               GROUP !---------------------------------------------------------
ASCNumber                   STRING( 10) ! SC01 MANDATORY "000_100880"               Default "000_100880", repeated for every entry
CountryCode                 STRING(  2) ! SC02 MANDATORY "GB"                       ISO 3166-1
RegionCode                  STRING(  2) ! SC03 MANDATORY "30"                       Format(99)
!PTVersions                 STRING(255) ! SC04 OPTIONAL                             VarChar
!Currency                   STRING(  3) ! SC05 OPTIONAL                             Format(XXX) ISO 4217
                          END   !---------------------------------------------------------
CustomerBlock             GROUP !---------------------------------------------------------
!CustomerType                STRING( 2)  ! CU01 OPTIONAL                            Format10(X)
!BusinessName                STRING(50)  ! CU02 OPTIONAL                            Format(VarChar)
!FirstName                   STRING(50)  ! CU03 OPTIONAL                            Format(VarChar)
!PhoneNumber                 STRING(20)  ! CU04 OPTIONAL                            Format(VarChar)
!FaxNumber                   STRING(20)  ! CU05 OPTIONAL                            Format(VarChar)
!EMailAddress                STRING(50)  ! CU06 OPTIONAL                            Format(VarChar)
!ClubNokiaID                 STRING(10)  ! CU07 OPTIONAL                            Format(VarChar)
                          END   !---------------------------------------------------------
WorkOrderBlock            GROUP !---------------------------------------------------------
!ReturnType                 STRING(  2) ! WO01 OPTIONAL
WorkOrderID                 STRING( 10) ! WO02 MANDATORY (job:Ref_Number)
CustomerSymptomCode         STRING(255) ! WO03 OPTIONAL  (job:FaultCode_1)
CustomerSymptomText         STRING(255) ! WO04 OPTIONAL  (jbn:Fault_Description)
!WorkOrderStatus            STRING(255) ! WO05 OPTIONAL
ServiceCode                 STRING(  2) ! WO06 MANDATORY (job:FaultCode_2)          
!TechnicianName             STRING( 30) ! WO07 OPTIONAL
TechnicianComment           STRING(255) ! WO08 OPTIONAL  BLANK                      USE BLANK NOT(jbn:Engineers_Notes)
!LabourCost                 STRING( 15) ! WO09 OPTIONAL
!OtherCost                  STRING( 15) ! WO10 OPTIONAL
!QuingTime                  STRING(  3) ! WO11 OPTIONAL                             Format 3(9)
!WalkInAccessTime           STRING(  4) ! WO12 OPTIONAL                             Format 4(9)
SenderCountry               STRING(  2) ! WO13 OPTIONAL                             ISO 3166-1 HARDCODED GB/IE
                          END   !---------------------------------------------------------
PhoneItemBlock            GROUP !---------------------------------------------------------
PrimarySerialNumber         STRING( 15) ! PH01 MANDATORY      (job:ESN            ) IMEI
AdditionalSerialNumber      STRING(  8) ! PH02 OPTIONAL       (job:FaultCode_3    )
!SalesModel                 STRING( 10) ! PH03 OPTIONAL
!ProductType                STRING( 10) ! PH04 OPTIONAL
ProductCode                 STRING( 30) ! PH05 OPTIONAL       (job:FaultCode_4    )
ReceiveItems                STRING(  2) ! PH06 OPTIONAL MULTI (                   )
SWVersion                   STRING(  7) ! PH07 OPTIONAL       (job:FaultCode_5    ) "V xx.yy"
NewSWVersion                STRING(  7) ! PH08 OPTIONAL       (job:FaultCode_6    ) "V xx.yy"
HWVersion                   STRING( 10) ! PH09 OPTIONAL       (job:FaultCode_7    ) "####"
!NewHWVersion               STRING( 10) ! PH10 OPTIONAL       (                   ) "####"
ManufacturingDate           STRING(  6) ! PH11 OPTIONAL       (job:FaultCode_8    ) @D "yyyyMM"
!LabelDateCode              STRING(  7) ! PH12 OPTIONAL       (                   ) ""
POPDate                     STRING( 10) ! PH13 OPTIONAL       (job:DOP            ) Proof of purchase date, "DD.MM.YYYY"
PHWarrantyStatus            STRING(  1) ! PH14 OPTIONAL       (job:FaultCode_9    ) "1" Yes in warranty period/No "0"
!WarrantyOverrideReasonCode STRING(  3) ! PH15 OPTIONAL
!WarrantyRefNumber          STRING( 20) ! PH16 OPTIONAL
ReceivingDateTime           STRING( 20) ! PH17 MANDATORY      (job:Date_Booked    ) "dd.mm.yyyy hh:nn" (?job:Time_Booked?)
!ExpectedReturnTime         STRING( 20) ! PH18 OPTIONAL
!ExpectedReturnTimeReasonCode STRING( 3) ! PH19 OPTIONAL
!QuoteStatus                STRING(  3) ! PH20 OPTIONAL
ShippingDateTime            STRING( 20) ! PH21 OPTIONAL       (job:Date_Despatched) "dd.mm.yyyy hh:nn"
SpecialWarrantyRefNumber    STRING( 20) ! PH22 OPTIONAL       (job:FaultCode_10   ) STRING(15))
BlueToothAddress            STRING( 20) ! PH23 OPTIONAL       (job:FaultCode_11   )
BlueToothHW                 STRING( 20) ! PH24 OPTIONAL       (job:FaultCode_12   )
                          END   !---------------------------------------------------------
SWAPBlock                 GROUP !---------------------------------------------------------
SWAPDateTime                STRING(20) ! SW01 MANDATORY        (job:Exchange_Despatched) "dd.mm.yyyy hh:mm"
SWAPReasonCode              STRING( 3) ! SW02 MANDATORY        (                       ) VarChar
NewPrimarySerialNumber      STRING(20) ! SW03 MANDATORY        (xch:ESN                ) VarChar  job:Exchange_Unit_Number
!NewAdditionalSerialNumber  STRING(20) ! SW04 OPTIONAL MULTI
!NewSalesModel              STRING(10) ! SW05 OPTIONAL
!NewProductType             STRING(10) ! SW06 OPTIONAL
!NewProductCode             STRING(16) ! SW07 OPTIONAL
!NewSoftwareVersion         STRING(10) ! SW08 OPTIONAL
!OriginalPhoneRepairAction  STRING( 2) ! SW09 OPTIONAL                                 Code for action on original broken phone
                          END   !---------------------------------------------------------
RepairLineBlock           GROUP !---------------------------------------------------------
RepairSymptomCode           STRING(255) ! RL01 MANDATORY      (wpr:Fault_Code4)
FaultCode                   STRING(255) ! RL02 MANDATORY      (wpr:Fault_Code3)
KeyRepair                   STRING(  1) ! RL03 MANDATORY      (wpr:Fault_Code5)        "1" Yes is a key repair/No "0" Default as "1"
PartNumber                  STRING( 30) ! RL04 OPTIONAL MULTI (wpr:Part_Number)
!FieldServiceBulletin       STRING( 15) ! RL05 OPTIONAL       VarChar
CCTReferenceNumber          STRING( 30) ! RL06 OPTIONAL       (wpr:Fault_Code1)        Format "A999"
RepairModule                STRING( 30) ! RL07 OPTIONAL       (wpr:Fault_Code2)        
PartReplaced                STRING(  1) ! RL08 OPTIONAL       (wpr:Fault_Code6)        "1" Yes in warranty period/No "0"
!PartCost                   STRING( 15) ! RL09 OPTIONAL                                VarChar
!PriceGroup                 STRING(  3) ! RL10 OPTIONAL                                VarChar
!RLWarrantyStatus           STRING(  1) ! RL11 OPTIONAL                                Format "9" Required in Americas/APAC NOT Europe
                          END   !---------------------------------------------------------
                        End
                    End
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
!-----------------------------------------------
    MAP
After   PROCEDURE ( STRING, STRING ), STRING
AppendString    PROCEDURE(STRING, STRING, STRING), STRING
Before                    PROCEDURE ( STRING, STRING ), STRING
CheckJobOK              PROCEDURE(), LONG ! BOOL
CleanString         PROCEDURE( *STRING )
DateToNokiaString PROCEDURE( DATE ), STRING
DateTimeToNokiaString PROCEDURE( DATE, TIME ), STRING
DateToString PROCEDURE(DATE), STRING
EvaluateFunction                PROCEDURE( STRING ), STRING
EvaluateXMLElementQueue          PROCEDURE( STRING, STRING ), STRING
FillWarrantyParts       PROCEDURE( LONG ), LONG ! BOOL
GetAccessoryCode            PROCEDURE( STRING ), STRING
GetAttribute        PROCEDURE( STRING, STRING ), STRING
GetCustomerName PROCEDURE(), STRING
GetExchangeDate             PROCEDURE( LONG ), STRING
GetExpectedShipDate PROCEDURE(), STRING!(2)
GetTimestamp        PROCEDURE( DATE, TIME ), STRING
GetRecordNumber      PROCEDURE(), LONG !

GetTransactionCode PROCEDURE(), STRING
GetXMLElementQueue          PROCEDURE( STRING, STRING ), STRING
GetEmailServer    PROCEDURE(), STRING
GetEmailPort      PROCEDURE(), LONG
GetEmailFrom      PROCEDURE(), STRING
GetEmailTo        PROCEDURE(), STRING
IsMessageHTMLFull   PROCEDURE(), LONG ! BOOL
LoadExchange        PROCEDURE( LONG ), LONG ! BOOL
LoadJOBACC              PROCEDURE( LONG, LONG ), LONG ! BOOL
LoadJOBNOTES      PROCEDURE( LONG ), LONG ! BOOL
LoadJOBSE      PROCEDURE( LONG ), LONG ! BOOL
!LoadWEBJOB      PROCEDURE( LONG ), LONG ! BOOL
LoadWARPARTS            PROCEDURE( LONG, LONG ), LONG ! BOOL
NumberToBool    PROCEDURE(LONG), STRING
SectionToXMLElementQueue       PROCEDURE(STRING, STRING)
SetEmailServer    PROCEDURE()! 
SetEmailPort      PROCEDURE()! 
SetEmailFrom      PROCEDURE()! 
SetEmailTo        PROCEDURE()! 
StringToBool            PROCEDURE( STRING ), LONG ! BOOL
SWAPDateTime        PROCEDURE(), STRING
WriteDebug      PROCEDURE( STRING )
!-----------------------------------------------
WriteTABLE PROCEDURE(LONG)
WriteTD PROCEDURE(STRING)
WriteTH PROCEDURE(STRING)
WriteTR PROCEDURE(LONG)
!-----------------------------------------------
WriteXMLTag PROCEDURE( STRING, STRING, STRING )
WriteStartXMLTag PROCEDURE( STRING, STRING )
WriteEndXMLTag PROCEDURE( STRING )
WriteXMLTagFromXMLElementQueue PROCEDURE(STRING, STRING)
XMLEncode               PROCEDURE( STRING ), STRING
!-----------------------------------------------
    END ! MAP
!-----------------------------------------------
! Logfile Declarations
LOGFILE               FILE,DRIVER('ASCII'),OEM,PRE(log),CREATE,BINDABLE,THREAD
Record                   RECORD,PRE()
recbuff                     STRING(256)
                         END
                     END

               MAP
OpenLog                  PROCEDURE(STRING f, BYTE newflag=0),BYTE
CloseLog                 PROCEDURE()
WriteLog                 PROCEDURE(STRING s, BYTE logflag=0)
              END
! ProgressWindow Declarations
 
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte
 
progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY,DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
 
! Excel EQUATES

!----------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic  EQUATE(0FFFFEFF7h) ! Constants.
xlSolid      EQUATE(        1 ) ! Constants.
xlLeft       EQUATE(0FFFFEFDDh) ! Constants.
xlRight      EQUATE(0FFFFEFC8h) ! Constants.
xlLastCell   EQUATE(       11 ) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!----------------------------------------------------
! Office EQUATES

!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:ReturnedBatchNumber)

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?String2:2{prop:FontColor} = -1
    ?String2:2{prop:Color} = 15066597
    If ?param:EndDate{prop:ReadOnly} = True
        ?param:EndDate{prop:FontColor} = 65793
        ?param:EndDate{prop:Color} = 15066597
    Elsif ?param:EndDate{prop:Req} = True
        ?param:EndDate{prop:FontColor} = 65793
        ?param:EndDate{prop:Color} = 8454143
    Else ! If ?param:EndDate{prop:Req} = True
        ?param:EndDate{prop:FontColor} = 65793
        ?param:EndDate{prop:Color} = 16777215
    End ! If ?param:EndDate{prop:Req} = True
    ?param:EndDate{prop:Trn} = 0
    ?param:EndDate{prop:FontStyle} = font:Bold
    ?Excel:Visible{prop:Font,3} = -1
    ?Excel:Visible{prop:Color} = 15066597
    ?Excel:Visible{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!-----------------------------------------------
OKButtonPressed                     ROUTINE
    DATA
JobFound    LONG(False)
    CODE
        !------------------------------------------------------------------
        !debug:Active = True

        !WriteDebug('OKButtonPressed(START)')
        !------------------------------------------------------------------
        If FindManufacturer('NOKIA') = Level:Fatal
            EXIT
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        DO CheckINIFile

        SectionToXMLElementQueue('SERVICECENTRE', 'SC')
        SectionToXMLElementQueue(     'CUSTOMER', 'CU')
        SectionToXMLElementQueue(    'WORKORDER', 'WO')
        SectionToXMLElementQueue(        'PHONE', 'PH')
        SectionToXMLElementQueue(         'SWAP', 'SW')
        SectionToXMLElementQueue(   'REPAIRLINE', 'RL')

        !DO PrintXMLElementQueue
        !------------------------------------------------------------------
        LOC:ASCNumber = GetXMLElementQueue('SC01', '999_100880')

        LOC:Filename = ''
        DO LoadFileDialog

        IF CLIP(LOC:Filename) = ''
            EXIT
        END!If
        !------------------------------------------------------------------
        !Found
        Remove(JobsExport)

        Open(JobsExport)
        If Error()
            Create(JobsExport)
            Open(JobsExport)
            If Error()
                Case MessageEx('Cannot create export file.', LOC:ApplicationName,|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx

                EXIT
            End!If Error()

        End!If Error()

        ! Start Change 4536 BE(22/07/2004)
        IF NOT OpenLog(xml:Filename, true) THEN
            MessageEx('Cannot create xml file.', LOC:ApplicationName,|
                      'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                      beep:systemhand,msgex:samewidths,84,26,0)
            EXIT
        END
        ! End Change 4536 BE(22/07/2004)
        !------------------------------------------------------------------
        recordspercycle         = 25
        recordsprocessed        = 0
        percentprogress         = 0
        progress:thermometer    = 0
        Progress:HighWaterMark  = 10

        !Start - To hopefully create an accurate progress bar. - TrkBs: 5096 (DBH: 06-12-2004)
        If func:BatchNumber = 0
            recordstoprocess        = CountEDIRecords('NOKA','NO',0,param:EndDate)
        Else ! If func:BatchNumber = 0
            recordstoprocess        = CountEDIRecords('NOKIA','YES',func:BatchNumber,0)
        End ! If func:BatchNumber = 0
        !End   - To hopefully create an accurate progress bar. - TrkBs: 5096 (DBH: 06-12-2004)
        

        thiswindow.reset(1)
        open(progresswindow)

        ?progress:userstring{prop:text} = 'Exporting Completed Jobs...'
        ?progress:pcttext{prop:text}    = '0% Completed'
        !------------------------------------------------------------------
        !---Before Routine

        CompCount# = 0
        !------------------------------------------------------------------
        ! Ref_Number_Key    KEY( job:Ref_Number     ),NOCASE,PRIMARY
        ! DateCompletedKey  KEY( job:Date_Completed ),DUP,NOCASE
        ! EDI_Key           KEY( job:Manufacturer, job:EDI, job:EDI_Batch_Number, job:Ref_Number ),DUP,NOCASE
        !

        If func:BatchNumber = 0
            !Is this a new claim?
            Access:JOBS.ClearKey(job:EDI_Key)
                !job:Date_Completed = param:StartDate
                job:Manufacturer     = 'NOKIA'
                job:EDI              = 'NO'
            Set(job:EDI_Key, job:EDI_Key)

        Else !If func:Batch_Number = 0
            !Is this a reprint of an old batch?
            Access:JOBS.ClearKey(job:EDI_Key)
                !job:Date_Completed = param:StartDate
                job:Manufacturer     = 'NOKIA'
                job:EDI              = 'YES'
                job:EDI_Batch_Number = func:BatchNumber
            Set(job:EDI_Key, job:EDI_Key)

        End !If func:Batch_Number = 0

                                    
        LOOP WHILE Access:JOBS.NEXT() = Level:Benign
            !--------------------------------------------------------------
            ?progress:userstring{prop:Text} = 'Done : ' & CompCount# & ' Found: ' & Count#
            Display()

            CompCount# += 1

            Do GetNextRecord2
            Do cancelcheck
            If tmp:cancel = 1
                GOTO Finalize
            End!If tmp:cancel = 1
            !--------------------------------------------------------------
!            IF job:Date_Completed > param:EndDate
!                BREAK
!            END !IF

            IF job:Manufacturer     <> 'NOKIA'
              BREAK
            END

            If func:BatchNumber = 0
                IF job:EDI              <> 'NO'
                  BREAK
                END
                If job:Date_Completed > param:EndDate
                    Cycle
                End !If job:Date_Completed > tmp:ProcessBeforeDate
                If job:Warranty_Job <> 'YES'
                    Cycle
                End !If job:Warranty_Job <> 'YES'
            Else !If func:BatchNumber = 0
                IF job:EDI              <> 'YES'
                  BREAK
                END
                If job:EDI_Batch_Number <> func:BatchNumber
                    Break
                End !If job:EDI_Batch_Number <> func:BatchNumber
            End !If func:BatchNumber = 0
            !--------------------------------------------------------------
            IF NOT CheckJobOK()
                CYCLE
            END !IF
            !==============================================================
            !WriteDebug('LOOP BODY')

            count_parts# = 0
            !save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
    !            If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
    !                Cycle
    !            End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = wpr:part_ref_number
                if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    !Start - Check to see if this is the "accessory" version of the report - TrkBs: 5006 (DBH: 19-01-2005)
                    If func:Accessory = True
                        If sto:accessory <> 'YES'
                            Cycle
                        End!If sto:accessory = 'YES'
                    Else ! If func:Accessory = True
                        If sto:accessory = 'YES'
                            Cycle
                        End!If sto:accessory = 'YES'
                    End ! If func:Accessory = True
                    !End   - Check to see if this is the "accessory" version of the report - TrkBs: 5006 (DBH: 19-01-2005)

                    If sto:ExchangeUnit = 'YES'
                        Cycle
                    End!If sto:ExchangeUnit = 'YES'
                Else
                    !If can't find stock item, then don't know if accessory. - TrkBs: 5006 (DBH: 19-01-2005)
                    If func:Accessory = True
                        Cycle
                    End !If func:Accessory = True
                end
                count_parts# += 1

            end !loop
            !access:warparts.restorefile(save_wpr_id)


            RecordCount += 1
            IF Count_Parts# > 0
                IF JobFound = False
                    JobFound = True

                    DO WriteHeaderXML
                END !IF

                DO WriteWorkOrder
            END
            !--------------------------------------------------------------
            Count#     += 1
            !--------------------------------------------------------------
            If func:BatchNumber = 0
                If job:Ignore_Warranty_Charges <> 'YES'
                    Pricing_Routine('W',labour",parts",pass",a")
                    If pass" = True
                        job:Labour_Cost_Warranty    = Labour"
                        job:Parts_Cost_Warranty     = Parts"
                        job:Sub_Total_Warranty      = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                    End !If pass" = True

                End !If job:Ignore_Warranty_Charges <> 'YES'

                pos = Position(job:EDI_Key)

                job:EDI = 'YES'
                job:EDI_Batch_Number    = Man:Batch_Number
                AddToAudit
                Access:JOBS.Update()
                Reset(job:EDI_Key,pos)
            End !If func:BatchNumber = 0
        End !Loop

        ?progress:userstring{prop:Text} = 'Done : ' & CompCount# & ' Found: ' & Count#
        Display()
        !------------------------------------------------------------------
        !---After Routine
        IF JobFound = True
            DO WriteFooterXML
        END !IF

        ! Start Change 4536 BE(22/07/2004)
        CloseLog()
        ! End Change 4536 BE(22/07/2004)

        !------------------------------------------------------------------
Finalize Setcursor()
        !------------------------------------------------------------------
        Do EndPrintRun

        close(progresswindow)
        Close(JOBSExport)
        !------------------------------------------------------------------
        IF func:BatchNumber = 0
            If Count# <> 0
                If Access:EDIBATCH.PrimeRecord() = Level:Benign
                    ebt:Batch_Number    = man:Batch_Number
                    ebt:Manufacturer    = 'NOKIA'
                    If Access:EDIBATCH.TryInsert() = Level:Benign
                        !Insert Successful
                    Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                        !Insert Failed
                    End !If Access:EDIBATCH.TryInsert() = Level:Benign

                    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                    man:Manufacturer = 'NOKIA'
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        !Found
                        man:Batch_Number += 1
                        Access:MANUFACT.Update()
                    Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                
                Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                  '<13,10>'&|
                  '<13,10>Create EDI Batch File.','ServiceBase 2000',|
                               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            End !If Count# <> 0
            glo:Select1  = ebt:Batch_Number
            glo:select3  = 'NOKIA'
            Nokia_accessories_Claim_Report
            glo:select1 = ''
            glo:Select3 = ''


            !Start - Pass the Batch Number back for the Accessory Version - TrkBs: 5006 (DBH: 19-01-2005)
            tmp:ReturnedBatchNumber = ebt:Batch_Number
            !End   - Pass the Batch Number back for the Accessory Version - TrkBs: 5006 (DBH: 19-01-2005)

        Else !IF func:BatchNumber = 0 And Count# <> 0

            ! Start Change 2806 BE(22/07/03)

            !No need to show the message again for the accessory export - TrkBs: 5006 (DBH: 19-01-2005)
            If func:Accessory = False

                IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                    MessageEx('Created Batch Number ' & func:BatchNumber & '.'&|
                                '<13,10>'&|
                                '<13,10>Create EDI Batch File.','ServiceBase 2000',|
                                'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                beep:systemasterisk,msgex:samewidths,84,26,0)
                ELSE
                    MessageEx('Re-Created Batch Number ' & func:BatchNumber & '.'&|
                                '<13,10>'&|
                                '<13,10>Re-Create EDI Batch File.','ServiceBase 2000',|
                                'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                beep:systemasterisk,msgex:samewidths,84,26,0)
                END
                ! End Change 2806 BE(22/07/03)
            End ! If func:Accessory = False

            !Start - Only call the accessory report after the initial export - TrkBs: 5006 (DBH: 19-01-2005)
            If func:Accessory = False
                glo:Select1  = func:BatchNumber
                glo:select3  = 'NOKIA'
                Nokia_accessories_Claim_Report
                glo:select1 = ''
                glo:Select3 = ''
            End ! If func:Accessory = False
            !End   - Only call the accessory report after the initial export - TrkBs: 5006 (DBH: 19-01-2005)

            !Start - Pass the Batch Number back for the Accessory Version - TrkBs: 5006 (DBH: 19-01-2005)
            tmp:ReturnedBatchNumber = func:BatchNumber
            !End   - Pass the Batch Number back for the Accessory Version - TrkBs: 5006 (DBH: 19-01-2005)

        End !IF func:BatchNumber = 0 And Count# <> 0

        !------------------------------------------------------------------
        !WriteDebug('OKButtonPressed(END)')
        post(event:closewindow)
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
WriteCustomer               ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !CustomerBlock             GROUP !----- OPTIONAL ------- OPTIONAL ------- OPTIONAL --------
        ! CustomerType                STRING( 2)  ! CU01 OPTIONAL                            Format10(X)
        ! BusinessName                STRING(50)  ! CU02 OPTIONAL                            Format(VarChar)
        ! FirstName                   STRING(50)  ! CU03 OPTIONAL                            Format(VarChar)
        ! PhoneNumber                 STRING(20)  ! CU04 OPTIONAL                            Format(VarChar)
        ! FaxNumber                   STRING(20)  ! CU05 OPTIONAL                            Format(VarChar)
        ! EMailAddress                STRING(50)  ! CU06 OPTIONAL                            Format(VarChar)
        ! ClubNokiaID                 STRING(10)  ! CU07 OPTIONAL                            Format(VarChar)
        !                          END   !---------------------------------------------------------
        !------------------------------------------------------------------
        ! CustomerType                STRING( 2)  ! CU01 OPTIONAL                            Format10(X)
        ! EC End Customer                     <-- Like John Smith
        ! CR Carrier/Operator                 <-- Like "AT&T", "SingTel"
        ! DR Dealer/Cellular Service Reseller <-- Like "Radio Shack"
        ! SC Shared ASC Repairer              <-- Like "Accord Express"
        ! IC Internal Customer                <-- When ASC repair their own stock
        ! NC Nokia                            <-- When ASC repair Nokia Consignment Stock
        ! CP Collection Point                 <-- Company
        !------------------------------------------------------------------
        !WriteDebug('WriteCustomer()')

        CLEAR(jobexp:CustomerBlock)

!        IF True = True
!            WriteXMLTag('CUSTOMER', '', '')
!        ELSE
!            WriteStartXMLTag('CUSTOMER', '')
!                jobexp:CustomerType = 'EC'
!                jobexp:BusinessName = job:Company_Name
!                jobexp:FirstName    = job:Initial
!                jobexp:PhoneNumber  = job:Telephone_Number
!                jobexp:FaxNumber    = job:Fax_Number
!                jobexp:EMailAddress = jobe:EndUserEmailAddress
!                jobexp:ClubNokiaID  = ''

                WriteXMLTag('CU01', '', 'EC')
                IF job:Surname = ''
                  WriteXMLTag('CU02', '', job:Company_Name)
                ELSE
                  WriteXMLTag('CU02', '', job:Surname)
                END
                ! Start - Do not include these fields on the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                If ~func:Accessory
                    WriteXMLTag('CU03', '', job:Initial)
                    WriteXMLTag('CU04', '', job:Mobile_Number)
                    WriteXMLTag('CU05', '', job:Fax_Number)
                    WriteXMLTag('CU06', '', jobe:EndUserEmailAddress)
                    WriteXMLTag('CU07', '', '')
                End ! If func:Accessory
                ! End   - Do not include these fields on the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)!            WriteEndXMLTag('CUSTOMER')

!            WriteEndXMLTag('CUSTOMER')
!        END !IF
        !------------------------------------------------------------------
    EXIT
WriteWorkOrder               ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !WorkOrderBlock            GROUP !---------------------------------------------------------
        !   WorkOrderID                 STRING( 10) ! WO02 MANDATORY (job:Ref_Number)
        !   CustomerSymptomCode         STRING(255) ! WO03 OPTIONAL  (job:FaultCode_1)
        !   CustomerSymptomText         STRING(255) ! WO04 OPTIONAL  (jbn:Fault_Description)
        !   ServiceCode                 STRING(  2) ! WO06 MANDATORY (job:FaultCode_2) ###################################################
        !   TechnicianComment           STRING(255) ! WO08 OPTIONAL  USE BLANK NOT(jbn:Engineers_Notes)
        !   SenderCountry               STRING(  2) ! WO13 OPTIONAL  ISO 3166-1 HARDCODED GB/IE
        !                  END
        !------------------------------------------------------------------
        !WriteDebug('WriteWorkOrder()')

        WriteStartXMLTag('CUSTOMER', '')
            DO WriteCustomer

            CLEAR(jobexp:WorkOrderBlock)

            WriteStartXMLTag('WORKORDER', '')
                jobexp:WorkOrderID         = GetXMLElementQueue('WO02', '&job:Ref_Number'       )
                jobexp:CustomerSymptomCode = GetXMLElementQueue('WO03', '&job:Fault_Code1'      )
                jobexp:CustomerSymptomText = GetXMLElementQueue('WO04', '&jbn:Fault_Description')
                jobexp:ServiceCode         = GetXMLElementQueue('WO06', '&job:Fault_Code2'      )
                jobexp:TechnicianComment   = GetXMLElementQueue('WO08', '&jbn:Engineers_Notes'  )
                jobexp:SenderCountry       = GetXMLElementQueue('WO13', '$GB'                   )

                WriteXMLTag('WO01', '', GetXMLElementQueue('WO01', '$SR'       )    )

                ! Start - Append "A" to the job number of the accessory claim - TrkBs: 5887 (DBH: 16-06-2005)
                If ~func:Accessory
                    WriteXMLTag('WO02', '', jobexp:WorkOrderID        )
                Else ! If ~func:Accessory
                    WriteXMLTag('WO02', '', Clip(jobexp:WorkOrderID) & 'A'        )
                End ! If ~func:Accessory
                ! End   - Append "A" to the job number of the accessory claim - TrkBs: 5887 (DBH: 16-06-2005)

                WriteXMLTag('WO03', '', jobexp:CustomerSymptomCode)
                
                If ~func:Accessory
                    ! Do not display in the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                    WriteXMLTag('WO04', '', jobexp:CustomerSymptomText)
                    ! Start Change BE18 BE(07/11/03)
                    !WriteXMLTag('WU05', '', '')
                    WriteXMLTag('WO05', '', '')
                    ! End Change BE18 BE(07/11/03)
                End ! If ~func:Accessory
                
                 !Start - If accessory export, default to "AR", otherwise use Service Code - TrkBs: 5006 (DBH: 19-01-2005)
                If func:Accessory
                    WriteXMLTag('WO06', '', 'AR'       )
                Else ! If func:Accessory
                    WriteXMLTag('WO06', '', jobexp:ServiceCode        )
                End ! If func:Accessory
                !End   - If accessory export, default to "AR", otherwise use Service Code - TrkBs: 5006 (DBH: 19-01-2005)
                
                WriteXMLTag('WO07', '', GetXMLElementQueue('WO07', '&job:Engineer'      )    )

                If ~func:Accessory
                    ! Do not display in the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                    WriteXMLTag('WO08', '', jobexp:TechnicianComment  )
                End ! If ~func:Accessory

                ! Start Change BE18 BE(07/11/03)
                !WriteXMLTag('WU09', '', '')
                !WriteXMLTag('WU10', '', '')
                !WriteXMLTag('WU11', '', '')
                !WriteXMLTag('WU12', '', '')
                WriteXMLTag('WO09', '', '')
                If ~func:Accessory
                    ! Do not display in the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                    WriteXMLTag('WO10', '', '')
                    WriteXMLTag('WO11', '', '')
                    WriteXMLTag('WO12', '', '')
                End ! If ~func:Accessory
                ! End Change BE18 BE(07/11/03)
                WriteXMLTag('WO13', '', jobexp:SenderCountry      )

                DO WritePhoneItem
            WriteEndXMLTag('WORKORDER')
        WriteEndXMLTag('CUSTOMER')
        !------------------------------------------------------------------
        CleanString(jobexp:CustomerSymptomText)
        CleanString(jobexp:TechnicianComment)
        !------------------------------------------------------------------
    EXIT
WritePhoneItem          ROUTINE
    DATA
MyValue LIKE(elementQ:Value)
    CODE
        !------------------------------------------------------------------
        !PhoneItemBlock            GROUP !---------------------------------------------------------
        !    PrimarySerialNumber         STRING( 15) ! PH01 MANDATORY      (job:ESN            ) IMEI
        !    AdditionalSerialNumber      STRING(  8) ! PH02 OPTIONAL       (job:FaultCode_3    )
        !    ProductCode                 STRING( 30) ! PH05 OPTIONAL       (job:Product_Code   )

        !    ReceiveItems                STRING(  2) ! PH06 OPTIONAL MULTI (                   )
        !    SWVersion                   STRING(  7) ! PH07 OPTIONAL       (job:FaultCode_5    ) "V xx.yy"
        !    NewSWVersion                STRING(  7) ! PH08 OPTIONAL       (job:FaultCode_6    ) "V xx.yy" #########################################

        !    HWVersion                   STRING( 10) ! PH09 OPTIONAL       (job:FaultCode_7    ) "####"
        !    ManufacturingDate           STRING(  6) ! PH11 OPTIONAL       (job:FaultCode_8    ) @D "yyyyMM"
        !    POPDate                     STRING( 10) ! PH13 OPTIONAL       (job:DOP            ) Proof of purchase date, "DD.MM.YYYY"

        !    PHWarrantyStatus            STRING(  1) ! PH14 OPTIONAL       (job:FaultCode_9    ) "1" Yes in warranty period/No "0"
        !    ReceivingDateTime           STRING( 20) ! PH17 MANDATORY       (job:Date_Booked    ) "dd.mm.yyyy hh:nn" (?job:Time_Booked?)
        !    ShippingDateTime            STRING( 20) ! PH21 OPTIONAL       (job:Date_Despatched) "dd.mm.yyyy hh:nn"

        !    SpecialWarrantyRefNumber    STRING( 20) ! PH22 OPTIONAL       (job:FaultCode_10   ) STRING(15))
        !    BlueToothAddress            STRING( 20) ! PH23 OPTIONAL       (job:FaultCode_11   )
        !    BlueToothHW                 STRING( 20) ! PH23 OPTIONAL       (job:FaultCode_12   )
        !                          END
        !------------------------------------------------------------------
        !WriteDebug('WritePhoneItem()')

        CLEAR(jobexp:PhoneItemBlock)

        WriteStartXMLTag('PHONE', '')
            !--------------------------------------------------------------
            !    PrimarySerialNumber         STRING( 15) ! PH01 MANDATORY      (job:ESN            ) IMEI
            !    AdditionalSerialNumber      STRING(  8) ! PH02 OPTIONAL       (job:FaultCode_3    )
            !    ProductCode                 STRING( 30) ! PH05 OPTIONAL       (job:FaultCode_4    )

            jobexp:PrimarySerialNumber    = GetXMLElementQueue('PH01', '&job:ESN'        )
            jobexp:AdditionalSerialNumber = GetXMLElementQueue('PH02', '&job:Fault_Code3')
            jobexp:ProductCode            = GetXMLElementQueue('PH05', '&job:ProductCode') !EvaluateXMLElementQueue('PH05', 'job:ProductCode') !

            !Start - If accessory export, blank this field, otherwise it's serial number - TrkBs: 5006 (DBH: 19-01-2005)
            If func:Accessory = True
                WriteXMLTag('PH01', '', '')
            Else ! If func:Accessory
                WriteXMLTag('PH01', '', jobexp:PrimarySerialNumber)
            End ! If func:Accessory
            !End   - If accessory export, blank this field, otherwise it's serial number - TrkBs: 5006 (DBH: 19-01-2005)

            If ~func:Accessory
                ! Do not display in the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                WriteXMLTag('PH02', '', jobexp:AdditionalSerialNumber)
                WriteXMLTag('PH03', '', '')
                WriteXMLTag('PH04', '', '')
            End ! If ~func:Accessory


            !Start - If accessory export, this is Fault Code 1 (same as cust symp code) - TrkBs: 5006 (DBH: 19-01-2005)
            If func:Accessory = True
                !Bit of a bodge, but have to get the first accessory part number - TrkBs: 5006 (DBH: 15-03-2005)
                FoundPart# = False
                Save_wpr_ID = Access:WARPARTS.SaveFile()
                Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number  = job:Ref_Number
                Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                Loop
                    If Access:WARPARTS.NEXT()
                       Break
                    End !If
                    If wpr:Ref_Number  <> job:Ref_Number      |
                        Then Break.  ! End If
                    Access:STOCK.ClearKey(sto:Ref_Number_Key)
                    sto:Ref_Number = wpr:Part_Ref_Number
                    If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        !Found
                        If sto:Accessory = 'YES'
                            WriteXMLTag('PH05', '', wpr:Part_Number)
                            FoundPart# = True
                            Break
                        End ! If sto:Accessory = 'YES'
                    Else !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                        !Error
                        Cycle
                    End !If Access:STOCK.TryFetch(sto:Ref_Number_Key) = Level:Benign
                End !Loop
                Access:WARPARTS.RestoreFile(Save_wpr_ID)

                If FoundPart# = False
                    WriteXMLTag('PH05', '', '')
                End ! If FoundPart# = False
            Else ! If func:Accessory = True
                WriteXMLTag('PH05', '', jobexp:ProductCode)
            End ! If func:Accessory = True
            !End   - If accessory export, this is Fault Code 1 (same as cust symp code) - TrkBs: 5006 (DBH: 19-01-2005)
            
            !--------------------------------------------------------------
            !    ReceiveItems                STRING(  2) ! PH06 OPTIONAL MULTI (                   )
            !    SWVersion                   STRING(  7) ! PH07 OPTIONAL       (job:FaultCode_5    ) "V xx.yy"
            !    NewSWVersion                STRING(  7) ! PH08 OPTIONAL       (job:FaultCode_6    ) "V xx.yy" 

            jobexp:SWVersion    = GetXMLElementQueue('PH07', '&job:Fault_Code5')
            jobexp:NewSWVersion = GetXMLElementQueue('PH08', '&job:Fault_Code6')

            DO WriteReceiveItems ! needs to be duplicated for each csv line
            !WriteXMLTag('PH06', '', '')
            If ~func:Accessory
                ! Do not display in the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                WriteXMLTag('PH07', '', jobexp:SWVersion)
                WriteXMLTag('PH08', '', jobexp:NewSWVersion)
                !--------------------------------------------------------------
                !    HWVersion                   STRING( 10) ! PH09 OPTIONAL       (job:FaultCode_7    ) "####"
                !    ManufacturingDate           STRING(  6) ! PH11 OPTIONAL       (job:FaultCode_8    ) @D "yyyyMM"
                !    POPDate                     STRING( 10) ! PH13 OPTIONAL       (job:DOP            ) Proof of purchase date, "DD.MM.YYYY"

                jobexp:HWVersion         = GetXMLElementQueue('PH09', '&job:Fault_Code7'           )
                jobexp:ManufacturingDate = GetXMLElementQueue('PH11', '&job:Fault_Code8'           )
                

                WriteXMLTag('PH09', '', jobexp:HWVersion)
                WriteXMLTag('PH11', '', jobexp:ManufacturingDate)
                WriteXMLTag('PH12', '', '')
            End ! If ~func:Accessory
            jobexp:POPDate           = GetXMLElementQueue('PH13', '@DateToNokiaString(job:DOP)')
            WriteXMLTag('PH13', '', jobexp:POPDate)
            !--------------------------------------------------------------
            !    PHWarrantyStatus            STRING(  1) ! PH14 OPTIONAL       (job:FaultCode_9    ) "1" Yes in warranty period/No "0"
            !    ReceivingDateTime           STRING( 20) ! PH17 MANDATORY      (job:Date_Booked    ) "dd.mm.yyyy hh:nn" (?job:Time_Booked?)
            !    ShippingDateTime            STRING( 20) ! PH21 OPTIONAL       (job:Date_Despatched) "dd.mm.yyyy hh:nn"

            jobexp:PHWarrantyStatus  = GetXMLElementQueue('PH14', '&job:Fault_Code9')
            !jobexp:PHWarrantyStatus  = GetXMLElementQueue('PH14', '&job:Fault_Code9'                                        )
            jobexp:ReceivingDateTime = GetXMLElementQueue('PH17', '@DateTimeToNokiaString(job:Date_Booked, job:Time_Booked)') ! DateTimeToNokiaString(job:Date_Booked, job:Time_Booked)
            ! Shipping Time to also show Time - TrkBs: 5887 (DBH: 16-06-2005)
            IF job:date_Despatched = ''
              !jobexp:ShippingDateTime  = GetXMLElementQueue('PH21', DateTimeToNokiaString(job:Date_Completed, job:Time_Completed)                  )
              jobexp:ShippingDateTime  = DateTimeToNokiaString(job:Date_Completed, job:Time_Completed)
            ELSE
                ! Time Despatched is not recorded, so search the audit trail for matching despatch entry - TrkBs: 5887 (DBH: 16-06-2005)
                FoundDespatchDate# = False
                Save_aud_ID = Access:AUDIT.SaveFile()
                Access:AUDIT.ClearKey(aud:Ref_Number_Key)
                aud:Ref_Number = job:Ref_Number
                aud:Date       = job:Date_Despatched
                Set(aud:Ref_Number_Key,aud:Ref_Number_Key)
                Loop
                    If Access:AUDIT.NEXT()
                       Break
                    End !If
                    If aud:Ref_Number <> job:Ref_Number       |
                    Or aud:Date       <> job:Date_Despatched      |
                        Then Break.  ! End If
                    If Instring('JOB DESPATCHED',aud:Action,1,1)
                        !jobexp:ShippingDateTime  = GetXMLElementQueue('PH21', DateTimeToNokiaString(job:Date_Despatched,aud:Date))
                        jobexp:ShippingDateTime  = DateTimeToNokiaString(job:Date_Despatched,aud:Time)
                        FoundDespatchDate# = True
                        Break
                    End ! If Instring('JOB DESPATCHED',aud:Action,1,1)
                    ! Sometimes there is no "JOB DESPATCHED" entry in the audit trial - TrkBs: 6371 (DBH: 16-09-2005)
                    If Instring('MANUAL DESPATCH CONFIRMATION',aud:Action,1,1) And aud:Type = 'JOB'
                        jobexp:ShippingDateTime  = DateTimeToNokiaString(job:Date_Despatched,aud:Time)
                        FoundDespatchDate# = True
                        Break
                    End !If Instring('MANUAL DESPATCH CONFIRMATION') And aud:Type = 'JOB'
                End !Loop
                Access:AUDIT.RestoreFile(Save_aud_ID)
                If FoundDespatchDate# = False
                    !jobexp:ShippingDateTime  = GetXMLElementQueue('PH21', DateToNokiaString(job:Date_Despatched)                 )
                    jobexp:ShippingDateTime  = DateToNokiaString(job:Date_Despatched)
                End ! If FoundDespatchDate# = False
                ! End   - Time Despatched is not recorded, so search the audit trail for matching despatch entry - TrkBs: 5887 (DBH: 16-06-2005)
            END

            WriteXMLTag('PH14', '', jobexp:PHWarrantyStatus)
            If ~func:Accessory
                ! Do not display in the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                WriteXMLTag('PH15', '', GetXMLElementQueue('PH15', '&job:Fault_Code12'))
                WriteXMLTag('PH16', '', '')
            End ! If ~func:Accessory
            WriteXMLTag('PH17', '', jobexp:ReceivingDateTime)
            If ~func:Accessory
                ! Do not display in the Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                WriteXMLTag('PH18', '', '')
                WriteXMLTag('PH19', '', '')
                WriteXMLTag('PH20', '', '')
            End ! If ~func:Accessory
            WriteXMLTag('PH21', '', jobexp:ShippingDateTime)
            !--------------------------------------------------------------
            !    SpecialWarrantyRefNumber    STRING( 20) ! PH22 OPTIONAL       (job:FaultCode_10   ) STRING(15))
            !    BlueToothAddress            STRING( 20) ! PH23 OPTIONAL       (job:FaultCode_11   )
            !    BlueToothHW                 STRING( 20) ! PH24 OPTIONAL       (job:FaultCode_12   )

            jobexp:SpecialWarrantyRefNumber = GetXMLElementQueue('PH22', '&job:Fault_Code10')
            jobexp:BlueToothAddress         = GetXMLElementQueue('PH23', '&job:Fault_Code11')
            jobexp:BlueToothHW              = GetXMLElementQueue('PH14', '&job:Fault_Code12')

            ! Start Change 4122 BE(05/04/04)
            !IF jobexp:SpecialWarrantyRefNumber = ''
            !  WriteXMLTag('PH22', '', CLIP(jobexp:SpecialWarrantyRefNumber))
            !ELSE
            !  WriteXMLTag('PH22', '', 'POP'&CLIP(jobexp:SpecialWarrantyRefNumber))
            !END
            If ~func:Accessory
                ! Do not display in Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                WriteXMLTag('PH22', '', CLIP(jobexp:SpecialWarrantyRefNumber))
                ! Start Change 4122 BE(05/04/04)

                WriteXMLTag('PH23', '', jobexp:BlueToothAddress)
                WriteXMLTag('PH24', '', jobexp:BlueToothHW)

            End ! If ~func:Accessory
            !--------------------------------------------------------------
            DO WriteReceiveItemsCSV

            !IF job:Exchange_Unit_Number > 0
            !    DO WriteSWAPItem
            !ELSE
            DO WriteRepairLine
            !END !IF
            !--------------------------------------------------------------
        WriteEndXMLTag('PHONE')
        !------------------------------------------------------------------
    EXIT
WriteReceiveItems       ROUTINE ! needs to be duplicated for each csv line
    DATA
JOBACCFirst LONG(True)
Temp        STRING(255)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('WriteReceiveItems()')

        FREE(ReceiveItem_Queue)

        JOBACCFirst = True
        LOOP WHILE LoadJOBACC(job:Ref_Number, JOBACCFirst)
            !WriteDebug('WriteReceiveItems(LOOP)')

            JOBACCFirst = False

            Temp = GetAccessoryCode(CLIP(jac:Accessory))
            IF CLIP(Temp) > 0
                WriteXMLTag('PH06', '', CLIP(Temp))

                CLEAR(ReceiveItem_Queue)
                    recQ:ReceiveItem = CLIP(Temp)
                ADD(ReceiveItem_Queue)
            END !IF
        END !LOOP
        !------------------------------------------------------------------
    EXIT
WriteSWAPItem       ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !SWAPBlock                 GROUP !---------------------------------------------------------
        !    SWAPDateTime                STRING(20) ! SW01 MANDATORY      (job:Exchange_Despatched) "dd.mm.yyyy hh:mm"
        !    SWAPReasonCode              STRING( 3) ! SW02 MANDATORY      (                       ) VarChar
        !    NewPrimarySerialNumber      STRING(20) ! SW03 MANDATORY      (xch:ESN                ) VarChar  job:Exchange_Unit_Number
        !                          END
        !------------------------------------------------------------------
        !WriteDebug('WriteSWAPItem()')

        IF NOT LoadExchange(job:Exchange_Unit_Number)
            !WriteDebug('WriteSWAPItem(FAIL LoadExchange(' & job:Exchange_Unit_Number & '))')
            ! ERROR
            EXIT
        END !IF

        !WriteDebug('WriteSWAPItem(OK)')

        CLEAR(jobexp:SWAPBlock)
        CLEAR(jobexp:RepairLineBlock)

        WriteStartXMLTag('SWAP', '')
            jobexp:SWAPDateTime           = GetXMLElementQueue('SW01', '@SWAPDateTime()') ! GetExchangeDate(job:Ref_Number)
            jobexp:SWAPReasonCode         = GetXMLElementQueue('SW02', ''               )
            jobexp:NewPrimarySerialNumber = GetXMLElementQueue('SW03', '&xch:ESN'       )

            WriteXMLTag('SW01', '', jobexp:SWAPDateTime)
            WriteXMLTag('SW02', '', jobexp:SWAPReasonCode)
            WriteXMLTag('SW03', '', jobexp:NewPrimarySerialNumber)
            WriteXMLTag('SW04', '', '')
            WriteXMLTag('SW05', '', '')
            WriteXMLTag('SW06', '', '')
            WriteXMLTag('SW07', '', '')
            WriteXMLTag('SW08', '', '')
            WriteXMLTag('SW09', '', '')

        WriteEndXMLTag('SWAP')
        !------------------------------------------------------------------
        ! Uncomment to generate csv file
        !
        Add(JobsExport)

        !WriteDebug('WriteSWAPItem(EXIT, jobexp:SWAPDateTime="' & CLIP(jobexp:SWAPDateTime) & '")')
        !------------------------------------------------------------------
    EXIT
WriteRepairLine       ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !RepairLineBlock           GROUP !---------------------------------
        !    RepairSymptomCode           STRING(255) ! RL01 MANDATORY      (wpr:Fault_Code4)
        !    FaultCode                   STRING(255) ! RL02 MANDATORY      (wpr:Fault_Code3)
        !    KeyRepair                   STRING(  1) ! RL03 MANDATORY      (wpr:Fault_Code5) "1" Yes is a key repair/No "0" Default as "1"
        !    PartNumber                  STRING( 30) ! RL04 OPTIONAL MULTI (wpr:Part_Number)
        !    CCTReferenceNumber          STRING( 30) ! RL06 OPTIONAL       (wpr:Fault_Code1) Format "A999"
        !    RepairModule                STRING( 30) ! RL07 OPTIONAL       (wpr:Fault_Code2) 
        !    PartReplaced                STRING(  1) ! RL08 OPTIONAL       (wpr:Fault_Code6) "1" Yes in warranty period/No "0"
        !                          END
        !------------------------------------------------------------------
        !WriteDebug('WriteRepairLine()')

        IF FillWarrantyParts(job:Ref_Number) = False
            !WriteDebug('WriteRepairLine(FillWarrantyParts(FAIL))')

            EXIT
        END !IF

        LOOP x# = 1 TO RECORDS(WarrantyPart_Queue)
            CLEAR(jobexp:SWAPBlock)
            CLEAR(jobexp:RepairLineBlock)

            GET(WarrantyPart_Queue, x#)
            !WriteDebug('WriteRepairLine(LOOP="' & CLIP(wprQ:PartNumber) & '")')

            ! Start Change 4140 BE(27/04/04)
            LOOP wprQ:Qty TIMES
            ! End Change 4140 BE(27/04/04)

                WriteStartXMLTag('REPAIRROW', '')
                    !----------------------------------------------------------
                    !    RepairSymptomCode           STRING(255) ! RL01 MANDATORY      (wpr:Fault_Code4)
                    !    FaultCode                   STRING(255) ! RL02 MANDATORY      (wpr:Fault_Code3)
                    !    KeyRepair                   STRING(  1) ! RL03 MANDATORY      (wpr:Fault_Code5) "1" Yes is a key repair/No "0" Default as "1"
                    ! Start Change BE031 BE(05/08/2004)
                    !jobexp:RepairSymptomCode = GetXMLElementQueue('RL01', '&wprQ:RepairSymptomCode')
                    jobexp:RepairSymptomCode = GetXMLElementQueue('RL01', '&job:Fault_Code10')
                    ! End Change BE031 BE(05/08/2004)
                    jobexp:FaultCode         = GetXMLElementQueue('RL02', '&wprQ:FaultCode'        )
                    jobexp:KeyRepair         = GetXMLElementQueue('RL03', '&wprQ:KeyRepair'        )

                    ! Start Change BE031 BE(05/08/2004)
                    !WriteXMLTag('RL01', '', job:Fault_Code10)
                    !WriteXMLTag('RL02', '', wprQ:FaultCode)
                    !WriteXMLTag('RL03', '', wprQ:KeyRepair)
                    WriteXMLTag('RL01', '', jobexp:RepairSymptomCode)
                    WriteXMLTag('RL02', '', jobexp:FaultCode)
                    WriteXMLTag('RL03', '', jobexp:KeyRepair)
                    ! End Change BE031 BE(05/08/2004)

                    !----------------------------------------------------------
                    jobexp:PartNumber         = GetXMLElementQueue('RL04', '&wprQ:PartNumber')
                    jobexp:CCTReferenceNumber = GetXMLElementQueue('RL06', '&wprQ:CCTReferenceNumber')
                    jobexp:RepairModule       = GetXMLElementQueue('RL07', '&wprQ:RepairModule'      )

                    !Start - If accessory export, "part replaced" is defaulted to '0' - TrkBs: 5006 (DBH: 19-01-2005)
                    If func:Accessory = True
                        wprq:PartReplaced = '0'
                    End ! If func:Accessory = True
                    !End   - If accessory export, "part replaced" is defaulted to '0' - TrkBs: 5006 (DBH: 19-01-2005)

                    jobexp:PartReplaced       = GetXMLElementQueue('RL08', '&wprQ:PartReplaced'      )

                    ! Start Change BE031 BE(05/08/2004)
                    !WriteXMLTag('RL04', '', wprQ:PartNumber)
                    !WriteXMLTag('RL05', '', '')
                    !WriteXMLTag('RL06', '', wprQ:CCTReferenceNumber)
                    !WriteXMLTag('RL07', '', wprQ:RepairModule)
                    !WriteXMLTag('RL08', '', wprQ:PartReplaced)
                    !WriteXMLTag('RL09', '', '')
                    !WriteXMLTag('RL10', '', '')
                    !WriteXMLTag('RL11', '', '')
                    WriteXMLTag('RL04', '', jobexp:PartNumber)
                    If ~func:Accessory
                        ! Do not display in Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                        WriteXMLTag('RL05', '', '')
                        WriteXMLTag('RL06', '', jobexp:CCTReferenceNumber)
                        WriteXMLTag('RL07', '', jobexp:RepairModule)
                    End ! If ~func:Accessory
                    WriteXMLTag('RL08', '', jobexp:PartReplaced)
                    WriteXMLTag('RL09', '', '')
                    If ~func:Accessory
                        ! Do not display in Accessory export - TrkBs: 5663 (DBH: 19-04-2005)
                        WriteXMLTag('RL10', '', '')
                        WriteXMLTag('RL11', '', '')
                    End ! If ~func:Accessory
                    ! Start Change BE031 BE(05/08/2004)

                    !----------------------------------------------------------
                WriteEndXMLTag('REPAIRROW')

                CleanString(jobexp:FaultCode)
                !--------------------------------------------------------------
                ! Uncomment to generate csv file
                !
                Add(JobsExport)

            ! Start Change 4140 BE(27/04/04)
            END
            ! End Change 4140 BE(27/04/04)

            !WriteDebug('WriteRepairLine("' & CLIP(jobexp:PartNumber) & '")')
            !--------------------------------------------------------------
        END !LOOP

        !WriteDebug('WriteRepairLine(EXIT)')
        !------------------------------------------------------------------
    EXIT
WriteReceiveItemsCSV       ROUTINE ! needs to be duplicated for each csv line
    DATA
    CODE
        !------------------------------------------------------------------
        !WriteDebug('WriteReceiveItemsCSV()')

        CLEAR(jobexp:SWAPBlock)
        CLEAR(jobexp:RepairLineBlock)

        LOOP x# = 1 TO RECORDS(ReceiveItem_Queue)
            GET(ReceiveItem_Queue, x#)
            jobexp:ReceiveItems = recQ:ReceiveItem
            !--------------------------------------------------------------
            ! Uncomment to generate csv file
            !
            IF CLIP(jobexp:ReceiveItems) <> ''
                !WriteDebug('WriteReceiveItemsCSV("' & jobexp:ReceiveItems & '")')

                ADD(JobsExport)
            END !IF
            !--------------------------------------------------------------
        END !LOOP

        FREE(ReceiveItem_Queue)
        jobexp:ReceiveItems = ''
        !------------------------------------------------------------------
    EXIT
WriteJobs               ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !WriteDebug('WriteJobs()')

        DO WriteDataCSV
        !------------------------------------------------------------------
    EXIT
!-----------------------------------------------
PrintXMLElementQueue       ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        !WriteDebug('PrintXMLElementQueue(START)')

        LOOP x# = 1 TO RECORDS(XMLElement_Queue)
            GET(XMLElement_Queue, x#)
            !WriteDebug('    Code="' & CLIP(elementQ:Code) & '", Value="' & CLIP(elementQ:Value) & '")')
        END ! LOOP

        !WriteDebug('PrintXMLElementQueue(END)')
        !------------------------------------------------------------------
    EXIT
SendMessage     ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        IF CLIP(LOC:EmailSubject) = ''
            LOC:EmailSubject = CLIP(LOC:ProgramName) & ' (' & FORMAT(TODAY(), @D8) & ' ' & FORMAT(CLOCK(), @T6) & ') '
        END !IF

        param:MessageNumber += 1
        !------------------------------------------------------------------
        EmailServer      = param:EmailServer
        EmailPort        = param:EmailPort
        EmailFrom        = param:EmailFrom
        EmailTo          = param:EmailTo
        EmailSubject     = CLIP(LOC:EmailSubject) & '[' & param:MessageNumber & ']'
        EmailCC          = ''
        EmailBCC         = ''
        EmailFileList    = '' ! Could be 'c:\test.txt'
        EmailMessageText = ''
        EmailMessageHTML = ''
        !------------------------------------------------------------------
        DO WriteFooterHTML

        LOOP x# = 1 TO RECORDS(HTML_Queue)
            GET(HTML_Queue, x#)
            EmailMessageHTML = CLIP(EmailMessageHTML) & CLIP(html:LINE)
        END !LOOP

        !SETCLIPBOARD(EmailMessageHTML)
        !------------------------------------------------------------------
        !         pEmailServer, pEmailPort, pEmailFrom, pEmailTo, pEmailSubject, pEmailCC, pEmailBcc, pEmailFileList, pEmailMessageText, pEmailMessageHTML
        !SendEMail( EmailServer,  EmailPort,  EmailFrom,  EmailTo,  EmailSubject,  EmailCC,  EmailBCC,  EmailFileList,  EmailMessageText,  EmailMessageHTML)
        !------------------------------------------------------------------
        FREE(HTML_Queue)
        TotalCharacters = 0

        DO WriteHeaderHTML
        !------------------------------------------------------------------
!        setcursor (CURSOR:WAIT)
        !------------------------------------------------------------------
SendFTP     ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
!        setcursor (CURSOR:WAIT)
!            FTP.ServerHost               = 'ftp.crc-group.com'
!            FTP.User                     = 'anonymous'
!            FTP.Password                 = 'anonymous@hotmail.com'
!            FTP.BinaryTransfer           = 1
!            FTP.ChangeDir('/fromSB/crc')    !          = 'FromSB' ! RemoteFTP.CurrentDirectory
!            FTP.ControlPort              = 21
!            FTP.OpenNewControlConnection = 1
!            FTP.PutFile('FROM name', 'TO name')
!        setcursor
        !------------------------------------------------------------------
!-----------------------------------------------
WriteHeaderCSV                     ROUTINE
        !------------------------------------------------------------------
        Clear(jobexp:Record)
            jobexp:ASCNumber                = 'ASCNumber' ! Default "000_100880", repeated for every entry
            jobexp:WorkOrderID              = 'WO02' ! WO02 (job:Ref_Number)
            jobexp:CustomerSymptomCode      = 'WO03' ! WO03 (job:FaultCode_1)
            jobexp:CustomerSymptomText      = 'WO04' ! WO04 (jbn:Fault_Description)
            jobexp:ServiceCode              = '06'   ! WO06 ??? ###################################################
            jobexp:TechnicianComment        = 'WO08' ! WO08 (jbn:Engineers_Notes)
            jobexp:SenderCountry            = '13'   ! STRING(2)                       ! WO13 ISO 3166-1
            jobexp:PrimarySerialNumber      = 'PH01' ! PH01 (job:ESN) IMEI
            jobexp:AdditionalSerialNumber   = 'PH02' ! PH02 (job:FaultCode_7)
            jobexp:ProductCode              = 'PH05' ! PH05 (job:Model_Number)
            jobexp:SWVersion                = 'PH07' ! PH07 (job:FaultCode_9) "V xx.yy"
            jobexp:NewSWVersion             = 'PH08' ! PH08 ??? "V xx.yy" #########################################
            jobexp:HWVersion                = 'PH09' ! PH09 (job:FaultCode_8) "####"
            jobexp:ManufacturingDate        = 'PH11' ! PH11 (job:FaultCode_10) @D "yyyyMM"
            jobexp:PHWarrantyStatus         = '1'    ! PH14 job:Warranty_Job already Checked, CHOOSE(job:Warranty_Job = 'YES', '1', '0')           jobexp:ReceivingDateTime        = 'PH17' ! PH17 "dd.mm.yyyy hh:nn" (?job:Time_Booked?)
            jobexp:ReceivingDateTime        = 'PH17' ! PH17 "dd.mm.yyyy hh:nn" (?job:Time_Booked?)

            jobexp:ShippingDateTime         = 'PH21' ! PH21 "dd.mm.yyyy hh:nn"
            jobexp:POPDate                  = 'PH13' ! PH13 (job:DOP) Proof of purchase date, "DD.MM.YYYY"
            jobexp:SpecialWarrantyRefNumber = 'PH22' ! PH22 (job:Mobile_Number STRING(15))
            jobexp:BlueToothAddress         = 'PH23' ! PH23 (job:FaultCode_5)
            jobexp:BlueToothHW              = 'PH24' ! PH24 (job:FaultCode_6)
            jobexp:RepairSymptomCode        = 'RL01' ! RL01 (job:FaultCode_11)
            jobexp:FaultCode                = 'RL02' ! RL02 (job:FaultCode_12)
            jobexp:KeyRepair                = '3'    ! RL03 "1" Yes is a key repair/No "0" Default as "1"
            jobexp:PartNumber               = 'RL04' ! RL04 (job:FaultCode_9)
            jobexp:CCTReferenceNumber       = 'RL06' ! RL06 (job:FaultCode_10)
            jobexp:RepairModule             = '07'   ! RL07 ??? ###################################################
            jobexp:PartReplaced             = '4'    ! PH14 "1" Yes in warranty period/No "0"
        Add(JobsExport)
        !------------------------------------------------------------------
WriteDataCSV                     ROUTINE
        !------------------------------------------------------------------
        Clear(jobexp:Record)
            jobexp:ASCNumber                = LOC:ASCNumber                          ! Default "000_100880", repeated for every entry
            jobexp:WorkOrderID              = job:Ref_Number                         ! WO02 (job:Ref_Number)
            jobexp:CustomerSymptomCode      = job:Fault_Code1                        ! WO03 (job:FaultCode_1)
            jobexp:CustomerSymptomText      = jbn:Fault_Description                  ! WO04 (jbn:Fault_Description)
            jobexp:ServiceCode              = '$$' ! STRING(2)                       ! WO06 ??? ###################################################
            jobexp:TechnicianComment        = jbn:Engineers_Notes                    ! WO08 (jbn:Engineers_Notes)
            jobexp:SenderCountry            = 'GB' ! STRING(2)                       ! WO13 ISO 3166-1
            jobexp:PrimarySerialNumber      = job:ESN                                ! PH01 (job:ESN) IMEI
            jobexp:AdditionalSerialNumber   = job:Fault_Code7                        ! PH02 (job:FaultCode_7)
            jobexp:ProductCode              = job:Model_Number                       ! PH05 (job:Model_Number)
            jobexp:SWVersion                = job:Fault_Code9                        !* PH07 (job:FaultCode_9) "V xx.yy"
            jobexp:NewSWVersion             = 'V %%.??'                              ! PH08 ??? "V xx.yy" #########################################
            jobexp:HWVersion                = job:Fault_Code8                        ! PH09 (job:FaultCode_8) "####"
            jobexp:ManufacturingDate        = job:Fault_Code10                       !* PH11 (job:FaultCode_10) @D "yyyyMM"
            jobexp:PHWarrantyStatus         = '1'                                    ! PH14 job:Warranty_Job already Checked, CHOOSE(job:Warranty_Job = 'YES', '1', '0')
            jobexp:ReceivingDateTime        = DateToNokiaString(job:Date_Booked)     ! PH17 "dd.mm.yyyy hh:nn" (?job:Time_Booked?)
            jobexp:ShippingDateTime         = DateToNokiaString(job:Date_Despatched) ! PH21 "dd.mm.yyyy hh:nn"
            jobexp:POPDate                  = DateToNokiaString(job:DOP)             ! PH13 (job:DOP) Proof of purchase date, "DD.MM.YYYY"
            jobexp:SpecialWarrantyRefNumber = job:Mobile_Number                      ! PH22 (job:Mobile_Number STRING(15))
            jobexp:BlueToothAddress         = job:Fault_Code5                        ! PH23 (job:FaultCode_5)
            jobexp:BlueToothHW              = job:Fault_Code6                        ! PH23 (job:FaultCode_6)
            jobexp:RepairSymptomCode        = job:Fault_Code11                       ! RL01 (job:FaultCode_11)
            jobexp:FaultCode                = job:Fault_Code12                       ! RL02 (job:FaultCode_12)
            jobexp:KeyRepair                = '1'                                    ! RL03 "1" Yes is a key repair/No "0" Default as "1"
            jobexp:PartNumber               = job:Fault_Code9                        !* RL04 (job:FaultCode_9)
            jobexp:CCTReferenceNumber       = job:Fault_Code10                       !* RL06 (job:FaultCode_10)
            jobexp:RepairModule             = '##'                                   ! RL07 ??? ###################################################
            jobexp:PartReplaced             = '1'                                    ! PH14 "1" Yes in warranty period/No "0"
            !--------------------------------------------------------------
            CleanString(jobexp:CustomerSymptomText)
            CleanString(jobexp:TechnicianComment)
            CleanString(jobexp:FaultCode)
            !--------------------------------------------------------------
        Add(JobsExport)
        !------------------------------------------------------------------
!-----------------------------------------------
WriteHeaderHTML         ROUTINE
        !------------------------------------------------------------------
        FREE(HTML_Queue)
        TotalCharacters = 0

        ADD(HTML_Queue)
            CLEAR(HTML_Queue)

            WriteTABLE(False)
                WriteTR(False)
                WriteTH( 'Main Account Number'      )
                WriteTH( 'Account Number'           )
                WriteTH( 'Contact Name'             )
                WriteTH( 'Branch'                   )
                WriteTH( 'Company Name'             )
                WriteTH( 'Address Line1'            )
                WriteTH( 'Address Line2'            )
                WriteTH( 'Address Line3'            )
                WriteTH( 'Postcode'                 )
                WriteTH( 'Telephone Number'         )
                WriteTH( 'Fax Number'               )
                WriteTH( 'Labour Discount Code'     )
                WriteTH( 'Retail Discount Code'     )
                WriteTH( 'Parts Discount Code'      )
                WriteTH( 'Labour VAT Code'          )
                WriteTH( 'Retail VAT Code'          )
                WriteTH( 'Parts VAT Code'           )
                WriteTH( 'Stop Account'             )
                WriteTH( 'VAT Number'               )
                WriteTH( 'Retail Payment Type'      )
                WriteTH( 'Retail Price Structure'   )
                WriteTH( 'Invoice Customer Address' )
                WriteTH( 'Euro Applies'             )
                WriteTR(True)
            !
        PUT(HTML_Queue)

        TotalCharacters += LEN(CLIP(html:LINE))
        !------------------------------------------------------------------
WriteDataHTML         ROUTINE
        !------------------------------------------------------------------
        ADD(HTML_Queue)
            CLEAR(HTML_Queue)

            WriteTR(False)
                WriteTD( CLIP(sub:Main_Account_Number)      )
                WriteTD( CLIP(sub:Account_Number)           )
                WriteTD( CLIP(sub:Contact_Name)             )
                WriteTD( CLIP(sub:Branch)                   )
                WriteTD( CLIP(sub:Company_Name)             )
                WriteTD( CLIP(sub:Address_Line1)            )
                WriteTD( CLIP(sub:Address_Line2)            )
                WriteTD( CLIP(sub:Address_Line3)            )
                WriteTD( CLIP(sub:Postcode)                 )
                WriteTD( CLIP(sub:Telephone_Number)         )
                WriteTD( CLIP(sub:Fax_Number)               )
                WriteTD( CLIP(sub:Labour_Discount_Code)     )
                WriteTD( CLIP(sub:Retail_Discount_Code)     )
                WriteTD( CLIP(sub:Parts_Discount_Code)      )
                WriteTD( CLIP(sub:Labour_VAT_Code)          )
                WriteTD( CLIP(sub:Retail_VAT_Code)          )
                WriteTD( CLIP(sub:Parts_VAT_Code)           )
                WriteTD( CLIP(sub:Stop_Account)             )
                WriteTD( CLIP(sub:VAT_Number)               )
                WriteTD( CLIP(sub:Retail_Payment_Type)      )
                WriteTD( CLIP(sub:Retail_Price_Structure)   )
                WriteTD( CLIP(sub:Invoice_Customer_Address) )
                WriteTD( NumberToBool(sub:EuroApplies)      )
            WriteTR(True)
        PUT(HTML_Queue)

        TotalCharacters += LEN(CLIP(html:LINE))
        !------------------------------------------------------------------
        IF IsMessageHTMLFull()
            DO SendMessage
        END !IF
        !------------------------------------------------------------------
WriteFooterHTML         ROUTINE
        !------------------------------------------------------------------
        ADD(HTML_Queue)
            CLEAR(HTML_Queue)

            WriteTABLE(True)
        PUT(HTML_Queue)

        TotalCharacters += LEN(CLIP(html:LINE))
        !------------------------------------------------------------------
!-----------------------------------------------
WriteHeaderXML         ROUTINE
    DATA
Attribute             STRING(255)
TimestampAttribute    STRING( 50)
FileCreatorAttribute  STRING( 50)
ClaimGroupIDAttribute STRING( 50)
    CODE
        !------------------------------------------------------------------
        ! RegionCode
        !---------------------------------------
        ! 11 Canada
        ! 12 United States (+ Minor Outlaying I)
        ! 13 Mexico
        ! 14 Brazil
        ! 15 Latin America North (LTAN)
        ! 16 Latin America South (LTAS)
        ! 20 Australasian Pacific (APAC)
        ! 30 EU & Africa
        ! 99 Region Unknown
        !------------------------------------------------------------------
        !WriteDebug('WriteHeaderXML()')

        ! Start Change 4536 BE(22/07/2004)
        !LinePrint('<?xml version="1.0" encoding="iso-8859-1" ?>',  xml:Filename)
        !LinePrint(                                            '',  xml:Filename)
        WriteLog('<?xml version="1.0" encoding="iso-8859-1" ?>')
        WriteLog('')
        ! End Change 4536 BE(22/07/2004)

        !LinePrint('<!DOCTYPE RepairData SYSTEM "RepairData.dtd">', xml:Filename)
        !LinePrint('',                                              xml:Filename)

        TimestampAttribute    = GetAttribute(   'TIMESTAMP', GetTimestamp(TODAY(), CLOCK()) )
        FileCreatorAttribute  = GetAttribute( 'FILECREATOR', GETINI('REPAIRDATA',  'FILECREATOR', '', '.\SBCR0060.ini'))
        ClaimGroupIDAttribute = GetAttribute('CLAIMGROUPID', GETINI('REPAIRDATA', 'CLAIMGROUPID', '', '.\SBCR0060.ini'))

        Attribute = AppendString(TimestampAttribute,  FileCreatorAttribute, ' ')
        Attribute = AppendString(         Attribute, ClaimGroupIDAttribute, ' ')

        WriteStartXMLTag(    'REPAIRDATA', Attribute)
        WriteStartXMLTag( 'SERVICECENTER', ''       )
        !------------------------------------------------------------------
        jobexp:ASCNumber    = LOC:ASCNumber                     ! STRING( 10) ! SC01 MANDATORY Default "000_100880", repeated for every entry
        jobexp:CountryCode  = GetXMLElementQueue('SC02', 'GB' ) ! STRING(  2) ! SC02 MANDATORY ISO 3166-1
        jobexp:RegionCode   = GetXMLElementQueue('SC03', '30' ) ! STRING(  2) ! SC03 MANDATORY Format(99) 30=EU & Africa

        WriteXMLTag('SC01', '', jobexp:ASCNumber)
        WriteXMLTag('SC02', '', jobexp:CountryCode)
        WriteXMLTag('SC03', '', jobexp:RegionCode)

        ! Start Change BE18 BE(04/11/03)
        !WriteXMLTag('SC04', '', '2.1')
        !WriteXMLTag('SC05', '', 'GBP')
        WriteXMLTag('SC04', '', GetXMLElementQueue('SC04', '2.1' ))
        WriteXMLTag('SC05', '', GetXMLElementQueue('SC05', 'GBP' ))
        ! Start Change BE18 BE(04/11/03)
        !------------------------------------------------------------------
    EXIT
!WriteHeaderXML         ROUTINE
!        !------------------------------------------------------------------
!        WriteTABLE(False)
!            WriteTR(False)
!            WriteTH( 'Main Account Number'      )
!            WriteTH( 'Account Number'           )
!            WriteTH( 'Contact Name'             )
!            WriteTH( 'Branch'                   )
!            WriteTH( 'Company Name'             )
!            WriteTH( 'Address Line1'            )
!            WriteTH( 'Address Line2'            )
!            WriteTH( 'Address Line3'            )
!            WriteTH( 'Postcode'                 )
!            WriteTH( 'Telephone Number'         )
!            WriteTH( 'Fax Number'               )
!            WriteTH( 'Labour Discount Code'     )
!            WriteTH( 'Retail Discount Code'     )
!            WriteTH( 'Parts Discount Code'      )
!            WriteTH( 'Labour VAT Code'          )
!            WriteTH( 'Retail VAT Code'          )
!            WriteTH( 'Parts VAT Code'           )
!            WriteTH( 'Stop Account'             )
!            WriteTH( 'VAT Number'               )
!            WriteTH( 'Retail Payment Type'      )
!            WriteTH( 'Retail Price Structure'   )
!            WriteTH( 'Invoice Customer Address' )
!            WriteTH( 'Euro Applies'             )
!            WriteTR(True)
!        !------------------------------------------------------------------
WriteDataXML         ROUTINE
        !------------------------------------------------------------------
        !WriteDebug('WriteDataXML()')
        !WriteXMLTag PROCEDURE(IN:Tag, IN:Attributes, IN:Value)

        WriteStartXMLTag('RepairLine', '')
            WriteXMLTag('ASCNumber', '', jobexp:ASCNumber               ) ! Default "000_100880", repeated for every entry
            WriteXMLTag(     'WO02', '', jobexp:WorkOrderID             ) ! WO02 (job:Ref_Number)
            WriteXMLTag(     'WO03', '', jobexp:CustomerSymptomCode     ) ! WO03 (job:FaultCode_1)
            WriteXMLTag(     'WO04', '', jobexp:CustomerSymptomText     ) ! WO04 (jbn:Fault_Description)
            WriteXMLTag(     'WO06', '', jobexp:ServiceCode             ) ! WO06 ??? ###################################################
            WriteXMLTag(     'WO08', '', jobexp:TechnicianComment       ) ! WO08 (jbn:Engineers_Notes)
            WriteXMLTag(     'WO13', '', jobexp:SenderCountry           ) ! WO13 ISO 3166-1
            WriteXMLTag(     'PH01', '', jobexp:PrimarySerialNumber     ) ! PH01 (job:ESN) IMEI
            WriteXMLTag(     'PH02', '', jobexp:AdditionalSerialNumber  ) ! PH02 (job:FaultCode_7)
            WriteXMLTag(     'PH05', '', jobexp:ProductCode             ) ! PH05 (job:Model_Number)
            WriteXMLTag(     'PH07', '', jobexp:SWVersion               ) ! PH07 (job:FaultCode_9) "V xx.yy"
            WriteXMLTag(     'PH08', '', jobexp:NewSWVersion            ) ! PH08 ??? "V xx.yy" #########################################
            WriteXMLTag(     'PH09', '', jobexp:HWVersion               ) ! PH09 (job:FaultCode_8) "####"
            WriteXMLTag(     'PH11', '', jobexp:ManufacturingDate       ) ! PH11 (job:FaultCode_10) @D "yyyyMM"
            WriteXMLTag(     'PH14', '', jobexp:PHWarrantyStatus        ) ! PH14 job:Warranty_Job already Checked, CHOOSE(job:Warranty_Job = 'YES', '1', '0')
            WriteXMLTag(     'PH17', '', jobexp:ReceivingDateTime       ) ! PH17 "dd.mm.yyyy hh:nn" (?job:Time_Booked?)
            WriteXMLTag(     'PH21', '', jobexp:ShippingDateTime        ) ! PH21 "dd.mm.yyyy hh:nn"
            WriteXMLTag(     'PH13', '', jobexp:POPDate                 ) ! PH13 (job:DOP) Proof of purchase date, "DD.MM.YYYY"
            WriteXMLTag(     'PH22', '', jobexp:SpecialWarrantyRefNumber) ! PH22 (job:Mobile_Number STRING(15))
            WriteXMLTag(     'PH23', '', jobexp:BlueToothAddress        ) ! PH23 (job:FaultCode_5)
            WriteXMLTag(     'PH24', '', jobexp:BlueToothHW             ) ! PH24 (job:FaultCode_6)
            WriteXMLTag(     'RL01', '', jobexp:RepairSymptomCode       ) ! RL01 (job:FaultCode_11)
            WriteXMLTag(     'RL02', '', jobexp:FaultCode               ) ! RL02 (job:FaultCode_12)
            WriteXMLTag(     'RL03', '', jobexp:KeyRepair               ) ! RL03 "1" Yes is a key repair/No "0" Default as "1"
            WriteXMLTag(     'RL04', '', jobexp:PartNumber              ) ! RL04 (job:FaultCode_9)
            WriteXMLTag(     'RL06', '', jobexp:CCTReferenceNumber      ) ! RL06 (job:FaultCode_10)
            WriteXMLTag(     'RL07', '', jobexp:RepairModule            ) ! RL07 ??? ###################################################
            WriteXMLTag(     'PH14', '', jobexp:PartReplaced            ) ! PH14 "1" Yes in warranty period/No "0"
        WriteEndXMLTag('RepairLine')
        !------------------------------------------------------------------
WriteFooterXML         ROUTINE
        !------------------------------------------------------------------
        !WriteDebug('WriteFooterXML()')

        WriteEndXMLTag('SERVICECENTER')
        WriteEndXMLTag('REPAIRDATA')
        !------------------------------------------------------------------
!-----------------------------------------------
CancelCheck                     routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        BEEP(BEEP:SystemAsterisk)  ;  YIELD()

        Case MessageEx('Are you sure you want to cancel?',LOC:ApplicationName,|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
                CancelPressed = True
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1

    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess) * 100

      if percentprogress > 100
        percentprogress = 100
      elsIF percentprogress > 80
        recordstoprocess = 3 * recordstoprocess
      end
    end

    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
    end

    Display()

endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
RefreshWindow                          ROUTINE
    !|
    !| This routine is used to keep all displays and control templates current.
    !|
    IF window{Prop:AcceptAll} THEN EXIT.
    DISPLAY()
    !ForceRefresh = False
!-----------------------------------------------
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

!        LOC:UserName = LOC:ApplicationName
!
!        Case MessageEx('Fail User Check For "' & CLIP(CommandLine) & '"?', LOC:ApplicationName & ' DEBUG',|
!                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
!            Of 1 ! &Yes Button
!                POST(Event:CloseWindow)
!
!                EXIT
!            Of 2 ! &No Button
!                EXIT
!        End!Case MessageEx
        !-----------------------------------------------
        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
            Case MessageEx('Attempting to use Workshop Performance Report<10,13>'            & |
                '   without using ' & LOC:ApplicationName & '.<10,13>'                       & |
                '   Start ' & LOC:ApplicationName & ' and run the report from there.<10,13>',  |
                LOC:ApplicationName,                                                           |
                'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
            Of 1 ! &OK Button
            END!Case MessageEx

           POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
        SET(USERS)
        Access:USERS.Clearkey(use:Password_Key)
        glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
        Access:USERS.Clearkey(use:Password_Key)
        use:Password = glo:Password
        If Access:USERS.Tryfetch(use:Password_Key)
            !Assert(0,'<13,10>Fetch Error<13,10>')
            Case MessageEx('Unable to find your logged in user details.', |
                    LOC:ApplicationName,                                   |
                    'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                Of 1 ! &OK Button
            End!Case MessageEx

            POST(Event:CloseWindow)

           EXIT
        End! If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
        !-----------------------------------------------
        LOC:UserName = use:Forename

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = use:Surname
        ELSE
            LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname 
        END !IF

        IF CLIP(LOC:UserName) = ''
            LOC:UserName = '<' & use:User_Code & '>'
        END !IF
        !-----------------------------------------------
    EXIT
GetFileName                             ROUTINE ! Generate default file name
    DATA
Desktop         CString(255)
DeskTopExists   BYTE
ApplicationPath STRING(255)

TempDate        STRING(6)
TempTime        STRING(6)
    CODE
!        !-----------------------------------------------
!        ! Generate default file name
!        !
!        !-----------------------------------------------
!        ! 4 Dec 2001 John Stop using def:Export_Path as previously directed
!        ! 4 Dec 2001 John Use generated date to create file name not parameters LOC:StartDate,LOC:EndDate
!        !
!        IF CLIP(LOC:FileName) <> ''
!            ! Already generated 
!            EXIT
!        END !IF
!
!        DeskTopExists = False
!
!        !SHGetSpecialFolderPath( GetDesktopWindow(), Desktop, CSIDL_DESKTOPDIRECTORY, FALSE )
!        if FileDialog('Select Directory', Desktop, , File:KeepDir + File:LongName + File:Directory)
!        end
!
!        LOC:DesktopPath = Desktop
!        !-----------------------------------------------
!        ApplicationPath = Desktop & '\' & LOC:ApplicationName & ' Export'
!        IF EXISTS(CLIP(ApplicationPath))
!            LOC:Path      = CLIP(ApplicationPath) & '\'
!            DeskTopExists = True
!
!        ELSE
!            Desktop  = CLIP(ApplicationPath)
!
!            IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!            ELSE
!!                MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path      = CLIP(ApplicationPath) & '\'
!                DeskTopExists = True
!            END !IF
!
!        END !IF
!        !-----------------------------------------------
!        IF DeskTopExists
!            ApplicationPath = CLIP(LOC:Path) & CLIP(LOC:ProgramName)
!            Desktop         = CLIP(ApplicationPath)
!
!            IF NOT EXISTS(CLIP(ApplicationPath))
!                IF MkDir( Desktop ) ! C runtime library 'clib.clw'
!!                    MESSAGE('MkDir(' & Desktop & ')=[TRUE]')
!!                ELSE
!!                    MESSAGE('MkDir(' & Desktop & ')=[FALSE]')
!                END !IF
!            END !IF
!
!            IF EXISTS(CLIP(ApplicationPath))
!                LOC:Path = CLIP(ApplicationPath) & '\'
!            END !IF
!        END !IF
!        !-----------------------------------------------
        ! Start Change BE016 BE(20/10/03)
        !TempTime     = FORMAT(CLOCK(), @T5)  ! "HHMMSS"
        !TempDate     = FORMAT(TODAY(), @D11) ! "YYMMDD"

        !Start - Pointless, but just incase the accessory report is run at exactly the same time - TrkBs: 5006 (DBH: 19-01-2005)
        If func:Accessory = True
            TempTime     = FORMAT(CLOCK() + 600, @T05)  ! "HHMMSS"
        Else ! If func:Accessory = True
            TempTime     = FORMAT(CLOCK(), @T05)  ! "HHMMSS"
        End ! If func:Accessory = True
        !End   - Pointless, but just incase the accessory report is run at exactly the same time - TrkBs: 5006 (DBH: 19-01-2005)

        TempDate     = FORMAT(TODAY(), @D011) ! "YYMMDD"
        ! Start Change BE016 BE(20/10/03)
        If Sub(Clip(man:EDI_Path),-1,1) <> '\'
            man:EDI_Path = Clip(man:EDI_path) & '\'
        End !If Sub(Clip(man:EDI_Path),-1,1) <> '\'

        xml:Filename = SHORTPATH(CLIP(MAN:EDI_Path)) & 'R' & CLIP(LOC:ASCNumber) & TempDate[5:6]  & TempDate[3:4] & TempDate[1:2] & TempTime & '.xml'
        csv:Filename = SHORTPATH(CLIP(MAN:EDI_Path)) & 'R' & CLIP(LOC:ASCNumber) & TempDate[5:6]  & TempDate[3:4] & TempDate[1:2] & TempTime & '.tab'

        LOC:FileName = xml:Filename
        !LOC:HTMLFilename = SHORTPATH(CLIP(LOC:Path)) & CLIP(LOC:ProgramName) & ' ' & LEFT(FORMAT(TODAY(), @D12))& ' ' & LEFT(FORMAT(CLOCK(), @T5)) & '.html'
        !-----------------------------------------------
    EXIT
LoadFileDialog                          ROUTINE ! Ask user if file name is empty
    DATA
OriginalPath STRING(255)
    CODE
        !-----------------------------------------------
        IF CLIP(LOC:Filename) = ''
            DO GetFileName 
        END !IF

!        OriginalPath = PATH()
!        SETPATH(LOC:Path) ! Required for Win98
!            !IF NOT FILEDIALOG('Save Spreadsheet', LOC:Filename, 'Microsoft Excel Workbook|*.XLS', |
!            IF NOT FILEDIALOG('Save File', LOC:Filename, 'XML File (*.xml)|*.xml', |
!                FILE:KeepDir + FILE:Save + FILE:NoError + FILE:LongName)
!
!                LOC:Filename = ''
!            END !IF
!        SETPATH(OriginalPath)
!
!        UPDATE()
!        DISPLAY()
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
PassViaClipboard                                            ROUTINE
    DATA
!ActiveWorkSheet CSTRING(20)
!ActiveCell      CSTRING(20)
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            clip:OriginalValue = CLIPBOARD()
            clip:Saved         = True
        END !IF
        !-----------------------------------------------
        !clip:Value = 'abc' ! <09>def<09>xyz'
        SETCLIPBOARD(CLIP(clip:Value))

        Excel{'ActiveSheet.Paste'} ! Special'}
        !-----------------------------------------------
    EXIT
ResetClipboard                                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF clip:Saved = False
            EXIT
        END !IF

        SETCLIPBOARD(clip:OriginalValue)
        !-----------------------------------------------
    EXIT
!-----------------------------------------------
TestXMLEncode           ROUTINE
        MESSAGE('XMLEncode("")="'         & XMLEncode('')        & '"')

        MESSAGE('XMLEncode("A")="'        & XMLEncode('A')       & '"')
        MESSAGE('XMLEncode("AB")="'       & XMLEncode('AB')      & '"')
        MESSAGE('XMLEncode("ABC")="'      & XMLEncode('ABC')     & '"')

        MESSAGE('XMLEncode("&")="'        & XMLEncode('&')       & '"')

        MESSAGE('XMLEncode("&A")="'       & XMLEncode('&A')      & '"')
        MESSAGE('XMLEncode("&AB")="'      & XMLEncode('&AB')     & '"')
        MESSAGE('XMLEncode("&ABC")="'     & XMLEncode('&ABC')    & '"')

        MESSAGE('XMLEncode("A&")="'       & XMLEncode('A&')      & '"')
        MESSAGE('XMLEncode("AB&")="'      & XMLEncode('AB&')     & '"')
        MESSAGE('XMLEncode("ABC&")="'     & XMLEncode('ABC&')    & '"')

        MESSAGE('XMLEncode("A&BC")="'     & XMLEncode('A&BC')    & '"')
        MESSAGE('XMLEncode("AB&C")="'     & XMLEncode('AB&C')    & '"')

        MESSAGE('XMLEncode("A&&B")="'     & XMLEncode('A&&B')    & '"')
        MESSAGE('XMLEncode("&A&B&C&")="'  & XMLEncode('&A&B&C&') & '"')
CheckINIFile           ROUTINE
    DATA
    CODE
        !------------------------------------------------------------------
        IF '*Missing*' <> CLIP( GETINI('Legend', 'Literal', '*Missing*', '.\sbcr0060.ini') )
            EXIT
        END !IF
        !------------------------------------------------------------------
        PUTINI(       'Legend',      'Literal',                                                        '$', '.\sbcr0060.ini')
        PUTINI(       'Legend',     'Evaluate',                                                        '&', '.\sbcr0060.ini')
        PUTINI(       'Legend',      'Execute',                                                        '@', '.\sbcr0060.ini')

        PUTINI(   'REPAIRDATA',  'FILECREATOR',                                                      'CRC', '.\sbcr0060.ini')
        PUTINI(   'REPAIRDATA', 'CLAIMGROUPID',                                                    '00000', '.\sbcr0060.ini')

        PUTINI('SERVICECENTRE',        'Items',                                                        '3', '.\sbcr0060.ini')
        PUTINI('SERVICECENTRE',         'SC01',                                              '$999_100880', '.\sbcr0060.ini')
        PUTINI('SERVICECENTRE',         'SC02',                                                      '$GB', '.\sbcr0060.ini')
        PUTINI('SERVICECENTRE',         'SC03',                                                      '$30', '.\sbcr0060.ini')

        PUTINI(     'CUSTOMER',        'Items',                                                        '7', '.\sbcr0060.ini')
        PUTINI(     'CUSTOMER',         'CU01',                                                         '', '.\sbcr0060.ini')
        PUTINI(     'CUSTOMER',         'CU02',                                                         '', '.\sbcr0060.ini')
        PUTINI(     'CUSTOMER',         'CU03',                                                         '', '.\sbcr0060.ini')
        PUTINI(     'CUSTOMER',         'CU04',                                                         '', '.\sbcr0060.ini')
        PUTINI(     'CUSTOMER',         'CU05',                                                         '', '.\sbcr0060.ini')
        PUTINI(     'CUSTOMER',         'CU06',                                                         '', '.\sbcr0060.ini')
        PUTINI(     'CUSTOMER',         'CU07',                                                         '', '.\sbcr0060.ini')

        PUTINI(    'WORKORDER',        'Items',                                                       '13', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO01',                                                         '', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO02',                                          '&job:Ref_Number', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO03',                                         '&job:Fault_Code1', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO04',                                   '&jbn:Fault_Description', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO05',                                                         '', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO06',                                         '&job:Fault_Code2', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO07',                                                         '', '.\sbcr0060.ini')
        !UTINI(    'WORKORDER',         'WO08',                                     '&jbn:Engineers_Notes', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO08',                                                         '', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO09',                                                         '', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO10',                                                         '', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO11',                                                         '', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO12',                                                         '', '.\sbcr0060.ini')
        PUTINI(    'WORKORDER',         'WO13',                                                      '$GB', '.\sbcr0060.ini')

        PUTINI(        'PHONE',        'Items',                                                       '24', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH01',                                                 '&job:ESN', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH02',                                         '&job:Fault_Code3', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH03',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH04',                                                         '', '.\sbcr0060.ini')
        !UTINI(        'PHONE',         'PH05',                                         '&job:Fault_Code4', '.\sbcr0060.ini')
        !UTINI(        'PHONE',         'PH05',                                        '&job:Product_Code', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH05',                                        '&job:Model_Number', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH06',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH07',                                         '&job:Fault_Code5', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH08',                                         '&job:Fault_Code6', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH09',                                         '&job:Fault_Code7', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH10',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH11',                                         '&job:Fault_Code8', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH12',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH13',                              '@DateToNokiaString(job:DOP)', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH14',                                         '&job:Fault_Code9', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH15',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH16',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH17', '@DateTimeToNokiaString(job:Date_Booked, job:Time_Booked)', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH18',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH19',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH20',                                                         '', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH21',                  '@DateToNokiaString(job:Date_Despatched)', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH22',                                        '&job:Fault_Code10', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH23',                                        '&job:Fault_Code11', '.\sbcr0060.ini')
        PUTINI(        'PHONE',         'PH24',                                        '&job:Fault_Code12', '.\sbcr0060.ini')

        PUTINI(         'SWAP',        'Items',                                                         '9', '.\sbcr0060.ini')
        !UTINI(         'SWAP',        'xxxxx',                                   'job:Exchange_Despatched', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW01',                                           '@SWAPDateTime()', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW02',                                                          '', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW03',                                                  '&xch:ESN', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW04',                                                          '', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW05',                                                          '', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW06',                                                          '', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW07',                                                          '', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW08',                                                          '', '.\sbcr0060.ini')
        PUTINI(         'SWAP',         'SW09',                                                          '', '.\sbcr0060.ini')

        PUTINI(   'REPAIRLINE',        'Items',                                                        '11', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL01',                                          '&wpr:Fault_Code4', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL02',                                          '&wpr:Fault_Code3', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL03',                                          '&wpr:Fault_Code5', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL04',                                          '&wpr:Part_Number', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL05',                                                          '', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL06',                                          '&wpr:Fault_Code1', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL07',                                          '&wpr:Fault_Code2', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL08',                                          '&wpr:Fault_Code6', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL09',                                                          '', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL10',                                                          '', '.\sbcr0060.ini')
        PUTINI(   'REPAIRLINE',         'RL11',                                                          '', '.\sbcr0060.ini')
        !------------------------------------------------------------------
    EXIT


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CRC_Nokia_EDI_XML_Export',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_wpr_id',save_wpr_id,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('save_aud_id',save_aud_id,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:StartRecordNumber',Parameter_Group:StartRecordNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:ENDRecordNumber',Parameter_Group:ENDRecordNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:MessageID',Parameter_Group:MessageID,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:EmailServer',Parameter_Group:EmailServer,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:EmailPort',Parameter_Group:EmailPort,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:EmailFrom',Parameter_Group:EmailFrom,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:EmailTo',Parameter_Group:EmailTo,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:MessageNumber',Parameter_Group:MessageNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:RecordNumber',Parameter_Group:RecordNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:StartDate',Parameter_Group:StartDate,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Parameter_Group:EndDate',Parameter_Group:EndDate,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:Account_Name',Local_Group:Account_Name,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:AccountNumber',Local_Group:AccountNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:ApplicationName',Local_Group:ApplicationName,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:ASCNumber',Local_Group:ASCNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:CommentText',Local_Group:CommentText,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:DesktopPath',Local_Group:DesktopPath,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:EmailSubject',Local_Group:EmailSubject,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:FileName',Local_Group:FileName,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:HTMLFilename',Local_Group:HTMLFilename,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:InitialRecordNumber',Local_Group:InitialRecordNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:KeepTrackOfRecordNumber',Local_Group:KeepTrackOfRecordNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:LastRecordNumber',Local_Group:LastRecordNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:MaxHTMLSize',Local_Group:MaxHTMLSize,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:Path',Local_Group:Path,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:ProgramName',Local_Group:ProgramName,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:SectionName',Local_Group:SectionName,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:Text',Local_Group:Text,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:UserCode',Local_Group:UserCode,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Local_Group:UserName',Local_Group:UserName,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('XML_Group:Filename',XML_Group:Filename,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('XML_Group:TotalCharacters',XML_Group:TotalCharacters,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('XML_Group:Depth',XML_Group:Depth,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Excel_Group:Excel',Excel_Group:Excel,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Excel_Group:excel:ColumnName',Excel_Group:excel:ColumnName,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Excel_Group:excel:ColumnWidth',Excel_Group:excel:ColumnWidth,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Excel_Group:excel:DateFormat',Excel_Group:excel:DateFormat,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Excel_Group:excel:OperatingSystem',Excel_Group:excel:OperatingSystem,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Excel_Group:Excel:Visible',Excel_Group:Excel:Visible,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:AccountValue',Misc_Group:AccountValue,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:AccountTag',Misc_Group:AccountTag,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:AccountCount',Misc_Group:AccountCount,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:AccountChange',Misc_Group:AccountChange,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:OPTION1',Misc_Group:OPTION1,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:RecordCount',Misc_Group:RecordCount,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:Result',Misc_Group:Result,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:Save_Job_ID',Misc_Group:Save_Job_ID,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:SAVEPATH',Misc_Group:SAVEPATH,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Misc_Group:TotalCharacters',Misc_Group:TotalCharacters,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Progress_Group:progress:Text',Progress_Group:progress:Text,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Progress_Group:progress:NextCancelCheck',Progress_Group:progress:NextCancelCheck,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Sheet_Group:TempLastCol',Sheet_Group:TempLastCol,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Sheet_Group:HeadLastCol',Sheet_Group:HeadLastCol,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Sheet_Group:DataLastCol',Sheet_Group:DataLastCol,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Sheet_Group:HeadSummaryRow',Sheet_Group:HeadSummaryRow,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Sheet_Group:DataSectionRow',Sheet_Group:DataSectionRow,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Sheet_Group:DataHeaderRow',Sheet_Group:DataHeaderRow,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Clipboard_Group:OriginalValue',Clipboard_Group:OriginalValue,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Clipboard_Group:Saved',Clipboard_Group:Saved,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Clipboard_Group:Value',Clipboard_Group:Value,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('HTML_Queue:LINE',HTML_Queue:LINE,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailServer',Email_Group:EmailServer,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailPort',Email_Group:EmailPort,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailFrom',Email_Group:EmailFrom,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailTo',Email_Group:EmailTo,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailSubject',Email_Group:EmailSubject,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailCC',Email_Group:EmailCC,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailBCC',Email_Group:EmailBCC,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailFileList',Email_Group:EmailFileList,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailMessageText',Email_Group:EmailMessageText,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Email_Group:EmailMessageHTML',Email_Group:EmailMessageHTML,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('XML_Queue:LINE',XML_Queue:LINE,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Debug_Group:Active',Debug_Group:Active,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('Debug_Group:Count',Debug_Group:Count,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:PartNumber',WarrantyPart_Queue:PartNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:RepairSymptomCode',WarrantyPart_Queue:RepairSymptomCode,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:FaultCode',WarrantyPart_Queue:FaultCode,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:KeyRepair',WarrantyPart_Queue:KeyRepair,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:CCTReferenceNumber',WarrantyPart_Queue:CCTReferenceNumber,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:RepairModule',WarrantyPart_Queue:RepairModule,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:PartReplaced',WarrantyPart_Queue:PartReplaced,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:Cost',WarrantyPart_Queue:Cost,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:Qty',WarrantyPart_Queue:Qty,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('WarrantyPart_Queue:FaultCode6',WarrantyPart_Queue:FaultCode6,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('ReceiveItem_Queue:ReceiveItem',ReceiveItem_Queue:ReceiveItem,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('XMLElement_Queue:Code',XMLElement_Queue:Code,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('XMLElement_Queue:Value',XMLElement_Queue:Value,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('pos',pos,'CRC_Nokia_EDI_XML_Export',1)
    SolaceViewVars('tmp:ReturnedBatchNumber',tmp:ReturnedBatchNumber,'CRC_Nokia_EDI_XML_Export',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2:2;  SolaceCtrlName = '?String2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?param:EndDate;  SolaceCtrlName = '?param:EndDate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Excel:Visible;  SolaceCtrlName = '?Excel:Visible';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('CRC_Nokia_EDI_XML_Export')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'CRC_Nokia_EDI_XML_Export')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:AUDSTATS.Open
  Relate:DEFAULTS.Open
  Relate:EXCHANGE.Open
  Access:JOBS.UseFile
  Access:WARPARTS.UseFile
  Access:JOBSE.UseFile
  Access:USERS.UseFile
  Access:SUBACCAD.UseFile
  Access:SUBTRACC.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:STOCK.UseFile
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
      LOC:ProgramName   = 'NSH Nokia EDI XML Export'               ! Job=2108      Cust=    Date=20 Nov 2002 John
      window{PROP:Text} = CLIP(LOC:ProgramName)
      ?Tab1{PROP:Text}  = CLIP(LOC:ProgramName) & ' Criteria'
      !--------------------------------------------------------------
      param:EndDate   = TODAY()
      param:StartDate = DATE(MONTH(param:EndDate), 1, YEAR(param:EndDate))
  
      Excel:Visible   = False ! INTEC = ON
      debug:Active    = False
      !--------------------------------------------------------------
      !DO GetUserName
      !--------------------------------------------------------------
      If func:BatchNumber <> 0
          !If a reprint, then you don't need to input a date, so
          !just call the export straight away
          Post(Event:Accepted,?OK)
      End !If func:BatchNumber <> 0
  
      ! Start Change BE031 BE(05/08/2004)
      BIND('wprQ:PartNumber', wprQ:PartNumber)
      BIND('wprQ:RepairSymptomCode', wprQ:RepairSymptomCode)
      BIND('wprQ:FaultCode', wprQ:FaultCode)
      BIND('wprQ:KeyRepair', wprQ:KeyRepair)
      BIND('wprQ:CCTReferenceNumber', wprQ:CCTReferenceNumber)
      BIND('wprQ:RepairModule', wprQ:RepairModule)
      BIND('wprQ:PartReplaced', wprQ:PartReplaced)
      BIND('wprQ:Cost', wprQ:Cost)
      BIND('wprQ:Qty', wprQ:Qty)
      BIND('wprQ:FaultCode6', wprQ:FaultCode6)
      ! End Change BE031 BE(05/08/2004)
  !Start - Hide the window, if re-edi-ing claims - TrkBs: 5096 (DBH: 06-12-2004)
  If func:BatchNumber <> 0
      0{prop:Hide} = True
  End ! func:BatchNumber <> 0
  !End   - Hide the window, if re-edi-ing claims - TrkBs: 5096 (DBH: 06-12-2004)
  Do RecolourWindow
  ?param:EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDSTATS.Close
    Relate:DEFAULTS.Close
    Relate:EXCHANGE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'CRC_Nokia_EDI_XML_Export',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      DO OKButtonPressed
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Cancel
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Cancel, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          param:EndDate = TINCALENDARStyle1(param:EndDate)
          Display(?param:EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'CRC_Nokia_EDI_XML_Export')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?param:EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
!-----------------------------------------------
After   PROCEDURE (LookFor, LookIn)! STRING
FirstChar     LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( LookFor, LookIn, 1, 1 )
        IF FirstChar = 0
            RETURN ''
        END !IF

        FirstChar += LEN(LookFor)

        RETURN SUB(LookIn, FirstChar, 255)
        !-----------------------------------------------------------------
AppendString    PROCEDURE(IN:First, IN:Second, IN:Separator)!STRING
    CODE
        !------------------------------------------------------------------
        IF CLIP(IN:First) = ''
            RETURN IN:Second

        ELSIF CLIP(IN:Second) = ''
            RETURN IN:First
        END ! IF

        RETURN CLIP(IN:First) & IN:Separator & CLIP(IN:Second)
        !------------------------------------------------------------------
Before   PROCEDURE (LookFor, LookIn)! STRING
FirstChar     LONG
    CODE
        !-----------------------------------------------------------------
        FirstChar = INSTRING( CLIP(LEFT(LookFor)), LookIn, 1, 1 )
        IF FirstChar = 0
            RETURN ''
        END !IF

        RETURN SUB(LookIn, 1, FirstChar - 1)
        !-----------------------------------------------------------------
CheckJobOK              PROCEDURE()! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF    NOT LoadJOBSE(job:Ref_Number)
            RETURN False

        ELSIF NOT LoadJOBNOTES(job:Ref_Number)
            RETURN False

        !ELSIF NOT LoadWEBJOB(job:Ref_Number)
        !    RETURN False
        !
        END !IF
 RETURN True
        !------------------------------------------------------------------
        ! Doube check that is isn't invoiced
        If job:Invoice_Number_Warranty <> 0
            RETURN False
        End !If job:Invoice_Number_Warranty <> 0
        !------------------------------------------------------------------
        ! Make sure it's a Warranty Job
        If job:Warranty_Job <> 'YES'
            RETURN False
        End !If job:Warranty_Job <> 'YES'
        !------------------------------------------------------------------
        ! Has this job already been done
!        If wob:OracleExportNumber <> 0
!            RETURN False
!        End !If wob:OracleExportNumber <> 0
        !------------------------------------------------------------------

!        count_parts# = 0
!        !save_wpr_id = access:warparts.savefile()
!        access:warparts.clearkey(wpr:part_number_key)
!        wpr:ref_number  = job:ref_number
!        set(wpr:part_number_key,wpr:part_number_key)
!        loop
!            if access:warparts.next()
!               break
!            end !if
!            if wpr:ref_number  <> job:ref_number      |
!                then break.  ! end if
!            yldcnt# += 1
!            if yldcnt# > 25
!               yield() ; yldcnt# = 0
!            end !if
!!            If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!!                Cycle
!!            End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!
!            access:stock.clearkey(sto:ref_number_key)
!            sto:ref_number = wpr:part_ref_number
!            if access:stock.fetch(sto:ref_number_key) = Level:Benign
!                If sto:accessory = 'YES'
!                    Cycle
!                End!If sto:accessory = 'YES'
!                If sto:ExchangeUnit = 'YES'
!                    Cycle
!                End!If sto:ExchangeUnit = 'YES'
!            end
!            count_parts# += 1
!        end !loop
!        !access:warparts.restorefile(save_wpr_id)
!
!        If count_parts# = 0
!            RETURN False
!        End !If job:Warranty_Job <> 'YES'


        RETURN True
        !------------------------------------------------------------------
CleanString         PROCEDURE( INOUT:String )
    CODE
        !------------------------------------------------------------------
        LOOP x# = 1 TO LEN(INOUT:String)
            CASE VAL(INOUT:String[ x# ])
            OF 00 TO 13
                INOUT:String[ x# ] = ' '
            ELSE
                ! NULL
            END !CASE

        END !LOOP
        !------------------------------------------------------------------
DateToNokiaString PROCEDURE(IN:Date)!STRING
    CODE
        !------------------------------------------------------------------
        IF IN:Date = 0
            RETURN ''
        ELSE
            RETURN LEFT(FORMAT(IN:Date, @D06.)) ! @D6    dd/mm/yyyy    31/10/1959
        END !IF
        !------------------------------------------------------------------
DateTimeToNokiaString PROCEDURE(IN:Date, IN:Time)!STRING
TempDate STRING(10)
TempTime STRING(10)
    CODE
        !------------------------------------------------------------------
        IF IN:Date = 0
            TempDate = ''
        ELSE
            TempDate = LEFT(FORMAT(IN:Date, @D06.))
        END !IF

        IF IN:Time = 0
            TempTime = ''
        ELSE
            TempTime = FORMAT(IN:Time, @T01)

!            IF TempTime[1] = ' '
!                TempTime[1] = '0'
!            END !IF
        END !IF

        RETURN AppendString(TempDate, TempTime, ' ')
        !------------------------------------------------------------------
DateToString PROCEDURE(IN:Date)!STRING
    CODE
        !------------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !------------------------------------------------------------------
EvaluateFunction                PROCEDURE( IN:String )! STRING
FunctionName STRING(100)
Parameter    STRING(100),DIM(10)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('EvaluateFunction("' & CLIP(IN:String) & '")START')

        FunctionName = CLIP(LEFT( Before('(', IN:String) ))
        !WriteDebug('    FunctionName="' & FunctionName & '")')

        Parameter[1] = CLIP(LEFT( Before(')', After('(', IN:String)) ))
        !WriteDebug('    Parameter[1]="' & CLIP(Parameter[1]) & '")')

        CASE UPPER(FunctionName)
        OF 'SWAPDATETIME'
            RETURN SWAPDateTime()

        OF 'DATETONOKIASTRING'
            RETURN DateToNokiaString(EVALUATE(Parameter[1]))

        OF 'DATETIMETONOKIASTRING'
            Parameter[2] = CLIP(LEFT( Before(',', Parameter[1]) ))
            !WriteDebug('    Parameter[2]="' & CLIP(Parameter[2]) & '")')

            Parameter[3] = CLIP(LEFT( After(',', Parameter[1]) ))
            !WriteDebug('    Parameter[3]="' & CLIP(Parameter[3]) & '")')

            RETURN DateTimeToNokiaString(EVALUATE(Parameter[2]), EVALUATE(Parameter[3]))

        ELSE
            ! IGNORE
        END !CASE

        !WriteDebug('EvaluateFunction("' & CLIP(IN:String) & '")EXIT')
        !------------------------------------------------------------------
EvaluateXMLElementQueue          PROCEDURE( IN:Tag, IN:Default )! STRING
MyValue LIKE(elementQ:Value)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('EvaluateXMLElementQueue(IN:Tag="' & CLIP(IN:Tag) & '", IN:Default="' & CLIP(IN:Default) & '")')

        MyValue = GetXMLElementQueue(IN:Tag, '')

        IF MyValue <> ''
            !WriteDebug('EvaluateXMLElementQueue(MyValue="' & CLIP(MyValue) & '")')

            MyValue = EVALUATE(MyValue)
        END !IF

        IF MyValue = ''
            !WriteDebug('EvaluateXMLElementQueue(MyValue = "")')
            MyValue = EVALUATE(IN:Default)
        END !IF

        !WriteDebug('EvaluateXMLElementQueue(RETURN="' & CLIP(MyValue) & '")')
        RETURN MyValue
        !------------------------------------------------------------------
FillWarrantyParts       PROCEDURE( IN:JobNumber ) ! LONG ! BOOL
WPRFirst LONG(True)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('FillWarrantyParts(' & IN:JobNumber & ')')

        ! Start Change 4420 BE(29/06/04)
        SET(Defaults,0)
        Access:Defaults.Next()
        ! Blank Part And Module if Fault Code 110 - TrkBs: 5887 (DBH: 16-06-2005)
        IF (INSTRING('SIGMA',CLIP(UPPER(def:User_Name)),1,1)) Or |
            (INSTRING('INTEC',CLIP(UPPER(def:User_Name)),1,1))
            sigma# = 1
        ELSE
            sigma# = 0
        END
        ! End Change 4420 BE(29/06/04)

        ! Start Change BE031 BE(05/08/2004)
        keyrepair# = false
        ! End Change BE031 BE(05/08/2004)

        FREE(WarrantyPart_Queue)
            WPRFirst = True
            LOOP WHILE LoadWARPARTS(job:Ref_Number, WPRFirst)
                WPRFirst = False
                !WriteDebug('FillWarrantyParts(LOOP  wpr:Part_Number="' & CLIP(wpr:Part_Number) & '", "' & CLIP(wpr:Description) & '")')
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = wpr:part_ref_number
                if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    !Check to see if this is the "accessory" version of the report - TrkBs: 5006 (DBH: 15-03-2005)
                    If func:Accessory = True
                        If sto:accessory <> 'YES'
                            Cycle
                        End!If sto:accessory = 'YES'
                    Else ! If func:Accessory = True
                        If sto:accessory = 'YES'
                            Cycle
                        End!If sto:accessory = 'YES'
                    End ! If func:Accessory = True
                    If sto:ExchangeUnit = 'YES'
                        Cycle
                    End!If sto:ExchangeUnit = 'YES'
                Else
                    !Don't continue if can't find item in stock. Don't know if it's an accessory - TrkBs: 5006 (DBH: 15-03-2005)
                    If func:Accessory = True
                        Cycle
                    End !If func:Accessory = True
                end
                CLEAR(WarrantyPart_Queue)
                    !    PartNumber                  STRING( 30) ! RL04 OPTIONAL MULTI (wpr:Part_Number)

                    ! Start Change 4420 BE(29/06/04)
                    !wprQ:PartNumber         = wpr:Part_Number
                    ! Changing (DBH 15/12/2005) #6869 - Blank fields for 110 AND 282
                    ! IF ((sigma# = 1) AND (wpr:Fault_Code3 = '110')) THEN
                    ! to (DBH 15/12/2005) #6869
                    IF ((sigma# = 1) AND (wpr:Fault_Code3 = '110' Or wpr:Fault_Code3 = '282') ) THEN
                    ! End (DBH 15/12/2005) #6869
                        wprQ:PartNumber         = ''
                        wprQ:RepairModule       = ''
                    ELSE
                        wprQ:PartNumber         = wpr:Part_Number
                        wprQ:RepairModule       = wpr:Fault_Code2
                    END
                    ! End Change 4420 BE(29/06/04)

                    
                    wprQ:Cost               = wpr:Quantity * wpr:Purchase_Cost
                    ! Start Change 4140 BE(27/04/04)
                    ! Start Change xxx BE(22/06/2004)
                    !wprQ:Cost = wpr:Quantity
                    wprQ:Qty = wpr:Quantity
                    ! Start Change xxx BE(22/06/2004)
                    ! End Change 4140 BE(27/04/04)

                    !    RepairSymptomCode           STRING(255) ! RL01 MANDATORY      (wpr:Fault_Code4)
                    !    FaultCode                   STRING(255) ! RL02 MANDATORY      (wpr:Fault_Code3)
                    !    KeyRepair                   STRING(  1) ! RL03 MANDATORY      (wpr:Fault_Code5) "1" Yes is a key repair/No "0" Default as "1"
                    wprQ:RepairSymptomCode  = ''
                    wprQ:FaultCode          = wpr:Fault_Code3
                    wprQ:KeyRepair          = wpr:Fault_Code4

                    ! Start Change BE031 BE(05/08/2004)
                    IF (wprQ:KeyRepair <> '0') THEN
                        keyrepair# = true
                    END
                    ! End Change BE031 BE(05/08/2004)

                    !    CCTReferenceNumber          STRING( 30) ! RL06 OPTIONAL       (wpr:Fault_Code1) Format "A999"
                    !    RepairModule                STRING( 30) ! RL07 OPTIONAL       (wpr:Fault_Code2) 
                    !    PartReplaced                STRING(  1) ! RL08 OPTIONAL       (wpr:Fault_Code6) "1" Yes in warranty period/No "0"
                    wprQ:CCTReferenceNumber = wpr:Fault_Code1
                    ! Start Change 4420 BE(29/06/04)
                    !wprQ:RepairModule       = wpr:Fault_Code2
                    ! End Change 4420 BE(29/06/04)
                    wprQ:PartReplaced       = wpr:Fault_Code5
                    ! Start Change BE031 BE(05/08/2004)
                    wprQ:FaultCode6         = wpr:Fault_Code6
                    ! End Change BE031 BE(05/08/2004)
                ADD(WarrantyPart_Queue, -wprQ:Cost, +wprQ:PartNumber)

                !WriteDebug('FillWarrantyParts(LOOP RECORDS(WarrantyPart_Queue) = "' & RECORDS(WarrantyPart_Queue) & '")')
            END ! LOOP
        SORT(WarrantyPart_Queue, -wprQ:Cost, +wprQ:PartNumber)
        !------------------------------------------------------------------
        !WriteDebug('FillWarrantyParts(RECORDS(WarrantyPart_Queue) = "' & RECORDS(WarrantyPart_Queue) & '")')
        ! Start Change BE031 BE(05/08/2004)
        !IF RECORDS(WarrantyPart_Queue) > 0
        IF (NOT keyrepair#) THEN
        ! End Change BE031 BE(05/08/2004)
            !WriteDebug('FillWarrantyParts(OK)')

            GET(WarrantyPart_Queue, 1)
                wprQ:KeyRepair = 1
            PUT(WarrantyPart_Queue)

            SORT(WarrantyPart_Queue, +wprQ:PartNumber, +wprQ:Cost)
            ! Start Change BE031 BE(05/08/2004)
            !RETURN True
            ! Start Change BE031 BE(05/08/2004)
        END !IF
        !------------------------------------------------------------------
        ! Start Change BE031 BE(05/08/2004)
        IF RECORDS(WarrantyPart_Queue) > 0 THEN
            RETURN true
        END
        ! End Change BE031 BE(05/08/2004)
        RETURN False
        !------------------------------------------------------------------
GetAccessoryCode            PROCEDURE( IN:Accessory )! STRING
    CODE
        !------------------------------------------------------------------
        ! Accessory (SELECT Distinct Accessory FROM "JOBACC" )
        !------------------------------ 
        ! ANTENNA
        ! BATTERY
        ! BC
        ! BLACK BOX ONLY
        ! CHARGER
        ! COVER - BACK
        ! COVER- NON-STANDARD
        ! DTX-1
        ! FC
        ! FULL BOX KIT
        ! FULL KIT
        ! HANDEST ONLY
        ! HANDSET (6090)
        ! HEADPHONES
        ! HEADSET
        ! LEATHER CASE
        ! MANUALS
        ! MEM CARD
        ! NO COVERS
        ! NON STANDARD COVER
        ! NONE
        ! RETAIL BOX
        ! SIM
        !------------------------------------------------------------------
        CASE IN:Accessory
        OF 'ANTENNA'
            RETURN 'AN'
        OF 'BATTERY'
            RETURN 'BP'
        OF 'BC'
            RETURN 'AC'
        OF 'BLACK BOX ONLY'
            RETURN 'TX'
        OF 'CHARGER'
            RETURN 'DC'
        OF 'DTX-1'
            RETURN 'AC'
        OF 'FC'
            RETURN 'AC'
        OF 'FULL BOX KIT' OROF 'FULL KIT'
            RETURN 'AC'
        OF 'LEATHER CASE'
            RETURN 'AC'
        OF 'MANUALS'
            RETURN 'AC'
        OF 'MEM CARD'
            RETURN 'MC'
        OF 'NO COVERS' OROF 'NONE'
            RETURN ''
        OF 'RETAIL BOX'
            RETURN 'AC'
        OF 'SIM'
            RETURN 'SC'
        END !CASE
        !------------------------------------------------------------------
        ! HANDEST ONLY
        ! HANDSET (6090)
        ! HEADPHONES
        ! HEADSET
        IF MATCH(IN:Accessory, '*HANDSET*')
            RETURN 'TX'
        ELSIF MATCH(IN:Accessory, '*PHONE*')
            RETURN 'TX'
        END !IF
        !------------------------------------------------------------------
        IF MATCH(IN:Accessory, '*HEADSET*')
            RETURN 'HS'
        END !IF
        !------------------------------------------------------------------
        ! NON STANDARD COVER
        ! COVER - BACK
        ! COVER- NON-STANDARD
        IF MATCH(IN:Accessory, '*COVER*')
            RETURN 'EC'
        END !IF
        !------------------------------------------------------------------
        ! ANTENNA'
        IF MATCH(IN:Accessory, '*ANTENNA*')
            RETURN 'AN'
        END !IF
        !------------------------------------------------------------------
        RETURN ''
        !------------------------------------------------------------------
GetAttribute        PROCEDURE( IN:Name, IN:Value )! STRING
    CODE
        !------------------------------------------------------------------
        IF CLIP(IN:Name) = ''
            RETURN ''
        END !IF

        RETURN ' ' & CLIP(IN:Name) & '="' & CLIP(IN:Value) & '"'
        !------------------------------------------------------------------
GetCustomerName PROCEDURE()!STRING
TEMP STRING(100)
    CODE
        !------------------------------------------------------------------
        ! STRING(30)!(job:Title+job:Initials+job:Surname) / job:Company_Name
        !------------------------------------------------------------------
        TEMP = AppendString( job:Title, job:Initial, ' ')
        TEMP = AppendString(      TEMP, job:Surname, ' ')

        IF CLIP(TEMP) = ''
            RETURN job:Company_Name
        END !IF

        RETURN CLIP(TEMP)
        !------------------------------------------------------------------
GetExchangeDate             PROCEDURE( IN:JobNumber )! STRING
ReturnValue STRING(20)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('GetExchangeDate(' & IN:JobNumber & ')')
        !------------------------------------------------------------------
        ! RecordNumberKey KEY( aus:RecordNumber                           ),NOCASE,PRIMARY
        ! DateChangedKey  KEY( aus:RefNumber,   aus:Type, aus:DateChanged ),DUP,NOCASE
        ! NewStatusKey    KEY( aus:RefNumber,   aus:Type, aus:NewStatus   ),DUP,NOCASE
        !
        Access:AUDSTATS.ClearKey(aus:NewStatusKey)
            aus:RefNumber = IN:JobNumber
            aus:Type       = 'EXC'
            aus:NewStatus  = '901 DESPATCHED'
        SET(aus:NewStatusKey, aus:NewStatusKey)

        LOOP WHILE Access:AUDSTATS.NEXT() = Level:Benign
            IF NOT    aus:RefNumber = IN:JobNumber
                !WriteDebug('GetExchangeDate(EOI aud:Ref_Number = ' & aus:RefNumber & ')')

                BREAK
            ELSIF NOT aus:Type       = 'EXC'
                !WriteDebug('GetExchangeDate(EOI aud:Type = "' & CLIP(aus:Type) & '")')

                BREAK
            ELSIF NOT aus:NewStatus  = '901 DESPATCHED'
                !WriteDebug('GetExchangeDate(EOI aus:NewStatus = ' & aus:NewStatus & ')')

                BREAK
            END !IF

            ReturnValue = DateTimeToNokiaString(aus:DateChanged, aus:TimeChanged)

            !WriteDebug('GetExchangeDate(LOOP=' & CLIP(ReturnValue) & '), aud:Ref_Number=(' & aus:RefNumber & ')')
        END !IF

        !WriteDebug('GetExchangeDate(ReturnValue=' & CLIP(ReturnValue) & ')')

        RETURN CLIP(ReturnValue)
        !------------------------------------------------------------------
GetExpectedShipDate PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        ! STRING('Delivery Not Known')!put "Delivery Not Known" if job:Current_Status IN("Awaiting Spares", "Spares Requested")
        !
        CASE job:Current_Status
        OF 'Awaiting Spares' OROF 'Spares Requested'
            RETURN 'Delivery Not Known'
        ELSE
            RETURN ''
        END !CASE
        !------------------------------------------------------------------
GetRecordNumber        PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',  'RecordNumber',                                  '0', '.\sbcr0060.ini')
        !------------------------------------------------------------------

GetEmailServer            PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',   'EmailServer',                      '192.168.0.229', '.\sbcr0060.ini')
        !------------------------------------------------------------------

GetEmailPort              PROCEDURE()! LONG
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',     'EmailPort',                                 '25', '.\sbcr0060.ini')
        !------------------------------------------------------------------

GetEmailFrom              PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',     'EmailFrom',   'j.griffiths@pccontrolsystems.com', '.\sbcr0060.ini')
        !------------------------------------------------------------------

GetEmailTo                PROCEDURE()! STRING
    CODE
        !------------------------------------------------------------------
        RETURN GETINI( 'Parameters',       'EmailTo',   'j.griffiths@pccontrolsystems.com', '.\sbcr0060.ini')
        !------------------------------------------------------------------

GetTimestamp        PROCEDURE( IN:Date, IN:Time )! STRING
TempDate STRING(10)
TempTime STRING(07)
    CODE
        !------------------------------------------------------------------
        IF CLIP(IN:Date) = 0
            RETURN ''
        END !IF

        TempDate = FORMAT(IN:Date, @D02) ! mm/dd/yyyy
        TempTime = FORMAT(IN:Time, @T01)  ! "hh:mm    " !TempTime = FORMAT(IN:Time, @T03) ! "hh:mmXM"

        RETURN TempDate[4:5] & '.' & TempDate[1:2] & '.' & TempDate[7:10] & ' ' & CLIP(TempTime)
        !------------------------------------------------------------------
GetTransactionCode PROCEDURE()!STRING(2)
    CODE
        !------------------------------------------------------------------
        CASE job:Current_Status
        OF 'EXCHNGE UNIT SENT'      ! 'EXCH. UNIT DESPATCHED'
            RETURN 'PR'
        OF 'ORIGINAL UNIT RETURNED' !
            RETURN 'RC'
        END !CASE

        IF      job:Unit_Type <> 'ACCESSORY'
            RETURN ''
        ELSIF job:Charge_Type <> 'EXCHANGE'
            RETURN ''
        ELSE
            RETURN 'AC'
        END !IF
        !------------------------------------------------------------------
GetXMLElementQueue          PROCEDURE( IN:Tag, IN:Default )! STRING
    CODE
        !------------------------------------------------------------------
        !WriteDebug('GetXMLElementQueue(IN:Tag="' & CLIP(IN:Tag) & '", IN:Default="' & CLIP(IN:Default) & '")')

        IF RECORDS(XMLElement_Queue) < 1
            !WriteDebug('GetXMLElementQueue(XMLElement_Queue Is Empty)')

            GOTO final
        END !IF
        !------------------------------------------------------------------
        CLEAR(XMLElement_Queue)
            elementQ:Code = IN:Tag
        GET(XMLElement_Queue, +elementQ:Code)

        CASE ERRORCODE()
        OF 00
            !WriteDebug('GetXMLElementQueue(Found="' & CLIP(elementQ:Value) & '")')
            CASE SUB(elementQ:Value, 1, 1)
            OF '$'
                RETURN                  SUB(elementQ:Value, 2, LEN(elementQ:Value))

            OF '&'
                RETURN EVALUATE(        SUB(elementQ:Value, 2, LEN(elementQ:Value)))

            OF '@'
                RETURN EvaluateFunction(SUB(elementQ:Value, 2, LEN(elementQ:Value)))

            ELSE
                RETURN CLIP(elementQ:Value)
            END !CASE

        OF 30
            ! NULL

        ELSE
            !WriteDebug('GetXMLElementQueue(ERROR (' & ERRORCODE() & '), "' & CLIP(ERROR()) & '")')

            CancelPressed = True

            RETURN ''
        END !CASE
        !------------------------------------------------------------------
        ! Not Found evaluate default value
        !
final   !WriteDebug('GetXMLElementQueue(NOT Found="' & CLIP(IN:Default) & '")')

!        CLEAR(XMLElement_Queue)
!            elementQ:Code  = IN:Tag
!            elementQ:Value = IN:Default
!        ADD(XMLElement_Queue, +elementQ:Code)

        CASE SUB(IN:Default, 1, 1)
        OF '$'
            RETURN SUB(                 IN:Default, 2, LEN(IN:Default))

        OF '&'
            RETURN EVALUATE(        SUB(IN:Default, 2, LEN(IN:Default)))

        OF '@'
            RETURN EvaluateFunction(SUB(IN:Default, 2, LEN(IN:Default)))

        ELSE
            RETURN CLIP(IN:Default)
        END !CASE
        !------------------------------------------------------------------
IsMessageHTMLFull   PROCEDURE()! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        IF LOC:MaxHTMLSize = 0
           LOC:MaxHTMLSize = (SIZE(EmailMessageHTML) * 0.95)
           !message('(SIZE(EmailMessageHTML) * 0.8)="' & LOC:MaxHTMLSize & '"')
           !LOC:MaxHTMLSize = (16384 * 0.8)
        END !IF

        IF TotalCharacters > LOC:MaxHTMLSize
            RETURN True
        ELSE
            RETURN False
        END !IF
        !------------------------------------------------------------------
LoadExchange        PROCEDURE( IN:Exchange )! LONG ! BOOL
    CODE
        ! Ref_Number_Key KEY( xch:Ref_Number ),NOCASE,PRIMARY

        Access:EXCHANGE.ClearKey(xch:Ref_Number_Key)
            xch:Ref_Number = IN:Exchange
        IF Access:EXCHANGE.TryFetch(xch:Ref_Number_Key) = level:Benign
            RETURN True
        ELSE
            RETURN False
        END !IF
LoadJOBACC              PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !WriteDebug('LoadJOBACC(IN:JobNumbe="' & IN:JobNumber & '", "' & IN:First & '")')

        IF IN:First = True
            !WriteDebug('LoadJOBACC(SETUP)')

            Access:JOBACC.ClearKey(jac:Ref_Number_Key)
                jac:Ref_Number = IN:JobNumber
            SET(jac:Ref_Number_Key, jac:Ref_Number_Key)
        END !IF

        IF NOT Access:JOBACC.NEXT() = Level:Benign
            !WriteDebug('LoadJOBACC(EOF)')
            RETURN False
        END !IF

        IF NOT jac:Ref_Number = IN:JobNumber
            !WriteDebug('LoadJOBACC(EOI)')
            RETURN False
        END !IF

        !WriteDebug('LoadJOBACC(OK)')

        RETURN True
        !-----------------------------------------------
LoadJOBNOTES      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = IN:JobNumber

        IF Access:JOBNOTES.Fetch(jbn:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadJOBSE      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = IN:JobNumber

        IF Access:JOBSE.Fetch(jobe:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
!LoadWEBJOB      PROCEDURE( IN:JobNumber )! LONG ! BOOL
!    CODE
!        !-----------------------------------------------
!        Access:WEBJOB.ClearKey(wob:RefNumberKey)
!            wob:RefNumber = IN:JobNumber
!        SET(wob:RefNumberKey, wob:RefNumberKey)
!
!        IF Access:WEBJOB.NEXT() <> Level:Benign
!            RETURN False
!        END !IF
!
!        IF NOT wob:RefNumber = IN:JobNumber
!            RETURN False
!        END !IF
!
!        RETURN True
!        !-----------------------------------------------
LoadWARPARTS            PROCEDURE( IN:JobNumber, IN:First)! LONG ! BOOL
    CODE
        !------------------------------------------------------------------
        ! Order_Number_Key         KEY( wpr:Ref_Number, wpr:Order_Number       ),DUP,NOCASE
        ! Order_Part_Key           KEY( wpr:Ref_Number, wpr:Order_Part_Number  ),DUP,NOCASE
        ! Part_Number_Key          KEY( wpr:Ref_Number, wpr:Part_Number        ),DUP,NOCASE
        ! RefPartRefNoKey          KEY( wpr:Ref_Number, wpr:Part_Ref_Number    ),DUP,NOCASE
        ! PendingRefNoKey          KEY( wpr:Ref_Number, wpr:Pending_Ref_Number ),DUP,NOCASE
        IF IN:First
            !WriteDebug('LoadWARPARTS(INITIALIZE)')

            Access:WARPARTS.ClearKey(wpr:RefPartRefNoKey)
                wpr:Ref_Number = IN:JobNumber
            SET(wpr:RefPartRefNoKey, wpr:RefPartRefNoKey)
        END !IF

        IF NOT Access:WARPARTS.NEXT() = Level:Benign
            !WriteDebug('LoadWARPARTS(EOF)')
            RETURN False
        END !IF

        IF NOT wpr:Ref_Number = IN:JobNumber
            !WriteDebug('LoadWARPARTS(EOI)')
            RETURN False
        END !IF

        !WriteDebug('LoadWARPARTS(OK)')
        RETURN True
        !------------------------------------------------------------------
NumberToBool    PROCEDURE(IN:Number)! STRING
    CODE
        !------------------------------------------------------------------
        IF IN:Number = True
            RETURN '1' ! True'
        ELSE
            RETURN '0' ! False'
        END !IF
        !------------------------------------------------------------------
SectionToXMLElementQueue       PROCEDURE(IN:Section, IN:CodePrefix)
MyItems LONG
MyCode  LIKE(elementQ:Code)
MyValue LIKE(elementQ:Value)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('SectionToXMLElementQueue(IN:Section="' & CLIP(IN:Section) & '", IN:CodePrefix="' & CLIP(IN:CodePrefix) & '")')

        MyItems = GETINI(IN:Section, 'Items', '0', '.\sbcr0060.ini')
        !WriteDebug('SectionToXMLElementQueue(Items=' & MyItems & ')')

        LOOP x# = 1 TO MyItems
            MyCode  = IN:CodePrefix & FORMAT(x#, @N02)
            MyValue = GETINI(IN:Section, MyCode, '', '.\sbcr0060.ini')

            IF (MyValue <> '')
                CLEAR(XMLElement_Queue)
                    !WriteDebug('SectionToXMLElementQueue(Add=' & x# & ',Code="' & CLIP(MyCode) & '", Value="' & CLIP(MyValue) & '")')

                    elementQ:Code  = MyCode
                    elementQ:Value = MyValue
                ADD(XMLElement_Queue, +elementQ:Code)
            END !IF
        END ! LOOP

        SORT(XMLElement_Queue, +elementQ:Code)
        !------------------------------------------------------------------
SetEmailServer    PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',  'EmailServer',    param:EmailServer, '.\sbcr0060.ini')
        !------------------------------------------------------------------

SetEmailPort      PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',    'EmailPort',      param:EmailPort, '.\sbcr0060.ini')
        !------------------------------------------------------------------

SetEmailFrom      PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',    'EmailFrom',      param:EmailFrom, '.\sbcr0060.ini')
        !------------------------------------------------------------------

SetEmailTo        PROCEDURE()!
    CODE
        !------------------------------------------------------------------
        PUTINI( 'Parameters',      'EmailTo',        param:EmailTo, '.\sbcr0060.ini')
        !------------------------------------------------------------------
StringToBool            PROCEDURE( IN:String )! LONG ! BOOL
    CODE
        IF LEN(IN:String) < 1
            RETURN False
        END !IF

        CASE UPPER(IN:String[ 1 ])
        OF '1' OROF 'Y'
            RETURN True
        ELSE
            RETURN False
        END !CASE
SWAPDateTime        PROCEDURE()! STRING
    CODE
        !-----------------------------------------------
        ! Called by SW01
        RETURN GetExchangeDate(job:Ref_Number)
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI(CLIP(LOC:ProgramName), debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
!-----------------------------------------------
WriteTABLE PROCEDURE(IN:Close)
    CODE
        !------------------------------------------------------------------
        IF IN:Close = True
            html:LINE = CLIP(html:LINE) & ('</TABLE><13,10>')
            LinePrint('</TABLE>', LOC:HTMLFilename)
        ELSE
            html:LINE = CLIP(html:LINE) & ('<TABLE BORDER="1"><13,10>')
            LinePrint('<TABLE BORDER="1">', LOC:HTMLFilename)
        END !IF
        !------------------------------------------------------------------
WriteTD PROCEDURE(IN:Value)
    CODE
        !------------------------------------------------------------------
        html:LINE = CLIP(html:LINE) & '<TD>' & CLIP(IN:Value) & '</TD>'

        LinePrint('<TD>' & CLIP(IN:Value) & '</TD>', LOC:HTMLFilename)
        !------------------------------------------------------------------
WriteTH PROCEDURE(IN:Value)
    CODE
        !------------------------------------------------------------------
        html:LINE = CLIP(html:LINE) & '<TH>' & CLIP(IN:Value) & '</TH>'

        LinePrint('<TH>' & CLIP(IN:Value) & '</TH>', LOC:HTMLFilename)
        !------------------------------------------------------------------
WriteTR PROCEDURE(IN:Close)
    CODE
        !------------------------------------------------------------------
        IF IN:Close = True
            html:LINE = CLIP(html:LINE) & '</TR><13,10>'

            LinePrint('</TR>', LOC:HTMLFilename)
        ELSE
            html:LINE = CLIP(html:LINE) & '<TR>'

            LinePrint('<TR>', LOC:HTMLFilename)
        END !IF
        !------------------------------------------------------------------
!-----------------------------------------------
WriteXMLTag PROCEDURE(IN:Tag, IN:Attributes, IN:Value)
Temp       STRING(1024)
Split      LONG(200)
TempLength LONG(0)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('WriteXMLTag(Tag="' & CLIP(IN:Tag) & '", Attrib="' & CLIP(IN:Attributes) & '", Value="' & CLIP(IN:Value) & '")')

        Temp = ALL(' ', xml:Depth * 4) & '<' & CLIP(IN:Tag)

        IF LEN(CLIP(IN:Attributes)) > 1
             Temp = CLIP(Temp) & ' ' & CLIP(IN:Attributes) 
        END !IF
        !------------------------------------------------------------------
        ! Comment out 10 Jan 03 - John
        ! Remove Comment to allow <Tag/> for empty nodes.
        !IF CLIP(IN:Value) = ''
        !    Temp = CLIP(Temp) & '/>'
        !    LinePrint(CLIP(Temp), xml:Filename)
        !
        !    RETURN
        !END !IF
        !------------------------------------------------------------------
        Temp = CLIP(Temp) & '>' & CLIP(XMLEncode(IN:Value)) &  '</' & CLIP(IN:Tag) & '>'

        TempLength = LEN(CLIP(Temp))

        IF TempLength < Split
            ! Start Change 4536 BE(22/07/2004)
            !LinePrint(CLIP(Temp), xml:Filename)
            WriteLog(Temp)
            ! End Change 4536 BE(22/07/2004)
            RETURN
        END !IF
        !------------------------------------------------------------------
        ! check for entities such as &apos; at the split location
!        LOOP x# = Split TO Split - 6 BY -1
!            IF Temp[x#] = '&'
!                Split = x#-1
!                BREAK
!            END !IF
!        END !LOOP
        LOOP x# = Split TO Split - 6 BY -1
            CASE Temp[x#]
            OF '&' OROF '<'
                Split = x#-1
                BREAK
            ELSE
                ! NULL
            END !CASE
        END !LOOP

        ! Start Change 4536 BE(22/07/2004)
        !LinePrint(SUB(Temp,       1,            Split), xml:Filename)
        !LinePrint(SUB(Temp, Split+1, TempLength-Split), xml:Filename)
        WriteLog(SUB(Temp, 1, Split))
        WriteLog(SUB(Temp, Split+1, TempLength-Split))
        ! End Change 4536 BE(22/07/2004)
        !------------------------------------------------------------------

!--------------------------------------------------------------------------
! General Solution for arbitary values that can be even bigger then 255
!        !------------------------------------------------------------------
!        StartPos = 1
!        Split    = 200
!
!        LOOP WHILE StartPos <= TempLength
!            ! check for :
!            !      1 entities such as &apos; at the split position
!            !      2 Closing Tag at split position
!            LOOP x# = Split TO Split - 6 BY -1
!               CASE Temp[x#]
!                OF '&' OROF '<'
!                    Split = x#-1
!                    BREAK
!               ELSE
!                    ! NULL
!                END !CASE
!            END !LOOP
!
!            LinePrint(SUB(Temp, StartPos, Split), xml:Filename)
!
!            StartPos = Split + 1
!            Split    += 200
!            IF Split > TempLength
!                Split = TempLength
!            END !IF
!        END !LOOP
!        !------------------------------------------------------------------
WriteXMLTagFromXMLElementQueue PROCEDURE(IN:Tag, IN:Attributes)
MyValue LIKE(elementQ:Value)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('WriteXMLTagFronXMLElementQueue(Tag="' & CLIP(IN:Tag) & '", Attrib="' & CLIP(IN:Attributes) & '")')

        MyValue = GetXMLElementQueue(IN:Tag, '')

        WriteXMLTag(IN:Tag, IN:Attributes, MyValue)

        !WriteDebug('WriteXMLTagFronXMLElementQueue(EXIT)')
        !------------------------------------------------------------------
WriteStartXMLTag PROCEDURE(IN:Tag, IN:Attributes)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('WriteStartXMLTag(Tag="' & CLIP(IN:Tag) & '", Attrib="' & CLIP(IN:Attributes) & '")')

        IF LEN(CLIP(IN:Attributes)) > 1
            ! Start Change 4536 BE(22/07/2004)
            !LinePrint(ALL(' ', xml:Depth * 4) & '<' & CLIP(IN:Tag) & ' ' & CLIP(IN:Attributes) & '>', xml:Filename)
            WriteLog(ALL(' ', xml:Depth * 4) & '<' & CLIP(IN:Tag) & ' ' & CLIP(IN:Attributes) & '>')
            ! End Change 4536 BE(22/07/2004)
        ELSE
            ! Start Change 4536 BE(22/07/2004)
            !LinePrint(ALL(' ', xml:Depth * 4) & '<' & CLIP(IN:Tag)                             & '>', xml:Filename)
            WriteLog(ALL(' ', xml:Depth * 4) & '<' & CLIP(IN:Tag) & '>')
            ! End Change 4536 BE(22/07/2004)
        END !IF

        xml:Depth += 1
        !------------------------------------------------------------------
WriteEndXMLTag PROCEDURE(IN:Tag)
    CODE
        !------------------------------------------------------------------
        !WriteDebug('WriteEndXMLTag(Tag="' & CLIP(IN:Tag) & '")')

        xml:Depth += -1

        ! Start Change 4536 BE(22/07/2004)
        !LinePrint(ALL(' ', xml:Depth * 4) & '</' & CLIP(IN:Tag) & '>', xml:Filename)
        WriteLog(ALL(' ', xml:Depth * 4) & '</' & CLIP(IN:Tag) & '>')
        ! End Change 4536 BE(22/07/2004)
        !------------------------------------------------------------------

XMLEncode               PROCEDURE( IN:String )! STRING
LenIN LONG
    CODE
        !------------------------------------------------------------------
        LenIN = LEN(IN:String)

        LOOP x# = 1 TO LenIN
            CASE IN:String[ x# ]
            OF '&'
                RETURN SUB(IN:String, 1, x#-1) & '&amp;'  & XMLEncode(SUB(IN:String, x#+1, LenIN-x#))
            OF '<'
                RETURN SUB(IN:String, 1, x#-1) & '&lt;'   & XMLEncode(SUB(IN:String, x#+1, LenIN-x#))
            OF '>'
                RETURN SUB(IN:String, 1, x#-1) & '&gt;'   & XMLEncode(SUB(IN:String, x#+1, LenIN-x#))
            OF '"'
                RETURN SUB(IN:String, 1, x#-1) & '&quot;' & XMLEncode(SUB(IN:String, x#+1, LenIN-x#))
            OF ''''
                RETURN SUB(IN:String, 1, x#-1) & '&apos;' & XMLEncode(SUB(IN:String, x#+1, LenIN-x#))
            ELSE
                ! NULL
            END !CASE

        END !LOOP

        RETURN SUB(IN:String, 1, LenIN)
        !------------------------------------------------------------------
!-----------------------------------------------
! Logfile Procedures
OpenLog     PROCEDURE(arg:f, arg:newflag)
    CODE
    LOGFILE{PROP:NAME} = CLIP(arg:f)

    IF (EXISTS(arg:f)) THEN
        IF (arg:newflag) THEN
            REMOVE(LOGFILE)
            CREATE(LOGFILE)
        END
    ELSE
        CREATE(LOGFILE)
    END

    OPEN(LOGFILE)
    IF (ERRORCODE()) THEN
        RETURN false
    END
    RETURN true

CloseLog     PROCEDURE()
    CODE
    CLOSE(LOGFILE)
    RETURN


WriteLog     PROCEDURE(arg:s, arg:logflag)
    CODE
    CLEAR(LOGFILE)
    IF (arg:logflag) THEN
        log:recbuff = FORMAT(TODAY(), @d06) & ' ' & FORMAT(CLOCK(), @t04) & ' ' & CLIP(arg:s)
    ELSE
        log:recbuff = CLIP(arg:s)
    END
    ADD(LOGFILE)
    RETURN
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
