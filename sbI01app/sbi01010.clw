

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBI01010.INC'),ONCE        !Local module procedure declarations
                     END


EDI_Defaults PROCEDURE                                !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::edi:Record  LIKE(edi:RECORD),STATIC
QuickWindow          WINDOW('Motorola EDI Defaults'),AT(,,158,104),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),HLP('EDI_Defaults'),TILED,SYSTEM,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,152,68),USE(?CurrentTab),SPREAD
                         TAB('Motorola'),USE(?Tab:1)
                           PROMPT('&Supplier Code'),AT(8,20),USE(?EDI:Supplier_Code:Prompt)
                           ENTRY(@s8),AT(84,20,64,10),USE(edi:Supplier_Code),FONT('Tahoma',,,FONT:bold),UPR
                           PROMPT('&Country Code'),AT(8,36),USE(?EDI:Country_Code:Prompt)
                           ENTRY(@S3),AT(84,36,64,10),USE(edi:Country_Code),FONT('Tahoma',,,FONT:bold),REQ,UPR
                           PROMPT('&Dealer ID'),AT(8,52),USE(?EDI:Dealer_ID:Prompt)
                           ENTRY(@s3),AT(84,52,64,10),USE(edi:Dealer_ID),FONT('Tahoma',,,FONT:bold),REQ,UPR
                         END
                       END
                       BUTTON('&OK'),AT(40,80,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(96,80,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       PANEL,AT(4,76,152,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?EDI:Supplier_Code:Prompt{prop:FontColor} = -1
    ?EDI:Supplier_Code:Prompt{prop:Color} = 15066597
    If ?edi:Supplier_Code{prop:ReadOnly} = True
        ?edi:Supplier_Code{prop:FontColor} = 65793
        ?edi:Supplier_Code{prop:Color} = 15066597
    Elsif ?edi:Supplier_Code{prop:Req} = True
        ?edi:Supplier_Code{prop:FontColor} = 65793
        ?edi:Supplier_Code{prop:Color} = 8454143
    Else ! If ?edi:Supplier_Code{prop:Req} = True
        ?edi:Supplier_Code{prop:FontColor} = 65793
        ?edi:Supplier_Code{prop:Color} = 16777215
    End ! If ?edi:Supplier_Code{prop:Req} = True
    ?edi:Supplier_Code{prop:Trn} = 0
    ?edi:Supplier_Code{prop:FontStyle} = font:Bold
    ?EDI:Country_Code:Prompt{prop:FontColor} = -1
    ?EDI:Country_Code:Prompt{prop:Color} = 15066597
    If ?edi:Country_Code{prop:ReadOnly} = True
        ?edi:Country_Code{prop:FontColor} = 65793
        ?edi:Country_Code{prop:Color} = 15066597
    Elsif ?edi:Country_Code{prop:Req} = True
        ?edi:Country_Code{prop:FontColor} = 65793
        ?edi:Country_Code{prop:Color} = 8454143
    Else ! If ?edi:Country_Code{prop:Req} = True
        ?edi:Country_Code{prop:FontColor} = 65793
        ?edi:Country_Code{prop:Color} = 16777215
    End ! If ?edi:Country_Code{prop:Req} = True
    ?edi:Country_Code{prop:Trn} = 0
    ?edi:Country_Code{prop:FontStyle} = font:Bold
    ?EDI:Dealer_ID:Prompt{prop:FontColor} = -1
    ?EDI:Dealer_ID:Prompt{prop:Color} = 15066597
    If ?edi:Dealer_ID{prop:ReadOnly} = True
        ?edi:Dealer_ID{prop:FontColor} = 65793
        ?edi:Dealer_ID{prop:Color} = 15066597
    Elsif ?edi:Dealer_ID{prop:Req} = True
        ?edi:Dealer_ID{prop:FontColor} = 65793
        ?edi:Dealer_ID{prop:Color} = 8454143
    Else ! If ?edi:Dealer_ID{prop:Req} = True
        ?edi:Dealer_ID{prop:FontColor} = 65793
        ?edi:Dealer_ID{prop:Color} = 16777215
    End ! If ?edi:Dealer_ID{prop:Req} = True
    ?edi:Dealer_ID{prop:Trn} = 0
    ?edi:Dealer_ID{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'EDI_Defaults',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'EDI_Defaults',1)
    SolaceViewVars('FilesOpened',FilesOpened,'EDI_Defaults',1)
    SolaceViewVars('ActionMessage',ActionMessage,'EDI_Defaults',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EDI:Supplier_Code:Prompt;  SolaceCtrlName = '?EDI:Supplier_Code:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?edi:Supplier_Code;  SolaceCtrlName = '?edi:Supplier_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EDI:Country_Code:Prompt;  SolaceCtrlName = '?EDI:Country_Code:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?edi:Country_Code;  SolaceCtrlName = '?edi:Country_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?EDI:Dealer_ID:Prompt;  SolaceCtrlName = '?EDI:Dealer_ID:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?edi:Dealer_ID;  SolaceCtrlName = '?edi:Dealer_ID';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Motorola EDI Defaults'
  OF DeleteRecord
    GlobalErrors.Throw(Msg:DeleteIllegal)
    RETURN
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    Relate:DEFEDI.Open
    SET(DEFEDI)
    CASE Access:DEFEDI.Next()
    OF LEVEL:NOTIFY OROF LEVEL:FATAL              !If end of file was hit
      GlobalRequest = InsertRecord
    ELSE
      GlobalRequest = ChangeRecord
    END
    Relate:DEFEDI.Close
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  GlobalErrors.SetProcedureName('EDI_Defaults')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'EDI_Defaults')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?EDI:Supplier_Code:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(edi:Record,History::edi:Record)
  SELF.AddHistoryField(?edi:Supplier_Code,1)
  SELF.AddHistoryField(?edi:Country_Code,3)
  SELF.AddHistoryField(?edi:Dealer_ID,4)
  SELF.AddUpdateFile(Access:DEFEDI)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:DEFEDI.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:DEFEDI
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.DeleteAction = Delete:None
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
    IF SELF.Request = ChangeRecord
      SET(DEFEDI)
      Access:DEFEDI.Next()
    END
  !--------------------------------------------------------------------------
  ! Tintools Single Control Record Update
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFEDI.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'EDI_Defaults',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    edi:Dealer_ID = 'N/A'
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'EDI_Defaults')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

