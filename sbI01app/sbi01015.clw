

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01015.INC'),ONCE        !Local module procedure declarations
                     END


Sagem_EDI            PROCEDURE  (f_batch_number)      ! Declare Procedure
fname                STRING(255)
save_job_id          USHORT,AUTO
pos                  STRING(255)
save_aud_id          USHORT,AUTO
count                REAL
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
EdiVersion           LONG
SigmaFlag            BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(1030)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
EDI_No         String(15)
Order_No       STRING(28)
Customer       STRING(28)
Warranty       STRING(5)
IMEI_In        STRING(18)
Sag_Ref_In     STRING(12)
Date_Code      STRING(10)
IMEI_Out       STRING(18)
Sag_Ref_Out    STRING(12)
Battery        STRING(12)
Charger        STRING(12)
Antenna        STRING(6)
DOP            STRING(11)
Booked         STRING(11)
Est_Out        STRING(11)
Est_In         STRING(11)
Despatched     STRING(11)
Cust_Code      STRING(8)
Fault_Code     STRING(8)
Soft_In        STRING(14)
Soft_Out       STRING(14)
Repair_Level   STRING(8)
Part1          STRING(12)
Part2          STRING(12)
Part3          STRING(12)
Part4          STRING(12)
Part5          STRING(12)
Part6          STRING(12)
Part7          STRING(12)
Part8          STRING(12)
Part9          STRING(12)
Refurb1        STRING(12)
Refurb2        STRING(12)
Refurb3        STRING(12)
Refurb4        STRING(12)
Refurb5        STRING(12)
Refurb6        STRING(12)
Refurb7        STRING(12)
RFS            STRING(12)
ModelNumber    String(30)
Re_Rep         STRING(30)
            .
Out_Header GROUP,OVER(Ouf:Out_Group),PRE(L2)
field1         STRING(11)
field2         STRING(33)
field3         STRING(11)
filer          STRING(255)
filer1         STRING(23)
            .

! Start Change 4563 BE(23/08/2004)
LEN_A0           EQUATE(8)
LEN_A1           EQUATE(27)
LEN_A2           EQUATE(27)
LEN_A3           EQUATE(4)
LEN_A4           EQUATE(17)
LEN_A5           EQUATE(11)
LEN_A6           EQUATE(9)
LEN_A7           EQUATE(17)
LEN_A8           EQUATE(11)
LEN_A9           EQUATE(4)
LEN_A10          EQUATE(5)
LEN_A11          EQUATE(5)
LEN_A12          EQUATE(12)
LEN_A13          EQUATE(12)
LEN_A14          EQUATE(12)
LEN_A15          EQUATE(12)
LEN_A16          EQUATE(12)
LEN_A17          EQUATE(5)
LEN_A18          EQUATE(5)
LEN_A19          EQUATE(8)
LEN_A20          EQUATE(8)
LEN_A21          EQUATE(5)
LEN_A22          EQUATE(11)
LEN_A23          EQUATE(11)
LEN_A24          EQUATE(11)
LEN_A25          EQUATE(11)
LEN_A26          EQUATE(11)
LEN_A27          EQUATE(11)
LEN_A28          EQUATE(11)
LEN_A29          EQUATE(11)
LEN_A30          EQUATE(11)
LEN_A31          EQUATE(11)
LEN_A32          EQUATE(11)
LEN_A33          EQUATE(11)
LEN_A34          EQUATE(11)
LEN_A35          EQUATE(11)
LEN_A36          EQUATE(11)
LEN_A37          EQUATE(11)
LEN_A38          EQUATE(12)
LEN_A39          EQUATE(32)
LEN_A40          EQUATE(32)
LEN_A41          EQUATE(32)
LEN_A42          EQUATE(32)
LEN_A43          EQUATE(32)
LEN_A44          EQUATE(32)

Out_Detail_V2 GROUP,PRE(L1V2)
A0             STRING(LEN_A0+1)     ! ARC Code - EDI Account Number
A1             STRING(LEN_A1+1)     ! Job Number
A2             STRING(LEN_A2+1)     ! Customer Name
A3             STRING(LEN_A3+1)     ! Warranty Status - 0 = Out of Warranty 1 = Under Warranty
A4             STRING(LEN_A4+1)     ! Incoming IMEI
A5             STRING(LEN_A5+1)     ! Incoming SAGEM Ref - MSN
A6             STRING(LEN_A6+1)     ! Production Date - Job Fault Code 8
A7             STRING(LEN_A7+1)     ! Outgoing IMEI
A8             STRING(LEN_A8+1)     ! Outgoing SAGEM Ref - MSN
A9             STRING(LEN_A9+1)     ! Incoming Battery presence - Job Fault Code 3
A10            STRING(LEN_A10+1)    ! Incoming Charge Prsence - Job Fault Code 4
A11            STRING(LEN_A11+1)    ! Antenna Presence - Job Fault Code 5
A12            STRING(LEN_A12+1)    ! Date of purchase
A13            STRING(LEN_A13+1)    ! Reception Date in ARC - In Workshop Date
A14            STRING(LEN_A14+1)    ! Quotation Date - N/A for INTEC
A15            STRING(LEN_A15+1)    ! Quotation Acceptance Date - N/A for INTEC
A16            STRING(LEN_A16+1)    ! Date of Shipment
A17            STRING(LEN_A17+1)    ! Customer Fault Code - Job Fault Code 6
A18            STRING(LEN_A18+1)    ! Engineer Fault Code - Job Fault Code 7
A19            STRING(LEN_A19+1)    ! Incoming Software Version - Job Fault Code 1
A20            STRING(LEN_A20+1)    ! Outgoing Software Version - Job Fault Code 2
A21            STRING(LEN_A21+1)    ! Repair Level
A22            STRING(LEN_A22+1)    ! Part Reference 1
A23            STRING(LEN_A23+1)    ! Part Reference 2
A24            STRING(LEN_A24+1)    ! Part Reference 3
A25            STRING(LEN_A25+1)    ! Part Reference 4
A26            STRING(LEN_A26+1)    ! Part Reference 5
A27            STRING(LEN_A27+1)    ! Part Reference 6
A28            STRING(LEN_A28+1)    ! Part Reference 7
A29            STRING(LEN_A29+1)    ! Part Reference 8
A30            STRING(LEN_A30+1)    ! Part Reference 9 - N/A for INTEC
A31            STRING(LEN_A31+1)    ! Refurb 1 - N/A for INTEC
A32            STRING(LEN_A32+1)    ! Refurb 2 - N/A for INTEC
A33            STRING(LEN_A33+1)    ! Refurb 3 - N/A for INTEC
A34            STRING(LEN_A34+1)    ! Refurb 4 - N/A for INTEC
A35            STRING(LEN_A35+1)    ! Refurb 5 - N/A for INTEC
A36            STRING(LEN_A36+1)    ! Refurb 6 - N/A for INTEC
A37            STRING(LEN_A37+1)    ! Refurb 7 - N/A for INTEC
A38            STRING(LEN_A38+1)    ! RFS - N/A for INTEC
A39            STRING(LEN_A39+1)    ! Model Number - N/A for INTEC
A40            STRING(LEN_A40+1)    ! Re-Rep - Job Fault Code 9 - N/A for INTEC
A41            STRING(LEN_A41+1)    ! ??? - N/A for INTEC
A42            STRING(LEN_A42+1)    ! ??? - N/A for INTEC
A43            STRING(LEN_A43+1)    ! ??? - N/A for INTEC
A44            STRING(LEN_A44)      ! ??? - N/A for INTEC
            END
!! Start Change 3883 BE(16/02/04)
!!Out_Detail_V2 GROUP,OVER(Ouf:Out_Group),PRE(L1V2)
!Out_Detail_V2 GROUP,PRE(L1V2)
!A0             STRING(9)    ! ARC Code - EDI Account Number
!A1             STRING(28)    ! Job Number
!A2             STRING(28)    ! Customer Name
!A3             STRING(5)     ! Warranty Status - 0 = Out of Warranty 1 = Under Warranty
!A4             STRING(18)    ! Incoming IMEI
!A5             STRING(12)    ! Incoming SAGEM Ref - MSN
!A6             STRING(10)    ! Production Date - Job Fault Code 8
!A7             STRING(18)    ! Outgoing IMEI
!A8             STRING(12)    ! Outgoing SAGEM Ref - MSN
!A9             STRING(5)     ! Incoming Battery presence - Job Fault Code 3
!A10            STRING(6)     ! Incoming Charge Prsence - Job Fault Code 4
!A11            STRING(6)     ! Antenna Presence - Job Fault Code 5
!A12            STRING(13)    ! Date of purchase
!A13            STRING(13)    ! Reception Date in ARC - In Workshop Date
!A14            STRING(13)    ! Quotation Date - N/A for INTEC
!A15            STRING(13)    ! Quotation Acceptance Date - N/A for INTEC
!A16            STRING(13)    ! Date of Shipment
!A17            STRING(6)     ! Customer Fault Code - Job Fault Code 6
!A18            STRING(6)     ! Engineer Fault Code - Job Fault Code 7
!A19            STRING(9)     ! Incoming Software Version - Job Fault Code 1
!A20            STRING(9)     ! Outgoing Software Version - Job Fault Code 2
!A21            STRING(6)     ! Repair Level
!A22            STRING(12)    ! Part Reference 1
!A23            STRING(12)    ! Part Reference 2
!A24            STRING(12)    ! Part Reference 3
!A25            STRING(12)    ! Part Reference 4
!A26            STRING(12)    ! Part Reference 5
!A27            STRING(12)    ! Part Reference 6
!A28            STRING(12)    ! Part Reference 7
!A29            STRING(12)    ! Part Reference 8
!A30            STRING(12)    ! Part Reference 9 - N/A for INTEC
!A31            STRING(12)    ! Refurb 1 - N/A for INTEC
!A32            STRING(12)    ! Refurb 2 - N/A for INTEC
!A33            STRING(12)    ! Refurb 3 - N/A for INTEC
!A34            STRING(12)    ! Refurb 4 - N/A for INTEC
!A35            STRING(12)    ! Refurb 5 - N/A for INTEC
!A36            STRING(12)    ! Refurb 6 - N/A for INTEC
!A37            STRING(12)    ! Refurb 7 - N/A for INTEC
!A38            STRING(13)    ! RFS - N/A for INTEC
!A39            STRING(33)    ! Model Number - N/A for INTEC
!A40            STRING(33)    ! Re-Rep - Job Fault Code 9 - N/A for INTEC
!A41            STRING(33)    ! ??? - N/A for INTEC
!A42            STRING(33)    ! ??? - N/A for INTEC
!A43            STRING(33)    ! ??? - N/A for INTEC
!A44            STRING(32)    ! ??? - N/A for INTEC
!            END
! End Change 4563 BE(23/08/2004)

!Out_Header_V2 GROUP,OVER(Ouf:Out_Group),PRE(L2V2)
Out_Header_V2 GROUP,PRE(L2V2)
field1         STRING(13)
field2         STRING(33)
field3         STRING(13)
            END
! End Change 3883 BE(16/02/04)

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
labour        REAL
start_date    LONG
end_Date      LONG

! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Sagem_EDI')      !Add Procedure to Log
  end


   Relate:DEFEDI2.Open
   Relate:DEFAULTS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:WARPARTS.Open
   Relate:EXCHANGE.Open
   Relate:CHARTYPE.Open
   Relate:SUBTRACC.Open
   Relate:TRADEACC.Open
   Relate:TRACHRGE.Open
   Relate:SUBCHRGE.Open
   Relate:STDCHRGE.Open
   Relate:DISCOUNT.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:AUDIT.Open
   Relate:JOBS.Open
   Relate:JOBSTAGE.Open
   Relate:REPTYDEF.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:STOCK.Open
!**Variable
    tmp:ManufacturerName = 'SAGEM'
    continue# = 0

    ! Start Change 4563 BE(20/08/2004)
    set(defaults)
    access:defaults.next()
    IF INSTRING('SIGMA',def:User_Name)
        SigmaFlag = true
    ELSE
        SigmaFlag = false
    END
    ! End Change 4563 BE(20/08/2004)

    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Benign
            ! Start Change 3808 BE(2/2/04)
            EdiVersion = MAN:SiemensNewEdi
            ! End Change 3808 BE(2/2/04)
            set(defedi2)
            If access:defedi2.next()
                ed2:country_code = 'N/A'
                access:defedi2.insert()
                Case MessageEx('The EDI Defaults for this Manufacturer have not been setup.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
                ManError# = 1
            End!If access:defedi.next()
        Else

            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
!**Variable

            ! Start Change 3883 BE(24/02/04)
            !IF f_batch_number = 0
            !    filename = CLIP(CLIP(MAN:EDI_Path)&'\'& Clip(man:edi_account_number) & '_' & FORMAT(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & '.RPT')
            !ELSE
            !    filename = CLIP(CLIP(MAN:EDI_Path)&'\'& Clip(man:edi_account_number) & '_' & FORMAT(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & '.RPT')
            !END
            IF (EdiVersion = 0) THEN
                IF f_batch_number = 0
                    filename = CLIP(CLIP(MAN:EDI_Path)&'\'& Clip(man:edi_account_number) & '_' & FORMAT(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & '.RPT')
                ELSE
                    filename = CLIP(CLIP(MAN:EDI_Path)&'\'& Clip(man:edi_account_number) & '_' & FORMAT(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & '.RPT')
                END
            ELSE
                !filename = CLIP(CLIP(MAN:EDI_Path)&'\'& Clip(man:edi_account_number) & '_' & |
                !        FORMAT(Year(Today()), @n04) & Format(Month(Today()),@n02) & FORMAT(Day(Today()),@n02) & '.RPT')
                fname = Clip(man:edi_account_number) & '_' & |
                        FORMAT(Year(Today()), @n04) & Format(Month(Today()),@n02) & FORMAT(Day(Today()),@n02) & '.RPT'
                filename = CLIP(MAN:EDI_Path) & '\' & CLIP(fname)
            END
            ! Start Change 3883 BE(24/02/04)

            OPEN(Out_File)                           ! Open the output file
            IF ERRORCODE()                           ! If error
                CREATE(Out_File)                       ! create a new file
                If Errorcode()
                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End!If Errorcode() 
                OPEN(out_file)        ! If still error then stop
            ELSE
                OPEN(out_file)
                EMPTY(Out_file)
            END
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate) * 2

                    x# = 1
                    setcursor(cursor:wait)
                    save_job_id = access:jobs.savefile()
                    access:jobs.clearkey(job:edi_key)
                    job:manufacturer     = tmp:ManufacturerName
                    job:edi              = 'NO'
                    set(job:edi_key,job:edi_key)
                    loop
                        if access:jobs.next()
                           break
                        end !if
                        if job:manufacturer     <> tmp:ManufacturerName      |
                        or job:edi              <> 'NO'      |
                            then break.  ! end if
                        ! Start Change 3883 BE(16/2/04)
                        !if job:date_completed > the_date"
                        IF (job:Date_Completed > tmp:ProcessBeforeDate) THEN
                        ! End Change 3883 BE(16/2/04)
                            Cycle
                        End!if job:date_completed > the_date"

                        ! Start Change 4563 BE(23/08/2004)
                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'
                        ! End Change 4563 BE(23/08/2004)

                        yldcnt# += 1
                        if yldcnt# > 25
                           yield() ; yldcnt# = 0
                        end !if
                        Do Getnextrecord2
                        ! Start Change 4563 BE(23/08/2004)
                        !If x# = 1
                        !    start_date  = job:date_booked
                        !    end_Date    = start_date
                        !    x# = 2
                        !Else!If count_temp = 0
                        !    If job:date_booked < start_date
                        !        start_date  = job:date_booked
                        !    End!If job:date_booked < start_date
                        !   If job:date_booked > end_Date
                        !       end_Date    = job:date_booked
                        !    End!If job:date_booked > end_Date
                        !End!If count_temp = 0
                        IF ((start_date = 0) OR (job:date_completed  < start_date)) THEN
                            start_date  = job:date_completed
                        END
                        IF ((end_date = 0) OR (job:date_completed  > end_date)) THEN
                            end_date  = job:date_completed
                        END
                        ! End Change 4563 BE(23/08/2004)
                    end !loop
                    access:jobs.restorefile(save_job_id)
                    setcursor()
                    ! Start Change 3808 BE(2/2/04)
                    !Do Export2
                    CASE (EdiVersion)
                    OF 0
                        DO Export2
                    OF 1
                        DO ExportHeadersV2
                    ELSE
                        DO ExportHeadersV2
                    END
                    ! End Change 3808 BE(2/2/04)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If

                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'

                        ! Start Change 3808 BE(2/2/04)
                        !Do Export
                        CASE (EdiVersion)
                        OF 0
                            DO Export
                        OF 1
                            DO ExportV2
                        ELSE
                            DO ExportV2
                        END
                        ! End Change 3808 BE(2/2/04)

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        pos = Position(job:EDI_Key)
                        !Found
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)
                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0) * 2

                    x# = 1
                    setcursor(cursor:wait)
                    save_job_id = access:jobs.savefile()
                    access:jobs.clearkey(job:edi_key)
                    job:manufacturer     = tmp:ManufacturerName
                    job:edi              = 'YES'
                    ! Start Change 4563 BE(23/08/2004)
                    job:EDI_Batch_Number = f_Batch_Number
                    ! End Change 4563 BE(23/08/2004)
                    set(job:edi_key,job:edi_key)
                    loop
                        if access:jobs.next()
                           break
                        end !if

                        ! Start Change 4563 BE(23/08/2004)
                        !if job:manufacturer     <> tmp:ManufacturerName      |
                        !or job:edi              <> 'YES'      |
                        !    then break.  ! end if
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        ! End Change 4563 BE(23/08/2004)

                        yldcnt# += 1
                        if yldcnt# > 25
                           yield() ; yldcnt# = 0
                        end !if
                        Do Getnextrecord2
                        ! Start Change 4563 BE(23/08/2004)
                        !If x# = 1
                        !    start_date  = job:date_booked
                        !    end_Date    = start_date
                        !    x# = 2
                        !Else!If count_temp = 0
                        !    If job:date_booked < start_date
                        !        start_date  = job:date_booked
                        !    End!If job:date_booked < start_date
                        !    If job:date_booked > end_Date
                        !       end_Date    = job:date_booked
                        !    End!If job:date_booked > end_Date
                        !End!If count_temp = 0
                        IF ((start_date = 0) OR (job:date_completed  < start_date)) THEN
                            start_date  = job:date_completed
                        END
                        IF ((end_date = 0) OR (job:date_completed  > end_date)) THEN
                            end_date  = job:date_completed
                        END
                        ! End Change 4563 BE(23/08/2004)
                    end !loop
                    access:jobs.restorefile(save_job_id)
                    setcursor()

                    ! Start Change 3808 BE(2/2/04)
                    !Do Export2
                    CASE (EdiVersion)
                    OF 0
                        DO Export2
                    OF 1
                        DO ExportHeadersV2
                    ELSE
                        DO ExportHeadersV2
                    END
                    ! End Change 3808 BE(2/2/04)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        ! Start Change 3808 BE(2/2/04)
                        !Do Export
                        CASE (EdiVersion)
                        OF 0
                            DO Export
                        OF 1
                            DO ExportV2
                        ELSE
                            DO ExportV2
                        END
                        ! End Change 3808 BE(2/2/04)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    ! Start Change 2806 BE(22/07/03)
                    !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !  '<13,10>'&|
                    !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                    !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !    Of 1 ! &OK Button
                    !End!Case MessageEx
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)

                End!If f_batch_number = 0
                CLOSE(out_file)
            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:DEFEDI2.Close
   Relate:DEFAULTS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:WARPARTS.Close
   Relate:EXCHANGE.Close
   Relate:CHARTYPE.Close
   Relate:SUBTRACC.Close
   Relate:TRADEACC.Close
   Relate:TRACHRGE.Close
   Relate:SUBCHRGE.Close
   Relate:STDCHRGE.Close
   Relate:DISCOUNT.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:AUDIT.Close
   Relate:JOBS.Close
   Relate:JOBSTAGE.Close
   Relate:REPTYDEF.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:STOCK.Close
Export         Routine
    Clear(ouf:record)
    L1:Edi_No       = '"'&Format(Upper(MAN:EDI_Account_Number),@s12)&'";'
    L1:Order_No     = '"'&UPPER(Strippoint(Format(job:ref_number,@s25)))&'";'
    L1:Customer     = '"'&UPPER(Format(job:surname,@s25))&'";'
    L1:Warranty     = '"UW";'
    IMEIError# = 0
    If job:Third_Party_Site <> ''
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        Set(jot:RefNumberKey,jot:RefNumberKey)
        If Access:JOBTHIRD.NEXT()
            IMEIError# = 1
        Else !If Access:JOBTHIRD.NEXT()
            If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 1
            Else !If jot:RefNumber <> job:Ref_Number            
                IMEIError# = 0                
            End !If jot:RefNumber <> job:Ref_Number            
        End !If Access:JOBTHIRD.NEXT()
    Else !job:Third_Party_Site <> ''
        IMEIError# = 1    
    End !job:Third_Party_Site <> ''

    If IMEIError# = 1
        L1:IMEI_In      = '"'&UPPER(Format(job:esn,@s15))&'";'
        L1:Sag_Ref_In   = '"'&UPPER(Format(job:msn,@s9))&'";'
    Else !IMEIError# = 1
        L1:IMEI_In      = '"'&UPPER(Format(jot:OriginalIMEI,@s15))&'";'
        L1:Sag_Ref_In   = '"'&UPPER(Format(jot:OriginalMSN,@s9))&'";'
    End !IMEIError# = 1
    L1:IMEI_Out      = '"'&UPPER(Format(job:esn,@s15))&'";'
    L1:Sag_Ref_Out   = '"'&UPPER(Format(job:msn,@s9))&'";'

    L1:Date_Code    = '"'&UPPER(Format(job:fault_code8,@s7))&'";'
    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = job:exchange_unit_number
    if access:exchange.tryfetch(xch:ref_number_key) = Level:Benign
        L1:IMEI_Out     = '"'&UPPER(Format(xch:esn,@s15))&'";'
        L1:Sag_Ref_Out  = '"'&UPPER(Format(xch:msn,@s9))&'";'
    End!if access:exchange.tryfetch(xch:ref_number_key)
    L1:Battery      = '"'&UPPER(Format(job:fault_code3,@s9))&'";'
    L1:Charger      = '"'&UPPER(Format(job:fault_code4,@s9))&'";'
    L1:Antenna      = '"'&UPPER(Format(job:fault_code5,@s3))&'";'
    If job:dop <> ''
        L1:DOP          = '"'&UPPER(Format(FORMAT(DAY(job:dop),@n02)&FORMAT(MONTH(job:dop),@n02)&YEAR(job:dop),@s8))&'";'
    Else!If l1:dop <> ''
        l1:dop          = '"        ";'
    End!If l1:dop <> ''
    L1:Booked       = '"'&UPPER(Format(FORMAT(DAY(job:date_booked),@n02)&FORMAT(MONTH(job:date_booked),@n02)&YEAR(job:date_booked),@s8))&'";'
    !loop through contacts to get dates for the estimate!
    found_out# = 0
    found_in# = 0
    save_aud_id = access:audit.savefile()
    access:audit.clearkey(aud:ref_number_key)
    aud:ref_number = job:ref_number
    set(aud:ref_number_key,aud:ref_number_key)
    loop
        if access:audit.next()
           break
        end !if
        if aud:ref_number <> job:ref_number      |
            then break.  ! end if
        If Instring(aud:action,1,1) = '520 ESTIMATE SENT'
            L1:Est_Out      = '"'&UPPER(Format(FORMAT(DAY(aud:date),@n02)&FORMAT(MONTH(aud:date),@n02)&YEAR(aud:date),@s8))&'";'
            found_out# = 1
        End!If Instring(aud:action,1,1) = '520 ESTIMATE SENT'
        If instring(aud:action,1,1) = '535 ESTIMATE ACCEPTED'
            L1:Est_In       = '"'&UPPER(Format(FORMAT(DAY(aud:date),@n02)&FORMAT(MONTH(aud:date),@n02)&YEAR(aud:date),@s8))&'";'
            found_in# = 1
        End!If instring(aud:action,1,1) = '535 ESTIMATE ACCEPTED'
    end !loop
    access:audit.restorefile(save_aud_id)
    If found_out# = 0
        l1:est_out  = '"        ";'
    End!If found_out# = 0
    If found_out# = 0
        l1:est_in   = '"        ";'
    End!If found_out# = 0
    If job:Exchange_Despatched <> ''
        L1:Despatched   = '"'&UPPER(Format(FORMAT(DAY(job:Exchange_Despatched),@n02)&FORMAT(MONTH(job:Exchange_Despatched),@n02)&YEAR(job:Exchange_Despatched),@s8))&'";'
    Else !If job:Exchange_Despatched <> ''
        L1:Despatched   = '"'&UPPER(Format(FORMAT(DAY(job:date_completed),@n02)&FORMAT(MONTH(job:Date_completed),@n02)&YEAR(job:date_completed),@s8))&'";'
    End !If job:Exchange_Despatched <> ''

    
    L1:Cust_Code    = '"'&UPPER(Format(job:fault_code6,@s5))&'";'
    L1:Fault_Code   = '"'&UPPER(Format(job:fault_code7,@s5))&'";'
    L1:Soft_In      = '"'&(Format(job:fault_code1,@s11))&'";'
    L1:Soft_Out     = '"'&(Format(job:fault_code2,@s11))&'";'
    access:reptydef.clearkey(rtd:warranty_key)
    rtd:warranty    = 'YES'
    rtd:repair_type = job:repair_type_warranty
    if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign
        L1:Repair_Level = '"' & Upper(Format(rtd:WarrantyCode,@s5))&'";'
    Else!if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign
        L1:Repair_Level = '"'&UPPER(Format(job:repair_type_warranty,@s5))&'";'
    End!if access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign

    xp#=0

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    set(wpr:part_number_key,wpr:part_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number  <> job:ref_number      |
            then break.  ! end if
        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
            Cycle
        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

        !Exclude part from EDI?
        If ExcludeFromEDI(wpr:Part_Ref_Number)
            Cycle
        End !If ExcludeFromEDI(wpr:Part_Ref_Number)

        xp#+=1
        CASE xp#
          OF 1
            If wpr:part_number = 'ADJUSTMENT'
                L1:part1 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part1 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'
          OF 2
            If wpr:part_number = 'ADJUSTMENT'
                L1:part2 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part2 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'
          OF 3
            If wpr:part_number = 'ADJUSTMENT'
                L1:part3 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part3 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'
          OF 4
            If wpr:part_number = 'ADJUSTMENT'
                L1:part4 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part4 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'
          OF 5
            If wpr:part_number = 'ADJUSTMENT'
                L1:part5 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part5 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'
          OF 6
            If wpr:part_number = 'ADJUSTMENT'
                L1:part6 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part6 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:art_number = 'ADJUSTMENT'
          OF 7
            If wpr:part_number = 'ADJUSTMENT'
                L1:part7 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part7 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'
          OF 8
            If wpr:part_number = 'ADJUSTMENT'
                L1:part8 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part8 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'
          OF 9
            If wpr:part_number = 'ADJUSTMENT'
                L1:part9 = '"'&Format('',@s9)&'";'
            Else!If wpr:part_number = 'ADJUSTMENT'
                L1:part9 = '"'&Format(wpr:part_number,@s9)&'";'
            End!If wpr:part_number = 'ADJUSTMENT'

        END

    end !loop
    access:warparts.restorefile(save_wpr_id)

    IF l1:part1 = ''
      L1:part1 = '"'&Format('',@s9)&'";'
    END
    IF l1:part2 = ''
      L1:part2 = '"'&Format('',@s9)&'";'
    END
    IF l1:part3 = ''
      L1:part3 = '"'&Format('',@s9)&'";'
    END
    IF l1:part4 = ''
      L1:part4 = '"'&Format('',@s9)&'";'
    END
    IF l1:part5 = ''
      L1:part5 = '"'&Format('',@s9)&'";'
    END
    IF l1:part6 = ''
      L1:part6 = '"'&Format('',@s9)&'";'
    END
    IF l1:part7 = ''
      L1:part7 = '"'&Format('',@s9)&'";'
    END
    IF l1:part8 = ''
      L1:part8 = '"'&Format('',@s9)&'";'
    END
    IF l1:part9 = ''
      L1:part9 = '"'&Format('',@s9)&'";'
    END
    L1:refurb1 = '"'&Format('',@s9)&'";'
    L1:refurb2 = '"'&Format('',@s9)&'";'
    L1:refurb3 = '"'&Format('',@s9)&'";'
    L1:refurb4 = '"'&Format('',@s9)&'";'
    L1:refurb5 = '"'&Format('',@s9)&'";'
    L1:refurb6 = '"'&Format('',@s9)&'";'
    L1:refurb7 = '"'&Format('',@s9)&'";'
    L1:RFS     = '"'&UPPER(Format(FORMAT(DAY(job:date_completed),@n02)&FORMAT(MONTH(job:Date_Completed),@n02)&YEAR(job:Date_Completed),@s8))&'";'
    L1:ModelNumber   = '"'&Format(job:model_number,@s30)&'";'
    L1:Re_Rep  = '"'&UPPER(Format(job:fault_code9,@s30))&'"'

    count += 1
    ADD(out_file)


Export2     Routine
    CLEAR(ouf:RECORD)
    CLEAR(mt_total)

    Set(defaults)
    access:defaults.next()

    L2:Field1 = '"'&Format('UK',@s8)&'";'
    L2:Field2 = '"'&Format(def:User_Name,@s30)&'";'
    L2:Field3 = '"'&Format(man:EDI_Account_Number,@s8)&'"'
    ADD(out_file)
    L2:Field1 = '"'&Format(FORMAT(DAY(TODAY()),@n02)&FORMAT(MONTH(TODAY()),@n02)&YEAR(TODAY()),@s8)&'";'
    L2:Field2 = '"'&Format(FORMAT(DAY(start_date),@n02)&FORMAT(MONTH(start_date),@n02)&YEAR(start_date),@s30)&'";'
    L2:Field3 = '"'&Format(FORMAT(DAY(end_date),@n02)&FORMAT(MONTH(end_date),@n02)&YEAR(end_date),@s8)&'"'
    ADD(out_file)
    If f_batch_number <> 0
        L2:Field1 = '"'&Format(Strippoint(Upper(Clip(f_batch_number))),@s8)&'";'
    Else!If f_batch_number = 0
        L2:Field1 = '"'&Format(Strippoint(Upper(Clip(man:Batch_Number))),@s8)&'";'
    End!If f_batch_number = 0
    L2:Field2 = '""'
    L2:Field3 = ''
    ADD(out_file)
    CLEAR(ouf:RECORD)
    LOOP h# = 1 to 7
      L2:Field1 = ''
      ADD(out_file)
    END
    L1:Edi_NO       = '"'&Format('A0',@s12)&'";'
    L1:Order_No     = '"'&format('A1',@s25)&'";'
    L1:Customer     = '"'&format('A2',@s25)&'";'
    L1:Warranty     = '"'&Format('A3',@s2)&'";'
    L1:IMEI_In      = '"'&Format('A4',@s15)&'";'
    L1:Sag_Ref_In   = '"'&Format('A5',@s9)&'";'
    L1:Date_Code    = '"'&Format('A6',@s7)&'";'
    L1:IMEI_Out     = '"'&Format('A7',@s15)&'";'
    L1:Sag_Ref_Out  = '"'&Format('A8',@s9)&'";'
    L1:Battery      = '"'&Format('A9',@s9)&'";'
    L1:Charger      = '"'&Format('A10',@s9)&'";'
    L1:Antenna      = '"'&Format('A11',@s3)&'";'
    L1:DOP          = '"'&Format('A12',@s8)&'";'
    L1:Booked       = '"'&Format('A13',@s8)&'";'
    L1:Est_Out      = '"'&FOrmat('A14',@s8)&'";'
    L1:Est_In       = '"'&Format('A15',@s8)&'";'
    L1:Despatched   = '"'&Format('A16',@s8)&'";'
    L1:Cust_Code    = '"'&Format('A17',@s5)&'";'
    L1:Fault_Code   = '"'&Format('A18',@s5)&'";'
    L1:Soft_In      = '"'&Format('A19',@s11)&'";'
    L1:Soft_Out     = '"'&Format('A20',@s11)&'";'
    L1:Repair_Level = '"'&Format('A21',@s5)&'";'
    L1:part1        = '"'&Format('A22',@s9)&'";'
    L1:part2        = '"'&Format('A23',@s9)&'";'
    L1:part3        = '"'&Format('A24',@s9)&'";'
    L1:part4        = '"'&Format('A25',@s9)&'";'
    L1:part5        = '"'&Format('A26',@s9)&'";'
    L1:part6        = '"'&Format('A27',@s9)&'";'
    L1:part7        = '"'&Format('A28',@s9)&'";'
    L1:part8        = '"'&Format('A29',@s9)&'";'
    L1:part9        = '"'&Format('A30',@s9)&'";'
    L1:Refurb1      = '"'&Format('A31',@s9)&'";'
    L1:Refurb2      = '"'&Format('A32',@s9)&'";'
    L1:Refurb3      = '"'&Format('A33',@s9)&'";'
    L1:Refurb4      = '"'&Format('A34',@s9)&'";'
    L1:Refurb5      = '"'&Format('A35',@s9)&'";'
    L1:Refurb6      = '"'&Format('A36',@s9)&'";'
    L1:Refurb7      = '"'&Format('A37',@s9)&'";'
    L1:RFS          = '"'&Format('A38',@s9)&'";'
    L1:ModelNumber  = '"'&Format('A39',@s9)&'";'
    L1:Re_Rep       = '"'&Format('A40',@s9)&'"'

    ADD(out_file)
ExportHeadersV2     Routine
    ! Line 1
    CLEAR(ouf:RECORD)
    CLEAR(Out_Header_V2)
    CLEAR(mt_total)

    Set(defaults)
    access:defaults.next()

    ! Start Change 4563 BE(20/082004)
    !L2V2:Field1 = '"' & Format('UK',@s10) & '";'
    !L2V2:Field1 = 'UK;'
    IF (SigmaFlag) THEN
        L2V2:Field1 = 'IRELAND;'
    ELSE
        L2V2:Field1 = 'UK;'
    END
    ! End Change 4563 BE(20/08/2004)


    !L2V2:Field2 = '"' & Format(def:User_Name,@s30) & '";'
    !L2V2:Field3 = '"' & Format(man:EDI_Account_Number,@s10) & '"'
    L2V2:Field2 = CLIP(def:User_Name) & ';'
    L2V2:Field3 = CLIP(man:EDI_Account_Number)

    DO CopyV2HeaderFields
    ADD(out_file)

    ! Line 2
    CLEAR(ouf:RECORD)
    CLEAR(Out_Header_V2)
    !L2V2:Field1 = '"' & FORMAT(TODAY(), @d06) & '";'
    !L2V2:Field2 = '"' & FORMAT(start_date,@d06) & ' {20}";'
    !L2V2:Field3 = '"' & FORMAT(end_date, @d06) & '"'
    L2V2:Field1 = FORMAT(TODAY(), @d06B) & ';'
    L2V2:Field2 = FORMAT(start_date,@d06B) & ';'
    L2V2:Field3 = FORMAT(end_date, @d06B) & ';'

    DO CopyV2HeaderFields
    ADD(out_file)

    ! Line 3
    CLEAR(ouf:RECORD)
    CLEAR(Out_Header_V2)

    IF (f_batch_number <> 0) THEN
        !L2V2:Field1 = '"' & Format(Strippoint(Upper(Clip(f_batch_number))), @s10) & '"'
        L2V2:Field1 = Strippoint(Upper(Clip(f_batch_number)))
    ELSE
        !L2V2:Field1 = '"' & Format(Strippoint(Upper(Clip(man:Batch_Number))), @s10) & '"'
        L2V2:Field1 = Strippoint(Upper(Clip(man:Batch_Number)))
    END

    DO CopyV2HeaderFields
    ADD(out_file)

    ! Line 4
    CLEAR(ouf:RECORD)
    CLEAR(Out_Header_V2)

    L2V2:Field1 = 'RRV2.41'

    DO CopyV2HeaderFields
    ADD(out_file)

    ! Line 5
    CLEAR(ouf:RECORD)
    ADD(out_file)

    ! Line 6
    CLEAR(ouf:RECORD)
    CLEAR(Out_Header_V2)

    !L2V2:Field1 = CLIP(fname)
    OUF:Out_Group.Line1 = CLIP(fname)
    !DO CopyV2HeaderFields
    ADD(out_file)

!    CLEAR(ouf:RECORD)
!    LOOP 7 TIMES
!        ADD(out_file)
!    END

    ! Lines 7-10
    CLEAR(ouf:RECORD)
    LOOP 4 TIMES
        ADD(out_file)
    END

    ! Line 11
    CLEAR(ouf:RECORD)
    CLEAR(Out_Header_V2)

!    L1V2:A0           = '"' & Format('A0',@s6) & '";'
!    L1V2:A1           = '"' & format('A1',@s25) & '";'
!    L1V2:A2           = '"' & format('A2',@s25) & '";'
!    L1V2:A3           = '"' & Format('A3',@s2) & '";'
!    L1V2:A4           = '"' & Format('A4',@s15) & '";'
!    L1V2:A5           = '"' & Format('A5',@s9) & '";'
!    L1V2:A6           = '"' & Format('A6', @s7) & '";'
!    L1V2:A7           = '"' & Format('A7',@s15) & '";'
!    L1V2:A8           = '"' & Format('A8',@s9) & '";'
!    L1V2:A9           = '"' & Format('A9',@s2) & '";'
!    L1V2:A10          = '"' & Format('A10',@s3) & '";'
!    L1V2:A11          = '"' & Format('A11',@s3) & '";'
!    L1V2:A12          = '"' & Format('A12', @s10) & '";'
!    L1V2:A13          = '"' & Format('A13', @s10) & '";'
!    L1V2:A14          = '"' & Format('A14', @s10) & '";'
!    L1V2:A15          = '"' & Format('A15', @s10) & '";'
!    L1V2:A16          = '"' & Format('A16', @s10) & '";'
!    L1V2:A17          = '"' & Format('A17',@s3) & '";'
!    L1V2:A18          = '"' & Format('A18',@s3) & '";'
!    L1V2:A19          = '"' & Format('A19',@s6) & '";'
!    L1V2:A20          = '"' & Format('A20',@s6) & '";'
!    L1V2:A21          = '"' & Format('A21',@s3) & '";'
!    L1V2:A22          = '"' & Format('A22',@s9) & '";'
!    L1V2:A23          = '"' & Format('A23',@s9) & '";'
!    L1V2:A24          = '"' & Format('A24',@s9) & '";'
!    L1V2:A25          = '"' & Format('A25',@s9) & '";'
!    L1V2:A26          = '"' & Format('A26',@s9) & '";'
!    L1V2:A27          = '"' & Format('A27',@s9) & '";'
!    L1V2:A28          = '"' & Format('A28',@s9) & '";'
!    L1V2:A29          = '"' & Format('A29',@s9) & '";'
!    L1V2:A30          = '"' & Format('A30',@s9) & '";'
!    L1V2:A31          = '"' & Format('A31',@s9) & '";'
!    L1V2:A32          = '"' & Format('A32',@s9) & '";'
!    L1V2:A33          = '"' & Format('A33',@s9) & '";'
!    L1V2:A34          = '"' & Format('A34',@s9) & '";'
!    L1V2:A35          = '"' & Format('A35',@s9) & '";'
!    L1V2:A36          = '"' & Format('A36',@s9) & '";'
!    L1V2:A37          = '"' & Format('A37',@s9) & '";'
!    L1V2:A38          = '"' & Format('A38',@s10) & '";'
!    L1V2:A39          = '"' & Format('A39',@s30) & '";'
!    L1V2:A40          = '"' & Format('A40',@s30) & '";'
!    L1V2:A41          = '"' & Format('A41',@s30) & '";'
!    L1V2:A42          = '"' & Format('A42',@s30) & '";'
!    L1V2:A43          = '"' & Format('A43',@s30) & '";'
!    L1V2:A44          = '"' & Format('A44',@s30) & '"'
    ! End Change 3883 BE(16/02/04)

    L1V2:A0           = 'A0;'
    L1V2:A1           = 'A1;'
    L1V2:A2           = 'A2;'
    L1V2:A3           = 'A3;'
    L1V2:A4           = 'A4;'
    L1V2:A5           = 'A5;'
    L1V2:A6           = 'A6;'
    L1V2:A7           = 'A7;'
    L1V2:A8           = 'A8;'
    L1V2:A9           = 'A9;'
    L1V2:A10          = 'A10;'
    L1V2:A11          = 'A11;'
    L1V2:A12          = 'A12;'
    L1V2:A13          = 'A13;'
    L1V2:A14          = 'A14;'
    L1V2:A15          = 'A15;'
    L1V2:A16          = 'A16;'
    L1V2:A17          = 'A17;'
    L1V2:A18          = 'A18;'
    L1V2:A19          = 'A19;'
    L1V2:A20          = 'A20;'
    L1V2:A21          = 'A21;'
    L1V2:A22          = 'A22;'
    L1V2:A23          = 'A23;'
    L1V2:A24          = 'A24;'
    L1V2:A25          = 'A25;'
    L1V2:A26          = 'A26;'
    L1V2:A27          = 'A27;'
    L1V2:A28          = 'A28;'
    L1V2:A29          = 'A29;'
    L1V2:A30          = 'A30;'
    L1V2:A31          = 'A31;'
    L1V2:A32          = 'A32;'
    L1V2:A33          = 'A33;'
    L1V2:A34          = 'A34;'
    L1V2:A35          = 'A35;'
    L1V2:A36          = 'A36;'
    L1V2:A37          = 'A37;'
    L1V2:A38          = 'A38;'
    L1V2:A39          = 'A39;'
    L1V2:A40          = 'A40;'
    L1V2:A41          = 'A41;'
    L1V2:A42          = 'A42;'
    L1V2:A43          = 'A43;'
    L1V2:A44          = 'A44;'

    ! Start Change 4563 BE(20/08/2004)
    !DO CopyV2HeaderFields
    DO CopyV2DetailFields
    ! End Change 4563 BE(20/08/2004)
    ADD(out_file)
ExportV2         Routine
Data
local:A16        Date()
Code
    Clear(ouf:record)
    CLEAR(Out_Detail_V2)

    !L1V2:A0           = '"' & Format(Upper(MAN:EDI_Account_Number),@s6) & '";'
    !L1V2:A1           = '"' & UPPER(Strippoint(Format(job:ref_number,@s25))) & '";'
    L1V2:A0           = CLIP(UPPER(SUB(MAN:EDI_Account_Number, 1, LEN_A0))) & ';'
    L1V2:A1           = CLIP(UPPER(Strippoint(SUB(job:ref_number, 1, LEN_A1)))) & ';'

    ! Start Change 4160 BE(23/04/04)
    !L1V2:A2           = '"' & UPPER(Format(job:surname,@s25)) & '";'
    IF (job:surname = '') THEN
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        IF (access:subtracc.fetch(sub:account_number_key) = Level:Benign) THEN
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_Number
            IF (access:tradeacc.fetch(tra:account_number_key) = Level:Benign) THEN
                IF (tra:company_name='') THEN
                    !L1V2:A2 = '"' & UPPER(Format(sub:main_account_Number,@s25)) & '";'
                    L1V2:A2 = CLIP(UPPER(SUB(sub:main_account_Number, 1, LEN_A2))) & ';'
                ELSE
                    !L1V2:A2 = '"' & UPPER(Format(tra:company_name,@s25)) & '";'
                    L1V2:A2 = CLIP(UPPER(SUB(tra:company_name, 1, LEN_A2))) & ';'
                END
            END
        END
    ELSE
        !L1V2:A2 = '"' & UPPER(Format(job:surname,@s25)) & '";'
        L1V2:A2 = CLIP(UPPER(SUB(job:surname, 1, LEN_A2))) & ';'
    END
    ! End Change 4160 BE(23/04/04)

    !L1V2:A3           = '"1 ";'
    L1V2:A3           = '1;'
    IMEIError# = 0
    IF (job:Third_Party_Site <> '') THEN
        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
        jot:RefNumber = job:Ref_Number
        SET(jot:RefNumberKey,jot:RefNumberKey)
        IF (Access:JOBTHIRD.NEXT() <> Level:Benign) THEN
            IMEIError# = 1
        ELSE
            IF (jot:RefNumber <> job:Ref_Number) THEN
                IMEIError# = 1
            ELSE
                IMEIError# = 0                
            END
        END
    ELSE
        access:JOBSE.clearkey(jobe:refnumberkey)
        jobe:refnumber = job:ref_number
        IF (access:JOBSE.fetch(jobe:refnumberkey) = Level:Benign) THEN
            IF (jobe:Pre_RF_Board_IMEI = '') THEN
                IMEIError# = 1
            ELSE
                IMEIError# = 2
            END
        ELSE
            IMEIError# = 1
        End
    END

    IF (IMEIError# = 1) THEN
        !L1V2:A4 = '"' & UPPER(Format(job:esn,@s15)) & '";'
        !L1V2:A5 = '"' & UPPER(Format(job:msn,@s9)) & '";'
        L1V2:A4 = CLIP(UPPER(SUB(job:esn, 1, LEN_A4))) & ';'
        L1V2:A5 = CLIP(UPPER(SUB(job:msn, 1, LEN_A5))) & ';'
    ELSIF (IMEIError# = 2) THEN
        !L1V2:A4 = '"' & UPPER(Format(jobe:Pre_RF_Board_IMEI, @s15)) & '";'
        !L1V2:A5 = '"' & UPPER(Format(job:msn,@s9)) & '";'
        L1V2:A4 = CLIP(UPPER(SUB(jobe:Pre_RF_Board_IMEI, 1, LEN_A4))) & ';'
        L1V2:A5 = CLIP(UPPER(SUB(job:msn, 1, LEN_A5))) & ';'
    ELSE
        !L1V2:A4 = '"' & UPPER(Format(jot:OriginalIMEI,@s15)) & '";'
        !L1V2:A5 = '"' & UPPER(Format(jot:OriginalMSN,@s9)) & '";'
        L1V2:A4 = CLIP(UPPER(SUB(jot:OriginalIMEI, 1, LEN_A4))) & ';'
        L1V2:A5 = CLIP(UPPER(SUB(jot:OriginalMSN, 1, LEN_A5))) & ';'
    END

    !L1V2:A6  = '"' & UPPER(Format(job:fault_code8,@s30)) & '";'
    !L1V2:A6  = '"' & UPPER(Format(job:fault_code8,@s30)) & '";'
    L1V2:A6  = CLIP(UPPER(SUB(job:fault_code8, 1, LEN_A6))) & ';'

    access:exchange.clearkey(xch:ref_number_key)
    xch:ref_number = job:exchange_unit_number
    IF (access:exchange.tryfetch(xch:ref_number_key) = Level:Benign) THEN
        !L1V2:A7     = '"' & UPPER(Format(xch:esn,@s15)) & '";'
        !L1V2:A8     = '"' & UPPER(Format(xch:msn,@s9)) & '";'
        L1V2:A7     = CLIP(UPPER(SUB(xch:esn, 1, LEN_A7))) & ';'
        L1V2:A8     = CLIP(UPPER(SUB(xch:msn, 1, LEN_A8))) & ';'
    ELSE
        !L1V2:A7     = '"' & UPPER(Format(job:esn,@s15)) & '";'
        !L1V2:A8     = '"' & UPPER(Format(job:msn,@s9)) & '";'
        L1V2:A7     = CLIP(UPPER(SUB(job:esn, 1, LEN_A7))) & ';'
        L1V2:A8     = CLIP(UPPER(SUB(job:msn, 1, LEN_A8))) & ';'
    END

    IF (UPPER(job:fault_code3) = 'YES') THEN
        !L1V2:A9 = '"1 ";'
        L1V2:A9 = '1;'
    ELSE
        !L1V2:A9 = '"0 ";'
        L1V2:A9 = '0;'
    END

    IF (UPPER(job:fault_code4) = 'YES')THEN
        !L1V2:A10 = '"1  ";'
        L1V2:A10 = '1;'
    ELSE
        !L1V2:A10 = '"0  ";'
        L1V2:A10 = '0;'
    END

    IF (UPPER(job:fault_code5) = 'YES') THEN
        !L1V2:A11 = '"1  ";'
        L1V2:A11 = '1;'
    ELSE
        !L1V2:A11 = '"0  ";'
        L1V2:A11 = '0;'
    END

    IF (job:dop <> '') THEN
        !L1V2:A12 = '"' & FORMAT(job:dop, @d06) & '";'
        L1V2:A12 = CLIP(FORMAT(job:dop, @d06B)) & ';'
    ELSE
        !L1V2:A12 = '" {10}";'
        L1V2:A12 = ';'
    END

    IF (job:Workshop = 'YES') THEN
        access:JOBSE.clearkey(jobe:refnumberkey)
        jobe:refnumber = job:ref_number
        IF (access:JOBSE.fetch(jobe:refnumberkey) = Level:Benign) THEN
            !L1V2:A13 = '"' & FORMAT(jobe:InWorkshopDate, @d06) & '";'
            L1V2:A13 = CLIP(FORMAT(jobe:InWorkshopDate, @d06B)) & ';'
        ELSE
            !L1V2:A13 = '"' & FORMAT(job:date_booked, @d06) & '";'
            L1V2:A13 = CLIP(FORMAT(job:date_booked, @d06B)) & ';'
        END
    ELSE
        !L1V2:A13 = '"' & FORMAT(job:date_booked, @d06) & '";'
        L1V2:A13 = CLIP(FORMAT(job:date_booked, @d06B)) & ';'
    END

    !loop through contacts to get dates for the estimate!
    !L1V2:A14   = '" {10}";'
    !L1V2:A15  = '" {10}";'
    L1V2:A14   = ';'
    L1V2:A15  = ';'
    RFBoardDate# = 0
    save_aud_id = access:audit.savefile()
    access:audit.clearkey(aud:ref_number_key)
    aud:ref_number = job:ref_number
    SET(aud:ref_number_key,aud:ref_number_key)
    LOOP
        IF (access:audit.next() <> Level:Benign) THEN
           BREAK
        END
        IF (aud:ref_number <> job:ref_number) THEN
            BREAK
        END

        !IF (INSTRING(aud:action, '535 ESTIMATE ACCEPTED', 1, 1)) THEN
        !    L1V2:A14 = '"' & FORMAT(aud:date, @d06) & '";'
        !END
        !IF (INSTRING(aud:action, '520 ESTIMATE SENT', 1,1)) THEN
        !    L1V2:A15 = '"' & FORMAT(aud:date, @d06) & '";'
        !END
        !IF (INSTRING(aud:notes, 'RF BOARD ATTACHED TO JOB', 1, 1)) THEN
        !    RFBoardDate# = aud:Date
        !END

         !4881 - These fields are blank for an EDI  (DBH: 02-12-2004)

!        IF (INSTRING('535 ESTIMATE ACCEPTED', aud:action, 1, 1)) THEN
!            !L1V2:A14 = '"' & FORMAT(aud:date, @d06) & '";'
!            L1V2:A14 = CLIP(FORMAT(aud:date, @d06B)) & ';'
!        END
!        IF (INSTRING('520 ESTIMATE SENT', aud:action, 1,1)) THEN
!            !L1V2:A15 = '"' & FORMAT(aud:date, @d06) & '";'
!            L1V2:A15 = CLIP(FORMAT(aud:date, @d06B)) & ';'
!        END
        IF (INSTRING('RF BOARD ATTACHED TO JOB', Upper(aud:notes), 1, 1)) THEN
            RFBoardDate# = aud:Date
        END
    END
    access:audit.restorefile(save_aud_id)

    !Start - Despatched Date. Use earliest comp/exch or rf date - TrkBs: 5555 (DBH: 16-03-2005)
    !Set local variable to blank - TrkBs: 5555 (DBH: 16-03-2005)
    local:A16 = ''
    If job:Date_Completed > 0
        !Use completed date - TrkBs: 5555 (DBH: 16-03-2005)
        local:A16 = job:Date_Completed
    End ! If job:Date_Completed > 0

    If job:Exchange_Despatched > 0 And (job:Exchange_Despatched < local:A16 Or local:A16 = 0)
        !If exchange despatched and less than comp date (or comp date is blank) - TrkBs: 5555 (DBH: 16-03-2005)
        local:A16 = job:Exchange_Despatched
    End ! If job:Exchange_Despatched > 0 And (job:Exchange_Despatched < job:Date_Completed)

    If RFBoardDate# > 0 And (RFBoardDate# < local:A16 Or local:A16 = 0)
        !If RF Date is less than comp & exch date (or comp & exch dates are blank) - TrkBs: 5555 (DBH: 16-03-2005)
        local:A16 = RFBoardDate#
    End ! If RFBoardDate# > 0 And (RFBoardDate# < local:A16)

    !Fill in the export variable with the correct date - TrkBs: 5555 (DBH: 16-03-2005)
    If local:A16 = 0
        L1V2:A16 = ';'
    Else ! If local:A16 = 0
        L1V2:A16 = Clip(Format(local:A16,@d06b)) & ';'
    End ! If local:A16 = 0
    !End   - Despatched Date. Use earliest comp/exch or rf date - TrkBs: 5555 (DBH: 16-03-2005)
    

    ! Start Change 4160 BE(04/05/04)
!    L1V2:A17          = '"' & UPPER(Format(job:fault_code6,@s5)) & '";'
!    L1V2:A18          = '"' & UPPER(Format(job:fault_code7,@s5)) & '";'
!    L1V2:A19          = '"' & (Format(job:fault_code1,@s11)) & '";'
!    L1V2:A20          = '"' & (Format(job:fault_code2,@s11)) & '";'
!
!    access:reptydef.clearkey(rtd:warranty_key)
!    rtd:warranty    = 'YES'
!    rtd:repair_type = job:repair_type_warranty
!    IF (access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign) THEN
!        L1V2:A21 = '"' & Upper(Format(rtd:WarrantyCode,@s5))&'";'
!    ELSE
!        L1V2:A21 = '"' & UPPER(Format(job:repair_type_warranty,@s5)) & '";'
!    END

    !L1V2:A17          = '"' & UPPER(Format(job:fault_code6,@s3)) & '";'
    !L1V2:A18          = '"' & UPPER(Format(job:fault_code7,@s3)) & '";'
    !L1V2:A19          = '"' & (Format(job:fault_code1,@s6)) & '";'
    !L1V2:A20          = '"' & (Format(job:fault_code2,@s6)) & '";'
    L1V2:A17          = CLIP(UPPER(SUB(job:fault_code6, 1, LEN_A17))) & ';'
    L1V2:A18          = CLIP(UPPER(SUB(job:fault_code7, 1, LEN_A18))) & ';'
    L1V2:A19          = CLIP(SUB(job:fault_code1, 1, LEN_A19)) & ';'
    L1V2:A20          = CLIP(SUB(job:fault_code2, 1, LEN_A20)) & ';'
    ! Start Change 4299 BE(19/05/04)
    !access:reptydef.clearkey(rtd:warranty_key)
    !rtd:warranty    = 'YES'
    !rtd:repair_type = job:repair_type_warranty
    !IF (access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign) THEN
    !    L1V2:A21 = '"' & Upper(Format(rtd:WarrantyCode,@s3))&'";'
    !ELSE
    !    L1V2:A21 = '"' & UPPER(Format(job:repair_type_warranty,@s3)) & '";'
    !END

    access:reptydef.clearkey(rtd:warranty_key)
    rtd:warranty    = 'YES'
    rtd:repair_type = job:repair_type_warranty
    IF (access:reptydef.tryfetch(rtd:warranty_key) = Level:Benign) THEN
        !L1V2:A21 = '"' & Upper(Format(rtd:WarrantyCode,@s3))&'";'
        L1V2:A21 = CLIP(UPPER(SUB(rtd:WarrantyCode, 1, LEN_A21))) & ';'
    ! Start Change 4563 BE(20/08/2004)
    ELSE
        !L1V2:A21 = '"   ";'
        L1V2:A21 = ';'
    ! End Change 4563 BE(20/08/2004)
    END
    ! End Change 4299 BE(19/05/04)
    ! End Change 4160 BE(04/05/04)

    xp#=0
    !L1V2:A22 = '" {9}";'
    !L1V2:A23 = '" {9}";'
    !L1V2:A24 = '" {9}";'
    !L1V2:A25 = '" {9}";'
    !L1V2:A26 = '" {9}";'
    !L1V2:A27 = '" {9}";'
    !L1V2:A28 = '" {9}";'
    !L1V2:A29 = '" {9}";'
    !L1V2:A30 = '" {9}";'
    L1V2:A22 = ';'
    L1V2:A23 = ';'
    L1V2:A24 = ';'
    L1V2:A25 = ';'
    L1V2:A26 = ';'
    L1V2:A27 = ';'
    L1V2:A28 = ';'
    L1V2:A29 = ';'
    L1V2:A30 = ';'
    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:part_number_key)
    wpr:ref_number  = job:ref_number
    SET(wpr:part_number_key,wpr:part_number_key)
    LOOP
        IF (access:warparts.next() <> Level:Benign) THEN
           BREAK
        END
        IF (wpr:ref_number  <> job:ref_number) THEN
            BREAK
        END
        IF (man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT') THEN
            CYCLE
        END

        !Exclude part from EDI?
        IF (ExcludeFromEDI(wpr:Part_Ref_Number)) THEN
            CYCLE
        END

        xp#+=1
        CASE xp#
          OF 1
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A22 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A22 = CLIP(SUB(wpr:part_number, 1, LEN_A22)) & ';'
            END
          OF 2
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A23 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A23 = CLIP(SUB(wpr:part_number, 1, LEN_A23)) & ';'
            END
          OF 3
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A24 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A24 = CLIP(SUB(wpr:part_number, 1, LEN_A24)) & ';'
            END
          OF 4
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A25 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A25 = CLIP(SUB(wpr:part_number, 1, LEN_A25)) & ';'
            END
          OF 5
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A26 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A26 = CLIP(SUB(wpr:part_number, 1, LEN_A26)) & ';'
            END
          OF 6
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A27 = '"'&Format(wpr:part_number,@s9)&'";'
                L1V2:A27 = CLIP(SUB(wpr:part_number, 1, LEN_A27)) & ';'
            END
          OF 7
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A28 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A28 = CLIP(SUB(wpr:part_number, 1, LEN_A28)) & ';'
            END
          OF 8
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A29 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A29 = CLIP(SUB(wpr:part_number, 1, LEN_A29)) & ';'
            END
          OF 9
            IF (wpr:part_number <> 'ADJUSTMENT') THEN
                !L1V2:A30 = '"' & Format(wpr:part_number,@s9) & '";'
                L1V2:A30 = CLIP(SUB(wpr:part_number, 1, LEN_A30)) & ';'
            END

        END

    END
    access:warparts.restorefile(save_wpr_id)

    !L1V2:A31 = '" {9}";'
    !L1V2:A32 = '" {9}";'
    !L1V2:A33 = '" {9}";'
    !L1V2:A34 = '" {9}";'
    !L1V2:A35 = '" {9}";'
    !L1V2:A36 = '" {9}";'
    !L1V2:A37 = '" {9}";'
    L1V2:A31 = ';'
    L1V2:A32 = ';'
    L1V2:A33 = ';'
    L1V2:A34 = ';'
    L1V2:A35 = ';'
    L1V2:A36 = ';'
    L1V2:A37 = ';'

    ! Start Change 3883 BE(10/03/04)
    !L1V2:A38 = '"' & FORMAT(job:date_completed,@d06) & '";'
    !L1V2:A39 = '"' & UPPER(Format(job:model_number,@s30)) & '";'
    !L1V2:A38 = '" {10}";'
    L1V2:A38 = ';'
    ! Start Change 4299 BE(19/05/04)
    !L1V2:A39 = '" {30}";'
    !L1V2:A39 = '"' & FORMAT(FORMAT(job:date_booked,@d06), @s30) & '";'
    ! Start Change 4497 BE(15/07/2004)
    !L1V2:A39 = FORMAT(job:date_booked,@d06) & ';'
    IF (SigmaFlag) THEN
        L1V2:A39 = CLIP(FORMAT(job:fault_code10,@d06B)) & ';'
    ELSE
        L1V2:A39 = CLIP(FORMAT(job:date_booked,@d06B)) & ';'
    END
    ! End Change 4497 BE(15/07/2004)

    ! End Change 4299 BE(19/05/04)
    ! Start Change 3883 BE(10/03/04)
    ! Start Change 4143 BE(16/04/04)
    !L1V2:A40 = '"' & UPPER(Format(job:fault_code9,@s30)) & '";'
    !L1V2:A40 = '" {30}";'
    L1V2:A40 = ';'
    ! End Change 4143 BE(16/04/04)
    !L1V2:A41 = '" {30}";'
    !L1V2:A42 = '" {30}";'
    !L1V2:A43 = '" {30}";'
    !L1V2:A44 = '" {30}"'
    L1V2:A41 = ';'
    L1V2:A42 = ';'
    L1V2:A43 = ';'
    L1V2:A44 = ';'

    count += 1
    DO CopyV2DetailFields
    ADD(out_file)
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
CopyV2HeaderFields    ROUTINE
    OUF:Out_Group.Line1 = CLIP(L2V2:Field1) & CLIP(L2V2:Field2) & CLIP(L2V2:Field3)

CopyV2DetailFields    ROUTINE
    OUF:Out_Group.Line1 = CLIP(L1V2:A0) & CLIP(L1V2:A1) & CLIP(L1V2:A2)  & |
                          CLIP(L1V2:A3) & CLIP(L1V2:A4) & CLIP(L1V2:A5)  & |
                          CLIP(L1V2:A6) & CLIP(L1V2:A7) & CLIP(L1V2:A8)  & |
                          CLIP(L1V2:A9) & CLIP(L1V2:A10) & CLIP(L1V2:A11)  & |
                          CLIP(L1V2:A12) & CLIP(L1V2:A13) & CLIP(L1V2:A14)  & |
                          CLIP(L1V2:A15) & CLIP(L1V2:A16) & CLIP(L1V2:A17)  & |
                          CLIP(L1V2:A18) & CLIP(L1V2:A19) & CLIP(L1V2:A20)  & |
                          CLIP(L1V2:A21) & CLIP(L1V2:A22) & CLIP(L1V2:A23)  & |
                          CLIP(L1V2:A24) & CLIP(L1V2:A25) & CLIP(L1V2:A26)  & |
                          CLIP(L1V2:A27) & CLIP(L1V2:A28) & CLIP(L1V2:A29)  & |
                          CLIP(L1V2:A30) & CLIP(L1V2:A31) & CLIP(L1V2:A32)  & |
                          CLIP(L1V2:A33) & CLIP(L1V2:A34) & CLIP(L1V2:A35)  & |
                          CLIP(L1V2:A36) & CLIP(L1V2:A37) & CLIP(L1V2:A38)  & |
                          CLIP(L1V2:A39) & CLIP(L1V2:A40) & CLIP(L1V2:A41)  & |
                          CLIP(L1V2:A42) & CLIP(L1V2:A43) & CLIP(L1V2:A44)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Sagem_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('fname',fname,'Sagem_EDI',1)
    SolaceViewVars('save_job_id',save_job_id,'Sagem_EDI',1)
    SolaceViewVars('pos',pos,'Sagem_EDI',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Sagem_EDI',1)
    SolaceViewVars('count',count,'Sagem_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Sagem_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Sagem_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Sagem_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Sagem_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Sagem_EDI',1)
    SolaceViewVars('EdiVersion',EdiVersion,'Sagem_EDI',1)
    SolaceViewVars('SigmaFlag',SigmaFlag,'Sagem_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
