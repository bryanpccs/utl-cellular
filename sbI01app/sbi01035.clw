

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01035.INC'),ONCE        !Local module procedure declarations
                     END


Sharp_Excel_EDI      PROCEDURE  (f_batch_number)      ! Declare Procedure
EdiVersion           LONG
EdiAccountNo         STRING(30)
SaveFileID_Group     GROUP,PRE()
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
save_job_id          USHORT,AUTO
                     END
Tmp_Group            GROUP,PRE(tmp)
ManufacturerName     STRING(30)
ProcessBeforeDate    DATE
CountRecords         LONG
                     END
Misc_Group           GROUP,PRE()
pos                  STRING(255)
                     END
Excel_Group          GROUP,PRE()
Excel                SIGNED
excel:DateFormat     STRING('dd mmm yyyy')
excel:ColumnName     STRING(50)
excel:ColumnWidth    REAL
excel:CommentText    STRING(255)
excel:OperatingSystem REAL
excel:Visible        BYTE
                     END
Local_Group          GROUP,PRE(LOC)
ApplicationName      STRING('ServiceBase 2000')
DesktopPath          STRING(255)
Filename             STRING(255)
Path                 STRING(255)
ProgramName          STRING(50)
SpreadsheetCreated   LONG(False)
Text                 STRING(255)
UserName             STRING(50)
Version              STRING('3.1.0000')
                     END
Sheet_Group          GROUP,PRE(sheet)
TempLastCol          STRING(1)
HeadLastCol          STRING('C')
DataLastCol          STRING('O')
HeadSummaryRow       LONG(9)
DataSectionRow       LONG(9)
DataHeaderRow        LONG(11)
                     END
WarParts_Group       GROUP,PRE(wp)
ChaAccessoriesCost   DECIMAL(7,2)
ChaProfit            DECIMAL(7,2)
ChaRevenue           DECIMAL(7,2)
ChaSparesCost        DECIMAL(7,2)
WarAccessoriesCost   DECIMAL(7,2)
WarProfit            DECIMAL(7,2)
WarRevenue           DECIMAL(7,2)
WarSparesCost        DECIMAL(7,2)
Parts                STRING(30),DIM(8)
DefectCode           STRING(30)
RepairCode           STRING(30)
                     END
Debug_Group          GROUP,PRE(debug)
Active               LONG
Count                LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
!---   Excel EQUATES   -------------------------------------------------------------
xlCalculationManual    EQUATE(0FFFFEFD9h) ! XlCalculation
xlCalculationAutomatic EQUATE(0FFFFEFF7h) ! XlCalculation
!----------------------------------------------------

!----------------------------------------------------
xlNone       EQUATE(0FFFFEFD2h)
xlContinuous EQUATE(        1 ) ! xlFillStyle
xlThin       EQUATE(        2 ) ! XlBorderWeight
!----------------------------------------------------

!----------------------------------------------------
xlAutomatic   EQUATE(0FFFFEFF7h) ! Constants.
xlSolid       EQUATE(        1 ) ! Constants.
xlLeft        EQUATE(0FFFFEFDDh) ! Constants.
xlRight       EQUATE(0FFFFEFC8h) ! Constants.
xlCenter      EQUATE(0FFFFEFF4h) ! Constants.
xlLastCell    EQUATE(       11 ) ! Constants.
xlTop         EQUATE(0FFFFEFC0h) ! Constants.
xlBottom      EQUATE(0FFFFEFF5h) ! Constants.

xlTopToBottom EQUATE(        1 ) ! Constants.
!----------------------------------------------------

!----------------------------------------------------
xlDiagonalDown     EQUATE( 5) ! XlBordersIndex
xlDiagonalUp       EQUATE( 6) ! XlBordersIndex
xlEdgeLeft         EQUATE( 7) ! XlBordersIndex
xlEdgeTop          EQUATE( 8) ! XlBordersIndex
xlEdgeBottom       EQUATE( 9) ! XlBordersIndex
xlEdgeRight        EQUATE(10) ! XlBordersIndex
xlInsideVertical   EQUATE(11) ! XlBordersIndex
xlInsideHorizontal EQUATE(12) ! XlBordersIndex
!----------------------------------------------------

!----------------------------------------------------
xlA1         EQUATE(        1 ) ! XlReferenceStyle
xlR1C1       EQUATE(0FFFFEFCAh) ! XlReferenceStyle
!----------------------------------------------------

!----------------------------------------------------
xlFilterCopy    EQUATE(2) ! XlFilterAction
xlFilterInPlace EQUATE(1) ! XlFilterAction
!----------------------------------------------------

!----------------------------------------------------
xlGuess EQUATE(0) ! XlYesNoGuess
xlYes   EQUATE(1) ! XlYesNoGuess
xlNo    EQUATE(2) ! XlYesNoGuess
!----------------------------------------------------

!----------------------------------------------------
xlAscending  EQUATE(1) ! XlSortOrder
xlDescending EQUATE(2) ! XlSortOrder
!----------------------------------------------------

!----------------------------------------------------
xlLandscape EQUATE(2) ! XlPageOrientation
xlPortrait  EQUATE(1) ! XlPageOrientation
!----------------------------------------------------

!----------------------------------------------------
xlDownThenOver EQUATE( 1 ) ! XlOrder
xlOverThenDown EQUATE( 2 ) ! XlOrder
!-----------------------------------------------------------------------------------
!---   MS Office EQUATES   ---------------------------------------------------------
!----------------------------------------------------
msoCTrue          EQUATE(        1 ) ! MsoTriState
msoFalse          EQUATE(        0 ) ! MsoTriState
msoTriStateMixed  EQUATE(0FFFFFFFEh) ! MsoTriState
msoTriStateToggle EQUATE(0FFFFFFFDh) ! MsoTriState
msoTrue           EQUATE(0FFFFFFFFh) ! MsoTriState
!----------------------------------------------------

!----------------------------------------------------
msoScaleFromBottomRight EQUATE(2) ! MsoScaleFrom
msoScaleFromMiddle      EQUATE(1) ! MsoScaleFrom
msoScaleFromTopLeft     EQUATE(0) ! MsoScaleFrom
!----------------------------------------------------

!----------------------------------------------------
msoPropertyTypeBoolean EQUATE(2) ! MsoDocProperties
msoPropertyTypeDate    EQUATE(3) ! MsoDocProperties
msoPropertyTypeFloat   EQUATE(5) ! MsoDocProperties
msoPropertyTypeNumber  EQUATE(1) ! MsoDocProperties
msoPropertyTypeString  EQUATE(4) ! MsoDocProperties
!----------------------------------------------------
!-------------------------------------------------------------------
    MAP
CalcLabour          PROCEDURE(), REAL
DateToString        PROCEDURE( DATE ), STRING
DateToString2       PROCEDURE( DATE ), STRING
LoadJOBSE      PROCEDURE( LONG ), LONG ! BOOL
LoadPARTS            PROCEDURE( LONG, *LONG ), LONG ! BOOL
LoadStock PROCEDURE( LONG ), LONG ! BOOL
LoadSUBTRACC   PROCEDURE( STRING ), LONG ! BOOL
LoadTRADEACC        PROCEDURE( STRING ), LONG ! BOOL
LoadWARPARTS            PROCEDURE( LONG, *LONG ), LONG ! BOOL
WriteDebug                PROCEDURE( STRING )
!-------------------------------------------------------------------
!           INCLUDE('clib.clw'),ONCE
! MODULE('shell32.LIB')
!    SHGetSpecialFolderPath( Unsigned,        *CString,           long,            long)long, Pascal, Raw, proc, NAME('SHGetSpecialFolderPathA')
!
!    SystemParametersInfo(   LONG uAction, LONG uParam, *? lpvParam, LONG fuWinIni),LONG,RAW,PROC,PASCAL,DLL(TRUE),NAME('SystemParametersInfoA')
!
! END !MODULE
!
! !INCLUDE('clib.clw'),ONCE
!!-----------------------------------------------------------------------------------
! Module('winapi')
!     ShellExecute(Unsigned, *CString, *CString, *CString, *CString,Signed),Unsigned, Pascal, Raw, Proc,name('ShellExecuteA')
!     GetDesktopWindow(), Unsigned, Pascal
!     GetTempPathA(ULONG,*Cstring),Ulong,PASCAL,RAW
!     Sleep(unsigned),pascal,name('sleep')
!!    ShellExecute(Unsigned, *CString, *CString, *CString, *CString,Signed),Unsigned, Pascal, Raw, Proc,name('ShellExecuteA')
!!    GetDesktopWindow(), Unsigned, Pascal
! End !MODULE
!-------------------------------------------------------------------
    END !MAP
!-------------------------------------------------------------------
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
!x            SHORT
!
!Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD
!RECORD      RECORD
!Out_Group       Group
!Line1           STRING(2000)
!          . ..
!
!Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
!type         STRING(2)
!date1        String(4)
!date3        String(2)
!date4        String(2)
!jobno        String(6)
!accountno    String(4)
!filler       String(48)
!modelno      String(14)
!serialno     String(14)
!servcode     String(1)
!dop          String(6)
!faultdate    String(6)
!compdate     String(6)
!filler2      String(124)
!faulttext    String(70)
!repairtext   String(70)
!filler3      String(10)
!condcode     String(1)
!symptom1     String(1)
!symptom2     String(1)
!extencode    String(1)
!filler4      String(26)
!cust_name    STRING(30)
!cust_add1    STRING(30)
!cust_add2    STRING(30)
!cust_town    STRING(30)
!cust_cnty    STRING(20)
!cust_pcod    STRING(10)
!cust_tel     STRING(15)
!DealerName   String(30)
!filler5      String(60)
!DealerCity   String(20)
!filler6      String(54)
!fillerx      String(1)
!          .
!!out_fil2    DOS,PRE(ou2),NAME(filename)
!Out_Part   GROUP,OVER(Ouf:Out_Group),PRE(L2)
!type1        STRING(2)
!jobno1       String(6)
!part_no      STRING(14)
!qty          STRING(6)
!filler1      String(14)
!sect_code    STRING(3)
!defect       STRING(1)
!repair       STRING(1)
!PanaInvoice  String(9)
!Filler2      String(32)
!Fillery      String(1)
!         .
!
!mt_total     REAL
!vat          REAL
!total        REAL
!sub_total    REAL
!man_fct      STRING(30)
!field_j       BYTE
!days          REAL
!time          REAL
!day           LONG
!tim1          LONG
!day_count     LONG
!count         REAL
!parts_temp         REAL
!labour_temp        REAL
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Sharp_Excel_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:JOBSTAGE.Open
   Relate:SUBTRACC.Open
   Relate:CHARTYPE.Open
   Relate:DISCOUNT.Open
   Relate:VATCODE.Open
   Relate:PARTS.Open
   Relate:WARPARTS.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:JOBNOTES.Open
   Relate:SUBCHRGE.Open
   Relate:TRACHRGE.Open
   Relate:STDCHRGE.Open
   Relate:EXCHANGE.Open
    DO ProcessJobs
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:JOBSTAGE.Close
   Relate:SUBTRACC.Close
   Relate:CHARTYPE.Close
   Relate:DISCOUNT.Close
   Relate:VATCODE.Close
   Relate:PARTS.Close
   Relate:WARPARTS.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:JOBNOTES.Close
   Relate:SUBCHRGE.Close
   Relate:TRACHRGE.Close
   Relate:STDCHRGE.Close
   Relate:EXCHANGE.Close
!-------------------------------------------------------------------
ProcessJobs         ROUTINE 
    DATA
    CODE
        !-----------------------------------------------------------------
        !debug:Active  = True
        excel:Visible = debug:Active
        !-----------------------------------------------------------------
        tmp:ManufacturerName = 'SHARP'
        !WriteDebug('Start tmp:ManufacturerName="' & CLIP(tmp:ManufacturerName) & '"')

        If f_batch_number = 0
            tmp:ProcessBeforeDate = ProcessBeforeDate()

            IF tmp:ProcessBeforeDate = ''
                !WriteDebug('Create New Batch, tmp:ProcessBeforeDate NOT SET by ProcessBeforeDate()')

                EXIT
            END !IF
        End!If f_batch_number = 0

        IF FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            !WriteDebug('Unable to find manufacturer "' & tmp:ManufacturerName & '"')

            EXIT
        END !IF
        !-----------------------------------------------------------------

        ! Start Change 3808 BE(2/2/04)
        EdiVersion = MAN:SiemensNewEdi
        ! End Change 3808 BE(2/2/04)

        ! Start Change 4116 BE(1/4/04)
        EdiAccountNo = man:edi_account_number
        ! End Change 4116 BE(1/4/04)

        LOC:FileName = LEFT(CLIP(MAN:EDI_Path))

        IF SUB(LOC:FileName, LEN(CLIP(LOC:FileName)), 1) <> '\'
            LOC:FileName = CLIP(LOC:FileName) & '\'
        END !IF

        LOC:FileName = CLIP(CLIP(LOC:FileName) & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.xls')

        ! Start Change 3808 BE(2/2/04)
        !WriteDebug('LOC:FileName="' & CLIP(LOC:FileName) & '"')
        ! End Change 3808 BE(2/2/04)
        !-----------------------------------------------------------------
        !-----Assume - Print all non-printed claims for Manufacturer X------------!
        YIELD()
        RecordsPerCycle = 25
        RecordsProcessed = 0
        PercentProgress = 0

        OPEN(ProgressWindow)
        Progress:Thermometer = 0
        ?Progress:PctText{Prop:Text} = '0% Completed'
        ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

        If f_batch_number = 0
            DO ProcessNewBatch
        ELSE
            ! Start Change 3808 BE(2/2/04)
            !WriteDebug('Check 1 Excel="' & Excel{'ActiveSheet.Name'} & '"' )
            ! End Change 3808 BE(2/2/04)
            DO ProcessOldBatch
            ! Start Change 3808 BE(2/2/04)
            !WriteDebug('Check 2 Excel="' & Excel{'ActiveSheet.Name'} & '"' )
            ! End Change 3808 BE(2/2/04)
        END !IF

        ! test excel here
        !message('POST ProcessXXXBatch, Excel("Sheets.Count")="' & Excel{'Sheets.Count'} & '"')

        IF LOC:SpreadsheetCreated = True
            !-------------------------------------------------------------
            ! Start Change 3808 BE(2/2/04)
            !WriteDebug('Closing Excel="' & Excel{'ActiveSheet.Name'} & '"' )
            !WriteDebug('ActiveWorkBook="' & Excel{'ActiveWorkBook.Name'} & '"' )
            !WriteDebug('Sheets="' & Excel{'Sheets.Count'} & '"' )
            ! End Change 3808 BE(2/2/04)

            Excel{'Sheets("Sheet3").Select'}
            IF Excel{'ActiveSheet.Name'} = 'Sheet3'
                ! Start Change 3808 BE(2/2/04)
                !WriteDebug('Delete(' & Excel{'ActiveSheet.Name'} & ')"')
                ! End Change 3808 BE(2/2/04)
                Excel{'ActiveSheet.Delete'}
            END !IF

            Excel{'Sheets("Sheet1").Select'}
            Excel{'Range("A2").Select'}

            Excel{'ActiveWorkBook.SaveAs("' & CLIP(LOC:FileName) & '")'}
            !Excel{'ActiveSheet.SaveAs("' & LEFT(CLIP(LOC:FileName)) & '")'}
            Excel{'ActiveWorkBook.Close()'}

            ! Start Change 3808 BE(2/2/04)
            !WriteDebug('Sheets="' & Excel{'Sheets.Count'} & '"' )
            ! End Change 3808 BE(2/2/04)

            DO XL_Finalize
            !-------------------------------------------------------------
        END !IF

        If f_batch_number = 0
            Case MessageEx('Created Batch Number ' & Man:Batch_Number-1 & '.'&|
                  '<13,10>'&|
                  '<13,10>Created EDI Batch File: <13,10>' & CLIP(LOC:FileName) & '.','ServiceBase 2000',|
                               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
            End!Case MessageEx
        ELSE

            ! Start Change 2806 BE(22/07/03)
            !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
            !  '<13,10>'&|
            !  '<13,10>EDI Batch File: <13,10>' & CLIP(LOC:FileName) & '.','ServiceBase 2000',|
            !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
            !Of 1 ! &OK Button
            !End!Case MessageEx
            IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>EDI Batch File: <13,10>' & CLIP(LOC:FileName) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
            ELSE
                MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>EDI Batch File: <13,10>' & CLIP(LOC:FileName) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
            END
            ! End Change 2806 BE(22/07/03)

        END !IF

        Close(ProgressWindow)

        ! test excel here
        ! Start Change 3808 BE(2/2/04)
        !WriteDebug('END')
        ! End Change 3808 BE(2/2/04)
        !-----------------------------------------------------------------

ProcessNewBatch             ROUTINE
            !----------------------------------------------------
            !WriteDebug('Create new batch')

            count_records# = 0
            tmp:CountRecords = 0
            Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)
            !----------------------------------------------------
            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:EDI_Key)
                job:Manufacturer = tmp:ManufacturerName
                job:EDI          = 'NO'
            Set(job:EDI_Key,job:EDI_Key)

            Loop WHILE Access:JOBS.NEXT() = Level:Benign
                IF NOT job:Manufacturer = tmp:ManufacturerName
                    !WriteDebug('JOBS EOI job:Manufacturer"' & CLIP(job:Manufacturer) & '" <> tmp:ManufacturerName"' & CLIP(tmp:ManufacturerName) & '"')

                    BREAK
                END !IF

                IF NOT job:EDI          = 'NO'
                    !WriteDebug('JOBS EOI job:EDI="' & job:EDI & '" <>"NO"')

                    BREAK
                END !IF

                If job:Date_Completed > tmp:ProcessBeforeDate
                    !WriteDebug('CYCLE job:Date_Completed"' & FORMAT(job:Date_Completed, @d8) & '" > tmp:ProcessBeforeDate"' & FORMAT(tmp:ProcessBeforeDate, @d8) & '"')

                    Cycle
                End !If job_ali:Date_Completed > tmp:ProcessBeforeDate

                If job:Warranty_Job <> 'YES'
                    !WriteDebug('CYCLE Not a Warranty job')

                    Cycle
                End !If job:Warranty_Job <> ''

                Do GetNextRecord2
                cancelcheck# += 1
                If cancelcheck# > (RecordsToProcess/100)
                    !WriteDebug('Do cancelcheck')

                    Do cancelcheck
                    If tmp:cancel = 1
                        !WriteDebug('Do cancelcheck=BREAK')
                        Break
                    End!If tmp:cancel = 1
                    cancelcheck# = 0
                End!If cancelcheck# > 50
                !--------------------------------------------------------------------
                ! Start Change 3808 BE(2/2/04)
                !Do Export
                CASE (EdiVersion)
                OF 0
                    DO Export
                OF 1
                    DO Export2
                ! Inserting (DBH 15/12/2005) #6871 - New version of export
                Of 2
                    Do ExportVersion2
                ELSE
                    DO ExportVersion2
                ! End (DBH 15/12/2005) #6871
                END
                ! End Change 3808 BE(2/2/04)
                !--------------------------------------------------------------------
                !WriteDebug('job:ignore_warranty_charges="' & job:ignore_warranty_charges & '"')

                If job:ignore_warranty_charges <> 'YES'
                    !WriteDebug('Repricing')

                    Pricing_Routine('W',labour",parts",pass",a")
                    If pass" = True
                        !WriteDebug('Update job with prices')

                        job:labour_cost_warranty = labour"
                        job:parts_cost_warranty  = parts"
                        job:sub_total_warranty   = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                    End!If pass" = False
                End!If job:ignore_warranty_charges <> 'YES'

                pos = Position(job:EDI_Key)
                    !Found
                    job:EDI                      = 'YES'
                    job:EDI_Batch_Number         = Man:Batch_Number
                    tmp:CountRecords            += 1
                    AddToAudit
                    Access:JOBS.Update()
                Reset(job:EDI_Key, pos)
                !WriteDebug('Updated job job:EDI="' & job:EDI & '", Should be "YES", Man:Batch_Number="' & Man:Batch_Number & '"')

            End !Loop
            Access:JOBS.RestoreFile(Save_job_ID)
            !----------------------------------------------------
            !WriteDebug('Exit JOBS')
            !----------------------------------------------------
            If tmp:CountRecords <> 0
                !WriteDebug('tmp:CountRecords"' & tmp:CountRecords & '" <> 0')

                If Access:EDIBATCH.PrimeRecord() = Level:Benign
                    !WriteDebug('Access:EDIBATCH.PrimeRecord()')
                    ! test excel here

                    ebt:Batch_Number    = man:Batch_Number
                    ebt:Manufacturer    = tmp:ManufacturerName
                    If Access:EDIBATCH.TryInsert() = Level:Benign
                        !Insert Successful
                        !WriteDebug('EDIBATCH Insert Successful')
                    Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                        !Insert Failed
                        !WriteDebug('EDIBATCH Insert Failed')
                    End !If Access:EDIBATCH.TryInsert() = Level:Benign

                    Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                    man:Manufacturer = tmp:ManufacturerName
                    If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        !Found
                        !WriteDebug('FOUND Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign')

                        man:Batch_Number += 1
                        Access:MANUFACT.Update()
                    Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        !Error
                        !WriteDebug('FAIL Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign')

                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                
            End !If tmp:CountRecords
            !----------------------------------------------------
ProcessOldBatch             ROUTINE
        !WriteDebug('Old Batch')

            tmp:CountRecords = 0
            Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

            Save_job_ID = Access:JOBS.SaveFile()
            Access:JOBS.ClearKey(job:EDI_Key)
                job:Manufacturer     = tmp:ManufacturerName
                job:EDI              = 'YES'
                job:EDI_Batch_Number = f_Batch_Number
            Set(job:EDI_Key,job:EDI_Key)

            Loop WHILE Access:JOBS.NEXT() = Level:Benign
                If NOT job:Manufacturer = tmp:ManufacturerName
                    !WriteDebug('JOBS EOI, job:Manufacturer"' & CLIP(job:Manufacturer) & '" = tmp:ManufacturerName"' & CLIP(tmp:ManufacturerName) & '"')

                    Break
                End !If

                IF NOT job:EDI = 'YES'
                    !WriteDebug('JOBS EOI, job:EDI"' & job:EDI & '" = "YES"')

                    Break
                End !If

                IF NOT job:EDI_Batch_Number = f_Batch_Number
                    !WriteDebug('JOBS EOI, job:EDI_Batch_Number"' & job:EDI_Batch_Number & '" = f_Batch_Number"' & f_Batch_Number & '"')

                    Break
                End !If

                !WriteDebug('JOBS Loop="' & job:Ref_Number & '"')

                Do GetNextRecord2
                cancelcheck# += 1
                If cancelcheck# > (RecordsToProcess/100)
                    !WriteDebug('Do cancelcheck')

                    Do cancelcheck
                    If tmp:cancel = 1
                        !WriteDebug('Do cancelcheck=Break Pressed')

                        Break
                    End!If tmp:cancel = 1
                    cancelcheck# = 0
                End!If cancelcheck# > 50
                !--------------------------------------------------------------------
                ! Start Change 3808 BE(2/2/04)
                !Do Export
                CASE (EdiVersion)
                OF 0
                    DO Export
                OF 1
                    DO Export2
                ! Inserting (DBH 15/12/2005) #6871 - New version of export
                Of 2
                    Do ExportVersion2
                ELSE
                    DO ExportVersion2
                ! End (DBH 15/12/2005) #6871
                END
                ! End Change 3808 BE(2/2/04)
                !--------------------------------------------------------------------
            End !Loop

            Access:JOBS.RestoreFile(Save_job_ID)

Export      Routine
    DATA
    CODE
        !--------------------------------------------------------------------------------------------
        !WriteDebug('Export Job ' & job:Ref_Number)
        !--------------------------------------------------------------------------------------------
        IF LOC:SpreadsheetCreated = False
            LOC:SpreadsheetCreated = True

            WriteDebug('1st Export Job Found, Create Heading')

            DO CreateHeading
        END !IF

        IF NOT LoadJOBSE(job:Ref_Number)
            jobe:SkillLevel = 0
        End !IF
        !--------------------------------------------------------------------------------------------
        !WriteDebug('Check a Excel="' & Excel{'ActiveSheet.Name'} & '"' )
        DO Calculate_ChaWar_ARCRRC_Costs
           !Excel{'Range("A1").Select'}
            Excel{'ActiveCell.Offset(0, 00).Formula'} = job:Ref_Number                              ! Job Number
            Excel{'ActiveCell.Offset(0, 01).Formula'} = job:Model_Number                            !
            Excel{'ActiveCell.Offset(0, 02).Formula'} = job:ESN                                     ! IMEI
            Excel{'ActiveCell.Offset(0, 03).Formula'} = job:MSN                                     !
            Excel{'ActiveCell.Offset(0, 04).Formula'} = DateToString(job:DOP)                       ! Date Of Purchase
            Excel{'ActiveCell.Offset(0, 05).Formula'} = DateToString(job:Date_Booked)               !
            Excel{'ActiveCell.Offset(0, 06).Formula'} = DateToString(job:Date_Completed)            !
            Excel{'ActiveCell.Offset(0, 07).Formula'} = job:Fault_Code1                             !
            Excel{'ActiveCell.Offset(0, 08).Formula'} = job:Fault_Code2                             !
            Excel{'ActiveCell.Offset(0, 09).Formula'} = wp:Parts[1]                                 ! WarParts
            Excel{'ActiveCell.Offset(0, 10).Formula'} = wp:DefectCode                               !
            Excel{'ActiveCell.Offset(0, 11).Formula'} = wp:RepairCode
            Excel{'ActiveCell.Offset(0, 12).Formula'} = wp:Parts[2]                                 ! Other Part 1
            Excel{'ActiveCell.Offset(0, 13).Formula'} = wp:Parts[3]                                 ! Other Part 2
            Excel{'ActiveCell.Offset(0, 14).Formula'} = wp:Parts[4]                                 ! Other Part 3
            ! Start Change 2249 BE(13/03/03)
            Excel{'ActiveCell.Offset(0, 15).Formula'} = wp:Parts[5]                                 ! Other Part 4
            Excel{'ActiveCell.Offset(0, 16).Formula'} = wp:Parts[6]                                 ! Other Part 5
            Excel{'ActiveCell.Offset(0, 17).Formula'} = wp:Parts[7]                                 ! Other Part 6
            Excel{'ActiveCell.Offset(0, 18).Formula'} = wp:Parts[8]                                 ! Other Part 6
            !Excel{'ActiveCell.Offset(0, 15).Formula'} = job:Repair_Type_Warranty                             !
            !Excel{'ActiveCell.Offset(0, 16).Formula'} = job:Labour_Cost_Warranty                    ! Total Labour (�)
            !Excel{'ActiveCell.Offset(0, 17).Formula'} = wp:WarSparesCost                            ! Total Parts (�)
            !Excel{'ActiveCell.Offset(0, 18).Formula'} = wp:WarSparesCost + job:Labour_Cost_Warranty ! Total (�)
            !Excel{'ActiveCell.Offset(0, 19).Formula'} = man:Account_Number
            Excel{'ActiveCell.Offset(0, 19).Formula'} = job:Repair_Type_Warranty                             !
            Excel{'ActiveCell.Offset(0, 20).Formula'} = job:Labour_Cost_Warranty                    ! Total Labour (�)
            Excel{'ActiveCell.Offset(0, 21).Formula'} = wp:WarSparesCost                            ! Total Parts (�)
            Excel{'ActiveCell.Offset(0, 22).Formula'} = wp:WarSparesCost + job:Labour_Cost_Warranty ! Total (�)
            Excel{'ActiveCell.Offset(0, 23).Formula'} = man:Account_Number
            ! End Change 2249 BE(13/03/03)

            Excel{'ActiveCell.Offset(0, 02).NumberFormat'} = '###############' ! IMEI
            Excel{'ActiveCell.Offset(0, 03).NumberFormat'} = '###############' !
        DO XL_ColFirst
        DO XL_RowDown
        !--------------------------------------------------------------------------------------------
    EXIT
Export2      Routine       ! Start Change 3808 BE(2/2/04)
    DATA
    CODE
        IF (LOC:SpreadsheetCreated = False) THEN
            LOC:SpreadsheetCreated = True
            DO CreateHeading2
        END

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:ref_number
        Access:JOBSE.Fetch(jobe:RefNumberKey)

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number
        SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
        count# = 0
        LOOP
            IF ((Access:WARPARTS.Next() <> level:Benign) OR (wpr:Ref_Number <> job:Ref_Number)) THEN
                BREAK
            END
            count# += 1

            Excel{'ActiveCell.Offset(0, 00).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 01).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 02).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 03).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 04).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 05).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 06).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 07).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 08).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 09).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 10).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 11).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 12).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 13).HorizontalAlignment'} =  xlCenter

            !Excel{'ActiveCell.Offset(0, 00).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 01).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 02).NumberFormat'} = CHR(64)
            Excel{'ActiveCell.Offset(0, 03).NumberFormat'} = '###############'
            ! Start Change 4750 BE(04/10/2004)
            !Excel{'ActiveCell.Offset(0, 04).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 05).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 06).NumberFormat'} = CHR(64)
            Excel{'ActiveCell.Offset(0, 04).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 05).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 06).NumberFormat'} = 'dd/mm/yyyy'
            ! Start Change 4750 BE(04/10/2004)

            !Excel{'ActiveCell.Offset(0, 07).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 08).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 09).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 10).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 11).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 12).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 13).NumberFormat'} = CHR(64)

            Excel{'ActiveCell.Offset(0, 00).Formula'} = CLIP(job:Ref_Number)                        ! Vendor Ref
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(job:Account_Number)         ! Vendor Number
            Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(EdiAccountNo)                ! Vendor Number
            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 02).Formula'} = CLIP(job:Model_Number)                      ! Model Number
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)                              ! Serial Number
            Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:MSN)                               ! Serial Number
            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 04).Formula'} = DateToString2(job:DOP)                      ! Date Of Purchase
            IF ((job:WorkShop = 'YES') AND (jobe:InWorkShopDate <> '')) THEN                        ! Receipt Date
                Excel{'ActiveCell.Offset(0, 05).Formula'} = DateToString2(jobe:InWorkShopDate)
            ELSE
                Excel{'ActiveCell.Offset(0, 05).Formula'} = ''
            END
            Excel{'ActiveCell.Offset(0, 06).Formula'} = DateToString2(job:Date_Completed)           ! Completion Date
            Excel{'ActiveCell.Offset(0, 07).Formula'} = CLIP(job:Repair_Type_Warranty)              ! Claim Type
            Excel{'ActiveCell.Offset(0, 08).Formula'} = CLIP(job:Fault_Code1)                       ! Condition Code
            Excel{'ActiveCell.Offset(0, 09).Formula'} = CLIP(job:Fault_Code2)                       ! Symptom Code
            Excel{'ActiveCell.Offset(0, 10).Formula'} = CLIP(wpr:Fault_Code1)                       ! Defect Code
            Excel{'ActiveCell.Offset(0, 11).Formula'} = CLIP(wpr:Fault_Code2)                       ! Repair Code
            Excel{'ActiveCell.Offset(0, 12).Formula'} = CLIP(wpr:Part_Number)                       ! Part Number
            Excel{'ActiveCell.Offset(0, 13).Formula'} = CLIP(wpr:Quantity)                          ! Qty

            DO XL_ColFirst
            DO XL_RowDown
        END

        IF (count# = 0) THEN

            Excel{'ActiveCell.Offset(0, 00).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 01).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 02).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 03).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 04).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 05).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 06).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 07).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 08).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 09).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 10).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 11).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 12).HorizontalAlignment'} =  xlCenter
            Excel{'ActiveCell.Offset(0, 13).HorizontalAlignment'} =  xlCenter

            !Excel{'ActiveCell.Offset(0, 00).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 01).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 02).NumberFormat'} = CHR(64)
            Excel{'ActiveCell.Offset(0, 03).NumberFormat'} = '###############'
            Excel{'ActiveCell.Offset(0, 04).NumberFormat'} = CHR(64)
            Excel{'ActiveCell.Offset(0, 05).NumberFormat'} = CHR(64)
            Excel{'ActiveCell.Offset(0, 06).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 07).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 08).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 09).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 10).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 11).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 12).NumberFormat'} = CHR(64)
            !Excel{'ActiveCell.Offset(0, 13).NumberFormat'} = CHR(64)

            Excel{'ActiveCell.Offset(0, 00).Formula'} = CLIP(job:Ref_Number)                        ! Vendor Ref
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(job:Account_Number)         ! Vendor Number
            Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(EdiAccountNo)                ! Vendor Number
            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 02).Formula'} = CLIP(job:Model_Number)                      ! Model Number
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)                              ! Serial Number
            Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:MSN)                               ! Serial Number
            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 04).Formula'} = DateToString2(job:DOP)                      ! Date Of Purchase
            IF ((job:WorkShop = 'YES') AND (jobe:InWorkShopDate <> '')) THEN                        ! Receipt Date
                Excel{'ActiveCell.Offset(0, 05).Formula'} = DateToString2(jobe:InWorkShopDate)
            ELSE
                Excel{'ActiveCell.Offset(0, 05).Formula'} = ''
            END
            Excel{'ActiveCell.Offset(0, 06).Formula'} = DateToString2(job:Date_Completed)           ! Completion Date
            Excel{'ActiveCell.Offset(0, 07).Formula'} = CLIP(job:Repair_Type_Warranty)              ! Claim Type
            Excel{'ActiveCell.Offset(0, 08).Formula'} = CLIP(job:Fault_Code1)                       ! Condition Code
            Excel{'ActiveCell.Offset(0, 09).Formula'} = CLIP(job:Fault_Code2)                       ! Symptom Code
            Excel{'ActiveCell.Offset(0, 10).Formula'} = ''                                          ! Defect Code
            Excel{'ActiveCell.Offset(0, 11).Formula'} = ''                                          ! Repair Code
            Excel{'ActiveCell.Offset(0, 12).Formula'} = ''                                          ! Part Number
            Excel{'ActiveCell.Offset(0, 13).Formula'} = ''                                          ! Qty

            DO XL_ColFirst
            DO XL_RowDown
        END
    EXIT
! End Change 3808 BE(2/2/04)
ExportVersion2      Routine
    ! New Version` - TrkBs: 6871 (DBH: 15-12-2005)
    DATA
    CODE
        IF (LOC:SpreadsheetCreated = False) THEN
            LOC:SpreadsheetCreated = True
            DO CreateHeadingVersion2
        END

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:ref_number
        Access:JOBSE.Fetch(jobe:RefNumberKey)

        Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
        wpr:Ref_Number = job:Ref_Number
        SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
        count# = 0
        LOOP
            IF ((Access:WARPARTS.Next() <> level:Benign) OR (wpr:Ref_Number <> job:Ref_Number)) THEN
                BREAK
            END
            count# += 1

            Excel{'ActiveCell.Offset(0, 00).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 01).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 02).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 03).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 04).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 05).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 06).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 07).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 08).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 09).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 10).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 11).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 12).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 13).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 14).HorizontalAlignment'} = xlLeft
            Excel{'ActiveCell.Offset(0, 15).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 16).HorizontalAlignment'} = xlRight
            Excel{'ActiveCell.Offset(0, 17).HorizontalAlignment'} = xlRight

            Excel{'ActiveCell.Offset(0, 03).NumberFormat'} = '###############'
            Excel{'ActiveCell.Offset(0, 04).NumberFormat'} = '###############'
            Excel{'ActiveCell.Offset(0, 05).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 06).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 07).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 16).NumberFormat'} = '######0.00'
            Excel{'ActiveCell.Offset(0, 17).NumberFormat'} = '######0.00'

            Excel{'ActiveCell.Offset(0, 00).Formula'} = CLIP(job:Ref_Number)                        ! Vendor Ref
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(job:Account_Number)         ! Vendor Number
            Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(EdiAccountNo)                ! Vendor Number
            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 02).Formula'} = CLIP(job:Model_Number)                      ! Model Number
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)

            ! Changing (DBH 15/12/2005) #6871 - Use IMEI and add Incoming/Outgoing columns
            ! Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:MSN)                               ! Serial Number
            ! to (DBH 15/12/2005) #6871
            ! Incoming IMEI - TrkBs: 6871 (DBH: 15-12-2005)
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
                jot:RefNumber   = job:Ref_Number
                If Access:JOBTHIRD.Tryfetch(jot:RefNumberKey) = Level:Benign
                    ! Found
                    Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(jot:OriginalIMEI)
                Else ! If Access:JOBTHIRD.Tryfetch(jot:RefNumberKey) = Level:Benign
                    ! Error
                    Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)
                End ! If Access:JOBTHIRD.Tryfetch(jot:RefNumberKey) = Level:Benign
            Else ! If job:Third_Party_Site <> ''
                Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)
            End ! If job:Third_Party_Site <> ''
            If jobe:Pre_RF_Board_IMEI <> ''
                Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(jobe:Pre_RF_Board_IMEI)
            End ! If jobe:Pre_RF_Board_IMEI <> ''
            ! End (DBH 15/12/2005) #6871
            ! Outgoing IMEI - TrkBs: 6871 (DBH: 15-12-2005)
            If job:Exchange_Unit_Number <> ''
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    ! Found
                    Excel{'ActiveCell.Offset(0, 04).Formula'} = CLIP(xch:ESN)
                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    ! Error
                    Excel{'ActiveCell.Offset(0, 04).Formula'} = CLIP(job:ESN)
                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            Else ! If job:Exchange_Unit_Number <> ''
                Excel{'ActiveCell.Offset(0, 04).Formula'} = CLIP(job:ESN)
            End ! If job:Exchange_Unit_Number <> ''

            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 05).Formula'} = DateToString2(job:DOP)                      ! Date Of Purchase
            IF ((job:WorkShop = 'YES') AND (jobe:InWorkShopDate <> '')) THEN                        ! Receipt Date
                Excel{'ActiveCell.Offset(0, 06).Formula'} = DateToString2(jobe:InWorkShopDate)
            ELSE
                Excel{'ActiveCell.Offset(0, 06).Formula'} = ''
            END
            Excel{'ActiveCell.Offset(0, 07).Formula'} = DateToString2(job:Date_Completed)           ! Completion Date
            Excel{'ActiveCell.Offset(0, 08).Formula'} = CLIP(job:Repair_Type_Warranty)              ! Claim Type
            ! Inserting (DBH 15/12/2005) #6871 - Add Section Code
            Excel{'ActiveCell.Offset(0, 09).Formula'} = CLIP(wpr:Fault_Code3)                       ! Section Code
            ! End (DBH 15/12/2005) #6871
            Excel{'ActiveCell.Offset(0, 10).Formula'} = CLIP(job:Fault_Code1)                       ! Condition Code
            Excel{'ActiveCell.Offset(0, 11).Formula'} = CLIP(job:Fault_Code2)                       ! Symptom Code
            Excel{'ActiveCell.Offset(0, 12).Formula'} = CLIP(wpr:Fault_Code1)                       ! Defect Code
            Excel{'ActiveCell.Offset(0, 13).Formula'} = CLIP(wpr:Fault_Code2)                       ! Repair Code
            Excel{'ActiveCell.Offset(0, 14).Formula'} = CLIP(wpr:Part_Number)                       ! Part Number
            Excel{'ActiveCell.Offset(0, 15).Formula'} = CLIP(wpr:Quantity)                          ! Qty
            Excel{'ActiveCell.Offset(0, 16).Formula'} = Format(job:Labour_Cost_Warranty,@n14.2)     ! Labour Cost
            Excel{'ActiveCell.Offset(0, 17).Formula'} = Format(job:Parts_Cost_Warranty,@n14.2)      ! Parts Cost

            DO XL_ColFirst
            DO XL_RowDown
        END

        IF (count# = 0) THEN
            Excel{'ActiveCell.Offset(0, 00).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 01).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 02).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 03).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 04).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 05).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 06).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 07).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 08).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 09).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 10).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 11).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 12).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 13).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 14).HorizontalAlignment'} = xlLeft
            Excel{'ActiveCell.Offset(0, 15).HorizontalAlignment'} = xlCenter
            Excel{'ActiveCell.Offset(0, 16).HorizontalAlignment'} = xlRight
            Excel{'ActiveCell.Offset(0, 17).HorizontalAlignment'} = xlRight

            Excel{'ActiveCell.Offset(0, 03).NumberFormat'} = '###############'
            Excel{'ActiveCell.Offset(0, 04).NumberFormat'} = '###############'
            Excel{'ActiveCell.Offset(0, 05).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 06).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 07).NumberFormat'} = 'dd/mm/yyyy'
            Excel{'ActiveCell.Offset(0, 16).NumberFormat'} = '######0.00'
            Excel{'ActiveCell.Offset(0, 17).NumberFormat'} = '######0.00'

            Excel{'ActiveCell.Offset(0, 00).Formula'} = CLIP(job:Ref_Number)                        ! Vendor Ref
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(job:Account_Number)         ! Vendor Number
            Excel{'ActiveCell.Offset(0, 01).Formula'} = '80000' & CLIP(EdiAccountNo)                ! Vendor Number
            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 02).Formula'} = CLIP(job:Model_Number)                      ! Model Number
            ! Start Change 4116 BE(01/04/2004)
            !Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)                              ! Serial Number
            ! Changing (DBH 15/12/2005) #6871 - Use IMEI and add Incoming/Outgoing columns
            ! Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:MSN)                               ! Serial Number
            ! to (DBH 15/12/2005) #6871
            ! Incoming IMEI - TrkBs: 6871 (DBH: 15-12-2005)
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.Clearkey(jot:RefNumberKey)
                jot:RefNumber   = job:Ref_Number
                If Access:JOBTHIRD.Tryfetch(jot:RefNumberKey) = Level:Benign
                    ! Found
                    Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(jot:OriginalIMEI)
                Else ! If Access:JOBTHIRD.Tryfetch(jot:RefNumberKey) = Level:Benign
                    ! Error
                    Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)
                End ! If Access:JOBTHIRD.Tryfetch(jot:RefNumberKey) = Level:Benign
            Else ! If job:Third_Party_Site <> ''
                Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(job:ESN)
            End ! If job:Third_Party_Site <> ''
            If jobe:Pre_RF_Board_IMEI <> ''
                Excel{'ActiveCell.Offset(0, 03).Formula'} = CLIP(jobe:Pre_RF_Board_IMEI)
            End ! If jobe:Pre_RF_Board_IMEI <> ''
            ! End (DBH 15/12/2005) #6871
            ! Outgoing IMEI - TrkBs: 6871 (DBH: 15-12-2005)
            If job:Exchange_Unit_Number <> ''
                Access:EXCHANGE.Clearkey(xch:Ref_Number_Key)
                xch:Ref_Number  = job:Exchange_Unit_Number
                If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    ! Found
                    Excel{'ActiveCell.Offset(0, 04).Formula'} = CLIP(xch:ESN)
                Else ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
                    ! Error
                    Excel{'ActiveCell.Offset(0, 04).Formula'} = CLIP(job:ESN)
                End ! If Access:EXCHANGE.Tryfetch(xch:Ref_Number_Key) = Level:Benign
            Else ! If job:Exchange_Unit_Number <> ''
                Excel{'ActiveCell.Offset(0, 04).Formula'} = CLIP(job:ESN)
            End ! If job:Exchange_Unit_Number <> ''

            ! End Change 4116 BE(01/04/2004)

            ! End Change 4116 BE(01/04/2004)
            Excel{'ActiveCell.Offset(0, 05).Formula'} = DateToString2(job:DOP)                      ! Date Of Purchase
            IF ((job:WorkShop = 'YES') AND (jobe:InWorkShopDate <> '')) THEN                        ! Receipt Date
                Excel{'ActiveCell.Offset(0, 06).Formula'} = DateToString2(jobe:InWorkShopDate)
            ELSE
                Excel{'ActiveCell.Offset(0, 06).Formula'} = ''
            END
            Excel{'ActiveCell.Offset(0, 07).Formula'} = DateToString2(job:Date_Completed)           ! Completion Date
            Excel{'ActiveCell.Offset(0, 08).Formula'} = CLIP(job:Repair_Type_Warranty)              ! Claim Type
            ! Inserting (DBH 15/12/2005) #6871 - Add Section Code
            Excel{'ActiveCell.Offset(0, 09).Formula'} = CLIP(wpr:Fault_Code3)                       ! Section Code
            ! End (DBH 15/12/2005) #6871
            Excel{'ActiveCell.Offset(0, 10).Formula'} = CLIP(job:Fault_Code1)                       ! Condition Code
            Excel{'ActiveCell.Offset(0, 11).Formula'} = CLIP(job:Fault_Code2)                       ! Symptom Code
            Excel{'ActiveCell.Offset(0, 12).Formula'} = ''                                          ! Defect Code
            Excel{'ActiveCell.Offset(0, 13).Formula'} = ''                                          ! Repair Code
            Excel{'ActiveCell.Offset(0, 14).Formula'} = ''                                          ! Part Number
            Excel{'ActiveCell.Offset(0, 15).Formula'} = ''                                          ! Qty
            Excel{'ActiveCell.Offset(0, 16).Formula'} = Format(job:Labour_Cost_Warranty,@n14.2)     ! Labour Cost
            Excel{'ActiveCell.Offset(0, 17).Formula'} = Format(job:Parts_Cost_Warranty,@n14.2)      ! Parts Cost

            DO XL_ColFirst
            DO XL_RowDown
        END
    EXIT
CreateHeading               ROUTINE
    DATA
    CODE
        DO XL_Setup
        DO XL_AddWorkbook

        !message('POST XL_AddWorkbook, Excel("Sheets.Count")="' & Excel{'Sheets.Count'} & '"')

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 12.14
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'} = 08.43
        Excel{'ActiveSheet.Columns("C:C").ColumnWidth'} = 18.29
        Excel{'ActiveSheet.Columns("D:D").ColumnWidth'} = 11.86
        Excel{'ActiveSheet.Columns("E:E").ColumnWidth'} = 10.43
        Excel{'ActiveSheet.Columns("F:F").ColumnWidth'} = 12.43
        Excel{'ActiveSheet.Columns("G:G").ColumnWidth'} = 10.29
        Excel{'ActiveSheet.Columns("H:H").ColumnWidth'} = 10.14
        Excel{'ActiveSheet.Columns("I:I").ColumnWidth'} = 09.43
        Excel{'ActiveSheet.Columns("J:J").ColumnWidth'} = 26.57
        Excel{'ActiveSheet.Columns("K:K").ColumnWidth'} = 05.86
        ! Start Change 2249 BE(13/03/03)
        !Excel{'ActiveSheet.Columns("L:O").ColumnWidth'} = 08.43
        !Excel{'ActiveSheet.Columns("P:P").ColumnWidth'} = 26.57
        !Excel{'ActiveSheet.Columns("Q:S").ColumnWidth'} = 08.43
        !Excel{'ActiveSheet.Columns("T:T").ColumnWidth'} = 13.29
        Excel{'ActiveSheet.Columns("L:S").ColumnWidth'} = 08.43
        Excel{'ActiveSheet.Columns("T:T").ColumnWidth'} = 26.57
        Excel{'ActiveSheet.Columns("U:W").ColumnWidth'} = 08.43
        Excel{'ActiveSheet.Columns("X:X").ColumnWidth'} = 13.29
        ! End Change 2249 BE(13/03/03)

        Excel{'Range("A1").Select'}
          Excel{'ActiveCell.Offset(0, 00).Formula'} = 'Job Number'
          Excel{'ActiveCell.Offset(0, 01).Formula'} = 'Model'
          Excel{'ActiveCell.Offset(0, 02).Formula'} = 'Faulty IMEI'
          Excel{'ActiveCell.Offset(0, 03).Formula'} = '2nd Serial Number'
          Excel{'ActiveCell.Offset(0, 04).Formula'} = 'D.O.P.'
          Excel{'ActiveCell.Offset(0, 05).Formula'} = 'Date Booked-in'
          Excel{'ActiveCell.Offset(0, 06).Formula'} = 'Date Completed'
          Excel{'ActiveCell.Offset(0, 07).Formula'} = 'Condition Code'
          Excel{'ActiveCell.Offset(0, 08).Formula'} = 'IRIS Code'
          Excel{'ActiveCell.Offset(0, 09).Formula'} = '1st Pt Used'
          Excel{'ActiveCell.Offset(0, 10).Formula'} = 'Defect Code'
          Excel{'ActiveCell.Offset(0, 11).Formula'} = 'Repair Code'
          Excel{'ActiveCell.Offset(0, 12).Formula'} = 'Other Part 1'
          Excel{'ActiveCell.Offset(0, 13).Formula'} = 'Other Part 2'
          Excel{'ActiveCell.Offset(0, 14).Formula'} = 'Other Part 3'
          ! Start Change 2249 BE(13/03/03)
          !Excel{'ActiveCell.Offset(0, 15).Formula'} = 'Repair Level'
          !Excel{'ActiveCell.Offset(0, 16).Formula'} = 'Total Labour (�)'
          !Excel{'ActiveCell.Offset(0, 17).Formula'} = 'Total Parts (�)'
          !Excel{'ActiveCell.Offset(0, 18).Formula'} = 'Total (�)'
          !Excel{'ActiveCell.Offset(0, 19).Formula'} = 'INTEC A/C NO.'
          Excel{'ActiveCell.Offset(0, 15).Formula'} = 'Other Part 4'
          Excel{'ActiveCell.Offset(0, 16).Formula'} = 'Other Part 5'
          Excel{'ActiveCell.Offset(0, 17).Formula'} = 'Other Part 6'
          Excel{'ActiveCell.Offset(0, 18).Formula'} = 'Other Part 7'
          Excel{'ActiveCell.Offset(0, 19).Formula'} = 'Repair Level'
          Excel{'ActiveCell.Offset(0, 20).Formula'} = 'Total Labour (�)'
          Excel{'ActiveCell.Offset(0, 21).Formula'} = 'Total Parts (�)'
          Excel{'ActiveCell.Offset(0, 22).Formula'} = 'Total (�)'
          Excel{'ActiveCell.Offset(0, 23).Formula'} = 'INTEC A/C NO.'
          ! Start Change 2249 BE(13/03/03)

          ! Start Change 2249 BE(13/03/03)
          !Excel{'Range("A1:T1").Select'}
          Excel{'Range("A1:X1").Select'}
          ! Start Change 2249 BE(13/03/03)
            DO XL_SetGrid
            DO XL_SetBold
            DO XL_SetWrapText
            Excel{'Selection.Interior.ColorIndex'}        = 34 ! CYAN
            Excel{'Selection.Interior.Pattern'}           = xlSolid
            Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic


          ! Start Change 2249 BE(13/03/03)
          !Excel{'Range("Q:S").Select'}
          Excel{'Range("U:W").Select'}
          ! End Change 2249 BE(13/03/03)
            Excel{'Selection.NumberFormat'} = '#,##0.00'

        Excel{'Range("A2").Select'}
    EXIT
CreateHeading2               ROUTINE
    DATA
    CODE
        DO XL_Setup
        DO XL_AddWorkbook

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 10.43
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'} = 23.71
        Excel{'ActiveSheet.Columns("C:C").ColumnWidth'} = 13.57
        Excel{'ActiveSheet.Columns("D:D").ColumnWidth'} = 15.43   !13.43
        Excel{'ActiveSheet.Columns("E:E").ColumnWidth'} = 15.29
        Excel{'ActiveSheet.Columns("F:F").ColumnWidth'} = 15.57
        Excel{'ActiveSheet.Columns("G:G").ColumnWidth'} = 15.43
        Excel{'ActiveSheet.Columns("H:H").ColumnWidth'} = 12.57 ! 10.57
        Excel{'ActiveSheet.Columns("I:I").ColumnWidth'} = 14.29
        Excel{'ActiveSheet.Columns("J:J").ColumnWidth'} = 14.14
        Excel{'ActiveSheet.Columns("K:K").ColumnWidth'} = 15.14
        Excel{'ActiveSheet.Columns("L:L").ColumnWidth'} = 15.00
        Excel{'ActiveSheet.Columns("M:M").ColumnWidth'} = 15.00
        Excel{'ActiveSheet.Columns("N:N").ColumnWidth'} = 8.00

        Excel{'Range("A1").Select'}
        Excel{'ActiveCell.Offset(0, 00).Formula'} = 'Vendor Ref'
        Excel{'ActiveCell.Offset(0, 01).Formula'} = 'Vendor Number'
        Excel{'ActiveCell.Offset(0, 02).Formula'} = 'Model Number'
        Excel{'ActiveCell.Offset(0, 03).Formula'} = 'Serial Number'
        Excel{'ActiveCell.Offset(0, 04).Formula'} = 'D.O.P.'
        Excel{'ActiveCell.Offset(0, 05).Formula'} = 'Receipt Date'
        Excel{'ActiveCell.Offset(0, 06).Formula'} = 'Completion Date'
        Excel{'ActiveCell.Offset(0, 07).Formula'} = 'Claim Type'
        Excel{'ActiveCell.Offset(0, 08).Formula'} = 'Condition Code'
        Excel{'ActiveCell.Offset(0, 09).Formula'} = 'Symptom Code'
        Excel{'ActiveCell.Offset(0, 10).Formula'} = 'Defect Code'
        Excel{'ActiveCell.Offset(0, 11).Formula'} = 'Repair Code'
        Excel{'ActiveCell.Offset(0, 12).Formula'} = 'Part Number'
        Excel{'ActiveCell.Offset(0, 13).Formula'} = 'Qty'

        Excel{'Range("A1:N1").Select'}

        DO XL_SetGrid
        DO XL_SetBold
        !DO XL_SetWrapText
        Excel{'Selection.Interior.ColorIndex'}        = 34 ! CYAN
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic

        Excel{'Range("A1").Select'}
        Excel{'ActiveCell.Offset(0, 00).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 01).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 02).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 03).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 04).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 05).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 06).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 07).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 08).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 09).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 10).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 11).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 12).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 13).HorizontalAlignment'} = xlCenter

        Excel{'Range("A2").Select'}
    EXIT
CreateHeadingVersion2               ROUTINE
    DATA
    CODE
        DO XL_Setup
        DO XL_AddWorkbook

        Excel{'ActiveSheet.Columns("A:A").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("B:B").ColumnWidth'} = 15
        Excel{'ActiveSheet.Columns("C:C").ColumnWidth'} = 15
        Excel{'ActiveSheet.Columns("D:D").ColumnWidth'} = 25
        Excel{'ActiveSheet.Columns("E:E").ColumnWidth'} = 25
        Excel{'ActiveSheet.Columns("F:F").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("G:G").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("H:H").ColumnWidth'} = 16
        Excel{'ActiveSheet.Columns("I:I").ColumnWidth'} = 25
        Excel{'ActiveSheet.Columns("J:J").ColumnWidth'} = 15
        Excel{'ActiveSheet.Columns("K:K").ColumnWidth'} = 15
        Excel{'ActiveSheet.Columns("L:L").ColumnWidth'} = 15
        Excel{'ActiveSheet.Columns("M:M").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("N:N").ColumnWidth'} = 12
        Excel{'ActiveSheet.Columns("O:O").ColumnWidth'} = 25
        Excel{'ActiveSheet.Columns("P:P").ColumnWidth'} = 5
        Excel{'ActiveSheet.Columns("Q:Q").ColumnWidth'} = 15
        Excel{'ActiveSheet.Columns("R:R").ColumnWidth'} = 15

        Excel{'Range("A1").Select'}
        Excel{'ActiveCell.Offset(0, 00).Formula'} = 'Vendor Ref'
        Excel{'ActiveCell.Offset(0, 01).Formula'} = 'Vendor Number'
        Excel{'ActiveCell.Offset(0, 02).Formula'} = 'Model Number'
        ! Changing (DBH 15/12/2005) #6871 - Change from MSN to Incoming/Outgoing IMEI
        ! Excel{'ActiveCell.Offset(0, 03).Formula'} = 'Serial Number'
        ! to (DBH 15/12/2005) #6871
        Excel{'ActiveCell.Offset(0, 03).Formula'} = 'Incoming IMEI Number'
        Excel{'ActiveCell.Offset(0, 04).Formula'} = 'Outgoing IMEI Number'
        ! End (DBH 15/12/2005) #6871

        Excel{'ActiveCell.Offset(0, 05).Formula'} = 'D.O.P.'
        Excel{'ActiveCell.Offset(0, 06).Formula'} = 'Receipt Date'
        Excel{'ActiveCell.Offset(0, 07).Formula'} = 'Completion Date'
        Excel{'ActiveCell.Offset(0, 08).Formula'} = 'Claim Type'
        ! Inserting (DBH 15/12/2005) #6871 - New Column
        Excel{'ActiveCell.Offset(0, 09).Formula'} = 'Section Code'
        ! End (DBH 15/12/2005) #6871
        Excel{'ActiveCell.Offset(0, 10).Formula'} = 'Condition Code'
        Excel{'ActiveCell.Offset(0, 11).Formula'} = 'Symptom Code'
        Excel{'ActiveCell.Offset(0, 12).Formula'} = 'Defect Code'
        Excel{'ActiveCell.Offset(0, 13).Formula'} = 'Repair Code'
        Excel{'ActiveCell.Offset(0, 14).Formula'} = 'Part Number'
        Excel{'ActiveCell.Offset(0, 15).Formula'} = 'Qty'
        Excel{'ActiveCell.Offset(0, 16).Formula'} = 'Labour Price'
        Excel{'ActiveCell.Offset(0, 17).Formula'} = 'Part Price'

        Excel{'Range("A1:R1").Select'}

        DO XL_SetGrid
        DO XL_SetBold
        !DO XL_SetWrapText
        Excel{'Selection.Interior.ColorIndex'}        = 34 ! CYAN
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic

        Excel{'Range("A1").Select'}
        Excel{'ActiveCell.Offset(0, 00).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 01).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 02).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 03).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 04).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 05).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 06).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 07).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 08).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 09).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 10).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 11).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 12).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 13).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 14).HorizontalAlignment'} = xlLeft
        Excel{'ActiveCell.Offset(0, 15).HorizontalAlignment'} = xlCenter
        Excel{'ActiveCell.Offset(0, 16).HorizontalAlignment'} = xlRight
        Excel{'ActiveCell.Offset(0, 17).HorizontalAlignment'} = xlRight

        Excel{'Range("A2").Select'}
    EXIT
Calculate_ChaWar_ARCRRC_Costs                      ROUTINE
    DATA
ParFirst LONG(True)
WprFirst LONG(True)
!JptFirst LONG(True)
!
PartIndex LONG(0)

!Cha_RRC LONG(1)
!Cha_ARC LONG(2)
!War_RRC LONG(3)
!War_ARC LONG(4)
    CODE
        !-----------------------------------------------------------------
        !WriteDebug('Calculate_ChaWar_ARCRRC_Costs')

        ! Start Change 3808 BE(2/2/04)
        !IF CancelPressed
        !    EXIT
        !END !IF
        ! End Change 3808 BE(2/2/04)
        !-----------------------------------------------------------------
        CLEAR(WarParts_Group)
!        CLEAR(ChaWar_ARCRRC_Group[Cha_RRC])
!        CLEAR(ChaWar_ARCRRC_Group[Cha_ARC])
!        CLEAR(ChaWar_ARCRRC_Group[War_RRC])
!        CLEAR(ChaWar_ARCRRC_Group[War_ARC])
!        !-----------------------------------------------------------------
!        wp:ChaAccessoriesCost = 0
!        wp:ChaSparesCost      = 0
!        wp:ChaProfit          = 0
!        wp:ChaRevenue         = 0
!
!        IF job:Chargeable_Job = 'YES'
!            LOOP WHILE LoadPARTS(job:Ref_Number, ParFirst)
!!                calc:SparesCostPrice[Cha_RRC] += par:RRCAveragePurchaseCost
!!                calc:SparesCostPrice[Cha_ARC] += par:RRCAveragePurchaseCost
!
!                IF LoadSTOCK(par:Part_Ref_Number)
!                    IF (sto:accessory = 'YES')
!                        wp:ChaAccessoriesCost += par:quantity * par:purchase_cost
!                    ELSE
!                        wp:ChaSparesCost      += par:quantity * par:purchase_cost
!                    END !IF
!                END !IF
!            END !LOOP
!
!            wp:ChaRevenue = job:Sub_Total
!            wp:ChaProfit  = wp:ChaRevenue - wp:ChaAccessoriesCost - wp:ChaSparesCost
            !-----------------------------------------------------------------
!            LOOP WHILE LoadJOBPAYMT(job:Ref_Number, JptFirst)
!                calc:AmountPaid[Cha_RRC]      += jpt:Amount
!                calc:AmountPaid[Cha_ARC]      += jpt:Amount
!            END !LOOP
            !-----------------------------------------------------------------
!            calc:SparesSellingPrice[Cha_RRC]   = jobe:InvRRCCPartsCost
!            calc:LabourRate[Cha_RRC]           = jobe:InvRRCCLabourCost
!            calc:HandlingFee[Cha_RRC]          = jobe:InvoiceHandlingFee
!            calc:ExchangeFee[Cha_RRC]          = jobe:InvoiceExchangeRate
!
!            calc:VAT[Cha_RRC]                  = (jobe:InvRRCCLabourCost   * (inv:RRCVatRateLabour / 100))
!            calc:VAT[Cha_RRC]                 += (jobe:InvRRCCPartsCost    * (inv:RRCVatRateParts  / 100))
!            calc:VAT[Cha_RRC]                 += (job:Invoice_Courier_Cost * (inv:RRCVatRateParts  / 100))
!
!            calc:TotalJobValue[Cha_RRC]        = jobe:InvRRCCLabourCost + jobe:InvRRCCPartsCost + job:Invoice_Courier_Cost + calc:VAT[Cha_RRC]
!            !-----------------------------------------------------------------
!            calc:SparesSellingPrice[Cha_ARC]   = job:Invoice_Parts_Cost
!            calc:LabourRate[Cha_ARC]           = job:Invoice_Labour_Cost
!            calc:HandlingFee[Cha_ARC]          = jobe:InvoiceHandlingFee
!            calc:ExchangeFee[Cha_ARC]          = jobe:InvoiceExchangeRate
!
!            calc:VAT[Cha_RRC]                  = (jobe:InvRRCCLabourCost   * (inv:Vat_Rate_Labour / 100))
!            calc:VAT[Cha_RRC]                 += (jobe:InvRRCCPartsCost    * (inv:Vat_Rate_Parts  / 100))
!            calc:VAT[Cha_ARC]                 += (job:Invoice_Courier_Cost * (inv:Vat_Rate_Parts  / 100))
!
!            calc:TotalJobValue[Cha_ARC]        = job:Invoice_Labour_Cost + job:Invoice_Parts_Cost + job:Invoice_Courier_Cost + calc:VAT[Cha_ARC]
!            !-------------------------------------------------------------
!        END !IF
        !-----------------------------------------------------------------
        wp:WarAccessoriesCost = 0
        wp:WarSparesCost      = 0
        wp:WarProfit          = 0
        wp:WarRevenue         = 0

        IF job:Warranty_Job = 'YES'
            LOOP WHILE LoadWARPARTS(job:Ref_Number, WprFirst)
                !WriteDebug('LoadWARPARTS(job:Ref_Number"' & job:Ref_Number & '", wpr:Part_Number"' & CLIP(wpr:Part_Number) & '"')

                ! Start Change 2249 BE(13/03/03)
                !IF PartIndex < 4
                IF PartIndex < 8
                ! End Change 2249 BE(13/03/03)
                    PartIndex           += 1
                    wp:Parts[PartIndex]  = wpr:Part_Number

                    IF PartIndex = 1
                        wp:DefectCode = wpr:Fault_Code1
                        wp:RepairCode = wpr:Fault_Code2
                    END !IF
                END !IF
!                calc:SparesSellingPrice[War_RRC]    = wpr:RRCSaleCost
!                calc:SparesSellingPrice[War_ARC]    = wpr:Sale_Cost


                IF LoadSTOCK(wpr:Part_Ref_Number)
                    IF (sto:accessory = 'YES')
                        wp:WarAccessoriesCost += wpr:quantity * wpr:purchase_cost
                    ELSE
                        wp:WarSparesCost      += wpr:quantity * wpr:purchase_cost
                    END !IF
                END !IF
            END !LOOP

            wp:WarRevenue = job:Sub_Total_Warranty
            wp:WarProfit  = wp:WarRevenue - wp:WarAccessoriesCost - wp:WarSparesCost
            !-----------------------------------------------------------------
!            calc:AmountPaid[War_RRC]           = 0 ! No payment taken for warranty jobs.
!
!            calc:SparesCostPrice[War_RRC]      = jobe:InvRRCWPartsCost
!            calc:LabourRate[War_RRC]           = jobe:InvRRCWLabourCost
!            calc:HandlingFee[War_RRC]          = jobe:InvoiceHandlingFee
!            calc:ExchangeFee[War_RRC]          = jobe:InvoiceExchangeRate
!
!            calc:VAT[War_RRC]                  = (jobe:InvRRCWLabourCost    * (inv:RRCVatRateLabour / 100))
!            calc:VAT[War_RRC]                 += (jobe:InvRRCWPartsCost     * (inv:RRCVatRateParts  / 100))
!            calc:VAT[War_RRC]                 += (job:WInvoice_Courier_Cost * (inv:RRCVatRateParts  / 100))
!
!            calc:TotalJobValue[War_RRC]        = jobe:InvRRCWLabourCost + jobe:InvRRCWPartsCost + job:WInvoice_Courier_Cost + calc:VAT[War_RRC]
            !-------------------------------------------------------------
!            calc:AmountPaid[War_ARC]           = 0 ! No payment taken for warranty jobs.
!
!            calc:SparesCostPrice[War_ARC]      = job:WInvoice_Parts_Cost
!            calc:LabourRate[War_ARC]           = job:WInvoice_Labour_Cost
!            calc:HandlingFee[War_ARC]          = jobe:InvoiceHandlingFee
!            calc:ExchangeFee[War_ARC]          = jobe:InvoiceExchangeRate
!
!            calc:VAT[War_ARC]                  = (job:WInvoice_Labour_Cost  * (inv:Vat_Rate_Labour / 100))
!            calc:VAT[War_ARC]                 += (job:WInvoice_Parts_Cost   * (inv:Vat_Rate_Parts  / 100))
!            calc:VAT[War_ARC]                 += (job:WInvoice_Courier_Cost * (inv:Vat_Rate_Parts  / 100))
!
!            calc:TotalJobValue[War_ARC]        = job:WInvoice_Labour_Cost + job:WInvoice_Parts_Cost + job:WInvoice_Courier_Cost + calc:VAT[War_ARC]
            !-------------------------------------------------------------
        END !IF
        !-----------------------------------------------------------------
        !Change to add (06 Dec 2002)
        !THIRD PARTY UNITS:
        !
        ! If a job has been sent to third party, I.e. job:third_party_site <> '',
        ! The Chargeable labour costs, both ARC and RRC are then equal to the 3rd Party Costs
        !
        ! job:Labour_Cost = jobe:ARC3rdPartyCost + jobe:ARC3rdPartyMarkup
        ! jobe:RRCCLabourCost = jobe:ARC3rdPartyCost + jobe:ARC3rdPartyMarkup
        !
        ! If you need to work out the VAT on this amount, then the Third Party TRDPARTY entry has  a vat rate.
        !
!        IF job:third_party_site <> ''
!            calc:Thd_PtyCost = jobe:ARC3rdPartyCost + jobe:ARC3rdPartyMarkup
!
!            Thd_PtyTax       = (job:WInvoice_Courier_Cost * (inv:Vat_Rate_Parts  / 100))
!        END !IF
        !-----------------------------------------------------------------
    EXIT
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
!Old_Code            ROUTINE
!!**Variable
!    tmp:ManufacturerName = 'SHARP'
!    continue# = 0
!    If f_batch_number = 0
!        tmp:ProcessBeforeDate   = ProcessBeforeDate()
!        If tmp:ProcessBeforeDate <> ''
!            continue# = 1
!        End!If tmp:ProcessBeforeDate <> ''
!    Else!If f_batch_number = 0
!        continue# = 1
!    End!If f_batch_number = 0
!    If continue# = 1
!        ManError# = 0
!        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
!            ManError# = 1
!        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign
!
!        If ManError# = 0
!
!            Error# = 0
!!**Variable
!            IF f_batch_number = 0
!                FileName = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.DAT')
!            ELSE
!                FileName = CLIP(CLIP(MAN:EDI_Path) & '\' & Format(man:edi_account_number,@s4) & Format(Month(Today()),@n02) & Format(Day(Today()),@n02) &'.DAT')
!            END
!            OPEN(Out_File)                           ! Open the output file
!            IF ERRORCODE()                           ! If error
!                CREATE(Out_File)                       ! create a new file
!                If Errorcode()
!                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
!                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                        Of 1 ! &OK Button
!                    End!Case MessageEx
!                    Error# = 1
!                End!If Errorcode() 
!                OPEN(out_file)        ! If still error then stop
!            ELSE
!                OPEN(out_file)
!                EMPTY(Out_file)
!            END
!!**Variable
!          !-----Assume - Print all non-printed claims for Manufacturer X------------!
!            If Error# = 0
!
!                YIELD()
!                RecordsPerCycle = 25
!                RecordsProcessed = 0
!                PercentProgress = 0
!
!                OPEN(ProgressWindow)
!                Progress:Thermometer = 0
!                ?Progress:PctText{Prop:Text} = '0% Completed'
!                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'
!
!                If f_batch_number = 0
!                    count_records# = 0
!                    tmp:CountRecords = 0
!                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)
!
!                    Save_job_ID = Access:JOBS.SaveFile()
!                    Access:JOBS.ClearKey(job:EDI_Key)
!                    job:Manufacturer     = tmp:ManufacturerName
!                    job:EDI              = 'NO'
!                    Set(job:EDI_Key,job:EDI_Key)
!                    Loop
!                        If Access:JOBS.NEXT()
!                           Break
!                        End !If
!                        If job:Manufacturer     <> tmp:ManufacturerName      |
!                        Or job:EDI              <> 'NO'      |
!                            Then Break.  ! End If
!                        Do GetNextRecord2
!                        cancelcheck# += 1
!                        If cancelcheck# > (RecordsToProcess/100)
!                            Do cancelcheck
!                            If tmp:cancel = 1
!                                Break
!                            End!If tmp:cancel = 1
!                            cancelcheck# = 0
!                        End!If cancelcheck# > 50
!
!                        If job:Date_Completed > tmp:ProcessBeforeDate
!                            Cycle
!                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate
!                        If job:Warranty_Job <> 'YES'
!                            Cycle
!                        End !If job:Warranty_Job <> ''
!
!                        Do Export
!
!                        If job:ignore_warranty_charges <> 'YES'
!                            Pricing_Routine('W',labour",parts",pass",a")
!                            If pass" = True
!                                job:labour_cost_warranty = labour"
!                                job:parts_cost_warranty  = parts"
!                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
!                            End!If pass" = False
!                        End!If job:ignore_warranty_charges <> 'YES'
!
!                        pos = Position(job:EDI_Key)
!                        !Found
!                        job:EDI = 'YES'
!                        job:EDI_Batch_Number    = Man:Batch_Number
!                        tmp:CountRecords += 1
!                        AddToAudit
!                        Access:JOBS.Update()
!                        Reset(job:EDI_Key,pos)
!
!                    End !Loop
!                    Access:JOBS.RestoreFile(Save_job_ID)
!                    Close(ProgressWindow)
!                    If tmp:CountRecords
!                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
!                            ebt:Batch_Number    = man:Batch_Number
!                            ebt:Manufacturer    = tmp:ManufacturerName
!                            If Access:EDIBATCH.TryInsert() = Level:Benign
!                                !Insert Successful
!                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
!                                !Insert Failed
!                            End !If Access:EDIBATCH.TryInsert() = Level:Benign
!
!                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
!                            man:Manufacturer = tmp:ManufacturerName
!                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!                                !Found
!                                man:Batch_Number += 1
!                                Access:MANUFACT.Update()
!                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!                                !Error
!                                !Assert(0,'<13,10>Fetch Error<13,10>')
!                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
!                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
!                        
!                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
!                          '<13,10>'&|
!                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
!                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!                            Of 1 ! &OK Button
!                        End!Case MessageEx
!                    End !If tmp:CountRecords
!
!                Else
!                    tmp:CountRecords = 0
!                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)
!
!                    Save_job_ID = Access:JOBS.SaveFile()
!                    Access:JOBS.ClearKey(job:EDI_Key)
!                    job:Manufacturer     = tmp:ManufacturerName
!                    job:EDI              = 'YES'
!                    job:EDI_Batch_Number = f_Batch_Number
!                    Set(job:EDI_Key,job:EDI_Key)
!                    Loop
!                        If Access:JOBS.NEXT()
!                           Break
!                        End !If
!                        If job:Manufacturer     <> tmp:ManufacturerName      |
!                        Or job:EDI              <> 'YES'      |
!                        Or job:EDI_Batch_Number <> f_Batch_Number      |
!                            Then Break.  ! End If
!                        Do GetNextRecord2
!                        cancelcheck# += 1
!                        If cancelcheck# > (RecordsToProcess/100)
!                            Do cancelcheck
!                            If tmp:cancel = 1
!                                Break
!                            End!If tmp:cancel = 1
!                            cancelcheck# = 0
!                        End!If cancelcheck# > 50
!
!                        Do Export
!                    End !Loop
!                    Access:JOBS.RestoreFile(Save_job_ID)
!                    Close(ProgressWindow)
!
!                    Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
!                      '<13,10>'&|
!                      '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
!                                   'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
!                        Of 1 ! &OK Button
!                    End!Case MessageEx
!
!                End!If f_batch_number = 0
!                CLOSE(out_file)
!            End ! If Error# = 0
!        End !If ManError# = 0
!    End
!Export ROUTINE
!    Clear(Ouf:Record)
!    Clear(Mt_Total)
!    l1:type    = 'A1'
!    l1:date1    = Format(Year(Today()),@n04)
!    l1:date3    = Format(month(Today()),@n02)
!    l1:date4    = Format(day(Today()),@n02)
!    l1:jobno    = Format(job:ref_number,@n06)
!    l1:AccountNo    = Format(man:EDI_Account_Number,@s4)
!    l1:filler   = Format('',@s48)
!    l1:modelno  = Format(job:model_number,@s14)
!    IMEIError# = 0
!    If job:Third_Party_Site <> ''
!        Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
!        jot:RefNumber = job:Ref_Number
!        Set(jot:RefNumberKey,jot:RefNumberKey)
!        If Access:JOBTHIRD.NEXT()
!            IMEIError# = 1
!        Else !If Access:JOBTHIRD.NEXT()
!            If jot:RefNumber <> job:Ref_Number            
!                IMEIError# = 1
!            Else !If jot:RefNumber <> job:Ref_Number            
!                IMEIError# = 0                
!            End !If jot:RefNumber <> job:Ref_Number            
!        End !If Access:JOBTHIRD.NEXT()
!    Else !job:Third_Party_Site <> ''
!        IMEIError# = 1    
!    End !job:Third_Party_Site <> ''
!
!    If IMEIError# = 1
!        l1:serialno = Format(job:esn,@s14)
!    Else !IMEIError# = 1
!        l1:serialno = Format(jot:OriginalIMEI,@s14)
!    End !IMEIError# = 1
!
!    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
!    jbn:RefNumber   = job:Ref_Number
!    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
!        !Found
!
!    Else! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
!        !Error
!        !Assert(0,'<13,10>Fetch Error<13,10>')
!    End! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
!    
!    
!    l1:servcode = Format(' ',@s1)
!    l1:dop      = Format(job:dop,@d11)
!    l1:faultdate = Format(job:date_booked,@d11)
!    l1:compdate = Format(job:date_completed,@d11)
!    l1:filler2  = Format('',@s124)
!    l1:faulttext = Format(Stripcomma(Stripreturn(jbn:fault_description)),@s70)
!    l1:repairtext = Format(Stripcomma(Stripreturn(jbn:invoice_Text)),@s70)
!    l1:filler3  = Format('',@s10)
!    l1:condcode = Format(job:fault_code1,@s1)
!    l1:symptom1 = Format(job:fault_code2,@s1)
!    l1:symptom2 = Format(job:fault_code3,@s1)
!    l1:extencode = Format(job:fault_code4,@s1)
!    l1:filler4  = Format('',@s26)
!    l1:cust_name    =  Format(Clip(job:title) & ' ' & Clip(job:initial) & ' ' & Clip(job:surname),@s30) !Cust_Name
!    l1:cust_add1    =  Format(job:address_line1,@s30)                           !Cust_Add1
!    l1:cust_add2    =  Format(job:address_line2,@s30)                           !Cust_Add2
!    l1:cust_town    =  Format(job:address_line3,@s30)                           !Cust_Add3
!    l1:cust_cnty    =  Format('',@s20)                         !Cust_Cnty
!    l1:cust_pcod    =  Format(job:postcode,@s10)                                !Cust_PCod
!    l1:cust_tel    =  Format(job:telephone_number,@s15)                        !Cust_Tel
!    l1:DealerName   = Format('',@s30)
!    l1:Filler5      = Format('',@s60)
!    l1:DealerCity   = Format('',@s20)
!    l1:Filler6      = Format('',@s54)
!    l1:Fillerx      = 'X'
!    !Add(out_file)
!
!    Clear(ouf:Record)
!    save_wpr_id = access:warparts.savefile()
!    access:warparts.clearkey(wpr:part_number_key)
!    wpr:ref_number  = job:ref_number
!    set(wpr:part_number_key,wpr:part_number_key)
!    loop
!        if access:warparts.next()
!           break
!        end !if
!        if wpr:ref_number  <> job:ref_number      |
!            then break.  ! end if
!        If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!            Cycle
!        End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!
!        l2:type1    = 'A2'
!        l2:jobno1    = Format(job:ref_number,@n06)
!        l2:part_no    =  Format(wpr:part_number,@s14)
!        l2:qty    =  Format(wpr:quantity,@n06)
!        l2:filler1  = Format('',@s14)
!        l2:sect_code    =  Format(wpr:Fault_Code2,@s3)
!        l2:defect    =  Format(wpr:Fault_Code3,@s1)
!        l2:repair    =  Format(wpr:Fault_Code4,@s1)
!        l2:PanaInvoice  = Format('',@s9)
!        l2:Filler2      = Format('',@s32)
!        l2:Fillery      = 'Y'
!        !Add(out_file)
!    end !loop
!    access:warparts.restorefile(save_wpr_id)
!    count += 1
!    Clear(Ouf:Record)
!
XL_Setup                                                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel = Create(0, Create:OLE)
        Excel{PROP:Create} = 'Excel.Application'

        Excel{'Application.DisplayAlerts'}  = False         ! no alerts to the user (do you want to save the file?) ! MOVED 10 BDec 2001 John
        Excel{'Application.ScreenUpdating'} = excel:Visible ! False
        Excel{'Application.Visible'}        = excel:Visible ! False

        Excel{'Application.Calculation'}    = xlCalculationManual
        
        DO XL_GetOperatingSystem
        !-----------------------------------------------
    EXIT
XL_Finalize                                                     ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Calculation'}    = xlCalculationAutomatic

        Excel{PROP:DEACTIVATE}
        !-----------------------------------------------
    EXIT
XL_AddSheet                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveWorkBook.Sheets("Sheet3").Select'}

        Excel{'ActiveWorkBook.Sheets.Add'}
        !-----------------------------------------------
    EXIT
XL_AddWorkbook               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Application.Workbooks.Add()'}

        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Title")'}            = CLIP(LOC:ProgramName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Author")'}           = CLIP(LOC:UserName)
        Excel{'ActiveWorkbook.BuiltinDocumentProperties("Application Name")'} = LOC:ApplicationName
        !-----------------------------------------------
        ! 4 Dec 2001 John
        ! Delete empty sheets

        Excel{'Sheets("Sheet2").Select'}
        Excel{'ActiveWindow.SelectedSheets.Delete'}

        Excel{'Sheets("Sheet1").Select'}
        !-----------------------------------------------
    EXIT
XL_SaveAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Save()'}
            Excel{'ActiveWorkBook.Close()'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_DropAllWorksheets                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOOP WHILE Excel{'WorkBooks.Count'} > 0
            Excel{'ActiveWorkBook.Close(' & FALSE & ')'}
        END !LOOP
        !-----------------------------------------------
    EXIT
XL_AddComment                ROUTINE
    DATA
xlComment CSTRING(20)
    CODE
        !-----------------------------------------------
        Excel{'Selection.AddComment("' & CLIP(excel:CommentText) & '")'}

!        xlComment{'Shape.IncrementLeft'} = 127.50
!        xlComment{'Shape.IncrementTop'}  =   8.25
!        xlComment{'Shape.ScaleWidth'}    =   1.76
!        xlComment{'Shape.ScaleHeight'}   =   0.69
        !-----------------------------------------------
    EXIT
XL_RowDown                   ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(1, 0).Select'}
        !-----------------------------------------------
    EXIT
XL_ColLeft                   ROUTINE
    DATA
CurrentCol LONG
    CODE
        !-----------------------------------------------
        CurrentCol = Excel{'ActiveCell.Col'}
        IF CurrentCol = 1
            EXIT
        END !IF

        Excel{'ActiveCell.Offset(0, -1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColRight                  ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, 1).Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirstSelect                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, Cells(' & Excel{'ActiveCell.Row'} & ', 1)).Select'}
        !Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_ColFirst                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'ActiveCell.Offset(0, -' & Excel{'ActiveCell.Column'} - 1 & ').Select'}
        !-----------------------------------------------
    EXIT
XL_SetLastCell                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_SetGrid                              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlDiagonalDown & ').LineStyle'} = xlNone
        Excel{'Selection.Borders(' & xlDiagonalUp   & ').LineStyle'} = xlNone

        DO XL_SetBorder

        Excel{'Selection.Borders(' & xlInsideVertical & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideVertical & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideVertical & ').ColorIndex'} = xlAutomatic

        Excel{'Selection.Borders(' & xlInsideHorizontal & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlInsideHorizontal & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorder                                ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetBorderLeft
        DO XL_SetBorderRight
        DO XL_SetBorderBottom
        DO XL_SetBorderTop
        !-----------------------------------------------
    EXIT
XL_SetBorderLeft                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeLeft & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeLeft & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeLeft & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderRight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeRight & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeRight & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeRight & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderBottom                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeBottom & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeBottom & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeBottom & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetBorderTop                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Borders(' & xlEdgeTop & ').LineStyle'}  = xlContinuous
        Excel{'Selection.Borders(' & xlEdgeTop & ').Weight'}     = xlThin
        Excel{'Selection.Borders(' & xlEdgeTop & ').ColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_HighLight                           ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 6 ! YELLOW
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
XL_SetTitle                            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Interior.ColorIndex'}        = 15 ! GREY
        Excel{'Selection.Interior.Pattern'}           = xlSolid
        Excel{'Selection.Interior.PatternColorIndex'} = xlAutomatic
        !-----------------------------------------------
    EXIT
XL_SetWrapText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.WrapText'}    = True
        !-----------------------------------------------
    EXIT
XL_SetGreyText                         ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.ColorIndex'}    = 16 ! grey
        !-----------------------------------------------
    EXIT
XL_SetBold                             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.Font.Bold'} = True
        !-----------------------------------------------
    EXIT
XL_FormatDate                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = excel:DateFormat
        !-----------------------------------------------
    EXIT
XL_FormatNumber                          ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.NumberFormat'} = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_FormatTextBox                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.ShapeRange.IncrementLeft'} = 127.5
        Excel{'Selection.ShapeRange.IncrementTop'}  =   8.25
!        Excel{'Selection.ShapeRange.ScaleWidth'}    =  '1.76, ' & msoFalse & ', ' & msoScaleFromBottomRight
!        Excel{'Selection.ShapeRange.ScaleHeight'}   =  '0.69, ' & msoFalse & ', ' & msoScaleFromTopLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentLeft              ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlLeft
        !-----------------------------------------------
    EXIT
XL_HorizontalAlignmentRight             ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlRight
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentTop                 ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlTop
        !-----------------------------------------------
    EXIT
XL_VerticalAlignmentCentre            ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.VerticalAlignment'} = xlCenter
        !-----------------------------------------------
    EXIT
XL_SetColumnHeader                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        DO XL_SetTitle
        DO XL_SetGrid
        DO XL_SetWrapText
        DO XL_SetBold
        !-----------------------------------------------
    EXIT
XL_SetColumn_Date                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = 'dd Mmm yyyy'

!        !Excel{'ActiveCell.FormulaR1C1'}  = LEFT(FORMAT(ret:Invoice_Date, @D8-))
        !-----------------------------------------------
    EXIT
XL_SetColumn_Number                     ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '#,##0.00'
        !-----------------------------------------------
    EXIT
XL_SetColumn_Percent                    ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'}   = '0.00%'
!        Excel{'ActiveCell.NumberFormat'} = CHR(64)       ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_SetColumn_Text                       ROUTINE
    DATA
col STRING(10)
    CODE
        !-----------------------------------------------
        Col = 'C' & Excel{'ActiveCell.Column'} & ':' & 'C' & Excel{'ActiveCell.Column'}
        Col = Excel{'Application.ConvertFormula( "' & CLIP(Col) & '", ' & xlR1C1 & ', ' & xlA1 & ')'}

        Excel{'ActiveWorkbook.Columns("' & CLIP(Col) & '").NumberFormat'} = CHR(64) ! Text (AT) Sign
        !-----------------------------------------------
    EXIT
XL_DataSelectRange                      ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Range(Selection, ActiveCell.SpecialCells(' & xlLastCell & ')).Select'}
        !-----------------------------------------------
    EXIT
XL_DataHideDuplicates                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AdvancedFilter Action:=' & xlFilterInPlace & ', Unique:=' & True }
        !-----------------------------------------------
    EXIT
XL_DataSortRange                            ROUTINE
    DATA
    CODE                     
        !-----------------------------------------------
        DO XL_DataSelectRange
        Excel{'ActiveWindow.ScrollColumn'} = 1
        Excel{'Selection.Sort Key1:=Range("' & LOC:Text & '"), Order1:=' & xlAscending & ', Header:=' & xlGuess & |
            ', OrderCustom:=1, MatchCase:=' & False & ', Orientation:=' & xlTopToBottom}
        !-----------------------------------------------
    EXIT
XL_DataAutoFilter                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.AutoFilter'}
        !-----------------------------------------------
    EXIT
XL_SetWorksheetLandscape                       ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'}     = xlLandscape
!        Excel{'ActiveSheet.PageSetup.FitToPagesWide'} = 1
!        Excel{'ActiveSheet.PageSetup.FitToPagesTall'} = 9999
!        Excel{'ActiveSheet.PageSetup.Order'}          = xlOverThenDown
        !-----------------------------------------------
    EXIT
XL_SetWorksheetPortrait                        ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.Orientation'} = xlPortrait
        !-----------------------------------------------
    EXIT
XL_PrintTitleRows                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        IF excel:OperatingSystem < 5
            EXIT
        END !IF
        !-----------------------------------------------
        Excel{'ActiveSheet.PageSetup.PrintTitleRows'} = CLIP(LOC:Text)
        !-----------------------------------------------
    EXIT
XL_GetCurrentCell                               ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        LOC:Text = Excel{'Application.ConvertFormula( "RC", ' & xlR1C1 & ', ' & xlA1 & ')'}
        !-----------------------------------------------
    EXIT

XL_MergeCells                                    ROUTINE
    DATA
    CODE
        !-----------------------------------------------
        Excel{'Selection.HorizontalAlignment'} = xlCenter
        Excel{'Selection.VerticalAlignment'}   = xlBottom
        Excel{'Selection.MergeCells'}          = True
        !-----------------------------------------------
    EXIT

XL_GetOperatingSystem                                                          ROUTINE
    DATA
OperatingSystem     STRING(50)
tmpLen              LONG
LEN_OperatingSystem LONG
    CODE
        !-----------------------------------------------
        excel:OperatingSystem = 0.0    ! Default/error value
        OperatingSystem       = Excel{'Application.OperatingSystem'}
        LEN_OperatingSystem   = LEN(CLIP(OperatingSystem))

        LOOP x# = LEN_OperatingSystem TO 1 BY -1

            CASE SUB(OperatingSystem, x#, 1)
            OF '0' OROF'1' OROF '2' OROF '3' OROF '4' OROF '5' OROF '6' OROF '7' OROF '8' OROF '9' OROF '.'
                ! NULL
            ELSE
                tmpLen                = LEN_OperatingSystem-x#+1
                excel:OperatingSystem = CLIP(SUB(OperatingSystem, x#+1, tmpLen))

                EXIT
            END !CASE
        END !LOOP
        !-----------------------------------------------
    EXIT
!=== Procedures ====================================================
DateToString        PROCEDURE( IN:Date )! STRING
result  STRING(6)
    CODE
        !-----------------------------------------------------------------
        RETURN LEFT(FORMAT(IN:Date, @D8))
        !-----------------------------------------------------------------
DateToString2        PROCEDURE( IN:Date )    ! Start Change 3808 BE(2/2/04)
!Start Change 4750 BE(04/10/2004)
!result   STRING(7)
!datestr     STRING(6)
!Start Change 4750 BE(04/10/2004)
    CODE
        !Start Change 4750 BE(04/10/2004)
        !datestr = FORMAT(IN:Date, @D011)
        !result = '''' & datestr[5 : 6] & datestr[3 : 4] & datestr[1 : 2]
        !RETURN result
        RETURN FORMAT(IN:Date, @D06B)
        !Start Change 4750 BE(04/10/2004)
! Start Change 3808 BE(2/2/04)
CalcLabour          PROCEDURE()! REAL
LabourAmount REAL
    CODE
        !-----------------------------------------------------------------
        LabourAmount    = 0

        IF job:chargeable_job <> 'YES' 
            RETURN LabourAmount
        END ! IF

        IF job:ignore_chargeable_charges = 'YES'
            RETURN LabourAmount
        END ! IF

        Access:CHARTYPE.ClearKey(cha:charge_type_key)
        IF job:warranty_job = 'YES'
            cha:charge_type = job:warranty_charge_type
        ELSE
            cha:charge_type = job:charge_type 
        END !IF
        
        If Access:CHARTYPE.Fetch(cha:charge_type_key)
            RETURN LabourAmount
        End !If access:chartype.clearkey(cha:charge_type_key)

        If cha:no_charge = 'YES'
            RETURN LabourAmount
        End !If cha:no_charge = 'YES'

        IF NOT LoadSUBTRACC( job:Account_Number )
            GOTO Final
        END !IF

        IF NOT LoadTRADEACC( sub:Main_Account_Number )
            GOTO Final
        END !IF

        If     tra:invoice_sub_accounts = 'YES' 
            Access:SUBCHRGE.ClearKey(suc:model_repair_type_key)
            suc:account_number = job:account_number
            suc:model_number   = job:model_number
            suc:charge_type    = job:charge_type
            suc:unit_type      = job:unit_type
            suc:repair_type    = job:repair_type
            if Access:SUBCHRGE.Fetch(suc:model_repair_type_key)
                access:trachrge.clearkey(trc:account_charge_key)
                trc:account_number = sub:main_account_number
                trc:model_number   = job:model_number
                trc:charge_type    = job:charge_type
                trc:unit_type      = job:unit_type
                trc:repair_type    = job:repair_type
                if Access:TRACHRGE.Fetch(trc:account_charge_key) = Level:Benign
                    LabourAmount = trc:cost

                    RETURN LabourAmount
                End!if access:trachrge.fetch(trc:account_charge_key)
            Else
                LabourAmount = suc:cost

                RETURN LabourAmount
            End!if access:subchrge.fetch(suc:model_repair_type_key)

        Else!If tra:invoice_sub_accounts = 'YES'
            Access:TRACHRGE.clearkey(trc:account_charge_key)
            trc:account_number = sub:main_account_number
            trc:model_number   = job:model_number
            trc:charge_type    = job:charge_type
            trc:unit_type      = job:unit_type
            trc:repair_type    = job:repair_type
            if Access:TRACHRGE.fetch(trc:account_charge_key) = Level:Benign
                LabourAmount = trc:cost

                RETURN LabourAmount
            End!if access:trachrge.fetch(trc:account_charge_key)

        End!If tra:invoice_sub_accounts = 'YES'

Final   Access:STDCHRGE.clearkey(sta:model_number_charge_key)
        sta:model_number = job:model_number
        IF job:warranty_job = 'YES'
            sta:charge_type  = job:warranty_charge_type
        ELSE
            sta:charge_type  = job:charge_type
        END !IF
        
        sta:unit_type    = job:unit_type
        sta:repair_type  = job:repair_type
        IF Access:STDCHRGE.fetch(sta:model_number_charge_key) = Level:Benign
            LabourAmount = sta:cost
        END !IF access:stdchrge.fetch(sta:model_number_charge_key)
        !-----------------------------------------------------------------
        RETURN LabourAmount
        !-----------------------------------------------------------------
LoadJOBSE      PROCEDURE( IN:JobNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = IN:JobNumber

        IF Access:JOBSE.Fetch(jobe:RefNumberKey) <> Level:Benign
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadPARTS            PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF IN:First
            IN:First = False

            Access:PARTS.ClearKey(par:Part_Number_Key)
                par:Ref_Number = IN:JobNumber
            SET(par:Part_Number_Key, par:Part_Number_Key)
        END !IF

        IF Access:PARTS.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT par:Ref_Number = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadStock PROCEDURE( IN:StockNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        !
        ! Manufacturer_Accessory_Key    KEY(sto:Location,sto:Accessory,sto:Manufacturer,sto:Part_Number                   ),DUP,NOCASE
        ! Shelf_Location_Accessory_Key  KEY(sto:Location,sto:Accessory,                 sto:Shelf_Location                ),DUP,NOCASE
        ! Location_Part_Description_Key KEY(sto:Location,                               sto:Part_Number,   sto:Description),DUP,NOCASE
        ! Description_Accessory_Key     KEY(sto:Location,sto:Accessory,                                    sto:Description),DUP,NOCASE
        ! Part_Number_Accessory_Key     KEY(sto:Location,sto:Accessory,                 sto:Part_Number                   ),DUP,NOCASE
        ! Location_Manufacturer_Key     KEY(sto:Location,              sto:Manufacturer,sto:Part_Number                   ),DUP,NOCASE
        ! Supplier_Accessory_Key        KEY(sto:Location,sto:Accessory,                 sto:Shelf_Location                ),DUP,NOCASE
        ! Description_Key               KEY(sto:Location,                                                  sto:Description),DUP,NOCASE
        ! Location_Key                  KEY(sto:Location,                               sto:Part_Number                   ),DUP,NOCASE
        ! Ref_Part_Description_Key      KEY(sto:Location,sto:Ref_Number,                sto:Part_Number,   sto:Description),DUP,NOCASE
        ! Shelf_Location_Key            KEY(sto:Location,                               sto:Shelf_Location                ),DUP,NOCASE
        ! SecondLocKey                  KEY(sto:Location,                               sto:Shelf_Location,sto:Second_Location,sto:Part_Number),DUP,NOCASE
        ! Supplier_Key                  KEY(sto:Location,sto:Supplier),DUP,NOCASE

        ! Ref_Part_Description2_Key     KEY(sto:Ref_Number,sto:Part_Number,sto:Description),DUP,NOCASE
        ! ExchangeAccDescKey            KEY(sto:ExchangeUnit,sto:Location,sto:Description),DUP,NOCASE
        ! ExchangeAccPartKey            KEY(sto:ExchangeUnit,sto:Location,sto:Part_Number),DUP,NOCASE

        ! Manufacturer_Key              KEY(sto:Manufacturer,sto:Part_Number),DUP,NOCASE
        ! Minimum_Description_Key       KEY(sto:Minimum_Stock,sto:Location,sto:Description),DUP,NOCASE
        ! Minimum_Part_Number_Key       KEY(sto:Minimum_Stock,sto:Location,sto:Part_Number),DUP,NOCASE
        ! RequestedKey                  KEY(sto:QuantityRequested,sto:Part_Number),DUP,NOCASE
        ! Ref_Number_Key                KEY(sto:Ref_Number),NOCASE,PRIMARY
        ! Sundry_Item_Key               KEY(sto:Sundry_Item),DUP,NOCASE
        !
        !-----------------------------------------------
        Access:STOCK.ClearKey( sto:Ref_Number_Key )
            sto:Ref_Number = IN:StockNumber
        SET(sto:Ref_Number_Key, sto:Ref_Number_Key)

        IF Access:STOCK.Next() <> Level:Benign
            RETURN False
        END !IF

        IF sto:Ref_Number <> IN:StockNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadSUBTRACC   PROCEDURE( IN:AccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        Access:SUBTRACC.ClearKey(sub:Account_Number_Key)
        sub:Account_Number = IN:AccountNumber

        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key) <> Level:Benign
            RETURN False
        END !IF

        IF sub:Account_Number <> IN:AccountNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadTRADEACC        PROCEDURE( IN:MainAccountNumber )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF tra:Account_Number = IN:MainAccountNumber
            ! Only load if not already at correct record
            RETURN True
        END !IF

        Access:TRADEACC.ClearKey(tra:Account_Number_Key)
        tra:Account_Number = IN:MainAccountNumber
        SET(tra:Account_Number_Key, tra:Account_Number_Key)

        IF Access:TRADEACC.NEXT() <> Level:Benign
            tra:Account_Number = ''

            RETURN False
        END !IF

        IF NOT tra:Account_Number = IN:MainAccountNumber
            tra:Account_Number = ''

            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
LoadWARPARTS            PROCEDURE( IN:JobNumber, IN:First )! LONG ! BOOL
    CODE
        !-----------------------------------------------
        IF IN:First
            IN:First = False

            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                wpr:Ref_Number = IN:JobNumber
            SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
        END !IF

        IF Access:WARPARTS.NEXT() <> Level:Benign
            RETURN False
        END !IF

        IF NOT wpr:Ref_Number = IN:JobNumber
            RETURN False
        END !IF

        RETURN True
        !-----------------------------------------------
WriteDebug      PROCEDURE( IN:Message )
    CODE
        !-----------------------------------------------
        IF NOT debug:Active
            RETURN
        END !IF

        debug:Count += 1

        PUTINI('SBI01App', debug:Count, IN:Message, 'C:\Debug.ini')
        !-----------------------------------------------
!===================================================================


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Sharp_Excel_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('EdiVersion',EdiVersion,'Sharp_Excel_EDI',1)
    SolaceViewVars('EdiAccountNo',EdiAccountNo,'Sharp_Excel_EDI',1)
    SolaceViewVars('SaveFileID_Group:save_wpr_id',SaveFileID_Group:save_wpr_id,'Sharp_Excel_EDI',1)
    SolaceViewVars('SaveFileID_Group:save_job_ali_id',SaveFileID_Group:save_job_ali_id,'Sharp_Excel_EDI',1)
    SolaceViewVars('SaveFileID_Group:save_job_id',SaveFileID_Group:save_job_id,'Sharp_Excel_EDI',1)
    SolaceViewVars('Tmp_Group:ManufacturerName',Tmp_Group:ManufacturerName,'Sharp_Excel_EDI',1)
    SolaceViewVars('Tmp_Group:ProcessBeforeDate',Tmp_Group:ProcessBeforeDate,'Sharp_Excel_EDI',1)
    SolaceViewVars('Tmp_Group:CountRecords',Tmp_Group:CountRecords,'Sharp_Excel_EDI',1)
    SolaceViewVars('Misc_Group:pos',Misc_Group:pos,'Sharp_Excel_EDI',1)
    SolaceViewVars('Excel_Group:Excel',Excel_Group:Excel,'Sharp_Excel_EDI',1)
    SolaceViewVars('Excel_Group:excel:DateFormat',Excel_Group:excel:DateFormat,'Sharp_Excel_EDI',1)
    SolaceViewVars('Excel_Group:excel:ColumnName',Excel_Group:excel:ColumnName,'Sharp_Excel_EDI',1)
    SolaceViewVars('Excel_Group:excel:ColumnWidth',Excel_Group:excel:ColumnWidth,'Sharp_Excel_EDI',1)
    SolaceViewVars('Excel_Group:excel:CommentText',Excel_Group:excel:CommentText,'Sharp_Excel_EDI',1)
    SolaceViewVars('Excel_Group:excel:OperatingSystem',Excel_Group:excel:OperatingSystem,'Sharp_Excel_EDI',1)
    SolaceViewVars('Excel_Group:excel:Visible',Excel_Group:excel:Visible,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:ApplicationName',Local_Group:ApplicationName,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:DesktopPath',Local_Group:DesktopPath,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:Filename',Local_Group:Filename,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:Path',Local_Group:Path,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:ProgramName',Local_Group:ProgramName,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:SpreadsheetCreated',Local_Group:SpreadsheetCreated,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:Text',Local_Group:Text,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:UserName',Local_Group:UserName,'Sharp_Excel_EDI',1)
    SolaceViewVars('Local_Group:Version',Local_Group:Version,'Sharp_Excel_EDI',1)
    SolaceViewVars('Sheet_Group:TempLastCol',Sheet_Group:TempLastCol,'Sharp_Excel_EDI',1)
    SolaceViewVars('Sheet_Group:HeadLastCol',Sheet_Group:HeadLastCol,'Sharp_Excel_EDI',1)
    SolaceViewVars('Sheet_Group:DataLastCol',Sheet_Group:DataLastCol,'Sharp_Excel_EDI',1)
    SolaceViewVars('Sheet_Group:HeadSummaryRow',Sheet_Group:HeadSummaryRow,'Sharp_Excel_EDI',1)
    SolaceViewVars('Sheet_Group:DataSectionRow',Sheet_Group:DataSectionRow,'Sharp_Excel_EDI',1)
    SolaceViewVars('Sheet_Group:DataHeaderRow',Sheet_Group:DataHeaderRow,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:ChaAccessoriesCost',WarParts_Group:ChaAccessoriesCost,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:ChaProfit',WarParts_Group:ChaProfit,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:ChaRevenue',WarParts_Group:ChaRevenue,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:ChaSparesCost',WarParts_Group:ChaSparesCost,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:WarAccessoriesCost',WarParts_Group:WarAccessoriesCost,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:WarProfit',WarParts_Group:WarProfit,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:WarRevenue',WarParts_Group:WarRevenue,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:WarSparesCost',WarParts_Group:WarSparesCost,'Sharp_Excel_EDI',1)
    
      Loop SolaceDim1# = 1 to 8
        SolaceFieldName" = 'WarParts_Group:Parts' & '[' & SolaceDim1# & ']'
        SolaceViewVars(SolaceFieldName",WarParts_Group:Parts[SolaceDim1#],'Sharp_Excel_EDI',1)
      End
    
    
    SolaceViewVars('WarParts_Group:DefectCode',WarParts_Group:DefectCode,'Sharp_Excel_EDI',1)
    SolaceViewVars('WarParts_Group:RepairCode',WarParts_Group:RepairCode,'Sharp_Excel_EDI',1)
    SolaceViewVars('Debug_Group:Active',Debug_Group:Active,'Sharp_Excel_EDI',1)
    SolaceViewVars('Debug_Group:Count',Debug_Group:Count,'Sharp_Excel_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
