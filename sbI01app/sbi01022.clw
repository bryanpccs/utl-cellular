

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01022.INC'),ONCE        !Local module procedure declarations
                     END


CountEDIRecords      PROCEDURE  (func:Manufacturer,func:EDI,func:BatchNumber,func:ProcessBeforeDate) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_job_ali_id      USHORT,AUTO
tmp:Count            LONG
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CountEDIRecords')      !Add Procedure to Log
  end


    SetCursor(cursor:wait)
    tmp:Count = 0
    Save_job_ali_ID = Access:JOBS_ALIAS.SaveFile()
    Access:JOBS_ALIAS.ClearKey(job_ali:EDI_Key)
    job_ali:Manufacturer     = func:Manufacturer
    job_ali:EDI              = func:EDI
    If func:BatchNumber <> 0
        job_ali:EDI_Batch_Number = func:BatchNumber    
    End !If func:BatchNumber <> 0
    Set(job_ali:EDI_Key,job_ali:EDI_Key)
    Loop
        If Access:JOBS_ALIAS.NEXT()
           Break
        End !If
        If job_ali:Manufacturer     <> func:Manufacturer      |
        Or job_ali:EDI              <> func:EDI      |
            Then Break.
        If func:BatchNumber <> 0
            If job_ali:EDI_Batch_Number <> func:BatchNumber
                Break
            End !If job_ali:EDI_Batch_Number <> func:BatchNumber
        Else !If func:BatchNumber <> 0
            If job_ali:date_completed > func:ProcessBeforeDate
                Cycle
            End!if job:date_completed > tmp:ProcessBeforeDate
        End !If func:BatchNumber <> 0

        tmp:Count += 1
    End !Loop
    Access:JOBS_ALIAS.RestoreFile(Save_job_ali_ID)
    Setcursor()

    Return tmp:Count


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CountEDIRecords',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'CountEDIRecords',1)
    SolaceViewVars('tmp:Count',tmp:Count,'CountEDIRecords',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
