

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01005.INC'),ONCE        !Local module procedure declarations
                     END


Nokia_EDI            PROCEDURE  (f_batch_number)      ! Declare Procedure
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
pos                  STRING(255)
ASC                  STRING(20)
CountryCode          STRING(20)
RegionCode           STRING(20)
SenderCountry        STRING(20)
tmp:PassedBatchNumber LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
x            SHORT

Out_File FILE,DRIVER('ASCII'),PRE(OUF),NAME(Filename),CREATE,BINDABLE,THREAD ! Record length 1016
RECORD      RECORD
Out_Group     GROUP
Line1           STRING(255)
Line2           STRING(199)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
Out_Detail GROUP,OVER(Ouf:Out_Group),PRE(L1)
blank         STRING(1)
Job_Part      STRING(2)
serv_code     STRING(5)
batch         STRING(11)
job_no        STRING(11)
purch_ord     STRING(11)
price_cat     STRING(4)
serial_no     STRING(16)
old_serno     STRING(16)
repair_date   STRING(11)
date_in       STRING(11)
auth_code     STRING(11)
unit_type     STRING(9)
product_code  STRING(16)
prod_s_no     STRING(16)
hardware_ID   STRING(16)
Software_V    STRING(16)
Manufct_date  STRING(11)
fault_type    STRING(3)
module        STRING(9)
circuit       STRING(7)
part_no       STRING(16)
qty           STRING(8)
fault         STRING(101)
inv_txt       STRING(101)
name          STRING(4)
date_claim    STRING(11)
           .

mt_total     REAL
vat          REAL
total        REAL
sub_total    REAL
man_fct      STRING(30)
field_j       BYTE
days          REAL
time          REAL
day           LONG
tim1          LONG
day_count     LONG
count         REAL


save_wpr_id   ushort,auto
save_job_id   ushort,auto
save_sto_id   ushort,auto


IOut_File FILE,DRIVER('ASCII'),PRE(IUF),NAME(Filename),CREATE,BINDABLE,THREAD ! Record length 1016
RECORD      RECORD
IOut_Group     GROUP
ILine1           STRING(2000)
          . . .

!out_file    DOS,PRE(ouf),NAME(filename)
IOut_Detail GROUP,OVER(Iuf:IOut_Group),PRE(I1)
blank1    STRING(2)
blank2    STRING(2)
blank3    STRING(2)
blank4    STRING(2)
blank5    STRING(2)
blank6    STRING(2)
Job_ID    STRING(11)
blank8    STRING(2)
blank9    STRING(2)
blank10   STRING(2)
blank11   STRING(2)
blank12   STRING(2)
blank13   STRING(2)
blank14   STRING(2)
blank15   STRING(2)
blank16   STRING(2)
blank17   STRING(2)
blank18   STRING(2)
blank19   STRING(2)
blank20   STRING(2)
Comments  STRING(21)
blank22   STRING(2)
blank23   STRING(2)
OrdNum    STRING(8)
blank25   STRING(2)
SerNum    STRING(17)
blank27   STRING(2)
blank28   STRING(2)
blank29   STRING(2)
blank30   STRING(2)
blank31   STRING(2)
model     STRING(16)
dop       STRING(7)
WarrAuth  STRING(11)
CustName  STRING(21)
CustAdd1  STRING(21)
CustAdd2  STRING(21)
CustAdd3  STRING(21)
CustAdd4  STRING(21)
RepDate   STRING(7)
blank41   STRING(2)
Comments2 STRING(21)
PurOrdNo  STRING(11)
RepPrCat  STRING(3)
fault_d   STRING(81)
alt_I     STRING(241)
blank47   STRING(2)
blank48   STRING(2)
blank49   STRING(2)
blank50   STRING(2)
blank51   STRING(2)
labrate   STRING(11)
partcost  STRING(11)
blank54   STRING(2)
blank55   STRING(2)
blank56   STRING(2)
blank57   STRING(2)
blank58   STRING(2)
blank59   STRING(2)
blank60   STRING(2)
blank61   STRING(2)
blank62   STRING(2)
blank63   STRING(2)
ClubID    STRING(11)
PartNumb  STRING(8)
           .
NokiaExport    File,Driver('ASCII'),Pre(nokex),Name(FileName),Create,Bindable,Thread
Record                  Record
Line1               String(2000)
                        End
                    End

! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Nokia_EDI')      !Add Procedure to Log
  end


   Relate:EXCHANGE.Open
   Relate:JOBS.Open
   Relate:MANUFACT.Open
   Relate:WARPARTS.Open
   Relate:EDIBATCH.Open
   Relate:MODELNUM.Open
   Relate:STOCK.Open
   Relate:AUDIT.Open
   Relate:STATUS.Open
   Relate:JOBSTAGE.Open
   Relate:DEFAULTS.Open
   Relate:JOBNOTES.Open
   Relate:MANFAULT.Open
   Relate:MANFAULO.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
!**Variable
    SET(Defaults,0)
    Access:Defaults.Next()
    ! Start Change BE18 BE(04/11/03)
    !IF INSTRING('INTEC',CLIP(UPPER(def:User_Name)),1,1)
    ! Start Change 4269 BE(28/06/2004)
    !IF (INSTRING('INTEC',CLIP(UPPER(def:User_Name)),1,1)  OR |
    !    INSTRING('SIGMA',CLIP(UPPER(def:User_Name)),1,1)) THEN
    IF (INSTRING('INTEC',CLIP(UPPER(def:User_Name)),1,1)  OR |
        INSTRING('SIGMA',CLIP(UPPER(def:User_Name)),1,1) OR |
        INSTRING('FONE LOGISTICS',CLIP(UPPER(def:User_Name)),1,1)) THEN
    ! End Change 4269 BE(28/06/2004)
    ! End Change BE18 BE(04/11/03)
        !Run XML
        !Call xml and return the batch number - TrkBs: 5006 (DBH: 19-01-2005)
        tmp:PassedBatchNumber = CRC_Nokia_EDI_XML_Export(f_Batch_Number,0)

        !If Intec, call the accessory version of xml - TrkBs: 5006 (DBH: 19-01-2005)
        If tmp:PassedBatchNumber <> 0
            Set(DEFAULTS,0)
            Access:DEFAULTS.Next()
            If Instring('INTEC',Clip(Upper(def:User_Name)),1,1)
                x# = CRC_Nokia_EDI_XML_Export(tmp:PassedBatchNumber,1)
            End ! If Instring('INTEC',Clip(Upper(def:User_Name)),1,1)
        End ! If tmp:PassedBatchNumber <> 0
    ELSE
      !Continue

        ! Start Change 3856 BE(3/2/04)
        ASC = GETINI('SERVICECENTRE','SC01',, CLIP(PATH()) & '\sbcr0060.ini')
        IF ((LEN(ASC) > 1) AND (ASC[1 : 1] = '$')) THEN
            ASC = ASC[2 : LEN(ASC)]
        END

        CountryCode = GETINI('SERVICECENTRE','SC02',, CLIP(PATH()) & '\sbcr0060.ini')
        IF ((LEN(CountryCode) > 1) AND (CountryCode[1 : 1] = '$')) THEN
            CountryCode = CountryCode[2 : LEN(CountryCode)]
        END

        RegionCode = GETINI('SERVICECENTRE','SC03',, CLIP(PATH()) & '\sbcr0060.ini')
        IF ((LEN(RegionCode) > 1) AND (RegionCode[1 : 1] = '$')) THEN
            RegionCode = RegionCode[2 : LEN(RegionCode)]
        END

        SenderCountry = GETINI('WORKORDER','WO13',, CLIP(PATH()) & '\sbcr0060.ini')
        IF ((LEN(SenderCountry) > 1) AND (SenderCountry[1 : 1] = '$')) THEN
            SenderCountry = SenderCountry[2 : LEN(SenderCountry)]
        END
        ! End Change 3856 BE(3/2/04)

        tmp:ManufacturerName = 'NOKIA'
        continue# = 0
        If f_batch_number = 0
            tmp:ProcessBeforeDate   = ProcessBeforeDate()
            If tmp:ProcessBeforeDate <> ''
                continue# = 1
            End!If tmp:ProcessBeforeDate <> ''
        Else!If f_batch_number = 0
            continue# = 1
        End!If f_batch_number = 0
        If continue# = 1
            ManError# = 0
            If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
                ManError# = 1
            End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

            If ManError# = 0

                Error# = 0

    !**Variable
                If man:NokiaType = 'NSH'
                    IF f_batch_number = 0
                      filename = CLIP(CLIP(MAN:EDI_Path)&'\NOK'&CLIP(Format(MAN:Batch_number,@n05))&'.TXT')
                    ELSE
                      filename = CLIP(CLIP(MAN:EDI_Path)&'\NOK'&CLIP(Format(f_Batch_number,@n05))&'.TXT')
                    END
                    OPEN(IOut_File)                           ! Open the output file
                    IF ERRORCODE()                           ! If error
                        CREATE(iOut_File)
                        If Errorcode()
                            Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            Error# = 1
                        End!If Errorcode() 
                        OPEN(iout_file)        ! If still error then stop
                    ELSE
                      Open(iout_file)
                      EMPTY(iOut_file)
                    END

                Else!If Instring('INTEC',def:user_name,1,1)
                    IF f_batch_number = 0
                      filename = CLIP(CLIP(MAN:EDI_Path)&'\NOK'&CLIP(Format(MAN:Batch_number,@n05))&'.CSV')
                    ELSE
                      filename = CLIP(CLIP(MAN:EDI_Path)&'\NOK'&CLIP(Format(f_Batch_number,@n05))&'.CSV')
                    END
                    !Use old/new format?
                    If man:SiemensNewEDI
                        Open(NokiaExport)
                        IF ERRORCODE()                           ! If error
                            Create(NokiaExport)
                            If Errorcode()
                                Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Error# = 1
                            End!If Errorcode() 
                            Open(NokiaExport)
                        ELSE
                          Open(NokiaExport)
                          ! Start Change ???Append File Bug BE(30/04/03)
                          ! NB this fix was not carried forward from V3.1
                          EMPTY(NokiaExport)
                          ! End Change ???Append File Bug BE(30/04/03)
                        END

                    Else !If man:SiemensNewEDI
                        OPEN(Out_File)                           ! Open the output file
                        IF ERRORCODE()                           ! If error
                            CREATE(Out_File)
                            If Errorcode()
                                Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                                Error# = 1
                            End!If Errorcode() 
                            OPEN(out_file)        ! If still error then stop
                        ELSE
                          Open(out_file)
                          ! Start Change ???Append File Bug BE(30/04/03)
                          ! NB this fix was not carried forward from V3.1
                          EMPTY(out_file)
                          ! End Change ???Append File Bug BE(30/04/03)
                        END
                    End !If man:SiemensNewEDI
                End!If Instring('INTEC',def:user_name,1,1)

    !**Variable
              !-----Assume - Print all non-printed claims for Manufacturer X------------!
                If Error# = 0

                    YIELD()
                    RecordsPerCycle = 25
                    RecordsProcessed = 0
                    PercentProgress = 0

                    OPEN(ProgressWindow)
                    Progress:Thermometer = 0
                    ?Progress:PctText{Prop:Text} = '0% Completed'
                    ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                    If f_batch_number = 0
                        count_records# = 0
                        tmp:CountRecords = 0
                        Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)


                        Save_job_ID = Access:JOBS.SaveFile()
                        Access:JOBS.ClearKey(job:EDI_Key)
                        job:Manufacturer     = tmp:ManufacturerName
                        job:EDI              = 'NO'
                        Set(job:EDI_Key,job:EDI_Key)
                        Loop
                            If Access:JOBS.NEXT()
                               Break
                            End !If
                            If job:Manufacturer     <> tmp:ManufacturerName      |
                            Or job:EDI              <> 'NO'      |
                                Then Break.  ! End If
                            Do GetNextRecord2
                            cancelcheck# += 1
                            If cancelcheck# > (RecordsToProcess/100)
                                Do cancelcheck
                                If tmp:cancel = 1
                                    Break
                                End!If tmp:cancel = 1
                                cancelcheck# = 0
                            End!If cancelcheck# > 50
                            If job:Date_Completed > tmp:ProcessBeforeDate
                                Cycle
                            End !If job:Date_Completed > tmp:ProcessBeforeDate
                            If job:Warranty_Job <> 'YES'
                                Cycle
                            End !If job:Warranty_Job <> 'YES'

                            If man:NokiaType = 'NSH'
                                Do Export_NSH
                            Else!If Instring('INTEC',def:user_name,1,1)
                                If man:SiemensNewEDI
                                    Do NewExport
                                Else !If man:SiemensNewEDI
                                    Do Export
                                End !If man:SiemensNewEDI
                            End!If Instring('INTEC',def:user_name,1,1)

                            If job:Ignore_Warranty_Charges <> 'YES'
                                Pricing_Routine('W',labour",parts",pass",a")
                                If pass" = True
                                    job:Labour_Cost_Warranty    = Labour"
                                    job:Parts_Cost_Warranty     = Parts"
                                    job:Sub_Total_Warranty      = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                                End !If pass" = True

                            End !If job:Ignore_Warranty_Charges <> 'YES'

                            pos = Position(job:EDI_Key)

                            job:EDI = 'YES'
                            job:EDI_Batch_Number    = Man:Batch_Number
                            tmp:CountRecords += 1
                            AddToAudit
                            Access:JOBS.Update()

                            Reset(job:EDI_Key,pos)


                        End !Loop
                        Access:JOBS.RestoreFile(Save_job_ID)
                        Close(ProgressWindow)
                        If tmp:CountRecords
                            If Access:EDIBATCH.PrimeRecord() = Level:Benign
                                ebt:Batch_Number    = man:Batch_Number
                                ebt:Manufacturer    = tmp:ManufacturerName
                                If Access:EDIBATCH.TryInsert() = Level:Benign
                                    !Insert Successful
                                Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                    !Insert Failed
                                End !If Access:EDIBATCH.TryInsert() = Level:Benign

                                Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                                man:Manufacturer = tmp:ManufacturerName
                                If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                    !Found
                                    man:Batch_Number += 1
                                    Access:MANUFACT.Update()
                                Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                    !Error
                                    !Assert(0,'<13,10>Fetch Error<13,10>')
                                End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                            End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            
                            Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                              '<13,10>'&|
                              '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                           'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            glo:Select1  = ebt:Batch_Number
                            glo:select3  = 'NOKIA'
                            Nokia_accessories_Claim_Report
                            glo:select1 = ''
                            glo:Select3 = ''

                        End !If tmp:CountRecords

                    Else
                        tmp:CountRecords = 0
                        Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                        Save_job_ID = Access:JOBS.SaveFile()
                        Access:JOBS.ClearKey(job:EDI_Key)
                        job:Manufacturer     = tmp:ManufacturerName
                        job:EDI              = 'YES'
                        job:EDI_Batch_Number = f_Batch_Number
                        Set(job:EDI_Key,job:EDI_Key)
                        Loop
                            If Access:JOBS.NEXT()
                               Break
                            End !If
                            If job:Manufacturer     <> tmp:ManufacturerName      |
                            Or job:EDI              <> 'YES'      |
                            Or job:EDI_Batch_Number <> f_Batch_Number      |
                                Then Break.  ! End If
                            Do GetNextRecord2
                            cancelcheck# += 1
                            If cancelcheck# > (RecordsToProcess/100)
                                Do cancelcheck
                                If tmp:cancel = 1
                                    Break
                                End!If tmp:cancel = 1
                                cancelcheck# = 0
                            End!If cancelcheck# > 50

                            If man:NokiaType = 'NSH'
                                Do Export_NSH
                            Else!If Instring('INTEC',def:user_name,1,1)
                                If man:SiemensNewEDI
                                    Do NewExport
                                Else !If man:SiemensNewEDI
                                    Do Export
                                End !If man:SiemensNewEDI
                                
                            End!If Instring('INTEC',def:user_name,1,1)
                        End !Loop
                        Access:JOBS.RestoreFile(Save_job_ID)
                        Close(ProgressWindow)

                        ! Start Change 2806 BE(22/07/03)
                        !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                        !  '<13,10>'&|
                        !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                        !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                        !    Of 1 ! &OK Button
                        !nd!Case MessageEx
                        IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                            MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                        '<13,10>'&|
                                        '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                        'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                        beep:systemasterisk,msgex:samewidths,84,26,0)
                        ELSE
                            MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                                '<13,10>'&|
                                '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                beep:systemasterisk,msgex:samewidths,84,26,0)
                        END
                        ! End Change 2806 BE(22/07/03)

                        glo:Select1  = f_Batch_Number
                        glo:select3  = 'NOKIA'
                        Nokia_accessories_Claim_Report
                        glo:select1 = ''
                        glo:Select3 = ''

                    End!If f_batch_number = 0
                    If man:NokiaType = 'NSH'
                        CLOSE(iout_file)
                    Else!If man:NokiaType = 'NSH'
                        If man:SiemensNewEDI
                            Close(NokiaExport)
                        Else !If man:SiemensNewEDI
                            CLOSE(out_file)
                        End !If man:SiemensNewEDI
                    End!If man:NokiaType = 'NSH'
                End ! If Error# = 0
            End !If ManError# = 0
        End
    END
   Relate:EXCHANGE.Close
   Relate:JOBS.Close
   Relate:MANUFACT.Close
   Relate:WARPARTS.Close
   Relate:EDIBATCH.Close
   Relate:MODELNUM.Close
   Relate:STOCK.Close
   Relate:AUDIT.Close
   Relate:STATUS.Close
   Relate:JOBSTAGE.Close
   Relate:DEFAULTS.Close
   Relate:JOBNOTES.Close
   Relate:MANFAULT.Close
   Relate:MANFAULO.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
Export      Routine
        count_parts# = 0
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                Cycle
            End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = wpr:part_ref_number
            if access:stock.fetch(sto:ref_number_key) = Level:Benign
                If sto:accessory = 'YES'
                    Cycle
                End!If sto:accessory = 'YES'
                If sto:ExchangeUnit = 'YES'
                    Cycle
                End!If sto:ExchangeUnit = 'YES'
            end
            count_parts# += 1
        end !loop
        access:warparts.restorefile(save_wpr_id)

        If count_parts# <> 0 !Non-Accessory Parts On Job
        
            CLEAR(ouf:RECORD)
            CLEAR(mt_total)
            l1:blank = '0'
            l1:job_part = ',J'
            l1:serv_code = ',' & SUB(MAN:EDI_Account_Number,1,4)
            IF f_batch_number = 0
              l1:batch = ',' & SUB(MAN:Batch_Number,1,10)
            ELSE
              l1:batch =  ',' & SUB(f_Batch_Number,1,10)
            END
            l1:job_no = ',' & SUB(job:Ref_Number,1,10)
            l1:purch_ord = ',' & SUB(job:Fault_Code7,1,10)
            l1:price_cat = ',' & SUB(job:Fault_Code8,1,3)
            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 0                
                    End !If jot:RefNumber <> job:Ref_Number            
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1    
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                l1:serial_no = ',' & SUB(job:ESN,1,15)
            Else !IMEIError# = 1
                l1:serial_no = ',' & SUB(jot:OriginalIMEI,1,15)
            End !IMEIError# = 1
            
!            If job:exchange_unit_number <> ''
!                access:exchange.clearkey(xch:ref_number_key)
!                xch:ref_number = job:exchange_unit_number
!                if access:exchange.fetch(xch:ref_number_key) = Level:Benign
!                   l1:old_serno = ',' & Sub(xch:esn,1,15)
!                Else
!                    l1:old_serno = ','
!                end
!            Else
            l1:old_serno = ','                                                              !Communicaid say
!            End!If job:exchange_unit_number <> ''                                          !this ain't needed
            l1:repair_date = ',' & Format(job:date_completed,@d6) 
            l1:date_in = ',' & Format(job:date_booked,@d6)
            l1:auth_code   = ',' & SUB(job:fault_code9,1,10)

            access:modelnum.clearkey(mod:model_number_key)
            mod:model_number = job:model_number
            if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                If mod:nokia_unit_type <> ''
                    l1:unit_type = ',' & SUB(mod:nokia_unit_type,1,8)
                Else!If mod:nokia_unit_type <> ''
                    l1:unit_type = ',' & SUB(job:Unit_Type,1,8)
                End!If mod:nokia_unit_type <> ''
            Else!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                l1:unit_type =',' & SUB(job:Unit_Type,1,8)
            End!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
            
            l1:product_code =',' &  Sub(job:fault_code1,1,15) 
            l1:prod_s_no = ',' & Sub(job:fault_code2,1,15) 
            l1:hardware_id = ',' & Sub(job:fault_code3,1,15)
            l1:software_v =',' &  Sub(job:fault_code4,1,15) 
            l1:manufct_date = ',' & Sub(job:fault_code5,1,10)
            l1:fault_type = ','
            l1:module = ','
            l1:circuit = ','
            l1:part_no = ','
            l1:qty = ','
            l1:fault = ',' & SUB(STRIPCOMMA(job:Fault_Code10),1,100)
            l1:inv_txt = ',' & SUB(STRIPCOMMA(job:Fault_Code11),1,100)
            !If it's a lookup, use the fault code desciptoin field instead
            access:manfault.clearkey(maf:field_number_key)
            maf:manufacturer = 'NOKIA'
            maf:field_number = 11
            if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
                If maf:lookup = 'YES'
                    access:manfaulo.clearkey(mfo:field_key)
                    mfo:manufacturer = 'NOKIA'
                    mfo:field_number = 11
                    mfo:field        = job:fault_code11
                    if access:manfaulo.tryfetch(mfo:field_key) = Level:Benign
                        l1:inv_txt = ',' & SUB(STRIPCOMMA(mfo:description),1,100)
                    End!if access:manfpalo.tryfetch(mfp:field_key) = Level:Benign
                End!If maf:lookup = 'YES'
            End!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign

            l1:name = ',' & Sub(Clip(Upper(job:who_booked)),1,3)
            l1:date_claim = ',' & Format(job:date_booked,@d6)
            count += 1
            Add(out_file)

            count_parts# = 0
            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                    Cycle
                End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = wpr:part_ref_number
                if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    If sto:accessory = 'YES'
                        Cycle
                    End!If sto:accessory = 'YES'
                    If sto:ExchangeUnit = 'YES'
                        Cycle
                    End!If sto:ExchangeUnit = 'YES'
                end
                count_parts# += 1
                l1:blank       = '0'
                l1:job_part    = ',P'
                l1:serv_code   = ','
                l1:batch       = ','
                l1:job_no      = ','
                l1:purch_ord   = ','
                l1:price_cat   = ','
                l1:serial_no   = ','
                l1:old_serno   = ','
                l1:repair_date = ','
                l1:date_in     = ','
                l1:auth_code   = ','
                l1:unit_type   = ','
                l1:product_code =','
                l1:prod_s_no    =','
                l1:hardware_ID  =','
                l1:software_v   =','
                l1:manufct_date =','
                If wpr:fault_code3 = 'N/A'
                    l1:fault_type  = ','
                Else!If wpr:fault_code3 = 'N/A'
                    l1:fault_type  = ','&SUB(wpr:fault_code3,1,2)
                End!If wpr:fault_code3 = 'N/A'
                If wpr:fault_code2 = 'N/A'
                    l1:module      = ','
                Else!If wpr:fault_code2 = 'N/A'
                    l1:module      = ','&SUB(wpr:fault_code2,1,8)
                End!If wpr:fault_code2 = 'N/A'
                If wpr:fault_code1 = 'N/A'
                    l1:circuit     = ','
                Else!If wpr:fault_code1 = ''
                    l1:circuit     = ','&SUB(wpr:fault_code1,1,6)
                End!If wpr:fault_code1 = ''
                l1:part_no     = ','&SUB(wpr:part_number,1,15)
                l1:qty         = ','&SUB(wpr:quantity,1,7)
                l1:fault       = ','
                l1:inv_txt     = ','
                l1:name        = ','
                l1:date_claim  = ','
                ADD(out_file)
            end !loop
            access:warparts.restorefile(save_wpr_id)

        End!If count_parts# <> 1 !Non-Accessory Parts On Job
NewExport      Routine
!        count_parts# = 0
!        save_wpr_id = access:warparts.savefile()
!        access:warparts.clearkey(wpr:part_number_key)
!        wpr:ref_number  = job:ref_number
!        set(wpr:part_number_key,wpr:part_number_key)
!        loop
!            if access:warparts.next()
!               break
!            end !if
!            if wpr:ref_number  <> job:ref_number      |
!                then break.  ! end if
!            yldcnt# += 1
!            if yldcnt# > 25
!               yield() ; yldcnt# = 0
!            end !if
!            If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!                Cycle
!            End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
!
!            access:stock.clearkey(sto:ref_number_key)
!            sto:ref_number = wpr:part_ref_number
!            if access:stock.fetch(sto:ref_number_key) = Level:Benign
!                If sto:accessory = 'YES'
!                    Cycle
!                End!If sto:accessory = 'YES'
!                If sto:ExchangeUnit = 'YES'
!                    Cycle
!                End!If sto:ExchangeUnit = 'YES'
!            end
!            count_parts# += 1
!        end !loop
!        access:warparts.restorefile(save_wpr_id)
!
!        If count_parts# <> 0 !Non-Accessory Parts On Job

            Clear(nokex:Record)
            nokex:Line1 = '0,J'
            !Service Centre Code
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(man:EDI_Account_Number),',',''),' ')

            !Batch Number
            If f_Batch_Number = 0
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(man:Batch_Number),',',''),' ')
            Else !f_Batch_Number = 0
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(f_batch_Number),',',''),' ')
            End !f_Batch_Number = 0

            !Job Number
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Ref_Number),',',''),' ')
            !Repair Type
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(CLip(job:Fault_Code7),',',''),' ')
            !Level
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code8),',',''),' ')

            !Serial Number
            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 0                
                    End !If jot:RefNumber <> job:Ref_Number            
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1    
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:ESN),',',''),' ')
            Else !IMEIError# = 1
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(jot:OriginalIMEI),',',''),' ')
            End !IMEIError# = 1

            !Old Serial Number
            nokex:Line1 = Clip(nokex:Line1) & ',' !Old Serial Number.. not needed

            !Repair Date
            nokex:Line1 = Clip(nokex:Line1) & ',' & Format(job:Date_Completed,@d06)
            !Date In
            nokex:Line1 = Clip(nokex:Line1) & ',' & Format(job:Date_Booked,@d06)
            !Authorisation Code
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code9),',',''),' ')
            !Equipment Type

            access:modelnum.clearkey(mod:model_number_key)
            mod:model_number = job:model_number
            if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                If mod:nokia_unit_type <> ''
                    nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(mod:nokia_unit_type),',',''),' ')
                Else!If mod:nokia_unit_type <> ''
                    nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Unit_Type),',',''),' ')
                End!If mod:nokia_unit_type <> ''
            Else!if access:modelnum.fetch(mod:model_number_key) = Level:Benign
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Unit_Type),',',''),' ')
            End!if access:modelnum.fetch(mod:model_number_key) = Level:Benign

            !Product Code
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code1),',',''),' ')
            !Production Serial Number
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_code2),',',''),' ')
            !Hardware ID
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code3),',',''),' ')
            !Software Version
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code4),',',''),' ')
            !Manufacturing Date
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code5),',',''),' ')
            !Fault Type
            nokex:Line1 = Clip(nokex:Line1) & ',' !Fault Type
            !Module
            nokex:Line1 = Clip(nokex:Line1) & ',' !Module
            !Circuit
            nokex:Line1 = Clip(nokex:Line1) & ',' !Circuit
            !Part Number
            nokex:Line1 = Clip(nokex:Line1) & ',' !Part Number
            !Part Quantity
            nokex:Line1 = Clip(nokex:Line1) & ',' !Quantity
            !Reported Fault
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code10),',',''),' ')

            !Fault Found
            !If it's a lookup, use the fault code desciptoin field instead
            UseLookup# = 0
            access:manfault.clearkey(maf:field_number_key)
            maf:manufacturer = 'NOKIA'
            maf:field_number = 11
            if access:manfault.tryfetch(maf:field_number_key) = Level:Benign
                If maf:lookup = 'YES'
                    access:manfaulo.clearkey(mfo:field_key)
                    mfo:manufacturer = 'NOKIA'
                    mfo:field_number = 11
                    mfo:field        = job:fault_code11
                    if access:manfaulo.tryfetch(mfo:field_key) = Level:Benign
                        UseLookup# = 1
                        nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(mfo:description),',',''),' ')
                    End!if access:manfpalo.tryfetch(mfp:field_key) = Level:Benign
                End!If maf:lookup = 'YES'
            End!if access:manfault.tryfetch(maf:field_number_key) = Level:Benign

            If UseLookup# = 0
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code11),',',''),' ')
            End !UseLookup# = 0

            !Who Booked Claim
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Who_Booked),',',''),' ')
            !Date Claim Booked
            nokex:Line1 = Clip(nokex:Line1) & ',' & Format(job:Date_Booked,@d06)

            !Club Nokia ID
            nokex:Line1 = Clip(nokex:Line1) & ',' !??

            ! Start Change 3856 BE(3/2/04)
            !!ASC Number
            !nokex:Line1 = Clip(nokex:Line1) & ',' !??
            !!Country Code
            !nokex:Line1 = Clip(nokex:Line1) & ',' !??
            !!Region Code
            !nokex:Line1 = Clip(nokex:Line1) & ',' !??
            !!Sender Country
            !nokex:Line1 = Clip(nokex:Line1) & ',' !??
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(CLIP(ASC),',',''),' ')
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(CLIP(CountryCode),',',''),' ')
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(CLIP(RegionCode),',',''),' ')
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(CLIP(SenderCountry),',',''),' ')
            ! Start Change 3856 BE(3/2/04)

            !POP Date
            nokex:Line1 = Clip(nokex:Line1) & ',' & Format(job:DOP,@D06)
            !Within Warranty
            nokex:Line1 = Clip(nokex:Line1) & ',1' !Assume it's in Warranty?? 
            !Special Warranty Ref Number
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code12),',',''),' ')
            !Blue Tooth Address
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code7),',',''),' ')
            !Blue Tooth Hardware
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:fault_Code8),',',''),' ')
            !Swap Reason Code
            nokex:Line1 = Clip(nokex:Line1) & ',' !??
            !Service Code
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code11),',',''),' ')

            ! Start Change 3807 BE(15/01/04)
            !Technician Comment
            !nokex:Line1 = Clip(nokex:Line1) & ',' & StripReturn(Clip(jbn:Engineers_Notes))
            Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
            jbn:RefNumber = job:Ref_Number
            SET(jbn:RefNumberKey,jbn:RefNumberKey)
            IF ((Access:JOBNOTES.NEXT() <> Level:Benign) OR (jbn:RefNumber <> job:Ref_Number)) THEN
                nokex:Line1 = Clip(nokex:Line1) & ','
            ELSE
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(jbn:Engineers_Notes),',',''),' ')
            END
            ! End Change 3807 BE(15/01/04)

            !New Software Version
            nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(job:Fault_Code6),',',''),' ')
            !Repair Symtpom Code
            nokex:Line1 = Clip(nokex:Line1) & ',' !?? 
            !Key Repair
            nokex:Line1 = Clip(nokex:Line1) & ',' !?? 
            !Part Replaced
            nokex:Line1 = Clip(nokex:Line1) & ',' !?? !Only if Exclude from ordering is not ticked
            !Shipping Date/Time
            nokex:Line1 = Clip(nokex:Line1) & ',' & Format(job:Date_Despatched,@d06.) & ' 00:00'

            Add(NokiaExport)


            save_wpr_id = access:warparts.savefile()
            access:warparts.clearkey(wpr:part_number_key)
            wpr:ref_number  = job:ref_number
            set(wpr:part_number_key,wpr:part_number_key)
            loop
                if access:warparts.next()
                   break
                end !if
                if wpr:ref_number  <> job:ref_number      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                    Cycle
                End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = wpr:part_ref_number
                if access:stock.fetch(sto:ref_number_key) = Level:Benign
                    If sto:accessory = 'YES'
                        Cycle
                    End!If sto:accessory = 'YES'
                    If sto:ExchangeUnit = 'YES'
                        Cycle
                    End!If sto:ExchangeUnit = 'YES'
                end

                Clear(nokex:Record)

                ! Start Change Nokia EDI Bug BE(12/05/03)
                ! Following Line was inexplicably changed between V3.1 and V3.2 !!!!
                ! So lets put it back the way it was...
                !nokex:Line1 = '0,P,,,,,,,,,,,,,,,,,'
                nokex:Line1 = '0,P,,,,,,,,,,,,,,,,'
                ! End Change Nokia EDI Bug BE(12/05/03)

                !Fault Type
                If wpr:fault_code3 = 'N/A'
                    nokex:Line1 = Clip(nokex:Line1) & ','
                Else!If wpr:fault_code3 = 'N/A'
                    nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(wpr:Fault_Code3),',',''),' ')
                End!If wpr:fault_code3 = 'N/A'
                ! Start Change 3249 BE(16/09/03)
                !!Module
                !If wpr:fault_code2 = 'N/A'
                !    nokex:Line1 = Clip(nokex:Line1) & ','
                !Else!If wpr:fault_code2 = 'N/A'
                !    nokex:Line1 = Clip(nokex:Line1) & ',' & Clip(wpr:Fault_Code2)
                !End!If wpr:fault_code2 = 'N/A'
                !!Circuit
                !If wpr:fault_code1 = 'N/A'
                !    nokex:Line1 = Clip(nokex:Line1) & ','
                !Else!If wpr:fault_code1 = ''
                !    nokex:Line1 = Clip(nokex:Line1) & ',' & Clip(wpr:Fault_Code1)
                !End!If wpr:fault_code1 = ''
                !Module
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(wpr:Fault_Code2),',',''),' ')
                !Circuit
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(wpr:Fault_Code1),',',''),' ')
                ! End Change 3249 BE(16/09/03)
                !Part Number
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(wpr:Part_Number),',',''),' ')
                !Quantity
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(wpr:Quantity),',',''),' ')

                ! Start Change Nokia EDI Bug BE(13/05/03)
                ! Following Line was inexplicably changed between V3.1 and V3.2 !!!!
                ! So lets put it back the way it was...
                !nokex:Line1 = Clip(nokex:Line1) & ',,,,,,,,,,,,,,,,,,,'
                nokex:Line1 = Clip(nokex:Line1) & ',,,,,,,,,,,,,,,,,,'
                ! End Change Nokia EDI Bug BE(13/05/03)

                ! Start NOKIA EDI Bug BE(07/05/03)
                !Repair Symtpom Code
                !nokex:Line1 = Clip(nokex:Line1) & ',' & Clip(wpr:Fault_Code1)
                ! Start Change 4372 BE(11/6/2004)
                !nokex:Line1 = Clip(nokex:Line1) & ','
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(wpr:Fault_Code5),',',''),' ')
                ! End Change 4372 BE(11/6/2004)
                ! End NOKIA EDI Bug BE(07/05/03)

                !Key Repair
                nokex:Line1 = Clip(nokex:Line1) & ',' & BHStripNonAlphaNum(BHStripReplace(Clip(wpr:Fault_Code4),',',''),' ')
                !Part Replaced
                If wpr:Exclude_From_Order = 'YES'
                    nokex:Line1 = Clip(nokex:Line1) & ',0'
                Else !If wpr:Exclude_From_Order = 'YES'
                    nokex:Line1 = Clip(nokex:Line1) & ',1'
                End !If wpr:Exclude_From_Order = 'YES'
                !Shipping Date/Time
                nokex:Line1 = Clip(nokex:Line1) & ','

                Add(NokiaExport)
            end !loop
            access:warparts.restorefile(save_wpr_id)





        

!        End!If count_parts# <> 1 !Non-Accessory Parts On Job
Export_NSH      Routine
        CLEAR(iuf:RECORD)
        CLEAR(mt_total)

        count_parts# = 0
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key,wpr:part_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number  <> job:ref_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'
                Cycle
            End!If man:includeadjustment <> 'YES' and wpr:part_number = 'ADJUSTMENT'

            access:stock.clearkey(sto:ref_number_key)
            sto:ref_number = wpr:part_ref_number
            if access:stock.fetch(sto:ref_number_key) = Level:Benign
                If sto:accessory = 'YES'
                    Cycle
                End!If sto:accessory = 'YES'
                If sto:ExchangeUnit = 'YES'
                    Cycle
                End!If sto:ExchangeUnit = 'YES'
            end
            count_parts# += 1

            I1:blank1    = ' '&CHR(9)
            I1:blank2    = ' '&CHR(9)
            I1:blank3    = ' '&CHR(9)
            I1:blank4    = ' '&CHR(9)
            I1:blank5    = ' '&CHR(9)
            I1:blank6    = ' '&CHR(9)
            I1:Job_ID    = Format(FORMAT(job:ref_number,@n010),@s10)&CHR(9)
            I1:blank8    = ' '&CHR(9)
            I1:blank9    = ' '&CHR(9)
            I1:blank10   = ' '&CHR(9)
            I1:blank11   = ' '&CHR(9)
            I1:blank12   = ' '&CHR(9)
            I1:blank13   = ' '&CHR(9)
            I1:blank14   = ' '&CHR(9)
            I1:blank15   = ' '&CHR(9)
            I1:blank16   = ' '&CHR(9)
            I1:blank17   = ' '&CHR(9)
            I1:blank18   = ' '&CHR(9)
            I1:blank19   = ' '&CHR(9)
            I1:blank20   = ' '&CHR(9)
            I1:Comments  = Format('',@s20)&CHR(9)
            I1:blank22   = ' '&CHR(9)
            I1:blank23   = ' '&CHR(9)
            I1:OrdNum    = Format(job:order_number,@s7)&CHR(9)
            I1:blank25   = ' '&CHR(9)
            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 0                
                    End !If jot:RefNumber <> job:Ref_Number            
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1    
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                I1:SerNum    = Format(job:esn,@s16)&CHR(9)
            Else !IMEIError# = 1
                I1:SerNum    = Format(jot:OriginalIMEI,@s16)&CHR(9)
            End !IMEIError# = 1
            
            I1:blank27   = ' '&CHR(9)
            I1:blank28   = ' '&CHR(9)
            I1:blank29   = ' '&CHR(9)
            I1:blank30   = ' '&CHR(9)
            I1:blank31   = ' '&CHR(9)
            I1:model     = Format(job:model_number,@s15)&CHR(9)
            I1:dop       = Format(FORMAT(job:dop,@d5),@s6)&CHR(9)
            I1:WarrAuth  = Format(job:fault_code9,@s10)&CHR(9)
            I1:CustName  = Format(job:surname,@s20)&CHR(9)
            I1:CustAdd1  = Format(job:address_line1,@s20)&CHR(9)
            I1:CustAdd2  = Format(job:address_line2,@s20)&CHR(9)
            I1:CustAdd3  = Format(job:address_line3,@s20)&CHR(9)
            I1:CustAdd4  = Format(job:postcode,@s20)&CHR(9)
            I1:RepDate   = Format(FORMAT(job:date_Completed,@d5),@s6)&CHR(9)
            I1:blank41   = ' '&CHR(9)
            I1:Comments2 = Format('',@s20)&CHR(9)
            I1:PurOrdNo  = Format(job:fault_code7,@s10)&CHR(9)
            I1:RepPrCat  = Format(job:fault_code8,@s2)&CHR(9)
            access:jobnotes.clearkey(jbn:RefNumberKey)
            jbn:RefNumber   = job:ref_number
            access:jobnotes.tryfetch(jbn:RefNumberKey)
            I1:fault_d   = Format(StripReturn(stripcomma(jbn:fault_Description)),@s80)&CHR(9)
            I1:alt_i     = Format(StripReturn(stripcomma(jbn:invoice_Text)),@s240)&CHR(9)
            I1:blank47   = ' '&CHR(9)
            I1:blank48   = ' '&CHR(9)
            I1:blank49   = ' '&CHR(9)
            I1:blank50   = ' '&CHR(9)
            I1:blank51   = ' '&CHR(9)
            I1:labrate   = Format(FORMAT(job:labour_cost_Warranty,@n10.2),@s10)&CHR(9)
            I1:partcost  = Format(FORMAT(job:parts_cost_warranty,@n10.2),@s10)&CHR(9)
            I1:blank54   = ' '&CHR(9)
            I1:blank55   = ' '&CHR(9)
            I1:blank56   = ' '&CHR(9)
            I1:blank57   = ' '&CHR(9)
            I1:blank58   = ' '&CHR(9)
            I1:blank59   = ' '&CHR(9)
            I1:blank60   = ' '&CHR(9)
            I1:blank61   = ' '&CHR(9)
            I1:blank62   = ' '&CHR(9)
            I1:blank63   = ' '&CHR(9)
            I1:ClubID    = Format('0',@s10)&CHR(9)
            I1:PartNumb  = Format(wpr:part_number,@s7)&CHR(9)
            ADD(iout_file)

        end !loop
        access:warparts.restorefile(save_wpr_id)

        If count_parts# = 0 !No Parts On Job
        
            I1:blank1    = ' '&CHR(9)
            I1:blank2    = ' '&CHR(9)
            I1:blank3    = ' '&CHR(9)
            I1:blank4    = ' '&CHR(9)
            I1:blank5    = ' '&CHR(9)
            I1:blank6    = ' '&CHR(9)
            I1:Job_ID    = Format(FORMAT(job:ref_number,@n010),@s10)&CHR(9)
            I1:blank8    = ' '&CHR(9)
            I1:blank9    = ' '&CHR(9)
            I1:blank10   = ' '&CHR(9)
            I1:blank11   = ' '&CHR(9)
            I1:blank12   = ' '&CHR(9)
            I1:blank13   = ' '&CHR(9)
            I1:blank14   = ' '&CHR(9)
            I1:blank15   = ' '&CHR(9)
            I1:blank16   = ' '&CHR(9)
            I1:blank17   = ' '&CHR(9)
            I1:blank18   = ' '&CHR(9)
            I1:blank19   = ' '&CHR(9)
            I1:blank20   = ' '&CHR(9)
            I1:Comments  = Format('',@s20)&CHR(9)
            I1:blank22   = ' '&CHR(9)
            I1:blank23   = ' '&CHR(9)
            I1:OrdNum    = Format(job:order_number,@s7)&CHR(9)
            I1:blank25   = ' '&CHR(9)
            IMEIError# = 0
            If job:Third_Party_Site <> ''
                Access:JOBTHIRD.ClearKey(jot:RefNumberKey)
                jot:RefNumber = job:Ref_Number
                Set(jot:RefNumberKey,jot:RefNumberKey)
                If Access:JOBTHIRD.NEXT()
                    IMEIError# = 1
                Else !If Access:JOBTHIRD.NEXT()
                    If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 1
                    Else !If jot:RefNumber <> job:Ref_Number            
                        IMEIError# = 0                
                    End !If jot:RefNumber <> job:Ref_Number            
                End !If Access:JOBTHIRD.NEXT()
            Else !job:Third_Party_Site <> ''
                IMEIError# = 1    
            End !job:Third_Party_Site <> ''

            If IMEIError# = 1
                I1:SerNum    = Format(job:esn,@s16)&CHR(9)
            Else !IMEIError# = 1
                I1:SerNum    = Format(jot:OriginalIMEI,@s16)&CHR(9)
            End !IMEIError# = 1
            I1:blank27   = ' '&CHR(9)
            I1:blank28   = ' '&CHR(9)
            I1:blank29   = ' '&CHR(9)
            I1:blank30   = ' '&CHR(9)
            I1:blank31   = ' '&CHR(9)
            I1:model     = Format(job:model_number,@s15)&CHR(9)
            I1:dop       = Format(FORMAT(job:dop,@d5),@s6)&CHR(9)
            I1:WarrAuth  = Format(job:fault_code9,@s10)&CHR(9)
            I1:CustName  = Format(job:surname,@s20)&CHR(9)
            I1:CustAdd1  = Format(job:address_line1,@s20)&CHR(9)
            I1:CustAdd2  = Format(job:address_line2,@s20)&CHR(9)
            I1:CustAdd3  = Format(job:address_line3,@s20)&CHR(9)
            I1:CustAdd4  = Format(job:postcode,@s20)&CHR(9)
            I1:RepDate   = Format(FORMAT(job:date_Completed,@d5),@s6)&CHR(9)
            I1:blank41   = ' '&CHR(9)
            I1:Comments2 = Format('',@s20)&CHR(9)
            I1:PurOrdNo  = Format(job:fault_code7,@s10)&CHR(9)
            I1:RepPrCat  = Format(job:fault_code8,@s2)&CHR(9)
            access:jobnotes.clearkey(jbn:RefNumberKey)
            jbn:RefNumber   = job:ref_number
            access:jobnotes.tryfetch(jbn:RefNumberKey)
            I1:fault_d   = Format(StripReturn(stripcomma(jbn:fault_Description)),@s80)&CHR(9)
            I1:alt_i     = Format(StripReturn(stripcomma(jbn:invoice_Text)),@s240)&CHR(9)
            I1:blank47   = ' '&CHR(9)
            I1:blank48   = ' '&CHR(9)
            I1:blank49   = ' '&CHR(9)
            I1:blank50   = ' '&CHR(9)
            I1:blank51   = ' '&CHR(9)
            I1:labrate   = Format(FORMAT(job:labour_cost_Warranty,@n10.2),@s10)&CHR(9)
            I1:partcost  = Format(FORMAT(job:parts_cost_warranty,@n10.2),@s10)&CHR(9)
            I1:blank54   = ' '&CHR(9)
            I1:blank55   = ' '&CHR(9)
            I1:blank56   = ' '&CHR(9)
            I1:blank57   = ' '&CHR(9)
            I1:blank58   = ' '&CHR(9)
            I1:blank59   = ' '&CHR(9)
            I1:blank60   = ' '&CHR(9)
            I1:blank61   = ' '&CHR(9)
            I1:blank62   = ' '&CHR(9)
            I1:blank63   = ' '&CHR(9)
            I1:ClubID    = Format('0',@s10)&CHR(9)
            I1:PartNumb  = Format(wpr:part_number,@s7)&CHR(9)
            ADD(iout_file)
        End!If count_parts# = 0 
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Nokia_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Nokia_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'Nokia_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'Nokia_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'Nokia_EDI',1)
    SolaceViewVars('pos',pos,'Nokia_EDI',1)
    SolaceViewVars('ASC',ASC,'Nokia_EDI',1)
    SolaceViewVars('CountryCode',CountryCode,'Nokia_EDI',1)
    SolaceViewVars('RegionCode',RegionCode,'Nokia_EDI',1)
    SolaceViewVars('SenderCountry',SenderCountry,'Nokia_EDI',1)
    SolaceViewVars('tmp:PassedBatchNumber',tmp:PassedBatchNumber,'Nokia_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
