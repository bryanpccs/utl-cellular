

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01032.INC'),ONCE        !Local module procedure declarations
                     END


VK_EDI               PROCEDURE  (f_batch_number)      ! Declare Procedure
save_job_id          USHORT,AUTO
pos                  STRING(255)
count                REAL
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
CSVFile FILE,DRIVER('BASIC'),PRE(csv),NAME(Filename),CREATE,BINDABLE,THREAD
RECORD      RECORD,PRE(csvrec)
LineType           STRING(1)     ! A
JobNumber          STRING(8)     ! B
Model              STRING(30)    ! C
ESN                STRING(20)    ! D
MSN                STRING(20)    ! E
OperationLevel     STRING(30)    ! F Repair Type Warranty Code
SymptomCode        STRING(30)    ! G Job Fault Code 1
RepairCode         STRING(30)    ! H Part Fault Code 1
PartAction         STRING(30)    ! I Part Fault Code 2
PartReference      STRING(30)    ! J
PartQuantity       STRING(8)     ! K
OriginalSWLevel    STRING(30)    ! L Job Fault Code 2
SWLevelAfterRepair STRING(30)    ! M Job Fault Code 3
InWorkshopDate     STRING(8)     ! N
RepairDate         STRING(8)     ! O
DespatchDate       STRING(8)     ! P
TotalLabourValue   STRING(11)    ! Q
TotalPartsValue    STRING(11)    ! R
TotalValue         STRING(11)    ! S
           END
        END




! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'VK_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:EXCHANGE.Open
   Relate:WARPARTS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:SUBTRACC.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:TRADEACC.Open
   Relate:JOBSTAGE.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:STOCK.Open
   Relate:JOBSE.Open
   Relate:REPTYDEF.Open
!**Variable
    tmp:ManufacturerName = 'VK'
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
!**Variable
            IF f_batch_number = 0
                !filename = CLIP(CLIP(MAN:EDI_Path)&'\VK Batch'&CLIP(Format(MAN:Batch_number,@n05))&'.CSV')
                filename = CLIP(CLIP(MAN:EDI_Path)&'\VK Batch'&CLIP(MAN:Batch_number)&'.CSV')
            ELSE
                !filename = CLIP(CLIP(MAN:EDI_Path)&'\VK Batch'&CLIP(Format(f_Batch_number,@n05))&'.CSV')
                filename = CLIP(CLIP(MAN:EDI_Path)&'\VK Batch'&CLIP(f_Batch_number)&'.CSV')
            END
            OPEN(CSVFile)                           ! Open the output file
            IF ERRORCODE()                           ! If error
                CREATE(CSVFile)                       ! create a new file
                If Errorcode()
                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.',|
                                   'ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                                   beep:systemhand,msgex:samewidths,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End!If Errorcode() 
                OPEN(CSVFile)        ! If still error then stop
            ELSE
                !OPEN(out_file)
                EMPTY(CSVFile)
            END
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            IF (Error# = 0) THEN

                !YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                IF (f_batch_number = 0) THEN
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    SET(job:EDI_Key,job:EDI_Key)
                    LOOP
                        IF ((Access:JOBS.NEXT() <> Level:Benign) OR |
                             (job:Manufacturer <> tmp:ManufacturerName) OR |
                             (job:EDI <> 'NO' )) THEN
                           BREAK
                        END
                        Do GetNextRecord2
                        cancelcheck# += 1
                        IF (cancelcheck# > (RecordsToProcess/100)) THEN
                            DO cancelcheck
                            IF (tmp:cancel = 1) THEN
                                BREAK
                            END
                            cancelcheck# = 0
                        END

                        IF ((job:Date_Completed > tmp:ProcessBeforeDate) OR (job:Warranty_Job <> 'YES')) THEN
                            CYCLE
                        END

                        DO Export

                        IF (job:ignore_warranty_charges <> 'YES') THEN
                            Pricing_Routine('W',labour",parts",pass",a")
                            IF (pass" = True) THEN
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            END
                        END

                        pos = POSITION(job:EDI_Key)
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        RESET(job:EDI_Key,pos)

                    END
                    Access:JOBS.RestoreFile(Save_job_ID)
                    CLOSE(ProgressWindow)
                    IF (tmp:CountRecords) THEN
                       IF (Access:EDIBATCH.PrimeRecord() = Level:Benign) THEN
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            Access:EDIBATCH.TryInsert()
                            !IF (Access:EDIBATCH.TryInsert() = Level:Benign) THEN
                                !Insert Successful
                            !Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            !END

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign) THEN
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            !Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            END
                        END
                        
                        MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                                  '<13,10>'&|
                                  '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                  'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                  beep:systemasterisk,msgex:samewidths,84,26,0)
                    END

                ELSE
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    SET(job:EDI_Key,job:EDI_Key)
                    LOOP
                        IF ((Access:JOBS.NEXT() <> Level:Benign) OR |
                            (job:Manufacturer <> tmp:ManufacturerName)  OR |
                            (job:EDI <> 'YES') OR |
                            (job:EDI_Batch_Number <> f_Batch_Number)) THEN
                            BREAK
                        END
                        DO GetNextRecord2
                        cancelcheck# += 1
                        IF (cancelcheck# > (RecordsToProcess/100)) THEN
                            DO cancelcheck
                            IF (tmp:cancel = 1) THEN
                                BREAK
                            END
                            cancelcheck# = 0
                        END

                        DO Export
                    END
                    Access:JOBS.RestoreFile(Save_job_ID)
                    CLOSE(ProgressWindow)

                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END

                END
                CLOSE(CSVFile)
            END
        END
    END
   Relate:JOBS.Close
   Relate:EXCHANGE.Close
   Relate:WARPARTS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:SUBTRACC.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:TRADEACC.Close
   Relate:JOBSTAGE.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:STOCK.Close
   Relate:JOBSE.Close
   Relate:REPTYDEF.Close
Export            Routine
    CLEAR(csv:RECORD)
    ! Initialise Header Record
    csvrec:LineType = 'H'
    csvrec:JobNumber = job:Ref_Number
    csvrec:Model = job:Model_Number
    csvrec:ESN = job:ESN
    csvrec:MSN = job:MSN

    access:REPTYDEF.clearkey(rtd:Warranty_Key)
    rtd:Warranty = 'YES'
    rtd:Repair_Type = job:Repair_Type_Warranty
    IF (access:REPTYDEF.fetch(rtd:Warranty_Key) = Level:Benign) THEN
        csvrec:OperationLevel = rtd:WarrantyCode
    END

    csvrec:SymptomCode = job:Fault_Code1
    csvrec:OriginalSWLevel = job:Fault_Code2
    csvrec:SWLevelAfterRepair = job:Fault_Code3

    access:JOBSE.clearkey(jobe:RefNumberKey)
    jobe:refNumber = job:Ref_Number
    IF (access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) THEN
        csvrec:InWorkshopDate = FORMAT(jobe:InWorkshopDate, @d012)
    END

    ! Start Change 4286 BE(17/05/04)
    !csvrec:RepairDate = FORMAT(job:Date_In_Repair, @d012)
    csvrec:RepairDate = FORMAT(job:Date_Completed, @d012)
    ! End Change 4286 BE(17/05/04)
    csvrec:DespatchDate = FORMAT(job:Date_Despatched, @d012)

    TotalLabourValue$ = job:Labour_Cost_Warranty
    TotalPartsValue$ = 0.0
    TotalValue$ = 0.0

    csvrec:TotalLabourValue = FORMAT(TotalLabourValue$, @n8.2)

    access:WARPARTS.clearkey(wpr:Part_Number_Key)
    wpr:ref_Number = job:Ref_Number
    SET(wpr:Part_Number_Key, wpr:Part_Number_Key)
    LOOP
        IF ((access:WARPARTS.next() <> Level:Benign) OR (wpr:ref_Number <> job:Ref_Number)) THEN
            BREAK
        END
        csvrec:RepairCode = wpr:Fault_Code1
        csvrec:PartAction = wpr:Fault_Code2
        csvrec:PartReference = wpr:Part_Number
        csvrec:PartQuantity = wpr:quantity

        TotalPartsValue$ = wpr:Purchase_Cost * wpr:Quantity
        TotalValue$ = TotalLabourValue$ + TotalPartsValue$

        csvrec:TotalPartsValue = FORMAT(TotalPartsValue$, @n8.2)
        csvrec:TotalValue = FORMAT(TotalValue$, @n8.2)

        ADD(CSVFile)

        csvrec:LineType = 'D'
        csvrec:OperationLevel = ''
        csvrec:SymptomCode = ''
        csvrec:OriginalSWLevel = ''
        csvrec:SWLevelAfterRepair = ''
        csvrec:InWorkshopDate = ''
        csvrec:RepairDate = ''
        csvrec:DespatchDate = ''
        csvrec:RepairCode = ''
        csvrec:PartAction = ''
        csvrec:PartReference = ''
        csvrec:PartQuantity = ''
        csvrec:TotalLabourValue = ''
        csvrec:TotalPartsValue = ''
        csvrec:TotalValue = ''

        TotalLabourValue$ = 0.0
        TotalPartsValue$ = 0.0
        TotalValue$ = 0.0
    END

    IF (csvrec:LineType = 'H') THEN
        csvrec:TotalPartsValue = FORMAT(TotalPartsValue$, @n8.2)
        csvrec:TotalValue = FORMAT(TotalValue$, @n8.2)
        ADD(CSVFile)
    END

    
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'VK_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'VK_EDI',1)
    SolaceViewVars('pos',pos,'VK_EDI',1)
    SolaceViewVars('count',count,'VK_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'VK_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'VK_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'VK_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'VK_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'VK_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
