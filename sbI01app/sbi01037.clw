

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01037.INC'),ONCE        !Local module procedure declarations
                     END


StandardFormat_EDI   PROCEDURE  (f_batch_number,f:Manufacturer) ! Declare Procedure
save_job_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
pos                  STRING(255)
tmp:CSVFile          STRING(255),STATIC
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
CSVFile    File,Driver('ASCII'),Pre(csv),Name(filename),Create,Bindable,Thread
Record                  Record
Line                    String(2000)
                        End
                    End
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'StandardFormat_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:WARPARTS.Open
   Relate:EXCHANGE.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:STATUS.Open
   Relate:AUDIT.Open
   Relate:STAHEAD.Open
   Relate:TRADEACC.Open
   Relate:SUBTRACC.Open
   Relate:JOBSTAGE.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
!**Variable
    tmp:ManufacturerName = f:Manufacturer
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
            ManError# = 1
        End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        If ManError# = 0

            Error# = 0
!**Variable
            If f_Batch_Number = 0
                filename = CLip(man:EDI_Path) & '\' & Clip(tmp:ManufacturerName) & ' ' & Clip(man:Batch_Number) & '-' &  Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & '.CSV'
            Else ! If f_Batch_Number = 0
                filename = CLip(man:EDI_Path) & '\' & Clip(tmp:ManufacturerName) & ' ' & Clip(f_Batch_Number) & '-' &  Format(Day(Today()),@n02) & Format(Month(Today()),@n02) & Year(Today()) & '.CSV'
            End ! If f_Batch_Number = 0
            
            Remove(FileName)
            Create(CSVFile)
            Open(CSVFile)
            IF ERRORCODE()                           ! If error
                If Errorcode()
                    Case MessageEx('Unable to create EDI File.<13,10><13,10>Please check your EDI Defaults for this Manufacturer.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Error# = 1
                End!If Errorcode() 
            END
!**Variable
          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            If Error# = 0

                YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                If f_batch_number = 0
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'NO'      |
                            Then Break.  ! End If

                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        If job:Date_Completed > tmp:ProcessBeforeDate
                            Cycle
                        End !If job_ali:Date_Completed > tmp:ProcessBeforeDate

                        If job:Warranty_Job <> 'YES'
                            Cycle
                        End !If job:Warranty_Job <> 'YES'


                        Do Export

                        pos = Position(job:EDI_Key)

                        If job:ignore_warranty_charges <> 'YES'
                            Pricing_Routine('W',labour",parts",pass",a")
                            If pass" = True
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            End!If pass" = False
                        End!If job:ignore_warranty_charges <> 'YES'

                        !Found
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1

                        AddToAudit
                        Access:JOBS.Update()
                        Reset(job:EDI_Key,pos)

                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)
                    If tmp:CountRecords
                        If Access:EDIBATCH.PrimeRecord() = Level:Benign
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Successful
                            Else !If Access:EDIBATCH.TryInsert() = Level:Benign
                                !Insert Failed
                            End !If Access:EDIBATCH.TryInsert() = Level:Benign

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Found
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                                !Error
                                !Assert(0,'<13,10>Fetch Error<13,10>')
                            End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
                        End !If Access:EDIBATCH.PrimeRecord() = Level:Benign
                        
                        Case MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                          '<13,10>'&|
                          '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    End !If tmp:CountRecords

                Else
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    Set(job:EDI_Key,job:EDI_Key)
                    Loop
                        If Access:JOBS.NEXT()
                           Break
                        End !If
                        If job:Manufacturer     <> tmp:ManufacturerName      |
                        Or job:EDI              <> 'YES'      |
                        Or job:EDI_Batch_Number <> f_Batch_Number      |
                            Then Break.  ! End If
                        Do GetNextRecord2
                        cancelcheck# += 1
                        If cancelcheck# > (RecordsToProcess/100)
                            Do cancelcheck
                            If tmp:cancel = 1
                                Break
                            End!If tmp:cancel = 1
                            cancelcheck# = 0
                        End!If cancelcheck# > 50

                        Do Export
                    End !Loop
                    Access:JOBS.RestoreFile(Save_job_ID)
                    Close(ProgressWindow)

                    ! Start Change 2806 BE(22/07/03)
                    !Case MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                    !  '<13,10>'&|
                    !  '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                    !               'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0)
                    !    Of 1 ! &OK Button
                    !End!Case MessageEx
                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END
                    ! End Change 2806 BE(22/07/03)

                End!If f_batch_number = 0
                CLOSE(CSVFile)
            End ! If Error# = 0
        End !If ManError# = 0
    End
   Relate:JOBS.Close
   Relate:WARPARTS.Close
   Relate:EXCHANGE.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:STATUS.Close
   Relate:AUDIT.Close
   Relate:STAHEAD.Close
   Relate:TRADEACC.Close
   Relate:SUBTRACC.Close
   Relate:JOBSTAGE.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
Export        Routine
    Access:JOBSE.Clearkey(jobe:RefNumberKey)
    jobe:RefNumber  = job:Ref_Number
    If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
    Clear(CSVFile)
    !Record Type
    csv:Line = '"01'
    !ACR Account Number
    csv:Line = Clip(csv:Line) & '","' & Clip(man:EDI_Account_Number)
    !Claim Number
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Order_Number)
    !ACR Job Number
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Ref_Number)
    !Data Received
    If jobe:InWorkshopDate <> ''
        csv:Line = Clip(csv:Line) & '","' & Format(jobe:InWorkshopDate,@d6b)
    Else !If jobe:InWorkshopDate <> ''
        csv:Line = Clip(csv:Line) & '","' & Format(job:date_booked,@d6b)
    End !"If jobe:InWorkshopDate <> ''
    !Date Completed
    csv:Line = Clip(csv:Line) & '","' & Format(job:Date_Completed,@d6b)
    !Customer Name
    If job:Company_Name <> ''
        csv:Line = Clip(csv:Line) & '","' & Clip(job:Company_Name)
    Else ! If job:Company_Name <> ''
        csv:Line = Clip(csv:Line) & '","' & Clip(job:Title) & ' ' & Clip(job:Initial) & ' ' & Clip(job:Surname)
    End ! If job:Company_Name <> ''
    !Customer Address 1
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Address_Line1)
    !Customer Address 2
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Address_Line2)
    !Customer Address 3
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Address_Line3)
    !Customer Telephone
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Telephone_Number)
    !Postcode
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Postcode)
    !IMEI Number
    If jobe:Pre_RF_Board_IMEI <> ''
        csv:Line = Clip(csv:Line) & '","' & Clip(jobe:Pre_RF_Board_IMEI)
    Else ! If jobe:Pre_RF_Board_IMEI <> ''
        csv:Line = Clip(csv:Line) & '","' & Clip(job:ESN)
    End ! If jobe:Pre_RF_Board_IMEI <> ''
    !MSN
    csv:Line = Clip(csv:Line) & '","' & Clip(job:MSN)
    !Model Number
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Model_Number)
    !DOP
    csv:Line = Clip(csv:Line) & '","' & Format(job:DOP,@d6b)
    !RF Board IMEI Replacement No
    If jobe:Pre_RF_Board_IMEI <> ''
        csv:Line = Clip(csv:Line) & '","' & Clip(job:ESN)
    Else ! If jobe:Pre_RF_Board_IMEI <> ''
        csv:Line = Clip(csv:Line) & '","'
    End ! If jobe:Pre_RF_Board_IMEI <> ''
    !Fault Description
    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
    jbn:RefNumber   = job:Ref_Number
    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        ! Found

    Else ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
        ! Error
    End ! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
    csv:Line = Clip(csv:Line) & '","' & Clip(StripReturn(jbn:Fault_Description))
    !Invoice Text
    csv:Line = Clip(csv:Line) & '","' & Clip(StripReturn(jbn:Invoice_Text))
    !Fault Code 1
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code1)
    !Fault Code 2
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code2)
    !Fault Code 3
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code3)
    !Fault Code 4
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code4)
    !Fault Code 5
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code5)
    !Fault Code 6
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code6)
    !Fault Code 7
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code7)
    !Fault Code 8
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code8)
    !Fault Code 9
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code9)
    !Fault Code 10
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code10)
    !Fault Code 11
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code11)
    !Fault Code 12
    csv:Line = Clip(csv:Line) & '","' & Clip(job:Fault_Code12)
    !Labour Code
    csv:Line = Clip(csv:Line) & '","' & Format(job:Labour_Cost_Warranty,@n_14.2)
    !Parts Cost
    csv:Line = Clip(csv:Line) & '","' & Format(Job:Parts_Cost_Warranty,@n_14.2)
    !Courier Cost
    csv:Line = Clip(csv:Line) & '","' & Format(job:Courier_Cost_Warranty,@n14.2) & '"'
    Add(CSVFile)

    Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
    wpr:Ref_Number  = job:Ref_Number
    Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
    Loop
        If Access:WARPARTS.NEXT()
           Break
        End !If
        If wpr:Ref_Number  <> job:Ref_Number      |
            Then Break.  ! End If
        !Record Type
        csv:Line = '"02'
        !Part Number
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Part_Number)
        !Quantity
        If wpr:Adjustment = 'YES'
            csv:Line = Clip(csv:Line) & '","0'
        Else ! If wpr:Adjustment = 'YES'
            csv:Line = Clip(csv:Line) & '","' & wpr:Quantity
        End ! If wpr:Adjustment = 'YES'
        !Part Fault Code 1
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code1)
        !Part Fault Code 2
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code2)
        !Part Fault Code 3
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code3)
        !Part Fault Code 4
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code4)
        !Part Fault Code 5
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code5)
        !Part Fault Code 6
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code6)
        !Part Fault Code 7
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code7)
        !Part Fault Code 8
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code8)
        !Part Fault Code 9
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code9)
        !Part Fault Code 10
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code10)
        !Part Fault Code 11
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code11)
        !Part Fault Code 12
        csv:Line = Clip(csv:Line) & '","' & Clip(wpr:Fault_Code12) & '"'
        Add(CSVFile)
    End !Loop


getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'StandardFormat_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'StandardFormat_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'StandardFormat_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'StandardFormat_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'StandardFormat_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'StandardFormat_EDI',1)
    SolaceViewVars('pos',pos,'StandardFormat_EDI',1)
    SolaceViewVars('tmp:CSVFile',tmp:CSVFile,'StandardFormat_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
