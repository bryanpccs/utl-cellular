

   MEMBER('sbi01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBI01034.INC'),ONCE        !Local module procedure declarations
                     END


LG_EDI               PROCEDURE  (f_batch_number)      ! Declare Procedure
save_job_id          USHORT,AUTO
pos                  STRING(255)
count                REAL
save_wpr_id          USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ManufacturerName STRING(30)
tmp:ProcessBeforeDate DATE
tmp:CountRecords     LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Arial',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Arial',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Arial',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?ProgressCancel),LEFT,ICON('cancel.gif')
     END
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'LG_EDI')      !Add Procedure to Log
  end


   Relate:JOBS.Open
   Relate:EXCHANGE.Open
   Relate:WARPARTS.Open
   Relate:MANUFACT.Open
   Relate:EDIBATCH.Open
   Relate:SUBTRACC.Open
   Relate:STATUS.Open
   Relate:STAHEAD.Open
   Relate:TRADEACC.Open
   Relate:JOBSTAGE.Open
   Relate:JOBTHIRD.Open
   Relate:JOBS_ALIAS.Open
   Relate:STOCK.Open
   Relate:JOBSE.Open
   Relate:REPTYDEF.Open
!**Variable
    tmp:ManufacturerName = 'LG'
    continue# = 0
    If f_batch_number = 0
        tmp:ProcessBeforeDate   = ProcessBeforeDate()
        If tmp:ProcessBeforeDate <> ''
            continue# = 1
        End!If tmp:ProcessBeforeDate <> ''
    Else!If f_batch_number = 0
        continue# = 1
    End!If f_batch_number = 0
    If continue# = 1
        ManError# = 0
        !If FindManufacturer(tmp:ManufacturerName) = Level:Fatal
        !    ManError# = 1
        !End !If FindManufacturer(tmp:ManufacturerName) = Level:Benign

        Access:MANUFACT.ClearKey(man:Manufacturer_Key)
        man:Manufacturer = tmp:ManufacturerName
        IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) <> Level:Benign) THEN
            ManError# = 1
        END

        If ManError# = 0

            Error# = 0

          !-----Assume - Print all non-printed claims for Manufacturer X------------!
            IF (Error# = 0) THEN

                !YIELD()
                RecordsPerCycle = 25
                RecordsProcessed = 0
                PercentProgress = 0

                OPEN(ProgressWindow)
                Progress:Thermometer = 0
                ?Progress:PctText{Prop:Text} = '0% Completed'
                ?progress:userstring{Prop:Text} = Clip(tmp:ManufacturerName) & ' EDI EXPORT'

                IF (f_batch_number = 0) THEN
                    count_records# = 0
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'NO',0,tmp:ProcessBeforeDate)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'NO'
                    SET(job:EDI_Key,job:EDI_Key)
                    LOOP
                        IF ((Access:JOBS.NEXT() <> Level:Benign) OR |
                             (job:Manufacturer <> tmp:ManufacturerName) OR |
                             (job:EDI <> 'NO' )) THEN
                           BREAK
                        END
                        Do GetNextRecord2
                        cancelcheck# += 1
                        IF (cancelcheck# > (RecordsToProcess/100)) THEN
                            DO cancelcheck
                            IF (tmp:cancel = 1) THEN
                                BREAK
                            END
                            cancelcheck# = 0
                        END

                        IF ((job:Date_Completed > tmp:ProcessBeforeDate) OR (job:Warranty_Job <> 'YES')) THEN
                            CYCLE
                        END

                        !DO Export

                        IF (job:ignore_warranty_charges <> 'YES') THEN
                            Pricing_Routine('W',labour",parts",pass",a")
                            IF (pass" = True) THEN
                                job:labour_cost_warranty = labour"
                                job:parts_cost_warranty  = parts"
                                job:sub_total_warranty = job:labour_cost_warranty + job:parts_cost_warranty + job:courier_cost_warranty
                            END
                        END

                        pos = POSITION(job:EDI_Key)
                        job:EDI = 'YES'
                        job:EDI_Batch_Number    = Man:Batch_Number
                        tmp:CountRecords += 1
                        AddToAudit
                        Access:JOBS.Update()
                        RESET(job:EDI_Key,pos)

                    END
                    Access:JOBS.RestoreFile(Save_job_ID)
                    CLOSE(ProgressWindow)
                    IF (tmp:CountRecords) THEN
                       IF (Access:EDIBATCH.PrimeRecord() = Level:Benign) THEN
                            ebt:Batch_Number    = man:Batch_Number
                            ebt:Manufacturer    = tmp:ManufacturerName
                            Access:EDIBATCH.TryInsert()

                            Access:MANUFACT.ClearKey(man:Manufacturer_Key)
                            man:Manufacturer = tmp:ManufacturerName
                            IF (Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign) THEN
                                man:Batch_Number += 1
                                Access:MANUFACT.Update()
                            END
                        END
                        
                        MessageEx('Created Batch Number ' & ebt:batch_Number & '.'&|
                                  '<13,10>'&|
                                  '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                  'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                  beep:systemasterisk,msgex:samewidths,84,26,0)
                    END

                ELSE
                    tmp:CountRecords = 0
                    Recordstoprocess = CountEDIRecords(tmp:ManufacturerName,'YES',f_batch_number,0)

                    Save_job_ID = Access:JOBS.SaveFile()
                    Access:JOBS.ClearKey(job:EDI_Key)
                    job:Manufacturer     = tmp:ManufacturerName
                    job:EDI              = 'YES'
                    job:EDI_Batch_Number = f_Batch_Number
                    SET(job:EDI_Key,job:EDI_Key)
                    LOOP
                        IF ((Access:JOBS.NEXT() <> Level:Benign) OR |
                            (job:Manufacturer <> tmp:ManufacturerName)  OR |
                            (job:EDI <> 'YES') OR |
                            (job:EDI_Batch_Number <> f_Batch_Number)) THEN
                            BREAK
                        END
                        DO GetNextRecord2
                        cancelcheck# += 1
                        IF (cancelcheck# > (RecordsToProcess/100)) THEN
                            DO cancelcheck
                            IF (tmp:cancel = 1) THEN
                                BREAK
                            END
                            cancelcheck# = 0
                        END

                        !DO Export
                    END
                    Access:JOBS.RestoreFile(Save_job_ID)
                    CLOSE(ProgressWindow)

                    IF (glo:select1 = 'REJECTEDCLAIMS') THEN
                        MessageEx('Created Batch Number ' & f_batch_Number & '.'&|
                                    '<13,10>'&|
                                    '<13,10>Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                                    'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                                    beep:systemasterisk,msgex:samewidths,84,26,0)
                    ELSE
                        MessageEx('Re-Created Batch Number ' & f_batch_Number & '.'&|
                            '<13,10>'&|
                            '<13,10>Re-Create EDI Batch File: <13,10>' & CLIP(filename) & '.','ServiceBase 2000',|
                            'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,|
                            beep:systemasterisk,msgex:samewidths,84,26,0)
                    END

                END
            END
        END
    END
   Relate:JOBS.Close
   Relate:EXCHANGE.Close
   Relate:WARPARTS.Close
   Relate:MANUFACT.Close
   Relate:EDIBATCH.Close
   Relate:SUBTRACC.Close
   Relate:STATUS.Close
   Relate:STAHEAD.Close
   Relate:TRADEACC.Close
   Relate:JOBSTAGE.Close
   Relate:JOBTHIRD.Close
   Relate:JOBS_ALIAS.Close
   Relate:STOCK.Close
   Relate:JOBSE.Close
   Relate:REPTYDEF.Close
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?ProgressCancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'LG_EDI',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_job_id',save_job_id,'LG_EDI',1)
    SolaceViewVars('pos',pos,'LG_EDI',1)
    SolaceViewVars('count',count,'LG_EDI',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'LG_EDI',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'LG_EDI',1)
    SolaceViewVars('tmp:ManufacturerName',tmp:ManufacturerName,'LG_EDI',1)
    SolaceViewVars('tmp:ProcessBeforeDate',tmp:ProcessBeforeDate,'LG_EDI',1)
    SolaceViewVars('tmp:CountRecords',tmp:CountRecords,'LG_EDI',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
