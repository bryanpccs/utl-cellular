

   MEMBER('sbb02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBB02003.INC'),ONCE        !Local module procedure declarations
                     END


EDIFooter            PROCEDURE  (func:Count,func:Line) ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    gen:line1    = 'UNS+S'''
    access:expgen.insert()

    gen:line1   = 'CNT+' & Clip(func:Line-1) & ':1'''
    access:expgen.insert()

    gen:line1   = 'UNT+' & Clip(func:Count) & '+17'''
    access:expgen.insert()
    gen:line1   = 'UNZ+1+' & Clip(ord:Order_Number) & ''''
    access:expgen.insert()

!Line 2
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
