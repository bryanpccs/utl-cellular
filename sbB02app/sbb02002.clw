

   MEMBER('sbb02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBB02002.INC'),ONCE        !Local module procedure declarations
                     END


EDIBody              PROCEDURE  (func:line)           ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
!added to ensure!
    SET(Defaults,0)
    Access:Defaults.Next()

    gen:line1    = 'LIN+' & CLip(func:Line) & '++' & Clip(glo:q_part_number) & ':EN'''
    access:expgen.insert()
    
    gen:line1    = 'IMD+F++TU::9:' & Clip(glo:q_description) & ''''
    access:expgen.insert()
    
    gen:line1    = 'QTY+21:' & Clip(glo:q_quantity) & ''''
    access:expgen.insert()
    
    gen:line1    = 'PRI+AAA:' & Clip(FORMAT((glo:q_purchase_cost*Clip(GETINI('EDI','PartsOrderEuroRate',,CLIP(PATH())&'\SB2KDEF.INI'))),@n11.2) * glo:q_quantity) & ''''
    access:expgen.insert()
    
    gen:line1    = 'TAX+7+VAT+++:::17.50+S'''
    access:expgen.insert()
!Lines 5
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
