

   MEMBER('sbb02app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBB02001.INC'),ONCE        !Local module procedure declarations
                     END


EDIHeader            PROCEDURE                        ! Declare Procedure
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
    Clear(gen:record)
    gen:line1    = 'UNB+UNOA:2+5013546126849+NOKIAMPSF:ZZ+' & Format(ord:Date,@d11) & ':' & Format(Clock(),@t2) & '+' & Clip(ord:Order_Number) & '+RADIOHEAD+ORDERS'''
    access:expgen.insert()

    gen:line1    = 'UNH+17+ORDERS:D:96A:UN'''
    access:expgen.insert()

    gen:line1    = 'BGM+220+' & Clip(ord:Order_Number) & '+9'''
    access:expgen.insert()
    
    gen:line1    = 'DTM+2:' & Format(ord:Date,@d12) & ':102'''
    access:expgen.insert()

    gen:line1    = 'NAD+BY+5013546126849::9++COMMUNICAID LTD'''
    access:expgen.insert()
    
    gen:line1    = 'RFF+IA:831'''
    access:expgen.insert()
    
    gen:line1    = 'NAD+SU+NMPFI::9'''
    access:expgen.insert()

    gen:line1    = 'NAD+DP+5013546126849::9++KINGFISHER WAY+HINCHINGBROOKE BUSINESS PARK:HUNTINGDON:CAMBS+++PE29 6FL'''
    access:expgen.insert()

    gen:line1    = 'CUX+2:EUR:9'''

    !was  = 'CUX+2:GBP:9'''

    access:expgen.insert()
!Lines 8
! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
