

   MEMBER('sbcr0078.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBCR0002.INC'),ONCE        !Local module procedure declarations
                     END


ReceiveDataReport PROCEDURE                           !Generated from procedure template - Window

StartDate            DATE
EndDate              DATE
TempDate             DATE
SavePath             STRING(255)
RecordCount          LONG
FoundCount           LONG
InWorkshopQ          QUEUE,PRE(iwq)
DateValue            DATE
JobRefValue          LONG
                     END
LOC_GROUP            GROUP,PRE(loc)
ApplicationName      STRING(30)
ProgramName          STRING(30)
UserName             STRING(61)
Filename             STRING(255)
                     END
CSVFile     FILE,DRIVER('BASIC','/ALWAYSQUOTE=ON'),PRE(csv),NAME(Out_Filename),CREATE,BINDABLE,THREAD
Record          RECORD
rec_type                STRING(8)       ! STRING(2)     ! Record Type always "RV"
tpm_rcv_no              STRING(10)      ! STRING(7)     ! TPM Reference (Work Order) number
acc_no                  STRING(6)       ! STRING(3)     ! TPM ID (Fix ID by each TPM)
product_code            STRING(12)      ! STRING(8)     ! Product Model Name
IMEI_no                 STRING(15)      ! STRING(15)    ! IMEI number
tpm_rcv_datetime        STRING(16)      ! STRING(10)    ! TPM Receive yyyymmdd
def_cnt_no              STRING(14)      ! STRING(14)    ! Vodafone Reference Number
shop_date               STRING(10)      ! STRING(10)    ! Shop Receive yyyymmdd
rcv_content_1           STRING(13)      ! STRING(4)     ! Shop Symptom Code 1
rcv_content_2           STRING(13)      ! STRING(4)     ! Shop Symptom Code 2(op)
rcv_content_3           STRING(13)      ! STRING(4)     ! Shop Symptom Code 3(op)
rcv_text                STRING(240)     ! STRING(240)   ! Comment from shop
create_datetime         STRING(15)      ! STRING(14)    ! Data create datetime
               END
            END




window               WINDOW('Receive Data Export'),AT(,,216,103),FONT('Tahoma',8,,),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       PROMPT('In Workshop Date From'),AT(8,16),USE(?Prompt1),TRN
                       ENTRY(@D08B),AT(104,16,64,10),USE(StartDate)
                       BUTTON,AT(172,16,10,10),USE(?PopCalendar),IMM,FLAT,LEFT,ICON('calenda2.ico')
                       PROMPT('In Workshop Date To'),AT(8,32),USE(?Prompt2),TRN
                       ENTRY(@D08B),AT(104,32,64,10),USE(EndDate)
                       BUTTON,AT(172,32,10,10),USE(?PopCalendar:2),FLAT,LEFT,ICON('calenda2.ico')
                       SHEET,AT(4,2,208,70),USE(?Sheet1),WIZARD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('${21}'),AT(20,45),USE(?Prompt5),TRN,HIDE
                           PROMPT('Record No.'),AT(20,56),USE(?RecordCountPrompt),TRN,HIDE
                           STRING(@n8),AT(64,56,32,),USE(RecordCount),TRN,HIDE,RIGHT
                           PROMPT('Found'),AT(112,56),USE(?FoundCountPrompt),TRN,HIDE,LEFT
                           STRING(@n8),AT(140,56),USE(FoundCount),TRN,HIDE
                         END
                       END
                       PANEL,AT(4,74,208,26),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('E&xport'),AT(88,78,56,16),USE(?OKButton),FLAT,LEFT,ICON('ok.ico'),DEFAULT
                       BUTTON('&Cancel'),AT(148,78,56,16),USE(?CancelButton),FLAT,LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Open                   PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?StartDate{prop:ReadOnly} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 15066597
    Elsif ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 8454143
    Else ! If ?StartDate{prop:Req} = True
        ?StartDate{prop:FontColor} = 65793
        ?StartDate{prop:Color} = 16777215
    End ! If ?StartDate{prop:Req} = True
    ?StartDate{prop:Trn} = 0
    ?StartDate{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?EndDate{prop:ReadOnly} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 15066597
    Elsif ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 8454143
    Else ! If ?EndDate{prop:Req} = True
        ?EndDate{prop:FontColor} = 65793
        ?EndDate{prop:Color} = 16777215
    End ! If ?EndDate{prop:Req} = True
    ?EndDate{prop:Trn} = 0
    ?EndDate{prop:FontStyle} = font:Bold
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?RecordCountPrompt{prop:FontColor} = -1
    ?RecordCountPrompt{prop:Color} = 15066597
    ?RecordCount{prop:FontColor} = -1
    ?RecordCount{prop:Color} = 15066597
    ?FoundCountPrompt{prop:FontColor} = -1
    ?FoundCountPrompt{prop:Color} = 15066597
    ?FoundCount{prop:FontColor} = -1
    ?FoundCount{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetUserName                            ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE

    CommandLine = CLIP(COMMAND(''))

    tmpPos = INSTRING('%', CommandLine)
    IF (NOT tmpPos) THEN
        MessageEx('Attempting to use ' & CLIP(LOC:ProgramName) & '<10,13>'           & |
            '   without using ' & CLIP(LOC:ApplicationName) & '.<10,13>'                        & |
            '   Start ' & CLIP(LOC:ApplicationName) & ' and run the report from there.<10,13>',   |
            CLIP(LOC:ApplicationName),                                                            |
            'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
       POST(Event:CloseWindow)
       EXIT
    END

    SET(USERS)
    Access:USERS.Clearkey(use:Password_Key)
    glo:Password = CLIP(SUB(CommandLine, tmpPos + 1, 30))
      
    Access:USERS.Clearkey(use:Password_Key)
    use:Password = glo:Password
    IF (Access:USERS.Tryfetch(use:Password_Key) <> Level:benign) THEN
        MessageEx('Unable to find your logged in user details.', |
                CLIP(LOC:ApplicationName),                                  |
                'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
        POST(Event:CloseWindow)
        EXIT
    END

    IF (CLIP(use:Forename) = '') THEN
        LOC:UserName = use:Surname
    ELSE
        LOC:UserName = CLIP(use:Forename) & ' ' & use:Surname
    END

    IF (CLIP(LOC:UserName) = '') THEN
        LOC:UserName = '<' & use:User_Code & '>'
    END

    EXIT
BuildInWorkshopQ    ROUTINE
    FREE(InWorkshopQ)

    Access:JOBSE.ClearKey(jobe:RecordNumberKey)
    SET(jobe:RecordNumberKey, 0)
    LOOP
        IF (Access:JOBSE.NEXT()) THEN
            BREAK
        END

        TempCount# += 1
        TempCount2# += 1
        IF (TempCount2# >= 61) THEN
            TempCount2# = 0
            RecordCount = TempCount#
            DISPLAY(?RecordCount)
        END

        IF (INRANGE(jobe:InWorkshopDate, StartDate, EndDate)) THEN
            iwq:DateValue = jobe:InWorkshopDate
            iwq:JobRefValue = jobe:RefNumber
            ADD(InWorkshopQ)
            FoundCount += 1
            DISPLAY(?FoundCount)
        END
    END

    SORT(InWorkshopQ, iwq:JobRefValue)
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('ReceiveDataReport')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:JOBNOTES.Open
  Access:JOBS.UseFile
  Access:JOBSE.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?StartDate{Prop:Alrt,255} = MouseLeft2
  ?EndDate{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBNOTES.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Open PROCEDURE

  CODE
  StartDate = TODAY()
  EndDate   = StartDate
  
  LOC:ApplicationName = 'ServiceBase 2000'
  LOC:ProgramName = 'Receive Data Export'
  LOC:UserName = ''
  
  DO GetUserName
    
  DISPLAY
  PARENT.Open


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          StartDate = TINCALENDARStyle1(StartDate)
          Display(?StartDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          EndDate = TINCALENDARStyle1(EndDate)
          Display(?EndDate)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?OKButton
      ThisWindow.Update
      savepath = PATH()
      
      SET(defaults)
      access:defaults.next()
      
      fname" = '\RV002-' & FORMAT(TODAY(), @D012) & '.csv'
      
      IF (def:exportpath <> '') THEN
          Out_Filename = CLIP(def:exportpath) & fname"
      ELSE
          Out_Filename = 'C:' & fname"
      END
      
      IF (NOT FILEDIALOG('Choose File',Out_Filename,'*.*', file:save + file:keepdir + file:longname)) THEN
          SETPATH(savepath)
      ELSE
      
          IF (StartDate > EndDate) THEN
              TempDate = EndDate
              EndDate = StartDate
              StartDate = TempDate
          END
      
          REMOVE(CSVFile)
          CREATE(CSVFile)
          OPEN(CSVFile)
          IF (ERROR() <> Level:benign) THEN
              MessageEx('Cannot create export file.', LOC:ApplicationName,|
                          'Styles\stop.ico','&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,|
                          beep:systemhand,msgex:samewidths,84,26,0)
          ELSE
              SETCURSOR(cursor:wait)
      
              CLEAR(CSVFile)
              csv:rec_type         = 'rec_type'
              csv:tpm_rcv_no       = 'tpm_rcv_no'
              csv:acc_no           = 'acc_no'
              csv:product_code     = 'product_code'
              csv:IMEI_no          = 'IMEI_no'
              csv:tpm_rcv_datetime = 'tpm_rcv_datetime'
              csv:def_cnt_no       = 'def_cnt_no'
              csv:shop_date        = 'shop_date'
              csv:rcv_content_1    = 'rcv_content_1'
              csv:rcv_content_2    = 'rcv_content_2'
              csv:rcv_content_3    = 'rcv_content_3'
              csv:rcv_text         = 'rcv_text'
              csv:create_datetime  = 'create_datetime'
              ADD(CSVFile)
      
              thedate" = FORMAT(TODAY(), @d012) & ' ' & FORMAT(CLOCK(), @T01)
      
              RecordCount = 0
              FoundCount = 0
              TempCount# = 0
              TempCount2# = 0
              UNHIDE(?RecordCountPrompt)
              UNHIDE(?RecordCount)
              UNHIDE(?FoundCountPrompt)
              UNHIDE(?FoundCount)
              DISPLAY()
      
              ?Prompt5{PROP:Text} = 'Building Index...'
              UNHIDE(?Prompt5)
              DISPLAY(?Prompt5)
      
              DO BuildInWorkshopQ
      
              ?Prompt5{PROP:Text} = 'Exporting Data...'
              DISPLAY(?Prompt5)
      
              RecordCount = 0
              FoundCount = 0
              TempCount# = 0
              TempCount2# = 0
              DISPLAY()
      
              !access:JOBS.clearkey(job:date_booked_key)
              !job:date_booked = StartDate
              !SET(job:date_booked_key,job:date_booked_key)
              loopcount# = RECORDS(InWorkshopQ)
              LOOP idx# = 1 TO loopcount#
      
                  GET(InWorkshopQ, idx#)
      
                  TempCount# += 1
                  TempCount2# += 1
                  IF (TempCount2# >= 61) THEN
                      TempCount2# = 0
                      RecordCount = TempCount#
                      DISPLAY(?RecordCount)
                  END
      
                  access:jobs.clearkey(job:Ref_Number_Key)
                  job:ref_number = iwq:JobRefValue
                  IF ((access:jobs.fetch(job:Ref_Number_Key) <> Level:Benign) OR |
                      (job:manufacturer <> 'SHARP') OR (job:Workshop <> 'YES')) THEN
                      CYCLE
                  END
      
                  !IF ((job:manufacturer <> 'SHARP') OR (job:Workshop <> 'YES')) THEN
                  !    CYCLE
                  !END
      
                  access:jobse.clearkey(jobe:RefNumberKey)
                  jobe:refnumber = job:ref_number
                  !IF ((access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign)  OR |
                  !    (jobe:InWorkshopDate < StartDate) OR (jobe:InWorkshopDate > EndDate)) THEN
                  !    CYCLE
                  !END
                  IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                      CYCLE
                  END
      
                  FoundCount += 1
                  DISPLAY(?FoundCount)
      
                  CLEAR(CSVFile)
                  csv:rec_type         = 'RV'
                  csv:tpm_rcv_no       = SUB(CLIP(LEFT(job:ref_number)), 1, 7)
                  csv:acc_no           = '002'
                  csv:product_code     = SUB(CLIP(LEFT(job:model_number)), 1, 8)
                  csv:IMEI_no          = SUB(CLIP(LEFT(job:ESN)), 1, 15)
                  csv:tpm_rcv_datetime = FORMAT(jobe:InworkshopDate, @D012)
                  csv:def_cnt_no       = SUB(CLIP(LEFT(job:order_number)), 1, 14)
                  csv:shop_date        = FORMAT(job:date_booked, @d012)
                  csv:rcv_content_1    = SUB(CLIP(LEFT(job:Fault_Code4)), 1, 4)
                  csv:rcv_content_2    = ''
                  csv:rcv_content_3    = ''
                  access:jobnotes.clearkey(jbn:RefNumberKey)
                  jbn:RefNumber = job:ref_number
                  IF (access:jobnotes.fetch(jbn:RefNumberKey) = Level:Benign) THEN
                      csv:rcv_text    = SUB(jbn:fault_description, 1, 240)
                  ELSE
                      csv:rcv_text    = ''
                  END
                  csv:create_datetime  = CLIP(LEFT(thedate"))
                  ADD(CSVFile)
      
              END
      
              RecordCount = TempCount#
              DISPLAY(?RecordCount)
              CLOSE(CSVFile)
              SETCURSOR()
              MESSAGE('Receive Data Export Complete')
              POST(Event:CloseWindow)
          END
      
      END
      
    OF ?CancelButton
      ThisWindow.Update
      POST(Event:CloseWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  CASE FIELD()
  OF ?StartDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?EndDate
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

