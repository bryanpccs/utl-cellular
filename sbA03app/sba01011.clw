

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01011.INC'),ONCE        !Local module procedure declarations
                     END


CheckParts           PROCEDURE  (f_type)              ! Declare Procedure
save_par_id          USHORT,AUTO
save_wpr_id          USHORT,AUTO
tmp:ReturnValue      LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CheckParts')      !Add Procedure to Log
  end


    ! Return (1) = Pending Order
    ! Return (2) = On Order
    ! Return (0) = Received Order Or No Parts With Anything To Do WIth Orders
    tmp:ReturnValue = 0
    Case f_type
    Of 'C'
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:part_number_key)
        par:ref_number  = job:ref_number
        set(par:part_number_key, par:part_number_key)
        loop
            if access:parts.next()
                break
            end ! if
            if par:ref_number  <> job:ref_number      |
                then break                                                         ! end if
            end ! if
            If par:pending_ref_number <> '' and par:Order_Number = ''
                tmp:ReturnValue = 1
            End ! If
            If par:order_number <> ''
                If par:date_received = ''
                    tmp:ReturnValue = 2
                End! If par:date_received = ''
            End! If par:order_number <> ''
            found_received# = 1
        end ! loop
        access:parts.restorefile(save_par_id)
        setcursor()

    Of 'W'
        setcursor(cursor:wait)
        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:part_number_key)
        wpr:ref_number  = job:ref_number
        set(wpr:part_number_key, wpr:part_number_key)
        loop
            if access:warparts.next()
                break
            end ! if
            if wpr:ref_number  <> job:ref_number      |
                then break                                                         ! end if
            end ! if
            If wpr:pending_ref_number <> '' and wpr:Order_Number = ''
                tmp:ReturnValue = 1
            End ! If
            If wpr:order_number <> ''
                If wpr:date_received = ''
                    tmp:ReturnValue = 2
                End! If wpr:date_recieved = ''
            End! If wpr:order_number <> ''
        end ! loop
        access:warparts.restorefile(save_wpr_id)
        setcursor()

    End! Case f_type
    Return tmp:ReturnValue




SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CheckParts',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_par_id',save_par_id,'CheckParts',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'CheckParts',1)
    SolaceViewVars('tmp:ReturnValue',tmp:ReturnValue,'CheckParts',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
