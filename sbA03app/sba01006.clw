

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01006.INC'),ONCE        !Local module procedure declarations
                     END


Esn_Model_Routine    PROCEDURE  (f_esn,f_model_number,f_new_model_number,f_pass) ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
! Before Embed Point: %DataSection) DESC(Data Section) ARG()
save_esn_id     ushort,auto
! After Embed Point: %DataSection) DESC(Data Section) ARG()
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Esn_Model_Routine')      !Add Procedure to Log
  end


    f_pass = 0
    add_model# = 0
    If f_esn <> 'N/A' And f_esn <> ''
        If f_model_number <> ''
            found# = 1
            access:esnmodel.clearkey(esn:esn_key)
            esn:esn          = Sub(f_esn,1,6)
            esn:model_number = f_model_number
            if access:esnmodel.fetch(esn:esn_key)
                found# = 0
            end!if access:esnmodel.fetch(esn:esn_key)

            If found# = 0
                setcursor(cursor:wait)
                count# = 0
                save_esn_id = access:esnmodel.savefile()
                access:esnmodel.clearkey(esn:esn_only_key)
                esn:esn = Sub(f_esn,1,6)
                set(esn:esn_only_key,esn:esn_only_key)
                loop
                    if access:esnmodel.next()
                       break
                    end !if
                    if esn:esn <> Sub(f_esn,1,6)      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    count# += 1
                    If count# > 1
                        Break
                    End!If count# > 1
                end !loop
                access:esnmodel.restorefile(save_esn_id)
                setcursor()
                If count# = 0
                    f_new_model_number = f_model_number
                    add_model# = 1
                End!If count# = 0

                If count# = 1
                    access:esnmodel.clearkey(esn:esn_only_key)
                    esn:esn = Sub(f_esn,1,6)
                    if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
                        Case MessageEx('There is a mismatch between the selected Model Number and the entered E.S.N. / I.M.E.I.<13,10><13,10>This E.S.N. corresponse to a '&clip(esn:model_number)&'.<13,10><13,10>Do you wish to :<13,10><13,10>1) Change the Model Number of this unit to '&clip(esn:model_number)&', 2) Ignore the mismatch?','ServiceBase 2000',|
                                       'Styles\warn.ico','|&Change Model|&Ignore Mismatch|&Cancel',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                            Of 1 ! &Change Model Button
                                f_new_model_number = esn:model_number
                                f_pass = 1
                            Of 2 ! &Ignore Mismatch Button
                                f_pass = 1
                                f_new_model_number = f_model_number
                                add_model# = 1
                            Of 3 ! &Cancel Button
                        End!Case MessageEx
                    end!if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign            
                End!If count# = 1
                If count# > 1
                    Case MessageEx('There is a mismatch between the selected Model Number and the entered E.S.N. / I.M.E.I.<13,10><13,10>This E.S.N. corresponse to more than one Model Number.<13,10>Do you wish to :<13,10><13,10>1) Select a new Model Number for this unit,<13,10>2) Ignore the mismatch?','ServiceBase 2000',|
                                   'Styles\warn.ico','|&Select Model|&Ignore Mismatch|&Cancel',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                        Of 1 ! &Select Model Button
                            saverequest#      = globalrequest
                            globalresponse    = requestcancelled
                            globalrequest     = selectrecord
                            glo:select7 = f_esn
                            select_esn_model
                            glo:select7 = ''
                            if globalresponse = requestcompleted
                                f_new_model_number = esn:model_number
                                f_pass = 1
                            end
                            globalrequest     = saverequest#
                        Of 2 ! &Ignore Mismatch Button
                            f_new_model_number = f_model_number
                            f_pass = 1
                           add_model# = 1
                        Of 3 ! &Cancel Button
                    End!Case MessageEx
                End!If count# > 1
            End!If found# = 0
        Else!If job:model_number <> ''
            count# = 0
            setcursor(cursor:wait)
            save_esn_id = access:esnmodel.savefile()
            access:esnmodel.clearkey(esn:esn_only_key)
            esn:esn = Sub(f_esn,1,6)
            set(esn:esn_only_key,esn:esn_only_key)
            loop
                if access:esnmodel.next()
                   break
                end !if
                if esn:esn <> Sub(f_esn,1,6)      |
                    then break.  ! end if
                yldcnt# += 1
                if yldcnt# > 25
                   yield() ; yldcnt# = 0
                end !if
                count# += 1
                If count# > 1
                    Break
                End!If count# > 1
            end !loop
            access:esnmodel.restorefile(save_esn_id)
            setcursor()    
            
            If count# = 1
                access:esnmodel.clearkey(esn:esn_only_key)
                esn:esn = Sub(f_esn,1,6)
                if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
                    f_new_model_number    = esn:model_number
                    f_pass = 1
                end!if access:esnmodel.fetch(esn:esn_only_key) = Level:Benign
            End!If count# = 1
            
            If count# > 1

                saverequest#      = globalrequest
                globalresponse    = requestcancelled
                globalrequest     = selectrecord
                glo:select7 = f_esn
                select_esn_model
                glo:select7 = ''
                if globalresponse = requestcompleted
                    f_new_model_number = esn:model_number
                    f_pass = 1
                end
                globalrequest     = saverequest#

            End!If count# > 1
        End!If job:model_number <> ''

        If add_model# = 1
            get(esnmodel,0)
            if access:esnmodel.primerecord() = Level:Benign
                esn:esn           = f_esn
                esn:model_number  = f_new_model_number
                access:esnmodel.insert()
            end!if access:esnmodel.primerecord() = Level:Benign
        End!If add_model# = 1
    End!If f_esn <> 'N/A'
    


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Esn_Model_Routine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
