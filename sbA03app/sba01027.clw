

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01027.INC'),ONCE        !Local module procedure declarations
                     END


CommonFaultsWar      PROCEDURE  (f_refnumber,f_location) ! Declare Procedure
Local                CLASS
PartAlreadyAttached  Procedure(Long func:RefNumber,String func:PartNumber),Byte
                     END
tmp_RF_Board_IMEI    STRING(20)
tmp:engstockrefnumber LONG
tmp:engstockquantity LONG
tmp:mainstockrefnumber LONG
tmp:mainstockquantity LONG
tmp:engstocksundry   BYTE(0)
tmp:mainstocksundry  BYTE(0)
tmp:mainstore        STRING(30)
tmp:ordered          BYTE(0)
save_cwp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_war_ali_id      USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'CommonFaultsWar')      !Add Procedure to Log
  end


!Add Warranty Common Fault Parts
    setcursor(cursor:wait)
    save_cwp_id = access:commonwp.savefile()
    access:commonwp.clearkey(cwp:ref_number_key)
    cwp:ref_number = f_refnumber
    set(cwp:ref_number_key,cwp:ref_number_key)
    loop
        if access:commonwp.next()
           break
        end !if
        if cwp:ref_number <> f_refnumber      |
            then break.  ! end if

        !TH 09/02/04
        Access:LOCATION.ClearKey(loc:ActiveLocationKey)
        loc:Active   = 1
        loc:Location = f_location
        If Access:LOCATION.TryFetch(loc:ActiveLocationKey) = Level:Benign.

        ! Start Change 4652 BE(2/09/2004)
        !if loc:PickNoteEnable then
        tmp_RF_Board_IMEI = ''
        if NOT loc:PickNoteEnable then
        ! End Change 4652 BE(2/09/2004)
            ! Start Change 2116 BE(15/07/03)
            tmp_RF_Board_IMEI = ''
            access:stock.clearkey(sto:location_key)
            sto:location    = f_location
            sto:part_number = cwp:part_number
            IF (access:stock.tryfetch(sto:location_key) = Level:Benign) AND |
                (sto:RF_BOARD) THEN
                glo:select2 = ''
                glo:select3 = job:model_number
                glo:select4 = cwp:part_number
                SETCURSOR()
                EnterNewIMEI
                !SETCURSOR(cursor:wait)
                tmp_RF_Board_IMEI = CLIP(glo:select2)
                glo:select2 = ''
                glo:select3 = ''
                glo:select4 = ''
                IF (tmp_RF_Board_IMEI = '') THEN
                    CYCLE
                END
            END
            ! End Change 2116 BE(15/07/03)
        end

        If cwp:exclude_from_order = 'YES'
            If access:WARPARTS.primerecord() = Level:Benign
                wpr:ref_number           = job:ref_number
                wpr:adjustment           = cwp:adjustment
                wpr:part_number          = cwp:Part_Number
                wpr:description          = cwp:Description
                wpr:supplier             = cwp:Supplier
                wpr:purchase_cost        = cwp:Purchase_Cost
                wpr:sale_cost            = cwp:Sale_Cost
                wpr:quantity             = cwp:Quantity
                wpr:fault_codes_checked  = 'YES'
                wpr:fault_code1    = cwp:fault_code1
                wpr:fault_code2    = cwp:fault_code2
                wpr:fault_code3    = cwp:fault_code3
                wpr:fault_code4    = cwp:fault_code4
                wpr:fault_code5    = cwp:fault_code5
                wpr:fault_code6    = cwp:fault_code6
                wpr:fault_code7    = cwp:fault_code7
                wpr:fault_code8    = cwp:fault_code8
                wpr:fault_code9    = cwp:fault_code9
                wpr:fault_code10   = cwp:fault_code10
                wpr:fault_code11   = cwp:fault_code11
                wpr:fault_code12   = cwp:fault_code12
                                  
                wpr:exclude_from_order   = cwp:exclude_from_order
                wpr:date_ordered         = Today()
                Access:WARPARTS.Tryinsert()
            End!If access:parts.primerecord() = Level:Benign
        Else!If cwp:exclude_from_order = 'YES'
            !Clear the variables
            tmp:engstockrefnumber   = 0
            tmp:engstockquantity    = 0
            tmp:engstocksundry      = 0
            tmp:mainstockrefnumber  = 0
            tmp:mainstockquantity   = 0
            tmp:mainstocksundry     = 0
Compile('***',Debug=1)
    Message('Warranty Part Required: ' & cwp:part_number & |
            'Quantity: ' & cwp:quantity,'Debug Message',icon:exclamation)
***

            !Find the part at the Engineers Location
            access:stock.clearkey(sto:location_key)
            sto:location    = f_location
            sto:part_number = cwp:part_number
            if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number has been found in the Engineers' Location
                !Try and match Model Number
                access:stomodel.clearkey(stm:model_number_key)
                stm:ref_number   = sto:ref_number
                stm:manufacturer = job:manufacturer
                stm:model_number = job:model_number
                if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
Compile('***',Debug=1)
    Message('Warranty Part Found At Engineers Location: ' & |
            '|Stock Ref Number: ' & sto:ref_number & |
            '|Quantity Stock: ' & sto:quantity_stock,'Debug Message',icon:exclamation)
***

                    !Found Part At Engineers Location
                    If sto:Suspend
                        tmp:engstockrefnumber   = 0
                        tmp:engstockquantity    = 0
                    Else !If sto:Suspend
                        tmp:engstockrefnumber   = sto:ref_number
                        tmp:engstockquantity    = sto:quantity_stock
                        If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 1
                        Else!If sto:sundry_item = 'YES'
                            tmp:engstocksundry = 0
                        End!If sto:sundry_item = 'YES'
                    End !If sto:Suspend

                Else!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
                    !Part Number does not match Model Number
                End!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
            Else!if access:stock.tryfetch(sto:location_key) = Level:Benign
                !Part Number NOT found at engineer's location
            End!if access:stock.tryfetch(sto:location_key) = Level:Benign

            !As with jobs, parts should only come from the Engineer's Location,
            !not from Main Store.
            !If I keep the Main Store variables at zero, above, then I shouldn't need
            !to change anything else.

!            !Find the Main Store Location
!            tmp:mainstore   = ''
!            setcursor(cursor:wait)
!            save_loc_id = access:location.savefile()
!            access:location.clearkey(loc:main_store_key)
!            loc:main_store = 'YES'
!            set(loc:main_store_key,loc:main_store_key)
!            loop
!                if access:location.next()
!                   break
!                end !if
!                if loc:main_store <> 'YES'      |
!                    then break.  ! end if
!                tmp:mainstore   = loc:location
!                Break
!            end !loop
!            access:location.restorefile(save_loc_id)
!            setcursor()
!
!
!            if tmp:mainstore <> ''
!Compile('***',Debug=1)
!    Message('Warranty: There IS a main store: ' & tmp:mainstore,'Debug Message',icon:exclamation)
!***
!                !If a Main Store Location exists
!                access:stock.clearkey(sto:location_key)
!                sto:location    = tmp:mainstore
!                sto:part_number = cwp:part_number
!                if access:stock.tryfetch(sto:location_key) = Level:Benign
!                    !Part Number exists in Main Store
!                    !Does the Model Number match?
!                    access:stomodel.clearkey(stm:model_number_key)
!                    stm:ref_number   = sto:Ref_number
!                    stm:manufacturer = job:manufacturer
!                    stm:model_number = job:model_number
!                    if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!Compile('***',Debug=1)
!    Message('Warranty Part AT Main Store Location: ' &|
!            '|Stock Ref Number: ' & sto:ref_number & |
!            '|Quantity: ' & sto:quantity_stock,'Debug Message',icon:exclamation)
!***
!
!                        If sto:Suspend
!                            tmp:mainstockrefnumber  = 0
!                            tmp:mainstockquantity   = 0
!                        Else !If sto:Suspend
!                            tmp:mainstockrefnumber  = sto:ref_number
!                            tmp:mainstockquantity   = sto:quantity_stock
!
!                            If sto:sundry_item = 'YES'
!                                tmp:mainstocksundry = 1
!                            Else!If sto:sundry_item = 'YES'
!                                tmp:mainstocksundry = 0
!                            End!If sto:sundry_item = 'YES'
!                        End !If sto:Suspend
!
!
!                    Else!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!                        !Part Number does not match Model Number
!                    End!if access:stomodel.tryfetch(stm:model_number_key) = Level:Benign
!                Else!if access:stock.tryfetch(sto:location_key) = Level:Benign
!                    !Part Number does not exists in Main Store
!                End!if access:stock.tryfetch(sto:location_key) = Level:Benign
!            End!if tmp:mainstore <> ''

            If tmp:engstockrefnumber = 0 !And tmp:mainstockrefnumber = 0
                Case MessageEx('Cannot add the following part to this job. It has either been suspended or cannot be found in Engineer''s Location:<13,10><13,10>'&Clip(cwp:part_number)&'<13,10>'&Clip(cwp:description),'ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    Of 1 ! &OK Button
                End!Case MessageEx
            Else
                tmp:ordered = 0
                Do AddParts
                
            End!If tmp:engstockrefnumber = 0 And tmp:mainstockrefnumber = 0


        End!If cwp:exclude_from_order = 'YES'

        ! Start Change 2116 BE(15/07/03)
        IF (tmp_RF_Board_IMEI <> '') THEN
            GET(audit,0)
            IF (access:audit.primerecord() = level:benign) THEN
                aud:notes         = 'RF Board attached to job.<13,10>New IMEI ' & Clip(tmp_RF_Board_IMEI) & |
                                    '<13,10>Old IMEI ' & Clip(job:esn)
                aud:ref_number    = job:ref_number
                aud:date          = Today()
                aud:time          = Clock()
                access:users.clearkey(use:password_key)
                use:password =glo:password
                access:users.fetch(use:password_key)
                aud:user = use:user_code
                aud:action        = 'I.M.E.I. NUMBER CHANGED TO ' & Clip(tmp_RF_Board_IMEI)
                IF (access:audit.insert() <> Level:benign) THEN
                    access:audit.cancelautoinc()
                END
            END
            access:JOBSE.clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            IF (access:JOBSE.fetch(jobe:RefNumberKey) <> Level:Benign) THEN
                IF (access:JOBSE.primerecord() = Level:Benign) THEN
                    jobe:RefNumber  = job:Ref_Number
                    IF (access:JOBSE.tryinsert()) THEN
                        access:JOBSE.cancelautoinc()
                    END
                END
            END
            IF (jobe:PRE_RF_BOARD_IMEI = '') THEN
                jobe:PRE_RF_BOARD_IMEI = job:ESN
                access:jobse.update()
            END
            job:ESN = tmp_RF_Board_IMEI
            access:jobs.update()
        END
        ! End Change 2116 BE(15/07/03)

    end !loop
    access:commonwp.restorefile(save_cwp_id)
    setcursor()
    Return tmp:ordered
AddParts        Routine

    Set(Defaults)
    access:Defaults.next()

    !Is part already attached?
    Found# = 0
    If tmp:EngStockRefNumber
        If Local.PartAlreadyAttached(tmp:EngStockRefNumber,cwp:Part_Number)
            Found# = 1
        End !If Local.PartAlreadyAttached(tmp:EngStockRefNumber,cwp:Part_Number)
    End !If tmp:EngStockRefNumber
    If Found# = 0

        
        If access:warparts.primerecord() = Level:Benign
            !Insert a New Part
            wpr:quantity             = cwp:Quantity
            Do AddPartDetails

            If tmp:engstockrefnumber <> 0
                !Engineers Part Exists
                If tmp:engstocksundry
                    !If part is a sundry, don't take from stock and just add it.
                    wpr:part_ref_number = tmp:engstockrefnumber
                    wpr:date_ordered    = Today()
                    access:warparts.tryinsert()
                Else!If tmp:engstocksundry
                    If cwp:quantity > tmp:engstockquantity
                        !The Quantity Required is more than in the Engineers Location
                        If tmp:engstockquantity = 0
                            !If there is NO engineers stock
                            If tmp:mainstockrefnumber <> 0
                                !If there is stock in Main Stock
                                If wpr:quantity > tmp:mainstockquantity
                                    !If Stock required is more than Main Stock Quantity
                                    If access:ordpend.primerecord() = Level:Benign
                                        ope:part_ref_number = tmp:engstockrefnumber
                                        ope:job_number      = job:ref_number
                                        ope:part_type       = 'WAR'
                                        ope:supplier        = wpr:supplier
                                        ope:part_number     = wpr:part_number
                                        ope:Description     = wpr:Description
                                        If tmp:mainstockquantity = 0
                                            !If there is NO Main Store stock
                                            !Order all the stock for the engineer part
                                            ope:quantity        = cwp:quantity
                                            access:ordpend.insert()
                                            tmp:ordered = 1
                                            wpr:pending_ref_number  = ope:ref_number
                                            wpr:part_ref_number = tmp:engstockrefnumber
                                            access:warparts.insert()
                                            if def:PickNoteNormal or def:PickNoteMainStore then
                                                CreatePickingNote (sto:Location)
                                                InsertPickingPart ('warranty')
                                            end
                                        Else!If tmp:mainstockquantity = 0
                                            !If there is some Main Store Stock, but not enough
                                            !make an order for the difference, and change the
                                            !part's quantity to match what was taken from Main Store
                                            ope:quantity    = cwp:quantity - tmp:mainstockquantity
                                            access:ordpend.insert()
                                            tmp:ordered = 1
                                            wpr:quantity        = tmp:mainstockquantity
                                            wpr:date_ordered    = Today()
                                            wpr:part_ref_number = tmp:mainstockquantity
                                            access:warparts.insert()
                                            if def:PickNoteNormal or def:PickNoteMainStore then
                                                CreatePickingNote (sto:Location)
                                                InsertPickingPart ('warranty')
                                            end

                                            !Take the item from Stock
                                            access:stock.clearkey(sto:ref_number_key)
                                            sto:ref_number      = tmp:mainstockrefnumber
                                            If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                !Take The quantity from stock
                                                sto:quantity_stock  = 0
                                                access:stock.update()

                                                If access:stohist.primerecord() = Level:Benign
                                                    shi:quantity    = tmp:mainstockquantity
                                                    Do AddStockHistory
                                                End!If access:stohist.primerecord() = Level:Benign
                                            End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign

                                            !Create a new part for the Pending Order
                                            If access:warparts.primerecord() = Level:benign
                                                wpr:quantity    = cwp:quantity - tmp:mainstockquantity
                                                wpr:pending_ref_number  = ope:ref_number
                                                access:warparts.insert()
                                                if def:PickNoteNormal or def:PickNoteMainStore then
                                                    CreatePickingNote (sto:Location)
                                                    InsertPickingPart ('warranty')
                                                end
                                            End!If access:warparts.primerecord() = Level:benign
                                        End!If tmp:mainstockquantity = 0

                                        ! Start Change 4652 BE(02/09/2004)
                                        ! Status 330 Spares Requested
                                        GetStatus(330,2,'JOB')
                                        ! End Change 4652 BE(02/09/2004)

                                     End!If access:ordpend.primerecord() = Level:Benign
                                Else!If wpr:quantity > tmp:mainstockquantity
                                    !There is sufficent Stock In the Main Store
                                    !Take the item from stock
                                    wpr:quantity    = cwp:quantity
                                    wpr:date_ordered    = Today()
                                    wpr:part_ref_number = tmp:mainstockrefnumber
                                    access:warparts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('warranty')
                                    end

                                    access:stock.clearkey(sto:ref_number_key)
                                    sto:ref_number  = tmp:mainstockrefnumber
                                    If access:stock.tryfetch(sto:ref_number_key) = level:benign
                                        sto:quantity_stock -= cwp:quantity
                                        If sto:quantity_stock < 0
                                            sto:quantity_stock = 0
                                        End!If sto:quantity_stock < 0
                                        access:stock.update()

                                        If access:stohist.primerecord() = Level:benign
                                            shi:quantity    = cwp:quantity
                                            Do AddStockHistory
                                        End!If access:stohist.primerecord() = Level:benign
                                    End!If access:stock.tryfetch(sto:ref_number_key) = level:benign
                                End!If wpr:quantity > tmp:mainstockquantity
                            Else!If tmp:mainstockrefnumber <> 0
                                !There is No Main Store Stock Item
                                !Create an order from the Engineers Part
                                If access:ordpend.primerecord() = Level:Benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'WAR'
                                    ope:supplier        = wpr:supplier
                                    ope:part_number     = wpr:part_number
                                    ope:quantity        = cwp:quantity
                                    ope:Description     = wpr:Description
                                    access:ordpend.insert()
                                    tmp:ordered = 1
                                    wpr:quantity        = cwp:quantity
                                    wpr:pending_ref_number  = ope:ref_number
                                    wpr:Part_Ref_Number = tmp:EngStockRefNumber
                                    access:warparts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('warranty')
                                    end

                                    ! Start Change 4652 BE(02/09/2004)
                                    ! Status 330 Spares Requested
                                    GetStatus(330,2,'JOB')
                                    ! End Change 4652 BE(02/09/2004)

                                End!If access:ordpend.primerecord() = Level:Benign
                            End!If tmp:mainstockrefnumber <> 0
                        Else!If tmp:engstockquantity = 0
                            !There is some stock in the Engineer's Location, but not enough
                            !Take the quantity left from engineers stock
                            wpr:quantity        = tmp:engstockquantity
                            wpr:date_ordered    = Today()
                            wpr:part_ref_number = tmp:engstockrefnumber
                            access:warparts.insert()
                            if def:PickNoteNormal or def:PickNoteMainStore then
                                CreatePickingNote (sto:Location)
                                InsertPickingPart ('warranty')
                            end

                            access:stock.clearkey(sto:ref_number_key)
                            sto:ref_number  = tmp:engstockrefnumber
                            If access:stock.tryfetch(sto:ref_number_key) = level:Benign
                                sto:quantity_stock = 0
                                access:stock.update()

                                If access:stohist.primerecord() = Level:Benign
                                    shi:quantity    = tmp:engstockquantity
                                    Do AddStockHistory
                                End!If access:stohist.primerecord() = Level:Benign
                            End!If access:stock.tryfetch(sto:ref_number_key) = level:Benign

                            !Is there a main store item as well?
                            If tmp:mainstockrefnumber <> 0
                                !There is a main store item, is there enough in stock?
                                If (cwp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                    !Quantity requested is higher than main store and engineer put together
                                    !Create an order
                                    If access:ordpend.primerecord() = Level:benign
                                        ope:part_ref_number = tmp:engstockrefnumber
                                        ope:job_number      = job:ref_number
                                        ope:part_type       = 'WAR'
                                        ope:supplier        = wpr:supplier

                                        If access:warparts.primerecord() = Level:Benign
                                            Do AddPartDetails
                                            wpr:part_ref_number = tmp:engstockrefnumber
                                            If tmp:mainstockquantity = 0
                                                !There is no stock in Main Store
                                                wpr:quantity    = cwp:quantity - tmp:engstockquantity
                                                wpr:pending_ref_number = ope:ref_number
                                                access:warparts.insert()
                                                if def:PickNoteNormal or def:PickNoteMainStore then
                                                    CreatePickingNote (sto:Location)
                                                    InsertPickingPart ('warranty')
                                                end
                                                ope:quantity    = wpr:quantity
                                                access:ordpend.insert()
                                                tmp:ordered = 1
                                            Else!If tmp:mainstockquantity = 0
                                                !Take the stock from main store.
                                                !Create an order pending for the rest
                                                wpr:quantity    = tmp:mainstockquantity
                                                wpr:date_ordered    = Today()
                                                wpr:part_ref_number = tmp:mainstockrefnumber
                                                access:warparts.insert()
                                                if def:PickNoteNormal or def:PickNoteMainStore then
                                                    CreatePickingNote (sto:Location)
                                                    InsertPickingPart ('warranty')
                                                end

                                                If access:warparts.primerecord() = Level:benign
                                                    Do AddPartDetails
                                                    wpr:part_ref_number = tmp:engstockrefnumber
                                                    wpr:quantity        = cwp:quantity - (tmp:engstockquantity + tmp:mainstockquantity)
                                                    wpr:pending_ref_number  = ope:ref_number
                                                    access:warparts.insert()
                                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                                        CreatePickingNote (sto:Location)
                                                        InsertPickingPart ('warranty')
                                                    end
                                                    ope:quantity        = wpr:quantity
                                                    access:ordpend.insert()
                                                    tmp:ordered = 1
                                                End!If access:warparts.primerecord() = Level:benign

                                                Access:stock.clearkey(sto:ref_number_key)
                                                sto:ref_number  = tmp:mainstockrefnumber
                                                If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                    sto:quantity_stock = 0
                                                    access:stock.update()
                                                End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                                If access:stohist.primerecord() = Level:Benign
                                                    shi:quantity    = tmp:mainstockquantity
                                                    Do AddStockHistory
                                                End!If access:stohist.primerecord() = Level:Benign
                                            End!If tmp:mainstockquantity = 0
                                        End!If access:warparts.primerecord() = Level:Benign

                                        ! Start Change 4652 BE(02/09/2004)
                                        ! Status 330 Spares Requested
                                        GetStatus(330,2,'JOB')
                                        ! End Change 4652 BE(02/09/2004)

                                    End!If access:ordpend.primerecord(0 = Level:benign
                                Else!If (cwp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                                    !The is sufficient stock left in the Main Store
                                    wpr:quantity    = cwp:quantity - tmp:engstockquantity
                                    wpr:part_ref_number = tmp:mainstockrefnumber
                                    wpr:date_ordered    = Today()
                                    access:warparts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('warranty')
                                    end

                                    access:stock.clearkey(sto:ref_number_key)
                                    sto:ref_number  = tmp:mainstockrefnumber
                                    If Access:stock.tryfetch(sto:ref_number_key) = level:Benign
                                        sto:quantity_stock  -= (cwp:quantity - tmp:engstockquantity)
                                        If sto:quantity_stock < 0
                                            sto:quantity_stock = 0
                                        End!If sto:quantity_stock < 0
                                        access:stock.update()
                                    End!If Access:stock.tryfetch(sto:ref_number_key) = level:Benign

                                    If access:stohist.primerecord() = Level:Benign
                                        shi:quantity    = cwp:quantity - tmp:engstockquantity
                                        Do AddStockHistory
                                    End!If access:stohist.primerecord() = Level:Benign
                                End!If (cwp:quantity - tmp:engstockquantity) > tmp:mainstockquantity
                            Else!If tmp:mainstockrefnumber <> 0
                                !If there is no Main Store Item
                                !Create an order for the remaining
                                If access:ordpend.primerecord() = Level:Benign
                                    ope:part_ref_number = tmp:engstockrefnumber
                                    ope:job_number      = job:ref_number
                                    ope:part_type       = 'WAR'
                                    ope:supplier        = wpr:supplier
                                    ope:part_number     = wpr:part_number
                                    ope:Description     = wpr:Description
                                    ope:quantity        = cwp:quantity - tmp:engstockquantity
                                    access:ordpend.insert()
                                    tmp:ordered = 1
                                    If access:warparts.primerecord() = Level:Benign
                                        Do AddPartDetails
                                        wpr:pending_ref_number  = ope:ref_number
                                        wpr:part_ref_number     = tmp:engstockrefnumber
                                        wpr:quantity            = cwp:quantity - tmp:engstockquantity
                                        access:warparts.insert()
                                        if def:PickNoteNormal or def:PickNoteMainStore then
                                            CreatePickingNote (sto:Location)
                                            InsertPickingPart ('warranty')
                                        end
                                    End!If access:warparts.primerecord() = Level:Benign

                                    ! Start Change 4652 BE(02/09/2004)
                                    ! Status 330 Spares Requested
                                    GetStatus(330,2,'JOB')
                                    ! End Change 4652 BE(02/09/2004)

                                End!If access:ordpend.primerecord() = Level:Benign
                            End!If tmp:mainstockrefnumber <> 0
                        End!If tmp:engstockquantity = 0
                    Else!If cwp:quantity > tmp:engstockquantity
                        !If there is enough in the engineer's location
                        wpr:quantity   = cwp:quantity
                        wpr:date_ordered    = Today()
                        wpr:part_ref_number = tmp:engstockrefnumber
                        access:warparts.insert()
                        if def:PickNoteNormal or def:PickNoteMainStore then
                            CreatePickingNote (sto:Location)
                            InsertPickingPart ('warranty')
                        end
                        access:stock.clearkey(sto:ref_number_key)
                        sto:ref_number  = tmp:engstockrefnumber
                        If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                            sto:quantity_stock -= cwp:quantity
                            If sto:quantity_stock < 0
                                sto:quantity_stock = 0
                            End!If sto:quantity_stock < 0
                            access:stock.update()
                            If access:stohist.primerecord() = level:Benign
                                shi:quantity    = cwp:quantity
                                Do AddStockHistory
                            End!If access:stohist.primerecord() = level:Benign
                        End!If access:stock.tryfetch(sto:ref_number_key) = Level:benign
                    End!If cwp:quantity > tmp:engstockquantity
                End!If tmp:engstocksundry
            Else!If tmp:engstockrefnumber <> 0
                !If there is no engineer's stock
                !Is there a main stock item?
                If tmp:mainstockrefnumber <> 0
                    !There is a main store item
                    If tmp:mainstocksundry
                        wpr:quantity    = tmp:mainstockquantity
                        wpr:date_ordered = Today()
                        wpr:part_ref_number  = tmp:mainstockrefnumber
                        access:warparts.insert()
                        if def:PickNoteNormal or def:PickNoteMainStore then
                            CreatePickingNote (sto:Location)
                            InsertPickingPart ('warranty')
                        end
                    Else!If tmp:mainstocksundry
                        If cwp:quantity > tmp:mainstockquantity
                            !Quantity Required is more than main store quantity
                            If access:ordpend.primerecord() = Level:Benign
                                ope:part_ref_number     = tmp:mainstockrefnumber
                                ope:job_number          = job:ref_number
                                ope:part_type           = 'WAR'
                                ope:supplier            = wpr:supplier
                                ope:part_number         = wpr:part_number
                                If tmp:mainstockquantity = 0
                                    !If there is no stock in the main store
                                    ope:quantity    = cwp:quantity
                                    access:ordpend.insert()
                                    tmp:ordered = 1
                                    wpr:quantity    = cwp:quantity
                                    wpr:pending_ref_number  = ope:ref_number
                                    wpr:part_ref_number     = tmp:mainstockrefnumber
                                    access:warparts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('warranty')
                                    end
                                Else!If tmp:mainstockquantity = 0
                                    !take the remaining items from main store, and order the rest
                                    ope:quantity    = cwp:quantity - tmp:mainstockquantity
                                    access:ordpend.insert()
                                    tmp:ordered = 1
                                    wpr:quantity        = tmp:mainstockquantity
                                    wpr:date_ordered    = Today()
                                    wpr:part_ref_number = tmp:mainstockrefnumber
                                    access:warparts.insert()
                                    if def:PickNoteNormal or def:PickNoteMainStore then
                                        CreatePickingNote (sto:Location)
                                        InsertPickingPart ('warranty')
                                    end
                                    access:stock.clearkey(sto:ref_number_key)
                                    sto:ref_number      = tmp:mainstockrefnumber
                                    If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                        sto:quantity_stock = 0
                                        access:stock.update()
                                        If access:stohist.primerecord() = Level:Benign
                                            shi:quantity    = tmp:mainstockquantity
                                            If access:stohist.tryinsert()
                                                access:stohist.cancelautoinc()
                                            End!If access:stohist.tryinsert()
                                        End!If access:stohist.primerecord() = Level:Benign
                                    End!If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                    If access:warparts.primerecord() = Level:Benign
                                        Do AddPartDetails
                                        wpr:quantity            = cwp:quantity - tmp:mainstockquantity
                                        wpr:pending_ref_number  = ope:ref_number
                                        wpr:part_ref_number     = tmp:mainstockrefnumber
                                        If access:warparts.tryinsert()
                                            access:warparts.cancelautoinc()
                                        End!If access:warparts.tryinsert()
                                        if def:PickNoteNormal or def:PickNoteMainStore then
                                            CreatePickingNote (sto:Location)
                                            InsertPickingPart ('warranty')
                                        end
                                    End!If access:warparts.primerecord() = Level:Benign
                                End!If tmp:mainstockquantity = 0

                                ! Start Change 4652 BE(02/09/2004)
                                ! Status 330 Spares Requested
                                GetStatus(330,2,'JOB')
                                ! End Change 4652 BE(02/09/2004)

                            End!If access:ordpend.primerecord() = Level:Benign
                        Else!If cwp:quantity > tmp:mainstockquantity
                            !If there is enough quantity in the main store
                            wpr:quantity        = cwp:quantity
                            wpr:date_ordered    = Today()
                            wpr:part_ref_number = tmp:mainstockrefnumber
                            access:warparts.insert()
                            if def:PickNoteNormal or def:PickNoteMainStore then
                                CreatePickingNote (sto:Location)
                                InsertPickingPart ('warranty')
                            end
                            access:stock.clearkey(sto:ref_number_key)
                            If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                !Found
                                sto:quantity_stock -= cwp:quantity
                                If sto:quantity_stock < 0
                                    sto:quantity_stock = 0
                                End!If sto:quantity_stock < 0
                                access:stock.update()
                            Else! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                                !Error
                            End! If access:stock.tryfetch(sto:ref_number_key) = Level:Benign
                            If access:stohist.primerecord() = Level:Benign
                                shi:quantity    = cwp:quantity
                                Do AddStockHistory
                            End!If access:stohist.primerecord() = Level:Benign
                        End!If cwp:quantity > tmp:mainstockquantity
                    End!If tmp:mainstocksundry
                Else!If tmp:mainstockrefnumber <> 0
                    !If there is no main store item
                    If access:ordpend.primerecord() = Level:Benign
                        ope:part_ref_number     = ''
                        ope:job_number          = job:ref_number
                        ope:part_type           = 'WAR'
                        ope:supplier            = wpr:supplier
                        ope:part_number         = wpr:part_number
                        ope:quantity            = wpr:quantity
                        ope:Description         = wpr:Description
                        If access:ordpend.tryinsert()
                            access:ordpend.cancelautoinc()
                        End!If access:ordpend.tryinsert()
                        wpr:pending_ref_number  = ope:ref_number
                        access:warparts.insert()
                        if def:PickNoteNormal or def:PickNoteMainStore then
                            CreatePickingNote (sto:Location)
                            InsertPickingPart ('warranty')
                        end

                        ! Start Change 4652 BE(02/09/2004)
                        ! Status 330 Spares Requested
                        GetStatus(330,2,'JOB')
                        ! End Change 4652 BE(02/09/2004)

                    End!If access:ordpend.primerecord() = Level:Benign
                End!If tmp:mainstockrefnumber <> 0
            End!If tmp:engstockrefnumber <> 0
        End!If access:warparts.primerecord() = Level:Benign
    End !If Found# = 0
AddPartDetails      Routine
    wpr:ref_number           = job:ref_number
    wpr:adjustment           = cwp:adjustment
    wpr:part_number          = cwp:Part_Number
    wpr:description          = cwp:Description
    wpr:supplier             = cwp:Supplier
    wpr:purchase_cost        = cwp:Purchase_Cost
    wpr:sale_cost            = cwp:Sale_Cost
    
    wpr:fault_codes_checked  = 'YES'
    If wpr:fault_Code1 = ''
        wpr:Fault_Code1         = cwp:fault_Code1
    End!If wpr:FaultCode1           = ''            
    If wpr:Fault_Code2          = ''
        wpr:Fault_Code2         = cwp:fault_Code2
    End!If wpr:FaultCode2           = ''            
    If wpr:Fault_Code3          = ''
        wpr:Fault_Code3         = cwp:fault_Code3
    End!If wpr:FaultCode3           = ''            
    If wpr:Fault_Code4          = ''
        wpr:Fault_Code4         = cwp:fault_Code4
    End!If wpr:FaultCode4           = ''            
    If wpr:Fault_Code5          = ''
        wpr:Fault_Code5         = cwp:fault_Code5
    End!If wpr:FaultCode5           = ''            
    If wpr:Fault_Code6          = ''
        wpr:Fault_Code6         = cwp:fault_Code6
    End!If wpr:FaultCode6           = ''            
    If wpr:Fault_Code7          = ''
        wpr:Fault_Code7         = cwp:fault_Code7
    End!If wpr:FaultCode7           = ''            
    If wpr:Fault_Code8          = ''
        wpr:Fault_Code8         = cwp:fault_Code8
    End!If wpr:FaultCode8           = ''            
    If wpr:Fault_Code9          = ''
        wpr:Fault_Code9         = cwp:fault_Code9
    End!If wpr:FaultCode9           = ''            
    If wpr:Fault_Code10         = ''
        wpr:Fault_Code10        = cwp:fault_Code10
    End!If wpr:FaultCode10      = ''            
    If wpr:Fault_Code11         = ''
        wpr:Fault_Code11        = cwp:fault_Code11
    End!If wpr:FaultCode11      = ''            
    If wpr:Fault_Code12         = ''
        wpr:Fault_Code12        = cwp:fault_Code12
    End
    wpr:exclude_from_order   = cwp:exclude_from_order
AddStockHistory     Routine
    shi:ref_number           = sto:ref_number
    access:users.clearkey(use:password_key)
    use:password              = glo:password
    access:users.fetch(use:password_key)
    shi:user                  = use:user_code    
    shi:date                 = today()
    shi:transaction_type     = 'DEC'
    shi:despatch_note_number = wpr:despatch_note_number
    shi:job_number           = job:ref_number
    shi:purchase_cost        = wpr:purchase_cost
    shi:sale_cost            = wpr:sale_cost
    shi:retail_cost          = wpr:retail_cost
    shi:notes                = 'STOCK DECREMENTED'
    if access:stohist.insert()
       access:stohist.cancelautoinc()
    end
Local.PartAlreadyAttached       Procedure(Long func:RefNumber,String func:PartNumber)
Code
        Access:STOCK.Clearkey(sto:Ref_Number_Key)
        sto:Ref_Number  = func:RefNumber
        If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Found
            If sto:AllowDuplicate = 0
                Save_war_ali_ID = Access:WARPARTS_ALIAS.SaveFile()
                Access:WARPARTS_ALIAS.ClearKey(war_ali:Part_Number_Key)
                war_ali:Ref_Number  = job:Ref_Number
                war_ali:Part_Number = func:PartNumber
                Set(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
                Loop
                    If Access:WARPARTS_ALIAS.NEXT()
                       Break
                    End !If
                    If war_ali:Ref_Number  <> job:Ref_Number      |
                    Or war_ali:Part_Number <> func:PartNumber     |
                        Then Break.  ! End If
                    If war_ali:Date_Received = ''
                        Case MessageEx('Warranty Part ' & Clip(cwp:Part_Number) & ' is already attached to this job.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx

                        Return Level:Fatal
                        Break
                    End !If par:Record_Number <> wpr_ali:Record_Number
                End !Loop
                Access:WARPARTS_ALIAS.RestoreFile(Save_war_ali_ID)
            End !If sto:AllowDuplicate = 0
        Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'CommonFaultsWar',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Local',Local,'CommonFaultsWar',1)
    SolaceViewVars('tmp_RF_Board_IMEI',tmp_RF_Board_IMEI,'CommonFaultsWar',1)
    SolaceViewVars('tmp:engstockrefnumber',tmp:engstockrefnumber,'CommonFaultsWar',1)
    SolaceViewVars('tmp:engstockquantity',tmp:engstockquantity,'CommonFaultsWar',1)
    SolaceViewVars('tmp:mainstockrefnumber',tmp:mainstockrefnumber,'CommonFaultsWar',1)
    SolaceViewVars('tmp:mainstockquantity',tmp:mainstockquantity,'CommonFaultsWar',1)
    SolaceViewVars('tmp:engstocksundry',tmp:engstocksundry,'CommonFaultsWar',1)
    SolaceViewVars('tmp:mainstocksundry',tmp:mainstocksundry,'CommonFaultsWar',1)
    SolaceViewVars('tmp:mainstore',tmp:mainstore,'CommonFaultsWar',1)
    SolaceViewVars('tmp:ordered',tmp:ordered,'CommonFaultsWar',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'CommonFaultsWar',1)
    SolaceViewVars('save_loc_id',save_loc_id,'CommonFaultsWar',1)
    SolaceViewVars('save_war_ali_id',save_war_ali_id,'CommonFaultsWar',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
