

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01009.INC'),ONCE        !Local module procedure declarations
                     END


AccessoryCheck       PROCEDURE  (f_type)              ! Declare Procedure
save_lac_id          USHORT,AUTO
save_jac_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'AccessoryCheck')      !Add Procedure to Log
  end


    Case f_type
        OF 'LOAN'
            glo:Select1    = job:ref_number
            glo:Select2    = ''
            glo:Select12    = job:model_number
            Validate_Job_Accessories
            If globalresponse   <> 1
                Return(3)
            End!If globalresponse   = 2


            !Check For Missing Accessory
            save_lac_id    = access:loanacc.savefile()
            access:loanacc.clearkey(lac:ref_number_key)
            lac:ref_number    = job:loan_unit_number
            set(lac:ref_number_key,lac:ref_number_key)
            Loop
                If access:loanacc.next()
                    Break
                End!If access:jobacc.next()
                If lac:Ref_number <> job:loan_unit_number
                    Break
                End!If lac:Ref_number <> job:loan_unit_number

                Sort(glo:queue,glo:pointer)
                glo:pointer = lac:accessory
                Get(glo:queue,glo:pointer)
                If Error()
                    Case MessageEx('This unit has a missing accessory.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return(1)
                    Break
                End!If Error()
            End!Loop

            !Check For Mismatch

            Loop x# = 1 To Records(glo:queue)
                Get(glo:queue,x#)
                access:loanacc.clearkey(lac:ref_number_key)
                lac:ref_number  = job:loan_unit_number
                lac:accessory   = glo:pointer
                If access:loanacc.tryfetch(lac:ref_number_key)
                    Case MessageEx('There is a mismatch between the selected Accessories and the Accessories booked in with the unit.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return(2) !Mismatch Accessory
                End!If access:jobacc.tryfetch(jac:ref_number_key)
            End!Loop x# = 1 To Records(glo:queue)

        ELSE

            glo:Select1    = job:ref_number
            glo:Select2    = ''
            glo:Select12    = job:model_number
            Validate_Job_Accessories
            If globalresponse   <> 1
                Return(3)
            End!If globalresponse   = 2

            !Check For Missing Accessory
            save_jac_id    = access:jobacc.savefile()
            access:jobacc.clearkey(jac:ref_number_key)
            jac:ref_number    = job:ref_number
            set(jac:ref_number_key,jac:ref_number_key)
            Loop
                If access:jobacc.next()
                    Break
                End!If access:jobacc.next()
                If jac:ref_number    <> job:ref_number
                    Break
                End!If jac:ref_number    <> job:ref_number

                Sort(glo:queue,glo:pointer)
                glo:pointer = jac:accessory
                Get(glo:queue,glo:pointer)
                If Error()
                    Case MessageEx('This unit has a missing accessory.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return(1)
                    Break
                End!If Error()
            End!Loop

            !Check For Mismatch

            Loop x# = 1 To Records(glo:queue)
                Get(glo:queue,x#)
                access:jobacc.clearkey(jac:ref_number_key)
                jac:ref_number  = job:ref_number
                jac:accessory   = glo:pointer
                If access:jobacc.tryfetch(jac:ref_number_key)
                    Case MessageEx('There is a mismatch between the selected Accessories and the Accessories booked in with the unit.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    Return(2) !Mismatch Accessory

                End!If access:jobacc.tryfetch(jac:ref_number_key)
            End!Loop x# = 1 To Records(glo:queue)


    End!Case f_type
    Return Level:Benign



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AccessoryCheck',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_lac_id',save_lac_id,'AccessoryCheck',1)
    SolaceViewVars('save_jac_id',save_jac_id,'AccessoryCheck',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
