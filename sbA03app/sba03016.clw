

   MEMBER('sba03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA03016.INC'),ONCE        !Local module procedure declarations
                     END


PickEngineersSkillLevel PROCEDURE (func:SkillLevel)   !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:SkillLevel       LONG
BRW1::View:Browse    VIEW(USERS)
                       PROJECT(use:Surname)
                       PROJECT(use:Forename)
                       PROJECT(use:Team)
                       PROJECT(use:SkillLevel)
                       PROJECT(use:User_Code)
                       PROJECT(use:User_Type)
                       PROJECT(use:Active)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
use:Surname            LIKE(use:Surname)              !List box control field - type derived from field
use:Forename           LIKE(use:Forename)             !List box control field - type derived from field
use:Team               LIKE(use:Team)                 !List box control field - type derived from field
use:SkillLevel         LIKE(use:SkillLevel)           !List box control field - type derived from field
use:User_Code          LIKE(use:User_Code)            !Primary key field - type derived from field
use:User_Type          LIKE(use:User_Type)            !Browse key field - type derived from field
use:Active             LIKE(use:Active)               !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK5::use:Active           LIKE(use:Active)
HK5::use:User_Type        LIKE(use:User_Type)
! ---------------------------------------- Higher Keys --------------------------------------- !
QuickWindow          WINDOW('Browse the Engineers File'),AT(,,474,232),FONT('Tahoma',8,,),CENTER,IMM,HLP('Browse_Manufacturers'),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,36,372,188),USE(?Browse:1),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('120L(2)|M~Surname~@s30@80L(2)|M~Forename~@s30@120L(2)|M~Team~@s30@32L(2)|M~Skill' &|
   ' Level~@s8@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(396,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       SHEET,AT(4,4,384,224),USE(?CurrentTab),SPREAD
                         TAB('By Surname'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(use:Surname),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('Close'),AT(396,208,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?use:Surname{prop:ReadOnly} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 15066597
    Elsif ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 8454143
    Else ! If ?use:Surname{prop:Req} = True
        ?use:Surname{prop:FontColor} = 65793
        ?use:Surname{prop:Color} = 16777215
    End ! If ?use:Surname{prop:Req} = True
    ?use:Surname{prop:Trn} = 0
    ?use:Surname{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PickEngineersSkillLevel',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'PickEngineersSkillLevel',1)
    SolaceViewVars('FilesOpened',FilesOpened,'PickEngineersSkillLevel',1)
    SolaceViewVars('tmp:SkillLevel',tmp:SkillLevel,'PickEngineersSkillLevel',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?use:Surname;  SolaceCtrlName = '?use:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PickEngineersSkillLevel')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'PickEngineersSkillLevel')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:USERS.Open
  SELF.FilesOpened = True
  tmp:SkillLevel  = func:SkillLevel
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:USERS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,use:User_Type_Active_Key)
  BRW1.AddRange(use:Active)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?use:Surname,use:Surname,1,BRW1)
  BRW1.SetFilter('(use:SkillLevel >= tmp:SkillLevel)')
  BIND('tmp:SkillLevel',tmp:SkillLevel)
  BRW1.AddField(use:Surname,BRW1.Q.use:Surname)
  BRW1.AddField(use:Forename,BRW1.Q.use:Forename)
  BRW1.AddField(use:Team,BRW1.Q.use:Team)
  BRW1.AddField(use:SkillLevel,BRW1.Q.use:SkillLevel)
  BRW1.AddField(use:User_Code,BRW1.Q.use:User_Code)
  BRW1.AddField(use:User_Type,BRW1.Q.use:User_Type)
  BRW1.AddField(use:Active,BRW1.Q.use:Active)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:USERS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'PickEngineersSkillLevel',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'PickEngineersSkillLevel')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?use:Surname
      Select(?Browse:1)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  GET(SELF.Order.RangeList.List,1)
  Self.Order.RangeList.List.Right = 'ENGINEER'
  GET(SELF.Order.RangeList.List,2)
  Self.Order.RangeList.List.Right = 'YES'
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

