

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01023.INC'),ONCE        !Local module procedure declarations
                     END


ToBeLoaned           PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ToBeLoaned')      !Add Procedure to Log
  end


!Will this or does this job have an Loan unit on it?
!1 = Loan unit is already attached to the job
!2 = Loan unit is NOT attached, but the transit type is marked 'Loan Unit Required'
!3 = Loan unit is NOT attached, but the Loan Status is 103 Loan Unit Required
    If job:Loan_unit_number <> ''
    Compile('***',Debug=1)
        Message('Routine: ToBeLoaned||Returned: 1','Debug Message',icon:exclamation)
    ***
        Return 1
    End!If job:Loan_unit_number <> ''

    access:trantype.clearkey(trt:transit_type_key)
    trt:transit_type = job:transit_type
    if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
        If trt:Loan_unit = 'YES'
    Compile('***',Debug=1)
        Message('Routine: ToBeLoaned||Returned: 2','Debug Message',icon:exclamation)
    ***
            Return 2
        End!If trt:Loan = 'YES'
    End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign



    If Sub(job:Loan_status,1,3) = '103'
    Compile('***',Debug=1)
        Message('Routine: ToBeLoaned||Returned: 3','Debug Message',icon:exclamation)
    ***
        Return 3
    End!If Sub(job:Loan_status,1,3) = '108'
    Compile('***',Debug=1)
        Message('Routine: ToBeLoaned||Returned: Level:Benign','Debug Message',icon:exclamation)
    ***

    Return Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ToBeLoaned',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
