

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01022.INC'),ONCE        !Local module procedure declarations
                     END


ForceDespatch        PROCEDURE                        ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'ForceDespatch')      !Add Procedure to Log
  end


!Force a job to be marked as despatched
    job:Date_Despatched     = Today()
    job:Consignment_Number  = 'N/A'
    access:users.clearkey(use:Password_Key)
    use:Password        = glo:Password
    access:users.tryfetch(use:Password_Key)
    job:Despatch_User    = use:User_Code
    Get(desbatch,0)
    If access:desbatch.primerecord() = Level:Benign
        If access:desbatch.insert()
            access:desbatch.cancelautoinc()
        End!If access:desbatch.insert()
    End!If access:desbatch.primerecord() = Level:Benign
    job:Despatch_Number  = dbt:Batch_Number
    Compile('***',Debug=1)
        Message('Routine: Force Despatch','Debug Message',icon:exclamation)
    ***


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ForceDespatch',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
