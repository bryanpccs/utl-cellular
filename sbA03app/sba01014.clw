

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01014.INC'),ONCE        !Local module procedure declarations
                     END


Capitalize           PROCEDURE  (f_string)            ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Capitalize')      !Add Procedure to Log
  end


    f_string = CLIP(LEFT(f_string))

    STR_LEN#  = LEN(f_string)
    STR_POS#  = 1

    f_string = UPPER(SUB(f_string,STR_POS#,1)) & SUB(f_string,STR_POS#+1,STR_LEN#-1)

    LOOP STR_POS# = 1 TO STR_LEN#

     IF SUB(f_string,STR_POS#,1) = ' ' OR SUB(f_string,STR_POS#,1) = '-'  |
        OR SUB(f_string,STR_POS#,1) = '.' OR SUB(f_string,STR_POS#,1) = '/' |
        OR SUB(f_string,STR_POS#,1) = '&' OR SUB(f_string,STR_POS#,1) = '(' OR SUB(f_string,STR_POS#,1) = CHR(39)
        f_string = SUB(f_string,1,STR_POS#) & UPPER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)
     ELSE
        f_string = SUB(f_string,1,STR_POS#) & LOWER(SUB(f_string,STR_POS#+1,1)) & SUB(f_string,STR_POS#+2,STR_LEN#-1)

     .
    .

    RETURN(f_string)



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Capitalize',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
