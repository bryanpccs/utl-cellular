

   MEMBER('sba03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA03013.INC'),ONCE        !Local module procedure declarations
                     END


PickRepairTypes PROCEDURE (func:ModelNumber,func:AccountNumber,func:ChargeType,func:UnitType,func:Type) !Generated from procedure template - Window

CurrentTab           STRING(80)
save_rep_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:RepairType       STRING(30)
save_rept_id         USHORT,AUTO
tmp:AddToList        BYTE(0)
tmp:Cost             REAL
tmp:ModelNumber      STRING(30)
tmp:ShowHide1        BYTE(0)
tmp:ShowHide2        BYTE(0)
tmp:TradeAccountNumber STRING(30)
tmp:CheckHeadTrade   BYTE(0)
TradeTypesQueue      QUEUE,PRE(traque)
RepairType           STRING(30)
Cost                 REAL
                     END
ChaTypesQueue        QUEUE,PRE(chaque)
RepairType           STRING(30)
Cost                 REAL
                     END
WarTypesQueue        QUEUE,PRE(warque)
RepairType           STRING(30)
Cost                 REAL
                     END
QuickWindow          WINDOW('Browse Repair Types'),AT(,,247,188),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('PickRepairTypes'),SYSTEM,GRAY,DOUBLE
                       BUTTON('&Select'),AT(168,20,76,20),USE(?Select),LEFT,ICON('select.ico')
                       SHEET,AT(4,4,156,180),USE(?CurrentTab),SPREAD
                         TAB('Trade Repair Types'),USE(?tab1),HIDE
                           LIST,AT(8,24,148,156),USE(?TraList),ALRT(MouseLeft2),ALRT(EnterKey),FORMAT('120L(2)|M~Repair Type~@s30@56R(2)|M~Cost~@n14.2@'),FROM(TradeTypesQueue)
                         END
                         TAB('All Repair Types'),USE(?Tab2),HIDE
                           LIST,AT(8,24,148,156),USE(?ChaList),ALRT(MouseLeft2),ALRT(EnterKey),FORMAT('120L(2)|M~Repair Type~@s30@56R(2)|M~Cost~L@n14.2@'),FROM(ChaTypesQueue)
                         END
                         TAB('All Repair Types'),USE(?Tab3),HIDE
                           LIST,AT(8,24,148,156),USE(?WarList),ALRT(MouseLeft2),ALRT(EnterKey),FORMAT('120L(2)|M~Repair Type~@s30@56R(2)|M~Cost~L@n14.2@'),FROM(WarTypesQueue)
                         END
                       END
                       BUTTON('Close'),AT(168,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()
  RETURN(tmp:RepairType)

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?tab1{prop:Color} = 15066597
    ?TraList{prop:FontColor} = 65793
    ?TraList{prop:Color}= 16777215
    ?TraList{prop:Color,2} = 16777215
    ?TraList{prop:Color,3} = 12937777
    ?Tab2{prop:Color} = 15066597
    ?ChaList{prop:FontColor} = 65793
    ?ChaList{prop:Color}= 16777215
    ?ChaList{prop:Color,2} = 16777215
    ?ChaList{prop:Color,3} = 12937777
    ?Tab3{prop:Color} = 15066597
    ?WarList{prop:FontColor} = 65793
    ?WarList{prop:Color}= 16777215
    ?WarList{prop:Color,2} = 16777215
    ?WarList{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PickRepairTypes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'PickRepairTypes',1)
    SolaceViewVars('save_rep_id',save_rep_id,'PickRepairTypes',1)
    SolaceViewVars('tmp:RepairType',tmp:RepairType,'PickRepairTypes',1)
    SolaceViewVars('save_rept_id',save_rept_id,'PickRepairTypes',1)
    SolaceViewVars('tmp:AddToList',tmp:AddToList,'PickRepairTypes',1)
    SolaceViewVars('tmp:Cost',tmp:Cost,'PickRepairTypes',1)
    SolaceViewVars('tmp:ModelNumber',tmp:ModelNumber,'PickRepairTypes',1)
    SolaceViewVars('tmp:ShowHide1',tmp:ShowHide1,'PickRepairTypes',1)
    SolaceViewVars('tmp:ShowHide2',tmp:ShowHide2,'PickRepairTypes',1)
    SolaceViewVars('tmp:TradeAccountNumber',tmp:TradeAccountNumber,'PickRepairTypes',1)
    SolaceViewVars('tmp:CheckHeadTrade',tmp:CheckHeadTrade,'PickRepairTypes',1)
    SolaceViewVars('TradeTypesQueue:RepairType',TradeTypesQueue:RepairType,'PickRepairTypes',1)
    SolaceViewVars('TradeTypesQueue:Cost',TradeTypesQueue:Cost,'PickRepairTypes',1)
    SolaceViewVars('ChaTypesQueue:RepairType',ChaTypesQueue:RepairType,'PickRepairTypes',1)
    SolaceViewVars('ChaTypesQueue:Cost',ChaTypesQueue:Cost,'PickRepairTypes',1)
    SolaceViewVars('WarTypesQueue:RepairType',WarTypesQueue:RepairType,'PickRepairTypes',1)
    SolaceViewVars('WarTypesQueue:Cost',WarTypesQueue:Cost,'PickRepairTypes',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Select;  SolaceCtrlName = '?Select';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tab1;  SolaceCtrlName = '?tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TraList;  SolaceCtrlName = '?TraList';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ChaList;  SolaceCtrlName = '?ChaList';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?WarList;  SolaceCtrlName = '?WarList';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PickRepairTypes')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'PickRepairTypes')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Select
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:REPAIRTY.Open
  Relate:REPTYDEF.Open
  Access:SUBCHRGE.UseFile
  Access:TRACHRGE.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:STDCHRGE.UseFile
  SELF.FilesOpened = True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  
  Clear(TradeTypesQueue)
  Free(TradeTypesQueue)
  Clear(ChaTypesQueue)
  Free(ChaTypesQueue)
  Clear(WarTypesQueue)
  Free(WarTypesQueue)
  
  Save_rep_ID = Access:REPAIRTY.SaveFile()
  Access:REPAIRTY.ClearKey(rep:Model_Number_Key)
  rep:Model_Number = func:ModelNumber
  Set(rep:Model_Number_Key,rep:Model_Number_Key)
  Loop
      If Access:REPAIRTY.NEXT()
         Break
      End !If
      If rep:Model_Number <> func:ModelNumber      |
          Then Break.  ! End If
  
      If func:Type = 'C'
          If rep:Chargeable <> 'YES'
              Cycle
          End !If rep:Chargeable <> 'YES'
      End !If func:Type = 'C'
  
      If func:Type = 'W'
          If rep:Warranty <> 'YES'
              Cycle
          End !If rep:Warranty <> 'YES'
      End !If func:Type = 'W'
  
  !Not needed yet
  !    If func:BER
  !        Access:REPTYDEF.ClearKey(rtd:Repair_Type_Key)
  !        rtd:Repair_Type = rep:Repair_Type
  !        If Access:REPTYDEF.TryFetch(rtd:Repair_Type_Key) = Level:Benign
  !            !Found
  !            If rtd:BER
  !                Cycle
  !            End !If rtd:BER
  !        Else!If Access:REPTYDEF.TryFetch(rtd:Repair_Type_Key) = Level:Benign
  !            !Error
  !            !Assert(0,'<13,10>Fetch Error<13,10>')
  !        End!If Access:REPTYDEF.TryFetch(rtd:Repair_Type_Key) = Level:Benign
  !    End !If func:BER
  
      tmp:AddToList = 0
      tmp:Cost = 0
  
      tmp:TradeAccountNumber  = func:AccountNumber
      tmp:CheckHeadTrade = 0
      Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
      sub:Account_Number  = func:AccountNumber
      If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
          !Found
          Access:TRADEACC.Clearkey(tra:Account_Number_Key)
          tra:Account_Number  = sub:Main_Account_Number
          If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Found
              If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                  Access:SUBCHRGE.ClearKey(suc:Model_Repair_Type_Key)
                  suc:Account_Number = func:AccountNumber
                  suc:Model_Number   = func:ModelNumber
                  suc:Charge_Type    = func:ChargeType
                  suc:Unit_Type      = func:UnitType
                  suc:Repair_Type    = rep:Repair_type
                  If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
                      !Found
                      tmp:AddToList   = 1
                      tmp:Cost        = suc:Cost
                  Else
                      tmp:CheckHeadTrade = 1
                  End!If Access:SUBCHRGE.TryFetch(suc:Model_Repair_Type_Key) = Level:Benign
              Else !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
                  tmp:CheckHeadTrade = 1
              End !If tra:Invoice_Sub_Accounts = 'YES' and tra:Use_Sub_Accounts = 'YES'
              If tmp:CheckHeadTrade
                  Access:TRACHRGE.ClearKey(trc:Account_Charge_Key)
                  trc:Account_Number = tra:Account_Number
                  trc:Model_Number   = func:ModelNumber
                  trc:Charge_Type    = func:ChargeType
                  trc:Unit_Type      = func:UnitType
                  trc:Repair_Type    = rep:Repair_Type
                  If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                      !Found
                      tmp:AddToList   = 1
                      tmp:Cost        = trc:Cost
                  Else!If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End!If Access:TRACHRGE.TryFetch(trc:Account_Charge_Key) = Level:Benign
              End !If tmp:CheckHeadTrade
  
          Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          
      Else! If Access:SUBTRACC.Tryfetch(sub:AccountNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUBTRACC.Tryfetch(sub:AccountNumberKey) = Level:Benign
  
  
      If tmp:AddToList
          Sort(TradeTypesQueue,traque:RepairType)
          traque:RepairType   = rep:Repair_Type
          Get(TradeTypesQueue,traque:RepairType)
          If Error()
              traque:RepairType   = rep:Repair_Type
  
              If def:ShowRepTypeCosts
                  traque:Cost       = tmp:Cost
              Else !If def:ShowRepTypeCosts
                  traque:Cost       = ''
              End !If def:ShowRepTypeCosts
              Add(TradeTypesQueue)
  
          End !If Error()
  
          Case func:Type
              Of 'C'
                  Sort(ChaTypesQueue,chaque:RepairType)
                  chaque:RepairType   = rep:Repair_Type
                  Get(ChaTypesQueue,chaque:RepairType)
                  If Error()
                      If def:ShowRepTypeCosts
                          chaque:Cost = tmp:Cost
                      Else !If def:ShowRepTypeCosts
                          chaque:Cost = ''
                      End !If def:ShowRepTypeCosts
                      chaque:RepairType   = rep:Repair_Type
                      Add(ChaTypesQueue)
                  End !If Error()
              Of 'W'
                  Sort(WarTypesQueue,warque:RepairType)
                  warque:RepairType   = rep:Repair_Type
                  Get(WarTypesQueue,warque:RepairType)
                  If Error()
                      If def:ShowRepTypeCosts
                          warque:Cost = tmp:Cost
                      Else !If def:ShowRepTypeCosts
                          chaque:Cost = ''
                      End !If def:ShowRepTypeCosts
                      warque:RepairType   = rep:Repair_Type
                      Add(WarTypesQueue)
                  End !If Error()
          End !Case func:Type
  
      Else!If tmp:AddToList
          Access:STDCHRGE.ClearKey(sta:Model_Number_Charge_Key)
          sta:Model_Number = func:ModelNumber
          sta:Charge_Type  = func:ChargeType
          sta:Unit_Type    = func:UnitType
          sta:Repair_Type  = rep:Repair_Type
          If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
              !Found
              Case func:Type
                  Of 'C'
                      Sort(ChaTypesQueue,chaque:RepairType)
                      chaque:RepairType   = rep:Repair_Type
                      Get(ChaTypesQueue,chaque:RepairType)
                      If Error()
                          If def:ShowRepTypeCosts
                              chaque:Cost = sta:Cost
                          Else !If def:ShowRepTypeCosts
                              chaque:Cost = ''
                          End !If def:ShowRepTypeCosts
                          chaque:RepairType   = rep:Repair_Type
                          Add(ChaTypesQueue)
                      End !If Error()
                  Of 'W'
                      Sort(WarTypesQueue,warque:RepairType)
                      warque:RepairType   = rep:Repair_Type
                      Get(WarTypesQueue,warque:RepairType)
                      If Error()
                          If def:ShowRepTypeCosts
                              warque:Cost = sta:Cost
                          Else !If def:ShowRepTypeCosts
                              warque:Cost = ''
                          End !If def:ShowRepTypeCosts
                          warque:RepairType   = rep:Repair_Type
                          Add(WarTypesQueue)
                      End !If Error()
              End !Case func:Type
  
          Else!If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:STDCHRGE.TryFetch(sta:Model_Number_Charge_Key) = Level:Benign
  
      End !If tmp:AddToList
  End !Loop
  Access:REPAIRTY.RestoreFile(Save_rep_ID)
  
  Sort(TradeTypesQueue,traque:RepairType)
  Sort(ChaTypesQueue,chaque:RepairType)
  Sort(WarTypesQueue,warque:RepairType)
  
  OPEN(QuickWindow)
  SELF.Opened=True
  Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
  sub:Account_Number  = func:AccountNumber
  If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Found
      Access:TRADEACC.Clearkey(tra:Account_Number_Key)
      tra:Account_Number  = sub:Main_Account_Number
      If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Found
          If tra:ShowRepairTypes
              Unhide(?Tab1)
          Else !If tra:ShowRepairTypes
              Hide(?Tab1)
          End !If tra:ShowRepairTypes
      Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
      
  Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
  End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
  
  
  Case func:Type
      Of 'W'
          Unhide(?Tab3)
      Of 'C'
          Unhide(?Tab2)
  End !func:Type
  Do RecolourWindow
  ?TraList{prop:vcr} = TRUE
  ?ChaList{prop:vcr} = TRUE
  ?WarList{prop:vcr} = TRUE
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:REPAIRTY.Close
    Relate:REPTYDEF.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'PickRepairTypes',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Select
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
      Case Choice(?CurrentTab)
          Of 1
              Get(TradeTypesQueue,Choice(?TraList))
              If ~Error()
                  tmp:RepairType  = traque:RepairType
                  Post(Event:CloseWindow)
              End !If ~Error()
              
          Of 2
              Get(ChaTypesQueue,Choice(?ChaList))
              If ~Error()
                  tmp:RepairType = chaque:RepairType
                  Post(Event:CloseWindow)
              End !If ~Error()
              
          Of 3
              Get(WarTypesQueue,Choice(?WarList))
              If ~Error()
                  tmp:RepairType = warque:RepairType
                  Post(Event:CloseWindow)
              End !If ~Error()
      
      End !Choice(?CurrentTab)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Select, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'PickRepairTypes')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?TraList
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TraList, AlertKey)
      Post(Event:Accepted,?Select)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?TraList, AlertKey)
    END
  OF ?ChaList
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChaList, AlertKey)
      Post(Event:Accepted,?Select)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ChaList, AlertKey)
    END
  OF ?WarList
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarList, AlertKey)
      Post(Event:Accepted,?Select)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?WarList, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

