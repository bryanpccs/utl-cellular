

   MEMBER('sba03app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA03025.INC'),ONCE        !Local module procedure declarations
                     END


PickSubAddresses PROCEDURE (func:RefNumber)           !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
main_account_temp    STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
RefNumber            LONG
BRW1::View:Browse    VIEW(SUBACCAD)
                       PROJECT(sua:AccountNumber)
                       PROJECT(sua:CompanyName)
                       PROJECT(sua:Postcode)
                       PROJECT(sua:RecordNumber)
                       PROJECT(sua:RefNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sua:AccountNumber      LIKE(sua:AccountNumber)        !List box control field - type derived from field
sua:CompanyName        LIKE(sua:CompanyName)          !List box control field - type derived from field
sua:Postcode           LIKE(sua:Postcode)             !List box control field - type derived from field
sua:RecordNumber       LIKE(sua:RecordNumber)         !Primary key field - type derived from field
sua:RefNumber          LIKE(sua:RefNumber)            !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Sub Account Addresses File'),AT(,,378,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Sub_Accounts'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,280,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),ALRT(InsertKey),FORMAT('69L(2)|M~Account Number~@s15@120L(2)|M~Company Name~@s30@60L(2)|M~Postcode~@s15@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(300,20,76,20),USE(?Select:2),LEFT,ICON('select.ico')
                       SHEET,AT(4,4,288,180),USE(?CurrentTab),SPREAD
                         TAB('By Account Number'),USE(?Tab:4)
                           ENTRY(@s15),AT(8,20,64,10),USE(sua:AccountNumber),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Company Name'),USE(?Tab3)
                           ENTRY(@s30),AT(8,20,124,10),USE(sua:CompanyName),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       BUTTON('Close'),AT(300,164,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       BUTTON('DELETE THIS'),AT(276,200,76,20),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:4{prop:Color} = 15066597
    If ?sua:AccountNumber{prop:ReadOnly} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 15066597
    Elsif ?sua:AccountNumber{prop:Req} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 8454143
    Else ! If ?sua:AccountNumber{prop:Req} = True
        ?sua:AccountNumber{prop:FontColor} = 65793
        ?sua:AccountNumber{prop:Color} = 16777215
    End ! If ?sua:AccountNumber{prop:Req} = True
    ?sua:AccountNumber{prop:Trn} = 0
    ?sua:AccountNumber{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?sua:CompanyName{prop:ReadOnly} = True
        ?sua:CompanyName{prop:FontColor} = 65793
        ?sua:CompanyName{prop:Color} = 15066597
    Elsif ?sua:CompanyName{prop:Req} = True
        ?sua:CompanyName{prop:FontColor} = 65793
        ?sua:CompanyName{prop:Color} = 8454143
    Else ! If ?sua:CompanyName{prop:Req} = True
        ?sua:CompanyName{prop:FontColor} = 65793
        ?sua:CompanyName{prop:Color} = 16777215
    End ! If ?sua:CompanyName{prop:Req} = True
    ?sua:CompanyName{prop:Trn} = 0
    ?sua:CompanyName{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674






ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('PickSubAddresses')
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:SUBACCAD.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Access:TRADEACC.UseFile
  Access:USERS.UseFile
  SELF.FilesOpened = True
  RefNumber   = func:RefNumber
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SUBACCAD,SELF)
  Brw1.SelectWholeRecord=True
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sua:CompanyNameKey)
  BRW1.AddRange(sua:RefNumber,RefNumber)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?sua:CompanyName,sua:CompanyName,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sua:AccountNumberKey)
  BRW1.AddRange(sua:RefNumber,RefNumber)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sua:AccountNumber,sua:AccountNumber,1,BRW1)
  BRW1.AddField(sua:AccountNumber,BRW1.Q.sua:AccountNumber)
  BRW1.AddField(sua:CompanyName,BRW1.Q.sua:CompanyName)
  BRW1.AddField(sua:Postcode,BRW1.Q.sua:Postcode)
  BRW1.AddField(sua:RecordNumber,BRW1.Q.sua:RecordNumber)
  BRW1.AddField(sua:RefNumber,BRW1.Q.sua:RefNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:4{PROP:TEXT} = 'By Account Number'
    ?Tab3{PROP:TEXT} = 'By Company Name'
    ?Browse:1{PROP:FORMAT} ='69L(2)|M~Account Number~@s15@#1#120L(2)|M~Company Name~@s30@#2#60L(2)|M~Postcode~@s15@#3#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:SUBACCAD.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
  END
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='69L(2)|M~Account Number~@s15@#1#120L(2)|M~Company Name~@s30@#2#60L(2)|M~Postcode~@s15@#3#'
          ?Tab:4{PROP:TEXT} = 'By Account Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='120L(2)|M~Company Name~@s30@#2#69L(2)|M~Account Number~@s15@#1#60L(2)|M~Postcode~@s15@#3#'
          ?Tab3{PROP:TEXT} = 'By Company Name'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

