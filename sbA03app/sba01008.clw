

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01008.INC'),ONCE        !Local module procedure declarations
                     END


SecurityCheck        PROCEDURE  (f_area)              ! Declare Procedure
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'SecurityCheck')      !Add Procedure to Log
  end


! security check
    If FullAccess(glo:PassAccount,glo:Password) = Level:Benign
        Return Level:Benign
    Else!If password = 'JOB:ENTER'
        access:users_alias.clearkey(use_ali:password_key)
        use_ali:password    = glo:password
        If access:users_alias.fetch(use_ali:password_key) = Level:Benign
            access:accareas_alias.clearkey(acc_ali:access_level_key)
            acc_ali:user_level = use_ali:user_level
            acc_ali:access_area = f_area
            If access:accareas_alias.fetch(acc_ali:access_level_key) = Level:Benign
                Return Level:Benign
            End
        End
    End!If password = 'JOB:ENTER'
    Return Level:Fatal


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'SecurityCheck',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
