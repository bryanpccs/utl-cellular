

   MEMBER('sba01app.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('SBA01032.INC'),ONCE        !Local module procedure declarations
                     END


PricingRoutine       PROCEDURE  (F_Type)              ! Declare Procedure
save_par_id          USHORT,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_wpr_id          USHORT,AUTO
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'PricingRoutine')      !Add Procedure to Log
  end


    !Return Values
    !1  = No account
    !2  = Ignore Pricing Structures
    !3  = Pricing Error

    Case f_type
        Of 'C'
            If job:account_number = ''
                job:parts_cost = 0
                job:labour_cost = 0
                Return 1    !Cannot run pricing structure
            End!If job:account_number = ''

            If job:chargeable_job = 'YES'
                If job:ignore_chargeable_charges = 'YES'
                    Return 2    !Charges ignored
                End!If job:ignore_chargeable_charges = 'YES'

                access:chartype.clearkey(cha:charge_type_key)
                cha:charge_type = job:charge_type
                If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
                    !Found
                    If cha:no_charge = 'YES'
                        job:labour_cost = 0
                        job:parts_cost  = 0
                    Else!If cha:no_charge = 'YES'
                        If cha:zero_parts = 'YES'
                            job:parts_cost = 0
                        Else!If cha:zero_parts = 'YES''
                            save_par_id = access:parts.savefile()
                            access:parts.clearkey(par:part_number_key)
                            par:ref_number  = job:ref_number
                            set(par:part_number_key,par:part_number_key)
                            loop
                                if access:parts.next()
                                   break
                                end !if
                                if par:ref_number  <> job:ref_number      |
                                    then break.  ! end if
                                job:parts_cost  += par:sale_cost * par:quantity
                            end !loop
                            access:parts.restorefile(save_par_id)
                        End!If cha:zero_parts = 'YES''
                    End!If cha:no_charge = 'YES'

                    access:subtracc.clearkey(sub:account_number_key)
                    sub:account_number  = job:account_number
                    If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                        !Found
                        access:tradeacc.clearkey(tra:account_number_key)
                        tra:account_number  = sub:main_account_number
                        If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                            !Found

                        Else! If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                            !Error
                            Case MessageEx('Cannot find the Main Account '&Clip(job:account_number)&' attached to Job '&clip(job:ref_number)&'.','ServiceBase 2000',|
                                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            job:parts_cost = 0
                            job:labour_cost = 0

                            Return 1

                        End! If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
                        
                    Else! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
                        !Error
                        Case MessageEx('Cannot find the Sub Account '&Clip(job:account_number)&' attached to Job '&clip(job:ref_number)&'.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        job:parts_cost = 0
                        job:labour_cost = 0

                        Return 1
                    End! If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign

                    If tra:ZeroChargeable   = 'YES'
                        job:parts_cost   = 0
                    End!If tra:ZeroChargeable   = 'YES'

                    If tra:Invoice_Sub_accounts = 'YES'
                        !try and find a sub account charge structure
                        access:subchrge.clearkey(suc:model_repair_type_key)
                        suc:account_number = job:account_number
                        suc:model_number   = job:model_number
                        suc:charge_type    = job:charge_type
                        suc:unit_type      = job:unit_type
                        suc:repair_type    = job:repair_type
                        if access:subchrge.tryfetch(suc:model_repair_type_key) = Level:Benign
                            job:labour_cost  = suc:cost
                        Else!if access:subchrge.tryfetch(suc:model_repair_type_key) = Level:Benign
                            !Can't find, try the main account charge structure
                            access:trachrge.clearkey(trc:account_charge_key)
                            trc:account_number = job:account_number
                            trc:model_number   = job:model_number
                            trc:charge_type    = job:charge_type
                            trc:unit_type      = job:unit_type
                            trc:repair_type    = job:repair_type
                            if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                                job:labour_cost  = trc:cost
                            Else!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                                !Can't find structure, try the Default Charges
                                access:stdchrge.clearkey(sta:model_number_charge_key)
                                sta:model_number = job:model_number
                                sta:charge_type  = job:charge_type
                                sta:unit_type    = job:unit_type
                                sta:repair_type  = job:repair_type
                                if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                    job:labour_cost  = sta:cost
                                Else!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                    !Can't find default structure
                                    job:parts_cost = 0
                                    job:labour_cost = 0

                                    Return 3
                                End!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                            End!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                        End!if access:subchrge.tryfetch(suc:model_repair_type_key) = Level:Benign
                    Else!If tra:Invoice_Sub_accounts = 'YES'
                        !Try and find a Trade Charge Structure
                        access:trachrge.clearkey(trc:account_charge_key)
                        trc:account_number = sub:main_account_number
                        trc:model_number   = job:model_number
                        trc:charge_type    = job:charge_type
                        trc:unit_type      = job:unit_type
                        trc:repair_type    = job:repair_type
                        if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                            job:labour_cost  = trc:cost
                        Else!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign
                            !Can't find structure, try the Default Charges
                            access:stdchrge.clearkey(sta:model_number_charge_key)
                            sta:model_number = job:model_number
                            sta:charge_type  = job:charge_type
                            sta:unit_type    = job:unit_type
                            sta:repair_type  = job:repair_type
                            if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                job:labour_cost  = sta:cost
                            Else!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                                !Can't find default structure
                                job:parts_cost = 0
                                job:labour_cost = 0

                                Return 3
                            End!if access:stdchrge.tryfetch(sta:model_number_charge_key) = Level:Benign
                        End!if access:trachrge.tryfetch(trc:account_charge_key) = Level:Benign

                    End!If tra:Invoice_Sub_accounts = 'YES'
                Else! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
                    !Error
                    job:parts_cost = 0
                    job:labour_cost = 0
                    Return 3    !Error
                End! If access:chartype.tryfetch(cha:charge_type_key) = Level:Benign
            End!If job:chargeble_job = 'YES'
        Of 'W'
    End!Case f_type


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'PricingRoutine',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_par_id',save_par_id,'PricingRoutine',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'PricingRoutine',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
