

   MEMBER('celrapqa.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA001.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA002.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA005.INC'),ONCE        !Req'd for module callout resolution
                     END


JB:ComputerName CSTRING(255)
JB:ComputerLen  ULONG(255)
Update_QA PROCEDURE (f_type)                          !Generated from procedure template - Window

FilesOpened          BYTE
Is14DayRepackage     BYTE
IniFilepath          STRING(255)
bookmark             USHORT
RepackageDefaults    GROUP,PRE(RPG)
OriginalAccount1     STRING(15)
OriginalAccount2     STRING(15)
OriginalChargeType   STRING(30)
NewAccount           STRING(15)
NewChargeType        STRING(30)
PrintJobLabel        BYTE
                     END
JobsengRec           GROUP,PRE(JER)
JobNumber            LONG
UserCode             STRING(3)
DateAllocated        DATE
TimeAllocated        STRING(20)
AllocatedBy          STRING(3)
EngSkillLevel        LONG
JobSkillLevel        STRING(30)
                     END
JobexhisRec          GROUP,PRE(JXR)
Ref_Number           REAL
Loan_Unit_Number     REAL
Date                 DATE
Time                 TIME
User                 STRING(3)
Status               STRING(60)
                     END
JoblohisRec          GROUP,PRE(JLR)
Ref_Number           REAL
Loan_Unit_Number     REAL
Date                 DATE
Time                 TIME
User                 STRING(3)
Status               STRING(60)
                     END
JobstageRec          GROUP,PRE(JSR)
Ref_Number           REAL
Job_Stage            STRING(30)
Date                 DATE
Time                 TIME
User                 STRING(3)
                     END
JobaccRec            GROUP,PRE(JAR)
Ref_Number           REAL
Accessory            STRING(30)
                     END
AuditRec             GROUP,PRE(AIR)
Ref_Number           REAL
Date                 DATE
Time                 TIME
User                 STRING(3)
Action               STRING(80)
DummyField           STRING(2)
Notes                STRING(255)
Type                 STRING('''JOB''')
                     END
AudstatsRec          GROUP,PRE(ASR)
RefNumber            LONG
Type                 STRING(3)
DateChanged          DATE
TimeChanged          STRING(20)
OldStatus            STRING(30)
NewStatus            STRING(30)
UserCode             STRING(3)
                     END
JobseRec             GROUP,PRE(JSR)
RefNumber            LONG
JobMark              BYTE(0)
TraFaultCode1        STRING(30)
TraFaultCode2        STRING(30)
TraFaultCode3        STRING(30)
TraFaultCode4        STRING(30)
TraFaultCode5        STRING(30)
TraFaultCode6        STRING(30)
TraFaultCode7        STRING(30)
TraFaultCode8        STRING(30)
TraFaultCode9        STRING(30)
TraFaultCode10       STRING(30)
TraFaultCode11       STRING(30)
TraFaultCode12       STRING(30)
SIMNumber            STRING(30)
JobReceived          BYTE(0)
SkillLevel           LONG
UPSFlagCode          STRING(1)
FailedDelivery       BYTE(0)
CConfirmSecondEntry  STRING('0 {2}')
WConfirmSecondEntry  STRING(3)
EndUserEmailAddress  STRING(255)
Network              STRING(30)
ExchangeReason       STRING(255)
LoanReason           STRING(255)
InWorkshopDate       DATE
InWorkshopTime       TIME
Pre_RF_Board_IMEI    STRING(20)
                     END
JobnotesRec          GROUP,PRE(JNR)
RefNumber            LONG
Fault_Description    STRING(255)
Engineers_Notes      STRING(255)
Invoice_Text         STRING(255)
Collection_Text      STRING(255)
Delivery_Text        STRING(255)
ColContactName       STRING(30)
ColDepartment        STRING(30)
DelContactName       STRING(30)
DelDepartment        STRING(30)
                     END
Local                CLASS
ValidateParts        PROCEDURE (),Byte
ValidateNetwork      PROCEDURE (),Byte
                     END
date_error_Temp      STRING(30)
Date_Completed_Temp  DATE
Time_Completed_Temp  STRING(20)
Check_For_Bouncers_Temp STRING(30)
Print_Despatch_Note_Temp STRING(30)
Saved_Ref_Number_Temp LONG
Saved_Esn_Temp       STRING(30)
Saved_MSN_Temp       STRING(30)
tmp:DespatchClose    BYTE(0)
tmp:RestockingNote   BYTE(0)
save_wpr_id          USHORT,AUTO
save_qap_id          USHORT,AUTO
save_par_id          USHORT,AUTO
save_xch_id          USHORT,AUTO
save_lac_id          USHORT,AUTO
save_aud_id          USHORT
job_queue_temp       QUEUE,PRE(jobque)
job_number           LONG
type                 STRING(7)
record_number        LONG
                     END
error_temp           BYTE(1)
Job_Number_Temp      REAL
serial_number_temp   STRING(20)
model_details_temp   STRING(60)
handset_type_temp    STRING(30)
date_booked_temp     DATE
save_jac_id          USHORT,AUTO
update_text_temp     STRING(100)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ConsignNo        STRING(30)
tmp:AccountNumber    STRING(30)
tmp:OldConsignno     STRING(30)
sav:Path             STRING(255)
tmp:LabelError       STRING(30)
account_number2_temp STRING(30)
save_cou_ali_id      USHORT,AUTO
save_job_ali_id      USHORT,AUTO
tmp:ParcellineName   STRING(255),STATIC
tmp:WorkstationName  STRING(30)
window               WINDOW('Rapid QA Update'),AT(,,359,150),FONT('Tahoma',8,,),CENTER,ALRT(F5Key),ALRT(F9Key),ALRT(F10Key),GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,224,116),USE(?Sheet1),SPREAD
                         TAB('Rapid QA Update'),USE(?Tab1)
                           PROMPT('Job Number'),AT(8,20),USE(?Job_Number_Temp:Prompt),TRN
                           ENTRY(@s9),AT(84,20,64,10),USE(Job_Number_Temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Unit Serial Number'),AT(8,36),USE(?serial_number_temp:Prompt),TRN
                           ENTRY(@s20),AT(84,36,124,10),USE(serial_number_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           GROUP('Unit Details'),AT(8,48,216,52),USE(?Group1),BOXED,COLOR(COLOR:Silver)
                             PROMPT('Model:'),AT(16,60),USE(?Prompt3)
                             STRING(@s60),AT(84,60),USE(model_details_temp),FONT(,,,FONT:bold)
                             PROMPT('Handset Type:'),AT(16,72),USE(?Prompt4)
                             STRING(@s30),AT(84,72,124,12),USE(handset_type_temp),FONT(,,,FONT:bold)
                             PROMPT('Date Booked:'),AT(16,84),USE(?Prompt5)
                             STRING(@d6b),AT(84,84),USE(date_booked_temp),FONT(,8,,FONT:bold)
                           END
                           STRING(@s100),AT(8,104,216,12),USE(update_text_temp),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(232,4,124,116),USE(?Sheet2),SPREAD
                         TAB('Jobs Successfully Updated'),USE(?Tab2)
                           LIST,AT(236,20,116,96),USE(?List1),VSCROLL,FORMAT('56L(2)|M~Job Number~@s8@28L(2)|M~Type~@s7@'),FROM(job_queue_temp)
                         END
                       END
                       PANEL,AT(4,124,352,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('QA &Pass [F9]'),AT(76,128,64,16),USE(?QA_PAss),DISABLE,LEFT,ICON('OK.gif')
                       BUTTON('QA &Fail [F10]'),AT(144,128,64,16),USE(?qa_fail),DISABLE,LEFT,ICON('Cancel.gif')
                       BUTTON('&Finish [ESC]'),AT(296,128,56,16),USE(?Finish),LEFT,ICON('thumbs.gif'),STD(STD:Close)
                       BUTTON('Fault Codes [F5]'),AT(8,128,64,16),USE(?FaultCodes),DISABLE,LEFT,ICON('FauCode.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
OutFile FILE,DRIVER('ASCII'),PRE(OUF),NAME(filename3),CREATE,BINDABLE,THREAD
Record  Record
line1       String(255)
            End
        End
Include('ParcDef.inc')
TempFilePath         CSTRING(255)
! Start Change 3684 BE(10/12/03)
    MAP
CreateNewJob                PROCEDURE()
GetOriginalAccountNumber    PROCEDURE(LONG),STRING
Check14DayB                 PROCEDURE(LONG),LONG
    END
! End Change 3684 BE(10/12/03)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Job_Number_Temp:Prompt{prop:FontColor} = -1
    ?Job_Number_Temp:Prompt{prop:Color} = 15066597
    If ?Job_Number_Temp{prop:ReadOnly} = True
        ?Job_Number_Temp{prop:FontColor} = 65793
        ?Job_Number_Temp{prop:Color} = 15066597
    Elsif ?Job_Number_Temp{prop:Req} = True
        ?Job_Number_Temp{prop:FontColor} = 65793
        ?Job_Number_Temp{prop:Color} = 8454143
    Else ! If ?Job_Number_Temp{prop:Req} = True
        ?Job_Number_Temp{prop:FontColor} = 65793
        ?Job_Number_Temp{prop:Color} = 16777215
    End ! If ?Job_Number_Temp{prop:Req} = True
    ?Job_Number_Temp{prop:Trn} = 0
    ?Job_Number_Temp{prop:FontStyle} = font:Bold
    ?serial_number_temp:Prompt{prop:FontColor} = -1
    ?serial_number_temp:Prompt{prop:Color} = 15066597
    If ?serial_number_temp{prop:ReadOnly} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 15066597
    Elsif ?serial_number_temp{prop:Req} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 8454143
    Else ! If ?serial_number_temp{prop:Req} = True
        ?serial_number_temp{prop:FontColor} = 65793
        ?serial_number_temp{prop:Color} = 16777215
    End ! If ?serial_number_temp{prop:Req} = True
    ?serial_number_temp{prop:Trn} = 0
    ?serial_number_temp{prop:FontStyle} = font:Bold
    ?Group1{prop:Font,3} = -1
    ?Group1{prop:Color} = 15066597
    ?Group1{prop:Trn} = 0
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?model_details_temp{prop:FontColor} = -1
    ?model_details_temp{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?handset_type_temp{prop:FontColor} = -1
    ?handset_type_temp{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?date_booked_temp{prop:FontColor} = -1
    ?date_booked_temp{prop:Color} = 15066597
    ?update_text_temp{prop:FontColor} = -1
    ?update_text_temp{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
FailParts       Routine
    reason" = ''
    QA_Failure_Reason(reason")

    get(audit,0)
    if access:audit.primerecord() = level:benign
        aud:notes         = 'PARTS USED:-'

        If job:Chargeable_Job = 'YES'
            Save_par_ID = Access:PARTS.SaveFile()
            Access:PARTS.ClearKey(par:Part_Number_Key)
            par:Ref_Number  = job:Ref_Number
            Set(par:Part_Number_Key,par:Part_Number_Key)
            Loop
                If Access:PARTS.NEXT()
                   Break
                End !If
                If par:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(par:Part_Number)
            End !Loop
            Access:PARTS.RestoreFile(Save_par_ID)

        End !If job:Chargeable_Job = 'YES'

        If job:Warranty_Job = 'YES'
            Save_wpr_ID = Access:WARPARTS.SaveFile()
            Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
            wpr:Ref_Number  = job:Ref_Number
            Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
            Loop
                If Access:WARPARTS.NEXT()
                   Break
                End !If
                If wpr:Ref_Number  <> job:Ref_Number      |
                    Then Break.  ! End If
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(wpr:Part_Number)
            End !Loop
            Access:WARPARTS.RestoreFile(Save_wpr_ID)

        End !If job:Chargeable_Job = 'YES'

        aud:Notes   = Clip(aud:Notes) & '<13,10,13,10>PARTS VALIDATED:-'

        Loop x# = 1 To Records(glo:Queue)
            Get(glo:Queue,x#)
            Access:QAPARTSTEMP.ClearKey(qap:RecordNumberKey)
            qap:RecordNumber = glo:Pointer
            If Access:QAPARTSTEMP.TryFetch(qap:RecordNumberKey) = Level:Benign
                !Found
                aud:Notes = Clip(aud:Notes) & '<13,10>' & Clip(qap:PartNumber)
            Else!If Access:QAPARTSTEMP.TryFetch(qap:RecordNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End!If Access:QAPARTSTEMP.TryFetch(qap:RecordNumberKey) = Level:Benign
        End !Loop x# = 1 To Records(glo:Queue)

        !Save Notes For Report
        glo:notes_global    = Clip(aud:notes)

        aud:ref_number    = job:ref_number
        aud:date          = today()
        aud:time          = clock()
        aud:type          = 'JOB'
        access:users.clearkey(use:password_key)
        use:password = glo:password
        access:users.fetch(use:password_key)
        aud:user = use:user_code
        Case f_type
            Of 'PRE'
                aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
            Of 'PRI'
                aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
        End !Case f_type
        access:audit.insert()
    end!if access:audit.primerecord() = level:benign
    glo:select1 = job:ref_number
    If def:qa_failed_label = 'YES'
        QA_Failed_Label
    End!If def:qa_failed_label = 'YES'
    If def:qa_failed_report = 'YES'
        QA_Failed
    End!If def:qa_failed_report = 'YES'
    glo:select1 = ''
    glo:notes_global    = ''
    Case f_type
        Of 'PRE'
            GetStatus(625,1,'JOB')
        Else!Case f_type
            GetStatus(615,1,'JOB')
    End!Case f_type

    job:qa_rejected = 'YES'
    job:date_qa_rejected    = Today()
    job:time_qa_rejected    = Clock()
    access:jobs.update()
    Do passed_fail_update
Check_Fields        Routine
    error_temp = 1
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number = job_number_temp
    if access:jobs.fetch(job:ref_number_key) = Level:Benign
        found# = 0
        If job:esn = serial_number_temp
            model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
            If job:third_party_site <> ''
                handset_type_temp  = 'Thid Party Repair'
            Else!If job:third_party_site <> ''
                handset_type_temp  = 'Repair'
            End!If job:third_party_site <> ''
            
            date_booked_temp   = job:date_booked
            found# = 1
        End!If job:esn = serial_number_temp
        If job:msn = serial_number_temp And found# = 0
            model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
            If job:third_party_site <> ''
                handset_type_temp  = 'Thid Party Repair'
            Else!If job:third_party_site <> ''
                handset_type_temp  = 'Repair'
            End!If job:third_party_site <> ''
            date_booked_temp   = job:date_booked
        End!If job:esn = serial_number_temp
        If job:exchange_unit_number <> '' and found# = 0
            access:exchange.clearkey(xch:ref_number_key)
            xch:ref_number = job:exchange_unit_number
            if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                If xch:esn = serial_number_temp
                    model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                    handset_type_temp  = 'Exchange'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
                If xch:msn = serial_number_temp And found# = 0
                    model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                    handset_type_temp  = 'Exchange'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
            end
        End!If job:exchange_unit_number <> ''
        If job:loan_unit_number <> '' And found# = 0
            access:loan.clearkey(loa:ref_number_key)
            loa:ref_number = job:loan_unit_number
            if access:loan.fetch(loa:ref_number_key) = Level:Benign
                If loa:esn = serial_number_temp
                    model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                    handset_type_temp  = 'Loan'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
                If loa:msn = serial_number_temp And found# = 0
                    model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                    handset_type_temp  = 'Loan'
                    date_booked_temp   = job:date_booked
                    found# = 1
                End!If job:esn = serial_number_temp
            end
        End!If job:loan_unit_number <> '' And found# = 0
        If found# = 0
            Case MessageEx('The selected Serial Number does not exist on the selected Job.','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' Update Failed'
            ?update_text_temp{prop:fontcolor} = color:red

            Select(?job_number_temp)
        End!If found# = 0
        If found# = 1
            error_temp = 0
        End
    Else!if access:jobs.fetch(job:ref_number_key) = Level:Benign
        Case MessageEx('Unable to find selected Job.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
        update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' Update Failed'
        ?update_text_temp{prop:fontcolor} = color:red

        Select(?job_number_temp)
    end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
Failed_Update       Routine
        update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' Update Failed'
        ?update_text_temp{prop:fontcolor} = color:red
        DO Disable_Buttons

Passed_update       Routine
        ! Job was updated correctly. Now release it - TrkBs: 6376 (DBH: 16-09-2005)
        RELEASE(JOBS)
        beep(beep:systemasterisk)
        jobque:job_number = job_number_temp
        jobque:record_number += 1
        jobque:type = 'QA PASS'
        Add(job_queue_temp)
        Sort(job_queue_temp,-jobque:record_number)

        update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' Updated Successfully'
        ?update_text_temp{prop:fontcolor} = color:navy
        job_number_temp = ''
        serial_number_temp = ''
        DO Disable_Buttons

Passed_fail_update       Routine
        beep(beep:systemasterisk)
        jobque:job_number = job_number_temp
        jobque:record_number += 1
        jobque:type = 'QA FAIL'
        Add(job_queue_temp)
        Sort(job_queue_temp,-jobque:record_number)

        update_text_temp = 'Job Number: ' & Clip(job_number_temp) & ' Updated Successfully'
        ?update_text_temp{prop:fontcolor} = color:navy
        job_number_temp = ''
        serial_number_temp = ''
        DO Disable_Buttons

Disable_Buttons     ROUTINE
        Disable(?qa_pass)
        Disable(?qa_fail)
        Disable(?FaultCodes)
QA_Group        Routine
Check_For_Despatch        Routine
    Set(defaults)
    access:defaults.next()

    despatch# = 0

    if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        despatch# = 1
    Else!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'
        access:subtracc.clearkey(sub:account_number_key)
        sub:account_number = job:account_number
        if access:subtracc.fetch(sub:account_number_key) = Level:Benign
            access:tradeacc.clearkey(tra:account_number_key)
            tra:account_number = sub:main_account_number
            if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
                If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If sub:despatch_invoiced_jobs <> 'YES' And sub:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                Else!If tra:use_sub_accounts = 'YES'
                    If job:chargeable_job = 'YES'
                        If tra:despatch_invoiced_jobs <> 'YES' And tra:despatch_paid_jobs <> 'YES'
                            despatch# = 1
                        End!If sub:despatch_invoiced_jobs <> 'YES'
                    Else!If job:chargeable_job = 'YES'
                        despatch# = 1
                    End!If job:chargeable_job = 'YES'
                End!If tra:use_sub_accounts = 'YES'
            end!if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
        end!if access:subtracc.fetch(sub:account_number_key) = Level:Benign
    End!if job:chargeable_job <> 'YES' and job:warranty_job = 'YES'

    If despatch# = 1
        tmp:DespatchClose   = 0
        tmp:RestockingNote  = 0
        If job:despatched <> 'YES'
            If ToBeLoaned() = 1
                restock# = 0
                If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                    Case MessageEx('A Loan Unit has been issued to this job but the Initial Transit Type has been set-up so that an Exchange Unit is required.<13,10><13,10>Do you wish to continue and mark this unit for Despatch back to the Customer, or do you want to Restock it?','ServiceBase 2000',|
                                   'Styles\question.ico','|&Continue|&Restock Unit',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                        Of 1 ! &Continue Button
                        Of 2 ! &Restock Unit Button
                            ForceDespatch()
                            tmp:restockingnote = 1
                            restock# = 1
                    End!Case MessageEx
                End!If ToBeExchanged() = 2 !Transit Type says the job will be exchanged
                If restock# = 0
                    job:despatched  = 'LAT'
                    job:despatch_type   = 'JOB'
                End!If restock# = 0
            Else!IF ToBeLoaned() = 1
                If ToBeExchanged() ! Job Will Be Exchanged
                    ForceDespatch()
                    tmp:restockingnote = 1
                Else!If ToBeExchanged() ! Job Will Be Exchanged
                    job:date_Despatched = DespatchANC(job:courier,'JOB')

                    access:courier.clearkey(cou:courier_key)
                    cou:courier = job:courier
                    if access:courier.tryfetch(cou:courier_key) = Level:Benign
                        If cou:despatchclose = 'YES'
                            tmp:DespatchClose   = 1
                        Else!If cou:despatchclose = 'YES'
                            tmp:DespatchClose   = 0
                        End!If cou:despatchclose = 'YES'
                    End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                End!If ToBeExchanged() ! Job Will Be Exchanged
            End!If ToBeLoaned() = 1
        End!If job:despatched <> 'YES'
    End!If despatch# = 1
!Closing         Routine
!!Do the automatic despatch
!    If tmp:DespatchClose    = 1
!        despatch# = 1
!        access:subtracc.clearkey(sub:account_number_key)
!        sub:account_number  = job:account_number
!        If access:subtracc.tryfetch(sub:account_number_key) = Level:Benign
!            access:tradeacc.clearkey(tra:account_number_key)
!            tra:account_number  = sub:main_account_number
!            If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
!                If tra:skip_despatch = 'YES'
!                    despatch# = 0
!                End!If tra:skip_despatch = 'YES'
!            End!If access:tradeacc.tryfetch(tra:account_number_key) = Level:Benign
!        End!If access;subtracc.tryfetch(sub:account_number_key) = Level:Benign
!        If despatch# = 1
!            access:courier.clearkey(cou:courier_key)
!            cou:courier = job:courier
!            if access:courier.tryfetch(cou:courier_key)
!                Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
!                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
!                    Of 1 ! &OK Button
!                End!Case MessageEx
!            Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
!                If access:desbatch.primerecord() = Level:Benign
!                    If access:desbatch.tryinsert()
!                        access:desbatch.cancelautoinc()
!                    Else!If access:despatch.tryinsert()
!                        Include('despsing.inc')
!                    End!If access:despatch.tryinsert()
!                End!If access:desbatch.primerecord() = Level:Benign
!            End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
!        End!If despatch# = 1
!    End!If tmp:DespatchClose    = 1
!
!    If tmp:RestockingNote = 1
!        glo:Select1 = job:ref_number
!        restocking_note
!        glo:Select1 = ''
!    End!If tmp:RestockingNote = 1
!    If print_despatch_note_temp = 'YES' and tmp:RestockingNote <> 1
!    !    
!    !    restock# = 0
!        glo:select1  = job:ref_number
!    !    If ToBeExchanged()
!    !    !Has this unit got an Exchange, or is it expecting an exchange
!    !        restock# = 1
!    !    Else!If job:exchange_unit_number
!    !        !Does the transit type expect an exchange?
!    !        access:trantype.clearkey(trt:transit_type_key)
!    !        trt:transit_type = job:transit_type
!    !        if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
!    !            if trt:exchange_unit = 'YES'
!    !                restock# = 1
!    !            End!if trt:exchange_unit = 'YES'
!    !        End!if access:trantype.tryfetch(trt:transit_type_key) = Level:benign
!    !    End!If job:exchange_unit_number <> ''
!    !    !If there is an loan unit attached, assume that a despatch note is required.
!    !    If job:loan_unit_number <> ''
!    !!        If job:despatched <> 'LAT'
!    !!            restock# = 1
!    !!        Else!If job:despatched <> 'LAT'
!    !            restock# = 0
!    !!        End!If job:despatched <> 'LAT'
!    !    End!If job:loan_unit_number <> ''
!    !
!    !    If restock# = 1
!    !        restocking_note
!    !    Else!If restock# = 1
!            despatch_note
!    !    End!If restock# = 1
!        glo:select1  = ''
!    End!If print_despatch_note_temp = 'YES'
!    !
!    !
CompletedBit        Routine
        !To avoid not changing to the right status,
        !change to completed, first, and then it should
        !Change again to one of the below status's if necessary
        GetStatus(705,1,'JOB')
        Print_Despatch_Note_Temp = ''
        If ExchangeAccount(job:Account_Number)
            !Is the Trade Account used for Exchange Units
            !if so, then there's no need to despatch the
            !unit normal. Just auto fill the despatch fields
            !and mark the job as to return to exchange stock.
            ForceDespatch
            GetStatus(707,1,'JOB')
        Else !If ExchangeAccount(job:Account_Number)

            !Will the job be restocked, i.e. has, or will have, an
            !Exchange Unit attached, or is it a normal despatch.
            restock# = 0
            If ExchangeAccount(job:Account_Number)
                restock# = 1
            Else !If ExchangeAccount(job:Account_Number)
                glo:select1  = job:ref_number
                If ToBeExchanged()
                !Has this unit got an Exchange, or is it expecting an exchange
                    restock# = 1
                End!If job:exchange_unit_number <> ''
                !If there is an loan unit attached, assume that a despatch note is required.
                If job:loan_unit_number <> ''
                    restock# = 0
                End!If job:loan_unit_number <> ''
            End !If ExchangeAccount(job:Account_Number)

            ! Start Change 3684 BE(10/12/03)
            !If CompletePrintDespatch(job:Account_Number) = Level:Benign
            !IF ((CompletePrintDespatch(job:Account_Number) = Level:Benign) AND |
            !    ((Is14DayRepackage = 0) OR ((Is14DayRepackage = 1) AND (job:repair_type_warranty[1 : 6] = '20 BER')))) THEN
            IF ((CompletePrintDespatch(job:Account_Number) = Level:Benign) AND |
                 (Check14DayB(job:ref_number) = 0)) THEN
            ! End Change 3684 BE(10/12/03)
                Print_Despatch_Note_Temp = 'YES'
            End !CompletePrintDespatch(job:Account_Number) = Level:Benign

            If restock# = 0

                !Check the Accounts' "Despatch Paid/Invoiced" jobs ticks
                !If either of those are ticked then don't despatch yet
                !print_despatch_note_temp = ''

                ! Start Change 3684 BE(10/12/03)
                !If AccountAllowedToDespatch()
                !IF (AccountAllowedToDespatch() AND |
                !    ((Is14DayRepackage = 0) OR ((Is14DayRepackage = 1) AND (job:repair_type_warranty[1 : 6] = '20 BER')))) THEN
                IF ((AccountAllowedToDespatch()) AND |
                     (Check14DayB(job:ref_number) = 0)) THEN
                ! End Change 3684 BE(10/12/03)

                    !Do we need to despatch at completion?
                    If DespatchAtClosing()
                        !Does the Trade Account have "Skip Despatch" set?
                        If AccountSkipDespatch(job:Account_Number) = Level:Benign
                            access:courier.clearkey(cou:courier_key)
                            cou:courier = job:courier
                            if access:courier.tryfetch(cou:courier_key)
                                Case MessageEx('Cannot find the Courier attached to this job.','ServiceBase 2000',|
                                               'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                    Of 1 ! &OK Button
                                End!Case MessageEx
                            Else!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                                !Give the job a unique Despatch Number
                                !then call the despsing.inc.
                                !This calls the relevant exports etc, and different
                                !despatch procedures depending on the Courier.
                                !It will then print a Despatch Note and change
                                !The status as required.
                                If access:desbatch.primerecord() = Level:Benign
                                    If access:desbatch.tryinsert()
                                        access:desbatch.cancelautoinc()
                                    Else!If access:despatch.tryinsert()
                                        DespatchSingle()
                                        Print_Despatch_Note_Temp = ''
                                    End!If access:despatch.tryinsert()
                                End!If access:desbatch.primerecord() = Level:Benign
                            End!if access:courier.tryfetch(cou:courier_key) = Level:Benign
                        Else
                            GetStatus(705,1,'JOB')
                        End !If AccountSkipDespatch = Level:Benign
                    Else !If DespatchAtClosing()
                        If job:Loan_Unit_Number <> ''
                            GetStatus(811,1,'JOB')
                        Else
                            GetStatus(705,1,'JOB')
                        End !If job:Loan_Unit_Number <> ''
                    End !If DespatchAtClosing()
                Else !If AccountAllowedToDespatch
                    GetStatus(705,1,'JOB')
                End !If AccountAllowedToDespatch
            Else !If restock# = 0
                If job:Exchange_Unit_Number <> ''
                    !Are either of the Chargeable or Warranty repair types BERs?
                    !If so, then don't change the status to 707 because it shouldn't
                    !be returned to the Exchange Stock.
                    Comp# = 0
                    If job:Chargeable_Job = 'YES'
                        If BERRepairType(job:Repair_Type) = Level:Benign
                             Comp# = 1
                        End !If BERRepairType(job:Repair_Type) = Level:Benign
                    End !If job:Chageable_Job = 'YES'
                    If job:Warranty_Job = 'YES' and Comp# = 0
                        If BERRepairType(job:Repair_Type_Warranty) = Level:Benign
                            Comp# = 1
                        End !If BERRepairType(job:Repair_Type) = Level:Benign
                    End !If job:Warranty_Job = 'YES'
                    If Comp# = 1
                        Case MessageEx('This unit has been exchanged but is a B.E.R. Repair. '&|
                          '<13,10>DO NOT place this unit into Exchange Stock.','ServiceBase 2000',|
                                       'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        GetStatus(705,1,'JOB')
                        Print_Despatch_Note_Temp = 'NO'
                    Else !If Comp# = 1
                        Case MessageEx('This unit has been exchanged. It should now be placed into Exchange Stock.','ServiceBase 2000',|
                                       'Styles\idea.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        GetStatus(707,1,'JOB')
                    End !If Comp# = 1
                Else !If job:Exchange_Unit_Number <> ''
                    GetStatus(705,1,'JOB')
                End !If job:Exchange_Unit_Number <> ''    
            End !If restock# = 0
            !Does the Account care about Bouncers?
            Check_For_Bouncers_Temp = 1
            If CompleteCheckForBouncer(job:Account_Number)
                Check_For_Bouncers_Temp = 0
            End !CompleteCheckForBouncer(job:Account_Number) = Level:Benign

        End !If ExchangeAccount(job:Account_Number)

        !Fill in the relevant fields

        saved_ref_number_temp   = job:ref_number
        saved_esn_temp  = job:esn
        saved_msn_temp  = job:msn
        job:date_completed = Today()
        job:time_completed = Clock()
        date_completed_temp = job:date_completed
        time_completed_temp = job:time_completed
        job:completed = 'YES'

        If Check_For_Bouncers_Temp
            !Check if the job is a Bouncer and add
            !it to the Bouncer List
            ! Start Change 3684 BE(23/01/2004)
            !If CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
            BouncerCount# = CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
            IF (((~Is14DayRepackage) AND (BouncerCount# > 0)) OR ((Is14DayRepackage) AND (BouncerCount# > 2))) THEN
            ! End Change 3684 BE(23/01/2004)
                !Bouncer
                AddBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
            Else !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

            End !CountBouncers(job:Ref_Number,job:Date_Booked,job:ESN)

        End !If Check_For_Bouncers_Temp

        !Check if this job is an EDI job
        Access:MODELNUM.Clearkey(mod:Model_Number_Key)
        mod:Model_Number    = job:Model_Number
        If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Found
            job:edi = PendingJob(mod:Manufacturer)
            job:Manufacturer    = mod:Manufacturer
        Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign

        !Mark the job's exchange unit as being available
        CompleteDoExchange(job:Exchange_Unit_Number)

        Do QA_Group

        ! Start Change 3684 BE(22/01/04)
        IF (Is14DayRepackage = 1) THEN
            GetStatus(810,1,'JOB')
            job:despatched  = 'REA'
            job:despatch_Type = 'JOB'
            job:current_courier = job:courier
        END
        ! End Change 3684 BE(22/01/04)

        !ThisMakeover.SetWindow(win:form)
        Access:JOBS.TryUpdate()

        If ExchangeAccount(job:Account_Number)
            Case MessageEx('This is an Exchange Repair. Please re-stock the unit.','ServiceBase 2000',|
                           'Styles\warn.ico','|&OK',1,1,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
            If def:Use_Job_Label= 'YES'
                glo:Select1 = job:Ref_Number
                Set(DEFAULTS)
                Access:DEFAULTS.Next()
                Case def:label_printer_type
                    Of 'TEC B-440 / B-442'
                        Thermal_Labels('ER')
                    Of 'TEC B-452'
                        Thermal_Labels_B452
                End!Case def:themal_printer_type
            End !If def:Use_Job_Label= 'YES'
        End !If ExchangeAccount(job:Account_Number)

        If print_despatch_note_temp = 'YES'
            If restock# = 1
                restocking_note
            Else!If restock# = 1
                despatch_note
            End!If restock# = 1
            glo:select1  = ''
        End!If print_despatch_note_temp = 'YES'

        ! Start Change 3684 BE(22/01/04)
        SETCURSOR
        ! End Change 3684 BE(22/01/04)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_QA',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_QA',1)
    SolaceViewVars('Is14DayRepackage',Is14DayRepackage,'Update_QA',1)
    SolaceViewVars('IniFilepath',IniFilepath,'Update_QA',1)
    SolaceViewVars('bookmark',bookmark,'Update_QA',1)
    SolaceViewVars('RepackageDefaults:OriginalAccount1',RepackageDefaults:OriginalAccount1,'Update_QA',1)
    SolaceViewVars('RepackageDefaults:OriginalAccount2',RepackageDefaults:OriginalAccount2,'Update_QA',1)
    SolaceViewVars('RepackageDefaults:OriginalChargeType',RepackageDefaults:OriginalChargeType,'Update_QA',1)
    SolaceViewVars('RepackageDefaults:NewAccount',RepackageDefaults:NewAccount,'Update_QA',1)
    SolaceViewVars('RepackageDefaults:NewChargeType',RepackageDefaults:NewChargeType,'Update_QA',1)
    SolaceViewVars('RepackageDefaults:PrintJobLabel',RepackageDefaults:PrintJobLabel,'Update_QA',1)
    SolaceViewVars('JobsengRec:JobNumber',JobsengRec:JobNumber,'Update_QA',1)
    SolaceViewVars('JobsengRec:UserCode',JobsengRec:UserCode,'Update_QA',1)
    SolaceViewVars('JobsengRec:DateAllocated',JobsengRec:DateAllocated,'Update_QA',1)
    SolaceViewVars('JobsengRec:TimeAllocated',JobsengRec:TimeAllocated,'Update_QA',1)
    SolaceViewVars('JobsengRec:AllocatedBy',JobsengRec:AllocatedBy,'Update_QA',1)
    SolaceViewVars('JobsengRec:EngSkillLevel',JobsengRec:EngSkillLevel,'Update_QA',1)
    SolaceViewVars('JobsengRec:JobSkillLevel',JobsengRec:JobSkillLevel,'Update_QA',1)
    SolaceViewVars('JobexhisRec:Ref_Number',JobexhisRec:Ref_Number,'Update_QA',1)
    SolaceViewVars('JobexhisRec:Loan_Unit_Number',JobexhisRec:Loan_Unit_Number,'Update_QA',1)
    SolaceViewVars('JobexhisRec:Date',JobexhisRec:Date,'Update_QA',1)
    SolaceViewVars('JobexhisRec:Time',JobexhisRec:Time,'Update_QA',1)
    SolaceViewVars('JobexhisRec:User',JobexhisRec:User,'Update_QA',1)
    SolaceViewVars('JobexhisRec:Status',JobexhisRec:Status,'Update_QA',1)
    SolaceViewVars('JoblohisRec:Ref_Number',JoblohisRec:Ref_Number,'Update_QA',1)
    SolaceViewVars('JoblohisRec:Loan_Unit_Number',JoblohisRec:Loan_Unit_Number,'Update_QA',1)
    SolaceViewVars('JoblohisRec:Date',JoblohisRec:Date,'Update_QA',1)
    SolaceViewVars('JoblohisRec:Time',JoblohisRec:Time,'Update_QA',1)
    SolaceViewVars('JoblohisRec:User',JoblohisRec:User,'Update_QA',1)
    SolaceViewVars('JoblohisRec:Status',JoblohisRec:Status,'Update_QA',1)
    SolaceViewVars('JobstageRec:Ref_Number',JobstageRec:Ref_Number,'Update_QA',1)
    SolaceViewVars('JobstageRec:Job_Stage',JobstageRec:Job_Stage,'Update_QA',1)
    SolaceViewVars('JobstageRec:Date',JobstageRec:Date,'Update_QA',1)
    SolaceViewVars('JobstageRec:Time',JobstageRec:Time,'Update_QA',1)
    SolaceViewVars('JobstageRec:User',JobstageRec:User,'Update_QA',1)
    SolaceViewVars('JobaccRec:Ref_Number',JobaccRec:Ref_Number,'Update_QA',1)
    SolaceViewVars('JobaccRec:Accessory',JobaccRec:Accessory,'Update_QA',1)
    SolaceViewVars('AuditRec:Ref_Number',AuditRec:Ref_Number,'Update_QA',1)
    SolaceViewVars('AuditRec:Date',AuditRec:Date,'Update_QA',1)
    SolaceViewVars('AuditRec:Time',AuditRec:Time,'Update_QA',1)
    SolaceViewVars('AuditRec:User',AuditRec:User,'Update_QA',1)
    SolaceViewVars('AuditRec:Action',AuditRec:Action,'Update_QA',1)
    SolaceViewVars('AuditRec:DummyField',AuditRec:DummyField,'Update_QA',1)
    SolaceViewVars('AuditRec:Notes',AuditRec:Notes,'Update_QA',1)
    SolaceViewVars('AuditRec:Type',AuditRec:Type,'Update_QA',1)
    SolaceViewVars('AudstatsRec:RefNumber',AudstatsRec:RefNumber,'Update_QA',1)
    SolaceViewVars('AudstatsRec:Type',AudstatsRec:Type,'Update_QA',1)
    SolaceViewVars('AudstatsRec:DateChanged',AudstatsRec:DateChanged,'Update_QA',1)
    SolaceViewVars('AudstatsRec:TimeChanged',AudstatsRec:TimeChanged,'Update_QA',1)
    SolaceViewVars('AudstatsRec:OldStatus',AudstatsRec:OldStatus,'Update_QA',1)
    SolaceViewVars('AudstatsRec:NewStatus',AudstatsRec:NewStatus,'Update_QA',1)
    SolaceViewVars('AudstatsRec:UserCode',AudstatsRec:UserCode,'Update_QA',1)
    SolaceViewVars('JobseRec:RefNumber',JobseRec:RefNumber,'Update_QA',1)
    SolaceViewVars('JobseRec:JobMark',JobseRec:JobMark,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode1',JobseRec:TraFaultCode1,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode2',JobseRec:TraFaultCode2,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode3',JobseRec:TraFaultCode3,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode4',JobseRec:TraFaultCode4,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode5',JobseRec:TraFaultCode5,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode6',JobseRec:TraFaultCode6,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode7',JobseRec:TraFaultCode7,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode8',JobseRec:TraFaultCode8,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode9',JobseRec:TraFaultCode9,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode10',JobseRec:TraFaultCode10,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode11',JobseRec:TraFaultCode11,'Update_QA',1)
    SolaceViewVars('JobseRec:TraFaultCode12',JobseRec:TraFaultCode12,'Update_QA',1)
    SolaceViewVars('JobseRec:SIMNumber',JobseRec:SIMNumber,'Update_QA',1)
    SolaceViewVars('JobseRec:JobReceived',JobseRec:JobReceived,'Update_QA',1)
    SolaceViewVars('JobseRec:SkillLevel',JobseRec:SkillLevel,'Update_QA',1)
    SolaceViewVars('JobseRec:UPSFlagCode',JobseRec:UPSFlagCode,'Update_QA',1)
    SolaceViewVars('JobseRec:FailedDelivery',JobseRec:FailedDelivery,'Update_QA',1)
    SolaceViewVars('JobseRec:CConfirmSecondEntry',JobseRec:CConfirmSecondEntry,'Update_QA',1)
    SolaceViewVars('JobseRec:WConfirmSecondEntry',JobseRec:WConfirmSecondEntry,'Update_QA',1)
    SolaceViewVars('JobseRec:EndUserEmailAddress',JobseRec:EndUserEmailAddress,'Update_QA',1)
    SolaceViewVars('JobseRec:Network',JobseRec:Network,'Update_QA',1)
    SolaceViewVars('JobseRec:ExchangeReason',JobseRec:ExchangeReason,'Update_QA',1)
    SolaceViewVars('JobseRec:LoanReason',JobseRec:LoanReason,'Update_QA',1)
    SolaceViewVars('JobseRec:InWorkshopDate',JobseRec:InWorkshopDate,'Update_QA',1)
    SolaceViewVars('JobseRec:InWorkshopTime',JobseRec:InWorkshopTime,'Update_QA',1)
    SolaceViewVars('JobseRec:Pre_RF_Board_IMEI',JobseRec:Pre_RF_Board_IMEI,'Update_QA',1)
    SolaceViewVars('JobnotesRec:RefNumber',JobnotesRec:RefNumber,'Update_QA',1)
    SolaceViewVars('JobnotesRec:Fault_Description',JobnotesRec:Fault_Description,'Update_QA',1)
    SolaceViewVars('JobnotesRec:Engineers_Notes',JobnotesRec:Engineers_Notes,'Update_QA',1)
    SolaceViewVars('JobnotesRec:Invoice_Text',JobnotesRec:Invoice_Text,'Update_QA',1)
    SolaceViewVars('JobnotesRec:Collection_Text',JobnotesRec:Collection_Text,'Update_QA',1)
    SolaceViewVars('JobnotesRec:Delivery_Text',JobnotesRec:Delivery_Text,'Update_QA',1)
    SolaceViewVars('JobnotesRec:ColContactName',JobnotesRec:ColContactName,'Update_QA',1)
    SolaceViewVars('JobnotesRec:ColDepartment',JobnotesRec:ColDepartment,'Update_QA',1)
    SolaceViewVars('JobnotesRec:DelContactName',JobnotesRec:DelContactName,'Update_QA',1)
    SolaceViewVars('JobnotesRec:DelDepartment',JobnotesRec:DelDepartment,'Update_QA',1)
    SolaceViewVars('Local',Local,'Update_QA',1)
    SolaceViewVars('date_error_Temp',date_error_Temp,'Update_QA',1)
    SolaceViewVars('Date_Completed_Temp',Date_Completed_Temp,'Update_QA',1)
    SolaceViewVars('Time_Completed_Temp',Time_Completed_Temp,'Update_QA',1)
    SolaceViewVars('Check_For_Bouncers_Temp',Check_For_Bouncers_Temp,'Update_QA',1)
    SolaceViewVars('Print_Despatch_Note_Temp',Print_Despatch_Note_Temp,'Update_QA',1)
    SolaceViewVars('Saved_Ref_Number_Temp',Saved_Ref_Number_Temp,'Update_QA',1)
    SolaceViewVars('Saved_Esn_Temp',Saved_Esn_Temp,'Update_QA',1)
    SolaceViewVars('Saved_MSN_Temp',Saved_MSN_Temp,'Update_QA',1)
    SolaceViewVars('tmp:DespatchClose',tmp:DespatchClose,'Update_QA',1)
    SolaceViewVars('tmp:RestockingNote',tmp:RestockingNote,'Update_QA',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Update_QA',1)
    SolaceViewVars('save_qap_id',save_qap_id,'Update_QA',1)
    SolaceViewVars('save_par_id',save_par_id,'Update_QA',1)
    SolaceViewVars('save_xch_id',save_xch_id,'Update_QA',1)
    SolaceViewVars('save_lac_id',save_lac_id,'Update_QA',1)
    SolaceViewVars('save_aud_id',save_aud_id,'Update_QA',1)
    SolaceViewVars('job_queue_temp:job_number',job_queue_temp:job_number,'Update_QA',1)
    SolaceViewVars('job_queue_temp:type',job_queue_temp:type,'Update_QA',1)
    SolaceViewVars('job_queue_temp:record_number',job_queue_temp:record_number,'Update_QA',1)
    SolaceViewVars('error_temp',error_temp,'Update_QA',1)
    SolaceViewVars('Job_Number_Temp',Job_Number_Temp,'Update_QA',1)
    SolaceViewVars('serial_number_temp',serial_number_temp,'Update_QA',1)
    SolaceViewVars('model_details_temp',model_details_temp,'Update_QA',1)
    SolaceViewVars('handset_type_temp',handset_type_temp,'Update_QA',1)
    SolaceViewVars('date_booked_temp',date_booked_temp,'Update_QA',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Update_QA',1)
    SolaceViewVars('update_text_temp',update_text_temp,'Update_QA',1)
    SolaceViewVars('tmp:ConsignNo',tmp:ConsignNo,'Update_QA',1)
    SolaceViewVars('tmp:AccountNumber',tmp:AccountNumber,'Update_QA',1)
    SolaceViewVars('tmp:OldConsignno',tmp:OldConsignno,'Update_QA',1)
    SolaceViewVars('sav:Path',sav:Path,'Update_QA',1)
    SolaceViewVars('tmp:LabelError',tmp:LabelError,'Update_QA',1)
    SolaceViewVars('account_number2_temp',account_number2_temp,'Update_QA',1)
    SolaceViewVars('save_cou_ali_id',save_cou_ali_id,'Update_QA',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Update_QA',1)
    SolaceViewVars('tmp:ParcellineName',tmp:ParcellineName,'Update_QA',1)
    SolaceViewVars('tmp:WorkstationName',tmp:WorkstationName,'Update_QA',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job_Number_Temp:Prompt;  SolaceCtrlName = '?Job_Number_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Job_Number_Temp;  SolaceCtrlName = '?Job_Number_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?serial_number_temp:Prompt;  SolaceCtrlName = '?serial_number_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?serial_number_temp;  SolaceCtrlName = '?serial_number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Group1;  SolaceCtrlName = '?Group1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?model_details_temp;  SolaceCtrlName = '?model_details_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?handset_type_temp;  SolaceCtrlName = '?handset_type_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?date_booked_temp;  SolaceCtrlName = '?date_booked_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?update_text_temp;  SolaceCtrlName = '?update_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List1;  SolaceCtrlName = '?List1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?QA_PAss;  SolaceCtrlName = '?QA_PAss';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?qa_fail;  SolaceCtrlName = '?qa_fail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Finish;  SolaceCtrlName = '?Finish';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FaultCodes;  SolaceCtrlName = '?FaultCodes';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Update_QA')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_QA')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Job_Number_Temp:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Clear(job_queue_temp)
  Free(job_queue_temp)
  Relate:AUDIT.Open
  Relate:COURIER_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DESBATCH.Open
  Relate:EXCHANGE.Open
  Relate:JOBS2_ALIAS.Open
  Relate:NETWORKS.Open
  Relate:PARTS_ALIAS.Open
  Relate:REPTYDEF.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:VATCODE.Open
  Relate:WARPARTS_ALIAS.Open
  Access:JOBS.UseFile
  Access:JOBSTAGE.UseFile
  Access:LOAN.UseFile
  Access:STATUS.UseFile
  Access:USERS.UseFile
  Access:COURIER.UseFile
  Access:LOANACC.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:MANUFACT.UseFile
  Access:MODELNUM.UseFile
  Access:CHARTYPE.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:INVOICE.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:JOBSE.UseFile
  Access:JOBNOTES.UseFile
  Access:JOBACC.UseFile
  Access:AUDSTATS.UseFile
  Access:JOBSENG.UseFile
  Access:JOBEXHIS.UseFile
  Access:JOBLOHIS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  ! Start Change 3684 BE(10/12/03)
  IniFilepath =  CLIP(PATH()) & '\CEL14DAY.INI'
  rpg:OriginalAccount1 = GETINI('DEFAULTS','OriginalAccount1',,IniFilepath)
  rpg:OriginalAccount2 = GETINI('DEFAULTS','OriginalAccount2',,IniFilepath)
  rpg:OriginalChargeType = GETINI('DEFAULTS','OriginalChargeType',,IniFilepath)
  rpg:NewAccount = GETINI('DEFAULTS','NewAccount',,IniFilepath)
  rpg:NewChargeType = GETINI('DEFAULTS','NewChargeType',,IniFilepath)
  rpg:PrintJoblabel = GETINI('DEFAULTS','PrintJoblabel',0,IniFilepath)
  ! End Change 3684 BE(10/12/03)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:AUDIT.Close
    Relate:COURIER_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DESBATCH.Close
    Relate:EXCHANGE.Close
    Relate:JOBS2_ALIAS.Close
    Relate:NETWORKS.Close
    Relate:PARTS_ALIAS.Close
    Relate:REPTYDEF.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:VATCODE.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_QA',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?serial_number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?serial_number_temp, Accepted)
      IF LEN(CLIP(serial_number_temp)) = 18
        !Ericsson IMEI!
        serial_number_temp = SUB(serial_number_temp,4,15)
        !ESN_Entry_Temp = Job:ESN
        UPDATE()
        DISPLAY()
      ELSE
        !Job:ESN = ESN_Entry_Temp
      END
      Disable(?qa_pass)
      Disable(?qa_fail)
      Disable(?FaultCodes)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
          found# = 0
          If job:esn = serial_number_temp
              If job:workshop <> 'YES'
                  found# = 4
              Else!If job:workshop <> 'YES'
                  model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
                  If job:third_party_site <> ''
                      handset_type_temp  = 'Third Party Repair'
                  Else!If job:third_party_site <> ''
                      handset_type_temp  = 'Repair'
                  End!If job:third_party_site <> ''
                  
                  date_booked_temp   = job:date_booked
                  found# = 1
      
              End!If job:workshop <> 'YES'
      
          End!If job:esn = serial_number_temp
          If job:msn = serial_number_temp And found# = 0
              If job:workshop <> 'YES'
                  found# = 4
              Else!If job:workshop <> 'YES'
                  model_details_temp = Clip(job:model_number) & ' - ' & Clip(job:manufacturer)
                  If job:third_party_site <> ''
                      handset_type_temp  = 'Third Party Repair'
                  Else!If job:third_party_site <> ''
                      handset_type_temp  = 'Repair'
                  End!If job:third_party_site <> ''
                  date_booked_temp   = job:date_booked
                  found# = 1
      
              End!If job:workshop <> 'YES'
          End!If job:esn = serial_number_temp
          If job:exchange_unit_number <> '' and found# = 0
              access:exchange.clearkey(xch:ref_number_key)
              xch:ref_number = job:exchange_unit_number
              if access:exchange.fetch(xch:ref_number_key) = Level:Benign
                  If xch:esn = serial_number_temp
                      If job:exchange_consignment_number <> ''
                          Case MessageEx('Error! You have entered the Serial Number of the Exchange Unit attached to the selected job.<13,10><13,10>This unit has already been despatched.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          found# = 3
                      Else!If job:exchange_consignment_number <> ''
                          model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                          handset_type_temp  = 'Exchange'
                          date_booked_temp   = job:date_booked
                          found# = 1
                      End!If job:exchange_consignment_number <> ''
                  End!If job:esn = serial_number_temp
                  If xch:msn = serial_number_temp And found# = 0
                      If job:exchange_consignment_number <> ''
                          Case MessageEx('Error! You have entered the Serial Number of the Exchange Unit attached to the selected job.<13,10><13,10>This unit has already been despatched.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          found# = 3
                      Else!If job:exchange_consignment_number <> ''
      
                          model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                          handset_type_temp  = 'Exchange'
                          date_booked_temp   = job:date_booked
                          found# = 1
                      End!If job:exchange_consignment_number <> ''
                  End!If job:esn = serial_number_temp
              end
              If found# = 0 and job:third_party_site <> ''
                  save_xch_id = access:exchange.savefile()
                  access:exchange.clearkey(xch:esn_only_key)
                  xch:esn = serial_number_temp
                  set(xch:esn_only_key,xch:esn_only_key)
                  loop
                      if access:exchange.next()
                         break
                      end !if
                      if xch:esn <> serial_number_temp      |
                          then break.  ! end if
                      If xch:job_number   = job:ref_number
                          model_details_temp = Clip(xch:model_number) & ' - ' & Clip(xch:manufacturer)
                          handset_type_temp  = 'Third Party Site'
                          date_booked_temp   = job:date_booked
                          found# = 1
                          Break
                      End!If xch:job_number   = job:ref_number
                  end !loop
                  access:exchange.restorefile(save_xch_id)
              End!If found# = 0
          End!If job:exchange_unit_number <> ''
          If job:loan_unit_number <> '' And found# = 0
              access:loan.clearkey(loa:ref_number_key)
              loa:ref_number = job:loan_unit_number
              if access:loan.fetch(loa:ref_number_key) = Level:Benign
                  If loa:esn = serial_number_temp
                      If job:loan_consignment_number <> ''
                          Case MessageEx('Error! You have entered the Serial Number of the Loan Unit attached to the selected job.<13,10><13,10>This unit has already been despatched.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          found# = 3
                      Else!If job:exchange_consignment_number <> ''
      
                          model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                          handset_type_temp  = 'Loan'
                          date_booked_temp   = job:date_booked
                          found# = 1
                      End!If job:exchange_consignment_number <> ''
                  End!If job:esn = serial_number_temp
                  If loa:msn = serial_number_temp And found# = 0
                      If job:loan_consignment_number <> ''
                          Case MessageEx('Error! You have entered the Serial Number of the Loan Unit attached to the selected job.<13,10><13,10>This unit has already been despatched.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          found# = 3
                      Else!If job:exchange_consignment_number <> ''
                          model_details_temp = Clip(loa:model_number) & ' - ' & Clip(loa:manufacturer)
                          handset_type_temp  = 'Loan'
                          date_booked_temp   = job:date_booked
                          found# = 1
                      End!!If job:exchange_consignment_number <> ''
                  End!If job:esn = serial_number_temp
              end
          End!If job:loan_unit_number <> '' And found# = 0
          Case found#
              Of 0
                  Case MessageEx('The selected Serial Number does not exist on the selected Job.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Do failed_update
      
                  Select(?job_number_temp)
              Of 1
                  Enable(?qa_pass)
                  Enable(?qa_fail)
                  Enable(?FaultCodes)
              Of 2
                  Case MessageEx('Error! You have entered the Serial Number of the Customer''s Unit.<13,10><13,10>The selected job has an Exchange Unit attached that has yet to be despatched. This must PASS Manual QA first.<13,10><13,10>You must enter the serial number of the Exchange Unit to continue.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Select(?serial_number_temp)
                  Do failed_update
              Of 3
                  Do failed_update
                  Select(?job_number_temp)
              Of 4
                  Do failed_update
                  Case MessageEx('Error! The selected unit is not in the Workshop.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  Select(?job_number_temp)
          End!Case found#
      
      
      Else!if access:jobs.fetch(job:ref_number_key) = Level:Benign
          Case MessageEx('Unable to find selected Job.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          DO failed_update
          Select(?job_number_temp)
      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
      
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?serial_number_temp, Accepted)
    OF ?QA_PAss
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QA_PAss, Accepted)
      update_text_temp = ''
      Set(Defaults)
      access:Defaults.next()
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
          ! Is the record in use? - TrkBs: 6376 (DBH: 16-09-2005)
          Pointer# = Pointer(JOBS)
          Hold(JOBS,1)
          Get(JOBS,Pointer#)
          If Errorcode() = 43
              Case MessageEx('This job is currently in use by another station.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                  Of 1 ! &OK Button
              End !Case MessageEx
              Do failed_update
              Cycle
          End !If Errorcode() = 43
      
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Found
              If f_Type = 'PRE' And ~man:UseElectronicQA
                  Case MessageEx('The Manufacturer of the selected job is not setup to do Electronic QA.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
      
              Else !If f_Type = 'PRE' And ~man:UseElectronicQA
                  error# = 0
      
                  ! Start Change 3684 BE(10/12/03)
                  Is14DayRepackage = 0
                  IF (rpg:NewAccount <> '' AND job:Account_Number = rpg:NewAccount) THEN
                      Is14DayRepackage = 1
                  END
                  ! End Change 3684 BE(10/12/03)
      
                  If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
                      If ~man:QALoanExchange
                          error# = 1
                          Case MessageEx('This Manufacturer is not setup to QA Loan/Exchange Units.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!If def:qaexchloan <> 'YES'
      
                          error# = 1                                                              !Stop from doing the job check
                          If handset_type_temp = 'Exchange'                                               !Is it an Exchange that was scanned?
                              If job:exchange_consignment_number = ''                                     !Check that is isn't already ready
                                  type# = 2 !1 - Job, 2 - Exchange, 3 - Loan
                                  Case AccessoryCheck('EXCHANGE')
                                      Of 1 Orof 2
                                          reason" = ''
                                          QA_Failure_Reason(reason")
                                          get(audit,0)
                                          if access:audit.primerecord() = level:benign
                                              aud:notes         = 'ACCESSORIES BOOKED IN:-'
                                              save_jac_id = access:jobacc.savefile()
                                              access:jobacc.clearkey(jac:ref_number_key)
                                              jac:ref_number = job:Ref_number
                                              set(jac:ref_number_key,jac:ref_number_key)
                                              loop
                                                  if access:jobacc.next()
                                                     break
                                                  end !if
                                                  if jac:ref_number <> job:Ref_number      |
                                                      then break.  ! end if
                                                  aud:notes   = Clip(aud:notes) & '<13,10>' & Clip(jac:accessory)
                                              end !loop
                                              access:jobacc.restorefile(save_jac_id)
      
                                              aud:notes       = Clip(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                              Loop x# = 1 To Records(glo:queue)
                                                  Get(glo:queue,x#)
                                                  aud:notes       = CLip(aud:notes) & '<13,10>' & Clip(glo:pointer)
                                              End!Loop x# = 1 To Records(glo:queue)
                                              !Save Notes For Report
                                              glo:notes_global    = Clip(aud:notes)
      
                                              aud:ref_number    = job:ref_number
                                              aud:date          = today()
                                              aud:time          = clock()
                                              aud:type          = 'EXC'
                                              access:users.clearkey(use:password_key)
                                              use:password = glo:password
                                              access:users.fetch(use:password_key)
                                              aud:user = use:user_code
                                              Case f_type
                                                  Of 'PRE'
                                                      aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                  Of 'PRI'
                                                      aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                              End !Case f_type
                                              access:audit.insert()
                                          end!if access:audit.primerecord() = level:benign
                                          glo:select1 = job:exchange_unit_number
                                          If def:qa_failed_label = 'YES'
                                              QA_Failed_Label_Exchange
                                          End!If def:qa_failed_label = 'YES'
                                          If def:qa_failed_report = 'YES'
                                              QA_Failed_Exchange
                                          End!If def:qa_failed_report = 'YES'
                                          glo:select1 = ''
                                          glo:notes_global    = ''
                                          Case f_type
                                              Of 'PRE'
                                                  GetStatus(625,1,'EXC')
                                              Else!Case f_type
                                                  GetStatus(615,1,'EXC')
                                          End!Case f_type
      
                                          job:qa_rejected = 'YES'
                                          job:date_qa_rejected    = Today()
                                          job:time_qa_rejected    = Clock()
                                          access:jobs.update()
                                          Do passed_fail_update
      
                                      Of 3
      
                                      Of 0
                                      
      
      
                                          Case f_type                                                             !to be despatched.
                                              Of 'PRE'
                                                  Do passed_update
                                                  get(audit,0)
                                                  if access:audit.primerecord() = level:benign
                                                      aud:notes         = 'EXCHANGE UNIT NUMBER: ' & Clip(job:Exchange_unit_number)
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = today()
                                                      aud:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED (EXC)'
                                                      aud:type          = 'EXC'
                                                      access:audit.insert()
                                                  end!�if access:audit.primerecord() = level:benign
      
                                                  GetStatus(620,1,'EXC') !Electronic QA passed
      
                                                  access:jobs.update()
                                                  access:exchange.clearkey(xch:ref_number_key)
                                                  xch:ref_number = job:exchange_unit_number
                                                  if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                      xch:available = 'QA2'
                                                      access:exchange.update()
                                                      get(exchhist,0)
                                                      if access:exchhist.primerecord() = level:benign
                                                          exh:ref_number   = xch:ref_number
                                                          exh:date          = today()
                                                          exh:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          exh:user = use:user_code
                                                          exh:status        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                                          access:exchhist.insert()
                                                      end!if access:exchhist.primerecord() = level:benign
                                                  end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                  If def:QAPassLabel = 1
                                                      glo:Select1  = job:Exchange_Unit_Number
                                                      QA_Passed_Label_Exchange
                                                      glo:Select1  = ''
                                                  End!If def:QAPassLabel = 1
      
      
                                              Of 'PRI'
                                                  error_courier# = 0
                                                  If job:exchange_courier = ''
                                                      Case MessageEx('There is NO Courier attached to this Exchange Unit!<13,10><13,10>You will not be able to QA this unit until you have attached one, as it will not be able to be despatched correctly.<13,10><13,10>Do you wish to select a Courier Now?','ServiceBase 2000',|
                                                                     'Styles\stop.ico','|&Select Courier|&Cancel QA',2,2,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                          Of 1 ! &Select Courier Button
                                                              saverequest#      = globalrequest
                                                              globalresponse    = requestcancelled
                                                              globalrequest     = selectrecord
                                                              browse_courier
                                                              if globalresponse = requestcompleted
                                                                  job:exchange_courier = cou:courier
                                                              else
                                                                  error_courier# = 1
                                                                  Case MessageEx('This unit has NOT been QA''d at this time.','ServiceBase 2000',|
                                                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                                      Of 1 ! &OK Button
                                                                  End!Case MessageEx
                                                              end
                                                              globalrequest     = saverequest#
                                                          Of 2 ! &Cancel QA Button
                                                              error_courier# = 1
                                                      End!Case MessageEx
                                                  End!If job:exchange_courier = ''
                                                  If error_courier# = 1
                                                      do failed_update
                                                  Else!If error_courier# = 1
                                                      do passed_update
      
                                                      job:Exchange_Despatched = DespatchANC(job:Exchange_Courier,'EXC')
      
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      job:current_courier = job:exchange_courier
                                                      Access:courier.clearkey(cou:courier_key)
                                                      cou:courier = job:exchange_courier
                                                      access:courier.tryfetch(cou:courier_key)
                                                      If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                          job:excservice = cou:service
                                                      Else!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                          job:excservice = ''
                                                      End!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
      
                                                      GetStatus(610,1,'EXC') !Despatch Exchange Unit
      
                                                      get(audit,0)
                                                      if access:audit.primerecord() = level:benign
                                                          aud:notes         = 'EXCHANGE UNIT NUMBER: ' & Clip(job:Exchange_unit_number)
                                                          aud:ref_number    = job:ref_number
                                                          aud:date          = today()
                                                          aud:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          aud:user = use:user_code
                                                          aud:action        = 'RAPID QA UPDATE: MANUAL QA PASSED (EXC)'
                                                          aud:type          = 'EXC'
                                                          access:audit.insert()
                                                      end!�if access:audit.primerecord() = level:benign
                                                      access:jobs.update()
                                                      access:exchange.clearkey(xch:ref_number_key)
                                                      xch:ref_number = job:exchange_unit_number
                                                      if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                          xch:available = 'EXC'
                                                          access:exchange.update()
                                                          get(exchhist,0)
                                                          if access:exchhist.primerecord() = level:benign
                                                              exh:ref_number   = xch:ref_number
                                                              exh:date          = today()
                                                              exh:time          = clock()
                                                              access:users.clearkey(use:password_key)
                                                              use:password =glo:password
                                                              access:users.fetch(use:password_key)
                                                              exh:user = use:user_code
                                                              exh:status        = 'UNIT EXCHANGED ON JOB NO: ' & CLip(job:Ref_number)
                                                              access:exchhist.insert()
                                                          end!if access:exchhist.primerecord() = level:benign
                                                      end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
      
                                                      If def:QAPassLabel = 1
                                                          glo:Select1  = job:Exchange_Unit_Number
                                                          QA_Passed_Label_Exchange
                                                          glo:Select1  = ''
                                                      End!If def:QAPassLabel = 1
      
                                                  End!If error_courier# = 0
                                          End!Case f_type
                                  End!Case AccessoryCheck('EXCHANGE')
                              End!If job:exchange_consignment_number = '' And job:despatched <> 'EXC'
                          End!If handset_type_temp = 'Exchange'
                          IF handset_type_temp = 'Loan'
                              If job:loan_consignment_number = '' And job:despatched <> 'LOA'         !Check that is isn't already ready
      
                                  Case AccessoryCheck('LOAN')
                                      Of 1 Orof 2 !Failed QA
                                          reason" = ''
                                          QA_Failure_Reason(reason")
                                          get(audit,0)
                                          if access:audit.primerecord() = level:benign
                                              aud:notes         = 'ACCESSORIES BOOKED IN:-'
                                              save_lac_id = access:loanacc.savefile()
                                              access:loanacc.clearkey(lac:ref_number_key)
                                              lac:ref_number = job:Loan_unit_Number
                                              set(lac:ref_number_key,lac:ref_number_key)
                                              loop
                                                  if access:loanacc.next()
                                                     break
                                                  end !if
                                                  if lac:ref_number <> job:loan_unit_number      |
                                                      then break.  ! end if
                                                  aud:notes   = Clip(aud:notes) & '<13,10>' & Clip(lac:accessory)
                                              end !loop
                                              access:loanacc.restorefile(save_lac_id)
      
                                              aud:notes       = Clip(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                              Loop x# = 1 To Records(glo:queue)
                                                  Get(glo:queue,x#)
                                                  aud:notes       = CLip(aud:notes) & '<13,10>' & Clip(glo:pointer)
                                              End!Loop x# = 1 To Records(glo:queue)
                                              !Save Notes For Report
                                              glo:notes_global    = Clip(aud:notes)
      
                                              aud:ref_number    = job:ref_number
                                              aud:date          = today()
                                              aud:time          = clock()
                                              aud:type          = 'LOA'
                                              access:users.clearkey(use:password_key)
                                              use:password = glo:password
                                              access:users.fetch(use:password_key)
                                              aud:user = use:user_code
                                              Case f_type
                                                  Of 'PRE'
                                                      aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                  Of 'PRI'
                                                      aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                              End !Case f_type
                                              access:audit.insert()
                                          end!if access:audit.primerecord() = level:benign
                                          glo:select1 = job:loan_unit_number
                                          If def:qa_failed_label = 'YES'
                                              QA_Failed_Label_Loan
                                          End!If def:qa_failed_label = 'YES'
                                          If def:qa_failed_report = 'YES'
                                              QA_Failed_Loan
                                          End!If def:qa_failed_report = 'YES'
                                          glo:select1 = ''
                                          glo:notes_global    = ''
                                          Case f_type
                                              Of 'PRE'
                                                  GetStatus(625,1,'LOA')
                                              Else!Case f_type
                                                  GetStatus(615,1,'LOA')
                                          End!Case f_type
      
                                          job:qa_rejected = 'YES'
                                          job:date_qa_rejected    = Today()
                                          job:time_qa_rejected    = Clock()
                                          access:jobs.update()
                                          Do passed_fail_update
      
                                      Of 3   !Cancelled
      
                                      Of 0 !Passed
                                          Case f_type                                                             !to be despatched.
                                              Of 'PRE'
                                                  Do passed_update
                                                  get(audit,0)
                                                  if access:audit.primerecord() = level:benign
                                                      aud:notes         = 'LOAN UNIT NUMBER: ' & Clip(job:Loan_unit_number)
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = today()
                                                      aud:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED (LOA)'
                                                      aud:type          = 'LOA'
                                                      access:audit.insert()
                                                  end!�if access:audit.primerecord() = level:benign
                                                  GetStatus(620,1,'LOA')
      
                                                  access:jobs.update()
      
                                                  access:loan.clearkey(loa:ref_number_key)
                                                  loa:ref_number = job:loan_unit_number
                                                  if access:loan.tryfetch(loa:ref_number_key) = Level:benign
                                                      loa:available = 'QA2'
                                                      access:loan.update()
                                                      get(loanhist,0)
                                                      if access:loanhist.primerecord() = level:benign
                                                          loh:ref_number   = xch:ref_number
                                                          loh:date          = today()
                                                          loh:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          loh:user = use:user_code
                                                          loh:status        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                                          access:loanhist.insert()
                                                      end!if access:exchhist.primerecord() = level:benign
                                                  end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                  If def:QAPassLabel = 1
                                                      glo:Select1  = job:Loan_Unit_Number
                                                      QA_Passed_Label_Loan
                                                      glo:Select1  = ''
                                                  End!If def:QAPassLabel = 1
      
      
                                              Of 'PRI'
                                                  error_courier# = 0
                                                  If job:loan_courier = ''
                                                      Case MessageEx('There is NO Courier attached to this Loan Unit!<13,10><13,10>You will not be able to QA this unit until you have attached one, as it will not be able to be despatched correctly.<13,10><13,10>Do you wish to select a Courier Now?','ServiceBase 2000',|
                                                                     'Styles\stop.ico','|&Select Courier|&Cancel QA',2,2,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                          Of 1 ! &Select Courier Button
                                                              saverequest#      = globalrequest
                                                              globalresponse    = requestcancelled
                                                              globalrequest     = selectrecord
                                                              browse_courier
                                                              if globalresponse = requestcompleted
                                                                  job:loan_courier = cou:courier
                                                              else
                                                                  error_courier# = 1
                                                                  Case MessageEx('This unit has NOT been QA''d at this time.','ServiceBase 2000',|
                                                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                                                      Of 1 ! &OK Button
                                                                  End!Case MessageEx
                                                              end
                                                              globalrequest     = saverequest#
                                                          Of 2 ! &Cancel QA Button
                                                              error_courier# = 1
                                                      End!Case MessageEx
      
                                                  End!If job:loan_courier = ''
                                                  If job:error_Courier# = 1
                                                      do failed_update
                                                  Else!If job:error_Courier# = 1
                                                      do passed_update
      
                                                      job:Loan_Despatched = DespatchANC(job:Loan_Courier,'LOA')
                                                      job:current_courier = job:loan_courier
      
                                                      Access:courier.clearkey(cou:courier_key)
                                                      cou:courier = job:loan_courier
                                                      access:courier.tryfetch(cou:courier_key)
                                                      If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                          job:loaservice = cou:service
                                                      Else!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
                                                          job:loaservice = ''
                                                      End!If cou:courier_type = 'CITY LINK' or cou:courier_type = 'LABEL G'
      
                                                      GetStatus(610,1,'LOA')
      
                                                      get(audit,0)
                                                      if access:audit.primerecord() = level:benign
                                                          aud:notes         = 'LOAN UNIT NUMBER: ' & Clip(job:Loan_unit_number)
                                                          aud:ref_number    = job:ref_number
                                                          aud:date          = today()
                                                          aud:time          = clock()
                                                          access:users.clearkey(use:password_key)
                                                          use:password =glo:password
                                                          access:users.fetch(use:password_key)
                                                          aud:user = use:user_code
                                                          aud:action        = 'RAPID QA UPDATE: MANUAL QA PASSED (LOA)'
                                                          aud:type          = 'LOA'
                                                          access:audit.insert()
                                                      end!�if access:audit.primerecord() = level:benign
                                                      access:jobs.update()
                                                      access:loan.clearkey(loa:ref_number_key)
                                                      loa:ref_number = job:loan_unit_number
                                                      if access:loan.tryfetch(loa:ref_number_key) = Level:benign
                                                          loa:available = 'LOA'
                                                          access:loan.update()
                                                          get(loanhist,0)
                                                          if access:loanhist.primerecord() = level:benign
                                                              loh:ref_number   = xch:ref_number
                                                              loh:date          = today()
                                                              loh:time          = clock()
                                                              access:users.clearkey(use:password_key)
                                                              use:password =glo:password
                                                              access:users.fetch(use:password_key)
                                                              loh:user = use:user_code
                                                              loh:status        = 'UNIT LOANED ON JOB NO: ' & CLip(job:Ref_number)
                                                              access:loanhist.insert()
                                                          end!if access:exchhist.primerecord() = level:benign
                                                      end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                                  End!If job:error_Courier# = 1
                                                  If def:QAPassLabel = 1
                                                      glo:Select1  = job:Loan_Unit_Number
                                                      QA_Passed_Label_Loan
                                                      glo:Select1  = ''
                                                  End!If def:QAPassLabel = 1
      
      
                                          End!Case f_type
                                  
                                  
                                  End!If pass# = 1
                              End!If job:loan_consignment_number = '' And job:despatched <> 'LOA'
                          End!IF handset_type_temp = 'Loan'
      
                      End!If def:qaexchloan <> 'YES'
                  
                  ELse!!If hanset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
                      If ~man:UseQA
                          Case MessageEx('This Manufacturer is not setup to QA Units.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else !If ~man:UseQA
                          If handset_type_temp = 'Repair' Or handset_type_temp = 'Third Party Repair'
                              If job:date_Completed <> ''
                                  Case MessageEx('Cannot QA! The selected job has been completed!','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  error# = 1
                              End!If job:date_Completed = ''
                          End!If handset_type_temp = 'Repair'
      
                          If error# = 0
                              If CheckRefurb()
                                  Case MessageEx('This unit is authorised for Refurbishment.<13,10><13,10>Is the Refurbishment Complete?','ServiceBase 2000',|
                                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Arial',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                      Of 1 ! &Yes Button
                                      Of 2 ! &No Button
                                          error# = 1
                                          job:qa_rejected = 'YES'
                                          job:date_qa_rejected    = Today()
                                          job:time_qa_Rejected    = Clock()
                                          status_audit# = 1
                                          Case f_type
                                              Of 'PRE'
                                                  GetStatus(625,1,'JOB')
                                              Else
                                                  GetStatus(615,1,'JOB')
                                          End!Case f_type
                                          access:jobs.update()
                                          reason" = ''
                                          QA_Failure_Reason(reason")
                                          get(audit,0)
                                          if access:audit.primerecord() = level:benign
                                              aud:notes         = 'REFURBISHMENT NOT COMPLETE'
                                              aud:ref_number    = job:ref_number
                                              aud:date          = today()
                                              aud:time          = clock()
                                              aud:type          = 'JOB'
                                              access:users.clearkey(use:password_key)
                                              use:password = glo:password
                                              access:users.fetch(use:password_key)
                                              aud:user = use:user_code
                                              Case f_type
                                                  Of 'PRE'
                                                      aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                  Of 'PRI'
                                                      aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                              End !Case f_type
                                              access:audit.insert()
                                          end!�if access:audit.primerecord() = level:benign
                                          glo:notes_global    = Clip(Reason")
                                          glo:select1 = job:ref_number
                                          If def:qa_failed_label = 'YES'
                                              QA_Failed_Label
                                          End!If def:qa_failed_label = 'YES'
                                          If def:qa_failed_report = 'YES'
                                              QA_Failed
                                          End!If def:qa_failed_report = 'YES'
                                          glo:select1 = ''
                                          glo:notes_global    = ''
      
      
                                  End!Case MessageEx
                              End!If CheckRefurb()
                              If error# = 0
                                  If job:exchange_unit_number = ''
                                      type# = 1 !Job
                                      Case AccessoryCheck('JOB')
                                          Of 1 orof 2
                                              reason" = ''
                                              QA_Failure_Reason(reason")
      
                                              get(audit,0)
                                              if access:audit.primerecord() = level:benign
                                                  aud:notes         = 'ACCESSORIES BOOKED IN:-'
                                                  save_jac_id = access:jobacc.savefile()
                                                  access:jobacc.clearkey(jac:ref_number_key)
                                                  jac:ref_number = job:Ref_number
                                                  set(jac:ref_number_key,jac:ref_number_key)
                                                  loop
                                                      if access:jobacc.next()
                                                         break
                                                      end !if
                                                      if jac:ref_number <> job:Ref_number      |
                                                          then break.  ! end if
                                                      aud:notes   = Clip(aud:notes) & '<13,10>' & Clip(jac:accessory)
                                                  end !loop
                                                  access:jobacc.restorefile(save_jac_id)
      
                                                  aud:notes       = Clip(aud:notes) & '<13,10,13,10>ACCESSORIES BOOKED OUT:-'
                                                  Loop x# = 1 To Records(glo:queue)
                                                      Get(glo:queue,x#)
                                                      aud:notes       = CLip(aud:notes) & '<13,10>' & Clip(glo:pointer)
                                                  End!Loop x# = 1 To Records(glo:queue)
                                                  !Save Notes For Report
                                                  glo:notes_global    = Clip(aud:notes)
      
                                                  aud:ref_number    = job:ref_number
                                                  aud:date          = today()
                                                  aud:time          = clock()
                                                  aud:type          = 'JOB'
                                                  access:users.clearkey(use:password_key)
                                                  use:password = glo:password
                                                  access:users.fetch(use:password_key)
                                                  aud:user = use:user_code
                                                  Case f_type
                                                      Of 'PRE'
                                                          aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                      Of 'PRI'
                                                          aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                                  End !Case f_type
                                                  access:audit.insert()
                                              end!if access:audit.primerecord() = level:benign
                                              glo:select1 = job:ref_number
                                              If def:qa_failed_label = 'YES'
                                                  QA_Failed_Label
                                              End!If def:qa_failed_label = 'YES'
                                              If def:qa_failed_report = 'YES'
                                                  QA_Failed
                                              End!If def:qa_failed_report = 'YES'
                                              glo:select1 = ''
                                              glo:notes_global    = ''
                                              Case f_type
                                                  Of 'PRE'
                                                      GetStatus(625,1,'JOB')
                                                  Else!Case f_type
                                                      GetStatus(615,1,'JOB')
                                              End!Case f_type
      
                                              job:qa_rejected = 'YES'
                                              job:date_qa_rejected    = Today()
                                              job:time_qa_rejected    = Clock()
                                              access:jobs.update()
                                              Do passed_fail_update
      
                                      Of 3
                                          pass# = 0
                                      Of 0
                                          pass# = 1
                                      End!If AccessoryCheck('EXCHANGE')
                                  Else!If job:exchange_uit_number = ''
                                      pass# = 1
                                  End!If job:exchange_uit_number = ''
                              End!If error# = 0
      
                              If pass# = 1
                                  pass# = Local.ValidateParts()
                              End !If pass# = 1
      
                              If pass# = 1
                                  pass# = Local.ValidateNetwork()
                              End !If pass# = 1
      
                              If pass# = 1                                                                    !If passed fill in all the relevant
      
                                  Case f_type
                                      Of 'PRE'
                                          ! Start Change 3684 BE(10/12/03)
                                          IF (Is14DayRepackage) THEN
                                              CreateNewJob()
                                          END
                                          ! End Change 3684 BE(10/12/03)
                                          GetStatus(620,1,'JOB')
                                          get(audit,0)
                                          if access:audit.primerecord() = level:benign
                                              aud:notes         = ''
                                              aud:ref_number    = job:ref_number
                                              aud:date          = today()
                                              aud:time          = clock()
                                              access:users.clearkey(use:password_key)
                                              use:password =glo:password
                                              access:users.fetch(use:password_key)
                                              aud:user = use:user_code
                                              aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA PASSED'
                                              access:audit.insert()
                                          end!�if access:audit.primerecord() = level:benign
                                          If def:QAPassLabel = 1
                                              glo:Select1  = job:Ref_Number
                                              QA_Passed_Label
                                              glo:Select1  = ''
                                          End!If def:QAPassLabel = 1
                                          access:jobs.update()
      
                                          Do passed_update
      
      
                                      Of 'PRI'
                                          !Auto complete the job... will need to default this later
                                          Complete# = 0
                                          CompleteError# = 0
                                          If man:UseQA
                                              If man:QAAtCompletion
                                                  glo:ErrorText = ''
                                                  CompulsoryFieldCheck('C')
                                                  If glo:ErrorText <> ''
                                                      glo:ErrorText = 'QA FAILURE! You cannot QA because of the following error(s): <13,10>' & Clip(glo:ErrorText)
                                                      Error_Text
                                                      glo:errortext = ''
      
                                                      reason" = ''
                                                      QA_Failure_Reason(reason")
                                                      CompleteError# = 1
                                                      
                                                      get(audit,0)
                                                      if access:audit.primerecord() = level:benign
                                                          aud:Notes         = 'FAILED COMPLETION CHECK'
                                                          aud:ref_number    = job:ref_number
                                                          aud:date          = today()
                                                          aud:time          = clock()
                                                          aud:type          = 'JOB'
                                                          access:users.clearkey(use:password_key)
                                                          use:password = glo:password
                                                          access:users.fetch(use:password_key)
                                                          aud:user = use:user_code
                                                          Case f_type
                                                              Of 'PRE'
                                                                  aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                                              Of 'PRI'
                                                                  aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                                                          End !Case f_type
                                                          access:audit.insert()
                                                      end!if access:audit.primerecord() = level:benign
                                                      glo:select1 = job:ref_number
                                                      If def:qa_failed_label = 'YES'
                                                          QA_Failed_Label
                                                      End!If def:qa_failed_label = 'YES'
                                                      If def:qa_failed_report = 'YES'
                                                          QA_Failed
                                                      End!If def:qa_failed_report = 'YES'
                                                      glo:select1 = ''
                                                      glo:notes_global    = ''
                                                      GetStatus(615,1,'JOB')
      
                                                      job:qa_rejected = 'YES'
                                                      job:date_qa_rejected    = Today()
                                                      job:time_qa_rejected    = Clock()
                                                      access:jobs.update()
                                                      Do passed_fail_update
      
                                                  Else
                                                      Complete# = 1
                                                  End !If glo:ErrorText <> ''
                                              Else
                                                  GetStatus(610,1,'JOB')
                                              End !If man:QAAtCompletion
      
                                              If CompleteError# = 0
      
                                                  ! Start Change 3684 BE(10/12/03)
                                                  IF (Is14DayRepackage) THEN
                                                      CreateNewJob()
                                                  END
                                                  ! End Change 3684 BE(10/12/03)
      
                                                  job:qa_passed      = 'YES'
                                                  job:date_qa_passed = Today()
                                                  job:time_qa_passed = Clock()
      
                                                  get(audit,0)
                                                  if access:audit.primerecord() = level:benign
                                                      aud:notes         = ''
                                                      aud:ref_number    = job:ref_number
                                                      aud:date          = today()
                                                      aud:time          = clock()
                                                      access:users.clearkey(use:password_key)
                                                      use:password =glo:password
                                                      access:users.fetch(use:password_key)
                                                      aud:user = use:user_code
                                                      aud:action        = 'RAPID QA UPDATE: MANUAL QA PASSED'
                                                      access:audit.insert()
                                                  end!�if access:audit.primerecord() = level:benign
                                                  If def:QAPassLabel = 1
                                                      glo:Select1  = job:Ref_Number
                                                      QA_Passed_Label
                                                      glo:Select1  = ''
                                                  End!If def:QAPassLabel = 1
      
                                                  If Complete# = 1
                                                      If job:ignore_chargeable_charges <> 'YES'
                                                          Pricing_Routine('C',labour",parts",pass",discount")
                                                          If pass" = True
                                                              job:labour_cost = labour"
                                                              job:parts_cost  = parts"
                                                          Else
                                                              job:labour_cost = 0
                                                              job:parts_cost  = 0
                                                          End!If pass" = True
                                                      End! If job:ignore_chargeable_charges <> 'YES'
                                                  
                                                      If job:ignore_warranty_charges <> 'YES'
                                                          Pricing_Routine('W',labour",parts",pass",discount")
                                                          If pass" = True
                                                              job:labour_cost_warranty    = labour"
                                                              job:parts_cost_warranty     = parts"
                                                          Else
                                                              job:labour_cost_warranty = 0
                                                              job:parts_cost_warranty = 0
                                                          End!If pass_warranty" = True
                                                      End!If job:ignore_warranty_charges <> 'YES'
      
                                                      !Text too long, so will call a procedure
                                                      Do CompletedBit
      
                                                      !ThisMakeover.SetWindow(win:form)
                                                  End !If Complete# = 1
                                                  Access:JOBS.Update()
                                                  !Do closing
                                                  Do Passed_Update
      
                                              End !If CompleteError# = 0
                                              
      
                                          End !If man:UseQA
      
                                  End!Case f_type
                              End!If pass# = 1
                          Else!If error# = 0
                              Do failed_update
                          End!If error# = 0
                      End !If ~man:UseQA
                  End!If hanset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
              End !If f_Type = 'PRE' And ~man:UseElectronicQA
          Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Case MessageEx('Error! Cannot find the Manufacturer attached to this job!','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      
      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
      Display()
      Select(?job_number_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?QA_PAss, Accepted)
    OF ?qa_fail
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?qa_fail, Accepted)
      ThisWindow.PostCompleted()
      Set(Defaults)
      access:Defaults.next()
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = job_number_temp
      if access:jobs.fetch(job:ref_number_key) = Level:Benign
          error# = 0
          Access:MANUFACT.ClearKey(man:Manufacturer_Key)
          man:Manufacturer = job:Manufacturer
          If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Found
              If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
                  If ~man:QALoanExchange
                      error# = 1
                      Case MessageEx('This Manufacturer is not setup to QA Loan/Exchange Units.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  Else!If def:qaexchLoan <> 'YES'
                      error# = 1
                      If handset_type_temp = 'Exchange'                                               !Is it an Exchange that was scanned?
                          If job:exchange_consignment_number = ''                                     !Check that is isn't already ready
                              Do passed_fail_update
                              reason" = ''
                              qa_failure_reason(reason")
                              glo:notes_global = Clip(reason")
                              If def:qa_failed_label = 'YES'
                                  glo:select1  = job:exchange_unit_number
                                  QA_Failed_Label_Exchange
                                  glo:select1  = ''
                              End!If def:qa_failed_label = 'YES'
                              If def:qa_failed_report = 'YES'
                                  glo:select1 = job:exchange_unit_number
                                  QA_Failed_Exchange
                                  glo:select1 = ''
                              End!If def:qa_failed_report = 'YES'
                              glo:notes_global = ''
      
                              job:exchange_status = 'QA FAILED'
                              job:despatched = ''
                              job:despatch_type = ''
                              Case f_type
                                  Of 'PRE'
                                      GetStatus(625,1,'EXC') !electronic qa rejected
      
                                  Of 'PRI'
                                      GetStatus(615,1,'EXC') !manual qa rejected
      
                              End!Case f_type
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'EXCHANGE UNIT NUMBER: ' & Clip(job:Exchange_unit_number) &|
                                                      '<13,10>QA FAILURE REASON: ' & CLip(reason")
                                  aud:ref_number    = job:ref_number
                                  aud:date          = today()
                                  aud:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  Case f_type
                                      Of 'PRE'
                                          aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA FAILED (EXC)'
                                      Of 'PRI'
                                          aud:action        = 'RAPID QA UPDATE: MANUAL QA FAILED (EXC)'
                                  End!Case f_type
                                  aud:type      = 'EXC'
                                  
                                  access:audit.insert()
                              end!�if access:audit.primerecord() = level:benign
                              access:jobs.update()
                              access:exchange.clearkey(xch:ref_number_key)
                              xch:ref_number = job:exchange_unit_number
                              if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                                  xch:available = 'QAF'
                                  access:exchange.update()
                                  get(exchhist,0)
                                  if access:exchhist.primerecord() = level:benign
                                      exh:ref_number   = xch:ref_number
                                      exh:date          = today()
                                      exh:time          = clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      exh:user = use:user_code
                                      exh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLip(Reason")
                                      access:exchhist.insert()
                                  end!if access:exchhist.primerecord() = level:benign
                              end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                          End!If job:exchange_consignment_number = '' And job:despatched <> 'EXC'
                      End!If handset_type_temp = 'Exchange'
                      IF handset_type_temp = 'Loan'
                          If job:loan_consignment_number = '' 
                              Do passed_fail_update
                              reason" = ''
                              qa_failure_reason(reason")
                              glo:notes_global = Clip(reason")
                              If def:qa_failed_label = 'YES'
                                  glo:select1  = job:loan_unit_number
                                  QA_Failed_Label_Loan
                                  glo:select1  = ''
                              End!If def:qa_failed_label = 'YES'
                              If def:qa_failed_report = 'YES'
                                  glo:select1 = job:loan_unit_number
                                  QA_Failed_Loan
                                  glo:select1 = ''
                              End!If def:qa_failed_report = 'YES'
                              glo:notes_global = ''
      
                              job:loan_status = 'QA FAILED'
                              job:despatched = ''
                              job:despatch_type = ''
                              Case f_type
                                  Of 'PRE'
                                      GetStatus(625,1,'LOA') !electronic qa rejected
      
                                  Of 'PRI'
                                      GetStatus(615,1,'LOA') !electronic qa rejected
      
                              End!Case f_type
                              job:status_end_date = end_date"
                              job:status_end_time = end_time"
                              get(audit,0)
                              if access:audit.primerecord() = level:benign
                                  aud:notes         = 'LOAN UNIT NUMBER: ' & Clip(job:Loan_unit_number) &|
                                                      '<13,10>QA FAILURE REASON: ' & CLip(reason")
                                  aud:ref_number    = job:ref_number
                                  aud:date          = today()
                                  aud:time          = clock()
                                  access:users.clearkey(use:password_key)
                                  use:password =glo:password
                                  access:users.fetch(use:password_key)
                                  aud:user = use:user_code
                                  Case f_type
                                      Of 'PRE'
                                          aud:action        = 'RAPID QA UPDATE: ELECTRONIC QA FAILED (LOA)'
                                      Of 'PRI'
                                          aud:action        = 'RAPID QA UPDATE: MANUAL QA FAILED (LOA)'
                                  End!Case f_type
                                  aud:type = 'LOA'
                                  
                                  access:audit.insert()
                              end!�if access:audit.primerecord() = level:benign
                              access:jobs.update()
                              access:loan.clearkey(loa:ref_number_key)
                              loa:ref_number = job:loan_unit_number
                              if access:loan.tryfetch(loa:ref_number_key) = Level:benign
                                  loa:available = 'QAF'
                                  access:loan.update()
                                  get(loanhist,0)
                                  if access:loanhist.primerecord() = level:benign
                                      loh:ref_number   = xch:ref_number
                                      loh:date          = today()
                                      loh:time          = clock()
                                      access:users.clearkey(use:password_key)
                                      use:password =glo:password
                                      access:users.fetch(use:password_key)
                                      loh:user = use:user_code
                                      loh:status        = 'RAPID QA UPDATE: QA FAILED - ' & CLip(Reason")
                                      access:loanhist.insert()
                                  end!if access:exchhist.primerecord() = level:benign
                              end!if access:exchange.tryfetch(xch:ref_number_key) = Level:benign
                          End!If job:loan_consignment_number = '' And job:despatched <> 'LOA'
                      End!IF handset_type_temp = 'Loan'
      
                  End!If def:qaexchLoan <> 'YES'
              
              Else!!If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
                  If job:date_completed <> ''
                      error# = 0
                      Case MessageEx('Cannot QA! The selected job has been completed.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                  End!If job:date_completed <> ''
                  If error# = 0
                      job:qa_rejected      = 'YES'
                      job:date_qa_rejected = Today()
                      job:time_qa_rejected = Clock()
                      Case f_type
                          Of 'PRE'
                              GetStatus(625,1,'JOB') !electronic qa rejected
                          Of 'PRI'
                              GetStatus(615,1,'JOB') !manual qa rejected
                      End!Case f_type
      
                      access:jobs.update()
                      Do passed_fail_update
      
                      beep(beep:systemasterisk)
                      reason" = ''
                      qa_failure_reason(reason")
                      get(audit,0)
                      if access:audit.primerecord() = level:benign
                          aud:notes         = ''
                          aud:ref_number    = job:ref_number
                          aud:date          = today()
                          aud:time          = clock()
                          access:users.clearkey(use:password_key)
                          use:password =glo:password
                          access:users.fetch(use:password_key)
                          aud:user = use:user_code
                          Case f_type
                              Of 'PRE'
                                  aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                              Of 'PRI'
                                  aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                          End !Case f_type
                          
                          access:audit.insert()
                      end!�if access:audit.primerecord() = level:benign
                      glo:notes_global = Clip(reason")
                      If def:qa_failed_label = 'YES'
                          glo:select1  = job:ref_number
                          QA_Failed_Label
                          glo:select1  = ''
                      End!If def:qa_failed_label = 'YES'
                      If def:qa_failed_report = 'YES'
                          glo:select1 = job:ref_number
                          QA_Failed
                          glo:select1 = ''
                      End!If def:qa_failed_report = 'YES'
                      glo:notes_global = ''
                  Else!If error# = 0
                      Do failed_update
                  End!If error# = 0
              End!If handset_type_temp = 'Exchange' Or handset_type_temp = 'Loan'
      
          Else!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
              Case MessageEx('Error! Cannot find the Manufacturer attached to this job.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Arial',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If Access:MANUFACT.TryFetch(man:Manufacturer_Key) = Level:Benign
      end!if access:jobs.fetch(job:ref_number_key) = Level:Benign
      Display()
      job_number_temp = ''
      model_details_temp = ''
      handset_type_temp  = ''
      date_booked_temp   = ''
      serial_number_temp = ''
      Display()
      Select(?job_number_temp)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?qa_fail, Accepted)
    OF ?FaultCodes
      ThisWindow.Update
      Fault_Codes_Window(0)
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FaultCodes, Accepted)
      Access:Jobs.Update()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FaultCodes, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_QA')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case KeyCode()
          Of F5Key
              If ?FaultCodes{prop:Disable} = 0
                  Post(Event:Accepted,?FaultCodes)
              End !If ?FaultCodes{prop:Disable} = 0
          Of F9Key
              If ?QA_Pass{prop:Disable} = 0
                  Post(Event:Accepted,?QA_Pass)
              End !If ?QA_Pass{prop:Disable} = 0
          Of F10Key
              If ?QA_Fail{prop:Disable} = 0
                  Post(Event:Accepted,?QA_Fail)
              End !If ?QA_Fail{prop:Disable} = 0
          Of EscKey
              Post(Event:Accepted,?Finish)
      End !KeyCode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Case f_type
          Of 'PRE'
              0{prop:text} = 'Rapid QA Update: Electronic'
          Of 'PRI'
              0{prop:text} = 'Rapid QA Update: Manual'
      End!Case f_type
        IF GetComputerName(JB:ComputerName,JB:ComputerLen).
        tmp:WorkstationName=JB:ComputerName
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
CreateNewJob     Procedure()    ! Start Change 3684 BE(10/12/03)
    CODE
    IF (access:jobs_alias.primerecord() = level:benign) THEN
        jobref# = job_ali:ref_number

        job_ali:Record :=: job:Record

        job_ali:ref_number = jobref#

        ! Start Change 4276 BE(11/05/04)
        job_ali:Date_Booked                 = TODAY()
        job_ali:Time_Booked                 = CLOCK()
        ! End Change 4276 BE(11/05/04)

        job_ali:Engineer                    = ''
        job_ali:On_Test                     = 'NO'
        job_ali:Date_On_Test                = ''
        job_ali:Time_On_Test                = ''
        job_ali:In_repair                   = 'NO'
        job_ali:Date_In_Repair              = ''
        job_ali:Time_In_Repair              = ''
        job_ali:Insurance_Reference_Number  = ''
        job_ali:Authority_Number            = ''
        job_ali:despatch_user               = ''
        job_ali:despatch_number             = ''

        job_ali:loan_despatched             = ''
        job_ali:loan_despatch_number        = ''
        job_ali:loan_courier                = ''
        job_ali:loan_consignment_number     = ''
        job_ali:loan_despatched_user        = ''
        job_ali:loaservice                  = ''
        job_ali:loan_unit_number            = ''
        job_ali:loan_user                   = ''

        job_ali:exchange_despatched         = ''
        job_ali:exchange_despatch_number    = ''
        job_ali:exchange_courier            = ''
        job_ali:exchange_consignment_number = ''
        job_ali:exchange_despatched_user    = ''
        job_ali:excservice                  = ''
        job_ali:exchange_unit_number        = ''
        job_ali:exchange_user               = ''

        access:trantype.clearkey(trt:transit_type_key)
        trt:transit_type = job:transit_type
        IF (access:trantype.tryfetch(trt:transit_type_key) = Level:Benign) THEN
            job:workshop = trt:InWorkshop
            job_ali:current_status = trt:initial_status
            job_ali:exchange_status = trt:Exchangestatus
            job_ali:loan_status = trt:Loanstatus
        ELSE
            job_ali:current_status = ''
            job_ali:exchange_status = ''
            job_ali:loan_status = ''
        END

        job_ali:PreviousStatus = 'ERROR'

        IF (access:jobstage.primerecord() = Level:Benign) THEN
            jst:ref_number  = job_ali:ref_number
            jst:job_stage   = job_ali:current_status
            jst:date        = Today()
            jst:time        = Clock()
            access:users.clearkey(use:password_key)
            use:password =glo:password
            access:users.fetch(use:password_key)
            jst:user        = use:user_code
            IF (access:jobstage.tryinsert() <> Level:Benign) THEN
                access:jobstage.cancelautoinc()
            END
        END

        ! BE 22/01/2004
        !job_ali:Account_Number = rpg:OriginalAccount
        !job_ali:Warranty_Charge_Type = rpg:OriginalChargeType
        job_ali:Warranty_Job   = 'NO'
        job_ali:Chargeable_Job = 'YES'
        job_ali:Charge_Type    = rpg:OriginalChargeType
        job_ali:Account_Number = GetOriginalAccountNumber(job:ref_number)

        IF (access:audit.primerecord() = level:benign) THEN
            aud:notes         = 'JOB CREATED THROUGH THE 14-DAY REPACKAGING PROCESS' & |
                                '<13,10>OLD JOB NUMBER: ' & CLIP(job:Ref_Number)
            aud:ref_number    = job_ali:ref_number
            aud:date          = Today()
            aud:time          = Clock()
            access:users.clearkey(use:password_key)
            use:password =glo:password
            access:users.fetch(use:password_key)
            aud:user = use:user_code
            aud:action        = '14-DAY REPACKAGING(B)'
            IF (access:audit.insert() <> Level:Benign) THEN
                access:audit.cancelautoinc()
            end
        END

        IF (Access:AUDSTATS.Primerecord() = Level:Benign) THEN
            aus:RefNumber    = job_ali:Ref_Number
            aus:Type         = 'JOB'
            aus:DateChanged  = Today()
            aus:TimeChanged  = Clock()
            aus:OldStatus    = job_ali:PreviousStatus
            aus:NewStatus    = job_ali:Current_Status
            access:users.clearkey(use:password_key)
            use:password =glo:password
            access:users.fetch(use:password_key)
            aus:UserCode     = use:User_Code
            IF (Access:AUDSTATS.Tryinsert() <> Level:Benign) THEN
                Access:AUDSTATS.Cancelautoinc()
            END
        END

        IF (Access:AUDSTATS.Primerecord() = Level:Benign) THEN
            aus:RefNumber    = job_ali:Ref_Number
            aus:Type         = 'EXC'
            aus:DateChanged  = Today()
            aus:TimeChanged  = Clock()
            aus:OldStatus    = ''
            aus:NewStatus    = job_ali:Exchange_Status
            access:users.clearkey(use:password_key)
            use:password =glo:password
            access:users.fetch(use:password_key)
            aus:UserCode     = use:User_Code
            IF (Access:AUDSTATS.Tryinsert() <> Level:Benign) THEN
                Access:AUDSTATS.Cancelautoinc()
            END
        END

        IF (Access:AUDSTATS.Primerecord() = Level:Benign) THEN
            aus:RefNumber    = job_ali:Ref_Number
            aus:Type         = 'LOA'
            aus:DateChanged  = Today()
            aus:TimeChanged  = Clock()
            aus:OldStatus    = ''
            aus:NewStatus    = job_ali:Loan_Status
            access:users.clearkey(use:password_key)
            use:password =glo:password
            access:users.fetch(use:password_key)
            aus:UserCode     = use:User_Code
            IF (Access:AUDSTATS.Tryinsert() <> Level:Benign) THEN
                Access:AUDSTATS.Cancelautoinc()
            END
        END

        IF (access:jobs_alias.insert() <> Level:benign) THEN
            access:jobs_alias.cancelautoinc()
        END

        Access:JOBSE.ClearKey(jobe:RefNumberKey)
        jobe:RefNumber = job:Ref_Number
        IF (access:JOBSE.fetch(jobe:RefNumberKey) = Level:Benign) THEN
            jsr:RefNumber           = job_ali:Ref_Number
            jsr:JobMark             = jobe:JobMark
            jsr:TraFaultCode1       = jobe:TraFaultCode1
            jsr:TraFaultCode2       = jobe:TraFaultCode2
            jsr:TraFaultCode3       = jobe:TraFaultCode3
            jsr:TraFaultCode4       = jobe:TraFaultCode4
            jsr:TraFaultCode5       = jobe:TraFaultCode5
            jsr:TraFaultCode6       = jobe:TraFaultCode6
            jsr:TraFaultCode7       = jobe:TraFaultCode7
            jsr:TraFaultCode8       = jobe:TraFaultCode8
            jsr:TraFaultCode9       = jobe:TraFaultCode9
            jsr:TraFaultCode10      = jobe:TraFaultCode10
            jsr:TraFaultCode11      = jobe:TraFaultCode11
            jsr:TraFaultCode12      = jobe:TraFaultCode12
            jsr:SIMNumber           = jobe:SIMNumber
            jsr:JobReceived         = jobe:JobReceived
            jsr:Skilllevel          = jobe:Skilllevel
            jsr:UPSFlagCode         = jobe:UPSFlagCode
            jsr:FailedDelivery      = jobe:FailedDelivery
            jsr:CConfirmSecondEntry = jobe:CConfirmSecondEntry
            jsr:WConfirmSecondEntry = jobe:WConfirmSecondEntry
            jsr:EndUserEmailAddress = jobe:EndUserEmailAddress
            jsr:Network             = jobe:Network
            !jsr:ExchangeReason      = jobe:ExchangeReason
            !jsr:LoanReason          = jobe:LoanReason
            jsr:ExchangeReason      = ''
            jsr:LoanReason          = ''
            IF (job_ali:Workshop = 'YES') THEN
                jsr:InWorkshopDate      = jobe:InWorkshopDate
                jsr:InWorkshopTime      = jobe:InWorkshopTime
            ELSE
                jsr:InWorkshopDate      = ''
                jsr:InWorkshopTime      = ''
            END
            jsr:Pre_Rf_Board_IMEI   = jobe:Pre_Rf_Board_IMEI

            IF (access:JOBSE.primerecord() = level:benign) THEN
                jobe:RefNumber           = jsr:RefNumber
                jobe:JobMark             = jsr:JobMark
                jobe:TraFaultCode1       = jsr:TraFaultCode1
                jobe:TraFaultCode2       = jsr:TraFaultCode2
                jobe:TraFaultCode3       = jsr:TraFaultCode3
                jobe:TraFaultCode4       = jsr:TraFaultCode4
                jobe:TraFaultCode5       = jsr:TraFaultCode5
                jobe:TraFaultCode6       = jsr:TraFaultCode6
                jobe:TraFaultCode7       = jsr:TraFaultCode7
                jobe:TraFaultCode8       = jsr:TraFaultCode8
                jobe:TraFaultCode9       = jsr:TraFaultCode9
                jobe:TraFaultCode10      = jsr:TraFaultCode10
                jobe:TraFaultCode11      = jsr:TraFaultCode11
                jobe:TraFaultCode12      = jsr:TraFaultCode12
                jobe:SIMNumber           = jsr:SIMNumber
                jobe:JobReceived         = jsr:JobReceived
                jobe:Skilllevel          = jsr:Skilllevel
                jobe:UPSFlagCode         = jsr:UPSFlagCode
                jobe:FailedDelivery      = jsr:FailedDelivery
                jobe:CConfirmSecondEntry = jsr:CConfirmSecondEntry
                jobe:WConfirmSecondEntry = jsr:WConfirmSecondEntry
                jobe:EndUserEmailAddress = jsr:EndUserEmailAddress
                jobe:Network             = jsr:Network
                jobe:ExchangeReason      = jsr:ExchangeReason
                jobe:LoanReason          = jsr:LoanReason
                jobe:InWorkshopDate      = jsr:InWorkshopDate
                jobe:InWorkshopTime      = jsr:InWorkshopTime
                jobe:Pre_Rf_Board_IMEI   = jsr:Pre_Rf_Board_IMEI
                IF (access:JOBSE.insert() <> Level:benign) THEN
                        access:JOBSE.cancelautoinc()
                END
            END
        END

        Access:JOBNOTES.ClearKey(jbn:RefNumberKey)
        jbn:RefNumber = job:Ref_Number
        IF (access:JOBNOTES.fetch(jbn:RefNumberKey) = Level:Benign) THEN

            jnr:RefNumber         = job_ali:Ref_Number
            jnr:Fault_Description = jbn:Fault_Description
            !jnr:Engineers_Notes   = jbn:Engineers_Notes
            jnr:Engineers_Notes   = ''
            !jnr:Invoice_Text      = jbn:Invoice_Text
            jnr:Invoice_Text      = ''
            jnr:Collection_Text   = jbn:Collection_Text
            jnr:Delivery_Text     = jbn:Delivery_Text
            jnr:ColContactName    = jbn:ColContatName
            jnr:ColDepartment     = jbn:ColDepartment
            jnr:DelContactName    = jbn:DelContactName
            jnr:DelDepartment     = jbn:DelDepartment

            IF (access:JOBNOTES.primerecord() = level:benign) THEN
                jbn:RefNumber         = jnr:RefNumber 
                jbn:Fault_Description = jnr:Fault_Description
                jbn:Engineers_Notes   = jnr:Engineers_Notes
                jbn:Invoice_Text      = jnr:Invoice_Text
                jbn:Collection_Text   = jnr:Collection_Text
                jbn:Delivery_Text     = jnr:Delivery_Text
                jbn:ColContatName     = jnr:ColContactName
                jbn:ColDepartment     = jnr:ColDepartment
                jbn:DelContactName    = jnr:DelContactName
                jbn:DelDepartment     = jnr:DelDepartment
                IF (access:JOBNOTES.insert() <> Level:benign) THEN
                    access:JOBNOTES.cancelautoinc()
                END
            END

        END

        ! Copy Chargeable Parts
        !access:PARTS_ALIAS.Clearkey(par_ali:part_number_key)
        !par_ali:Ref_Number = job:Ref_Number
        !SET(par_ali:Part_Number_Key,par_ali:Part_Number_Key)
        !LOOP
        !    IF ((access:parts_alias.next() <> Level:Benign) OR (par_ali:Ref_Number <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !    IF (access:PARTS.PrimeRecord() = Level:benign) THEN
        !       recno# = par:Record_Number
        !        par:Record :=: par_ali:record
        !        par:Record_Number = recno#
        !        par:Ref_Number = job_ali:Ref_Number
        !        IF (access:PARTS.insert() <> Level:benign) THEN
        !            access:PARTS.cancelautoinc()
        !       END
        !   END
        !END

        ! Copy Warranty Parts
        !access:WARPARTS_ALIAS.Clearkey(war_ali:part_number_key)
        !war_ali:Ref_Number = job:Ref_Number
        !SET(war_ali:Part_Number_Key,war_ali:Part_Number_Key)
        !LOOP
        !    IF ((access:warparts_alias.next() <> Level:Benign) OR (war_ali:Ref_Number <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !    IF (access:WARPARTS.PrimeRecord() = Level:benign) THEN
        !        recno# = wpr:Record_Number
        !        wpr:Record :=: war_ali:record
        !        wpr:Record_Number = recno#
        !        wpr:Ref_Number = job_ali:Ref_Number
        !        IF (access:WARPARTS.insert() <> Level:benign) THEN
        !            access:WARPARTS.cancelautoinc()
        !        END
        !    END
        !END

        ! BE 22/01/2004
        !save_wpr_id = access:warparts.savefile()
        !access:warparts.clearkey(wpr:part_number_key)
        !wpr:ref_number  = job_ali:ref_number
        !SET(wpr:part_number_key,wpr:part_number_key)
        !LOOP
        !    IF ((access:warparts.next() <> Level:benign) OR (wpr:ref_number <> job:ref_number)) THEN
        !       BREAK
        !    END
        !    IF (access:parts.primerecord() = Level:Benign) THEN
        !        record_number#  = par:record_number
        !        par:record      :=: wpr:record
        !        par:record_number   = record_number#
        !        IF (access:parts.insert() <> level:Benign) THEN
        !            access:parts.cancelautoinc()
        !        END
        !    END
        !    DELETE(warparts)
        !END
        !ccess:warparts.restorefile(save_wpr_id)

        ! Copy Accessories
        Access:JOBACC.ClearKey(jac:Ref_Number_Key)
        jac:Ref_Number = job:Ref_Number
        SET(jac:Ref_Number_Key,jac:Ref_Number_Key)
        LOOP
            IF ((Access:JOBACC.NEXT() <> Level:benign) OR (jac:Ref_Number <> job:Ref_Number)) THEN
                BREAK
            END

            jar:Ref_Number = job_ali:Ref_Number
            jar:Accessory  = jac:Accessory

            bookmark = access:JOBACC.Savefile()

            IF (access:JOBACC.PrimeRecord() = Level:benign) THEN
                jac:Ref_Number = jar:Ref_Number
                jac:Accessory  = jar:Accessory
                IF (access:JOBACC.insert() <> Level:benign) THEN
                    access:JOBACC.cancelautoinc()
                END
            END

            access:JOBACC.Restorefile(bookmark)
        END

        ! Copy Jobstage
        !Access:JOBSTAGE.ClearKey(jst:Ref_Number_Key)
        !jst:Ref_Number = job:Ref_Number
        !SET(jst:Ref_Number_Key,jst:Ref_Number_Key)
        !LOOP
        !    IF ((Access:JOBSTAGE.NEXT() <> Level:benign) OR (jst:Ref_Number <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !
        !    jsr:Ref_Number = job_ali:Ref_Number
        !    jsr:Job_Stage  = jst:Job_Stage
        !    jsr:Date       = jst:Date
        !    jsr:Time       = jst:Time
        !    jsr:User       = jst:User
        !
        !    bookmark = Access:JOBSTAGE.Savefile()
        !
        !    IF (Access:JOBSTAGE.PrimeRecord() = Level:benign) THEN
        !
        !        jst:Ref_Number = jsr:Ref_Number
        !        jst:Job_Stage  = jsr:Job_Stage
        !        jst:Date       = jsr:Date
        !        jst:Time       = jsr:Time
        !        jst:User       = jsr:User
        !
        !        IF (Access:JOBSTAGE.insert() <> Level:benign) THEN
        !            Access:JOBSTAGE.cancelautoinc()
        !        END
        !    END
        !
        !    Access:JOBSTAGE.Restorefile(bookmark)
        !END

        ! Copy Audstats
        !Access:AUDSTATS.ClearKey(aus:DateChangedKey)
        !aus:RefNumber = job:Ref_Number
        !SET(aus:DateChangedKey,aus:DateChangedKey)
        !LOOP
        !    IF ((Access:AUDSTATS.NEXT() <> Level:benign) OR (aus:RefNumber <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !
        !    asr:RefNumber    = job_ali:Ref_Number
        !    asr:Type         = aus:Type
        !    asr:DateChanged  = aus:DateChanged
        !    asr:TimeChanged  = aus:TimeChanged
        !    asr:OldStatus    = aus:OldStatus
        !    asr:NewStatus    = aus:NewStatus
        !    asr:UserCode     = aus:UserCode
        !
        !    bookmark = access:AUDSTATS.Savefile()
        !
        !    IF (Access:AUDSTATS.PrimeRecord() = Level:benign) THEN
        !
        !        aus:RefNumber    = asr:RefNumber
        !        aus:Type         = asr:Type
        !        aus:DateChanged  = asr:DateChanged
        !        aus:TimeChanged  = asr:TimeChanged
        !        aus:OldStatus    = asr:OldStatus
        !        aus:NewStatus    = asr:NewStatus
        !        aus:UserCode     = asr:UserCode
        !
        !        IF (Access:AUDSTATS.insert() <> Level:benign) THEN
        !            Access:AUDSTATS.cancelautoinc()
        !        END
        !    END
        !
        !    access:AUDSTATS.Restorefile(bookmark)
        !END

        ! Copy Audit
        !Access:AUDIT.ClearKey(aud:Ref_Number_Key)
        !aud:Ref_Number = job:Ref_Number
        !SET(aud:Ref_Number_Key,aud:Ref_Number_Key)
        !LOOP
        !    IF ((Access:AUDIT.NEXT() <> Level:benign) OR (aud:Ref_Number <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !
        !    air:Ref_Number   = job_ali:Ref_Number
        !    air:Date         = aud:Date
        !    air:Time         = aud:Time
        !    air:User         = aud:User
        !    air:Action       = aud:Action
        !    air:DummyField   = aud:DummyField
        !    air:Notes        = aud:Notes
        !    air:Type         = aud:Type
        !
        !    bookmark = Access:AUDIT.Savefile()
        !
        !    IF (Access:AUDIT.PrimeRecord() = Level:benign) THEN
        !
        !        aud:Ref_Number   = air:Ref_Number
        !        aud:Date         = air:Date
        !        aud:Time         = air:Time
        !        aud:User         = air:User
        !        aud:Action       = air:Action
        !        aud:DummyField   = air:DummyField
        !        aud:Notes        = air:Notes
        !        aud:Type         = air:Type
        !
        !        IF (Access:AUDIT.insert() <> Level:benign) THEN
        !            Access:AUDIT.cancelautoinc()
        !        END
        !    END
        !
        !    Access:AUDIT.Restorefile(bookmark)
        !END

        ! Copy Jobseng
        !Access:JOBSENG.ClearKey(joe:JobNumberKey)
        !joe:JobNumber = job:Ref_Number
        !SET(joe:JobNumberKey,joe:JobNumberKey)
        !LOOP
        !    IF ((Access:JOBSENG.NEXT() <> Level:benign) OR (joe:JobNumber <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !
        !    jer:JobNumber     = job_ali:Ref_Number
        !    jer:UserCode      = joe:UserCode
        !    jer:DateAllocated = joe:DateAllocated
        !    jer:TimeAllocated = joe:TimeAllocated
        !    jer:AllocatedBy   = joe:AllocatedBy
        !    jer:EngSkillLevel = joe:EngSkillLevel
        !    jer:JobSkillLevel = joe:JobSkillLevel
        !
        !    bookmark = Access:JOBSENG.Savefile()
        !
        !    IF (Access:JOBSENG.PrimeRecord() = Level:benign) THEN
        !
        !        joe:JobNumber     = jer:JobNumber
        !        joe:UserCode      = jer:UserCode
        !        joe:DateAllocated = jer:DateAllocated
        !        joe:TimeAllocated = jer:TimeAllocated
        !        joe:AllocatedBy   = jer:AllocatedBy
        !        joe:EngSkillLevel = jer:EngSkillLevel
        !        joe:JobSkillLevel = jer:JobSkillLevel
        !
        !        IF (Access:JOBSENG.insert() <> Level:benign) THEN
        !            Access:JOBSENG.cancelautoinc()
        !        END
        !    END
        !
        !    Access:JOBSENG.Restorefile(bookmark)
        !END

        ! Copy Jobexhis
        !Access:JOBEXHIS.ClearKey(jxh:Ref_Number_Key)
        !jxh:Ref_Number = job:Ref_Number
        !SET(jxh:Ref_Number_Key,jxh:Ref_Number_Key)
        !LOOP
        !    IF ((Access:JOBEXHIS.NEXT() <> Level:benign) OR (jxh:Ref_Number <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !
        !    jxr:Ref_Number        = job_ali:Ref_Number
        !    jxr:Loan_Unit_Number  = jxh:Loan_Unit_Number
        !    jxr:Date              = jxh:Date
        !    jxr:Time              = jxh:Time
        !    jxr:User              = jxh:User
        !    jxr:Status            = jxh:Status
        !
        !    bookmark = Access:JOBEXHIS.Savefile()
        !
        !    IF (Access:JOBEXHIS.PrimeRecord() = Level:benign) THEN
        !
        !        jxh:Ref_Number        = jxr:Ref_Number
        !        jxh:Loan_Unit_Number  = jxr:Loan_Unit_Number
        !        jxh:Date              = jxr:Date
        !        jxh:Time              = jxr:Time
        !        jxh:User              = jxr:User
        !        jxh:Status            = jxr:Status
        !
        !        IF (Access:JOBEXHIS.insert() <> Level:benign) THEN
        !            Access:JOBEXHIS.cancelautoinc()
        !        END
        !    END
        !
        !    Access:JOBEXHIS.Restorefile(bookmark)
        !END

        ! Copy Joblohis
        !Access:JOBLOHIS.ClearKey(jlh:Ref_Number_Key)
        !jlh:Ref_Number = job:Ref_Number
        !SET(jlh:Ref_Number_Key,jlh:Ref_Number_Key)
        !LOOP
        !    IF ((Access:JOBLOHIS.NEXT() <> Level:benign) OR (jlh:Ref_Number <> job:Ref_Number)) THEN
        !        BREAK
        !    END
        !
        !    jlr:Ref_Number        = job_ali:Ref_Number
        !    jlr:Loan_Unit_Number  = jlh:Loan_Unit_Number
        !    jlr:Date              = jlh:Date
        !    jlr:Time              = jlh:Time
        !    jlr:User              = jlh:User
        !    jlr:Status            = jlh:Status
        !
        !    bookmark = Access:JOBLOHIS.Savefile()
        !
        !    IF (Access:JOBLOHIS.PrimeRecord() = Level:benign) THEN
        !
        !        jlh:Ref_Number        = jlr:Ref_Number
        !        jlh:Loan_Unit_Number  = jlr:Loan_Unit_Number
        !        jlh:Date              = jlr:Date
        !        jlh:Time              = jlr:Time
        !        jlh:User              = jlr:User
        !        jlh:Status            = jlr:Status
        !
        !        IF (Access:JOBLOHIS.insert() <> Level:benign) THEN
        !            Access:JOBLOHIS.cancelautoinc()
        !        END
        !    END
        !
        !    Access:JOBLOHIS.Restorefile(bookmark)
        !END

        ! BE(22/01/2004)
        IF (rpg:PrintJobLabel) THEN
            Set(defaults)
            access:defaults.next()
            glo:select1 = job_ali:Ref_number
            CASE def:label_printer_type
                OF 'TEC B-440 / B-442'
                    Thermal_Labels('')
                OF 'TEC B-452'
                    Thermal_Labels_B452
            END
            glo:select1 = ''
        END
    END

    RETURN
! End Change 3684 BE(10/12/03)
Local.ValidateParts     Procedure()
Code
      !OK, not do Parts/Network check
      Access:MANUFACT.Clearkey(man:Manufacturer_Key)
      man:Manufacturer    = job:Manufacturer
      If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Found
          If man:QAParts
              If GetTempPathA(255,TempFilePath)
                  If Sub(TempFilePath,-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & ''
                  Else !If Sub(TempFilePath,-1,1) = '\'
                      glo:FileName = Clip(TempFilePath) & '\'
                  End !If Sub(TempFilePath,-1,1) = '\'
              End

              glo:FileName = Clip(glo:FileName) & 'QAPARTS' & Clock() & '.TMP'

              Remove(glo:FileName)
              Access:QAPARTSTEMP.Open()
              Access:QAPARTSTEMP.UseFile()
              If job:Chargeable_Job = 'YES'
                  Save_par_ID = Access:PARTS.SaveFile()
                  Access:PARTS.ClearKey(par:Part_Number_Key)
                  par:Ref_Number  = job:Ref_Number
                  Set(par:Part_Number_Key,par:Part_Number_Key)
                  Loop
                      If Access:PARTS.NEXT()
                         Break
                      End !If
                      If par:Ref_Number  <> job:Ref_Number      |
                          Then Break.  ! End If
                      If par:Part_Number = 'ADJUSTMENT' or par:Adjustment = 'YES'
                        Cycle
                      End !If par:Part_Number = 'ADJUSTMENT'

                      If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign
                          qap:PartNumber       = par:Part_Number
                          qap:Description      = par:Description
                          qap:PartRecordNumber = par:Record_Number
                          qap:PartType         = 'CHA'
                          If Access:QAPARTSTEMP.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:QAPARTSTEMP.TryInsert() = Level:Benign
                              !Insert Failed
                          End !QAPIf Access:QAPARTSTEMP.TryInsert() = Level:Benign
                      End !If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign

                  End !Loop
                  Access:PARTS.RestoreFile(Save_par_ID)

              End !If job:Chargable_Job = 'YES'
              If job:Warranty_Job = 'YES'
                  Save_wpr_ID = Access:WARPARTS.SaveFile()
                  Access:WARPARTS.ClearKey(wpr:Part_Number_Key)
                  wpr:Ref_Number  = job:Ref_Number
                  Set(wpr:Part_Number_Key,wpr:Part_Number_Key)
                  Loop
                      If Access:WARPARTS.NEXT()
                         Break
                      End !If
                      If wpr:Ref_Number  <> job:Ref_Number      |
                          Then Break.  ! End If
                      If wpr:Part_Number = 'ADJUSTMENT' or wpr:Adjustment = 'YES'
                        Cycle
                      End !If wpr:Part_Number = 'ADJUSTMENT'

                      If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign
                          qap:PartNumber       = wpr:Part_Number
                          qap:Description      = wpr:Description
                          qap:PartRecordNumber = wpr:Record_Number
                          qap:PartType         = 'WAR'
                          If Access:QAPARTSTEMP.TryInsert() = Level:Benign
                              !Insert Successful
                          Else !If Access:QAPARTSTEMP.TryInsert() = Level:Benign
                              !Insert Failed
                          End !QAPIf Access:QAPARTSTEMP.TryInsert() = Level:Benign
                      End !If Access:QAPARTSTEMP.PrimeRecord() = Level:Benign

                  End !Loop
                  Access:WARPARTS.RestoreFile(Save_wpr_ID)
              End !If job:Chargable_Job = 'YES'

              Case ValidateParts()
                 Of 0
                     Access:QAPARTSTEMP.Close()
                     Remove(glo:FileName)

                     Return 0
                 Of 1 !Passed
                     Access:QAPARTSTEMP.Close()
                     Remove(glo:FileName)

                     Return 1
                 Of 2 !Failed
                     Do FailParts
                     Access:QAPARTSTEMP.Close()
                     Remove(glo:FileName)

                     Return 0

              End !Case ValidateParts()
          Else
                Return 1
          End !If man:QAParts
      Else! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Return 1
      End! If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign

Local.ValidateNetwork     Procedure()
Code
      !OK, not do Parts/Network check
    Access:MANUFACT.Clearkey(man:Manufacturer_Key)
    man:Manufacturer    = job:Manufacturer
    If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign
        !Found
        If man:QANetwork

            Access:JOBSE.Clearkey(jobe:RefNumberKey)
            jobe:RefNumber  = job:Ref_Number
            If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Found
                SaveRequest#      = GlobalRequest
                GlobalResponse    = RequestCancelled
                GlobalRequest     = SelectRecord
                PickNetworks
                If Globalresponse = RequestCompleted
                    If jobe:Network = net:Network
                        Return 1
                    Else !If jobe:Network = net:Network

                        Beep(Beep:SystemHand)  ;  Yield()
                        Case Message('Mismatch Network.'&|
                                '||This job will now fail QA.', |
                                'ServiceBase 2000', Icon:Hand, |
                                 Button:OK, Button:OK, 0)
                        Of Button:OK
                        End !CASE

                        reason" = ''
                        QA_Failure_Reason(reason")

                        get(audit,0)
                        if access:audit.primerecord() = level:benign
                            aud:notes         = 'NETWORK RECORDED: ' & Clip(jobe:Network) & |
                                                '<13,10,13,10>NETWORK VALIDATED: ' & Clip(net:Network)
                            glo:notes_global    = Clip(aud:notes)

                            aud:ref_number    = job:ref_number
                            aud:date          = today()
                            aud:time          = clock()
                            aud:type          = 'JOB'
                            access:users.clearkey(use:password_key)
                            use:password = glo:password
                            access:users.fetch(use:password_key)
                            aud:user = use:user_code
                            Case f_type
                                Of 'PRE'
                                    aud:action        = 'QA REJECTION ELECTRONIC: ' & Clip(reason")
                                Of 'PRI'
                                    aud:action        = 'QA REJECTION MANUAL: ' & Clip(reason")
                            End !Case f_type
                            access:audit.insert()
                        end!if access:audit.primerecord() = level:benign
                        glo:select1 = job:ref_number
                        If def:qa_failed_label = 'YES'
                            QA_Failed_Label
                        End!If def:qa_failed_label = 'YES'
                        If def:qa_failed_report = 'YES'
                            QA_Failed
                        End!If def:qa_failed_report = 'YES'
                        glo:select1 = ''
                        glo:notes_global    = ''
                        Case f_type
                            Of 'PRE'
                                GetStatus(625,1,'JOB')
                            Else!Case f_type
                                GetStatus(615,1,'JOB')
                        End!Case f_type

                        job:qa_rejected = 'YES'
                        job:date_qa_rejected    = Today()
                        job:time_qa_rejected    = Clock()
                        access:jobs.update()
                        Do passed_fail_update

                        Return 0

                    End !If jobe:Network <> net:Network
                Else
                    Return 0
                End
                GlobalRequest     = SaveRequest#

            Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
            End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
        Else
            Return 1
        End !If man:QANetwork
    Else
        Return 1
    End !If Access:MANUFACT.Tryfetch(man:Manufacturer_Key) = Level:Benign


GetOriginalAccountNumber    PROCEDURE(job_no)    ! Start Change 3684 BE(10/12/03)
result  STRING(15)
    CODE
    result = ''
    save_aud_id = access:audit.savefile()
    access:audit.clearkey(aud:Action_Key)
    aud:ref_number  = job_no
    aud:action = '14-DAY REPACKAGING(A)'
    SET(aud:Action_Key,aud:Action_Key)
    LOOP
        IF ((access:audit.next() <> Level:benign) OR (aud:ref_number <> job_no)) THEN
           BREAK
        END
        lenotes# = LEN(aud:Notes)
        IF ((lenotes# > 18) AND (aud:Notes[1 :17] = 'ORIGINAL ACCOUNT:')) THEN
            result = CLIP(aud:Notes[19 : lenotes#])
            BREAK
        END
    END
    access:audit.restorefile(save_aud_id)
    RETURN result
! End Change 3684 BE(10/12/03)
Check14DayB     PROCEDURE(job_no)         ! Start Change 3684 BE(2/01/04)
result      LONG
    CODE
    result = 0
    save_aud_id = access:audit.savefile()
    access:audit.clearkey(aud:Action_Key)
    aud:ref_number  = job_no
    aud:action = '14-DAY REPACKAGING(B)'
    SET(aud:Action_Key,aud:Action_Key)
    LOOP
        IF ((access:audit.next() <> Level:benign) OR (aud:ref_number <> job_no) OR |
            (aud:action <> '14-DAY REPACKAGING(B)')) THEN
           BREAK
        END
        result =1
        BREAK
    END
    access:audit.restorefile(save_aud_id)
    RETURN result
! End Change 3684 BE(22/01/04)
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
