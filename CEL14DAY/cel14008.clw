

   MEMBER('cel14day.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CEL14008.INC'),ONCE        !Local module procedure declarations
                     END


UpdateDefaults PROCEDURE                              !Generated from procedure template - Window

IniFilepath          STRING(255)
PrintJobLabel        BYTE
OriginalAccount1     STRING(15)
OriginalChargeType   STRING(30)
NewAccount           STRING(15)
NewChargeType        STRING(30)
OriginalAccount2     STRING(15)
window               WINDOW('14-Day Repackaging Defaults'),AT(,,293,151),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(3,4,285,116),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           BUTTON('...'),AT(268,20,10,10),USE(?CallLookup),LEFT,ICON('list3.ico')
                           PROMPT('Original Account 2'),AT(16,36),USE(?Prompt5),TRN
                           ENTRY(@s15),AT(136,36,124,10),USE(OriginalAccount2),FONT(,,,FONT:bold)
                           BUTTON('...'),AT(268,36,10,10),USE(?CallLookup:5),LEFT,ICON('list3.ico')
                           PROMPT('Original Account 1'),AT(15,20),USE(?Prompt1),TRN
                           ENTRY(@s15),AT(136,20,124,10),USE(OriginalAccount1),FONT(,,,FONT:bold)
                           PROMPT('Original Chargeable Charge Type'),AT(15,52),USE(?Prompt2),TRN
                           ENTRY(@s30),AT(136,52,124,10),USE(OriginalChargeType),FONT(,,,FONT:bold)
                           BUTTON('...'),AT(268,52,10,10),USE(?CallLookup:2),LEFT,ICON('list3.ico')
                           PROMPT('New Account'),AT(15,68),USE(?Prompt3),TRN
                           ENTRY(@s15),AT(136,68,124,10),USE(NewAccount),FONT(,,,FONT:bold)
                           PROMPT('New Warranty Charge Type'),AT(15,84),USE(?Prompt4),TRN
                           ENTRY(@s30),AT(136,84,124,10),USE(NewChargeType),FONT(,,,FONT:bold)
                           CHECK('Print New Job Label {24}'),AT(15,100),USE(PrintJobLabel),TRN,LEFT
                           BUTTON('...'),AT(268,84,10,10),USE(?CallLookup:4),LEFT,ICON('list3.ico')
                           BUTTON('...'),AT(268,68,10,10),USE(?CallLookup:3),LEFT,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(3,124,285,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Cancel'),AT(224,128,56,16),USE(?CancelButton)
                       BUTTON('OK'),AT(164,128,56,16),USE(?OkButton)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
look:OriginalAccount2                Like(OriginalAccount2)
look:OriginalAccount1                Like(OriginalAccount1)
look:OriginalChargeType                Like(OriginalChargeType)
look:NewAccount                Like(NewAccount)
look:NewChargeType                Like(NewChargeType)
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    If ?OriginalAccount2{prop:ReadOnly} = True
        ?OriginalAccount2{prop:FontColor} = 65793
        ?OriginalAccount2{prop:Color} = 15066597
    Elsif ?OriginalAccount2{prop:Req} = True
        ?OriginalAccount2{prop:FontColor} = 65793
        ?OriginalAccount2{prop:Color} = 8454143
    Else ! If ?OriginalAccount2{prop:Req} = True
        ?OriginalAccount2{prop:FontColor} = 65793
        ?OriginalAccount2{prop:Color} = 16777215
    End ! If ?OriginalAccount2{prop:Req} = True
    ?OriginalAccount2{prop:Trn} = 0
    ?OriginalAccount2{prop:FontStyle} = font:Bold
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    If ?OriginalAccount1{prop:ReadOnly} = True
        ?OriginalAccount1{prop:FontColor} = 65793
        ?OriginalAccount1{prop:Color} = 15066597
    Elsif ?OriginalAccount1{prop:Req} = True
        ?OriginalAccount1{prop:FontColor} = 65793
        ?OriginalAccount1{prop:Color} = 8454143
    Else ! If ?OriginalAccount1{prop:Req} = True
        ?OriginalAccount1{prop:FontColor} = 65793
        ?OriginalAccount1{prop:Color} = 16777215
    End ! If ?OriginalAccount1{prop:Req} = True
    ?OriginalAccount1{prop:Trn} = 0
    ?OriginalAccount1{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?OriginalChargeType{prop:ReadOnly} = True
        ?OriginalChargeType{prop:FontColor} = 65793
        ?OriginalChargeType{prop:Color} = 15066597
    Elsif ?OriginalChargeType{prop:Req} = True
        ?OriginalChargeType{prop:FontColor} = 65793
        ?OriginalChargeType{prop:Color} = 8454143
    Else ! If ?OriginalChargeType{prop:Req} = True
        ?OriginalChargeType{prop:FontColor} = 65793
        ?OriginalChargeType{prop:Color} = 16777215
    End ! If ?OriginalChargeType{prop:Req} = True
    ?OriginalChargeType{prop:Trn} = 0
    ?OriginalChargeType{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?NewAccount{prop:ReadOnly} = True
        ?NewAccount{prop:FontColor} = 65793
        ?NewAccount{prop:Color} = 15066597
    Elsif ?NewAccount{prop:Req} = True
        ?NewAccount{prop:FontColor} = 65793
        ?NewAccount{prop:Color} = 8454143
    Else ! If ?NewAccount{prop:Req} = True
        ?NewAccount{prop:FontColor} = 65793
        ?NewAccount{prop:Color} = 16777215
    End ! If ?NewAccount{prop:Req} = True
    ?NewAccount{prop:Trn} = 0
    ?NewAccount{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    If ?NewChargeType{prop:ReadOnly} = True
        ?NewChargeType{prop:FontColor} = 65793
        ?NewChargeType{prop:Color} = 15066597
    Elsif ?NewChargeType{prop:Req} = True
        ?NewChargeType{prop:FontColor} = 65793
        ?NewChargeType{prop:Color} = 8454143
    Else ! If ?NewChargeType{prop:Req} = True
        ?NewChargeType{prop:FontColor} = 65793
        ?NewChargeType{prop:Color} = 16777215
    End ! If ?NewChargeType{prop:Req} = True
    ?NewChargeType{prop:Trn} = 0
    ?NewChargeType{prop:FontStyle} = font:Bold
    ?PrintJobLabel{prop:Font,3} = -1
    ?PrintJobLabel{prop:Color} = 15066597
    ?PrintJobLabel{prop:Trn} = 0
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateDefaults')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?CallLookup
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:CHARTYPE.Open
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  IniFilepath =  CLIP(PATH()) & '\CEL14DAY.INI'
  OriginalAccount1 = GETINI('DEFAULTS','OriginalAccount1',,IniFilepath)
  OriginalAccount2 = GETINI('DEFAULTS','OriginalAccount2',,IniFilepath)
  OriginalChargeType = GETINI('DEFAULTS','OriginalChargeType',,IniFilepath)
  NewAccount = GETINI('DEFAULTS','NewAccount',,IniFilepath)
  NewChargeType = GETINI('DEFAULTS','NewChargeType',,IniFilepath)
  PrintJoblabel = GETINI('DEFAULTS','PrintJoblabel',0,IniFilepath)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSubAccounts
      PickSubAccounts
      PickChargeableChargeTypes
      PickSubAccounts
      PickWarrantyChargeTypes
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?CallLookup
      ThisWindow.Update
      sub:Account_Number = OriginalAccount1
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        OriginalAccount1 = sub:Account_Number
      END
      ThisWindow.Reset(1)
    OF ?OriginalAccount2
      IF OriginalAccount2 OR ?OriginalAccount2{Prop:Req}
        sub:Account_Number = OriginalAccount2
        !Save Lookup Field Incase Of error
        look:OriginalAccount2        = OriginalAccount2
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            OriginalAccount2 = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            OriginalAccount2 = look:OriginalAccount2
            SELECT(?OriginalAccount2)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:5
      ThisWindow.Update
      sub:Account_Number = OriginalAccount2
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        OriginalAccount2 = sub:Account_Number
      END
      ThisWindow.Reset(1)
    OF ?OriginalAccount1
      IF OriginalAccount1 OR ?OriginalAccount1{Prop:Req}
        sub:Account_Number = OriginalAccount1
        !Save Lookup Field Incase Of error
        look:OriginalAccount1        = OriginalAccount1
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            OriginalAccount1 = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            OriginalAccount1 = look:OriginalAccount1
            SELECT(?OriginalAccount1)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?OriginalChargeType
      IF OriginalChargeType OR ?OriginalChargeType{Prop:Req}
        cha:Charge_Type = OriginalChargeType
        !Save Lookup Field Incase Of error
        look:OriginalChargeType        = OriginalChargeType
        IF Access:CHARTYPE.TryFetch(cha:Charge_Type_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            OriginalChargeType = cha:Charge_Type
          ELSE
            !Restore Lookup On Error
            OriginalChargeType = look:OriginalChargeType
            SELECT(?OriginalChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:2
      ThisWindow.Update
      cha:Charge_Type = OriginalChargeType
      IF SELF.Run(3,SelectRecord) = RequestCompleted
        OriginalChargeType = cha:Charge_Type
      END
      ThisWindow.Reset(1)
    OF ?NewAccount
      IF NewAccount OR ?NewAccount{Prop:Req}
        sub:Account_Number = NewAccount
        !Save Lookup Field Incase Of error
        look:NewAccount        = NewAccount
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            NewAccount = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            NewAccount = look:NewAccount
            SELECT(?NewAccount)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?NewChargeType
      IF NewChargeType OR ?NewChargeType{Prop:Req}
        cha:Charge_Type = NewChargeType
        !Save Lookup Field Incase Of error
        look:NewChargeType        = NewChargeType
        IF Access:CHARTYPE.TryFetch(cha:Warranty_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            NewChargeType = cha:Charge_Type
          ELSE
            !Restore Lookup On Error
            NewChargeType = look:NewChargeType
            SELECT(?NewChargeType)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?CallLookup:4
      ThisWindow.Update
      cha:Charge_Type = NewChargeType
      IF SELF.Run(5,SelectRecord) = RequestCompleted
        NewChargeType = cha:Charge_Type
      END
      ThisWindow.Reset(1)
    OF ?CallLookup:3
      ThisWindow.Update
      sub:Account_Number = NewAccount
      IF SELF.Run(1,SelectRecord) = RequestCompleted
        NewAccount = sub:Account_Number
      END
      ThisWindow.Reset(1)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      PUTINI('DEFAULTS','OriginalAccount1',OriginalAccount1,IniFilepath)
      PUTINI('DEFAULTS','OriginalAccount2',OriginalAccount2,IniFilepath)
      PUTINI('DEFAULTS','OriginalChargeType',OriginalChargeType,IniFilepath)
      PUTINI('DEFAULTS','NewAccount',NewAccount,IniFilepath)
      PUTINI('DEFAULTS','NewChargeType',NewChargeType,IniFilepath)
      PUTINI('DEFAULTS','PrintJoblabel',PrintJoblabel,IniFilepath)
      
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

