

   MEMBER('wilfredtosb.clw')                          ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('WILFR005.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('WILFR001.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

Local                CLASS
ImportJobs           Procedure(String func:FileName,String func:ShortName)
                     END
tmp:ImportFile       CSTRING(255),STATIC
ConnectionStr        STRING(255),STATIC
INIFilePath          STRING(255)
window               WINDOW('Wilfred To SB'),AT(,,69,15),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       PROMPT('FTP In Progress...'),AT(4,0),USE(?Prompt1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ImportFile      File,Driver('BASIC', '/COMMA = 9' ),Pre(impfil),Name(tmp:ImportFile),Create,Bindable,Thread
Record                  Record
RefNumber               Long()
ConsignmentNumber       String(60)
Manufacturer            String(30)
ModelNumber             String(30)
IMEINumber              String(30)
StatusCode              String(30)
ResolutionCode          String(255)
                        End
                    End
! Dummy SQL table
SQLFile             FILE,DRIVER('Scalable'),OEM,OWNER(ConnectionStr),NAME('WEBJOB'),PRE(SQL),BINDABLE,THREAD
Record                  RECORD
SQLRefNumber                Long, Name('Ref_Number')
                        END
                    END
TempFiles   Queue(file:Queue),pre(fil)
            End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  OPEN(window)
  SELF.Opened=True
  Free(FileQueue)
  Clear(FileQueue)
  
  ErrorLog('')
  ErrorLog('WILFRED2SB Started===========================')
  
  Loop Until FTP() = 1
  
  End ! Until FTP() = 1
  
  ErrorLog('FTP process: Completed')
  
  Directory(TempFiles,Clip(Path()) & '\WilfredToSend\desp_?????????????.tab',ff_:DIRECTORY)
  Loop x# = 1 To Records(TempFiles)
      Get(TempFiles,x#)
      If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
          Cycle
      Else !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
          ErrorLog('Importing: ' & Clip(fil:Name) & ' - ' & Round(fil:Size/1024,.01) & 'kb')
          Local.ImportJobs(Clip(Path()) & '\WilfredToSend\' & CLip(fil:name),Clip(fil:Name))
      End !If Band(fil:Attrib,ff_:Directory) And fil:ShortName <> '..' And fil:ShortName <> '.'
  End !x# = 1 To Records(ScanFiles)
  
  !Loop x# = 1 To Records(FileQueue)
  !    Get(FileQueue,x#)
  !    ErrorLog('Begin Importing: ' & Clip(filque:FileName))
  !    Local.ImportJobs(filque:FileName,filque:ShortName)
  !End ! x# = 1 To Records(FileQueue)
  
  ErrorLog('WILFRED2SB Finished==========================')
  
  Post(Event:CloseWindow)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  GlobalErrors.SetProcedureName
  RETURN ReturnValue

Local.ImportJobs        Procedure(String func:FileName, String func:ShortName)
local:UpdateStatus      Byte(0)
local:JobUpdated        Byte(0)
Code

    ! Start - Is this file a duplicate? (DBH: 01-04-2005)
    If Exists(Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))
        ErrorLog('Duplicate File Error: ' & Clip(func:ShortName))
        Return
    End ! If Exists(Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))
    ! End   - Is this file a duplicate? (DBH: 01-04-2005)

    Relate:JOBS.Open()
    Relate:DEFAULTS.Open()
    Relate:USERS.Open()
    Relate:EXCHANGE.Open()
    Relate:DESBATCH.Open()
    Relate:STATUS.Open()

    IniFilepath =  Clip(Path()) & '\Webimp.ini'
    ConnectionStr = GetIni('Defaults', 'Connection', '', Clip(IniFilepath))

    Open(SQLFile)

    CountNormal# = 0
    CountStatus# = 0

    Access:EXCHANGE.UseFile()
    tmp:ImportFile = func:FileName
    Open(ImportFile)
    Set(ImportFile,0)
    Loop
        Next(ImportFile)
        If Error()
            Break
        End !If Error()

        !Is there a job number here? - TrkBs:  (DBH: 02-02-2005)
        If impfil:RefNumber <> ''
            Access:JOBS.Clearkey(job:Ref_Number_Key)
            job:Ref_Number  = impfil:RefNumber
            If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Found
                local:JobUpdated = False
                !Is there an exchange unit to add? - TrkBs:  (DBH: 02-02-2005)
                !Make sure they aren't sending the job imei back too - TrkBs:  (DBH: 11-02-2005)
                If impfil:IMEINumber <> '' And impfil:IMEINumber <> job:ESN
                    !Only add exchange if not already attached - TrkBs:  (DBH: 02-02-2005)
                    If job:Exchange_Unit_Number = ''
                        If Access:exchange.PrimeRecord() = Level:Benign
                            xch:Model_Number   = impfil:ModelNumber
                            xch:Manufacturer   = impfil:Manufacturer
                            xch:ESN            = impfil:IMEINumber
                            xch:Location       = 'WILFRED EXCHANGE'
                            xch:Shelf_Location = 'WILFRED EXCHANGE'
                            xch:Date_Booked    = Today()
                            xch:Available      = 'EXC'
                            xch:Job_Number     = job:Ref_Number
                            xch:Stock_Type     = 'WILFRED EXCHANGE'
                            If Access:exchange.TryInsert() = Level:Benign
                                !Insert Successful
                                job:Exchange_Unit_Number = xch:Ref_Number
                                job:Exchange_User       = 'WEB'
                                job:Exchange_Despatched = Today()
                                job:Exchange_Despatched_User = 'WEB'
                                job:Exchange_Consignment_Number = impfil:ConsignmentNumber
                                GetStatusAlternative(125,0,'EXC')

                                CountNormal# += 1

                                Access:JOBS.Update()
                                local:JobUpdated = True

                                If Access:AUDIT.PrimeRecord() = Level:Benign
                                    aud:Notes         = 'JOB UPDATED BY WILFRED<13,10>EXCHANGE UNIT NO: ' & Clip(job:Exchange_Unit_Number) & |
                                                        '<13,10>I.M.E.I. NO: ' & Clip(impfil:IMEINumber)
                                    aud:Ref_Number    = job:ref_number
                                    aud:Date          = Today()
                                    aud:Time          = Clock()
                                    aud:Type          = 'EXC'
                                    aud:User          = 'WEB'
                                    aud:Action        = 'EXCHANGE UNIT ATTACHED'
                                    Access:AUDIT.Insert()
                                End!If Access:AUDIT.PrimeRecord() = Level:Benign

                            Else !If Access:exchange.TryInsert() = Level:Benign
                                !Insert Failed
                                Access:exchange.CancelAutoInc()
                            End !If Access:exchange.TryInsert() = Level:Benign
                        End !If Access:exchange.PrimeRecord() = Level:Benign
                    End ! If job:Exchange_Unit_Number = ''
                End ! If impfil:IMEINumber <> ''

                !These Status are sent by SB, so ignore if Wilfred sends them - TrkBs:  (DBH: 02-02-2005)
                local:UpdateStatus = True
                Case impfil:StatusCode
                    Of '114' OrOf |
                        '135' OrOf |
                        '798' OrOf |
                        '930' OrOf |
                        '940'
                        local:UpdateStatus = False
                End ! Case impfil:StatusCode

                !Do not update jobs with specific SID status  (DBH: 23-11-2004)
                !Allow them to change "114" jobs - TrkBs:  (DBH: 08-03-2005)
                Case Sub(job:Current_Status,1,3)
                    !Of '114'  OrOf |    ! Awaiting Arrival
                    Of  '798' OrOf |    ! Job Cancelled By Vodafone
                        '930' OrOf |    ! Arrived Back At Store
                        '940'           ! Phone Returned To Customer
                        Cycle            
                End ! Case Sub(job:Current_Status,1,3)

                If local:UpdateStatus = True
                    GetStatusAlternative(Sub(impfil:StatusCode,1,3),0,'JOB')
                    local:JobUpdated = True

                    !When a job is despatched from the service centre an SMS notification message is sent to the customer.
                    !If the job is being updated with a despatch status, update the WebJob table (GK 18-01-2005)
                    Case Sub(job:Current_Status,1,3)
                        Of '901' OrOf |    ! 901 DESPATCHED
                           '905' OrOf |    ! 905 DESPATCHED UNPAID
                           '910'           ! 910 DESPATCH PAID
                            !Set SMSType field to D = Send SMS with despatch from Service Centre text
                            SQLFile{Prop:SQL} = 'UPDATE WEBJOB SET SMSType = ''D'' WHERE SBJobNo = ' & job:Ref_Number
                    End ! Case Sub(job:Current_Status,1,3)

                    CountStatus# += 1
                End ! If local:UpdateStatus = True

                !Is there a resolution code? - TrkBs:  (DBH: 02-02-2005)
                If Clip(impfil:ResolutionCode) <> ''
                    Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
                    jbn:RefNumber  = job:Ref_Number
                    If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
                        !Found
                        jbn:Invoice_Text = Clip(jbn:Invoice_Text) & '<13,10>' & Clip(impfil:ResolutionCode)
                        Access:JOBNOTES.Update()
                        local:JobUpdated = True
                    Else ! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
                        !Error
                    End ! If Access:JOBNOTES.Tryfetch(jbn:Ref_Number_Key) = Level:Benign
                End ! If impfil:ResolutionCode <> ''

                !Is there a consignment number? - TrkBs:  (DBH: 02-02-2005)
                If Clip(impfil:ConsignmentNumber) <> ''
                    !If there is an exchange attached, update the exchange consignment no - TrkBs:  (DBH: 02-02-2005)
                    If job:Exchange_Unit_Number <> ''
                        job:Exchange_Consignment_Number = impfil:ConsignmentNumber
                    Else ! If job:Exchange_Unit_Number <> ''
                        job:Consignment_Number = impfil:ConsignmentNumber
                    End ! If job:Exchange_Unit_Number <> ''
                    local:JobUpdated = True
                End ! If Clip(impfil:ConsignmentNumber) <> ''

                Access:JOBS.Update()

                !The job has been updated by wilfred - TrkBs:  (DBH: 02-02-2005)
                If local:JobUpdated = True
                    If Access:AUDIT.PrimeRecord() = Level:Benign
                        aud:Notes         = 'IMPORT FILE: ' & Clip(func:ShortName)
                        aud:Ref_Number    = job:ref_number
                        aud:Date          = Today()
                        aud:Time          = Clock()
                        aud:Type          = 'JOB'
                        aud:User          = 'WEB'
                        aud:Action        = 'JOB UPDATED BY WILFRED'
                        Access:AUDIT.Insert()
                    End!If Access:AUDIT.PrimeRecord() = Level:Benign
                End ! If local:JobUpdated = True
            Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                !Error
                Cycle
            End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
        Else ! If impfil:RefNumber <> ''
            Cycle
        End ! If impfil:RefNumber <> ''
    End !Loop(ImportFile)
    Close(ImportFile)
    Close(SQLFile)
    Relate:JOBS.Close()
    Relate:DEFAULTS.Close()
    Relate:USERS.Close()
    Relate:EXCHANGE.Close()
    Relate:DESBATCH.Close()
    Relate:STATUS.Close()

    ErrorLog('Exchage Jobs Imported: ' & CountNormal#)
    ErrorLog('Status Update Jobs Imported: ' & CountStatus#)

    Rename(Clip(Path()) & '\WilfredToSend\' & Clip(func:ShortName),Clip(Path()) & '\WilfredSent\' & Clip(func:ShortName))

     IF Error()
        Case Error()
            Of ''
                Error" = 'Same File Already Exists'
            Of 2
                Error" = 'File Not Found'
            Of 5
                Error" = 'Access Denied'
            Of 52
                Error" = 'File Aready Open'
            Else
                Error" = Error()
        End ! Case Error()
        ErrorLog('Rename Error (' & Clip(Error") & '): ' & Clip(func:ShortName))
    Else ! If Error()
        ErrorLog('File Placed in ''Sent'' folder')
    End ! If Error()
    !End   - Debug - TrkBs:  (DBH: 25-01-2005)

