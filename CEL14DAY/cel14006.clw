

   MEMBER('cel14day.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CEL14006.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CEL14008.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CEL14009.INC'),ONCE        !Req'd for module callout resolution
                     END


Main PROCEDURE                                        !Generated from procedure template - Frame

LocalRequest         LONG
start_help_temp      STRING(255)
pos                  STRING(255)
loc:lastcount        LONG
loc:lasttime         LONG
FilesOpened          BYTE
CurrentTab           STRING(80)
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
comp_name_temp       STRING(255)
AllowUserToExit      BYTE
Date_Temp            STRING(60)
Time_Temp            STRING(60)
SRCF::Version        STRING(512)
SRCF::Format         STRING(512)
CPCSExecutePopUp     BYTE
BackerDDEName        STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
AppFrame             APPLICATION('14-Day Repackaging Process - ServiceBase 2000'),AT(,,678,415),FONT('Tahoma',8,,),CENTER,IMM,HVSCROLL,ICON('Pc.ico'),ALRT(F12Key),STATUS(-1,26,26,120,40),TIMER(6000),SYSTEM,MAX,MAXIMIZE,RESIZE
                       MENUBAR
                         MENU('&File'),USE(?File)
                           ITEM('&Re-Login'),USE(?ReLogin)
                           ITEM,SEPARATOR
                           ITEM('E&xit'),USE(?Exit),MSG('Exit this application'),STD(STD:Close)
                         END
                         MENU('&Edit')
                           ITEM('Cu&t'),USE(?Cut),MSG('Remove item to Windows Clipboard'),STD(STD:Cut)
                           ITEM('&Copy'),USE(?Copy),MSG('Copy item to Windows Clipboard'),STD(STD:Copy)
                           ITEM('&Paste'),USE(?Paste),MSG('Paste contents of Windows Clipboard'),STD(STD:Paste)
                         END
                         MENU('&Window'),MSG('Create and Arrange windows'),STD(STD:WindowList)
                           ITEM('T&ile'),USE(?Tile),MSG('Make all open windows visible'),STD(STD:TileWindow)
                           ITEM('&Cascade'),USE(?Cascade),MSG('Stack all open windows'),STD(STD:CascadeWindow)
                           ITEM('&Arrange Icons'),USE(?Arrange),MSG('Align all window icons'),STD(STD:ArrangeIcons)
                         END
                         ITEM('&Help'),USE(?Help)
                       END
                       TOOLBAR,AT(0,0,,28),COLOR(COLOR:Silver),TILED
                         BUTTON('Exit'),AT(396,1,76,20),USE(?Button6),FLAT,LEFT,ICON('exitcomp.gif'),STD(STD:Close)
                         BUTTON('14-Day Repackaging'),AT(92,1,76,20),USE(?RepackageButton),FLAT,LEFT,ICON('files.gif')
                         BUTTON('Defaults'),AT(4,1,84,20),USE(?DefaultsButton),FLAT,LEFT,ICON('Files.gif')
                         PANEL,AT(388,1,1,21),USE(?Panel2),BEVEL(0,0,02010H)
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
Login      Routine
    glo:select1 = 'N'
    Insert_Password
    If glo:select1 = 'Y'
        Halt
    Else
        !access:users.open
        access:users.usefile
        access:users.clearkey(use:password_key)
        use:password =glo:password
        access:users.fetch(use:password_key)
        set(defaults)
        access:defaults.next()
        0{prop:text} = '14-Day Repackaging Process - ServiceBase 2000. Version: ' & |
                        Clip(def:version_number) & '       Current User: ' & |
                        Clip(use:forename) & ' ' & Clip(use:surname)
        !If glo:password <> 'JOB:ENTER'
        !    Do Security_Check
        !End
        !access:users.close
    End
Log_out      Routine
    access:users.open
    access:users.usefile
    access:logged.open
    access:logged.usefile
    access:users.clearkey(use:password_key)
    use:password =glo:password
    if access:users.fetch(use:password_key) = Level:Benign
        access:logged.clearkey(log:user_code_key)
        log:user_code = use:user_code
        log:time      = glo:timelogged
        if access:logged.fetch(log:user_code_key) = Level:Benign
            delete(logged)
        end !if
    end !if
    access:users.close
    access:logged.close
Security_Check      Routine
    check_access('MENU - FILE',x")
    if x" = true
        enable(?file)
    else
        disable(?file)
    end

    check_access('14-DAY REPACKAGING DEFAULTS',x")
    if x" = true
        enable(?DefaultsButton)
    else
        disable(?DefaultsButton)
    end

    check_access('RE-LOGIN',x")
    if x" = true
        enable(?relogin)
    else
        disable(?relogin)
    end
Menu::File ROUTINE                                    !Code for menu items on ?File
  CASE ACCEPTED()
  OF ?ReLogin
    ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
    Do Log_out
    Do Login
    ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReLogin, Accepted)
  END
! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = 1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:LOGGED.Open
  Relate:USERS.Open
  SELF.FilesOpened = True
  Close(StartWindow)
  OPEN(AppFrame)
  SELF.Opened=True
  Do RecolourWindow
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      Channel = DDESERVER('CEL14DAY') !set program as a DDE Server
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  error# = 1
  LOOP x# = 1 TO LEN(CLIP(Command()))
      IF (Sub(Command(),x#,1) = '%') THEN
          glo:Password = CLIP(SUB(Command(),x#+1,30))
          Access:USERS.Clearkey(use:Password_Key)
          use:Password    = glo:Password
          If Access:USERS.Tryfetch(use:Password_Key) = Level:Benign
              error# = 0
              ! Start Change 4916 BE(3/11/2004)
              set(defaults)
              access:defaults.next()
              0{prop:text} = '14-Day Repackaging Process - ServiceBase 2000. Version: ' & |
                             Clip(def:version_number) & '       Current User: ' &|
                             Clip(use:forename) & ' ' & Clip(use:surname)
              ! End Change 4916 BE(3/11/2004)
          END
          BREAK
      END
  END
  
  IF (error# = 1) THEN
      DO Login
  END
  
  IF (glo:password <> 'JOB:ENTER') THEN
      DO Security_Check
  END
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
      PUTINI('ALLOCATEJOB','Engineer','',CLIP(PATH()) & '\CELRAPEN.INI')
      PUTINI('ALLOCATEJOB','Location','',CLIP(PATH()) & '\CELRAPEN.INI')
      PUTINI('ALLOCATEJOB','Status','',CLIP(PATH()) & '\CELRAPEN.INI')
      PUTINI('ALLOCATEJOB','UserCode','',CLIP(PATH()) & '\CELRAPEN.INI')
    Do log_out
    Relate:DEFAULTS.Close
    Relate:LOGGED.Close
    Relate:USERS.Close
  END
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE ACCEPTED()
    ELSE
      DO Menu::File                                   !Process menu items on ?File menu
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Help
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Help, Accepted)
      !start_help_temp = 'c:\windows\explorer.exe ' & Clip(Path()) & '\help\index.htm'
      RUN(Clip(Path()) & '\help\help.exe')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Help, Accepted)
    OF ?RepackageButton
      Repackage
    OF ?DefaultsButton
      UpdateDefaults
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      omit('***',ClarionetUsed=0)
      If ~clarionetserver:Active()
      ***
      IF EVENT() = Event:DDEexecute
          IF DDEVALUE() = 'INFRONT' !tells it to bring itself to the front
              AppFrame{PROP:Iconize} = FALSE
              BringWindowToTop(AppFrame{PROP:Handle}) !and this does it
          END
      END
      omit('***',ClarionetUsed=0)
      End !If clarionetserver:Active()
      ***
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Do Log_out
      Do Login
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
         AppFrame{Prop:Active}=1
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

