

   MEMBER('cel14day.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CEL14009.INC'),ONCE        !Local module procedure declarations
                     END


Repackage PROCEDURE                                   !Generated from procedure template - Window

IniFilepath          STRING(255)
JobNumber            LONG
ImeiNumber           STRING(20)
PrintJobLabel        BYTE
OriginalAccount1     STRING(15)
OriginalAccount2     STRING(15)
OriginalChargeType   STRING(30)
NewAccount           STRING(15)
NewChargeType        STRING(30)
JobsQueue            QUEUE,PRE(JBQ)
JobNo                LONG
ImeiNo               STRING(20)
                     END
window               WINDOW('14-Day Repackaging Process'),AT(,,355,159),FONT('Tahoma',8,,),CENTER,GRAY,DOUBLE
                       SHEET,AT(4,4,348,124),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           PROMPT('Jobs Successfully Repackaged'),AT(148,8),USE(?Prompt3)
                           ENTRY(@p<<<<<<<#pb),AT(40,32,92,12),USE(JobNumber),LEFT,FONT(,,,FONT:bold)
                           LIST,AT(148,20,200,104),USE(?List1),IMM,SKIP,VSCROLL,FORMAT('59L(2)|M~Job No~@s8@80L(2)|M~I.M.E.I. Number~@s20@'),FROM(JobsQueue)
                           PROMPT('Job No.'),AT(12,32),USE(?Prompt1)
                           PROMPT('IMEI No.'),AT(12,48),USE(?Prompt2)
                           ENTRY(@s20),AT(40,48,92,12),USE(ImeiNumber),FONT(,,,FONT:bold)
                         END
                       END
                       PANEL,AT(4,132,348,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Next'),AT(92,136,56,16),USE(?NextButton)
                       BUTTON('Finish'),AT(292,136,56,16),USE(?FinishButton)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?JobNumber{prop:ReadOnly} = True
        ?JobNumber{prop:FontColor} = 65793
        ?JobNumber{prop:Color} = 15066597
    Elsif ?JobNumber{prop:Req} = True
        ?JobNumber{prop:FontColor} = 65793
        ?JobNumber{prop:Color} = 8454143
    Else ! If ?JobNumber{prop:Req} = True
        ?JobNumber{prop:FontColor} = 65793
        ?JobNumber{prop:Color} = 16777215
    End ! If ?JobNumber{prop:Req} = True
    ?JobNumber{prop:Trn} = 0
    ?JobNumber{prop:FontStyle} = font:Bold
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?ImeiNumber{prop:ReadOnly} = True
        ?ImeiNumber{prop:FontColor} = 65793
        ?ImeiNumber{prop:Color} = 15066597
    Elsif ?ImeiNumber{prop:Req} = True
        ?ImeiNumber{prop:FontColor} = 65793
        ?ImeiNumber{prop:Color} = 8454143
    Else ! If ?ImeiNumber{prop:Req} = True
        ?ImeiNumber{prop:FontColor} = 65793
        ?ImeiNumber{prop:Color} = 16777215
    End ! If ?ImeiNumber{prop:Req} = True
    ?ImeiNumber{prop:Trn} = 0
    ?ImeiNumber{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Repackage')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:JOBS.Open
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  IniFilepath =  CLIP(PATH()) & '\CEL14DAY.INI'
  OriginalAccount1 = GETINI('DEFAULTS','OriginalAccount1',,IniFilepath)
  OriginalAccount2 = GETINI('DEFAULTS','OriginalAccount2',,IniFilepath)
  OriginalChargeType = GETINI('DEFAULTS','OriginalChargeType',,IniFilepath)
  NewAccount = GETINI('DEFAULTS','NewAccount',,IniFilepath)
  NewChargeType = GETINI('DEFAULTS','NewChargeType',,IniFilepath)
  PrintJoblabel = GETINI('DEFAULTS','PrintJoblabel',0,IniFilepath)
  
  IF (OriginalAccount1 = '' OR OriginalAccount2 = '' OR OriginalChargeType = '' OR NewAccount = '' OR NewChargeType = '') THEN
      MESSAGE('14-Day Repackaging Defaults have not been set up')
      Post(Event:CloseWindow)
  END
  
  ! Initialise Jobs Queue
  FREE(JobsQueue)
  SORT(JobsQueue, jbq:JobNo)
  
  SELF.SetAlerts()
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?NextButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NextButton, Accepted)
      access:jobs.clearkey(job:ref_number_key)
      job:ref_number = JobNumber
      IF (access:jobs.fetch(job:ref_number_key) <> Level:Benign) THEN
          MESSAGE('Job Not Found')
      ELSIF (job:ESN <> ImeiNumber) THEN
          MESSAGE('IMEI Number does not match job')
      ELSIF ((job:Account_Number <> OriginalAccount1) AND (job:Account_Number <> OriginalAccount2)) THEN
          MESSAGE('Job not booked to Account ' & CLIP(OriginalAccount1) & ' or ' & CLIP(OriginalAccount2))
      ELSE
          temp_account" = job:Account_Number
          job:Account_Number = NewAccount
          job:Warranty_Job = 'YES'
          job:Chargeable_Job = 'NO'
          job:Warranty_Charge_Type = NewChargeType
          access:jobs.update()
      
          !IF (PrintJobLabel) THEN
          !    Set(defaults)
          !    access:defaults.next()
          !    glo:select1 = job:Ref_number
          !    CASE def:label_printer_type
          !        OF 'TEC B-440 / B-442'
          !            Thermal_Labels('')
          !        OF 'TEC B-452'
          !            Thermal_Labels_B452
          !    END
          !    glo:select1 = ''
          !END
      
          IF (access:audit.primerecord() = level:benign) THEN
              aud:ref_number    = job:ref_number
              aud:date          = today()
              aud:time          = clock()
              aud:type          = 'JOB'
              access:users.clearkey(use:password_key)
              use:password = glo:password
              access:users.fetch(use:password_key)
              aud:user          = use:user_code
              aud:action        = '14-DAY REPACKAGING(A)'
              aud:notes         = 'ORIGINAL ACCOUNT: ' & CLIP(temp_account")
              access:audit.insert()
          END
      
          count# = 0
          access:parts.clearkey(par:part_number_key)
          par:ref_number  = job:ref_number
          SET(par:part_number_key,par:part_number_key)
          LOOP
              IF ((access:parts.next() <> Level:Benign) OR (par:ref_number  <> job:ref_number)) THEN
                  BREAK
              END
              IF (access:warparts.primerecord() = Level:Benign) THEN
                  record_number#  = wpr:record_number
                  wpr:record      :=: par:record
                  wpr:record_number   = record_number#
                  IF (access:warparts.insert() <> Level:Benign) THEN
                      access:warparts.cancelautoinc()
                  END
              END
              DELETE(parts)
              count# += 1
          END
      
          IF (count# <> 0) THEN
              IF (access:audit.primerecord() = level:benign) THEN
                  aud:ref_number    = job:ref_number
                  aud:date          = today()
                  aud:time          = clock()
                  aud:type          = 'JOB'
                  access:users.clearkey(use:password_key)
                  use:password = glo:password
                  access:users.fetch(use:password_key)
                  aud:user = use:user_code
                  aud:action        = 'CHARGEABLE PARTS TRANSFERRED TO WARRANTY'
                  IF (access:audit.insert() <> Level:Benign) THEN
                      access:audit.cancelautoinc()
                  END
               END
          END
      
          CLEAR(JobsQueue)
          jbq:jobno = JobNumber
          jbq:imeino = ImeiNumber
          ADD(JobsQueue, jbq:jobno)
      
          JobNumber = ''
          ImeiNumber = ''
      
          SELECT(?JobNumber)
      
          DISPLAY()
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?NextButton, Accepted)
    OF ?FinishButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishButton, Accepted)
      Post(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?FinishButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

