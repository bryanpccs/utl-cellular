

   MEMBER('celrapid.clw')                             ! This is a MEMBER module

                     MAP
                       INCLUDE('CELRA006.INC'),ONCE        !Local module procedure declarations
                     END


Lookup_Postcode      PROCEDURE  (Postcode,Address_1,Address_2,Address_3,ThreadFlag) ! Declare Procedure
Flag                 BYTE
LocalAddressPath     STRING(255)
LocalThreadFlag      BYTE
LowByte              BYTE
HiByte               BYTE
OS_Version           ULONG
Local_Address        STRING(100)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
  CODE
! Before Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
  if GLO:SolaceViewVariables = true then   !If toolbox is active
    SolaceViewVars('',0,'LogProc',0,'Lookup_Postcode')      !Add Procedure to Log
  end


   Relate:DEFAULTS.Open
   !Postcode Address Program Procedure
   !==================================

   !Lookup_Postcode(STRING,STRING,STRING,STRING),BYTE
   !Lookup_Postcode(Postcode,Address_1,Address_2,Address_3),Flag

   !Flag      = 0 (Success)
   !          = 1 (No Post Code Match)
   !          = 2 (Internal Error)

   !Get Address Path From Defaults File
   SET(Defaults)
   IF Access:Defaults.NEXT() THEN
     Flag = 2
     DO Program_Error
   END !IF

   !figure out the damn operating system and reverse flags appropriately
   !to cope with difference in threading models
   OS_Version = GetVersion()
   LowByte = BShift(OS_Version,-8)
   HiByte = OS_Version
   !if not NT
   IF LowByte <> 0 THEN
      LocalThreadFlag = 1
   ELSE
       LocalThreadFlag = ThreadFlag
   END !IF
   !Call Address Program

   SETCLIPBOARD(CLIP(Postcode))
   RUN(CLIP(def:postcode_path) & '\Addressc.exe',LocalThreadFlag)
   IF ERROR() THEN
     Flag = 2
     DO Program_Error
   END !IF
   Local_Address = CLIPBOARD()
   DO Convert_Address
   Relate:Defaults.Close
   Flag = 0
   RETURN(Flag)

Program_Error        ROUTINE
   !Error Handling Routine
   Relate:Defaults.Close
   RETURN(Flag)

Convert_Address     ROUTINE
   !Format String
   A# = 1
   Num# = 1
   IF Local_Address[Num#] <> '#' THEN
     LOOP UNTIL Num# = LEN(CLIP(Local_Address))
       IF Local_Address[Num#] = ';' THEN BREAK.
       Address_1[A#] = Local_Address[Num#]
       A# += 1
       Num# += 1
     END !LOOP
     Num# += 1
     A# = 1
     LOOP UNTIL Num# = LEN(CLIP(Local_Address))
       IF Local_Address[Num#] = ';' THEN BREAK.
       Address_2[A#] = Local_Address[Num#]
       A# += 1
       Num# += 1
     END !LOOP

     Num# += 1
     A# = 1

     Check# = 0
     LOOP B# = Num# TO LEN(CLIP(Local_Address))
       IF Local_Address[B#] = ';' THEN Check# = 1.
     END !LOOP

     IF Check# = 1 THEN
       LOOP UNTIL Num# = LEN(CLIP(Local_Address))
         IF Local_Address[Num#] = ';' THEN BREAK.
         Address_3[A#] = Local_Address[Num#]
         A# += 1
         Num# += 1
       END !Loop
     END !IF
   ELSE
     Flag = 1
     DO Program_Error
   END !IF

   !Uppercase Strings
   Address_1 = UPPER(Address_1)
   Address_2 = UPPER(Address_2)
   Address_3 = UPPER(Address_3)



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Lookup_Postcode',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('Flag',Flag,'Lookup_Postcode',1)
    SolaceViewVars('LocalAddressPath',LocalAddressPath,'Lookup_Postcode',1)
    SolaceViewVars('LocalThreadFlag',LocalThreadFlag,'Lookup_Postcode',1)
    SolaceViewVars('LowByte',LowByte,'Lookup_Postcode',1)
    SolaceViewVars('HiByte',HiByte,'Lookup_Postcode',1)
    SolaceViewVars('OS_Version',OS_Version,'Lookup_Postcode',1)
    SolaceViewVars('Local_Address',Local_Address,'Lookup_Postcode',1)


! After Embed Point: %ProcessedCode) DESC(Processed Code) ARG()
