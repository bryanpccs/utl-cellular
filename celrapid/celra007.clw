

   MEMBER('celrapid.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA007.INC'),ONCE        !Local module procedure declarations
                     END


Amend_Delivery_Address PROCEDURE                      !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
window               WINDOW('Delivery Address'),AT(,,219,148),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,112),USE(?Sheet1),SPREAD
                         TAB('Delivery Address'),USE(?Tab1)
                           PROMPT('Company Name'),AT(8,20),USE(?JOB:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(job:Company_Name_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Postcode'),AT(8,36),USE(?JOB:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(84,36,64,10),USE(job:Postcode_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           BUTTON('Clear / Copy'),AT(148,36,60,10),USE(?Clear_Address),LEFT,ICON(ICON:Cut)
                           PROMPT('Address'),AT(8,48),USE(?JOB:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,48,124,10),USE(job:Address_Line1_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(84,60,124,10),USE(job:Address_Line2_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(84,72,124,10),USE(job:Address_Line3_Delivery),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(8,88),USE(?JOB:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,88,64,10),USE(job:Telephone_Delivery),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       PANEL,AT(4,120,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(156,124,56,16),USE(?Close),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?JOB:Company_Name:Prompt{prop:FontColor} = -1
    ?JOB:Company_Name:Prompt{prop:Color} = 15066597
    If ?job:Company_Name_Delivery{prop:ReadOnly} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 15066597
    Elsif ?job:Company_Name_Delivery{prop:Req} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 8454143
    Else ! If ?job:Company_Name_Delivery{prop:Req} = True
        ?job:Company_Name_Delivery{prop:FontColor} = 65793
        ?job:Company_Name_Delivery{prop:Color} = 16777215
    End ! If ?job:Company_Name_Delivery{prop:Req} = True
    ?job:Company_Name_Delivery{prop:Trn} = 0
    ?job:Company_Name_Delivery{prop:FontStyle} = font:Bold
    ?JOB:Postcode:Prompt{prop:FontColor} = -1
    ?JOB:Postcode:Prompt{prop:Color} = 15066597
    If ?job:Postcode_Delivery{prop:ReadOnly} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 15066597
    Elsif ?job:Postcode_Delivery{prop:Req} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 8454143
    Else ! If ?job:Postcode_Delivery{prop:Req} = True
        ?job:Postcode_Delivery{prop:FontColor} = 65793
        ?job:Postcode_Delivery{prop:Color} = 16777215
    End ! If ?job:Postcode_Delivery{prop:Req} = True
    ?job:Postcode_Delivery{prop:Trn} = 0
    ?job:Postcode_Delivery{prop:FontStyle} = font:Bold
    ?JOB:Address_Line1:Prompt{prop:FontColor} = -1
    ?JOB:Address_Line1:Prompt{prop:Color} = 15066597
    If ?job:Address_Line1_Delivery{prop:ReadOnly} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line1_Delivery{prop:Req} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line1_Delivery{prop:Req} = True
        ?job:Address_Line1_Delivery{prop:FontColor} = 65793
        ?job:Address_Line1_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line1_Delivery{prop:Req} = True
    ?job:Address_Line1_Delivery{prop:Trn} = 0
    ?job:Address_Line1_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line2_Delivery{prop:ReadOnly} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line2_Delivery{prop:Req} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line2_Delivery{prop:Req} = True
        ?job:Address_Line2_Delivery{prop:FontColor} = 65793
        ?job:Address_Line2_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line2_Delivery{prop:Req} = True
    ?job:Address_Line2_Delivery{prop:Trn} = 0
    ?job:Address_Line2_Delivery{prop:FontStyle} = font:Bold
    If ?job:Address_Line3_Delivery{prop:ReadOnly} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 15066597
    Elsif ?job:Address_Line3_Delivery{prop:Req} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 8454143
    Else ! If ?job:Address_Line3_Delivery{prop:Req} = True
        ?job:Address_Line3_Delivery{prop:FontColor} = 65793
        ?job:Address_Line3_Delivery{prop:Color} = 16777215
    End ! If ?job:Address_Line3_Delivery{prop:Req} = True
    ?job:Address_Line3_Delivery{prop:Trn} = 0
    ?job:Address_Line3_Delivery{prop:FontStyle} = font:Bold
    ?JOB:Telephone_Number:Prompt{prop:FontColor} = -1
    ?JOB:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?job:Telephone_Delivery{prop:ReadOnly} = True
        ?job:Telephone_Delivery{prop:FontColor} = 65793
        ?job:Telephone_Delivery{prop:Color} = 15066597
    Elsif ?job:Telephone_Delivery{prop:Req} = True
        ?job:Telephone_Delivery{prop:FontColor} = 65793
        ?job:Telephone_Delivery{prop:Color} = 8454143
    Else ! If ?job:Telephone_Delivery{prop:Req} = True
        ?job:Telephone_Delivery{prop:FontColor} = 65793
        ?job:Telephone_Delivery{prop:Color} = 16777215
    End ! If ?job:Telephone_Delivery{prop:Req} = True
    ?job:Telephone_Delivery{prop:Trn} = 0
    ?job:Telephone_Delivery{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Amend_Delivery_Address',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Company_Name:Prompt;  SolaceCtrlName = '?JOB:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Delivery;  SolaceCtrlName = '?job:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Postcode:Prompt;  SolaceCtrlName = '?JOB:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Delivery;  SolaceCtrlName = '?job:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Address;  SolaceCtrlName = '?Clear_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Address_Line1:Prompt;  SolaceCtrlName = '?JOB:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Delivery;  SolaceCtrlName = '?job:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Delivery;  SolaceCtrlName = '?job:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Delivery;  SolaceCtrlName = '?job:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Telephone_Number:Prompt;  SolaceCtrlName = '?JOB:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Delivery;  SolaceCtrlName = '?job:Telephone_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Amend_Delivery_Address')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Amend_Delivery_Address')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?JOB:Company_Name:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:JOBS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Amend_Delivery_Address',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job:Postcode_Delivery
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Postcode_Delivery, Accepted)
      Postcode_Routine(job:postcode_delivery,job:address_line1_delivery,job:address_line2_delivery,job:address_line3_delivery)
      Select(?job:address_line1_delivery,1)
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Postcode_Delivery, Accepted)
    OF ?Clear_Address
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      glo:select1 = 'DELIVERY'
      If job:address_line1_delivery <> ''
          glo:select2 = '1'
      Else
          glo:select2 = '2'
      End
      If job:address_line1_collection = ''
          glo:select3 = 'COLLECTION'
      End
      
      Clear_Address_Window
      
      Case glo:select1
          Of '1'
              job:postcode_delivery      = ''
              job:company_name_delivery  = ''
              job:address_line1_delivery = ''
              job:address_line2_delivery = ''
              job:address_line3_delivery = ''
              job:telephone_delivery     = ''
              Select(?Job:postcode_delivery)
          Of '2'
              job:postcode_delivery      = job:postcode
              job:company_name_delivery  = job:company_name
              job:address_line1_delivery = job:address_line1
              job:address_line2_delivery = job:address_line2
              job:address_line3_delivery = job:address_line3
              job:telephone_delivery     = job:telephone_number
          Of '3'
              job:postcode_delivery      = job:postcode_collection
              job:company_name_delivery  = job:company_name_collection
              job:address_line1_delivery = job:address_line1_collection
              job:address_line2_delivery = job:address_line2_collection
              job:address_line3_delivery = job:address_line3_collection
              job:telephone_delivery     = job:telephone_collection
          Of 'CANCEL'
      End
      Select(?job:postcode_delivery)
      Display()
      glo:select1 = ''
      glo:select2 = ''
      glo:select3 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Address, Accepted)
    OF ?job:Telephone_Delivery
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Telephone_Delivery, Accepted)
      
          temp_string = Clip(left(job:Telephone_Delivery))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Delivery    = temp_string
          Display(?job:Telephone_Delivery)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Telephone_Delivery, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Amend_Delivery_Address')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

