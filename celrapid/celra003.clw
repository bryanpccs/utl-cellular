

   MEMBER('celrapid.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA003.INC'),ONCE        !Local module procedure declarations
                     END


Amend_Invoice_Address PROCEDURE                       !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
!! ** Bryan Harrison (c)1998 **
temp_string         String(255)
window               WINDOW('Customer Address'),AT(,,219,167),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),GRAY,DOUBLE
                       SHEET,AT(4,4,212,132),USE(?Sheet1),SPREAD
                         TAB('Customer Address'),USE(?Tab1)
                           PROMPT('Title'),AT(84,16),USE(?Title),TRN,FONT(,7,,)
                           ENTRY(@s4),AT(84,24,24,10),USE(job:Title),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s1),AT(108,24,20,10),USE(job:Initial),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Initial'),AT(108,16),USE(?Initial),TRN,FONT(,7,,)
                           PROMPT('Surname'),AT(128,16),USE(?Surname),TRN,FONT(,7,,)
                           ENTRY(@s30),AT(128,24,80,10),USE(job:Surname),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Company Name'),AT(8,40),USE(?JOB:Company_Name:Prompt),TRN
                           ENTRY(@s30),AT(84,40,124,10),USE(job:Company_Name),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Customer Name'),AT(8,24),USE(?Customer_Name),TRN
                           PROMPT('Postcode'),AT(8,56),USE(?JOB:Postcode:Prompt),TRN
                           ENTRY(@s10),AT(84,56,64,10),USE(job:Postcode),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Address'),AT(8,68),USE(?JOB:Address_Line1:Prompt),TRN
                           ENTRY(@s30),AT(84,68,124,10),USE(job:Address_Line1),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(84,80,124,10),USE(job:Address_Line2),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           ENTRY(@s30),AT(84,92,124,10),USE(job:Address_Line3),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Telephone Number'),AT(8,108),USE(?JOB:Telephone_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,108,64,10),USE(job:Telephone_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Fax Number'),AT(8,120),USE(?JOB:Fax_Number:Prompt),TRN
                           ENTRY(@s15),AT(84,120,64,10),USE(job:Fax_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       PANEL,AT(4,140,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(156,144,56,16),USE(?Close),LEFT,ICON('ok.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
szpostcode      CString(15)
szpath          CString(255)
szaddress       CString(255)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Title{prop:FontColor} = -1
    ?Title{prop:Color} = 15066597
    If ?job:Title{prop:ReadOnly} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 15066597
    Elsif ?job:Title{prop:Req} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 8454143
    Else ! If ?job:Title{prop:Req} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 16777215
    End ! If ?job:Title{prop:Req} = True
    ?job:Title{prop:Trn} = 0
    ?job:Title{prop:FontStyle} = font:Bold
    If ?job:Initial{prop:ReadOnly} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 15066597
    Elsif ?job:Initial{prop:Req} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 8454143
    Else ! If ?job:Initial{prop:Req} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 16777215
    End ! If ?job:Initial{prop:Req} = True
    ?job:Initial{prop:Trn} = 0
    ?job:Initial{prop:FontStyle} = font:Bold
    ?Initial{prop:FontColor} = -1
    ?Initial{prop:Color} = 15066597
    ?Surname{prop:FontColor} = -1
    ?Surname{prop:Color} = 15066597
    If ?job:Surname{prop:ReadOnly} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 15066597
    Elsif ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 8454143
    Else ! If ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 16777215
    End ! If ?job:Surname{prop:Req} = True
    ?job:Surname{prop:Trn} = 0
    ?job:Surname{prop:FontStyle} = font:Bold
    ?JOB:Company_Name:Prompt{prop:FontColor} = -1
    ?JOB:Company_Name:Prompt{prop:Color} = 15066597
    If ?job:Company_Name{prop:ReadOnly} = True
        ?job:Company_Name{prop:FontColor} = 65793
        ?job:Company_Name{prop:Color} = 15066597
    Elsif ?job:Company_Name{prop:Req} = True
        ?job:Company_Name{prop:FontColor} = 65793
        ?job:Company_Name{prop:Color} = 8454143
    Else ! If ?job:Company_Name{prop:Req} = True
        ?job:Company_Name{prop:FontColor} = 65793
        ?job:Company_Name{prop:Color} = 16777215
    End ! If ?job:Company_Name{prop:Req} = True
    ?job:Company_Name{prop:Trn} = 0
    ?job:Company_Name{prop:FontStyle} = font:Bold
    ?Customer_Name{prop:FontColor} = -1
    ?Customer_Name{prop:Color} = 15066597
    ?JOB:Postcode:Prompt{prop:FontColor} = -1
    ?JOB:Postcode:Prompt{prop:Color} = 15066597
    If ?job:Postcode{prop:ReadOnly} = True
        ?job:Postcode{prop:FontColor} = 65793
        ?job:Postcode{prop:Color} = 15066597
    Elsif ?job:Postcode{prop:Req} = True
        ?job:Postcode{prop:FontColor} = 65793
        ?job:Postcode{prop:Color} = 8454143
    Else ! If ?job:Postcode{prop:Req} = True
        ?job:Postcode{prop:FontColor} = 65793
        ?job:Postcode{prop:Color} = 16777215
    End ! If ?job:Postcode{prop:Req} = True
    ?job:Postcode{prop:Trn} = 0
    ?job:Postcode{prop:FontStyle} = font:Bold
    ?JOB:Address_Line1:Prompt{prop:FontColor} = -1
    ?JOB:Address_Line1:Prompt{prop:Color} = 15066597
    If ?job:Address_Line1{prop:ReadOnly} = True
        ?job:Address_Line1{prop:FontColor} = 65793
        ?job:Address_Line1{prop:Color} = 15066597
    Elsif ?job:Address_Line1{prop:Req} = True
        ?job:Address_Line1{prop:FontColor} = 65793
        ?job:Address_Line1{prop:Color} = 8454143
    Else ! If ?job:Address_Line1{prop:Req} = True
        ?job:Address_Line1{prop:FontColor} = 65793
        ?job:Address_Line1{prop:Color} = 16777215
    End ! If ?job:Address_Line1{prop:Req} = True
    ?job:Address_Line1{prop:Trn} = 0
    ?job:Address_Line1{prop:FontStyle} = font:Bold
    If ?job:Address_Line2{prop:ReadOnly} = True
        ?job:Address_Line2{prop:FontColor} = 65793
        ?job:Address_Line2{prop:Color} = 15066597
    Elsif ?job:Address_Line2{prop:Req} = True
        ?job:Address_Line2{prop:FontColor} = 65793
        ?job:Address_Line2{prop:Color} = 8454143
    Else ! If ?job:Address_Line2{prop:Req} = True
        ?job:Address_Line2{prop:FontColor} = 65793
        ?job:Address_Line2{prop:Color} = 16777215
    End ! If ?job:Address_Line2{prop:Req} = True
    ?job:Address_Line2{prop:Trn} = 0
    ?job:Address_Line2{prop:FontStyle} = font:Bold
    If ?job:Address_Line3{prop:ReadOnly} = True
        ?job:Address_Line3{prop:FontColor} = 65793
        ?job:Address_Line3{prop:Color} = 15066597
    Elsif ?job:Address_Line3{prop:Req} = True
        ?job:Address_Line3{prop:FontColor} = 65793
        ?job:Address_Line3{prop:Color} = 8454143
    Else ! If ?job:Address_Line3{prop:Req} = True
        ?job:Address_Line3{prop:FontColor} = 65793
        ?job:Address_Line3{prop:Color} = 16777215
    End ! If ?job:Address_Line3{prop:Req} = True
    ?job:Address_Line3{prop:Trn} = 0
    ?job:Address_Line3{prop:FontStyle} = font:Bold
    ?JOB:Telephone_Number:Prompt{prop:FontColor} = -1
    ?JOB:Telephone_Number:Prompt{prop:Color} = 15066597
    If ?job:Telephone_Number{prop:ReadOnly} = True
        ?job:Telephone_Number{prop:FontColor} = 65793
        ?job:Telephone_Number{prop:Color} = 15066597
    Elsif ?job:Telephone_Number{prop:Req} = True
        ?job:Telephone_Number{prop:FontColor} = 65793
        ?job:Telephone_Number{prop:Color} = 8454143
    Else ! If ?job:Telephone_Number{prop:Req} = True
        ?job:Telephone_Number{prop:FontColor} = 65793
        ?job:Telephone_Number{prop:Color} = 16777215
    End ! If ?job:Telephone_Number{prop:Req} = True
    ?job:Telephone_Number{prop:Trn} = 0
    ?job:Telephone_Number{prop:FontStyle} = font:Bold
    ?JOB:Fax_Number:Prompt{prop:FontColor} = -1
    ?JOB:Fax_Number:Prompt{prop:Color} = 15066597
    If ?job:Fax_Number{prop:ReadOnly} = True
        ?job:Fax_Number{prop:FontColor} = 65793
        ?job:Fax_Number{prop:Color} = 15066597
    Elsif ?job:Fax_Number{prop:Req} = True
        ?job:Fax_Number{prop:FontColor} = 65793
        ?job:Fax_Number{prop:Color} = 8454143
    Else ! If ?job:Fax_Number{prop:Req} = True
        ?job:Fax_Number{prop:FontColor} = 65793
        ?job:Fax_Number{prop:Color} = 16777215
    End ! If ?job:Fax_Number{prop:Req} = True
    ?job:Fax_Number{prop:Trn} = 0
    ?job:Fax_Number{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Amend_Invoice_Address',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Title;  SolaceCtrlName = '?Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Title;  SolaceCtrlName = '?job:Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Initial;  SolaceCtrlName = '?job:Initial';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Initial;  SolaceCtrlName = '?Initial';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Surname;  SolaceCtrlName = '?Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Surname;  SolaceCtrlName = '?job:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Company_Name:Prompt;  SolaceCtrlName = '?JOB:Company_Name:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name;  SolaceCtrlName = '?job:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Customer_Name;  SolaceCtrlName = '?Customer_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Postcode:Prompt;  SolaceCtrlName = '?JOB:Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode;  SolaceCtrlName = '?job:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Address_Line1:Prompt;  SolaceCtrlName = '?JOB:Address_Line1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1;  SolaceCtrlName = '?job:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2;  SolaceCtrlName = '?job:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3;  SolaceCtrlName = '?job:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Telephone_Number:Prompt;  SolaceCtrlName = '?JOB:Telephone_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Telephone_Number;  SolaceCtrlName = '?job:Telephone_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Fax_Number:Prompt;  SolaceCtrlName = '?JOB:Fax_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Fax_Number;  SolaceCtrlName = '?job:Fax_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  ClarioNET:InitWindow(ClarioNETWindow, window, 1)    !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Amend_Invoice_Address')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Amend_Invoice_Address')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Title
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
      Hide(?job:surname)
      Hide(?job:title)
      Hide(?title)
      Hide(?surname)
      Hide(?customer_name)
      Hide(?initial)
      Hide(?job:initial)
      access:subtracc.clearkey(sub:account_number_key)
      sub:account_number = job:account_number
      if access:subtracc.fetch(sub:account_number_key) = Level:Benign
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number = sub:main_account_Number
          if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
              If tra:use_contact_name = 'YES'
                  Unhide(?job:surname)
                  Unhide(?job:title)
                  Unhide(?title)
                  Unhide(?surname)
                  Unhide(?customer_name)
                  unhide(?initial)
                  unhide(?job:initial)
              End
          end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
      end !if access:subtracc.fetch(sub:account_number_key) = Level:Benign
  Do RecolourWindow
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Amend_Invoice_Address',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job:Postcode
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Postcode, Accepted)
      Postcode_Routine(job:postcode,job:address_line1,job:address_line2,job:address_line3)
      Select(?job:address_line1,1)
      Display()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Postcode, Accepted)
    OF ?job:Telephone_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Telephone_Number, Accepted)
      
          temp_string = Clip(left(job:Telephone_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Telephone_Number    = temp_string
          Display(?job:Telephone_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Telephone_Number, Accepted)
    OF ?job:Fax_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fax_Number, Accepted)
      
          temp_string = Clip(left(job:Fax_Number))
          string_length# = Len(temp_string)
          string_position# = 1
      
          temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
          Loop string_position# = 1 To string_length#
              If sub(temp_string,string_position#,1) = ' '
                  temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
              End
          End
      
          job:Fax_Number    = temp_string
          Display(?job:Fax_Number)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Fax_Number, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Amend_Invoice_Address')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

