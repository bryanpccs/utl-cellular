

   MEMBER('celrapid.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('CELRA004.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA009.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('CELRA013.INC'),ONCE        !Req'd for module callout resolution
                     END


Update_Jobs PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
MSN_Field_Mask       STRING(30)
ValidateMobileNumber BYTE(0)
temp_255string       STRING(255)
save_job_id          USHORT,AUTO
save_moc_id          USHORT,AUTO
force_fault_codes_temp STRING(3)
tmp:printreceipt     BYTE(0)
tmp:retain           BYTE(0)
save_xch_id          USHORT,AUTO
sav:path             STRING(255)
save_cha_id          USHORT,AUTO
save_maf_id          USHORT,AUTO
Transit_Type_Temp    STRING(30)
print_label_temp     STRING('NO {1}')
label_type_temp      STRING(30)
print_job_card_temp  STRING('NO {1}')
sav:ref_number       LONG
sav:esn              STRING(20)
save_jac_id          USHORT,AUTO
location_temp        STRING(30)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
Account_Number_Temp  STRING(15)
save_job_ali_id      USHORT
model_number_temp    STRING(30)
ESN_temp             STRING(16)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
left_title_temp      STRING(80)
right_title_temp     STRING(80)
accessories_temp     STRING(30)
save_taf_id          USHORT,AUTO
ESN_Entry_Temp       STRING(18)
tmp:SkillLevel       LONG
tmp:Print_JobCard    BYTE(0)
tmp:Network          STRING(30)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::job:Record  LIKE(job:RECORD),STATIC
!! ** Bryan Harrison (c)1998 **
temp_string String(255)
QuickWindow          WINDOW('Update the JOBS File'),AT(,,530,251),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Update_Jobs'),ALRT(F4Key),ALRT(F5Key),ALRT(F6Key),ALRT(F7Key),ALRT(F8Key),ALRT(F9Key),SYSTEM,GRAY,DOUBLE,MDI
                       SHEET,AT(4,4,220,216),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Tab 1'),USE(?Tab1)
                           STRING(@s80),AT(8,8,208,12),USE(left_title_temp),LEFT,FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Transit Type'),AT(8,20),USE(?JOB:Transit_Type:Prompt),TRN
                           ENTRY(@s30),AT(80,20,124,8),USE(job:Transit_Type),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(208,20,10,8),USE(?Lookup_Transit_Type),SKIP,ICON('list3.ico')
                           PROMPT('I.M.E.I. Number'),AT(8,32),USE(?JOB:ESN:Prompt),TRN
                           ENTRY(@s20),AT(80,32,124,8),USE(job:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('M.S.N.'),AT(8,44),USE(?JOB:MSN:Prompt),TRN,HIDE
                           ENTRY(@s16),AT(80,44,124,8),USE(job:MSN),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Product Code'),AT(8,56),USE(?job:ProductCode:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(80,56,124,8),USE(job:ProductCode),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(DownKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(EnterKey),UPR
                           BUTTON,AT(208,56,10,8),USE(?LookupProductCode),SKIP,ICON('List3.ico')
                           PROMPT('Model Number'),AT(8,68),USE(?JOB:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(80,68,124,8),USE(job:Model_Number),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(208,68,10,8),USE(?Lookup_Model_Number),SKIP,ICON('list3.ico')
                           PROMPT('Unit Type '),AT(8,80),USE(?JOB:Unit_Type:Prompt:2),TRN
                           ENTRY(@s30),AT(80,80,124,8),USE(job:Unit_Type),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),UPR
                           BUTTON,AT(208,80,10,8),USE(?Lookup_Unit_Type),SKIP,ICON('list3.ico')
                           PROMPT('SIM Number'),AT(8,92),USE(?jobe:SIMNumber:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(80,92,124,8),USE(jobe:SIMNumber),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('SIM Number'),TIP('SIM Number'),UPR
                           ENTRY(@s30),AT(80,104,124,8),USE(job:Colour),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),UPR
                           BUTTON,AT(208,104,10,8),USE(?Lookup_Colour),SKIP,HIDE,ICON('list3.ico')
                           PROMPT('Network'),AT(8,116),USE(?tmp:Network:Prompt),TRN,HIDE,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(80,116,124,8),USE(tmp:Network),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Network'),TIP('Network'),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),ALRT(DownKey),UPR
                           BUTTON,AT(208,116,10,8),USE(?LookupNetwork),SKIP,HIDE,ICON('List3.ico')
                           ENTRY(@d6),AT(80,128,56,8),USE(job:DOP),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Skill Level'),AT(144,128),USE(?tmp:SkillLevel:Prompt),TRN,HIDE,FONT(,8,,)
                           SPIN(@n8),AT(180,128,24,8),USE(tmp:SkillLevel),HIDE,RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Job Skill Level'),TIP('Job Skill Level'),UPR,STEP(1)
                           PROMPT('Date Of Purchase'),AT(8,128),USE(?JOB:DOP:Prompt),TRN
                           PROMPT('Mobile Number'),AT(8,140),USE(?JOB:Mobile_Number:Prompt),TRN
                           ENTRY(@s15),AT(80,140,124,8),USE(job:Mobile_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Location'),AT(8,152),USE(?JOB:Location:Prompt),TRN
                           ENTRY(@s30),AT(80,152,124,8),USE(job:Location),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight)
                           BUTTON,AT(208,152,10,8),USE(?Lookup_Location),SKIP,ICON('list3.ico')
                           PROMPT('Authority Number'),AT(8,164),USE(?JOB:Authority_Number:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(80,164,124,8),USE(job:Authority_Number),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Insurance Ref'),AT(8,176),USE(?job:insurance_reference_number:prompt),TRN
                           ENTRY(@s30),AT(80,176,124,8),USE(job:Insurance_Reference_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           STRING('BOUNCER'),AT(8,204),USE(?bouncer_text),HIDE,FONT(,14,COLOR:Red,FONT:bold)
                           CHECK('Print Duplicate Job Card'),AT(128,204),USE(tmp:Print_JobCard),HIDE,MSG('Print Duplicate Job Card'),TIP('Print Duplicate Job Card'),VALUE('1','0')
                           PROMPT('Colour'),AT(8,104),USE(?job:Colour:Prompt),TRN,HIDE
                         END
                       END
                       SHEET,AT(228,4,240,152),USE(?Sheet3),WIZARD,SPREAD
                         TAB('Tab 3'),USE(?Tab3)
                           STRING(@s80),AT(236,8,224,12),USE(right_title_temp),RIGHT,FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Account Number'),AT(236,20),USE(?JOB:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(324,20,124,8),USE(job:Account_Number),LEFT,FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),REQ,UPR
                           BUTTON,AT(452,20,10,8),USE(?Lookup_Account_Number),SKIP,ICON('list3.ico')
                           PROMPT('Order Number'),AT(236,32),USE(?JOB:Order_Number:Prompt),TRN
                           ENTRY(@s30),AT(324,32,124,8),USE(job:Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Customer Name'),AT(236,56),USE(?Customer_Name),TRN,HIDE
                           PROMPT('Title'),AT(324,48),USE(?job:Title:Prompt),TRN,HIDE,FONT(,7,,)
                           PROMPT('Initial'),AT(352,48),USE(?job:Initial:Prompt),TRN,HIDE,FONT(,7,,)
                           ENTRY(@s4),AT(324,56,24,8),USE(job:Title),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           ENTRY(@s1),AT(352,56,12,8),USE(job:Initial),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Surname'),AT(368,48),USE(?job:Surname:Prompt),TRN,HIDE,FONT(,7,,)
                           ENTRY(@s30),AT(368,56,80,8),USE(job:Surname),HIDE,LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           CHECK('Chargeable Job'),AT(236,72,64,12),USE(job:Chargeable_Job),LEFT,VALUE('YES','NO')
                           BUTTON,AT(312,72,10,8),USE(?lookup_Charge_Type),ICON('list3.ico')
                           ENTRY(@s30),AT(324,72,124,8),USE(job:Charge_Type),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           CHECK('Warranty Job'),AT(236,84,64,12),USE(job:Warranty_Job),LEFT,VALUE('YES','NO')
                           BUTTON,AT(312,84,10,8),USE(?Lookup_Warranty_Charge_Type),ICON('list3.ico')
                           ENTRY(@s30),AT(324,84,124,8),USE(job:Warranty_Charge_Type),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           CHECK('Intermittent Fault'),AT(236,96,64,12),USE(job:Intermittent_Fault),HIDE,LEFT,VALUE('YES','NO')
                           PROMPT('Incoming Courier'),AT(236,112),USE(?JOB:Incoming_Courier:Prompt),TRN
                           ENTRY(@s30),AT(324,112,124,8),USE(job:Incoming_Courier),FONT('Tahoma',8,,FONT:bold),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(DownKey),ALRT(MouseRight),UPR
                           BUTTON,AT(452,112,10,8),USE(?Lookup_Courier),SKIP,ICON('list3.ico')
                           PROMPT('Consignment Number'),AT(236,128),USE(?JOB:Incoming_Consignment_Number:Prompt),TRN
                           ENTRY(@s30),AT(324,128,124,8),USE(job:Incoming_Consignment_Number),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Incoming Date'),AT(236,144),USE(?JOB:Incoming_Date:Prompt),TRN
                           ENTRY(@d6b),AT(324,144,64,8),USE(job:Incoming_Date),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                       END
                       PANEL,AT(472,4,56,216),USE(?Panel2),FILL(COLOR:Silver)
                       SHEET,AT(228,160,240,60),USE(?Sheet2),BELOW,WIZARD,SPREAD
                         TAB('Address Details'),USE(?InvoiceAddress)
                           PROMPT('Customer Address'),AT(232,164),USE(?Invoice_Address),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Company Name'),AT(236,176),USE(?CompanyName:Prompt),TRN
                           BUTTON('A&mend Addresses [F9]'),AT(356,164,104,12),USE(?Amend),LEFT,ICON('ADDMEMO1.ICO')
                           PROMPT('Address'),AT(236,184),USE(?Address:prompt),TRN
                           STRING(@s30),AT(324,176,100,12),USE(job:Company_Name),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(324,184,100,10),USE(job:Address_Line1),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Postcode'),AT(236,208),USE(?Postcode:Prompt),TRN
                           STRING(@s30),AT(324,192,100,10),USE(job:Address_Line2),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(324,200,100,10),USE(job:Address_Line3),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s10),AT(324,208,100,10),USE(job:Postcode),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         END
                         TAB('Collection Address'),USE(?CollectionAddress)
                           PROMPT('Collection Address'),AT(232,164),USE(?Invoice_Address:2),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Company Name'),AT(236,176),USE(?CompanyName:Prompt:2),TRN
                           STRING(@s30),AT(324,176,100,12),USE(job:Company_Name_Collection),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           BUTTON('A&mend Addresses [F9]'),AT(360,164,100,12),USE(?Amend:2),LEFT,ICON('ADDMEMO1.ICO')
                           PROMPT('Address'),AT(236,184),USE(?Address:prompt:2),TRN
                           STRING(@s30),AT(324,184,100,10),USE(job:Address_Line1_Collection),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(324,192,100,10),USE(job:Address_Line2_Collection),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(324,200,100,10),USE(job:Address_Line3_Collection),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Postcode'),AT(236,208),USE(?Postcode:Prompt:2),TRN
                           STRING(@s10),AT(324,208,100,10),USE(job:Postcode_Collection),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         END
                         TAB('Delivery Address'),USE(?DeliveryAddress)
                           PROMPT('Delivery Address'),AT(232,164),USE(?Invoice_Address:3),FONT(,10,COLOR:Navy,FONT:bold,CHARSET:ANSI)
                           PROMPT('Company Name'),AT(236,176),USE(?CompanyName:Prompt:3),TRN
                           STRING(@s30),AT(324,176,100,12),USE(job:Company_Name_Delivery),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           BUTTON('A&mend Addresses [F9]'),AT(360,164,100,12),USE(?Amend:3),LEFT,ICON('ADDMEMO1.ICO')
                           PROMPT('Address'),AT(236,184),USE(?Address:prompt:3),TRN
                           STRING(@s30),AT(324,184,100,10),USE(job:Address_Line1_Delivery),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(324,192,100,10),USE(job:Address_Line2_Delivery),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           STRING(@s30),AT(324,200,100,10),USE(job:Address_Line3_Delivery),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Postcode'),AT(236,208),USE(?Postcode:Prompt:3),TRN
                           STRING(@s10),AT(324,208,100,10),USE(job:Postcode_Delivery),TRN,FONT(,,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PROMPT('Contact History [F8]'),AT(476,200,48,16),USE(?Prompt35:5),CENTER
                       PANEL,AT(4,224,524,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON,AT(476,8,48,16),USE(?Lookup_Accessory),ICON('stock_qu.gif')
                       PROMPT('Accessories [F4]'),AT(476,24,48,16),USE(?Prompt35),CENTER
                       PROMPT('Fault Codes [F5]'),AT(476,68,48,16),USE(?Prompt35:2),CENTER
                       PROMPT('Fault Descrip [F6]'),AT(476,112,48,16),USE(?Prompt35:3),CENTER
                       PROMPT('Engineers Notes [F7]'),AT(476,156,48,16),CENTER
                       BUTTON,AT(476,52,48,16),USE(?Fault_Codes_Lookup),LEFT,ICON('fault.gif')
                       BUTTON,AT(476,96,48,16),USE(?Fault_Description),LEFT,ICON('History.gif')
                       BUTTON,AT(476,140,48,16),USE(?engineers_notes),LEFT,ICON('History.gif')
                       BUTTON,AT(476,184,48,16),USE(?contact_history),LEFT,ICON('audit.gif')
                       BUTTON('&Repair History [F10]'),AT(8,228,68,16),USE(?Repair_History),HIDE,LEFT,ICON(ICON:Print1)
                       BUTTON('&OK'),AT(412,228,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(468,228,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
look:job:Transit_Type                Like(job:Transit_Type)
look:job:ProductCode                Like(job:ProductCode)
look:job:Model_Number                Like(job:Model_Number)
look:job:Unit_Type                Like(job:Unit_Type)
look:job:Colour                Like(job:Colour)
look:tmp:Network                Like(tmp:Network)
look:job:Location                Like(job:Location)
look:job:Account_Number                Like(job:Account_Number)
look:job:Incoming_Courier                Like(job:Incoming_Courier)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?left_title_temp{prop:FontColor} = -1
    ?left_title_temp{prop:Color} = 15066597
    ?JOB:Transit_Type:Prompt{prop:FontColor} = -1
    ?JOB:Transit_Type:Prompt{prop:Color} = 15066597
    If ?job:Transit_Type{prop:ReadOnly} = True
        ?job:Transit_Type{prop:FontColor} = 65793
        ?job:Transit_Type{prop:Color} = 15066597
    Elsif ?job:Transit_Type{prop:Req} = True
        ?job:Transit_Type{prop:FontColor} = 65793
        ?job:Transit_Type{prop:Color} = 8454143
    Else ! If ?job:Transit_Type{prop:Req} = True
        ?job:Transit_Type{prop:FontColor} = 65793
        ?job:Transit_Type{prop:Color} = 16777215
    End ! If ?job:Transit_Type{prop:Req} = True
    ?job:Transit_Type{prop:Trn} = 0
    ?job:Transit_Type{prop:FontStyle} = font:Bold
    ?JOB:ESN:Prompt{prop:FontColor} = -1
    ?JOB:ESN:Prompt{prop:Color} = 15066597
    If ?job:ESN{prop:ReadOnly} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 15066597
    Elsif ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 8454143
    Else ! If ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 16777215
    End ! If ?job:ESN{prop:Req} = True
    ?job:ESN{prop:Trn} = 0
    ?job:ESN{prop:FontStyle} = font:Bold
    ?JOB:MSN:Prompt{prop:FontColor} = -1
    ?JOB:MSN:Prompt{prop:Color} = 15066597
    If ?job:MSN{prop:ReadOnly} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 15066597
    Elsif ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 8454143
    Else ! If ?job:MSN{prop:Req} = True
        ?job:MSN{prop:FontColor} = 65793
        ?job:MSN{prop:Color} = 16777215
    End ! If ?job:MSN{prop:Req} = True
    ?job:MSN{prop:Trn} = 0
    ?job:MSN{prop:FontStyle} = font:Bold
    ?job:ProductCode:Prompt{prop:FontColor} = -1
    ?job:ProductCode:Prompt{prop:Color} = 15066597
    If ?job:ProductCode{prop:ReadOnly} = True
        ?job:ProductCode{prop:FontColor} = 65793
        ?job:ProductCode{prop:Color} = 15066597
    Elsif ?job:ProductCode{prop:Req} = True
        ?job:ProductCode{prop:FontColor} = 65793
        ?job:ProductCode{prop:Color} = 8454143
    Else ! If ?job:ProductCode{prop:Req} = True
        ?job:ProductCode{prop:FontColor} = 65793
        ?job:ProductCode{prop:Color} = 16777215
    End ! If ?job:ProductCode{prop:Req} = True
    ?job:ProductCode{prop:Trn} = 0
    ?job:ProductCode{prop:FontStyle} = font:Bold
    ?JOB:Model_Number:Prompt{prop:FontColor} = -1
    ?JOB:Model_Number:Prompt{prop:Color} = 15066597
    If ?job:Model_Number{prop:ReadOnly} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 15066597
    Elsif ?job:Model_Number{prop:Req} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 8454143
    Else ! If ?job:Model_Number{prop:Req} = True
        ?job:Model_Number{prop:FontColor} = 65793
        ?job:Model_Number{prop:Color} = 16777215
    End ! If ?job:Model_Number{prop:Req} = True
    ?job:Model_Number{prop:Trn} = 0
    ?job:Model_Number{prop:FontStyle} = font:Bold
    ?JOB:Unit_Type:Prompt:2{prop:FontColor} = -1
    ?JOB:Unit_Type:Prompt:2{prop:Color} = 15066597
    If ?job:Unit_Type{prop:ReadOnly} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 15066597
    Elsif ?job:Unit_Type{prop:Req} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 8454143
    Else ! If ?job:Unit_Type{prop:Req} = True
        ?job:Unit_Type{prop:FontColor} = 65793
        ?job:Unit_Type{prop:Color} = 16777215
    End ! If ?job:Unit_Type{prop:Req} = True
    ?job:Unit_Type{prop:Trn} = 0
    ?job:Unit_Type{prop:FontStyle} = font:Bold
    ?jobe:SIMNumber:Prompt{prop:FontColor} = -1
    ?jobe:SIMNumber:Prompt{prop:Color} = 15066597
    If ?jobe:SIMNumber{prop:ReadOnly} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 15066597
    Elsif ?jobe:SIMNumber{prop:Req} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 8454143
    Else ! If ?jobe:SIMNumber{prop:Req} = True
        ?jobe:SIMNumber{prop:FontColor} = 65793
        ?jobe:SIMNumber{prop:Color} = 16777215
    End ! If ?jobe:SIMNumber{prop:Req} = True
    ?jobe:SIMNumber{prop:Trn} = 0
    ?jobe:SIMNumber{prop:FontStyle} = font:Bold
    If ?job:Colour{prop:ReadOnly} = True
        ?job:Colour{prop:FontColor} = 65793
        ?job:Colour{prop:Color} = 15066597
    Elsif ?job:Colour{prop:Req} = True
        ?job:Colour{prop:FontColor} = 65793
        ?job:Colour{prop:Color} = 8454143
    Else ! If ?job:Colour{prop:Req} = True
        ?job:Colour{prop:FontColor} = 65793
        ?job:Colour{prop:Color} = 16777215
    End ! If ?job:Colour{prop:Req} = True
    ?job:Colour{prop:Trn} = 0
    ?job:Colour{prop:FontStyle} = font:Bold
    ?tmp:Network:Prompt{prop:FontColor} = -1
    ?tmp:Network:Prompt{prop:Color} = 15066597
    If ?tmp:Network{prop:ReadOnly} = True
        ?tmp:Network{prop:FontColor} = 65793
        ?tmp:Network{prop:Color} = 15066597
    Elsif ?tmp:Network{prop:Req} = True
        ?tmp:Network{prop:FontColor} = 65793
        ?tmp:Network{prop:Color} = 8454143
    Else ! If ?tmp:Network{prop:Req} = True
        ?tmp:Network{prop:FontColor} = 65793
        ?tmp:Network{prop:Color} = 16777215
    End ! If ?tmp:Network{prop:Req} = True
    ?tmp:Network{prop:Trn} = 0
    ?tmp:Network{prop:FontStyle} = font:Bold
    If ?job:DOP{prop:ReadOnly} = True
        ?job:DOP{prop:FontColor} = 65793
        ?job:DOP{prop:Color} = 15066597
    Elsif ?job:DOP{prop:Req} = True
        ?job:DOP{prop:FontColor} = 65793
        ?job:DOP{prop:Color} = 8454143
    Else ! If ?job:DOP{prop:Req} = True
        ?job:DOP{prop:FontColor} = 65793
        ?job:DOP{prop:Color} = 16777215
    End ! If ?job:DOP{prop:Req} = True
    ?job:DOP{prop:Trn} = 0
    ?job:DOP{prop:FontStyle} = font:Bold
    ?tmp:SkillLevel:Prompt{prop:FontColor} = -1
    ?tmp:SkillLevel:Prompt{prop:Color} = 15066597
    If ?tmp:SkillLevel{prop:ReadOnly} = True
        ?tmp:SkillLevel{prop:FontColor} = 65793
        ?tmp:SkillLevel{prop:Color} = 15066597
    Elsif ?tmp:SkillLevel{prop:Req} = True
        ?tmp:SkillLevel{prop:FontColor} = 65793
        ?tmp:SkillLevel{prop:Color} = 8454143
    Else ! If ?tmp:SkillLevel{prop:Req} = True
        ?tmp:SkillLevel{prop:FontColor} = 65793
        ?tmp:SkillLevel{prop:Color} = 16777215
    End ! If ?tmp:SkillLevel{prop:Req} = True
    ?tmp:SkillLevel{prop:Trn} = 0
    ?tmp:SkillLevel{prop:FontStyle} = font:Bold
    ?JOB:DOP:Prompt{prop:FontColor} = -1
    ?JOB:DOP:Prompt{prop:Color} = 15066597
    ?JOB:Mobile_Number:Prompt{prop:FontColor} = -1
    ?JOB:Mobile_Number:Prompt{prop:Color} = 15066597
    If ?job:Mobile_Number{prop:ReadOnly} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 15066597
    Elsif ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 8454143
    Else ! If ?job:Mobile_Number{prop:Req} = True
        ?job:Mobile_Number{prop:FontColor} = 65793
        ?job:Mobile_Number{prop:Color} = 16777215
    End ! If ?job:Mobile_Number{prop:Req} = True
    ?job:Mobile_Number{prop:Trn} = 0
    ?job:Mobile_Number{prop:FontStyle} = font:Bold
    ?JOB:Location:Prompt{prop:FontColor} = -1
    ?JOB:Location:Prompt{prop:Color} = 15066597
    If ?job:Location{prop:ReadOnly} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 15066597
    Elsif ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 8454143
    Else ! If ?job:Location{prop:Req} = True
        ?job:Location{prop:FontColor} = 65793
        ?job:Location{prop:Color} = 16777215
    End ! If ?job:Location{prop:Req} = True
    ?job:Location{prop:Trn} = 0
    ?job:Location{prop:FontStyle} = font:Bold
    ?JOB:Authority_Number:Prompt{prop:FontColor} = -1
    ?JOB:Authority_Number:Prompt{prop:Color} = 15066597
    If ?job:Authority_Number{prop:ReadOnly} = True
        ?job:Authority_Number{prop:FontColor} = 65793
        ?job:Authority_Number{prop:Color} = 15066597
    Elsif ?job:Authority_Number{prop:Req} = True
        ?job:Authority_Number{prop:FontColor} = 65793
        ?job:Authority_Number{prop:Color} = 8454143
    Else ! If ?job:Authority_Number{prop:Req} = True
        ?job:Authority_Number{prop:FontColor} = 65793
        ?job:Authority_Number{prop:Color} = 16777215
    End ! If ?job:Authority_Number{prop:Req} = True
    ?job:Authority_Number{prop:Trn} = 0
    ?job:Authority_Number{prop:FontStyle} = font:Bold
    ?job:insurance_reference_number:prompt{prop:FontColor} = -1
    ?job:insurance_reference_number:prompt{prop:Color} = 15066597
    If ?job:Insurance_Reference_Number{prop:ReadOnly} = True
        ?job:Insurance_Reference_Number{prop:FontColor} = 65793
        ?job:Insurance_Reference_Number{prop:Color} = 15066597
    Elsif ?job:Insurance_Reference_Number{prop:Req} = True
        ?job:Insurance_Reference_Number{prop:FontColor} = 65793
        ?job:Insurance_Reference_Number{prop:Color} = 8454143
    Else ! If ?job:Insurance_Reference_Number{prop:Req} = True
        ?job:Insurance_Reference_Number{prop:FontColor} = 65793
        ?job:Insurance_Reference_Number{prop:Color} = 16777215
    End ! If ?job:Insurance_Reference_Number{prop:Req} = True
    ?job:Insurance_Reference_Number{prop:Trn} = 0
    ?job:Insurance_Reference_Number{prop:FontStyle} = font:Bold
    ?bouncer_text{prop:FontColor} = -1
    ?bouncer_text{prop:Color} = 15066597
    ?tmp:Print_JobCard{prop:Font,3} = -1
    ?tmp:Print_JobCard{prop:Color} = 15066597
    ?tmp:Print_JobCard{prop:Trn} = 0
    ?job:Colour:Prompt{prop:FontColor} = -1
    ?job:Colour:Prompt{prop:Color} = 15066597
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?right_title_temp{prop:FontColor} = -1
    ?right_title_temp{prop:Color} = 15066597
    ?JOB:Account_Number:Prompt{prop:FontColor} = -1
    ?JOB:Account_Number:Prompt{prop:Color} = 15066597
    If ?job:Account_Number{prop:ReadOnly} = True
        ?job:Account_Number{prop:FontColor} = 65793
        ?job:Account_Number{prop:Color} = 15066597
    Elsif ?job:Account_Number{prop:Req} = True
        ?job:Account_Number{prop:FontColor} = 65793
        ?job:Account_Number{prop:Color} = 8454143
    Else ! If ?job:Account_Number{prop:Req} = True
        ?job:Account_Number{prop:FontColor} = 65793
        ?job:Account_Number{prop:Color} = 16777215
    End ! If ?job:Account_Number{prop:Req} = True
    ?job:Account_Number{prop:Trn} = 0
    ?job:Account_Number{prop:FontStyle} = font:Bold
    ?JOB:Order_Number:Prompt{prop:FontColor} = -1
    ?JOB:Order_Number:Prompt{prop:Color} = 15066597
    If ?job:Order_Number{prop:ReadOnly} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 15066597
    Elsif ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 8454143
    Else ! If ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 16777215
    End ! If ?job:Order_Number{prop:Req} = True
    ?job:Order_Number{prop:Trn} = 0
    ?job:Order_Number{prop:FontStyle} = font:Bold
    ?Customer_Name{prop:FontColor} = -1
    ?Customer_Name{prop:Color} = 15066597
    ?job:Title:Prompt{prop:FontColor} = -1
    ?job:Title:Prompt{prop:Color} = 15066597
    ?job:Initial:Prompt{prop:FontColor} = -1
    ?job:Initial:Prompt{prop:Color} = 15066597
    If ?job:Title{prop:ReadOnly} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 15066597
    Elsif ?job:Title{prop:Req} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 8454143
    Else ! If ?job:Title{prop:Req} = True
        ?job:Title{prop:FontColor} = 65793
        ?job:Title{prop:Color} = 16777215
    End ! If ?job:Title{prop:Req} = True
    ?job:Title{prop:Trn} = 0
    ?job:Title{prop:FontStyle} = font:Bold
    If ?job:Initial{prop:ReadOnly} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 15066597
    Elsif ?job:Initial{prop:Req} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 8454143
    Else ! If ?job:Initial{prop:Req} = True
        ?job:Initial{prop:FontColor} = 65793
        ?job:Initial{prop:Color} = 16777215
    End ! If ?job:Initial{prop:Req} = True
    ?job:Initial{prop:Trn} = 0
    ?job:Initial{prop:FontStyle} = font:Bold
    ?job:Surname:Prompt{prop:FontColor} = -1
    ?job:Surname:Prompt{prop:Color} = 15066597
    If ?job:Surname{prop:ReadOnly} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 15066597
    Elsif ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 8454143
    Else ! If ?job:Surname{prop:Req} = True
        ?job:Surname{prop:FontColor} = 65793
        ?job:Surname{prop:Color} = 16777215
    End ! If ?job:Surname{prop:Req} = True
    ?job:Surname{prop:Trn} = 0
    ?job:Surname{prop:FontStyle} = font:Bold
    ?job:Chargeable_Job{prop:Font,3} = -1
    ?job:Chargeable_Job{prop:Color} = 15066597
    ?job:Chargeable_Job{prop:Trn} = 0
    If ?job:Charge_Type{prop:ReadOnly} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 15066597
    Elsif ?job:Charge_Type{prop:Req} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 8454143
    Else ! If ?job:Charge_Type{prop:Req} = True
        ?job:Charge_Type{prop:FontColor} = 65793
        ?job:Charge_Type{prop:Color} = 16777215
    End ! If ?job:Charge_Type{prop:Req} = True
    ?job:Charge_Type{prop:Trn} = 0
    ?job:Charge_Type{prop:FontStyle} = font:Bold
    ?job:Warranty_Job{prop:Font,3} = -1
    ?job:Warranty_Job{prop:Color} = 15066597
    ?job:Warranty_Job{prop:Trn} = 0
    If ?job:Warranty_Charge_Type{prop:ReadOnly} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 15066597
    Elsif ?job:Warranty_Charge_Type{prop:Req} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 8454143
    Else ! If ?job:Warranty_Charge_Type{prop:Req} = True
        ?job:Warranty_Charge_Type{prop:FontColor} = 65793
        ?job:Warranty_Charge_Type{prop:Color} = 16777215
    End ! If ?job:Warranty_Charge_Type{prop:Req} = True
    ?job:Warranty_Charge_Type{prop:Trn} = 0
    ?job:Warranty_Charge_Type{prop:FontStyle} = font:Bold
    ?job:Intermittent_Fault{prop:Font,3} = -1
    ?job:Intermittent_Fault{prop:Color} = 15066597
    ?job:Intermittent_Fault{prop:Trn} = 0
    ?JOB:Incoming_Courier:Prompt{prop:FontColor} = -1
    ?JOB:Incoming_Courier:Prompt{prop:Color} = 15066597
    If ?job:Incoming_Courier{prop:ReadOnly} = True
        ?job:Incoming_Courier{prop:FontColor} = 65793
        ?job:Incoming_Courier{prop:Color} = 15066597
    Elsif ?job:Incoming_Courier{prop:Req} = True
        ?job:Incoming_Courier{prop:FontColor} = 65793
        ?job:Incoming_Courier{prop:Color} = 8454143
    Else ! If ?job:Incoming_Courier{prop:Req} = True
        ?job:Incoming_Courier{prop:FontColor} = 65793
        ?job:Incoming_Courier{prop:Color} = 16777215
    End ! If ?job:Incoming_Courier{prop:Req} = True
    ?job:Incoming_Courier{prop:Trn} = 0
    ?job:Incoming_Courier{prop:FontStyle} = font:Bold
    ?JOB:Incoming_Consignment_Number:Prompt{prop:FontColor} = -1
    ?JOB:Incoming_Consignment_Number:Prompt{prop:Color} = 15066597
    If ?job:Incoming_Consignment_Number{prop:ReadOnly} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 15066597
    Elsif ?job:Incoming_Consignment_Number{prop:Req} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 8454143
    Else ! If ?job:Incoming_Consignment_Number{prop:Req} = True
        ?job:Incoming_Consignment_Number{prop:FontColor} = 65793
        ?job:Incoming_Consignment_Number{prop:Color} = 16777215
    End ! If ?job:Incoming_Consignment_Number{prop:Req} = True
    ?job:Incoming_Consignment_Number{prop:Trn} = 0
    ?job:Incoming_Consignment_Number{prop:FontStyle} = font:Bold
    ?JOB:Incoming_Date:Prompt{prop:FontColor} = -1
    ?JOB:Incoming_Date:Prompt{prop:Color} = 15066597
    If ?job:Incoming_Date{prop:ReadOnly} = True
        ?job:Incoming_Date{prop:FontColor} = 65793
        ?job:Incoming_Date{prop:Color} = 15066597
    Elsif ?job:Incoming_Date{prop:Req} = True
        ?job:Incoming_Date{prop:FontColor} = 65793
        ?job:Incoming_Date{prop:Color} = 8454143
    Else ! If ?job:Incoming_Date{prop:Req} = True
        ?job:Incoming_Date{prop:FontColor} = 65793
        ?job:Incoming_Date{prop:Color} = 16777215
    End ! If ?job:Incoming_Date{prop:Req} = True
    ?job:Incoming_Date{prop:Trn} = 0
    ?job:Incoming_Date{prop:FontStyle} = font:Bold
    ?Panel2{prop:Fill} = 15066597

    ?Sheet2{prop:Color} = 15066597
    ?InvoiceAddress{prop:Color} = 15066597
    ?Invoice_Address{prop:FontColor} = -1
    ?Invoice_Address{prop:Color} = 15066597
    ?CompanyName:Prompt{prop:FontColor} = -1
    ?CompanyName:Prompt{prop:Color} = 15066597
    ?Address:prompt{prop:FontColor} = -1
    ?Address:prompt{prop:Color} = 15066597
    ?job:Company_Name{prop:FontColor} = -1
    ?job:Company_Name{prop:Color} = 15066597
    ?job:Address_Line1{prop:FontColor} = -1
    ?job:Address_Line1{prop:Color} = 15066597
    ?Postcode:Prompt{prop:FontColor} = -1
    ?Postcode:Prompt{prop:Color} = 15066597
    ?job:Address_Line2{prop:FontColor} = -1
    ?job:Address_Line2{prop:Color} = 15066597
    ?job:Address_Line3{prop:FontColor} = -1
    ?job:Address_Line3{prop:Color} = 15066597
    ?job:Postcode{prop:FontColor} = -1
    ?job:Postcode{prop:Color} = 15066597
    ?CollectionAddress{prop:Color} = 15066597
    ?Invoice_Address:2{prop:FontColor} = -1
    ?Invoice_Address:2{prop:Color} = 15066597
    ?CompanyName:Prompt:2{prop:FontColor} = -1
    ?CompanyName:Prompt:2{prop:Color} = 15066597
    ?job:Company_Name_Collection{prop:FontColor} = -1
    ?job:Company_Name_Collection{prop:Color} = 15066597
    ?Address:prompt:2{prop:FontColor} = -1
    ?Address:prompt:2{prop:Color} = 15066597
    ?job:Address_Line1_Collection{prop:FontColor} = -1
    ?job:Address_Line1_Collection{prop:Color} = 15066597
    ?job:Address_Line2_Collection{prop:FontColor} = -1
    ?job:Address_Line2_Collection{prop:Color} = 15066597
    ?job:Address_Line3_Collection{prop:FontColor} = -1
    ?job:Address_Line3_Collection{prop:Color} = 15066597
    ?Postcode:Prompt:2{prop:FontColor} = -1
    ?Postcode:Prompt:2{prop:Color} = 15066597
    ?job:Postcode_Collection{prop:FontColor} = -1
    ?job:Postcode_Collection{prop:Color} = 15066597
    ?DeliveryAddress{prop:Color} = 15066597
    ?Invoice_Address:3{prop:FontColor} = -1
    ?Invoice_Address:3{prop:Color} = 15066597
    ?CompanyName:Prompt:3{prop:FontColor} = -1
    ?CompanyName:Prompt:3{prop:Color} = 15066597
    ?job:Company_Name_Delivery{prop:FontColor} = -1
    ?job:Company_Name_Delivery{prop:Color} = 15066597
    ?Address:prompt:3{prop:FontColor} = -1
    ?Address:prompt:3{prop:Color} = 15066597
    ?job:Address_Line1_Delivery{prop:FontColor} = -1
    ?job:Address_Line1_Delivery{prop:Color} = 15066597
    ?job:Address_Line2_Delivery{prop:FontColor} = -1
    ?job:Address_Line2_Delivery{prop:Color} = 15066597
    ?job:Address_Line3_Delivery{prop:FontColor} = -1
    ?job:Address_Line3_Delivery{prop:Color} = 15066597
    ?Postcode:Prompt:3{prop:FontColor} = -1
    ?Postcode:Prompt:3{prop:Color} = 15066597
    ?job:Postcode_Delivery{prop:FontColor} = -1
    ?job:Postcode_Delivery{prop:Color} = 15066597
    ?Prompt35:5{prop:FontColor} = -1
    ?Prompt35:5{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597

    ?Prompt35{prop:FontColor} = -1
    ?Prompt35{prop:Color} = 15066597
    ?Prompt35:2{prop:FontColor} = -1
    ?Prompt35:2{prop:Color} = 15066597
    ?Prompt35:3{prop:FontColor} = -1
    ?Prompt35:3{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
CheckRequiredFields     Routine
    If ForceTransitType('B')
        ?job:Transit_Type{prop:color} = 0BDFFFFH
        ?job:Transit_Type{prop:Req} = 1
    Else !If ForceTransitType('B')
        ?job:Transit_Type{prop:color} = color:white
        ?job:Transit_Type{prop:Req} = 0
    End !If ForceTransitType('B')

    If ForceIMEI('B')
        ?job:ESN{prop:color} = 0BDFFFFH
        ?job:ESN{prop:Req} = 1
    Else !If ForceIMEI('B')
        ?job:ESN{prop:color} = color:white
        ?job:ESN{prop:Req} = 0
    End !If ForceIMEI('B')

    ! Start Change 2941 BE(06/11/03)
    !If ForceMSN(job:Manufacturer,'B')
    If ForceMSN(job:Manufacturer,job:Workshop,'B')
    ! Start Change 2941 BE(06/11/03)
        ?job:MSN{prop:color} = 0BDFFFFH
        ?job:MSN{prop:Req} = 1
    Else !If ForceMSN('B')
        ?job:MSN{prop:color} = color:white
        ?job:MSN{prop:Req} = 0
    End !If ForceMSN('B')

    If ForceModelNumber('B')
        ?job:Model_Number{prop:color} = 0BDFFFFH
        ?job:Model_Number{prop:Req} = 1
    Else !If ForceModelNumber('B')
        ?job:Model_Number{prop:color} = color:white
        ?job:Model_Number{prop:Req} = 0
    End !If ForceModelNumber('B')

    If ForceUnitType('B')
        ?job:Unit_Type{prop:color} = 0BDFFFFH
        ?job:Unit_Type{prop:Req} = 1
    Else !If ForceUnitType{'B')
        ?job:Unit_Type{prop:color} = color:white
        ?job:Unit_Type{prop:Req} = 0
    End !If ForceUnitType{'B')

    If ForceColour('B')
        ?job:Colour{prop:color} = 0BDFFFFH
        ?job:Colour{prop:Req} = 1
    Else !If ForceColour('B')
        ?job:Colour{prop:color} = color:white
        ?job:Colour{prop:Req} = 0
    End !If ForceColour('B')

    If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
        ?job:DOP{prop:color} = 0BDFFFFH
        ?job:DOP{prop:Req} = 1
    Else !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')
        ?job:DOP{prop:color} = color:white
        ?job:DOP{prop:Req} = 0
    End !If ForceDOP(job:Transit_Type,job:Manufacturer,job:Warranty_Job,'B')

    If ForceMobileNumber('B')
        ?job:Mobile_Number{prop:color} = 0BDFFFFH
        ?job:Mobile_Number{prop:Req} = 1
    Else !If ForceMobileNumber('B')
        ?job:Mobile_Number{prop:color} = color:white
        ?job:Mobile_Number{prop:Req} = 0
    End !If ForceMobileNumber('B')

    If ForceLocation(job:Transit_Type,job:Workshop,'B')
        ?job:Location{prop:color} = 0BDFFFFH
        ?job:Location{prop:Req} = 1
    Else !If ForceLocation(job:Transit_Type,job:Workshop,'B')
        ?job:Location{prop:color} = color:white
        ?job:Location{prop:Req} = 0
    End !If ForceLocation(job:Transit_Type,job:Workshop,'B')

    If ForceAuthorityNumber('B')
        ?job:Authority_Number{prop:color} = 0BDFFFFH
        ?job:Authority_Number{prop:Req} = 1
    Else !If ForceAuthorityNumber('B')
        ?job:Authority_Number{prop:color} = color:white
        ?job:Authority_Number{prop:Req} = 0
    End !If ForceAuthorityNumber('B')

    If ForceOrderNumber(job:Account_Number,'B')
        ?job:Order_Number{prop:color} = 0BDFFFFH
        ?job:Order_Number{prop:Req} = 1
    Else !If ForceOrderNumber(job:Account_Number,'B')
        ?job:Order_Number{prop:color} = color:white
        ?job:Order_Number{prop:Req} = 0
    End !If ForceOrderNumber(job:Account_Number,'B')

    If ForceCustomerName(job:Account_Number,'B')
        ?job:Initial{prop:color} = 0BDFFFFH
        ?job:Surname{prop:color} = 0BDFFFFH
        ?job:Title{prop:color} = 0BDFFFFH
        ?job:Surname{prop:Req} = 1
    Else !If ForceCustomerName(job:Account_Number,'B')
        ?job:Initial{prop:color} = color:white
        ?job:Surname{prop:color} = color:white
        ?job:Title{prop:color} = color:white
        ?job:Surname{prop:Req} = 0
    End !If ForceCustomerName(job:Account_Number,'B')

    If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = 0BDFFFFH
        ?job:incoming_consignment_number{prop:color} = 0BDFFFFH
        ?job:incoming_date{prop:color} = 0BDFFFFH
        ?job:Incoming_Courier{prop:Req} = 1
        ?job:Incoming_Consignment_Number{prop:Req} = 1
        ?job:Incoming_Date{prop:Req} = 1
    Else !If ForceIncomingCourier('B')
        ?job:Incoming_Courier{prop:color} = color:white
        ?job:incoming_consignment_number{prop:color} = color:white
        ?job:incoming_date{prop:color} = color:white
        ?job:Incoming_Courier{prop:Req} = 0
        ?job:Incoming_Consignment_Number{prop:Req} = 0
        ?job:Incoming_Date{prop:Req} = 0
    End !If ForceIncomingCourier('B')

    If ForceNetwork('B')
        ?tmp:Network{prop:color} = 0BDFFFFH
        ?tmp:Network{prop:Req} = 1
    Else !If ForceNetwork('B')
        ?tmp:Network{prop:color} = color:White
        ?tmp:Network{prop:Req} = 0
    End !If ForceNetwork('B')

Estimate_Check      Routine
        !Check to see if the selected charge type
        !requires an Estimate to be forced.
        access:chartype.clearkey(cha:warranty_key)
        cha:charge_type = job:charge_type
        cha:warranty    = 'NO'
        if access:chartype.fetch(cha:warranty_key) = Level:Benign
            If cha:Force_Estimate = 'YES'
                job:Estimate = 'YES'
            Else ! !If cha:Force_Estimate = 'YES'
                !If you assign a Charge Type that doesn't
                !have "Force Estimate", just that the Trade Account
                !doesn't have "Force Estimate" setup.
                !Because if it doesn't you can turn estimate off.
                Access:SUBTRACC.Clearkey(sub:Account_Number_Key)
                sub:Account_Number  = job:Account_Number
                If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Found
                    Access:TRADEACC.Clearkey(tra:Account_Number_Key)
                    tra:Account_Number  = sub:Main_Account_Number
                    If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Found
                        If tra:force_estimate <> 'YES'
                            job:Estimate = 'NO'
                        End !If tra:force_estimate <> 'YES'
                    Else! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                    End! If Access:TRADEACC.Tryfetch(tra:Account_Number_Key) = Level:Benign
                    
                Else! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                End! If Access:SUBTRACC.Tryfetch(sub:Account_Number_Key) = Level:Benign
                
            End ! !If cha:Force_Estimate = 'YES'
        End !if access:chartype.fetch(cha:charge_type_key) = Level:Benign
ShowTitle       Routine
    left_title_temp = 'Repair Details: - Job Number: ' & CLip(job:ref_number)
    right_title_temp = 'Date Booked: ' & Format(job:date_booked,@d6b) & ' ' & Format(job:time_booked,@t1) & |
                        ' - ' & job:who_booked
Compile('***',Debug=1)
    Message('job:who_booked: ' & job:who_booked,'Debug Message',icon:exclamation)
***
    Display()
check_force_fault_codes    Routine
    force_fault_codes_temp = 'NO'
    If job:chargeable_job = 'YES'
        required# = 0
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:chargeable = 'YES'
    If job:warranty_job = 'YES' And force_fault_codes_temp <> 'YES'
        access:chartype.clearkey(cha:charge_type_key)
        cha:charge_type = job:warranty_charge_type
        If access:chartype.fetch(cha:charge_type_key) = Level:Benign
            If cha:force_warranty = 'YES'
                force_fault_codes_temp = 'YES'
            End
        end !if
    End!If job:warranty_job = 'YES'
Bouncer_Text        Routine
    If job:bouncer = 'B'
        Unhide(?bouncer_text)
    Else!If job:bouncer = 'B'
        Hide(?bouncer_text)
    End!If job:bouncer = 'B'

    
Save_Fields     Routine
    esn_temp        = job:esn
    location_temp    = job:location
    account_number_temp = job:account_number
    transit_Type_temp = job:transit_type
Transit_Type_Bit        Routine
Time_Remaining          Routine      !Turnaround Time Nightmare
History_Check           Routine

!    found# = 0
!    setcursor(cursor:wait)
!    save_job_ali_id = access:jobs_alias.savefile()
!    access:jobs_alias.clearkey(job_ali:esn_key)
!    job_ali:esn = job:esn
!    set(job_ali:esn_key,job_ali:esn_key)
!    loop
!        if access:jobs_alias.next()
!           break
!        end !if
!        if job_ali:esn <> job:esn      |
!            then break.  ! end if
!        If job_ali:ref_number    <> job:ref_number
!            found# = 1
!            Break
!        End!If job_ali:ref_number    <> job:ref_number
!    end !loop
!    access:jobs_alias.restorefile(save_job_ali_id)
!    setcursor()
!    If found# = 1
!        Unhide(?repair_history)
!    Else!If found# = 1
!        Hide(?repair_history)
!    End!If found# = 1
Trade_Account_Status        Routine  ! Trade Account Status Display
!    Hide(?job:surname)
!    Hide(?job:title)
!    Hide(?job:title:prompt)
!    Hide(?job:title:prompt:2)
!    Hide(?surname:prompt)
!    access:subtracc.clearkey(sub:account_number_key)
!    sub:account_number = job:account_number
!    if access:subtracc.fetch(sub:account_number_key) = Level:Benign
!        access:tradeacc.clearkey(tra:account_number_key)
!        tra:account_number = sub:main_account_Number
!        if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!            If tra:use_contact_name = 'YES'
!                Unhide(?job:surname)
!                Unhide(?job:title)
!                Unhide(?job:title:prompt)
!                Unhide(?surname:prompt)
!                Unhide(?job:title:prompt:2)
!            End
!        end !if access:tradeacc.fetch(tra:account_number_key) = Level:Benign
!    end !if access:subtracc.fetch(sub:account_number_key) = Level:Benign
Trade_Account_Lookup_Bit        Routine
ESN_Model_Check     Routine
    Esn_Model_Routine(job:esn,job:model_number,model",pass")
    If pass" = 1
        job:model_number = model"
        access:modelnum.clearkey(mod:model_number_key)
        mod:model_number = job:model_number
        if access:modelnum.fetch(mod:model_number_key) = Level:Benign
            job:manufacturer = mod:manufacturer
            If mod:specify_unit_type = 'YES'
                job:unit_type = mod:unit_type
            End!If mod:specify_unit_type = 'YES'
            If mod:product_Type <> ''
                job:fault_code1 = mod:product_type
            End!If mod:product_Type <> ''
        end
        model_number_temp = job:model_number
        Do check_msn
    End!If fill_in# = 1
    Display()
Check_Msn       Routine   !Does Manufacturer Use MSN Number
    Access:manufact.Clearkey(man:manufacturer_key)
    man:manufacturer    = job:manufacturer
    If Access:manufact.Fetch(man:manufacturer_key) = Level:Benign
        ! Start Change 4239 BE(20/05/04)
        MSN_Field_Mask = man:MSNFieldMask
        ! End Change 4239 BE(20/05/04)

        If man:use_msn = 'YES'
            Unhide(?job:msn)
            Unhide(?job:msn:prompt)
            If thiswindow.request = Insertrecord
                Set(defaults)
                access:defaults.next()
                If DEF:Force_MSN = 'B'
                    ?job:msn{prop:req} = 1
                Else
                    ?job:msn{prop:req} = 0
                End!If DEF:Force_MSN = 'B'

            End!If thiswindow.request = Insertrecord
        Else
            Hide(?job:msn)
            Hide(?job:msn:prompt)
        End
    End !If Access:manufact.Fetch(man:manufacturer_key) = Level:Benign
Update_accessories      Routine
    count# = 0
    setcursor(cursor:wait)
    save_jac_id = access:jobacc.savefile()
    access:jobacc.clearkey(jac:ref_number_key)
    jac:ref_number = job:ref_number
    set(jac:ref_number_key,jac:ref_number_key)
    loop
        if access:jobacc.next()
           break
        end !if
        if jac:ref_number <> job:ref_number      |
            then break.  ! end if
        count# += 1
    end !loop
    access:jobacc.restorefile(save_jac_id)
    setcursor()

    If count# = 1
        access:jobacc.clearkey(jac:ref_number_key)
        jac:ref_number = job:ref_number
        Set(jac:ref_number_key,jac:ref_number_key)
        access:jobacc.next()
        Accessories_Temp = jac:accessory
    Else
        accessories_temp = Clip(count#)
    End
    Display()
Location_Bit     Routine
        job:location = loi:location
        If location_temp <> ''
    !Add To Old Location
            access:locinter.clearkey(loi:location_key)
            loi:location = location_temp
            If access:locinter.fetch(loi:location_key) = Level:Benign
                If loi:allocate_spaces = 'YES'
                    loi:current_spaces+= 1
                    loi:location_available = 'YES'
                    access:locinter.update()
                End
            end !if
        End!If location_temp <> ''
    !Take From New Location
        access:locinter.clearkey(loi:location_key)
        loi:location = job:location
        If access:locinter.fetch(loi:location_key) = Level:Benign
            If loi:allocate_spaces = 'YES'
                loi:current_spaces -= 1
                If loi:current_spaces< 1
                    loi:current_spaces = 0
                    loi:location_available = 'NO'
                End
                access:locinter.update()
            End
        end !if
        location_temp  = job:location

!        If job:date_completed = ''
!            If job:engineer <> ''
!                status_number# = 310 !allocated to engineer
!                status_audit# = thiswindow.request
!                include('status.inc')
!            Else!If job:engineer <> ''
!                status_number# = 305 !awaiting allocation
!                status_audit# = thiswindow.request
!                include('status.inc')
!            End!If job:engineer <> ''
!        End!If job:date_completed = ''
        Display()
        


Pricing     Routine
    If job:invoice_number <> ''
        job:labour_cost = job:invoice_labour_cost
        job:parts_cost  = job:invoice_parts_cost
    Else!End!If job:invoice_number <> ''
!       Pricing_Routine(labour",parts",labour_warranty",parts_warranty",pass",pass_warranty",discount",discount_warranty")
        Pricing_Routine('C',labour",parts",pass",a")
        If pass" = True
            job:labour_cost = labour"
            job:parts_cost  = parts"
        Else
            job:labour_cost = 0
            job:parts_cost  = 0
        End!If pass" = True
    End!If job:invoice_number <> ''
    IF job:invoice_number_warranty <> ''
        job:labour_cost_warranty = JOB:WInvoice_Labour_Cost
        job:parts_cost_warranty  = JOB:WInvoice_Parts_Cost
    Else!IF job:warranty_invoice_number
        Pricing_Routine('W',labour",parts",pass",a")
        If pass" = True
            job:labour_cost_warranty    = labour"
            job:parts_cost_warranty     = parts"
        Else
            job:labour_cost_warranty = 0
            job:parts_cost_warranty = 0
        End!If pass_warranty" = True
    End!IF job:warranty_invoice_number
    Display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Jobs',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Jobs',1)
    SolaceViewVars('MSN_Field_Mask',MSN_Field_Mask,'Update_Jobs',1)
    SolaceViewVars('ValidateMobileNumber',ValidateMobileNumber,'Update_Jobs',1)
    SolaceViewVars('temp_255string',temp_255string,'Update_Jobs',1)
    SolaceViewVars('save_job_id',save_job_id,'Update_Jobs',1)
    SolaceViewVars('save_moc_id',save_moc_id,'Update_Jobs',1)
    SolaceViewVars('force_fault_codes_temp',force_fault_codes_temp,'Update_Jobs',1)
    SolaceViewVars('tmp:printreceipt',tmp:printreceipt,'Update_Jobs',1)
    SolaceViewVars('tmp:retain',tmp:retain,'Update_Jobs',1)
    SolaceViewVars('save_xch_id',save_xch_id,'Update_Jobs',1)
    SolaceViewVars('sav:path',sav:path,'Update_Jobs',1)
    SolaceViewVars('save_cha_id',save_cha_id,'Update_Jobs',1)
    SolaceViewVars('save_maf_id',save_maf_id,'Update_Jobs',1)
    SolaceViewVars('Transit_Type_Temp',Transit_Type_Temp,'Update_Jobs',1)
    SolaceViewVars('print_label_temp',print_label_temp,'Update_Jobs',1)
    SolaceViewVars('label_type_temp',label_type_temp,'Update_Jobs',1)
    SolaceViewVars('print_job_card_temp',print_job_card_temp,'Update_Jobs',1)
    SolaceViewVars('sav:ref_number',sav:ref_number,'Update_Jobs',1)
    SolaceViewVars('sav:esn',sav:esn,'Update_Jobs',1)
    SolaceViewVars('save_jac_id',save_jac_id,'Update_Jobs',1)
    SolaceViewVars('location_temp',location_temp,'Update_Jobs',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'Update_Jobs',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'Update_Jobs',1)
    SolaceViewVars('Account_Number_Temp',Account_Number_Temp,'Update_Jobs',1)
    SolaceViewVars('save_job_ali_id',save_job_ali_id,'Update_Jobs',1)
    SolaceViewVars('model_number_temp',model_number_temp,'Update_Jobs',1)
    SolaceViewVars('ESN_temp',ESN_temp,'Update_Jobs',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Jobs',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Jobs',1)
    SolaceViewVars('left_title_temp',left_title_temp,'Update_Jobs',1)
    SolaceViewVars('right_title_temp',right_title_temp,'Update_Jobs',1)
    SolaceViewVars('accessories_temp',accessories_temp,'Update_Jobs',1)
    SolaceViewVars('save_taf_id',save_taf_id,'Update_Jobs',1)
    SolaceViewVars('ESN_Entry_Temp',ESN_Entry_Temp,'Update_Jobs',1)
    SolaceViewVars('tmp:SkillLevel',tmp:SkillLevel,'Update_Jobs',1)
    SolaceViewVars('tmp:Print_JobCard',tmp:Print_JobCard,'Update_Jobs',1)
    SolaceViewVars('tmp:Network',tmp:Network,'Update_Jobs',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?left_title_temp;  SolaceCtrlName = '?left_title_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Transit_Type:Prompt;  SolaceCtrlName = '?JOB:Transit_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Transit_Type;  SolaceCtrlName = '?job:Transit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Transit_Type;  SolaceCtrlName = '?Lookup_Transit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:ESN:Prompt;  SolaceCtrlName = '?JOB:ESN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:MSN:Prompt;  SolaceCtrlName = '?JOB:MSN:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:MSN;  SolaceCtrlName = '?job:MSN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ProductCode:Prompt;  SolaceCtrlName = '?job:ProductCode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ProductCode;  SolaceCtrlName = '?job:ProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupProductCode;  SolaceCtrlName = '?LookupProductCode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Model_Number:Prompt;  SolaceCtrlName = '?JOB:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Model_Number;  SolaceCtrlName = '?job:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Model_Number;  SolaceCtrlName = '?Lookup_Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Unit_Type:Prompt:2;  SolaceCtrlName = '?JOB:Unit_Type:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Unit_Type;  SolaceCtrlName = '?job:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Unit_Type;  SolaceCtrlName = '?Lookup_Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:SIMNumber:Prompt;  SolaceCtrlName = '?jobe:SIMNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?jobe:SIMNumber;  SolaceCtrlName = '?jobe:SIMNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Colour;  SolaceCtrlName = '?job:Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Colour;  SolaceCtrlName = '?Lookup_Colour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network:Prompt;  SolaceCtrlName = '?tmp:Network:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Network;  SolaceCtrlName = '?tmp:Network';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupNetwork;  SolaceCtrlName = '?LookupNetwork';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:DOP;  SolaceCtrlName = '?job:DOP';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SkillLevel:Prompt;  SolaceCtrlName = '?tmp:SkillLevel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SkillLevel;  SolaceCtrlName = '?tmp:SkillLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:DOP:Prompt;  SolaceCtrlName = '?JOB:DOP:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Mobile_Number:Prompt;  SolaceCtrlName = '?JOB:Mobile_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Mobile_Number;  SolaceCtrlName = '?job:Mobile_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Location:Prompt;  SolaceCtrlName = '?JOB:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Location;  SolaceCtrlName = '?job:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Location;  SolaceCtrlName = '?Lookup_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Authority_Number:Prompt;  SolaceCtrlName = '?JOB:Authority_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Authority_Number;  SolaceCtrlName = '?job:Authority_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:insurance_reference_number:prompt;  SolaceCtrlName = '?job:insurance_reference_number:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Insurance_Reference_Number;  SolaceCtrlName = '?job:Insurance_Reference_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?bouncer_text;  SolaceCtrlName = '?bouncer_text';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Print_JobCard;  SolaceCtrlName = '?tmp:Print_JobCard';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Colour:Prompt;  SolaceCtrlName = '?job:Colour:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?right_title_temp;  SolaceCtrlName = '?right_title_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Account_Number:Prompt;  SolaceCtrlName = '?JOB:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Account_Number;  SolaceCtrlName = '?job:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Account_Number;  SolaceCtrlName = '?Lookup_Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Order_Number:Prompt;  SolaceCtrlName = '?JOB:Order_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Order_Number;  SolaceCtrlName = '?job:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Customer_Name;  SolaceCtrlName = '?Customer_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Title:Prompt;  SolaceCtrlName = '?job:Title:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Initial:Prompt;  SolaceCtrlName = '?job:Initial:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Title;  SolaceCtrlName = '?job:Title';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Initial;  SolaceCtrlName = '?job:Initial';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Surname:Prompt;  SolaceCtrlName = '?job:Surname:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Surname;  SolaceCtrlName = '?job:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Chargeable_Job;  SolaceCtrlName = '?job:Chargeable_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?lookup_Charge_Type;  SolaceCtrlName = '?lookup_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Charge_Type;  SolaceCtrlName = '?job:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Job;  SolaceCtrlName = '?job:Warranty_Job';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Warranty_Charge_Type;  SolaceCtrlName = '?Lookup_Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Warranty_Charge_Type;  SolaceCtrlName = '?job:Warranty_Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Intermittent_Fault;  SolaceCtrlName = '?job:Intermittent_Fault';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Incoming_Courier:Prompt;  SolaceCtrlName = '?JOB:Incoming_Courier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Incoming_Courier;  SolaceCtrlName = '?job:Incoming_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Courier;  SolaceCtrlName = '?Lookup_Courier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Incoming_Consignment_Number:Prompt;  SolaceCtrlName = '?JOB:Incoming_Consignment_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Incoming_Consignment_Number;  SolaceCtrlName = '?job:Incoming_Consignment_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?JOB:Incoming_Date:Prompt;  SolaceCtrlName = '?JOB:Incoming_Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Incoming_Date;  SolaceCtrlName = '?job:Incoming_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?InvoiceAddress;  SolaceCtrlName = '?InvoiceAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoice_Address;  SolaceCtrlName = '?Invoice_Address';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompanyName:Prompt;  SolaceCtrlName = '?CompanyName:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend;  SolaceCtrlName = '?Amend';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address:prompt;  SolaceCtrlName = '?Address:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name;  SolaceCtrlName = '?job:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1;  SolaceCtrlName = '?job:Address_Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode:Prompt;  SolaceCtrlName = '?Postcode:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2;  SolaceCtrlName = '?job:Address_Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3;  SolaceCtrlName = '?job:Address_Line3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode;  SolaceCtrlName = '?job:Postcode';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CollectionAddress;  SolaceCtrlName = '?CollectionAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoice_Address:2;  SolaceCtrlName = '?Invoice_Address:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompanyName:Prompt:2;  SolaceCtrlName = '?CompanyName:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Collection;  SolaceCtrlName = '?job:Company_Name_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend:2;  SolaceCtrlName = '?Amend:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address:prompt:2;  SolaceCtrlName = '?Address:prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Collection;  SolaceCtrlName = '?job:Address_Line1_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Collection;  SolaceCtrlName = '?job:Address_Line2_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Collection;  SolaceCtrlName = '?job:Address_Line3_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode:Prompt:2;  SolaceCtrlName = '?Postcode:Prompt:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Collection;  SolaceCtrlName = '?job:Postcode_Collection';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DeliveryAddress;  SolaceCtrlName = '?DeliveryAddress';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Invoice_Address:3;  SolaceCtrlName = '?Invoice_Address:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CompanyName:Prompt:3;  SolaceCtrlName = '?CompanyName:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Company_Name_Delivery;  SolaceCtrlName = '?job:Company_Name_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Amend:3;  SolaceCtrlName = '?Amend:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Address:prompt:3;  SolaceCtrlName = '?Address:prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line1_Delivery;  SolaceCtrlName = '?job:Address_Line1_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line2_Delivery;  SolaceCtrlName = '?job:Address_Line2_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Address_Line3_Delivery;  SolaceCtrlName = '?job:Address_Line3_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Postcode:Prompt:3;  SolaceCtrlName = '?Postcode:Prompt:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Postcode_Delivery;  SolaceCtrlName = '?job:Postcode_Delivery';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt35:5;  SolaceCtrlName = '?Prompt35:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Accessory;  SolaceCtrlName = '?Lookup_Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt35;  SolaceCtrlName = '?Prompt35';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt35:2;  SolaceCtrlName = '?Prompt35:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt35:3;  SolaceCtrlName = '?Prompt35:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Codes_Lookup;  SolaceCtrlName = '?Fault_Codes_Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Description;  SolaceCtrlName = '?Fault_Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?engineers_notes;  SolaceCtrlName = '?engineers_notes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?contact_history;  SolaceCtrlName = '?contact_history';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Repair_History;  SolaceCtrlName = '?Repair_History';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Job'
  OF ChangeRecord
    ActionMessage = 'Changing A Job'
  END
  QuickWindow{Prop:Text} = ActionMessage
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  If insert_global    = 'YES'
      Globalrequest   = Insertrecord
  End!If insert_global    = 'YES'
  GlobalErrors.SetProcedureName('Update_Jobs')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Jobs')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?left_title_temp
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(job:Record,History::job:Record)
  SELF.AddHistoryField(?job:Transit_Type,30)
  SELF.AddHistoryField(?job:ESN,16)
  SELF.AddHistoryField(?job:MSN,17)
  SELF.AddHistoryField(?job:ProductCode,18)
  SELF.AddHistoryField(?job:Model_Number,14)
  SELF.AddHistoryField(?job:Unit_Type,19)
  SELF.AddHistoryField(?job:Colour,20)
  SELF.AddHistoryField(?job:DOP,27)
  SELF.AddHistoryField(?job:Mobile_Number,70)
  SELF.AddHistoryField(?job:Location,24)
  SELF.AddHistoryField(?job:Authority_Number,25)
  SELF.AddHistoryField(?job:Insurance_Reference_Number,26)
  SELF.AddHistoryField(?job:Account_Number,39)
  SELF.AddHistoryField(?job:Order_Number,42)
  SELF.AddHistoryField(?job:Title,60)
  SELF.AddHistoryField(?job:Initial,61)
  SELF.AddHistoryField(?job:Surname,62)
  SELF.AddHistoryField(?job:Chargeable_Job,13)
  SELF.AddHistoryField(?job:Charge_Type,36)
  SELF.AddHistoryField(?job:Warranty_Job,12)
  SELF.AddHistoryField(?job:Warranty_Charge_Type,37)
  SELF.AddHistoryField(?job:Intermittent_Fault,32)
  SELF.AddHistoryField(?job:Incoming_Courier,135)
  SELF.AddHistoryField(?job:Incoming_Consignment_Number,136)
  SELF.AddHistoryField(?job:Incoming_Date,137)
  SELF.AddHistoryField(?job:Company_Name,64)
  SELF.AddHistoryField(?job:Address_Line1,65)
  SELF.AddHistoryField(?job:Address_Line2,66)
  SELF.AddHistoryField(?job:Address_Line3,67)
  SELF.AddHistoryField(?job:Postcode,63)
  SELF.AddHistoryField(?job:Company_Name_Collection,72)
  SELF.AddHistoryField(?job:Address_Line1_Collection,73)
  SELF.AddHistoryField(?job:Address_Line2_Collection,74)
  SELF.AddHistoryField(?job:Address_Line3_Collection,75)
  SELF.AddHistoryField(?job:Postcode_Collection,71)
  SELF.AddHistoryField(?job:Company_Name_Delivery,79)
  SELF.AddHistoryField(?job:Address_Line1_Delivery,78)
  SELF.AddHistoryField(?job:Address_Line2_Delivery,80)
  SELF.AddHistoryField(?job:Address_Line3_Delivery,81)
  SELF.AddHistoryField(?job:Postcode_Delivery,77)
  SELF.AddUpdateFile(Access:JOBS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:AUDIT.Open
  Relate:COLOUR.Open
  Relate:COMMONCP.Open
  Relate:DEFAULTS.Open
  Relate:DEFRAPID.Open
  Relate:ESNMODEL.Open
  Relate:EXCHANGE.Open
  Relate:JOBS2_ALIAS.Open
  Relate:NETWORKS.Open
  Relate:STAHEAD.Open
  Relate:TRANTYPE.Open
  Relate:TURNARND.Open
  Relate:USELEVEL.Open
  Relate:USERS_ALIAS.Open
  Access:JOBSTAGE.UseFile
  Access:USERS.UseFile
  Access:MODELNUM.UseFile
  Access:JOBS_ALIAS.UseFile
  Access:MANUFACT.UseFile
  Access:STATUS.UseFile
  Access:COMMONWP.UseFile
  Access:COMMONFA.UseFile
  Access:MODELCOL.UseFile
  Access:MANFAULT.UseFile
  Access:SUBTRACC.UseFile
  Access:TRADEACC.UseFile
  Access:COURIER.UseFile
  Access:UNITTYPE.UseFile
  Access:LOCINTER.UseFile
  Access:JOBSE.UseFile
  Access:TRAFAULO.UseFile
  Access:TRAFAULT.UseFile
  Access:PRODCODE.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:JOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  quickwindow{prop:buffer} = 2
  !Show Fields
  Set(defaults)
  access:defaults.next()
  If def:show_mobile_number = 'YES'
      Hide(?job:mobile_number)
      Hide(?job:mobile_number:prompt)
  Else
      Unhide(?job:mobile_number)
      Unhide(?job:mobile_number:prompt)
  End
  If def:HideColour   = 'YES'
      Hide(?job:Colour)
      Hide(?lookup_colour)
      Hide(?job:colour:prompt)
  Else!If def:HideColour   = 'YES'
      UnHide(?job:Colour)
      UnHide(?lookup_colour)
      UnHide(?job:colour:prompt)
  End!If def:HideColour   = 'YES'
  
  If DEF:HideInCourier = 'YES'
      Hide(?job:incoming_courier:prompt)
      Hide(?job:incoming_courier)
      Hide(?lookup_Courier)
      Hide(?job:incoming_consignment_number)
      Hide(?job:incoming_consignment_number:prompt)
      Hide(?job:incoming_date)
      HIde(?job:incoming_date:prompt)
  Else!If DEF:HideInCourier = 'YES'
      UnHide(?job:incoming_courier:prompt)
      UnHide(?job:incoming_courier)
      Unhide(?lookup_courier)
      UnHide(?job:incoming_consignment_number)
      UnHide(?job:incoming_consignment_number:prompt)
      UnHide(?job:incoming_date)
      UnHIde(?job:incoming_date:prompt)
  
  End!If DEF:HideInCourier = 'YES'
  If def:hideproduct = 'YES'
      Hide(?job:productcode)
      Hide(?job:productcode:prompt)
      Hide(?LookupProductCode)
  End!If def:hideproduct = 'YES'
  
  If DEF:HideIntFault = 'YES'
      Hide(?job:intermittent_fault)
  Else!If DEF:HideIntFault = 'YES'
      UnHide(?job:intermittent_fault)
  End!If DEF:HideIntFault = 'YES'
  
  If def:hide_authority_number = 'YES'
      Hide(?Job:authority_number)
      Hide(?job:authority_number:prompt)
  Else
      UnHide(?Job:authority_number)
      UnHide(?job:authority_number:prompt)
  End!If def:hide_authority_number = 'YES'
  If def:HideLocation
      ?job:Location{prop:hide} = 1
      ?job:Location:Prompt{prop:hide} = 1
      ?lookup_location{prop:Hide} = 1
  End !def:HideLocation
  
  ! Start Change 4078 BE(22/07/2004)
  IF (def:hide_insurance = 'YES') THEN
      HIDE(?Job:Insurance_Reference_Number)
      HIDE(?job:insurance_reference_number:prompt)
  Else
      UNHIDE(?Job:Insurance_Reference_Number)
      UNHIDE(?job:insurance_reference_number:prompt)
  END
  ! Start Change 4078 BE(22/07/2004)
  
  Do check_Msn
  Do trade_account_status
  
  If GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?job:Colour:Prompt{prop:Text} = Clip(GETINI('RENAME','ColourName',,CLIP(PATH())&'\SB2KDEF.INI'))
  End !GETINI('RENAME','RenameColour',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  If GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:SkillLevel{prop:Hide} = 1
      ?tmp:SkillLevel:prompt{prop:Hide} = 1
  Else
      ?tmp:SkillLevel{prop:Hide} = 0
      ?tmp:SkillLevel:prompt{prop:Hide} = 0
  
  End !GETINI('HIDE','SkillLevel',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  
  If GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
      ?tmp:Network{prop:Hide} = 1
      ?tmp:Network:Prompt{prop:Hide} = 1
      ?LookupNetwork{prop:Hide} = 1
  Else
      ?tmp:Network{prop:Hide} = 0
      ?tmp:Network:Prompt{prop:Hide} = 0
      ?LookupNetwork{prop:Hide} = 0
  
  End !GETINI('HIDE','HideNetwork',,CLIP(PATH())&'\SB2KDEF.INI') = 1
  IF INSTRING('SIGMA',def:User_Name)
    ?tmp:Network:Prompt{PROP:Text} = 'Factory Code'
  END
  
  ! Start Change 2821 BE(24/06/03)
  ValidateMobileNumber = GETINI('VALIDATE','MobileNumber',,CLIP(PATH())&'\SB2KDEF.INI')
  ! End Change 2821 BE(24/06/03)
  !Which Address To Show
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Case def:Browse_Option
      Of 'INV'
          Select(?Sheet2,1)
      Of 'COL'
          Select(?Sheet2,2)
      Of 'DEL'
          Select(?Sheet2,3)
  End!Case def:Browse_Option
  Post(event:accepted,?job:chargeable_job)
  Post(event:accepted,?job:warranty_job)
  Do History_Check
  Do transit_type_bit
  Do bouncer_text
  !Check Compulsory Fields
  Set(defaults)
  access:defaults.next()
  Do CheckRequiredFields
  
  
  
  
  IF Records(trantype) = 1
    SET(trantype,0)
    IF Access:Trantype.Next()
      !Error!
    ELSE
      job:Transit_Type = trt:Transit_Type
      UPDATE()
      DISPLAY()
      POST(Event:Accepted,?job:Transit_Type)
      ?job:Transit_type{PROP:Skip} = TRUE
    END
  END
  tmp:Print_Jobcard = FALSE
  Do RecolourWindow
  IF ?job:Transit_Type{Prop:Tip} AND ~?Lookup_Transit_Type{Prop:Tip}
     ?Lookup_Transit_Type{Prop:Tip} = 'Select ' & ?job:Transit_Type{Prop:Tip}
  END
  IF ?job:Transit_Type{Prop:Msg} AND ~?Lookup_Transit_Type{Prop:Msg}
     ?Lookup_Transit_Type{Prop:Msg} = 'Select ' & ?job:Transit_Type{Prop:Msg}
  END
  IF ?job:Model_Number{Prop:Tip} AND ~?Lookup_Model_Number{Prop:Tip}
     ?Lookup_Model_Number{Prop:Tip} = 'Select ' & ?job:Model_Number{Prop:Tip}
  END
  IF ?job:Model_Number{Prop:Msg} AND ~?Lookup_Model_Number{Prop:Msg}
     ?Lookup_Model_Number{Prop:Msg} = 'Select ' & ?job:Model_Number{Prop:Msg}
  END
  IF ?job:Unit_Type{Prop:Tip} AND ~?Lookup_Unit_Type{Prop:Tip}
     ?Lookup_Unit_Type{Prop:Tip} = 'Select ' & ?job:Unit_Type{Prop:Tip}
  END
  IF ?job:Unit_Type{Prop:Msg} AND ~?Lookup_Unit_Type{Prop:Msg}
     ?Lookup_Unit_Type{Prop:Msg} = 'Select ' & ?job:Unit_Type{Prop:Msg}
  END
  IF ?job:Colour{Prop:Tip} AND ~?Lookup_Colour{Prop:Tip}
     ?Lookup_Colour{Prop:Tip} = 'Select ' & ?job:Colour{Prop:Tip}
  END
  IF ?job:Colour{Prop:Msg} AND ~?Lookup_Colour{Prop:Msg}
     ?Lookup_Colour{Prop:Msg} = 'Select ' & ?job:Colour{Prop:Msg}
  END
  IF ?job:Location{Prop:Tip} AND ~?Lookup_Location{Prop:Tip}
     ?Lookup_Location{Prop:Tip} = 'Select ' & ?job:Location{Prop:Tip}
  END
  IF ?job:Location{Prop:Msg} AND ~?Lookup_Location{Prop:Msg}
     ?Lookup_Location{Prop:Msg} = 'Select ' & ?job:Location{Prop:Msg}
  END
  IF ?job:Account_Number{Prop:Tip} AND ~?Lookup_Account_Number{Prop:Tip}
     ?Lookup_Account_Number{Prop:Tip} = 'Select ' & ?job:Account_Number{Prop:Tip}
  END
  IF ?job:Account_Number{Prop:Msg} AND ~?Lookup_Account_Number{Prop:Msg}
     ?Lookup_Account_Number{Prop:Msg} = 'Select ' & ?job:Account_Number{Prop:Msg}
  END
  IF ?job:Incoming_Courier{Prop:Tip} AND ~?Lookup_Courier{Prop:Tip}
     ?Lookup_Courier{Prop:Tip} = 'Select ' & ?job:Incoming_Courier{Prop:Tip}
  END
  IF ?job:Incoming_Courier{Prop:Msg} AND ~?Lookup_Courier{Prop:Msg}
     ?Lookup_Courier{Prop:Msg} = 'Select ' & ?job:Incoming_Courier{Prop:Msg}
  END
  IF ?job:ProductCode{Prop:Tip} AND ~?LookupProductCode{Prop:Tip}
     ?LookupProductCode{Prop:Tip} = 'Select ' & ?job:ProductCode{Prop:Tip}
  END
  IF ?job:ProductCode{Prop:Msg} AND ~?LookupProductCode{Prop:Msg}
     ?LookupProductCode{Prop:Msg} = 'Select ' & ?job:ProductCode{Prop:Msg}
  END
  IF ?tmp:Network{Prop:Tip} AND ~?LookupNetwork{Prop:Tip}
     ?LookupNetwork{Prop:Tip} = 'Select ' & ?tmp:Network{Prop:Tip}
  END
  IF ?tmp:Network{Prop:Msg} AND ~?LookupNetwork{Prop:Msg}
     ?LookupNetwork{Prop:Msg} = 'Select ' & ?tmp:Network{Prop:Msg}
  END
  IF SELF.Request = ViewRecord
    ?job:Transit_Type{PROP:ReadOnly} = True
    DISABLE(?Lookup_Transit_Type)
    ?job:ESN{PROP:ReadOnly} = True
    ?job:MSN{PROP:ReadOnly} = True
    DISABLE(?job:ProductCode)
    DISABLE(?LookupProductCode)
    ?job:Model_Number{PROP:ReadOnly} = True
    DISABLE(?Lookup_Model_Number)
    ?job:Unit_Type{PROP:ReadOnly} = True
    DISABLE(?Lookup_Unit_Type)
    ?jobe:SIMNumber{PROP:ReadOnly} = True
    ?job:Colour{PROP:ReadOnly} = True
    DISABLE(?Lookup_Colour)
    ?tmp:Network{PROP:ReadOnly} = True
    DISABLE(?LookupNetwork)
    ?job:DOP{PROP:ReadOnly} = True
    ?job:Mobile_Number{PROP:ReadOnly} = True
    ?job:Location{PROP:ReadOnly} = True
    DISABLE(?Lookup_Location)
    ?job:Authority_Number{PROP:ReadOnly} = True
    ?job:Insurance_Reference_Number{PROP:ReadOnly} = True
    ?job:Account_Number{PROP:ReadOnly} = True
    DISABLE(?Lookup_Account_Number)
    ?job:Order_Number{PROP:ReadOnly} = True
    ?job:Title{PROP:ReadOnly} = True
    ?job:Initial{PROP:ReadOnly} = True
    ?job:Surname{PROP:ReadOnly} = True
    DISABLE(?lookup_Charge_Type)
    ?job:Charge_Type{PROP:ReadOnly} = True
    DISABLE(?Lookup_Warranty_Charge_Type)
    ?job:Warranty_Charge_Type{PROP:ReadOnly} = True
    ?job:Incoming_Courier{PROP:ReadOnly} = True
    DISABLE(?Lookup_Courier)
    ?job:Incoming_Consignment_Number{PROP:ReadOnly} = True
    ?job:Incoming_Date{PROP:ReadOnly} = True
    DISABLE(?Amend)
    DISABLE(?Amend:2)
    DISABLE(?Amend:3)
    DISABLE(?Lookup_Accessory)
    DISABLE(?Fault_Codes_Lookup)
    DISABLE(?Fault_Description)
    DISABLE(?engineers_notes)
    DISABLE(?contact_history)
    DISABLE(?Repair_History)
    HIDE(?OK)
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  IF ?job:Chargeable_Job{Prop:Checked} = True
    ENABLE(?lookup_Charge_Type)
    ENABLE(?JOB:Charge_Type)
  END
  IF ?job:Chargeable_Job{Prop:Checked} = False
    job:Charge_Type = ''
    DISABLE(?lookup_Charge_Type)
    DISABLE(?JOB:Charge_Type)
  END
  IF ?job:Warranty_Job{Prop:Checked} = True
    ENABLE(?Lookup_Warranty_Charge_Type)
    ENABLE(?JOB:Warranty_Charge_Type)
  END
  IF ?job:Warranty_Job{Prop:Checked} = False
    job:Warranty_Charge_Type = ''
    DISABLE(?Lookup_Warranty_Charge_Type)
    DISABLE(?JOB:Warranty_Charge_Type)
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  insert_Global   = ''
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:AUDIT.Close
    Relate:COLOUR.Close
    Relate:COMMONCP.Close
    Relate:DEFAULTS.Close
    Relate:DEFRAPID.Close
    Relate:ESNMODEL.Close
    Relate:EXCHANGE.Close
    Relate:JOBS2_ALIAS.Close
    Relate:NETWORKS.Close
    Relate:STAHEAD.Close
    Relate:TRANTYPE.Close
    Relate:TURNARND.Close
    Relate:USELEVEL.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Jobs',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(PrimeFields, ())
    job:EDI = 'XXX'
    job:Loan_Status = '101 NOT ISSUED'
    job:Exchange_Status = '101 NOT ISSUED'
    job:Third_Party_Printed = 'NO'
    job:Completed = 'NO'
    job:Despatched = 'NO'
    job:Incoming_Date = Today()
  PARENT.PrimeFields
  If insert_global    = 'YES'
      access:jobs.primerecord()
  End!If insert_global    = 'YES'
  access:users.clearkey(use:password_key)
  use:password    = glo:password
  access:users.fetch(use:password_key)
  job:who_booked  = use:user_code
  get(jobstage,0)
  if access:jobstage.primerecord() = level:benign
  
      jst:ref_number = job:ref_number
      jst:job_stage  = '000 NEW JOB'
      jst:date       = Today()
      jst:time       = Clock()
      access:users.clearkey(use:password_key)
      use:password =glo:password
      access:users.fetch(use:password_key)
      jst:user = use:user_code
      if access:jobstage.tryinsert()
          access:jobstage.cancelautoinc()
      end
  
  end!if access:jobstage.primerecord() = level:benign
  
  GetStatus(0,ThisWindow.Request,'JOB')
  
  job:incoming_date   = Today()
  tmp:SkillLevel = 0
  
  !Save Fields
  Do save_Fields
  Do ShowTitle
  ?job:dop{prop:req} = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(PrimeFields, ())


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickTransitTypes
      BrowseProductCodes
      BrowseMODELNUM
      BrowseUNITTYPE
      Pick_Model_Colour
      PickNetworks
      Browse_Available_Locations
      PickSubAccounts
      PickCouriers
    END
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?lookup_Charge_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_Charge_Type, Accepted)
      glo:select1 = 'NO'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_Charge_Type, Accepted)
    OF ?Lookup_Warranty_Charge_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
      glo:select1 = 'YES'
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
    OF ?Fault_Description
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Description, Accepted)
      access:jobnotes.clearkey(jbn:RefNumberKey)
      jbn:RefNumber   = job:Ref_Number
      If access:jobnotes.tryfetch(jbn:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:RefNumber   = job:ref_number
              access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Description, Accepted)
    OF ?engineers_notes
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?engineers_notes, Accepted)
      access:jobnotes.clearkey(jbn:RefNumberKey)
      jbn:RefNumber   = job:Ref_Number
      If access:jobnotes.tryfetch(jbn:RefNumberKey)
          If access:jobnotes.primerecord() = Level:Benign
              jbn:RefNumber   = job:ref_number
              access:jobnotes.tryinsert()
          End!If access:jobnotes.primerecord() = Level:Benign
      End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?engineers_notes, Accepted)
    OF ?contact_history
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?contact_history, Accepted)
      glo:select12 = job:ref_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?contact_history, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?job:Transit_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Transit_Type, Accepted)
      If ~0{prop:acceptall}
      IF job:Transit_Type OR ?job:Transit_Type{Prop:Req}
        trt:Transit_Type = job:Transit_Type
        !Save Lookup Field Incase Of error
        look:job:Transit_Type        = job:Transit_Type
        IF Access:TRANTYPE.TryFetch(trt:Transit_Type_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            job:Transit_Type = trt:Transit_Type
          ELSE
            !Restore Lookup On Error
            job:Transit_Type = look:job:Transit_Type
            SELECT(?job:Transit_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      Set(defaults)
      access:defaults.next()
      access:trantype.clearkey(trt:transit_type_key)
      trt:transit_type = job:transit_type
      if access:trantype.fetch(trt:transit_type_key) = Level:Benign
          hide# = 0
          If trt:collection_address <> 'YES'
              JOB:Postcode_Collection=''
              JOB:Company_Name_Collection=''
              JOB:Address_Line1_Collection=''
              JOB:Address_Line2_Collection=''
              JOB:Address_Line3_Collection=''
              JOB:Telephone_Collection=''
          End
          hide# = 0
          If trt:delivery_address <> 'YES'
              JOB:Postcode_Delivery=''
              JOB:Address_Line1_Delivery=''
              JOB:Company_Name_Delivery=''
              JOB:Address_Line2_Delivery=''
              JOB:Address_Line3_Delivery=''
              JOB:Telephone_Delivery=''
          End!If trt:delivery_address <> 'YES'
          If trt:force_dop = 'YES' and job:warranty_job = 'YES'
              ?job:dop{prop:req} = 1
          Else!If trt:force_dop = 'YES'
              ?job:dop{prop:req} = 0
          End!If trt:force_dop = 'YES'
      
          If trt:exchange_unit = 'YES'
              tmp:retain = 1
          Else
              tmp:retain = 0
          End!If trt:exchange_unit = 'YES'
          If trt:InternalLocation <> ''
              job:Location    = trt:InternalLocation
          End !If trt:InternalLocation <> ''
          If trt:InWorkshop = 'YES'
              job:workshop = 'YES'
          Else!If trt:InWorkshop = 'YES'
              job:workshop = 'NO'
          End!If trt:InWorkshop = 'YES'
          If trt:location <> 'YES'
              Hide(?job:location)
              Hide(?job:location:prompt)
              Hide(?lookup_location)
              ?job:location{prop:req} = 0
         Else
              Unhide(?job:location)
              unhide(?job:location:prompt)
              Unhide(?lookup_location)
              If trt:force_location = 'YES' and job:workshop = 'YES'
                  ?job:location{prop:req} = 1
              Else
                  ?job:location{prop:req} = 0
              End!If trt:forcelocation = 'YES' and job:workshop = 'YES'
          End
          If ThisWindow.Request = Insertrecord
              GetStatus(Sub(trt:initial_status,1,3),1,'JOB')
      
              GetStatus(Sub(TRT:ExchangeStatus,1,3),1,'EXC')
      
              GetStatus(Sub(trt:loanStatus,1,3),1,'LOA')
      
              Do time_remaining
      
              If trt:workshop_label = 'YES'
                  print_label_temp = 'YES'
              Else
                  print_label_temp = 'NO'
              End
              If trt:job_card = 'YES'
                  print_job_card_temp = 'YES'
                  ?tmp:Print_JobCard{prop:Hide} = 0
                  tmp:Print_JobCard = 0
              Else!If trt:job_card = 'YES'
                  print_job_card_temp = 'NO'
                  ?tmp:Print_JobCard{prop:Hide} = 1
              End!If trt:job_card = 'YES'
              If trt:jobreceipt = 'YES'
                  tmp:printreceipt = 1
              Else!If trt:jobreceipt = 'YES'
                  tmp:printreceipt = 0
              End!If trt:jobreceipt = 'YES'
              error# = 0
              If error# = 0
                  job:turnaround_time = trt:initial_priority
                  access:turnarnd.clearkey(tur:turnaround_time_key)
                  tur:turnaround_time = job:turnaround_time
                  if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
                      Turnaround_Routine(tur:days,tur:hours,end_date",end_time")
                      job:turnaround_end_date = Clip(end_date")
                      job:turnaround_end_time = Clip(end_time")
                      Do Time_Remaining
                  end!if access:turnarnd.fetch(tur:turnaround_time_key) = Level:Benign
              End!If error# = 0
          End
      end!if access:trantype.fetch(trt:transit_type_key) = Level:Benign
      End !If ~0{prop:acceptall}
      
      Do CheckRequiredFields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Transit_Type, Accepted)
    OF ?Lookup_Transit_Type
      ThisWindow.Update
      trt:Transit_Type = job:Transit_Type
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          job:Transit_Type = trt:Transit_Type
          Select(?+1)
      ELSE
          Select(?job:Transit_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Transit_Type)
    OF ?job:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
      
       temp_string = Clip(left(job:ESN))
       string_length# = Len(temp_string)
       string_position# = 1
      
       temp_string = Upper(Sub(temp_string,string_position#,1)) & Sub(temp_string,string_position# + 1,String_length# - 1)
      
       Loop string_position# = 1 To string_length#
       If sub(temp_string,string_position#,1) = ' '
       temp_string = sub(temp_string,1,string_position# - 1) & Sub(temp_string,string_position# + 1,String_length# - 1)
       End
       End
      
       job:ESN = temp_string
       Display(?job:ESN)
      If ~0{prop:acceptall}
          Set(DEFAULTS)
          Access:DEFAULTS.Next()
      
          !Added by Neil - Code to strip 18 char IMEI's and 15 char MSN's
          If Len(Clip(job:ESN)) = 18
              job:ESN = Sub(job:ESN,4,15)
              Display(?job:ESN)
          End !If Len(Clip(job:ESN)) = 18
      
          job:Model_Number = IMEIModelRoutine(job:ESN,job:Model_Number)
      
          !Check for Bouncers
          Error# = 0
          If job:ESN <> 'N/A' And job:ESN <> ''
              Bouncers# = 0
              Bouncers# = CountBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
              If Bouncers#
                  If def:Allow_Bouncer <> 'YES'
                      Case MessageEx('The selected I.M.E.I. has been entered previously within the default period. '&|
                        '<13,10>'&|
                        '<13,10>Please select another I.M.E.I. Number.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      job:esn = ''
                      error# = 1
                      hide(?bouncer_text)
                  Else !If def:Allow_Bouncer <> 'YES'
                      If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
                          If LiveBouncers(job:Ref_Number,job:Date_Booked,job:ESN)
                              ! Start Change 3615 BE(26/11/03)
                              SETCURSOR()
                              ! End Change 3615 BE(26/11/03)
                              Case MessageEx('The selected I.M.E.I. is already entered onto a live job. You must complete this previous job before you can continue booking.','ServiceBase 2000',|
                                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                  Of 1 ! &OK Button
                              End!Case MessageEx
                              ! Start Change 3615 BE(26/11/03)
                              job:esn = ''
                              ! End Change 3615 BE(26/11/03)
                              Error# = 1
                          ! Start Change 2760 BE(23/06/03)
                          ELSE
                              MessageEx('This I.M.E.I. Number has been entered onto the system before.<13,10><13,10>Select the ''Repair History'' button to view it''s History.','ServiceBase 2000','Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
                              job:bouncer = 'B'
                              Unhide(?repair_history)
                              Unhide(?bouncer_text)
                              ?bouncer_text{prop:text} = Clip(Bouncers#) & ' BOUNCER(S)'
                          ! End Change 2760 BE(23/06/03)
                          End !If LiveBouncer(job:Ref_Number,job:Date_Booked,job:ESN)
                      Else
                          Case MessageEx('This I.M.E.I. Number has been entered onto the system before.<13,10><13,10>Select the ''Repair History'' button to view it''s History.','ServiceBase 2000','Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0)
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          job:bouncer = 'B'
                          Unhide(?repair_history)
                          Unhide(?bouncer_text)
                          ?bouncer_text{prop:text} = Clip(Bouncers#) & ' BOUNCER(S)'
                      End !If Clip(GETINI('BOUNCER','StopLive',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      
                  End !If def:Allow_Bouncer <> 'YES'
              Else !If Bouncers#
                  job:bouncer = ''
                  Hide(?repair_history)
              End !If Bouncers#
          End !If job:ESN <> 'N/A' And job:ESN <> ''
          If Error# = 0
              Access:MODELNUM.Clearkey(mod:Model_Number_Key)
              mod:Model_Number    = job:Model_Number
              If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                  !Found
                  job:Manufacturer    = mod:Manufacturer
                  If mod:Specify_Unit_Type = 'YES'
                      job:Unit_Type = mod:Unit_Type
                  End !If mod:Specify_Unit_Type = 'YES'
                  Model_Number_Temp   = job:Model_Number
                  Post(Event:Accepted,?job:Model_Number)
      
                  Do Check_MSN
                  If ?job:MSN{prop:Hide} = 0
                      Select(?job:MSN)
                  Else !If ?job:MSN{prop:Hide} = 0
                      If job:Colour <> ''
                          Select(?job:Colour)
                      Else !If job:Colour <> ''
                          Select(?+1)
                      End !If job:Colour <> ''
                  End !If ?job:MSN{prop:Hide} = 0
              Else! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
                  !Error
                  !Assert(0,'<13,10>Fetch Error<13,10>')
              End! If Access:MODELNUM.Tryfetch(mod:Model_Number_Key) = Level:Benign
              
          End !If Error# = 0
      
          !Check to see if the IMEI matches the account number, if there is one.
          If Error# = 0 And job:Account_Number <> ''
              If ExchangeAccount(job:Account_Number)
                  Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                  xch:ESN = job:ESN
                  If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                      !Found
                      !Write the relevant fields to say this unit is in repair and take completed.
                      !Otherwise the use could click cancel and screw everything up.
                  Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      Case MessageEx('The selected Trade Account is setup to repair Exchange Units, but the entered I.M.E.I. Number cannot be found in Exchange Stock.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      Error# = 1
                  End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
              Else !ExchangeAccount(job:Account_Number)
                  Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
                  xch:ESN = job:ESN
                  If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                      !Found
                      If xch:Available <> 'DES'
                          Case MessageEx('The entered I.M.E.I. matches that of an entry in the Exchange Database. This unit is not marked as "Despatched" so may still exist in your stock.'&|
                            '<13,10>'&|
                            '<13,10>If you continue, this job will be treated as a normal customer repair and not a "In House Exchange Repair". These "In House" repairs should be booked in using an Account that is marked as "Exchange Account".'&|
                            '<13,10>'&|
                            '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                              Of 1 ! &Yes Button
                              Of 2 ! &No Button
                                  Error# = 1
                          End!Case MessageEx
                      End!Case MessageEx
      
                  Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
              End !ExchangeAccount(job:Account_Number)
          End !Error# = 0
      
          Display()
          Do History_Check
          Do check_msn
      
          Do bouncer_text
          Do check_msn
      
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Accepted)
    OF ?job:MSN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:MSN, Accepted)
      If ~0{prop:acceptall}
          
          IF LEN(CLIP(job:MSN)) = 11 AND job:Manufacturer = 'ERICSSON'
            !Ericsson MSN!
            Job:MSN = SUB(job:MSN,2,10)
            UPDATE()
          END
          !End!
      
          ! Start Change 4239 BE(20/05/04)
      !    If CheckLength('MSN',job:model_number,job:msn)
      !        ! Start 2651 BE(02/06/03)
      !        job:msn = ''
      !        ! End 2651 BE(02/06/03)
      !        Select(?job:msn)
      !    Else!If error# = 1
      !        If job:model_number <> '' And thiswindow.request = Insertrecord
      !            If job:colour <> ''
      !                Select(?job:dop)
      !            Else!If xch:colour <> ''
      !                Select(?job:colour)
      !            End!If xch:colour <> ''
      !        End!If xch:model_number <
      !    End!If error# = 1
      
          !IF (MSN_Field_Mask <> '') THEN
          IF (job:warranty_job = 'YES' AND MSN_Field_Mask <> '') THEN
              IF ((job:MSN <> '') AND (CheckFaultFormat(job:MSN, MSN_Field_Mask))) THEN
                  BEEP()
                  MessageEx('MSN Failed Format Validation',|
                            'ServiceBase 2000','Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,|
                            CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
                  job:MSN = ''
                  SELECT(?job:MSN)
              END
          ELSIF CheckLength('MSN',job:model_number,job:msn)
              ! Start 2651 BE(02/06/03)
              job:msn = ''
              ! End 2651 BE(02/06/03)
              Select(?job:msn)
          ELSE
              If job:model_number <> '' And thiswindow.request = Insertrecord
                  If job:colour <> ''
                      Select(?job:dop)
                  Else!If xch:colour <> ''
                      Select(?job:colour)
                  End!If xch:colour <> ''
              End!If xch:model_number <
          END
          ! End Change 4239 BE(20/05/04)
      
      
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:MSN, Accepted)
    OF ?job:ProductCode
      IF job:ProductCode OR ?job:ProductCode{Prop:Req}
        prd:ProductCode = job:ProductCode
        !Save Lookup Field Incase Of error
        look:job:ProductCode        = job:ProductCode
        IF Access:PRODCODE.TryFetch(prd:ProductCodeKey)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            job:ProductCode = prd:ProductCode
          ELSE
            !Restore Lookup On Error
            job:ProductCode = look:job:ProductCode
            SELECT(?job:ProductCode)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupProductCode
      ThisWindow.Update
      prd:ProductCode = job:ProductCode
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          job:ProductCode = prd:ProductCode
          Select(?+1)
      ELSE
          Select(?job:ProductCode)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:ProductCode)
    OF ?job:Model_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Model_Number, Accepted)
      If ~0{prop:acceptall}
      IF job:Model_Number OR ?job:Model_Number{Prop:Req}
        mod:Model_Number = job:Model_Number
        !Save Lookup Field Incase Of error
        look:job:Model_Number        = job:Model_Number
        IF Access:MODELNUM.TryFetch(mod:Model_Number_Key)
          IF SELF.Run(3,SelectRecord) = RequestCompleted
            job:Model_Number = mod:Model_Number
          ELSE
            !Restore Lookup On Error
            job:Model_Number = look:job:Model_Number
            SELECT(?job:Model_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = job:model_number
      If access:modelnum.fetch(mod:model_number_key) = Level:Benign
          job:manufacturer = mod:manufacturer
          !Only autofil the Product Code Fault Code if it's Nokia and they
          !are not using the other Product Code field.
          !Only autofil the Product Code Fault Code if it's Nokia and they
          !are not using the other Product Code field.
          If ?job:ProductCode{prop:Hide} = 1
              If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                  job:fault_code1 = mod:product_type
              End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
          Else !If ?job:ProductCode{prop:Hide} = 1
              If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA'  Or job:manufacturer = 'MOTOROLA'
                  job:fault_code1 = job:ProductCode
              End!If job:manufacturer = 'SIEMENS' Or job:manufacturer = 'NOKIA' Or job:manufacturer = 'MOTOROLA'
          End !If ?job:ProductCode{prop:Hide} = 1
          model_number_temp   = job:model_number
      End!If access:modelnum.fetch(mod:model_number_key)
      
      count# = 0
      colour" = ''
      save_moc_id = access:modelcol.savefile()
      access:modelcol.clearkey(moc:colour_key)
      moc:model_number = job:model_number
      set(moc:colour_key,moc:colour_key)
      loop
          if access:modelcol.next()
             break
          end !if
          if moc:model_number <> job:model_number      |
              then break.  ! end if
          count# += 1
          colour" = moc:colour
          If count# > 1
              Break
          End!If count# > 1
      end !loop
      access:modelcol.restorefile(save_moc_id)
      If count# = 1
          job:colour  = colour"
      End!If count# = 1
      
      !Added by Neil!
      
      job:Model_Number = IMEIModelRoutine(job:ESN,job:Model_Number)
      
      !DO ESN_Model_Check
      
      !do check_msn
      
      If mod:specify_unit_type = 'YES'
          job:unit_type   = mod:unit_type
      End!If mod:specify_unit_type = 'YES'
      
      Do Check_MSN
      
      If CheckLength('IMEI',job:Model_Number,job:ESN)
          job:ESN = ''
          Select(?job:ESN)
          Error# = 1
      End !If CheckLength('IMEI',job:Model_Number,job:ESN)
      
      End !If ~0{prop:acceptall}
      
      Do CheckRequiredFields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Model_Number, Accepted)
    OF ?Lookup_Model_Number
      ThisWindow.Update
      mod:Model_Number = job:Model_Number
      
      IF SELF.RUN(3,Selectrecord)  = RequestCompleted
          job:Model_Number = mod:Model_Number
          Select(?+1)
      ELSE
          Select(?job:Model_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Model_Number)
    OF ?job:Unit_Type
      IF job:Unit_Type OR ?job:Unit_Type{Prop:Req}
        uni:Unit_Type = job:Unit_Type
        !Save Lookup Field Incase Of error
        look:job:Unit_Type        = job:Unit_Type
        IF Access:UNITTYPE.TryFetch(uni:Unit_Type_Key)
          IF SELF.Run(4,SelectRecord) = RequestCompleted
            job:Unit_Type = uni:Unit_Type
          ELSE
            !Restore Lookup On Error
            job:Unit_Type = look:job:Unit_Type
            SELECT(?job:Unit_Type)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Unit_Type
      ThisWindow.Update
      uni:Unit_Type = job:Unit_Type
      
      IF SELF.RUN(4,Selectrecord)  = RequestCompleted
          job:Unit_Type = uni:Unit_Type
          Select(?+1)
      ELSE
          Select(?job:Unit_Type)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Unit_Type)
    OF ?job:Colour
      IF job:Colour OR ?job:Colour{Prop:Req}
        moc:Colour = job:Colour
        moc:Model_Number = JOB:Model_Number
        !Save Lookup Field Incase Of error
        look:job:Colour        = job:Colour
        IF Access:MODELCOL.TryFetch(moc:Colour_Key)
          IF SELF.Run(5,SelectRecord) = RequestCompleted
            job:Colour = moc:Colour
          ELSE
            CLEAR(moc:Model_Number)
            !Restore Lookup On Error
            job:Colour = look:job:Colour
            SELECT(?job:Colour)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Colour
      ThisWindow.Update
      moc:Colour = job:Colour
      moc:Model_Number = JOB:Model_Number
      
      IF SELF.RUN(5,Selectrecord)  = RequestCompleted
          job:Colour = moc:Colour
          Select(?+1)
      ELSE
          Select(?job:Colour)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Colour)
    OF ?tmp:Network
      IF tmp:Network OR ?tmp:Network{Prop:Req}
        net:Network = tmp:Network
        !Save Lookup Field Incase Of error
        look:tmp:Network        = tmp:Network
        IF Access:NETWORKS.TryFetch(net:NetworkKey)
          IF SELF.Run(6,SelectRecord) = RequestCompleted
            tmp:Network = net:Network
          ELSE
            !Restore Lookup On Error
            tmp:Network = look:tmp:Network
            SELECT(?tmp:Network)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupNetwork
      ThisWindow.Update
      net:Network = tmp:Network
      
      IF SELF.RUN(6,Selectrecord)  = RequestCompleted
          tmp:Network = net:Network
          Select(?+1)
      ELSE
          Select(?tmp:Network)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:Network)
    OF ?job:DOP
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:DOP, Accepted)
      If ~0{prop:acceptall}
          error# = 0
          If job:dop > Today()
              Case MessageEx('You have entered an invalid date.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Select(?job:dop)
              error# = 1
          End!If job:dop > Today()
          If error# = 0
              If job:warranty_job = 'YES'
                  access:manufact.clearkey(man:manufacturer_key)
                  man:manufacturer = job:manufacturer
                  if access:manufact.fetch(man:manufacturer_key) = Level:Benign
                      If job:dop < (job:date_booked - man:warranty_period) and man:warranty_period <> 0 and job:dop <> ''
                          Case MessageEx('This unit is outside the Manufacturer''s Warranty Period.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
      
                          ! Start Change 2601 BE(09/05/03)
                          !GetStatus(130,ThisWindow.Request,'JOB') !dop query
                          IF (job:current_status[1:3] <> '130') THEN
                              GetStatus(130,thiswindow.request,'JOB') !DOP query
                          END
                          ! End Change 2601 BE(09/05/03)
      
                          DO time_remaining
                          get(audit,0)
                          if access:audit.primerecord() = level:benign
                              aud:ref_number    = job:ref_number
                              aud:date          = today()
                              aud:time          = clock()
                              access:users.clearkey(use:password_key)
                              use:password =glo:password
                              access:users.fetch(use:password_key)
                              aud:user = use:user_code
                              aud:action        = 'D.O.P. OVER WARRANTY PERIOD BY ' & (job:date_booked - (job:dop + man:warranty_period)) & 'DAY(S)'
                              ! Start Change 2561 BE(28/04/03)
                              aud:notes = 'DATE BOOKED: ' & FORMAT(job:date_booked, @d06) & |
                                          '<13><10>DOP: ' & FORMAT(job:dop, @d06) & |
                                          '<13><10>WARRANTY PERIOD: ' & FORMAT(man:warranty_period, @n6) & ' DAYS'
                              ! End Change 2561 BE(28/04/03)
                              access:audit.insert()
                          end!�if access:audit.primerecord() = level:benign
      
                      ! Start Change 2601 BE(09/05/03)
                      ELSE
                          IF (job:current_status[1:3] = '130') THEN
                              ! Revert to previous status if DOP now valid
                              IF (LEN(CLIP(job:PreviousStatus)) > 0) THEN
                                  GetStatus(job:PreviousStatus[1:3],thiswindow.request,'JOB')
                              ELSE
                                  ! No Previous Status Recorded so Default to no status
                                  !GetStatus(0,thiswindow.request,'JOB')
                                  job:current_status = ''
                              END
                          END
                      ! End Change 2601 BE(09/05/03)
      
                      End!If job:dop > man:warranty_period + Today()
                  end!if access:manufact.fetch(man:manufacturer_key) = Level:Benign
              End!If job:warranty_job = 'YES'
          End!If error# = 0
      
      End!If ~0{prop:acceptall}
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:DOP, Accepted)
    OF ?job:Mobile_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Mobile_Number, Accepted)
      ! Start Change MobileNumberValidate BE (23/06/03)
       If ~0{prop:acceptall}
          len# = LEN(CLIP(job:mobile_number))
          iy# = 0
          mobile_error# = 0
          LOOP ix# = 1 TO len#
              IF (job:mobile_number[ix#] <> ' ') THEN
                  IF ((job:mobile_number[ix#] < '0') OR (job:mobile_number[ix#] > '9')) THEN
                      mobile_error# = 1
                  END
                  iy# += 1
                  temp_255string[iy#] = job:mobile_number[ix#]
              END
          END
      
          ! Check for Validation error here... (mobile_error#)
          IF ((ValidateMobileNumber) AND (mobile_error# = 1)) THEN
              MessageEx('Mobile Number must be numeric','ServiceBase 2000',|
                      'Styles\stop.ico','&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,|
                      '',0,beep:systemhand,msgex:samewidths,84,26,0)
              SELECT(?job:mobile_number)
              job:mobile_number = ''
              CYCLE
          ELSE
              IF (iy# > 0) THEN
                  job:mobile_number = temp_255string[1 : iy#]
                  job:mobile_number[1] = UPPER(job:mobile_number[1])
              ELSE
                  job:mobile_number = ''
              END
              Display(?job:mobile_number)
          END
       END
       ! End Change MobileNumberValidate BE (23/06/03)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Mobile_Number, Accepted)
    OF ?job:Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, Accepted)
      IF job:Location OR ?job:Location{Prop:Req}
        loi:Location = job:Location
        loi:Location_Available = 'YES'
        !Save Lookup Field Incase Of error
        look:job:Location        = job:Location
        IF Access:LOCINTER.TryFetch(loi:Location_Available_Key)
          IF SELF.Run(7,SelectRecord) = RequestCompleted
            job:Location = loi:Location
          ELSE
            CLEAR(loi:Location_Available)
            !Restore Lookup On Error
            job:Location = look:job:Location
            SELECT(?job:Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! Amend Location Levels
      If job:location <> location_temp
          job:location = loi:location
          If location_temp <> ''
      !Add To Old Location
              access:locinter.clearkey(loi:location_key)
              loi:location = location_temp
              If access:locinter.fetch(loi:location_key) = Level:Benign
                  If loi:allocate_spaces = 'YES'
                      loi:current_spaces+= 1
                      loi:location_available = 'YES'
                      access:locinter.update()
                  End
              end !if
          End!If location_temp <> ''
      !Take From New Location
          access:locinter.clearkey(loi:location_key)
          loi:location = job:location
          If access:locinter.fetch(loi:location_key) = Level:Benign
              If loi:allocate_spaces = 'YES'
                  loi:current_spaces -= 1
                  If loi:current_spaces< 1
                      loi:current_spaces = 0
                      loi:location_available = 'NO'
                  End
                  access:locinter.update()
              End
          end !if
          location_temp  = job:location
      
          Display()
              
      
      
      End!If job:location <> location_temp
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, Accepted)
    OF ?Lookup_Location
      ThisWindow.Update
      loi:Location = job:Location
      
      IF SELF.RUN(7,Selectrecord)  = RequestCompleted
          job:Location = loi:Location
          Select(?+1)
      ELSE
          Select(?job:Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Location)
    OF ?job:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Account_Number, Accepted)
      IF job:Account_Number OR ?job:Account_Number{Prop:Req}
        sub:Account_Number = job:Account_Number
        !Save Lookup Field Incase Of error
        look:job:Account_Number        = job:Account_Number
        IF Access:SUBTRACC.TryFetch(sub:Account_Number_Key)
          IF SELF.Run(8,SelectRecord) = RequestCompleted
            job:Account_Number = sub:Account_Number
          ELSE
            !Restore Lookup On Error
            job:Account_Number = look:job:Account_Number
            SELECT(?job:Account_Number)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          error# = 0
          stop# = 0
          access:subtracc.clearkey(sub:account_number_key)
          sub:account_number  = job:account_number
          access:subtracc.fetch(sub:account_number_key)
          access:tradeacc.clearkey(tra:account_number_key)
          tra:account_number  = sub:main_account_number
          If access:tradeacc.fetch(tra:account_number_key) = Level:Benign
              If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If sub:Stop_Account = 'YES'
                      stop# = 1
                  End !If tra:Stop_Account = 'YES'
              Else !If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
                  If tra:Stop_Account = 'YES'
                      stop# = 1
                  End !If tra:Stop_Account = 'YES'
              End !If tra:Invoice_Sub_Accounts = 'YES' And tra:Use_Sub_Accounts = 'YES'
          End!If access:tradeacc.fetch(tra:account_number_key) = Level:Benign
      
          If stop# = 0
              If tra:ForceOrderNumber
                  ?job:Order_Number{prop:req} = 1
              Else !If tra:ForceOrderNumber
                  ?job:Order_Number{prop:req} = 0
              End !If tra:ForceOrderNumber
              error# = 0
              access:trantype.clearkey(trt:transit_type_key)
              trt:transit_type = job:transit_type
              if access:trantype.fetch(trt:transit_type_key) = Level:Benign
                  If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                      beep(beep:systemhand)  ;  yield()
                      message('Loan Units are not authorised for thisTrade Customer. Amend the '&|
                              'Trade Customer details or select another Initial Transit Type.', |
                              'ServiceBase 2000', icon:hand)
                      error# = 1
                  End !If trt:loan_unit = 'YES' And tra:allow_loan <> 'YES'
                  If trt:exchange_unit = 'YES' And tra:allow_exchange <> 'YES' And error# = 0
                      beep(beep:systemhand)  ;  yield()
                      message('Exchange Units are not authorised for thisTrade Customer. Amend the '&|
                              'Trade Customer details or select another Initial Transit Type.', |
                              'ServiceBase 2000', icon:hand)
                      error# = 1
                  End !If trt:exchange_unit = 'YES' And tra:allow_loan <> 'YES'
              end
      
              !Check to see if the IMEI matches the account number, if there is one.
              If Error# = 0 And job:ESN <> ''
                  If ExchangeAccount(job:Account_Number)
                      Access:EXCHANGE.ClearKey(xch:ESN_Only_Key)
                      xch:ESN = job:ESN
                      If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                          !Found
                          !Write the relevant fields to say this unit is in repair and take completed.
                          !Otherwise the use could click cancel and screw everything up.
                      Else!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          Case MessageEx('The selected Trade Account is setup to repair Exchange Units, but the entered I.M.E.I. Number cannot be found in Exchange Stock.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          Error# = 1
                      End!If Access:EXCHANGE.TryFetch(xch:ESN_Only_Key) = Level:Benign
                  Else
                      Access:EXCHANGE.Clearkey(xch:ESN_Only_Key)
                      xch:ESN = job:ESN
                      If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                          !Found
                          If xch:Available <> 'DES'
                              Case MessageEx('The entered I.M.E.I. matches that of an entry in the Exchange Database. This unit is not marked as "Despatched" so may still exist in your stock.'&|
                                '<13,10>'&|
                                '<13,10>If you continue, this job will be treated as a normal customer repair and not a "In House Exchange Repair". These "In House" repairs should be booked in using an Account that is marked as "Exchange Account".'&|
                                '<13,10>'&|
                                '<13,10>Are you sure you want to continue?','ServiceBase 2000',|
                                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                  Of 1 ! &Yes Button
                                  Of 2 ! &No Button
                                      Error# = 1
                              End!Case MessageEx
                          End!Case MessageEx
      
                      Else! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                      End! If Access:EXCHANGE.Tryfetch(xch:ESN_Only_Key) = Level:Benign
                      
                  End !ExchangeAccount(job:Account_Number)
              End !Error# = 0
      
              If error# = 0
                  access:trantype.clearkey(trt:transit_type_key)
                  trt:transit_type = job:transit_type
                  if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                      If tra:refurbcharge = 'YES' And trt:exchange_unit = 'YES'
                          job:chargeable_job  = 'YES'
                          job:warranty_job    = 'YES'
                          job:charge_type = tra:chargetype
                          job:warranty_charge_type    = tra:warchargetype
                      End!If tra:refurbcharge = 'YES'
                  end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                  If tra:force_estimate = 'YES'
                      job:estimate_if_over    = tra:estimate_if_over
                      job:estimate        = 'YES'
                  End!If tra:force_estimate = 'YES'
                  If tra:turnaround_time <> ''
                      job:turnaround_time = tra:turnaround_time
                  End!If tra:turnaround_time <> ''
      
                  If tra:Use_Contact_Name = 'YES'
                      Unhide(?job:surname)
                      Unhide(?job:title)
                      Unhide(?job:title:prompt)
                      Unhide(?job:surname:prompt)
                      Unhide(?customer_name)
                      unhide(?job:initial:prompt)
                      unhide(?job:initial)
                  Else
                      Hide(?job:surname)
                      Hide(?job:title)
                      Hide(?job:title:prompt)
                      Hide(?job:surname:prompt)
                      Hide(?customer_name)
                      Hide(?job:initial:prompt)
                      Hide(?job:initial)
      
                  End
      
      
                  If tra:invoice_sub_accounts  <> 'YES'
                      If tra:use_customer_address <> 'YES'
                          JOB:Postcode            = tra:Postcode
                          JOB:Company_Name        = tra:Company_Name
                          JOB:Address_Line1       = tra:Address_Line1
                          JOB:Address_Line2       = tra:Address_Line2
                          JOB:Address_Line3       = tra:Address_Line3
                          JOB:Telephone_Number    = tra:Telephone_Number
                          JOB:Fax_Number          = tra:Fax_Number
                      End!If tra:use_customer_address <> 'YES'
                  Else!If tra:invoice_sub_accounts  <> 'YES'
                      If sub:use_customer_address <> 'YES'
                          JOB:Postcode            = sub:Postcode
                          JOB:Company_Name        = sub:Company_Name
                          JOB:Address_Line1       = sub:Address_Line1
                          JOB:Address_Line2       = sub:Address_Line2
                          JOB:Address_Line3       = sub:Address_Line3
                          JOB:Telephone_Number    = sub:Telephone_Number
                          JOB:Fax_Number          = sub:Fax_Number
                      End!If sub:use_customer_address <> 'YES'
                  End!If tra:invoice_sub_accounts  <> 'YES'
                  If tra:use_sub_accounts <> 'YES'
                      If tra:use_delivery_address = 'YES'
                          job:postcode_delivery   = tra:postcode
                          job:company_name_delivery   = tra:company_name
                          job:address_line1_delivery  = tra:address_line1
                          job:address_line2_delivery  = tra:address_line2
                          job:address_line3_delivery  = tra:address_line3
                          job:telephone_delivery      = tra:telephone_number
                      End !If tra:use_delivery_address = 'YES'
                      If tra:use_collection_address = 'YES'
                          job:postcode_collection   = tra:postcode
                          job:company_name_collection   = tra:company_name
                          job:address_line1_collection  = tra:address_line1
                          job:address_line2_collection  = tra:address_line2
                          job:address_line3_collection  = tra:address_line3
                          job:telephone_collection      = tra:telephone_number
                      End !If tra:use_delivery_address = 'YES'
      
                      job:courier = tra:courier_outgoing
                      job:incoming_courier = tra:courier_incoming
                      job:exchange_courier = job:courier
                      job:loan_courier = job:courier
                  Else
                      If sub:use_delivery_address = 'YES'
                          job:postcode_delivery      = sub:postcode
                          job:company_name_delivery  = sub:company_name
                          job:address_line1_delivery = sub:address_line1
                          job:address_line2_delivery = sub:address_line2
                          job:address_line3_delivery = sub:address_line3
                          job:telephone_delivery     = sub:telephone_number
                      End
                      If sub:use_collection_address = 'YES'
                          job:postcode_collection      = sub:postcode
                          job:company_name_collection  = sub:company_name
                          job:address_line1_collection = sub:address_line1
                          job:address_line2_collection = sub:address_line2
                          job:address_line3_collection = sub:address_line3
                          job:telephone_collection     = sub:telephone_number
                      End
      
                      job:courier = sub:courier_outgoing
                      job:incoming_courier = sub:courier_incoming
                      job:exchange_courier = job:courier
                      job:loan_courier = job:courier
                  End
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = job:courier
                  access:courier.tryfetch(cou:courier_key)
                  IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                      job:loaservice  = cou:service
                      job:excservice  = cou:service
                      job:jobservice  = cou:service
                  Else!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
                      job:loaservice  = ''
                      job:excservice  = ''
                      job:jobservice  = ''
                  End!IF cou:courier_type = 'CITY LINK' Or cou:courier_Type = 'LABEL G'
      
                  Post(Event:accepted,?job:incoming_courier)
              Else!If error# = 0
                  job:account_number  = ''
                  Select(?job:account_number)
              End!If error# = 0
          Else!If stop# = 0
              beep(beep:systemhand)  ;  yield()
              message('The selected Trade Account is on Stop. A new transaction cannot '&|
                      'be created for this customer.'&|
                      '||Please select another.', |
                      'ServiceBase 2000', icon:hand)
              job:account_number = ''
              Display()
          End!If stop# = 0
          Display()
      
      
      End!If ~0{prop:acceptall}
      Display()
      Do CheckRequiredFields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Account_Number, Accepted)
    OF ?Lookup_Account_Number
      ThisWindow.Update
      sub:Account_Number = job:Account_Number
      
      IF SELF.RUN(8,Selectrecord)  = RequestCompleted
          job:Account_Number = sub:Account_Number
          Select(?+1)
      ELSE
          Select(?job:Account_Number)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Account_Number)
    OF ?job:Chargeable_Job
      IF ?job:Chargeable_Job{Prop:Checked} = True
        ENABLE(?lookup_Charge_Type)
        ENABLE(?JOB:Charge_Type)
      END
      IF ?job:Chargeable_Job{Prop:Checked} = False
        job:Charge_Type = ''
        DISABLE(?lookup_Charge_Type)
        DISABLE(?JOB:Charge_Type)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Chargeable_Job, Accepted)
      If ~0{prop:acceptall}
          If job:chargeable_job = 'YES'
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              If def:ChaChargeType <> ''
                  job:Charge_Type = def:ChaChargeType
              Else!If def:ChaChargeType <> ''
                  charge_type"    = ''
                  found# = 0
                  save_cha_id = access:chartype.savefile()                                            !It's too late, but the warranty tick is not
                  access:chartype.clearkey(cha:warranty_key)                                          !Defaulting to no
                  cha:warranty    = 'NO'
                  set(cha:warranty_key,cha:warranty_key)
                  loop
                      if access:chartype.next()
                         break
                      end !if
                      If cha:warranty <> 'NO' Then Break.
                      found# += 1
                      charge_type"    = cha:charge_type
                      If found# > 1
                          Break
                      End!If found# > 1
      
                  end !loop
                  access:chartype.restorefile(save_cha_id)
                  If found# = 1
                      job:charge_type = charge_type"
                      Do Estimate_Check
                      Select(?job:warranty_job)
                  Else
                      Select(?lookup_charge_type)
                  End!If found# = 1
              End!If def:ChaChargeType <> ''
              Do Estimate_Check
              Display(?job:charge_type)
              
          End!If job:warranty_job = 'YES'
      End!If ~0{prop:acceptall}
      Do CheckRequiredFields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Chargeable_Job, Accepted)
    OF ?lookup_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_Charge_Type, Accepted)
      case globalresponse
          of requestcompleted
              job:charge_type = cha:charge_type
              Do Estimate_Check
              select(?+2)
          of requestcancelled
      !        job:charge_type = ''
              select(?-1)
      end!case globalreponse
      display(?job:charge_type)
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?lookup_Charge_Type, Accepted)
    OF ?job:Warranty_Job
      IF ?job:Warranty_Job{Prop:Checked} = True
        ENABLE(?Lookup_Warranty_Charge_Type)
        ENABLE(?JOB:Warranty_Charge_Type)
      END
      IF ?job:Warranty_Job{Prop:Checked} = False
        job:Warranty_Charge_Type = ''
        DISABLE(?Lookup_Warranty_Charge_Type)
        DISABLE(?JOB:Warranty_Charge_Type)
      END
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
      If ~0{prop:acceptall}
          If job:warranty_job = 'YES'
              Set(DEFAULTS)
              Access:DEFAULTS.Next()
              If def:WarChargeType <> ''
                  job:Warranty_Charge_Type = def:WarChargeType
              Else!If def:WarChargeType <> ''
                  charge_type"    = ''
                  found# = 0
                  save_cha_id = access:chartype.savefile()
                  access:chartype.clearkey(cha:warranty_key)
                  cha:warranty    = 'YES'
                  set(cha:warranty_key,cha:warranty_key)
                  loop
                      if access:chartype.next()
                         break
                      end !if
                      if cha:warranty    <> 'YES'      |
                          then break.  ! end if
                      found# += 1
                      charge_type"    = cha:charge_type
                      If found# > 1
                          Break
                      End!If found# > 1
      
                  end !loop
                  access:chartype.restorefile(save_cha_id)
                  If found# = 1
                      job:warranty_Charge_type = charge_type"
                      Select(?JOB:Intermittent_Fault)
                  Else
                      Select(?lookup_warranty_charge_type)
                  End!If found# = 1
              End!If def:WarChargeType <> ''
              Display(?job:warranty_charge_type)
              
          End!If job:warranty_job = 'YES'
      End!If ~0{prop:acceptall}
      If job:warranty_job = 'NO'
          job:edi = 'XXX'
          job:warranty_charge_type = ''
      End!If job:warranty_job = 'NO'
      Display()
      Do CheckRequiredFields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Warranty_Job, Accepted)
    OF ?Lookup_Warranty_Charge_Type
      ThisWindow.Update
      GlobalRequest = SelectRecord
      Browse_Warranty_Charge_Types
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
      case globalresponse
          of requestcompleted
              job:warranty_charge_type = cha:charge_type
              select(?+2)
          of requestcancelled
      !        job:charge_type = ''
              select(?-1)
      end!case globalreponse
      display(?job:warranty_charge_type)
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Warranty_Charge_Type, Accepted)
    OF ?job:Intermittent_Fault
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Intermittent_Fault, Accepted)
      If ~0{prop:acceptall}
          If job:intermittent_fault = 'YES'
              access:jobnotes.clearkey(jbn:refnumberkey)
              jbn:refnumber = job:ref_number
              If access:jobnotes.fetch(jbn:refnumberkey) = Level:Benign
                  If jbn:fault_description = ''
                      jbn:fault_description = 'INTERMITTENT FAULT'
                  Else!If job:fault_description = ''
                      jbn:fault_description = Clip(jbn:fault_description) & '<13,10>INTERMITTENT FAULT'
                  End!If job:fault_description = ''
              End!If access:jobnotes.fetch(jbn:ref_number_key) = Level:Benign
              If ?job:incoming_courier <> '' And thiswindow.request = Insertrecord
                  If ?job:incoming_consignment_number <> ''
                      Select(?amend)
                  Else!If ?job:incoming_consignment_number <> ''
                      Select(?job:incoming_consignment_number)
                  End!If ?job:incoming_consignment_number <> ''
              Else!If ?job:incoming_courier <> '' And thiswindow.request = Insertrecord
                  Select(?job:incoming_courier)
              End!If ?job:incoming_courier <> '' And thiswindow.request = Insertrecord
          End!If job:intermittent_fault = 'YES'
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Intermittent_Fault, Accepted)
    OF ?job:Incoming_Courier
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Incoming_Courier, Accepted)
      IF job:Incoming_Courier OR ?job:Incoming_Courier{Prop:Req}
        cou:Courier = job:Incoming_Courier
        !Save Lookup Field Incase Of error
        look:job:Incoming_Courier        = job:Incoming_Courier
        IF Access:COURIER.TryFetch(cou:Courier_Key)
          IF SELF.Run(9,SelectRecord) = RequestCompleted
            job:Incoming_Courier = cou:Courier
          ELSE
            !Restore Lookup On Error
            job:Incoming_Courier = look:job:Incoming_Courier
            SELECT(?job:Incoming_Courier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      If ~0{prop:acceptall}
          If thiswindow.request = Insertrecord
              label# = 0
              access:trantype.clearkey(trt:transit_type_key)
              trt:transit_type = job:transit_type
              if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                  If trt:anccollnote = 'YES'
                      label# = 1
                  End!If trt:loan_unit = 'YES' or trt:exchange_unit = 'YES'
              Else!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
                  label# = 0
              End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              If label# = 1
                  access:courier.clearkey(cou:courier_key)
                  cou:courier = job:incoming_courier
                  if access:courier.tryfetch(cou:courier_key) = Level:Benign
                      If cou:courier_type = 'ANC'
                          Set(Defaults)
                          access:defaults.next()
                          If job:incoming_consignment_number = ''
                              job:incoming_consignment_number = def:anccollectionno
                              Display()
                          End!If job:incoming_consignment_number = ''
                      End!If cou:courier_type = 'ANC'
                  end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
              End!If label# = 1
          End!If thiswindow.request = Insertrecord
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Incoming_Courier, Accepted)
    OF ?Lookup_Courier
      ThisWindow.Update
      cou:Courier = job:Incoming_Courier
      
      IF SELF.RUN(9,Selectrecord)  = RequestCompleted
          job:Incoming_Courier = cou:Courier
          Select(?+1)
      ELSE
          Select(?job:Incoming_Courier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?job:Incoming_Courier)
    OF ?Amend
      ThisWindow.Update
      Show_Addresses
      ThisWindow.Reset
    OF ?Amend:2
      ThisWindow.Update
      Show_Addresses
      ThisWindow.Reset
    OF ?Amend:3
      ThisWindow.Update
      Show_Addresses
      ThisWindow.Reset
    OF ?Lookup_Accessory
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Accessory, Accepted)
      If job:Model_Number = ''
          Case MessageEx('You must enter a Model Number.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else
          count# = 0
      
          Save_jac_Id = Access:JOBACC.Savefile()
          Access:JOBACC.ClearKey(jac:Ref_Number_Key)
          jac:Ref_Number = job:Ref_Number
          Set(jac:Ref_Number_key,jac:Ref_Number_Key)
          Loop
              If Access:JOBACC.Next()
                 Break
              End !if
              If jac:Ref_Number <> job:Ref_Number      |
                  Then Break.  ! end if
              count# = 1
              Break
          End !loop
          Access:JOBACC.Restorefile(Save_jac_Id)
      
          glo:Select1  = job:Model_Number
          glo:Select2  = job:Ref_Number
          If count# = 0
              If SecurityCheck('JOBS - AMEND ACCESSORIES')
                  Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              Else!If SecurityCheck('JOBS - AMEND ACCESSORIES')
                  Rapid_Accessory_insert
              End!If SecurityCheck('JOBS - AMEND ACCESSORIES')
          Else!If count# = 0
              Browse_job_accessories
          End!If count# = 0
      
          glo:Select1  = ''
          glo:Select2  = ''
      
          Do Update_Accessories
      End
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup_Accessory, Accepted)
    OF ?Fault_Codes_Lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes_Lookup, Accepted)
      Access:JOBNOTES.Clearkey(jbn:RefNumberKey)
      jbn:RefNumber   = job:Ref_number
      If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
          !Found
      
      Else! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          access:jobnotes.clearkey(jbn:RefNumberKey)
          jbn:RefNumber   = job:Ref_Number
          If access:jobnotes.tryfetch(jbn:RefNumberKey)
              If access:jobnotes.primerecord() = Level:Benign
                  jbn:RefNumber   = job:ref_number
                  access:jobnotes.tryinsert()
              End!If access:jobnotes.primerecord() = Level:Benign
          End!If access:jobnotes.tryfetch(jbn:RefNumberKey)
      
      End! If Access:JOBNOTES.Tryfetch(jbn:RefNumberKey) = Level:Benign
      
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
      
      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          If Access:JOBSE.Primerecord() = Level:Benign
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryinsert()
                  Access:JOBSE.Cancelautoinc()
              End!If Access:JOBSE.Tryinsert()
          End!If Access:JOBSE.Primerecord() = Level:Benign
      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      
      
      
      
      
      If job:model_Number = ''
          Case MessageEx('You must select a Model Number.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If job:model_Number = ''
          Fault_Codes_Window(thiswindow.request)
      End!If job:model_Number = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes_Lookup, Accepted)
    OF ?Fault_Description
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Fault_Description
      ThisWindow.Reset
    OF ?engineers_notes
      ThisWindow.Update
      GlobalRequest = ChangeRecord
      Engineers_Notes
      ThisWindow.Reset
    OF ?contact_history
      ThisWindow.Update
      Browse_Contact_History
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?contact_history, Accepted)
      glo:select12 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?contact_history, Accepted)
    OF ?Repair_History
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repair_History, Accepted)
      glo:select1 = 'ESN'
      glo:select2  = job:esn
      glo:select11 = job:ref_number
      glo:select12 = 'VIEW ONLY'
      Bouncer_History
      glo:select1 = ''
      glo:select2  = ''
      glo:select11 = ''
      glo:select12 = ''
      
      
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Repair_History, Accepted)
    OF ?OK
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      If insert_global    = 'YES'
          insert_Global = ''
      End!If insert_global    = 'YES'
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  !Error Checking
  error# = 0
  If ?job:msn{prop:hide} = 0
      access:modelnum.clearkey(mod:model_number_key)
      mod:model_number = job:model_number
      if access:modelnum.fetch(mod:model_number_key) = Level:benign
          If job:esn <> ''
              If Len(Clip(job:esn)) < mod:esn_length_from Or Len(Clip(job:esn)) > mod:esn_length_to
                  beep(beep:systemhand)  ;  yield()
                  message('The E.S.N. / I.M.E.I. entered should be between '&|
                          clip(mod:esn_length_from)&' And '&clip(mod:esn_length_to)&' characters '&|
                          'in length.', |
                          'ServiceBase 2000', icon:hand)
                  error# = 1
                  Select(?job:ESN)
              End !If Len(job:esn) < mod:esn_length_from Or Len(job:esn) > mod:esn_length_to
              If man:use_msn = 'YES' and job:msn <> '' and job:msn <> 'N/A'
                  If len(Clip(job:msn)) < mod:msn_length_from Or len(Clip(job:msn)) > mod:msn_length_to
                      beep(beep:systemhand)  ;  yield()
                      message('The M.S.N. entered should be between '&|
                              clip(mod:msn_length_from)&' And '&clip(mod:msn_length_to)&' characters '&|
                              'in length.', |
                              'ServiceBase 2000', icon:hand)
                      error# = 1
                      Select(?job:msn)
                  End!If len(job:msn) < mod:msn_length_from Or len(job:msn) > mod:msn_length_to
              End!If man:use_msn = 'YES'
          End !If job:esn <> ''
      End!if access:modelnum.fetch(mod:model_number_key) = Level:benign
  End!If ?job:msn{prop:hide} = 0
  If error# = 1
      Cycle
  End!If error# = 1
  If thiswindow.request = Insertrecord
  !Error Checking
      !Move save fields to here so that the compulsory fields are filled in
      !Hopefully this record will be deleted if the job isn't completed.
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
  
      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          If Access:JOBSE.PrimeRecord() = Level:Benign
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.TryInsert() = Level:Benign
                  !Insert Successful
              Else !If Access:JOBSE.TryInsert() = Level:Benign
                  !Insert Failed
              End !If Access:JOBSE.TryInsert() = Level:Benign
          End !If Access:JOBSE.PrimeRecord() = Level:Benign
      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
      jobe:SkillLevel = tmp:SkillLevel
      jobe:Network    = tmp:Network
      If job:Workshop = 'YES'
          jobe:InWorkshopDate = Today()
          jobe:InWorkshopTime = Clock()
      End !job:Workshop = 'YES'
      access:JOBSE.Update()
  
  
      cont# = 1
      acc_count# = 0
      save_jac_id = access:jobacc.savefile()
      access:jobacc.clearkey(jac:ref_number_key)
      jac:ref_number = job:ref_number
      set(jac:ref_number_key,jac:ref_number_key)
      loop
          if access:jobacc.next()
             break
          end !if
          if jac:ref_number <> job:ref_number      |
              then break.  ! end if
          acc_count# = 1
          Break
      end !loop
      access:jobacc.restorefile(save_jac_id)
  
      If acc_count# = 0 
          Case MessageEx('You have not entered any Accessories for this job.<13,10><13,10>Do you wish to enter them before you completed booking?','ServiceBase 2000','Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,-1,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  cont# = 0
                  Cycle
              Of 2 ! &No Button
                  tmp:retain = 0
          End!Case MessageEx
      End!If acc_count# = 0
  
      If cont# = 1
          error# = 0
          CompulsoryFieldCheck('B')
          If glo:ErrorText <> ''
              glo:errortext = 'You cannot complete booking due to the following error(s): <13,10>' & Clip(glo:errortext)
              Error_Text
              glo:errortext = ''
              Cycle
          End!If glo:ErrorText <> ''
  
        !End Of Error Checking
  
          get(audit,0)
          if access:audit.primerecord() = level:benign
              aud:notes         = 'UNIT DETAILS: ' & Clip(job:manufacturer) & ' ' & Clip(job:model_number) & ' - ' & Clip(job:unit_type) & |
                                  '<13,10>E.S.N. / I.M.E.I.: ' & Clip(job:esn) & |
                                  '<13,10>M.S.N.: ' & Clip(job:msn)
              If job:Chargeable_job = 'YES'
                  aud:Notes   = Clip(aud:Notes) & '<13,10>CHARGEABLE CHARGE TYPE: ' & Clip(job:Charge_Type)
              End !If job:Chargeable_job = 'YES'
              If job:Warranty_Job = 'YES'
                  aud:Notes   = Clip(aud:Notes) & '<13,10>WARRANTY CHARGE TYPE: ' & Clip(job:Warranty_Charge_Type)
              End !If job:Warranty_Job = 'YES'
              count# = 0
              save_jac_id = access:jobacc.savefile()
              access:jobacc.clearkey(jac:ref_number_key)
              jac:ref_number = job:ref_number
              set(jac:ref_number_key,jac:ref_number_key)
              loop
                  if access:jobacc.next()
                     break
                  end !if
                  if jac:ref_number <> job:ref_number      |
                      then break.  ! end if
                  count# += 1
                  aud:notes = Clip(aud:notes) & '<13,10>ACCESSORY ' & Clip(count#) & ': ' & Clip(jac:accessory)
              end !loop
              access:jobacc.restorefile(save_jac_id)
  
              aud:ref_number    = job:ref_number
              aud:date          = Today()
              aud:time          = Clock()
              access:users.clearkey(use:password_key)
              use:password =glo:password
              access:users.fetch(use:password_key)
              aud:user = use:user_code
              aud:action        = 'RAPID NEW JOB BOOKING INITIAL ENTRY'
              if access:audit.insert()
                  access:audit.cancelautoinc()
              end
          end!if access:audit.primerecord() = level:benign
  
          if job:warranty_job = 'YES'
              job:edi = Set_Edi(job:manufacturer)
          End!if job:warranty_job = 'YES'
  
          If job:chargeable_job = 'YES'
              labour" = 0
              parts"  = 0
              pass"   = False
              Pricing_Routine('C',labour",parts",pass",discount")
              If pass" = true
                  job:labour_cost = labour"
                  job:parts_cost  = parts"
              Else!If pass" = true
                  job:labour_cost = 0
                  job:parts_cost  = 0
              End!If pass" = true
  
              !Is this job an estimate, if so change the status
              ! Start Change 3922 BE(08/03/04)
              !If job:Estimate = 'YES'
              IF (job:Estimate = 'YES' AND job:Estimate_Accepted <> 'YES') THEN
                  !GetStatus(505,1,'JOB')
                  Total_Price('E', vat", total", balance")
                  Total_Price('C', vat2", total2", balance2")
                  total$ = total" + total2"
                  IF ((job:estimate_if_over = 0.0) OR (total$ > job:estimate_if_over)) THEN
                    GetStatus(505,1,'JOB')
                  END
              END
              ! End Change 3922 BE(08/03/04)
          End!If job:chargeable_job = 'YES'
          If job:warranty_job = 'YES'
              labour" = 0
              parts"  = 0
              pass"   = False
              Pricing_Routine('W',labour",parts",pass",discount")
              If pass" = true
                  job:labour_cost_warranty    = labour"
                  job:parts_cost_warranty     = parts"
              Else!If pass" = true
                  job:labour_cost_warranty    = 0
                  job:parts_cost_warranty     = 0
              End!If pass" = true
          End!If job:warranty_job = 'YES'
  
          set(Defrapid)
          access:defrapid.next()
  
          If print_label_temp = 'YES'
              Set(defaults)
              access:defaults.next()
              Case def:label_printer_type
                  Of 'TEC B-440 / B-442'
                      Label_Type_temp = 1
                  Of 'TEC B-452'
                      label_type_temp = 2
              End!Case def:themal_printer_type
          End!If print_label_temp = 'YES'
  
          sav:ref_number   = job:ref_number
          sav:esn          = job:esn
      End!If cont# = 1
  End!If thiswindow.request = Insertrecord
  !ANC Label
  If thiswindow.request = Insertrecord
      anc# = 0
      access:courier.clearkey(cou:courier_key)
      cou:courier = job:courier
      if access:courier.tryfetch(cou:courier_key) = Level:Benign
          If cou:courier_type = 'ANC'
              anc# = 1
          End!If cou:courier_type = 'ANC'
      end!if access:courier.tryfetch(cou:courier_key) = Level:Benign
      IF anc# = 1
          label# = 0
          access:trantype.clearkey(trt:transit_type_key)
          trt:transit_type = job:transit_type
          if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              If trt:anccollnote = 'YES'
                  label# = 1
              End!If trt:loan_unit = 'YES' or trt:exchange_unit = 'YES'
          Else!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
              label# = 0
          End!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
      End!IF anc# = 1
  
      IF label# = 1
          glo:file_name = 'C:\ANC.TXT'
          Remove(expgen)
          If access:expgen.open()
              Stop(error())
              Return Level:Benign
          End!If access:expgen.open()
          access:expgen.usefile()
          Clear(gen:record)
          gen:line1   = job:ref_number
          gen:line1   = Sub(gen:line1,1,10) & job:company_name
          gen:line1   = Sub(gen:line1,1,40) & job:address_line1_delivery
          gen:line1   = Sub(gen:line1,1,70) & job:address_line2_delivery
          gen:line1   = Sub(gen:line1,1,100) & job:address_line3_delivery
          gen:line1   = Sub(gen:line1,1,130) & job:postcode_delivery
          gen:line1   = Sub(gen:line1,1,150) & job:telephone_delivery
          gen:line1   = Sub(gen:line1,1,175) & job:unit_type
          gen:line1   = Sub(gen:line1,1,195) & job:model_number
          gen:line1   = Sub(gen:line1,1,215) & cou:ContractNumber
          access:expgen.insert()
          Close(expgen)
          access:expgen.close()
          cou:anccount += 1
          If cou:ANCCount = 999
              cou:ANCCount = 1
          End
          access:courier.update()
          Set(Defaults)
          access:defaults.next()
          DEF:ANCCollectionNo += 10
          access:defaults.update()
          sav:path    = path()
          setcursor(cursor:wait)
          setpath(Clip(cou:ancpath))
          Run('ancpaper.exe',1)
          Setpath(sav:path)
          setcursor()
  
      End!IF label# = 1
  End!If thiswindow.request = Insertrecord
  Hide(?Repair_History)
  Hide(?Bouncer_Text)
  !Save Fields
  Access:JOBSE.Clearkey(jobe:RefNumberKey)
  jobe:RefNumber  = job:Ref_Number
  If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Found
  
  Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      !Error
      !Assert(0,'<13,10>Fetch Error<13,10>')
      If Access:JOBSE.PrimeRecord() = Level:Benign
          jobe:RefNumber  = job:Ref_Number
          If Access:JOBSE.TryInsert() = Level:Benign
              !Insert Successful
          Else !If Access:JOBSE.TryInsert() = Level:Benign
              !Insert Failed
          End !If Access:JOBSE.TryInsert() = Level:Benign
      End !If Access:JOBSE.PrimeRecord() = Level:Benign
  End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
  
  jobe:SkillLevel = tmp:SkillLevel
  jobe:Network    = tmp:Network
  If job:Workshop = 'YES'
      jobe:InWorkshopDate = Today()
      jobe:InWorkshopTime = Clock()
  End !job:Workshop = 'YES'
  access:JOBSE.Update()
  ?tmp:Print_JobCard{prop:Hide} = 1
  ReturnValue = PARENT.TakeCompleted()
  If thiswindow.request = Insertrecord
      Set(defaults)
      access:defaults.next()
      If print_label_temp = 'YES' And def:use_job_label = 'YES'
          glo:select1 = sav:ref_number
      !   case label_type_temp
      !       Of 1
                  If job:bouncer = 'B'
                      Thermal_Labels('SE')
                  Else!If job:bouncer = 'B'
                      If ExchangeAccount(job:Account_Number)
                          Thermal_Labels('ER')
                      Else !If ExchangeAccount(job:Account_Number)
                          Thermal_Labels('')
                      End !If ExchangeAccount(job:Account_Number)
                  End!If job:bouncer = 'B'
                  
      !       Of 2
      !           Thermal_Labels_B452
      !   End!case label_type_temp
          glo:select1 = ''
      End!If print_label_temp = 'YES'
      If print_job_card_temp = 'YES'
          glo:select1  = sav:ref_number
          Job_Card
          glo:select1  = ''
      End!If print_job_card_temp = 'YES'
      If tmp:Print_Jobcard = TRUE and tmp:Print_JobCard{prop:Hide} = 0
          glo:select1  = sav:ref_number
          Job_Card
          glo:select1  = ''
      End!If print_job_card_temp = 'YES'
      IF tmp:retain = 1 And def:use_job_label = 'YES'
          glo:select1  = sav:ref_number
          job_retained_accessories_label
          glo:select1  = ''
      end!if access:trantype.tryfetch(trt:transit_type_key) = Level:Benign
  
      If tmp:printreceipt = 1
          glo:select1  = sav:ref_number
          Job_receipt
          glo:select1  = ''
      End!If tmp:printreceipt = 1
  End!If thiswindow.request = Insertrecord
  Do save_fields
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Jobs')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?job:Transit_Type
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Transit_Type, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Transit Type')
             Post(Event:Accepted,?Lookup_Transit_Type)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Transit_Type)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Transit_Type, AlertKey)
    END
  OF ?job:ProductCode
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ProductCode, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Product Code')
             Post(Event:Accepted,?LookupProductCode)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupProductCode)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ProductCode, AlertKey)
    END
  OF ?job:Model_Number
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Model_Number, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Model Number')
             Post(Event:Accepted,?Lookup_Model_Number)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Model_Number)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Model_Number, AlertKey)
    END
  OF ?job:Unit_Type
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Unit_Type, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Unit Type')
             Post(Event:Accepted,?Lookup_Unit_Type)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Unit_Type)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Unit_Type, AlertKey)
    END
  OF ?job:Colour
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Colour, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Colour')
             Post(Event:Accepted,?Lookup_Colour)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Colour)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Colour, AlertKey)
    END
  OF ?tmp:Network
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Network, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Networks')
             Post(Event:Accepted,?LookupNetwork)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupNetwork)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:Network, AlertKey)
    END
  OF ?job:Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Location')
             Post(Event:Accepted,?Lookup_Location)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Location)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Location, AlertKey)
    END
  OF ?job:Account_Number
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Account_Number, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Account Number')
             Post(Event:Accepted,?Lookup_Account_Number)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Account_Number)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Account_Number, AlertKey)
    END
  OF ?job:Incoming_Courier
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Incoming_Courier, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Courier')
             Post(Event:Accepted,?Lookup_Courier)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?Lookup_Courier)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Incoming_Courier, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Transit_Type
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Transit_Type, Selected)
      Do showtitle
      Access:JOBSE.Clearkey(jobe:RefNumberKey)
      jobe:RefNumber  = job:Ref_Number
      If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Found
      Else! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          If Access:JOBSE.Primerecord() = Level:Benign
              jobe:RefNumber  = job:Ref_Number
              If Access:JOBSE.Tryinsert()
                  Access:JOBSE.Cancelautoinc()
              End!If Access:.Tryinsert()
          End!If Access:JOBSE.Primerecord() = Level:Benign
      End! If Access:JOBSE.Tryfetch(jobe:RefNumberKey) = Level:Benign
      Display(?jobe:SIMNumber)
      
!      thismakeover.setwindow(win:form)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Transit_Type, Selected)
    OF ?job:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Selected)
      !Title
      left_title_temp = 'Repair Details: - Job Number: ' & CLip(job:ref_number)
      right_title_temp = 'Date Booked: ' & Format(job:date_booked,@d6b) & ' ' & Format(job:time_booked,@t1) & |
                          ' - ' & job:who_booked
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case Keycode()
          Of F4Key
              Post(Event:Accepted,?Lookup_Accessory)
          Of F5Key
              Post(Event:Accepted,?Fault_Codes_Lookup)
          Of F6Key
              Post(Event:Accepted,?Fault_Description)
          Of F7Key
              Post(Event:Accepted,?Engineers_Notes)
          Of F8Key
              Post(Event:Accepted,?Contact_History)
          Of F9Key
              Case Choice(?Sheet2)
                  Of 1
                      Post(Event:Accepted,?Amend)
                  Of 2
                      Post(Event:Accepted,?Amend:2)
                  Of 3
                      Post(Event:Accepted,?Amend:3)
              End !Case Choice(?Sheet2)
          Of F10Key
              If ?Repair_History{prop:Hide} = 0
                  Post(Event:Accepted,?Repair_History)
              End !If ?Repair_History{prop:Hide} = 0
      End !Keycode()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

