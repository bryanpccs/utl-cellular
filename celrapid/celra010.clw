

   MEMBER('celrapid.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('CELRA010.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('CELRA012.INC'),ONCE        !Req'd for module callout resolution
                     END





Browse_Jobs PROCEDURE                                 !Generated from procedure template - Window

ThisThreadActive BYTE
CurrentTab           STRING(80)
FilesOpened          BYTE
Account_Number_temp  STRING(15)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?Account_Number_temp
sub:Account_Number     LIKE(sub:Account_Number)       !List box control field - type derived from field
sub:Company_Name       LIKE(sub:Company_Name)         !List box control field - type derived from field
sub:RecordNumber       LIKE(sub:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(JOBS)
                       PROJECT(job:Ref_Number)
                       PROJECT(job:Account_Number)
                       PROJECT(job:Model_Number)
                       PROJECT(job:ESN)
                       PROJECT(job:Order_Number)
                       PROJECT(job:Bouncer)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
job:Ref_Number         LIKE(job:Ref_Number)           !List box control field - type derived from field
job:Account_Number     LIKE(job:Account_Number)       !List box control field - type derived from field
job:Model_Number       LIKE(job:Model_Number)         !List box control field - type derived from field
job:ESN                LIKE(job:ESN)                  !List box control field - type derived from field
job:Order_Number       LIKE(job:Order_Number)         !List box control field - type derived from field
job:Bouncer            LIKE(job:Bouncer)              !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(SUBTRACC)
                       PROJECT(sub:Account_Number)
                       PROJECT(sub:Company_Name)
                       PROJECT(sub:RecordNumber)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,63)                !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse The Jobs File'),AT(,,440,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('pc.ico'),HLP('Browse_Jobs'),SYSTEM,GRAY,RESIZE,MDI
                       LIST,AT(8,36,340,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('44R(2)|M~Job Number~@s8@60L(2)|M~Account Number~@s15@120L(2)|M~Model Number~@s30' &|
   '@64L(2)|M~ESN/IMEI~@s16@120L(2)|M~Order Number~@s30@32L(2)|M~Bouncer~@s8@'),FROM(Queue:Browse:1)
                       BUTTON('Payment Details'),AT(360,40,76,20),USE(?PaymentDetails),LEFT,ICON('MONEY.GIF')
                       BUTTON('Change'),AT(360,92,76,20),USE(?Change),LEFT,ICON('edit.gif')
                       BUTTON('Print Routines'),AT(360,16,76,20),USE(?Reprint_Label),LEFT,ICON(ICON:Print1)
                       SHEET,AT(4,4,348,180),USE(?CurrentTab),SPREAD
                         TAB('By Job Number'),USE(?Tab:2)
                           ENTRY(@s8),AT(8,20,64,10),USE(job:Ref_Number),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                         END
                         TAB('By E.S.N. / I.M.E.I.'),USE(?Tab2)
                           ENTRY(@s16),AT(8,20,124,10),USE(job:ESN),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Account Number'),USE(?Tab3)
                           ENTRY(@s30),AT(8,20,124,10),USE(job:Order_Number),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           COMBO(@s15),AT(136,20,124,10),USE(Account_Number_temp),IMM,VSCROLL,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),FORMAT('67L(2)|M@s15@120L(2)|M@s30@'),DROP(10,250),FROM(Queue:FileDropCombo)
                         END
                       END
                       BUTTON('Close'),AT(360,164,76,20),USE(?Close),LEFT,ICON('Cancel.ico')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
OrderInit              PROCEDURE(<Key K>)            !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 2
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 3
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

ClarioNETWindow        ClarioNETWindowClass           !---ClarioNET 19
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore1Step1         StepLongClass   !LONG            !Xplore: Column displaying job:Ref_Number
Xplore1Locator1      IncrementalLocatorClass          !Xplore: Column displaying job:Ref_Number
Xplore1Step2         StepStringClass !STRING          !Xplore: Column displaying job:Account_Number
Xplore1Locator2      IncrementalLocatorClass          !Xplore: Column displaying job:Account_Number
Xplore1Step3         StepStringClass !STRING          !Xplore: Column displaying job:Model_Number
Xplore1Locator3      IncrementalLocatorClass          !Xplore: Column displaying job:Model_Number
Xplore1Step4         StepStringClass !STRING          !Xplore: Column displaying job:ESN
Xplore1Locator4      IncrementalLocatorClass          !Xplore: Column displaying job:ESN
Xplore1Step6         StepStringClass !STRING          !Xplore: Column displaying job:Bouncer
Xplore1Locator6      IncrementalLocatorClass          !Xplore: Column displaying job:Bouncer

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?job:Ref_Number{prop:ReadOnly} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 15066597
    Elsif ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 8454143
    Else ! If ?job:Ref_Number{prop:Req} = True
        ?job:Ref_Number{prop:FontColor} = 65793
        ?job:Ref_Number{prop:Color} = 16777215
    End ! If ?job:Ref_Number{prop:Req} = True
    ?job:Ref_Number{prop:Trn} = 0
    ?job:Ref_Number{prop:FontStyle} = font:Bold
    ?Tab2{prop:Color} = 15066597
    If ?job:ESN{prop:ReadOnly} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 15066597
    Elsif ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 8454143
    Else ! If ?job:ESN{prop:Req} = True
        ?job:ESN{prop:FontColor} = 65793
        ?job:ESN{prop:Color} = 16777215
    End ! If ?job:ESN{prop:Req} = True
    ?job:ESN{prop:Trn} = 0
    ?job:ESN{prop:FontStyle} = font:Bold
    ?Tab3{prop:Color} = 15066597
    If ?job:Order_Number{prop:ReadOnly} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 15066597
    Elsif ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 8454143
    Else ! If ?job:Order_Number{prop:Req} = True
        ?job:Order_Number{prop:FontColor} = 65793
        ?job:Order_Number{prop:Color} = 16777215
    End ! If ?job:Order_Number{prop:Req} = True
    ?job:Order_Number{prop:Trn} = 0
    ?job:Order_Number{prop:FontStyle} = font:Bold
    If ?Account_Number_temp{prop:ReadOnly} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 15066597
    Elsif ?Account_Number_temp{prop:Req} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 8454143
    Else ! If ?Account_Number_temp{prop:Req} = True
        ?Account_Number_temp{prop:FontColor} = 65793
        ?Account_Number_temp{prop:Color} = 16777215
    End ! If ?Account_Number_temp{prop:Req} = True
    ?Account_Number_temp{prop:Trn} = 0
    ?Account_Number_temp{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Jobs',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Jobs',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Jobs',1)
    SolaceViewVars('Account_Number_temp',Account_Number_temp,'Browse_Jobs',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PaymentDetails;  SolaceCtrlName = '?PaymentDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reprint_Label;  SolaceCtrlName = '?Reprint_Label';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Ref_Number;  SolaceCtrlName = '?job:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:ESN;  SolaceCtrlName = '?job:ESN';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?job:Order_Number;  SolaceCtrlName = '?job:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Account_Number_temp;  SolaceCtrlName = '?Account_Number_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Jobs','?Browse:1')        !Xplore
  BRW1.SequenceNbr = 1                                !Xplore
  BRW1.ViewOrder   = True                             !Xplore
  Xplore1.RestoreHeader = True                        !Xplore
  Xplore1.AscDesc       = 0                           !Xplore
  ClarioNET:InitWindow(ClarioNETWindow, QuickWindow, 1) !---ClarioNET 38
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Jobs')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Jobs')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.ResetOnGainFocus = 1
  SELF.AutoToolbar = 0
  SELF.AutoRefresh = 0
  SELF.AddItem(Toolbar)
  IF GlobalRequest <> SelectRecord
    ThreadQ.Proc='Browse_Jobs'
    GET(ThreadQ,ThreadQ.Proc)
    IF ERRORCODE()
      ThreadQ.ThreadNo = THREAD()
      ADD(ThreadQ,ThreadQ.Proc)
      ThisThreadActive = THREAD()
    ELSE
      POST(EVENT:GainFocus,,ThreadQ.ThreadNo)
      RETURN Level:Fatal
    END
  END
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:INVOICE.Open
  Relate:USERS_ALIAS.Open
  Relate:VATCODE.Open
  Access:TRADEACC.UseFile
  Access:JOBPAYMT_ALIAS.UseFile
  Access:JOBS_ALIAS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:JOBS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'celrapid.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  BRW1.Q &= Queue:Browse:1
  BRW1.RetainRow = 0
  BRW1.AddSortOrder(,job:ESN_Key)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?job:ESN,job:ESN,1,BRW1)
  BRW1.AddSortOrder(,job:AccOrdNoKey)
  BRW1.AddRange(job:Account_Number,Account_Number_temp)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?job:Order_Number,job:Order_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,job:Ref_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?job:Ref_Number,job:Ref_Number,1,BRW1)
  BRW1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  BRW1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  BRW1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  BRW1.AddField(job:ESN,BRW1.Q.job:ESN)
  BRW1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  BRW1.AddField(job:Bouncer,BRW1.Q.job:Bouncer)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  FDCB7.Init(Account_Number_temp,?Account_Number_temp,Queue:FileDropCombo.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo,Relate:SUBTRACC,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo
  FDCB7.AddSortOrder(sub:Account_Number_Key)
  FDCB7.AddField(sub:Account_Number,FDCB7.Q.sub:Account_Number)
  FDCB7.AddField(sub:Company_Name,FDCB7.Q.sub:Company_Name)
  FDCB7.AddField(sub:RecordNumber,FDCB7.Q.sub:RecordNumber)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  ! EIP Not supported by ClarioNET. If Active, turn it off.
  IF ClarioNETServer:Active()
    IF BRW1.AskProcedure = 0
      CLEAR(BRW1.AskProcedure, 1)
    END
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Job Number'
    ?Tab2{PROP:TEXT} = 'By E.S.N. / I.M.E.I.'
    ?Tab3{PROP:TEXT} = 'By Account Number'
    ?Browse:1{PROP:FORMAT} ='44R(2)|M~Job Number~@s8@#1#60L(2)|M~Account Number~@s15@#2#120L(2)|M~Model Number~@s30@#3#64L(2)|M~ESN/IMEI~@s16@#4#120L(2)|M~Order Number~@s30@#5#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:INVOICE.Close
    Relate:USERS_ALIAS.Close
    Relate:VATCODE.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore1.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Jobs','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  IF ThisThreadActive
    ThreadQ.Proc='Browse_Jobs'
    GET(ThreadQ,ThreadQ.Proc)
    DELETE(ThreadQ)
  END
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Jobs',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ClarioNET:OpenWindowInit(ClarioNETWindow)           !---ClarioNET 97
  ReturnValue = PARENT.Run()
  ClarioNET:KillWindow(ClarioNETWindow)               !---ClarioNET 98
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  Insert_global = ''
  do_update# = False
  
  If Request = ChangeRecord
      If SecurityCheck('JOBS - CHANGE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('JOBS - CHANGE')
          do_update# = True
      End!If SecurityCheck('JOBS - CHANGE')
  
  End!If Request = ChangeRecord
  
  If do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateJobs
    ReturnValue = GlobalResponse
  END
  End!If do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(job:Ref_Number,BRW1.Q.job:Ref_Number)
  Xplore1.AddField(job:Account_Number,BRW1.Q.job:Account_Number)
  Xplore1.AddField(job:Model_Number,BRW1.Q.job:Model_Number)
  Xplore1.AddField(job:ESN,BRW1.Q.job:ESN)
  Xplore1.AddField(job:Order_Number,BRW1.Q.job:Order_Number)
  Xplore1.AddField(job:Bouncer,BRW1.Q.job:Bouncer)
  BRW1.FileOrderNbr = BRW1.AddSortOrder()             !Xplore Sort Order for File Sequence
  BRW1.SetOrder('')                                   !Xplore
  Xplore1.AddAllColumnSortOrders(0)                   !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Reprint_Label
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Label, Accepted)
      glo:select1 = brw1.q.job:ref_number
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Label, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?PaymentDetails
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PaymentDetails, Accepted)
      If SecurityCheck('PAYMENT DETAILS')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!If SecurityCheck('PAYMENT DETAILS')
          Thiswindow.reset(1)
      
          Access:JOBS_ALIAS.Clearkey(job_ali:Ref_Number_Key)
          job_ali:Ref_Number  = brw1.q.job:Ref_Number
          If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
              !Found
              continue# = 1
              If job_ali:Warranty_Job = 'YES' and job_ali:Chargeable_Job <> 'YES'
                  continue# = 0
                  Case MessageEx('This job is marked as a Warranty Job Only.'&|
                    '<13,10>'&|
                    '<13,10>It can only be marked as paid by Reconciling the Claim.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              End!If job_ali:Warranty_Job = 'YES' and job_ali:Chargeable_Job <> 'YES'
      
              If job_ali:Date_Completed = '' And continue# = 1
                  Case MessageEx('This job has not been completed.'&|
                    '<13,10>'&|
                    '<13,10>Are you sure you want to add/amend payments?','ServiceBase 2000',|
                                 'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                      Of 2 ! &No Button
                          continue# = 0
                  End!Case MessageEx
              End!If job_ali:Date_Completed = '' And continue# = 1
      
              If continue# = 1
                  glo:Select1 = brw1.q.job:Ref_Number
                  Browse_Payments
      
                  Access:JOBS.Clearkey(job:Ref_Number_Key)
                  job:Ref_Number  = glo:Select1
                  If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Found
                      Include('ChkPaid.inc')
                  Else! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                  End! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                  glo:Select1 = ''
              End!If continue# = 1
          Else! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End! If Access:JOBS_ALIAS.Tryfetch(job_ali:Ref_Number_Key) = Level:Benign
          
      End!If SecurityCheck('PAYMENT DETAILS')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PaymentDetails, Accepted)
    OF ?Reprint_Label
      ThisWindow.Update
      Print_Routines
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Label, Accepted)
      glo:select1 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Label, Accepted)
    OF ?Account_Number_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number_temp, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Account_Number_temp, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Jobs')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  IF ClarioNET:TakeEvent(ClarioNETWindow)             !---ClarioNET 43
    BREAK
  END
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?CurrentTab
      !Extension Templete Code:XploreOOPBrowse,ControlEventHandling,?CurrentTab,'NewSelection',PRIORITY(4000)
      BRW1.ViewOrder = False                          !Xplore
      BRW1.FileSeqOn = False                          !Xplore
      Xplore1.EraseVisual()                           !Xplore
      Xplore1.SetFilters()
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='44R(2)|M~Job Number~@s8@#1#60L(2)|M~Account Number~@s15@#2#120L(2)|M~Model Number~@s30@#3#64L(2)|M~ESN/IMEI~@s16@#4#120L(2)|M~Order Number~@s30@#5#'
          ?Tab:2{PROP:TEXT} = 'By Job Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='64L(2)|M~ESN/IMEI~@s16@#4#44R(2)|M~Job Number~@s8@#1#60L(2)|M~Account Number~@s15@#2#120L(2)|M~Model Number~@s30@#3#120L(2)|M~Order Number~@s30@#5#'
          ?Tab2{PROP:TEXT} = 'By E.S.N. / I.M.E.I.'
        OF 3
          ?Browse:1{PROP:FORMAT} ='120L(2)|M~Order Number~@s30@#5#44R(2)|M~Job Number~@s8@#1#60L(2)|M~Account Number~@s15@#2#120L(2)|M~Model Number~@s30@#3#64L(2)|M~ESN/IMEI~@s16@#4#'
          ?Tab3{PROP:TEXT} = 'By Account Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?job:Ref_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Ref_Number, Selected)
    OF ?job:ESN
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:ESN, Selected)
    OF ?job:Order_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Order_Number, Selected)
      Select(?Browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?job:Order_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = Start(DebugToolbox,25000,'')
            GLO:SolaceViewVariables = true
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:CloseWindow
      ClarioNET:CloseWindow(ClarioNETWindow)          !---ClarioNET 89
    OF EVENT:OpenWindow
      ClarioNET:OpenWindow(ClarioNETWindow)           !---ClarioNET 94
    OF EVENT:GainFocus
      IF QuickWindow{PROP:Iconize}=TRUE
        QuickWindow{PROP:Iconize}=FALSE
        IF QuickWindow{PROP:Active}<>TRUE
           QuickWindow{PROP:Active}=TRUE
        END
      END
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
    OF EVENT:GainFocus
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementName'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW1.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW1.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'GetFreeElementPosition'
  IF BRW1.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.ChangeControl=?Change
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW1.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore1.sq)                    !Xplore
    Xplore1.SetupOrder(BRW1.SortOrderNbr)             !Xplore
    GET(Xplore1.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW1.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW1.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW1.FileOrderNbr,Force)      !Xplore
  END                                                 !Xplore
  IF Choice(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 3
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore1.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.OrderInit PROCEDURE(<KEY K>)
!This method is located here (in BRW1) because SELF.ORDER queue is protected in the ViewManager
  CODE
  !0{PROP:Text} = 'SortOrder=' & POINTER(SELF.ORDER) & ' order=' & SELF.ORDER.Order & ' fe=' & SELF.ORDER.FreeElement
  CLEAR(SELF.ORDER.LimitType)                         !Xplore
  FREE(SELF.ORDER.Filter)                             !Xplore
  DISPOSE(SELF.ORDER.Filter)                          !Xplore
  SELF.ORDER.Filter &= NEW FilterQueue                !Xplore
  DISPOSE(SELF.ORDER.RangeList)                       !Xplore
  SELF.Order.RangeList &= NEW BufferedPairsClass      !Xplore
  SELF.ORDER.MainKey &= K                             !Xplore
  SELF.Order.RangeList.Init                           !Xplore
  DISPOSE(SELF.Order.Order)                           !Xplore
  PUT(SELF.Order)                                     !Xplore
  RETURN                                              !Xplore
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue BYTE
PV          &PrintPreviewClass
  CODE
  !Standard Previewer
  PV           &= NEW(PrintPreviewClass)
  PV.Init(PQ)
  PV.Maximize   = True
  ReturnValue   = PV.DISPLAY(PageWidth,1,1,1)
  PV.Kill()
  DISPOSE(PV)
  RETURN ReturnValue
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !job:Ref_Number
        ColumnString     = SUB(LEFT(BRW1.Q.job:Ref_Number),1,20)
  OF 2 !job:Account_Number
  OF 3 !job:Model_Number
  OF 4 !job:ESN
  OF 5 !job:Order_Number
        ColumnString     = SUB(LEFT(BRW1.Q.job:Order_Number),1,20)
  OF 6 !job:Bouncer
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  BRW1.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore1Step6)
  SELF.FQ.SortField = SELF.BC.AddSortOrder()
  EXECUTE SortQRecord
    BEGIN
      Xplore1Step1.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore1Locator1)
      Xplore1Locator1.Init(?job:Ref_Number,job:Ref_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator2)
      Xplore1Locator2.Init(,job:Account_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator3)
      Xplore1Locator3.Init(,job:Model_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step4.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator4)
      Xplore1Locator4.Init(?job:ESN,job:ESN,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore1Step6.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore1Locator6)
      Xplore1Locator6.Init(,job:Bouncer,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
I     LONG
J     LONG
SaveQ LONG
  CODE
  SaveQ = POINTER(SELF.Fq)
  LOOP I = 1 to RECORDS(SELF.Fq)
    GET(SELF.Fq,I)
    IF NOT SELF.FQ.Sortfield
      CYCLE
    END
    LOOP J = 1 TO 2
      IF SELF.BC.SetSort(SELF.Fq.SortField + J - 1).
      IF Choice(?CurrentTab) = 2
        BRW1.OrderInit(job:ESN_Key)
        SELF.FindAnySubFields(0,J-1)
      ELSIF Choice(?CurrentTab) = 3
        BRW1.OrderInit(job:AccOrdNoKey)
        BRW1.AddRange(job:Account_Number,Account_Number_temp)
        SELF.FindAnySubFields(1,J-1)
      ELSE
        BRW1.OrderInit(job:Ref_Number_Key)
        SELF.FindAnySubFields(0,J-1)
      END !IF ...
    END !LOOP J = 1 TO 2
  END !LOOP
  GET(SELF.Fq,SaveQ)
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(JOBS)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('job:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Job Number')                 !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Job Number')                 !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Account_Number')         !Field Name
                SHORT(60)                             !Default Column Width
                PSTRING('Account Number')             !Header
                PSTRING('@s15')                       !Picture
                PSTRING('Account Number')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Model_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Model Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Model Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:ESN')                    !Field Name
                SHORT(80)                             !Default Column Width
                PSTRING('ESN/IMEI')                   !Header
                PSTRING('@s16')                       !Picture
                PSTRING('ESN/IMEI')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                PSTRING('job:Order_Number')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Order Number')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Order Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('job:Bouncer')                !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Bouncer')                    !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Bouncer')                    !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('I')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(6)
!--------------------------
XpSortFields    GROUP,STATIC
                PSTRING('job:Ref_Number')             !Column Field
                SHORT(1)                              !Nos of Sort Fields
                PSTRING('job:Ref_Number')             !Sort Field Seq:1
                BYTE(1)                               !Descending
                PSTRING('@s8')                        !Picture
                !-------------------------
                PSTRING('job:Order_Number')           !Column Field
                SHORT(0)                              !Nos of Sort Fields
                !-------------------------
                END
XpSortDim       SHORT(2)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

