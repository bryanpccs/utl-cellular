

   MEMBER('sba02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA02014.INC'),ONCE        !Local module procedure declarations
                     END


UpdateORDPARTS PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::orp:Record  LIKE(orp:RECORD),STATIC
QuickWindow          WINDOW('Update the ORDPARTS File'),AT(,,244,146),FONT('Arial',8,,),CENTER,IMM,HLP('UpdateORDPARTS'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,348,180),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Order Number'),AT(8,20),USE(?ORP:Order_Number:Prompt),TRN
                           ENTRY(@n012),AT(76,20,52,10),USE(orp:Order_Number),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Order Number')
                           PROMPT('Ref Number'),AT(8,34),USE(?ORP:Ref_Number:Prompt),TRN
                           ENTRY(@n012),AT(76,34,52,10),USE(orp:Part_Ref_Number),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Part Ref Number')
                           PROMPT('Quantity'),AT(8,48),USE(?ORP:Quantity:Prompt)
                           SPIN(@p<<<<<<<#p),AT(76,48,55,10),USE(orp:Quantity),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR,MSG('Quantity')
                           PROMPT('Purchase Cost'),AT(8,62),USE(?ORP:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(76,62,60,10),USE(orp:Purchase_Cost),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Sale Cost'),AT(8,76),USE(?ORP:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(76,76,60,10),USE(orp:Sale_Cost),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Job Number Stock'),AT(8,90),USE(?ORP:Job_Number_Stock:Prompt),TRN
                           ENTRY(@s12),AT(76,90,52,10),USE(orp:Job_Number),SKIP,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY,MSG('Job Number')
                           PROMPT('Date Received'),AT(8,104),USE(?ORP:Date_Received:Prompt),TRN
                           ENTRY(@d6b),AT(76,104,104,10),USE(orp:Date_Received),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&OK'),AT(4,122,56,16),USE(?OK),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(84,122,56,16),USE(?Cancel),FLAT,LEFT,ICON('cancel.gif')
                       BUTTON('DELETE THIS'),AT(164,122,76,20),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateORDPARTS',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateORDPARTS',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateORDPARTS',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateORDPARTS',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateORDPARTS',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateORDPARTS',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ORP:Order_Number:Prompt;  SolaceCtrlName = '?ORP:Order_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Order_Number;  SolaceCtrlName = '?orp:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ORP:Ref_Number:Prompt;  SolaceCtrlName = '?ORP:Ref_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Part_Ref_Number;  SolaceCtrlName = '?orp:Part_Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ORP:Quantity:Prompt;  SolaceCtrlName = '?ORP:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Quantity;  SolaceCtrlName = '?orp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ORP:Purchase_Cost:Prompt;  SolaceCtrlName = '?ORP:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Purchase_Cost;  SolaceCtrlName = '?orp:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ORP:Sale_Cost:Prompt;  SolaceCtrlName = '?ORP:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Sale_Cost;  SolaceCtrlName = '?orp:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ORP:Job_Number_Stock:Prompt;  SolaceCtrlName = '?ORP:Job_Number_Stock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Job_Number;  SolaceCtrlName = '?orp:Job_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ORP:Date_Received:Prompt;  SolaceCtrlName = '?ORP:Date_Received:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Date_Received;  SolaceCtrlName = '?orp:Date_Received';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting a ORDPARTS Record'
  OF ChangeRecord
    ActionMessage = 'Changing a ORDPARTS Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateORDPARTS')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateORDPARTS')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?ORP:Order_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orp:Record,History::orp:Record)
  SELF.AddHistoryField(?orp:Order_Number,1)
  SELF.AddHistoryField(?orp:Part_Ref_Number,3)
  SELF.AddHistoryField(?orp:Quantity,4)
  SELF.AddHistoryField(?orp:Purchase_Cost,7)
  SELF.AddHistoryField(?orp:Sale_Cost,8)
  SELF.AddHistoryField(?orp:Job_Number,10)
  SELF.AddHistoryField(?orp:Date_Received,13)
  SELF.AddUpdateFile(Access:ORDPARTS)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDPARTS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDPARTS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPARTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateORDPARTS',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateORDPARTS')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

