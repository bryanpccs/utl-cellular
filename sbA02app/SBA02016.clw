

   MEMBER('sba02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA02016.INC'),ONCE        !Local module procedure declarations
                     END


UpdateINVOICE PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
FilesOpened          BYTE
ActionMessage        CSTRING(40)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::inv:Record  LIKE(inv:RECORD),STATIC
QuickWindow          WINDOW('Update the INVOICE File'),AT(,,212,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateINVOICE'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,204,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Invoice Number'),AT(8,20),USE(?INV:Invoice_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<<#p),AT(80,20,44,10),USE(inv:Invoice_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           OPTION('Invoice Type'),AT(80,34,50,36),USE(inv:Invoice_Type),BOXED
                             RADIO('CHA'),AT(84,44),USE(?INV:Invoice_Type:Radio1),VALUE('CHA')
                             RADIO('WAR'),AT(84,56),USE(?INV:Invoice_Type:Radio2),VALUE('WAR')
                           END
                           PROMPT('Job Number'),AT(8,74),USE(?INV:Job_Number:Prompt),TRN
                           ENTRY(@p<<<<<<<<#p),AT(80,74,44,10),USE(inv:Job_Number),SKIP,LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Yellow,COLOR:Black,COLOR:Silver),REQ,UPR,READONLY
                           PROMPT('Date Created'),AT(8,88),USE(?INV:Date_Created:Prompt),TRN
                           ENTRY(@d6b),AT(80,88,104,10),USE(inv:Date_Created),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Account Number'),AT(8,102),USE(?INV:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(80,102,64,10),USE(inv:Account_Number),SKIP,LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Yellow,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Total'),AT(8,116),USE(?INV:Total:Prompt),TRN
                           ENTRY(@N14.2B),AT(80,116,60,10),USE(inv:Total),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White)
                           PROMPT('Labour Rate'),AT(8,130),USE(?INV:Vat_Rate_Labour:Prompt),TRN
                           ENTRY(@N14.2B),AT(80,130,60,10),USE(inv:Vat_Rate_Labour),DECIMAL(14),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Parts Rate'),AT(8,144),USE(?INV:Vat_Rate_Parts:Prompt),TRN
                           ENTRY(@N14.2B),AT(80,144,60,10),USE(inv:Vat_Rate_Parts),DECIMAL(14),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Retail Sales Rate'),AT(8,20),USE(?INV:Vat_Rate_Retail:Prompt),TRN
                           ENTRY(@n14.2B),AT(80,20,60,10),USE(inv:Vat_Rate_Retail),DECIMAL(14),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('V.A.T. Number'),AT(8,34),USE(?INV:VAT_Number:Prompt),TRN
                           ENTRY(@s30),AT(80,34,124,10),USE(inv:VAT_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('V.A.T. Number'),AT(8,48),USE(?INV:Invoice_VAT_Number:Prompt),TRN
                           ENTRY(@s30),AT(80,48,124,10),USE(inv:Invoice_VAT_Number),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Currency'),AT(8,62),USE(?INV:Currency:Prompt),TRN
                           ENTRY(@s30),AT(80,62,124,10),USE(inv:Currency),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('EDI Batch Number'),AT(8,76),USE(?INV:Batch_Number:Prompt)
                           ENTRY(@n6),AT(80,76,40,10),USE(inv:Batch_Number),UPR
                           PROMPT('Manufacturer'),AT(8,90),USE(?INV:Manufacturer:Prompt),TRN
                           ENTRY(@s30),AT(80,90,124,10),USE(inv:Manufacturer),SKIP,LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:Silver,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Claim Reference'),AT(8,104),USE(?INV:Claim_Reference:Prompt),TRN
                           ENTRY(@s30),AT(80,104,124,10),USE(inv:Claim_Reference),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Total Claimed'),AT(8,118),USE(?INV:Total_Claimed:Prompt)
                           ENTRY(@n14.2),AT(80,118,60,10),USE(inv:Total_Claimed),DECIMAL(14),MSG('r'),TIP('r')
                           PROMPT('Labour Paid:'),AT(8,132),USE(?INV:Labour_Paid:Prompt)
                           ENTRY(@n14.2),AT(80,132,60,10),USE(inv:Labour_Paid),DECIMAL(14)
                           PROMPT('Parts Paid:'),AT(8,146),USE(?INV:Parts_Paid:Prompt)
                           ENTRY(@n14.2),AT(80,146,60,10),USE(inv:Parts_Paid),DECIMAL(14)
                         END
                         TAB('General (cont. 2)'),USE(?Tab:3)
                           PROMPT('Reconciled Date:'),AT(8,20),USE(?INV:Reconciled_Date:Prompt)
                           ENTRY(@d6b),AT(80,20,104,10),USE(inv:Reconciled_Date)
                         END
                       END
                       BUTTON('OK'),AT(65,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(114,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(163,164,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateINVOICE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateINVOICE',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateINVOICE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateINVOICE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Invoice_Number:Prompt;  SolaceCtrlName = '?INV:Invoice_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Invoice_Number;  SolaceCtrlName = '?inv:Invoice_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Invoice_Type;  SolaceCtrlName = '?inv:Invoice_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Invoice_Type:Radio1;  SolaceCtrlName = '?INV:Invoice_Type:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Invoice_Type:Radio2;  SolaceCtrlName = '?INV:Invoice_Type:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Job_Number:Prompt;  SolaceCtrlName = '?INV:Job_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Job_Number;  SolaceCtrlName = '?inv:Job_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Date_Created:Prompt;  SolaceCtrlName = '?INV:Date_Created:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Date_Created;  SolaceCtrlName = '?inv:Date_Created';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Account_Number:Prompt;  SolaceCtrlName = '?INV:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Account_Number;  SolaceCtrlName = '?inv:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Total:Prompt;  SolaceCtrlName = '?INV:Total:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Total;  SolaceCtrlName = '?inv:Total';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Vat_Rate_Labour:Prompt;  SolaceCtrlName = '?INV:Vat_Rate_Labour:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Vat_Rate_Labour;  SolaceCtrlName = '?inv:Vat_Rate_Labour';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Vat_Rate_Parts:Prompt;  SolaceCtrlName = '?INV:Vat_Rate_Parts:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Vat_Rate_Parts;  SolaceCtrlName = '?inv:Vat_Rate_Parts';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Vat_Rate_Retail:Prompt;  SolaceCtrlName = '?INV:Vat_Rate_Retail:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Vat_Rate_Retail;  SolaceCtrlName = '?inv:Vat_Rate_Retail';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:VAT_Number:Prompt;  SolaceCtrlName = '?INV:VAT_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:VAT_Number;  SolaceCtrlName = '?inv:VAT_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Invoice_VAT_Number:Prompt;  SolaceCtrlName = '?INV:Invoice_VAT_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Invoice_VAT_Number;  SolaceCtrlName = '?inv:Invoice_VAT_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Currency:Prompt;  SolaceCtrlName = '?INV:Currency:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Currency;  SolaceCtrlName = '?inv:Currency';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Batch_Number:Prompt;  SolaceCtrlName = '?INV:Batch_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Batch_Number;  SolaceCtrlName = '?inv:Batch_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Manufacturer:Prompt;  SolaceCtrlName = '?INV:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Manufacturer;  SolaceCtrlName = '?inv:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Claim_Reference:Prompt;  SolaceCtrlName = '?INV:Claim_Reference:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Claim_Reference;  SolaceCtrlName = '?inv:Claim_Reference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Total_Claimed:Prompt;  SolaceCtrlName = '?INV:Total_Claimed:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Total_Claimed;  SolaceCtrlName = '?inv:Total_Claimed';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Labour_Paid:Prompt;  SolaceCtrlName = '?INV:Labour_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Labour_Paid;  SolaceCtrlName = '?inv:Labour_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Parts_Paid:Prompt;  SolaceCtrlName = '?INV:Parts_Paid:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Parts_Paid;  SolaceCtrlName = '?inv:Parts_Paid';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?INV:Reconciled_Date:Prompt;  SolaceCtrlName = '?INV:Reconciled_Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?inv:Reconciled_Date;  SolaceCtrlName = '?inv:Reconciled_Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a INVOICE Record'
  OF ChangeRecord
    ActionMessage = 'Changing a INVOICE Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateINVOICE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateINVOICE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?INV:Invoice_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(inv:Record,History::inv:Record)
  SELF.AddHistoryField(?inv:Invoice_Number,1)
  SELF.AddHistoryField(?inv:Invoice_Type,2)
  SELF.AddHistoryField(?inv:Job_Number,3)
  SELF.AddHistoryField(?inv:Date_Created,4)
  SELF.AddHistoryField(?inv:Account_Number,5)
  SELF.AddHistoryField(?inv:Total,7)
  SELF.AddHistoryField(?inv:Vat_Rate_Labour,8)
  SELF.AddHistoryField(?inv:Vat_Rate_Parts,9)
  SELF.AddHistoryField(?inv:Vat_Rate_Retail,10)
  SELF.AddHistoryField(?inv:VAT_Number,11)
  SELF.AddHistoryField(?inv:Invoice_VAT_Number,12)
  SELF.AddHistoryField(?inv:Currency,13)
  SELF.AddHistoryField(?inv:Batch_Number,14)
  SELF.AddHistoryField(?inv:Manufacturer,15)
  SELF.AddHistoryField(?inv:Claim_Reference,16)
  SELF.AddHistoryField(?inv:Total_Claimed,17)
  SELF.AddHistoryField(?inv:Labour_Paid,19)
  SELF.AddHistoryField(?inv:Parts_Paid,20)
  SELF.AddHistoryField(?inv:Reconciled_Date,21)
  SELF.AddUpdateFile(Access:INVOICE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:INVOICE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:INVOICE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:INVOICE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateINVOICE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateINVOICE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

