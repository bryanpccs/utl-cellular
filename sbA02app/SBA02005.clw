

   MEMBER('sba02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA02005.INC'),ONCE        !Local module procedure declarations
                     END


UpdateTRACHRGE PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::trc:Record  LIKE(trc:RECORD),STATIC
QuickWindow          WINDOW('Update the TRACHRGE File'),AT(,,244,146),FONT('Arial',8,,),CENTER,IMM,HLP('UpdateTRACHRGE'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,348,180),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Trade Account Number'),AT(8,20),USE(?TRC:Account_Number:Prompt),TRN
                           ENTRY(@s15),AT(92,20,64,12),USE(trc:Account_Number),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Manufacturer'),AT(8,34),USE(?TRC:Manufacturer:Prompt),TRN
                           PROMPT('Charge Type'),AT(8,48),USE(?TRC:Charge_Type:Prompt),TRN
                           ENTRY(@s30),AT(92,48,124,12),USE(trc:Charge_Type),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Unit Type'),AT(8,62),USE(?TRC:Unit_Type:Prompt),TRN
                           ENTRY(@s30),AT(92,62,124,12),USE(trc:Unit_Type),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Model Number'),AT(8,76),USE(?TRC:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(92,76,124,12),USE(trc:Model_Number),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Repair Type'),AT(8,90),USE(?TRC:Repair_Type:Prompt),TRN
                           ENTRY(@s30),AT(92,90,124,12),USE(trc:Repair_Type),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Cost'),AT(8,104),USE(?TRC:Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(92,104,60,12),USE(trc:Cost),LEFT,FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&OK'),AT(4,122,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(84,122,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateTRACHRGE',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateTRACHRGE',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateTRACHRGE',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateTRACHRGE',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateTRACHRGE',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateTRACHRGE',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRC:Account_Number:Prompt;  SolaceCtrlName = '?TRC:Account_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trc:Account_Number;  SolaceCtrlName = '?trc:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRC:Manufacturer:Prompt;  SolaceCtrlName = '?TRC:Manufacturer:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRC:Charge_Type:Prompt;  SolaceCtrlName = '?TRC:Charge_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trc:Charge_Type;  SolaceCtrlName = '?trc:Charge_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRC:Unit_Type:Prompt;  SolaceCtrlName = '?TRC:Unit_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trc:Unit_Type;  SolaceCtrlName = '?trc:Unit_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRC:Model_Number:Prompt;  SolaceCtrlName = '?TRC:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trc:Model_Number;  SolaceCtrlName = '?trc:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRC:Repair_Type:Prompt;  SolaceCtrlName = '?TRC:Repair_Type:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trc:Repair_Type;  SolaceCtrlName = '?trc:Repair_Type';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?TRC:Cost:Prompt;  SolaceCtrlName = '?TRC:Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?trc:Cost;  SolaceCtrlName = '?trc:Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateTRACHRGE')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateTRACHRGE')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?TRC:Account_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(trc:Record,History::trc:Record)
  SELF.AddHistoryField(?trc:Account_Number,1)
  SELF.AddHistoryField(?trc:Charge_Type,2)
  SELF.AddHistoryField(?trc:Unit_Type,3)
  SELF.AddHistoryField(?trc:Model_Number,4)
  SELF.AddHistoryField(?trc:Repair_Type,5)
  SELF.AddHistoryField(?trc:Cost,6)
  SELF.AddUpdateFile(Access:TRACHRGE)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:TRACHRGE.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:TRACHRGE
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:TRACHRGE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateTRACHRGE',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateTRACHRGE')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

