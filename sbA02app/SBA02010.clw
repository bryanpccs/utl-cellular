

   MEMBER('sba02app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBA02010.INC'),ONCE        !Local module procedure declarations
                     END


Update_Logged PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::log:Record  LIKE(log:RECORD),STATIC
QuickWindow          WINDOW('Update the LOGGED File'),AT(,,244,118),FONT('Arial',8,,),CENTER,IMM,HLP('Update_Logged'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,348,180),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('User Code'),AT(8,20),USE(?LOG:User_Code:Prompt),TRN
                           ENTRY(@s3),AT(61,20,40,10),USE(log:User_Code),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Surname'),AT(8,34),USE(?LOG:Surname:Prompt),TRN
                           ENTRY(@s30),AT(61,34,124,10),USE(log:Surname),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Forename'),AT(8,48),USE(?LOG:Forename:Prompt),TRN
                           ENTRY(@s30),AT(61,48,124,10),USE(log:Forename),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Date Logged'),AT(8,62),USE(?LOG:Date:Prompt),TRN
                           ENTRY(@d6b),AT(61,62,104,10),USE(log:Date),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Time Logged'),AT(8,76),USE(?LOG:Time:Prompt),TRN
                           ENTRY(@t1),AT(61,76,104,10),USE(log:Time),FONT('Arial',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       BUTTON('&OK'),AT(4,94,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(84,94,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Logged',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Logged',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Update_Logged',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Logged',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Logged',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Update_Logged',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG:User_Code:Prompt;  SolaceCtrlName = '?LOG:User_Code:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log:User_Code;  SolaceCtrlName = '?log:User_Code';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG:Surname:Prompt;  SolaceCtrlName = '?LOG:Surname:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log:Surname;  SolaceCtrlName = '?log:Surname';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG:Forename:Prompt;  SolaceCtrlName = '?LOG:Forename:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log:Forename;  SolaceCtrlName = '?log:Forename';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG:Date:Prompt;  SolaceCtrlName = '?LOG:Date:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log:Date;  SolaceCtrlName = '?log:Date';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOG:Time:Prompt;  SolaceCtrlName = '?LOG:Time:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?log:Time;  SolaceCtrlName = '?log:Time';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Logged')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Logged')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOG:User_Code:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(log:Record,History::log:Record)
  SELF.AddHistoryField(?log:User_Code,1)
  SELF.AddHistoryField(?log:Surname,3)
  SELF.AddHistoryField(?log:Forename,4)
  SELF.AddHistoryField(?log:Date,5)
  SELF.AddHistoryField(?log:Time,6)
  SELF.AddUpdateFile(Access:LOGGED)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOGGED.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOGGED
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOGGED.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Logged',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Logged')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

