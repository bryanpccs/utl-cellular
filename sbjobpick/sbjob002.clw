

   MEMBER('sbjobpick.clw')                            ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB002.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBJOB001.INC'),ONCE        !Req'd for module callout resolution
                       INCLUDE('SBJOB004.INC'),ONCE        !Req'd for module callout resolution
                     END


BrowsePick PROCEDURE                                  !Generated from procedure template - Window

CurrentTab           STRING(80)
temp_desc            STRING(255)
tempvar              LONG
BRW1::View:Browse    VIEW(PICKNOTE)
                       PROJECT(pnt:PickNoteRef)
                       PROJECT(pnt:JobReference)
                       PROJECT(pnt:EngineerCode)
                       PROJECT(pnt:Location)
                       PROJECT(pnt:IsPrinted)
                       PROJECT(pnt:PrintedBy)
                       PROJECT(pnt:PrintedDate)
                       PROJECT(pnt:PrintedTime)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
pnt:PickNoteRef        LIKE(pnt:PickNoteRef)          !List box control field - type derived from field
pnt:JobReference       LIKE(pnt:JobReference)         !List box control field - type derived from field
pnt:EngineerCode       LIKE(pnt:EngineerCode)         !List box control field - type derived from field
pnt:Location           LIKE(pnt:Location)             !List box control field - type derived from field
pnt:IsPrinted          LIKE(pnt:IsPrinted)            !List box control field - type derived from field
pnt:PrintedBy          LIKE(pnt:PrintedBy)            !List box control field - type derived from field
pnt:PrintedDate        LIKE(pnt:PrintedDate)          !List box control field - type derived from field
pnt:PrintedTime        LIKE(pnt:PrintedTime)          !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW8::View:Browse    VIEW(PICKDET)
                       PROJECT(pdt:PickDetailRef)
                       PROJECT(pdt:PartRefNumber)
                       PROJECT(pdt:Quantity)
                       PROJECT(pdt:IsChargeable)
                       PROJECT(pdt:IsDelete)
                       PROJECT(pdt:DeleteReason)
                       PROJECT(pdt:PickNoteRef)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List:2
pdt:PickDetailRef      LIKE(pdt:PickDetailRef)        !List box control field - type derived from field
pdt:PartRefNumber      LIKE(pdt:PartRefNumber)        !List box control field - type derived from field
pdt:Quantity           LIKE(pdt:Quantity)             !List box control field - type derived from field
pdt:IsChargeable       LIKE(pdt:IsChargeable)         !List box control field - type derived from field
pdt:IsDelete           LIKE(pdt:IsDelete)             !List box control field - type derived from field
pdt:DeleteReason       LIKE(pdt:DeleteReason)         !List box control field - type derived from field
pdt:PickNoteRef        LIKE(pdt:PickNoteRef)          !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse Picking Requests'),AT(,,676,400),FONT('Tahoma',8,,),IMM,HLP('BrowsePick'),SYSTEM,GRAY,RESIZE
                       LIST,AT(12,20,344,316),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('64R(2)|M~Pick Note Ref~C(0)@n-14@64R(2)|M~Job Reference~C(0)@n-14@56L(2)|M~Engin' &|
   'eer Code~@s3@80L(2)|M~Location~@s30@44R(2)|M~Is Printed~C(0)@n3@44L(2)|M~Printed' &|
   ' By~@s3@80R(2)|M~Printed Date~C(0)@d17@80R(2)|M~Printed Time~C(0)@t7@'),FROM(Queue:Browse:1)
                       PANEL,AT(4,372,668,24),USE(?Panel1)
                       SHEET,AT(4,4,360,364),USE(?CurrentTab),SPREAD
                         TAB('Unprocessed Picking Requests'),USE(?Tab:2),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                           BUTTON('Report'),AT(292,344,56,16),USE(?Button2)
                           BUTTON('Main'),AT(12,344,56,16),USE(?Button3)
                         END
                       END
                       SHEET,AT(368,4,304,364),USE(?Sheet2),SPREAD
                         TAB('Items Requested'),USE(?Tab2),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                           LIST,AT(376,27,284,157),USE(?List3),HSCROLL,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),FORMAT('80L(2)|M~recno~@s20@80L(2)|M~partnum~@s20@1020L(2)|M~desc~@s255@80L(2)|M~quantit' &|
   'y~@s20@80L(2)|M~typepart~@s20@'),FROM(compoundparts)
                           LIST,AT(376,188,284,172),USE(?List:2),IMM,MSG('Browsing Records'),FORMAT('56R|M~Pick Detail Ref~L(2)@n-14@56R|M~Part Ref Number~L(2)@n-14@56R|M~Quantity~L' &|
   '(2)@n-14@12R|M~Is Chargeable~L(2)@n3@12R|M~Is Delete~L(2)@n3@1020R|M~Delete Reas' &|
   'on~L(2)@s255@'),FROM(Queue:Browse)
                         END
                       END
                       BUTTON('Close'),AT(612,376,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
BRW8                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse                  !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597

    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List3{prop:FontColor} = 65793
    ?List3{prop:Color}= 16777215
    ?List3{prop:Color,2} = 16777215
    ?List3{prop:Color,3} = 12937777
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
GetCMDline ROUTINE
    DATA
CommandLine STRING(255)
tmpPos      LONG
    CODE
        !-----------------------------------------------
        CommandLine = CLIP(COMMAND(''))

        tmpPos = INSTRING('%', CommandLine)
        IF NOT tmpPos
           BEEP(beep:systemhand)
           message('Attempting to use sbjobpick.exe without using ServiceBase 2000' & '.|'                        & |
                'Start ServiceBase 2000 and run the program from there.','ServiceBase 2000 Error',ICON:Exclamation)

           POST(Event:CloseWindow)

           EXIT
        END !IF tmpPos
        !-----------------------------------------------
    EXIT

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('BrowsePick')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:PARTS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Access:STOCK.UseFile
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PICKNOTE,SELF)
  BRW8.Init(?List:2,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:PICKDET,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List3{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
    DO GetCMDline
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,pnt:keyonjobno)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,pnt:JobReference,1,BRW1)
  BRW1.AddField(pnt:PickNoteRef,BRW1.Q.pnt:PickNoteRef)
  BRW1.AddField(pnt:JobReference,BRW1.Q.pnt:JobReference)
  BRW1.AddField(pnt:EngineerCode,BRW1.Q.pnt:EngineerCode)
  BRW1.AddField(pnt:Location,BRW1.Q.pnt:Location)
  BRW1.AddField(pnt:IsPrinted,BRW1.Q.pnt:IsPrinted)
  BRW1.AddField(pnt:PrintedBy,BRW1.Q.pnt:PrintedBy)
  BRW1.AddField(pnt:PrintedDate,BRW1.Q.pnt:PrintedDate)
  BRW1.AddField(pnt:PrintedTime,BRW1.Q.pnt:PrintedTime)
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,pdt:keyonpicknote)
  BRW8.AddRange(pdt:PickNoteRef,pnt:PickNoteRef)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,pdt:PickNoteRef,1,BRW8)
  BRW8.AppendOrder('pdt:PickDetailRef')
  BRW8.AddField(pdt:PickDetailRef,BRW8.Q.pdt:PickDetailRef)
  BRW8.AddField(pdt:PartRefNumber,BRW8.Q.pdt:PartRefNumber)
  BRW8.AddField(pdt:Quantity,BRW8.Q.pdt:Quantity)
  BRW8.AddField(pdt:IsChargeable,BRW8.Q.pdt:IsChargeable)
  BRW8.AddField(pdt:IsDelete,BRW8.Q.pdt:IsDelete)
  BRW8.AddField(pdt:DeleteReason,BRW8.Q.pdt:DeleteReason)
  BRW8.AddField(pdt:PickNoteRef,BRW8.Q.pdt:PickNoteRef)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PARTS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Button2
      ThisWindow.Update
      PickReport
      ThisWindow.Reset
    OF ?Button3
      ThisWindow.Update
      Main
      ThisWindow.Reset
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    free(compoundparts)
    clear(compoundparts)
    
    access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
    pdt:PickNoteRef = pnt:PickNoteRef
    set(pdt:keyonpicknote,pdt:keyonpicknote)
    loop
        if access:pickdet.next() then break. !no records
        if pdt:PickNoteRef <> pnt:PickNoteRef THEN BREAK.
        !if pdt:Quantity <> 0 then
            if pdt:IsChargeable and (~pdt:IsDelete) then !its a chargeable part not deleted
                Access:parts.CLEARKEY(par:recordnumberkey)
                par:Record_Number = pdt:PartRefNumber
                if Access:parts.Fetch(par:recordnumberkey) then
                    !error
                    !compoundparts.desc = 'parts deleted'
                else
                    compoundparts.partnum = par:Part_Number
                    get(compoundparts,compoundparts.partnum)
                    if error() then !no existing
                        clear(compoundparts)
                        !compoundparts.recno = pdt:PickNoteRef
                        compoundparts.desc = par:Description
                        compoundparts.partnum = par:Part_Number
                        compoundparts.quantity = pdt:Quantity
                        compoundparts.typepart = 'chargeable'
                        add(compoundparts)
                    else
                        compoundparts.quantity += pdt:Quantity
                        put(compoundparts)
                    end
                end
            else
                if (~pdt:IsChargeable) and (~pdt:IsDelete) !Warranty parts
                    Access:warparts.CLEARKEY(wpr:recordnumberkey)
                    wpr:Record_Number = pdt:PartRefNumber
                    if Access:warparts.Fetch(wpr:recordnumberkey) then
                       !error
                        !compoundparts.desc = 'warparts deleted'
                    else
                        compoundparts.partnum = wpr:Part_Number
                        get(compoundparts,compoundparts.partnum)
                        if error() then!no existing
                            clear(compoundparts)
                            !compoundparts.recno = pdt:PickNoteRef
                            compoundparts.desc = wpr:Description
                            compoundparts.partnum = wpr:Part_Number
                            compoundparts.quantity = pdt:Quantity
                            compoundparts.typepart = 'warranty'
                            add(compoundparts)
                        else
                            !update value
                            compoundparts.quantity += pdt:Quantity
                            put(compoundparts)
                        end
                    end
                end
            end
        !end !if pdt:quantity > 0
    end !loop through pickdets
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

