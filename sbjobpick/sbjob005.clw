

   MEMBER('sbjobpick.clw')                            ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB005.INC'),ONCE        !Local module procedure declarations
                     END


Mainold PROCEDURE                                     !Generated from procedure template - Window

BRW1::View:Browse    VIEW(PICKNOTE)
                       PROJECT(pnt:PickNoteRef)
                       PROJECT(pnt:JobReference)
                       PROJECT(pnt:PickNoteNumber)
                       PROJECT(pnt:EngineerCode)
                       PROJECT(pnt:Location)
                     END
pnt:PickNoteRef      QUEUE                            !Queue declaration for browse/combo box using ?List
pnt:PickNoteRef        LIKE(pnt:PickNoteRef)          !List box control field - type derived from field
pnt:JobReference       LIKE(pnt:JobReference)         !List box control field - type derived from field
pnt:PickNoteNumber     LIKE(pnt:PickNoteNumber)       !List box control field - type derived from field
pnt:EngineerCode       LIKE(pnt:EngineerCode)         !List box control field - type derived from field
pnt:Location           LIKE(pnt:Location)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Caption'),AT(,,398,288),GRAY,DOUBLE
                       STRING('Jobs with Picking Notes'),AT(18,14),USE(?String1),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       LIST,AT(14,30,114,10),USE(?List1)
                       CHECK('Check 1'),AT(324,28),USE(glo:ErrorText),FONT('Arial',8,,FONT:regular,CHARSET:ANSI)
                       LIST,AT(12,48,376,200),USE(?List),IMM,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),MSG('Browsing Records'),FORMAT('56R|M~Pick Note Ref~L(2)@n-14@56R|M~Job Reference~L(2)@n-14@56R|M~Pick Note Numb' &|
   'er~L(2)@n-14@12R|M~Engineer Code~L(2)@s3@120R|M~Location~L(2)@s30@'),FROM(pnt:PickNoteRef)
                       PANEL,AT(4,260,392,24),USE(?Panel2)
                       BUTTON('&OK'),AT(276,264,56,16),USE(?OK),LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),ICON('OK.gif')
                       BUTTON('Cancel'),AT(336,264,56,16),USE(?Cancel),LEFT,FONT('Arial',8,,FONT:regular,CHARSET:ANSI),ICON('Cancel.gif')
                       PANEL,AT(4,4,392,252),USE(?Panel1)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?List
Q                      &pnt:PickNoteRef               !Reference to browse queue
TakeNewSelection       PROCEDURE(),DERIVED
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?List1{prop:FontColor} = 65793
    ?List1{prop:Color}= 16777215
    ?List1{prop:Color,2} = 16777215
    ?List1{prop:Color,3} = 12937777
    ?glo:ErrorText{prop:Font,3} = -1
    ?glo:ErrorText{prop:Color} = 15066597
    ?glo:ErrorText{prop:Trn} = 0
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel2{prop:Fill} = 15066597

    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Mainold')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:PICKNOTE.Open
  SELF.FilesOpened = True
  BRW1.Init(?List,pnt:PickNoteRef.ViewPosition,BRW1::View:Browse,pnt:PickNoteRef,Relate:PICKNOTE,SELF)
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?List1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  BRW1.Q &= pnt:PickNoteRef
  BRW1.AddSortOrder(,)
  BRW1.AddField(pnt:PickNoteRef,BRW1.Q.pnt:PickNoteRef)
  BRW1.AddField(pnt:JobReference,BRW1.Q.pnt:JobReference)
  BRW1.AddField(pnt:PickNoteNumber,BRW1.Q.pnt:PickNoteNumber)
  BRW1.AddField(pnt:EngineerCode,BRW1.Q.pnt:EngineerCode)
  BRW1.AddField(pnt:Location,BRW1.Q.pnt:Location)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PICKNOTE.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

