

   MEMBER('sbjobpick.clw')                            ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB003.INC'),ONCE        !Local module procedure declarations
                     END


UpdatePICKDET PROCEDURE                               !Generated from procedure template - Window

CurrentTab           STRING(80)
ActionMessage        CSTRING(40)
History::pdt:Record  LIKE(pdt:RECORD),STATIC
QuickWindow          WINDOW('Update the PICKDET File'),AT(,,358,182),FONT('MS Sans Serif',8,,),IMM,HLP('UpdatePICKDET'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,350,156),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Pick Detail Ref:'),AT(8,20),USE(?pdt:PickDetailRef:Prompt)
                           ENTRY(@n-14),AT(76,20,64,10),USE(pdt:PickDetailRef),RIGHT(1)
                           PROMPT('Pick Note Ref:'),AT(8,34),USE(?pdt:PickNoteRef:Prompt)
                           ENTRY(@n-14),AT(76,34,64,10),USE(pdt:PickNoteRef),RIGHT(1)
                           PROMPT('Part Ref Number:'),AT(8,48),USE(?pdt:PartRefNumber:Prompt)
                           ENTRY(@n-14),AT(76,48,64,10),USE(pdt:PartRefNumber),RIGHT(1)
                           PROMPT('Quantity:'),AT(8,62),USE(?pdt:Quantity:Prompt)
                           ENTRY(@n-14),AT(76,62,64,10),USE(pdt:Quantity),RIGHT(1)
                           PROMPT('Is Chargeable:'),AT(8,76),USE(?pdt:IsChargeable:Prompt)
                           ENTRY(@n3),AT(76,76,40,10),USE(pdt:IsChargeable)
                           PROMPT('Request Date:'),AT(8,90),USE(?pdt:RequestDate:Prompt)
                           ENTRY(@d17),AT(76,90,104,10),USE(pdt:RequestDate)
                           PROMPT('Request Time:'),AT(8,104),USE(?pdt:RequestTime:Prompt)
                           ENTRY(@t7),AT(76,104,104,10),USE(pdt:RequestTime)
                           PROMPT('Engineer Code:'),AT(8,118),USE(?pdt:EngineerCode:Prompt)
                           ENTRY(@s3),AT(76,118,40,10),USE(pdt:EngineerCode)
                           PROMPT('Is In Stock:'),AT(8,132),USE(?pdt:IsInStock:Prompt)
                           ENTRY(@n3),AT(76,132,40,10),USE(pdt:IsInStock)
                           PROMPT('Is Printed:'),AT(8,146),USE(?pdt:IsPrinted:Prompt)
                           ENTRY(@n3),AT(76,146,40,10),USE(pdt:IsPrinted)
                         END
                         TAB('General (cont.)'),USE(?Tab:2)
                           PROMPT('Is Delete:'),AT(8,20),USE(?pdt:IsDelete:Prompt)
                           ENTRY(@n3),AT(76,20,40,10),USE(pdt:IsDelete)
                           PROMPT('Delete Reason:'),AT(8,34),USE(?pdt:DeleteReason:Prompt)
                           ENTRY(@s255),AT(76,34,274,10),USE(pdt:DeleteReason)
                           PROMPT('Usercode:'),AT(8,48),USE(?pdt:Usercode:Prompt)
                           ENTRY(@s3),AT(76,48,40,10),USE(pdt:Usercode)
                         END
                       END
                       BUTTON('OK'),AT(211,164,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(260,164,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(309,164,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?pdt:PickDetailRef:Prompt{prop:FontColor} = -1
    ?pdt:PickDetailRef:Prompt{prop:Color} = 15066597
    If ?pdt:PickDetailRef{prop:ReadOnly} = True
        ?pdt:PickDetailRef{prop:FontColor} = 65793
        ?pdt:PickDetailRef{prop:Color} = 15066597
    Elsif ?pdt:PickDetailRef{prop:Req} = True
        ?pdt:PickDetailRef{prop:FontColor} = 65793
        ?pdt:PickDetailRef{prop:Color} = 8454143
    Else ! If ?pdt:PickDetailRef{prop:Req} = True
        ?pdt:PickDetailRef{prop:FontColor} = 65793
        ?pdt:PickDetailRef{prop:Color} = 16777215
    End ! If ?pdt:PickDetailRef{prop:Req} = True
    ?pdt:PickDetailRef{prop:Trn} = 0
    ?pdt:PickDetailRef{prop:FontStyle} = font:Bold
    ?pdt:PickNoteRef:Prompt{prop:FontColor} = -1
    ?pdt:PickNoteRef:Prompt{prop:Color} = 15066597
    If ?pdt:PickNoteRef{prop:ReadOnly} = True
        ?pdt:PickNoteRef{prop:FontColor} = 65793
        ?pdt:PickNoteRef{prop:Color} = 15066597
    Elsif ?pdt:PickNoteRef{prop:Req} = True
        ?pdt:PickNoteRef{prop:FontColor} = 65793
        ?pdt:PickNoteRef{prop:Color} = 8454143
    Else ! If ?pdt:PickNoteRef{prop:Req} = True
        ?pdt:PickNoteRef{prop:FontColor} = 65793
        ?pdt:PickNoteRef{prop:Color} = 16777215
    End ! If ?pdt:PickNoteRef{prop:Req} = True
    ?pdt:PickNoteRef{prop:Trn} = 0
    ?pdt:PickNoteRef{prop:FontStyle} = font:Bold
    ?pdt:PartRefNumber:Prompt{prop:FontColor} = -1
    ?pdt:PartRefNumber:Prompt{prop:Color} = 15066597
    If ?pdt:PartRefNumber{prop:ReadOnly} = True
        ?pdt:PartRefNumber{prop:FontColor} = 65793
        ?pdt:PartRefNumber{prop:Color} = 15066597
    Elsif ?pdt:PartRefNumber{prop:Req} = True
        ?pdt:PartRefNumber{prop:FontColor} = 65793
        ?pdt:PartRefNumber{prop:Color} = 8454143
    Else ! If ?pdt:PartRefNumber{prop:Req} = True
        ?pdt:PartRefNumber{prop:FontColor} = 65793
        ?pdt:PartRefNumber{prop:Color} = 16777215
    End ! If ?pdt:PartRefNumber{prop:Req} = True
    ?pdt:PartRefNumber{prop:Trn} = 0
    ?pdt:PartRefNumber{prop:FontStyle} = font:Bold
    ?pdt:Quantity:Prompt{prop:FontColor} = -1
    ?pdt:Quantity:Prompt{prop:Color} = 15066597
    If ?pdt:Quantity{prop:ReadOnly} = True
        ?pdt:Quantity{prop:FontColor} = 65793
        ?pdt:Quantity{prop:Color} = 15066597
    Elsif ?pdt:Quantity{prop:Req} = True
        ?pdt:Quantity{prop:FontColor} = 65793
        ?pdt:Quantity{prop:Color} = 8454143
    Else ! If ?pdt:Quantity{prop:Req} = True
        ?pdt:Quantity{prop:FontColor} = 65793
        ?pdt:Quantity{prop:Color} = 16777215
    End ! If ?pdt:Quantity{prop:Req} = True
    ?pdt:Quantity{prop:Trn} = 0
    ?pdt:Quantity{prop:FontStyle} = font:Bold
    ?pdt:IsChargeable:Prompt{prop:FontColor} = -1
    ?pdt:IsChargeable:Prompt{prop:Color} = 15066597
    If ?pdt:IsChargeable{prop:ReadOnly} = True
        ?pdt:IsChargeable{prop:FontColor} = 65793
        ?pdt:IsChargeable{prop:Color} = 15066597
    Elsif ?pdt:IsChargeable{prop:Req} = True
        ?pdt:IsChargeable{prop:FontColor} = 65793
        ?pdt:IsChargeable{prop:Color} = 8454143
    Else ! If ?pdt:IsChargeable{prop:Req} = True
        ?pdt:IsChargeable{prop:FontColor} = 65793
        ?pdt:IsChargeable{prop:Color} = 16777215
    End ! If ?pdt:IsChargeable{prop:Req} = True
    ?pdt:IsChargeable{prop:Trn} = 0
    ?pdt:IsChargeable{prop:FontStyle} = font:Bold
    ?pdt:RequestDate:Prompt{prop:FontColor} = -1
    ?pdt:RequestDate:Prompt{prop:Color} = 15066597
    If ?pdt:RequestDate{prop:ReadOnly} = True
        ?pdt:RequestDate{prop:FontColor} = 65793
        ?pdt:RequestDate{prop:Color} = 15066597
    Elsif ?pdt:RequestDate{prop:Req} = True
        ?pdt:RequestDate{prop:FontColor} = 65793
        ?pdt:RequestDate{prop:Color} = 8454143
    Else ! If ?pdt:RequestDate{prop:Req} = True
        ?pdt:RequestDate{prop:FontColor} = 65793
        ?pdt:RequestDate{prop:Color} = 16777215
    End ! If ?pdt:RequestDate{prop:Req} = True
    ?pdt:RequestDate{prop:Trn} = 0
    ?pdt:RequestDate{prop:FontStyle} = font:Bold
    ?pdt:RequestTime:Prompt{prop:FontColor} = -1
    ?pdt:RequestTime:Prompt{prop:Color} = 15066597
    If ?pdt:RequestTime{prop:ReadOnly} = True
        ?pdt:RequestTime{prop:FontColor} = 65793
        ?pdt:RequestTime{prop:Color} = 15066597
    Elsif ?pdt:RequestTime{prop:Req} = True
        ?pdt:RequestTime{prop:FontColor} = 65793
        ?pdt:RequestTime{prop:Color} = 8454143
    Else ! If ?pdt:RequestTime{prop:Req} = True
        ?pdt:RequestTime{prop:FontColor} = 65793
        ?pdt:RequestTime{prop:Color} = 16777215
    End ! If ?pdt:RequestTime{prop:Req} = True
    ?pdt:RequestTime{prop:Trn} = 0
    ?pdt:RequestTime{prop:FontStyle} = font:Bold
    ?pdt:EngineerCode:Prompt{prop:FontColor} = -1
    ?pdt:EngineerCode:Prompt{prop:Color} = 15066597
    If ?pdt:EngineerCode{prop:ReadOnly} = True
        ?pdt:EngineerCode{prop:FontColor} = 65793
        ?pdt:EngineerCode{prop:Color} = 15066597
    Elsif ?pdt:EngineerCode{prop:Req} = True
        ?pdt:EngineerCode{prop:FontColor} = 65793
        ?pdt:EngineerCode{prop:Color} = 8454143
    Else ! If ?pdt:EngineerCode{prop:Req} = True
        ?pdt:EngineerCode{prop:FontColor} = 65793
        ?pdt:EngineerCode{prop:Color} = 16777215
    End ! If ?pdt:EngineerCode{prop:Req} = True
    ?pdt:EngineerCode{prop:Trn} = 0
    ?pdt:EngineerCode{prop:FontStyle} = font:Bold
    ?pdt:IsInStock:Prompt{prop:FontColor} = -1
    ?pdt:IsInStock:Prompt{prop:Color} = 15066597
    If ?pdt:IsInStock{prop:ReadOnly} = True
        ?pdt:IsInStock{prop:FontColor} = 65793
        ?pdt:IsInStock{prop:Color} = 15066597
    Elsif ?pdt:IsInStock{prop:Req} = True
        ?pdt:IsInStock{prop:FontColor} = 65793
        ?pdt:IsInStock{prop:Color} = 8454143
    Else ! If ?pdt:IsInStock{prop:Req} = True
        ?pdt:IsInStock{prop:FontColor} = 65793
        ?pdt:IsInStock{prop:Color} = 16777215
    End ! If ?pdt:IsInStock{prop:Req} = True
    ?pdt:IsInStock{prop:Trn} = 0
    ?pdt:IsInStock{prop:FontStyle} = font:Bold
    ?pdt:IsPrinted:Prompt{prop:FontColor} = -1
    ?pdt:IsPrinted:Prompt{prop:Color} = 15066597
    If ?pdt:IsPrinted{prop:ReadOnly} = True
        ?pdt:IsPrinted{prop:FontColor} = 65793
        ?pdt:IsPrinted{prop:Color} = 15066597
    Elsif ?pdt:IsPrinted{prop:Req} = True
        ?pdt:IsPrinted{prop:FontColor} = 65793
        ?pdt:IsPrinted{prop:Color} = 8454143
    Else ! If ?pdt:IsPrinted{prop:Req} = True
        ?pdt:IsPrinted{prop:FontColor} = 65793
        ?pdt:IsPrinted{prop:Color} = 16777215
    End ! If ?pdt:IsPrinted{prop:Req} = True
    ?pdt:IsPrinted{prop:Trn} = 0
    ?pdt:IsPrinted{prop:FontStyle} = font:Bold
    ?Tab:2{prop:Color} = 15066597
    ?pdt:IsDelete:Prompt{prop:FontColor} = -1
    ?pdt:IsDelete:Prompt{prop:Color} = 15066597
    If ?pdt:IsDelete{prop:ReadOnly} = True
        ?pdt:IsDelete{prop:FontColor} = 65793
        ?pdt:IsDelete{prop:Color} = 15066597
    Elsif ?pdt:IsDelete{prop:Req} = True
        ?pdt:IsDelete{prop:FontColor} = 65793
        ?pdt:IsDelete{prop:Color} = 8454143
    Else ! If ?pdt:IsDelete{prop:Req} = True
        ?pdt:IsDelete{prop:FontColor} = 65793
        ?pdt:IsDelete{prop:Color} = 16777215
    End ! If ?pdt:IsDelete{prop:Req} = True
    ?pdt:IsDelete{prop:Trn} = 0
    ?pdt:IsDelete{prop:FontStyle} = font:Bold
    ?pdt:DeleteReason:Prompt{prop:FontColor} = -1
    ?pdt:DeleteReason:Prompt{prop:Color} = 15066597
    If ?pdt:DeleteReason{prop:ReadOnly} = True
        ?pdt:DeleteReason{prop:FontColor} = 65793
        ?pdt:DeleteReason{prop:Color} = 15066597
    Elsif ?pdt:DeleteReason{prop:Req} = True
        ?pdt:DeleteReason{prop:FontColor} = 65793
        ?pdt:DeleteReason{prop:Color} = 8454143
    Else ! If ?pdt:DeleteReason{prop:Req} = True
        ?pdt:DeleteReason{prop:FontColor} = 65793
        ?pdt:DeleteReason{prop:Color} = 16777215
    End ! If ?pdt:DeleteReason{prop:Req} = True
    ?pdt:DeleteReason{prop:Trn} = 0
    ?pdt:DeleteReason{prop:FontStyle} = font:Bold
    ?pdt:Usercode:Prompt{prop:FontColor} = -1
    ?pdt:Usercode:Prompt{prop:Color} = 15066597
    If ?pdt:Usercode{prop:ReadOnly} = True
        ?pdt:Usercode{prop:FontColor} = 65793
        ?pdt:Usercode{prop:Color} = 15066597
    Elsif ?pdt:Usercode{prop:Req} = True
        ?pdt:Usercode{prop:FontColor} = 65793
        ?pdt:Usercode{prop:Color} = 8454143
    Else ! If ?pdt:Usercode{prop:Req} = True
        ?pdt:Usercode{prop:FontColor} = 65793
        ?pdt:Usercode{prop:Color} = 16777215
    End ! If ?pdt:Usercode{prop:Req} = True
    ?pdt:Usercode{prop:Trn} = 0
    ?pdt:Usercode{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Adding a PICKDET Record'
  OF ChangeRecord
    ActionMessage = 'Changing a PICKDET Record'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdatePICKDET')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?pdt:PickDetailRef:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(pdt:Record,History::pdt:Record)
  SELF.AddHistoryField(?pdt:PickDetailRef,1)
  SELF.AddHistoryField(?pdt:PickNoteRef,2)
  SELF.AddHistoryField(?pdt:PartRefNumber,3)
  SELF.AddHistoryField(?pdt:Quantity,4)
  SELF.AddHistoryField(?pdt:IsChargeable,5)
  SELF.AddHistoryField(?pdt:RequestDate,6)
  SELF.AddHistoryField(?pdt:RequestTime,7)
  SELF.AddHistoryField(?pdt:EngineerCode,8)
  SELF.AddHistoryField(?pdt:IsInStock,9)
  SELF.AddHistoryField(?pdt:IsPrinted,10)
  SELF.AddHistoryField(?pdt:IsDelete,11)
  SELF.AddHistoryField(?pdt:DeleteReason,12)
  SELF.AddHistoryField(?pdt:Usercode,13)
  SELF.AddUpdateFile(Access:PICKDET)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:PICKDET.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:PICKDET
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PICKDET.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

