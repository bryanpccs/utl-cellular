

   MEMBER('sbjobpick.clw')                            ! This is a MEMBER module

                     MAP
                       INCLUDE('SBJOB001.INC'),ONCE        !Local module procedure declarations
                     END








PickReport PROCEDURE
RejectRecord         LONG,AUTO
LocalRequest         LONG,AUTO
LocalResponse        LONG,AUTO
FilesOpened          LONG
WindowOpened         LONG
RecordsToProcess     LONG,AUTO
RecordsProcessed     LONG,AUTO
RecordsPerCycle      LONG,AUTO
RecordsThisCycle     LONG,AUTO
PercentProgress      BYTE
RecordStatus         BYTE,AUTO
EndOfReport          BYTE,AUTO
ReportRunDate        LONG,AUTO
ReportRunTime        LONG,AUTO
ReportPageNo         SHORT,AUTO
FileOpensReached     BYTE
PartialPreviewReq    BYTE
DisplayProgress      BYTE
InitialPath          CSTRING(128)
Progress:Thermometer BYTE
IniFileToUse         STRING(64)
!-----------------------------------------------------------------------------
Process:View         VIEW(PICKNOTE)
                       PROJECT(pnt:EngineerCode)
                       PROJECT(pnt:JobReference)
                       PROJECT(pnt:Location)
                       PROJECT(pnt:PickNoteRef)
                       PROJECT(pnt:Usercode)
                     END
report               REPORT,AT(1000,2080,6000,6917),PRE(RPT),FONT('Arial',10,,),THOUS
                       HEADER,AT(1000,1000,6000,1104),USE(?unnamed)
                         STRING('Picking Note for job:'),AT(3177,52,1406,208),USE(?unnamed:4),LEFT,FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(4635,573,1302,208),USE(pnt:Usercode)
                         STRING('Engineer Code :'),AT(250,594),USE(?String10),TRN
                         STRING('User Code :'),AT(3844,604),USE(?String11),TRN
                         TEXT,AT(1250,313,1302,208),USE(pnt:PickNoteRef)
                         TEXT,AT(1250,573,1302,208),USE(pnt:EngineerCode)
                         TEXT,AT(4635,52,1302,208),USE(pnt:JobReference),FONT('Arial',10,,FONT:bold,CHARSET:ANSI)
                         TEXT,AT(4635,313,1302,208),USE(pnt:Location)
                         STRING('Pick note ref. no. :'),AT(104,313),USE(?String9),TRN
                         STRING('Location :'),AT(3958,313),USE(?String8),TRN
                         STRING('Type'),AT(52,854),USE(?String5),TRN
                         STRING('Product Code'),AT(990,854),USE(?String4),TRN
                         STRING('Description'),AT(2344,854),USE(?String6),TRN
                         LINE,AT(52,1042,5885,0),USE(?Line1),COLOR(COLOR:Black)
                         STRING('Quantity'),AT(4771,833),USE(?String7),TRN
                       END
detail1                DETAIL,AT(,,,260),USE(?unnamed:3)
                         TEXT,AT(4792,52,365,208),USE(quantity),FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         TEXT,AT(2344,52,2396,208),USE(desc),FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         TEXT,AT(990,52,1302,208),USE(partnum),FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                         TEXT,AT(52,52,885,208),USE(typepart),FONT('Arial',10,,FONT:regular,CHARSET:ANSI)
                       END
                       FOOTER,AT(1000,9000,6000,219),USE(?unnamed:2)
                         STRING(@pPage <<<#p),AT(5250,30,700,135),PAGENO,USE(?PageCount),FONT('Arial',8,,FONT:regular)
                       END
                     END
ProgressWindow WINDOW('Progress...'),AT(,,162,64),FONT('MS Sans Serif',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(Progress:Thermometer),AT(25,15,111,12),RANGE(0,100),HIDE
       STRING(' '),AT(71,15,21,17),FONT('Arial',18,,FONT:bold),USE(?Spinner:Ctl),CENTER,HIDE
       STRING(''),AT(0,3,161,10),USE(?Progress:UserString),CENTER
       STRING(''),AT(0,30,161,10),USE(?Progress:PctText),TRN,CENTER
       BUTTON('Cancel'),AT(55,42,50,15),USE(?Progress:Cancel)
     END

PrintSkipDetails        BOOL,AUTO

PrintPreviewQueue       QUEUE,PRE
PrintPreviewImage         STRING(128)
                        END


PreviewReq              BYTE(0)
ReportWasOpened         BYTE(0)
PROPPRINT:Landscape     EQUATE(07B1FH)
PreviewOptions          BYTE(0)
Wmf2AsciiName           STRING(64)
AsciiLineOption         LONG(0)
EmailOutputReq          BYTE(0)
AsciiOutputReq          BYTE(0)
CPCSPgOfPgOption        BYTE(0)
CPCSPageScanOption      BYTE(0)
CPCSPageScanPageNo      LONG(0)
SAV::Device             STRING(64)
PSAV::Copies            SHORT
CollateCopies           REAL
CancelRequested         BYTE





! CPCS Template version  v5.50
! CW Template version    v5.5
! CW Version             5507
! CW Template Family     ABC

  CODE
  PUSHBIND
  GlobalErrors.SetProcedureName('PickReport')
  DO GetDialogStrings
  InitialPath = PATH()
  FileOpensReached = False
  PRINTER{PROPPRINT:Copies} = 1
  SETCURSOR
  CASE MESSAGE(CPCS:AskPrvwDlgText,CPCS:AskPrvwDlgTitle,ICON:Question,BUTTON:Yes+BUTTON:No+BUTTON:Cancel,BUTTON:Yes)
    OF BUTTON:Yes
      PreviewReq = True
    OF BUTTON:No
      PreviewReq = False
    OF BUTTON:Cancel
       DO ProcedureReturn
  END
  LocalRequest = GlobalRequest
  LocalResponse = RequestCancelled
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  FileOpensReached = True
  FilesOpened = True
  Relate:PICKNOTE.Open
  
  
  RecordsToProcess = BYTES(PICKNOTE)
  RecordsPerCycle = 25
  RecordsProcessed = 0
  PercentProgress = 0
  LOOP WHILE KEYBOARD(); ASK.
  SETKEYCODE(0)
  OPEN(ProgressWindow)
  UNHIDE(?Progress:Thermometer)
  Progress:Thermometer = 0
  ?Progress:PctText{Prop:Text} = CPCS:ProgWinPctText
  IF PreviewReq
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrvw
  ELSE
    ProgressWindow{Prop:Text} = CPCS:ProgWinTitlePrnt
  END
  ?Progress:UserString{Prop:Text}=CPCS:ProgWinUsrText
  SEND(PICKNOTE,'QUICKSCAN=on')
  ReportRunDate = TODAY()
  ReportRunTime = CLOCK()
!  LOOP WHILE KEYBOARD(); ASK.
!  SETKEYCODE(0)
  ACCEPT
    IF KEYCODE()=ESCKEY
      SETKEYCODE(0)
      POST(EVENT:Accepted,?Progress:Cancel)
      CYCLE
    END
    CASE EVENT()
    OF Event:CloseWindow
      IF LocalResponse = RequestCancelled OR PartialPreviewReq = True
      END

    OF Event:OpenWindow
      DO OpenReportRoutine
    OF Event:Timer
        free(compoundparts)
        clear(compoundparts)
        
        access:PICKDET.clearkey(pdt:keyonpicknote) !check for existing picking parts record
        pdt:PickNoteRef = pnt:PickNoteRef
        set(pdt:keyonpicknote,pdt:keyonpicknote)
        loop
            if access:pickdet.next() then break. !no records
            if pdt:PickNoteRef <> pnt:PickNoteRef THEN BREAK.
            !if pdt:Quantity <> 0 then
                if pdt:IsChargeable and (~pdt:IsDelete) then !its a chargeable part not deleted
                    Access:parts.CLEARKEY(par:recordnumberkey)
                    par:Record_Number = pdt:PartRefNumber
                    if Access:parts.Fetch(par:recordnumberkey) then
                        !error
                        !compoundparts.desc = 'parts deleted'
                    else
                        compoundparts.partnum = par:Part_Number
                        get(compoundparts,compoundparts.partnum)
                        if error() then !no existing
                            clear(compoundparts)
                            !compoundparts.recno = pdt:PickNoteRef
                            compoundparts.desc = par:Description
                            compoundparts.partnum = par:Part_Number
                            compoundparts.quantity = pdt:Quantity
                            compoundparts.typepart = 'chargeable'
                            add(compoundparts)
                        else
                            !update value
                            compoundparts.quantity += pdt:Quantity
                            put(compoundparts)
                        end
                        print (rpt:detail1)
                    end
                else
                    if (~pdt:IsChargeable) and (~pdt:IsDelete) !Warranty parts
                        Access:warparts.CLEARKEY(wpr:recordnumberkey)
                        wpr:Record_Number = pdt:PartRefNumber
                        if Access:warparts.Fetch(wpr:recordnumberkey) then
                           !error
                            !compoundparts.desc = 'warparts deleted'
                        else
                            compoundparts.partnum = wpr:Part_Number
                            get(compoundparts,compoundparts.partnum)
                            if error() then!no existing
                                clear(compoundparts)
                                !compoundparts.recno = pdt:PickNoteRef
                                compoundparts.desc = wpr:Description
                                compoundparts.partnum = wpr:Part_Number
                                compoundparts.quantity = pdt:Quantity
                                compoundparts.typepart = 'warranty'
                                add(compoundparts)
                            else
                                !update value
                                compoundparts.quantity += pdt:Quantity
                                put(compoundparts)
                            end
                            print (rpt:detail1)
                        end
                    end
                end
            !end !if pdt:quantity > 0
        end !loop through pickdets
        
        LocalResponse = RequestCompleted
        Break
    ELSE
    END
    CASE FIELD()
    OF ?Progress:Cancel
      CASE Event()
      OF Event:Accepted
        CASE MESSAGE(CPCS:AreYouSureText,CPCS:AreYouSureTitle,ICON:Question,BUTTON:No+BUTTON:Yes,BUTTON:No)
          OF BUTTON:No
            CYCLE
        END
        CancelRequested = True
        RecordsPerCycle = 1
        IF PreviewReq = False OR ~RECORDS(PrintPreviewQueue)
          LocalResponse = RequestCancelled
          ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
        ELSE
          CASE MESSAGE(CPCS:PrvwPartialText,CPCS:PrvwPartialTitle,ICON:Question,BUTTON:No+BUTTON:Yes+BUTTON:Ignore,BUTTON:No)
            OF BUTTON:No
              LocalResponse = RequestCancelled
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
            OF BUTTON:Ignore
              CancelRequested = False
              CYCLE
            OF BUTTON:Yes
              LocalResponse = RequestCompleted
              PartialPreviewReq = True
              ProgressWindow{PROP:Timer}=0; POST(EVENT:CloseWindow)
          END
        END
      END
    END
  END
  IF SEND(PICKNOTE,'QUICKSCAN=off').
  IF PreviewReq
    ProgressWindow{PROP:HIDE}=True
    IF (LocalResponse = RequestCompleted AND KEYCODE()<>EscKey) OR PartialPreviewReq=True
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        report{PROPPRINT:COPIES}=CollateCopies
        INIFileToUse = 'CPCSRPTS.INI'
        GlobalResponse = PrintPreview(PrintPreviewQueue,100,CLIP(INIFileToUse),report,PreviewOptions,,,'','')
        IF GlobalResponse = RequestCompleted
          PSAV::Copies = PRINTER{PROPPRINT:Copies}
          PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
          report{PROP:FlushPreview} = True
          PRINTER{PROPPRINT:Copies} = PSAV::Copies
        END
      END
      LocalResponse = GlobalResponse
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrvwText,CPCS:NthgToPrvwTitle,ICON:Exclamation)
    END
  ELSIF KEYCODE()<>EscKey
    IF (ReportWasOpened AND report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR |
       (ReportWasOpened AND (AsciiOutputReq OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True OR EmailOutputReq))
      ENDPAGE(report)
      IF ~RECORDS(PrintPreviewQueue)
        MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
      ELSE
        IF CPCSPgOfPgOption = True
          AssgnPgOfPg(PrintPreviewQueue)
        END
        CollateCopies = PRINTER{PROPPRINT:COPIES}
        PRINTER{PROPPRINT:Copies} = report{PROPPRINT:Copies}
        IF report{PROPPRINT:COLLATE}=True
          HandleCopies(PrintPreviewQueue,CollateCopies)
        ELSE
          HandleCopies(PrintPreviewQueue,report{PROPPRINT:Copies})
        END
        report{PROP:FlushPreview} = True
      END
    ELSIF ~ReportWasOpened
      MESSAGE(CPCS:NthgToPrntText,CPCS:NthgToPrntTitle,ICON:Exclamation)
    END
  ELSE
    FREE(PrintPreviewQueue)
  END
  CLOSE(report)
  FREE(PrintPreviewQueue)
  
  DO ProcedureReturn

ProcedureReturn ROUTINE
  SETCURSOR
  IF FileOpensReached
    Relate:PICKNOTE.Close
  END
  IF LocalResponse
    GlobalResponse = LocalResponse
  ELSE
    GlobalResponse = RequestCancelled
  END
  GlobalErrors.SetProcedureName()
  POPBIND
  IF UPPER(CLIP(InitialPath)) <> UPPER(CLIP(PATH()))
    SETPATH(InitialPath)
  END
  RETURN


GetDialogStrings       ROUTINE
  INIFileToUse = 'CPCSRPTS.INI'
  FillCpcsIniStrings(INIFileToUse,'','Preview','Do you wish to PREVIEW this Report?')



DisplayProgress  ROUTINE
  RecordsProcessed += 1
  RecordsThisCycle += 1
  DisplayProgress = False
  IF PercentProgress < 100
    PercentProgress = (RecordsProcessed / RecordsToProcess)*100
    IF PercentProgress > 100
      PercentProgress = 100
    END
  END
  IF PercentProgress <> Progress:Thermometer THEN
    Progress:Thermometer = PercentProgress
    DisplayProgress = True
  END
  ?Progress:PctText{Prop:Text} = FORMAT(PercentProgress,@N3) & CPCS:ProgWinPctText
  IF DisplayProgress; DISPLAY().

OpenReportRoutine     ROUTINE
  PRINTER{PROPPRINT:Copies} = 1
  IF ~ReportWasOpened
    OPEN(report)
    ReportWasOpened = True
  END
  IF PRINTER{PROPPRINT:Copies} <> report{PROPPRINT:Copies}
    report{PROPPRINT:Copies} = PRINTER{PROPPRINT:Copies}
  END
  CollateCopies = report{PROPPRINT:Copies}
  IF report{PROPPRINT:COLLATE}=True
    report{PROPPRINT:Copies} = 1
  END
  IF report{PROP:TEXT}=''
    report{PROP:TEXT}='PickReport'
  END
  IF PreviewReq = True OR (report{PROPPRINT:Copies} > 1 AND ~(Supported(PROPPRINT:Copies)=True)) OR report{PROPPRINT:Collate} = True OR CPCSPgOfPgOption = True OR CPCSPageScanOption = True
    report{Prop:Preview} = PrintPreviewImage
  END


RecolourWindow      Routine

    Do RecolourWindow:Window

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674





