

   MEMBER('sbjobpick.clw')                            ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBJOB004.INC'),ONCE        !Local module procedure declarations
                     END


Main PROCEDURE                                        !Generated from procedure template - Window

CurrentTab           STRING(80)
BRW1::View:Browse    VIEW(PICKDET)
                       PROJECT(pdt:PickDetailRef)
                       PROJECT(pdt:PickNoteRef)
                       PROJECT(pdt:PartRefNumber)
                       PROJECT(pdt:Quantity)
                       PROJECT(pdt:IsChargeable)
                       PROJECT(pdt:RequestDate)
                       PROJECT(pdt:RequestTime)
                       PROJECT(pdt:EngineerCode)
                       PROJECT(pdt:IsInStock)
                       PROJECT(pdt:IsDelete)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
pdt:PickDetailRef      LIKE(pdt:PickDetailRef)        !List box control field - type derived from field
pdt:PickNoteRef        LIKE(pdt:PickNoteRef)          !List box control field - type derived from field
pdt:PartRefNumber      LIKE(pdt:PartRefNumber)        !List box control field - type derived from field
pdt:Quantity           LIKE(pdt:Quantity)             !List box control field - type derived from field
pdt:IsChargeable       LIKE(pdt:IsChargeable)         !List box control field - type derived from field
pdt:RequestDate        LIKE(pdt:RequestDate)          !List box control field - type derived from field
pdt:RequestTime        LIKE(pdt:RequestTime)          !List box control field - type derived from field
pdt:EngineerCode       LIKE(pdt:EngineerCode)         !List box control field - type derived from field
pdt:IsInStock          LIKE(pdt:IsInStock)            !List box control field - type derived from field
pdt:IsDelete           LIKE(pdt:IsDelete)             !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the PICKDET File'),AT(,,659,435),FONT('Tahoma',8,,),IMM,HLP('Newmain'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,20,640,368),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('64R(2)|M~Pick Detail Ref~C(0)@n-14@64R(2)|M~Pick Note Ref~C(0)@n-14@64R(2)|M~Par' &|
   't Ref Number~C(0)@n-14@64R(2)|M~Quantity~C(0)@n-14@56R(2)|M~Is Chargeable~C(0)@n' &|
   '3@80R(2)|M~Request Date~C(0)@d17@80R(2)|M~Request Time~C(0)@t7@56L(2)|M~Engineer' &|
   ' Code~@s3@48R(2)|M~Is In Stock~C(0)@n3@12R(2)|M~Is Delete~C(0)@n3@'),FROM(Queue:Browse:1)
                       BUTTON('&Insert'),AT(508,392,45,14),USE(?Insert:2)
                       BUTTON('&Change'),AT(556,392,45,14),USE(?Change:2),DEFAULT
                       BUTTON('&Delete'),AT(604,392,45,14),USE(?Delete:2)
                       SHEET,AT(4,4,652,408),USE(?CurrentTab)
                         TAB('pdt:keypickdet'),USE(?Tab:2)
                         END
                       END
                       BUTTON('Close'),AT(564,416,45,14),USE(?Close)
                       BUTTON('Help'),AT(612,416,45,14),USE(?Help),STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW1::Sort0:StepClass StepLongClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Main')
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:PICKDET.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:PICKDET,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,pdt:keypickdet)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,pdt:PickDetailRef,1,BRW1)
  BRW1.AddField(pdt:PickDetailRef,BRW1.Q.pdt:PickDetailRef)
  BRW1.AddField(pdt:PickNoteRef,BRW1.Q.pdt:PickNoteRef)
  BRW1.AddField(pdt:PartRefNumber,BRW1.Q.pdt:PartRefNumber)
  BRW1.AddField(pdt:Quantity,BRW1.Q.pdt:Quantity)
  BRW1.AddField(pdt:IsChargeable,BRW1.Q.pdt:IsChargeable)
  BRW1.AddField(pdt:RequestDate,BRW1.Q.pdt:RequestDate)
  BRW1.AddField(pdt:RequestTime,BRW1.Q.pdt:RequestTime)
  BRW1.AddField(pdt:EngineerCode,BRW1.Q.pdt:EngineerCode)
  BRW1.AddField(pdt:IsInStock,BRW1.Q.pdt:IsInStock)
  BRW1.AddField(pdt:IsDelete,BRW1.Q.pdt:IsDelete)
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:PICKDET.Close
  END
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdatePICKDET
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?Change:2
    SELF.DeleteControl=?Delete:2
  END


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

