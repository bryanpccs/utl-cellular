

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01029.INC'),ONCE        !Local module procedure declarations
                     END


ReceivingStock PROCEDURE (func:OrderNumber)           !Generated from procedure template - Window

!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::7:TAGFLAG          BYTE(0)
DASBRW::7:TAGMOUSE         BYTE(0)
DASBRW::7:TAGDISPSTATUS    BYTE(0)
DASBRW::7:QUEUE           QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::9:TAGFLAG          BYTE(0)
DASBRW::9:TAGMOUSE         BYTE(0)
DASBRW::9:TAGDISPSTATUS    BYTE(0)
DASBRW::9:QUEUE           QUEUE
Pointer2                      LIKE(GLO:Pointer2)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
save_orstmp_id       USHORT,AUTO
save_orjtmp_id       USHORT,AUTO
save_loc_id          USHORT,AUTO
tmp:yes              BYTE(1)
tmp:OrderNumber      LONG
tmp:TagJob           STRING(1)
tmp:TagStock         STRING(1)
tmp:StockTotal       LONG
tmp:JobTotal         LONG
tmp:Remaining        LONG
tmp:NotFulFilled     BYTE(0)
tmp:JobRequested     LONG
tmp:StockRequested   LONG
tmp:StockDifference  LONG
tmp:JobDifference    LONG
tmp:QuantityUsed     LONG
tmp:QuantityUser     LONG
tmp:RequestedTotal   LONG
tmp:FulfilledTotal   LONG
tmp:RemainingTotal   LONG
tmp:Chargeable       STRING('0')
tmp:Warranty         STRING(20)
tmp:DespatchNoteNumber STRING(30)
tmp:JobsUsed         LONG
tmp:StockUsed        LONG
tmp:OverAllocated    BYTE(0)
BRW4::View:Browse    VIEW(ORDPARTS)
                       PROJECT(orp:Part_Number)
                       PROJECT(orp:Description)
                       PROJECT(orp:Purchase_Cost)
                       PROJECT(orp:Sale_Cost)
                       PROJECT(orp:Retail_Cost)
                       PROJECT(orp:Quantity)
                       PROJECT(orp:Record_Number)
                       PROJECT(orp:All_Received)
                       PROJECT(orp:Order_Number)
                       PROJECT(orp:Part_Ref_Number)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
orp:Part_Number        LIKE(orp:Part_Number)          !List box control field - type derived from field
orp:Part_Number_NormalFG LONG                         !Normal forground color
orp:Part_Number_NormalBG LONG                         !Normal background color
orp:Part_Number_SelectedFG LONG                       !Selected forground color
orp:Part_Number_SelectedBG LONG                       !Selected background color
orp:Description        LIKE(orp:Description)          !List box control field - type derived from field
orp:Description_NormalFG LONG                         !Normal forground color
orp:Description_NormalBG LONG                         !Normal background color
orp:Description_SelectedFG LONG                       !Selected forground color
orp:Description_SelectedBG LONG                       !Selected background color
orp:Purchase_Cost      LIKE(orp:Purchase_Cost)        !List box control field - type derived from field
orp:Purchase_Cost_NormalFG LONG                       !Normal forground color
orp:Purchase_Cost_NormalBG LONG                       !Normal background color
orp:Purchase_Cost_SelectedFG LONG                     !Selected forground color
orp:Purchase_Cost_SelectedBG LONG                     !Selected background color
orp:Sale_Cost          LIKE(orp:Sale_Cost)            !List box control field - type derived from field
orp:Sale_Cost_NormalFG LONG                           !Normal forground color
orp:Sale_Cost_NormalBG LONG                           !Normal background color
orp:Sale_Cost_SelectedFG LONG                         !Selected forground color
orp:Sale_Cost_SelectedBG LONG                         !Selected background color
orp:Retail_Cost        LIKE(orp:Retail_Cost)          !List box control field - type derived from field
orp:Retail_Cost_NormalFG LONG                         !Normal forground color
orp:Retail_Cost_NormalBG LONG                         !Normal background color
orp:Retail_Cost_SelectedFG LONG                       !Selected forground color
orp:Retail_Cost_SelectedBG LONG                       !Selected background color
orp:Quantity           LIKE(orp:Quantity)             !List box control field - type derived from field
orp:Quantity_NormalFG  LONG                           !Normal forground color
orp:Quantity_NormalBG  LONG                           !Normal background color
orp:Quantity_SelectedFG LONG                          !Selected forground color
orp:Quantity_SelectedBG LONG                          !Selected background color
orp:Record_Number      LIKE(orp:Record_Number)        !List box control field - type derived from field
orp:Record_Number_NormalFG LONG                       !Normal forground color
orp:Record_Number_NormalBG LONG                       !Normal background color
orp:Record_Number_SelectedFG LONG                     !Selected forground color
orp:Record_Number_SelectedBG LONG                     !Selected background color
orp:All_Received       LIKE(orp:All_Received)         !List box control field - type derived from field
orp:All_Received_NormalFG LONG                        !Normal forground color
orp:All_Received_NormalBG LONG                        !Normal background color
orp:All_Received_SelectedFG LONG                      !Selected forground color
orp:All_Received_SelectedBG LONG                      !Selected background color
orp:Order_Number       LIKE(orp:Order_Number)         !Browse key field - type derived from field
orp:Part_Ref_Number    LIKE(orp:Part_Ref_Number)      !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW5::View:Browse    VIEW(ORDJOBS)
                       PROJECT(orjtmp:JobNumber)
                       PROJECT(orjtmp:Quantity)
                       PROJECT(orjtmp:RecordNumber)
                       PROJECT(orjtmp:PartNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?List:2
tmp:TagJob             LIKE(tmp:TagJob)               !List box control field - type derived from local data
tmp:TagJob_Icon        LONG                           !Entry's icon ID
orjtmp:JobNumber       LIKE(orjtmp:JobNumber)         !List box control field - type derived from field
orjtmp:Quantity        LIKE(orjtmp:Quantity)          !List box control field - type derived from field
tmp:Chargeable         LIKE(tmp:Chargeable)           !List box control field - type derived from local data
tmp:Chargeable_Icon    LONG                           !Entry's icon ID
tmp:Warranty           LIKE(tmp:Warranty)             !List box control field - type derived from local data
tmp:Warranty_Icon      LONG                           !Entry's icon ID
orjtmp:RecordNumber    LIKE(orjtmp:RecordNumber)      !Primary key field - type derived from field
orjtmp:PartNumber      LIKE(orjtmp:PartNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ORDSTOCK)
                       PROJECT(orstmp:Location)
                       PROJECT(orstmp:Quantity)
                       PROJECT(orstmp:RecordNumber)
                       PROJECT(orstmp:PartNumber)
                     END
Queue:Browse:2       QUEUE                            !Queue declaration for browse/combo box using ?List:3
tmp:TagStock           LIKE(tmp:TagStock)             !List box control field - type derived from local data
tmp:TagStock_Icon      LONG                           !Entry's icon ID
orstmp:Location        LIKE(orstmp:Location)          !List box control field - type derived from field
orstmp:Quantity        LIKE(orstmp:Quantity)          !List box control field - type derived from field
orstmp:RecordNumber    LIKE(orstmp:RecordNumber)      !Primary key field - type derived from field
orstmp:PartNumber      LIKE(orstmp:PartNumber)        !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
window               WINDOW('Receiving Stock Order'),AT(,,599,311),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),ALRT(F2Key),ALRT(F10Key),ALRT(F7Key),ALRT(F8Key),TIMER(50),GRAY,DOUBLE
                       SHEET,AT(4,4,592,116),USE(?Sheet1),SPREAD
                         TAB('Parts On Order'),USE(?Tab1)
                           BUTTON('&Insert'),AT(448,12,42,12),USE(?Insert:3),HIDE
                           BUTTON('&Amend Order [F2]'),AT(524,60,68,16),USE(?Change),LEFT,ICON('order.gif')
                           BUTTON('&Delete'),AT(490,12,42,12),USE(?Delete),HIDE
                         END
                       END
                       LIST,AT(8,28,512,88),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('128L(2)|M*~Part Number~@s30@128L(2)|M*~Description~@s30@56L(2)|M*~Purchase Cost~' &|
   '@n14.2@56L(2)|M*~Sale Cost~@n14.2@56L(2)|M*~Retail Cost~@n14.2@73D(2)|M*~Quantit' &|
   'y~L@p<<<<<<<<<<<<<<#p@48D(2)|M*~record number~L@n12@12D(2)|M*~All Received~L@s3@'),FROM(Queue:Browse)
                       SHEET,AT(4,124,200,156),USE(?Sheet2:2),SPREAD
                         TAB('Job Requests'),USE(?Tab3)
                           LIST,AT(8,144,132,132),USE(?List:2),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@38L(2)|M~Job No~@s8@46R(2)|M~Quantity~L@n8@12L(2)J~C~@s1@12L(2)J~W~@s' &|
   '1@'),FROM(Queue:Browse:1)
                           BUTTON('&Rev tags'),AT(68,168,50,13),USE(?DASREVTAG),HIDE
                           BUTTON('sho&W tags'),AT(68,184,70,13),USE(?DASSHOWTAG),HIDE
                           BUTTON('&Insert'),AT(24,240,42,12),USE(?Insert),HIDE
                           BUTTON('A&mend Request'),AT(144,144,56,16),USE(?AmendJobRequest),LEFT,ICON('fixer.gif')
                           BUTTON('&Delete Request'),AT(144,164,56,16),USE(?DeleteJobRequest),LEFT,ICON('delete.gif')
                           BUTTON('&Tag'),AT(144,220,56,16),USE(?DASTAG),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All [F7]'),AT(144,240,56,16),USE(?DASTAGAll),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(144,260,56,16),USE(?DASUNTAGALL),LEFT,ICON('UnTag.gif')
                         END
                       END
                       SHEET,AT(208,124,220,156),USE(?Sheet2),SPREAD
                         TAB('Stock Request'),USE(?Tab2)
                           LIST,AT(212,144,152,131),USE(?List:3),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('11L(2)I@s1@90L(2)|M~Location~@s30@32L(2)|M~Quantity~@n8@'),FROM(Queue:Browse:2)
                           BUTTON('&Rev tags'),AT(276,168,50,13),USE(?DASREVTAG:2),HIDE
                           BUTTON('sho&W tags'),AT(276,184,70,13),USE(?DASSHOWTAG:2),HIDE
                           BUTTON('&Insert'),AT(228,208,42,12),USE(?Insert:2),HIDE
                           BUTTON('&Amend Request'),AT(368,144,56,16),USE(?AmendStockRequest),LEFT,ICON('stock_qu.gif')
                           BUTTON('&Delete Request'),AT(368,164,56,16),USE(?DeleteStockRequest),LEFT,ICON('delete.gif')
                           BUTTON('&Tag'),AT(368,220,56,16),USE(?DASTAG:2),LEFT,ICON('Tag.gif')
                           BUTTON('T&ag All [F8]'),AT(368,240,56,16),USE(?DASTAGAll:2),LEFT,ICON('TagAll.gif')
                           BUTTON('&UnTag All'),AT(368,260,56,16),USE(?DASUNTAGALL:2),LEFT,ICON('UnTag.gif')
                         END
                       END
                       SHEET,AT(432,124,164,156),USE(?Sheet4),WIZARD,SPREAD
                         TAB('Order Details'),USE(?OrderDetailsTab)
                           PROMPT('Request Details'),AT(436,128),USE(?RequestDetails),FONT(,,,FONT:underline)
                           PROMPT('Requested'),AT(468,144),USE(?Requested)
                           LINE,AT(508,144,0,46),USE(?Line2),COLOR(COLOR:Black)
                           PROMPT('Jobs'),AT(436,156),USE(?Prompt6)
                           STRING(@s5),AT(472,156),USE(tmp:JobRequested),TRN,RIGHT,FONT(,,,FONT:bold),COLOR(COLOR:Silver)
                           PROMPT('Stock'),AT(436,168),USE(?Stock)
                           STRING(@s5),AT(552,184),USE(tmp:RemainingTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s8),AT(540,236),USE(tmp:FulfilledTotal,,?tmp:FulfilledTotal:2),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           PROMPT('Order Details'),AT(436,204),USE(?OrderDetails),FONT(,,,FONT:underline)
                           STRING(@s5),AT(512,184),USE(tmp:FulfilledTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s5),AT(472,184),USE(tmp:RequestedTotal),TRN,RIGHT,FONT(,,,FONT:bold),COLOR(COLOR:Silver)
                           LINE,AT(472,180,112,0),USE(?Line4),COLOR(COLOR:Black)
                           STRING(@s5),AT(552,168),USE(tmp:StockDifference),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s5),AT(472,168),USE(tmp:StockRequested),TRN,RIGHT,FONT(,,,FONT:bold),COLOR(COLOR:Silver)
                           LINE,AT(544,144,0,46),USE(?Line2:2),COLOR(COLOR:Black)
                           PROMPT('Difference'),AT(552,144),USE(?Difference)
                           PROMPT('Fulfilled'),AT(516,144),USE(?Fulfilled)
                           STRING(@s8),AT(540,220),USE(orp:Quantity),RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Fulfilled Quantity'),AT(436,236),USE(?FulFilledQuantity)
                           STRING(@s5),AT(512,156),USE(tmp:JobTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s5),AT(552,156),USE(tmp:JobDifference),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           STRING(@s5),AT(512,168),USE(tmp:StockTotal),TRN,RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Silver)
                           LINE,AT(437,252,108,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@s8),AT(540,260),USE(tmp:Remaining),RIGHT,FONT(,,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Quantity Remaining On Order'),AT(436,260),USE(?Prompt1:4)
                           PROMPT('Order Quantity'),AT(436,220),USE(?Prompt1)
                         END
                       END
                       BUTTON('Close [ESC]'),AT(536,288,56,16),USE(?Close),LEFT,ICON('cancel.gif')
                       PROMPT('Despatch Note Number'),AT(8,292),USE(?tmp:DespatchNoteNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                       ENTRY(@s30),AT(92,292,124,10),USE(tmp:DespatchNoteNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Despatch Note Number'),TIP('Despatch Note Number'),UPR
                       PANEL,AT(4,284,592,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('Process Tagged Orders [F10]'),AT(224,288,84,16),USE(?ProcessTaggedOrders),LEFT,ICON('STOCK_IN.GIF')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW4                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW4::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW5                 CLASS(BrowseClass)               !Browse using ?List:2
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW5::Sort0:Locator  StepLocatorClass                 !Default Locator
BRW6                 CLASS(BrowseClass)               !Browse using ?List:3
Q                      &Queue:Browse:2                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetFromView          PROCEDURE(),DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
        Map
CreateNewPart       Procedure(Long  func:Quantity)
AddStockHistory     Procedure(Long  func:Quantity)
DecStockHistory     Procedure(Long, Long)
UpdateStockHistory  PROCEDURE(String, Long, Long)
        End
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Sheet2:2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?List:2{prop:FontColor} = 65793
    ?List:2{prop:Color}= 16777215
    ?List:2{prop:Color,2} = 16777215
    ?List:2{prop:Color,3} = 12937777
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?List:3{prop:FontColor} = 65793
    ?List:3{prop:Color}= 16777215
    ?List:3{prop:Color,2} = 16777215
    ?List:3{prop:Color,3} = 12937777
    ?Sheet4{prop:Color} = 15066597
    ?OrderDetailsTab{prop:Color} = 15066597
    ?RequestDetails{prop:FontColor} = -1
    ?RequestDetails{prop:Color} = 15066597
    ?Requested{prop:FontColor} = -1
    ?Requested{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?tmp:JobRequested{prop:FontColor} = -1
    ?tmp:JobRequested{prop:Color} = 15066597
    ?Stock{prop:FontColor} = -1
    ?Stock{prop:Color} = 15066597
    ?tmp:RemainingTotal{prop:FontColor} = -1
    ?tmp:RemainingTotal{prop:Color} = 15066597
    ?tmp:FulfilledTotal:2{prop:FontColor} = -1
    ?tmp:FulfilledTotal:2{prop:Color} = 15066597
    ?OrderDetails{prop:FontColor} = -1
    ?OrderDetails{prop:Color} = 15066597
    ?tmp:FulfilledTotal{prop:FontColor} = -1
    ?tmp:FulfilledTotal{prop:Color} = 15066597
    ?tmp:RequestedTotal{prop:FontColor} = -1
    ?tmp:RequestedTotal{prop:Color} = 15066597
    ?tmp:StockDifference{prop:FontColor} = -1
    ?tmp:StockDifference{prop:Color} = 15066597
    ?tmp:StockRequested{prop:FontColor} = -1
    ?tmp:StockRequested{prop:Color} = 15066597
    ?Difference{prop:FontColor} = -1
    ?Difference{prop:Color} = 15066597
    ?Fulfilled{prop:FontColor} = -1
    ?Fulfilled{prop:Color} = 15066597
    ?orp:Quantity{prop:FontColor} = -1
    ?orp:Quantity{prop:Color} = 15066597
    ?FulFilledQuantity{prop:FontColor} = -1
    ?FulFilledQuantity{prop:Color} = 15066597
    ?tmp:JobTotal{prop:FontColor} = -1
    ?tmp:JobTotal{prop:Color} = 15066597
    ?tmp:JobDifference{prop:FontColor} = -1
    ?tmp:JobDifference{prop:Color} = 15066597
    ?tmp:StockTotal{prop:FontColor} = -1
    ?tmp:StockTotal{prop:Color} = 15066597
    ?tmp:Remaining{prop:FontColor} = -1
    ?tmp:Remaining{prop:Color} = 15066597
    ?Prompt1:4{prop:FontColor} = -1
    ?Prompt1:4{prop:Color} = 15066597
    ?Prompt1{prop:FontColor} = -1
    ?Prompt1{prop:Color} = 15066597
    ?tmp:DespatchNoteNumber:Prompt{prop:FontColor} = -1
    ?tmp:DespatchNoteNumber:Prompt{prop:Color} = 15066597
    If ?tmp:DespatchNoteNumber{prop:ReadOnly} = True
        ?tmp:DespatchNoteNumber{prop:FontColor} = 65793
        ?tmp:DespatchNoteNumber{prop:Color} = 15066597
    Elsif ?tmp:DespatchNoteNumber{prop:Req} = True
        ?tmp:DespatchNoteNumber{prop:FontColor} = 65793
        ?tmp:DespatchNoteNumber{prop:Color} = 8454143
    Else ! If ?tmp:DespatchNoteNumber{prop:Req} = True
        ?tmp:DespatchNoteNumber{prop:FontColor} = 65793
        ?tmp:DespatchNoteNumber{prop:Color} = 16777215
    End ! If ?tmp:DespatchNoteNumber{prop:Req} = True
    ?tmp:DespatchNoteNumber{prop:Trn} = 0
    ?tmp:DespatchNoteNumber{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::7:DASTAGONOFF Routine
  GET(Queue:Browse:1,CHOICE(?List:2))
  BRW5.UpdateBuffer
   glo:Queue.Pointer = orjtmp:RecordNumber
   GET(glo:Queue,glo:Queue.Pointer)
  IF ERRORCODE()
     glo:Queue.Pointer = orjtmp:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
    tmp:TagJob = '*'
  ELSE
    DELETE(glo:Queue)
    tmp:TagJob = ''
  END
    Queue:Browse:1.tmp:TagJob = tmp:TagJob
  IF (tmp:TagJob= '*')
    Queue:Browse:1.tmp:TagJob_Icon = 3
  ELSE
    Queue:Browse:1.tmp:TagJob_Icon = 2
  END
  PUT(Queue:Browse:1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW5.Reset
  FREE(glo:Queue)
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue.Pointer = orjtmp:RecordNumber
     ADD(glo:Queue,glo:Queue.Pointer)
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASUNTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue)
  BRW5.Reset
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASREVTAGALL Routine
  ?List:2{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::7:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue)
    GET(glo:Queue,QR#)
    DASBRW::7:QUEUE = glo:Queue
    ADD(DASBRW::7:QUEUE)
  END
  FREE(glo:Queue)
  BRW5.Reset
  LOOP
    NEXT(BRW5::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::7:QUEUE.Pointer = orjtmp:RecordNumber
     GET(DASBRW::7:QUEUE,DASBRW::7:QUEUE.Pointer)
    IF ERRORCODE()
       glo:Queue.Pointer = orjtmp:RecordNumber
       ADD(glo:Queue,glo:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW5.ResetSort(1)
  SELECT(?List:2,CHOICE(?List:2))
DASBRW::7:DASSHOWTAG Routine
   CASE DASBRW::7:TAGDISPSTATUS
   OF 0
      DASBRW::7:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::7:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::7:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW5.ResetSort(1)
   SELECT(?List:2,CHOICE(?List:2))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::9:DASTAGONOFF Routine
  GET(Queue:Browse:2,CHOICE(?List:3))
  BRW6.UpdateBuffer
   glo:Queue2.Pointer2 = orstmp:RecordNumber
   GET(glo:Queue2,glo:Queue2.Pointer2)
  IF ERRORCODE()
     glo:Queue2.Pointer2 = orstmp:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
    tmp:TagStock = '*'
  ELSE
    DELETE(glo:Queue2)
    tmp:TagStock = ''
  END
    Queue:Browse:2.tmp:TagStock = tmp:TagStock
  IF (tmp:TagStock = '*')
    Queue:Browse:2.tmp:TagStock_Icon = 2
  ELSE
    Queue:Browse:2.tmp:TagStock_Icon = 1
  END
  PUT(Queue:Browse:2)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::9:DASTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(glo:Queue2)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     glo:Queue2.Pointer2 = orstmp:RecordNumber
     ADD(glo:Queue2,glo:Queue2.Pointer2)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::9:DASUNTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(glo:Queue2)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::9:DASREVTAGALL Routine
  ?List:3{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::9:QUEUE)
  LOOP QR# = 1 TO RECORDS(glo:Queue2)
    GET(glo:Queue2,QR#)
    DASBRW::9:QUEUE = glo:Queue2
    ADD(DASBRW::9:QUEUE)
  END
  FREE(glo:Queue2)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::9:QUEUE.Pointer2 = orstmp:RecordNumber
     GET(DASBRW::9:QUEUE,DASBRW::9:QUEUE.Pointer2)
    IF ERRORCODE()
       glo:Queue2.Pointer2 = orstmp:RecordNumber
       ADD(glo:Queue2,glo:Queue2.Pointer2)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List:3,CHOICE(?List:3))
DASBRW::9:DASSHOWTAG Routine
   CASE DASBRW::9:TAGDISPSTATUS
   OF 0
      DASBRW::9:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::9:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG:2{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::9:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG:2{PROP:Text} = 'Show All'
      ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG:2{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List:3,CHOICE(?List:3))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
AddNewPart      Routine
    Stop('This should NOT appear')
AddStockHistory     Routine
    If Access:STOHIST.Primerecord() = Level:Benign
        shi:Ref_Number              = sto:Ref_Number
        shi:Transaction_Type        = 'ADD'
        shi:Despatch_Note_Number    = tmp:DespatchNoteNumber
        shi:Quantity                = (orp:Quantity - tmp:QuantityUsed)
        shi:Date                    = Today()
        shi:Purchase_Cost           = sto:Purchase_Cost
        shi:Sale_Cost               = sto:Sale_Cost
        Access:USERS.Clearkey(use:Password_Key)
        use:Password                = glo:Password
        Access:USERS.Tryfetch(use:Password_Key)
        shi:User                    = use:User_Code
        shi:Notes                   = 'STOCK ADDED FROM ORDER'
        shi:information             = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                        '<13,10>DESPATCH NOTE NO: ' & Clip(orp:DespatchNoteNumber)
        If Access:STOHIST.Tryinsert()
            Access:STOHIST.Cancelautoinc()
        End!If Access:STOHIST.Tryinsert()
    End!If Access:STOHIST.Primerecord() = Level:Benign
Check_job_status        Routine
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = orjtmp:JobNumber
    If access:jobs.fetch(job:ref_number_key) = Level:Benign
        Case CheckParts('C')
            Of 1
                GetStatus(330,0,'JOB')
            Of 2
                GetStatus(335,0,'JOB')
            Else
                Case CheckParts('W')
                    Of 1
                        GetStatus(330,0,'JOB')
                    Of 2
                        GetStatus(335,0,'JOB')
                    Else
                        GetStatus(345,0,'JOB')
                End!Case CheckParts('W')
        End!Case CheckParts('C')
        end_date" = ''
        access:jobs.update()
   End !If access:jobs.fetch(job:ref_number_key) = Level:Benign


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'ReceivingStock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('save_orstmp_id',save_orstmp_id,'ReceivingStock',1)
    SolaceViewVars('save_orjtmp_id',save_orjtmp_id,'ReceivingStock',1)
    SolaceViewVars('save_loc_id',save_loc_id,'ReceivingStock',1)
    SolaceViewVars('tmp:yes',tmp:yes,'ReceivingStock',1)
    SolaceViewVars('tmp:OrderNumber',tmp:OrderNumber,'ReceivingStock',1)
    SolaceViewVars('tmp:TagJob',tmp:TagJob,'ReceivingStock',1)
    SolaceViewVars('tmp:TagStock',tmp:TagStock,'ReceivingStock',1)
    SolaceViewVars('tmp:StockTotal',tmp:StockTotal,'ReceivingStock',1)
    SolaceViewVars('tmp:JobTotal',tmp:JobTotal,'ReceivingStock',1)
    SolaceViewVars('tmp:Remaining',tmp:Remaining,'ReceivingStock',1)
    SolaceViewVars('tmp:NotFulFilled',tmp:NotFulFilled,'ReceivingStock',1)
    SolaceViewVars('tmp:JobRequested',tmp:JobRequested,'ReceivingStock',1)
    SolaceViewVars('tmp:StockRequested',tmp:StockRequested,'ReceivingStock',1)
    SolaceViewVars('tmp:StockDifference',tmp:StockDifference,'ReceivingStock',1)
    SolaceViewVars('tmp:JobDifference',tmp:JobDifference,'ReceivingStock',1)
    SolaceViewVars('tmp:QuantityUsed',tmp:QuantityUsed,'ReceivingStock',1)
    SolaceViewVars('tmp:QuantityUser',tmp:QuantityUser,'ReceivingStock',1)
    SolaceViewVars('tmp:RequestedTotal',tmp:RequestedTotal,'ReceivingStock',1)
    SolaceViewVars('tmp:FulfilledTotal',tmp:FulfilledTotal,'ReceivingStock',1)
    SolaceViewVars('tmp:RemainingTotal',tmp:RemainingTotal,'ReceivingStock',1)
    SolaceViewVars('tmp:Chargeable',tmp:Chargeable,'ReceivingStock',1)
    SolaceViewVars('tmp:Warranty',tmp:Warranty,'ReceivingStock',1)
    SolaceViewVars('tmp:DespatchNoteNumber',tmp:DespatchNoteNumber,'ReceivingStock',1)
    SolaceViewVars('tmp:JobsUsed',tmp:JobsUsed,'ReceivingStock',1)
    SolaceViewVars('tmp:StockUsed',tmp:StockUsed,'ReceivingStock',1)
    SolaceViewVars('tmp:OverAllocated',tmp:OverAllocated,'ReceivingStock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2:2;  SolaceCtrlName = '?Sheet2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:2;  SolaceCtrlName = '?List:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AmendJobRequest;  SolaceCtrlName = '?AmendJobRequest';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DeleteJobRequest;  SolaceCtrlName = '?DeleteJobRequest';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List:3;  SolaceCtrlName = '?List:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG:2;  SolaceCtrlName = '?DASREVTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG:2;  SolaceCtrlName = '?DASSHOWTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:2;  SolaceCtrlName = '?Insert:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AmendStockRequest;  SolaceCtrlName = '?AmendStockRequest';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DeleteStockRequest;  SolaceCtrlName = '?DeleteStockRequest';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG:2;  SolaceCtrlName = '?DASTAG:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll:2;  SolaceCtrlName = '?DASTAGAll:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL:2;  SolaceCtrlName = '?DASUNTAGALL:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OrderDetailsTab;  SolaceCtrlName = '?OrderDetailsTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?RequestDetails;  SolaceCtrlName = '?RequestDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Requested;  SolaceCtrlName = '?Requested';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line2;  SolaceCtrlName = '?Line2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobRequested;  SolaceCtrlName = '?tmp:JobRequested';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock;  SolaceCtrlName = '?Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RemainingTotal;  SolaceCtrlName = '?tmp:RemainingTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FulfilledTotal:2;  SolaceCtrlName = '?tmp:FulfilledTotal:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OrderDetails;  SolaceCtrlName = '?OrderDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:FulfilledTotal;  SolaceCtrlName = '?tmp:FulfilledTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RequestedTotal;  SolaceCtrlName = '?tmp:RequestedTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line4;  SolaceCtrlName = '?Line4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StockDifference;  SolaceCtrlName = '?tmp:StockDifference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StockRequested;  SolaceCtrlName = '?tmp:StockRequested';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line2:2;  SolaceCtrlName = '?Line2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Difference;  SolaceCtrlName = '?Difference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fulfilled;  SolaceCtrlName = '?Fulfilled';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Quantity;  SolaceCtrlName = '?orp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?FulFilledQuantity;  SolaceCtrlName = '?FulFilledQuantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobTotal;  SolaceCtrlName = '?tmp:JobTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:JobDifference;  SolaceCtrlName = '?tmp:JobDifference';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:StockTotal;  SolaceCtrlName = '?tmp:StockTotal';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Remaining;  SolaceCtrlName = '?tmp:Remaining';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1:4;  SolaceCtrlName = '?Prompt1:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt1;  SolaceCtrlName = '?Prompt1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchNoteNumber:Prompt;  SolaceCtrlName = '?tmp:DespatchNoteNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:DespatchNoteNumber;  SolaceCtrlName = '?tmp:DespatchNoteNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ProcessTaggedOrders;  SolaceCtrlName = '?ProcessTaggedOrders';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('ReceivingStock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'ReceivingStock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Insert:3
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:AUDIT.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDJOBS.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:ORDSTOCK.Open
  Relate:PARTS_ALIAS.Open
  Relate:STAHEAD.Open
  Relate:WARPARTS_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:LOCATION.UseFile
  Access:ORDERS.UseFile
  Access:JOBS.UseFile
  Access:STATUS.UseFile
  Access:JOBSTAGE.UseFile
  SELF.FilesOpened = True
  tmp:OrderNumber = func:OrderNumber
  BRW4.Init(?List,Queue:Browse.ViewPosition,BRW4::View:Browse,Queue:Browse,Relate:ORDPARTS,SELF)
  BRW5.Init(?List:2,Queue:Browse:1.ViewPosition,BRW5::View:Browse,Queue:Browse:1,Relate:ORDJOBS,SELF)
  BRW6.Init(?List:3,Queue:Browse:2.ViewPosition,BRW6::View:Browse,Queue:Browse:2,Relate:ORDSTOCK,SELF)
  OPEN(window)
  SELF.Opened=True
  brw4.DeleteControl = 0
  brw4.InsertControl = 0
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List:2{prop:vcr} = TRUE
  ?List:3{prop:vcr} = TRUE
  ! support for CPCS
  BRW4.Q &= Queue:Browse
  BRW4.AddSortOrder(,orp:Order_Number_Key)
  BRW4.AddRange(orp:Order_Number,tmp:OrderNumber)
  BRW4.AddLocator(BRW4::Sort0:Locator)
  BRW4::Sort0:Locator.Init(,orp:Part_Ref_Number,1,BRW4)
  BRW4.AddField(orp:Part_Number,BRW4.Q.orp:Part_Number)
  BRW4.AddField(orp:Description,BRW4.Q.orp:Description)
  BRW4.AddField(orp:Purchase_Cost,BRW4.Q.orp:Purchase_Cost)
  BRW4.AddField(orp:Sale_Cost,BRW4.Q.orp:Sale_Cost)
  BRW4.AddField(orp:Retail_Cost,BRW4.Q.orp:Retail_Cost)
  BRW4.AddField(orp:Quantity,BRW4.Q.orp:Quantity)
  BRW4.AddField(orp:Record_Number,BRW4.Q.orp:Record_Number)
  BRW4.AddField(orp:All_Received,BRW4.Q.orp:All_Received)
  BRW4.AddField(orp:Order_Number,BRW4.Q.orp:Order_Number)
  BRW4.AddField(orp:Part_Ref_Number,BRW4.Q.orp:Part_Ref_Number)
  BRW5.Q &= Queue:Browse:1
  BRW5.AddSortOrder(,orjtmp:JobNumberKey)
  BRW5.AddRange(orjtmp:PartNumber,orp:Part_Number)
  BRW5.AddLocator(BRW5::Sort0:Locator)
  BRW5::Sort0:Locator.Init(,orjtmp:JobNumber,1,BRW5)
  BIND('tmp:TagJob',tmp:TagJob)
  BIND('tmp:Chargeable',tmp:Chargeable)
  BIND('tmp:Warranty',tmp:Warranty)
  ?List:2{PROP:IconList,1} = '~bluetick.ico'
  ?List:2{PROP:IconList,2} = '~notick1.ico'
  ?List:2{PROP:IconList,3} = '~tick1.ico'
  BRW5.AddField(tmp:TagJob,BRW5.Q.tmp:TagJob)
  BRW5.AddField(orjtmp:JobNumber,BRW5.Q.orjtmp:JobNumber)
  BRW5.AddField(orjtmp:Quantity,BRW5.Q.orjtmp:Quantity)
  BRW5.AddField(tmp:Chargeable,BRW5.Q.tmp:Chargeable)
  BRW5.AddField(tmp:Warranty,BRW5.Q.tmp:Warranty)
  BRW5.AddField(orjtmp:RecordNumber,BRW5.Q.orjtmp:RecordNumber)
  BRW5.AddField(orjtmp:PartNumber,BRW5.Q.orjtmp:PartNumber)
  BRW6.Q &= Queue:Browse:2
  BRW6.AddSortOrder(,orstmp:LocationKey)
  BRW6.AddRange(orstmp:PartNumber,orp:Part_Number)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(,orstmp:Location,1,BRW6)
  BIND('tmp:TagStock',tmp:TagStock)
  ?List:3{PROP:IconList,1} = '~notick1.ico'
  ?List:3{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:TagStock,BRW6.Q.tmp:TagStock)
  BRW6.AddField(orstmp:Location,BRW6.Q.orstmp:Location)
  BRW6.AddField(orstmp:Quantity,BRW6.Q.orstmp:Quantity)
  BRW6.AddField(orstmp:RecordNumber,BRW6.Q.orstmp:RecordNumber)
  BRW6.AddField(orstmp:PartNumber,BRW6.Q.orstmp:PartNumber)
  BRW5.AskProcedure = 1
  BRW6.AskProcedure = 2
  BRW4.AskProcedure = 3
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  ?DASSHOWTAG:2{PROP:Text} = 'Show All'
  ?DASSHOWTAG:2{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG:2{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab1{PROP:TEXT} = 'Parts On Order'
    ?List{PROP:FORMAT} ='128L(2)|M*~Part Number~@s30@#1#128L(2)|M*~Description~@s30@#6#56L(2)|M*~Purchase Cost~@n14.2@#11#56L(2)|M*~Sale Cost~@n14.2@#16#56L(2)|M*~Retail Cost~@n14.2@#21#73D(2)|M*~Quantity~L@p<<<<<<<#p@#26#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(glo:Queue2)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:AUDIT.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDJOBS.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:ORDSTOCK.Close
    Relate:PARTS_ALIAS.Close
    Relate:STAHEAD.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'ReceivingStock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = True
  If Field() = ?DeleteStockRequest
      do_update# = False
      Case MessageEx('Are you sure you want to remove the selected Request?','ServiceBase 2000',|
                     'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,48,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Delete(ORDSTOCK)
          Of 2 ! &No Button
      End!Case MessageEx
  End!If Field() = ?DeleteStockRequest
  IF request = InsertRecord
      do_update# = False
  End!request = Insert
  
  If do_update# = True
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      AmendJobRequest
      AmendStockRequest
      UpdatePartsOrder
    END
    ReturnValue = GlobalResponse
  END
      If ReturnValue = RequestCompleted
          Do DASBRW::7:DASUNTAGALL
          Do DASBRW::9:DASUNTAGALL
      End !If ReturnValue = RequestCompleted
  End!If do_update# = True
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::7:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAG:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::9:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?ProcessTaggedOrders
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessTaggedOrders, Accepted)
    brw4.UpdateViewRecord
    If brw4.q.orp:All_Received = 'YES'
        Case MessageEx('This item has already been received.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    Else !brw4.q.ord:All_Recieved = 'YES'
        !Were all the parts selected?
        Error# = 0
        If tmp:DespatchNoteNumber = ''
            Case MessageEx('You have not entered a Purchase Order Number.'&|
              '<13,10>'&|
              '<13,10>Do you wish to enter it, or continue?','ServiceBase 2000',|
                           'Styles\question.ico','|&Enter Desp Note No|&Continue|&Cancel',3,3,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                Of 1 ! &Enter Desp Note No Button
                    tmp:DespatchNoteNumber = GetDespatchNumber()
                Of 2 ! &Continue Button
                Of 3 ! &Cancel Button
                    Error# = 1
            End!Case MessageEx
        End !If tmp:DespatchNoteNumber = ''
        If error# = 0
            tmp:NotFulfilled = 0
            tmp:QuantityUsed = 0
            tmp:JobsUsed    = 0
            tmp:StockUsed   = 0
            Save_orjtmp_ID = Access:ORDJOBS.SaveFile()
            Set(orjtmp:RecordNumberKey)
            Loop
                If Access:ORDJOBS.NEXT()
                   Break
                End !If
                Clear(glo:Queue)
                glo:Pointer = orjtmp:RecordNumber
                Get(glo:Queue,glo:Pointer)
                If Error()
                    tmp:NotFulfilled = 1
                Else
                    tmp:QuantityUsed += orjtmp:Quantity
                    tmp:JobsUsed += orjtmp:Quantity
                End!If Error()
            End !Loop
            Access:ORDJOBS.RestoreFile(Save_orjtmp_ID)

            Save_orstmp_ID = Access:ORDSTOCK.SaveFile()
            Set(orstmp:RecordNumberKey)
            Loop
                If Access:ORDSTOCK.NEXT()
                   Break
                End !If
                Clear(glo:Queue2)
                glo:Pointer2    = orstmp:RecordNumber
                Get(glo:Queue2,glo:Pointer2)
                If Error()
                    tmp:NotFulFilled = 1
                ELse
                    tmp:QuantityUsed += orstmp:Quantity
                    tmp:StockUsed   += orstmp:Quantity
                End!If Error()
            End !Loop
            Access:ORDSTOCK.RestoreFile(Save_orstmp_ID)

            continue# = 1
            ! Start Change 3372  BE(14/10/03)
            diffaction# = 0
            ! End Change 3372  BE(14/10/03)
            Access:ORDPARTS.Clearkey(orp:Record_Number_key)
            orp:Record_Number   = brw4.q.orp:Record_Number
            If Access:ORDPARTS.Tryfetch(orp:Record_Number_key) = Level:Benign
                If tmp:QuantityUsed < orp:Quantity
                    Case MessageEx('You have not allocated all the parts on this order item.'&|
                      '<13,10>Do you wish to:'&|
                      '<13,10>'&|
                      '<13,10>1) Put the remaining parts into stock'&|
                      '<13,10>'&|
                      '<13,10>2) Amend the order and re-request the parts'&|
                      '<13,10>'&|
                      '<13,10>3) Amend the order, ignore the difference and DO NOT re-request the parts.'&|
                      '<13,10>'&|
                      '<13,10>4) Mark the remaining parts to "Arrive Later"?'&|
                      '<13,10>'&|
                      '<13,10>','ServiceBase 2000',|
                                   'Styles\stop.ico','|&1) Add To Stock|&2) Re-Request|&3) Ignore|&4) Arrive Later|&Cancel',5,5,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &1) Add To Stock Button
                            !Need to find if this part exists in the MAIN STORE and add it there.
                            !If not, we'll need some horrible code to check for another location,
                            !and possible add a new part.

                            ! Start Change 3372  BE(14/10/03)
                            !diffaction# = 1
                            ! End Change 3372  BE(14/10/03)

                            Access:STOCK.Clearkey(sto:Location_Key)
                            sto:Location    = MainStoreLocation()
                            sto:Part_Number = orp:Part_Number
                            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Found
                                sto:Quantity_Stock += (orp:Quantity - tmp:QuantityUsed)
                                If Access:STOCK.Update() = Level:Benign
                                    Do AddStockHistory
                                End!If Access:STOCK.Update()
                                orp:Quantity        = (orp:Quantity - tmp:QuantityUsed)
                                orp:All_Received    = 'YES'
                                orp:Date_Received   = Today()
                                orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                Access:ORDPARTS.Update()
                            Else! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Can't find it in Main Store, try the other locations
                                FoundPart# = 0
                                Save_loc_ID = Access:LOCATION.SaveFile()
                                Access:LOCATION.Clearkey(loc:Location_Key)
                                Set(loc:Location_Key)
                                Loop
                                    If Access:LOCATION.Next() 
                                       Break
                                    End !If
                                    Access:STOCK.Clearkey(sto:Location_Key)
                                    If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                        !Found
                                        FoundPart# = 1
                                        sto:Quantity_Stock += (orp:Quantity - tmp:QuantityUsed)
                                        If Access:STOCK.Update() = Level:Benign
                                            Do AddStockHistory
                                        End!If Access:STOCK.Update()
                                        orp:Quantity        = (orp:Quantity - tmp:QuantityUsed)
                                        orp:All_Received    = 'YES'
                                        orp:Date_Received   = Today()
                                        orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                        Access:ORDPARTS.Update()
                                        Break
                                    Else! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                        !Error
                                        !Assert(0,'<13,10>Fetch Error<13,10>')
                                    End! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    
                                End !Loop
                                Access:LOCATION.RestoreFile(Save_loc_ID)

                                If FoundPart# = 0
                                    Case MessageEx('Cannot find the selected part in stock.'&|
                                      '<13,10>'&|
                                      '<13,10>Do you wish to add it now?','ServiceBase 2000',|
                                                   'Styles\stop.ico','|&Yes|&No',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                        Of 1 ! &Yes Button
                                            !Add Part To Stock
                                            CreateNewPart(orp:Quantity - tmp:QuantityUsed)
                                            orp:Quantity        = tmp:QuantityUsed
                                            orp:All_Received    = 'YES'
                                            orp:Date_Received   = Today()
                                            orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                            Access:ORDPARTS.Update()

                                        Of 2 ! &No Button
                                            Case MessageEx('Do you wish to amend the order and mark it as received?','ServiceBase 2000',|
                                                           'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                                Of 1 ! &Yes Button
                                                    orp:Quantity        = tmp:QuantityUsed
                                                    orp:All_Received    = 'YES'
                                                    orp:Date_Received   = Today()
                                                    orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                                    Access:ORDPARTS.Update()
                                                Of 2 ! &No Button
                                                    continue# = 0
                                            End!Case MessageEx

                                    End!Case MessageEx
                                End !If FoundPart# = 0
                            End! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                            
                        Of 2 ! &2) Re-Request Button
                            !Check for Main Store parts, get the supplier
                            !make and order and increase the requested quantity

                            ! Start Change 3372  BE(14/10/03)
                            !diffaction# = 2
                            ! End Change 3372  BE(14/10/03)

                            Access:STOCK.Clearkey(sto:Location_Key)
                            sto:Location    = MainStoreLocation()
                            sto:Part_Number = orp:Part_Number
                            If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                MakePartsRequest(sto:Supplier,sto:Part_Number,sto:Description,(orp:Quantity - tmp:QuantityUsed))
                                sto:QuantityRequested += (orp:Quantity - tmp:QuantityUsed)
                                Access:STOCK.Update()
                                orp:Quantity        = tmp:QuantityUsed
                                orp:All_Received    = 'YES'
                                orp:Date_Received   = Today()
                                orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                Access:ORDPARTS.Update()
                            Else!If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                !Cannot find the part in the Main Store, try the other locations
                                FoundPart# = 0
                                Save_loc_ID = Access:LOCATION.SaveFile()
                                Access:LOCATION.Clearkey(loc:Location_Key)
                                Set(loc:Location_Key)
                                Loop
                                    If Access:LOCATION.NEXT()
                                       Break
                                    End !If

                                    Access:STOCK.Clearkey(sto:Location_Key)
                                    sto:Location    = loc:Location
                                    sto:Part_Number = orp:Part_Number
                                    If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                        !Found
                                        Case MessageEx('This part exists in the following location:'&|
                                          '<13,10>'&|
                                          '<13,10>' & Clip(loc:Location) & '.'&|
                                          '<13,10>'&|
                                          '<13,10>Do you wish to request the parts for this location?','ServiceBase 2000',|
                                                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                                            Of 1 ! &Yes Button
                                                MakePartsRequest(sto:Supplier,sto:Part_Number,sto:Description,(orp:Quantity - tmp:QuantityUsed))
                                                sto:QuantityRequested += (orp:Quantity - tmp:QuantityUsed)
                                                FoundPart# = 1
                                                Break
                                            Of 2 ! &No Button
                                        End!Case MessageEx
                                    End! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                                    
                                End !Loop
                                Access:LOCATION.RestoreFile(Save_loc_ID)

                                If FoundPart# = 0
                                    Case MessageEx('Cannot find the selected part in Stock. You will need to add this before you can re-request it.'&|
                                      '<13,10>'&|
                                      '<13,10>Do you wish to add this part now?','ServiceBase 2000',|
                                                   'Styles\stop.ico','|&Yes|&No',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                        Of 1 ! &Yes Button
                                            !Add Part To Stock
                                            CreateNewPart(orp:Quantity - tmp:QuantityUsed)
                                        Of 2 ! &No Button
                                            continue# = 0
                                            !Cancel
                                    End!Case MessageEx
                                Else !If FoundPart# = 0
                                    orp:Quantity        = tmp:QuantityUsed
                                    orp:All_Received    = 'YES'
                                    orp:Date_Received   = Today()
                                    orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                    Access:ORDPARTS.Update()
                                End !If FoundPart# = 0
                            End!If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign

                        Of 3 ! &3) Ignore Button

                            ! Start Change 3372  BE(14/10/03)
                            !diffaction# = 3
                            ! End Change 3372  BE(14/10/03)

                            orp:Quantity        = tmp:QuantityUsed
                            orp:All_Received    = 'YES'
                            orp:Date_Received   = Today()
                            orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                            Access:ORDPARTS.Update()

                        Of 4 ! &4) Arrive Later Button

                            ! Start Change 3372  BE(14/10/03)
                            !diffaction# = 4
                            ! End Change 3372  BE(14/10/03)

                            Access:ORDPARTS_ALIAS.ClearKey(orp_ali:record_number_key)
                            orp_ali:record_number = brw4.q.orp:Record_Number
                            If Access:ORDPARTS_ALIAS.TryFetch(orp_ali:record_number_key) = Level:Benign
                                If Access:ORDPARTS.Primerecord() = Level:Benign
                                    RecordNumber$   = orp:Record_Number
                                    orp:Record      :=: orp_ali:Record
                                    orp:Record_NUmber   = RecordNumber$
                                    orp:Quantity    = orp_ali:Quantity - tmp:QuantityUsed
                                    If Access:ORDPARTS.Tryinsert()
                                        Access:ORDPARTS.Cancelautoinc()
                                    End!If Access:ORDPARTS.Tryinsert()
                                End!If Access:ORDPARTS.Primerecord() = Level:Benign
                            End!If Access:ORDPARTS_ALIAS.TryFetch(orp_ali:record_number_key) = Level:Benign

                            !Reget the current order item
                            Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
                            orp:Record_number   = brw4.q.orp:Record_Number
                            If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
                                !Now mark the current order item as received
                                orp:Quantity        = tmp:QuantityUsed
                                orp:All_Received    = 'YES'
                                orp:Date_Received   = Today()
                                orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                Access:ORDPARTS.Update()
                            End! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign

                        Of 5 ! &Cancel Button

                            ! Start Change 3372  BE(14/10/03)
                            !diffaction# = 5
                            ! End Change 3372  BE(14/10/03)

                    End!Case MessageEx
                Else!If tmp:QuantityUsed < orp:Quantity
                    If tmp:JobsUsed > orp:Quantity
                        Case MessageEx('Error!'&|
                          '<13,10>You are attempting to allocate more parts to Jobs that are on the order.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        continue# = 0
                    Else !If tmp:JobsUsed > orp:Quantity
                        IF tmp:QuantityUsed > orp:Quantity
                            Case MessageEx('This order has now been fulfilled. '&|
                              '<13,10>'&|
                              '<13,10>But be aware there are still outstanding requests.','ServiceBase 2000',|
                                           'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                                Of 1 ! &OK Button
                            End!Case MessageEx
                            ! Start Change 3372 BE(15/10/03)
                            !Reset STock Used with how much of the order is left to be allocated
                            tmp:StockUsed   = orp:Quantity - tmp:JobsUsed
                            orp:All_Received    = 'YES'
                            orp:Date_Received   = Today()
                            orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                            Access:ORDPARTS.Update()
                            ! End Change 3372 BE(15/10/03)
                        Else !IF tmp:QuantityUsed > orp:Quantity
                            orp:quantity        = tmp:QuantityUsed
                            orp:All_Received    = 'YES'
                            orp:Date_Received   = Today()
                            orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                            Access:ORDPARTS.Update()
                        End !IF tmp:QuantityUsed > orp:Quantity

                    End !If tmp:JobsUsed > orp:Quantity
                End!If tmp:QuantityUsed < orp:Quantity
            End!If Access:ORDPARTS.Tryfetch(orp:Record_Number_key) = Level:Benign

            If continue# = 1

                !All items have been fulfilled
                !Reset STock Used with how much of the order is left to be allocated
                ! Start Change 3372 BE(15/10/03)
                !tmp:StockUsed   = orp:Quantity - tmp:JobsUsed
                ! End Change 3372 BE(15/10/03)
                Loop x# = 1 To Records(glo:Queue2)
                    Get(glo:Queue2,x#)
                    !Get Ordstock record
                    Access:ORDSTOCK.ClearKey(orstmp:RecordNumberKey)
                    orstmp:RecordNumber = glo:Pointer2
                    If Access:ORDSTOCK.TryFetch(orstmp:RecordNumberKey) = Level:Benign
                        !Get Stock record

                        Access:STOCK.ClearKey(sto:Location_Key)
                        sto:Location    = orstmp:Location
                        sto:Part_Number = orstmp:PartNumber
                        If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                            !Keep a tally of how many parts have been taken
                            If orstmp:Quantity > tmp:StockUsed
                                tmp:QuantityUser += orstmp:Quantity
                                sto:Quantity_Stock  += tmp:StockUsed
                                sto:QuantityRequested -= tmp:StockUsed
                                If sto:QuantityRequested < 0
                                    sto:QuantityRequested = 0
                                End !If sto:QuantityReqested < 0

                                !What do we do with the rest?
!                                        orp:quantity        = tmp:StockUsed
                                ! Start Change 3372 BE(15/10/03)
                                !orp:All_Received    = 'YES'
                                !orp:Date_Received   = Today()
                                !orp:DespatchNoteNumber  = tmp:DespatchNoteNumber
                                !Access:ORDPARTS.Update()
                                ! End Change 3372 BE(15/10/03)

                                If Access:STOCK.Update() = Level:Benign
                                    If tmp:StockUsed > 0
                                        AddStockHistory(tmp:StockUsed)
                                    End !If tmp:StockUsed > 0
                                End !If Access:STOCK.Update() = Level:Benign
                                tmp:StockUsed = 0

                            Else !If orstmp:Quantity > tmp:StockUsed
                                tmp:QuantityUser += orstmp:Quantity
                                sto:Quantity_Stock += orstmp:Quantity
                                sto:QuantityRequested -= orstmp:Quantity
                                If sto:QuantityRequested < 0
                                    sto:QuantityRequested = 0
                                End!If sto:QuantityRequested < 0
                                tmp:StockUSed -= orstmp:Quantity
                                If Access:STOCK.Update() = Level:benign
                                    AddStockHistory(orstmp:Quantity)
                                    Delete(ORDSTOCK)
                                End!If Access:STOCK.Update() = Level:benign

                            End !If orstmp:Quantity > tmp:StockUsed
                        End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    End!If Access:ORDSTOCK.TryFetch(orstmp:RecordNumberKey) = Level:Benign
                End!Loop x# = 1 To Records(glo:Queue2)

                !Loop through JOBS table
                Loop x# = 1 To Records(glo:Queue)
                    Get(glo:Queue,x#)
                    Access:ORDJOBS.Clearkey(orjtmp:RecordNumberKey)
                    orjtmp:RecordNumber = glo:Pointer
                    If Access:ORDJOBS.Tryfetch(orjtmp:RecordNumberKey) = Level:Benign
                        Case orjtmp:CharWarr
                            Of 'C'
                                Access:PARTS.Clearkey(par:RecordNumberKey)
                                par:Record_Number   = orjtmp:RefNumber
                                If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                                    !Found
                                    !Start Change 3372 BE(14/10/03)
                                    ! Note: The following code is original.
                                    par:Requested = False
                                    par:Order_Number = orp:Order_Number
                                    par:Date_Received = Today()
                                    par:Despatch_Note_Number = tmp:DespatchNoteNumber
                                    Access:PARTS.Update()
                                    ! Note: The following amendment is for fulfilling Part Orders
                                    !       but has been removed because the "AMEND REQUEST" button actually
                                    !       amends the request and is not designed for PART ODER Function.
                                    !
                                    !IF ((diffaction# = 4) AND (orjtmp:Quantity < par:Quantity)) THEN
                                    !    ! Create New Record
                                    !    PartRecordNumber# =  par:record_number
                                    !    Access:PARTS_ALIAS.ClearKey(par_ali:recordnumberkey)
                                    !    par_ali:record_number = PartRecordNumber#
                                    !    IF (access:PARTS_ALIAS.TryFetch(par_ali:recordnumberkey) = Level:Benign) THEN
                                    !        IF (Access:PARTS.Primerecord() = Level:Benign) THEN
                                    !            RecordNumberSave#   = par:Record_Number
                                    !            par:Record      :=: par_ali:Record
                                    !            par:Record_Number   = RecordNumberSave#
                                    !            par:Quantity    = par_ali:Quantity - orjtmp:Quantity
                                    !            IF (Access:PARTS.Tryinsert() <> Level:Benign) THEN
                                    !                Access:PARTS.Cancelautoinc()
                                    !            END
                                    !        END
                                    !    END
                                    !    ! Re-Fetch Current Record
                                    !    Access:PARTS.ClearKey(par:recordnumberkey)
                                    !    par:record_number = PartRecordNumber#
                                    !    IF (access:PARTS.TryFetch(par:recordnumberkey) = Level:Benign) THEN
                                    !        par:Quantity = orjtmp:Quantity
                                    !        par:Requested = False
                                    !        par:Order_Number = orp:Order_Number
                                    !        par:Date_Received = Today()
                                    !        par:Despatch_Note_Number = tmp:DespatchNoteNumber
                                    !        Access:PARTS.Update()
                                    !    END
                                    !ELSE
                                    !    par:Requested = False
                                    !    par:Order_Number = orp:Order_Number
                                    !    par:Date_Received = Today()
                                    !    par:Despatch_Note_Number = tmp:DespatchNoteNumber
                                    !    Access:PARTS.Update()
                                    !END
                                    !End Change 3372 BE(14/10/03)
                                    !Start Change 3372 BE(14/10/03)
                                    UpdateStockHistory(orjtmp:partnumber, orjtmp:quantity, orjtmp:jobnumber)
                                    !End Change 3372 BE(14/10/03)
                                    Do Check_Job_Status
                                Else! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                                    !Error
                                    Assert(0,'<13,10>Fetch Error. Cannot find Chargeable Part<13,10>')
                                End! If Access:PARTS.Tryfetch(par:Record_Number_Key) = Level:Benign
                                
                            Of 'W'
                                Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                                wpr:Record_Number   = orjtmp:RefNumber
                                If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                                    !Found
                                    !Start Change 3372 BE(14/10/03)
                                    ! Note: The following code is original.
                                    wpr:Requested = False
                                    wpr:Order_Number    = orp:Order_Number
                                    wpr:Date_Received   = Today()
                                    wpr:Despatch_Note_Number = tmp:DespatchNoteNumber
                                    Access:WARPARTS.Update()
                                    ! Note: The following amendment is for fulfilling Part Orders
                                    !       but has been removed because the "AMEND REQUEST" button actually
                                    !       amends the request and is not designed for PART ODER Function.
                                    !
                                    !IF ((diffaction# = 4) AND (orjtmp:Quantity < wpr:Quantity)) THEN
                                    !    ! Create New Record
                                    !    PartRecordNumber# =  wpr:record_number
                                    !    Access:WARPARTS_ALIAS.ClearKey(war_ali:recordnumberkey)
                                    !    war_ali:record_number = PartRecordNumber#
                                    !    IF (access:WARPARTS_ALIAS.TryFetch(war_ali:recordnumberkey) = Level:Benign) THEN
                                    !        IF (Access:WARPARTS.Primerecord() = Level:Benign) THEN
                                    !            RecordNumberSave#   = wpr:Record_Number
                                    !            wpr:Record      :=: war_ali:Record
                                    !            wpr:Record_Number   = RecordNumberSave#
                                    !            wpr:Quantity    = war_ali:Quantity - orjtmp:Quantity
                                    !            IF (Access:WARPARTS.Tryinsert() <> Level:Benign) THEN
                                    !                Access:WARPARTS.Cancelautoinc()
                                    !            END
                                    !        END
                                    !    END
                                    !    ! Re-Fetch Current Record
                                    !    Access:WARPARTS.ClearKey(wpr:recordnumberkey)
                                    !    wpr:record_number = PartRecordNumber#
                                    !    IF (access:WARPARTS.TryFetch(wpr:recordnumberkey) = Level:Benign) THEN
                                    !        wpr:Quantity = orjtmp:Quantity
                                    !        wpr:Requested = False
                                    !        wpr:Order_Number = orp:Order_Number
                                    !        wpr:Date_Received = Today()
                                    !        wpr:Despatch_Note_Number = tmp:DespatchNoteNumber
                                    !        Access:WARPARTS.Update()
                                    !    END
                                    !ELSE
                                    !    wpr:Requested = False
                                    !    wpr:Order_Number = orp:Order_Number
                                    !    wpr:Date_Received = Today()
                                    !    wpr:Despatch_Note_Number = tmp:DespatchNoteNumber
                                    !    Access:WARPARTS.Update()
                                    !END
                                    !End Change 3372 BE(14/10/03)
                                    Do Check_Job_Status
                                    !Start Change 3372 BE(14/10/03)
                                    UpdateStockHistory(orjtmp:partnumber, orjtmp:quantity, orjtmp:jobnumber)
                                    !End Change 3372 BE(14/10/03)
                                Else! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign
                                    !Error
                                    Assert(0,'<13,10>Fetch Error. Cannot find Warranty Part<13,10>')
                                End! If Access:WARPARTS.Tryfetch(wpr:Record_Number_Key) = Level:Benign

                        End!Case orjtmp:CharWarr
                        Delete(ORDJOBS)
                    End! If Access:ORDJOBS.Tryfetch(orjtmp:RecordNumberKey) = Level:Benign
                    
                End!Loop x# = 1 To Records(glo:Queue)
                Do DASBRW::7:DASUNTAGALL
                Do DASBRW::9:DASUNTAGALL

            End!If continue# = 1

        End !If error# = 0
    End !brw4.q.ord:All_Recieved = 'YES'
    
      BRW4.ResetSort(1)
      BRW5.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ProcessTaggedOrders, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'ReceivingStock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet1
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet1)
        OF 1
          ?List{PROP:FORMAT} ='128L(2)|M*~Part Number~@s30@#1#128L(2)|M*~Description~@s30@#6#56L(2)|M*~Purchase Cost~@n14.2@#11#56L(2)|M*~Sale Cost~@n14.2@#16#56L(2)|M*~Retail Cost~@n14.2@#21#73D(2)|M*~Quantity~L@p<<<<<<<#p@#26#'
          ?Tab1{PROP:TEXT} = 'Parts On Order'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List:2
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:2{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:2{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::7:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List:2{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    OF ?List:3
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List:3{PROPLIST:MouseDownRow} > 0) 
        CASE ?List:3{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::9:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG:2)
               ?List:3{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case Keycode()
          Of F2Key
              Post(Event:Accepted,?Change)
          Of F10Key
              Post(Event:Accepted,?ProcessTaggedOrders)
          Of F7Key
              Post(Event:Accepted,?DasTagAll)
          Of F8Key
              Post(Event:Accepted,?DasTagAll:2)
      End !Field()
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::7:DASUNTAGALL
      Do DASBRW::9:DASUNTAGALL
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:Timer
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
      tmp:StockTotal = 0
      Loop x# = 1 To Records(glo:Queue2)
          Get(glo:Queue2,x#)
          Access:ORDSTOCK.ClearKey(orstmp:RecordNumberKey)
          orstmp:RecordNumber = glo:Pointer2
          If Access:ORDSTOCK.TryFetch(orstmp:RecordNumberKey) = Level:Benign
              tmp:StockTotal += orstmp:Quantity
          End!If Access:ORDSTOCK.TryFetch(orstmp:RecordNumberKey) = Level:Benign
          Display(?tmp:StockTotal)
      End!Loop x# = 1 To Records(glo:Queue2)
      
      tmp:JobTotal = 0
      Loop y# = 1 To Records(glo:Queue)
          Get(glo:Queue,y#)
          Access:ORDJOBS.Clearkey(orjtmp:RecordNumberKey)
          orjtmp:RecordNumber = glo:Pointer
          If Access:ORDJOBS.Tryfetch(orjtmp:RecordNumberKey) = Level:Benign
              !Found
              tmp:JobTotal += orjtmp:Quantity
          End! If Access:ORDJOBS.Tryfetch(orjtmp:RecordNumberKey) = Level:Benign
          Display(?tmp:JobTotal)
      End!Loop y# = 1 To Records(glo:Queue)
      
      tmp:JobDifference = tmp:JobRequested - tmp:JobTotal
      tmp:StockDifference = tmp:StockRequested - tmp:StockTotal
      
      tmp:RequestedTotal = tmp:JobRequested + tmp:StockRequested
      tmp:FulfilledTotal = tmp:JobTotal + tmp:StockTotal
      tmp:RemainingTotal = tmp:JobDifference + tmp:StockDifference
      
      tmp:Remaining = orp:Quantity - tmp:StockTotal - tmp:JobTotal
      
      !Colours
      If tmp:JobDifference > 0
          ?tmp:JobDifference{prop:Fontcolor} = Color:Red
      Else!If tmp:JobDifference < 0
          ?tmp:JobDifference{prop:Fontcolor} = Color:Green
      End!If tmp:JobDifference < 0
      
      If tmp:StockDifference > 0
          ?tmp:StockDifference{prop:Fontcolor} = Color:Red
      Else!If tmp:StockDifference < 0
          ?tmp:StockDifference{prop:Fontcolor} = Color:Green
      End!If tmp:StockDifference < 0
      
      If tmp:RemainingTotal > 0
          ?tmp:RemainingTotal{prop:Fontcolor} = Color:Red
      Else!If tmp:RemainingTotal < 0
          ?tmp:RemainingTotal{prop:Fontcolor} = Color:Green
      End!If tmp:RemainingTotal < 0
      
      If tmp:Remaining > 0
          ?tmp:Remaining{prop:Fontcolor} = Color:Navy
      Else!If tmp:Remaining > 0
          If tmp:Remaining < 0
              ?tmp:Remaining{prop:Fontcolor} = Color:Red
          Else!If tmp:Remaining < 0
              ?tmp:Remaining{prop:Fontcolor} = Color:Green
          End!If tmp:Remaining < 0
      End!If tmp:Remaining > 0
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(Timer)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
CreateNewPart           Procedure(Long  func:Quantity)
    Code
    If Access:STOCK.Primerecord() = Level:Benign
        sto:Part_Number     = orp:Part_Number
        sto:Description     = orp:Description
        sto:Location        = MainStoreLocation()
        sto:Quantity_Stock  = func:Quantity
        sto:Purchase_Cost   = orp:Purchase_Cost
        sto:Sale_Cost       = orp:Sale_Cost
        sto:Retail_Cost     = orp:Retail_Cost
        Access:ORDERS.Clearkey(ord:Order_Number_Key)
        ord:Order_Number    = orp:Order_Number
        If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
            sto:Supplier    = ord:Supplier
        End! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
        If Access:STOCK.Tryinsert()
            Access:STOCK.Cancelautoinc()
        Else !If Access:STOCK.Tryinsert()
            Globalrequest = ChangeRecord
            AddNewPart(brw4.q.orp:Record_Number)
        End !If Access:STOCK.Tryinsert()
            
    End!If Access:STOCK.Primerecord() = Level:Benign
AddStockHistory             Procedure(Long  func:Quantity)
    Code
    If Access:STOHIST.Primerecord() = Level:Benign
        shi:Ref_Number              = sto:Ref_Number
        shi:Transaction_Type        = 'ADD'
        shi:Despatch_Note_Number    = orp:DespatchNoteNumber
        shi:Quantity                = func:Quantity
        shi:Date                    = Today()
        shi:Purchase_Cost           = sto:Purchase_Cost
        shi:Sale_Cost               = sto:Sale_Cost
        Access:USERS.Clearkey(use:Password_Key)
        use:Password                = glo:Password
        Access:USERS.Tryfetch(use:Password_Key)
        shi:User                    = use:User_Code
        shi:Notes                   = 'STOCK ADDED FROM ORDER'
        shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                        '<13,10>DESPATCH NOTE NO: ' & Clip(orp:DespatchNoteNumber)
        If Access:STOHIST.Tryinsert()
            Access:STOHIST.Cancelautoinc()
        End!If Access:STOHIST.Tryinsert()
    End!If Access:STOHIST.Primerecord() = Level:Benign
DecStockHistory PROCEDURE(arg_qty, arg_jobnumber)
    CODE
    IF (access:STOHIST.primerecord() = Level:Benign) THEN
        shi:ref_number = sto:ref_number
        shi:transaction_type = 'DEC'
        shi:despatch_note_number = orp:despatchnotenumber
        shi:quantity = arg_qty
        shi:Date = Today()
        shi:purchase_cost = sto:purchase_cost
        shi:sale_cost = sto:sale_cost
        shi:job_number = arg_jobnumber
        access:users.clearkey(use:password_key)
        use:password = glo:password
        access:users.tryfetch(use:password_key)
        shi:user = use:user_code
        shi:notes = 'STOCK DECREMENTED'
        IF (access:STOHIST.tryinsert()) THEN
            access:STOHIST.cancelautoinc()
        END
    END
    RETURN
UpdateStockHistory  PROCEDURE(arg_partno, arg_qty, arg_jobnumber)
    CODE
    ! Try main location first
    access:STOCK.ClearKey(sto:location_key)
    sto:location = MainStoreLocation()
    sto:part_number = arg_partno
    IF (access:STOCK.tryfetch(sto:location_key) = Level:Benign) THEN
        AddStockHistory(arg_qty)
        DecStockHistory(arg_qty, arg_jobnumber)
    ELSE
        ! Otherwise try each location in turn
        save_loc_id = access:location.SaveFile()
        LOOP
            IF (access:location.next() <> Level:Benign) THEN
                BREAK
            END
            access:stock.clearkey(sto:location_key)
            sto:location = loc:location
            sto:part_number = orp:part_number
            IF (access:stock.tryfetch(sto:location_key) = Level:Benign) THEN
                AddStockHistory(arg_qty)
                DecStockHistory(arg_qty, arg_jobnumber)
                BREAK
            END
        END
        access:location.RestoreFile(save_loc_id)
    END
    RETURN

BRW4.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW4.SetQueueRecord PROCEDURE

  CODE
  PARENT.SetQueueRecord
  SELF.Q.orp:Part_Number_NormalFG = -1
  SELF.Q.orp:Part_Number_NormalBG = -1
  SELF.Q.orp:Part_Number_SelectedFG = -1
  SELF.Q.orp:Part_Number_SelectedBG = -1
  SELF.Q.orp:Description_NormalFG = -1
  SELF.Q.orp:Description_NormalBG = -1
  SELF.Q.orp:Description_SelectedFG = -1
  SELF.Q.orp:Description_SelectedBG = -1
  SELF.Q.orp:Purchase_Cost_NormalFG = -1
  SELF.Q.orp:Purchase_Cost_NormalBG = -1
  SELF.Q.orp:Purchase_Cost_SelectedFG = -1
  SELF.Q.orp:Purchase_Cost_SelectedBG = -1
  SELF.Q.orp:Sale_Cost_NormalFG = -1
  SELF.Q.orp:Sale_Cost_NormalBG = -1
  SELF.Q.orp:Sale_Cost_SelectedFG = -1
  SELF.Q.orp:Sale_Cost_SelectedBG = -1
  SELF.Q.orp:Retail_Cost_NormalFG = -1
  SELF.Q.orp:Retail_Cost_NormalBG = -1
  SELF.Q.orp:Retail_Cost_SelectedFG = -1
  SELF.Q.orp:Retail_Cost_SelectedBG = -1
  SELF.Q.orp:Quantity_NormalFG = -1
  SELF.Q.orp:Quantity_NormalBG = -1
  SELF.Q.orp:Quantity_SelectedFG = -1
  SELF.Q.orp:Quantity_SelectedBG = -1
  SELF.Q.orp:Record_Number_NormalFG = -1
  SELF.Q.orp:Record_Number_NormalBG = -1
  SELF.Q.orp:Record_Number_SelectedFG = -1
  SELF.Q.orp:Record_Number_SelectedBG = -1
  SELF.Q.orp:All_Received_NormalFG = -1
  SELF.Q.orp:All_Received_NormalBG = -1
  SELF.Q.orp:All_Received_SelectedFG = -1
  SELF.Q.orp:All_Received_SelectedBG = -1
   
   
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:Part_Number_NormalFG = 255
     SELF.Q.orp:Part_Number_NormalBG = 16777215
     SELF.Q.orp:Part_Number_SelectedFG = 16777215
     SELF.Q.orp:Part_Number_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:Part_Number_NormalFG = 8421504
     SELF.Q.orp:Part_Number_NormalBG = 16777215
     SELF.Q.orp:Part_Number_SelectedFG = 16777215
     SELF.Q.orp:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Part_Number_NormalFG = -1
     SELF.Q.orp:Part_Number_NormalBG = -1
     SELF.Q.orp:Part_Number_SelectedFG = -1
     SELF.Q.orp:Part_Number_SelectedBG = -1
   END
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:Description_NormalFG = 255
     SELF.Q.orp:Description_NormalBG = 16777215
     SELF.Q.orp:Description_SelectedFG = 16777215
     SELF.Q.orp:Description_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:Description_NormalFG = 8421504
     SELF.Q.orp:Description_NormalBG = 16777215
     SELF.Q.orp:Description_SelectedFG = 16777215
     SELF.Q.orp:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Description_NormalFG = -1
     SELF.Q.orp:Description_NormalBG = -1
     SELF.Q.orp:Description_SelectedFG = -1
     SELF.Q.orp:Description_SelectedBG = -1
   END
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:Purchase_Cost_NormalFG = 255
     SELF.Q.orp:Purchase_Cost_NormalBG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedFG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:Purchase_Cost_NormalFG = 8421504
     SELF.Q.orp:Purchase_Cost_NormalBG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedFG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Purchase_Cost_NormalFG = -1
     SELF.Q.orp:Purchase_Cost_NormalBG = -1
     SELF.Q.orp:Purchase_Cost_SelectedFG = -1
     SELF.Q.orp:Purchase_Cost_SelectedBG = -1
   END
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:Sale_Cost_NormalFG = 255
     SELF.Q.orp:Sale_Cost_NormalBG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedFG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:Sale_Cost_NormalFG = 8421504
     SELF.Q.orp:Sale_Cost_NormalBG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedFG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Sale_Cost_NormalFG = -1
     SELF.Q.orp:Sale_Cost_NormalBG = -1
     SELF.Q.orp:Sale_Cost_SelectedFG = -1
     SELF.Q.orp:Sale_Cost_SelectedBG = -1
   END
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:Retail_Cost_NormalFG = 255
     SELF.Q.orp:Retail_Cost_NormalBG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedFG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:Retail_Cost_NormalFG = 8421504
     SELF.Q.orp:Retail_Cost_NormalBG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedFG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Retail_Cost_NormalFG = -1
     SELF.Q.orp:Retail_Cost_NormalBG = -1
     SELF.Q.orp:Retail_Cost_SelectedFG = -1
     SELF.Q.orp:Retail_Cost_SelectedBG = -1
   END
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:Quantity_NormalFG = 255
     SELF.Q.orp:Quantity_NormalBG = 16777215
     SELF.Q.orp:Quantity_SelectedFG = 16777215
     SELF.Q.orp:Quantity_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:Quantity_NormalFG = 8421504
     SELF.Q.orp:Quantity_NormalBG = 16777215
     SELF.Q.orp:Quantity_SelectedFG = 16777215
     SELF.Q.orp:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Quantity_NormalFG = -1
     SELF.Q.orp:Quantity_NormalBG = -1
     SELF.Q.orp:Quantity_SelectedFG = -1
     SELF.Q.orp:Quantity_SelectedBG = -1
   END
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:Record_Number_NormalFG = 255
     SELF.Q.orp:Record_Number_NormalBG = 16777215
     SELF.Q.orp:Record_Number_SelectedFG = 16777215
     SELF.Q.orp:Record_Number_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:Record_Number_NormalFG = 8421504
     SELF.Q.orp:Record_Number_NormalBG = 16777215
     SELF.Q.orp:Record_Number_SelectedFG = 16777215
     SELF.Q.orp:Record_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Record_Number_NormalFG = -1
     SELF.Q.orp:Record_Number_NormalBG = -1
     SELF.Q.orp:Record_Number_SelectedFG = -1
     SELF.Q.orp:Record_Number_SelectedBG = -1
   END
   IF (orp:All_Received <> 'YES')
     SELF.Q.orp:All_Received_NormalFG = 255
     SELF.Q.orp:All_Received_NormalBG = 16777215
     SELF.Q.orp:All_Received_SelectedFG = 16777215
     SELF.Q.orp:All_Received_SelectedBG = 255
   ELSIF(orp:All_Received = 'YES')
     SELF.Q.orp:All_Received_NormalFG = 8421504
     SELF.Q.orp:All_Received_NormalBG = 16777215
     SELF.Q.orp:All_Received_SelectedFG = 16777215
     SELF.Q.orp:All_Received_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:All_Received_NormalFG = -1
     SELF.Q.orp:All_Received_NormalBG = -1
     SELF.Q.orp:All_Received_SelectedFG = -1
     SELF.Q.orp:All_Received_SelectedBG = -1
   END


BRW4.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?AmendJobRequest
    SELF.DeleteControl=?DeleteJobRequest
  END


BRW5.ResetFromView PROCEDURE

tmp:JobRequested:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:ORDJOBS.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:JobRequested:Sum += orjtmp:Quantity
  END
  tmp:JobRequested = tmp:JobRequested:Sum
  PARENT.ResetFromView
  Relate:ORDJOBS.SetQuickScan(0)
  SETCURSOR()


BRW5.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = orjtmp:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    IF ERRORCODE()
      tmp:TagJob = ''
    ELSE
      tmp:TagJob = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:TagJob= '*')
    SELF.Q.tmp:TagJob_Icon = 3
  ELSE
    SELF.Q.tmp:TagJob_Icon = 2
  END
  IF (orjtmp:CharWarr = 'C')
    SELF.Q.tmp:Chargeable_Icon = 1
  ELSE
    SELF.Q.tmp:Chargeable_Icon = 0
  END
  IF (orjtmp:CharWarr = 'W')
    SELF.Q.tmp:Warranty_Icon = 1
  ELSE
    SELF.Q.tmp:Warranty_Icon = 0
  END


BRW5.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW5.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW5::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW5::RecordStatus=ReturnValue
  IF BRW5::RecordStatus NOT=Record:OK THEN RETURN BRW5::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue.Pointer = orjtmp:RecordNumber
     GET(glo:Queue,glo:Queue.Pointer)
    EXECUTE DASBRW::7:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW5::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW5::RecordStatus
  RETURN ReturnValue


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:2
    SELF.ChangeControl=?AmendStockRequest
    SELF.DeleteControl=?DeleteStockRequest
  END


BRW6.ResetFromView PROCEDURE

tmp:StockRequested:Sum REAL
  CODE
  SETCURSOR(Cursor:Wait)
  Relate:ORDSTOCK.SetQuickScan(1)
  SELF.Reset
  LOOP
    CASE SELF.Next()
    OF Level:Notify
      BREAK
    OF Level:Fatal
      RETURN
    END
    SELF.SetQueueRecord
    tmp:StockRequested:Sum += orstmp:Quantity
  END
  tmp:StockRequested = tmp:StockRequested:Sum
  PARENT.ResetFromView
  Relate:ORDSTOCK.SetQuickScan(0)
  SETCURSOR()


BRW6.SetQueueRecord PROCEDURE

  CODE
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = orstmp:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    IF ERRORCODE()
      tmp:TagStock = ''
    ELSE
      tmp:TagStock = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  PARENT.SetQueueRecord
  IF (tmp:TagStock = '*')
    SELF.Q.tmp:TagStock_Icon = 2
  ELSE
    SELF.Q.tmp:TagStock_Icon = 1
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     glo:Queue2.Pointer2 = orstmp:RecordNumber
     GET(glo:Queue2,glo:Queue2.Pointer2)
    EXECUTE DASBRW::9:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
