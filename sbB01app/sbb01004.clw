

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01004.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Order PROCEDURE                                 !Generated from procedure template - Window

LocalRequest         LONG
Average_Text_Temp    STRING(8)
save_shi_id          USHORT,AUTO
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
sale_cost_temp       REAL
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
supplier_temp        STRING(30)
Days_7_Temp          LONG
Days_30_Temp         LONG
Days_60_Temp         LONG
Days_90_Temp         LONG
Average_Temp         LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?supplier_temp
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB5::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('Stock Order'),AT(,,359,119),FONT('Tahoma',8,,),CENTER,ICON('PC.ICO'),TILED,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,84),USE(?Sheet1),SPREAD
                         TAB('Stock Order'),USE(?Tab1)
                           PROMPT('Quantity'),AT(9,20),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(85,20,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR,RANGE(1,99999999)
                           PROMPT('Supplier'),AT(9,36),USE(?Prompt3)
                           COMBO(@s30),AT(85,36,124,10),USE(supplier_temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo)
                           PROMPT('Reason'),AT(9,52),USE(?Additional_Notes_Temp:Prompt),HIDE
                           TEXT,AT(85,52,124,28),USE(additional_notes_temp),HIDE,FONT(,,,FONT:bold),UPR
                         END
                       END
                       SHEET,AT(220,4,136,84),USE(?Sheet2),SPREAD
                         TAB('Stock Usage'),USE(?Tab2)
                           PROMPT('0 - 7 Days'),AT(224,20),USE(?Prompt5)
                           STRING(@n-14),AT(280,20),USE(Days_7_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(224,32),USE(?Prompt5:2)
                           STRING(@n-14),AT(280,32),USE(Days_30_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('31 - 60 Days'),AT(224,44),USE(?Prompt5:3)
                           STRING(@n-14),AT(280,44),USE(Days_60_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('61 - 90 Days'),AT(224,56),USE(?Prompt5:4)
                           STRING(@n-14),AT(280,56),USE(Days_90_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(284,66,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Average Daily Use'),AT(224,72),USE(?Prompt5:5)
                           STRING(@s8),AT(312,72),USE(Average_Text_Temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       PANEL,AT(4,92,352,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(240,96,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(296,96,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB5                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?quantity_temp{prop:ReadOnly} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 15066597
    Elsif ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 8454143
    Else ! If ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 16777215
    End ! If ?quantity_temp{prop:Req} = True
    ?quantity_temp{prop:Trn} = 0
    ?quantity_temp{prop:FontStyle} = font:Bold
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    If ?supplier_temp{prop:ReadOnly} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 15066597
    Elsif ?supplier_temp{prop:Req} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 8454143
    Else ! If ?supplier_temp{prop:Req} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 16777215
    End ! If ?supplier_temp{prop:Req} = True
    ?supplier_temp{prop:Trn} = 0
    ?supplier_temp{prop:FontStyle} = font:Bold
    ?Additional_Notes_Temp:Prompt{prop:FontColor} = -1
    ?Additional_Notes_Temp:Prompt{prop:Color} = 15066597
    If ?additional_notes_temp{prop:ReadOnly} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 15066597
    Elsif ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 8454143
    Else ! If ?additional_notes_temp{prop:Req} = True
        ?additional_notes_temp{prop:FontColor} = 65793
        ?additional_notes_temp{prop:Color} = 16777215
    End ! If ?additional_notes_temp{prop:Req} = True
    ?additional_notes_temp{prop:Trn} = 0
    ?additional_notes_temp{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Days_7_Temp{prop:FontColor} = -1
    ?Days_7_Temp{prop:Color} = 15066597
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    ?Days_30_Temp{prop:FontColor} = -1
    ?Days_30_Temp{prop:Color} = 15066597
    ?Prompt5:3{prop:FontColor} = -1
    ?Prompt5:3{prop:Color} = 15066597
    ?Days_60_Temp{prop:FontColor} = -1
    ?Days_60_Temp{prop:Color} = 15066597
    ?Prompt5:4{prop:FontColor} = -1
    ?Prompt5:4{prop:Color} = 15066597
    ?Days_90_Temp{prop:FontColor} = -1
    ?Days_90_Temp{prop:Color} = 15066597
    ?Prompt5:5{prop:FontColor} = -1
    ?Prompt5:5{prop:Color} = 15066597
    ?Average_Text_Temp{prop:FontColor} = -1
    ?Average_Text_Temp{prop:Color} = 15066597
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Order',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Stock_Order',1)
    SolaceViewVars('Average_Text_Temp',Average_Text_Temp,'Stock_Order',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Stock_Order',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Order',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Stock_Order',1)
    SolaceViewVars('purchase_cost_Temp',purchase_cost_Temp,'Stock_Order',1)
    SolaceViewVars('sale_cost_temp',sale_cost_temp,'Stock_Order',1)
    SolaceViewVars('date_received_temp',date_received_temp,'Stock_Order',1)
    SolaceViewVars('additional_notes_temp',additional_notes_temp,'Stock_Order',1)
    SolaceViewVars('despatch_note_temp',despatch_note_temp,'Stock_Order',1)
    SolaceViewVars('supplier_temp',supplier_temp,'Stock_Order',1)
    SolaceViewVars('Days_7_Temp',Days_7_Temp,'Stock_Order',1)
    SolaceViewVars('Days_30_Temp',Days_30_Temp,'Stock_Order',1)
    SolaceViewVars('Days_60_Temp',Days_60_Temp,'Stock_Order',1)
    SolaceViewVars('Days_90_Temp',Days_90_Temp,'Stock_Order',1)
    SolaceViewVars('Average_Temp',Average_Temp,'Stock_Order',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_temp;  SolaceCtrlName = '?quantity_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?supplier_temp;  SolaceCtrlName = '?supplier_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Additional_Notes_Temp:Prompt;  SolaceCtrlName = '?Additional_Notes_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?additional_notes_temp;  SolaceCtrlName = '?additional_notes_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_7_Temp;  SolaceCtrlName = '?Days_7_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_30_Temp;  SolaceCtrlName = '?Days_30_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:3;  SolaceCtrlName = '?Prompt5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_60_Temp;  SolaceCtrlName = '?Days_60_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:4;  SolaceCtrlName = '?Prompt5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Days_90_Temp;  SolaceCtrlName = '?Days_90_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:5;  SolaceCtrlName = '?Prompt5:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Average_Text_Temp;  SolaceCtrlName = '?Average_Text_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Order')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Order')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Prompt7
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:DEFAULTS.Open
  Relate:ORDPEND.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  FDCB5.Init(supplier_temp,?supplier_temp,Queue:FileDropCombo.ViewPosition,FDCB5::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB5.Q &= Queue:FileDropCombo
  FDCB5.AddSortOrder(sup:Company_Name_Key)
  FDCB5.AddField(sup:Company_Name,FDCB5.Q.sup:Company_Name)
  FDCB5.AddField(sup:RecordNumber,FDCB5.Q.sup:RecordNumber)
  FDCB5.AddUpdateField(sup:Company_Name,supplier_temp)
  ThisWindow.AddItem(FDCB5.WindowComponent)
  FDCB5.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:DEFAULTS.Close
    Relate:ORDPEND.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Order',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      !Now for the new parts ordering
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number    = glo:select1
      If access:stock.fetch(sto:ref_number_key) = Level:Benign
          Case def:SummaryOrders
              Of 0 !Old Fashioned ORders
                  If access:ordpend.primerecord() = Level:Benign
                      ope:part_ref_number = sto:ref_number
                      ope:part_type       = 'STO'
                      ope:supplier        = supplier_temp
                      ope:part_number     = sto:part_number
                      ope:Description     = sto:description
                      ope:job_number      = ''
                      ope:quantity        = quantity_temp
                      ope:Reason          = Additional_Notes_Temp
                      access:ordpend.insert()
                  End !If access:ordpend.primerecord() = Level:Benign
      
              Of 1 !New ORders
                  MakePartsRequest(Supplier_Temp,sto:Part_Number,sto:Description,Quantity_Temp)
      
      !            Access:ORDPEND.ClearKey(ope:Supplier_Key)
      !            ope:Supplier    = Supplier_Temp
      !            ope:Part_Number = sto:Part_Number
      !            If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
      !                ope:quantity += Quantity_Temp
      !                Access:ORDPEND.Update()
      !            Else!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
      !                If access:ordpend.primerecord() = Level:Benign
      !                    ope:part_ref_number = sto:ref_number
      !                    ope:part_type       = 'STO'
      !                    ope:supplier        = supplier_temp
      !                    ope:part_number     = sto:part_number
      !                    ope:Description     = sto:description
      !                    ope:job_number      = ''
      !                    ope:quantity        = quantity_temp
      !                    access:ordpend.insert()
      !                End !If access:ordpend.primerecord() = Level:Benign
      !            End!If Access:ORDPEND.TryFetch(ope:Supplier_Key) = Level:Benign
                  sto:QuantityRequested += Quantity_Temp
                  Access:STOCK.Update()
      
          End!Case def:SummaryOrders
      
          !sto:quantity_to_order += quantity_temp
          !sto:pending_ref_number   = ope:ref_number
          !access:stock.update()
          !I don't understand why I was equating a sto:pending_ref_number field....the same part can be
          !ordered many times. Why? Why? Why? Brain broken again no doubt.
      End!If access:stock.fetch(sto:ref_number_key) = Level:Benign
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    OF ?CancelButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CancelButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Order')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:stock.clearkey(sto:ref_number_key)
      sto:ref_number    = glo:select1
      If access:stock.fetch(sto:ref_number_key)
          Return(Level:Fatal)    
      End!If access:stock.fetch(sto:ref_number_key)
      ref_number# = sto:ref_number
      Include('stockuse.inc')
      
      quantity_temp           = 1
      date_received_temp      = Today()
      purchase_cost_temp      = sto:purchase_cost
      sale_cost_temp          = sto:sale_cost
      supplier_temp           = sto:supplier
      Display()
      
      Set(DEFAULTS)
      Access:DEFAULTS.Next()
      Case def:SummaryOrders
          Of 0
              ?Additional_Notes_Temp{prop:Hide} = 0
              ?Additional_Notes_Temp:Prompt{prop:Hide} = 0
          Of 1
              ?Additional_Notes_Temp{prop:Hide} = 1
              ?Additional_Notes_Temp:Prompt{prop:Hide} = 1
      End !def:SummaryOrders
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

