

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01034.INC'),ONCE        !Local module procedure declarations
                     END


AmendJobRequest PROCEDURE                             !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
sav:Quantity         LONG
window               WINDOW('Amend Job Request'),AT(,,220,91),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,56),USE(?Sheet1),SPREAD
                         TAB('Amend Job Request'),USE(?Tab1)
                           PROMPT('Location'),AT(8,20),USE(?orjtmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s8),AT(84,20,124,10),USE(orjtmp:JobNumber),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),UPR,READONLY
                           PROMPT('Quantity'),AT(8,36),USE(?orjtmp:Quantity:Prompt),TRN
                           SPIN(@n8),AT(84,36,64,10),USE(orjtmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity'),TIP('Quantity'),UPR,RANGE(1,99999),STEP(1)
                         END
                       END
                       PANEL,AT(4,64,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,68,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(156,68,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?orjtmp:Location:Prompt{prop:FontColor} = -1
    ?orjtmp:Location:Prompt{prop:Color} = 15066597
    If ?orjtmp:JobNumber{prop:ReadOnly} = True
        ?orjtmp:JobNumber{prop:FontColor} = 65793
        ?orjtmp:JobNumber{prop:Color} = 15066597
    Elsif ?orjtmp:JobNumber{prop:Req} = True
        ?orjtmp:JobNumber{prop:FontColor} = 65793
        ?orjtmp:JobNumber{prop:Color} = 8454143
    Else ! If ?orjtmp:JobNumber{prop:Req} = True
        ?orjtmp:JobNumber{prop:FontColor} = 65793
        ?orjtmp:JobNumber{prop:Color} = 16777215
    End ! If ?orjtmp:JobNumber{prop:Req} = True
    ?orjtmp:JobNumber{prop:Trn} = 0
    ?orjtmp:JobNumber{prop:FontStyle} = font:Bold
    ?orjtmp:Quantity:Prompt{prop:FontColor} = -1
    ?orjtmp:Quantity:Prompt{prop:Color} = 15066597
    If ?orjtmp:Quantity{prop:ReadOnly} = True
        ?orjtmp:Quantity{prop:FontColor} = 65793
        ?orjtmp:Quantity{prop:Color} = 15066597
    Elsif ?orjtmp:Quantity{prop:Req} = True
        ?orjtmp:Quantity{prop:FontColor} = 65793
        ?orjtmp:Quantity{prop:Color} = 8454143
    Else ! If ?orjtmp:Quantity{prop:Req} = True
        ?orjtmp:Quantity{prop:FontColor} = 65793
        ?orjtmp:Quantity{prop:Color} = 16777215
    End ! If ?orjtmp:Quantity{prop:Req} = True
    ?orjtmp:Quantity{prop:Trn} = 0
    ?orjtmp:Quantity{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AmendJobRequest',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('ActionMessage',ActionMessage,'AmendJobRequest',1)
    SolaceViewVars('sav:Quantity',sav:Quantity,'AmendJobRequest',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:Location:Prompt;  SolaceCtrlName = '?orjtmp:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:JobNumber;  SolaceCtrlName = '?orjtmp:JobNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:Quantity:Prompt;  SolaceCtrlName = '?orjtmp:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orjtmp:Quantity;  SolaceCtrlName = '?orjtmp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AmendJobRequest')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'AmendJobRequest')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orjtmp:Location:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:ORDJOBS)
  Relate:ORDJOBS.Open
  Relate:PARTS.Open
  Access:WARPARTS.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDJOBS
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  sav:Quantity    = orstmp:Quantity
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDJOBS.Close
    Relate:PARTS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'AmendJobRequest',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF orjtmp:Quantity <> sav:Quantity
      Case MessageEx('Are you sure you want to change the Quantity Requested?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Case orjtmp:CharWarr
                  Of 'C'
                      Access:PARTS.ClearKey(par:RequestedKey)
                      par:Requested   = True
                      par:Part_Number = orjtmp:PartNumber
                      If Access:PARTS.TryFetch(par:RequestedKey) = Level:Benign
                          par:Quantity    = orjtmp:Quantity
                          Access:PARTS.Update()
                      End!If Access:PARTS.TryFetch(par:RequestedKey) = Level:Benign
                  Of 'W'
                      Access:WARPARTS.ClearKey(par:RequestedKey)
                      wpr:Requested   = True
                      wpr:Part_Number = orjtmp:PartNumber
                      If Access:WARPARTS.TryFetch(wpr:RequestedKey) = Level:Benign
                          wpr:Quantity    = orjtmp:Quantity
                          Access:WARPARTS.Update()
                      End!If Access:PARTS.TryFetch(par:RequestedKey) = Level:Benign
  
  
              End!Case orjtmp:CharWarr
          Of 2 ! &No Button
              orjtmp:Quantity = sav:Quantity
              Display(?orjtmp:Quantity)
              Cycle
      End!Case MessageEx
  End!orstmp:Quantity <> sav:Quantity
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'AmendJobRequest')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

