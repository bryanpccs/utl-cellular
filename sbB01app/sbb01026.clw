

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01026.INC'),ONCE        !Local module procedure declarations
                     END


Receive_Order PROCEDURE                               !Generated from procedure template - Window

LocalRequest         LONG
save_stm_id          USHORT,AUTO
save_ccp_id          USHORT,AUTO
sav:partnumber       STRING(30)
sav:description      STRING(30)
sav:purchase_cost    REAL
sav:sale_cost        REAL
sav:retail_cost      REAL
sav:ShelfLocation    STRING(30)
sav:SecondLocation   STRING(30)
save_ope_id          USHORT,AUTO
save_orp_ali_id      USHORT,AUTO
save_shi_id          USHORT,AUTO
average_temp         LONG
save_sto_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
tmp:CostsChanged     BYTE(0)
save_res_id          USHORT,AUTO
FilesOpened          BYTE
quantity_temp        LONG
purchase_cost_Temp   REAL
save_cwp_id          USHORT,AUTO
sale_cost_temp       REAL
pos                  STRING(255)
date_received_temp   DATE
additional_notes_temp STRING(255)
despatch_note_temp   STRING(30)
percentage_mark_up_temp REAL
parts_record_number_Temp REAL
warparts_record_number_temp REAL
retstock_record_number_temp LONG
no_of_labels_temp    REAL
retail_cost_temp     REAL
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
Average_text_temp    STRING(8)
tmp:quantitytoorder  LONG
tmp:quantityonorder  LONG
tmp:PartNumber       STRING(30)
tmp:Description      STRING(30)
tmp:SaleCost         REAL
tmp:PurchaseCost     REAL
tmp:RetailCost       REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
tmp:ShelfLocation    STRING(30)
tmp:SecondLocation   STRING(30)
window               WINDOW('Amend Item On Parts Order'),AT(,,371,243),FONT('Tahoma',8,,),CENTER,ICON('Pc.ico'),ALRT(F9Key),TILED,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,224,208),USE(?Sheet1),SPREAD
                         TAB('Item Details'),USE(?Tab1)
                           PROMPT('Location'),AT(8,20),USE(?STO:Location:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(sto:Location),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(,COLOR:Black,COLOR:Silver),UPR,READONLY
                           PROMPT('Shelf Location'),AT(8,36),USE(?STO:Shelf_Location:Prompt),TRN
                           ENTRY(@s30),AT(84,36,124,10),USE(tmp:ShelfLocation),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                           BUTTON,AT(212,36,10,10),USE(?LookupShelfLocation),SKIP,HIDE,ICON('List3.ico')
                           PROMPT('2nd Location'),AT(8,52),USE(?STO:Second_Location:Prompt),TRN
                           ENTRY(@s30),AT(84,52,124,10),USE(tmp:SecondLocation),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           PROMPT('Part Number'),AT(8,68),USE(?tmp:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(tmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Description'),AT(8,84),USE(?tmp:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(tmp:Description),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR
                           PROMPT('Quantity'),AT(8,100),USE(?Prompt7)
                           SPIN(@p<<<<<<<#p),AT(84,100,64,10),USE(quantity_temp),RIGHT,FONT('Tahoma',8,,FONT:bold),UPR,RANGE(1,99999999)
                           PROMPT('Despatch Note'),AT(8,116),USE(?despatch_note_temp:prompt)
                           ENTRY(@s30),AT(84,116,124,10),USE(despatch_note_temp),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Purchase Cost'),AT(8,132),USE(?Select_Global3:Prompt)
                           ENTRY(@n14.2),AT(84,132,64,10),USE(tmp:PurchaseCost),FONT('Tahoma',8,,FONT:bold)
                           PROMPT('Trade Cost'),AT(8,148),USE(?Select_Global4:Prompt)
                           ENTRY(@n14.2),AT(84,148,64,10),USE(tmp:SaleCost),FONT('Tahoma',8,,FONT:bold)
                           PROMPT('Retail Cost'),AT(8,164),USE(?retail_cost_temp:Prompt),TRN
                           ENTRY(@n14.2),AT(84,164,64,10),USE(tmp:RetailCost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Date Received'),AT(8,180),USE(?Select_Global5:Prompt)
                           ENTRY(@d6b),AT(84,180,64,10),USE(date_received_temp),FONT('Tahoma',,,FONT:bold)
                           PROMPT('Number Of Labels'),AT(8,196),USE(?no_of_labels:Prompt),TRN
                           SPIN(@s6),AT(84,196,64,10),USE(no_of_labels_temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,STEP(1)
                           BUTTON,AT(152,180,10,10),USE(?PopCalendar),SKIP,ICON('calenda2.ico')
                         END
                       END
                       SHEET,AT(232,4,136,208),USE(?Sheet5),SPREAD
                         TAB('Stock Details'),USE(?Tab7)
                           PROMPT('Quantity In Stock'),AT(236,20),USE(?Prompt16)
                           STRING(@N8),AT(317,20),USE(sto:Quantity_Stock),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('Quantity To Order'),AT(236,36),USE(?Prompt16:2)
                           STRING(@n-14),AT(292,36),USE(tmp:quantitytoorder),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('Quantity On Order'),AT(236,52),USE(?Prompt16:3)
                           STRING(@n-14),AT(292,52),USE(tmp:quantityonorder),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('Stock Usage'),AT(236,72),USE(?Prompt19)
                           PROMPT('0 - 7 Days'),AT(236,84),USE(?Prompt5)
                           STRING(@n-14),AT(292,84),USE(days_7_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(236,96),USE(?Prompt5:2)
                           STRING(@n-14),AT(292,96),USE(days_30_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('31 - 60 Days'),AT(236,108),USE(?Prompt5:3)
                           STRING(@n-14),AT(292,108),USE(days_60_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('61 - 90 Days'),AT(236,120),USE(?Prompt5:4)
                           STRING(@n-14),AT(292,120),USE(days_90_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(295,132,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Average Daily Use'),AT(236,136),USE(?Prompt5:5)
                           STRING(@s8),AT(324,136),USE(Average_text_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Order Reason'),AT(236,156),USE(?orp:Reason:Prompt),HIDE
                           TEXT,AT(236,164,128,44),USE(orp:Reason),SKIP,HIDE,VSCROLL,FONT(,,,FONT:bold),READONLY,MSG('Reason')
                         END
                       END
                       PANEL,AT(4,216,364,24),USE(?Panel2),FILL(COLOR:Silver)
                       BUTTON('Previous Despatch No. (F9)'),AT(8,220,88,16),USE(?PreviousDespatch),SKIP,LEFT,ICON(ICON:PrevPage)
                       BUTTON('&OK'),AT(252,220,56,16),USE(?OkButton),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(308,220,56,16),USE(?CancelButton),LEFT,ICON('Cancel.gif'),STD(STD:Close)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_par_id   ushort,auto
save_wpr_id   ushort,auto
    MAP            ! Start Change 3505 BE(10/11/03)
UpdateStockHistory  PROCEDURE(Long, Long, String, Long, String, Long)
    END
! End Change 3505 BE(10/11/03)
!Save Entry Fields Incase Of Lookup
look:tmp:ShelfLocation                Like(tmp:ShelfLocation)
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?STO:Location:Prompt{prop:FontColor} = -1
    ?STO:Location:Prompt{prop:Color} = 15066597
    If ?sto:Location{prop:ReadOnly} = True
        ?sto:Location{prop:FontColor} = 65793
        ?sto:Location{prop:Color} = 15066597
    Elsif ?sto:Location{prop:Req} = True
        ?sto:Location{prop:FontColor} = 65793
        ?sto:Location{prop:Color} = 8454143
    Else ! If ?sto:Location{prop:Req} = True
        ?sto:Location{prop:FontColor} = 65793
        ?sto:Location{prop:Color} = 16777215
    End ! If ?sto:Location{prop:Req} = True
    ?sto:Location{prop:Trn} = 0
    ?sto:Location{prop:FontStyle} = font:Bold
    ?STO:Shelf_Location:Prompt{prop:FontColor} = -1
    ?STO:Shelf_Location:Prompt{prop:Color} = 15066597
    If ?tmp:ShelfLocation{prop:ReadOnly} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 15066597
    Elsif ?tmp:ShelfLocation{prop:Req} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 8454143
    Else ! If ?tmp:ShelfLocation{prop:Req} = True
        ?tmp:ShelfLocation{prop:FontColor} = 65793
        ?tmp:ShelfLocation{prop:Color} = 16777215
    End ! If ?tmp:ShelfLocation{prop:Req} = True
    ?tmp:ShelfLocation{prop:Trn} = 0
    ?tmp:ShelfLocation{prop:FontStyle} = font:Bold
    ?STO:Second_Location:Prompt{prop:FontColor} = -1
    ?STO:Second_Location:Prompt{prop:Color} = 15066597
    If ?tmp:SecondLocation{prop:ReadOnly} = True
        ?tmp:SecondLocation{prop:FontColor} = 65793
        ?tmp:SecondLocation{prop:Color} = 15066597
    Elsif ?tmp:SecondLocation{prop:Req} = True
        ?tmp:SecondLocation{prop:FontColor} = 65793
        ?tmp:SecondLocation{prop:Color} = 8454143
    Else ! If ?tmp:SecondLocation{prop:Req} = True
        ?tmp:SecondLocation{prop:FontColor} = 65793
        ?tmp:SecondLocation{prop:Color} = 16777215
    End ! If ?tmp:SecondLocation{prop:Req} = True
    ?tmp:SecondLocation{prop:Trn} = 0
    ?tmp:SecondLocation{prop:FontStyle} = font:Bold
    ?tmp:PartNumber:Prompt{prop:FontColor} = -1
    ?tmp:PartNumber:Prompt{prop:Color} = 15066597
    If ?tmp:PartNumber{prop:ReadOnly} = True
        ?tmp:PartNumber{prop:FontColor} = 65793
        ?tmp:PartNumber{prop:Color} = 15066597
    Elsif ?tmp:PartNumber{prop:Req} = True
        ?tmp:PartNumber{prop:FontColor} = 65793
        ?tmp:PartNumber{prop:Color} = 8454143
    Else ! If ?tmp:PartNumber{prop:Req} = True
        ?tmp:PartNumber{prop:FontColor} = 65793
        ?tmp:PartNumber{prop:Color} = 16777215
    End ! If ?tmp:PartNumber{prop:Req} = True
    ?tmp:PartNumber{prop:Trn} = 0
    ?tmp:PartNumber{prop:FontStyle} = font:Bold
    ?tmp:Description:Prompt{prop:FontColor} = -1
    ?tmp:Description:Prompt{prop:Color} = 15066597
    If ?tmp:Description{prop:ReadOnly} = True
        ?tmp:Description{prop:FontColor} = 65793
        ?tmp:Description{prop:Color} = 15066597
    Elsif ?tmp:Description{prop:Req} = True
        ?tmp:Description{prop:FontColor} = 65793
        ?tmp:Description{prop:Color} = 8454143
    Else ! If ?tmp:Description{prop:Req} = True
        ?tmp:Description{prop:FontColor} = 65793
        ?tmp:Description{prop:Color} = 16777215
    End ! If ?tmp:Description{prop:Req} = True
    ?tmp:Description{prop:Trn} = 0
    ?tmp:Description{prop:FontStyle} = font:Bold
    ?Prompt7{prop:FontColor} = -1
    ?Prompt7{prop:Color} = 15066597
    If ?quantity_temp{prop:ReadOnly} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 15066597
    Elsif ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 8454143
    Else ! If ?quantity_temp{prop:Req} = True
        ?quantity_temp{prop:FontColor} = 65793
        ?quantity_temp{prop:Color} = 16777215
    End ! If ?quantity_temp{prop:Req} = True
    ?quantity_temp{prop:Trn} = 0
    ?quantity_temp{prop:FontStyle} = font:Bold
    ?despatch_note_temp:prompt{prop:FontColor} = -1
    ?despatch_note_temp:prompt{prop:Color} = 15066597
    If ?despatch_note_temp{prop:ReadOnly} = True
        ?despatch_note_temp{prop:FontColor} = 65793
        ?despatch_note_temp{prop:Color} = 15066597
    Elsif ?despatch_note_temp{prop:Req} = True
        ?despatch_note_temp{prop:FontColor} = 65793
        ?despatch_note_temp{prop:Color} = 8454143
    Else ! If ?despatch_note_temp{prop:Req} = True
        ?despatch_note_temp{prop:FontColor} = 65793
        ?despatch_note_temp{prop:Color} = 16777215
    End ! If ?despatch_note_temp{prop:Req} = True
    ?despatch_note_temp{prop:Trn} = 0
    ?despatch_note_temp{prop:FontStyle} = font:Bold
    ?Select_Global3:Prompt{prop:FontColor} = -1
    ?Select_Global3:Prompt{prop:Color} = 15066597
    If ?tmp:PurchaseCost{prop:ReadOnly} = True
        ?tmp:PurchaseCost{prop:FontColor} = 65793
        ?tmp:PurchaseCost{prop:Color} = 15066597
    Elsif ?tmp:PurchaseCost{prop:Req} = True
        ?tmp:PurchaseCost{prop:FontColor} = 65793
        ?tmp:PurchaseCost{prop:Color} = 8454143
    Else ! If ?tmp:PurchaseCost{prop:Req} = True
        ?tmp:PurchaseCost{prop:FontColor} = 65793
        ?tmp:PurchaseCost{prop:Color} = 16777215
    End ! If ?tmp:PurchaseCost{prop:Req} = True
    ?tmp:PurchaseCost{prop:Trn} = 0
    ?tmp:PurchaseCost{prop:FontStyle} = font:Bold
    ?Select_Global4:Prompt{prop:FontColor} = -1
    ?Select_Global4:Prompt{prop:Color} = 15066597
    If ?tmp:SaleCost{prop:ReadOnly} = True
        ?tmp:SaleCost{prop:FontColor} = 65793
        ?tmp:SaleCost{prop:Color} = 15066597
    Elsif ?tmp:SaleCost{prop:Req} = True
        ?tmp:SaleCost{prop:FontColor} = 65793
        ?tmp:SaleCost{prop:Color} = 8454143
    Else ! If ?tmp:SaleCost{prop:Req} = True
        ?tmp:SaleCost{prop:FontColor} = 65793
        ?tmp:SaleCost{prop:Color} = 16777215
    End ! If ?tmp:SaleCost{prop:Req} = True
    ?tmp:SaleCost{prop:Trn} = 0
    ?tmp:SaleCost{prop:FontStyle} = font:Bold
    ?retail_cost_temp:Prompt{prop:FontColor} = -1
    ?retail_cost_temp:Prompt{prop:Color} = 15066597
    If ?tmp:RetailCost{prop:ReadOnly} = True
        ?tmp:RetailCost{prop:FontColor} = 65793
        ?tmp:RetailCost{prop:Color} = 15066597
    Elsif ?tmp:RetailCost{prop:Req} = True
        ?tmp:RetailCost{prop:FontColor} = 65793
        ?tmp:RetailCost{prop:Color} = 8454143
    Else ! If ?tmp:RetailCost{prop:Req} = True
        ?tmp:RetailCost{prop:FontColor} = 65793
        ?tmp:RetailCost{prop:Color} = 16777215
    End ! If ?tmp:RetailCost{prop:Req} = True
    ?tmp:RetailCost{prop:Trn} = 0
    ?tmp:RetailCost{prop:FontStyle} = font:Bold
    ?Select_Global5:Prompt{prop:FontColor} = -1
    ?Select_Global5:Prompt{prop:Color} = 15066597
    If ?date_received_temp{prop:ReadOnly} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 15066597
    Elsif ?date_received_temp{prop:Req} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 8454143
    Else ! If ?date_received_temp{prop:Req} = True
        ?date_received_temp{prop:FontColor} = 65793
        ?date_received_temp{prop:Color} = 16777215
    End ! If ?date_received_temp{prop:Req} = True
    ?date_received_temp{prop:Trn} = 0
    ?date_received_temp{prop:FontStyle} = font:Bold
    ?no_of_labels:Prompt{prop:FontColor} = -1
    ?no_of_labels:Prompt{prop:Color} = 15066597
    If ?no_of_labels_temp{prop:ReadOnly} = True
        ?no_of_labels_temp{prop:FontColor} = 65793
        ?no_of_labels_temp{prop:Color} = 15066597
    Elsif ?no_of_labels_temp{prop:Req} = True
        ?no_of_labels_temp{prop:FontColor} = 65793
        ?no_of_labels_temp{prop:Color} = 8454143
    Else ! If ?no_of_labels_temp{prop:Req} = True
        ?no_of_labels_temp{prop:FontColor} = 65793
        ?no_of_labels_temp{prop:Color} = 16777215
    End ! If ?no_of_labels_temp{prop:Req} = True
    ?no_of_labels_temp{prop:Trn} = 0
    ?no_of_labels_temp{prop:FontStyle} = font:Bold
    ?Sheet5{prop:Color} = 15066597
    ?Tab7{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?sto:Quantity_Stock{prop:FontColor} = -1
    ?sto:Quantity_Stock{prop:Color} = 15066597
    ?Prompt16:2{prop:FontColor} = -1
    ?Prompt16:2{prop:Color} = 15066597
    ?tmp:quantitytoorder{prop:FontColor} = -1
    ?tmp:quantitytoorder{prop:Color} = 15066597
    ?Prompt16:3{prop:FontColor} = -1
    ?Prompt16:3{prop:Color} = 15066597
    ?tmp:quantityonorder{prop:FontColor} = -1
    ?tmp:quantityonorder{prop:Color} = 15066597
    ?Prompt19{prop:FontColor} = -1
    ?Prompt19{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?days_7_temp{prop:FontColor} = -1
    ?days_7_temp{prop:Color} = 15066597
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    ?days_30_temp{prop:FontColor} = -1
    ?days_30_temp{prop:Color} = 15066597
    ?Prompt5:3{prop:FontColor} = -1
    ?Prompt5:3{prop:Color} = 15066597
    ?days_60_temp{prop:FontColor} = -1
    ?days_60_temp{prop:Color} = 15066597
    ?Prompt5:4{prop:FontColor} = -1
    ?Prompt5:4{prop:Color} = 15066597
    ?days_90_temp{prop:FontColor} = -1
    ?days_90_temp{prop:Color} = 15066597
    ?Prompt5:5{prop:FontColor} = -1
    ?Prompt5:5{prop:Color} = 15066597
    ?Average_text_temp{prop:FontColor} = -1
    ?Average_text_temp{prop:Color} = 15066597
    ?orp:Reason:Prompt{prop:FontColor} = -1
    ?orp:Reason:Prompt{prop:Color} = 15066597
    If ?orp:Reason{prop:ReadOnly} = True
        ?orp:Reason{prop:FontColor} = 65793
        ?orp:Reason{prop:Color} = 15066597
    Elsif ?orp:Reason{prop:Req} = True
        ?orp:Reason{prop:FontColor} = 65793
        ?orp:Reason{prop:Color} = 8454143
    Else ! If ?orp:Reason{prop:Req} = True
        ?orp:Reason{prop:FontColor} = 65793
        ?orp:Reason{prop:Color} = 16777215
    End ! If ?orp:Reason{prop:Req} = True
    ?orp:Reason{prop:Trn} = 0
    ?orp:Reason{prop:FontStyle} = font:Bold
    ?Panel2{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
ChangeRelated       Routine
    !Update Stock Model File
!    setcursor(cursor:wait)
!    save_stm_id = access:stomodel.savefile()
!    access:stomodel.clearkey(stm:ref_part_description)
!    stm:ref_number  = sto:ref_number
!    stm:part_number = sto:part_number
!    set(stm:ref_part_description,stm:ref_part_description)
!    loop
!        if access:stomodel.next()
!           break
!        end !if
!        if stm:ref_number  <> sto:Ref_number      |
!        or stm:part_number <> sto:part_number      |
!            then break.  ! end if
!        pos = Position(stm:ref_part_description)
!        stm:part_number = tmp:partnumber
!        stm:description = tmp:description
!        access:stomodel.update()
!        Reset(stm:ref_part_description,pos)
!    end !loop
!    access:stomodel.restorefile(save_stm_id)
!    setcursor()
!
!    !Update Common Faults
!    setcursor(cursor:wait)
!    save_ccp_id = access:commoncp.savefile()
!    clear(ccp:record, -1)
!    ccp:part_number = sto:part_number
!    set(ccp:partnumberkey,ccp:partnumberkey)
!    loop
!        next(commoncp)
!        if errorcode()                 |
!           or ccp:part_number <> sto:part_number      |
!           then break.  ! end if
!        pos = Position(ccp:PartNumberKey)
!        ccp:part_number = tmp:PartNumber
!        access:commoncp.update()
!        Reset(ccp:PartNumberKey,pos)
!    end !loop
!    access:commoncp.restorefile(save_ccp_id)
!    setcursor()
!
!    setcursor(cursor:wait)
!    save_cwp_id = access:commonwp.savefile()
!    clear(cwp:record, -1)
!    cwp:part_number = sto:part_number
!    set(cwp:partnumberkey,cwp:partnumberkey)
!    loop
!        next(commonwp)
!        if errorcode()                 |
!           or cwp:part_number <> sto:part_number      |
!           then break.  ! end if
!        pos = Position(cwp:PartNumberKey)
!        cwp:Part_Number = tmp:PartNumber
!        access:commonwp.update()
!        Reset(cwp:PartNumberKey,pos)
!    end !loop
!    access:commonwp.restorefile(save_cwp_id)
!    setcursor()
Hide_Fields     Routine
    If percentage_mark_up_temp <> ''
        ?tmp:SaleCost{prop:readonly} = 1
        ?tmp:SaleCost{prop:skip} = 1
        ?tmp:SaleCost{prop:color} = color:silver
        tmp:SaleCost = tmp:PurchaseCost + (tmp:PurchaseCost * (percentage_mark_up_temp/100))
    Else
        ?tmp:SaleCost{prop:readonly} = 0
        ?tmp:SaleCost{prop:skip} = 0
        ?tmp:SaleCost{prop:color} = color:white
    End
    Display()
Get_Record_Numbers      Routine
    setcursor(cursor:wait)
    save_par_id = access:parts.savefile()
    access:parts.clearkey(par:order_number_key)
    par:ref_number   = orp:job_number
    par:order_number = orp:order_number
    set(par:order_number_key,par:order_number_key)
    loop
        if access:parts.next()
           break
        end !if
        if par:ref_number   <> orp:job_number      |
        or par:order_number <> orp:order_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If par:date_received = '' And par:part_number = sav:PartNumber
            parts_record_number_temp = par:record_number
            Break
        End!If par:date_received = '' And par:part_number = sav:PartNumber
    end !loop
    access:parts.restorefile(save_par_id)

    save_wpr_id = access:warparts.savefile()
    access:warparts.clearkey(wpr:order_number_key)
    wpr:ref_number   = orp:job_number
    wpr:order_number = orp:order_number
    set(wpr:order_number_key,wpr:order_number_key)
    loop
        if access:warparts.next()
           break
        end !if
        if wpr:ref_number   <> orp:job_number      |
        or wpr:order_number <> orp:order_number      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If wpr:date_received = '' And wpr:part_number = sav:PartNumber
            warparts_record_number_temp = wpr:record_number
            Break
        End!If wpr:date_received = '' And wpr:part_number = sav:PartNumber
    end !loop
    access:warparts.restorefile(save_wpr_id)

    save_res_id = access:retstock.savefile()
    access:retstock.clearkey(res:order_number_key)
    res:ref_number  = orp:job_number
    res:order_number    = orp:order_number
    Set(res:order_number_key,res:order_number_key)
    Loop
        if access:retstock.next()
            break
        End
        if res:ref_number   <> orp:job_number   |
        or res:order_number <> orp:order_number |
            then break.
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        If res:date_received = '' And res:part_number = sav:PartNumber
            retstock_record_number_temp = res:record_number
            Break
        End!If wpr:date_received = '' And wpr:part_number = sav:PartNumber
    End!Loop
    access:retstock.restorefile(save_res_id)

    setcursor()
Normal_Receive      Routine
    Case orp:part_type
        Of 'JOB'
            access:parts.clearkey(par:RecordNumberKey) !Update The Part On The Job
            par:record_number    = parts_record_number_temp
            If access:parts.fetch(par:RecordNumberKey) = Level:Benign
                par:date_received            = date_received_temp
                par:despatch_note_number    = despatch_note_temp
                par:quantity                = quantity_temp
                access:parts.update()
                ! Start Change 4803 BE(12/10/2004)
                !UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                ! Start Change 3505 BE(10/11/03)
                UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, par:quantity, par:despatch_note_number, par:ref_number)
                ! End Change 3505 BE(10/11/03)
                ! Start Change 4803 BE(12/10/2004)
                UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
            End!If access:parts.fetch(par:RecordNumberKey) = Level:Benign
            
        Of 'WAR'
            access:warparts.clearkey(wpr:RecordNumberKey)
            wpr:record_number = warparts_record_number_temp
            if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                wpr:date_received = date_received_temp
                wpr:despatch_note_number = despatch_note_temp
                wpr:quantity            = quantity_temp
                access:warparts.update()
                ! Start Change 4803 BE(12/10/2004)
                !UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                ! Start Change 4803 BE(12/10/2004)
                ! Start Change 3505 BE(10/11/03)
                UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, wpr:quantity, wpr:despatch_note_number, wpr:ref_number)
                ! End Change 3505 BE(10/11/03)
                ! Start Change 4803 BE(12/10/2004)
                UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                ! Start Change 4803 BE(12/10/2004)
                quantity_to_job$ = wpr:quantity
            end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
            
            If orp:part_ref_number <> ''        !If Stock Part.. Update Stock Details
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = orp:part_ref_number
                if access:stock.fetch(sto:ref_number_key)
                    Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                Else!if access:stock.fetch(sto:ref_number_key)
                    If sto:quantity_stock < 0
                        sto:quantity_stock = 0
                    End!If sto:quantity_stock < 0
                    access:stock.update()
                    !No Stock History Need Because Part Updated
                End!!if access:stock.fetch(sto:ref_number_key)                                            
            End!If orp:part_ref_number <> ''
        
        Of 'STO' OrOf 'RET'
            StockPart# = 1
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = orp:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found

            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                Access:STOCK.ClearKey(sto:Location_Key)
                sto:Location    = MainStoreLocation()
                sto:Part_Number = orp:Part_Number
                If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Found

                Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    StockPart# = 0
                End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            If StockPart# = 1
                get(stohist,0)
                if access:stohist.primerecord() = Level:Benign
                    shi:ref_number           = sto:ref_number
                    shi:transaction_type     = 'ADD'
                    shi:despatch_note_number = despatch_note_temp
                    shi:quantity             = quantity_temp
                    shi:date                 = Today()
                    shi:purchase_cost        = tmp:PurchaseCost
                    shi:sale_cost            = tmp:SaleCost
                    shi:retail_cost          = tmp:RetailCost
                    shi:job_number           = ''
                    shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)
                    If orp:Reason <> ''
                        shi:Information = Clip(shi:Information) & '<13,10>ORDER REASON: <13,10>' & Clip(orp:Reason)
                    End !If orp:Reason <> ''
                    access:users.clearkey(use:password_key)
                    use:password    = glo:password
                    access:users.fetch(use:password_key)
                    shi:user                 = use:user_code
                    shi:notes                = 'STOCK ADDED FROM ORDER'
                    if access:stohist.insert()
                       access:stohist.cancelautoinc()
                    end
                end!if access:stohist.primerecord() = Level:Benign

                sto:quantity_stock += quantity_temp
                sto:quantity_on_order -= quantity_temp
                If sto:quantity_on_order < 0
                    sto:quantity_on_order = 0
                End
                If sto:quantity_stock < 0
                    sto:quantity_stock = 0
                End!If sto:quantity_stock < 0
                access:stock.update()

            end !If Stockpart# = 1
!        Of 'RET'
!            orp:allocated_to_sale   = 'NO'

    End!Case orp:part_type
Reorder_Receive     Routine
    Case orp:part_type
        Of 'JOB'
            access:parts.clearkey(par:RecordNumberKey) !Update The Part On The Job
            par:record_number    = parts_record_number_temp
            If access:parts.fetch(par:RecordNumberKey) = Level:Benign
                par:date_received            = date_received_temp
                par:despatch_note_number    = despatch_note_temp
                par:quantity                = quantity_temp
                access:parts.update()
                ! Start Change 4803 BE(12/10/2004)
                !UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                ! Start Change 3505 BE(10/11/03)
                UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, par:quantity, par:despatch_note_number, par:ref_number)
                ! End Change 3505 BE(10/11/03)
                ! Start Change 4803 BE(12/10/2004)
                UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)

                get(parts,0) ! Create New Part For New Order
                if access:parts.primerecord() = Level:Benign
                    access:parts_alias.clearkey(par_ali:RecordNumberKey)
                    par_ali:record_number = parts_record_number_temp
                    if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
                        record_number$           = par:record_number
                        par:record              :=: par_ali:record
                        par:record_number        = record_number$
                        par:purchase_cost        = tmp:PurchaseCost
                        par:sale_cost            = tmp:SaleCost
                        par:quantity             = orp:quantity - quantity_temp
                        par:pending_ref_number   = ope:ref_number
                        par:date_received        = ''
                        par:date_ordered         = ''
                        par:order_number         = ''
                        if access:parts.insert()
                           access:parts.cancelautoinc()
                        end
                    end!if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
                end!if access:parts.primerecord() = Level:Benign                            
                
            End!If access:parts.fetch(par:RecordNumberKey) = Level:Benign
            
        Of 'WAR'
            access:warparts.clearkey(wpr:RecordNumberKey)
            wpr:record_number = warparts_record_number_temp
            if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                wpr:date_received             = date_received_temp
                wpr:despatch_note_number     = despatch_note_temp
                wpr:quantity                = quantity_temp
                access:warparts.update()
                ! Start Change 4803 BE(12/10/2004)
                !UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                ! Start Change 3505 BE(10/11/03)
                UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, wpr:quantity, wpr:despatch_note_number, wpr:ref_number)
                ! End Change 3505 BE(10/11/03)
                ! Start Change 4803 BE(12/10/2004)
                UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                get(warparts,0)
                if access:warparts.primerecord() = Level:Benign
                    access:warparts_alias.clearkey(war_ali:RecordNumberKey)
                    war_ali:record_number = warparts_record_number_temp
                    if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
                        record_number$           = wpr:record_number
                        wpr:record              :=: war_ali:record
                        wpr:record_number        = record_number$
                        wpr:purchase_cost        = tmp:PurchaseCost
                        wpr:sale_cost            = tmp:SaleCost
                        wpr:quantity             = orp:quantity - quantity_temp
                        wpr:pending_ref_number   = ope:ref_number
                        wpr:date_received        = ''
                        wpr:date_ordered         = ''
                        wpr:order_number         = ''
                        if access:warparts.insert()
                           access:warparts.cancelautoinc()
                        end
                    end!if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
                end !if access:warparts.primerecord() = Level:Benign                        
                
            end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
            
        
         Of 'STO'
            StockPart# = 1
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = orp:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found

            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                Access:STOCK.ClearKey(sto:Location_Key)
                sto:Location    = MainStoreLocation()
                sto:Part_Number = orp:Part_Number
                If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Found

                Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    StockPart# = 0
                End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            If StockPart# = 1
                get(stohist,0)
                if access:stohist.primerecord() = Level:Benign
                    shi:ref_number           = sto:ref_number
                    shi:transaction_type     = 'ADD'
                    shi:despatch_note_number = despatch_note_temp
                    shi:quantity             = quantity_temp
                    shi:date                 = Today()
                    shi:purchase_cost        = tmp:PurchaseCost
                    shi:sale_cost            = tmp:SaleCost
                    shi:retail_cost          = tmp:RetailCost
                    shi:job_number           = ''
                    shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)
                    If orp:Reason <> ''
                        shi:Information = Clip(shi:Information) & '<13,10>ORDER REASON: <13,10>' & Clip(orp:Reason)
                    End !If orp:Reason <> ''

                    access:users.clearkey(use:password_key)
                    use:password    = glo:password
                    access:users.fetch(use:password_key)
                    shi:user                 = use:user_code
                    shi:notes                = 'STOCK ADDED FROM ORDER'
                    if access:stohist.insert()
                       access:stohist.cancelautoinc()
                    end
                end!if access:stohist.primerecord() = Level:Benign


                If sto:quantity_on_order < 0
                    sto:quantity_on_order = 0
                End
                If sto:quantity_stock < 0
                    sto:quantity_stock = 0
                End!If sto:quantity_stock < 0
                access:stock.update()
            end !if access:stock.fetch(sto:ref_number_key)
        Of 'RET'

            orp:allocated_to_sale   = 'NO'

    End!Case orp:part_type
Split_Receive       Routine
        Case orp:part_type
            Of 'JOB'
                access:parts.clearkey(par:RecordNumberKey)    !Update Job Part
                par:record_number = parts_record_number_temp
                if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                    par:date_received = date_received_temp
                    par:despatch_note_number = despatch_note_temp
                    par:quantity = orp:quantity
                    access:parts.update()
                    ! Start Change 4803 BE(12/10/2004)
                    !UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                    ! End Change 4803 BE(12/10/2004)
                    ! Start Change 3505 BE(10/11/03)
                    UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, par:quantity, par:despatch_note_number, par:ref_number)
                    ! End Change 3505 BE(10/11/03)
                    ! Start Change 4803 BE(12/10/2004)
                    UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                    ! End Change 4803 BE(12/10/2004)
                end!if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                If orp:part_ref_number <> ''
                    access:stock.clearkey(sto:ref_number_key)
                    sto:ref_number = orp:part_ref_number
                    if access:stock.fetch(sto:ref_number_key)
                        Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    Else!if access:stock.fetch(sto:ref_number_key)
                        get(stohist,0)
                        if access:stohist.primerecord() = Level:Benign
                            shi:ref_number           = sto:ref_number
                            shi:transaction_type     = 'ADD'
                            shi:despatch_note_number = despatch_note_temp
                            shi:quantity             = quantity_temp - orp:quantity
                            shi:date                 = Today()
                            shi:purchase_cost        = tmp:PurchaseCost
                            shi:sale_cost            = tmp:SaleCost
                            shi:retail_cost          = tmp:RetailCost
                            shi:job_number           = ''
                            shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                        '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)

                            access:users.clearkey(use:password_key)
                            use:password    = glo:password
                            access:users.fetch(use:password_key)
                            shi:user                 = use:user_code
                            shi:notes                = 'SPLIT ITEMS, STOCK ADDED FROM ORDER'
                            if access:stohist.insert()
                               access:stohist.cancelautoinc()
                            end
                        end!if access:stohist.primerecord() = Level:Benign


                        sto:quantity_stock += (quantity_temp - orp:quantity)
                        sto:quantity_on_order -= orp:quantity
                        If sto:quantity_on_order < 0
                            sto:quantity_on_order = 0
                        End
                        If sto:quantity_stock < 0
                            sto:quantity_stock = 0
                        End!If sto:quantity_stock < 0
                        access:stock.update()
                    end !if access:stock.fetch(sto:ref_number_key)
                Else!If orp:part_ref_number <> ''                                            !Add external part
                    glo:select1 = ''
                    glo:select2 = ''
                    glo:select3 = ''
                    Pick_Locations
                    If glo:select1 = ''
                        Case MessageEx('The Excess Parts will NOT be added to stock.','ServiceBase 2000',|
                                       'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    Else!If glo:select1 = ''
                        do_delete# = 1
                        get(stock,0)
                        if access:stock.primerecord() = level:benign
                            sto:part_number            = par:part_number
                            sto:description            = par:description
                            sto:supplier               = par:supplier
                            sto:purchase_cost          = par:purchase_cost
                            sto:sale_cost              = par:sale_cost
                            sto:shelf_location         = glo:select2
                            sto:manufacturer           = job:manufacturer
                            sto:location               = glo:select1
                            sto:second_location        = glo:select3
                            sto:quantity_stock         = (quantity_temp - orp:quantity)
                            access:stock.insert()
                            get(stohist,0)
                            if access:stohist.primerecord() = level:benign
                                shi:ref_number           = sto:ref_number
                                access:users.clearkey(use:password_key)
                                use:password              = glo:password
                                access:users.fetch(use:password_key)
                                shi:user                  = use:user_code    
                                shi:date                 = today()
                                shi:transaction_type     = 'ADD'
                                shi:despatch_note_number = par:despatch_note_number
                                shi:job_number           = job:ref_number
                                shi:quantity             = (quantity_temp - orp:quantity)
                                shi:purchase_cost        = par:purchase_cost
                                shi:sale_cost            = par:sale_cost
                                shi:retail_cost          = par:retail_cost
                                shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                            '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)
                                shi:notes                = 'SPLIT ITEMS, STOCK ADDED FROM ORDER'
                                access:stohist.insert()
                            end!if access:stohist.primerecord() = level:benign
                        End!if access:stock.primerecord() = level:benign
                    End!If glo:select1 = ''
                End!If orp:part_ref_number <> ''
                
            
            Of 'WAR'
                access:warparts.clearkey(wpr:RecordNumberKey)
                wpr:record_number = warparts_record_number_temp
                if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                    wpr:date_received = date_received_temp
                    wpr:despatch_note_number = despatch_note_temp
                    wpr:quantity = orp:quantity
                    access:warparts.update()
                    ! Start Change 4803 BE(12/10/2004)
                    !UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                    ! End Change 4803 BE(12/10/2004)
                    ! Start Change 3505 BE(10/11/03)
                    UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, wpr:quantity, wpr:despatch_note_number, wpr:ref_number)
                    ! End Change 3505 BE(10/11/03)
                    ! Start Change 4803 BE(12/10/2004)
                    UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                    ! End Change 4803 BE(12/10/2004)
                end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                If orp:part_ref_number <> ''
                    access:stock.clearkey(sto:ref_number_key)
                    sto:ref_number = orp:part_ref_number
                    if access:stock.fetch(sto:ref_number_key)
                        Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    Else!if access:stock.fetch(sto:ref_number_key)

                        get(stohist,0)
                        if access:stohist.primerecord() = Level:Benign
                            shi:ref_number           = sto:ref_number
                            shi:transaction_type     = 'ADD'
                            shi:despatch_note_number = despatch_note_temp
                            shi:quantity             = quantity_temp - orp:quantity
                            shi:date                 = Today()
                            shi:purchase_cost        = tmp:PurchaseCost
                            shi:sale_cost            = tmp:SaleCost
                            shi:retail_cost          = tmp:RetailCost
                            shi:job_number           = ''
                            shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                        '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)
                            access:users.clearkey(use:password_key)
                            use:password    = glo:password
                            access:users.fetch(use:password_key)
                            shi:user                 = use:user_code
                            shi:notes                = 'SPLIT ITEMS, STOCK ADDED FROM ORDER'
                            if access:stohist.insert()
                               access:stohist.cancelautoinc()
                            end
                        end!if access:stohist.primerecord() = Level:Benign


                        sto:quantity_stock += (quantity_temp - orp:quantity)
                        sto:quantity_on_order -= orp:quantity
                        If sto:quantity_on_order < 0
                            sto:quantity_on_order = 0
                        End
                        If sto:quantity_stock < 0
                            sto:quantity_stock = 0
                        End!If sto:quantity_stock < 0
                        access:stock.update()
                    end !if access:stock.fetch(sto:ref_number_key)
                Else!If orp:part_ref_number <> ''
                    glo:select1 = ''
                    glo:select2 = ''
                    glo:select3 = ''
                    Pick_Locations
                    If glo:select1 = ''
                        Case MessageEx('The Excess Parts will NOT be added to stock.','ServiceBase 2000',|
                                       'Styles\warn.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                    Else!If glo:select1 = ''
                        do_delete# = 1
                        get(stock,0)
                        if access:stock.primerecord() = level:benign
                            sto:part_number            = wpr:part_number
                            sto:description            = wpr:description
                            sto:supplier               = wpr:supplier
                            sto:purchase_cost          = wpr:purchase_cost
                            sto:sale_cost              = wpr:sale_cost
                            sto:shelf_location         = glo:select2
                            sto:manufacturer           = job:manufacturer
                            sto:location               = glo:select1
                            sto:second_location        = glo:select3
                            sto:quantity_stock         = (quantity_temp - orp:quantity)
                            access:stock.insert()
                            get(stohist,0)
                            if access:stohist.primerecord() = level:benign
                                shi:ref_number           = sto:ref_number
                                access:users.clearkey(use:password_key)
                                use:password              = glo:password
                                access:users.fetch(use:password_key)
                                shi:user                  = use:user_code    
                                shi:date                 = today()
                                shi:transaction_type     = 'ADD'
                                shi:despatch_note_number = wpr:despatch_note_number
                                shi:job_number           = job:ref_number
                                shi:quantity             = (quantity_temp - orp:quantity)
                                shi:purchase_cost        = wpr:purchase_cost
                                shi:sale_cost            = wpr:sale_cost
                                shi:retail_cost          = wpr:retail_cost
                                shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                            '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)
                                shi:notes                = 'SPLIT ITEMS, STOCK ADDED FROM ORDER'
                                access:stohist.insert()
                            end!if access:stohist.primerecord() = level:benign
                        End!if access:stock.primerecord() = level:benign
                    End!If glo:select1 = ''

                End!If orp:part_ref_number <> ''
            
            Of 'STO'
                StockPart# = 1
                Access:STOCK.Clearkey(sto:Ref_Number_Key)
                sto:Ref_Number  = orp:Part_Ref_Number
                If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Found

                Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Access:STOCK.ClearKey(sto:Location_Key)
                    sto:Location    = MainStoreLocation()
                    sto:Part_Number = orp:Part_Number
                    If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        !Found

                    Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                        !Error
                        !Assert(0,'<13,10>Fetch Error<13,10>')
                        Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                            Of 1 ! &OK Button
                        End!Case MessageEx
                        StockPart# = 0
                    End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                If StockPart# = 1
                    get(stohist,0)
                    if access:stohist.primerecord() = Level:Benign
                        shi:ref_number           = sto:ref_number
                        shi:transaction_type     = 'ADD'
                        shi:despatch_note_number = despatch_note_temp
                        shi:quantity             = quantity_temp
                        shi:date                 = Today()
                        shi:purchase_cost        = tmp:PurchaseCost
                        shi:sale_cost            = tmp:SaleCost
                        shi:retail_cost          = tmp:RetailCost
                        shi:job_number           = ''
                        shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)
                        If orp:Reason <> ''
                            shi:Information = Clip(shi:Information) & '<13,10>ORDER REASON: <13,10>' & Clip(orp:Reason)
                        End !If orp:Reason <> ''

                        access:users.clearkey(use:password_key)
                        use:password    = glo:password
                        access:users.fetch(use:password_key)
                        shi:user                 = use:user_code
                        shi:notes                = 'STOCK ADDED FROM ORDER'
                        if access:stohist.insert()
                           access:stohist.cancelautoinc()
                        end
                    end!if access:stohist.primerecord() = Level:Benign


                    sto:quantity_stock += quantity_temp
                    sto:quantity_on_order -= orp:quantity
                    If sto:quantity_on_order < 0
                        sto:quantity_on_order = 0
                    End
                    If sto:quantity_stock < 0
                        sto:quantity_stock = 0
                    End!If sto:quantity_stock < 0
                    access:stock.update()
                end !if access:stock.fetch(sto:ref_number_key)

            Of 'RET'
                access:stock.clearkey(sto:ref_number_key)
                sto:ref_number = orp:part_ref_number
                if access:stock.fetch(sto:ref_number_key)
                    Case MessageEx('Error! Unable to find Stock Part.','ServiceBase 2000','styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0)
                        Of 1 ! &OK Button
                    End!Case MessageEx
                Else!if access:stock.fetch(sto:ref_number_key)
                    get(stohist,0)
                    if access:stohist.primerecord() = Level:Benign
                        shi:ref_number           = sto:ref_number
                        shi:transaction_type     = 'ADD'
                        shi:despatch_note_number = despatch_note_temp
                        shi:quantity             = quantity_temp - orp:quantity
                        shi:date                 = Today()
                        shi:purchase_cost        = tmp:PurchaseCost
                        shi:sale_cost            = tmp:SaleCost
                        shi:retail_cost          = tmp:RetailCost
                        shi:job_number           = ''
                        shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                    '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)

                        access:users.clearkey(use:password_key)
                        use:password    = glo:password
                        access:users.fetch(use:password_key)
                        shi:user                 = use:user_code
                        shi:notes                = 'SPLIT ITEMS, STOCK ADDED FROM ORDER'
                        if access:stohist.insert()
                           access:stohist.cancelautoinc()
                        end
                    end!if access:stohist.primerecord() = Level:Benign

                    sto:quantity_stock += (quantity_temp - orp:quantity)
                    sto:quantity_on_order -= orp:quantity
                    If sto:quantity_on_order < 0
                        sto:quantity_on_order = 0
                    End
                    If sto:quantity_stock < 0
                        sto:quantity_stock = 0
                    End!If sto:quantity_stock < 0
                    access:stock.update()
                end !if access:stock.fetch(sto:ref_number_key)
                orp:allocated_to_sale   = 'NO'

        End!Case orp:part_type
Arrive_Later_Receive        Routine
    Case orp:part_type
        Of 'JOB'
            access:parts.clearkey(par:RecordNumberKey) !Update The Part On The Job
            par:record_number    = parts_record_number_temp
            If access:parts.fetch(par:RecordNumberKey) = Level:Benign

                par:date_received            = date_received_temp
                par:despatch_note_number    = despatch_note_temp
                par:quantity                = quantity_temp
                access:parts.update()
                ! Start Change 4803 BE(12/10/2004)
                !UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                ! Start Change 3505 BE(10/11/03)
                UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, par:quantity, par:despatch_note_number, par:ref_number)
                ! End Change 3505 BE(10/11/03)
                ! Start Change 4803 BE(12/10/2004)
                UpdatePickPartsReceived(par:Ref_Number,par:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                get(parts,0) ! Create New Part For New Order
                if access:parts.primerecord() = Level:Benign
                    access:parts_alias.clearkey(par_ali:RecordNumberKey)
                    par_ali:record_number = parts_record_number_temp
                    if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
                        record_number$           = par:record_number
                        par:record              :=: par_ali:record
                        par:record_number        = record_number$
                        par:purchase_cost        = tmp:PurchaseCost
                        par:sale_cost            = tmp:SaleCost
                        par:quantity             = orp:quantity - quantity_temp
                        par:date_received        = ''
                        par:date_ordered         = date_received_temp
                        par:order_number         = ord:order_number
                        if access:parts.insert()
                           access:parts.cancelautoinc()
                        end
                    end!if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:Benign
                end!if access:parts.primerecord() = Level:Benign                            
                
            End!If access:parts.fetch(par:RecordNumberKey) = Level:Benign
            

        Of 'WAR'
            access:warparts.clearkey(wpr:RecordNumberKey)
            wpr:record_number = warparts_record_number_temp
            if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                wpr:date_received             = date_received_temp
                wpr:despatch_note_number     = despatch_note_temp
                wpr:quantity                = quantity_temp
                access:warparts.update()
                ! Start Change 4803 BE(12/10/2004)
                !UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                ! Start Change 3505 BE(10/11/03)
                UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, wpr:quantity, wpr:despatch_note_number, wpr:ref_number)
                ! End Change 3505 BE(10/11/03)
                ! Start Change 4803 BE(12/10/2004)
                UpdatePickPartsReceived(wpr:Ref_Number,wpr:Record_Number,quantity_temp) !amend picking records
                ! End Change 4803 BE(12/10/2004)
                get(warparts,0)
                if access:warparts.primerecord() = Level:Benign
                    access:warparts_alias.clearkey(war_ali:RecordNumberKey)
                    war_ali:record_number = warparts_record_number_temp
                    if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
                        record_number$           = wpr:record_number
                        wpr:record              :=: war_ali:record
                        wpr:record_number        = record_number$
                        wpr:purchase_cost        = tmp:PurchaseCost
                        wpr:sale_cost            = tmp:SaleCost
                        wpr:quantity             = orp:quantity - quantity_temp
                        wpr:date_received        = ''
                        wpr:date_ordered         = date_received_temp
                        wpr:order_number         = ord:order_number
                        if access:warparts.insert()
                           access:warparts.cancelautoinc()
                        end
                    end!if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:benign
                end !if access:warparts.primerecord() = Level:Benign                        
                
            end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
            

        Of 'STO'
            StockPart# = 1
            Access:STOCK.Clearkey(sto:Ref_Number_Key)
            sto:Ref_Number  = orp:Part_Ref_Number
            If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Found

            Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                !Error
                !Assert(0,'<13,10>Fetch Error<13,10>')
                Access:STOCK.ClearKey(sto:Location_Key)
                sto:Location    = MainStoreLocation()
                sto:Part_Number = orp:Part_Number
                If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Found

                Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                    !Error
                    !Assert(0,'<13,10>Fetch Error<13,10>')
                    Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                   'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                        Of 1 ! &OK Button
                    End!Case MessageEx
                    StockPart# = 0
                End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
            End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
            If StockPart# = 1
                get(stohist,0)
                if access:stohist.primerecord() = Level:Benign
                    shi:ref_number           = sto:ref_number
                    shi:transaction_type     = 'ADD'
                    shi:despatch_note_number = despatch_note_temp
                    shi:quantity             = quantity_temp
                    shi:date                 = Today()
                    shi:purchase_cost        = tmp:PurchaseCost
                    shi:sale_cost            = tmp:SaleCost
                    shi:retail_cost          = tmp:RetailCost
                    shi:job_number           = ''
                    shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier) &|
                                                '<13,10>DESPATCH NOTE NO: ' & Clip(despatch_note_temp)
                    If orp:Reason <> ''
                        shi:Information = Clip(shi:Information) & '<13,10>ORDER REASON: <13,10>' & Clip(orp:Reason)
                    End !If orp:Reason <> ''

                    access:users.clearkey(use:password_key)
                    use:password    = glo:password
                    access:users.fetch(use:password_key)
                    shi:user                 = use:user_code
                    shi:notes                = 'STOCK ADDED FROM ORDER'
                    if access:stohist.insert()
                       access:stohist.cancelautoinc()
                    end
                end!if access:stohist.primerecord() = Level:Benign

                sto:quantity_stock += quantity_temp
                If sto:quantity_stock < 0
                    sto:quantity_stock = 0
                End!If sto:quantity_stock < 0
                access:stock.update()
            end !if access:stock.fetch(sto:ref_number_key)
        Of 'RET'
            orp:allocated_to_sale   = 'NO'


    End!Case orp:part_type
Replicate       Routine
    If tmp:CostsChanged = 1
        Case MessageEx('Do you wish to update all occurances of this part with the amended details?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                PartsChanging(sav:PartNumber,tmp:PartNumber,sav:Description,tmp:Description,sav:Sale_Cost,tmp:SaleCost,|
                                sav:Purchase_Cost,tmp:PurchaseCost,sav:Retail_Cost,tmp:RetailCost,sav:ShelfLocation,tmp:ShelfLocation,|
                                sav:SecondLocation,tmp:SecondLocation)

!                setcursor(cursor:wait)
!                save_loc_id = access:location.savefile()
!                set(loc:location_key)
!                loop
!                    if access:location.next()
!                       break
!                    end !if
!                    save_sto_id = access:stock.savefile()
!                    access:stock.clearkey(sto:location_part_description_key)
!                    sto:location    = loc:location
!                    sto:part_number = sav:partnumber
!                    sto:description = sav:description
!                    set(sto:location_part_description_key,sto:location_part_description_key)
!                    loop
!                        if access:stock.next()
!                           break
!                        end !if
!                        if sto:location    <> loc:location  |
!                        or sto:part_number <> sav:partnumber  |
!                        or sto:description <> sav:description  |
!                            then break.  ! end if
!                        If sto:ref_number       = sto_ali:ref_number
!                            Cycle
!                        End!If sto:ref_number_temp  = ref_number_temp
!                        sto:purchase_cost=sav:Purchase_Cost
!                        sto:sale_cost=sav:Sale_Cost
!                        sto:retail_cost=sav:Retail_Cost
!                        If (tmp:partnumber <> sav:partnumber) Or (tmp:description <> Sav:description)
!                            pos = Position(sto:location_part_description_key)
!                            sto:part_number = tmp:partnumber
!                            sto:description = tmp:description
!                        End!If (tmp:partnumber <> sto:part_number) Or (tmp:description <> sto:description)
!                        Compile('***',Debug=1)
!                            message('Stock Part Updated.','Debug Message', icon:exclamation)
!                        ***
!                        access:stock.update()
!                        If (tmp:partnumber <> sav:partnumber) Or (tmp:description <> sav:description)
!                            Reset(sto:location_part_description_key,pos)
!                        End!If (tmp:partnumber <> sto:part_number) Or (tmp:description <> sto:description)
!
!                        !Update Stock Model
!
!                        access:stomodel.clearkey(stm:ref_part_description)
!                        stm:ref_number  = sto:ref_number
!                        stm:part_number = sav:partnumber
!                        stm:location    = sto:location
!                        stm:description = sav:Description
!                        If access:stomodel.tryfetch(stm:ref_part_description) = Level:Benign
!                            !Found
!                            stm:part_number = sto:part_number
!                            stm:description = sto:description
!                            access:stomodel.update()
!                        Else! If access:stomodel.tryfetch(stm:ref_part_description) = Level:Benign
!                            !Error
!                        End! If access:stomodel.tryfetch(stm:ref_part_description) = Level:Benign
!                        
!
!                        get(stohist,0)
!                        if access:stohist.primerecord() = level:benign
!                            shi:ref_number           = sto:ref_number
!                            access:users.clearkey(use:password_key)
!                            use:password              = glo:password
!                            access:users.fetch(use:password_key)
!                            shi:user                  = use:user_code    
!                            shi:date                 = today()
!                            shi:transaction_type     = 'ADD'
!                            shi:despatch_note_number = ''
!                            shi:job_number           = ''
!                            shi:quantity             = 0
!                            shi:purchase_cost        = STO:PURCHASE_COST
!                            shi:sale_cost            = STO:SALE_COST
!                            shi:retail_cost          = STO:RETAIL_COST
!                            shi:information          = 'PREVIOUS DETAILS:-' & |
!                                                        '<13,10>PART NUMBER: ' & CLip(sav:PartNumber) &|
!                                                        '<13,10>DESCRIPTION: ' & CLip(sav:Description) &|
!                                                        '<13,10>PURCHASE COST: ' & CLip(sav:Purchase_Cost) &|
!                                                        '<13,10>TRADE PRICE: ' & CLip(sav:Sale_Cost) &|
!                                                        '<13,10>RETAIL PRICE: ' & CLip(Sav:Retail_Cost)
!                            shi:notes                = 'DETAILS CHANGED'
!                            if access:stohist.insert()
!                               access:stohist.cancelautoinc()
!                            end
!                        end!if access:stohist.primerecord() = level:benign
!                    end !loop
!                    access:stock.restorefile(save_sto_id)
!                    setcursor()
!                End!if access:stock.fetch(sto:ref_number_key) = Level:Benign
            Of 2 ! &No Button
        End!Case MessageEx

    End!If tmp:CostsChanged = 1


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Receive_Order',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('LocalRequest',LocalRequest,'Receive_Order',1)
    SolaceViewVars('save_stm_id',save_stm_id,'Receive_Order',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'Receive_Order',1)
    SolaceViewVars('sav:partnumber',sav:partnumber,'Receive_Order',1)
    SolaceViewVars('sav:description',sav:description,'Receive_Order',1)
    SolaceViewVars('sav:purchase_cost',sav:purchase_cost,'Receive_Order',1)
    SolaceViewVars('sav:sale_cost',sav:sale_cost,'Receive_Order',1)
    SolaceViewVars('sav:retail_cost',sav:retail_cost,'Receive_Order',1)
    SolaceViewVars('sav:ShelfLocation',sav:ShelfLocation,'Receive_Order',1)
    SolaceViewVars('sav:SecondLocation',sav:SecondLocation,'Receive_Order',1)
    SolaceViewVars('save_ope_id',save_ope_id,'Receive_Order',1)
    SolaceViewVars('save_orp_ali_id',save_orp_ali_id,'Receive_Order',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Receive_Order',1)
    SolaceViewVars('average_temp',average_temp,'Receive_Order',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Receive_Order',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Receive_Order',1)
    SolaceViewVars('tmp:CostsChanged',tmp:CostsChanged,'Receive_Order',1)
    SolaceViewVars('save_res_id',save_res_id,'Receive_Order',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Receive_Order',1)
    SolaceViewVars('quantity_temp',quantity_temp,'Receive_Order',1)
    SolaceViewVars('purchase_cost_Temp',purchase_cost_Temp,'Receive_Order',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'Receive_Order',1)
    SolaceViewVars('sale_cost_temp',sale_cost_temp,'Receive_Order',1)
    SolaceViewVars('pos',pos,'Receive_Order',1)
    SolaceViewVars('date_received_temp',date_received_temp,'Receive_Order',1)
    SolaceViewVars('additional_notes_temp',additional_notes_temp,'Receive_Order',1)
    SolaceViewVars('despatch_note_temp',despatch_note_temp,'Receive_Order',1)
    SolaceViewVars('percentage_mark_up_temp',percentage_mark_up_temp,'Receive_Order',1)
    SolaceViewVars('parts_record_number_Temp',parts_record_number_Temp,'Receive_Order',1)
    SolaceViewVars('warparts_record_number_temp',warparts_record_number_temp,'Receive_Order',1)
    SolaceViewVars('retstock_record_number_temp',retstock_record_number_temp,'Receive_Order',1)
    SolaceViewVars('no_of_labels_temp',no_of_labels_temp,'Receive_Order',1)
    SolaceViewVars('retail_cost_temp',retail_cost_temp,'Receive_Order',1)
    SolaceViewVars('days_7_temp',days_7_temp,'Receive_Order',1)
    SolaceViewVars('days_30_temp',days_30_temp,'Receive_Order',1)
    SolaceViewVars('days_60_temp',days_60_temp,'Receive_Order',1)
    SolaceViewVars('days_90_temp',days_90_temp,'Receive_Order',1)
    SolaceViewVars('Average_text_temp',Average_text_temp,'Receive_Order',1)
    SolaceViewVars('tmp:quantitytoorder',tmp:quantitytoorder,'Receive_Order',1)
    SolaceViewVars('tmp:quantityonorder',tmp:quantityonorder,'Receive_Order',1)
    SolaceViewVars('tmp:PartNumber',tmp:PartNumber,'Receive_Order',1)
    SolaceViewVars('tmp:Description',tmp:Description,'Receive_Order',1)
    SolaceViewVars('tmp:SaleCost',tmp:SaleCost,'Receive_Order',1)
    SolaceViewVars('tmp:PurchaseCost',tmp:PurchaseCost,'Receive_Order',1)
    SolaceViewVars('tmp:RetailCost',tmp:RetailCost,'Receive_Order',1)
    SolaceViewVars('tmp:ShelfLocation',tmp:ShelfLocation,'Receive_Order',1)
    SolaceViewVars('tmp:SecondLocation',tmp:SecondLocation,'Receive_Order',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Location:Prompt;  SolaceCtrlName = '?STO:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Location;  SolaceCtrlName = '?sto:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Shelf_Location:Prompt;  SolaceCtrlName = '?STO:Shelf_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:ShelfLocation;  SolaceCtrlName = '?tmp:ShelfLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupShelfLocation;  SolaceCtrlName = '?LookupShelfLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Second_Location:Prompt;  SolaceCtrlName = '?STO:Second_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SecondLocation;  SolaceCtrlName = '?tmp:SecondLocation';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PartNumber:Prompt;  SolaceCtrlName = '?tmp:PartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PartNumber;  SolaceCtrlName = '?tmp:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Description:Prompt;  SolaceCtrlName = '?tmp:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:Description;  SolaceCtrlName = '?tmp:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt7;  SolaceCtrlName = '?Prompt7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?quantity_temp;  SolaceCtrlName = '?quantity_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_note_temp:prompt;  SolaceCtrlName = '?despatch_note_temp:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?despatch_note_temp;  SolaceCtrlName = '?despatch_note_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global3:Prompt;  SolaceCtrlName = '?Select_Global3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PurchaseCost;  SolaceCtrlName = '?tmp:PurchaseCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global4:Prompt;  SolaceCtrlName = '?Select_Global4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SaleCost;  SolaceCtrlName = '?tmp:SaleCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retail_cost_temp:Prompt;  SolaceCtrlName = '?retail_cost_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:RetailCost;  SolaceCtrlName = '?tmp:RetailCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select_Global5:Prompt;  SolaceCtrlName = '?Select_Global5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?date_received_temp;  SolaceCtrlName = '?date_received_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?no_of_labels:Prompt;  SolaceCtrlName = '?no_of_labels:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?no_of_labels_temp;  SolaceCtrlName = '?no_of_labels_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet5;  SolaceCtrlName = '?Sheet5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16;  SolaceCtrlName = '?Prompt16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Quantity_Stock;  SolaceCtrlName = '?sto:Quantity_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:2;  SolaceCtrlName = '?Prompt16:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:quantitytoorder;  SolaceCtrlName = '?tmp:quantitytoorder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16:3;  SolaceCtrlName = '?Prompt16:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:quantityonorder;  SolaceCtrlName = '?tmp:quantityonorder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt19;  SolaceCtrlName = '?Prompt19';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_7_temp;  SolaceCtrlName = '?days_7_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_30_temp;  SolaceCtrlName = '?days_30_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:3;  SolaceCtrlName = '?Prompt5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_60_temp;  SolaceCtrlName = '?days_60_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:4;  SolaceCtrlName = '?Prompt5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_90_temp;  SolaceCtrlName = '?days_90_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:5;  SolaceCtrlName = '?Prompt5:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Average_text_temp;  SolaceCtrlName = '?Average_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Reason:Prompt;  SolaceCtrlName = '?orp:Reason:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Reason;  SolaceCtrlName = '?orp:Reason';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel2;  SolaceCtrlName = '?Panel2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PreviousDespatch;  SolaceCtrlName = '?PreviousDespatch';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Receive_Order')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Receive_Order')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STO:Location:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:COMMONCP.Open
  Relate:DEFAULTS.Open
  Relate:LOCATION.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:STOCK_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:USERS.UseFile
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:RETSTOCK.UseFile
  Access:STOMODEL.UseFile
  Access:COMMONWP.UseFile
  Access:LOCSHELF.UseFile
  SELF.FilesOpened = True
  ref_number# = sto:ref_number
  Include('stockuse.inc')
  tmp:quantityonorder = 0
  tmp:quantitytoorder = 0
  
  setcursor(cursor:wait)                                                                  !file. And work out the quantity
  save_ope_id = access:ordpend.savefile()                                                 !awaiting order
  access:ordpend.clearkey(ope:part_ref_number_key)
  ope:part_ref_number =  sto:ref_number
  set(ope:part_ref_number_key,ope:part_ref_number_key)
  loop
      if access:ordpend.next()
         break
      end !if
      if ope:part_ref_number <> sto:ref_number      |
          then break.  ! end if
      yldcnt# += 1
      if yldcnt# > 25
         yield() ; yldcnt# = 0
      end !if
      tmp:quantitytoorder += ope:quantity
  end !loop
  access:ordpend.restorefile(save_ope_id)
  setcursor()
  
  setcursor(cursor:wait)                                                                  !file. But check with the order
  save_orp_ali_id = access:ordparts_alias.savefile()                                                !file first to see if the whole
  access:ordparts_alias.clearkey(orp_ali:ref_number_key)                                            !order has been received. This
  orp_ali:part_ref_number = sto:ref_number                                                    !should hopefully speed things
  set(orp_ali:ref_number_key,orp_ali:ref_number_key)                                              !up.
  loop
      if access:ordparts_alias.next()
         break
      end !if
      if orp_ali:part_ref_number <> sto:ref_number      |
          then break.  ! end if
      access:orders.clearkey(ord:order_number_key)
      ord:order_number = orp_ali:order_number
      if access:orders.fetch(ord:order_number_key) = Level:Benign
          If ord:all_received <> 'YES'
              If orp_ali:all_received <> 'YES'
                  tmp:quantityonorder += orp_ali:quantity
              End!If orp:all_received <> 'YES'
          End!If ord:all_received <> 'YES'
      end!if access:orders.fetch(ord:order_number_key) = Level:Benign
  end !loop
  access:ordparts_alias.restorefile(save_orp_ali_id)
  setcursor()
  
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ?date_received_temp{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  IF ?tmp:ShelfLocation{Prop:Tip} AND ~?LookupShelfLocation{Prop:Tip}
     ?LookupShelfLocation{Prop:Tip} = 'Select ' & ?tmp:ShelfLocation{Prop:Tip}
  END
  IF ?tmp:ShelfLocation{Prop:Msg} AND ~?LookupShelfLocation{Prop:Msg}
     ?LookupShelfLocation{Prop:Msg} = 'Select ' & ?tmp:ShelfLocation{Prop:Msg}
  END
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:COMMONCP.Close
    Relate:DEFAULTS.Close
    Relate:LOCATION.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:STOCK_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Receive_Order',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    BrowseShelfLocations
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?OkButton
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      tmp:CostsChanged    = 0
      If sav:PartNumber <> tmp:PartNumber Or |
          sav:Description <> tmp:Description Or |
          sav:Sale_Cost <> tmp:SaleCost Or |
          sav:Purchase_Cost <> tmp:PurchaseCost Or |
          sav:Retail_Cost <> tmp:RetailCost   Or |
          sav:ShelfLocation <> tmp:ShelfLocation Or |
          sav:SecondLocation <> tmp:SecondLocation
      
          Case MessageEx('Are you sure you want to alter the details of this item?','ServiceBase 2000','styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,'''''',0+2+0+0,84,26,0)
              Of 1 ! &Yes Button
                  tmp:CostsChanged    = 1
                  orp:purchase_cost   = tmp:PurchaseCost
                  orp:sale_cost       = tmp:SaleCost
                  orp:retail_cost     = tmp:RetailCost
                  orp:part_number     = tmp:partnumber
                  orp:description     = tmp:description
                  orp:DespatchNoteNumber = despatch_note_temp
      
              Of 2 ! &No Button
                  
                  tmp:PurchaseCost  = orp:purchase_cost
                  tmp:SaleCost      = orp:sale_cost
                  tmp:RetailCost    = orp:retail_cost
                  tmp:partnumber      = orp:part_number
                  tmp:description     = orp:description
                  tmp:ShelfLocation   = sav:ShelfLocation
                  tmp:SecondLocation  = sav:SecondLocation
      
          End!Case MessagheEx
      End
      Display()
      
      
      parts_record_number_temp = ''
      warparts_record_number_temp = ''
      Do Get_Record_Numbers
      
      !Write the Despatch Note Number!
      orp:DespatchNoteNumber = despatch_note_temp
      
      !Write the changes back to the job if necessary
      If tmp:CostsChanged = 1
          Case orp:Part_Type
              Of 'JOB'
                  Access:PARTS.Clearkey(par:RecordNumberKey)
                  par:Record_Number   = Parts_Record_Number_Temp
                  If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                      !Found
                      par:Part_Number     = tmp:PartNumber
                      par:Description     = tmp:Description
                      par:Sale_Cost       = tmp:SaleCost
                      par:Purchase_Cost   = tmp:PurchaseCost
                      par:Retail_Cost     = tmp:RetailCost
                      Access:PARTS.Update()
                  Else! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
                      !Error
                      Assert(0,'<13,10>Could not retrieve the associated part from the Job<13,10>')
                  End! If Access:PARTS.Tryfetch(par:RecordNumberKey) = Level:Benign
      
              Of 'WAR'
                  Access:WARPARTS.Clearkey(wpr:RecordNumberKey)
                  wpr:Record_Number   = Warparts_Record_Number_Temp
                  If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                      !Found
                      wpr:Part_Number     = tmp:PartNumber
                      wpr:Description     = tmp:Description
                      wpr:Sale_Cost       = tmp:SaleCost
                      wpr:Purchase_Cost   = tmp:PurchaseCost
                      wpr:Retail_Cost     = tmp:RetailCost
                      Access:WARPARTS.Update()
                  Else! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                      !Error
                      Assert(0,'<13,10>Could not retrieve the associated part from the Job<13,10>')
                  End! If Access:WARPARTS.Tryfetch(wpr:RecordNumberKey) = Level:Benign
                  
              Of 'STO'
                  StockPart# = 1
                  Access:STOCK.Clearkey(sto:Ref_Number_Key)
                  sto:Ref_Number  = orp:Part_Ref_Number
                  If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Found
      
                  Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Error
                      !Assert(0,'<13,10>Fetch Error<13,10>')
                      Access:STOCK.ClearKey(sto:Location_Key)
                      sto:Location    = MainStoreLocation()
                      sto:Part_Number = orp:Part_Number
                      If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                          !Found
      
                      Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                          !Error
                          !Assert(0,'<13,10>Fetch Error<13,10>')
                          Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                          StockPart# = 0
                      End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  If StockPart# = 1
                      !Found
                      sto:Part_Number     = tmp:PartNumber
                      sto:Description     = tmp:Description
                      sto:Sale_Cost       = tmp:SaleCost
                      sto:Purchase_Cost   = tmp:PurchaseCost
                      sto:Retail_Cost     = tmp:RetailCost
                      sto:Second_Location = tmp:SecondLocation
                      sto:Shelf_Location  = tmp:ShelfLocation
                      Access:STOCK.Update()
      
                      if access:stohist.primerecord() = level:benign
                          shi:ref_number           = sto:ref_number
                          access:users.clearkey(use:password_key)
                          use:password              = glo:password
                          access:users.fetch(use:password_key)
                          shi:user                  = use:user_code    
                          shi:date                 = today()
                          shi:transaction_type     = 'ADD'
                          shi:despatch_note_number = ''
                          shi:job_number           = ''
                          shi:quantity             = 0
                          shi:purchase_cost        = STO:PURCHASE_COST
                          shi:sale_cost            = STO:SALE_COST
                          shi:retail_cost          = STO:RETAIL_COST
                          shi:information          = 'PREVIOUS DETAILS:-'
                          If tmp:PartNumber <> sav:PartNumber
                              shi:Information = Clip(shi:Information) & '<13,10>PART NUMBER: ' & CLip(sav:PartNumber)
                          End!If func:OldPartNumber <> func:NewPartNumber
                          If tmp:Description <> sav:Description
                              shi:Information = Clip(shi:Information) & '<13,10>DESCRIPTION: ' & CLip(sav:Description)
                          End!If func:OldDescription <> func:NewDescription
                          If tmp:PurchaseCost <> sav:Purchase_Cost
                              shi:Information = Clip(shi:Information) & '<13,10>PURCHASE COST: ' & CLip(sav:Purchase_Cost)
                          End!If func:OldPurchaseCost <> func:NewPurchaseCost
                          If tmp:SaleCost <> sav:Sale_Cost
                              shi:Information = Clip(shi:Information) & '<13,10>TRADE PRICE: ' & CLip(sav:Sale_Cost)
                          End!If func:OldSaleCost <> func:NewSaleCost
                          If tmp:RetailCost <> sav:Retail_Cost
                              shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(sav:Retail_Cost)
                          End!If func:OldRetailCost <> func:NewRetailCost
                          If tmp:ShelfLocation <> sav:ShelfLocation
                              shi:Information = Clip(shi:Information) & '<13,10>SHELF LOCATION: ' & CLip(sav:ShelfLocation)
                          End !If tmp:ShelfLocatoin <> sav:ShelfLocation
                          If tmp:SecondLocation <> sav:SecondLocation
                              shi:Information = Clip(shi:Information) & '<13,10>RETAIL PRICE: ' & CLip(sav:SecondLocation)
                          End !If tmp:SecondLocation <> sav:SecondLocation
      
                          shi:notes                = 'DETAILS CHANGED'
                          if access:stohist.insert()
                             access:stohist.cancelautoinc()
                          end
                      end!if access:stohist.primerecord() = level:benign
      
                  Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                      !Error
                      Assert(0,'<13,10>Could not retrieve the associated part from Stock<13,10>')
                  End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
                  
          End!Case orp:Part_Type
      End!If tmp:CostsChanged = 1
      
      If quantity_temp = orp:quantity                                                             !If not values are changed.
          orp:number_received     = orp:quantity                                                  !Everythng has arrived
          orp:date_received       = date_received_temp
          orp:all_received        = 'YES'
          access:ordparts.update()
          quantity_to_job$ = 0
          Do Normal_Receive
      ElsIf quantity_temp < orp:quantity
          Case MessageEx('The quantity received is less than the quantity requested.<13,10>Do you wish to:<13,10><13,10>1) Ignore the difference and amend the original item<13,10>2) Re-Order the remaining items.<13,10>3) Remaining items will arrive later.','ServiceBase 2000','styles\Warn.ico','|&Ignore Difference|&Re-Order Remaining|&Will Arrive Later|&Cancel',4,4,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,'''''''''''',0+2+0+0,84,26,0)
              Of 1 ! &Ignore Difference Button
      
                  Do Normal_Receive
      
                  orp:quantity = quantity_temp
                  orp:all_received = 'YES'
                  access:ordparts.update()
              Of 2 ! &Re-Order Remaining Button
      
                  get(ordpend,0)
                  if access:ordpend.primerecord() = Level:Benign
                      ope:part_ref_number = orp:part_ref_number
                      ope:job_number      = orp:job_number
                      ope:part_type       = orp:part_type
                      ope:supplier        = ord:supplier
                      ope:part_number     = orp:part_number
                      ope:description     = orp:description
                      ope:quantity        = orp:quantity - quantity_temp
                      if access:ordpend.insert()
                         access:ordpend.cancelautoinc()
                      end
                  end !if access:ordpend.primerecord() = Level:Benign
      
                  Do Reorder_Receive
      
                  orp:quantity    = quantity_temp
                  orp:all_received = 'YES'
                  access:ordparts.update()
              Of 3 ! &Will Arrive Later Button
                  Do arrive_later_receive
      
                  glo:select3  = 'NEW ORDER'
                  glo:select4  = orp:record_number
                  glo:select5  = quantity_temp
                  orp:quantity    = orp:quantity - quantity_temp
                  glo:select6  = date_received_temp
                  orp:date_received = ''
                  access:ordparts.update()
              Of 4 ! &Cancel Button
                  RETURN Level:Fatal
          End!Case MessagheEx
      
      Elsif quantity_temp > orp:quantity
      
          Case MessageEx('The quantity received is higher than the quantity ordered.<13,10>Do you wish to:<13,10><13,10>1) Ignore this difference<13,10><13,10>2) Split the items and put the remainder into stock.','ServiceBase 2000','Styles\Warn.ico','|&Ignore Difference|&Split Items|&Cancel',3,3,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,'''''''',0+2+0+0,84,26,0)
              Of 1 ! &Ignore Difference Button
                  Do normal_receive
      
                  orp:quantity = quantity_temp
                  orp:all_received = 'YES'
                  access:ordparts.update()
      
              Of 2 ! &Split Items Button
                  Do split_receive
      
                  orp:all_received = 'YES'
                  access:ordparts.update()
      
              Of 3 ! &Cancel Button
                  Return Level:fatal
          End!Case MessageEx
      End !If quantity_temp = orp:quantity
      
      If no_of_labels_temp > 0
          Loop x# = 1 To no_of_labels_temp
              glo:select1  = orp:part_ref_number
              Case def:label_printer_type
                  Of 'TEC B-440 / B-442'
                      Stock_Label(date_received_temp,quantity_temp,orp:order_number,despatch_note_temp,tmp:SaleCost)
                  Of 'TEC B-452'
                      Stock_Label_B452(date_received_temp,quantity_temp,orp:order_number,despatch_note_temp,tmp:SaleCost)
              End!Case def:label_printer_type
              glo:select1 = ''
          End!Loop x# = 1 To no_of_labels_temp
      End!If no_of_labels_temp > 0
      If despatch_Note_temp <> ''
          glo:select24 = despatch_note_temp
      End!If despatch_Note_temp <> ''
      
      Do Replicate
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?tmp:ShelfLocation
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ShelfLocation, Accepted)
      If tmp:ShelfLocation{prop:Skip} = 0
      
      
      IF tmp:ShelfLocation OR ?tmp:ShelfLocation{Prop:Req}
        los:Shelf_Location = tmp:ShelfLocation
        los:Site_Location = sto:Location
        GLO:Select1 = sto:Location
        !Save Lookup Field Incase Of error
        look:tmp:ShelfLocation        = tmp:ShelfLocation
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            tmp:ShelfLocation = los:Shelf_Location
          ELSE
            CLEAR(los:Site_Location)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            tmp:ShelfLocation = look:tmp:ShelfLocation
            SELECT(?tmp:ShelfLocation)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      End !If tmp:ShelfLocation{prop:Skip} = 0
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:ShelfLocation, Accepted)
    OF ?LookupShelfLocation
      ThisWindow.Update
      los:Shelf_Location = tmp:ShelfLocation
      GLO:Select1 = sto:Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          tmp:ShelfLocation = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?tmp:ShelfLocation)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?tmp:ShelfLocation)
    OF ?tmp:PurchaseCost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PurchaseCost, Accepted)
      Do Hide_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?tmp:PurchaseCost, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          date_received_temp = TINCALENDARStyle1(date_received_temp)
          Display(?date_received_temp)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?PreviousDespatch
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PreviousDespatch, Accepted)
      Post(Event:AlertKey,0)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?PreviousDespatch, Accepted)
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
       POST(Event:CloseWindow)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Receive_Order')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?date_received_temp
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      access:ordparts.clearkey(orp:record_number_key)
      orp:record_number = glo:select2
      if access:ordparts.fetch(orp:record_number_key)
          Case MessageEx('Error! Cannot access the Order''s Parts File','ServiceBase 2000','styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0)
              Of 1 ! &OK Button
          End!Case MessageEx
          Return Level:Fatal
      end
      access:orders.clearkey(ord:order_number_key)
      ord:order_number = glo:select1
      if access:orders.fetch(ord:order_number_key)
          Case MessageEx('Error! Cannot access the Order File.','ServiceBase 2000','styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0)
              Of 1 ! &OK Button
          End!Case MessageEx
         Return Level:Fatal
      end
      !All Received
      If orp:all_received = 'YES'
          Hide(?OKbutton)
          Disable(?despatch_note_temp)
          Disable(?despatch_note_temp:prompt)
          Disable(?PreviousDespatch)
      Else
          orp:date_received = Today()
      End
      Display()
      
      If orp:Reason <> ''
          ?orp:Reason{prop:Hide} = 0
          ?orp:Reason:Prompt{prop:Hide} = 0
      End !orp:Reason <> ''
      If orp:Part_Type = 'STO'
          ?tmp:ShelfLocation{prop:Skip} = 0
          ?tmp:ShelfLocation{prop:ReadOnly} = 0
          ?tmp:SecondLocation{prop:Skip} = 0
          ?tmp:SecondLocation{prop:ReadOnly} = 0
          ?LookupShelfLocation{prop:Hide} = 0
      Else !orp:Part_Type = 'STO'
          ?tmp:ShelfLocation{prop:Skip} = 1
          ?tmp:ShelfLocation{prop:ReadOnly} = 1
          ?tmp:SecondLocation{prop:Skip} = 1
          ?tmp:SecondLocation{prop:ReadOnly} = 1
          ?LookupShelfLocation{prop:Hide} = 1
      End !orp:Part_Type = 'STO'
      !ThisMakeOver.SetWindow(Win:FORM)
      
      quantity_temp       = ORP:Quantity
      date_received_temp  = orp:date_received
      tmp:PurchaseCost  = ORP:Purchase_Cost
      tmp:SaleCost      = ORP:Sale_Cost
      tmp:RetailCost    = orp:retail_cost
      tmp:partnumber      = orp:part_number
      tmp:description     = orp:description
      
      despatch_note_temp  = orp:DespatchNoteNumber
      !espatch_note_temp  =
      
      !Get the Part's Details from stock, if the ref number has been saved.
      !If not, use the main store location.
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = orp:Part_Ref_Number
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
      
      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
          Access:STOCK.ClearKey(sto:Location_Key)
          sto:Location    = MainStoreLocation()
          sto:Part_Number = orp:Part_Number
          If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Found
          Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
              !Error
              !Assert(0,'<13,10>Fetch Error<13,10>')
          End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
      
      ! Start Change 2342 BE(17/03/03)
      percentage_mark_up_temp = sto:Percentage_Mark_Up
      If (percentage_mark_up_temp <> '') THEN
          ! Do Not Re-Calculate SaleCost at this stage
          ! Only if Purchase Cost is subsequently changed
          ! By User
          ?tmp:SaleCost{prop:readonly} = 1
          ?tmp:SaleCost{prop:skip} = 1
          ?tmp:SaleCost{prop:color} = color:silver
      End
      ! End Change 2342 BE(17/03/03)
      
      tmp:ShelfLocation   = sto:Shelf_Location
      tmp:SecondLocation  = sto:Second_Location
      sav:ShelfLocation   = tmp:ShelfLocation
      sav:SecondLocation  = tmp:SecondLocation
      sav:partnumber      = tmp:PartNumber
      sav:description     = tmp:Description
      sav:purchase_cost   = tmp:PurchaseCost
      sav:retail_cost     = tmp:RetailCost
      sav:sale_cost       = tmp:SaleCost
      !Labels
      set(defaults)
      access:defaults.next()
      If def:receive_stock_label = 'YES'
          Enable(?no_of_labels_temp)
          no_of_labels_temp = 1
      Else!If def:stock_label = 'YES'
          Disable(?no_of_labels_temp)
          no_of_labels_temp = 0
      End!If def:stock_label = 'YES'
      
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      If orp:all_received <> 'YES'
          If glo:select24 <> ''
              despatch_note_temp = glo:select24
          Else!If glo:select24 <> ''
              Case MessageEx('You have not entered a despatch note number previously.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End!If glo:select24 <> ''
          Display(?despatch_note_temp)
      End!If orp:all_received <> 'YES'
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
UpdateStockHistory  PROCEDURE(IN:PartRefNum, IN:OrderNumber, IN:Supplier, IN:Qty, IN:DespatchNoteNumber, IN:JobRef)    ! Start Change 3505 BE(10/11/03)
    CODE
    access:stock.clearkey(sto:ref_number_key)
    sto:ref_number = IN:PartRefNum
    IF ( access:stock.fetch(sto:ref_number_key) = Level:Benign) THEN

        access:users.clearkey(use:password_key)
        use:password = glo:password
        access:users.tryfetch(use:password_key)
        usercode" = use:user_code

        IF (Access:STOHIST.Primerecord() = Level:Benign) THEN
            shi:Ref_Number              = IN:PartRefNum
            shi:Transaction_Type        = 'ADD'
            shi:Despatch_Note_Number    = IN:DespatchNoteNumber
            shi:Quantity                = IN:Qty
            shi:Date                    = Today()
            shi:Purchase_Cost           = sto:Purchase_Cost
            shi:Sale_Cost               = sto:Sale_Cost
            shi:User                    = usercode"
            shi:Notes                   = 'STOCK ADDED FROM ORDER'
            shi:information          = 'ORDER NUMBER: ' & Clip(IN:OrderNumber) & '<13,10>SUPPLIER: ' & Clip(IN:Supplier) &|
                                        '<13,10>DESPATCH NOTE NO: ' & Clip(IN:DespatchNoteNumber)
            IF (Access:STOHIST.Tryinsert() = Level:Benign) THEN
                Access:STOHIST.Cancelautoinc()
            END
        END

        IF (access:STOHIST.primerecord() = Level:Benign) THEN
            shi:ref_number = IN:PartRefNum
            shi:transaction_type = 'DEC'
            shi:despatch_note_number = IN:DespatchNoteNumber
            shi:quantity = IN:Qty
            shi:Date = Today()
            shi:purchase_cost = sto:purchase_cost
            shi:sale_cost = sto:sale_cost
            shi:job_number = IN:JobRef
            shi:user = usercode"
            shi:notes = 'STOCK DECREMENTED'
            IF (access:STOHIST.tryinsert()) THEN
                access:STOHIST.cancelautoinc()
            END
        END

        ! Start Change 4801 BE(12/10/2004)
        ! Start Change 4803 BE(11/10/2004)
        !ReprintPartReceivedPickNote(IN:JobRef, IN:PartRefNum)
        ! End Change 4803 BE(11/10/2004)
        ReprintPartReceivedPickNote(IN:JobRef, IN:PartRefNum, true)
        ! End Change 4801 BE(12/10/2004)

    END

    RETURN
! Start Change 3505 BE(10/11/03)
! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
