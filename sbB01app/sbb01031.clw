

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01031.INC'),ONCE        !Local module procedure declarations
                     END


UpdateOrderStock PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
History::orstmp:Record LIKE(orstmp:RECORD),STATIC
QuickWindow          WINDOW('Update the ORDSTOCK File'),AT(,,196,112),FONT('MS Sans Serif',8,,),IMM,HLP('UpdateOrderStock'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,188,86),USE(?CurrentTab)
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Record Number'),AT(8,20),USE(?orstmp:RecordNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,20,40,10),USE(orstmp:RecordNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Record Number'),TIP('Record Number'),UPR
                           PROMPT('Order Number'),AT(8,34),USE(?orstmp:OrderNumber:Prompt),TRN
                           ENTRY(@s8),AT(64,34,40,10),USE(orstmp:OrderNumber),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Order Number'),TIP('Order Number'),UPR
                           PROMPT('Part Number'),AT(8,48),USE(?orstmp:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,48,124,10),USE(orstmp:PartNumber),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR
                           PROMPT('Location'),AT(8,62),USE(?orstmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(64,62,124,10),USE(orstmp:Location),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),UPR
                           PROMPT('Quantity'),AT(8,76),USE(?orstmp:Quantity:Prompt),TRN
                           SPIN(@n8),AT(64,76,51,10),USE(orstmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity'),TIP('Quantity'),UPR,STEP(1)
                         END
                       END
                       BUTTON('OK'),AT(98,94,45,14),USE(?OK),DEFAULT
                       BUTTON('Cancel'),AT(147,94,45,14),USE(?Cancel)
                       BUTTON('Help'),AT(147,4,45,14),USE(?Help),HIDE,STD(STD:Help)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
ToolbarForm          ToolbarUpdateClass               !Form Toolbar Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?orstmp:RecordNumber:Prompt{prop:FontColor} = -1
    ?orstmp:RecordNumber:Prompt{prop:Color} = 15066597
    If ?orstmp:RecordNumber{prop:ReadOnly} = True
        ?orstmp:RecordNumber{prop:FontColor} = 65793
        ?orstmp:RecordNumber{prop:Color} = 15066597
    Elsif ?orstmp:RecordNumber{prop:Req} = True
        ?orstmp:RecordNumber{prop:FontColor} = 65793
        ?orstmp:RecordNumber{prop:Color} = 8454143
    Else ! If ?orstmp:RecordNumber{prop:Req} = True
        ?orstmp:RecordNumber{prop:FontColor} = 65793
        ?orstmp:RecordNumber{prop:Color} = 16777215
    End ! If ?orstmp:RecordNumber{prop:Req} = True
    ?orstmp:RecordNumber{prop:Trn} = 0
    ?orstmp:RecordNumber{prop:FontStyle} = font:Bold
    ?orstmp:OrderNumber:Prompt{prop:FontColor} = -1
    ?orstmp:OrderNumber:Prompt{prop:Color} = 15066597
    If ?orstmp:OrderNumber{prop:ReadOnly} = True
        ?orstmp:OrderNumber{prop:FontColor} = 65793
        ?orstmp:OrderNumber{prop:Color} = 15066597
    Elsif ?orstmp:OrderNumber{prop:Req} = True
        ?orstmp:OrderNumber{prop:FontColor} = 65793
        ?orstmp:OrderNumber{prop:Color} = 8454143
    Else ! If ?orstmp:OrderNumber{prop:Req} = True
        ?orstmp:OrderNumber{prop:FontColor} = 65793
        ?orstmp:OrderNumber{prop:Color} = 16777215
    End ! If ?orstmp:OrderNumber{prop:Req} = True
    ?orstmp:OrderNumber{prop:Trn} = 0
    ?orstmp:OrderNumber{prop:FontStyle} = font:Bold
    ?orstmp:PartNumber:Prompt{prop:FontColor} = -1
    ?orstmp:PartNumber:Prompt{prop:Color} = 15066597
    If ?orstmp:PartNumber{prop:ReadOnly} = True
        ?orstmp:PartNumber{prop:FontColor} = 65793
        ?orstmp:PartNumber{prop:Color} = 15066597
    Elsif ?orstmp:PartNumber{prop:Req} = True
        ?orstmp:PartNumber{prop:FontColor} = 65793
        ?orstmp:PartNumber{prop:Color} = 8454143
    Else ! If ?orstmp:PartNumber{prop:Req} = True
        ?orstmp:PartNumber{prop:FontColor} = 65793
        ?orstmp:PartNumber{prop:Color} = 16777215
    End ! If ?orstmp:PartNumber{prop:Req} = True
    ?orstmp:PartNumber{prop:Trn} = 0
    ?orstmp:PartNumber{prop:FontStyle} = font:Bold
    ?orstmp:Location:Prompt{prop:FontColor} = -1
    ?orstmp:Location:Prompt{prop:Color} = 15066597
    If ?orstmp:Location{prop:ReadOnly} = True
        ?orstmp:Location{prop:FontColor} = 65793
        ?orstmp:Location{prop:Color} = 15066597
    Elsif ?orstmp:Location{prop:Req} = True
        ?orstmp:Location{prop:FontColor} = 65793
        ?orstmp:Location{prop:Color} = 8454143
    Else ! If ?orstmp:Location{prop:Req} = True
        ?orstmp:Location{prop:FontColor} = 65793
        ?orstmp:Location{prop:Color} = 16777215
    End ! If ?orstmp:Location{prop:Req} = True
    ?orstmp:Location{prop:Trn} = 0
    ?orstmp:Location{prop:FontStyle} = font:Bold
    ?orstmp:Quantity:Prompt{prop:FontColor} = -1
    ?orstmp:Quantity:Prompt{prop:Color} = 15066597
    If ?orstmp:Quantity{prop:ReadOnly} = True
        ?orstmp:Quantity{prop:FontColor} = 65793
        ?orstmp:Quantity{prop:Color} = 15066597
    Elsif ?orstmp:Quantity{prop:Req} = True
        ?orstmp:Quantity{prop:FontColor} = 65793
        ?orstmp:Quantity{prop:Color} = 8454143
    Else ! If ?orstmp:Quantity{prop:Req} = True
        ?orstmp:Quantity{prop:FontColor} = 65793
        ?orstmp:Quantity{prop:Color} = 16777215
    End ! If ?orstmp:Quantity{prop:Req} = True
    ?orstmp:Quantity{prop:Trn} = 0
    ?orstmp:Quantity{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateOrderStock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateOrderStock',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateOrderStock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:RecordNumber:Prompt;  SolaceCtrlName = '?orstmp:RecordNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:RecordNumber;  SolaceCtrlName = '?orstmp:RecordNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:OrderNumber:Prompt;  SolaceCtrlName = '?orstmp:OrderNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:OrderNumber;  SolaceCtrlName = '?orstmp:OrderNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:PartNumber:Prompt;  SolaceCtrlName = '?orstmp:PartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:PartNumber;  SolaceCtrlName = '?orstmp:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Location:Prompt;  SolaceCtrlName = '?orstmp:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Location;  SolaceCtrlName = '?orstmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Quantity:Prompt;  SolaceCtrlName = '?orstmp:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Quantity;  SolaceCtrlName = '?orstmp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Help;  SolaceCtrlName = '?Help';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateOrderStock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateOrderStock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orstmp:RecordNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(orstmp:Record,History::orstmp:Record)
  SELF.AddHistoryField(?orstmp:RecordNumber,1)
  SELF.AddHistoryField(?orstmp:OrderNumber,2)
  SELF.AddHistoryField(?orstmp:PartNumber,3)
  SELF.AddHistoryField(?orstmp:Location,4)
  SELF.AddHistoryField(?orstmp:Quantity,5)
  SELF.AddUpdateFile(Access:ORDSTOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDSTOCK.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  ToolBarForm.HelpButton=?Help
  SELF.AddItem(ToolbarForm)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDSTOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateOrderStock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateOrderStock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

