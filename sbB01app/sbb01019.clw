

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01019.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Stock_History PROCEDURE                        !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
Date_In_Temp         DATE
Date_Out_Temp        DATE
ref_number_temp      STRING(20)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(STOHIST)
                       PROJECT(shi:Despatch_Note_Number)
                       PROJECT(shi:Quantity)
                       PROJECT(shi:Purchase_Cost)
                       PROJECT(shi:Sale_Cost)
                       PROJECT(shi:Retail_Cost)
                       PROJECT(shi:User)
                       PROJECT(shi:Notes)
                       PROJECT(shi:Information)
                       PROJECT(shi:Record_Number)
                       PROJECT(shi:Ref_Number)
                       PROJECT(shi:Date)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
Date_In_Temp           LIKE(Date_In_Temp)             !List box control field - type derived from local data
Date_Out_Temp          LIKE(Date_Out_Temp)            !List box control field - type derived from local data
shi:Despatch_Note_Number LIKE(shi:Despatch_Note_Number) !List box control field - type derived from field
shi:Quantity           LIKE(shi:Quantity)             !List box control field - type derived from field
ref_number_temp        LIKE(ref_number_temp)          !List box control field - type derived from local data
shi:Purchase_Cost      LIKE(shi:Purchase_Cost)        !List box control field - type derived from field
shi:Sale_Cost          LIKE(shi:Sale_Cost)            !List box control field - type derived from field
shi:Retail_Cost        LIKE(shi:Retail_Cost)          !List box control field - type derived from field
shi:User               LIKE(shi:User)                 !List box control field - type derived from field
shi:Notes              LIKE(shi:Notes)                !List box control field - type derived from field
shi:Information        LIKE(shi:Information)          !Browse hot field - type derived from field
shi:Record_Number      LIKE(shi:Record_Number)        !Primary key field - type derived from field
shi:Ref_Number         LIKE(shi:Ref_Number)           !Browse key field - type derived from field
shi:Date               LIKE(shi:Date)                 !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Stock History File'),AT(,,668,348),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Stock_History'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,20,652,232),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('49R(2)|M~Date In~C(0)@d6b@49R(2)|M~Date Out~C(0)@d6b@87L(2)|M~Despatch Note Numb' &|
   'er~@s30@41R(2)|M~Quantity~C(0)@n8@51L(2)|M~Job/Retail No~@s10@59R(2)|M~Purchase ' &|
   'Cost~@n-14.2@53R(2)|M~Sale Cost~@n-14.2@56R(2)|M~Retail Cost~@n14.2@21L(2)|M~Use' &|
   'r~@s3@80L(2)|M~Status~@s255@'),FROM(Queue:Browse:1)
                       SHEET,AT(4,4,660,252),USE(?CurrentTab),SPREAD
                         TAB('By Date'),USE(?Tab:2)
                         END
                       END
                       BUTTON('Close'),AT(588,324,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                       SHEET,AT(4,260,492,84),USE(?Sheet2),WIZARD
                         TAB('Tab 2'),USE(?Tab2)
                           TEXT,AT(8,264,484,76),USE(shi:Information),SKIP,VSCROLL,FONT('Tahoma',8,,FONT:bold),READONLY
                         END
                       END
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
SetQueueRecord         PROCEDURE(),DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?shi:Information{prop:ReadOnly} = True
        ?shi:Information{prop:FontColor} = 65793
        ?shi:Information{prop:Color} = 15066597
    Elsif ?shi:Information{prop:Req} = True
        ?shi:Information{prop:FontColor} = 65793
        ?shi:Information{prop:Color} = 8454143
    Else ! If ?shi:Information{prop:Req} = True
        ?shi:Information{prop:FontColor} = 65793
        ?shi:Information{prop:Color} = 16777215
    End ! If ?shi:Information{prop:Req} = True
    ?shi:Information{prop:Trn} = 0
    ?shi:Information{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Stock_History',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Stock_History',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Stock_History',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Stock_History',1)
    SolaceViewVars('Date_In_Temp',Date_In_Temp,'Browse_Stock_History',1)
    SolaceViewVars('Date_Out_Temp',Date_Out_Temp,'Browse_Stock_History',1)
    SolaceViewVars('ref_number_temp',ref_number_temp,'Browse_Stock_History',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?shi:Information;  SolaceCtrlName = '?shi:Information';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Stock_History')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Stock_History')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:STOCK.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOHIST,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,shi:Ref_Number_Key)
  BRW1.AddRange(shi:Ref_Number,GLO:Select1)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(,shi:Date,1,BRW1)
  BIND('Date_In_Temp',Date_In_Temp)
  BIND('Date_Out_Temp',Date_Out_Temp)
  BIND('ref_number_temp',ref_number_temp)
  BIND('GLO:Select1',GLO:Select1)
  BRW1.AddField(Date_In_Temp,BRW1.Q.Date_In_Temp)
  BRW1.AddField(Date_Out_Temp,BRW1.Q.Date_Out_Temp)
  BRW1.AddField(shi:Despatch_Note_Number,BRW1.Q.shi:Despatch_Note_Number)
  BRW1.AddField(shi:Quantity,BRW1.Q.shi:Quantity)
  BRW1.AddField(ref_number_temp,BRW1.Q.ref_number_temp)
  BRW1.AddField(shi:Purchase_Cost,BRW1.Q.shi:Purchase_Cost)
  BRW1.AddField(shi:Sale_Cost,BRW1.Q.shi:Sale_Cost)
  BRW1.AddField(shi:Retail_Cost,BRW1.Q.shi:Retail_Cost)
  BRW1.AddField(shi:User,BRW1.Q.shi:User)
  BRW1.AddField(shi:Notes,BRW1.Q.shi:Notes)
  BRW1.AddField(shi:Information,BRW1.Q.shi:Information)
  BRW1.AddField(shi:Record_Number,BRW1.Q.shi:Record_Number)
  BRW1.AddField(shi:Ref_Number,BRW1.Q.shi:Ref_Number)
  BRW1.AddField(shi:Date,BRW1.Q.shi:Date)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Stock_History',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Stock_History')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  CASE (shi:Transaction_Type)
  OF 'ADD'
    Date_In_Temp = shi:Date
  OF 'REC'
    Date_In_Temp = shi:Date
  ELSE
    Date_In_Temp = ''
  END
  IF (shi:Transaction_Type = 'DEC')
    Date_Out_Temp = shi:Date
  ELSE
    Date_Out_Temp = ''
  END
  IF (shi:Job_Number <> 0)
    ref_number_temp = shi:Job_Number
  ELSE
    ref_number_temp = 'Stock'
  END
  PARENT.SetQueueRecord
  SELF.Q.Date_In_Temp = Date_In_Temp                  !Assign formula result to display queue
  SELF.Q.Date_Out_Temp = Date_Out_Temp                !Assign formula result to display queue
  SELF.Q.ref_number_temp = ref_number_temp            !Assign formula result to display queue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

