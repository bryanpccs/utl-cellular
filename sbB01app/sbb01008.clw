

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01008.INC'),ONCE        !Local module procedure declarations
                       INCLUDE('SBB01007.INC'),ONCE        !Req'd for module callout resolution
                     END


UpdateSTOCK PROCEDURE                                 !Generated from procedure template - Window

CurrentTab           STRING(80)
save_ccp_id          USHORT,AUTO
save_cwp_id          USHORT,AUTO
save_sto_ali_id      USHORT,AUTO
average_temp         LONG
save_shi_id          USHORT,AUTO
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
Average_text_temp    STRING(8)
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
pos                  STRING(255)
sav2:group           GROUP,PRE(sav2)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
Retail_Cost          REAL
Percentage_Mark_Up   REAL
Shelf_Location       STRING(30)
Manufacturer         STRING(30)
Second_Location      STRING(30)
Minimum_Level        REAL
Reorder_Level        REAL
Accessory            STRING('''NO''')
ExchangeUnit         STRING('''NO''')
Suspend              BYTE(0)
                     END
sav:group            GROUP,PRE(sav)
Part_Number          STRING(30)
Description          STRING(30)
Supplier             STRING(30)
Purchase_Cost        REAL
Sale_Cost            REAL
Retail_Cost          REAL
Percentage_Mark_Up   REAL
Shelf_Location       STRING(30)
Manufacturer         STRING(30)
Second_Location      STRING(30)
Minimum_Level        REAL
Reorder_Level        REAL
Accessory            STRING('''NO''')
ExchangeUnit         STRING('''NO''')
Suspend              BYTE(0)
                     END
ref_number_temp      LONG
Part_Number_temp     STRING(30)
save_ope_id          USHORT,AUTO
Description_temp     STRING(30)
Manufacturer_temp    STRING(30)
Accessory_temp       STRING('''NO''')
save_orp_id          USHORT,AUTO
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
CPCSExecutePopUp     BYTE
FP::PopupString      STRING(4096)
FP::ReturnValues     QUEUE,PRE(FP:)
RVPtr                LONG
                     END
Replicate_Temp       STRING('NO {1}')
OtherLocationQueue   QUEUE,PRE(othque)
Location             STRING(30)
QuantityInStock      LONG
Ref_Number           LONG
                     END
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:2 QUEUE                           !Queue declaration for browse/combo box using ?sto:Manufacturer
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB7::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
BRW8::View:Browse    VIEW(STOMODEL)
                       PROJECT(stm:Model_Number)
                       PROJECT(stm:Ref_Number)
                       PROJECT(stm:Manufacturer)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
stm:Model_Number       LIKE(stm:Model_Number)         !List box control field - type derived from field
stm:Ref_Number         LIKE(stm:Ref_Number)           !Primary key field - type derived from field
stm:Manufacturer       LIKE(stm:Manufacturer)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::sto:Record  LIKE(sto:RECORD),STATIC
QuickWindow          WINDOW('Update Stock'),AT(,,543,348),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('UpdateSTOCK'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,232,224),USE(?Sheet2),WIZARD,SPREAD
                         TAB('General Details'),USE(?Tab3)
                           GROUP('Part Details'),AT(8,8,220,44),USE(?PartDetailsGroup),BOXED
                             PROMPT('Part Number'),AT(12,20),USE(?STO:Part_Number:Prompt),TRN
                             ENTRY(@s30),AT(84,20,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                             PROMPT('Description'),AT(12,36),USE(?STO:Description:Prompt),TRN
                             ENTRY(@s30),AT(84,36,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),REQ,UPR
                           END
                           GROUP('Location Details'),AT(8,56,220,72),USE(?LocationDetailsGroup),BOXED
                             PROMPT('Location'),AT(12,67),USE(?sto:Location:Prompt),TRN
                             ENTRY(@s30),AT(84,64,124,10),USE(sto:Location),SKIP,FONT('Tahoma',8,,FONT:bold),UPR,READONLY
                             PROMPT('Shelf Location'),AT(12,80),USE(?STO:Shelf_Location:Prompt),TRN
                             ENTRY(@s30),AT(84,80,124,10),USE(sto:Shelf_Location),FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                             BUTTON,AT(212,80,10,10),USE(?Lookup_Shelf_Location),ICON('list3.ico')
                             PROMPT('2nd Location'),AT(12,96),USE(?STO:Second_Location:Prompt),TRN
                             ENTRY(@s30),AT(84,96,124,10),USE(sto:Second_Location),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White),UPR
                             PROMPT('Supplier'),AT(12,112,26,10),USE(?STO:Supplier:Prompt),TRN
                             ENTRY(@s30),AT(84,112,124,10),USE(sto:Supplier),FONT('Tahoma',8,,FONT:bold),ALRT(MouseLeft2),ALRT(EnterKey),REQ,UPR
                             BUTTON,AT(212,112,10,10),USE(?Lookup_Supplier),ICON('list3.ico')
                           END
                           GROUP('Cost Details'),AT(8,132,220,92),USE(?CostDetailsGroup),BOXED
                             PROMPT('Purchase Cost'),AT(12,144),USE(?STO:Purchase_Cost:Prompt),TRN
                             ENTRY(@n14.2),AT(84,144,64,10),USE(sto:Purchase_Cost),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             PROMPT('Percentage Mark Up'),AT(12,160),USE(?STO:Percentage_Mark_Up:Prompt),TRN
                             ENTRY(@n6.2),AT(84,160,64,10),USE(sto:Percentage_Mark_Up),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White)
                             PROMPT('  %  '),AT(152,160),USE(?Prompt18),TRN,COLOR(COLOR:Silver)
                             PROMPT('Trade Price'),AT(12,176),USE(?STO:Sale_Cost:Prompt),TRN
                             ENTRY(@n14.2),AT(84,176,64,10),USE(sto:Sale_Cost),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             CHECK('Accessory'),AT(84,208),USE(sto:Accessory),TRN,COLOR(COLOR:Silver),VALUE('YES','NO')
                             ENTRY(@n14.2),AT(160,208,64,10),USE(sto:AccessoryCost),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Accessory Cost'),TIP('Accessory Cost'),UPR
                             PROMPT('Retail Price'),AT(12,192),USE(?STO:Retail_Cost:Prompt),TRN
                             ENTRY(@n14.2),AT(84,192,64,10),USE(sto:Retail_Cost),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),COLOR(COLOR:White)
                             PROMPT('Accessory Cost'),AT(160,200),USE(?sto:AccessoryCost:Prompt),TRN,FONT(,7,,)
                           END
                         END
                       END
                       SHEET,AT(384,4,156,224),USE(?Sheet1),WIZARD,SPREAD
                         TAB('Model Details'),USE(?Model_Number_Tab)
                           PROMPT('Model Numbers That This Part Can Be Used For (Service Repairs Only)'),AT(388,8,148,24),USE(?Prompt26)
                           PROMPT('Manufacturer'),AT(388,32),USE(?Prompt27),FONT(,7,,)
                           COMBO(@s30),AT(388,40,124,10),USE(sto:Manufacturer),VSCROLL,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:2)
                           LIST,AT(388,56,148,124),USE(?List),IMM,VSCROLL,COLOR(COLOR:White),MSG('Browsing Records'),FORMAT('120L(2)~Model Number~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(388,184,56,16),USE(?Insert),LEFT,ICON('insert.gif')
                           BUTTON('&Delete'),AT(480,184,56,16),USE(?Delete),LEFT,ICON('delete.gif')
                           GROUP('Fault Codes Group'),AT(384,204,152,20),USE(?Fault_Codes_Group),HIDE
                             BUTTON('View Codes'),AT(480,204,56,16),USE(?Fault_Codes_Button),HIDE,LEFT,ICON('spy.ico')
                             CHECK('Assign Fault Codes'),AT(388,208),USE(sto:Assign_Fault_Codes),LEFT,VALUE('YES','NO')
                           END
                         END
                         TAB('Serial Numbers'),USE(?Serial_Number_Tab),HIDE
                           ENTRY(@s16),AT(388,20,124,10),USE(ste:Serial_Number),LEFT,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                         END
                       END
                       SHEET,AT(240,232,140,84),USE(?Sheet5),SPREAD
                         TAB('Stock Usage'),USE(?Tab7)
                           PROMPT('0 - 7 Days'),AT(244,244),USE(?Prompt5)
                           STRING(@n-14),AT(300,244),USE(days_7_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(244,256),USE(?Prompt5:2)
                           STRING(@n-14),AT(300,256),USE(days_30_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('31 - 60 Days'),AT(244,268),USE(?Prompt5:3)
                           STRING(@n-14),AT(300,268),USE(days_60_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('61 - 90 Days'),AT(244,280),USE(?Prompt5:4)
                           STRING(@n-14),AT(300,280),USE(days_90_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(303,292,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Average Daily Use'),AT(244,296),USE(?Prompt5:5)
                           STRING(@s8),AT(332,296),USE(Average_text_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       SHEET,AT(384,232,156,84),USE(?Sheet7),WIZARD,SPREAD
                         TAB('Tab 8'),USE(?Tab8)
                           PROMPT('Stock In Other Locations'),AT(388,236),USE(?Prompt25)
                           LIST,AT(388,248,148,64),USE(?List2),VSCROLL,FORMAT('96L(2)|M~Location~@s30@32L(2)|M~Qty In Stock~@s8@'),FROM(OtherLocationQueue)
                         END
                       END
                       SHEET,AT(240,4,140,224),USE(?Sheet6),WIZARD,SPREAD
                         TAB('Tab 6'),USE(?Tab6)
                           GROUP('Part Defaults'),AT(244,12,132,212),USE(?PartTypeDetailsGroup),BOXED
                             CHECK('Sundry Item'),AT(252,24),USE(sto:Sundry_Item),TRN,COLOR(COLOR:Silver),VALUE('YES','NO')
                             CHECK('Exchange Unit'),AT(252,48),USE(sto:ExchangeUnit),TRN,RIGHT,VALUE('YES','NO')
                             CHECK('Suspend Part'),AT(252,36),USE(sto:Suspend),TRN,MSG('Suspend Part'),TIP('Suspend Part'),VALUE('1','0')
                             GROUP('Access Levels'),AT(248,88,124,20),USE(?AccessLevels),BOXED
                               CHECK('E1'),AT(256,96),USE(sto:E1),MSG('E1'),TIP('E1'),VALUE('1','0')
                               CHECK('E2'),AT(298,96),USE(sto:E2),MSG('E2'),TIP('E2'),VALUE('1','0')
                               CHECK('E3'),AT(340,96),USE(sto:E3),MSG('E3'),TIP('E3'),VALUE('1','0')
                             END
                             CHECK('Exclude From EDI'),AT(252,112),USE(sto:ExcludeFromEDI),TRN,MSG('Exclude From EDI'),TIP('Exclude From EDI'),VALUE('1','0')
                             CHECK('Allow Duplicate Part On Jobs'),AT(252,60),USE(sto:AllowDuplicate),TRN,VALUE('1','0'),MSG('Allow Duplicate Part On Jobs')
                             CHECK('RF Board'),AT(252,72),USE(sto:RF_Board),TRN,VALUE('1','0')
                             STRING(@p<<<<<<<<#p),AT(284,144),USE(sto:Ref_Number),HIDE
                             GROUP,AT(248,180,151,32),USE(?Stock_Levels_Group),HIDE
                               PROMPT('Minimum Level '),AT(252,184,67,11),USE(?STO:Minimum_Level:Prompt),TRN,COLOR(COLOR:Silver)
                               ENTRY(@n8),AT(328,184,64,10),USE(sto:Minimum_Level),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                               PROMPT('Make Order Up To'),AT(252,200),USE(?STO:Reorder_Level:Prompt),TRN,COLOR(COLOR:Silver)
                               ENTRY(@n8),AT(328,200,64,10),USE(sto:Reorder_Level),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                             END
                           END
                         END
                       END
                       SHEET,AT(4,232,232,84),USE(?Sheet4),SPREAD
                         TAB('Availability Details'),USE(?Tab5)
                           STRING('Quantity In Stock'),AT(8,252),USE(?String4),FONT(,12,,),COLOR(COLOR:Silver)
                           PROMPT('Stock'),AT(96,252,44,12),USE(?Stock_Prompt),RIGHT,FONT('Tahoma',12,,FONT:bold),COLOR(COLOR:Silver)
                           BUTTON('Zero Quantity'),AT(145,252,67,13),USE(?Clear_Stock),SKIP,LEFT,TIP('Reset Quantity In Stock'),ICON('delete.ico')
                           STRING('Quantity To Order'),AT(8,272),USE(?String8),FONT('Tahoma',12,,),COLOR(COLOR:Silver)
                           PROMPT('To Order'),AT(96,272,44,12),USE(?To_Order_Prompt),RIGHT,FONT(,12,,FONT:bold),COLOR(COLOR:Silver)
                           STRING('Quantity On Order'),AT(8,292),USE(?String10),FONT(,12,,),COLOR(COLOR:Silver)
                           PROMPT('On Order'),AT(96,292,44,12),USE(?On_Order_Prompt),RIGHT,FONT(,12,,FONT:bold),COLOR(COLOR:Silver)
                           PROMPT('(All Locations)'),AT(148,292),USE(?SummaryOrdersPrompt),TRN,HIDE,FONT(,12,,)
                         END
                       END
                       BUTTON('&OK'),AT(424,324,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(480,324,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,320,536,24),USE(?Panel1),FILL(COLOR:Silver)
                       CHECK('Replicate To All Sites'),AT(8,328),USE(Replicate_Temp),HIDE,COLOR(COLOR:Silver),VALUE('YES','NO')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Reset                  PROCEDURE(BYTE Force=0),DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB7                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:2         !Reference to browse queue type
                     END

BRW8                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW8::Sort0:Locator  StepLocatorClass                 !Default Locator
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
! Moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,43,161,10),USE(?progress:text),CENTER,FONT('Tahoma',8,,)
     END
save_stm_id   ushort,auto
save_map_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:sto:Shelf_Location                Like(sto:Shelf_Location)
look:sto:Supplier                Like(sto:Supplier)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?PartDetailsGroup{prop:Font,3} = -1
    ?PartDetailsGroup{prop:Color} = 15066597
    ?PartDetailsGroup{prop:Trn} = 0
    ?STO:Part_Number:Prompt{prop:FontColor} = -1
    ?STO:Part_Number:Prompt{prop:Color} = 15066597
    If ?sto:Part_Number{prop:ReadOnly} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 15066597
    Elsif ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 8454143
    Else ! If ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 16777215
    End ! If ?sto:Part_Number{prop:Req} = True
    ?sto:Part_Number{prop:Trn} = 0
    ?sto:Part_Number{prop:FontStyle} = font:Bold
    ?STO:Description:Prompt{prop:FontColor} = -1
    ?STO:Description:Prompt{prop:Color} = 15066597
    If ?sto:Description{prop:ReadOnly} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 15066597
    Elsif ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 8454143
    Else ! If ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 16777215
    End ! If ?sto:Description{prop:Req} = True
    ?sto:Description{prop:Trn} = 0
    ?sto:Description{prop:FontStyle} = font:Bold
    ?LocationDetailsGroup{prop:Font,3} = -1
    ?LocationDetailsGroup{prop:Color} = 15066597
    ?LocationDetailsGroup{prop:Trn} = 0
    ?sto:Location:Prompt{prop:FontColor} = -1
    ?sto:Location:Prompt{prop:Color} = 15066597
    If ?sto:Location{prop:ReadOnly} = True
        ?sto:Location{prop:FontColor} = 65793
        ?sto:Location{prop:Color} = 15066597
    Elsif ?sto:Location{prop:Req} = True
        ?sto:Location{prop:FontColor} = 65793
        ?sto:Location{prop:Color} = 8454143
    Else ! If ?sto:Location{prop:Req} = True
        ?sto:Location{prop:FontColor} = 65793
        ?sto:Location{prop:Color} = 16777215
    End ! If ?sto:Location{prop:Req} = True
    ?sto:Location{prop:Trn} = 0
    ?sto:Location{prop:FontStyle} = font:Bold
    ?STO:Shelf_Location:Prompt{prop:FontColor} = -1
    ?STO:Shelf_Location:Prompt{prop:Color} = 15066597
    If ?sto:Shelf_Location{prop:ReadOnly} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 15066597
    Elsif ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 8454143
    Else ! If ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 16777215
    End ! If ?sto:Shelf_Location{prop:Req} = True
    ?sto:Shelf_Location{prop:Trn} = 0
    ?sto:Shelf_Location{prop:FontStyle} = font:Bold
    ?STO:Second_Location:Prompt{prop:FontColor} = -1
    ?STO:Second_Location:Prompt{prop:Color} = 15066597
    If ?sto:Second_Location{prop:ReadOnly} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 15066597
    Elsif ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 8454143
    Else ! If ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 16777215
    End ! If ?sto:Second_Location{prop:Req} = True
    ?sto:Second_Location{prop:Trn} = 0
    ?sto:Second_Location{prop:FontStyle} = font:Bold
    ?STO:Supplier:Prompt{prop:FontColor} = -1
    ?STO:Supplier:Prompt{prop:Color} = 15066597
    If ?sto:Supplier{prop:ReadOnly} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 15066597
    Elsif ?sto:Supplier{prop:Req} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 8454143
    Else ! If ?sto:Supplier{prop:Req} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 16777215
    End ! If ?sto:Supplier{prop:Req} = True
    ?sto:Supplier{prop:Trn} = 0
    ?sto:Supplier{prop:FontStyle} = font:Bold
    ?CostDetailsGroup{prop:Font,3} = -1
    ?CostDetailsGroup{prop:Color} = 15066597
    ?CostDetailsGroup{prop:Trn} = 0
    ?STO:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?STO:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?sto:Purchase_Cost{prop:ReadOnly} = True
        ?sto:Purchase_Cost{prop:FontColor} = 65793
        ?sto:Purchase_Cost{prop:Color} = 15066597
    Elsif ?sto:Purchase_Cost{prop:Req} = True
        ?sto:Purchase_Cost{prop:FontColor} = 65793
        ?sto:Purchase_Cost{prop:Color} = 8454143
    Else ! If ?sto:Purchase_Cost{prop:Req} = True
        ?sto:Purchase_Cost{prop:FontColor} = 65793
        ?sto:Purchase_Cost{prop:Color} = 16777215
    End ! If ?sto:Purchase_Cost{prop:Req} = True
    ?sto:Purchase_Cost{prop:Trn} = 0
    ?sto:Purchase_Cost{prop:FontStyle} = font:Bold
    ?STO:Percentage_Mark_Up:Prompt{prop:FontColor} = -1
    ?STO:Percentage_Mark_Up:Prompt{prop:Color} = 15066597
    If ?sto:Percentage_Mark_Up{prop:ReadOnly} = True
        ?sto:Percentage_Mark_Up{prop:FontColor} = 65793
        ?sto:Percentage_Mark_Up{prop:Color} = 15066597
    Elsif ?sto:Percentage_Mark_Up{prop:Req} = True
        ?sto:Percentage_Mark_Up{prop:FontColor} = 65793
        ?sto:Percentage_Mark_Up{prop:Color} = 8454143
    Else ! If ?sto:Percentage_Mark_Up{prop:Req} = True
        ?sto:Percentage_Mark_Up{prop:FontColor} = 65793
        ?sto:Percentage_Mark_Up{prop:Color} = 16777215
    End ! If ?sto:Percentage_Mark_Up{prop:Req} = True
    ?sto:Percentage_Mark_Up{prop:Trn} = 0
    ?sto:Percentage_Mark_Up{prop:FontStyle} = font:Bold
    ?Prompt18{prop:FontColor} = -1
    ?Prompt18{prop:Color} = 15066597
    ?STO:Sale_Cost:Prompt{prop:FontColor} = -1
    ?STO:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?sto:Sale_Cost{prop:ReadOnly} = True
        ?sto:Sale_Cost{prop:FontColor} = 65793
        ?sto:Sale_Cost{prop:Color} = 15066597
    Elsif ?sto:Sale_Cost{prop:Req} = True
        ?sto:Sale_Cost{prop:FontColor} = 65793
        ?sto:Sale_Cost{prop:Color} = 8454143
    Else ! If ?sto:Sale_Cost{prop:Req} = True
        ?sto:Sale_Cost{prop:FontColor} = 65793
        ?sto:Sale_Cost{prop:Color} = 16777215
    End ! If ?sto:Sale_Cost{prop:Req} = True
    ?sto:Sale_Cost{prop:Trn} = 0
    ?sto:Sale_Cost{prop:FontStyle} = font:Bold
    ?sto:Accessory{prop:Font,3} = -1
    ?sto:Accessory{prop:Color} = 15066597
    ?sto:Accessory{prop:Trn} = 0
    If ?sto:AccessoryCost{prop:ReadOnly} = True
        ?sto:AccessoryCost{prop:FontColor} = 65793
        ?sto:AccessoryCost{prop:Color} = 15066597
    Elsif ?sto:AccessoryCost{prop:Req} = True
        ?sto:AccessoryCost{prop:FontColor} = 65793
        ?sto:AccessoryCost{prop:Color} = 8454143
    Else ! If ?sto:AccessoryCost{prop:Req} = True
        ?sto:AccessoryCost{prop:FontColor} = 65793
        ?sto:AccessoryCost{prop:Color} = 16777215
    End ! If ?sto:AccessoryCost{prop:Req} = True
    ?sto:AccessoryCost{prop:Trn} = 0
    ?sto:AccessoryCost{prop:FontStyle} = font:Bold
    ?STO:Retail_Cost:Prompt{prop:FontColor} = -1
    ?STO:Retail_Cost:Prompt{prop:Color} = 15066597
    If ?sto:Retail_Cost{prop:ReadOnly} = True
        ?sto:Retail_Cost{prop:FontColor} = 65793
        ?sto:Retail_Cost{prop:Color} = 15066597
    Elsif ?sto:Retail_Cost{prop:Req} = True
        ?sto:Retail_Cost{prop:FontColor} = 65793
        ?sto:Retail_Cost{prop:Color} = 8454143
    Else ! If ?sto:Retail_Cost{prop:Req} = True
        ?sto:Retail_Cost{prop:FontColor} = 65793
        ?sto:Retail_Cost{prop:Color} = 16777215
    End ! If ?sto:Retail_Cost{prop:Req} = True
    ?sto:Retail_Cost{prop:Trn} = 0
    ?sto:Retail_Cost{prop:FontStyle} = font:Bold
    ?sto:AccessoryCost:Prompt{prop:FontColor} = -1
    ?sto:AccessoryCost:Prompt{prop:Color} = 15066597
    ?Sheet1{prop:Color} = 15066597
    ?Model_Number_Tab{prop:Color} = 15066597
    ?Prompt26{prop:FontColor} = -1
    ?Prompt26{prop:Color} = 15066597
    ?Prompt27{prop:FontColor} = -1
    ?Prompt27{prop:Color} = 15066597
    If ?sto:Manufacturer{prop:ReadOnly} = True
        ?sto:Manufacturer{prop:FontColor} = 65793
        ?sto:Manufacturer{prop:Color} = 15066597
    Elsif ?sto:Manufacturer{prop:Req} = True
        ?sto:Manufacturer{prop:FontColor} = 65793
        ?sto:Manufacturer{prop:Color} = 8454143
    Else ! If ?sto:Manufacturer{prop:Req} = True
        ?sto:Manufacturer{prop:FontColor} = 65793
        ?sto:Manufacturer{prop:Color} = 16777215
    End ! If ?sto:Manufacturer{prop:Req} = True
    ?sto:Manufacturer{prop:Trn} = 0
    ?sto:Manufacturer{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Fault_Codes_Group{prop:Font,3} = -1
    ?Fault_Codes_Group{prop:Color} = 15066597
    ?Fault_Codes_Group{prop:Trn} = 0
    ?sto:Assign_Fault_Codes{prop:Font,3} = -1
    ?sto:Assign_Fault_Codes{prop:Color} = 15066597
    ?sto:Assign_Fault_Codes{prop:Trn} = 0
    ?Serial_Number_Tab{prop:Color} = 15066597
    If ?ste:Serial_Number{prop:ReadOnly} = True
        ?ste:Serial_Number{prop:FontColor} = 65793
        ?ste:Serial_Number{prop:Color} = 15066597
    Elsif ?ste:Serial_Number{prop:Req} = True
        ?ste:Serial_Number{prop:FontColor} = 65793
        ?ste:Serial_Number{prop:Color} = 8454143
    Else ! If ?ste:Serial_Number{prop:Req} = True
        ?ste:Serial_Number{prop:FontColor} = 65793
        ?ste:Serial_Number{prop:Color} = 16777215
    End ! If ?ste:Serial_Number{prop:Req} = True
    ?ste:Serial_Number{prop:Trn} = 0
    ?ste:Serial_Number{prop:FontStyle} = font:Bold
    ?Sheet5{prop:Color} = 15066597
    ?Tab7{prop:Color} = 15066597
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?days_7_temp{prop:FontColor} = -1
    ?days_7_temp{prop:Color} = 15066597
    ?Prompt5:2{prop:FontColor} = -1
    ?Prompt5:2{prop:Color} = 15066597
    ?days_30_temp{prop:FontColor} = -1
    ?days_30_temp{prop:Color} = 15066597
    ?Prompt5:3{prop:FontColor} = -1
    ?Prompt5:3{prop:Color} = 15066597
    ?days_60_temp{prop:FontColor} = -1
    ?days_60_temp{prop:Color} = 15066597
    ?Prompt5:4{prop:FontColor} = -1
    ?Prompt5:4{prop:Color} = 15066597
    ?days_90_temp{prop:FontColor} = -1
    ?days_90_temp{prop:Color} = 15066597
    ?Prompt5:5{prop:FontColor} = -1
    ?Prompt5:5{prop:Color} = 15066597
    ?Average_text_temp{prop:FontColor} = -1
    ?Average_text_temp{prop:Color} = 15066597
    ?Sheet7{prop:Color} = 15066597
    ?Tab8{prop:Color} = 15066597
    ?Prompt25{prop:FontColor} = -1
    ?Prompt25{prop:Color} = 15066597
    ?List2{prop:FontColor} = 65793
    ?List2{prop:Color}= 16777215
    ?List2{prop:Color,2} = 16777215
    ?List2{prop:Color,3} = 12937777
    ?Sheet6{prop:Color} = 15066597
    ?Tab6{prop:Color} = 15066597
    ?PartTypeDetailsGroup{prop:Font,3} = -1
    ?PartTypeDetailsGroup{prop:Color} = 15066597
    ?PartTypeDetailsGroup{prop:Trn} = 0
    ?sto:Sundry_Item{prop:Font,3} = -1
    ?sto:Sundry_Item{prop:Color} = 15066597
    ?sto:Sundry_Item{prop:Trn} = 0
    ?sto:ExchangeUnit{prop:Font,3} = -1
    ?sto:ExchangeUnit{prop:Color} = 15066597
    ?sto:ExchangeUnit{prop:Trn} = 0
    ?sto:Suspend{prop:Font,3} = -1
    ?sto:Suspend{prop:Color} = 15066597
    ?sto:Suspend{prop:Trn} = 0
    ?AccessLevels{prop:Font,3} = -1
    ?AccessLevels{prop:Color} = 15066597
    ?AccessLevels{prop:Trn} = 0
    ?sto:E1{prop:Font,3} = -1
    ?sto:E1{prop:Color} = 15066597
    ?sto:E1{prop:Trn} = 0
    ?sto:E2{prop:Font,3} = -1
    ?sto:E2{prop:Color} = 15066597
    ?sto:E2{prop:Trn} = 0
    ?sto:E3{prop:Font,3} = -1
    ?sto:E3{prop:Color} = 15066597
    ?sto:E3{prop:Trn} = 0
    ?sto:ExcludeFromEDI{prop:Font,3} = -1
    ?sto:ExcludeFromEDI{prop:Color} = 15066597
    ?sto:ExcludeFromEDI{prop:Trn} = 0
    ?sto:AllowDuplicate{prop:Font,3} = -1
    ?sto:AllowDuplicate{prop:Color} = 15066597
    ?sto:AllowDuplicate{prop:Trn} = 0
    ?sto:RF_Board{prop:Font,3} = -1
    ?sto:RF_Board{prop:Color} = 15066597
    ?sto:RF_Board{prop:Trn} = 0
    ?sto:Ref_Number{prop:FontColor} = -1
    ?sto:Ref_Number{prop:Color} = 15066597
    ?Stock_Levels_Group{prop:Font,3} = -1
    ?Stock_Levels_Group{prop:Color} = 15066597
    ?Stock_Levels_Group{prop:Trn} = 0
    ?STO:Minimum_Level:Prompt{prop:FontColor} = -1
    ?STO:Minimum_Level:Prompt{prop:Color} = 15066597
    If ?sto:Minimum_Level{prop:ReadOnly} = True
        ?sto:Minimum_Level{prop:FontColor} = 65793
        ?sto:Minimum_Level{prop:Color} = 15066597
    Elsif ?sto:Minimum_Level{prop:Req} = True
        ?sto:Minimum_Level{prop:FontColor} = 65793
        ?sto:Minimum_Level{prop:Color} = 8454143
    Else ! If ?sto:Minimum_Level{prop:Req} = True
        ?sto:Minimum_Level{prop:FontColor} = 65793
        ?sto:Minimum_Level{prop:Color} = 16777215
    End ! If ?sto:Minimum_Level{prop:Req} = True
    ?sto:Minimum_Level{prop:Trn} = 0
    ?sto:Minimum_Level{prop:FontStyle} = font:Bold
    ?STO:Reorder_Level:Prompt{prop:FontColor} = -1
    ?STO:Reorder_Level:Prompt{prop:Color} = 15066597
    If ?sto:Reorder_Level{prop:ReadOnly} = True
        ?sto:Reorder_Level{prop:FontColor} = 65793
        ?sto:Reorder_Level{prop:Color} = 15066597
    Elsif ?sto:Reorder_Level{prop:Req} = True
        ?sto:Reorder_Level{prop:FontColor} = 65793
        ?sto:Reorder_Level{prop:Color} = 8454143
    Else ! If ?sto:Reorder_Level{prop:Req} = True
        ?sto:Reorder_Level{prop:FontColor} = 65793
        ?sto:Reorder_Level{prop:Color} = 16777215
    End ! If ?sto:Reorder_Level{prop:Req} = True
    ?sto:Reorder_Level{prop:Trn} = 0
    ?sto:Reorder_Level{prop:FontStyle} = font:Bold
    ?Sheet4{prop:Color} = 15066597
    ?Tab5{prop:Color} = 15066597
    ?String4{prop:FontColor} = -1
    ?String4{prop:Color} = 15066597
    ?Stock_Prompt{prop:FontColor} = -1
    ?Stock_Prompt{prop:Color} = 15066597
    ?String8{prop:FontColor} = -1
    ?String8{prop:Color} = 15066597
    ?To_Order_Prompt{prop:FontColor} = -1
    ?To_Order_Prompt{prop:Color} = 15066597
    ?String10{prop:FontColor} = -1
    ?String10{prop:Color} = 15066597
    ?On_Order_Prompt{prop:FontColor} = -1
    ?On_Order_Prompt{prop:Color} = 15066597
    ?SummaryOrdersPrompt{prop:FontColor} = -1
    ?SummaryOrdersPrompt{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597

    ?Replicate_Temp{prop:Font,3} = -1
    ?Replicate_Temp{prop:Color} = 15066597
    ?Replicate_Temp{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine   !Put Into Calculation Loop
    YIELD()
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
        display()
      end
    end

Fault_Coding        Routine
    count# = 0
    setcursor(cursor:wait)

    If sto:manufacturer <> ''
        save_map_id = access:manfaupa.savefile()
        access:manfaupa.clearkey(map:field_number_key)
        map:manufacturer = sto:manufacturer
        set(map:field_number_key,map:field_number_key)
        loop
            if access:manfaupa.next()
               break
            end !if
            if map:manufacturer <> sto:manufacturer      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            count# = 1
            Break
        end !loop
        access:manfaupa.restorefile(save_map_id)
        setcursor()
    End!If sto:manufacturer <> ''
    If count# = 1
        Unhide(?fault_Codes_group)
    Else
        Hide(?fault_codes_group)
        sto:assign_fault_codes = 'NO'
    End

    If sto:assign_fault_codes = 'YES'
        Unhide(?fault_codes_button)
    Else
        Hide(?fault_codes_button)
    End
Update_StoModel     Routine
    setcursor(cursor:wait)
    
    save_stm_id = access:stomodel.savefile()
    access:stomodel.clearkey(stm:ref_part_description)
    stm:location    = sto:location
    stm:ref_number  = sto:ref_number
    stm:part_number = sto:part_number
    stm:description = sto:description
    set(stm:ref_part_description,stm:ref_part_description)
    loop
        if access:stomodel.next()
           break
        end !if
        if stm:location    <> sto:location      |
        or stm:ref_number  <> sto:ref_number      |
        or stm:part_number <> sto:part_number      |
        or stm:description <> sto:description      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if
        stm:accessory   = sto:accessory
        access:stomodel.update()
    end !loop
    access:stomodel.restorefile(save_stm_id)
    setcursor()
Hide_Fields     Routine
    If sto:percentage_mark_up <> ''
        ?sto:sale_cost{prop:readonly} = 1
        ?sto:sale_cost{prop:skip} = 1
        ?sto:sale_cost{prop:color} = color:silver
    Else
        ?sto:sale_cost{prop:readonly} = 0
        ?sto:sale_cost{prop:skip} = 0
        ?sto:sale_cost{prop:color} = color:white
    End
    If sto:sundry_item = 'YES'
        Disable(?stock_levels_group)
    Else!If sto:sundry_item = 'YES'
        Enable(?stock_levels_group)
    End!If sto:sundry_item = 'YES'

    Display()
Show_Details            Routine
    ?stock_prompt{prop:text}       = sto:quantity_stock
    ?to_order_prompt{prop:text}    = sto:quantity_to_order
    ! Start Change 3148 BE(26/09/03)
    !?on_order_prompt{prop:text}    = sto:quantity_on_order
    SET(DEFAULTS)
    access:DEFAULTS.next()
    IF (def:SummaryOrders = 0) THEN
        ?on_order_prompt{prop:text}    = sto:quantity_on_order
        HIDE(?SummaryOrdersPrompt)
    ELSE
        tmpqty# = sto:quantity_on_order
        access:ordparts.clearkey(orp:ref_number_key)
        orp:part_ref_number = 0
        orp:part_number = sto:part_number
        SET(orp:ref_number_key,orp:ref_number_key)
        LOOP
            IF ((access:ordparts.next() <> Level:Benign) OR |
                (orp:part_ref_number <> 0) OR |
                (orp:part_number <> sto:part_number)) THEN
                BREAK
            END
            IF ((orp:Order_Number <> '') AND (orp:all_received = 'NO')) THEN
                access:orders.clearkey(ord:order_number_key)
                ord:order_number = orp:order_number
                IF ((access:orders.fetch(ord:order_number_key) = Level:Benign) AND (ord:all_received = 'NO'))
                    tmpqty# += orp:quantity
                END
            END
        END
        ?on_order_prompt{prop:text} = tmpqty#
        UNHIDE(?SummaryOrdersPrompt)
    END
    ! End Change 3148 BE(26/09/03)
    Display()
Lookup_Shelf_Location       Routine
        glo:select2  = sto:location

        saverequest#      = globalrequest
        globalresponse    = requestcancelled
        globalrequest     = selectrecord
        browse_shelf_location
        if globalresponse = requestcompleted
            sto:shelf_location = los:shelf_location
            display()
        end
        globalrequest     = saverequest#
Fill_List   Routine
ReplicateToPendingParts   ROUTINE    ! Start Change 4795 BE(11/10/2004)
    access:ordpend.clearkey(ope:Part_Ref_Number_Key)
    ope:Part_Ref_Number = sto:ref_number
    SET(ope:Part_Ref_Number_Key,ope:Part_Ref_Number_Key)
    LOOP
        IF ((access:ordpend.next() <> Level:Benign) OR |
            (ope:Part_Ref_Number <> sto:ref_number)) THEN
           BREAK
        END
        ope:Supplier = sav2:Supplier
        ope:Part_Number = sav2:Part_Number
        ope:Description = sav2:Description
        CASE ope:Part_Type
            OF 'JOB'
                access:parts.clearkey(par:PendingRefNoKey)
                par:Ref_Number = ope:Job_Number
                par:Pending_Ref_Number = ope:Ref_Number
                IF (access:parts.fetch(par:PendingRefNoKey) = Level:Benign) THEN
                    IF (par:Part_Ref_Number = ope:Part_Ref_Number) THEN
                        par:Part_Number   = sav2:Part_Number
                        par:Description   = sav2:Description
                        par:Supplier      = sav2:Supplier
                        par:Purchase_Cost = sav2:Purchase_Cost
                        par:Sale_Cost     = sav2:Sale_Cost
                        par:Retail_Cost   = sav2:Retail_Cost
                        access:parts.update()
                    END
                END
            OF 'WAR'
                access:warparts.clearkey(wpr:PendingRefNoKey)
                wpr:Ref_Number = ope:Job_Number
                wpr:Pending_Ref_Number = ope:Ref_Number
                IF (access:warparts.fetch(wpr:PendingRefNoKey) = Level:Benign) THEN
                    IF (wpr:Part_Ref_Number = ope:Part_Ref_Number) THEN
                        wpr:Part_Number   = sav2:Part_Number
                        wpr:Description   = sav2:Description
                        wpr:Supplier      = sav2:Supplier
                        wpr:Purchase_Cost = sav2:Purchase_Cost
                        wpr:Sale_Cost     = sav2:Sale_Cost
                        wpr:Retail_Cost   = sav2:Retail_Cost
                        access:warparts.update()
                    END
                END
        END
        access:ordpend.update()
    END
    ! End Change 4795 BE(11/10/2004)
ReplicateToOrderedParts   ROUTINE    ! Start Change 4795 BE(11/10/2004)
    access:ordparts.clearkey(orp:Ref_Number_Key)
    orp:Part_Ref_Number = sto:ref_number
    SET(orp:Ref_Number_Key,orp:Ref_Number_Key)
    LOOP
        IF ((access:ordparts.next() <> Level:Benign) OR |
            (orp:Part_Ref_Number <> sto:ref_number)) THEN
           BREAK
        END
        IF (orp:All_Received <> 'YES') THEN
            orp:Part_Number   = sav2:Part_Number
            orp:Description   = sav2:Description
            orp:Purchase_Cost = sav2:Purchase_Cost
            orp:Sale_Cost     = sav2:Sale_Cost
            orp:Retail_Cost   = sav2:Retail_Cost

            CASE orp:Part_Type
                OF 'JOB'
                    access:parts.clearkey(par:Order_Number_Key)
                    par:Ref_Number = orp:Job_Number
                    par:Order_Number = orp:Order_Number
                    IF (access:parts.fetch(par:Order_Number_Key) = Level:Benign) THEN
                        IF (par:Part_Ref_Number = orp:Part_Ref_Number) THEN
                            par:Part_Number   = sav2:Part_Number
                            par:Description   = sav2:Description
                            par:Supplier      = sav2:Supplier
                            par:Purchase_Cost = sav2:Purchase_Cost
                            par:Sale_Cost     = sav2:Sale_Cost
                            par:Retail_Cost   = sav2:Retail_Cost
                            access:parts.update()
                        END
                    END
                OF 'WAR'
                    access:warparts.clearkey(wpr:Order_Number_Key)
                    wpr:Ref_Number = orp:Job_Number
                    wpr:Order_Number = orp:Order_Number
                    IF (access:warparts.fetch(wpr:Order_Number_Key) = Level:Benign) THEN
                        IF (wpr:Part_Ref_Number = orp:Part_Ref_Number) THEN
                            wpr:Part_Number   = sav2:Part_Number
                            wpr:Description   = sav2:Description
                            wpr:Supplier      = sav2:Supplier
                            wpr:Purchase_Cost = sav2:Purchase_Cost
                            wpr:Sale_Cost     = sav2:Sale_Cost
                            wpr:Retail_Cost   = sav2:Retail_Cost
                            access:warparts.update()
                        END
                    END
            END
            access:ordparts.update()
        END
    END
    ! End Change 4795 BE(11/10/2004)


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateSTOCK',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateSTOCK',1)
    SolaceViewVars('save_ccp_id',save_ccp_id,'UpdateSTOCK',1)
    SolaceViewVars('save_cwp_id',save_cwp_id,'UpdateSTOCK',1)
    SolaceViewVars('save_sto_ali_id',save_sto_ali_id,'UpdateSTOCK',1)
    SolaceViewVars('average_temp',average_temp,'UpdateSTOCK',1)
    SolaceViewVars('save_shi_id',save_shi_id,'UpdateSTOCK',1)
    SolaceViewVars('days_7_temp',days_7_temp,'UpdateSTOCK',1)
    SolaceViewVars('days_30_temp',days_30_temp,'UpdateSTOCK',1)
    SolaceViewVars('days_60_temp',days_60_temp,'UpdateSTOCK',1)
    SolaceViewVars('days_90_temp',days_90_temp,'UpdateSTOCK',1)
    SolaceViewVars('Average_text_temp',Average_text_temp,'UpdateSTOCK',1)
    SolaceViewVars('save_loc_id',save_loc_id,'UpdateSTOCK',1)
    SolaceViewVars('save_sto_id',save_sto_id,'UpdateSTOCK',1)
    SolaceViewVars('pos',pos,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Part_Number',sav2:group:Part_Number,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Description',sav2:group:Description,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Supplier',sav2:group:Supplier,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Purchase_Cost',sav2:group:Purchase_Cost,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Sale_Cost',sav2:group:Sale_Cost,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Retail_Cost',sav2:group:Retail_Cost,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Percentage_Mark_Up',sav2:group:Percentage_Mark_Up,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Shelf_Location',sav2:group:Shelf_Location,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Manufacturer',sav2:group:Manufacturer,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Second_Location',sav2:group:Second_Location,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Minimum_Level',sav2:group:Minimum_Level,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Reorder_Level',sav2:group:Reorder_Level,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Accessory',sav2:group:Accessory,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:ExchangeUnit',sav2:group:ExchangeUnit,'UpdateSTOCK',1)
    SolaceViewVars('sav2:group:Suspend',sav2:group:Suspend,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Part_Number',sav:group:Part_Number,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Description',sav:group:Description,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Supplier',sav:group:Supplier,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Purchase_Cost',sav:group:Purchase_Cost,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Sale_Cost',sav:group:Sale_Cost,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Retail_Cost',sav:group:Retail_Cost,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Percentage_Mark_Up',sav:group:Percentage_Mark_Up,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Shelf_Location',sav:group:Shelf_Location,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Manufacturer',sav:group:Manufacturer,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Second_Location',sav:group:Second_Location,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Minimum_Level',sav:group:Minimum_Level,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Reorder_Level',sav:group:Reorder_Level,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Accessory',sav:group:Accessory,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:ExchangeUnit',sav:group:ExchangeUnit,'UpdateSTOCK',1)
    SolaceViewVars('sav:group:Suspend',sav:group:Suspend,'UpdateSTOCK',1)
    SolaceViewVars('ref_number_temp',ref_number_temp,'UpdateSTOCK',1)
    SolaceViewVars('Part_Number_temp',Part_Number_temp,'UpdateSTOCK',1)
    SolaceViewVars('save_ope_id',save_ope_id,'UpdateSTOCK',1)
    SolaceViewVars('Description_temp',Description_temp,'UpdateSTOCK',1)
    SolaceViewVars('Manufacturer_temp',Manufacturer_temp,'UpdateSTOCK',1)
    SolaceViewVars('Accessory_temp',Accessory_temp,'UpdateSTOCK',1)
    SolaceViewVars('save_orp_id',save_orp_id,'UpdateSTOCK',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateSTOCK',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateSTOCK',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateSTOCK',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateSTOCK',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'UpdateSTOCK',1)
    SolaceViewVars('FP::PopupString',FP::PopupString,'UpdateSTOCK',1)
    SolaceViewVars('FP::ReturnValues:RVPtr',FP::ReturnValues:RVPtr,'UpdateSTOCK',1)
    SolaceViewVars('Replicate_Temp',Replicate_Temp,'UpdateSTOCK',1)
    SolaceViewVars('OtherLocationQueue:Location',OtherLocationQueue:Location,'UpdateSTOCK',1)
    SolaceViewVars('OtherLocationQueue:QuantityInStock',OtherLocationQueue:QuantityInStock,'UpdateSTOCK',1)
    SolaceViewVars('OtherLocationQueue:Ref_Number',OtherLocationQueue:Ref_Number,'UpdateSTOCK',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartDetailsGroup;  SolaceCtrlName = '?PartDetailsGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Part_Number:Prompt;  SolaceCtrlName = '?STO:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Description:Prompt;  SolaceCtrlName = '?STO:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LocationDetailsGroup;  SolaceCtrlName = '?LocationDetailsGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Location:Prompt;  SolaceCtrlName = '?sto:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Location;  SolaceCtrlName = '?sto:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Shelf_Location:Prompt;  SolaceCtrlName = '?STO:Shelf_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location;  SolaceCtrlName = '?sto:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Shelf_Location;  SolaceCtrlName = '?Lookup_Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Second_Location:Prompt;  SolaceCtrlName = '?STO:Second_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Second_Location;  SolaceCtrlName = '?sto:Second_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Supplier:Prompt;  SolaceCtrlName = '?STO:Supplier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Supplier;  SolaceCtrlName = '?sto:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup_Supplier;  SolaceCtrlName = '?Lookup_Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CostDetailsGroup;  SolaceCtrlName = '?CostDetailsGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Purchase_Cost:Prompt;  SolaceCtrlName = '?STO:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Purchase_Cost;  SolaceCtrlName = '?sto:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Percentage_Mark_Up:Prompt;  SolaceCtrlName = '?STO:Percentage_Mark_Up:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Percentage_Mark_Up;  SolaceCtrlName = '?sto:Percentage_Mark_Up';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt18;  SolaceCtrlName = '?Prompt18';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Sale_Cost:Prompt;  SolaceCtrlName = '?STO:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Sale_Cost;  SolaceCtrlName = '?sto:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Accessory;  SolaceCtrlName = '?sto:Accessory';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:AccessoryCost;  SolaceCtrlName = '?sto:AccessoryCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Retail_Cost:Prompt;  SolaceCtrlName = '?STO:Retail_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Retail_Cost;  SolaceCtrlName = '?sto:Retail_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:AccessoryCost:Prompt;  SolaceCtrlName = '?sto:AccessoryCost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Model_Number_Tab;  SolaceCtrlName = '?Model_Number_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt26;  SolaceCtrlName = '?Prompt26';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt27;  SolaceCtrlName = '?Prompt27';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Manufacturer;  SolaceCtrlName = '?sto:Manufacturer';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Codes_Group;  SolaceCtrlName = '?Fault_Codes_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Fault_Codes_Button;  SolaceCtrlName = '?Fault_Codes_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Assign_Fault_Codes;  SolaceCtrlName = '?sto:Assign_Fault_Codes';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Serial_Number_Tab;  SolaceCtrlName = '?Serial_Number_Tab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ste:Serial_Number;  SolaceCtrlName = '?ste:Serial_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet5;  SolaceCtrlName = '?Sheet5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab7;  SolaceCtrlName = '?Tab7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_7_temp;  SolaceCtrlName = '?days_7_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:2;  SolaceCtrlName = '?Prompt5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_30_temp;  SolaceCtrlName = '?days_30_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:3;  SolaceCtrlName = '?Prompt5:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_60_temp;  SolaceCtrlName = '?days_60_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:4;  SolaceCtrlName = '?Prompt5:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_90_temp;  SolaceCtrlName = '?days_90_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5:5;  SolaceCtrlName = '?Prompt5:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Average_text_temp;  SolaceCtrlName = '?Average_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet7;  SolaceCtrlName = '?Sheet7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab8;  SolaceCtrlName = '?Tab8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt25;  SolaceCtrlName = '?Prompt25';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List2;  SolaceCtrlName = '?List2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet6;  SolaceCtrlName = '?Sheet6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartTypeDetailsGroup;  SolaceCtrlName = '?PartTypeDetailsGroup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Sundry_Item;  SolaceCtrlName = '?sto:Sundry_Item';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:ExchangeUnit;  SolaceCtrlName = '?sto:ExchangeUnit';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Suspend;  SolaceCtrlName = '?sto:Suspend';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AccessLevels;  SolaceCtrlName = '?AccessLevels';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:E1;  SolaceCtrlName = '?sto:E1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:E2;  SolaceCtrlName = '?sto:E2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:E3;  SolaceCtrlName = '?sto:E3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:ExcludeFromEDI;  SolaceCtrlName = '?sto:ExcludeFromEDI';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:AllowDuplicate;  SolaceCtrlName = '?sto:AllowDuplicate';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:RF_Board;  SolaceCtrlName = '?sto:RF_Board';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Ref_Number;  SolaceCtrlName = '?sto:Ref_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Levels_Group;  SolaceCtrlName = '?Stock_Levels_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Minimum_Level:Prompt;  SolaceCtrlName = '?STO:Minimum_Level:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Minimum_Level;  SolaceCtrlName = '?sto:Minimum_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Reorder_Level:Prompt;  SolaceCtrlName = '?STO:Reorder_Level:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Reorder_Level;  SolaceCtrlName = '?sto:Reorder_Level';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet4;  SolaceCtrlName = '?Sheet4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String4;  SolaceCtrlName = '?String4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Prompt;  SolaceCtrlName = '?Stock_Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Clear_Stock;  SolaceCtrlName = '?Clear_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String8;  SolaceCtrlName = '?String8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?To_Order_Prompt;  SolaceCtrlName = '?To_Order_Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String10;  SolaceCtrlName = '?String10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?On_Order_Prompt;  SolaceCtrlName = '?On_Order_Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SummaryOrdersPrompt;  SolaceCtrlName = '?SummaryOrdersPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Replicate_Temp;  SolaceCtrlName = '?Replicate_Temp';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Stock Item'
  OF ChangeRecord
    ActionMessage = 'Changing A Stock Item'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateSTOCK')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateSTOCK')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STO:Part_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(sto:Record,History::sto:Record)
  SELF.AddHistoryField(?sto:Part_Number,3)
  SELF.AddHistoryField(?sto:Description,4)
  SELF.AddHistoryField(?sto:Location,13)
  SELF.AddHistoryField(?sto:Shelf_Location,11)
  SELF.AddHistoryField(?sto:Second_Location,14)
  SELF.AddHistoryField(?sto:Supplier,5)
  SELF.AddHistoryField(?sto:Purchase_Cost,6)
  SELF.AddHistoryField(?sto:Percentage_Mark_Up,10)
  SELF.AddHistoryField(?sto:Sale_Cost,7)
  SELF.AddHistoryField(?sto:Accessory,26)
  SELF.AddHistoryField(?sto:AccessoryCost,9)
  SELF.AddHistoryField(?sto:Retail_Cost,8)
  SELF.AddHistoryField(?sto:Manufacturer,12)
  SELF.AddHistoryField(?sto:Assign_Fault_Codes,28)
  SELF.AddHistoryField(?sto:Sundry_Item,1)
  SELF.AddHistoryField(?sto:ExchangeUnit,30)
  SELF.AddHistoryField(?sto:Suspend,32)
  SELF.AddHistoryField(?sto:E1,45)
  SELF.AddHistoryField(?sto:E2,46)
  SELF.AddHistoryField(?sto:E3,47)
  SELF.AddHistoryField(?sto:ExcludeFromEDI,49)
  SELF.AddHistoryField(?sto:AllowDuplicate,48)
  SELF.AddHistoryField(?sto:RF_Board,50)
  SELF.AddHistoryField(?sto:Ref_Number,2)
  SELF.AddHistoryField(?sto:Minimum_Level,18)
  SELF.AddHistoryField(?sto:Reorder_Level,19)
  SELF.AddUpdateFile(Access:STOCK)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:COMMONCP.Open
  Relate:DEFAULTS.Open
  Relate:LOCSHELF.Open
  Relate:STOCK_ALIAS.Open
  Relate:STOMODEL_ALIAS.Open
  Relate:USERS_ALIAS.Open
  Access:STOHIST.UseFile
  Access:STOMODEL.UseFile
  Access:MANFAUPA.UseFile
  Access:ORDERS.UseFile
  Access:ORDPEND.UseFile
  Access:ORDPARTS.UseFile
  Access:SUPPLIER.UseFile
  Access:COMMONWP.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.CancelAction = Cancel:Cancel+Cancel:Query
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW8.Init(?List,Queue:Browse.ViewPosition,BRW8::View:Browse,Queue:Browse,Relate:STOMODEL,SELF)
    ref_number# = sto:ref_number
    Include('stockuse.inc')
    !Evaluate Values
    If thiswindow.request <> Insertrecord
        Set(DEFAULTS)
        Access:DEFAULTS.Next()

        STO:Quantity_To_Order = 0                                                               !Go throught the pending order
        Case def:SummaryOrders
            Of 0 !Old Way
                setcursor(cursor:wait)                                                                  !file. And work out the quantity
                save_ope_id = access:ordpend.savefile()                                                 !awaiting order
                access:ordpend.clearkey(ope:part_ref_number_key)
                ope:part_ref_number =  sto:ref_number
                set(ope:part_ref_number_key,ope:part_ref_number_key)
                loop
                    if access:ordpend.next()
                       break
                    end !if
                    if ope:part_ref_number <> sto:ref_number      |
                        then break.  ! end if
                    yldcnt# += 1
                    if yldcnt# > 25
                       yield() ; yldcnt# = 0
                    end !if
                    sto:quantity_to_order += ope:quantity
                end !loop
                access:ordpend.restorefile(save_ope_id)
                setcursor()

            Of 1 !New Way
                sto:Quantity_To_Order   = sto:QuantityRequested
        End !Case def:SummaryOrders

        sto:quantity_on_order = 0                                                               !Go throught the order parts
        setcursor(cursor:wait)                                                                  !file. But check with the order
        save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
        access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
        orp:part_ref_number = sto:ref_number                                                    !should hopefully speed things
        set(orp:ref_number_key,orp:ref_number_key)                                              !up.
        loop
            if access:ordparts.next()
               break
            end !if
            if orp:part_ref_number <> sto:ref_number      |
                then break.  ! end if
            If orp:order_number <> ''
                access:orders.clearkey(ord:order_number_key)
                ord:order_number = orp:order_number
                if access:orders.fetch(ord:order_number_key) = Level:Benign
                    If ord:all_received = 'NO'
                        If orp:all_received = 'NO'
                            sto:quantity_on_order += orp:quantity
                        End!If orp:all_received <> 'YES'
                    End!If ord:all_received <> 'YES'
                end!if access:orders.fetch(ord:order_number_key) = Level:Benign
            End!If orp:order_number <> ''
        end !loop
        access:ordparts.restorefile(save_orp_id)
        setcursor()

        IF sto:quantity_to_order < 0
            sto:quantity_to_order = 0
        End!IF sto:quantity_to_order < 0
        If sto:quantity_on_order < 0
            sto:quantity_on_order = 0
        End!If sto:quantity_on_order < 0
    End!If thiswindow.request <> Insertrecord

    Clear(OtherLocationQueue)
    Free(OtherLocationQueue)
    !Find out how much stock there is in the other locations
    Save_loc_ID = Access:LOCATION.SaveFile()
    Access:LOCATION.ClearKey(loc:ActiveLocationKey)
    loc:Active   = 1
    Set(loc:ActiveLocationKey,loc:ActiveLocationKey)
    Loop
        If Access:LOCATION.NEXT()
           Break
        End !If
        If loc:Active   <> 1      |
            Then Break.  ! End If
        If loc:Location = sto:Location
            Cycle
        End !If loc:Location = sto:Location

        Access:STOCK_ALIAS.ClearKey(sto_ali:Location_Part_Description_Key)
        sto_ali:Location    = loc:Location
        sto_ali:Part_Number = sto:Part_Number
        sto_ali:Description = sto:Description
        If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
            !Found
            othque:Location = loc:Location
            othque:QuantityInStock = sto_ali:Quantity_Stock

            ! Start Change 3317 BE(12/11/03)
            othque:Ref_Number = sto_ali:ref_number
            ! End Change 3317 BE(12/11/03)

            Add(OtherLocationQueue)
        Else!If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End!If Access:STOCK_ALIAS.TryFetch(sto_ali:Location_Part_Description_Key) = Level:Benign
    End !Loop
    Access:LOCATION.RestoreFile(Save_loc_ID)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do Show_Details
  Do Hide_Fields
  
  If Clip(GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
      ?AccessLevels{prop:Hide} = 0
  End !If Clip(GETINI('WEBMASTER','StockLevels',,CLIP(PATH())&'\SB2KDEF.INI')) = 1
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  ?List2{prop:vcr} = TRUE
  ! support for CPCS
  IF ?sto:Shelf_Location{Prop:Tip} AND ~?Lookup_Shelf_Location{Prop:Tip}
     ?Lookup_Shelf_Location{Prop:Tip} = 'Select ' & ?sto:Shelf_Location{Prop:Tip}
  END
  IF ?sto:Shelf_Location{Prop:Msg} AND ~?Lookup_Shelf_Location{Prop:Msg}
     ?Lookup_Shelf_Location{Prop:Msg} = 'Select ' & ?sto:Shelf_Location{Prop:Msg}
  END
  IF ?sto:Supplier{Prop:Tip} AND ~?Lookup_Supplier{Prop:Tip}
     ?Lookup_Supplier{Prop:Tip} = 'Select ' & ?sto:Supplier{Prop:Tip}
  END
  IF ?sto:Supplier{Prop:Msg} AND ~?Lookup_Supplier{Prop:Msg}
     ?Lookup_Supplier{Prop:Msg} = 'Select ' & ?sto:Supplier{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW8.Q &= Queue:Browse
  BRW8.AddSortOrder(,stm:Mode_Number_Only_Key)
  BRW8.AddRange(stm:Ref_Number,sto:Ref_Number)
  BRW8.AddLocator(BRW8::Sort0:Locator)
  BRW8::Sort0:Locator.Init(,stm:Model_Number,1,BRW8)
  BRW8.AddField(stm:Model_Number,BRW8.Q.stm:Model_Number)
  BRW8.AddField(stm:Ref_Number,BRW8.Q.stm:Ref_Number)
  BRW8.AddField(stm:Manufacturer,BRW8.Q.stm:Manufacturer)
  IF ?sto:Accessory{Prop:Checked} = True
    UNHIDE(?sto:AccessoryCost:Prompt)
    UNHIDE(?sto:AccessoryCost)
  END
  IF ?sto:Accessory{Prop:Checked} = False
    HIDE(?sto:AccessoryCost:Prompt)
    HIDE(?sto:AccessoryCost)
  END
  FDCB7.Init(sto:Manufacturer,?sto:Manufacturer,Queue:FileDropCombo:2.ViewPosition,FDCB7::View:FileDropCombo,Queue:FileDropCombo:2,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB7.Q &= Queue:FileDropCombo:2
  FDCB7.AddSortOrder(man:Manufacturer_Key)
  FDCB7.AddField(man:Manufacturer,FDCB7.Q.man:Manufacturer)
  FDCB7.AddField(man:RecordNumber,FDCB7.Q.man:RecordNumber)
  FDCB7.AddUpdateField(man:Manufacturer,sto:Manufacturer)
  ThisWindow.AddItem(FDCB7.WindowComponent)
  FDCB7.DefaultFill = 0
  BRW8.AskProcedure = 3
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:COMMONCP.Close
    Relate:DEFAULTS.Close
    Relate:LOCSHELF.Close
    Relate:STOCK_ALIAS.Close
    Relate:STOMODEL_ALIAS.Close
    Relate:USERS_ALIAS.Close
  !Replicate Changes
  If thiswindow.request <> insertrecord
      If thiswindow.response = RequestCompleted
          If sav2:Part_Number <> sav:Part_Number Or |
              sav2:Description <> sav:Description Or |
              sav2:Supplier <> sav:Supplier Or |
              sav2:Purchase_Cost <> sav:Purchase_Cost Or |
              sav2:Sale_Cost <> sav:Sale_Cost Or |
              sav2:Retail_Cost <> sav:Retail_Cost Or |
              sav2:Percentage_Mark_Up <> sav:Percentage_Mark_Up Or |
              sav2:Shelf_Location <> sav:Shelf_Location Or |
              sav2:Manufacturer <> sav:Manufacturer Or |
              sav2:Second_Location <> sav:Second_Location Or |
              sav2:Minimum_Level <> sav:Minimum_Level Or |
              sav2:Reorder_Level <> sav:Reorder_Level Or |
              sav2:ExchangeUnit <> sav:ExchangeUnit Or |
              sav2:Suspend <> sav:Suspend Or |
              sav2:Accessory <> sav:Accessory
  
              Case MessageEx('The Part Details have changed. Do you wish to replicate this information to all other occurances of this part?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                      relate:stock.open()
                      relate:commoncp.open()
                      relate:commonwp.open()
                      RecordsPerCycle     = 10
                      RecordsProcessed    = 0
                      PercentProgress     = 0
                      SETCURSOR(cursor:wait)
                      Open(ProgressWindow)
                      Progress:Thermometer    = 0
                      ?Progress:PctText{prop:text} = '0% Completed'
                      ?Progress:text{prop:text}='REPLICATING'
                      RecordsToProcess  = RECORDS(location)
                      setcursor(cursor:wait)
                      save_loc_id = access:location.savefile()
                      set(loc:location_key)
                      loop
                          if access:location.next()
                             break
                          end !if
                          DO GetNextRecord2
                          save_sto_id = access:stock.savefile()
                          access:stock.clearkey(sto:location_part_description_key)
                          sto:location    = loc:location
                          sto:part_number = sav:part_number
                          sto:description = sav:description
                          set(sto:location_part_description_key,sto:location_part_description_key)
                          loop
                              if access:stock.next()
                                 break
                              end !if
                              IF sto:location <> loc:location
                                BREAK
                              END ! end if
                              IF sto:part_number <> sav:part_number
                                BREAK
                              END ! end if
                              IF sto:description <> sav:description
                                BREAK
                              END ! end if
  
                              ! Start Change 4795 BE(12/10/2004)
                              DO ReplicateToPendingParts
                              DO ReplicateToOrderedParts
                              ! End Change 4795 BE(12/10/2004)
  
                              If sto:ref_number = ref_number_temp
                                  Cycle
                              End!If sto:ref_number_temp  = ref_number_temp
  
                              save_pos# = 0
                              !No need to reset position if the key elements don't change
                              If sto:part_number <> sav2:part_number or sto:description <> sav2:description
                                  pos = Position(sto:location_part_description_key)
                                  save_pos# = 1
                              End!If sto:part_number <> sav2:part_number or sto:description <> sav2:description
  
                              access:stomodel.clearkey(stm:ref_part_description)
                              stm:ref_number  = sto:Ref_number
                              stm:part_number = sto:part_number
                              stm:location    = sto:location
                              stm:description = sto:description
                              if access:stomodel.tryfetch(stm:ref_part_description) = Level:Benign
                                  stm:part_number = sav2:part_number
                                  stm:description = sav2:description
                                  stm:accessory   = sav2:accessory
                                  access:stomodel.update()
                              end!if access:stomodel.tryfetch(stm:ref_part_description) = Level:Benign
  
                              sto:part_number=sav2:Part_Number
                              sto:description=sav2:Description
                              sto:supplier=sav2:Supplier
                              sto:purchase_cost=sav2:Purchase_Cost
                              sto:sale_cost=sav2:Sale_Cost
                              sto:retail_cost=sav2:Retail_Cost
                              sto:percentage_mark_up=sav2:Percentage_Mark_Up
                              sto:shelf_location=sav2:Shelf_Location
                              sto:manufacturer=sav2:Manufacturer
                              sto:second_location=sav2:Second_Location
                              sto:minimum_level=sav2:Minimum_Level
                              sto:reorder_level=sav2:Reorder_Level
                              sto:accessory=sav2:Accessory
                              sto:ExchangeUnit=sav2:ExchangeUnit
                              sto:Suspend=sav2:Suspend
                              access:stock.update()
  
                              access:stomodel.clearkey(stm:ref_part_description)
                              stm:ref_number  = sto:Ref_number
                              stm:part_number = sav:part_number
                              stm:location    = sto:location
                              stm:description = sav:description
                              if access:stomodel.tryfetch(stm:ref_part_description) = Level:Benign
                                  stm:part_number = sav2:part_number
                                  stm:description = sav2:description
                                  stm:accessory   = sav2:accessory
                                  access:stomodel.update()
                              end!if access:stomodel.tryfetch(stm:ref_part_description) = Level:Benign
  
                              If save_pos# = 1
                                  Reset(sto:location_part_description_key,pos)
                              End!If save_pos# = 1
  
                              access:locshelf.clearkey(los:shelf_location_key)        !See if the shelf location
                              los:site_location  = loc:location                       !exists under the new
                              los:shelf_location = sto:shelf_location                 !location.
                              if access:locshelf.tryfetch(los:shelf_location_key)
                                  ! get(locshelf,0)
                                  if access:locshelf.primerecord() = Level:Benign
                                      los:site_location  = loc:location
                                      los:shelf_location = sto:shelf_location
                                      if access:locshelf.tryinsert()
                                         access:locshelf.cancelautoinc()
                                      end
                                  End!if access:locshelf.primerecord() = Level:Benign
  
                              end
  
                              !get(stohist,0)
                              if access:stohist.primerecord() = level:benign
                                  shi:ref_number           = sto:ref_number
                                  access:users.clearkey(use:password_key)
                                  use:password              =glo:password
                                  access:users.fetch(use:password_key)
                                  shi:user                  = use:user_code    
                                  shi:date                 = today()
                                  shi:transaction_type     = 'ADD'
                                  shi:despatch_note_number = ''
                                  shi:job_number           = ''
                                  shi:quantity             = 0
                                  shi:purchase_cost        = STO:PURCHASE_COST
                                  shi:sale_cost            = STO:SALE_COST
                                  shi:retail_cost          = STO:RETAIL_COST
                                  shi:information          = 'PREVIOUS DETAILS:-' & |
                                                              '<13,10>PART NUMBER: ' & Clip(sav:Part_Number) &|
                                                              '<13,10>DESCRIPTION: ' & Clip(sav:Description) &|
                                                              '<13,10>SUPPLIER: ' & CLip(sav:Supplier) &|
                                                              '<13,10>PURCHASE COST: ' & CLip(sav:Purchase_Cost) &|
                                                              '<13,10>TRADE PRICE: ' & CLip(sav:Sale_Cost) &|
                                                              '<13,10>RETAIL PRICE: ' & CLip(sav:Retail_Cost) &|
                                                              '<13,10>SHELF LOCATION: ' & CLip(sav:Shelf_Location) &|
                                                              '<13,10>2ND LOCATION: ' & CLip(sav:Second_Location) &|
                                                              '<13,10>MANUFACTURER: ' & CLip(sav:Manufacturer) &|
                                                              '<13,10>MINIMUM LEVEL: ' & CLip(sav:Minimum_Level) &|
                                                              '<13,10>MAKE UP TO LEVEL: ' & Clip(sav:Reorder_Level)
                                  shi:notes                = 'DETAILS CHANGED'
                                  if access:stohist.insert()
                                     access:stohist.cancelautoinc()
                                  end
                              end!if access:stohist.primerecord() = level:benign
                          end !loop
                          access:stock.restorefile(save_sto_id)
  
                      end !loop
                      access:location.restorefile(save_loc_id)
  
                      ! Start Change 4795 BE(12/10/2004)
                      DO ReplicateToPendingParts
                      DO ReplicateToOrderedParts
                      ! End Change 4795 BE(12/10/2004)
  
                      setcursor()
                      CLOSE(ProgressWindow)
                      RecordsPerCycle     = 10
                      RecordsProcessed    = 0
                      PercentProgress     = 0
                      SETCURSOR(cursor:wait)
                      Open(ProgressWindow)
                      Progress:Thermometer    = 0
                      ?Progress:PctText{prop:text} = '0% Completed'
                      ?Progress:text{prop:text}='UPDATING PARTS'
                      RecordsToProcess  = RECORDS(ccp:partnumberkey)+RECORDS(cwp:partnumberkey)
                      !Reset the part numbers in Common Faults
                      setcursor(cursor:wait)
                      save_ccp_id = access:commoncp.savefile()
                      access:commoncp.clearkey(ccp:partnumberkey)
                      ccp:part_number = sav:part_number
                      set(ccp:partnumberkey,ccp:partnumberkey)
                      ! Start Change 2392 BE(19/03/03)
                      save_pos# = 0
                      IF (ccp:Part_Number <> sav2:Part_Number) THEN
                          save_pos# = 1
                      END
                      ! End Change 2392 BE(19/03/03)
                      loop
                          if access:commoncp.next()
                             break
                          end !if
                          DO GetNextRecord2
                          if ccp:part_number <> sav:part_number      |
                              then break.  ! end if
  
                          ! Start Change 2392 BE(19/03/03)
                          IF (save_pos# = 1) THEN
                          ! End Change 2392 BE(19/03/03)
                              pos = Position(ccp:partnumberkey)
                          ! Start Change 2392 BE(19/03/03)
                          END
                          ! End Change 2392 BE(19/03/03)
  
                          ccp:Part_Number  = sav2:Part_Number
                          ccp:Description = sav2:Description
                          ! Start Change 2392 BE(19/03/03
                          ccp:supplier = sav2:Supplier
                          ccp:purchase_cost = sav2:Purchase_Cost
                          ccp:sale_cost = sav2:Sale_Cost
                          ! End Change 2392 BE(19/03/03)
                          access:commoncp.update()
  
                          ! Start Change 2392 BE(19/03/03)
                          IF (save_pos# = 1) THEN
                          ! End Change 2392 BE(19/03/03
                              Reset(ccp:PartNumberKey,pos)
                          ! Start Change 2392 BE(19/03/03)
                          END
                          ! End Change 2392 BE(19/03/03)
  
                      end !loop
                      access:commoncp.restorefile(save_ccp_id)
                      setcursor()
  
                      setcursor(cursor:wait)
                      save_cwp_id = access:commonwp.savefile()
                      access:commonwp.clearkey(cwp:partnumberkey)
                      cwp:part_number = sav:part_number
                      set(cwp:partnumberkey,cwp:partnumberkey)
                     ! Start Change 2392 BE(19/03/03)
                      save_pos# = 0
                      IF (cwp:Part_Number <> sav2:Part_Number) THEN
                          save_pos# = 1
                      END
                      ! End Change 2392 BE(19/03/03)
                      loop
                          if access:commonwp.next()
                             break
                          end !if
                          DO GetNextRecord2
                          if cwp:part_number <> sav:part_number      |
                              then break.  ! end if
  
                          ! Start Change 2392 BE(19/03/03)
                          IF (save_pos# = 1) THEN
                          ! End Change 2392 BE(19/03/03)
                              pos = Position(cwp:partnumberkey)
                          ! Start Change 2392 BE(19/03/03
                          END
                          ! End Change 2392 BE(19/03/03)
  
                          cwp:Part_Number  = sav2:Part_Number
                          cwp:Description = sav2:Description
                          ! Start Change 2392 BE(19/03/03
                          cwp:supplier = sav2:Supplier
                          cwp:purchase_cost = sav2:Purchase_Cost
                          cwp:sale_cost = sav2:Sale_Cost
                          ! End Change 2392 BE(19/03/03)
                          access:commonwp.update()
  
                          ! Start Change 2392 BE(19/03/03)
                          IF (save_pos# = 1) THEN
                          ! End Change 2392 BE(19/03/03)
                              Reset(cwp:PartNumberKey,pos)
                          ! Start Change 2392 BE(19/03/03)
                          END
                          ! End Change 2392 BE(19/03/03)
  
                      end !loop
                      access:commonwp.restorefile(save_cwp_id)
                      setcursor()
                      CLOSE(ProgressWindow)
  
                      relate:stock.close()
                      relate:commoncp.close()
                      relate:commonwp.close()
                  Of 2 ! &No Button
  
                      ! Start Change 4795 BE(12/10/2004)
                      DO ReplicateToPendingParts
                      DO ReplicateToOrderedParts
                      ! End Change 4795 BE(12/10/2004)
  
              End!Case MessageEx
          End
      End!If thiswindow.response <> RequestCancelled
  End!If thiswindow.request <> insertrecord
  END
  If replicate_temp = 'YES' And thiswindow.response <> Requestcancelled
      Pick_Other_Location('SINGLE',ref_number_temp)
  End!If replicate_temp = 'YES'
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateSTOCK',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    sto:Location = glo:select1
    ref_number_temp = sto:ref_number
  PARENT.PrimeFields


ThisWindow.Reset PROCEDURE(BYTE Force=0)

  CODE
  SELF.ForcedReset += Force
  IF QuickWindow{Prop:AcceptAll} THEN RETURN.
  los:Site_Location = sto:Location                    ! Assign linking field value
  los:Shelf_Location = sto:Shelf_Location             ! Assign linking field value
  Access:LOCSHELF.Fetch(los:Shelf_Location_Key)
  PARENT.Reset(Force)


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    If request = 1    !Insert
        If sto:manufacturer = '' Or sto:part_number = '' Or sto:description = '' Or |
            sto:location = ''
            Case MessageEx('The following fields MUST be filled in before you can attach Model Numbers to this part:<13,10><13,10>Part Number<13,10>Description<13,10>Manufacturer','ServiceBase 2000',|
                           'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                Of 1 ! &OK Button
            End!Case MessageEx
        Else!sto:location = ''
            glo:select2  = sto:manufacturer
            Globalrequest   = Selectrecord
            Pick_Manufactuer_Model
            If Records(glo:Q_ModelNumber)

                ! Start Change 3317 BE(12/11/03)
                ReplicateModels# = GETINI('Stock','ReplicateModels',,CLIP(PATH())&'\SB2KDEF.INI')
                ! End Change 3317 BE(12/11/03)

                Loop x# = 1 To Records(glo:Q_ModelNumber)
                    get(glo:Q_ModelNumber,x#)
                    get(stomodel,0)
                    if access:stomodel.primerecord() = level:benign

                        stm:ref_number   = sto:ref_number
                        stm:manufacturer = sto:manufacturer
                        stm:model_number = glo:model_number_pointer
                        stm:part_number  = sto:part_number
                        stm:description  = sto:description
                        stm:location     = sto:location
                        stm:accessory    = sto:accessory
                        if access:stomodel.tryinsert()
                            access:stomodel.cancelautoinc()
                        end
                    end!if access:stomodel.primerecord() = level:benign

                    ! Start Change 3317 BE(12/11/03)
                    IF (ReplicateModels#) THEN
                        LOOP y# = 1 TO RECORDS(OtherLocationQueue)
                            GET(OtherLocationQueue, y#)
                            IF (access:stomodel.primerecord() = level:benign) THEN
                                stm:ref_number   = othque:ref_number
                                stm:manufacturer = sto:manufacturer
                                stm:model_number = glo:model_number_pointer
                                stm:part_number  = sto:part_number
                                stm:description  = sto:description
                                stm:location     = othque:Location
                                stm:accessory    = sto:accessory
                                IF (access:stomodel.tryinsert() <> Level:benign) THEN
                                    access:stomodel.cancelautoinc()
                                END
                            END
                        END
                    END
                    ! End Change 3317 BE(12/11/03)

                End !Loop x$ = 1 To Records(glo:Q_ModelNumber)
            End !If Records(glo:Q_ModelNumber)
            glo:select2 = ''
        End!sto:location = ''

    ! Start Change 3317 BE(12/11/03)
    ELSIF (request = DeleteRecord) THEN
        Manufacturer" =  stm:Manufacturer
        Model_Number" =  stm:Model_Number
        GlobalRequest = Request
        Update_Stock_Model_Numbers
        ReturnValue = GlobalResponse
        ReplicateModels# = GETINI('Stock','ReplicateModels',,CLIP(PATH())&'\SB2KDEF.INI')
        IF (ReplicateModels#) THEN
            LOOP y# = 1 TO RECORDS(OtherLocationQueue)
                GET(OtherLocationQueue, y#)
                access:stomodel_alias.clearkey(stm_ali:Model_Number_key)
                stm_ali:ref_number = othque:ref_number
                stm_ali:Manufacturer = CLIP(Manufacturer")
                stm_ali:Model_Number = CLIP(Model_Number")
                IF (access:stomodel_alias.tryfetch(stm_ali:Model_Number_key) = Level:benign) THEN
                    DELETE(STOMODEL_ALIAS)
                END
            END
        END
        !DISPLAY()
    ! End Change 3317 BE(12/11/03)

    Else
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      BrowseShelfLocations
      Browse_Suppliers
      Update_Stock_Model_Numbers
    END
    ReturnValue = GlobalResponse
  END
  End !If request = 1    !Insert
  BRW8.ResetSort(1)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?sto:Accessory
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Accessory, Accepted)
      If sto:accessory <> accessory_temp
          If accessory_temp = 'NO'
			Case MessageEx('Are you sure you want to mark this item as an Accessory?','ServiceBase 2000',|
			               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
				Of 1 ! &Yes Button

                  accessory_temp = sto:accessory
                  Do update_stomodel
				Of 2 ! &No Button

                  sto:accessory   = accessory_temp
			End!Case MessageEx
          Else!If accessory_temp = 'NO'
			Case MessageEx('Are you sure you want to mark this item as NOT an Accessory?','ServiceBase 2000',|
			               'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
				Of 1 ! &Yes Button
                  accessory_temp = sto:accessory
                  Do update_stomodel
				Of 2 ! &No Button
                  sto:accessory   = accessory_temp
			End!Case MessageEx
          End!If accessory_temp = 'YES'
      End!If sto:accessory <> accessory_temp
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Accessory, Accepted)
    OF ?sto:Suspend
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Suspend, Accepted)
      If ~0{prop:AcceptAll}
          If sto:Suspend = 1
              Case MessageEx('You have selected to Suspend this part.'&|
                '<13,10>'&|
                '<13,10>You will not be able to use this part for repairs, order it or sell it.'&|
                '<13,10>'&|
                '<13,10>Are you sure?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                  Of 2 ! &No Button
                      sto:Suspend = 0
              End!Case MessageEx
          Else !If sto:Suspend = 1
              Case MessageEx('This part is now free to be used for repairs, ordering or selling.','ServiceBase 2000',|
                             'Styles\idea.ico','|&OK',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
          End !If sto:Suspend = 1
      End !0{prop:AcceptAll}
      Display(?sto:Suspend)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Suspend, Accepted)
    OF ?OK
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
      sav2:Part_Number    =sto:Part_Number
      sav2:Description    =sto:Description
      sav2:Supplier       =sto:Supplier
      sav2:Purchase_Cost  =sto:Purchase_Cost
      sav2:Sale_Cost      =sto:Sale_Cost
      sav2:Retail_Cost    =sto:Retail_Cost
      sav2:Percentage_Mark_Up=sto:Percentage_Mark_Up
      sav2:Shelf_Location =sto:Shelf_Location
      sav2:Manufacturer   =sto:Manufacturer
      sav2:Second_Location=sto:Second_Location
      sav2:Minimum_Level  =sto:Minimum_Level
      sav2:Reorder_Level  =sto:Reorder_Level
      sav2:Accessory      =sto:Accessory
      sav2:ExchangeUnit   =sto:ExchangeUnit
      sav2:Suspend        =sto:Suspend
      
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OK, Accepted)
    OF ?Replicate_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Temp, Accepted)
      If ~0{prop:acceptall}
          If replicate_Temp = 'YES'
              Case MessageEx('Are you sure you want to replicate this Part across all your Site Locations?','ServiceBase 2000',|
                             'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                  Of 2 ! &No Button
                      replicate_Temp = 'NO'
              End!Case MessageEx
          End!If replicate_Temp = 'YES'
      End!If ~0{prop:acceptall}
      Display()
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Replicate_Temp, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?sto:Shelf_Location
      IF sto:Shelf_Location OR ?sto:Shelf_Location{Prop:Req}
        los:Site_Location = sto:Location
        los:Shelf_Location = sto:Shelf_Location
        los:Site_Location = sto:Location
        GLO:Select1 = sto:Location
        !Save Lookup Field Incase Of error
        look:sto:Shelf_Location        = sto:Shelf_Location
        IF Access:LOCSHELF.TryFetch(los:Shelf_Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            sto:Location = los:Site_Location
            sto:Shelf_Location = los:Shelf_Location
          ELSE
            CLEAR(los:Site_Location)
            CLEAR(GLO:Select1)
            !Restore Lookup On Error
            sto:Shelf_Location = look:sto:Shelf_Location
            SELECT(?sto:Shelf_Location)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Shelf_Location
      ThisWindow.Update
      los:Site_Location = sto:Location
      los:Shelf_Location = sto:Shelf_Location
      GLO:Select1 = sto:Location
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          sto:Location = los:Site_Location
          sto:Shelf_Location = los:Shelf_Location
          Select(?+1)
      ELSE
          Select(?sto:Shelf_Location)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sto:Shelf_Location)
    OF ?sto:Supplier
      IF sto:Supplier OR ?sto:Supplier{Prop:Req}
        sup:Company_Name = sto:Supplier
        !Save Lookup Field Incase Of error
        look:sto:Supplier        = sto:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(2,SelectRecord) = RequestCompleted
            sto:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            sto:Supplier = look:sto:Supplier
            SELECT(?sto:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?Lookup_Supplier
      ThisWindow.Update
      sup:Company_Name = sto:Supplier
      
      IF SELF.RUN(2,Selectrecord)  = RequestCompleted
          sto:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?sto:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?sto:Supplier)
    OF ?sto:Purchase_Cost
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Purchase_Cost, Accepted)
      If sto:percentage_mark_up <> ''
          sto:sale_cost = sto:purchase_cost + (sto:purchase_cost * (sto:percentage_mark_up/100))
      End
      Do hide_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Purchase_Cost, Accepted)
    OF ?sto:Percentage_Mark_Up
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Percentage_Mark_Up, Accepted)
      If sto:percentage_mark_up <> ''
          sto:sale_cost = sto:purchase_cost + (sto:purchase_cost * (sto:percentage_mark_up/100))
      Else
          Select(?sto:sale_cost)
      End
      Do hide_Fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Percentage_Mark_Up, Accepted)
    OF ?sto:Accessory
      IF ?sto:Accessory{Prop:Checked} = True
        UNHIDE(?sto:AccessoryCost:Prompt)
        UNHIDE(?sto:AccessoryCost)
      END
      IF ?sto:Accessory{Prop:Checked} = False
        HIDE(?sto:AccessoryCost:Prompt)
        HIDE(?sto:AccessoryCost)
      END
      ThisWindow.Reset
    OF ?sto:Manufacturer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Manufacturer, Accepted)
      If ~0{prop:acceptall}
          If sto:manufacturer <> manufacturer_temp
              If manufacturer_temp = ''
                  manufacturer_temp = sto:manufacturer
              Else!If manufacturer_temp = ''
                  Case MessageEx('Warning! If you continue to change the Manufacturer of this stock item, all the attached Model Numbers will be deleted.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                                 'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
                      Of 1 ! &Yes Button
                        manufacturer_temp = sto:manufacturer
                        setcursor(cursor:wait)
                        save_stm_id = access:stomodel.savefile()
                        access:stomodel.clearkey(stm:model_number_key)
                        stm:ref_number   = sto:ref_number
                        set(stm:model_number_key,stm:model_number_key)
                        loop
                            if access:stomodel.next()
                               break
                            end !if
                            if stm:ref_number   <> sto:ref_number      |
                                then break.  ! end if
                            delete(stomodel)
                            yldcnt# += 1
                            if yldcnt# > 25
                               yield() ; yldcnt# = 0
                            end !if
                        end !loop
                        access:stomodel.restorefile(save_stm_id)
                        setcursor()
                      Of 2 ! &No Button
                        sto:manufacturer = manufacturer_temp
                  End!Case MessageEx
      
              End!If manufacturer_temp = ''
          End!If sto:manufacturer <> manufacturer_temp And manufacturer_temp <> ''
          Display(?sto:manufacturer)
      End!If ~0{prop:acceptall}
      Do Fault_Coding
      BRW8.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Manufacturer, Accepted)
    OF ?Fault_Codes_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes_Button, Accepted)
      Access:STOMODEL.ClearKey(stm:Model_Number_Key)
      stm:Ref_Number   = sto:Ref_Number
      stm:Manufacturer = sto:Manufacturer
      stm:Model_Number = brw8.q.stm:Model_Number
      If Access:STOMODEL.TryFetch(stm:Model_Number_Key) = Level:Benign
          GlobalRequest = ChangeRecord
          Stock_Fault_Codes
      End
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Fault_Codes_Button, Accepted)
    OF ?sto:Assign_Fault_Codes
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Assign_Fault_Codes, Accepted)
      Do fault_coding
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Assign_Fault_Codes, Accepted)
    OF ?sto:Sundry_Item
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Sundry_Item, Accepted)
      Do hide_fields
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Sundry_Item, Accepted)
    OF ?Clear_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Stock, Accepted)
    If SecurityCheck('ZERO QUANTITY IN STOCK')
        Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                       'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
            Of 1 ! &OK Button
        End!Case MessageEx
    Else !If SecurityCheck('ZERO QUANTITY IN STOCK')

        Case MessageEx('This will zero the Quantity In Stock.<13,10><13,10>Are you sure?','ServiceBase 2000',|
                       'Styles\warn.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                shi:ref_number           = sto:ref_number
                shi:transaction_type     = 'DEC'
                shi:despatch_note_number = ''
                shi:quantity             = sto:quantity_stock
                shi:date                 = Today()
                shi:purchase_cost        = sto:purchase_cost
                shi:sale_cost            = sto:sale_cost
                shi:retail_cost          = sto:retail_cost
                access:users.clearkey(use:password_key)
                use:password  =glo:password
                access:users.fetch(use:password_key)
                shi:user    = use:user_code
                shi:notes                = 'STOCK VALUE ZEROED'
                access:stohist.insert()
                sto:quantity_stock = 0
            Of 2 ! &No Button
        End!Case MessageEx

            Do show_details
    End !If SecurityCheck('ZERO QUANTITY IN STOCK')
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Clear_Stock, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  If sto:sundry_item = 'YES'
      sto:quantity_stock    = 1
      sto:quantity_to_order = 0
      sto:quantity_on_order = 0
  End!If sto:sundry_item = 'YES'
  
  setcursor(cursor:wait)
  
  found# = 0
  save_stm_id = access:stomodel.savefile()
  access:stomodel.clearkey(stm:model_number_key)
  stm:ref_number   = sto:ref_number
  stm:manufacturer = sto:manufacturer
  set(stm:model_number_key,stm:model_number_key)
  loop
      if access:stomodel.next()
         break
      end !if
      if stm:ref_number   <> sto:ref_number
        BREAK
      END  ! end if  |
      IF stm:manufacturer <> sto:manufacturer  
        BREAK
      END  ! end if
      found# = 1
      Break
  end !loop
  access:stomodel.restorefile(save_stm_id)
  setcursor()
  
  If found# = 0
    Case MessageEx('There are NO Model Numbers attached to this part. If you continue this item will not appear in Service Stock Lists.<13,10><13,10>(Note: it will still appear in Retail Stock Lists)<13,10>Are you sure?','ServiceBase 2000',|
                   'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
        Of 1 ! &Yes Button
        Of 2 ! &No Button
          Select(?sto:manufacturer)
          Cycle
    End!Case MessageEx
  End
    
  !Is this a new record, and has this part already been added to this location?
  found# = 0
  save_sto_ali_id = access:stock_alias.savefile()
  access:stock_alias.clearkey(sto_ali:location_key)
  sto_ali:location    = sto:location
  sto_ali:part_number = sto:part_number
  set(sto_ali:location_key,sto_ali:location_key)
  loop
      if access:stock_alias.next()
         break
      end !if
      IF sto_ali:location    <> sto:location
        BREAK
      END !End if     |
      IF sto_ali:part_number <> sto:part_number      
        BREAK
      END  ! end if
      If sto:ref_number <> sto_ali:ref_number
          found# += 1
          Break
      End!If sto:ref_number <> sto_ali:ref_number
  end !loop
  access:stock_alias.restorefile(save_sto_ali_id)
  
  If found# = 1
      Case MessageEx('The entered Part Number already exists in this Location.<13,10><13,10>Are you sure you want to continue.','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
          Of 2 ! &No Button
              Select(?sto:part_number)
              Cycle
      End!Case MessageEx
  End!If found# = 1
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateSTOCK')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?sto:Shelf_Location
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Shelf_Location, AlertKey)
      Post(Event:accepted,?Lookup_Shelf_Location)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Shelf_Location, AlertKey)
    END
  OF ?sto:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Supplier, AlertKey)
      Post(Event:accepted,?Lookup_Supplier)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sto:Supplier, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Save Fields
      manufacturer_temp   = sto:manufacturer
      part_number_temp    = sto:part_number
      description_temp    = sto:description
      accessory_temp      = sto:accessory
      
      sav:Part_Number         = STO:Part_Number
      sav:Description         = STO:Description
      sav:Supplier            = STO:Supplier
      sav:Purchase_Cost       = STO:Purchase_Cost
      sav:Sale_Cost           = STO:Sale_Cost
      sav:Retail_Cost         = STO:Retail_Cost
      sav:Percentage_Mark_Up  = STO:Percentage_Mark_Up
      sav:Shelf_Location      = STO:Shelf_Location
      sav:Manufacturer        = STO:Manufacturer
      sav:Second_Location     = STO:Second_Location
      sav:Minimum_Level       = STO:Minimum_Level
      sav:Reorder_Level       = STO:Reorder_Level
      sav:Accessory           = STO:Accessory
      sav:Suspend             = sto:suspend
      sav:ExchangeUnit        = sto:ExchangeUnit
      
      Compile('***',Debug=1)
          Unhide(?sto:ref_number)
      ***
      Do Fault_Coding
      If thiswindow.request = Insertrecord
          Unhide(?replicate_temp)
      End!If thiswindow.request = Insertrecord
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      Do Fill_List
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW8.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.DeleteControl=?Delete
  END


BRW8.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

