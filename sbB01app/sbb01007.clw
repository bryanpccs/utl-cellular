

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01007.INC'),ONCE        !Local module procedure declarations
                     END


Stock_Fault_Codes PROCEDURE                           !Generated from procedure template - Window

FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
Window               WINDOW('Fault Codes'),AT(,,242,259),FONT('Tahoma',8,,FONT:regular),CENTER,ICON('PC.ICO'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,236,224),USE(?Sheet1),SPREAD
                         TAB('Fault Codes'),USE(?Tab1)
                           PROMPT('Model Number'),AT(8,20),USE(?stm:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(stm:Model_Number),SKIP,FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 1:'),AT(8,36),USE(?stm:FaultCode1:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,36,124,10),USE(stm:FaultCode1),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 1')
                           ENTRY(@d6b),AT(56,36,64,10),USE(stm:FaultCode1,,?stm:FaultCode1:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 1')
                           BUTTON,AT(212,36,10,10),USE(?Lookup),HIDE,ICON('list3.ico')
                           BUTTON,AT(224,36,10,10),USE(?PopCalendar),HIDE,ICON('calenda2.ico')
                           PROMPT('Fault Code 2:'),AT(8,52),USE(?stm:FaultCode2:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,52,124,10),USE(stm:FaultCode2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 2')
                           ENTRY(@d6b),AT(56,52,64,10),USE(stm:FaultCode2,,?stm:FaultCode2:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 2')
                           BUTTON,AT(212,52,10,10),USE(?Lookup:2),HIDE,ICON('list3.ico')
                           BUTTON,AT(224,52,10,10),USE(?PopCalendar:2),HIDE,ICON('calenda2.ico')
                           PROMPT('Fault Code 3:'),AT(8,68),USE(?stm:FaultCode3:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,68,124,10),USE(stm:FaultCode3),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 3')
                           ENTRY(@d6b),AT(56,68,64,10),USE(stm:FaultCode3,,?stm:FaultCode3:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 3')
                           BUTTON,AT(212,68,10,10),USE(?Lookup:3),HIDE,ICON('list3.ico')
                           BUTTON,AT(224,68,10,10),USE(?PopCalendar:3),HIDE,ICON('calenda2.ico')
                           PROMPT('Fault Code 4:'),AT(8,84),USE(?stm:FaultCode4:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,84,124,10),USE(stm:FaultCode4),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 4')
                           ENTRY(@d6b),AT(56,84,64,10),USE(stm:FaultCode4,,?stm:FaultCode4:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 4')
                           BUTTON,AT(224,84,10,10),USE(?PopCalendar:4),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,84,10,10),USE(?Lookup:4),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 5:'),AT(8,100),USE(?stm:FaultCode5:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,100,124,10),USE(stm:FaultCode5),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 5')
                           ENTRY(@d6b),AT(56,100,64,10),USE(stm:FaultCode5,,?stm:FaultCode5:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 5')
                           BUTTON,AT(224,100,10,10),USE(?PopCalendar:5),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,100,10,10),USE(?Lookup:5),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 6:'),AT(8,116),USE(?stm:FaultCode6:Prompt),TRN,HIDE
                           ENTRY(@s30),AT(84,116,124,10),USE(stm:FaultCode6),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 6')
                           ENTRY(@d6b),AT(56,116,64,10),USE(stm:FaultCode6,,?stm:FaultCode6:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 6')
                           BUTTON,AT(224,116,10,10),USE(?PopCalendar:6),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,116,10,10),USE(?Lookup:6),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 7:'),AT(8,132),USE(?stm:FaultCode7:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,132,124,10),USE(stm:FaultCode7),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 7')
                           ENTRY(@d6b),AT(56,132,64,10),USE(stm:FaultCode7,,?stm:FaultCode7:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 7')
                           BUTTON,AT(224,132,10,10),USE(?PopCalendar:7),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,132,10,10),USE(?Lookup:7),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 8:'),AT(8,148),USE(?stm:FaultCode8:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,148,124,10),USE(stm:FaultCode8),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 8')
                           ENTRY(@d6b),AT(56,148,64,10),USE(stm:FaultCode8,,?stm:FaultCode8:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 8')
                           BUTTON,AT(224,148,10,10),USE(?PopCalendar:8),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,148,10,10),USE(?Lookup:8),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 9:'),AT(8,164),USE(?stm:FaultCode9:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,164,124,10),USE(stm:FaultCode9),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 9')
                           ENTRY(@d6b),AT(56,164,64,10),USE(stm:FaultCode9,,?stm:FaultCode9:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 9')
                           BUTTON,AT(224,164,10,10),USE(?PopCalendar:9),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,164,10,10),USE(?Lookup:9),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 10:'),AT(8,180),USE(?stm:FaultCode10:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,180,124,10),USE(stm:FaultCode10),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 10')
                           ENTRY(@d6b),AT(56,180,64,10),USE(stm:FaultCode10,,?stm:FaultCode10:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 10')
                           BUTTON,AT(224,180,10,10),USE(?PopCalendar:10),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,180,10,10),USE(?Lookup:10),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 11:'),AT(8,196),USE(?stm:FaultCode11:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,196,124,10),USE(stm:FaultCode11),HIDE,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,MSG('Fault Code 11')
                           ENTRY(@d6b),AT(56,196,64,10),USE(stm:FaultCode11,,?stm:FaultCode11:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 11')
                           BUTTON,AT(224,196,10,10),USE(?PopCalendar:11),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,196,10,10),USE(?Lookup:11),HIDE,ICON('list3.ico')
                           PROMPT('Fault Code 12:'),AT(8,212),USE(?stm:FaultCode12:Prompt),TRN,HIDE
                           ENTRY(@S255),AT(84,212,124,10),USE(stm:FaultCode12),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 12')
                           ENTRY(@d6b),AT(56,212,64,10),USE(stm:FaultCode12,,?stm:FaultCode12:2),HIDE,FONT('Tahoma',8,,FONT:bold),UPR,MSG('Fault Code 12')
                           BUTTON,AT(224,212,10,10),USE(?PopCalendar:12),HIDE,ICON('calenda2.ico')
                           BUTTON,AT(212,212,10,10),USE(?Lookup:12),HIDE,ICON('list3.ico')
                         END
                       END
                       PANEL,AT(4,232,236,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(124,236,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(180,236,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_map_id   ushort,auto
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?stm:Model_Number:Prompt{prop:FontColor} = -1
    ?stm:Model_Number:Prompt{prop:Color} = 15066597
    If ?stm:Model_Number{prop:ReadOnly} = True
        ?stm:Model_Number{prop:FontColor} = 65793
        ?stm:Model_Number{prop:Color} = 15066597
    Elsif ?stm:Model_Number{prop:Req} = True
        ?stm:Model_Number{prop:FontColor} = 65793
        ?stm:Model_Number{prop:Color} = 8454143
    Else ! If ?stm:Model_Number{prop:Req} = True
        ?stm:Model_Number{prop:FontColor} = 65793
        ?stm:Model_Number{prop:Color} = 16777215
    End ! If ?stm:Model_Number{prop:Req} = True
    ?stm:Model_Number{prop:Trn} = 0
    ?stm:Model_Number{prop:FontStyle} = font:Bold
    ?stm:FaultCode1:Prompt{prop:FontColor} = -1
    ?stm:FaultCode1:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode1{prop:ReadOnly} = True
        ?stm:FaultCode1{prop:FontColor} = 65793
        ?stm:FaultCode1{prop:Color} = 15066597
    Elsif ?stm:FaultCode1{prop:Req} = True
        ?stm:FaultCode1{prop:FontColor} = 65793
        ?stm:FaultCode1{prop:Color} = 8454143
    Else ! If ?stm:FaultCode1{prop:Req} = True
        ?stm:FaultCode1{prop:FontColor} = 65793
        ?stm:FaultCode1{prop:Color} = 16777215
    End ! If ?stm:FaultCode1{prop:Req} = True
    ?stm:FaultCode1{prop:Trn} = 0
    ?stm:FaultCode1{prop:FontStyle} = font:Bold
    If ?stm:FaultCode1:2{prop:ReadOnly} = True
        ?stm:FaultCode1:2{prop:FontColor} = 65793
        ?stm:FaultCode1:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode1:2{prop:Req} = True
        ?stm:FaultCode1:2{prop:FontColor} = 65793
        ?stm:FaultCode1:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode1:2{prop:Req} = True
        ?stm:FaultCode1:2{prop:FontColor} = 65793
        ?stm:FaultCode1:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode1:2{prop:Req} = True
    ?stm:FaultCode1:2{prop:Trn} = 0
    ?stm:FaultCode1:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode2:Prompt{prop:FontColor} = -1
    ?stm:FaultCode2:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode2{prop:ReadOnly} = True
        ?stm:FaultCode2{prop:FontColor} = 65793
        ?stm:FaultCode2{prop:Color} = 15066597
    Elsif ?stm:FaultCode2{prop:Req} = True
        ?stm:FaultCode2{prop:FontColor} = 65793
        ?stm:FaultCode2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode2{prop:Req} = True
        ?stm:FaultCode2{prop:FontColor} = 65793
        ?stm:FaultCode2{prop:Color} = 16777215
    End ! If ?stm:FaultCode2{prop:Req} = True
    ?stm:FaultCode2{prop:Trn} = 0
    ?stm:FaultCode2{prop:FontStyle} = font:Bold
    If ?stm:FaultCode2:2{prop:ReadOnly} = True
        ?stm:FaultCode2:2{prop:FontColor} = 65793
        ?stm:FaultCode2:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode2:2{prop:Req} = True
        ?stm:FaultCode2:2{prop:FontColor} = 65793
        ?stm:FaultCode2:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode2:2{prop:Req} = True
        ?stm:FaultCode2:2{prop:FontColor} = 65793
        ?stm:FaultCode2:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode2:2{prop:Req} = True
    ?stm:FaultCode2:2{prop:Trn} = 0
    ?stm:FaultCode2:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode3:Prompt{prop:FontColor} = -1
    ?stm:FaultCode3:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode3{prop:ReadOnly} = True
        ?stm:FaultCode3{prop:FontColor} = 65793
        ?stm:FaultCode3{prop:Color} = 15066597
    Elsif ?stm:FaultCode3{prop:Req} = True
        ?stm:FaultCode3{prop:FontColor} = 65793
        ?stm:FaultCode3{prop:Color} = 8454143
    Else ! If ?stm:FaultCode3{prop:Req} = True
        ?stm:FaultCode3{prop:FontColor} = 65793
        ?stm:FaultCode3{prop:Color} = 16777215
    End ! If ?stm:FaultCode3{prop:Req} = True
    ?stm:FaultCode3{prop:Trn} = 0
    ?stm:FaultCode3{prop:FontStyle} = font:Bold
    If ?stm:FaultCode3:2{prop:ReadOnly} = True
        ?stm:FaultCode3:2{prop:FontColor} = 65793
        ?stm:FaultCode3:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode3:2{prop:Req} = True
        ?stm:FaultCode3:2{prop:FontColor} = 65793
        ?stm:FaultCode3:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode3:2{prop:Req} = True
        ?stm:FaultCode3:2{prop:FontColor} = 65793
        ?stm:FaultCode3:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode3:2{prop:Req} = True
    ?stm:FaultCode3:2{prop:Trn} = 0
    ?stm:FaultCode3:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode4:Prompt{prop:FontColor} = -1
    ?stm:FaultCode4:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode4{prop:ReadOnly} = True
        ?stm:FaultCode4{prop:FontColor} = 65793
        ?stm:FaultCode4{prop:Color} = 15066597
    Elsif ?stm:FaultCode4{prop:Req} = True
        ?stm:FaultCode4{prop:FontColor} = 65793
        ?stm:FaultCode4{prop:Color} = 8454143
    Else ! If ?stm:FaultCode4{prop:Req} = True
        ?stm:FaultCode4{prop:FontColor} = 65793
        ?stm:FaultCode4{prop:Color} = 16777215
    End ! If ?stm:FaultCode4{prop:Req} = True
    ?stm:FaultCode4{prop:Trn} = 0
    ?stm:FaultCode4{prop:FontStyle} = font:Bold
    If ?stm:FaultCode4:2{prop:ReadOnly} = True
        ?stm:FaultCode4:2{prop:FontColor} = 65793
        ?stm:FaultCode4:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode4:2{prop:Req} = True
        ?stm:FaultCode4:2{prop:FontColor} = 65793
        ?stm:FaultCode4:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode4:2{prop:Req} = True
        ?stm:FaultCode4:2{prop:FontColor} = 65793
        ?stm:FaultCode4:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode4:2{prop:Req} = True
    ?stm:FaultCode4:2{prop:Trn} = 0
    ?stm:FaultCode4:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode5:Prompt{prop:FontColor} = -1
    ?stm:FaultCode5:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode5{prop:ReadOnly} = True
        ?stm:FaultCode5{prop:FontColor} = 65793
        ?stm:FaultCode5{prop:Color} = 15066597
    Elsif ?stm:FaultCode5{prop:Req} = True
        ?stm:FaultCode5{prop:FontColor} = 65793
        ?stm:FaultCode5{prop:Color} = 8454143
    Else ! If ?stm:FaultCode5{prop:Req} = True
        ?stm:FaultCode5{prop:FontColor} = 65793
        ?stm:FaultCode5{prop:Color} = 16777215
    End ! If ?stm:FaultCode5{prop:Req} = True
    ?stm:FaultCode5{prop:Trn} = 0
    ?stm:FaultCode5{prop:FontStyle} = font:Bold
    If ?stm:FaultCode5:2{prop:ReadOnly} = True
        ?stm:FaultCode5:2{prop:FontColor} = 65793
        ?stm:FaultCode5:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode5:2{prop:Req} = True
        ?stm:FaultCode5:2{prop:FontColor} = 65793
        ?stm:FaultCode5:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode5:2{prop:Req} = True
        ?stm:FaultCode5:2{prop:FontColor} = 65793
        ?stm:FaultCode5:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode5:2{prop:Req} = True
    ?stm:FaultCode5:2{prop:Trn} = 0
    ?stm:FaultCode5:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode6:Prompt{prop:FontColor} = -1
    ?stm:FaultCode6:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode6{prop:ReadOnly} = True
        ?stm:FaultCode6{prop:FontColor} = 65793
        ?stm:FaultCode6{prop:Color} = 15066597
    Elsif ?stm:FaultCode6{prop:Req} = True
        ?stm:FaultCode6{prop:FontColor} = 65793
        ?stm:FaultCode6{prop:Color} = 8454143
    Else ! If ?stm:FaultCode6{prop:Req} = True
        ?stm:FaultCode6{prop:FontColor} = 65793
        ?stm:FaultCode6{prop:Color} = 16777215
    End ! If ?stm:FaultCode6{prop:Req} = True
    ?stm:FaultCode6{prop:Trn} = 0
    ?stm:FaultCode6{prop:FontStyle} = font:Bold
    If ?stm:FaultCode6:2{prop:ReadOnly} = True
        ?stm:FaultCode6:2{prop:FontColor} = 65793
        ?stm:FaultCode6:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode6:2{prop:Req} = True
        ?stm:FaultCode6:2{prop:FontColor} = 65793
        ?stm:FaultCode6:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode6:2{prop:Req} = True
        ?stm:FaultCode6:2{prop:FontColor} = 65793
        ?stm:FaultCode6:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode6:2{prop:Req} = True
    ?stm:FaultCode6:2{prop:Trn} = 0
    ?stm:FaultCode6:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode7:Prompt{prop:FontColor} = -1
    ?stm:FaultCode7:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode7{prop:ReadOnly} = True
        ?stm:FaultCode7{prop:FontColor} = 65793
        ?stm:FaultCode7{prop:Color} = 15066597
    Elsif ?stm:FaultCode7{prop:Req} = True
        ?stm:FaultCode7{prop:FontColor} = 65793
        ?stm:FaultCode7{prop:Color} = 8454143
    Else ! If ?stm:FaultCode7{prop:Req} = True
        ?stm:FaultCode7{prop:FontColor} = 65793
        ?stm:FaultCode7{prop:Color} = 16777215
    End ! If ?stm:FaultCode7{prop:Req} = True
    ?stm:FaultCode7{prop:Trn} = 0
    ?stm:FaultCode7{prop:FontStyle} = font:Bold
    If ?stm:FaultCode7:2{prop:ReadOnly} = True
        ?stm:FaultCode7:2{prop:FontColor} = 65793
        ?stm:FaultCode7:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode7:2{prop:Req} = True
        ?stm:FaultCode7:2{prop:FontColor} = 65793
        ?stm:FaultCode7:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode7:2{prop:Req} = True
        ?stm:FaultCode7:2{prop:FontColor} = 65793
        ?stm:FaultCode7:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode7:2{prop:Req} = True
    ?stm:FaultCode7:2{prop:Trn} = 0
    ?stm:FaultCode7:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode8:Prompt{prop:FontColor} = -1
    ?stm:FaultCode8:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode8{prop:ReadOnly} = True
        ?stm:FaultCode8{prop:FontColor} = 65793
        ?stm:FaultCode8{prop:Color} = 15066597
    Elsif ?stm:FaultCode8{prop:Req} = True
        ?stm:FaultCode8{prop:FontColor} = 65793
        ?stm:FaultCode8{prop:Color} = 8454143
    Else ! If ?stm:FaultCode8{prop:Req} = True
        ?stm:FaultCode8{prop:FontColor} = 65793
        ?stm:FaultCode8{prop:Color} = 16777215
    End ! If ?stm:FaultCode8{prop:Req} = True
    ?stm:FaultCode8{prop:Trn} = 0
    ?stm:FaultCode8{prop:FontStyle} = font:Bold
    If ?stm:FaultCode8:2{prop:ReadOnly} = True
        ?stm:FaultCode8:2{prop:FontColor} = 65793
        ?stm:FaultCode8:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode8:2{prop:Req} = True
        ?stm:FaultCode8:2{prop:FontColor} = 65793
        ?stm:FaultCode8:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode8:2{prop:Req} = True
        ?stm:FaultCode8:2{prop:FontColor} = 65793
        ?stm:FaultCode8:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode8:2{prop:Req} = True
    ?stm:FaultCode8:2{prop:Trn} = 0
    ?stm:FaultCode8:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode9:Prompt{prop:FontColor} = -1
    ?stm:FaultCode9:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode9{prop:ReadOnly} = True
        ?stm:FaultCode9{prop:FontColor} = 65793
        ?stm:FaultCode9{prop:Color} = 15066597
    Elsif ?stm:FaultCode9{prop:Req} = True
        ?stm:FaultCode9{prop:FontColor} = 65793
        ?stm:FaultCode9{prop:Color} = 8454143
    Else ! If ?stm:FaultCode9{prop:Req} = True
        ?stm:FaultCode9{prop:FontColor} = 65793
        ?stm:FaultCode9{prop:Color} = 16777215
    End ! If ?stm:FaultCode9{prop:Req} = True
    ?stm:FaultCode9{prop:Trn} = 0
    ?stm:FaultCode9{prop:FontStyle} = font:Bold
    If ?stm:FaultCode9:2{prop:ReadOnly} = True
        ?stm:FaultCode9:2{prop:FontColor} = 65793
        ?stm:FaultCode9:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode9:2{prop:Req} = True
        ?stm:FaultCode9:2{prop:FontColor} = 65793
        ?stm:FaultCode9:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode9:2{prop:Req} = True
        ?stm:FaultCode9:2{prop:FontColor} = 65793
        ?stm:FaultCode9:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode9:2{prop:Req} = True
    ?stm:FaultCode9:2{prop:Trn} = 0
    ?stm:FaultCode9:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode10:Prompt{prop:FontColor} = -1
    ?stm:FaultCode10:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode10{prop:ReadOnly} = True
        ?stm:FaultCode10{prop:FontColor} = 65793
        ?stm:FaultCode10{prop:Color} = 15066597
    Elsif ?stm:FaultCode10{prop:Req} = True
        ?stm:FaultCode10{prop:FontColor} = 65793
        ?stm:FaultCode10{prop:Color} = 8454143
    Else ! If ?stm:FaultCode10{prop:Req} = True
        ?stm:FaultCode10{prop:FontColor} = 65793
        ?stm:FaultCode10{prop:Color} = 16777215
    End ! If ?stm:FaultCode10{prop:Req} = True
    ?stm:FaultCode10{prop:Trn} = 0
    ?stm:FaultCode10{prop:FontStyle} = font:Bold
    If ?stm:FaultCode10:2{prop:ReadOnly} = True
        ?stm:FaultCode10:2{prop:FontColor} = 65793
        ?stm:FaultCode10:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode10:2{prop:Req} = True
        ?stm:FaultCode10:2{prop:FontColor} = 65793
        ?stm:FaultCode10:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode10:2{prop:Req} = True
        ?stm:FaultCode10:2{prop:FontColor} = 65793
        ?stm:FaultCode10:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode10:2{prop:Req} = True
    ?stm:FaultCode10:2{prop:Trn} = 0
    ?stm:FaultCode10:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode11:Prompt{prop:FontColor} = -1
    ?stm:FaultCode11:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode11{prop:ReadOnly} = True
        ?stm:FaultCode11{prop:FontColor} = 65793
        ?stm:FaultCode11{prop:Color} = 15066597
    Elsif ?stm:FaultCode11{prop:Req} = True
        ?stm:FaultCode11{prop:FontColor} = 65793
        ?stm:FaultCode11{prop:Color} = 8454143
    Else ! If ?stm:FaultCode11{prop:Req} = True
        ?stm:FaultCode11{prop:FontColor} = 65793
        ?stm:FaultCode11{prop:Color} = 16777215
    End ! If ?stm:FaultCode11{prop:Req} = True
    ?stm:FaultCode11{prop:Trn} = 0
    ?stm:FaultCode11{prop:FontStyle} = font:Bold
    If ?stm:FaultCode11:2{prop:ReadOnly} = True
        ?stm:FaultCode11:2{prop:FontColor} = 65793
        ?stm:FaultCode11:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode11:2{prop:Req} = True
        ?stm:FaultCode11:2{prop:FontColor} = 65793
        ?stm:FaultCode11:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode11:2{prop:Req} = True
        ?stm:FaultCode11:2{prop:FontColor} = 65793
        ?stm:FaultCode11:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode11:2{prop:Req} = True
    ?stm:FaultCode11:2{prop:Trn} = 0
    ?stm:FaultCode11:2{prop:FontStyle} = font:Bold
    ?stm:FaultCode12:Prompt{prop:FontColor} = -1
    ?stm:FaultCode12:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode12{prop:ReadOnly} = True
        ?stm:FaultCode12{prop:FontColor} = 65793
        ?stm:FaultCode12{prop:Color} = 15066597
    Elsif ?stm:FaultCode12{prop:Req} = True
        ?stm:FaultCode12{prop:FontColor} = 65793
        ?stm:FaultCode12{prop:Color} = 8454143
    Else ! If ?stm:FaultCode12{prop:Req} = True
        ?stm:FaultCode12{prop:FontColor} = 65793
        ?stm:FaultCode12{prop:Color} = 16777215
    End ! If ?stm:FaultCode12{prop:Req} = True
    ?stm:FaultCode12{prop:Trn} = 0
    ?stm:FaultCode12{prop:FontStyle} = font:Bold
    If ?stm:FaultCode12:2{prop:ReadOnly} = True
        ?stm:FaultCode12:2{prop:FontColor} = 65793
        ?stm:FaultCode12:2{prop:Color} = 15066597
    Elsif ?stm:FaultCode12:2{prop:Req} = True
        ?stm:FaultCode12:2{prop:FontColor} = 65793
        ?stm:FaultCode12:2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode12:2{prop:Req} = True
        ?stm:FaultCode12:2{prop:FontColor} = 65793
        ?stm:FaultCode12:2{prop:Color} = 16777215
    End ! If ?stm:FaultCode12:2{prop:Req} = True
    ?stm:FaultCode12:2{prop:Trn} = 0
    ?stm:FaultCode12:2{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Fault_Coding        Routine   ! Manufacturers Fault Codes

    Hide(?stm:Faultcode1)
    Hide(?stm:Faultcode1:2)
    Hide(?stm:Faultcode2)
    Hide(?stm:Faultcode2:2)
    Hide(?stm:Faultcode3)
    Hide(?stm:Faultcode3:2)
    Hide(?stm:Faultcode4)
    Hide(?stm:Faultcode4:2)
    Hide(?stm:Faultcode5)
    Hide(?stm:Faultcode5:2)
    Hide(?stm:Faultcode6)
    Hide(?stm:Faultcode6:2)
    Hide(?stm:Faultcode7)
    Hide(?stm:Faultcode7:2)
    Hide(?stm:Faultcode8)
    Hide(?stm:Faultcode8:2)
    Hide(?stm:Faultcode9)
    Hide(?stm:Faultcode9:2)
    Hide(?stm:Faultcode10)
    Hide(?stm:Faultcode10:2)
    Hide(?stm:Faultcode11)
    Hide(?stm:Faultcode11:2)
    Hide(?stm:Faultcode12)
    Hide(?stm:Faultcode12:2)
    Hide(?stm:Faultcode1:prompt)
    Hide(?stm:Faultcode2:prompt)
    Hide(?stm:Faultcode3:prompt)
    Hide(?stm:Faultcode4:prompt)
    Hide(?stm:Faultcode5:prompt)
    Hide(?stm:Faultcode6:prompt)
    Hide(?stm:Faultcode7:prompt)
    Hide(?stm:Faultcode8:prompt)
    Hide(?stm:Faultcode9:prompt)
    Hide(?stm:Faultcode10:prompt)
    Hide(?stm:Faultcode11:prompt)
    Hide(?stm:Faultcode12:prompt)
    Hide(?popcalendar)
    Hide(?popcalendar:2)
    Hide(?popcalendar:3)
    Hide(?popcalendar:4)
    Hide(?popcalendar:5)
    Hide(?popcalendar:6)
    Hide(?popcalendar:7)
    Hide(?popcalendar:8)
    Hide(?popcalendar:9)
    Hide(?popcalendar:10)
    Hide(?popcalendar:11)
    Hide(?popcalendar:12)
    Hide(?lookup)
    Hide(?lookup:2)
    Hide(?lookup:3)
    Hide(?lookup:4)
    Hide(?lookup:5)
    Hide(?lookup:6)
    Hide(?lookup:7)
    Hide(?lookup:8)
    Hide(?lookup:9)
    Hide(?lookup:10)
    Hide(?lookup:11)
    Hide(?lookup:12)

    required# = 0

    setcursor(cursor:wait)
    
    save_map_id = access:manfaupa.savefile()
    access:manfaupa.clearkey(map:field_number_key)
    map:manufacturer = sto:manufacturer
    set(map:field_number_key,map:field_number_key)
    loop
        if access:manfaupa.next()
           break
        end !if
        if map:manufacturer <> sto:manufacturer      |
            then break.  ! end if
        yldcnt# += 1
        if yldcnt# > 25
           yield() ; yldcnt# = 0
        end !if

        Case map:field_number
            Of 1
                Unhide(?stm:Faultcode1:prompt)
                ?stm:Faultcode1:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar)
                    ?popcalendar{prop:xpos} = 300
                    Unhide(?stm:Faultcode1:2)
                    ?stm:Faultcode1:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode1)
                    If map:lookup = 'YES'
                        Unhide(?lookup)
                    End
                End
            Of 2
                Unhide(?stm:Faultcode2:prompt)
                ?stm:Faultcode2:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:2)
                    ?popcalendar:2{prop:xpos} = 300
                    Unhide(?stm:Faultcode2:2)
                    ?stm:Faultcode2:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode2)
                    If map:lookup = 'YES'
                        Unhide(?lookup:2)
                    End
                End

            Of 3
                Unhide(?stm:Faultcode3:prompt)
                ?stm:Faultcode3:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:3)
                    ?popcalendar:3{prop:xpos} = 300
                    Unhide(?stm:Faultcode3:2)
                    ?stm:Faultcode3:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode3)
                    If map:lookup = 'YES'
                        Unhide(?lookup:3)
                    End
                End

            Of 4
                Unhide(?stm:Faultcode4:prompt)
                ?stm:Faultcode4:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:4)
                    ?popcalendar:4{prop:xpos} = 300
                    Unhide(?stm:Faultcode4:2)
                    ?stm:Faultcode4:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode4)
                    If map:lookup = 'YES'
                        Unhide(?lookup:4)
                    End
                End

            Of 5
                Unhide(?stm:Faultcode5:prompt)
                ?stm:Faultcode5:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:5)
                    ?popcalendar:5{prop:xpos} = 300
                    Unhide(?stm:Faultcode5:2)
                    ?stm:Faultcode5:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode5)
                    If map:lookup = 'YES'
                        Unhide(?lookup:5)
                    End
                End

            Of 6
                Unhide(?stm:Faultcode6:prompt)
                ?stm:Faultcode6:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:6)
                    ?popcalendar:6{prop:xpos} = 300
                    Unhide(?stm:Faultcode6:2)
                    ?stm:Faultcode6:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode6)
                    If map:lookup = 'YES'
                        Unhide(?lookup:6)
                    End
                End
            Of 7
                Unhide(?stm:Faultcode7:prompt)
                ?stm:Faultcode7:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:7)
                    ?popcalendar:7{prop:xpos} = 300
                    Unhide(?stm:Faultcode7:2)
                    ?stm:Faultcode7:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode7)
                    If map:lookup = 'YES'
                        Unhide(?lookup:7)
                    End
                End

            Of 8
                Unhide(?stm:Faultcode8:prompt)
                ?stm:Faultcode8:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:8)
                    ?popcalendar:8{prop:xpos} = 300
                    Unhide(?stm:Faultcode8:2)
                    ?stm:Faultcode8:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode8)
                    If map:lookup = 'YES'
                        Unhide(?lookup:8)
                    End
                End

            Of 9
                Unhide(?stm:Faultcode9:prompt)
                ?stm:Faultcode9:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:9)
                    ?popcalendar:9{prop:xpos} = 300
                    Unhide(?stm:Faultcode9:2)
                    ?stm:Faultcode9:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode9)
                    If map:lookup = 'YES'
                        Unhide(?lookup:9)
                    End
                End

            Of 10
                Unhide(?stm:Faultcode10:prompt)
                ?stm:Faultcode10:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:10)
                    ?popcalendar:10{prop:xpos} = 152
                    Unhide(?stm:Faultcode10:2)
                    ?stm:Faultcode10:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode10)
                    If map:lookup = 'YES'
                        Unhide(?lookup:10)
                    End
                End

            Of 11
                Unhide(?stm:Faultcode11:prompt)
                ?stm:Faultcode11:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:11)
                    ?popcalendar:11{prop:xpos} = 152
                    Unhide(?stm:Faultcode11:2)
                    ?stm:Faultcode11:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode11)
                    If map:lookup = 'YES'
                        Unhide(?lookup:11)
                    End
                End

            Of 12
                Unhide(?stm:Faultcode12:prompt)
                ?stm:Faultcode12:prompt{prop:text} = map:field_name
                If map:field_type = 'DATE'
                    Unhide(?popcalendar:12)
                    ?popcalendar:12{prop:xpos} = 152
                    Unhide(?stm:Faultcode12:2)
                    ?stm:Faultcode12:2{prop:xpos} = 84
                Else
                    Unhide(?stm:Faultcode12)
                    If map:lookup = 'YES'
                        Unhide(?lookup:12)
                    End
                End

        End
    end !loop
    access:manfaupa.restorefile(save_map_id)
    setcursor()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Stock_Fault_Codes',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Stock_Fault_Codes',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Stock_Fault_Codes',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Model_Number:Prompt;  SolaceCtrlName = '?stm:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Model_Number;  SolaceCtrlName = '?stm:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode1:Prompt;  SolaceCtrlName = '?stm:FaultCode1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode1;  SolaceCtrlName = '?stm:FaultCode1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode1:2;  SolaceCtrlName = '?stm:FaultCode1:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup;  SolaceCtrlName = '?Lookup';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar;  SolaceCtrlName = '?PopCalendar';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode2:Prompt;  SolaceCtrlName = '?stm:FaultCode2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode2;  SolaceCtrlName = '?stm:FaultCode2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode2:2;  SolaceCtrlName = '?stm:FaultCode2:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:2;  SolaceCtrlName = '?Lookup:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:2;  SolaceCtrlName = '?PopCalendar:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode3:Prompt;  SolaceCtrlName = '?stm:FaultCode3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode3;  SolaceCtrlName = '?stm:FaultCode3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode3:2;  SolaceCtrlName = '?stm:FaultCode3:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:3;  SolaceCtrlName = '?Lookup:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:3;  SolaceCtrlName = '?PopCalendar:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode4:Prompt;  SolaceCtrlName = '?stm:FaultCode4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode4;  SolaceCtrlName = '?stm:FaultCode4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode4:2;  SolaceCtrlName = '?stm:FaultCode4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:4;  SolaceCtrlName = '?PopCalendar:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:4;  SolaceCtrlName = '?Lookup:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode5:Prompt;  SolaceCtrlName = '?stm:FaultCode5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode5;  SolaceCtrlName = '?stm:FaultCode5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode5:2;  SolaceCtrlName = '?stm:FaultCode5:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:5;  SolaceCtrlName = '?PopCalendar:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:5;  SolaceCtrlName = '?Lookup:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode6:Prompt;  SolaceCtrlName = '?stm:FaultCode6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode6;  SolaceCtrlName = '?stm:FaultCode6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode6:2;  SolaceCtrlName = '?stm:FaultCode6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:6;  SolaceCtrlName = '?PopCalendar:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:6;  SolaceCtrlName = '?Lookup:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode7:Prompt;  SolaceCtrlName = '?stm:FaultCode7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode7;  SolaceCtrlName = '?stm:FaultCode7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode7:2;  SolaceCtrlName = '?stm:FaultCode7:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:7;  SolaceCtrlName = '?PopCalendar:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:7;  SolaceCtrlName = '?Lookup:7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode8:Prompt;  SolaceCtrlName = '?stm:FaultCode8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode8;  SolaceCtrlName = '?stm:FaultCode8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode8:2;  SolaceCtrlName = '?stm:FaultCode8:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:8;  SolaceCtrlName = '?PopCalendar:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:8;  SolaceCtrlName = '?Lookup:8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode9:Prompt;  SolaceCtrlName = '?stm:FaultCode9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode9;  SolaceCtrlName = '?stm:FaultCode9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode9:2;  SolaceCtrlName = '?stm:FaultCode9:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:9;  SolaceCtrlName = '?PopCalendar:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:9;  SolaceCtrlName = '?Lookup:9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode10:Prompt;  SolaceCtrlName = '?stm:FaultCode10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode10;  SolaceCtrlName = '?stm:FaultCode10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode10:2;  SolaceCtrlName = '?stm:FaultCode10:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:10;  SolaceCtrlName = '?PopCalendar:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:10;  SolaceCtrlName = '?Lookup:10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode11:Prompt;  SolaceCtrlName = '?stm:FaultCode11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode11;  SolaceCtrlName = '?stm:FaultCode11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode11:2;  SolaceCtrlName = '?stm:FaultCode11:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:11;  SolaceCtrlName = '?PopCalendar:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:11;  SolaceCtrlName = '?Lookup:11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode12:Prompt;  SolaceCtrlName = '?stm:FaultCode12:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode12;  SolaceCtrlName = '?stm:FaultCode12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode12:2;  SolaceCtrlName = '?stm:FaultCode12:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PopCalendar:12;  SolaceCtrlName = '?PopCalendar:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Lookup:12;  SolaceCtrlName = '?Lookup:12';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Stock_Fault_Codes')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Stock_Fault_Codes')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?stm:Model_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:STOMODEL)
  Relate:CHARTYPE.Open
  Access:MANUFACT.UseFile
  Access:STOCK.UseFile
  Access:MANFAUPA.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOMODEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(Window)
  SELF.Opened=True
  Do RecolourWindow
  ?stm:FaultCode1{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode2{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode3{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode4{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode5{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode6{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode7{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode8{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode9{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode10{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode11{Prop:Alrt,255} = MouseLeft2
  ?stm:FaultCode12{Prop:Alrt,255} = MouseLeft2
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:CHARTYPE.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Stock_Fault_Codes',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?stm:FaultCode1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode1, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 1
      if access:manfaupa.fetch(map:field_number_key)
          If map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 1
              mfp:field        = stm:Faultcode1
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode1 = mfp:field
                  else
                      stm:Faultcode1 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode1)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          End!If map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode1, Accepted)
    OF ?Lookup
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 1
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode1 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup, Accepted)
    OF ?PopCalendar
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode1 = TINCALENDARStyle1(stm:FaultCode1)
          Display(?stm:FaultCode1)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?stm:FaultCode2
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode2, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 2
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 2
              mfp:field        = stm:Faultcode2
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode2 = mfp:field
                  else
                      stm:Faultcode2 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode2)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode2, Accepted)
    OF ?Lookup:2
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 2
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode2 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:2, Accepted)
    OF ?PopCalendar:2
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode2 = TINCALENDARStyle1(stm:FaultCode2)
          Display(?stm:FaultCode2)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?stm:FaultCode3
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode3, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 3
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 3
              mfp:field        = stm:Faultcode3
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode3 = mfp:field
                  else
                      stm:Faultcode3 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode3)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode3, Accepted)
    OF ?Lookup:3
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 3
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode3 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:3, Accepted)
    OF ?PopCalendar:3
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode3 = TINCALENDARStyle1(stm:FaultCode3)
          Display(?stm:FaultCode3)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?stm:FaultCode4
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode4, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 4
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 4
              mfp:field        = stm:Faultcode4
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode4 = mfp:field
                  else
                      stm:Faultcode4 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode4)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode4, Accepted)
    OF ?PopCalendar:4
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode4 = TINCALENDARStyle1(stm:FaultCode4)
          Display(?stm:FaultCode4)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:4
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 4
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode4 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:4, Accepted)
    OF ?stm:FaultCode5
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode5, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 5
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 5
              mfp:field        = stm:Faultcode5
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode5 = mfp:field
                  else
                      stm:Faultcode5 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode5)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode5, Accepted)
    OF ?PopCalendar:5
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode5 = TINCALENDARStyle1(stm:FaultCode5)
          Display(?stm:FaultCode5)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:5
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 5
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode5 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:5, Accepted)
    OF ?stm:FaultCode6
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode6, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 6
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 6
              mfp:field        = stm:Faultcode6
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode6 = mfp:field
                  else
                      stm:Faultcode6 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode6)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode6, Accepted)
    OF ?PopCalendar:6
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode6 = TINCALENDARStyle1(stm:FaultCode6)
          Display(?stm:FaultCode6)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:6
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 6
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode6 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:6, Accepted)
    OF ?stm:FaultCode7
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode7, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 7
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 7
              mfp:field        = stm:Faultcode7
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode7 = mfp:field
                  else
                      stm:Faultcode7 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode7)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode7, Accepted)
    OF ?PopCalendar:7
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode7 = TINCALENDARStyle1(stm:FaultCode7)
          Display(?stm:FaultCode7)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:7
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 7
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode7 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:7, Accepted)
    OF ?stm:FaultCode8
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode8, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 8
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 8
              mfp:field        = stm:Faultcode8
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode8 = mfp:field
                  else
                      stm:Faultcode8 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode8)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode8, Accepted)
    OF ?PopCalendar:8
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode8 = TINCALENDARStyle1(stm:FaultCode8)
          Display(?stm:FaultCode8)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:8
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 8
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode8 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:8, Accepted)
    OF ?stm:FaultCode9
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode9, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 9
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 9
              mfp:field        = stm:Faultcode9
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode9 = mfp:field
                  else
                      stm:Faultcode9 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode9)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode9, Accepted)
    OF ?PopCalendar:9
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode9 = TINCALENDARStyle1(stm:FaultCode9)
          Display(?stm:FaultCode9)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:9
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 9
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode9 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:9, Accepted)
    OF ?stm:FaultCode10
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode10, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 10
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 10
              mfp:field        = stm:Faultcode10
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode10 = mfp:field
                  else
                      stm:Faultcode10 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode10)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode10, Accepted)
    OF ?PopCalendar:10
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode10 = TINCALENDARStyle1(stm:FaultCode10)
          Display(?stm:FaultCode10)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:10
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 10
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode10 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:10, Accepted)
    OF ?stm:FaultCode11
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode11, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 11
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 11
              mfp:field        = stm:Faultcode11
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode11 = mfp:field
                  else
                      stm:Faultcode11 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode11)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode11, Accepted)
    OF ?PopCalendar:11
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode11 = TINCALENDARStyle1(stm:FaultCode11)
          Display(?stm:FaultCode11)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:11
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 11
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode11 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:11, Accepted)
    OF ?stm:FaultCode12
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode12, Accepted)
      access:manfaupa.clearkey(map:field_number_key)
      map:manufacturer = sto:manufacturer
      map:field_number = 12
      if access:manfaupa.fetch(map:field_number_key)
          if map:force_lookup = 'YES'
              access:manfpalo.clearkey(mfp:field_key)
              mfp:manufacturer = sto:manufacturer
              mfp:field_number = 12
              mfp:field        = stm:Faultcode12
              if access:manfpalo.fetch(mfp:field_key)
                  saverequest#      = globalrequest
                  globalresponse    = requestcancelled
                  globalrequest     = selectrecord
                  browse_manufacturer_part_lookup
                  if globalresponse = requestcompleted
                      stm:Faultcode12 = mfp:field
                  else
                      stm:Faultcode12 = ''
                      select(?-1)
                  end
                  display(?stm:Faultcode12)
                  globalrequest     = saverequest#
              end!if access:manfpalo.fetch(mfp:field_key)
          end!if map:force_lookup = 'YES'
      end!if access:manfaupa.fetch(map:field_number_key)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?stm:FaultCode12, Accepted)
    OF ?PopCalendar:12
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
          stm:FaultCode12 = TINCALENDARStyle1(stm:FaultCode12)
          Display(?stm:FaultCode12)
      !--------------------------------------------------------------------------
      ! Tinman Calendar
      !--------------------------------------------------------------------------
    OF ?Lookup:12
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
      saverequest#      = globalrequest
      globalresponse    = requestcancelled
      globalrequest     = selectrecord
      glo:select1  = STO:manufacturer
      glo:select2  = 12
      glo:select3  = ''
      browse_manufacturer_part_lookup
      if globalresponse = requestcompleted
          stm:Faultcode12 = mfp:field
          display()
      end
      glo:select1  = ''
      glo:select2  = ''
      glo:select3  = ''
      globalrequest     = saverequest#
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Lookup:12, Accepted)
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Stock_Fault_Codes')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?stm:FaultCode1
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar)
      CYCLE
    END
  OF ?stm:FaultCode2
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:2)
      CYCLE
    END
  OF ?stm:FaultCode3
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:3)
      CYCLE
    END
  OF ?stm:FaultCode4
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:4)
      CYCLE
    END
  OF ?stm:FaultCode5
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:5)
      CYCLE
    END
  OF ?stm:FaultCode6
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:6)
      CYCLE
    END
  OF ?stm:FaultCode7
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:7)
      CYCLE
    END
  OF ?stm:FaultCode8
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:8)
      CYCLE
    END
  OF ?stm:FaultCode9
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:9)
      CYCLE
    END
  OF ?stm:FaultCode10
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:10)
      CYCLE
    END
  OF ?stm:FaultCode11
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:11)
      CYCLE
    END
  OF ?stm:FaultCode12
    IF KEYCODE() = MouseLeft2 AND EVENT() = EVENT:PreAlertKey THEN
      POST(EVENT:Accepted,?PopCalendar:12)
      CYCLE
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do Fault_Coding
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

