

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01022.INC'),ONCE        !Local module procedure declarations
                     END


UpdateLOCATION PROCEDURE                              !Generated from procedure template - Window

CurrentTab           STRING(80)
Main_Store_Temp      STRING('''NO''')
LocalRequest         LONG
save_loc_ali_id      USHORT,AUTO
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW6::View:Browse    VIEW(LOCSHELF)
                       PROJECT(los:Shelf_Location)
                       PROJECT(los:Site_Location)
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
los:Shelf_Location     LIKE(los:Shelf_Location)       !List box control field - type derived from field
los:Site_Location      LIKE(los:Site_Location)        !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
History::loc:Record  LIKE(loc:RECORD),STATIC
QuickWindow          WINDOW('Update the LOCATION File'),AT(,,384,236),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('UpdateLOCATION'),TILED,SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,212,200),USE(?Sheet3),SPREAD
                         TAB('Site Location'),USE(?Tab3)
                           PROMPT('Location'),AT(12,20),USE(?LOC:Location:Prompt),TRN
                           ENTRY(@s30),AT(88,20,124,10),USE(loc:Location),FONT('Tahoma',8,,FONT:bold),REQ,UPR
                           CHECK('Main Store'),AT(88,36),USE(loc:Main_Store),VALUE('YES','NO')
                           CHECK('Active'),AT(88,52),USE(loc:Active),MSG('Active'),TIP('Active'),VALUE('1','0')
                           CHECK('Pick Notes Enabled'),AT(88,68),USE(loc:PickNoteEnable),VALUE('1','0')
                         END
                       END
                       SHEET,AT(220,4,160,200),USE(?Sheet2),SPREAD
                         TAB('Shelf Location'),USE(?Tab2)
                           ENTRY(@s30),AT(224,20,124,10),USE(los:Shelf_Location),FONT('Tahoma',8,,FONT:bold),UPR
                           LIST,AT(224,36,152,144),USE(?List),IMM,VSCROLL,MSG('Browsing Records'),VCR,FORMAT('120L(2)~Shelf Location~@s30@'),FROM(Queue:Browse)
                           BUTTON('&Insert'),AT(224,184,48,12),USE(?Insert),LEFT,ICON('Insert.ico')
                           BUTTON('&Change'),AT(276,184,48,12),USE(?Change),LEFT,ICON('Edit.ico')
                           BUTTON('&Delete'),AT(328,184,48,12),USE(?Delete),LEFT,ICON('Delete.ico')
                         END
                       END
                       BUTTON('&OK'),AT(264,212,56,16),USE(?OK),LEFT,ICON('OK.gif')
                       BUTTON('Cancel'),AT(320,212,56,16),USE(?Cancel),LEFT,ICON('Cancel.gif')
                       PANEL,AT(4,208,376,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

BRW6                 CLASS(BrowseClass)               !Browse using ?List
Q                      &Queue:Browse                  !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW6::Sort0:Locator  IncrementalLocatorClass          !Default Locator
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet3{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?LOC:Location:Prompt{prop:FontColor} = -1
    ?LOC:Location:Prompt{prop:Color} = 15066597
    If ?loc:Location{prop:ReadOnly} = True
        ?loc:Location{prop:FontColor} = 65793
        ?loc:Location{prop:Color} = 15066597
    Elsif ?loc:Location{prop:Req} = True
        ?loc:Location{prop:FontColor} = 65793
        ?loc:Location{prop:Color} = 8454143
    Else ! If ?loc:Location{prop:Req} = True
        ?loc:Location{prop:FontColor} = 65793
        ?loc:Location{prop:Color} = 16777215
    End ! If ?loc:Location{prop:Req} = True
    ?loc:Location{prop:Trn} = 0
    ?loc:Location{prop:FontStyle} = font:Bold
    ?loc:Main_Store{prop:Font,3} = -1
    ?loc:Main_Store{prop:Color} = 15066597
    ?loc:Main_Store{prop:Trn} = 0
    ?loc:Active{prop:Font,3} = -1
    ?loc:Active{prop:Color} = 15066597
    ?loc:Active{prop:Trn} = 0
    ?loc:PickNoteEnable{prop:Font,3} = -1
    ?loc:PickNoteEnable{prop:Color} = 15066597
    ?loc:PickNoteEnable{prop:Trn} = 0
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    If ?los:Shelf_Location{prop:ReadOnly} = True
        ?los:Shelf_Location{prop:FontColor} = 65793
        ?los:Shelf_Location{prop:Color} = 15066597
    Elsif ?los:Shelf_Location{prop:Req} = True
        ?los:Shelf_Location{prop:FontColor} = 65793
        ?los:Shelf_Location{prop:Color} = 8454143
    Else ! If ?los:Shelf_Location{prop:Req} = True
        ?los:Shelf_Location{prop:FontColor} = 65793
        ?los:Shelf_Location{prop:Color} = 16777215
    End ! If ?los:Shelf_Location{prop:Req} = True
    ?los:Shelf_Location{prop:Trn} = 0
    ?los:Shelf_Location{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateLOCATION',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateLOCATION',1)
    SolaceViewVars('Main_Store_Temp',Main_Store_Temp,'UpdateLOCATION',1)
    SolaceViewVars('LocalRequest',LocalRequest,'UpdateLOCATION',1)
    SolaceViewVars('save_loc_ali_id',save_loc_ali_id,'UpdateLOCATION',1)
    SolaceViewVars('FilesOpened',FilesOpened,'UpdateLOCATION',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateLOCATION',1)
    SolaceViewVars('RecordChanged',RecordChanged,'UpdateLOCATION',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet3;  SolaceCtrlName = '?Sheet3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LOC:Location:Prompt;  SolaceCtrlName = '?LOC:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loc:Location;  SolaceCtrlName = '?loc:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loc:Main_Store;  SolaceCtrlName = '?loc:Main_Store';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loc:Active;  SolaceCtrlName = '?loc:Active';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?loc:PickNoteEnable;  SolaceCtrlName = '?loc:PickNoteEnable';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?los:Shelf_Location;  SolaceCtrlName = '?los:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert;  SolaceCtrlName = '?Insert';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change;  SolaceCtrlName = '?Change';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete;  SolaceCtrlName = '?Delete';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Inserting A Site Location'
  OF ChangeRecord
    ActionMessage = 'Changing A Site Location'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('UpdateLOCATION')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateLOCATION')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?LOC:Location:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(loc:Record,History::loc:Record)
  SELF.AddHistoryField(?loc:Location,2)
  SELF.AddHistoryField(?loc:Main_Store,3)
  SELF.AddHistoryField(?loc:Active,4)
  SELF.AddHistoryField(?loc:PickNoteEnable,5)
  SELF.AddUpdateFile(Access:LOCATION)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:LOCATION.Open
  Relate:LOCATION_ALIAS.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:LOCATION
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:LOCSHELF,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?List{prop:vcr} = TRUE
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,los:Shelf_Location_Key)
  BRW6.AddRange(los:Site_Location,Relate:LOCSHELF,Relate:LOCATION)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?los:Shelf_Location,los:Shelf_Location,1,BRW6)
  BRW6.AddField(los:Shelf_Location,BRW6.Q.los:Shelf_Location)
  BRW6.AddField(los:Site_Location,BRW6.Q.los:Site_Location)
  BRW6.AskProcedure = 1
  BRW6.AddToolbarTarget(Toolbar)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:LOCATION_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateLOCATION',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 0
  If loc:location = ''
  	Case MessageEx('You have not entered a Site Location.','ServiceBase 2000',|
  	               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
  		Of 1 ! &OK Button
  	End!Case MessageEx
      do_update# = 0
  Else!If loc:location = ''
      do_update# = 1
  End!If loc:location = ''
  If do_update# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    Update_Shelf_Location
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?loc:Main_Store
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loc:Main_Store, Accepted)
      If ~0{prop:acceptall}
          If loc:main_store   <> main_store_temp
              If loc:main_store = 'YES'
                  error# = 0
                  setcursor(cursor:wait)
                  save_loc_ali_id = access:location_alias.savefile()
                  set(location_alias)
                  loop
                      if access:location_alias.next()
                         break
                      end !if
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      If loc_ali:main_store = 'YES'
                          If loc_ali:location <> loc:location
      						Case MessageEx('The Location '&clip(loc_ali:location)&' has already been marked as the Main Store.<13,10><13,10>Only ONE location can be the Main Store.','ServiceBase 2000',|
      						               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
      							Of 1 ! &OK Button
      						End!Case MessageEx
                              error# = 1
                              Break
                          End!If loc_ali:location <> loc:location
                      End!If loc_ali:main_store = 'YES'
                  end !loop
                  access:location_alias.restorefile(save_loc_ali_id)
                  setcursor()
                  If error# = 0
                      main_store_temp = loc:main_store
                  End!
                  IF error# = 1
                      loc:main_store  = main_store_temp
                  End!If error# = 0
              Else!If loc:main_store = 'YES'
                  main_store_temp = loc:main_store
              End!If loc:main_store = 'YES'
          End!If loc:main_store   <> main_store_temp
          Display(?loc:main_store)
      End!If ~0{prop:acceptall}
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?loc:Main_Store, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateLOCATION')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?los:Shelf_Location
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?los:Shelf_Location, Selected)
      Select(?List)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?los:Shelf_Location, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      !Save_Fields
      if securitycheck('PICK PART DEFAULTS') = level:benign
          enable(?loc:PickNoteEnable)
      else!if securitycheck('PICK PART DEFAULTS') = level:benign
          disable(?loc:PickNoteEnable)
      end!if securitycheck('PICK PART DEFAULTS') = level:benign
      
      main_store_temp = loc:main_store
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW6.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults


BRW6.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert
    SELF.ChangeControl=?Change
    SELF.DeleteControl=?Delete
  END


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

