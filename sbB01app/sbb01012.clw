

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01012.INC'),ONCE        !Local module procedure declarations
                     END


Browse_Suppliers PROCEDURE                            !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:Account_Number)
                       PROJECT(sup:Telephone_Number)
                       PROJECT(sup:Address_Line1)
                       PROJECT(sup:Postcode)
                       PROJECT(sup:Fax_Number)
                       PROJECT(sup:Contact_Name)
                       PROJECT(sup:RecordNumber)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:Account_Number     LIKE(sup:Account_Number)       !List box control field - type derived from field
sup:Telephone_Number   LIKE(sup:Telephone_Number)     !List box control field - type derived from field
sup:Address_Line1      LIKE(sup:Address_Line1)        !List box control field - type derived from field
sup:Postcode           LIKE(sup:Postcode)             !List box control field - type derived from field
sup:Fax_Number         LIKE(sup:Fax_Number)           !List box control field - type derived from field
sup:Contact_Name       LIKE(sup:Contact_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
QuickWindow          WINDOW('Browse the Supplier File'),AT(,,440,188),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Suppliers'),SYSTEM,GRAY,RESIZE
                       LIST,AT(8,36,340,144),USE(?Browse:1),IMM,HVSCROLL,MSG('Browsing Records'),FORMAT('124L(2)|M~Company Name~@s30@64L(2)|M~Account Number~@s15@68L(2)|M~Telephone Numb' &|
   'er~@s15@80L(2)|M~Address~@s30@44L(2)|M~Postcode~@s10@64L(2)|M~Fax Number~@s15@80' &|
   'L(2)|M~Contact Name~@s60@'),FROM(Queue:Browse:1)
                       BUTTON('&Select'),AT(360,20,76,20),USE(?Select:2),LEFT,ICON('SELECT.ICO')
                       BUTTON('&Insert'),AT(360,88,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(360,112,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO'),DEFAULT
                       BUTTON('&Delete'),AT(360,136,76,20),USE(?Delete:3),LEFT,ICON('DELETE.ICO')
                       SHEET,AT(4,4,348,180),USE(?CurrentTab),SPREAD
                         TAB('By Company Name'),USE(?Tab:2)
                           ENTRY(@s30),AT(8,20,124,10),USE(sup:Company_Name),FONT('Tahoma',8,,FONT:regular),UPR
                         END
                         TAB('By Account Number'),USE(?Tab:3)
                           ENTRY(@s15),AT(8,20,64,10),USE(sup:Account_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                       END
                       BUTTON('Close'),AT(360,164,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeSelected           PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - CHOICE(?CurrentTab) = 2
BRW1::Sort0:StepClass StepStringClass                 !Default Step Manager
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - CHOICE(?CurrentTab) = 2
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    If ?sup:Company_Name{prop:ReadOnly} = True
        ?sup:Company_Name{prop:FontColor} = 65793
        ?sup:Company_Name{prop:Color} = 15066597
    Elsif ?sup:Company_Name{prop:Req} = True
        ?sup:Company_Name{prop:FontColor} = 65793
        ?sup:Company_Name{prop:Color} = 8454143
    Else ! If ?sup:Company_Name{prop:Req} = True
        ?sup:Company_Name{prop:FontColor} = 65793
        ?sup:Company_Name{prop:Color} = 16777215
    End ! If ?sup:Company_Name{prop:Req} = True
    ?sup:Company_Name{prop:Trn} = 0
    ?sup:Company_Name{prop:FontStyle} = font:Bold
    ?Tab:3{prop:Color} = 15066597
    If ?sup:Account_Number{prop:ReadOnly} = True
        ?sup:Account_Number{prop:FontColor} = 65793
        ?sup:Account_Number{prop:Color} = 15066597
    Elsif ?sup:Account_Number{prop:Req} = True
        ?sup:Account_Number{prop:FontColor} = 65793
        ?sup:Account_Number{prop:Color} = 8454143
    Else ! If ?sup:Account_Number{prop:Req} = True
        ?sup:Account_Number{prop:FontColor} = 65793
        ?sup:Account_Number{prop:Color} = 16777215
    End ! If ?sup:Account_Number{prop:Req} = True
    ?sup:Account_Number{prop:Trn} = 0
    ?sup:Account_Number{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Suppliers',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Suppliers',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Suppliers',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Suppliers',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Select:2;  SolaceCtrlName = '?Select:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Company_Name;  SolaceCtrlName = '?sup:Company_Name';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Account_Number;  SolaceCtrlName = '?sup:Account_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Browse_Suppliers')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Suppliers')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:SUPPLIER.Open
  Relate:USERS_ALIAS.Open
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:SUPPLIER,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,sup:AccountNumberKey)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sup:Account_Number,sup:Account_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,sup:Company_Name_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sup:Company_Name,sup:Company_Name,1,BRW1)
  BRW1.AddField(sup:Company_Name,BRW1.Q.sup:Company_Name)
  BRW1.AddField(sup:Account_Number,BRW1.Q.sup:Account_Number)
  BRW1.AddField(sup:Telephone_Number,BRW1.Q.sup:Telephone_Number)
  BRW1.AddField(sup:Address_Line1,BRW1.Q.sup:Address_Line1)
  BRW1.AddField(sup:Postcode,BRW1.Q.sup:Postcode)
  BRW1.AddField(sup:Fax_Number,BRW1.Q.sup:Fax_Number)
  BRW1.AddField(sup:Contact_Name,BRW1.Q.sup:Contact_Name)
  BRW1.AddField(sup:RecordNumber,BRW1.Q.sup:RecordNumber)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:2{PROP:TEXT} = 'By Company Name'
    ?Tab:3{PROP:TEXT} = 'By Account Number'
    ?Browse:1{PROP:FORMAT} ='124L(2)|M~Company Name~@s30@#1#64L(2)|M~Account Number~@s15@#2#68L(2)|M~Telephone Number~@s15@#3#80L(2)|M~Address~@s30@#4#44L(2)|M~Postcode~@s10@#5#64L(2)|M~Fax Number~@s15@#6#80L(2)|M~Contact Name~@s60@#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:SUPPLIER.Close
    Relate:USERS_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Suppliers',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
    do_update# = true
  
    case request
        of insertrecord
            If SecurityCheck('SUPPLIERS - INSERT')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of changerecord
            If SecurityCheck('SUPPLIERS - CHANGE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
        of deleterecord
            If SecurityCheck('SUPPLIERS - DELETE')
                case messageex('You do not have access to this option.','ServiceBase 2000',|
                               'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,charset:ansi,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                    of 1 ! &ok button
                end!case messageex
                do_update# = false
            end
    end !case request
  
    if do_update# = true
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateSUPPLIER
    ReturnValue = GlobalResponse
  END
  end!if do_update# = true
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Suppliers')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='124L(2)|M~Company Name~@s30@#1#64L(2)|M~Account Number~@s15@#2#68L(2)|M~Telephone Number~@s15@#3#80L(2)|M~Address~@s30@#4#44L(2)|M~Postcode~@s10@#5#64L(2)|M~Fax Number~@s15@#6#80L(2)|M~Contact Name~@s60@#7#'
          ?Tab:2{PROP:TEXT} = 'By Company Name'
        OF 2
          ?Browse:1{PROP:FORMAT} ='64L(2)|M~Account Number~@s15@#2#124L(2)|M~Company Name~@s30@#1#68L(2)|M~Telephone Number~@s15@#3#80L(2)|M~Address~@s30@#4#44L(2)|M~Postcode~@s10@#5#64L(2)|M~Fax Number~@s15@#6#80L(2)|M~Contact Name~@s60@#7#'
          ?Tab:3{PROP:TEXT} = 'By Account Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeSelected PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE FIELD()
    OF ?sup:Company_Name
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Company_Name, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Company_Name, Selected)
    OF ?sup:Account_Number
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Account_Number, Selected)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?sup:Account_Number, Selected)
    END
  ReturnValue = PARENT.TakeSelected()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  SELF.SelectControl = ?Select:2
  SELF.HideSelect = 1
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF CHOICE(?CurrentTab) = 2
    RETURN SELF.SetSort(1,Force)
  ELSE
    RETURN SELF.SetSort(2,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

