

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBB01018.INC'),ONCE        !Local module procedure declarations
                     END





Browse_Stock PROCEDURE                                !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
CurrentTab           STRING(80)
average_temp         LONG
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
average_text_temp    STRING(30)
save_shi_id          USHORT,AUTO
tmp:LastOrdered      DATE
save_orp_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
tmp:partnumber       STRING(30)
tmp:description      STRING(30)
tmp:location         STRING(30)
LocalRequest         LONG
FilesOpened          BYTE
Location_Temp        STRING(30)
Manufacturer_Temp    STRING(30)
CPCSExecutePopUp     BYTE
accessory_temp       STRING('ALL')
shelf_location_temp  STRING(60)
no_temp              STRING('NO')
yes_temp             STRING('YES')
BrFilter1            STRING(300)
BrLocator1           STRING(30)
BrFilter2            STRING(300)
BrLocator2           STRING(30)
tmp:QtyOnOrder       LONG
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo:1 QUEUE                           !Queue declaration for browse/combo box using ?Manufacturer_Temp
man:Manufacturer       LIKE(man:Manufacturer)         !List box control field - type derived from field
man:RecordNumber       LIKE(man:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW1::View:Browse    VIEW(STOCK)
                       PROJECT(sto:Part_Number)
                       PROJECT(sto:Description)
                       PROJECT(sto:Manufacturer)
                       PROJECT(sto:Supplier)
                       PROJECT(sto:Quantity_Stock)
                       PROJECT(sto:Ref_Number)
                       PROJECT(sto:Purchase_Cost)
                       PROJECT(sto:Sale_Cost)
                       PROJECT(sto:Retail_Cost)
                       PROJECT(sto:Location)
                       PROJECT(sto:Accessory)
                       PROJECT(sto:Shelf_Location)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
sto:Part_Number        LIKE(sto:Part_Number)          !List box control field - type derived from field
sto:Description        LIKE(sto:Description)          !List box control field - type derived from field
sto:Manufacturer       LIKE(sto:Manufacturer)         !List box control field - type derived from field
shelf_location_temp    LIKE(shelf_location_temp)      !List box control field - type derived from local data
sto:Supplier           LIKE(sto:Supplier)             !List box control field - type derived from field
sto:Quantity_Stock     LIKE(sto:Quantity_Stock)       !List box control field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !List box control field - type derived from field
sto:Purchase_Cost      LIKE(sto:Purchase_Cost)        !List box control field - type derived from field
sto:Sale_Cost          LIKE(sto:Sale_Cost)            !List box control field - type derived from field
sto:Retail_Cost        LIKE(sto:Retail_Cost)          !List box control field - type derived from field
no_temp                LIKE(no_temp)                  !Browse hot field - type derived from local data
yes_temp               LIKE(yes_temp)                 !Browse hot field - type derived from local data
days_7_temp            LIKE(days_7_temp)              !Browse hot field - type derived from local data
days_30_temp           LIKE(days_30_temp)             !Browse hot field - type derived from local data
days_60_temp           LIKE(days_60_temp)             !Browse hot field - type derived from local data
days_90_temp           LIKE(days_90_temp)             !Browse hot field - type derived from local data
average_text_temp      LIKE(average_text_temp)        !Browse hot field - type derived from local data
sto:Location           LIKE(sto:Location)             !Browse key field - type derived from field
sto:Accessory          LIKE(sto:Accessory)            !Browse key field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !Browse key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
! ---------------------------------------- Higher Keys --------------------------------------- !
HK8::sto:Accessory        LIKE(sto:Accessory)
HK8::sto:Location         LIKE(sto:Location)
HK8::sto:Manufacturer     LIKE(sto:Manufacturer)
! ---------------------------------------- Higher Keys --------------------------------------- !
FDCB26::View:FileDropCombo VIEW(MANUFACT)
                       PROJECT(man:Manufacturer)
                       PROJECT(man:RecordNumber)
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask1          DECIMAL(10,0,0)                  !Xplore
XploreMask11          DECIMAL(10,0,48)                !Xplore
XploreTitle1         STRING(' ')                      !Xplore
xpInitialTab1        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the Stock File'),AT(,,648,334),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Stock'),SYSTEM,GRAY,RESIZE
                       STRING('Add Stock'),AT(28,308),USE(?String1),TRN
                       OPTION('Part Type'),AT(231,4,189,19),USE(accessory_temp),BOXED,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                         RADIO('Non-Accessory'),AT(240,11),USE(?accessory_temp:Radio1),TRN,VALUE('NO')
                         RADIO('Accessory'),AT(316,11),USE(?accessory_temp:Radio2),VALUE('YES')
                         RADIO('All'),AT(384,11),USE(?accessory_temp:Radio3),VALUE('ALL')
                       END
                       LIST,AT(8,64,540,236),USE(?Browse:1),IMM,MSG('Browsing Records'),ALRT(HomeKey),FORMAT('86L(2)|M~Part Number~@s30@86L(2)|M~Description~@s30@86L(2)|M~Manufacturer~@s30@1' &|
   '32L(2)|M~Shelf Location~@s60@97L(2)|M~Supplier~@s30@/48R(2)|M~Qty In Stock~L@N8@' &|
   '48L(2)|M~Reference Number~@n012@/56L(2)|M~Purchase Cost~@n14.2@/56L(2)|M~Sale Co' &|
   'st~@n14.2@/56L(2)|M~Retail Cost~@n14.2@/'),FROM(Queue:Browse:1)
                       BUTTON('Browse Model Stock'),AT(564,168,76,20),USE(?browse_model_stock),LEFT,ICON('stock_br.gif')
                       BUTTON('Rebuild Model Stock'),AT(564,144,76,20),USE(?rebuild_model_stock),LEFT,ICON('Gearwork.ico')
                       STRING('Deplete Stock'),AT(148,308),USE(?String2),TRN
                       STRING('Stock History'),AT(264,308),USE(?String3),TRN
                       STRING('Order Stock'),AT(384,308),USE(?String4),TRN
                       STRING('Stock Transfer'),AT(499,308),USE(?String5),TRN
                       BUTTON('&Insert'),AT(564,236,76,20),USE(?Insert:3),LEFT,ICON('INSERT.ICO')
                       BUTTON('&Change'),AT(564,260,76,20),USE(?Change:3),LEFT,ICON('EDIT.ICO'),DEFAULT
                       BUTTON('&Delete'),AT(564,284,76,20),USE(?Delete:3),LEFT,ICON('DELETE.ICO')
                       SHEET,AT(4,32,552,300),USE(?CurrentTab),SPREAD
                         TAB('By &Part Number'),USE(?Tab:3)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Part_Number),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Desc&ription'),USE(?Tab:4)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Description),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By S&helf Location'),USE(?Tab:5)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Shelf_Location),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By Su&pplier'),USE(?Tab:6)
                           ENTRY(@s30),AT(8,48,124,10),USE(sto:Supplier),FONT('Tahoma',8,,FONT:bold),UPR
                         END
                         TAB('By &Manufacturer'),USE(?Tab5)
                           ENTRY(@s30),AT(136,48,124,10),USE(sto:Part_Number,,?STO:Part_Number:2),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Part Number'),AT(268,48),USE(?Prompt2),FONT(,,COLOR:White,)
                           COMBO(@s30),AT(8,48,124,10),USE(Manufacturer_Temp),VSCROLL,FONT('Tahoma',8,,FONT:bold),UPR,FORMAT('120L(2)@s30@'),DROP(10),FROM(Queue:FileDropCombo:1)
                         END
                       END
                       SHEET,AT(560,4,84,84),USE(?Sheet2),COLOR(COLOR:Gray),WIZARD,SPREAD
                         TAB('Tab 6'),USE(?Tab6)
                           PROMPT('Qty On Order'),AT(564,8),USE(?Prompt4),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           STRING(@s4),AT(616,8),USE(tmp:QtyOnOrder),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('(All Locations)'),AT(564,16),USE(?SummaryOrdersPrompt),HIDE,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Last Order'),AT(564,24),USE(?Prompt4:2),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           STRING(@d6b),AT(580,24),USE(tmp:LastOrdered),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Costs'),AT(564,34),USE(?Prompt9),LEFT,FONT(,,COLOR:White,FONT:bold+FONT:underline)
                           STRING(@n9.2),AT(592,44),USE(sto:Purchase_Cost),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                           STRING(@n9.2),AT(592,56),USE(sto:Sale_Cost),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                           STRING(@n9.2),AT(592,68),USE(sto:Retail_Cost),TRN,RIGHT,FONT(,,COLOR:White,FONT:bold,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Retail Cost'),AT(564,68),USE(?Prompt4:5),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Purch Cost:'),AT(564,44),USE(?Prompt4:3),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                           PROMPT('Trade Cost:'),AT(564,56),USE(?Prompt4:4),TRN,FONT(,,COLOR:White,,CHARSET:ANSI),COLOR(COLOR:Gray)
                         END
                       END
                       PROMPT('Site Location'),AT(8,12),USE(?Location_Temp:Prompt),FONT(,,COLOR:White,,CHARSET:ANSI)
                       ENTRY(@s30),AT(84,12,124,10),USE(Location_Temp),FONT(,,,FONT:bold),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseLeft2),ALRT(MouseRight),UPR
                       BUTTON,AT(212,12,10,10),USE(?LookupSiteLocations),SKIP,FLAT,ICON('List3.ico')
                       BUTTON('Close'),AT(564,312,76,20),USE(?Close),LEFT,ICON('CANCEL.ICO')
                       PANEL,AT(4,4,552,24),USE(?Panel1),FILL(COLOR:Gray)
                       GROUP,AT(8,300,504,24),USE(?Button_Group)
                         BUTTON,AT(8,304,16,16),USE(?Add_Stock),LEFT,ICON('insert.ico')
                         BUTTON,AT(126,304,16,16),USE(?Deplete_Stock),LEFT,ICON('Delete.ico')
                         BUTTON,AT(244,304,16,16),USE(?Stock_History),LEFT,ICON('book.ico')
                         BUTTON,AT(362,304,16,16),USE(?Order_Stock),LEFT,ICON('phone.ico')
                         BUTTON,AT(480,304,16,16),USE(?Stock_Transfer),LEFT,ICON('arrow.ico')
                       END
                     END

! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto

progress:thermometer byte
progresswindow window('Progress...'),at(,,164,64),font('Tahoma',8,,font:regular),center,timer(1),gray, |
         double
       progress,use(progress:thermometer),at(25,15,111,12),range(0,100)
       string(''),at(0,3,161,10),use(?progress:userstring),center,font('Tahoma',8,,)
       string(''),at(0,30,161,10),use(?progress:pcttext),trn,center,font('Tahoma',8,,)
     end
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse:1                !Reference to browse queue
ApplyRange             PROCEDURE(),BYTE,PROC,DERIVED
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetQueue             PROCEDURE(BYTE ResetMode),DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  EntryLocatorClass                !Default Locator
BRW1::Sort5:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
BRW1::Sort2:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
BRW1::Sort10:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
BRW1::Sort11:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
BRW1::Sort3:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
BRW1::Sort12:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
BRW1::Sort13:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
BRW1::Sort4:Locator  EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
BRW1::Sort14:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
BRW1::Sort15:Locator EntryLocatorClass                !Conditional Locator - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
BRW1::Sort0:StepClass StepClass                       !Default Step Manager
BRW1::Sort5:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
BRW1::Sort6:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
BRW1::Sort7:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
BRW1::Sort1:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
BRW1::Sort8:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
BRW1::Sort9:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
BRW1::Sort2:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
BRW1::Sort10:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
BRW1::Sort11:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
BRW1::Sort3:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
BRW1::Sort12:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
BRW1::Sort13:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
BRW1::Sort4:StepClass StepStringClass                 !Conditional Step Manager - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
BRW1::Sort14:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
BRW1::Sort15:StepClass StepStringClass                !Conditional Step Manager - Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

FDCB26               CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo:1         !Reference to browse queue type
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_stm_id   ushort,auto
!Save Entry Fields Incase Of Lookup
look:Location_Temp                Like(Location_Temp)
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore1              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?String1{prop:FontColor} = -1
    ?String1{prop:Color} = 15066597
    ?accessory_temp{prop:Font,3} = -1
    ?accessory_temp{prop:Color} = 15066597
    ?accessory_temp{prop:Trn} = 0
    ?accessory_temp:Radio1{prop:Font,3} = -1
    ?accessory_temp:Radio1{prop:Color} = 15066597
    ?accessory_temp:Radio1{prop:Trn} = 0
    ?accessory_temp:Radio2{prop:Font,3} = -1
    ?accessory_temp:Radio2{prop:Color} = 15066597
    ?accessory_temp:Radio2{prop:Trn} = 0
    ?accessory_temp:Radio3{prop:Font,3} = -1
    ?accessory_temp:Radio3{prop:Color} = 15066597
    ?accessory_temp:Radio3{prop:Trn} = 0
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?String2{prop:FontColor} = -1
    ?String2{prop:Color} = 15066597
    ?String3{prop:FontColor} = -1
    ?String3{prop:Color} = 15066597
    ?String4{prop:FontColor} = -1
    ?String4{prop:Color} = 15066597
    ?String5{prop:FontColor} = -1
    ?String5{prop:Color} = 15066597
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:3{prop:Color} = 15066597
    If ?sto:Part_Number{prop:ReadOnly} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 15066597
    Elsif ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 8454143
    Else ! If ?sto:Part_Number{prop:Req} = True
        ?sto:Part_Number{prop:FontColor} = 65793
        ?sto:Part_Number{prop:Color} = 16777215
    End ! If ?sto:Part_Number{prop:Req} = True
    ?sto:Part_Number{prop:Trn} = 0
    ?sto:Part_Number{prop:FontStyle} = font:Bold
    ?Tab:4{prop:Color} = 15066597
    If ?sto:Description{prop:ReadOnly} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 15066597
    Elsif ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 8454143
    Else ! If ?sto:Description{prop:Req} = True
        ?sto:Description{prop:FontColor} = 65793
        ?sto:Description{prop:Color} = 16777215
    End ! If ?sto:Description{prop:Req} = True
    ?sto:Description{prop:Trn} = 0
    ?sto:Description{prop:FontStyle} = font:Bold
    ?Tab:5{prop:Color} = 15066597
    If ?sto:Shelf_Location{prop:ReadOnly} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 15066597
    Elsif ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 8454143
    Else ! If ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 16777215
    End ! If ?sto:Shelf_Location{prop:Req} = True
    ?sto:Shelf_Location{prop:Trn} = 0
    ?sto:Shelf_Location{prop:FontStyle} = font:Bold
    ?Tab:6{prop:Color} = 15066597
    If ?sto:Supplier{prop:ReadOnly} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 15066597
    Elsif ?sto:Supplier{prop:Req} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 8454143
    Else ! If ?sto:Supplier{prop:Req} = True
        ?sto:Supplier{prop:FontColor} = 65793
        ?sto:Supplier{prop:Color} = 16777215
    End ! If ?sto:Supplier{prop:Req} = True
    ?sto:Supplier{prop:Trn} = 0
    ?sto:Supplier{prop:FontStyle} = font:Bold
    ?Tab5{prop:Color} = 15066597
    If ?STO:Part_Number:2{prop:ReadOnly} = True
        ?STO:Part_Number:2{prop:FontColor} = 65793
        ?STO:Part_Number:2{prop:Color} = 15066597
    Elsif ?STO:Part_Number:2{prop:Req} = True
        ?STO:Part_Number:2{prop:FontColor} = 65793
        ?STO:Part_Number:2{prop:Color} = 8454143
    Else ! If ?STO:Part_Number:2{prop:Req} = True
        ?STO:Part_Number:2{prop:FontColor} = 65793
        ?STO:Part_Number:2{prop:Color} = 16777215
    End ! If ?STO:Part_Number:2{prop:Req} = True
    ?STO:Part_Number:2{prop:Trn} = 0
    ?STO:Part_Number:2{prop:FontStyle} = font:Bold
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    If ?Manufacturer_Temp{prop:ReadOnly} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 15066597
    Elsif ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 8454143
    Else ! If ?Manufacturer_Temp{prop:Req} = True
        ?Manufacturer_Temp{prop:FontColor} = 65793
        ?Manufacturer_Temp{prop:Color} = 16777215
    End ! If ?Manufacturer_Temp{prop:Req} = True
    ?Manufacturer_Temp{prop:Trn} = 0
    ?Manufacturer_Temp{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab6{prop:Color} = 15066597
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597
    ?tmp:QtyOnOrder{prop:FontColor} = -1
    ?tmp:QtyOnOrder{prop:Color} = 15066597
    ?SummaryOrdersPrompt{prop:FontColor} = -1
    ?SummaryOrdersPrompt{prop:Color} = 15066597
    ?Prompt4:2{prop:FontColor} = -1
    ?Prompt4:2{prop:Color} = 15066597
    ?tmp:LastOrdered{prop:FontColor} = -1
    ?tmp:LastOrdered{prop:Color} = 15066597
    ?Prompt9{prop:FontColor} = -1
    ?Prompt9{prop:Color} = 15066597
    ?sto:Purchase_Cost{prop:FontColor} = -1
    ?sto:Purchase_Cost{prop:Color} = 15066597
    ?sto:Sale_Cost{prop:FontColor} = -1
    ?sto:Sale_Cost{prop:Color} = 15066597
    ?sto:Retail_Cost{prop:FontColor} = -1
    ?sto:Retail_Cost{prop:Color} = 15066597
    ?Prompt4:5{prop:FontColor} = -1
    ?Prompt4:5{prop:Color} = 15066597
    ?Prompt4:3{prop:FontColor} = -1
    ?Prompt4:3{prop:Color} = 15066597
    ?Prompt4:4{prop:FontColor} = -1
    ?Prompt4:4{prop:Color} = 15066597
    ?Location_Temp:Prompt{prop:FontColor} = -1
    ?Location_Temp:Prompt{prop:Color} = 15066597
    If ?Location_Temp{prop:ReadOnly} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 15066597
    Elsif ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 8454143
    Else ! If ?Location_Temp{prop:Req} = True
        ?Location_Temp{prop:FontColor} = 65793
        ?Location_Temp{prop:Color} = 16777215
    End ! If ?Location_Temp{prop:Req} = True
    ?Location_Temp{prop:Trn} = 0
    ?Location_Temp{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597

    ?Button_Group{prop:Font,3} = -1
    ?Button_Group{prop:Color} = 15066597
    ?Button_Group{prop:Trn} = 0

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
getnextrecord2      routine
  recordsprocessed += 1
  recordsthiscycle += 1
  if percentprogress < 100
    percentprogress = (recordsprocessed / recordstoprocess)*100
    if percentprogress > 100
      percentprogress = 100
    end
    if percentprogress <> progress:thermometer then
      progress:thermometer = percentprogress
      ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      display()
    end
  end
endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()
Refresh_Browse      Routine
!    thiswindow.reset(1)
!    Case Choice(?CurrentTab)
!        Of 5
!            If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                sto:accessory   = Upper(accessory_temp)
!                brw1.addrange(sto:manufacturer,manufacturer_temp)
!                brw1.applyrange 
!            Else!If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                sto:accessory   = Upper(accessory_temp)
!                brw1.addrange(sto:manufacturer,manufacturer_temp)
!                brw1.applyrange 
!            End!If accessory_temp <> 'ALL'
!        Else
!            If accessory_temp = 'ALL'
!                brw1.addrange(sto:location,location_temp)
!                brw1.applyrange 
!            Else!If accessory_temp = 'ALL'
!                sto:location    = Upper(location_temp)
!                brw1.addrange(sto:accessory,accessory_temp)
!                brw1.applyrange 
!            End!If accessory_temp <> 'ALL'
!    End!Case Choice(?CurrentTab)
!    thiswindow.reset(1)
!    Brw1.resetsort(1)



SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Stock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Stock',1)
    SolaceViewVars('average_temp',average_temp,'Browse_Stock',1)
    SolaceViewVars('days_7_temp',days_7_temp,'Browse_Stock',1)
    SolaceViewVars('days_30_temp',days_30_temp,'Browse_Stock',1)
    SolaceViewVars('days_60_temp',days_60_temp,'Browse_Stock',1)
    SolaceViewVars('days_90_temp',days_90_temp,'Browse_Stock',1)
    SolaceViewVars('average_text_temp',average_text_temp,'Browse_Stock',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Browse_Stock',1)
    SolaceViewVars('tmp:LastOrdered',tmp:LastOrdered,'Browse_Stock',1)
    SolaceViewVars('save_orp_id',save_orp_id,'Browse_Stock',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Browse_Stock',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Browse_Stock',1)
    SolaceViewVars('tmp:partnumber',tmp:partnumber,'Browse_Stock',1)
    SolaceViewVars('tmp:description',tmp:description,'Browse_Stock',1)
    SolaceViewVars('tmp:location',tmp:location,'Browse_Stock',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Stock',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Stock',1)
    SolaceViewVars('Location_Temp',Location_Temp,'Browse_Stock',1)
    SolaceViewVars('Manufacturer_Temp',Manufacturer_Temp,'Browse_Stock',1)
    SolaceViewVars('CPCSExecutePopUp',CPCSExecutePopUp,'Browse_Stock',1)
    SolaceViewVars('accessory_temp',accessory_temp,'Browse_Stock',1)
    SolaceViewVars('shelf_location_temp',shelf_location_temp,'Browse_Stock',1)
    SolaceViewVars('no_temp',no_temp,'Browse_Stock',1)
    SolaceViewVars('yes_temp',yes_temp,'Browse_Stock',1)
    SolaceViewVars('BrFilter1',BrFilter1,'Browse_Stock',1)
    SolaceViewVars('BrLocator1',BrLocator1,'Browse_Stock',1)
    SolaceViewVars('BrFilter2',BrFilter2,'Browse_Stock',1)
    SolaceViewVars('BrLocator2',BrLocator2,'Browse_Stock',1)
    SolaceViewVars('tmp:QtyOnOrder',tmp:QtyOnOrder,'Browse_Stock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?String1;  SolaceCtrlName = '?String1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp;  SolaceCtrlName = '?accessory_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio1;  SolaceCtrlName = '?accessory_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio2;  SolaceCtrlName = '?accessory_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?accessory_temp:Radio3;  SolaceCtrlName = '?accessory_temp:Radio3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?browse_model_stock;  SolaceCtrlName = '?browse_model_stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?rebuild_model_stock;  SolaceCtrlName = '?rebuild_model_stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String2;  SolaceCtrlName = '?String2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String3;  SolaceCtrlName = '?String3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String4;  SolaceCtrlName = '?String4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?String5;  SolaceCtrlName = '?String5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Insert:3;  SolaceCtrlName = '?Insert:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Change:3;  SolaceCtrlName = '?Change:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:3;  SolaceCtrlName = '?Tab:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Part_Number;  SolaceCtrlName = '?sto:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:4;  SolaceCtrlName = '?Tab:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Description;  SolaceCtrlName = '?sto:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:5;  SolaceCtrlName = '?Tab:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location;  SolaceCtrlName = '?sto:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:6;  SolaceCtrlName = '?Tab:6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Supplier;  SolaceCtrlName = '?sto:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab5;  SolaceCtrlName = '?Tab5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Part_Number:2;  SolaceCtrlName = '?STO:Part_Number:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Manufacturer_Temp;  SolaceCtrlName = '?Manufacturer_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab6;  SolaceCtrlName = '?Tab6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:QtyOnOrder;  SolaceCtrlName = '?tmp:QtyOnOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SummaryOrdersPrompt;  SolaceCtrlName = '?SummaryOrdersPrompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:2;  SolaceCtrlName = '?Prompt4:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:LastOrdered;  SolaceCtrlName = '?tmp:LastOrdered';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt9;  SolaceCtrlName = '?Prompt9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Purchase_Cost;  SolaceCtrlName = '?sto:Purchase_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Sale_Cost;  SolaceCtrlName = '?sto:Sale_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Retail_Cost;  SolaceCtrlName = '?sto:Retail_Cost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:5;  SolaceCtrlName = '?Prompt4:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:3;  SolaceCtrlName = '?Prompt4:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4:4;  SolaceCtrlName = '?Prompt4:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp:Prompt;  SolaceCtrlName = '?Location_Temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Location_Temp;  SolaceCtrlName = '?Location_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSiteLocations;  SolaceCtrlName = '?LookupSiteLocations';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Button_Group;  SolaceCtrlName = '?Button_Group';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Add_Stock;  SolaceCtrlName = '?Add_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Deplete_Stock;  SolaceCtrlName = '?Deplete_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_History;  SolaceCtrlName = '?Stock_History';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Order_Stock;  SolaceCtrlName = '?Order_Stock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Stock_Transfer;  SolaceCtrlName = '?Stock_Transfer';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore1.GetBbSize('Browse_Stock','?Browse:1')       !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Stock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Stock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?String1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:DEFSTOCK.Open
  Relate:LOCATION.Open
  Relate:USERS_ALIAS.Open
  Access:ORDERS.UseFile
  Access:ORDPARTS.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:STOCK,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  ! Start Change 3148 BE(26/09/03)
  SET(DEFAULTS)
  access:DEFAULTS.next()
  IF (def:SummaryOrders = 1) THEN
      UNHIDE(?SummaryOrdersPrompt)
  END
  ! End Change 3148 BE(26/09/03)
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore1.Init(ThisWindow,BRW1,Queue:Browse:1,QuickWindow,?Browse:1,'sbb01app.INI','>Header',0,BRW1.ViewOrder,Xplore1.RestoreHeader,BRW1.SequenceNbr,XploreMask1,XploreMask11,XploreTitle1,BRW1.FileSeqOn)
  ! support for CPCS
  IF ?Location_Temp{Prop:Tip} AND ~?LookupSiteLocations{Prop:Tip}
     ?LookupSiteLocations{Prop:Tip} = 'Select ' & ?Location_Temp{Prop:Tip}
  END
  IF ?Location_Temp{Prop:Msg} AND ~?LookupSiteLocations{Prop:Msg}
     ?LookupSiteLocations{Prop:Msg} = 'Select ' & ?Location_Temp{Prop:Msg}
  END
  BRW1.Q &= Queue:Browse:1
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort5:Locator)
  BRW1::Sort5:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort6:Locator)
  BRW1::Sort6:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Part_Number_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort7:Locator)
  BRW1::Sort7:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort8:Locator)
  BRW1::Sort8:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Description_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort9:Locator)
  BRW1::Sort9:Locator.Init(?sto:Description,sto:Description,1,BRW1)
  BRW1.AddSortOrder(,sto:Shelf_Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?sto:Shelf_Location,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Shelf_Location_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort10:Locator)
  BRW1::Sort10:Locator.Init(?sto:Shelf_Location,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Shelf_Location_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort11:Locator)
  BRW1::Sort11:Locator.Init(?sto:Shelf_Location,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Supplier_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort3:Locator)
  BRW1::Sort3:Locator.Init(?sto:Supplier,sto:Supplier,1,BRW1)
  BRW1.AddSortOrder(,sto:Supplier_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort12:Locator)
  BRW1::Sort12:Locator.Init(?sto:Supplier,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Supplier_Accessory_Key)
  BRW1.AddRange(sto:Accessory)
  BRW1.AddLocator(BRW1::Sort13:Locator)
  BRW1::Sort13:Locator.Init(?sto:Supplier,sto:Shelf_Location,1,BRW1)
  BRW1.AddSortOrder(,sto:Location_Manufacturer_Key)
  BRW1.AddRange(sto:Manufacturer)
  BRW1.AddLocator(BRW1::Sort4:Locator)
  BRW1::Sort4:Locator.Init(?STO:Part_Number:2,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Manufacturer_Accessory_Key)
  BRW1.AddRange(sto:Manufacturer)
  BRW1.AddLocator(BRW1::Sort14:Locator)
  BRW1::Sort14:Locator.Init(?STO:Part_Number:2,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Manufacturer_Accessory_Key)
  BRW1.AddRange(sto:Manufacturer)
  BRW1.AddLocator(BRW1::Sort15:Locator)
  BRW1::Sort15:Locator.Init(?STO:Part_Number:2,sto:Part_Number,1,BRW1)
  BRW1.AddSortOrder(,sto:Location_Key)
  BRW1.AddRange(sto:Location)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?sto:Part_Number,sto:Part_Number,1,BRW1)
  BIND('shelf_location_temp',shelf_location_temp)
  BIND('no_temp',no_temp)
  BIND('yes_temp',yes_temp)
  BIND('days_7_temp',days_7_temp)
  BIND('days_30_temp',days_30_temp)
  BIND('days_60_temp',days_60_temp)
  BIND('days_90_temp',days_90_temp)
  BIND('average_text_temp',average_text_temp)
  BIND('Location_Temp',Location_Temp)
  BIND('Manufacturer_Temp',Manufacturer_Temp)
  BIND('accessory_temp',accessory_temp)
  BIND('tmp:QtyOnOrder',tmp:QtyOnOrder)
  BRW1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  BRW1.AddField(sto:Description,BRW1.Q.sto:Description)
  BRW1.AddField(sto:Manufacturer,BRW1.Q.sto:Manufacturer)
  BRW1.AddField(shelf_location_temp,BRW1.Q.shelf_location_temp)
  BRW1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  BRW1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  BRW1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  BRW1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  BRW1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  BRW1.AddField(sto:Retail_Cost,BRW1.Q.sto:Retail_Cost)
  BRW1.AddField(no_temp,BRW1.Q.no_temp)
  BRW1.AddField(yes_temp,BRW1.Q.yes_temp)
  BRW1.AddField(days_7_temp,BRW1.Q.days_7_temp)
  BRW1.AddField(days_30_temp,BRW1.Q.days_30_temp)
  BRW1.AddField(days_60_temp,BRW1.Q.days_60_temp)
  BRW1.AddField(days_90_temp,BRW1.Q.days_90_temp)
  BRW1.AddField(average_text_temp,BRW1.Q.average_text_temp)
  BRW1.AddField(sto:Location,BRW1.Q.sto:Location)
  BRW1.AddField(sto:Accessory,BRW1.Q.sto:Accessory)
  BRW1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  BRW1.AskProcedure = 2
  FDCB26.Init(Manufacturer_Temp,?Manufacturer_Temp,Queue:FileDropCombo:1.ViewPosition,FDCB26::View:FileDropCombo,Queue:FileDropCombo:1,Relate:MANUFACT,ThisWindow,GlobalErrors,0,1,0)
  FDCB26.Q &= Queue:FileDropCombo:1
  FDCB26.AddSortOrder(man:Manufacturer_Key)
  FDCB26.AddField(man:Manufacturer,FDCB26.Q.man:Manufacturer)
  FDCB26.AddField(man:RecordNumber,FDCB26.Q.man:RecordNumber)
  FDCB26.AddUpdateField(man:Manufacturer,Manufacturer_Temp)
  ThisWindow.AddItem(FDCB26.WindowComponent)
  FDCB26.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab:3{PROP:TEXT} = 'By &Part Number'
    ?Tab:4{PROP:TEXT} = 'By Desc&ription'
    ?Tab:5{PROP:TEXT} = 'By S&helf Location'
    ?Tab:6{PROP:TEXT} = 'By Su&pplier'
    ?Tab5{PROP:TEXT} = 'By &Manufacturer'
    ?Browse:1{PROP:FORMAT} ='86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#48L(2)|M~Reference Number~@n012@/#7#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:DEFSTOCK.Close
    Relate:LOCATION.Close
    Relate:USERS_ALIAS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore1.sq.Col = Xplore1.CurrentCol
    GET(Xplore1.sq,Xplore1.sq.Col)
    IF Xplore1.ListType = 1 AND BRW1.ViewOrder = False !Xplore
      BRW1.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore1.PutInix('Browse_Stock','?Browse:1',BRW1.SequenceNbr,Xplore1.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore1.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Stock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
      Include('StockUpd.inc','BeforeUpdate')
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    EXECUTE Number
      PickSiteLocations
      UpdateSTOCK
    END
    ReturnValue = GlobalResponse
  END
      Include('StockUpd.inc','AfterUpdate')
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore1.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore1.AddField(sto:Part_Number,BRW1.Q.sto:Part_Number)
  Xplore1.AddField(sto:Description,BRW1.Q.sto:Description)
  Xplore1.AddField(sto:Manufacturer,BRW1.Q.sto:Manufacturer)
  Xplore1.AddField(sto:Supplier,BRW1.Q.sto:Supplier)
  Xplore1.AddField(sto:Quantity_Stock,BRW1.Q.sto:Quantity_Stock)
  Xplore1.AddField(sto:Ref_Number,BRW1.Q.sto:Ref_Number)
  Xplore1.AddField(sto:Purchase_Cost,BRW1.Q.sto:Purchase_Cost)
  Xplore1.AddField(sto:Sale_Cost,BRW1.Q.sto:Sale_Cost)
  Xplore1.AddField(sto:Retail_Cost,BRW1.Q.sto:Retail_Cost)
  Xplore1.AddField(no_temp,BRW1.Q.no_temp)
  Xplore1.AddField(yes_temp,BRW1.Q.yes_temp)
  Xplore1.AddField(days_7_temp,BRW1.Q.days_7_temp)
  Xplore1.AddField(days_30_temp,BRW1.Q.days_30_temp)
  Xplore1.AddField(days_60_temp,BRW1.Q.days_60_temp)
  Xplore1.AddField(days_90_temp,BRW1.Q.days_90_temp)
  Xplore1.AddField(average_text_temp,BRW1.Q.average_text_temp)
  Xplore1.AddField(sto:Location,BRW1.Q.sto:Location)
  Xplore1.AddField(sto:Accessory,BRW1.Q.sto:Accessory)
  Xplore1.AddField(sto:Shelf_Location,BRW1.Q.sto:Shelf_Location)
  BRW1.FileOrderNbr = BRW1.AddSortOrder(,sto:Location_Key) !Xplore Sort Order for File Sequence
  BRW1.AddRange(sto:Location)                         !Xplore
  BRW1.SetOrder('')                                   !Xplore
  BRW1.ViewOrder = True                               !Xplore
  Xplore1.AddAllColumnSortOrders(1)                   !Xplore
  BRW1.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Add_Stock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
      If SecurityCheck('STOCK - ADD STOCK')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case MessageEx('Cannot Add Stock. This part has been marked as a Sundry Item.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Add_Stock(brw1.q.sto:ref_number)
          End!If error# = 0
      
      end!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
    OF ?Deplete_Stock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
      If SecurityCheck('STOCK - DEPLETE STOCK')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case MessageEx('Cannot Deplete Stock! This item has been marked as a Sundry Item.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              Deplete_Stock(brw1.q.sto:ref_number)
          End!If error# = 0
      
      End!!if x" = false
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
    OF ?Order_Stock
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
      If SecurityCheck('STOCK - ORDER STOCK')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
          Thiswindow.reset
          error# = 0
          access:stock.clearkey(sto:ref_number_key)
          sto:ref_number = brw1.q.sto:ref_number
          if access:stock.fetch(sto:ref_number_key)
              error# = 1
          Else!if access:stock.fetch(sto:ref_number_key)
              If sto:sundry_item = 'YES'
                  Case MessageEx('Cannot Order! This item has been marked as a Sundry Item.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
                  error# = 1
              Else
                  If sto:Suspend
                      Case MessageEx('This part cannot be Ordered. It has been suspended.','ServiceBase 2000',|
                                     'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                          Of 1 ! &OK Button
                      End!Case MessageEx
                      error# = 1
                  End !If sto:Suspend
              End!If sto:sundry_item = 'YES'
          end!if access:stock.fetch(sto:ref_number_key)
      
          If error# = 0
              glo:select1 = brw1.q.sto:ref_number
              Stock_Order
              glo:select1 = ''
          End!If error# = 0
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
    OF ?Stock_Transfer
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
      If SecurityCheck('STOCK - STOCK TRANSFER')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Thiswindow.reset
          glo:select1 = location_temp
          glo:select2 = brw1.q.STO:Ref_Number
          Transfer_stock
          glo:select1 = ''
          glo:select2 = ''
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?accessory_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
      Do refresh_browse
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?accessory_temp, Accepted)
    OF ?browse_model_stock
      ThisWindow.Update
      START(Browse_Stock_By_Model, 25000)
      ThisWindow.Reset
    OF ?rebuild_model_stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rebuild_model_stock, Accepted)
      If SecurityCheck('STOCK - REBUILD STOCK')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000','Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0)
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
      
          Case MessageEx('This is a maintenance facility. <13,10><13,10>It is used for when a Stock Item has been removed from Stock Control, but may still be found when selecting a part for a job.<13,10><13,10>(Only run this if the above problem has occured as this routine will take a LONG TIME!)','ServiceBase 2000','Styles\warn.ico','|&OK|&Cancel',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemexclamation,msgex:samewidths,84,26,1200)
              Of 1 ! &OK Button
                  recordspercycle     = 25
                  recordsprocessed    = 0
                  percentprogress     = 0
                  setcursor(cursor:wait)
                  open(progresswindow)
                  progress:thermometer    = 0
                  ?progress:pcttext{prop:text} = '0% Completed'
                  recordstoprocess    = Records(stomodel)
                  save_stm_id = access:stomodel.savefile()
                  set(stm:model_number_key)
                  loop
                      if access:stomodel.next()
                         break
                      end !if
                      Do getNextRecord2
                      yldcnt# += 1
                      if yldcnt# > 25
                         yield() ; yldcnt# = 0
                      end !if
                      access:stock.clearkey(sto:ref_part_description_key)
                      sto:location    = stm:location
                      sto:ref_number  = stm:ref_number
                      sto:part_number = stm:part_number
                      sto:description = stm:description
                      if access:stock.fetch(sto:ref_part_description_key)
                         Delete(stomodel)
                      end
                  end !loop
                  access:stomodel.restorefile(save_stm_id)
                  setcursor()
      
                  close(progresswindow)
              Of 2 ! &Cancel Button
          End!Case MessageEx
      
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?rebuild_model_stock, Accepted)
    OF ?Manufacturer_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
      BRW1.ResetSort(1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Manufacturer_Temp, Accepted)
    OF ?Location_Temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
      BRW1.ResetSort(1)
      Do refresh_browse
      Select(?Browse:1)
      IF Location_Temp OR ?Location_Temp{Prop:Req}
        loc:Location = Location_Temp
        !Save Lookup Field Incase Of error
        look:Location_Temp        = Location_Temp
        IF Access:LOCATION.TryFetch(loc:Location_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            Location_Temp = loc:Location
          ELSE
            !Restore Lookup On Error
            Location_Temp = look:Location_Temp
            SELECT(?Location_Temp)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      !Free(BRW1.ListQueue)
      BRW1.ApplyRange
      BRW1.ResetSort(1)
      Select(?Browse:1)
      ! ---------------------------------------- Higher Keys --------------------------------------- !
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, Accepted)
    OF ?LookupSiteLocations
      ThisWindow.Update
      loc:Location = Location_Temp
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          Location_Temp = loc:Location
          Select(?+1)
      ELSE
          Select(?Location_Temp)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?Location_Temp)
    OF ?Add_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Add_Stock, Accepted)
    OF ?Deplete_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Deplete_Stock, Accepted)
    OF ?Stock_History
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_History, Accepted)
      If SecurityCheck('STOCK - STOCK HISTORY')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else!if x" = false
          Thiswindow.reset
          glo:select1 = brw1.q.STO:Ref_Number
          Browse_Stock_History
          glo:select1 = ''
      End!Else!if x" = false
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_History, Accepted)
    OF ?Order_Stock
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
      BRW1.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Order_Stock, Accepted)
    OF ?Stock_Transfer
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Stock_Transfer, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Stock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore1.IgnoreEvent = True                       !Xplore
     Xplore1.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?Browse:1
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?Browse:1
    ! Before Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
    !Qty On Order
    tmp:QtyOnOrder = ''
    tmp:LastOrdered = 0                                                          !file. But check with the order
    save_orp_id = access:ordparts.savefile()                                                !file first to see if the whole
    access:ordparts.clearkey(orp:ref_number_key)                                            !order has been received. This
    orp:part_ref_number = brw1.q.sto:ref_number                                                    !should hopefully speed things
    set(orp:ref_number_key,orp:ref_number_key)                                              !up.
    loop
        if access:ordparts.next()
           break
        end !if
        if orp:part_ref_number <> brw1.q.sto:ref_number      |
            then break.  ! end if
        If orp:Order_Number <> ''
            access:orders.clearkey(ord:order_number_key)
            ord:order_number = orp:order_number
            if access:orders.fetch(ord:order_number_key) = Level:Benign
                If ord:all_received = 'NO'
                    If orp:all_received = 'NO'
                        tmp:QtyOnOrder += orp:quantity
                        If ord:date > tmp:LastOrdered
                            tmp:LastOrdered = ord:date
                        End!If ord:date > tmp:LastOrdered
                    End!If orp:all_received <> 'YES'
                End!If ord:all_received <> 'YES'
            end!if access:orders.fetch(ord:order_number_key) = Level:Benign
        End !If orp:Order_Number <> ''
    end !loop
    ! Start Change 3148 BE(26/09/03)
    IF (def:SummaryOrders = 1) THEN
        access:ordparts.clearkey(orp:ref_number_key)      
        orp:part_ref_number = 0
        orp:part_number = brw1.q.sto:part_number
        SET(orp:ref_number_key,orp:ref_number_key)
        LOOP
            IF ((access:ordparts.next() <> Level:Benign) OR |
                (orp:part_ref_number <> 0) OR |
                (orp:part_number <> brw1.q.sto:part_number)) THEN
               BREAK
            END
            IF ((orp:Order_Number <> '') AND (orp:all_received = 'NO')) THEN
                access:orders.clearkey(ord:order_number_key)
                ord:order_number = orp:order_number
                IF ((access:orders.fetch(ord:order_number_key) = Level:Benign) AND (ord:all_received = 'NO'))
                    tmp:QtyOnOrder += orp:quantity
                    IF (ord:date > tmp:LastOrdered) THEN
                        tmp:LastOrdered = ord:date
                    END
                END
            END
        END
    END
    ! End Change 3148 BE(26/09/03)
    access:ordparts.restorefile(save_orp_id)
    Display(?tmp:QtyOnOrder)
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
      If keycode() = HomeKey
          Presskey(CtrlHome)
      End!If keycode() = HomeKey
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, AlertKey)
    END
    ! After Embed Point: %ControlHandling) DESC(Control Handling) ARG(?Browse:1)
  OF ?Location_Temp
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Site Location')
             Post(Event:Accepted,?LookupSiteLocations)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupSiteLocations)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Location_Temp, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?CurrentTab
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
      Do refresh_browse
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?CurrentTab)
        OF 1
          ?Browse:1{PROP:FORMAT} ='86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#48L(2)|M~Reference Number~@n012@/#7#'
          ?Tab:3{PROP:TEXT} = 'By &Part Number'
        OF 2
          ?Browse:1{PROP:FORMAT} ='86L(2)|M~Description~@s30@#2#86L(2)|M~Part Number~@s30@#1#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#48L(2)|M~Reference Number~@n012@/#7#'
          ?Tab:4{PROP:TEXT} = 'By Desc&ription'
        OF 3
          ?Browse:1{PROP:FORMAT} ='132L(2)|M~Shelf Location~@s60@#4#86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#48L(2)|M~Reference Number~@n012@/#7#'
          ?Tab:5{PROP:TEXT} = 'By S&helf Location'
        OF 4
          ?Browse:1{PROP:FORMAT} ='97L(2)|M~Supplier~@s30@/#5#86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#48R(2)|M~Qty In Stock~L@N8@#6#48L(2)|M~Reference Number~@n012@/#7#'
          ?Tab:6{PROP:TEXT} = 'By Su&pplier'
        OF 5
          ?Browse:1{PROP:FORMAT} ='86L(2)|M~Part Number~@s30@#1#86L(2)|M~Description~@s30@#2#86L(2)|M~Manufacturer~@s30@#3#132L(2)|M~Shelf Location~@s60@#4#97L(2)|M~Supplier~@s30@/#5#48R(2)|M~Qty In Stock~L@N8@#6#48L(2)|M~Reference Number~@n012@/#7#'
          ?Tab5{PROP:TEXT} = 'By &Manufacturer'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?CurrentTab, NewSelection)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
          0{prop:buffer} = 1
      ! Site Location Defaults
      Set(defstock)
      access:defstock.next()
      If dst:use_site_location = 'YES'
          location_temp = dst:site_location
      Else
          location_temp = ''
      End
      brw1.resetsort(1)
      Display()
      Select(?browse:1)
      
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 1)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore1.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 1                           !Xplore
        Xplore1.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


BRW1.ApplyRange PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  IF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = Manufacturer_Temp
     end
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
     GET(SELF.Order.RangeList.List,3)
     if not error()
         Self.Order.RangeList.List.Right = Manufacturer_Temp
     end
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
     GET(SELF.Order.RangeList.List,2)
     if not error()
         Self.Order.RangeList.List.Right = accessory_temp
     end
     GET(SELF.Order.RangeList.List,3)
     if not error()
         Self.Order.RangeList.List.Right = Manufacturer_Temp
     end
  ELSE
     GET(SELF.Order.RangeList.List,1)
     if not error()
         Self.Order.RangeList.List.Right = Location_Temp
     end
  END
  ! ---------------------------------------- Higher Keys --------------------------------------- !
  ReturnValue = PARENT.ApplyRange()
  RETURN ReturnValue


BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.InsertControl=?Insert:3
    SELF.ChangeControl=?Change:3
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetQueue PROCEDURE(BYTE ResetMode)

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))
  PARENT.ResetQueue(ResetMode)
  If Records(Queue:Browse:1)
      Enable(?Button_Group)
  Else
      Disable(?Button_Group)
  End
  
  
  
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(1, ResetQueue, (BYTE ResetMode))


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,1
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore1.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore1.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(2,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(3,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(4,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(5,Force)
  ELSIF Choice(?CurrentTab) = 2 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(6,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(7,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(8,Force)
  ELSIF Choice(?CurrentTab) = 3 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(9,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(10,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(11,Force)
  ELSIF Choice(?CurrentTab) = 4 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(12,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'ALL'
    RETURN SELF.SetSort(13,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'NO'
    RETURN SELF.SetSort(14,Force)
  ELSIF Choice(?CurrentTab) = 5 And Upper(accessory_temp) = 'YES'
    RETURN SELF.SetSort(15,Force)
  ELSE
    RETURN SELF.SetSort(16,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.SetQueueRecord PROCEDURE

  CODE
  shelf_location_temp = CLIP(sto:Shelf_Location) & ' / ' & CLIP(sto:Second_Location)
  PARENT.SetQueueRecord
  SELF.Q.shelf_location_temp = shelf_location_temp    !Assign formula result to display queue


BRW1.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,1,'TakeEvent'
  Xplore1.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?Browse:1                              !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore1.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore1.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore1.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore1.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW1.FileSeqOn = False                         !Xplore
       Xplore1.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW1.SavePosition()                           !Xplore
       Xplore1.HandleMyEvents()                       !Xplore
       !BRW1.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?Browse:1                         !Xplore
  PARENT.TakeEvent


BRW1.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore1.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW1.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW1.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW1.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW1.ResetPairsQ PROCEDURE()
  CODE
Xplore1.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore1.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW1.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !sto:Part_Number
  OF 2 !sto:Description
  OF 3 !sto:Manufacturer
  OF 4 !shelf_location_temp
  OF 5 !sto:Supplier
  OF 6 !sto:Quantity_Stock
  OF 7 !sto:Ref_Number
  OF 8 !sto:Purchase_Cost
  OF 9 !sto:Sale_Cost
  OF 10 !sto:Retail_Cost
  OF 11 !no_temp
  OF 12 !yes_temp
  OF 13 !days_7_temp
  OF 14 !days_30_temp
  OF 15 !days_60_temp
  OF 16 !days_90_temp
  OF 17 !average_text_temp
  OF 18 !sto:Location
  OF 19 !sto:Accessory
  OF 20 !sto:Shelf_Location
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore1.SetNewOrderFields PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  RETURN
!================================================================================
Xplore1.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore1.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW1.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(STOCK)
  END
  RETURN TotalRecords
!================================================================================
Xplore1.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('sto:Part_Number')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Description')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Manufacturer')           !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Manufacturer')               !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Manufacturer')               !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('shelf_location_temp')        !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('shelf_location_temp')        !Header
                PSTRING('@S20')                       !Picture
                PSTRING('shelf_location_temp')        !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Supplier')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Supplier')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Supplier')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Quantity_Stock')         !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Qty In Stock')               !Header
                PSTRING('@N8')                        !Picture
                PSTRING('Qty In Stock')               !Description
                STRING('D')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Reference Number')           !Header
                PSTRING('@n012')                      !Picture
                PSTRING('Reference Number')           !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Purchase_Cost')          !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Purchase Cost')              !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Purchase Cost')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Sale_Cost')              !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Sale Cost')                  !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Sale Cost')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Retail_Cost')            !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Retail Cost')                !Header
                PSTRING('@n14.2')                     !Picture
                PSTRING('Retail Cost')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('no_temp')                    !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('no_temp')                    !Header
                PSTRING('@S20')                       !Picture
                PSTRING('no_temp')                    !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('yes_temp')                   !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('yes_temp')                   !Header
                PSTRING('@S20')                       !Picture
                PSTRING('yes_temp')                   !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('days_7_temp')                !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('days_7_temp')                !Header
                PSTRING('@S20')                       !Picture
                PSTRING('days_7_temp')                !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('days_30_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('days_30_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('days_30_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('days_60_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('days_60_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('days_60_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('days_90_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('days_90_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('days_90_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('average_text_temp')          !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('average_text_temp')          !Header
                PSTRING('@S20')                       !Picture
                PSTRING('average_text_temp')          !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Location')               !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Location')                   !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Location')                   !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Accessory')              !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('Accessory')                  !Header
                PSTRING('@s3')                        !Picture
                PSTRING('Accessory')                  !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                PSTRING('sto:Shelf_Location')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Shelf Location')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Shelf Location')             !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(0)                               !Sort Not Allowed
                STRING(' ')                           !No Locator
                !-------------------------
                END
XpDim           SHORT(20)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

