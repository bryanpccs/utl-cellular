

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01038.INC'),ONCE        !Local module procedure declarations
                     END


UpdateMinimumStock PROCEDURE                          !Generated from procedure template - Window

CurrentTab           STRING(80)
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
Average_text_temp    STRING(8)
Average_Temp         REAL
save_shi_id          USHORT,AUTO
tmp:PurchaseCost     REAL
tmp:SaleCost         REAL
History::smin:Record LIKE(smin:RECORD),STATIC
QuickWindow          WINDOW('Minimum Stock Level Item'),AT(,,391,195),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,ICON('pc.ico'),HLP('UpdateMinimumStock'),SYSTEM,GRAY,DOUBLE
                       SHEET,AT(4,4,220,160),USE(?Sheet1),SPREAD
                         TAB('Part Details'),USE(?PartDetails)
                           PROMPT('Part Number'),AT(8,20),USE(?SMIN:PartNumber:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(smin:PartNumber),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Part Number'),TIP('Part Number'),UPR,READONLY
                           PROMPT('Description'),AT(8,36),USE(?SMIN:Description:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(smin:Description),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Descriptoin'),TIP('Descriptoin'),UPR,READONLY
                           PROMPT('Supplier'),AT(8,52),USE(?SMIN:Supplier:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(smin:Supplier),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Supplier'),TIP('Supplier'),ALRT(DownKey),ALRT(EnterKey),ALRT(MouseRight),ALRT(MouseLeft2),REQ,UPR
                           BUTTON,AT(212,52,10,10),USE(?LookupSuppliers),SKIP,ICON('List3.ico')
                           PROMPT('Purchase Cost'),AT(8,68),USE(?sto:Purchase_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,68,64,10),USE(tmp:PurchaseCost),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Sale Cost'),AT(8,84),USE(?sto:Sale_Cost:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(tmp:SaleCost),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR
                           PROMPT('Minimum Level'),AT(8,100),USE(?SMIN:MinLevel:Prompt),TRN
                           ENTRY(@s8),AT(84,100,64,10),USE(smin:MinLevel),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Minimum Level'),TIP('Minimum Level'),UPR,READONLY
                           PROMPT('Quantity On Order'),AT(8,116),USE(?SMIN:QuantityOnOrder:Prompt),TRN
                           ENTRY(@s8),AT(84,116,64,10),USE(smin:QuantityOnOrder),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity On Order'),TIP('Quantity On Order'),UPR,READONLY
                           PROMPT('Quantity In Stock'),AT(8,132),USE(?SMIN:QuantityInStock:Prompt),TRN
                           ENTRY(@s8),AT(84,132,64,10),USE(smin:QuantityInStock),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity In Stock'),TIP('Quantity In Stock'),UPR,READONLY
                           PROMPT('Quantity Required'),AT(8,148),USE(?smin:QuantityRequired:prompt),TRN
                           SPIN(@s8),AT(84,148,64,10),USE(smin:QuantityRequired),FONT('Tahoma',8,COLOR:Red,FONT:bold,CHARSET:ANSI),UPR,RANGE(1,99999),STEP(1),MSG('Quantity Required')
                         END
                       END
                       SHEET,AT(228,4,160,160),USE(?Sheet2),SPREAD
                         TAB('Stock Usage'),USE(?Tab2)
                           PROMPT('0 - 7 Days'),AT(236,20),USE(?07Days)
                           STRING(@n-14),AT(309,20),USE(days_7_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14),AT(309,32),USE(days_30_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           STRING(@n-14),AT(309,56),USE(days_90_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Average Daily Use'),AT(236,72),USE(?AverageDailyUse)
                           STRING(@s8),AT(341,72),USE(Average_text_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('Supplier Defaults'),AT(232,92),USE(?SupplierDefaults)
                           PROMPT('History Usage'),AT(236,104),USE(?Prompt16)
                           STRING(@n2),AT(365,104),USE(sup:HistoryUsage),RIGHT,FONT(,,,FONT:bold)
                           STRING(@s8),AT(341,116),USE(sup:Factor),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('Multiply By Factor Of'),AT(236,116),USE(?MultipleByFactorOf)
                           PROMPT('Normal Supply Period'),AT(236,128),USE(?NormalSupplyPeriod)
                           STRING(@n3),AT(361,128),USE(sup:Normal_Supply_Period),RIGHT,FONT(,,,FONT:bold)
                           PROMPT('Minimum Order Value'),AT(236,140),USE(?MinimumOrderValue)
                           STRING(@s8),AT(341,140),USE(sup:Minimum_Order_Value),RIGHT,FONT(,,,FONT:bold)
                           LINE,AT(377,68,-59,0),USE(?Line1),COLOR(COLOR:Black)
                           STRING(@n-14),AT(309,44),USE(days_60_temp),RIGHT,FONT(,8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(236,32),USE(?030Days)
                           PROMPT('31 - 60 Days'),AT(236,44),USE(?3160Days)
                           PROMPT('61 - 90 Days'),AT(236,56),USE(?6190Days)
                         END
                       END
                       PANEL,AT(4,168,384,24),USE(?ButtonPanel),FILL(COLOR:Silver)
                       BUTTON('Update And Order'),AT(8,172,100,16),USE(?UpdateAndOrder),LEFT,ICON('ok.gif')
                       BUTTON('&OK'),AT(272,172,56,16),USE(?OKButton),HIDE,LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(328,172,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                       BUTTON('Update And Remove From Ordering'),AT(124,172,100,16),USE(?UpdateAndRemove),LEFT,ICON('delete.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
look:smin:Supplier                Like(smin:Supplier)
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?PartDetails{prop:Color} = 15066597
    ?SMIN:PartNumber:Prompt{prop:FontColor} = -1
    ?SMIN:PartNumber:Prompt{prop:Color} = 15066597
    If ?smin:PartNumber{prop:ReadOnly} = True
        ?smin:PartNumber{prop:FontColor} = 65793
        ?smin:PartNumber{prop:Color} = 15066597
    Elsif ?smin:PartNumber{prop:Req} = True
        ?smin:PartNumber{prop:FontColor} = 65793
        ?smin:PartNumber{prop:Color} = 8454143
    Else ! If ?smin:PartNumber{prop:Req} = True
        ?smin:PartNumber{prop:FontColor} = 65793
        ?smin:PartNumber{prop:Color} = 16777215
    End ! If ?smin:PartNumber{prop:Req} = True
    ?smin:PartNumber{prop:Trn} = 0
    ?smin:PartNumber{prop:FontStyle} = font:Bold
    ?SMIN:Description:Prompt{prop:FontColor} = -1
    ?SMIN:Description:Prompt{prop:Color} = 15066597
    If ?smin:Description{prop:ReadOnly} = True
        ?smin:Description{prop:FontColor} = 65793
        ?smin:Description{prop:Color} = 15066597
    Elsif ?smin:Description{prop:Req} = True
        ?smin:Description{prop:FontColor} = 65793
        ?smin:Description{prop:Color} = 8454143
    Else ! If ?smin:Description{prop:Req} = True
        ?smin:Description{prop:FontColor} = 65793
        ?smin:Description{prop:Color} = 16777215
    End ! If ?smin:Description{prop:Req} = True
    ?smin:Description{prop:Trn} = 0
    ?smin:Description{prop:FontStyle} = font:Bold
    ?SMIN:Supplier:Prompt{prop:FontColor} = -1
    ?SMIN:Supplier:Prompt{prop:Color} = 15066597
    If ?smin:Supplier{prop:ReadOnly} = True
        ?smin:Supplier{prop:FontColor} = 65793
        ?smin:Supplier{prop:Color} = 15066597
    Elsif ?smin:Supplier{prop:Req} = True
        ?smin:Supplier{prop:FontColor} = 65793
        ?smin:Supplier{prop:Color} = 8454143
    Else ! If ?smin:Supplier{prop:Req} = True
        ?smin:Supplier{prop:FontColor} = 65793
        ?smin:Supplier{prop:Color} = 16777215
    End ! If ?smin:Supplier{prop:Req} = True
    ?smin:Supplier{prop:Trn} = 0
    ?smin:Supplier{prop:FontStyle} = font:Bold
    ?sto:Purchase_Cost:Prompt{prop:FontColor} = -1
    ?sto:Purchase_Cost:Prompt{prop:Color} = 15066597
    If ?tmp:PurchaseCost{prop:ReadOnly} = True
        ?tmp:PurchaseCost{prop:FontColor} = 65793
        ?tmp:PurchaseCost{prop:Color} = 15066597
    Elsif ?tmp:PurchaseCost{prop:Req} = True
        ?tmp:PurchaseCost{prop:FontColor} = 65793
        ?tmp:PurchaseCost{prop:Color} = 8454143
    Else ! If ?tmp:PurchaseCost{prop:Req} = True
        ?tmp:PurchaseCost{prop:FontColor} = 65793
        ?tmp:PurchaseCost{prop:Color} = 16777215
    End ! If ?tmp:PurchaseCost{prop:Req} = True
    ?tmp:PurchaseCost{prop:Trn} = 0
    ?tmp:PurchaseCost{prop:FontStyle} = font:Bold
    ?sto:Sale_Cost:Prompt{prop:FontColor} = -1
    ?sto:Sale_Cost:Prompt{prop:Color} = 15066597
    If ?tmp:SaleCost{prop:ReadOnly} = True
        ?tmp:SaleCost{prop:FontColor} = 65793
        ?tmp:SaleCost{prop:Color} = 15066597
    Elsif ?tmp:SaleCost{prop:Req} = True
        ?tmp:SaleCost{prop:FontColor} = 65793
        ?tmp:SaleCost{prop:Color} = 8454143
    Else ! If ?tmp:SaleCost{prop:Req} = True
        ?tmp:SaleCost{prop:FontColor} = 65793
        ?tmp:SaleCost{prop:Color} = 16777215
    End ! If ?tmp:SaleCost{prop:Req} = True
    ?tmp:SaleCost{prop:Trn} = 0
    ?tmp:SaleCost{prop:FontStyle} = font:Bold
    ?SMIN:MinLevel:Prompt{prop:FontColor} = -1
    ?SMIN:MinLevel:Prompt{prop:Color} = 15066597
    If ?smin:MinLevel{prop:ReadOnly} = True
        ?smin:MinLevel{prop:FontColor} = 65793
        ?smin:MinLevel{prop:Color} = 15066597
    Elsif ?smin:MinLevel{prop:Req} = True
        ?smin:MinLevel{prop:FontColor} = 65793
        ?smin:MinLevel{prop:Color} = 8454143
    Else ! If ?smin:MinLevel{prop:Req} = True
        ?smin:MinLevel{prop:FontColor} = 65793
        ?smin:MinLevel{prop:Color} = 16777215
    End ! If ?smin:MinLevel{prop:Req} = True
    ?smin:MinLevel{prop:Trn} = 0
    ?smin:MinLevel{prop:FontStyle} = font:Bold
    ?SMIN:QuantityOnOrder:Prompt{prop:FontColor} = -1
    ?SMIN:QuantityOnOrder:Prompt{prop:Color} = 15066597
    If ?smin:QuantityOnOrder{prop:ReadOnly} = True
        ?smin:QuantityOnOrder{prop:FontColor} = 65793
        ?smin:QuantityOnOrder{prop:Color} = 15066597
    Elsif ?smin:QuantityOnOrder{prop:Req} = True
        ?smin:QuantityOnOrder{prop:FontColor} = 65793
        ?smin:QuantityOnOrder{prop:Color} = 8454143
    Else ! If ?smin:QuantityOnOrder{prop:Req} = True
        ?smin:QuantityOnOrder{prop:FontColor} = 65793
        ?smin:QuantityOnOrder{prop:Color} = 16777215
    End ! If ?smin:QuantityOnOrder{prop:Req} = True
    ?smin:QuantityOnOrder{prop:Trn} = 0
    ?smin:QuantityOnOrder{prop:FontStyle} = font:Bold
    ?SMIN:QuantityInStock:Prompt{prop:FontColor} = -1
    ?SMIN:QuantityInStock:Prompt{prop:Color} = 15066597
    If ?smin:QuantityInStock{prop:ReadOnly} = True
        ?smin:QuantityInStock{prop:FontColor} = 65793
        ?smin:QuantityInStock{prop:Color} = 15066597
    Elsif ?smin:QuantityInStock{prop:Req} = True
        ?smin:QuantityInStock{prop:FontColor} = 65793
        ?smin:QuantityInStock{prop:Color} = 8454143
    Else ! If ?smin:QuantityInStock{prop:Req} = True
        ?smin:QuantityInStock{prop:FontColor} = 65793
        ?smin:QuantityInStock{prop:Color} = 16777215
    End ! If ?smin:QuantityInStock{prop:Req} = True
    ?smin:QuantityInStock{prop:Trn} = 0
    ?smin:QuantityInStock{prop:FontStyle} = font:Bold
    ?smin:QuantityRequired:prompt{prop:FontColor} = -1
    ?smin:QuantityRequired:prompt{prop:Color} = 15066597
    If ?smin:QuantityRequired{prop:ReadOnly} = True
        ?smin:QuantityRequired{prop:FontColor} = 65793
        ?smin:QuantityRequired{prop:Color} = 15066597
    Elsif ?smin:QuantityRequired{prop:Req} = True
        ?smin:QuantityRequired{prop:FontColor} = 65793
        ?smin:QuantityRequired{prop:Color} = 8454143
    Else ! If ?smin:QuantityRequired{prop:Req} = True
        ?smin:QuantityRequired{prop:FontColor} = 65793
        ?smin:QuantityRequired{prop:Color} = 16777215
    End ! If ?smin:QuantityRequired{prop:Req} = True
    ?smin:QuantityRequired{prop:Trn} = 0
    ?smin:QuantityRequired{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?07Days{prop:FontColor} = -1
    ?07Days{prop:Color} = 15066597
    ?days_7_temp{prop:FontColor} = -1
    ?days_7_temp{prop:Color} = 15066597
    ?days_30_temp{prop:FontColor} = -1
    ?days_30_temp{prop:Color} = 15066597
    ?days_90_temp{prop:FontColor} = -1
    ?days_90_temp{prop:Color} = 15066597
    ?AverageDailyUse{prop:FontColor} = -1
    ?AverageDailyUse{prop:Color} = 15066597
    ?Average_text_temp{prop:FontColor} = -1
    ?Average_text_temp{prop:Color} = 15066597
    ?SupplierDefaults{prop:FontColor} = -1
    ?SupplierDefaults{prop:Color} = 15066597
    ?Prompt16{prop:FontColor} = -1
    ?Prompt16{prop:Color} = 15066597
    ?sup:HistoryUsage{prop:FontColor} = -1
    ?sup:HistoryUsage{prop:Color} = 15066597
    ?sup:Factor{prop:FontColor} = -1
    ?sup:Factor{prop:Color} = 15066597
    ?MultipleByFactorOf{prop:FontColor} = -1
    ?MultipleByFactorOf{prop:Color} = 15066597
    ?NormalSupplyPeriod{prop:FontColor} = -1
    ?NormalSupplyPeriod{prop:Color} = 15066597
    ?sup:Normal_Supply_Period{prop:FontColor} = -1
    ?sup:Normal_Supply_Period{prop:Color} = 15066597
    ?MinimumOrderValue{prop:FontColor} = -1
    ?MinimumOrderValue{prop:Color} = 15066597
    ?sup:Minimum_Order_Value{prop:FontColor} = -1
    ?sup:Minimum_Order_Value{prop:Color} = 15066597
    ?days_60_temp{prop:FontColor} = -1
    ?days_60_temp{prop:Color} = 15066597
    ?030Days{prop:FontColor} = -1
    ?030Days{prop:Color} = 15066597
    ?3160Days{prop:FontColor} = -1
    ?3160Days{prop:Color} = 15066597
    ?6190Days{prop:FontColor} = -1
    ?6190Days{prop:Color} = 15066597
    ?ButtonPanel{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
UpdateStock     Routine
    Set(DEFAULTS)
    Access:DEFAULTS.Next()

    Access:STOCK.Clearkey(sto:Ref_Number_Key)
    sto:Ref_Number  = smin:PartRefNumber
    If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Found
        sto:Purchase_Cost   = tmp:PurchaseCost
        sto:Sale_Cost       = tmp:SaleCost
        sto:Supplier        = smin:Supplier
        Access:STOCK.Update()
    Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
        !Error
        !Assert(0,'<13,10>Fetch Error<13,10>')
    End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
CreateOrder     Routine
    Set(DEFAULTS)
    Access:DEFAULTS.Next()
    Case def:SummaryOrders
        Of 0 !Old Fashioned ORders
            If access:ordpend.primerecord() = Level:Benign
                ope:part_ref_number = sto:ref_number
                ope:part_type       = 'STO'
                ope:supplier        = smin:Supplier
                ope:part_number     = sto:part_number
                ope:Description     = sto:description
                ope:job_number      = ''
                ope:quantity        = smin:QuantityRequired
                access:ordpend.insert()
            End !If access:ordpend.primerecord() = Level:Benign

        Of 1 !New ORders
            MakePartsRequest(smin:Supplier,sto:Part_Number,sto:Description,smin:QuantityRequired)

            sto:QuantityRequested += smin:QuantityRequired
            Access:STOCK.Update()

    End!Case def:SummaryOrders


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'UpdateMinimumStock',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'UpdateMinimumStock',1)
    SolaceViewVars('ActionMessage',ActionMessage,'UpdateMinimumStock',1)
    SolaceViewVars('days_7_temp',days_7_temp,'UpdateMinimumStock',1)
    SolaceViewVars('days_30_temp',days_30_temp,'UpdateMinimumStock',1)
    SolaceViewVars('days_60_temp',days_60_temp,'UpdateMinimumStock',1)
    SolaceViewVars('days_90_temp',days_90_temp,'UpdateMinimumStock',1)
    SolaceViewVars('Average_text_temp',Average_text_temp,'UpdateMinimumStock',1)
    SolaceViewVars('Average_Temp',Average_Temp,'UpdateMinimumStock',1)
    SolaceViewVars('save_shi_id',save_shi_id,'UpdateMinimumStock',1)
    SolaceViewVars('tmp:PurchaseCost',tmp:PurchaseCost,'UpdateMinimumStock',1)
    SolaceViewVars('tmp:SaleCost',tmp:SaleCost,'UpdateMinimumStock',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?PartDetails;  SolaceCtrlName = '?PartDetails';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMIN:PartNumber:Prompt;  SolaceCtrlName = '?SMIN:PartNumber:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:PartNumber;  SolaceCtrlName = '?smin:PartNumber';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMIN:Description:Prompt;  SolaceCtrlName = '?SMIN:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:Description;  SolaceCtrlName = '?smin:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMIN:Supplier:Prompt;  SolaceCtrlName = '?SMIN:Supplier:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:Supplier;  SolaceCtrlName = '?smin:Supplier';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?LookupSuppliers;  SolaceCtrlName = '?LookupSuppliers';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Purchase_Cost:Prompt;  SolaceCtrlName = '?sto:Purchase_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:PurchaseCost;  SolaceCtrlName = '?tmp:PurchaseCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Sale_Cost:Prompt;  SolaceCtrlName = '?sto:Sale_Cost:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?tmp:SaleCost;  SolaceCtrlName = '?tmp:SaleCost';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMIN:MinLevel:Prompt;  SolaceCtrlName = '?SMIN:MinLevel:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:MinLevel;  SolaceCtrlName = '?smin:MinLevel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMIN:QuantityOnOrder:Prompt;  SolaceCtrlName = '?SMIN:QuantityOnOrder:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:QuantityOnOrder;  SolaceCtrlName = '?smin:QuantityOnOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SMIN:QuantityInStock:Prompt;  SolaceCtrlName = '?SMIN:QuantityInStock:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:QuantityInStock;  SolaceCtrlName = '?smin:QuantityInStock';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:QuantityRequired:prompt;  SolaceCtrlName = '?smin:QuantityRequired:prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?smin:QuantityRequired;  SolaceCtrlName = '?smin:QuantityRequired';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?07Days;  SolaceCtrlName = '?07Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_7_temp;  SolaceCtrlName = '?days_7_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_30_temp;  SolaceCtrlName = '?days_30_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_90_temp;  SolaceCtrlName = '?days_90_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?AverageDailyUse;  SolaceCtrlName = '?AverageDailyUse';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Average_text_temp;  SolaceCtrlName = '?Average_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?SupplierDefaults;  SolaceCtrlName = '?SupplierDefaults';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt16;  SolaceCtrlName = '?Prompt16';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:HistoryUsage;  SolaceCtrlName = '?sup:HistoryUsage';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Factor;  SolaceCtrlName = '?sup:Factor';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MultipleByFactorOf;  SolaceCtrlName = '?MultipleByFactorOf';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?NormalSupplyPeriod;  SolaceCtrlName = '?NormalSupplyPeriod';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Normal_Supply_Period;  SolaceCtrlName = '?sup:Normal_Supply_Period';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?MinimumOrderValue;  SolaceCtrlName = '?MinimumOrderValue';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sup:Minimum_Order_Value;  SolaceCtrlName = '?sup:Minimum_Order_Value';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_60_temp;  SolaceCtrlName = '?days_60_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?030Days;  SolaceCtrlName = '?030Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?3160Days;  SolaceCtrlName = '?3160Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?6190Days;  SolaceCtrlName = '?6190Days';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ButtonPanel;  SolaceCtrlName = '?ButtonPanel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UpdateAndOrder;  SolaceCtrlName = '?UpdateAndOrder';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OKButton;  SolaceCtrlName = '?OKButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?UpdateAndRemove;  SolaceCtrlName = '?UpdateAndRemove';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Minimum Stock Level Item'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('UpdateMinimumStock')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'UpdateMinimumStock')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?SMIN:PartNumber:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(smin:Record,History::smin:Record)
  SELF.AddHistoryField(?smin:PartNumber,3)
  SELF.AddHistoryField(?smin:Description,4)
  SELF.AddHistoryField(?smin:Supplier,5)
  SELF.AddHistoryField(?smin:MinLevel,7)
  SELF.AddHistoryField(?smin:QuantityOnOrder,8)
  SELF.AddHistoryField(?smin:QuantityInStock,9)
  SELF.AddHistoryField(?smin:QuantityRequired,10)
  SELF.AddUpdateFile(Access:STOCKMIN)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:ORDPEND.Open
  Relate:STOCKMIN.Open
  Access:STOCK.UseFile
  Access:SUPPLIER.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOCKMIN
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OKButton
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
      glo:DeleteStock = 0
      Access:STOCK.Clearkey(sto:Ref_Number_Key)
      sto:Ref_Number  = smin:PartRefNumber
      If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Found
          tmp:PurchaseCost    = sto:Purchase_Cost
          tmp:SaleCost        = sto:Sale_Cost
      Else! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:STOCK.Tryfetch(sto:Ref_Number_Key) = Level:Benign
  
      Access:SUPPLIER.Clearkey(sup:Company_Name_Key)
      sup:Company_Name    = smin:Supplier
      If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
          !Found
  
      Else! If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
          !Error
          !Assert(0,'<13,10>Fetch Error<13,10>')
      End! If Access:SUPPLIER.Tryfetch(sup:Company_Name_Key) = Level:Benign
      
      
      ref_number# = sto:ref_number
      Include('stockuse.inc')
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  IF ?smin:Supplier{Prop:Tip} AND ~?LookupSuppliers{Prop:Tip}
     ?LookupSuppliers{Prop:Tip} = 'Select ' & ?smin:Supplier{Prop:Tip}
  END
  IF ?smin:Supplier{Prop:Msg} AND ~?LookupSuppliers{Prop:Msg}
     ?LookupSuppliers{Prop:Msg} = 'Select ' & ?smin:Supplier{Prop:Msg}
  END
  Resizer.Init(AppStrategy:Surface,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPEND.Close
    Relate:STOCKMIN.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'UpdateMinimumStock',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run(Number,Request)
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    PickSuppliers
    ReturnValue = GlobalResponse
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?smin:Supplier
      IF smin:Supplier OR ?smin:Supplier{Prop:Req}
        sup:Company_Name = smin:Supplier
        !Save Lookup Field Incase Of error
        look:smin:Supplier        = smin:Supplier
        IF Access:SUPPLIER.TryFetch(sup:Company_Name_Key)
          IF SELF.Run(1,SelectRecord) = RequestCompleted
            smin:Supplier = sup:Company_Name
          ELSE
            !Restore Lookup On Error
            smin:Supplier = look:smin:Supplier
            SELECT(?smin:Supplier)
            CYCLE
          END
        END
      END
      ThisWindow.Reset()
    OF ?LookupSuppliers
      ThisWindow.Update
      sup:Company_Name = smin:Supplier
      
      IF SELF.RUN(1,Selectrecord)  = RequestCompleted
          smin:Supplier = sup:Company_Name
          Select(?+1)
      ELSE
          Select(?smin:Supplier)
      END     
      !ThisWindow.Request = ThisWindow.OriginalRequest
      !ThisWindow.Reset(1)
      Post(event:accepted,?smin:Supplier)
    OF ?UpdateAndOrder
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndOrder, Accepted)
      Do UpdateStock
      Do CreateOrder
      glo:DeleteStock = 1
      Post(Event:Accepted,?OkButton)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndOrder, Accepted)
    OF ?OKButton
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    OF ?UpdateAndRemove
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndRemove, Accepted)
      Do UpdateStock
      glo:DeleteStock = 1
      Post(Event:Accepted,?OkButton)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?UpdateAndRemove, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'UpdateMinimumStock')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?smin:Supplier
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?smin:Supplier, AlertKey)
      If Keycode() = MouseRight
         Execute Popup('Lookup Suppliers')
             Post(Event:Accepted,?LookupSuppliers)
         End!Execute Popup('Lookup Repair Type')
      Else!If Keycode() = MouseRight
         Post(Event:Accepted,?LookupSuppliers)
      End!If Keycode() = MouseRight
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?smin:Supplier, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

