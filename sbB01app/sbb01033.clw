

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01033.INC'),ONCE        !Local module procedure declarations
                     END


AmendStockRequest PROCEDURE                           !Generated from procedure template - Window

LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
ActionMessage        CSTRING(40)
sav:Quantity         LONG
window               WINDOW('Amend Stock Request'),AT(,,220,91),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('pc.ico'),GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,56),USE(?Sheet1),SPREAD
                         TAB('Amend Stock Request'),USE(?Tab1)
                           PROMPT('Location'),AT(8,20),USE(?orstmp:Location:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,20,124,10),USE(orstmp:Location),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Location'),TIP('Location'),UPR,READONLY
                           PROMPT('Quantity'),AT(8,36),USE(?orstmp:Quantity:Prompt),TRN
                           SPIN(@n8),AT(84,36,64,10),USE(orstmp:Quantity),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Quantity'),TIP('Quantity'),UPR,RANGE(1,99999),STEP(1)
                         END
                       END
                       PANEL,AT(4,64,212,24),USE(?Panel1),FILL(COLOR:Silver)
                       BUTTON('&OK'),AT(100,68,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT,REQ
                       BUTTON('Cancel'),AT(156,68,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeCompleted          PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?orstmp:Location:Prompt{prop:FontColor} = -1
    ?orstmp:Location:Prompt{prop:Color} = 15066597
    If ?orstmp:Location{prop:ReadOnly} = True
        ?orstmp:Location{prop:FontColor} = 65793
        ?orstmp:Location{prop:Color} = 15066597
    Elsif ?orstmp:Location{prop:Req} = True
        ?orstmp:Location{prop:FontColor} = 65793
        ?orstmp:Location{prop:Color} = 8454143
    Else ! If ?orstmp:Location{prop:Req} = True
        ?orstmp:Location{prop:FontColor} = 65793
        ?orstmp:Location{prop:Color} = 16777215
    End ! If ?orstmp:Location{prop:Req} = True
    ?orstmp:Location{prop:Trn} = 0
    ?orstmp:Location{prop:FontStyle} = font:Bold
    ?orstmp:Quantity:Prompt{prop:FontColor} = -1
    ?orstmp:Quantity:Prompt{prop:Color} = 15066597
    If ?orstmp:Quantity{prop:ReadOnly} = True
        ?orstmp:Quantity{prop:FontColor} = 65793
        ?orstmp:Quantity{prop:Color} = 15066597
    Elsif ?orstmp:Quantity{prop:Req} = True
        ?orstmp:Quantity{prop:FontColor} = 65793
        ?orstmp:Quantity{prop:Color} = 8454143
    Else ! If ?orstmp:Quantity{prop:Req} = True
        ?orstmp:Quantity{prop:FontColor} = 65793
        ?orstmp:Quantity{prop:Color} = 16777215
    End ! If ?orstmp:Quantity{prop:Req} = True
    ?orstmp:Quantity{prop:Trn} = 0
    ?orstmp:Quantity{prop:FontStyle} = font:Bold
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'AmendStockRequest',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('ActionMessage',ActionMessage,'AmendStockRequest',1)
    SolaceViewVars('sav:Quantity',sav:Quantity,'AmendStockRequest',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Location:Prompt;  SolaceCtrlName = '?orstmp:Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Location;  SolaceCtrlName = '?orstmp:Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Quantity:Prompt;  SolaceCtrlName = '?orstmp:Quantity:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orstmp:Quantity;  SolaceCtrlName = '?orstmp:Quantity';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record will be Added'
  OF ChangeRecord
    ActionMessage = 'Record will be Changed'
  END
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('AmendStockRequest')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'AmendStockRequest')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?orstmp:Location:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Cancel,RequestCancelled)
  SELF.AddUpdateFile(Access:ORDSTOCK)
  Relate:ORDPARTS.Open
  Relate:ORDSTOCK.Open
  Access:STOCK.UseFile
  SELF.FilesOpened = True
  SELF.Primary &= Relate:ORDSTOCK
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  sav:Quantity    = orstmp:Quantity
  OPEN(window)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:ORDPARTS.Close
    Relate:ORDSTOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'AmendStockRequest',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeCompleted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  IF orstmp:Quantity <> sav:Quantity
      Case MessageEx('Are you sure you want to change the Quantity Requested?','ServiceBase 2000',|
                     'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
          Of 1 ! &Yes Button
              Access:STOCK.Clearkey(sto:Location_Key)
              sto:Location    = orstmp:Location
              sto:Part_Number = orstmp:PartNumber
              If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                  !Found
                  sto:QuantityRequested   = orstmp:Quantity
                  Access:STOCK.Update()
              Else! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
                  !Error
                  Assert(0,'<13,10>Fetch Error<13,10>Cannot find Part Number (' & Clip(orstmp:PartNumber) & |
                              ') at Location (' & Clip(orstmp:Location) & ').')
              End! If Access:STOCK.Tryfetch(sto:Location_Key) = Level:Benign
  
          Of 2 ! &No Button
              orstmp:Quantity = sav:Quantity
              Display(?orstmp:Quantity)
              Cycle
      End!Case MessageEx
  End!orstmp:Quantity <> sav:Quantity
  
  ReturnValue = PARENT.TakeCompleted()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(TakeCompleted, (),BYTE)
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'AmendStockRequest')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

