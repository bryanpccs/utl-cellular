

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABBROWSE.INC'),ONCE
   INCLUDE('ABPOPUP.INC'),ONCE
   INCLUDE('ABQuery.INC'),ONCE
   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE
   INCLUDE('Xplore.INC'),ONCE

                     MAP
                       INCLUDE('SBB01027.INC'),ONCE        !Local module procedure declarations
                     END





Browse_Parts_Orders PROCEDURE                         !Generated from procedure template - Window

! Before Embed Point: %DataSection) DESC(Data for the procedure) ARG()
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
DASBRW::11:TAGFLAG         BYTE(0)
DASBRW::11:TAGMOUSE        BYTE(0)
DASBRW::11:TAGDISPSTATUS   BYTE(0)
DASBRW::11:QUEUE          QUEUE
Pointer                       LIKE(GLO:Pointer)
                          END
!--------------------------------------------------------------------------
! Tagging Data
!--------------------------------------------------------------------------
CurrentTab           STRING(80)
tmp:LabelType        BYTE(0)
save_res_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_sto_id          USHORT,AUTO
retstock_record_number_temp LONG
parts_record_number_temp REAL
warparts_record_number_temp REAL
LocalRequest         LONG
FilesOpened          BYTE
Received_Temp        STRING(20)
outstanding_temp     STRING('NO {1}')
job_number_temp      STRING(8)
type_temp            STRING(3)
model_temp           STRING(30)
account_temp         STRING(15)
exchange_temp        STRING(1)
tmp:tag              STRING('0')
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
BRW1::View:Browse    VIEW(ORDERS)
                       PROJECT(ord:Order_Number)
                       PROJECT(ord:Supplier)
                       PROJECT(ord:User)
                       PROJECT(ord:Date)
                       PROJECT(ord:Printed)
                       PROJECT(ord:All_Received)
                     END
Queue:Browse:1       QUEUE                            !Queue declaration for browse/combo box using ?Browse:1
ord:Order_Number       LIKE(ord:Order_Number)         !List box control field - type derived from field
ord:Supplier           LIKE(ord:Supplier)             !List box control field - type derived from field
ord:User               LIKE(ord:User)                 !List box control field - type derived from field
ord:Date               LIKE(ord:Date)                 !List box control field - type derived from field
ord:Printed            LIKE(ord:Printed)              !List box control field - type derived from field
ord:All_Received       LIKE(ord:All_Received)         !List box control field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
BRW6::View:Browse    VIEW(ORDPARTS)
                       PROJECT(orp:Part_Number)
                       PROJECT(orp:Description)
                       PROJECT(orp:Quantity)
                       PROJECT(orp:Purchase_Cost)
                       PROJECT(orp:Sale_Cost)
                       PROJECT(orp:Retail_Cost)
                       PROJECT(orp:Date_Received)
                       PROJECT(orp:All_Received)
                       PROJECT(orp:Part_Ref_Number)
                       PROJECT(orp:Record_Number)
                       PROJECT(orp:Order_Number)
                       JOIN(sto:Ref_Part_Description2_Key,orp:Part_Ref_Number,orp:Part_Number,orp:Description)
                         PROJECT(sto:Second_Location)
                         PROJECT(sto:Shelf_Location)
                         PROJECT(sto:Ref_Number)
                         PROJECT(sto:Part_Number)
                         PROJECT(sto:Description)
                       END
                     END
Queue:Browse         QUEUE                            !Queue declaration for browse/combo box using ?List
tmp:tag                LIKE(tmp:tag)                  !List box control field - type derived from local data
tmp:tag_Icon           LONG                           !Entry's icon ID
orp:Part_Number        LIKE(orp:Part_Number)          !List box control field - type derived from field
orp:Part_Number_NormalFG LONG                         !Normal forground color
orp:Part_Number_NormalBG LONG                         !Normal background color
orp:Part_Number_SelectedFG LONG                       !Selected forground color
orp:Part_Number_SelectedBG LONG                       !Selected background color
orp:Description        LIKE(orp:Description)          !List box control field - type derived from field
orp:Description_NormalFG LONG                         !Normal forground color
orp:Description_NormalBG LONG                         !Normal background color
orp:Description_SelectedFG LONG                       !Selected forground color
orp:Description_SelectedBG LONG                       !Selected background color
type_temp              LIKE(type_temp)                !List box control field - type derived from local data
type_temp_NormalFG     LONG                           !Normal forground color
type_temp_NormalBG     LONG                           !Normal background color
type_temp_SelectedFG   LONG                           !Selected forground color
type_temp_SelectedBG   LONG                           !Selected background color
job_number_temp        LIKE(job_number_temp)          !List box control field - type derived from local data
job_number_temp_NormalFG LONG                         !Normal forground color
job_number_temp_NormalBG LONG                         !Normal background color
job_number_temp_SelectedFG LONG                       !Selected forground color
job_number_temp_SelectedBG LONG                       !Selected background color
orp:Quantity           LIKE(orp:Quantity)             !List box control field - type derived from field
orp:Quantity_NormalFG  LONG                           !Normal forground color
orp:Quantity_NormalBG  LONG                           !Normal background color
orp:Quantity_SelectedFG LONG                          !Selected forground color
orp:Quantity_SelectedBG LONG                          !Selected background color
orp:Purchase_Cost      LIKE(orp:Purchase_Cost)        !List box control field - type derived from field
orp:Purchase_Cost_NormalFG LONG                       !Normal forground color
orp:Purchase_Cost_NormalBG LONG                       !Normal background color
orp:Purchase_Cost_SelectedFG LONG                     !Selected forground color
orp:Purchase_Cost_SelectedBG LONG                     !Selected background color
orp:Sale_Cost          LIKE(orp:Sale_Cost)            !List box control field - type derived from field
orp:Sale_Cost_NormalFG LONG                           !Normal forground color
orp:Sale_Cost_NormalBG LONG                           !Normal background color
orp:Sale_Cost_SelectedFG LONG                         !Selected forground color
orp:Sale_Cost_SelectedBG LONG                         !Selected background color
orp:Retail_Cost        LIKE(orp:Retail_Cost)          !List box control field - type derived from field
orp:Retail_Cost_NormalFG LONG                         !Normal forground color
orp:Retail_Cost_NormalBG LONG                         !Normal background color
orp:Retail_Cost_SelectedFG LONG                       !Selected forground color
orp:Retail_Cost_SelectedBG LONG                       !Selected background color
orp:Date_Received      LIKE(orp:Date_Received)        !List box control field - type derived from field
orp:Date_Received_NormalFG LONG                       !Normal forground color
orp:Date_Received_NormalBG LONG                       !Normal background color
orp:Date_Received_SelectedFG LONG                     !Selected forground color
orp:Date_Received_SelectedBG LONG                     !Selected background color
orp:All_Received       LIKE(orp:All_Received)         !List box control field - type derived from field
model_temp             LIKE(model_temp)               !List box control field - type derived from local data
model_temp_NormalFG    LONG                           !Normal forground color
model_temp_NormalBG    LONG                           !Normal background color
model_temp_SelectedFG  LONG                           !Selected forground color
model_temp_SelectedBG  LONG                           !Selected background color
account_temp           LIKE(account_temp)             !List box control field - type derived from local data
account_temp_NormalFG  LONG                           !Normal forground color
account_temp_NormalBG  LONG                           !Normal background color
account_temp_SelectedFG LONG                          !Selected forground color
account_temp_SelectedBG LONG                          !Selected background color
exchange_temp          LIKE(exchange_temp)            !List box control field - type derived from local data
exchange_temp_NormalFG LONG                           !Normal forground color
exchange_temp_NormalBG LONG                           !Normal background color
exchange_temp_SelectedFG LONG                         !Selected forground color
exchange_temp_SelectedBG LONG                         !Selected background color
orp:Part_Ref_Number    LIKE(orp:Part_Ref_Number)      !List box control field - type derived from field
sto:Second_Location    LIKE(sto:Second_Location)      !List box control field - type derived from field
sto:Shelf_Location     LIKE(sto:Shelf_Location)       !List box control field - type derived from field
orp:Record_Number      LIKE(orp:Record_Number)        !List box control field - type derived from field
orp:Order_Number       LIKE(orp:Order_Number)         !Browse key field - type derived from field
sto:Ref_Number         LIKE(sto:Ref_Number)           !Related join file key field - type derived from field
sto:Part_Number        LIKE(sto:Part_Number)          !Related join file key field - type derived from field
sto:Description        LIKE(sto:Description)          !Related join file key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
!Extension Templete Code:XploreOOPBrowse,DataSectionBeforeWindow
!CW Version         = 5507                            !Xplore
!CW TemplateVersion = v5.5                            !Xplore
XploreMask6          DECIMAL(10,0,538770663)          !Xplore
XploreMask16          DECIMAL(10,0,50)                !Xplore
XploreTitle6         STRING(' ')                      !Xplore
xpInitialTab6        SHORT                            !Xplore
QuickWindow          WINDOW('Browse the Parts Orders File'),AT(,,668,348),FONT('Tahoma',8,,),CENTER,IMM,ICON('PC.ICO'),HLP('Browse_Parts_Orders'),ALRT(F2Key),ALRT(F9Key),ALRT(F10Key),STATUS(-1,-1,-1,0),SYSTEM,GRAY,DOUBLE
                       LIST,AT(8,40,484,92),USE(?Browse:1),IMM,VSCROLL,MSG('Browsing Records'),FORMAT('67L(2)|M~Order Number~@n012@127L(2)|M~Supplier~@s30@27L(2)|M~User~@s3@52R(2)|M~D' &|
   'ate~C(0)@d6b@41L(2)|M~Printed~@s3@52L(2)|M~Received~@s3@'),FROM(Queue:Browse:1)
                       BUTTON('Close [ESC]'),AT(588,324,76,20),USE(?Close),LEFT,ICON('cancel.ico')
                       SHEET,AT(4,4,580,132),USE(?CurrentTab),SPREAD
                         TAB('By Order Number'),USE(?Tab:2)
                           OPTION('Order Type'),AT(376,16,112,20),USE(outstanding_temp),BOXED
                             RADIO('All'),AT(456,24),USE(?outstanding_temp:Radio2),VALUE('ALL')
                             RADIO('Outstanding'),AT(388,24),USE(?outstanding_temp:Radio1),VALUE('NO')
                           END
                           ENTRY(@n12b),AT(8,24,64,10),USE(ord:Order_Number),FONT('Tahoma',8,,FONT:bold),UPR
                           BUTTON('Delete Order'),AT(500,112,80,20),USE(?Delete:3),LEFT,ICON('delete.ico')
                           BUTTON('Reprint Order [F2]'),AT(500,40,80,20),USE(?Reprint_Order_Button),LEFT,ICON(ICON:Print1)
                         END
                       END
                       SHEET,AT(4,140,580,204),USE(?Sheet2),WIZARD,SPREAD
                         TAB('By Part Number'),USE(?Tab2)
                         END
                         TAB('By Part Number'),USE(?Tab3)
                         END
                       END
                       PROMPT('By Part Number'),AT(8,144),USE(?Prompt3)
                       PROMPT('Shelf Location'),AT(300,144),USE(?STO:Shelf_Location:Prompt),TRN
                       ENTRY(@s30),AT(364,144,124,10),USE(sto:Shelf_Location),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                       LIST,AT(8,176,484,140),USE(?List),IMM,HVSCROLL,MSG('Browsing Records'),ALRT(MouseLeft2),FORMAT('11L(2)J@s1@110L(2)|M*~Part Number~@s30@109L(2)|M*~Description~@s30@23L(2)|M*~Typ' &|
   'e~@s3@35R(2)|M*~Ref No~L@s8@38L(2)|M*~Quantity~@p<<<<<<<<<<<<<<#p@35R(2)|M*~Purchase~@n' &|
   '7.2@34R(2)|M*~Sale~@n7.2@34R(2)|M*~Retail~@n7.2@40R(2)|M*~Date Rcvd~@d6b@61L(2)|' &|
   'M~All Rcvd~@s3@35L(2)|M*~Model~@s30@35L(2)|M*~Account~@s15@35L(2)|M*~Exchange~@s' &|
   '1@48L(12)|M~Part Ref Number~@n012@120L(12)|M~Second Location~@s30@120L(12)|M~She' &|
   'lf Location~@s30@48D(12)|M~record number~L@n12@'),FROM(Queue:Browse)
                       ENTRY(@s30),AT(8,160,124,10),USE(orp:Part_Number),UPR
                       PROMPT('2nd Location'),AT(300,160),USE(?STO:Second_Location:Prompt),TRN
                       ENTRY(@s30),AT(364,160,124,10),USE(sto:Second_Location),SKIP,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                       BUTTON('Mark Tagged Received [F10]'),AT(500,236,80,20),USE(?All_Recieved_Button),HIDE,LEFT,ICON('despatch.gif')
                       BUTTON('Delete Part Off Order'),AT(500,296,80,20),USE(?Delete_Part_Button),LEFT,ICON('delete.ico')
                       BUTTON('Receive Part [F9]'),AT(500,176,80,20),USE(?Received_Button),HIDE,LEFT,ICON('stock_in.gif')
                       BUTTON('Receive Routine [F10]'),AT(500,204,80,20),USE(?ReceiveProcess),HIDE,LEFT,ICON('stock_in.gif')
                       BUTTON('&Tag    [F5]'),AT(8,320,56,16),USE(?DASTAG),HIDE,LEFT,ICON('Tag.gif')
                       BUTTON('T&ag All [F6]'),AT(64,320,56,16),USE(?DASTAGAll),HIDE,LEFT,ICON('TagAll.gif')
                       BUTTON('&UnTag All [F7]'),AT(120,320,56,16),USE(?DASUNTAGALL),HIDE,LEFT,ICON('UnTag.gif')
                       BUTTON('&Rev tags'),AT(244,228,50,13),USE(?DASREVTAG),HIDE
                       BUTTON('sho&W tags'),AT(300,228,70,13),USE(?DASSHOWTAG),HIDE
                       BOX,AT(228,324,8,8),USE(?Box1),COLOR(COLOR:Red),FILL(COLOR:Red)
                       PROMPT(' - Items Not Received'),AT(240,324),USE(?Prompt4)
                     END

BuildingWindow WINDOW,AT(,,124,21),FONT('Tahoma',8,,),COLOR(COLOR:Black),CENTER,GRAY,DOUBLE
       PROMPT('Building Parts List....'),AT(4,4),USE(?Prompt1),FONT(,12,COLOR:Lime,FONT:bold)
     END
! After Embed Point: %DataSection) DESC(Data for the procedure) ARG()
ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
Run                    PROCEDURE(USHORT Number,BYTE Request),BYTE,PROC,DERIVED
SetAlerts              PROCEDURE(),DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
BRW1                 CLASS(BrowseClass)               !Browse using ?Browse:1
Q                      &Queue:Browse:1                !Reference to browse queue
Init                   PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
                     END

BRW1::Sort0:Locator  IncrementalLocatorClass          !Default Locator
BRW1::Sort1:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'NO'
BRW1::Sort2:Locator  IncrementalLocatorClass          !Conditional Locator - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'ALL'
BRW1::Sort0:StepClass StepRealClass                   !Default Step Manager
BRW1::Sort1:StepClass StepRealClass                   !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'NO'
BRW1::Sort2:StepClass StepRealClass                   !Conditional Step Manager - Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'ALL'
BRW6                 CLASS(BrowseClass)               !Browse using ?List
ViewOrder              BYTE(0)                        !Xplore
FileSeqOn              BYTE(0)                        !Xplore
SequenceNbr            BYTE(0)                        !Xplore
SortOrderNbr           BYTE(0)                        !Xplore
FileOrderNbr           BYTE(0)                        !Xplore
LocatorField           STRING(30)                     !Xplore
SavedPosition          USHORT                         !Xplore
SavePosition           PROCEDURE()                    !Xplore
RestorePosition        PROCEDURE()                    !Xplore
RecordsInKey           PROCEDURE(),LONG,PROC          !Xplore
ResetPairsQ            PROCEDURE()                    !Xplore
Q                      &Queue:Browse                  !Reference to browse queue
GetFreeElementName     PROCEDURE(),STRING,DERIVED
GetFreeElementPosition PROCEDURE(),BYTE,DERIVED
ResetSort              PROCEDURE(BYTE Force),BYTE,PROC,DERIVED
SetQueueRecord         PROCEDURE(),DERIVED
TakeEvent              PROCEDURE(),DERIVED
TakeKey                PROCEDURE(),BYTE,PROC,DERIVED
TakeNewSelection       PROCEDURE(),DERIVED
ValidateRecord         PROCEDURE(),BYTE,DERIVED
                     END

BRW6::Sort0:Locator  IncrementalLocatorClass          !Default Locator
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

! Before Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()
save_orp_id   ushort,auto
save_par_id   ushort,auto
save_wpr_id   ushort,auto
! moving bar window
rejectrecord         long
recordstoprocess     long,auto
recordsprocessed     long,auto
recordspercycle      long,auto
recordsthiscycle     long,auto
percentprogress      byte
recordstatus         byte,auto
tmp:cancel           byte

progress:thermometer byte
progresswindow WINDOW('Progress...'),AT(,,164,64),FONT('Tahoma',8,,FONT:regular),CENTER,IMM,TIMER(1),GRAY, |
         DOUBLE
       PROGRESS,USE(progress:thermometer),AT(25,15,111,12),RANGE(0,100)
       STRING(''),AT(0,3,161,10),USE(?progress:userstring),CENTER,FONT('Tahoma',8,,)
       STRING(''),AT(0,30,161,10),USE(?progress:pcttext),TRN,CENTER,FONT('Tahoma',8,,)
       BUTTON('Cancel'),AT(54,44,56,16),USE(?cancel),LEFT,ICON('cancel.gif')
     END
    MAP            ! Start Change 3505 BE(10/11/03)
UpdateStockHistory  PROCEDURE(Long, Long, String, Long, String, Long)
    END
! End Change 3505 BE(10/11/03)
!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
!Extension Templete Code:XploreOOPBrowse,LocalDataAfterClasses
Xplore6              CLASS(XploreClass)               !Xplore
RestoreHeader          BYTE(0)
LastEvent              LONG
AddColumnSortOrder     PROCEDURE(LONG SortQRecord),VIRTUAL
ListBoxData            PROCEDURE(),PRIVATE,VIRTUAL
GetStringForGraph      PROCEDURE(SHORT QFieldNumber,*CSTRING Pic),VIRTUAL
SetNewOrderFields      PROCEDURE(),VIRTUAL
SetFilters             PROCEDURE(),VIRTUAL
GetRecordsInKey        PROCEDURE(),LONG,PROC,VIRTUAL
MyPreviewer            PROCEDURE(PreviewQueue PQ,REPORT CPCS),BYTE,VIRTUAL
                     END                              !Xplore
Xplore6Step1         StepCustomClass !                !Xplore: Column displaying tmp:tag
Xplore6Locator1      StepLocatorClass                 !Xplore: Column displaying tmp:tag
Xplore6Step2         StepStringClass !STRING          !Xplore: Column displaying orp:Part_Number
Xplore6Locator2      StepLocatorClass                 !Xplore: Column displaying orp:Part_Number
Xplore6Step3         StepStringClass !STRING          !Xplore: Column displaying orp:Description
Xplore6Locator3      StepLocatorClass                 !Xplore: Column displaying orp:Description
Xplore6Step4         StepCustomClass !                !Xplore: Column displaying type_temp
Xplore6Locator4      StepLocatorClass                 !Xplore: Column displaying type_temp
Xplore6Step5         StepCustomClass !                !Xplore: Column displaying job_number_temp
Xplore6Locator5      StepLocatorClass                 !Xplore: Column displaying job_number_temp
Xplore6Step6         StepLongClass   !LONG            !Xplore: Column displaying orp:Quantity
Xplore6Locator6      StepLocatorClass                 !Xplore: Column displaying orp:Quantity
Xplore6Step7         StepRealClass   !REAL            !Xplore: Column displaying orp:Purchase_Cost
Xplore6Locator7      StepLocatorClass                 !Xplore: Column displaying orp:Purchase_Cost
Xplore6Step8         StepRealClass   !REAL            !Xplore: Column displaying orp:Sale_Cost
Xplore6Locator8      StepLocatorClass                 !Xplore: Column displaying orp:Sale_Cost
Xplore6Step9         StepRealClass   !REAL            !Xplore: Column displaying orp:Retail_Cost
Xplore6Locator9      StepLocatorClass                 !Xplore: Column displaying orp:Retail_Cost
Xplore6Step10        StepCustomClass !DATE            !Xplore: Column displaying orp:Date_Received
Xplore6Locator10     StepLocatorClass                 !Xplore: Column displaying orp:Date_Received
Xplore6Step11        StepStringClass !STRING          !Xplore: Column displaying orp:All_Received
Xplore6Locator11     StepLocatorClass                 !Xplore: Column displaying orp:All_Received
Xplore6Step12        StepCustomClass !                !Xplore: Column displaying model_temp
Xplore6Locator12     StepLocatorClass                 !Xplore: Column displaying model_temp
Xplore6Step13        StepCustomClass !                !Xplore: Column displaying account_temp
Xplore6Locator13     StepLocatorClass                 !Xplore: Column displaying account_temp
Xplore6Step14        StepCustomClass !                !Xplore: Column displaying exchange_temp
Xplore6Locator14     StepLocatorClass                 !Xplore: Column displaying exchange_temp
Xplore6Step15        StepLongClass   !LONG            !Xplore: Column displaying orp:Part_Ref_Number
Xplore6Locator15     StepLocatorClass                 !Xplore: Column displaying orp:Part_Ref_Number
Xplore6Step16        StepStringClass !STRING          !Xplore: Column displaying sto:Second_Location
Xplore6Locator16     StepLocatorClass                 !Xplore: Column displaying sto:Second_Location
Xplore6Step17        StepStringClass !STRING          !Xplore: Column displaying sto:Shelf_Location
Xplore6Locator17     StepLocatorClass                 !Xplore: Column displaying sto:Shelf_Location
Xplore6Step18        StepLongClass   !LONG            !Xplore: Column displaying orp:Record_Number
Xplore6Locator18     StepLocatorClass                 !Xplore: Column displaying orp:Record_Number
Xplore6Step19        StepLongClass   !LONG            !Xplore: Column displaying orp:Order_Number
Xplore6Locator19     StepLocatorClass                 !Xplore: Column displaying orp:Order_Number
Xplore6Step20        StepLongClass   !LONG            !Xplore: Column displaying sto:Ref_Number
Xplore6Locator20     StepLocatorClass                 !Xplore: Column displaying sto:Ref_Number
Xplore6Step21        StepStringClass !STRING          !Xplore: Column displaying sto:Part_Number
Xplore6Locator21     StepLocatorClass                 !Xplore: Column displaying sto:Part_Number
Xplore6Step22        StepStringClass !STRING          !Xplore: Column displaying sto:Description
Xplore6Locator22     StepLocatorClass                 !Xplore: Column displaying sto:Description
! After Embed Point: %LocalDataafterClasses) DESC(Local Data After Object Declarations) ARG()

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Browse:1{prop:FontColor} = 65793
    ?Browse:1{prop:Color}= 16777215
    ?Browse:1{prop:Color,2} = 16777215
    ?Browse:1{prop:Color,3} = 12937777
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:2{prop:Color} = 15066597
    ?outstanding_temp{prop:Font,3} = -1
    ?outstanding_temp{prop:Color} = 15066597
    ?outstanding_temp{prop:Trn} = 0
    ?outstanding_temp:Radio2{prop:Font,3} = -1
    ?outstanding_temp:Radio2{prop:Color} = 15066597
    ?outstanding_temp:Radio2{prop:Trn} = 0
    ?outstanding_temp:Radio1{prop:Font,3} = -1
    ?outstanding_temp:Radio1{prop:Color} = 15066597
    ?outstanding_temp:Radio1{prop:Trn} = 0
    If ?ord:Order_Number{prop:ReadOnly} = True
        ?ord:Order_Number{prop:FontColor} = 65793
        ?ord:Order_Number{prop:Color} = 15066597
    Elsif ?ord:Order_Number{prop:Req} = True
        ?ord:Order_Number{prop:FontColor} = 65793
        ?ord:Order_Number{prop:Color} = 8454143
    Else ! If ?ord:Order_Number{prop:Req} = True
        ?ord:Order_Number{prop:FontColor} = 65793
        ?ord:Order_Number{prop:Color} = 16777215
    End ! If ?ord:Order_Number{prop:Req} = True
    ?ord:Order_Number{prop:Trn} = 0
    ?ord:Order_Number{prop:FontStyle} = font:Bold
    ?Sheet2{prop:Color} = 15066597
    ?Tab2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Prompt3{prop:FontColor} = -1
    ?Prompt3{prop:Color} = 15066597
    ?STO:Shelf_Location:Prompt{prop:FontColor} = -1
    ?STO:Shelf_Location:Prompt{prop:Color} = 15066597
    If ?sto:Shelf_Location{prop:ReadOnly} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 15066597
    Elsif ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 8454143
    Else ! If ?sto:Shelf_Location{prop:Req} = True
        ?sto:Shelf_Location{prop:FontColor} = 65793
        ?sto:Shelf_Location{prop:Color} = 16777215
    End ! If ?sto:Shelf_Location{prop:Req} = True
    ?sto:Shelf_Location{prop:Trn} = 0
    ?sto:Shelf_Location{prop:FontStyle} = font:Bold
    ?List{prop:FontColor} = 65793
    ?List{prop:Color}= 16777215
    ?List{prop:Color,2} = 16777215
    ?List{prop:Color,3} = 12937777
    If ?orp:Part_Number{prop:ReadOnly} = True
        ?orp:Part_Number{prop:FontColor} = 65793
        ?orp:Part_Number{prop:Color} = 15066597
    Elsif ?orp:Part_Number{prop:Req} = True
        ?orp:Part_Number{prop:FontColor} = 65793
        ?orp:Part_Number{prop:Color} = 8454143
    Else ! If ?orp:Part_Number{prop:Req} = True
        ?orp:Part_Number{prop:FontColor} = 65793
        ?orp:Part_Number{prop:Color} = 16777215
    End ! If ?orp:Part_Number{prop:Req} = True
    ?orp:Part_Number{prop:Trn} = 0
    ?orp:Part_Number{prop:FontStyle} = font:Bold
    ?STO:Second_Location:Prompt{prop:FontColor} = -1
    ?STO:Second_Location:Prompt{prop:Color} = 15066597
    If ?sto:Second_Location{prop:ReadOnly} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 15066597
    Elsif ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 8454143
    Else ! If ?sto:Second_Location{prop:Req} = True
        ?sto:Second_Location{prop:FontColor} = 65793
        ?sto:Second_Location{prop:Color} = 16777215
    End ! If ?sto:Second_Location{prop:Req} = True
    ?sto:Second_Location{prop:Trn} = 0
    ?sto:Second_Location{prop:FontStyle} = font:Bold
    ?Prompt4{prop:FontColor} = -1
    ?Prompt4{prop:Color} = 15066597

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
DASBRW::11:DASTAGONOFF Routine
  GET(Queue:Browse,CHOICE(?List))
  BRW6.UpdateBuffer
   GLO:Queue.Pointer = orp:Record_Number
   GET(GLO:Queue,GLO:Queue.Pointer)
  IF ERRORCODE()
     GLO:Queue.Pointer = orp:Record_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
    tmp:tag = '*'
  ELSE
    DELETE(GLO:Queue)
    tmp:tag = ''
  END
    Queue:Browse.tmp:tag = tmp:tag
  IF (tmp:tag = '*')
    Queue:Browse.tmp:tag_Icon = 2
  ELSE
    Queue:Browse.tmp:tag_Icon = 1
  END
  PUT(Queue:Browse)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  BRW6.Reset
  FREE(GLO:Queue)
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     GLO:Queue.Pointer = orp:Record_Number
     ADD(GLO:Queue,GLO:Queue.Pointer)
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASUNTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(GLO:Queue)
  BRW6.Reset
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASREVTAGALL Routine
  ?List{PROPLIST:MouseDownField} = 2
  SETCURSOR(CURSOR:Wait)
  FREE(DASBRW::11:QUEUE)
  LOOP QR# = 1 TO RECORDS(GLO:Queue)
    GET(GLO:Queue,QR#)
    DASBRW::11:QUEUE = GLO:Queue
    ADD(DASBRW::11:QUEUE)
  END
  FREE(GLO:Queue)
  BRW6.Reset
  LOOP
    NEXT(BRW6::View:Browse)
    IF ERRORCODE()
      BREAK
    END
     DASBRW::11:QUEUE.Pointer = orp:Record_Number
     GET(DASBRW::11:QUEUE,DASBRW::11:QUEUE.Pointer)
    IF ERRORCODE()
       GLO:Queue.Pointer = orp:Record_Number
       ADD(GLO:Queue,GLO:Queue.Pointer)
    END
  END
  SETCURSOR
  BRW6.ResetSort(1)
  SELECT(?List,CHOICE(?List))
DASBRW::11:DASSHOWTAG Routine
   CASE DASBRW::11:TAGDISPSTATUS
   OF 0
      DASBRW::11:TAGDISPSTATUS = 1    ! display tagged
      ?DASSHOWTAG{PROP:Text} = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing Tagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing Tagged'
   OF 1
      DASBRW::11:TAGDISPSTATUS = 2    ! display untagged
      ?DASSHOWTAG{PROP:Text} = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Msg}  = 'Showing UnTagged'
      ?DASSHOWTAG{PROP:Tip}  = 'Showing UnTagged'
   OF 2
      DASBRW::11:TAGDISPSTATUS = 0    ! display all
      ?DASSHOWTAG{PROP:Text} = 'Show All'
      ?DASSHOWTAG{PROP:Msg}  = 'Show All'
      ?DASSHOWTAG{PROP:Tip}  = 'Show All'
   END
   DISPLAY(?DASSHOWTAG{PROP:Text})
   BRW6.ResetSort(1)
   SELECT(?List,CHOICE(?List))
   EXIT
!--------------------------------------------------------------------------
! DAS_Tagging
!--------------------------------------------------------------------------
Check_job_status        Routine
    access:jobs.clearkey(job:ref_number_key)
    job:ref_number  = orp:job_number
    If access:jobs.fetch(job:ref_number_key) = Level:Benign
        Case CheckParts('C')
            Of 1
                GetStatus(330,0,'JOB')
            Of 2
                GetStatus(335,0,'JOB')
            Else
                Case CheckParts('W')
                    Of 1
                        GetStatus(330,0,'JOB')
                    Of 2
                        GetStatus(335,0,'JOB')
                    Else
                        GetStatus(345,0,'JOB')
                End!Case CheckParts('W')
        End!Case CheckParts('C')
        end_date" = ''
        access:jobs.update()
   End !If access:jobs.fetch(job:ref_number_key) = Level:Benign
        
Get_Record_Numbers      Routine
        setcursor(cursor:wait)
        save_par_id = access:parts.savefile()
        access:parts.clearkey(par:order_number_key)
        par:ref_number   = orp:job_number
        par:order_number = orp:order_number
        set(par:order_number_key,par:order_number_key)
        loop
            if access:parts.next()
               break
            end !if
            if par:ref_number   <> orp:job_number      |
            or par:order_number <> orp:order_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If par:date_received = '' And par:part_number = orp:part_number
                parts_record_number_temp = par:record_number
                Break
            End!If par:date_received = '' And par:part_number = orp:part_number
        end !loop
        access:parts.restorefile(save_par_id)

        save_wpr_id = access:warparts.savefile()
        access:warparts.clearkey(wpr:order_number_key)
        wpr:ref_number   = orp:job_number
        wpr:order_number = orp:order_number
        set(wpr:order_number_key,wpr:order_number_key)
        loop
            if access:warparts.next()
               break
            end !if
            if wpr:ref_number   <> orp:job_number      |
            or wpr:order_number <> orp:order_number      |
                then break.  ! end if
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If wpr:date_received = '' And wpr:part_number = orp:part_number
                warparts_record_number_temp = wpr:record_number
                Break
            End!If wpr:date_received = '' And wpr:part_number = orp:part_number
        end !loop
        access:warparts.restorefile(save_wpr_id)

        save_res_id = access:retstock.savefile()
        access:retstock.clearkey(res:order_number_key)
        res:ref_number  = orp:job_number
        res:order_number    = orp:order_number
        Set(res:order_number_key,res:order_number_key)
        Loop
            if access:retstock.next()
                break
            End
            if res:ref_number   <> orp:job_number   |
            or res:order_number <> orp:order_number |
                then break.
            yldcnt# += 1
            if yldcnt# > 25
               yield() ; yldcnt# = 0
            end !if
            If res:date_received = '' And res:part_number = orp:part_number
                retstock_record_number_temp = res:record_number
                Break
            End!If wpr:date_received = '' And wpr:part_number = orp:part_number
        End!Loop
        access:retstock.restorefile(save_res_id)

        setcursor()

AllReceived     Routine
!    orp:record_number   = brw6.q.orp:record_number
!    access:ordparts.clearkey(orp:record_number_key)
!    orp:record_number = brw6.q.orp:record_number
!    if access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
!        Do check_job_status
!    End!if access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
    found# = 0

    setcursor(cursor:wait)
    save_orp_id = access:ordparts.savefile()
    access:ordparts.clearkey(orp:order_number_key)
    orp:order_number    = brw1.q.ord:order_number
    set(orp:order_number_key,orp:order_number_key)
    loop
        if access:ordparts.next()
           break
        end !if
        if orp:order_number    <> brw1.q.ord:order_number      |
            then break.  ! end if
        If orp:all_received <> 'YES'
            found# = 1
            Break
        End
    end !loop
    access:ordparts.restorefile(save_orp_id)
    setcursor()

    if found# = 0
        Access:ORDERS.Clearkey(ord:Order_Number_Key)
        ord:Order_Number    = brw1.q.ord:Order_Number
        If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
            !Found
            ord:all_received = 'YES'
            access:orders.update()
            Case MessageEx('This order has been fulfilled as all parts have now been received.<13,10><13,10>It will now be removed from the Outstanding Orders List.<13,10><13,10>(This message will close automatically)','ServiceBase 2000','Styles\idea.ico','|&Close',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemasterisk,msgex:samewidths,84,26,800) 
                Of 1 ! &Close Button
            End!Case MessageEx

        Else! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
            !Error
            !Assert(0,'<13,10>Fetch Error<13,10>')
        End! If Access:ORDERS.Tryfetch(ord:Order_Number_Key) = Level:Benign
        
    End
    glo:select1 = ''
    glo:select2 = ''
    glo:select3 = ''
getnextrecord2      routine
    recordsprocessed += 1
    recordsthiscycle += 1
    if percentprogress < 100
      percentprogress = (recordsprocessed / recordstoprocess)*100
      if percentprogress > 100
        percentprogress = 100
      end
      if percentprogress <> progress:thermometer then
        progress:thermometer = percentprogress
        ?progress:pcttext{prop:text} = format(percentprogress,@n3) & '% Completed'
      end
    end
    Display()

cancelcheck         routine
    cancel# = 0
    tmp:cancel = 0
    accept
        Case Event()
            Of Event:Timer
                Break
            Of Event:CloseWindow
                cancel# = 1
                Break
            Of Event:accepted
                If Field() = ?Cancel
                    cancel# = 1
                    Break
                End!If Field() = ?Button1
        End!Case Event()
    End!accept
    If cancel# = 1
        Case MessageEx('Are you sure you want to cancel?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                tmp:cancel = 1
            Of 2 ! &No Button
        End!Case MessageEx
    End!If cancel# = 1


endprintrun         routine
    progress:thermometer = 100
    ?progress:pcttext{prop:text} = '100% Completed'
    close(progresswindow)
    display()


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Browse_Parts_Orders',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Browse_Parts_Orders',1)
    SolaceViewVars('tmp:LabelType',tmp:LabelType,'Browse_Parts_Orders',1)
    SolaceViewVars('save_res_id',save_res_id,'Browse_Parts_Orders',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Browse_Parts_Orders',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Browse_Parts_Orders',1)
    SolaceViewVars('retstock_record_number_temp',retstock_record_number_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('parts_record_number_temp',parts_record_number_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('warparts_record_number_temp',warparts_record_number_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Browse_Parts_Orders',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Browse_Parts_Orders',1)
    SolaceViewVars('Received_Temp',Received_Temp,'Browse_Parts_Orders',1)
    SolaceViewVars('outstanding_temp',outstanding_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('job_number_temp',job_number_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('type_temp',type_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('model_temp',model_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('account_temp',account_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('exchange_temp',exchange_temp,'Browse_Parts_Orders',1)
    SolaceViewVars('tmp:tag',tmp:tag,'Browse_Parts_Orders',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Browse:1;  SolaceCtrlName = '?Browse:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Close;  SolaceCtrlName = '?Close';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:2;  SolaceCtrlName = '?Tab:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?outstanding_temp;  SolaceCtrlName = '?outstanding_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?outstanding_temp:Radio2;  SolaceCtrlName = '?outstanding_temp:Radio2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?outstanding_temp:Radio1;  SolaceCtrlName = '?outstanding_temp:Radio1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ord:Order_Number;  SolaceCtrlName = '?ord:Order_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete:3;  SolaceCtrlName = '?Delete:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Reprint_Order_Button;  SolaceCtrlName = '?Reprint_Order_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab2;  SolaceCtrlName = '?Tab2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt3;  SolaceCtrlName = '?Prompt3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Shelf_Location:Prompt;  SolaceCtrlName = '?STO:Shelf_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Shelf_Location;  SolaceCtrlName = '?sto:Shelf_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?List;  SolaceCtrlName = '?List';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?orp:Part_Number;  SolaceCtrlName = '?orp:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STO:Second_Location:Prompt;  SolaceCtrlName = '?STO:Second_Location:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?sto:Second_Location;  SolaceCtrlName = '?sto:Second_Location';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?All_Recieved_Button;  SolaceCtrlName = '?All_Recieved_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Delete_Part_Button;  SolaceCtrlName = '?Delete_Part_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Received_Button;  SolaceCtrlName = '?Received_Button';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ReceiveProcess;  SolaceCtrlName = '?ReceiveProcess';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAG;  SolaceCtrlName = '?DASTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASTAGAll;  SolaceCtrlName = '?DASTAGAll';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASUNTAGALL;  SolaceCtrlName = '?DASUNTAGALL';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASREVTAG;  SolaceCtrlName = '?DASREVTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?DASSHOWTAG;  SolaceCtrlName = '?DASSHOWTAG';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Box1;  SolaceCtrlName = '?Box1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt4;  SolaceCtrlName = '?Prompt4';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Ask PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeAccept
  Xplore6.GetBbSize('Browse_Parts_Orders','?List')    !Xplore
  BRW6.SequenceNbr = 0                                !Xplore
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Browse_Parts_Orders')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Browse_Parts_Orders')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?Browse:1
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.AddItem(?Close,RequestCancelled)
  Relate:ACCAREAS_ALIAS.Open
  Relate:DEFAULTS.Open
  Relate:JOBS.Open
  Relate:LOCATION_ALIAS.Open
  Relate:ORDPARTS_ALIAS.Open
  Relate:PARTS_ALIAS.Open
  Relate:PICKDET.Open
  Relate:PICKNOTE.Open
  Relate:RETSALES.Open
  Relate:RETSALES_ALIAS.Open
  Relate:RETSTOCK_ALIAS.Open
  Relate:STATUS.Open
  Relate:USERS_ALIAS.Open
  Relate:WARPARTS_ALIAS.Open
  Access:PARTS.UseFile
  Access:STOCK.UseFile
  Access:STOHIST.UseFile
  Access:USERS.UseFile
  Access:JOBSTAGE.UseFile
  Access:WARPARTS.UseFile
  Access:ORDPEND.UseFile
  Access:RETSTOCK.UseFile
  Access:SUBTRACC.UseFile
  SELF.FilesOpened = True
  !Last Despatch Number
  glo:select24 = ''
  BRW1.Init(?Browse:1,Queue:Browse:1.ViewPosition,BRW1::View:Browse,Queue:Browse:1,Relate:ORDERS,SELF)
  BRW6.Init(?List,Queue:Browse.ViewPosition,BRW6::View:Browse,Queue:Browse,Relate:ORDPARTS,SELF)
  OPEN(QuickWindow)
  SELF.Opened=True
  Set(DEFAULTS)
  Access:DEFAULTS.Next()
  Case def:SummaryOrders
      Of 0
          ?Received_Button{prop:hide} = 0
          ?All_Recieved_Button{prop:hide} = 0
          ?DASTAG{prop:hide} = 0
          ?DASTAGALL{prop:hide} = 0
          ?DASUNTAGALL{prop:hide} = 0
      Of 1
          ?ReceiveProcess{prop:hide} = 0
          brw6.ChangeControl = 0
  End!def:SummaryOrders
  
  
  Do RecolourWindow
  ?Browse:1{prop:vcr} = TRUE
  ?List{prop:vcr} = TRUE
  !Extension Templete Code:XploreOOPBrowse,AfterWindowOpening
  !Extension Templete Code:XploreOOPBrowse,ProcedureSetup
  Xplore6.Init(ThisWindow,BRW6,Queue:Browse,QuickWindow,?List,'sbb01app.INI','>Header',0,BRW6.ViewOrder,Xplore6.RestoreHeader,BRW6.SequenceNbr,XploreMask6,XploreMask16,XploreTitle6,BRW6.FileSeqOn)
  ! support for CPCS
  BRW1.Q &= Queue:Browse:1
  BRW1::Sort1:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort1:StepClass,ord:Received_Key)
  BRW1.AddRange(ord:All_Received,outstanding_temp)
  BRW1.AddLocator(BRW1::Sort1:Locator)
  BRW1::Sort1:Locator.Init(?ORD:Order_Number,ord:Order_Number,1,BRW1)
  BRW1::Sort2:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort2:StepClass,ord:Order_Number_Key)
  BRW1.AddLocator(BRW1::Sort2:Locator)
  BRW1::Sort2:Locator.Init(?ord:Order_Number,ord:Order_Number,1,BRW1)
  BRW1::Sort0:StepClass.Init(+ScrollSort:AllowAlpha)
  BRW1.AddSortOrder(BRW1::Sort0:StepClass,ord:Order_Number_Key)
  BRW1.AddLocator(BRW1::Sort0:Locator)
  BRW1::Sort0:Locator.Init(?ord:Order_Number,ord:Order_Number,1,BRW1)
  BRW1.AddField(ord:Order_Number,BRW1.Q.ord:Order_Number)
  BRW1.AddField(ord:Supplier,BRW1.Q.ord:Supplier)
  BRW1.AddField(ord:User,BRW1.Q.ord:User)
  BRW1.AddField(ord:Date,BRW1.Q.ord:Date)
  BRW1.AddField(ord:Printed,BRW1.Q.ord:Printed)
  BRW1.AddField(ord:All_Received,BRW1.Q.ord:All_Received)
  BRW6.Q &= Queue:Browse
  BRW6.AddSortOrder(,orp:Part_Number_Key)
  BRW6.AddRange(orp:Order_Number,Relate:ORDPARTS,Relate:ORDERS)
  BRW6.AddLocator(BRW6::Sort0:Locator)
  BRW6::Sort0:Locator.Init(?orp:Part_Number,orp:Part_Number,1,BRW6)
  BIND('tmp:tag',tmp:tag)
  BIND('type_temp',type_temp)
  BIND('job_number_temp',job_number_temp)
  BIND('model_temp',model_temp)
  BIND('account_temp',account_temp)
  BIND('exchange_temp',exchange_temp)
  ?List{PROP:IconList,1} = '~notick1.ico'
  ?List{PROP:IconList,2} = '~tick1.ico'
  BRW6.AddField(tmp:tag,BRW6.Q.tmp:tag)
  BRW6.AddField(orp:Part_Number,BRW6.Q.orp:Part_Number)
  BRW6.AddField(orp:Description,BRW6.Q.orp:Description)
  BRW6.AddField(type_temp,BRW6.Q.type_temp)
  BRW6.AddField(job_number_temp,BRW6.Q.job_number_temp)
  BRW6.AddField(orp:Quantity,BRW6.Q.orp:Quantity)
  BRW6.AddField(orp:Purchase_Cost,BRW6.Q.orp:Purchase_Cost)
  BRW6.AddField(orp:Sale_Cost,BRW6.Q.orp:Sale_Cost)
  BRW6.AddField(orp:Retail_Cost,BRW6.Q.orp:Retail_Cost)
  BRW6.AddField(orp:Date_Received,BRW6.Q.orp:Date_Received)
  BRW6.AddField(orp:All_Received,BRW6.Q.orp:All_Received)
  BRW6.AddField(model_temp,BRW6.Q.model_temp)
  BRW6.AddField(account_temp,BRW6.Q.account_temp)
  BRW6.AddField(exchange_temp,BRW6.Q.exchange_temp)
  BRW6.AddField(orp:Part_Ref_Number,BRW6.Q.orp:Part_Ref_Number)
  BRW6.AddField(sto:Second_Location,BRW6.Q.sto:Second_Location)
  BRW6.AddField(sto:Shelf_Location,BRW6.Q.sto:Shelf_Location)
  BRW6.AddField(orp:Record_Number,BRW6.Q.orp:Record_Number)
  BRW6.AddField(orp:Order_Number,BRW6.Q.orp:Order_Number)
  BRW6.AddField(sto:Ref_Number,BRW6.Q.sto:Ref_Number)
  BRW6.AddField(sto:Part_Number,BRW6.Q.sto:Part_Number)
  BRW6.AddField(sto:Description,BRW6.Q.sto:Description)
  QuickWindow{PROP:MinWidth}=668
  QuickWindow{PROP:MinHeight}=240
  Resizer.Init(AppStrategy:Spread)
  SELF.AddItem(Resizer)
  Case def:SummaryORders
      Of 1
          ?List{Prop:IconList,1} = ''
          ?List{Prop:IconList,2} = ''
  End !Case def:SummaryORders
  BRW1.AskProcedure = 1
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  ?DASSHOWTAG{PROP:Text} = 'Show All'
  ?DASSHOWTAG{PROP:Msg}  = 'Show All'
  ?DASSHOWTAG{PROP:Tip}  = 'Show All'
  !--------------------------------------------------------------------------
  ! Tagging Init
  !--------------------------------------------------------------------------
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
    ?Tab2{PROP:TEXT} = 'By Part Number'
    ?Tab3{PROP:TEXT} = 'By Part Number'
    ?List{PROP:FORMAT} ='11L(2)I@s1@#1#110L(2)|M*~Part Number~@s30@#3#109L(2)|M*~Description~@s30@#8#25L(2)|M*~Type~@s3@#13#35R(2)|M*~Ref No~L@s8@#18#38L(2)|M*~Quantity~@p<<<<<<<#p@#23#35R(2)|M*~Purchase~@n7.2@#28#34R(2)|M*~Sale~@n7.2@#33#34R(2)|M*~Retail~@n7.2@#38#35L(2)|M*~Model~@s30@#49#35L(2)|M*~Account~@s15@#54#35L(2)|M*~Exchange~@s1@#59#40R(2)|M*~Date Rcvd~@d6b@#43#'
  !--------------------------------------------------------------------------
  ! Tinman Browse Reformat
  !--------------------------------------------------------------------------
  ! End Change 4803 BE(13/10/2004)
  IF INSTRING('INTEC',def:User_Name,1,1) THEN
    ?List{PROP:FORMAT} ='11L(2)J@s1@#1#110L(2)|M*~Part Number~@s30@#3#109L(2)|M*~Description~@s30@#8#23L(2)|M*~Type~@s3@#13#35R(2)|M*~Ref No~L@s8@#18#38L(2)|M*~Quantity~@p<<<<<<<#p@#23#35L(2)|M*~Model~@s30@#49#35L(2)|M*~Account~@s15@#54#35L(2)|M*~Exchange~@s1@#59#35R(2)|M*~Purchase~@n7.2@#28#34R(2)|M*~Sale~@n7.2@#33#34R(2)|M*~Retail~@n7.2@#38#40R(2)|M*~Date Rcvd~@d6b@#43#'
  END
  ! End Change 4803 BE(13/10/2004)
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  ReturnValue = PARENT.Kill()
  !Last Despatch Number
  Clear(glo:g_select1)
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
  FREE(GLO:Queue)
  !--------------------------------------------------------------------------
  ! Tagging Kill
  !--------------------------------------------------------------------------
    Relate:ACCAREAS_ALIAS.Close
    Relate:DEFAULTS.Close
    Relate:JOBS.Close
    Relate:LOCATION_ALIAS.Close
    Relate:ORDPARTS_ALIAS.Close
    Relate:PARTS_ALIAS.Close
    Relate:PICKDET.Close
    Relate:PICKNOTE.Close
    Relate:RETSALES.Close
    Relate:RETSALES_ALIAS.Close
    Relate:RETSTOCK_ALIAS.Close
    Relate:STATUS.Close
    Relate:USERS_ALIAS.Close
    Relate:WARPARTS_ALIAS.Close
    !Extension Templete Code:XploreOOPBrowse,AfterFileClose
    Xplore6.EraseVisual()                             !Xplore
  END
  !Extension Templete Code:XploreOOPBrowse,BeforeWindowClosing
  IF ThisWindow.Opened                                !Xplore
    Xplore6.sq.Col = Xplore6.CurrentCol
    GET(Xplore6.sq,Xplore6.sq.Col)
    IF Xplore6.ListType = 1 AND BRW6.ViewOrder = False !Xplore
      BRW6.SequenceNbr = 0                            !Xplore
    END                                               !Xplore
    Xplore6.PutInix('Browse_Parts_Orders','?List',BRW6.SequenceNbr,Xplore6.sq.AscDesc) !Xplore
  END                                                 !Xplore
  !Extension Templete Code:XploreOOPBrowse,EndOfProcedure
  Xplore6.Kill()                                      !Xplore
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Browse_Parts_Orders',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Kill, (),BYTE)
  RETURN ReturnValue


ThisWindow.Run PROCEDURE(USHORT Number,BYTE Request)

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  ReturnValue = PARENT.Run(Number,Request)
  do_update# = 1
  If request = DeleteRecord
      If SecurityCheck('PARTS ORDERS - DELETE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
          Do_Update# = 0
      Else !If SecurityCheck('PARTS ORDERS - DELETE')
          Found# = 0
          FoundJobs# = 0
          setcursor(cursor:wait)
          save_orp_id = access:ordparts.savefile()
          access:ordparts.clearkey(orp:part_number_key)
          orp:order_number = brw1.q.ord:order_number
          set(orp:part_number_key,orp:part_number_key)
          loop
              if access:ordparts.next()
                 break
              end !if
              if orp:order_number <> ord:order_number      |
                  then break.  ! end if
              yldcnt# += 1
              if yldcnt# > 25
                 yield() ; yldcnt# = 0
              end !if
              !Do not allow to delete if there are parts from jobs onthe order
              If orp:Part_Type <> 'STO' And orp:Date_Received = 0
                  FoundJobs# = 1
                  Break
              End !If orp:Part_Type <> 'STO'
              If orp:Date_Received = 0
                  found# = 1
                  Break
              End
  
          end !loop
          access:ordparts.restorefile(save_orp_id)
          setcursor()
          If FoundJobs# = 1
              Case MessageEx('Error! There are parts required for jobs on this order. You must remove these parts from the selected jobs before you can delete the order.','ServiceBase 2000',|
                             'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                  Of 1 ! &OK Button
              End!Case MessageEx
              Do_Update# = 0
          End !If FoundJobs# = 1
          If found# = 1
              Case MessageEx('Warning! There are unreceived parts on this order.'&|
                '<13,10>'&|
                '<13,10>If you continue all record of this order will be lost. Are you sure?','ServiceBase 2000',|
                             'Styles\trash.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,700,CHARSET:ANSI,8421631,'',0,48,msgex:samewidths,84,26,0) 
                  Of 1 ! &Yes Button
                  Of 2 ! &No Button
                      do_update# = 0
              End!Case MessageEx
          End
      End !If SecurityCheck('PARTS ORDERS - DELETE')
  
  End!If request = DeleteRecord
  If do_update# = 1
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  ELSE
    GlobalRequest = Request
    UpdateORDERS
    ReturnValue = GlobalResponse
  END
  End!If do_update# = 1
  
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Run, (USHORT Number,BYTE Request),BYTE)
  RETURN ReturnValue


ThisWindow.SetAlerts PROCEDURE

  CODE
  Xplore6.Upper = True                                !Xplore
  PARENT.SetAlerts
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'SetAlerts',PRIORITY(8000)
  Xplore6.AddField(tmp:tag,BRW6.Q.tmp:tag)
  Xplore6.AddField(orp:Part_Number,BRW6.Q.orp:Part_Number)
  Xplore6.AddField(orp:Description,BRW6.Q.orp:Description)
  Xplore6.AddField(type_temp,BRW6.Q.type_temp)
  Xplore6.AddField(job_number_temp,BRW6.Q.job_number_temp)
  Xplore6.AddField(orp:Quantity,BRW6.Q.orp:Quantity)
  Xplore6.AddField(orp:Purchase_Cost,BRW6.Q.orp:Purchase_Cost)
  Xplore6.AddField(orp:Sale_Cost,BRW6.Q.orp:Sale_Cost)
  Xplore6.AddField(orp:Retail_Cost,BRW6.Q.orp:Retail_Cost)
  Xplore6.AddField(orp:Date_Received,BRW6.Q.orp:Date_Received)
  Xplore6.AddField(orp:All_Received,BRW6.Q.orp:All_Received)
  Xplore6.AddField(model_temp,BRW6.Q.model_temp)
  Xplore6.AddField(account_temp,BRW6.Q.account_temp)
  Xplore6.AddField(exchange_temp,BRW6.Q.exchange_temp)
  Xplore6.AddField(orp:Part_Ref_Number,BRW6.Q.orp:Part_Ref_Number)
  Xplore6.AddField(sto:Second_Location,BRW6.Q.sto:Second_Location)
  Xplore6.AddField(sto:Shelf_Location,BRW6.Q.sto:Shelf_Location)
  Xplore6.AddField(orp:Record_Number,BRW6.Q.orp:Record_Number)
  Xplore6.AddField(orp:Order_Number,BRW6.Q.orp:Order_Number)
  Xplore6.AddField(sto:Ref_Number,BRW6.Q.sto:Ref_Number)
  Xplore6.AddField(sto:Part_Number,BRW6.Q.sto:Part_Number)
  Xplore6.AddField(sto:Description,BRW6.Q.sto:Description)
  BRW6.FileOrderNbr = BRW6.AddSortOrder(,orp:Part_Number_Key) !Xplore Sort Order for File Sequence
  BRW6.AddRange(orp:Order_Number,Relate:ORDPARTS,Relate:ORDERS) !Xplore
  BRW6.SetOrder('')                                   !Xplore
  BRW6.ViewOrder = True                               !Xplore
  Xplore6.AddAllColumnSortOrders(1)                   !Xplore
  BRW6.ViewOrder = False                              !Xplore


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
    CASE ACCEPTED()
    OF ?Reprint_Order_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
      ThisWindow.Update
      Clear(glo:q_partsorder)
      Free(glo:q_partsorder)
      glo:select1    = ord:order_number
      !Neils Bit - because someone omitted it....tum te tum
      Access:OrdParts.ClearKey(ORP:Order_Number_key)
      ORP:Order_Number = ORD:Order_Number
      SET(ORP:Order_Number_Key,ORP:Order_Number_Key)
      LOOP
        IF Access:OrdParts.Next()
          BREAK
        END
        IF ORP:Order_Number <> ORD:Order_Number
          BREAK
        END
        glo:q_order_number      = orp:order_number
        glo:q_part_number       = orp:part_number
        glo:q_description       = orp:description
        glo:q_supplier          = ord:supplier
        glo:q_purchase_cost     = orp:purchase_cost
        glo:q_sale_cost         = orp:sale_cost
      
        Sort(glo:q_partsorder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
        Get(glo:q_partsorder,glo:q_order_number,glo:q_part_number,glo:q_description,glo:q_purchase_cost)
        If Error()
            glo:q_quantity          = orp:quantity
            Add(glo:q_partsorder)
        Else
            glo:q_quantity          += orp:quantity
            Put(glo:q_partsorder)
        End
      END
      !Bryans bit again
      ord:printed = 'NO'
      Access:orders.update()
      
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
    OF ?All_Recieved_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?All_Recieved_Button, Accepted)
      Case MessageEx('This will mark ALL the tagged parts on this order as received.<13,10><13,10>Are you sure?','ServiceBase 2000','styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,0+2+0+0,84,26,0)
          Of 1 ! &Yes Button
              tmp:LabelType = 0
              set(DEFAULTS)
              Access:DEFAULTS.Next()
              If def:Receive_Stock_Label = 'YES'
                  Case MessageEx('Label(s) will now be printed. Do you wish to:'&|
                    '<13,10>'&|
                    '<13,10>a) Print a label per line'&|
                    '<13,10>'&|
                    '<13,10>b) Print a label per item?'&|
                    '<13,10>','ServiceBase 2000',|
                                 'Styles\question.ico','|&Label Per Line|&Label Per Item|&Skip Label Print',1,1,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
                      Of 1 ! &Label Per Line Button
                          tmp:LabelType = 1
                      Of 2 ! &Label Per Item Button
                          tmp:LabelType = 2
                      Of 3 ! &Skip Label Print Button
                          tmp:LabelType = 0
                  End!Case MessageEx
              End!If def:stock_label = 'YES'
      
              glo:select2  = ''
              Receive_All_Parts
      
              ThisWindow.Update
      
              setcursor(cursor:wait)
      
              recordspercycle     = 25
              recordsprocessed    = 0
              percentprogress     = 0
              setcursor(cursor:wait)
              open(progresswindow)
              progress:thermometer    = 0
              ?progress:pcttext{prop:text} = '0% Completed'
      
              recordstoprocess    = records(glo:queue)
      
              Loop x# = 1 To Records(glo:queue)
                  get(glo:queue,x#)
                  access:ordparts.clearkey(orp:record_number_key)
                  orp:record_number   = glo:pointer
                  If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                      !Found
      
                  Else! If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
                      !Error
                      Cycle
                  End! If access:ordparts.tryfetch(orp:record_number_key) = Level:Benign
      
                  Do GetNextRecord2
                  If orp:all_received = 'YES'
                      Cycle
                  End!If orp:all_received = 'YES'
      
                  ! Add a bit of error checking - TrkBs: 5665 (DBH: 11-05-2005)
                  If orp:Part_Type = 'JOB' Or orp:Part_Type = 'WAR'
      
                      ! Is this part for a job? - TrkBs: 5665 (DBH: 11-05-2005)
                      Access:JOBS.Clearkey(job:Ref_Number_Key)
                      job:Ref_Number  = orp:Job_Number
                      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          ! Found the job. Is it in use? - TrkBs: 5665 (DBH: 11-05-2005)
                          Pointer# = Pointer(JOBS)
                          Hold(JOBS, 1)
                          Get(JOBS, Pointer#)
                          If Errorcode() = 43
                              Case MessageEx('Cannot update Job Number ' & job:Ref_Number & '.' &|
                                             '<13,10>' &|
                                             '<13,10>It is currently in use by another station.' &|
                                             '<13,10>', 'ServiceBase 2000', |
                                             'Styles\stop.ico', '|&OK', 1, 1, '',, 'Tahoma', 8, 0, 700, CHARSET:ANSI, 14084079, '', 0, beep:systemhand, msgex:samewidths, 84, 26, 0)
                              Of 1 ! &OK Button
                                  Cycle
                              End! Case MessageEx
                          End ! If Errorcode() = 43
                          Release(JOBS)
      
                      Else ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
                          ! Can't find the job - TrkBs: 5665 (DBH: 11-05-2005)
                          Case MessageEx('Cannot find Job Number ' & Clip(orp:Job_Number) & '.', 'ServiceBase 2000', |
                                         'Styles\stop.ico', '|&OK', 1, 1, '',, 'Tahoma', 8, 0, 700, CHARSET:ANSI, 14084079, '', 0, beep:systemhand, msgex:samewidths, 84, 26, 0)
                          Of 1 ! &OK Button
                          End! Case MessageEx
                      End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
                      ! Set job status to "ordered", "received", or "requested" - TrkBs: 5665 (DBH: 11-05-2005)
                      Do check_job_status
      
                  End ! If orp:Part_Type = 'JOB' Or orp:Part_Type = 'WAR'
                  ! End   - Add a bit of error checking - TrkBs: 5665 (DBH: 11-05-2005)
      
                  orp:all_received = 'YES'
                  orp:date_received = Today()
                  orp:DespatchNoteNumber = glo:select2
                  access:ordparts.update()
      
                  parts_record_number_temp = ''
                  warparts_record_number_temp = ''
                  Do Get_Record_Numbers
      
                  quantity_to_job$ = 0
                  Case orp:part_type
                      Of 'JOB'
                          access:parts.clearkey(par:RecordNumberKey)
                          par:record_number = parts_record_number_temp
                          if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                              par:date_received = Today()
                              par:despatch_note_number    = glo:select2
                              access:parts.update()
      
                              ! Start Change 3505 BE(10/11/03)
                              ! Start Change 4803 BE(12/10/2004)
                              !UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, par:quantity, par:despatch_note_number, par:record_number)
                              ! End Change 4803 BE(12/10/2004)
                              ! End Change 3505 BE(10/11/03)
      
                              Do check_job_status
      
                              ! Start Change 4803 BE(12/10/2004)
                              UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, par:quantity, par:despatch_note_number, par:ref_number)
                              ! End Change 4803 BE(12/10/2004)
      
                              ! Start Change 4797 BE(22/10/2004)
                              UpdatePickPartsReceived(par:ref_number, par:record_number, orp:quantity)
                              ! End Change 4797 BE(22/10/2004)
      
                          end!if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                          access:stock.clearkey(sto:ref_number_key)
                          sto:ref_number = orp:part_ref_number
                          If access:stock.fetch(sto:ref_number_key) = Level:Benign
                  !            sto:quantity_stock += (orp:quantity - quantity_to_job$)
                              sto:quantity_on_order -= orp:quantity
                              If sto:quantity_on_order < 0
                                  sto:quantity_on_order = 0
                              End
                              If sto:quantity_stock < 0
                                  sto:quantity_stock = 0
                              End
                              access:stock.update()
                          End!If access:stock.fetch(sto:ref_number_key) = Level:Benign                
                          
                      Of 'WAR'
                          access:warparts.clearkey(wpr:RecordNumberKey)
                          wpr:record_number = warparts_record_number_temp
                          if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                              wpr:date_received    = Today()
                              wpr:despatch_note_number    = glo:select2
                              access:warparts.update()
      
                              ! Start Change 3505 BE(10/11/03)
                              ! Start Change 4803 BE(12/10/2004)
                              !UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, wpr:quantity, wpr:despatch_note_number, wpr:record_number)
                              ! Start Change 4803 BE(12/10/2004)
                              ! End Change 3505 BE(10/11/03)
      
                              Do check_job_status
      
                              ! Start Change 4803 BE(12/10/2004)
                              UpdateStockHistory(orp:part_ref_number, orp:order_number, ord:supplier, wpr:quantity, wpr:despatch_note_number, wpr:ref_number)
                              ! Start Change 4803 BE(12/10/2004)
      
                              ! Start Change 4797 BE(22/10/2004)
                              UpdatePickPartsReceived(wpr:ref_number, wpr:record_number, orp:quantity)
                              ! End Change 4797 BE(22/10/2004)
                          End!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                          access:stock.clearkey(sto:ref_number_key)
                          sto:ref_number = orp:part_ref_number
                          If access:stock.fetch(sto:ref_number_key) = Level:Benign
                  !            sto:quantity_stock += (orp:quantity - quantity_to_job$)
                              sto:quantity_on_order -= orp:quantity
                              If sto:quantity_on_order < 0
                                  sto:quantity_on_order = 0
                              End
                              If sto:quantity_stock < 0
                                  sto:quantity_stock = 0
                              End
                              access:stock.update()
                          End!If access:stock.fetch(sto:ref_number_key) = Level:Benign                
                          
                      Of 'STO' Orof 'RET'
                          StockPart# = 1
                          Access:STOCK.ClearKey(sto:Ref_Number_Key)
                          sto:Ref_Number = orp:Part_Ref_Number
                          If Access:Stock.fetch(sto:Ref_Number_Key)
                              !Used for Retail Sales, there will be no specific part number ordered.
                              !Therefore try and find the part in the Main Store.
                              Access:STOCK.ClearKey(sto:Location_Key)
                              sto:Location    = MainStoreLocation()
                              sto:Part_Number = orp:Part_Number
                              If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                  !Found
      
                              Else!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                                  !Error
                                  !Assert(0,'<13,10>Fetch Error<13,10>')
                                  Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                                      Of 1 ! &OK Button
                                  End!Case MessageEx
                                  StockPart# = 0
                              End!If Access:STOCK.TryFetch(sto:Location_Key) = Level:Benign
                          End!If Access:Stock.fetch(sto:Ref_Number_Key)
                          If StockPart# = 1
                              sto:quantity_stock += orp:quantity
                              sto:quantity_on_order -= orp:quantity
                              If sto:quantity_on_order < 0
                                  sto:quantity_on_order = 0
                              End
                              If sto:quantity_stock < 0
                                  sto:quantity_stock = 0
                              End!If sto:quantity_stock < 0
                              access:stock.update()
                              get(stohist,0)
                              if access:stohist.primerecord() = Level:Benign
                                  shi:ref_number           = sto:ref_number
                                  shi:transaction_type     = 'ADD'
                                  shi:despatch_note_number = glo:select2
                                  shi:quantity             = orp:quantity
                                  shi:date                 = Today()
                                  shi:purchase_cost        = sto:purchase_cost
                                  shi:sale_cost            = sto:sale_cost
                                  shi:retail_cost          = sto:retail_cost
                                  shi:job_number           = ''
                                  shi:information          = 'ORDER NUMBER: ' & Clip(orp:order_number) & '<13,10>SUPPLIER: ' & Clip(ord:supplier)
                                  access:users.clearkey(use:password_key)
                                  use:password    = glo:password
                                  access:users.fetch(use:password_key)
                                  shi:user                 = use:user_code
                                  shi:notes                = 'STOCK ADDED FROM ORDER'
                                  if access:stohist.insert()
                                     access:stohist.cancelautoinc()
                                  end
                              end!if access:stohist.primerecord() = Level:Benign
                          end !if access:stock.fetch(sto:ref_number_key)
      !                Of 'RET'
      !                    orp:allocated_to_sale = 'NO'
      !                    access:ordparts.update()
      
                  End!Case orp:part_type
      
                  Case tmp:LabelType
                      Of 1
                          glo:select1  = orp:part_ref_number
                          Case def:label_printer_type
                              Of 'TEC B-440 / B-442'
                                  Stock_Label(orp:Date_Received,orp:Quantity,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                              Of 'TEC B-452'
                                  Stock_Label_B452(orp:Date_Received,orp:Quantity,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                          End!Case def:label_printer_type
                          glo:select1 = ''
                      Of 2
                          Loop looptemp# = 1 To orp:Quantity
                              glo:select1  = orp:part_ref_number
                              Case def:label_printer_type
                                  Of 'TEC B-440 / B-442'
                                      Stock_Label(orp:Date_Received,1,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                                  Of 'TEC B-452'
                                      Stock_Label_B452(orp:Date_Received,1,orp:order_number,orp:DespatchNoteNumber,orp:Sale_Cost)
                              End!Case def:label_printer_type
                          End!Loop x# = 1 To no_of_labels_temp
                          glo:select1 = ''
                  End!Case tmp:LabelType
      
              End!Loop x# = 1 To Records(glo:queue)
              setcursor()
              close(progresswindow)
      
              glo:select2  = ''
              Do DASBRW::11:DASUNTAGALL
              Do AllReceived
          Of 2 ! &No Button
      End!Case MessageEx
      BRW1.ResetQueue(1)
      BRW6.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?All_Recieved_Button, Accepted)
    OF ?Received_Button
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
      ThisWindow.Update
      ! Is this part for a job? - TrkBs: 5665 (DBH: 11-05-2005)
      Access:JOBS.Clearkey(job:Ref_Number_Key)
      job:Ref_Number  = orp:Job_Number
      If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
          ! Found the job. Is it in use? - TrkBs: 5665 (DBH: 11-05-2005)
          Pointer# = Pointer(JOBS)
          Hold(JOBS, 1)
          Get(JOBS, Pointer#)
          If Errorcode() = 43
              Case MessageEx('Cannot update. Job Number ' & job:Ref_Number & ' is currently in use by another station.' &|
                             '<13,10>', 'ServiceBase 2000', |
                             'Styles\stop.ico', '|&OK', 1, 1, '',, 'Tahoma', 8, 0, 700, CHARSET:ANSI, 14084079, '', 0, beep:systemhand, msgex:samewidths, 84, 26, 0)
              Of 1 ! &OK Button
                  Cycle
              End! Case MessageEx
          End ! If Errorcode() = 43
          Release(JOBS)
      End ! If Access:JOBS.Tryfetch(job:Ref_Number_Key) = Level:Benign
      
      glo:select1 = ord:order_number
      glo:select2 = orp:record_number
      glo:Select3 = ''
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
    END
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?Browse:1
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Accepted)
      Do DASBRW::11:DASUNTAGALL
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Browse:1, Accepted)
    OF ?outstanding_temp
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?outstanding_temp, Accepted)
      Thiswindow.reset(1)
      Select(?browse:1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?outstanding_temp, Accepted)
    OF ?Reprint_Order_Button
      ThisWindow.Update
      Parts_Order
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
      glo:select1 = ''
      Clear(glo:q_partsorder)
      Free(glo:q_partsorder)
      BRW1.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Reprint_Order_Button, Accepted)
    OF ?Delete_Part_Button
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part_Button, Accepted)
      If SecurityCheck('PARTS ORDERS - DELETE')
          Case MessageEx('You do not have access to this option.','ServiceBase 2000',|
                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
              Of 1 ! &OK Button
          End!Case MessageEx
      Else !SecurityCheck('PARTS ORDERS - DELETE')
      
      
          Thiswindow.reset
          error# = 0
          glo:select1 = orp:record_number
      !    found# = 0
      !    setcursor(cursor:wait)
      !    save_orp_id = access:ordparts.savefile()
      !    access:ordparts.clearkey(orp:part_number_key)
      !    orp:order_number = brw1.q.ord:order_number
      !    set(orp:part_number_key,orp:part_number_key)
      !    loop
      !        if access:ordparts.next()
      !           break
      !        end !if
      !        if orp:order_number <> ord:order_number      |
      !            then break.  ! end if
      !        yldcnt# += 1
      !        if yldcnt# > 25
      !           yield() ; yldcnt# = 0
      !        end !if
      !        If orp:number_received <> ''
      !            found# = 1
      !            Break        
      !        End!    If orp:number_received <> ''
      !    end !loop
      !    access:ordparts.restorefile(save_orp_id)
      !    setcursor()
      !
      !    If found# = 1
      !        Case MessageEx('Unable to Delete.<13,10><13,10>One or more parts on this order has been received.','ServiceBase 2000','styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemhand,0+2+0+0,84,26,0)
      !            Of 1 ! &OK Button
      !        End!Case MessageEx
      !    Else!If found# = 1
      
          Access:ORDPARTS.Clearkey(orp:Record_Number_Key)
          orp:Record_Number   = glo:Select1
          If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
              !Found
              If orp:Part_Type <> 'STO'
                  Case MessageEx('Error! The select part has been ordered from a job. You must remove the part from the job.','ServiceBase 2000',|
                                 'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                      Of 1 ! &OK Button
                  End!Case MessageEx
              Else
              Case MessageEx('You are about to remove this part from Order Number '&Clip(ord:order_number)&'. Do you wish to:<13,10><13,10>1) Mark this part for re-ordering on the next parts order.<13,10><13,10>2) Remove this part from the order.','ServiceBase 2000','styles\Trash.ico','|&Re-Order|&Remove|&Cancel',3,3,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,'''beep:systemquestion''',0+2+0+0,84,26,0)
                  Of 1 ! &Re-Order Button
                          Case orp:part_type
                              Of 'JOB'
                                  get(ordpend,0)
                                  if access:ordpend.primerecord() = level:benign
                                      ope:part_ref_number = orp:part_ref_number
                                      ope:job_number      = orp:job_number
                                      ope:part_type       = 'JOB'
                                      ope:supplier        = ord:supplier
                                      ope:part_number     = orp:part_number
                                      ope:Description     = orp:description
                                      ope:quantity        = orp:quantity
                                      if access:ordpend.insert()
                                          access:ordpend.cancelautoinc()     
                                      end            
                                  end!if access:ordpend.primerecord() = level:benign    
                                  access:parts.clearkey(par:order_part_key)
                                  par:ref_number        = orp:job_number
                                  par:order_part_number = orp:record_number
                                  if access:parts.fetch(par:order_part_key) = Level:Benign
                                      par:date_ordered = ''
                                      par:order_number = ''
                                      par:exclude_from_order = 'YES'
                                      access:parts.update()
                                  end!if access:parts.fetch(par:order_part_key) = Level:Benign                            
                              
                              Of 'WAR'
                                  get(ordpend,0)
                                  if access:ordpend.primerecord() = level:benign
                                      ope:part_ref_number = orp:part_ref_number
                                      ope:job_number      = orp:job_number
                                      ope:part_type       = 'WAR'
                                      ope:supplier        = ord:supplier
                                      ope:part_number     = orp:part_number
                                      ope:quantity        = orp:quantity
                                      ope:description     = orp:description
                                      if access:ordpend.insert()
                                          access:ordpend.cancelautoinc()     
                                      end            
                                  end!if access:ordpend.primerecord() = level:benign    
                                  access:warparts.clearkey(wpr:order_part_key)
                                  wpr:ref_number        = orp:job_number
                                  wpr:order_part_number = orp:record_number
                                  if access:warparts.fetch(wpr:order_part_key) = Level:Benign
                                      wpr:date_ordered = ''
                                      wpr:order_number = ''
                                      wpr:exclude_from_order = 'YES'
                                      access:warparts.update()
                                  end!if access:parts.fetch(par:order_part_key) = Level:Benign                            
                              
                              Of 'STO'
                                  if access:ordpend.primerecord() = level:benign
                                      ope:part_ref_number = orp:part_ref_number
                                      ope:job_number      = orp:job_number
                                      ope:part_type       = 'STO'
                                      ope:supplier        = ord:supplier
                                      ope:part_number     = orp:part_number
                                      ope:description     = orp:description
                                      ope:quantity        = orp:quantity
                                      if access:ordpend.insert()
                                          access:ordpend.cancelautoinc()     
                                      end            
                                  end!if access:ordpend.primerecord() = level:benign
      
                              Of 'RET'
                                  get(ordpend,0)
                                  if access:ordpend.primerecord() = level:benign
      
                                      ope:part_ref_number = orp:part_ref_number
                                      ope:job_number  = orp:job_number
                                      ope:part_type   = 'RET'
                                      ope:supplier    = ord:supplier
                                      ope:part_number = orp:part_number
                                      ope:description   = orp:description
                                      ope:account_number  = orp:account_number
                                      ope:quantity        = orp:quantity
                                      if access:ordpend.insert()
                                          access:ordpend.cancelautoinc()     
                                      end            
                                  end!if access:ordpend.primerecord() = level:benign    
                                  access:parts.clearkey(par:order_part_key)
                                  par:ref_number        = orp:job_number
                                  par:order_part_number = orp:record_number
                                  if access:parts.fetch(par:order_part_key) = Level:Benign
                                      par:date_ordered = ''
                                      par:order_number = ''
                                      par:exclude_from_order = 'YES'
                                      access:parts.update()
                                  end!if access:parts.fetch(par:order_part_key) = Level:Benign                            
      
                          End!Case orp:part_type
                          If orp:Number_Received <> 0
                              get(stohist,0)
                              if access:stohist.primerecord() = level:benign
                                  shi:information          = 'REMOVED FROM ORDER NUMBER: ' & Clip(ord:order_number)
                                  shi:ref_number           = orp:part_ref_number
                                  access:users.clearkey(use:password_key)
                                  use:password = glo:password
                                  access:users.fetch(use:password_key)
                                  shi:user = use:user_code
                                  shi:transaction_type     = 'REC'
                                  shi:despatch_note_number = ''
                                  shi:job_number           = orp:job_number
                                  shi:quantity             = orp:quantity
                                  shi:date                 = Today()
                                  shi:purchase_cost        = orp:purchase_cost
                                  shi:sale_cost            = orp:sale_cost
                                  shi:retail_cost          = orp:retail_cost
                                  shi:notes                = 'STOCK ITEM REMOVE FROM ORDER'
                                  if access:stohist.insert()
                                      access:stohist.cancelautoinc()     
                                  end            
                              end!if access:stohist.primerecord() = level:benign            
      
                          End !If orp:Recieved <> 0
                          Delete(ordparts)
                 
                  Of 2 ! &Remove Button
                          remove# = 0
                          exclude# = 0
                          If orp:job_number <> ''
                              Case MessageEx('This part has been ordered for Job Number: '&clip(job:ref_number)&'. Do you wish to:<13,10><13,10>1) Remove the part from the job<13,10><13,10>2) Exclude the part on the job from ordering.','ServiceBase 2000','styles\Trash.ico','|&Remove From Job|&Exclude From Order|&Cancel',3,3,'',,'Tahoma',8,0,0,CHARSET:ANSI,12632256,'',0,beep:systemquestion,0+2+0+0,84,26,0)
                                  Of 1 ! &Remove From Job Button
                                      Case orp:part_type
                                          Of 'JOB'
                                              access:parts.clearkey(par:order_part_key)
                                              par:ref_number        = orp:job_number
                                              par:order_part_number = orp:record_number
                                              if access:parts.fetch(par:order_part_key) = Level:Benign
                                                  Delete(Parts)
                                              end !if access:parts.fetch(par:order_part_key) = Level:Benign
                                              remove# = 1
                                          
                                          Of 'WAR'
                                              access:warparts.clearkey(wpr:order_part_key)
                                              wpr:ref_number        = orp:job_number
                                              wpr:order_part_number = orp:record_number
                                              if access:warparts.fetch(wpr:order_part_key) = Level:Benign
                                                  Delete(warparts)
                                              end !if access:parts.fetch(par:order_part_key) = Level:Benign
                                              remove# = 1
                                      
                                      End!Case orp:part_type
                                  Of 2 ! &Exclude From Order Button
                                      Case orp:part_type
                                
                                          Of 'JOB'
                                              access:parts.clearkey(par:order_part_key)
                                              par:ref_number        = orp:job_number
                                              par:order_part_number = orp:record_number
                                              if access:parts.fetch(par:order_part_key) = Level:Benign
                                                  par:exclude_from_order = 'YES'
                                                  par:date_ordered = Today()
                                                  par:order_number = ''
                                                  access:parts.update()
                                              end !if
                                              exclude# =1
                                          
                                          Of 'WAR'
                                              access:warparts.clearkey(wpr:order_part_key)
                                              wpr:ref_number        = orp:job_number
                                              wpr:order_part_number = orp:record_number
                                              if access:warparts.fetch(wpr:order_part_key) = Level:Benign
                                                  wpr:exclude_from_order = 'YES'
                                                  wpr:date_ordered = Today()
                                                  wpr:order_number = ''
                                                  access:warparts.update()
                                              end !if
                                              exclude# =1
                                          
                                      End!Case orp:part_type                            
                                  Of 3 ! &Cancel Button
                              End!Case MessageEx
                          End !If orp:job_number <> ''
                          get(stohist,0)
                          if access:stohist.primerecord() = level:benign
                              If orp:part_ref_number <> ''
                                  If remove# = 1
                                      shi:information  = 'REMOVED FROM ORDER NUMBER: ' & Clip(ord:order_number) & |
                                                          '<13,10> REMOVED FROM JOB NUMBER: ' & Clip(orp:job_number)
                                  End
                                  If exclude# = 1
                                      shi:information  = 'REMOVED FROM ORDER NUMBER: ' & Clip(ord:order_number) & |
                                                          '<13,10> PART EXCLUDED FROM ORDER ON JOB NUMBER: ' & Clip(orp:job_number)
                                  End
                              Else
                                  shi:information          = 'REMOVED FROM ORDER NUMBER: ' & Clip(ord:order_number)
                              End
                              shi:Information = 'QTY: ' & Clip(orp:Quantity) & '<13,10>' & Clip(shi:Information)
                              shi:ref_number           = orp:part_ref_number
                              access:users.clearkey(use:password_key)
                              use:password = glo:password
                              access:users.fetch(use:password_key)
                              shi:user = use:user_code
                              shi:transaction_type     = 'REC'
                              shi:despatch_note_number = ''
                              shi:job_number           = orp:job_number
                              shi:quantity             = 0
                              shi:date                 = Today()
                              shi:purchase_cost        = orp:purchase_cost
                              shi:sale_cost            = orp:sale_cost
                              shi:retail_cost          = orp:retail_cost
                              shi:notes                = 'STOCK ITEM REMOVE FROM ORDER'
                              if access:stohist.insert()
                                  access:stohist.cancelautoinc()     
                              end            
                              Delete(ordparts)
                          End!if access:stohist.primerecord() = level:benign                
                  Of 3 ! &Cancel Button
              End!Case MessageEx
      
              End !If orp:Part_Type <> 'STO'
          Else ! If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
              !Error
          End !If Access:ORDPARTS.Tryfetch(orp:Record_Number_Key) = Level:Benign
      !    End!If found# = 1
          Do AllReceived
      End !SecurityCheck('PARTS ORDERS - DELETE')
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Delete_Part_Button, Accepted)
    OF ?Received_Button
      ThisWindow.Update
      Receive_Order
      ThisWindow.Reset
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
      If glo:select3 = 'NEW ORDER'
          access:ordparts_alias.clearkey(orp_ali:record_number_key)
          orp_ali:record_number = glo:select4
          if access:ordparts_alias.fetch(orp_ali:record_number_key) = Level:Benign
              get(ordparts,0)
              if access:ordparts.primerecord() = level:benign
                  orp:order_number    = orp_ali:order_number
                  orp:part_ref_number = orp_ali:part_ref_number
                  orp:quantity        = glo:select5
                  orp:part_number     = orp_ali:part_number
                  orp:description     = orp_ali:description
                  orp:purchase_cost   = orp_ali:purchase_cost
                  orp:sale_cost       = orp_ali:sale_cost
                  orp:job_number      = orp_ali:job_number
                  orp:number_received = orp_ali:number_received
                  orp:date_received   = glo:Select6
                  orp:part_type       = orp_ali:part_type
                  orp:all_received    = 'YES'
                  if access:ordparts.insert()
                      access:ordparts.cancelautoinc()
                  end
                  If orp_ali:part_ref_number <> ''
                      access:stock.clearkey(sto:ref_number_key)
                      sto:ref_number = orp_ali:part_ref_number
                      if access:stock.fetch(sto:ref_number_key)
                          Case MessageEx('Error! Unable to retrieve the Stock Part''s Details.','ServiceBase 2000',|
                                         'Styles\stop.ico','|&OK',1,1,'',,'Tahoma',8,0,700,CHARSET:ANSI,12632256,'',0,beep:systemhand,msgex:samewidths,84,26,0) 
                              Of 1 ! &OK Button
                          End!Case MessageEx
                      Else!if access:stock.fetch(sto:ref_number_key)
                          sto:purchase_cost   = orp_ali:purchase_cost
                          sto:sale_cost       = orp_ali:sale_cost
      !                    sto:quantity_stock += orp_ali:quantity
                          sto:quantity_on_order -= orp_ali:quantity
                          If sto:quantity_on_order < 0
                              sto:quantity_on_order = 0
                          End
                          If sto:quantity_stock < 0
                              sto:quantity_stock = 0
                          End!If sto:quantity_stock < 0
                          access:stock.update()
                      end !if access:stock.fetch(sto:ref_number_key)
                  End
      
              end!if access:ordparts.primerecord() = level:benign
      
              parts_record_number_temp = ''
              warparts_record_number_temp = ''
              Do Get_Record_Numbers
              found# = 0
              If orp_ali:part_type = 'JOB'
                  access:parts.clearkey(par:RecordNumberKey)
                  par:record_number = parts_record_number_temp
                  if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                      par:quantity = glo:select5
                      par:date_received = Today()
                      access:parts.update()
                  end!if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                  access:parts_alias.clearkey(par_ali:RecordNumberKey)
                  par_ali:record_number = parts_record_number_temp
                  if access:parts_alias.fetch(par_ali:RecordNumberKey) = Level:benign
                      Get(parts,0)
                      If access:parts.primerecord() = Level:Benign
                          record_number$           = par:record_number
                          par:record              :=: par_ali:record
                          par:record_number        = record_number$
                          par:quantity             = orp_ali:quantity
                          par:date_ordered         = Today()
                          par:date_received        = ''
                          par:order_part_number    = orp_ali:record_number
                          access:parts.insert()
                      End!If access:parts.primerecord() = Level:Benign
                  end !if access:parts.fetch(par:RecordNumberKey) = Level:Benign
              End !If orp_ali:part_type = 'JOB'
              If orp_ali:part_type = 'WAR'
                  access:warparts.clearkey(wpr:RecordNumberKey)
                  wpr:record_number = warparts_record_number_temp
                  if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                      wpr:quantity = glo:select5
                      wpr:date_received = Today()
                      access:warparts.update()
                  end!if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                  access:warparts_alias.clearkey(war_ali:RecordNumberKey)
                  war_ali:record_number = warparts_record_number_temp
                  if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:Benign
                      If access:warparts.primerecord() = Level:Benign
                          record_number$           = wpr:record_number
                          wpr:record              :=: war_ali:record
                          wpr:record_number        = record_number$
                          wpr:quantity             = orp_ali:quantity
                          wpr:date_ordered         = Today()
                          wpr:date_received        = ''
                          wpr:order_part_number    = orp_ali:record_number
                          access:warparts.insert()
                      End!If access:warparts.primerecord() = Level:Benign
                  end!if access:warparts_alias.fetch(war_ali:RecordNumberKey) = Level:Benign
              End !If orp_ali:part_type = 'WAR
              If orp_ali:part_type = 'RET'
                  access:retstock.clearkey(res:record_number_key)                                 !Make part on old order as received
                  res:record_number = retstock_record_number_temp
                  if access:retstock.tryfetch(res:record_number_key) = Level:Benign
                      res:despatched = 'OLD'
                      access:retstock.update()
                  End!if access:retstock.tryfetch(res:record_number_key) = Level:Benign
                  access:retstock_alias.clearkey(ret_ali:record_number_key)
                  ret_ali:record_number = retstock_record_number_temp
                  if access:retstock_alias.tryfetch(ret_ali:record_number_key) = Level:Benign     !Get the old part's alias
                      access:retsales_alias.clearkey(res_ali:ref_number_key)
                      res_ali:ref_number = res:ref_number
                      if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign    !Get the old Parts' Sale Alias
                          get(retsales,0)
                          if access:retsales.primerecord() = Level:Benign
                              ref_number$    = ret:ref_number
                              ret:record    :=: res_ali:record
                              ret:Ref_number    = ref_number$
                              ret:consignment_number = ''
                              ret:date_despatched = ''
                              ret:despatched  = ''
                              ret:despatch_number = ''
                              ret:invoice_number  = ''
                              ret:invoice_date    = ''
                              ret:invoice_courier_cost    = ''
                              ret:invoice_parts_cost      = ''
                              ret:invoice_sub_total       = ''
                              if access:retsales.insert()                                         !Create an new sale from the old one
                                  access:retsales.cancelautoinc()
                              Else!if access:retsales.insert()
                                  parts_cost$ = 0
                                  get(retstock,0)
                                  if access:retstock.primerecord() = Level:Benign                 !Create a new part from the old one
                                      record_number$    = res:record_number
                                      res:record    :=: ret_ali:record
                                      res:ref_number  = ret:ref_number
                                      res:record_number    = record_number$
                                      res:date_received   = Today()
                                      res:quantity        = glo:select5
                                      res:despatched  = 'YES'
                                      parts_cost$     = res:quantity * res:item_cost
                                      if access:retstock.insert()
                                          access:retstock.cancelautoinc()
                                      end
                                  end!if access:retstock.primerecord() = Level:Benign
                                  get(retstock,0)
                                  if access:retstock.primerecord() = Level:Benign                 !Create a new part from the old one
                                      record_number$    = res:record_number
                                      res:record    :=: ret_ali:record
                                      res:ref_number  = ret:ref_number
                                      res:record_number    = record_number$
                                      res:quantity        = orp_ali:quantity
                                      res:date_ordered        = Today()
                                      res:despatched  = 'ORD'
                                      res:order_part_number    = orp_ali:record_number
                                      if access:retstock.insert()
                                          access:retstock.cancelautoinc()
                                      end
                                      access:retsales.clearkey(ret:ref_number_key)
                                      ret:ref_number  = ref_number$
                                      If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                                          ret:parts_cost  = res:quantity * res:item_cost + parts_cost$
                                          ret:sub_total   = ret:parts_cost    + ret:courier_cost
                                          access:retsales.update()
                                      End!If access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
      
                                  end!if access:retstock.primerecord() = Level:Benign
                              End!if access:retsales.insert()
                          end!if access:retsales.primerecord() = Level:Benign
                      End!if access:retsales_alias.tryfetch(res_ali:ref_number_key) = Level:Benign
                  End!if access:retstock_alias.tryfetch(ret_ali:record_number_key) = Level:Benign
      
              End !If orp_ali:part_type = 'WAR
      
          End
      End
      ! All recieved?
      Do AllReceived
      BRW1.ResetQueue(1)
      BRW6.ResetQueue(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?Received_Button, Accepted)
    OF ?ReceiveProcess
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveProcess, Accepted)
      !recordspercycle         = 25
      !recordsprocessed        = 0
      !percentprogress         = 0
      !progress:thermometer    = 0
      !thiswindow.reset(1)
      !open(progresswindow)
      !
      !?progress:userstring{prop:text} = 'Building Parts List..'
      !?progress:pcttext{prop:text} = '0% Completed'
      !
      !recordstoprocess    = 3
      !!---Insert Routine
      !Display()
      !Do GetNextRecord2
      !
      !!---After Routine
      Open(BuildingWindow)
      Display()
      glo:File_Name   = 'J' & Format(Clock(),@n07) & '.TMP'
      glo:File_Name2  = 'O' & Format(Clock(),@n07) & '.TMP'
      
      Remove(ORDJOBS)
      Remove(ORDSTOCK)
      
      If Access:ORDJOBS.Open() = Level:Benign
          !Do GetNextRecord2
          If access:ORDJOBS.Usefile() = Level:Benign
              If Access:ORDSTOCK.Open() = Level:Benign
                  !Do GetNextRecord2
                  If Access:ORDSTOCK.Usefile() = Level:Benign
                      !Loop through Order Parts finding non-received ones
      !                Do EndPrintRun
      !                close(progresswindow)
                      Close(BuildingWindow)
                      RecordCount# = 0
                      Setcursor(Cursor:Wait)
                      Save_orp_ID = Access:ORDPARTS.SaveFile()
                      Access:ORDPARTS.ClearKey(orp:Order_Number_Key)
                      orp:Order_Number    = brw1.q.ord:Order_Number
                      Set(orp:Order_Number_Key,orp:Order_Number_Key)
                      Loop
                          If Access:ORDPARTS.NEXT()
                             Break
                          End !If
                          If orp:Order_Number    <> brw1.q.ord:Order_Number      |
                              Then Break.  ! End If
                          If orp:All_Received <> 'YES'
                              RecordCount# += 1
                          End!If orp:All_Received <> 'YES'
                      End !Loop
                      Access:ORDPARTS.RestoreFile(Save_orp_ID)
                      Setcursor()
      
                      recordspercycle         = 25
                      recordsprocessed        = 0
                      percentprogress         = 0
                      progress:thermometer    = 0
                      thiswindow.reset(1)
                      open(progresswindow)
      
                      ?progress:userstring{prop:text} = 'Building Order...'
                      ?progress:pcttext{prop:text} = '0% Completed'
      
                      recordstoprocess    =  RecordCount#
      
                      Setcursor(Cursor:Wait)
                      Save_orp_ID = Access:ORDPARTS.SaveFile()
                      Access:ORDPARTS.ClearKey(orp:Order_Number_Key)
                      orp:Order_Number    = brw1.q.ord:Order_Number
                      Set(orp:Order_Number_Key,orp:Order_Number_Key)
                      Loop
                          If Access:ORDPARTS.NEXT()
                             Break
                          End !If
                          If orp:Order_Number    <> brw1.q.ord:Order_Number      |
                              Then Break.  ! End If
                          If orp:All_Received <> 'YES'
                              Do GetNextRecord2
                              cancelcheck# += 1
                              If cancelcheck# > (RecordsToProcess/100)
                                  Do cancelcheck
                                  If tmp:cancel = 1
                                      Break
                                  End!If tmp:cancel = 1
                                  cancelcheck# = 0
                              End!If cancelcheck# > 50
      
                              !If not received, search through parts to find
                              !requests
                              Save_par_ID = Access:PARTS.SaveFile()
                              Access:PARTS.ClearKey(par:RequestedKey)
                              par:Requested   = True
                              par:Part_Number = orp:Part_Number
                              Set(par:RequestedKey,par:RequestedKey)
                              Loop
                                  If Access:PARTS.NEXT()
                                     Break
                                  End !If
                                  If par:Requested   <> True      |
                                  Or par:Part_Number <> orp:Part_Number      |
                                      Then Break.  ! End If
                                  ! Start Change 3372 BE(13/10/03)
                                  ! Ignore if not a pending order
                                  IF (par:Pending_Ref_Number = 0) THEN
                                      CYCLE
                                  END
                                  ! End Change 3372 BE(13/10/03)
                                  If Access:ORDJOBS.PrimeRecord() = Level:Benign
                                      orjtmp:OrderNumber  = orp:Order_Number
                                      orjtmp:PartNumber   = orp:Part_Number
                                      orjtmp:JobNumber    = par:Ref_Number
                                      orjtmp:CharWarr     = 'C'
                                      orjtmp:Quantity     = par:Quantity
                                      orjtmp:RefNumber    = par:Record_Number
                                      If Access:ORDJOBS.Insert()
                                          Access:ORDJOBS.CancelAutoInc()
                                      End!If Access:ORDJOBS.Insert()
                                  End!If Access:ORDJOBS.PrimeRecord() = Level:Benign
      
                              End !Loop
                              Access:PARTS.RestoreFile(Save_par_ID)
      
                              Save_wpr_ID = Access:WARPARTS.SaveFile()
                              Access:WARPARTS.ClearKey(wpr:RequestedKey)
                              wpr:Requested   = True
                              wpr:Part_Number = orp:Part_Number
                              Set(wpr:RequestedKey,wpr:RequestedKey)
                              Loop
                                  If Access:WARPARTS.NEXT()
                                     Break
                                  End !If
                                  If wpr:Requested   <> True  |
                                  Or wpr:Part_Number <> orp:Part_Number |
                                      Then Break.  ! End If
                                  ! Start Change 3372 BE(13/10/03)
                                  ! Ignore if not a pending order
                                  IF (wpr:Pending_Ref_Number = 0) THEN
                                      CYCLE
                                  END
                                  ! End Change 3372 BE(13/10/03)
                                  If Access:ORDJOBS.PrimeRecord() = Level:Benign
                                      orjtmp:OrderNumber  = orp:Order_Number
                                      orjtmp:PartNumber   = orp:Part_Number
                                      orjtmp:JobNumber    = wpr:Ref_Number
                                      orjtmp:CharWarr     = 'W'
                                      orjtmp:Quantity     = wpr:quantity
                                      orjtmp:RefNumber    = wpr:Record_Number
                                      If Access:ORDJOBS.Insert()
                                          Access:ORDJOBS.CancelAutoInc()
                                      End!If Access:ORDJOBS.Insert()
                                  End!If Access:ORDJOBS.PrimeRecord() = Level:Benign
                              End !Loop
                              Access:WARPARTS.RestoreFile(Save_wpr_ID)
      
                              !Now find any Stock Parts that have been requested.
                              !Loop through All the locations
                              !(Hopefully there won't be that many)
                              Save_loc_ID = Access:LOCATION.SaveFile()
                              Access:LOCATION.Clearkey(loc:Location_Key)
                              Set(loc:Location_Key)
                              Loop
                                  If Access:LOCATION.NEXT()
                                     Break
                                  End !If
                                  !Loop through all the parts in that location
                                  !with that part number. Again, there shouldn't be that
                                  !many, hopefully 1 per location
                                  Save_sto_ID = Access:STOCK.SaveFile()
                                  Access:STOCK.ClearKey(sto:Location_Key)
                                  sto:Location    = loc:Location
                                  sto:Part_Number = orp:Part_Number
                                  Set(sto:Location_Key,sto:Location_Key)
                                  Loop
                                      If Access:STOCK.NEXT()
                                         Break
                                      End !If
                                      If sto:Location    <> loc:Location      |
                                      Or sto:Part_Number <> orp:Part_Number      |
                                          Then Break.  ! End If
                                      !In case there is more than one part in one location
                                      If sto:QuantityRequested > 0
                                          Access:ORDSTOCK.ClearKey(orstmp:LocationKey)
                                          orstmp:Location = loc:Location
                                          If Access:ORDSTOCK.TryFetch(orstmp:LocationKey) = Level:Benign
                                              orstmp:Quantity += sto:QuantityRequested
                                              Access:ORDSTOCK.Update()
                                          Else!If Access:ORDSTOCK.TryFetch(orstmp:LocationKey) = Level:Benign
                                              If Access:ORDSTOCK.PrimeRecord() = Level:Benign
                                                  orstmp:OrderNumber  = orp:Order_Number
                                                  orstmp:PartNumber   = orp:Part_Number
                                                  orstmp:Location     = loc:Location
                                                  orstmp:Quantity     = sto:QuantityRequested
                                                  If Access:ORDSTOCK.Insert()
                                                      Access:ORDSTOCK.CancelAutoInc()
                                                  End!If Access:ORDSTOCK.Insert()
                                              End!If Access:ORDSTOCK.PrimeRecord() = Level:Benign
                                          End!If Access:ORDSTOCK.TryFetch(orstmp:LocationKey) = Level:Benign
                                      End!If sto:QuantityRequested > 0
                                  End !Loop
                                  Access:STOCK.RestoreFile(Save_sto_ID)
                              End !Loop
                              Access:LOCATION.RestoreFile(Save_loc_ID)
                          End!If orp:All_Received <> 'YES'
                      End !Loop
                      Access:ORDPARTS.RestoreFile(Save_orp_ID)
                      Setcursor()
                      Do EndPrintRun
                      close(progresswindow)
                      SetCursor()
                      !Call the browse screen
                      0{prop:statustext,3} = ''
                      Access:ORDSTOCK.Close()
                      Access:ORDJOBS.Close()
      
                      ReceivingStock(brw1.q.ord:Order_Number)
      
                  End!If Access:ORDSTOCK.Usefile()
              End!If Access:ORDSTOCK.Open()
          End!If access:ORDJOBS.Usefile()
      End!If Access:ORDJOBS.Open()
      !Create Temporary Files And Add Jobs and Stock Items
      
      Remove(ORDSTOCK)
      Remove(ORDJOBS)
      glo:File_name = ''
      glo:File_name2 = ''
      
      Do AllReceived
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?ReceiveProcess, Accepted)
    OF ?DASTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGONOFF
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASTAGAll
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASUNTAGALL
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASUNTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASREVTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASREVTAGALL
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    OF ?DASSHOWTAG
      ThisWindow.Update
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
        DO DASBRW::11:DASSHOWTAG
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Browse_Parts_Orders')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  !Extension Templete Code:XploreOOPBrowse,WindowManagerMethodCodeSection,'TakeEvent'
  IF Xplore6.IgnoreEvent = True                       !Xplore
     Xplore6.IgnoreEvent = False                      !Xplore
     CYCLE                                            !Xplore
  END                                                 !Xplore
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:PreAlertKey
      IF KEYCODE() = MouseRight                       !Xplore
         SETKEYCODE(0)                                !Xplore
         CYCLE                                        !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeFieldEvent()
  CASE FIELD()
  OF ?List
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
      If Keycode() = MouseLeft2
          Post(event:accepted,?Received_Button)
      End!If Keycode() = MouseLeft2
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?List, AlertKey)
    END
  END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeNewSelection PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
  ReturnValue = PARENT.TakeNewSelection()
    CASE FIELD()
    OF ?Sheet2
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
      CASE CHOICE(?Sheet2)
        OF 1
          ?List{PROP:FORMAT} ='11L(2)I@s1@#1#110L(2)|M*~Part Number~@s30@#3#109L(2)|M*~Description~@s30@#8#25L(2)|M*~Type~@s3@#13#35R(2)|M*~Ref No~L@s8@#18#38L(2)|M*~Quantity~@p<<<<<<<#p@#23#35R(2)|M*~Purchase~@n7.2@#28#34R(2)|M*~Sale~@n7.2@#33#34R(2)|M*~Retail~@n7.2@#38#35L(2)|M*~Model~@s30@#49#35L(2)|M*~Account~@s15@#54#35L(2)|M*~Exchange~@s1@#59#40R(2)|M*~Date Rcvd~@d6b@#43#'
          ?Tab2{PROP:TEXT} = 'By Part Number'
        OF 2
          ?List{PROP:FORMAT} ='11L(2)J@s1@#1#110L(2)|M*~Part Number~@s30@#3#109L(2)|M*~Description~@s30@#8#23L(2)|M*~Type~@s3@#13#35R(2)|M*~Ref No~L@s8@#18#38L(2)|M*~Quantity~@p<<<<<<<#p@#23#35L(2)|M*~Model~@s30@#49#35L(2)|M*~Account~@s15@#54#35L(2)|M*~Exchange~@s1@#59#35R(2)|M*~Purchase~@n7.2@#28#34R(2)|M*~Sale~@n7.2@#33#34R(2)|M*~Retail~@n7.2@#38#40R(2)|M*~Date Rcvd~@d6b@#43#'
          ?Tab3{PROP:TEXT} = 'By Part Number'
      END
      !--------------------------------------------------------------------------
      ! Tinman Browse Reformat
      !--------------------------------------------------------------------------
    OF ?List
      !--------------------------------------------------------------------------
      ! DAS_Tagging
      !--------------------------------------------------------------------------
      IF KEYCODE() = MouseLeft AND (?List{PROPLIST:MouseDownRow} > 0) 
        CASE ?List{PROPLIST:MouseDownField}
      
          OF 1
            DASBRW::11:TAGMOUSE = 1
            POST(EVENT:Accepted,?DASTAG)
               ?List{PROPLIST:MouseDownField} = 2
            CYCLE
         END
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    CASE EVENT()
    OF EVENT:AlertKey
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
      Case Keycode()
          Of F2Key
              Post(Event:Accepted,?Reprint_Order_Button)
          Of F5Key
              If ?Dastag{prop:Hide} = 0
                  Post(Event:Accepted,?Dastag)
              End !If ?Dastag{prop:Hide} = 0
          Of F6Key
              If ?Dastagall{prop:Hide} = 0
                  Post(Event:Accepted,?Dastagall)
              End !If ?Dastag{prop:Hide} = 0
          Of F7key
              If ?DasUntagall{prop:Hide} = 0
                  Post(Event:Accepted,?DasUntagall)
              End !If ?DasUntagall{prop:Hide} = 0
          Of F9Key
              Post(Event:Accepted,?Received_Button)
          Of F10Key
              If ?ReceiveProcess{prop:hide} = 0
                  Post(Event:Accepted,?ReceiveProcess)
              Else !If ?ReceiveProcess{prop:hide} = 0
                  Post(Event:Accepted,?All_Recieved_Button)
              End !If ?ReceiveProcess{prop:hide} = 0
      End !Keycode()
          
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(AlertKey)
    OF EVENT:OpenWindow
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
      Do DASBRW::11:DASUNTAGALL
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'OpenWindow',PRIORITY(8000)
      POST(xpEVENT:Xplore + 6)                        !Xplore
      ALERT(AltF12)                                   !Xplore
      ALERT(AltF11)                                   !Xplore
      ALERT(AltR)                                     !Xplore
      ALERT(AltM)                                     !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(OpenWindow)
    OF EVENT:GainFocus
      ! Before Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
      BRW1.ResetSort(1)
      BRW6.ResetSort(1)
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'GainFocus',PRIORITY(8000)
      Xplore6.GetColumnInfo()                         !Xplore
      ! After Embed Point: %WindowEventHandling) DESC(Window Event Handling) ARG(GainFocus)
    OF EVENT:Sized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Sized',PRIORITY(8000)
      Xplore6.GetColumnInfo()                         !Xplore
    OF EVENT:Iconized
      !Extension Templete Code:XploreOOPBrowse,WindowEventHandling,'Iconized',PRIORITY(9000)
      OF xpEVENT:Xplore + 6                           !Xplore
        Xplore6.GetColumnInfo()                       !Xplore
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

! Before Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
UpdateStockHistory  PROCEDURE(IN:PartRefNum, IN:OrderNumber, IN:Supplier, IN:Qty, IN:DespatchNoteNumber, IN:JobRef)    ! Start Change 3505 BE(10/11/03)
    CODE
    access:stock.clearkey(sto:ref_number_key)
    sto:ref_number = IN:PartRefNum
    IF ( access:stock.fetch(sto:ref_number_key) = Level:Benign) THEN

        access:users.clearkey(use:password_key)
        use:password = glo:password
        access:users.tryfetch(use:password_key)
        usercode" = use:user_code

        IF (Access:STOHIST.Primerecord() = Level:Benign) THEN
            shi:Ref_Number              = IN:PartRefNum
            shi:Transaction_Type        = 'ADD'
            shi:Despatch_Note_Number    = IN:DespatchNoteNumber
            shi:Quantity                = IN:Qty
            shi:Date                    = Today()
            shi:Purchase_Cost           = sto:Purchase_Cost
            shi:Sale_Cost               = sto:Sale_Cost
            shi:User                    = usercode"
            shi:Notes                   = 'STOCK ADDED FROM ORDER'
            shi:information          = 'ORDER NUMBER: ' & Clip(IN:OrderNumber) & '<13,10>SUPPLIER: ' & Clip(IN:Supplier) &|
                                        '<13,10>DESPATCH NOTE NO: ' & Clip(IN:DespatchNoteNumber)
            IF (Access:STOHIST.Tryinsert() = Level:Benign) THEN
                Access:STOHIST.Cancelautoinc()
            END
        END

        IF (access:STOHIST.primerecord() = Level:Benign) THEN
            shi:ref_number = IN:PartRefNum
            shi:transaction_type = 'DEC'
            shi:despatch_note_number = IN:DespatchNoteNumber
            shi:quantity = IN:Qty
            shi:Date = Today()
            shi:purchase_cost = sto:purchase_cost
            shi:sale_cost = sto:sale_cost
            shi:job_number = IN:JobRef
            shi:user = usercode"
            shi:notes = 'STOCK DECREMENTED'
            IF (access:STOHIST.tryinsert()) THEN
                access:STOHIST.cancelautoinc()
            END
        END

        ! Start Change 4801 BE(12/10/2004)
        ! Start Change 4803 BE(11/10/2004)
        !ReprintPartReceivedPickNote(IN:JobRef, IN:PartRefNum)
        ! End Change 4803 BE(11/10/2004)
        ReprintPartReceivedPickNote(IN:JobRef, IN:PartRefNum, true)
        ! End Change 4801 BE(12/10/2004)
    END

    RETURN
! Start Change 3505 BE(10/11/03)

BRW1.Init PROCEDURE(SIGNED ListBox,*STRING Posit,VIEW V,QUEUE Q,RelationManager RM,WindowManager WM)

  CODE
  PARENT.Init(ListBox,Posit,V,Q,RM,WM)
  IF WM.Request <> ViewRecord
    SELF.DeleteControl=?Delete:3
  END


BRW1.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  IF Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'NO'
    RETURN SELF.SetSort(1,Force)
  ELSIF Choice(?CurrentTab) = 1 And Upper(outstanding_temp) = 'ALL'
    RETURN SELF.SetSort(2,Force)
  ELSE
    RETURN SELF.SetSort(3,Force)
  END
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW1.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.GetFreeElementName PROCEDURE

ReturnValue          ANY

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,6,'GetFreeElementName'
  IF BRW6.ViewOrder = True                            !Xplore
     RETURN('UPPER(' & CLIP(BRW6.LocatorField) & ')') !Xplore
  END
  ReturnValue = PARENT.GetFreeElementName()
  RETURN ReturnValue


BRW6.GetFreeElementPosition PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,6,'GetFreeElementPosition'
  IF BRW6.ViewOrder = True                            !Xplore
     RETURN(1)                                        !Xplore
  END
  ReturnValue = PARENT.GetFreeElementPosition()
  RETURN ReturnValue


BRW6.ResetSort PROCEDURE(BYTE Force)

ReturnValue          BYTE,AUTO

  CODE
  !Extension Templete Code:XploreOOPBrowse,BeforeControlRefresh,6
   COMPILE('***NotC4***',_VER_C5)
  IF EVENT()>=XpEvent:AddFilter1 AND EVENT()<=XpEvent:VoidFilter
     IF Xplore6.FilterShared OR EVENT()=XpEvent:VoidFilter
        CS# = POINTER(SELF.Sort)
        LOOP I# = 1 TO RECORDS(SELF.Sort)
             PARENT.SetSort(I#)
             SELF.SetFilter(Xplore6.QC.GetFilter(),'Xplore')
        END
        PARENT.SetSort(CS#)
     ELSE
        SELF.SetFilter(Xplore6.QC.GetFilter(),'Xplore')
     END
  END
   !***NotC4***
  IF BRW6.ViewOrder = True                            !Xplore
    SavePtr# = POINTER(Xplore6.sq)                    !Xplore
    Xplore6.SetupOrder(BRW6.SortOrderNbr)             !Xplore
    GET(Xplore6.sq,SavePtr#)                          !Xplore
    SETCURSOR(Cursor:Wait)
    R# = SELF.SetSort(BRW6.SortOrderNbr,Force)        !Xplore
    SETCURSOR()
    RETURN R#                                         !Xplore
  ELSIF BRW6.FileSeqOn = True                         !Xplore
    RETURN SELF.SetSort(BRW6.FileOrderNbr,Force)      !Xplore
  ELSE
    RETURN SELF.SetSort(1,FORCE)
  END                                                 !Xplore
  ReturnValue = PARENT.ResetSort(Force)
  RETURN ReturnValue


BRW6.SetQueueRecord PROCEDURE

  CODE
  ! Before Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = orp:Record_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    IF ERRORCODE()
      tmp:tag = ''
    ELSE
      tmp:tag = '*'
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  PARENT.SetQueueRecord()      !FIX FOR CFW 4 (DASTAG)
  ! Start Change 4803 BE(13/10/2004)
  model_temp = ''
  account_temp = ''
  exchange_temp = ''
  ! End Change 4803 BE(13/10/2004)
  
  Case def:SummaryORders
      Of 1
            job_number_temp = 'Stock'
            type_temp       = 'STO'
      Else
          IF (orp:Job_Number <> '')
            job_number_temp = orp:Job_Number
            ! Start Change 4803 BE(13/10/2004)
            access:jobs.clearkey(job:ref_number_key)
            job:ref_number = orp:job_number
            IF (access:jobs.fetch(job:ref_number_key) = Level:Benign) THEN
              model_temp = job:model_number
              IF (job:Exchange_Unit_Number = '') THEN
                  exchange_temp = 'N'
              ELSE
                  exchange_temp = 'Y'
              END
              access:subtracc.clearkey(sub:Account_Number_Key)
              sub:Account_Number = job:Account_Number
              IF (access:subtracc.fetch(sub:Account_Number_Key) = Level:Benign) THEN
                  account_temp = sub:Company_Name
              ELSE
                  account_temp = job:Account_Number
              END
            END
            ! End Change 4803 BE(13/10/2004)
          ELSE
            job_number_temp = 'Stock'
          END
          CASE (orp:Part_Type)
          OF 'JOB'
            type_temp = 'CHA'
          ELSE
            type_temp = orp:Part_Type
          END
  End !Case def:SummaryORders
  ! Before Embed Point: %FormatBrowse) DESC(Legacy: Browser, Format an element of the queue) ARG(6)
  ! Show Received
  If orp:number_received = ''
      received_temp = 'NONE'
  Else
      If orp:number_received <> orp:quantity
          received_temp = orp:number_received
      Else
          received_temp = 'ALL'
      End
  End
  ! After Embed Point: %FormatBrowse) DESC(Legacy: Browser, Format an element of the queue) ARG(6)
  PARENT.SetQueueRecord
  IF (tmp:tag = '*')
    SELF.Q.tmp:tag_Icon = 2
  ELSE
    SELF.Q.tmp:tag_Icon = 1
  END
  SELF.Q.orp:Part_Number_NormalFG = -1
  SELF.Q.orp:Part_Number_NormalBG = -1
  SELF.Q.orp:Part_Number_SelectedFG = -1
  SELF.Q.orp:Part_Number_SelectedBG = -1
  SELF.Q.orp:Description_NormalFG = -1
  SELF.Q.orp:Description_NormalBG = -1
  SELF.Q.orp:Description_SelectedFG = -1
  SELF.Q.orp:Description_SelectedBG = -1
  SELF.Q.type_temp_NormalFG = -1
  SELF.Q.type_temp_NormalBG = -1
  SELF.Q.type_temp_SelectedFG = -1
  SELF.Q.type_temp_SelectedBG = -1
  SELF.Q.job_number_temp_NormalFG = -1
  SELF.Q.job_number_temp_NormalBG = -1
  SELF.Q.job_number_temp_SelectedFG = -1
  SELF.Q.job_number_temp_SelectedBG = -1
  SELF.Q.orp:Quantity_NormalFG = -1
  SELF.Q.orp:Quantity_NormalBG = -1
  SELF.Q.orp:Quantity_SelectedFG = -1
  SELF.Q.orp:Quantity_SelectedBG = -1
  SELF.Q.orp:Purchase_Cost_NormalFG = -1
  SELF.Q.orp:Purchase_Cost_NormalBG = -1
  SELF.Q.orp:Purchase_Cost_SelectedFG = -1
  SELF.Q.orp:Purchase_Cost_SelectedBG = -1
  SELF.Q.orp:Sale_Cost_NormalFG = -1
  SELF.Q.orp:Sale_Cost_NormalBG = -1
  SELF.Q.orp:Sale_Cost_SelectedFG = -1
  SELF.Q.orp:Sale_Cost_SelectedBG = -1
  SELF.Q.orp:Retail_Cost_NormalFG = -1
  SELF.Q.orp:Retail_Cost_NormalBG = -1
  SELF.Q.orp:Retail_Cost_SelectedFG = -1
  SELF.Q.orp:Retail_Cost_SelectedBG = -1
  SELF.Q.orp:Date_Received_NormalFG = -1
  SELF.Q.orp:Date_Received_NormalBG = -1
  SELF.Q.orp:Date_Received_SelectedFG = -1
  SELF.Q.orp:Date_Received_SelectedBG = -1
  SELF.Q.model_temp_NormalFG = -1
  SELF.Q.model_temp_NormalBG = -1
  SELF.Q.model_temp_SelectedFG = -1
  SELF.Q.model_temp_SelectedBG = -1
  SELF.Q.account_temp_NormalFG = -1
  SELF.Q.account_temp_NormalBG = -1
  SELF.Q.account_temp_SelectedFG = -1
  SELF.Q.account_temp_SelectedBG = -1
  SELF.Q.exchange_temp_NormalFG = -1
  SELF.Q.exchange_temp_NormalBG = -1
  SELF.Q.exchange_temp_SelectedFG = -1
  SELF.Q.exchange_temp_SelectedBG = -1
   
   
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Part_Number_NormalFG = 8421504
     SELF.Q.orp:Part_Number_NormalBG = 16777215
     SELF.Q.orp:Part_Number_SelectedFG = 16777215
     SELF.Q.orp:Part_Number_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Part_Number_NormalFG = 255
     SELF.Q.orp:Part_Number_NormalBG = 16777215
     SELF.Q.orp:Part_Number_SelectedFG = 16777215
     SELF.Q.orp:Part_Number_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Description_NormalFG = 8421504
     SELF.Q.orp:Description_NormalBG = 16777215
     SELF.Q.orp:Description_SelectedFG = 16777215
     SELF.Q.orp:Description_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Description_NormalFG = 255
     SELF.Q.orp:Description_NormalBG = 16777215
     SELF.Q.orp:Description_SelectedFG = 16777215
     SELF.Q.orp:Description_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.type_temp_NormalFG = 8421504
     SELF.Q.type_temp_NormalBG = 16777215
     SELF.Q.type_temp_SelectedFG = 16777215
     SELF.Q.type_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.type_temp_NormalFG = 255
     SELF.Q.type_temp_NormalBG = 16777215
     SELF.Q.type_temp_SelectedFG = 16777215
     SELF.Q.type_temp_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.job_number_temp_NormalFG = 8421504
     SELF.Q.job_number_temp_NormalBG = 16777215
     SELF.Q.job_number_temp_SelectedFG = 16777215
     SELF.Q.job_number_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.job_number_temp_NormalFG = 255
     SELF.Q.job_number_temp_NormalBG = 16777215
     SELF.Q.job_number_temp_SelectedFG = 16777215
     SELF.Q.job_number_temp_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Quantity_NormalFG = 8421504
     SELF.Q.orp:Quantity_NormalBG = 16777215
     SELF.Q.orp:Quantity_SelectedFG = 16777215
     SELF.Q.orp:Quantity_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Quantity_NormalFG = 255
     SELF.Q.orp:Quantity_NormalBG = 16777215
     SELF.Q.orp:Quantity_SelectedFG = 16777215
     SELF.Q.orp:Quantity_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Purchase_Cost_NormalFG = 8421504
     SELF.Q.orp:Purchase_Cost_NormalBG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedFG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Purchase_Cost_NormalFG = 255
     SELF.Q.orp:Purchase_Cost_NormalBG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedFG = 16777215
     SELF.Q.orp:Purchase_Cost_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Sale_Cost_NormalFG = 8421504
     SELF.Q.orp:Sale_Cost_NormalBG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedFG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Sale_Cost_NormalFG = 255
     SELF.Q.orp:Sale_Cost_NormalBG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedFG = 16777215
     SELF.Q.orp:Sale_Cost_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Retail_Cost_NormalFG = 8421504
     SELF.Q.orp:Retail_Cost_NormalBG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedFG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Retail_Cost_NormalFG = 255
     SELF.Q.orp:Retail_Cost_NormalBG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedFG = 16777215
     SELF.Q.orp:Retail_Cost_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.orp:Date_Received_NormalFG = 8421504
     SELF.Q.orp:Date_Received_NormalBG = 16777215
     SELF.Q.orp:Date_Received_SelectedFG = 16777215
     SELF.Q.orp:Date_Received_SelectedBG = 8421504
   ELSE
     SELF.Q.orp:Date_Received_NormalFG = 255
     SELF.Q.orp:Date_Received_NormalBG = 16777215
     SELF.Q.orp:Date_Received_SelectedFG = 16777215
     SELF.Q.orp:Date_Received_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.model_temp_NormalFG = 8421504
     SELF.Q.model_temp_NormalBG = 16777215
     SELF.Q.model_temp_SelectedFG = 16777215
     SELF.Q.model_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.model_temp_NormalFG = 255
     SELF.Q.model_temp_NormalBG = 16777215
     SELF.Q.model_temp_SelectedFG = 16777215
     SELF.Q.model_temp_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.account_temp_NormalFG = 8421504
     SELF.Q.account_temp_NormalBG = 16777215
     SELF.Q.account_temp_SelectedFG = 16777215
     SELF.Q.account_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.account_temp_NormalFG = 255
     SELF.Q.account_temp_NormalBG = 16777215
     SELF.Q.account_temp_SelectedFG = 16777215
     SELF.Q.account_temp_SelectedBG = 255
   END
   IF (orp:all_received = 'YES')
     SELF.Q.exchange_temp_NormalFG = 8421504
     SELF.Q.exchange_temp_NormalBG = 16777215
     SELF.Q.exchange_temp_SelectedFG = 16777215
     SELF.Q.exchange_temp_SelectedBG = 8421504
   ELSE
     SELF.Q.exchange_temp_NormalFG = 255
     SELF.Q.exchange_temp_NormalBG = 16777215
     SELF.Q.exchange_temp_SelectedFG = 16777215
     SELF.Q.exchange_temp_SelectedBG = 255
   END
  ! After Embed Point: %BrowserMethodCodeSection) DESC(Browser Method Code Section) ARG(6, SetQueueRecord, ())


BRW6.TakeEvent PROCEDURE

  CODE
  !Extension Templete Code:XploreOOPBrowse,BrowserMethodCodeSection,6,'TakeEvent'
  Xplore6.LastEvent = EVENT()                         !Xplore
  IF FOCUS() = ?List                                  !Xplore
    CASE EVENT()                                      !Xplore
    OF Event:AlertKey                                 !Xplore
      CASE KEYCODE()                                  !Xplore
      OF AltF11                                       !Xplore
         Xplore6.AdjustAllColumns()                   !Xplore
      OF AltF12                                       !Xplore
         Xplore6.ResetToDefault()                     !Xplore
      OF AltR                                         !Xplore
         Xplore6.ToggleBar()                          !Xplore
      OF AltM                                         !Xplore
         Xplore6.InvokePopup()                        !Xplore
      END                                             !Xplore
    OF xpEVENT:RightButtonUp                          !Xplore
       Xplore6.RightButtonUp                          !Xplore
    OF xpEVENT:LeftButtonUp                           !Xplore
       BRW6.FileSeqOn = False                         !Xplore
       Xplore6.LeftButtonUp(0)                        !Xplore
    OF xpEVENT:Reset
       ThisWindow.Reset(False)                        !Xplore
    ELSE
       !BRW6.SavePosition()                           !Xplore
       Xplore6.HandleMyEvents()                       !Xplore
       !BRW6.RestorePosition()                        !Xplore
    END !CASE EVENT()                                 !Xplore
  END !IF FOCUS() = ?List                             !Xplore
  PARENT.TakeEvent


BRW6.TakeKey PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  IF RECORDS(SELF.ListQueue)
    CASE KEYCODE()
    OF MouseLeft2
      !Extension Templete Code:XploreOOPBrowse,BrowseBoxDoubleClickHandler
      IF Xplore6.LeftButton2()                        !Xplore
        RETURN(0)                                     !Xplore
      END                                             !Xplore
    END
  END
  ReturnValue = PARENT.TakeKey()
  RETURN ReturnValue


BRW6.TakeNewSelection PROCEDURE

  CODE
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  omit('***',ClarionetUsed=0)
  If clarionetserver:Active()
      If Keycode() = MouseRightUp
          SetKeyCode(0)
      End !If Keycode() = MouseRightUp
  End !If glo:WebJob
  ***
  PARENT.TakeNewSelection


BRW6.ValidateRecord PROCEDURE

ReturnValue          BYTE,AUTO

BRW6::RecordStatus   BYTE,AUTO
  CODE
  ReturnValue = PARENT.ValidateRecord()
  BRW6::RecordStatus=ReturnValue
  IF BRW6::RecordStatus NOT=Record:OK THEN RETURN BRW6::RecordStatus.
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
     GLO:Queue.Pointer = orp:Record_Number
     GET(GLO:Queue,GLO:Queue.Pointer)
    EXECUTE DASBRW::11:TAGDISPSTATUS
       IF ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
       IF ~ERRORCODE() THEN BRW6::RecordStatus = RECORD:FILTERED END
    END
  !--------------------------------------------------------------------------
  ! DAS_Tagging
  !--------------------------------------------------------------------------
  ReturnValue=BRW6::RecordStatus
  RETURN ReturnValue

!=======================================================
!Extension Templete Code:XploreOOPBrowse,LocalProcedures
!=======================================================
BRW6.SavePosition PROCEDURE()
  CODE
  SELF.Savedposition = SELF.Primary.Me.SaveFile()     !Xplore
  RETURN
BRW6.RestorePosition PROCEDURE()
  CODE
  SELF.Primary.Me.RestoreFile(SELF.SavedPosition)     !Xplore
  RETURN
BRW6.RecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  !Done here because Order is PROTECTED
  IF SELF.Order.MainKey &= NULL                       !Xplore
    TotalRecords = 0                                  !Xplore
  ELSE                                                !Xplore
    TotalRecords = RECORDS(SELF.Order.MainKey)        !Xplore  
  END                                                 !Xplore
  RETURN TotalRecords                                 !Xplore
BRW6.ResetPairsQ PROCEDURE()
  CODE
Xplore6.MyPreviewer PROCEDURE(PreviewQueue PQ,REPORT CPCS)
ReturnValue LONG
SaveCopies  LONG
  CODE
  !CPCS Previewer (c) copyright Creative PC Solutions, 1995-97
  ReturnValue                  = PrintPreview(PQ,0,,CPCS,0,,,,)
  CASE ReturnValue
  OF RequestCompleted !1
     SaveCopies                = PRINTER{PROPPRINT:Copies}
     PRINTER{PROPPRINT:Copies} = CPCS{PROPPRINT:Copies}
     HandleCopies(PQ,CPCS{PROPPRINT:Copies})
     CPCS{PROP:FlushPreview}   = True
     PRINTER{PROPPRINT:Copies} = SaveCopies
  OF RequestCancelled !2
  END
  RETURN (False)
!================================================================================
!Xplore6.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*STRING ColumnString,*CSTRING Pic)
Xplore6.GetStringForGraph PROCEDURE(SHORT QFieldNumber,*CSTRING Pic)
ColumnPicture CSTRING(20)
ColumnString  STRING(80)
XpDay         SHORT
XpMonth       SHORT
XpTime        STRING(8)
NumericValue  LONG
NumericSlot   LONG
NumericStart  LONG
Total         DECIMAL(11,2)
TextSfx       STRING(20)
  CODE
  ColumnPicture = CLIP(Pic)
  ColumnString  = CLIP(LEFT(FORMAT(WHAT(SELF.BQ,QFieldNumber),ColumnPicture)))
  Total         = 0
  TextSfx       = ''
  CASE QFieldNumber
  !
  ! Syntax of BrowseBox Queue Field Names are: BRW6.Q.PRE:Field  NOT XploreOOPListChild or =>5501
  !
  OF 1 !tmp:tag
  OF 3 !orp:Part_Number
  OF 8 !orp:Description
  OF 13 !type_temp
  OF 18 !job_number_temp
  OF 23 !orp:Quantity
  OF 28 !orp:Purchase_Cost
  OF 33 !orp:Sale_Cost
  OF 38 !orp:Retail_Cost
  OF 43 !orp:Date_Received
  OF 48 !orp:All_Received
  OF 49 !model_temp
  OF 54 !account_temp
  OF 59 !exchange_temp
  OF 64 !orp:Part_Ref_Number
  OF 65 !sto:Second_Location
  OF 66 !sto:Shelf_Location
  OF 67 !orp:Record_Number
  OF 68 !orp:Order_Number
  OF 69 !sto:Ref_Number
  OF 70 !sto:Part_Number
  OF 71 !sto:Description
  END !CASE QFieldNumber
  SELF.QSel.Name     = CLIP(ColumnString)
  GET(SELF.QSel,SELF.QSel.Name)
  IF ERRORCODE()
     SELF.QSel.Name  = CLIP(ColumnString)
     SELF.QSel.Count = 0
     SELF.QSel.Total = 0
     ADD(SELF.QSel,+SELF.QSel.Name)
     GET(SELF.QSel,+SELF.QSel.Name)
  END
  SELF.QSel.Count   += 1
  SELF.QSel.Total   += Total
  TextSfx            = FORMAT(SELF.QSEL.Total,ColumnPicture)
  SELF.QSel.TextSfx  = TextSfx
  PUT(SELF.QSel)
  RETURN

!================================================================================
Xplore6.SetNewOrderFields PROCEDURE()
  CODE
  BRW6.LocatorField  = SELF.sq.Field
  RETURN
!================================================================================
Xplore6.AddColumnSortOrder PROCEDURE(LONG SortQRecord)
  CODE
  !!SELF.FQ.SortField = SELF.BC.AddSortOrder(Xplore6Step22,orp:Part_Number_Key)
  SELF.FQ.SortField = SELF.BC.AddSortOrder(,orp:Part_Number_Key)
  EXECUTE SortQRecord
    BEGIN
      Xplore6Step1.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator1)
      Xplore6Locator1.Init(,tmp:tag,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step2.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator2)
      Xplore6Locator2.Init(?orp:Part_Number,orp:Part_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step3.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator3)
      Xplore6Locator3.Init(,orp:Description,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step4.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator4)
      Xplore6Locator4.Init(,type_temp,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step5.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator5)
      Xplore6Locator5.Init(,job_number_temp,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step6.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore6Locator6)
      Xplore6Locator6.Init(,orp:Quantity,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step7.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator7)
      Xplore6Locator7.Init(,orp:Purchase_Cost,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step8.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator8)
      Xplore6Locator8.Init(,orp:Sale_Cost,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step9.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator9)
      Xplore6Locator9.Init(,orp:Retail_Cost,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step10.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator10)
      Xplore6Locator10.Init(,orp:Date_Received,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step11.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator11)
      Xplore6Locator11.Init(,orp:All_Received,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step12.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator12)
      Xplore6Locator12.Init(,model_temp,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step13.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator13)
      Xplore6Locator13.Init(,account_temp,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step14.Init(+ScrollSort:AllowAlpha) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator14)
      Xplore6Locator14.Init(,exchange_temp,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step15.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore6Locator15)
      Xplore6Locator15.Init(,orp:Part_Ref_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step16.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator16)
      Xplore6Locator16.Init(?sto:Second_Location,sto:Second_Location,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step17.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator17)
      Xplore6Locator17.Init(?sto:Shelf_Location,sto:Shelf_Location,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step18.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore6Locator18)
      Xplore6Locator18.Init(,orp:Record_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step19.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore6Locator19)
      Xplore6Locator19.Init(,orp:Order_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step20.Init(+ScrollSort:AllowNumeric) !!GPF(Thumb)
      SELF.BC.AddLocator(Xplore6Locator20)
      Xplore6Locator20.Init(,sto:Ref_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step21.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator21)
      Xplore6Locator21.Init(,sto:Part_Number,1,SELF.BC)
    END !BEGIN
    BEGIN
      Xplore6Step22.Init(+ScrollSort:AllowAlpha,ScrollBy:Runtime) !!GPF(Thumb) 
      SELF.BC.AddLocator(Xplore6Locator22)
      Xplore6Locator22.Init(,sto:Description,1,SELF.BC)
    END !BEGIN
  END !EXECUTE
  SELF.BC.AddRange(orp:Order_Number,Relate:ORDPARTS,Relate:ORDERS)
  RETURN
!================================================================================
Xplore6.SetFilters PROCEDURE()
  CODE
  RETURN
!================================================================================
Xplore6.GetRecordsInKey PROCEDURE()
TotalRecords LONG
  CODE
  TotalRecords = BRW6.RecordsInKey()
  IF TotalRecords = 0
     TotalRecords = RECORDS(ORDPARTS)
  END
  RETURN TotalRecords
!================================================================================
Xplore6.ListBoxData PROCEDURE()
XpFileFields    GROUP,STATIC
                PSTRING('tmp:tag')                    !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('tmp:tag')                    !Header
                PSTRING('@S20')                       !Picture
                PSTRING('tmp:tag')                    !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(1)                               !Icon Specified
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Part_Number')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Description')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('type_temp')                  !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('type_temp')                  !Header
                PSTRING('@S20')                       !Picture
                PSTRING('type_temp')                  !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('job_number_temp')            !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('job_number_temp')            !Header
                PSTRING('@S20')                       !Picture
                PSTRING('job_number_temp')            !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Quantity')               !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Quantity')                   !Header
                PSTRING('@p<<<<<<<<<<<<<<#p')         !Picture
                PSTRING('Quantity')                   !Description
                STRING('R')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Purchase_Cost')          !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Purchase')                   !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Purchase')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Sale_Cost')              !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Sale')                       !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Sale')                       !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Retail_Cost')            !Field Name
                SHORT(56)                             !Default Column Width
                PSTRING('Retail')                     !Header
                PSTRING('@n7.2')                      !Picture
                PSTRING('Retail')                     !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Date_Received')          !Field Name
                SHORT(40)                             !Default Column Width
                PSTRING('Date Rcvd')                  !Header
                PSTRING('@d6b')                       !Picture
                PSTRING('Date Rcvd')                  !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:All_Received')           !Field Name
                SHORT(12)                             !Default Column Width
                PSTRING('All Rcvd')                   !Header
                PSTRING('@s3')                        !Picture
                PSTRING('All Rcvd')                   !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('model_temp')                 !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('model_temp')                 !Header
                PSTRING('@S20')                       !Picture
                PSTRING('model_temp')                 !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('account_temp')               !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('account_temp')               !Header
                PSTRING('@S20')                       !Picture
                PSTRING('account_temp')               !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('exchange_temp')              !Field Name
                SHORT(99)                             !get real Value from listbox a Runtime
                PSTRING('exchange_temp')              !Header
                PSTRING('@S20')                       !Picture
                PSTRING('exchange_temp')              !Description
                STRING('L')                           !Field Justification - Default LEFT
                BYTE(1)                               !in Listbox
                BYTE(1)                               !Cell Color Specified
                BYTE(0)                               !No Icon
                BYTE(0)                               !Computed Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Part_Ref_Number')        !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Part Ref Number')            !Header
                PSTRING('@n012')                      !Picture
                PSTRING('Part Ref Number')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sto:Second_Location')        !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Second Location')            !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Second Location')            !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sto:Shelf_Location')         !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Shelf Location')             !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Shelf Location')             !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Record_Number')          !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('record number')              !Header
                PSTRING('@n12')                       !Picture
                PSTRING('Record Number')              !Description
                STRING('L')                           !Field Justification
                BYTE(1)                               !in Listbox
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('orp:Order_Number')           !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Order Number')               !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Order Number')               !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(1)                               !View(Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sto:Ref_Number')             !Field Name
                SHORT(32)                             !Default Column Width
                PSTRING('Reference Number')           !Header
                PSTRING('@s8')                        !Picture
                PSTRING('Reference Number')           !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sto:Part_Number')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Part Number')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Part Number')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                PSTRING('sto:Description')            !Field Name
                SHORT(120)                            !Default Column Width
                PSTRING('Description')                !Header
                PSTRING('@s30')                       !Picture
                PSTRING('Description')                !Description
                STRING('L')                           !Field Justification
                BYTE(0)                               !Hot Field
                BYTE(0)                               !No Cell Color
                BYTE(0)                               !No Icon
                BYTE(2)                               !View(Non-Primary File) Field
                LONG(0)                               !TAB Control
                BYTE(0)                               !Totals Not Required
                BYTE(1)                               !Graph Allowed
                BYTE(1)                               !Graph Type - Count
                BYTE(1)                               !Sort Allowed
                STRING('S')                           !Locator Type
                !-------------------------
                END
XpDim           SHORT(22)
!--------------------------
XpSortFields    GROUP,STATIC
                END
XpSortDim       SHORT(0)
!--------------------------
XpAddSortFields GROUP,STATIC
                BYTE(0)                               !1=Apply to all columns 0=Apply only to non-specific columns
                SHORT(0)                              !Nos of Additional Sort Fields
                END
  CODE
  SELF.qdim     = XpDim
  SELF.gr.lbf  &= XpFileFields
  SELF.qdimS    = XpSortDim
  SELF.grS.sf  &= XpSortFields
  SELF.grA.sf  &= XpAddSortFields
  SELF.GetQfields()
  DO CreateFormatQs
  RETURN

CreateFormatQs ROUTINE
  SELF.CreateFormatQ(1)

Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults
  SELF.SetStrategy(?Browse:1, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?CurrentTab, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?List, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?Sheet2, Resize:FixLeft+Resize:FixTop, Resize:Resize)
  SELF.SetStrategy(?ORP:Part_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)
  SELF.SetStrategy(?ORD:Order_Number, Resize:FixLeft+Resize:FixTop, Resize:LockSize)

! After Embed Point: %LocalProcedures) DESC(Local Procedures) ARG()
