

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABRESIZE.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01013.INC'),ONCE        !Local module procedure declarations
                     END


Update_Stock_Model_Numbers PROCEDURE                  !Generated from procedure template - Window

CurrentTab           STRING(80)
LocalRequest         LONG
FilesOpened          BYTE
ActionMessage        CSTRING(40)
RecordChanged        BYTE,AUTO
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
History::stm:Record  LIKE(stm:RECORD),STATIC
QuickWindow          WINDOW('Update the STOMODEL File'),AT(,,411,332),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,IMM,HLP('Update_Stock_Model_Numbers'),SYSTEM,GRAY,RESIZE
                       SHEET,AT(4,4,212,216),USE(?CurrentTab),SPREAD
                         TAB('General'),USE(?Tab:1)
                           PROMPT('Model Number'),AT(8,20),USE(?STM:Model_Number:Prompt),TRN
                           ENTRY(@s30),AT(84,20,124,10),USE(stm:Model_Number),FONT('Tahoma',8,,FONT:bold),COLOR(COLOR:White),UPR,READONLY
                           PROMPT('Fault Code 1'),AT(8,36),USE(?stm:FaultCode1:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,36,124,10),USE(stm:FaultCode1),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 1'),TIP('Fault Code 1'),UPR
                           PROMPT('Fault Code 2'),AT(8,52),USE(?stm:FaultCode2:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,52,124,10),USE(stm:FaultCode2),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 2'),TIP('Fault Code 2'),UPR
                           PROMPT('Fault Code 3'),AT(8,68),USE(?stm:FaultCode3:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,68,124,10),USE(stm:FaultCode3),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 3'),TIP('Fault Code 3'),UPR
                           PROMPT('Fault Code 4'),AT(8,84),USE(?stm:FaultCode4:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,84,124,10),USE(stm:FaultCode4),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 4'),TIP('Fault Code 4'),UPR
                           PROMPT('Fault Code 5'),AT(8,100),USE(?stm:FaultCode5:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,100,124,10),USE(stm:FaultCode5),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 5'),TIP('Fault Code 5'),UPR
                           PROMPT('Fault Code 6'),AT(8,116),USE(?stm:FaultCode6:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,116,124,10),USE(stm:FaultCode6),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 6'),TIP('Fault Code 6'),UPR
                           PROMPT('Fault Code 7'),AT(8,132),USE(?stm:FaultCode7:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,132,124,10),USE(stm:FaultCode7),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 7'),TIP('Fault Code 7'),UPR
                           PROMPT('Fault Code 8'),AT(8,148),USE(?stm:FaultCode8:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,148,124,10),USE(stm:FaultCode8),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 8'),TIP('Fault Code 8'),UPR
                           PROMPT('Fault Code 9'),AT(8,164),USE(?stm:FaultCode9:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(84,164,124,10),USE(stm:FaultCode9),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 9'),TIP('Fault Code 9'),UPR
                           PROMPT('Fault Code 10'),AT(11,180),USE(?stm:FaultCode10:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(63,180,124,10),USE(stm:FaultCode10),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 10'),TIP('Fault Code 10'),UPR
                           PROMPT('Fault Code 11'),AT(15,196),USE(?stm:FaultCode11:Prompt),TRN,LEFT,FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI)
                           ENTRY(@s30),AT(67,196,124,10),USE(stm:FaultCode11),LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),MSG('Fault Code 11'),TIP('Fault Code 11'),UPR
                         END
                       END
                       BUTTON('&OK'),AT(80,284,56,16),USE(?OK),LEFT,ICON('ok.gif'),DEFAULT
                       BUTTON('Cancel'),AT(160,284,56,16),USE(?Cancel),LEFT,ICON('cancel.gif')
                     END

ThisWindow           CLASS(WindowManager)
Ask                    PROCEDURE(),DERIVED
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
PrimeFields            PROCEDURE(),PROC,DERIVED
Run                    PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
Resizer              CLASS(WindowResizeClass)
Init                   PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup
CurCtrlFeq          LONG
FieldColorQueue     QUEUE
Feq                   LONG
OldColor              LONG
                    END

  CODE
  GlobalResponse = ThisWindow.Run()

RecolourWindow      Routine

    Do RecolourWindow:Window
    ?CurrentTab{prop:Color} = 15066597
    ?Tab:1{prop:Color} = 15066597
    ?STM:Model_Number:Prompt{prop:FontColor} = -1
    ?STM:Model_Number:Prompt{prop:Color} = 15066597
    If ?stm:Model_Number{prop:ReadOnly} = True
        ?stm:Model_Number{prop:FontColor} = 65793
        ?stm:Model_Number{prop:Color} = 15066597
    Elsif ?stm:Model_Number{prop:Req} = True
        ?stm:Model_Number{prop:FontColor} = 65793
        ?stm:Model_Number{prop:Color} = 8454143
    Else ! If ?stm:Model_Number{prop:Req} = True
        ?stm:Model_Number{prop:FontColor} = 65793
        ?stm:Model_Number{prop:Color} = 16777215
    End ! If ?stm:Model_Number{prop:Req} = True
    ?stm:Model_Number{prop:Trn} = 0
    ?stm:Model_Number{prop:FontStyle} = font:Bold
    ?stm:FaultCode1:Prompt{prop:FontColor} = -1
    ?stm:FaultCode1:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode1{prop:ReadOnly} = True
        ?stm:FaultCode1{prop:FontColor} = 65793
        ?stm:FaultCode1{prop:Color} = 15066597
    Elsif ?stm:FaultCode1{prop:Req} = True
        ?stm:FaultCode1{prop:FontColor} = 65793
        ?stm:FaultCode1{prop:Color} = 8454143
    Else ! If ?stm:FaultCode1{prop:Req} = True
        ?stm:FaultCode1{prop:FontColor} = 65793
        ?stm:FaultCode1{prop:Color} = 16777215
    End ! If ?stm:FaultCode1{prop:Req} = True
    ?stm:FaultCode1{prop:Trn} = 0
    ?stm:FaultCode1{prop:FontStyle} = font:Bold
    ?stm:FaultCode2:Prompt{prop:FontColor} = -1
    ?stm:FaultCode2:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode2{prop:ReadOnly} = True
        ?stm:FaultCode2{prop:FontColor} = 65793
        ?stm:FaultCode2{prop:Color} = 15066597
    Elsif ?stm:FaultCode2{prop:Req} = True
        ?stm:FaultCode2{prop:FontColor} = 65793
        ?stm:FaultCode2{prop:Color} = 8454143
    Else ! If ?stm:FaultCode2{prop:Req} = True
        ?stm:FaultCode2{prop:FontColor} = 65793
        ?stm:FaultCode2{prop:Color} = 16777215
    End ! If ?stm:FaultCode2{prop:Req} = True
    ?stm:FaultCode2{prop:Trn} = 0
    ?stm:FaultCode2{prop:FontStyle} = font:Bold
    ?stm:FaultCode3:Prompt{prop:FontColor} = -1
    ?stm:FaultCode3:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode3{prop:ReadOnly} = True
        ?stm:FaultCode3{prop:FontColor} = 65793
        ?stm:FaultCode3{prop:Color} = 15066597
    Elsif ?stm:FaultCode3{prop:Req} = True
        ?stm:FaultCode3{prop:FontColor} = 65793
        ?stm:FaultCode3{prop:Color} = 8454143
    Else ! If ?stm:FaultCode3{prop:Req} = True
        ?stm:FaultCode3{prop:FontColor} = 65793
        ?stm:FaultCode3{prop:Color} = 16777215
    End ! If ?stm:FaultCode3{prop:Req} = True
    ?stm:FaultCode3{prop:Trn} = 0
    ?stm:FaultCode3{prop:FontStyle} = font:Bold
    ?stm:FaultCode4:Prompt{prop:FontColor} = -1
    ?stm:FaultCode4:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode4{prop:ReadOnly} = True
        ?stm:FaultCode4{prop:FontColor} = 65793
        ?stm:FaultCode4{prop:Color} = 15066597
    Elsif ?stm:FaultCode4{prop:Req} = True
        ?stm:FaultCode4{prop:FontColor} = 65793
        ?stm:FaultCode4{prop:Color} = 8454143
    Else ! If ?stm:FaultCode4{prop:Req} = True
        ?stm:FaultCode4{prop:FontColor} = 65793
        ?stm:FaultCode4{prop:Color} = 16777215
    End ! If ?stm:FaultCode4{prop:Req} = True
    ?stm:FaultCode4{prop:Trn} = 0
    ?stm:FaultCode4{prop:FontStyle} = font:Bold
    ?stm:FaultCode5:Prompt{prop:FontColor} = -1
    ?stm:FaultCode5:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode5{prop:ReadOnly} = True
        ?stm:FaultCode5{prop:FontColor} = 65793
        ?stm:FaultCode5{prop:Color} = 15066597
    Elsif ?stm:FaultCode5{prop:Req} = True
        ?stm:FaultCode5{prop:FontColor} = 65793
        ?stm:FaultCode5{prop:Color} = 8454143
    Else ! If ?stm:FaultCode5{prop:Req} = True
        ?stm:FaultCode5{prop:FontColor} = 65793
        ?stm:FaultCode5{prop:Color} = 16777215
    End ! If ?stm:FaultCode5{prop:Req} = True
    ?stm:FaultCode5{prop:Trn} = 0
    ?stm:FaultCode5{prop:FontStyle} = font:Bold
    ?stm:FaultCode6:Prompt{prop:FontColor} = -1
    ?stm:FaultCode6:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode6{prop:ReadOnly} = True
        ?stm:FaultCode6{prop:FontColor} = 65793
        ?stm:FaultCode6{prop:Color} = 15066597
    Elsif ?stm:FaultCode6{prop:Req} = True
        ?stm:FaultCode6{prop:FontColor} = 65793
        ?stm:FaultCode6{prop:Color} = 8454143
    Else ! If ?stm:FaultCode6{prop:Req} = True
        ?stm:FaultCode6{prop:FontColor} = 65793
        ?stm:FaultCode6{prop:Color} = 16777215
    End ! If ?stm:FaultCode6{prop:Req} = True
    ?stm:FaultCode6{prop:Trn} = 0
    ?stm:FaultCode6{prop:FontStyle} = font:Bold
    ?stm:FaultCode7:Prompt{prop:FontColor} = -1
    ?stm:FaultCode7:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode7{prop:ReadOnly} = True
        ?stm:FaultCode7{prop:FontColor} = 65793
        ?stm:FaultCode7{prop:Color} = 15066597
    Elsif ?stm:FaultCode7{prop:Req} = True
        ?stm:FaultCode7{prop:FontColor} = 65793
        ?stm:FaultCode7{prop:Color} = 8454143
    Else ! If ?stm:FaultCode7{prop:Req} = True
        ?stm:FaultCode7{prop:FontColor} = 65793
        ?stm:FaultCode7{prop:Color} = 16777215
    End ! If ?stm:FaultCode7{prop:Req} = True
    ?stm:FaultCode7{prop:Trn} = 0
    ?stm:FaultCode7{prop:FontStyle} = font:Bold
    ?stm:FaultCode8:Prompt{prop:FontColor} = -1
    ?stm:FaultCode8:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode8{prop:ReadOnly} = True
        ?stm:FaultCode8{prop:FontColor} = 65793
        ?stm:FaultCode8{prop:Color} = 15066597
    Elsif ?stm:FaultCode8{prop:Req} = True
        ?stm:FaultCode8{prop:FontColor} = 65793
        ?stm:FaultCode8{prop:Color} = 8454143
    Else ! If ?stm:FaultCode8{prop:Req} = True
        ?stm:FaultCode8{prop:FontColor} = 65793
        ?stm:FaultCode8{prop:Color} = 16777215
    End ! If ?stm:FaultCode8{prop:Req} = True
    ?stm:FaultCode8{prop:Trn} = 0
    ?stm:FaultCode8{prop:FontStyle} = font:Bold
    ?stm:FaultCode9:Prompt{prop:FontColor} = -1
    ?stm:FaultCode9:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode9{prop:ReadOnly} = True
        ?stm:FaultCode9{prop:FontColor} = 65793
        ?stm:FaultCode9{prop:Color} = 15066597
    Elsif ?stm:FaultCode9{prop:Req} = True
        ?stm:FaultCode9{prop:FontColor} = 65793
        ?stm:FaultCode9{prop:Color} = 8454143
    Else ! If ?stm:FaultCode9{prop:Req} = True
        ?stm:FaultCode9{prop:FontColor} = 65793
        ?stm:FaultCode9{prop:Color} = 16777215
    End ! If ?stm:FaultCode9{prop:Req} = True
    ?stm:FaultCode9{prop:Trn} = 0
    ?stm:FaultCode9{prop:FontStyle} = font:Bold
    ?stm:FaultCode10:Prompt{prop:FontColor} = -1
    ?stm:FaultCode10:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode10{prop:ReadOnly} = True
        ?stm:FaultCode10{prop:FontColor} = 65793
        ?stm:FaultCode10{prop:Color} = 15066597
    Elsif ?stm:FaultCode10{prop:Req} = True
        ?stm:FaultCode10{prop:FontColor} = 65793
        ?stm:FaultCode10{prop:Color} = 8454143
    Else ! If ?stm:FaultCode10{prop:Req} = True
        ?stm:FaultCode10{prop:FontColor} = 65793
        ?stm:FaultCode10{prop:Color} = 16777215
    End ! If ?stm:FaultCode10{prop:Req} = True
    ?stm:FaultCode10{prop:Trn} = 0
    ?stm:FaultCode10{prop:FontStyle} = font:Bold
    ?stm:FaultCode11:Prompt{prop:FontColor} = -1
    ?stm:FaultCode11:Prompt{prop:Color} = 15066597
    If ?stm:FaultCode11{prop:ReadOnly} = True
        ?stm:FaultCode11{prop:FontColor} = 65793
        ?stm:FaultCode11{prop:Color} = 15066597
    Elsif ?stm:FaultCode11{prop:Req} = True
        ?stm:FaultCode11{prop:FontColor} = 65793
        ?stm:FaultCode11{prop:Color} = 8454143
    Else ! If ?stm:FaultCode11{prop:Req} = True
        ?stm:FaultCode11{prop:FontColor} = 65793
        ?stm:FaultCode11{prop:Color} = 16777215
    End ! If ?stm:FaultCode11{prop:Req} = True
    ?stm:FaultCode11{prop:Trn} = 0
    ?stm:FaultCode11{prop:FontStyle} = font:Bold

RecolourWindow:Window       Routine
    0{prop:Color} = 15592674


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Update_Stock_Model_Numbers',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('CurrentTab',CurrentTab,'Update_Stock_Model_Numbers',1)
    SolaceViewVars('LocalRequest',LocalRequest,'Update_Stock_Model_Numbers',1)
    SolaceViewVars('FilesOpened',FilesOpened,'Update_Stock_Model_Numbers',1)
    SolaceViewVars('ActionMessage',ActionMessage,'Update_Stock_Model_Numbers',1)
    SolaceViewVars('RecordChanged',RecordChanged,'Update_Stock_Model_Numbers',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?CurrentTab;  SolaceCtrlName = '?CurrentTab';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab:1;  SolaceCtrlName = '?Tab:1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?STM:Model_Number:Prompt;  SolaceCtrlName = '?STM:Model_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:Model_Number;  SolaceCtrlName = '?stm:Model_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode1:Prompt;  SolaceCtrlName = '?stm:FaultCode1:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode1;  SolaceCtrlName = '?stm:FaultCode1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode2:Prompt;  SolaceCtrlName = '?stm:FaultCode2:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode2;  SolaceCtrlName = '?stm:FaultCode2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode3:Prompt;  SolaceCtrlName = '?stm:FaultCode3:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode3;  SolaceCtrlName = '?stm:FaultCode3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode4:Prompt;  SolaceCtrlName = '?stm:FaultCode4:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode4;  SolaceCtrlName = '?stm:FaultCode4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode5:Prompt;  SolaceCtrlName = '?stm:FaultCode5:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode5;  SolaceCtrlName = '?stm:FaultCode5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode6:Prompt;  SolaceCtrlName = '?stm:FaultCode6:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode6;  SolaceCtrlName = '?stm:FaultCode6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode7:Prompt;  SolaceCtrlName = '?stm:FaultCode7:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode7;  SolaceCtrlName = '?stm:FaultCode7';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode8:Prompt;  SolaceCtrlName = '?stm:FaultCode8:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode8;  SolaceCtrlName = '?stm:FaultCode8';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode9:Prompt;  SolaceCtrlName = '?stm:FaultCode9:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode9;  SolaceCtrlName = '?stm:FaultCode9';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode10:Prompt;  SolaceCtrlName = '?stm:FaultCode10:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode10;  SolaceCtrlName = '?stm:FaultCode10';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode11:Prompt;  SolaceCtrlName = '?stm:FaultCode11:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?stm:FaultCode11;  SolaceCtrlName = '?stm:FaultCode11';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OK;  SolaceCtrlName = '?OK';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Cancel;  SolaceCtrlName = '?Cancel';Add(LSolCtrlQ,+SolaceUseRef)



ThisWindow.Ask PROCEDURE

  CODE
  CASE SELF.Request
  OF ViewRecord
    ActionMessage = 'View Record'
  OF InsertRecord
    ActionMessage = 'Record Will Be Added'
  OF ChangeRecord
    ActionMessage = 'Record Will Be Changed'
  END
  QuickWindow{Prop:Text} = ActionMessage
  PARENT.Ask


ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  GlobalErrors.SetProcedureName('Update_Stock_Model_Numbers')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Update_Stock_Model_Numbers')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?STM:Model_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  SELF.HistoryKey = 734
  SELF.AddHistoryFile(stm:Record,History::stm:Record)
  SELF.AddHistoryField(?stm:Model_Number,3)
  SELF.AddHistoryField(?stm:FaultCode1,8)
  SELF.AddHistoryField(?stm:FaultCode2,9)
  SELF.AddHistoryField(?stm:FaultCode3,10)
  SELF.AddHistoryField(?stm:FaultCode4,11)
  SELF.AddHistoryField(?stm:FaultCode5,12)
  SELF.AddHistoryField(?stm:FaultCode6,13)
  SELF.AddHistoryField(?stm:FaultCode7,14)
  SELF.AddHistoryField(?stm:FaultCode8,15)
  SELF.AddHistoryField(?stm:FaultCode9,16)
  SELF.AddHistoryField(?stm:FaultCode10,17)
  SELF.AddHistoryField(?stm:FaultCode11,18)
  SELF.AddUpdateFile(Access:STOMODEL)
  SELF.AddItem(?Cancel,RequestCancelled)
  Relate:STOCK.Open
  SELF.FilesOpened = True
  SELF.Primary &= Relate:STOMODEL
  IF SELF.Request = ViewRecord
    SELF.InsertAction = Insert:None
    SELF.DeleteAction = Delete:None
    SELF.ChangeAction = 0
    SELF.CancelAction = Cancel:Cancel
    SELF.OkControl = 0
  ELSE
    SELF.OkControl = ?OK
    IF SELF.PrimeUpdate() THEN RETURN Level:Notify.
  END
  OPEN(QuickWindow)
  SELF.Opened=True
  Do RecolourWindow
  ! support for CPCS
  Resizer.Init(AppStrategy:Spread,Resize:SetMinSize)
  SELF.AddItem(Resizer)
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:STOCK.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Update_Stock_Model_Numbers',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.PrimeFields PROCEDURE

  CODE
    stm:Manufacturer = sto:manufacturer
  PARENT.PrimeFields


ThisWindow.Run PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Run()
  IF SELF.Request = ViewRecord
    ReturnValue = RequestCancelled
  END
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OK
      ThisWindow.Update
      IF SELF.Request = ViewRecord
        POST(EVENT:CloseWindow)
      END
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Update_Stock_Model_Numbers')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


Resizer.Init PROCEDURE(BYTE AppStrategy=AppStrategy:Resize,BYTE SetWindowMinSize=False,BYTE SetWindowMaxSize=False)


  CODE
  PARENT.Init(AppStrategy,SetWindowMinSize,SetWindowMaxSize)
  SELF.SetParentDefaults

