

   MEMBER('sbb01app.clw')                             ! This is a MEMBER module


   INCLUDE('ABDROPS.INC'),ONCE
   INCLUDE('ABTOOLBA.INC'),ONCE
   INCLUDE('ABWINDOW.INC'),ONCE

                     MAP
                       INCLUDE('SBB01024.INC'),ONCE        !Local module procedure declarations
                     END


Amend_Pending_Part PROCEDURE                          !Generated from procedure template - Window

FilesOpened          BYTE
days_7_temp          LONG
days_30_temp         LONG
days_60_temp         LONG
days_90_temp         LONG
average_text_temp    STRING(8)
average_temp         LONG
tmp:CostsChanged     BYTE(0)
save_sto_id          USHORT,AUTO
save_loc_id          USHORT,AUTO
save_shi_id          USHORT,AUTO
save_res_id          USHORT,AUTO
part_record_number_temp REAL
retail_cost_temp     REAL
saved_retail_cost_temp REAL
saved_purchase_cost_temp REAL
saved_sale_cost_temp REAL
saved_quantity_temp  REAL
save_par_id          USHORT,AUTO
supplier_temp        STRING(30)
Purchase_Cost_temp   REAL
Sale_Cost_temp       REAL
save_wpr_id          USHORT,AUTO
Quantity_Temp        REAL
LSolCtrlQ            QUEUE,PRE()
SolaceUseRef         LONG
SolaceCtrlName       STRING(20)
                     END
Queue:FileDropCombo  QUEUE                            !Queue declaration for browse/combo box using ?supplier_temp
sup:Company_Name       LIKE(sup:Company_Name)         !List box control field - type derived from field
sup:RecordNumber       LIKE(sup:RecordNumber)         !Primary key field - type derived from field
Mark                   BYTE                           !Entry's marked status
ViewPosition           STRING(1024)                   !Entry's view position
                     END
FDCB2::View:FileDropCombo VIEW(SUPPLIER)
                       PROJECT(sup:Company_Name)
                       PROJECT(sup:RecordNumber)
                     END
window               WINDOW('Pending Part Details'),AT(,,359,167),FONT('Tahoma',8,,FONT:regular,CHARSET:ANSI),CENTER,ICON('PC.ICO'),TILED,GRAY,DOUBLE,IMM
                       SHEET,AT(4,4,212,132),USE(?Sheet1),SPREAD
                         TAB('Pending Part Details'),USE(?Tab1)
                           PROMPT('Part Number'),AT(8,20),USE(?OPE:Part_Number:Prompt)
                           ENTRY(@s30),AT(84,20,124,10),USE(ope:Part_Number),SKIP,FONT(,,,FONT:bold),UPR,READONLY
                           PROMPT('Description'),AT(8,36),USE(?OPE:Description:Prompt)
                           ENTRY(@s30),AT(84,36,124,10),USE(ope:Description),SKIP,LEFT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,READONLY
                           COMBO(@s30),AT(84,52,124,10),USE(supplier_temp),IMM,VSCROLL,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,FORMAT('120L(2)|M@s30@'),DROP(10,124),FROM(Queue:FileDropCombo)
                           PROMPT('Purchase Cost'),AT(8,68),USE(?Purchase_Cost_temp:Prompt),TRN
                           ENTRY(@n14.2),AT(84,68,64,10),USE(Purchase_Cost_temp),FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Trade Price'),AT(8,84),USE(?Sale_Cost_temp:Prompt),TRN
                           ENTRY(@n14.2),AT(84,84,64,10),USE(Sale_Cost_temp),LEFT,FONT('Tahoma',8,,FONT:bold),UPR
                           PROMPT('Retail Price'),AT(8,100),USE(?retail_cost_temp:Prompt)
                           ENTRY(@n14.2),AT(84,100,64,10),USE(retail_cost_temp),LEFT,FONT('Tahoma',,,FONT:bold,CHARSET:ANSI),UPR
                           SPIN(@s8),AT(84,116,64,10),USE(Quantity_Temp),FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI),UPR,RANGE(1,99999999),STEP(1)
                           PROMPT('Quantity'),AT(8,116),USE(?Prompt5)
                           PROMPT('Supplier'),AT(8,52),USE(?Prompt2)
                         END
                       END
                       SHEET,AT(220,4,136,132),USE(?Sheet2),SPREAD
                         TAB('Stock Usage'),USE(?Tab3)
                           PROMPT('0 - 7 Days'),AT(224,20),USE(?Prompt6)
                           STRING(@n-14),AT(280,20),USE(days_7_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('0 - 30 Days'),AT(224,32),USE(?Prompt6:2)
                           STRING(@n-14),AT(280,32),USE(days_30_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('31 - 60 Days'),AT(224,44),USE(?Prompt6:3)
                           STRING(@n-14),AT(280,44),USE(days_60_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           PROMPT('61 - 90 Days'),AT(224,56),USE(?Prompt6:4)
                           STRING(@n-14),AT(280,56),USE(days_90_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                           LINE,AT(283,68,67,0),USE(?Line1),COLOR(COLOR:Black)
                           PROMPT('Average Daily Use'),AT(224,72),USE(?Prompt6:5)
                           STRING(@s8),AT(312,72),USE(average_text_temp),RIGHT,FONT('Tahoma',8,,FONT:bold,CHARSET:ANSI)
                         END
                       END
                       BUTTON('&OK'),AT(240,144,56,16),USE(?OkButton),LEFT,ICON('ok.gif')
                       BUTTON('Cancel'),AT(296,144,56,16),USE(?CancelButton),LEFT,ICON('cancel.gif'),STD(STD:Close)
                       PANEL,AT(4,140,352,24),USE(?Panel1),FILL(COLOR:Silver)
                     END

ThisWindow           CLASS(WindowManager)
Init                   PROCEDURE(),BYTE,PROC,DERIVED
Kill                   PROCEDURE(),BYTE,PROC,DERIVED
TakeAccepted           PROCEDURE(),BYTE,PROC,DERIVED
TakeEvent              PROCEDURE(),BYTE,PROC,DERIVED
TakeFieldEvent         PROCEDURE(),BYTE,PROC,DERIVED
TakeWindowEvent        PROCEDURE(),BYTE,PROC,DERIVED
                     END

Toolbar              ToolbarClass
FDCB2                CLASS(FileDropComboClass)        !File drop combo manager
Q                      &Queue:FileDropCombo           !Reference to browse queue type
                     END

!Save Entry Fields Incase Of Lookup
!End Save Entry Fields Incase Of Lookup

  CODE
  GlobalResponse = ThisWindow.Run()

! Before Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()
RecolourWindow      Routine

    Do RecolourWindow:Window
    ?Sheet1{prop:Color} = 15066597
    ?Tab1{prop:Color} = 15066597
    ?OPE:Part_Number:Prompt{prop:FontColor} = -1
    ?OPE:Part_Number:Prompt{prop:Color} = 15066597
    If ?ope:Part_Number{prop:ReadOnly} = True
        ?ope:Part_Number{prop:FontColor} = 65793
        ?ope:Part_Number{prop:Color} = 15066597
    Elsif ?ope:Part_Number{prop:Req} = True
        ?ope:Part_Number{prop:FontColor} = 65793
        ?ope:Part_Number{prop:Color} = 8454143
    Else ! If ?ope:Part_Number{prop:Req} = True
        ?ope:Part_Number{prop:FontColor} = 65793
        ?ope:Part_Number{prop:Color} = 16777215
    End ! If ?ope:Part_Number{prop:Req} = True
    ?ope:Part_Number{prop:Trn} = 0
    ?ope:Part_Number{prop:FontStyle} = font:Bold
    ?OPE:Description:Prompt{prop:FontColor} = -1
    ?OPE:Description:Prompt{prop:Color} = 15066597
    If ?ope:Description{prop:ReadOnly} = True
        ?ope:Description{prop:FontColor} = 65793
        ?ope:Description{prop:Color} = 15066597
    Elsif ?ope:Description{prop:Req} = True
        ?ope:Description{prop:FontColor} = 65793
        ?ope:Description{prop:Color} = 8454143
    Else ! If ?ope:Description{prop:Req} = True
        ?ope:Description{prop:FontColor} = 65793
        ?ope:Description{prop:Color} = 16777215
    End ! If ?ope:Description{prop:Req} = True
    ?ope:Description{prop:Trn} = 0
    ?ope:Description{prop:FontStyle} = font:Bold
    If ?supplier_temp{prop:ReadOnly} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 15066597
    Elsif ?supplier_temp{prop:Req} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 8454143
    Else ! If ?supplier_temp{prop:Req} = True
        ?supplier_temp{prop:FontColor} = 65793
        ?supplier_temp{prop:Color} = 16777215
    End ! If ?supplier_temp{prop:Req} = True
    ?supplier_temp{prop:Trn} = 0
    ?supplier_temp{prop:FontStyle} = font:Bold
    ?Purchase_Cost_temp:Prompt{prop:FontColor} = -1
    ?Purchase_Cost_temp:Prompt{prop:Color} = 15066597
    If ?Purchase_Cost_temp{prop:ReadOnly} = True
        ?Purchase_Cost_temp{prop:FontColor} = 65793
        ?Purchase_Cost_temp{prop:Color} = 15066597
    Elsif ?Purchase_Cost_temp{prop:Req} = True
        ?Purchase_Cost_temp{prop:FontColor} = 65793
        ?Purchase_Cost_temp{prop:Color} = 8454143
    Else ! If ?Purchase_Cost_temp{prop:Req} = True
        ?Purchase_Cost_temp{prop:FontColor} = 65793
        ?Purchase_Cost_temp{prop:Color} = 16777215
    End ! If ?Purchase_Cost_temp{prop:Req} = True
    ?Purchase_Cost_temp{prop:Trn} = 0
    ?Purchase_Cost_temp{prop:FontStyle} = font:Bold
    ?Sale_Cost_temp:Prompt{prop:FontColor} = -1
    ?Sale_Cost_temp:Prompt{prop:Color} = 15066597
    If ?Sale_Cost_temp{prop:ReadOnly} = True
        ?Sale_Cost_temp{prop:FontColor} = 65793
        ?Sale_Cost_temp{prop:Color} = 15066597
    Elsif ?Sale_Cost_temp{prop:Req} = True
        ?Sale_Cost_temp{prop:FontColor} = 65793
        ?Sale_Cost_temp{prop:Color} = 8454143
    Else ! If ?Sale_Cost_temp{prop:Req} = True
        ?Sale_Cost_temp{prop:FontColor} = 65793
        ?Sale_Cost_temp{prop:Color} = 16777215
    End ! If ?Sale_Cost_temp{prop:Req} = True
    ?Sale_Cost_temp{prop:Trn} = 0
    ?Sale_Cost_temp{prop:FontStyle} = font:Bold
    ?retail_cost_temp:Prompt{prop:FontColor} = -1
    ?retail_cost_temp:Prompt{prop:Color} = 15066597
    If ?retail_cost_temp{prop:ReadOnly} = True
        ?retail_cost_temp{prop:FontColor} = 65793
        ?retail_cost_temp{prop:Color} = 15066597
    Elsif ?retail_cost_temp{prop:Req} = True
        ?retail_cost_temp{prop:FontColor} = 65793
        ?retail_cost_temp{prop:Color} = 8454143
    Else ! If ?retail_cost_temp{prop:Req} = True
        ?retail_cost_temp{prop:FontColor} = 65793
        ?retail_cost_temp{prop:Color} = 16777215
    End ! If ?retail_cost_temp{prop:Req} = True
    ?retail_cost_temp{prop:Trn} = 0
    ?retail_cost_temp{prop:FontStyle} = font:Bold
    If ?Quantity_Temp{prop:ReadOnly} = True
        ?Quantity_Temp{prop:FontColor} = 65793
        ?Quantity_Temp{prop:Color} = 15066597
    Elsif ?Quantity_Temp{prop:Req} = True
        ?Quantity_Temp{prop:FontColor} = 65793
        ?Quantity_Temp{prop:Color} = 8454143
    Else ! If ?Quantity_Temp{prop:Req} = True
        ?Quantity_Temp{prop:FontColor} = 65793
        ?Quantity_Temp{prop:Color} = 16777215
    End ! If ?Quantity_Temp{prop:Req} = True
    ?Quantity_Temp{prop:Trn} = 0
    ?Quantity_Temp{prop:FontStyle} = font:Bold
    ?Prompt5{prop:FontColor} = -1
    ?Prompt5{prop:Color} = 15066597
    ?Prompt2{prop:FontColor} = -1
    ?Prompt2{prop:Color} = 15066597
    ?Sheet2{prop:Color} = 15066597
    ?Tab3{prop:Color} = 15066597
    ?Prompt6{prop:FontColor} = -1
    ?Prompt6{prop:Color} = 15066597
    ?days_7_temp{prop:FontColor} = -1
    ?days_7_temp{prop:Color} = 15066597
    ?Prompt6:2{prop:FontColor} = -1
    ?Prompt6:2{prop:Color} = 15066597
    ?days_30_temp{prop:FontColor} = -1
    ?days_30_temp{prop:Color} = 15066597
    ?Prompt6:3{prop:FontColor} = -1
    ?Prompt6:3{prop:Color} = 15066597
    ?days_60_temp{prop:FontColor} = -1
    ?days_60_temp{prop:Color} = 15066597
    ?Prompt6:4{prop:FontColor} = -1
    ?Prompt6:4{prop:Color} = 15066597
    ?days_90_temp{prop:FontColor} = -1
    ?days_90_temp{prop:Color} = 15066597
    ?Prompt6:5{prop:FontColor} = -1
    ?Prompt6:5{prop:Color} = 15066597
    ?average_text_temp{prop:FontColor} = -1
    ?average_text_temp{prop:Color} = 15066597
    ?Panel1{prop:Fill} = 15066597


RecolourWindow:Window       Routine
    0{prop:Color} = 15592674
Replicate       Routine
    If tmp:CostsChanged = 1
        Case MessageEx('Do you wish to update all occurances of this part with the amended costs?','ServiceBase 2000',|
                       'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
            Of 1 ! &Yes Button
                access:stock_alias.clearkey(sto_ali:ref_number_key)
                sto_ali:ref_number = ope:part_ref_number
                if access:stock_alias.fetch(sto_ali:ref_number_key) = Level:Benign
                    setcursor(cursor:wait)
                    save_loc_id = access:location.savefile()
                    set(loc:location_key)
                    loop
                        if access:location.next()
                           break
                        end !if
                        save_sto_id = access:stock.savefile()
                        access:stock.clearkey(sto:location_part_description_key)
                        sto:location    = loc:location
                        sto:part_number = sto_ali:part_number
                        sto:description = sto_ali:description
                        set(sto:location_part_description_key,sto:location_part_description_key)
                        loop
                            if access:stock.next()
                               break
                            end !if
                            if sto:location    <> loc:location  |
                            or sto:part_number <> sto_ali:part_number  |
                            or sto:description <> sto_ali:description  |
                                then break.  ! end if
                            If sto:ref_number       = sto_ali:ref_number
                                Cycle
                            End!If sto:ref_number_temp  = ref_number_temp
                            sto:purchase_cost=sto_ali:Purchase_Cost
                            sto:sale_cost=sto_ali:Sale_Cost
                            sto:retail_cost=sto_ali:Retail_Cost
                            access:stock.update()

                            get(stohist,0)
                            if access:stohist.primerecord() = level:benign
                                shi:ref_number           = sto:ref_number
                                access:users.clearkey(use:password_key)
                                use:password              =glo:password
                                access:users.fetch(use:password_key)
                                shi:user                  = use:user_code    
                                shi:date                 = today()
                                shi:transaction_type     = 'ADD'
                                shi:despatch_note_number = ''
                                shi:job_number           = ''
                                shi:quantity             = 0
                                shi:purchase_cost        = STO:PURCHASE_COST
                                shi:sale_cost            = STO:SALE_COST
                                shi:retail_cost          = STO:RETAIL_COST
                                shi:information          = 'PREVIOUS DETAILS:-' & |
                                                            '<13,10>PURCHASE COST: ' & CLip(sto_ali:Purchase_Cost) &|
                                                            '<13,10>TRADE PRICE: ' & CLip(sto_ali:Sale_Cost) &|
                                                            '<13,10>RETAIL PRICE: ' & CLip(sto_ali:Retail_Cost)
                                shi:notes                = 'DETAILS CHANGED'
                                if access:stohist.insert()
                                   access:stohist.cancelautoinc()
                                end
                            end!if access:stohist.primerecord() = level:benign
                        end !loop
                        access:stock.restorefile(save_sto_id)
                    end !loop
                    access:location.restorefile(save_loc_id)
                    setcursor()
                End!if access:stock.fetch(sto:ref_number_key) = Level:Benign
            Of 2 ! &No Button
        End!Case MessageEx

    End!If tmp:CostsChanged = 1


SolaceVariView      Routine
  If 0{Prop:AcceptAll} = true then
    exit
  end
  SolaceCurThreadNo = 'Values for THREAD() = ' & Thread()
  SolaceViewVars('',0,'Amend_Pending_Part',3)
  SolaceViewVars('',0,'SolaceGlobalRefresh',0)
    SolaceViewVars('FilesOpened',FilesOpened,'Amend_Pending_Part',1)
    SolaceViewVars('days_7_temp',days_7_temp,'Amend_Pending_Part',1)
    SolaceViewVars('days_30_temp',days_30_temp,'Amend_Pending_Part',1)
    SolaceViewVars('days_60_temp',days_60_temp,'Amend_Pending_Part',1)
    SolaceViewVars('days_90_temp',days_90_temp,'Amend_Pending_Part',1)
    SolaceViewVars('average_text_temp',average_text_temp,'Amend_Pending_Part',1)
    SolaceViewVars('average_temp',average_temp,'Amend_Pending_Part',1)
    SolaceViewVars('tmp:CostsChanged',tmp:CostsChanged,'Amend_Pending_Part',1)
    SolaceViewVars('save_sto_id',save_sto_id,'Amend_Pending_Part',1)
    SolaceViewVars('save_loc_id',save_loc_id,'Amend_Pending_Part',1)
    SolaceViewVars('save_shi_id',save_shi_id,'Amend_Pending_Part',1)
    SolaceViewVars('save_res_id',save_res_id,'Amend_Pending_Part',1)
    SolaceViewVars('part_record_number_temp',part_record_number_temp,'Amend_Pending_Part',1)
    SolaceViewVars('retail_cost_temp',retail_cost_temp,'Amend_Pending_Part',1)
    SolaceViewVars('saved_retail_cost_temp',saved_retail_cost_temp,'Amend_Pending_Part',1)
    SolaceViewVars('saved_purchase_cost_temp',saved_purchase_cost_temp,'Amend_Pending_Part',1)
    SolaceViewVars('saved_sale_cost_temp',saved_sale_cost_temp,'Amend_Pending_Part',1)
    SolaceViewVars('saved_quantity_temp',saved_quantity_temp,'Amend_Pending_Part',1)
    SolaceViewVars('save_par_id',save_par_id,'Amend_Pending_Part',1)
    SolaceViewVars('supplier_temp',supplier_temp,'Amend_Pending_Part',1)
    SolaceViewVars('Purchase_Cost_temp',Purchase_Cost_temp,'Amend_Pending_Part',1)
    SolaceViewVars('Sale_Cost_temp',Sale_Cost_temp,'Amend_Pending_Part',1)
    SolaceViewVars('save_wpr_id',save_wpr_id,'Amend_Pending_Part',1)
    SolaceViewVars('Quantity_Temp',Quantity_Temp,'Amend_Pending_Part',1)


BuildCtrlQueue      Routine
  Free(LSolCtrlQ)
  SolaceUseRef = ?Sheet1;  SolaceCtrlName = '?Sheet1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab1;  SolaceCtrlName = '?Tab1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OPE:Part_Number:Prompt;  SolaceCtrlName = '?OPE:Part_Number:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ope:Part_Number;  SolaceCtrlName = '?ope:Part_Number';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OPE:Description:Prompt;  SolaceCtrlName = '?OPE:Description:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?ope:Description;  SolaceCtrlName = '?ope:Description';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?supplier_temp;  SolaceCtrlName = '?supplier_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Purchase_Cost_temp:Prompt;  SolaceCtrlName = '?Purchase_Cost_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Purchase_Cost_temp;  SolaceCtrlName = '?Purchase_Cost_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sale_Cost_temp:Prompt;  SolaceCtrlName = '?Sale_Cost_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sale_Cost_temp;  SolaceCtrlName = '?Sale_Cost_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retail_cost_temp:Prompt;  SolaceCtrlName = '?retail_cost_temp:Prompt';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?retail_cost_temp;  SolaceCtrlName = '?retail_cost_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Quantity_Temp;  SolaceCtrlName = '?Quantity_Temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt5;  SolaceCtrlName = '?Prompt5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt2;  SolaceCtrlName = '?Prompt2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Sheet2;  SolaceCtrlName = '?Sheet2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Tab3;  SolaceCtrlName = '?Tab3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6;  SolaceCtrlName = '?Prompt6';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_7_temp;  SolaceCtrlName = '?days_7_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:2;  SolaceCtrlName = '?Prompt6:2';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_30_temp;  SolaceCtrlName = '?days_30_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:3;  SolaceCtrlName = '?Prompt6:3';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_60_temp;  SolaceCtrlName = '?days_60_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:4;  SolaceCtrlName = '?Prompt6:4';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?days_90_temp;  SolaceCtrlName = '?days_90_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Line1;  SolaceCtrlName = '?Line1';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Prompt6:5;  SolaceCtrlName = '?Prompt6:5';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?average_text_temp;  SolaceCtrlName = '?average_text_temp';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?OkButton;  SolaceCtrlName = '?OkButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?CancelButton;  SolaceCtrlName = '?CancelButton';Add(LSolCtrlQ,+SolaceUseRef)
  SolaceUseRef = ?Panel1;  SolaceCtrlName = '?Panel1';Add(LSolCtrlQ,+SolaceUseRef)


! After Embed Point: %ProcedureRoutines) DESC(Procedure Routines) ARG()

ThisWindow.Init PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ! Before Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  GlobalErrors.SetProcedureName('Amend_Pending_Part')
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'LogProc',0,'Amend_Pending_Part')      !Add Procedure to Log
      end
    
    
  SELF.Request = GlobalRequest
  ReturnValue = PARENT.Init()
  IF ReturnValue THEN RETURN ReturnValue.
  SELF.FirstField = ?OPE:Part_Number:Prompt
  SELF.VCRRequest &= VCRRequest
  SELF.Errors &= GlobalErrors
  SELF.AddItem(Toolbar)
  CLEAR(GlobalRequest)
  CLEAR(GlobalResponse)
  Relate:LOCATION.Open
  Relate:RETSALES.Open
  Relate:STOCK_ALIAS.Open
  Access:ORDPEND.UseFile
  Access:PARTS.UseFile
  Access:WARPARTS.UseFile
  Access:STOCK.UseFile
  Access:RETSTOCK.UseFile
  Access:TRADEACC.UseFile
  Access:SUBTRACC.UseFile
  Access:STOHIST.UseFile
  SELF.FilesOpened = True
  OPEN(window)
  SELF.Opened=True
  access:ordpend.clearkey(ope:ref_number_key)
  ope:ref_number = glo:select1
  if access:ordpend.fetch(ope:ref_number_key)
      Return Level:Fatal
  Else!if access:ordpend.fetch(ope:ref_number_key)
      supplier_temp   = ope:supplier
      quantity_temp   = ope:quantity
      ref_number# = ope:part_ref_number
      Include('stockuse.inc')
      Case ope:part_type                                                                      !Work out the costs.
          Of 'JOB'                                                                            !'JOB' = Chargeable Part
              save_par_id = access:parts.savefile()
              access:parts.clearkey(par:PendingRefNoKey)
              par:ref_number         = ope:job_number
              par:pending_ref_number = ope:ref_number
              set(par:PendingRefNoKey,par:PendingRefNoKey)
              loop
                  if access:parts.next()
                     break
                  end !if
                  if par:ref_number         <> ope:job_number      |
                  or par:pending_ref_number <> ope:ref_number      |
                      then break.  ! end if
                  If par:part_number  = ope:part_number
                      purchase_cost_temp   = par:purchase_cost
                      sale_cost_temp      = par:sale_cost
                      retail_cost_temp    = par:retail_cost
                      part_record_number_temp = par:record_number
                      Break
                  End!If par:part_number  = ope:part_number
              end !loop
              access:parts.restorefile(save_par_id)
          Of 'WAR'                                                                            !'WAR' = Warranty Part
              save_wpr_id = access:warparts.savefile()
              access:warparts.clearkey(wpr:PendingRefNoKey)
              wpr:ref_number         = ope:job_number
              wpr:pending_ref_number = ope:ref_number
              set(wpr:PendingRefNoKey,wpr:PendingRefNoKey)
              loop
                  if access:warparts.next()
                     break
                  end !if
                  if wpr:ref_number         <> ope:job_number      |
                  or wpr:pending_ref_number <> ope:ref_number      |
                      then break.  ! end if
                  If wpr:part_number  = ope:part_number
                      purchase_cost_temp  = wpr:purchase_cost
                      sale_cost_temp      = wpr:sale_cost
                      retail_cost_temp    = wpr:retail_cost
                      part_record_number_temp = wpr:record_number
                      Break
                  End!If wpr:part_number  = ope:part_number
              end !loop
              access:warparts.restorefile(save_wpr_id)
          Of 'STO'                                                                            !'STO' = Stock Part
              access:stock.clearkey(sto:ref_number_key)
              sto:ref_number = ope:part_ref_number
              if access:stock.fetch(sto:ref_number_key) = Level:Benign
                  purchase_cost_temp  = sto:purchase_cost
                  sale_cost_temp      = sto:sale_cost
                  retail_cost_temp    = sto:retail_cost
              End!if access:stock.fetch(sto:ref_number_key)
          Of 'RET'
              save_res_id = access:retstock.savefile()
              access:retstock.clearkey(res:pending_ref_number_key)
              res:ref_number         = ope:job_number
              res:pending_ref_number = ope:ref_number
              set(res:pending_ref_number_key,res:pending_ref_number_key)
              loop
                  if access:retstock.next()
                     break
                  end !if
                  if res:ref_number         <> ope:job_number      |
                  or res:pending_ref_number <> ope:ref_number      |
                      then break.  ! end if
                  If res:part_number  = ope:part_number
                      purchase_cost_temp  = res:purchase_cost
                      sale_cost_temp      = res:sale_cost
                      retail_cost_temp    = res:retail_cost
                      part_record_number_temp    = res:record_number
                      Break
                  End!If res:part_number  = ope:part_number
              end !loop
              access:retstock.restorefile(save_res_id)
      End!Case ope:part_type
  End!if access:ordpend.fetch(ope:ref_number_key)
  saved_purchase_cost_temp    = purchase_cost_temp
  saved_sale_cost_temp        = sale_cost_temp
  saved_quantity_temp         = quantity_temp
  saved_retail_cost_temp      = retail_cost_temp
  Do RecolourWindow
  ! support for CPCS
  FDCB2.Init(supplier_temp,?supplier_temp,Queue:FileDropCombo.ViewPosition,FDCB2::View:FileDropCombo,Queue:FileDropCombo,Relate:SUPPLIER,ThisWindow,GlobalErrors,0,1,0)
  FDCB2.Q &= Queue:FileDropCombo
  FDCB2.AddSortOrder(sup:Company_Name_Key)
  FDCB2.AddField(sup:Company_Name,FDCB2.Q.sup:Company_Name)
  FDCB2.AddField(sup:RecordNumber,FDCB2.Q.sup:RecordNumber)
  ThisWindow.AddItem(FDCB2.WindowComponent)
  FDCB2.DefaultFill = 0
  SELF.SetAlerts()
      Alert(CtrlF12)                     !Alert key to activate Solace VariView Toolbox
      if GLO:SolaceViewVariables = true then  !If toolbox is active
        do SolaceVariView                     !Refresh Toolbox
        do BuildCtrlQueue
      end
    
  ! After Embed Point: %WindowManagerMethodCodeSection) DESC(WindowManager Method Executable Code Section) ARG(Init, (),BYTE)
  RETURN ReturnValue


ThisWindow.Kill PROCEDURE

ReturnValue          BYTE,AUTO

  CODE
  ReturnValue = PARENT.Kill()
  IF ReturnValue THEN RETURN ReturnValue.
  IF SELF.FilesOpened
    Relate:LOCATION.Close
    Relate:RETSALES.Close
    Relate:STOCK_ALIAS.Close
  END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        SolaceViewVars('',0,'Amend_Pending_Part',3)               !Remove Local variables
        SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
      end
    
  GlobalErrors.SetProcedureName
  RETURN ReturnValue


ThisWindow.TakeAccepted PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeAccepted()
    CASE ACCEPTED()
    OF ?OkButton
      ThisWindow.Update
      ! Before Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
      If supplier_temp    <> ope:supplier Or |
          purchase_cost_temp <> saved_purchase_cost_temp Or |
          sale_cost_temp <> saved_sale_cost_temp Or |
          retail_cost_temp <> saved_retail_cost_temp Or |
          quantity_temp <> saved_quantity_temp
          Case MessageEx('Are you sure you want to amend the details of this Pending Order?','ServiceBase 2000',|
                         'Styles\question.ico','|&Yes|&No',2,2,'',,'Tahoma',8,0,400,CHARSET:ANSI,12632256,'',0,beep:systemquestion,msgex:samewidths,84,26,0) 
              Of 1 ! &Yes Button
                  If supplier_temp <> ope:supplier
                      ope:supplier    = supplier_temp                                                 !If you change the supplier to one
                      access:ordpend.update()                                                         !that doesn't exist in the temp
                      access:supptemp.clearkey(suptmp:company_name_key)                               !supplier. Need to add the new one.
                      suptmp:company_name = supplier_temp
                      if access:supptemp.fetch(suptmp:company_name_key)
                          access:supplier.clearkey(sup:company_name_key)
                          sup:company_name = supplier_temp
                          if access:supplier.fetch(sup:company_name_key) = Level:benign
                              get(supptemp,0)
                              if access:supptemp.primerecord() = level:benign
                                  suptmp:record              :=: sup:record
                                  access:supptemp.tryinsert()
                              End!if access:supptemp.primerecord() = level:benign
          
                          End!if access:supplier.fetch(sup:company_name_key) = Level:benign
                      end!if access:supptemp.fetch(suptmp:company_name_key)
                  End!If supplier_temp <> ope:supplier
                  If sale_cost_temp <> saved_sale_cost_temp or purchase_cost_temp <> saved_purchase_cost_temp
                      Case ope:part_type
                          Of 'JOB'
                              access:parts.clearkey(par:RecordNumberKey)
                              par:record_number = part_record_number_temp
                              if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                                  par:sale_cost   = sale_cost_temp
                                  par:purchase_cost   = purchase_cost_temp
                                  par:retail_cost = retail_cost_temp
                                  access:parts.update()
                              End!if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                                       
                          Of 'WAR'
                              access:warparts.clearkey(wpr:RecordNumberKey)
                              wpr:record_number = part_record_number_temp
                              if access:warparts.fetch(wpr:RecordNumberKey) = Level:Benign
                                  wpr:sale_cost   = sale_cost_temp
                                  wpr:purchase_cost   = purchase_cost_temp
                                  wpr:quantity        = quantity_temp
                                  wpr:retail_cost      = retail_cost_temp
                                  access:warparts.update()
                              End!if access:parts.fetch(par:RecordNumberKey) = Level:Benign
                          Of 'STO'
                              access:stock.clearkey(sto:ref_number_key)
                              sto:ref_number = ope:part_ref_number
                              if access:stock.fetch(sto:ref_number_key) = Level:Benign
                                  sto:purchase_cost   = purchase_cost_temp
                                  sto:sale_cost       = sale_cost_temp
                                  sto:retail_cost     = retail_cost_temp
                                  access:stock.update()
                              End!if access:stock.fetch(sto:ref_number_key) = Level:Benign
                              Do replicate
                          OF 'RET'
          
          
                              access:retstock.clearkey(res:record_number_key)
                              res:record_number = part_record_number_temp
                              if access:retstock.tryfetch(res:record_number_key) = Level:Benign
                                  access:retsales.clearkey(ret:ref_number_key)
                                  ret:ref_number = res:ref_number
                                  if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
                                      res:sale_cost   = sale_cost_temp
                                      res:purchase_cost   = purchase_cost_temp
                                      res:retail_cost = retail_cost_temp
                                      res:quantity    = quantity_temp
          
                                      access:subtracc.clearkey(sub:account_number_key)
                                      sub:account_number = ret:account_number
                                      if access:subtracc.fetch(sub:account_number_key) = level:benign
                                          access:tradeacc.clearkey(tra:account_number_key) 
                                          tra:account_number = sub:main_account_number
                                          if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                              if tra:invoice_sub_accounts = 'YES'
                                                  Case sub:retail_price_structure
                                                      Of 'RET'
                                                          res:item_cost   = res:retail_cost
                                                      Else
                                                          res:item_cost   = res:Sale_cost
                                                  End!Case sub:retail_payment_type
          
                                              else!if tra:use_sub_accounts = 'YES'
                                                  Case tra:retail_price_structure
                                                      Of 'RET'
                                                          res:item_cost   = res:retail_cost
                                                      Else
                                                          res:item_cost   = res:Sale_cost
                                                  End!Case tra:retail_payment_Type
                                              end!if tra:use_sub_accounts = 'YES'
                                          end!if access:tradeacc.fetch(tra:account_number_key) = level:benign
                                      end!if access:subtracc.fetch(sub:account_number_key) = level:benign
          
                                      access:retstock.update()
                                  End!if access:retsales.tryfetch(ret:ref_number_key) = Level:Benign
          
                              End!if access:retstock.tryfetch(res:record_number_key) = Level:Benign
                      End!Case ope:part_type
                  End!If sale_cost_temp <> saved_sale_cost_temp or purchase_cost_temp <> saved_purchase_cost_temp
                  If quantity_temp <> saved_quantity_temp
                      ope:quantity    = quantity_temp
                      access:ordpend.update()
                  End!If quantity_temp <> saved_quantity_temp
                  Post(event:closewindow)
              Of 2 ! &No Button
          End!Case MessageEx
      
      End!If
      ! After Embed Point: %ControlEventHandling) DESC(Control Event Handling) ARG(?OkButton, Accepted)
    END
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        If 0{Prop:AcceptAll} = False then
          if (SolaceCurrentTab = 8 and SolaceNoRefreshEvents = true) or SolaceNoRefreshEvents = false then
            SolaceIgnoreEvent# = false
            if SolaceIgnoreEvent# = False then
              LSolCtrlQ.SolaceUseRef = Field()
              Get(LSolCtrlQ,LSolCtrlQ.SolaceUseRef)
              if Error() then
                Clear(LSolCtrlQ)
              end
              SolaceViewVars(LSolCtrlQ.SolaceCtrlName,Event(),'EventRefresh',0,'Amend_Pending_Part')        !Refresh Toolbox event
            end
            if Event() = Event:Resume then
              SolaceViewVars('',0,'SolaceGlobalRefresh',0)      !Refresh file status
            end
          else
            if Event() = Event:Resume then
              do SolaceVariView
            end
          end
        end
      end
    
  ReturnValue = PARENT.TakeEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeFieldEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
      if GLO:SolaceViewVariables = true then   !If toolbox is active
        do SolaceVariView                      !Refresh Toolbox
      end
    
    
  ReturnValue = PARENT.TakeFieldEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue


ThisWindow.TakeWindowEvent PROCEDURE

ReturnValue          BYTE,AUTO

Looped BYTE
  CODE
  LOOP
    IF Looped
      RETURN Level:Notify
    ELSE
      Looped = 1
    END
    CASE EVENT()
    OF EVENT:AlertKey
        if Keycode() = CtrlF12 then
          do SolaceVariView
          if GLO:SolaceViewVariables = false then
            SolaceThreadNo = -1
            GLO:SolaceViewVariables = true
            DebugToolbox('')
          end
          do BuildCtrlQueue
        End
        
        
        
    END
  ReturnValue = PARENT.TakeWindowEvent()
    RETURN ReturnValue
  END
  ReturnValue = Level:Fatal
  RETURN ReturnValue

